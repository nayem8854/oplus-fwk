package android.inputmethodservice;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.util.Xml;
import com.android.internal.R;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@Deprecated
public class Keyboard {
  private Key[] mShiftKeys = new Key[] { null, null };
  
  private int[] mShiftKeyIndices = new int[] { -1, -1 };
  
  private static float SEARCH_DISTANCE = 1.8F;
  
  private ArrayList<Row> rows = new ArrayList<>();
  
  public static final int EDGE_BOTTOM = 8;
  
  public static final int EDGE_LEFT = 1;
  
  public static final int EDGE_RIGHT = 2;
  
  public static final int EDGE_TOP = 4;
  
  private static final int GRID_HEIGHT = 5;
  
  private static final int GRID_SIZE = 50;
  
  private static final int GRID_WIDTH = 10;
  
  public static final int KEYCODE_ALT = -6;
  
  public static final int KEYCODE_CANCEL = -3;
  
  public static final int KEYCODE_DELETE = -5;
  
  public static final int KEYCODE_DONE = -4;
  
  public static final int KEYCODE_MODE_CHANGE = -2;
  
  public static final int KEYCODE_SHIFT = -1;
  
  static final String TAG = "Keyboard";
  
  private static final String TAG_KEY = "Key";
  
  private static final String TAG_KEYBOARD = "Keyboard";
  
  private static final String TAG_ROW = "Row";
  
  private int mCellHeight;
  
  private int mCellWidth;
  
  private int mDefaultHeight;
  
  private int mDefaultHorizontalGap;
  
  private int mDefaultVerticalGap;
  
  private int mDefaultWidth;
  
  private int mDisplayHeight;
  
  private int mDisplayWidth;
  
  private int[][] mGridNeighbors;
  
  private int mKeyHeight;
  
  private int mKeyWidth;
  
  private int mKeyboardMode;
  
  private List<Key> mKeys;
  
  private CharSequence mLabel;
  
  private List<Key> mModifierKeys;
  
  private int mProximityThreshold;
  
  private boolean mShifted;
  
  private int mTotalHeight;
  
  private int mTotalWidth;
  
  public static class Row {
    public int defaultHeight;
    
    public int defaultHorizontalGap;
    
    public int defaultWidth;
    
    ArrayList<Keyboard.Key> mKeys = new ArrayList<>();
    
    public int mode;
    
    private Keyboard parent;
    
    public int rowEdgeFlags;
    
    public int verticalGap;
    
    public Row(Keyboard param1Keyboard) {
      this.parent = param1Keyboard;
    }
    
    public Row(Resources param1Resources, Keyboard param1Keyboard, XmlResourceParser param1XmlResourceParser) {
      this.parent = param1Keyboard;
      TypedArray typedArray2 = param1Resources.obtainAttributes(Xml.asAttributeSet((XmlPullParser)param1XmlResourceParser), R.styleable.Keyboard);
      int i = param1Keyboard.mDisplayWidth, j = param1Keyboard.mDefaultWidth;
      this.defaultWidth = Keyboard.getDimensionOrFraction(typedArray2, 0, i, j);
      i = param1Keyboard.mDisplayHeight;
      j = param1Keyboard.mDefaultHeight;
      this.defaultHeight = Keyboard.getDimensionOrFraction(typedArray2, 1, i, j);
      i = param1Keyboard.mDisplayWidth;
      j = param1Keyboard.mDefaultHorizontalGap;
      this.defaultHorizontalGap = Keyboard.getDimensionOrFraction(typedArray2, 2, i, j);
      i = param1Keyboard.mDisplayHeight;
      j = param1Keyboard.mDefaultVerticalGap;
      this.verticalGap = Keyboard.getDimensionOrFraction(typedArray2, 3, i, j);
      typedArray2.recycle();
      TypedArray typedArray1 = param1Resources.obtainAttributes(Xml.asAttributeSet((XmlPullParser)param1XmlResourceParser), R.styleable.Keyboard_Row);
      this.rowEdgeFlags = typedArray1.getInt(0, 0);
      this.mode = typedArray1.getResourceId(1, 0);
    }
  }
  
  public static class Key {
    private static final int[] KEY_STATE_NORMAL;
    
    private static final int[] KEY_STATE_NORMAL_OFF = new int[] { 16842911 };
    
    private static final int[] KEY_STATE_NORMAL_ON = new int[] { 16842911, 16842912 };
    
    private static final int[] KEY_STATE_PRESSED;
    
    private static final int[] KEY_STATE_PRESSED_OFF = new int[] { 16842919, 16842911 };
    
    private static final int[] KEY_STATE_PRESSED_ON = new int[] { 16842919, 16842911, 16842912 };
    
    public int[] codes;
    
    public int edgeFlags;
    
    public int gap;
    
    public int height;
    
    public Drawable icon;
    
    public Drawable iconPreview;
    
    private Keyboard keyboard;
    
    public CharSequence label;
    
    public boolean modifier;
    
    public boolean on;
    
    public CharSequence popupCharacters;
    
    public int popupResId;
    
    public boolean pressed;
    
    public boolean repeatable;
    
    public boolean sticky;
    
    public CharSequence text;
    
    public int width;
    
    public int x;
    
    public int y;
    
    static {
      KEY_STATE_NORMAL = new int[0];
      KEY_STATE_PRESSED = new int[] { 16842919 };
    }
    
    public Key(Keyboard.Row param1Row) {
      this.keyboard = param1Row.parent;
      this.height = param1Row.defaultHeight;
      this.width = param1Row.defaultWidth;
      this.gap = param1Row.defaultHorizontalGap;
      this.edgeFlags = param1Row.rowEdgeFlags;
    }
    
    public Key(Resources param1Resources, Keyboard.Row param1Row, int param1Int1, int param1Int2, XmlResourceParser param1XmlResourceParser) {
      this(param1Row);
      this.x = param1Int1;
      this.y = param1Int2;
      TypedArray typedArray2 = param1Resources.obtainAttributes(Xml.asAttributeSet((XmlPullParser)param1XmlResourceParser), R.styleable.Keyboard);
      Keyboard keyboard = this.keyboard;
      param1Int2 = keyboard.mDisplayWidth;
      param1Int1 = param1Row.defaultWidth;
      this.width = Keyboard.getDimensionOrFraction(typedArray2, 0, param1Int2, param1Int1);
      keyboard = this.keyboard;
      param1Int2 = keyboard.mDisplayHeight;
      param1Int1 = param1Row.defaultHeight;
      this.height = Keyboard.getDimensionOrFraction(typedArray2, 1, param1Int2, param1Int1);
      keyboard = this.keyboard;
      param1Int2 = keyboard.mDisplayWidth;
      param1Int1 = param1Row.defaultHorizontalGap;
      this.gap = Keyboard.getDimensionOrFraction(typedArray2, 2, param1Int2, param1Int1);
      typedArray2.recycle();
      TypedArray typedArray1 = param1Resources.obtainAttributes(Xml.asAttributeSet((XmlPullParser)param1XmlResourceParser), R.styleable.Keyboard_Key);
      this.x += this.gap;
      TypedValue typedValue = new TypedValue();
      typedArray1.getValue(0, typedValue);
      if (typedValue.type == 16 || typedValue.type == 17) {
        this.codes = new int[] { typedValue.data };
      } else if (typedValue.type == 3) {
        this.codes = parseCSV(typedValue.string.toString());
      } 
      Drawable drawable2 = typedArray1.getDrawable(7);
      if (drawable2 != null) {
        param1Int1 = drawable2.getIntrinsicWidth();
        Drawable drawable = this.iconPreview;
        param1Int2 = drawable.getIntrinsicHeight();
        drawable2.setBounds(0, 0, param1Int1, param1Int2);
      } 
      this.popupCharacters = typedArray1.getText(2);
      this.popupResId = typedArray1.getResourceId(1, 0);
      this.repeatable = typedArray1.getBoolean(6, false);
      this.modifier = typedArray1.getBoolean(4, false);
      this.sticky = typedArray1.getBoolean(5, false);
      this.edgeFlags = param1Int1 = typedArray1.getInt(3, 0);
      this.edgeFlags = param1Int1 | param1Row.rowEdgeFlags;
      Drawable drawable1 = typedArray1.getDrawable(10);
      if (drawable1 != null)
        drawable1.setBounds(0, 0, drawable1.getIntrinsicWidth(), this.icon.getIntrinsicHeight()); 
      this.label = typedArray1.getText(9);
      this.text = typedArray1.getText(8);
      if (this.codes == null && !TextUtils.isEmpty(this.label))
        this.codes = new int[] { this.label.charAt(0) }; 
      typedArray1.recycle();
    }
    
    public void onPressed() {
      this.pressed ^= 0x1;
    }
    
    public void onReleased(boolean param1Boolean) {
      this.pressed ^= 0x1;
      if (this.sticky && param1Boolean)
        this.on ^= 0x1; 
    }
    
    int[] parseCSV(String param1String) {
      int i = 0;
      int j = 0;
      if (param1String.length() > 0) {
        int k = 0 + 1;
        while (true) {
          int m = param1String.indexOf(",", j + 1);
          i = k;
          if (m > 0) {
            k++;
            continue;
          } 
          break;
        } 
      } 
      int[] arrayOfInt = new int[i];
      byte b = 0;
      StringTokenizer stringTokenizer = new StringTokenizer(param1String, ",");
      for (; stringTokenizer.hasMoreTokens(); b++) {
        try {
          arrayOfInt[b] = Integer.parseInt(stringTokenizer.nextToken());
        } catch (NumberFormatException numberFormatException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error parsing keycodes ");
          stringBuilder.append(param1String);
          Log.e("Keyboard", stringBuilder.toString());
        } 
      } 
      return arrayOfInt;
    }
    
    public boolean isInside(int param1Int1, int param1Int2) {
      int i;
      boolean bool1, bool2, bool3;
      if ((this.edgeFlags & 0x1) > 0) {
        i = 1;
      } else {
        i = 0;
      } 
      if ((this.edgeFlags & 0x2) > 0) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      if ((this.edgeFlags & 0x4) > 0) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((this.edgeFlags & 0x8) > 0) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      int j = this.x;
      if (param1Int1 >= j || (i && param1Int1 <= j + this.width)) {
        i = this.x;
        if (param1Int1 < this.width + i || (bool1 && param1Int1 >= i)) {
          param1Int1 = this.y;
          if (param1Int2 >= param1Int1 || (bool2 && param1Int2 <= param1Int1 + this.height)) {
            param1Int1 = this.y;
            if (param1Int2 < this.height + param1Int1 || (bool3 && param1Int2 >= param1Int1))
              return true; 
          } 
        } 
      } 
      return false;
    }
    
    public int squaredDistanceFrom(int param1Int1, int param1Int2) {
      param1Int1 = this.x + this.width / 2 - param1Int1;
      param1Int2 = this.y + this.height / 2 - param1Int2;
      return param1Int1 * param1Int1 + param1Int2 * param1Int2;
    }
    
    public int[] getCurrentDrawableState() {
      int[] arrayOfInt = KEY_STATE_NORMAL;
      if (this.on) {
        if (this.pressed) {
          arrayOfInt = KEY_STATE_PRESSED_ON;
        } else {
          arrayOfInt = KEY_STATE_NORMAL_ON;
        } 
      } else if (this.sticky) {
        if (this.pressed) {
          arrayOfInt = KEY_STATE_PRESSED_OFF;
        } else {
          arrayOfInt = KEY_STATE_NORMAL_OFF;
        } 
      } else if (this.pressed) {
        arrayOfInt = KEY_STATE_PRESSED;
      } 
      return arrayOfInt;
    }
  }
  
  public Keyboard(Context paramContext, int paramInt) {
    this(paramContext, paramInt, 0);
  }
  
  public Keyboard(Context paramContext, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mDisplayWidth = paramInt3;
    this.mDisplayHeight = paramInt4;
    this.mDefaultHorizontalGap = 0;
    this.mDefaultWidth = paramInt3 /= 10;
    this.mDefaultVerticalGap = 0;
    this.mDefaultHeight = paramInt3;
    this.mKeys = new ArrayList<>();
    this.mModifierKeys = new ArrayList<>();
    this.mKeyboardMode = paramInt2;
    loadKeyboard(paramContext, paramContext.getResources().getXml(paramInt1));
  }
  
  public Keyboard(Context paramContext, int paramInt1, int paramInt2) {
    DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
    this.mDisplayWidth = displayMetrics.widthPixels;
    this.mDisplayHeight = displayMetrics.heightPixels;
    this.mDefaultHorizontalGap = 0;
    int i = this.mDisplayWidth / 10;
    this.mDefaultVerticalGap = 0;
    this.mDefaultHeight = i;
    this.mKeys = new ArrayList<>();
    this.mModifierKeys = new ArrayList<>();
    this.mKeyboardMode = paramInt2;
    loadKeyboard(paramContext, paramContext.getResources().getXml(paramInt1));
  }
  
  public Keyboard(Context paramContext, int paramInt1, CharSequence paramCharSequence, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: iload_2
    //   3: invokespecial <init> : (Landroid/content/Context;I)V
    //   6: iconst_0
    //   7: istore #6
    //   9: iconst_0
    //   10: istore #7
    //   12: iconst_0
    //   13: istore_2
    //   14: aload_0
    //   15: iconst_0
    //   16: putfield mTotalWidth : I
    //   19: new android/inputmethodservice/Keyboard$Row
    //   22: dup
    //   23: aload_0
    //   24: invokespecial <init> : (Landroid/inputmethodservice/Keyboard;)V
    //   27: astore #8
    //   29: aload #8
    //   31: aload_0
    //   32: getfield mDefaultHeight : I
    //   35: putfield defaultHeight : I
    //   38: aload #8
    //   40: aload_0
    //   41: getfield mDefaultWidth : I
    //   44: putfield defaultWidth : I
    //   47: aload #8
    //   49: aload_0
    //   50: getfield mDefaultHorizontalGap : I
    //   53: putfield defaultHorizontalGap : I
    //   56: aload #8
    //   58: aload_0
    //   59: getfield mDefaultVerticalGap : I
    //   62: putfield verticalGap : I
    //   65: aload #8
    //   67: bipush #12
    //   69: putfield rowEdgeFlags : I
    //   72: iload #4
    //   74: iconst_m1
    //   75: if_icmpne -> 85
    //   78: ldc 2147483647
    //   80: istore #9
    //   82: goto -> 89
    //   85: iload #4
    //   87: istore #9
    //   89: iconst_0
    //   90: istore #10
    //   92: iload #6
    //   94: istore #4
    //   96: iload #10
    //   98: aload_3
    //   99: invokeinterface length : ()I
    //   104: if_icmpge -> 273
    //   107: aload_3
    //   108: iload #10
    //   110: invokeinterface charAt : (I)C
    //   115: istore #11
    //   117: iload_2
    //   118: iload #9
    //   120: if_icmpge -> 148
    //   123: iload #4
    //   125: istore #12
    //   127: iload #7
    //   129: istore #6
    //   131: aload_0
    //   132: getfield mDefaultWidth : I
    //   135: iload #4
    //   137: iadd
    //   138: iload #5
    //   140: iadd
    //   141: aload_0
    //   142: getfield mDisplayWidth : I
    //   145: if_icmple -> 167
    //   148: iconst_0
    //   149: istore #12
    //   151: iload #7
    //   153: aload_0
    //   154: getfield mDefaultVerticalGap : I
    //   157: aload_0
    //   158: getfield mDefaultHeight : I
    //   161: iadd
    //   162: iadd
    //   163: istore #6
    //   165: iconst_0
    //   166: istore_2
    //   167: new android/inputmethodservice/Keyboard$Key
    //   170: dup
    //   171: aload #8
    //   173: invokespecial <init> : (Landroid/inputmethodservice/Keyboard$Row;)V
    //   176: astore_1
    //   177: aload_1
    //   178: iload #12
    //   180: putfield x : I
    //   183: aload_1
    //   184: iload #6
    //   186: putfield y : I
    //   189: aload_1
    //   190: iload #11
    //   192: invokestatic valueOf : (C)Ljava/lang/String;
    //   195: putfield label : Ljava/lang/CharSequence;
    //   198: aload_1
    //   199: iconst_1
    //   200: newarray int
    //   202: dup
    //   203: iconst_0
    //   204: iload #11
    //   206: iastore
    //   207: putfield codes : [I
    //   210: iinc #2, 1
    //   213: iload #12
    //   215: aload_1
    //   216: getfield width : I
    //   219: aload_1
    //   220: getfield gap : I
    //   223: iadd
    //   224: iadd
    //   225: istore #4
    //   227: aload_0
    //   228: getfield mKeys : Ljava/util/List;
    //   231: aload_1
    //   232: invokeinterface add : (Ljava/lang/Object;)Z
    //   237: pop
    //   238: aload #8
    //   240: getfield mKeys : Ljava/util/ArrayList;
    //   243: aload_1
    //   244: invokevirtual add : (Ljava/lang/Object;)Z
    //   247: pop
    //   248: iload #4
    //   250: aload_0
    //   251: getfield mTotalWidth : I
    //   254: if_icmple -> 263
    //   257: aload_0
    //   258: iload #4
    //   260: putfield mTotalWidth : I
    //   263: iinc #10, 1
    //   266: iload #6
    //   268: istore #7
    //   270: goto -> 96
    //   273: aload_0
    //   274: aload_0
    //   275: getfield mDefaultHeight : I
    //   278: iload #7
    //   280: iadd
    //   281: putfield mTotalHeight : I
    //   284: aload_0
    //   285: getfield rows : Ljava/util/ArrayList;
    //   288: aload #8
    //   290: invokevirtual add : (Ljava/lang/Object;)Z
    //   293: pop
    //   294: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #598	-> 0
    //   #599	-> 6
    //   #600	-> 9
    //   #601	-> 12
    //   #602	-> 14
    //   #604	-> 19
    //   #605	-> 29
    //   #606	-> 38
    //   #607	-> 47
    //   #608	-> 56
    //   #609	-> 65
    //   #610	-> 72
    //   #611	-> 89
    //   #612	-> 107
    //   #613	-> 117
    //   #615	-> 148
    //   #616	-> 151
    //   #617	-> 165
    //   #619	-> 167
    //   #620	-> 177
    //   #621	-> 183
    //   #622	-> 189
    //   #623	-> 198
    //   #624	-> 210
    //   #625	-> 213
    //   #626	-> 227
    //   #627	-> 238
    //   #628	-> 248
    //   #629	-> 257
    //   #611	-> 263
    //   #632	-> 273
    //   #633	-> 284
    //   #634	-> 294
  }
  
  final void resize(int paramInt1, int paramInt2) {
    int i = this.rows.size();
    for (paramInt2 = 0; paramInt2 < i; paramInt2++) {
      Row row = this.rows.get(paramInt2);
      int j = row.mKeys.size();
      int k = 0;
      int m = 0;
      byte b;
      for (b = 0; b < j; b++, k = n) {
        Key key = row.mKeys.get(b);
        int n = k;
        if (b > 0)
          n = k + key.gap; 
        m += key.width;
      } 
      if (k + m > paramInt1) {
        boolean bool = false;
        float f = (paramInt1 - k) / m;
        for (b = 0, m = bool; b < j; b++) {
          Key key = row.mKeys.get(b);
          key.width = (int)(key.width * f);
          key.x = m;
          m += key.width + key.gap;
        } 
      } 
    } 
    this.mTotalWidth = paramInt1;
  }
  
  public List<Key> getKeys() {
    return this.mKeys;
  }
  
  public List<Key> getModifierKeys() {
    return this.mModifierKeys;
  }
  
  protected int getHorizontalGap() {
    return this.mDefaultHorizontalGap;
  }
  
  protected void setHorizontalGap(int paramInt) {
    this.mDefaultHorizontalGap = paramInt;
  }
  
  protected int getVerticalGap() {
    return this.mDefaultVerticalGap;
  }
  
  protected void setVerticalGap(int paramInt) {
    this.mDefaultVerticalGap = paramInt;
  }
  
  protected int getKeyHeight() {
    return this.mDefaultHeight;
  }
  
  protected void setKeyHeight(int paramInt) {
    this.mDefaultHeight = paramInt;
  }
  
  protected int getKeyWidth() {
    return this.mDefaultWidth;
  }
  
  protected void setKeyWidth(int paramInt) {
    this.mDefaultWidth = paramInt;
  }
  
  public int getHeight() {
    return this.mTotalHeight;
  }
  
  public int getMinWidth() {
    return this.mTotalWidth;
  }
  
  public boolean setShifted(boolean paramBoolean) {
    for (Key key : this.mShiftKeys) {
      if (key != null)
        key.on = paramBoolean; 
    } 
    if (this.mShifted != paramBoolean) {
      this.mShifted = paramBoolean;
      return true;
    } 
    return false;
  }
  
  public boolean isShifted() {
    return this.mShifted;
  }
  
  public int[] getShiftKeyIndices() {
    return this.mShiftKeyIndices;
  }
  
  public int getShiftKeyIndex() {
    return this.mShiftKeyIndices[0];
  }
  
  private void computeNearestNeighbors() {
    this.mCellWidth = (getMinWidth() + 10 - 1) / 10;
    this.mCellHeight = (getHeight() + 5 - 1) / 5;
    this.mGridNeighbors = new int[50][];
    int[] arrayOfInt = new int[this.mKeys.size()];
    int i = this.mCellWidth;
    int j = this.mCellHeight;
    int k;
    for (k = 0; k < i * 10; k += this.mCellWidth) {
      int m;
      for (m = 0; m < j * 5; m += SYNTHETIC_LOCAL_VARIABLE_7) {
        Object object;
        boolean bool = false;
        int n = 0;
        while (true) {
          n++;
          object = SYNTHETIC_LOCAL_VARIABLE_9;
        } 
        int[] arrayOfInt2 = new int[object];
        System.arraycopy(arrayOfInt, 0, arrayOfInt2, 0, object);
        int[][] arrayOfInt1 = this.mGridNeighbors;
        n = this.mCellHeight;
        arrayOfInt1[m / n * 10 + k / this.mCellWidth] = arrayOfInt2;
        continue;
      } 
    } 
  }
  
  public int[] getNearestKeys(int paramInt1, int paramInt2) {
    if (this.mGridNeighbors == null)
      computeNearestNeighbors(); 
    if (paramInt1 >= 0 && paramInt1 < getMinWidth() && paramInt2 >= 0 && paramInt2 < getHeight()) {
      paramInt1 = paramInt2 / this.mCellHeight * 10 + paramInt1 / this.mCellWidth;
      if (paramInt1 < 50)
        return this.mGridNeighbors[paramInt1]; 
    } 
    return new int[0];
  }
  
  protected Row createRowFromXml(Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    return new Row(paramResources, this, paramXmlResourceParser);
  }
  
  protected Key createKeyFromXml(Resources paramResources, Row paramRow, int paramInt1, int paramInt2, XmlResourceParser paramXmlResourceParser) {
    return new Key(paramResources, paramRow, paramInt1, paramInt2, paramXmlResourceParser);
  }
  
  private void loadKeyboard(Context paramContext, XmlResourceParser paramXmlResourceParser) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_1
    //   3: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   6: astore #4
    //   8: iconst_0
    //   9: istore #5
    //   11: iconst_0
    //   12: istore #6
    //   14: iconst_0
    //   15: istore #7
    //   17: iconst_0
    //   18: istore #8
    //   20: iconst_0
    //   21: istore #9
    //   23: aconst_null
    //   24: astore #10
    //   26: aconst_null
    //   27: astore_1
    //   28: aload_2
    //   29: invokeinterface next : ()I
    //   34: istore #11
    //   36: iload #11
    //   38: iconst_1
    //   39: if_icmpeq -> 490
    //   42: iload #11
    //   44: iconst_2
    //   45: if_icmpne -> 351
    //   48: aload_2
    //   49: invokeinterface getName : ()Ljava/lang/String;
    //   54: astore #12
    //   56: ldc 'Row'
    //   58: aload #12
    //   60: invokevirtual equals : (Ljava/lang/Object;)Z
    //   63: istore #13
    //   65: iload #13
    //   67: ifeq -> 158
    //   70: aload_0
    //   71: aload #4
    //   73: aload_2
    //   74: invokevirtual createRowFromXml : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Row;
    //   77: astore_1
    //   78: aload_0
    //   79: getfield rows : Ljava/util/ArrayList;
    //   82: aload_1
    //   83: invokevirtual add : (Ljava/lang/Object;)Z
    //   86: pop
    //   87: aload_1
    //   88: getfield mode : I
    //   91: ifeq -> 119
    //   94: aload_1
    //   95: getfield mode : I
    //   98: istore #6
    //   100: aload_0
    //   101: getfield mKeyboardMode : I
    //   104: istore #8
    //   106: iload #6
    //   108: iload #8
    //   110: if_icmpeq -> 119
    //   113: iconst_1
    //   114: istore #6
    //   116: goto -> 122
    //   119: iconst_0
    //   120: istore #6
    //   122: iload #6
    //   124: ifeq -> 145
    //   127: aload_0
    //   128: aload_2
    //   129: invokespecial skipToEndOfRow : (Landroid/content/res/XmlResourceParser;)V
    //   132: iconst_0
    //   133: istore #6
    //   135: iconst_0
    //   136: istore #8
    //   138: goto -> 348
    //   141: astore_1
    //   142: goto -> 494
    //   145: iconst_0
    //   146: istore #6
    //   148: iconst_1
    //   149: istore #8
    //   151: goto -> 348
    //   154: astore_1
    //   155: goto -> 494
    //   158: ldc 'Key'
    //   160: aload #12
    //   162: invokevirtual equals : (Ljava/lang/Object;)Z
    //   165: istore #13
    //   167: iload #13
    //   169: ifeq -> 331
    //   172: aload_0
    //   173: aload #4
    //   175: aload_1
    //   176: iload #6
    //   178: iload #7
    //   180: aload_2
    //   181: invokevirtual createKeyFromXml : (Landroid/content/res/Resources;Landroid/inputmethodservice/Keyboard$Row;IILandroid/content/res/XmlResourceParser;)Landroid/inputmethodservice/Keyboard$Key;
    //   184: astore #10
    //   186: aload_0
    //   187: getfield mKeys : Ljava/util/List;
    //   190: aload #10
    //   192: invokeinterface add : (Ljava/lang/Object;)Z
    //   197: pop
    //   198: aload #10
    //   200: getfield codes : [I
    //   203: iconst_0
    //   204: iaload
    //   205: iconst_m1
    //   206: if_icmpne -> 283
    //   209: iconst_0
    //   210: istore #9
    //   212: iload #9
    //   214: aload_0
    //   215: getfield mShiftKeys : [Landroid/inputmethodservice/Keyboard$Key;
    //   218: arraylength
    //   219: if_icmpge -> 268
    //   222: aload_0
    //   223: getfield mShiftKeys : [Landroid/inputmethodservice/Keyboard$Key;
    //   226: iload #9
    //   228: aaload
    //   229: ifnonnull -> 262
    //   232: aload_0
    //   233: getfield mShiftKeys : [Landroid/inputmethodservice/Keyboard$Key;
    //   236: iload #9
    //   238: aload #10
    //   240: aastore
    //   241: aload_0
    //   242: getfield mShiftKeyIndices : [I
    //   245: iload #9
    //   247: aload_0
    //   248: getfield mKeys : Ljava/util/List;
    //   251: invokeinterface size : ()I
    //   256: iconst_1
    //   257: isub
    //   258: iastore
    //   259: goto -> 268
    //   262: iinc #9, 1
    //   265: goto -> 212
    //   268: aload_0
    //   269: getfield mModifierKeys : Ljava/util/List;
    //   272: aload #10
    //   274: invokeinterface add : (Ljava/lang/Object;)Z
    //   279: pop
    //   280: goto -> 307
    //   283: aload #10
    //   285: getfield codes : [I
    //   288: iconst_0
    //   289: iaload
    //   290: bipush #-6
    //   292: if_icmpne -> 307
    //   295: aload_0
    //   296: getfield mModifierKeys : Ljava/util/List;
    //   299: aload #10
    //   301: invokeinterface add : (Ljava/lang/Object;)Z
    //   306: pop
    //   307: aload_1
    //   308: getfield mKeys : Ljava/util/ArrayList;
    //   311: aload #10
    //   313: invokevirtual add : (Ljava/lang/Object;)Z
    //   316: pop
    //   317: iconst_1
    //   318: istore #9
    //   320: goto -> 348
    //   323: astore_1
    //   324: goto -> 494
    //   327: astore_1
    //   328: goto -> 494
    //   331: ldc 'Keyboard'
    //   333: aload #12
    //   335: invokevirtual equals : (Ljava/lang/Object;)Z
    //   338: ifeq -> 348
    //   341: aload_0
    //   342: aload #4
    //   344: aload_2
    //   345: invokespecial parseKeyboardAttributes : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)V
    //   348: goto -> 28
    //   351: iload #9
    //   353: istore #14
    //   355: iload #6
    //   357: istore #15
    //   359: iload #11
    //   361: iconst_3
    //   362: if_icmpne -> 479
    //   365: iload #9
    //   367: ifeq -> 427
    //   370: iconst_0
    //   371: istore #9
    //   373: iload #6
    //   375: aload #10
    //   377: getfield gap : I
    //   380: aload #10
    //   382: getfield width : I
    //   385: iadd
    //   386: iadd
    //   387: istore #6
    //   389: iload #9
    //   391: istore #14
    //   393: iload #6
    //   395: istore #15
    //   397: iload #6
    //   399: aload_0
    //   400: getfield mTotalWidth : I
    //   403: if_icmple -> 479
    //   406: aload_0
    //   407: iload #6
    //   409: putfield mTotalWidth : I
    //   412: iload #9
    //   414: istore #14
    //   416: iload #6
    //   418: istore #15
    //   420: goto -> 479
    //   423: astore_1
    //   424: goto -> 494
    //   427: iload #9
    //   429: istore #14
    //   431: iload #6
    //   433: istore #15
    //   435: iload #8
    //   437: ifeq -> 479
    //   440: iconst_0
    //   441: istore #8
    //   443: aload_1
    //   444: getfield verticalGap : I
    //   447: istore #14
    //   449: iload #7
    //   451: iload #14
    //   453: iadd
    //   454: istore #7
    //   456: aload_1
    //   457: getfield defaultHeight : I
    //   460: istore #14
    //   462: iload #14
    //   464: iload #7
    //   466: iadd
    //   467: istore #7
    //   469: iinc #5, 1
    //   472: goto -> 28
    //   475: astore_1
    //   476: goto -> 494
    //   479: iload #14
    //   481: istore #9
    //   483: iload #15
    //   485: istore #6
    //   487: goto -> 28
    //   490: goto -> 530
    //   493: astore_1
    //   494: new java/lang/StringBuilder
    //   497: dup
    //   498: invokespecial <init> : ()V
    //   501: astore_2
    //   502: aload_2
    //   503: ldc_w 'Parse error:'
    //   506: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   509: pop
    //   510: aload_2
    //   511: aload_1
    //   512: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   515: pop
    //   516: ldc 'Keyboard'
    //   518: aload_2
    //   519: invokevirtual toString : ()Ljava/lang/String;
    //   522: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   525: pop
    //   526: aload_1
    //   527: invokevirtual printStackTrace : ()V
    //   530: aload_0
    //   531: iload #7
    //   533: aload_0
    //   534: getfield mDefaultVerticalGap : I
    //   537: isub
    //   538: putfield mTotalHeight : I
    //   541: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #804	-> 0
    //   #805	-> 0
    //   #806	-> 0
    //   #807	-> 2
    //   #808	-> 2
    //   #809	-> 2
    //   #810	-> 2
    //   #811	-> 2
    //   #812	-> 2
    //   #813	-> 8
    //   #817	-> 28
    //   #818	-> 42
    //   #819	-> 48
    //   #820	-> 56
    //   #821	-> 70
    //   #822	-> 70
    //   #823	-> 70
    //   #824	-> 78
    //   #825	-> 87
    //   #826	-> 122
    //   #827	-> 127
    //   #828	-> 132
    //   #868	-> 141
    //   #826	-> 145
    //   #868	-> 154
    //   #830	-> 158
    //   #831	-> 172
    //   #832	-> 172
    //   #833	-> 186
    //   #834	-> 198
    //   #836	-> 209
    //   #837	-> 222
    //   #838	-> 232
    //   #839	-> 241
    //   #840	-> 259
    //   #836	-> 262
    //   #843	-> 268
    //   #844	-> 283
    //   #845	-> 295
    //   #847	-> 307
    //   #868	-> 323
    //   #848	-> 331
    //   #849	-> 341
    //   #851	-> 348
    //   #852	-> 365
    //   #853	-> 370
    //   #854	-> 373
    //   #855	-> 389
    //   #856	-> 406
    //   #868	-> 423
    //   #858	-> 427
    //   #859	-> 440
    //   #860	-> 443
    //   #861	-> 456
    //   #862	-> 462
    //   #868	-> 475
    //   #817	-> 479
    //   #871	-> 490
    //   #868	-> 493
    //   #869	-> 494
    //   #870	-> 526
    //   #872	-> 530
    //   #873	-> 541
    // Exception table:
    //   from	to	target	type
    //   28	36	493	java/lang/Exception
    //   48	56	493	java/lang/Exception
    //   56	65	493	java/lang/Exception
    //   70	78	154	java/lang/Exception
    //   78	87	154	java/lang/Exception
    //   87	106	154	java/lang/Exception
    //   127	132	141	java/lang/Exception
    //   158	167	493	java/lang/Exception
    //   172	186	327	java/lang/Exception
    //   186	198	323	java/lang/Exception
    //   198	209	323	java/lang/Exception
    //   212	222	323	java/lang/Exception
    //   222	232	323	java/lang/Exception
    //   232	241	323	java/lang/Exception
    //   241	259	323	java/lang/Exception
    //   268	280	323	java/lang/Exception
    //   283	295	323	java/lang/Exception
    //   295	307	323	java/lang/Exception
    //   307	317	323	java/lang/Exception
    //   331	341	423	java/lang/Exception
    //   341	348	423	java/lang/Exception
    //   373	389	423	java/lang/Exception
    //   397	406	423	java/lang/Exception
    //   406	412	423	java/lang/Exception
    //   443	449	423	java/lang/Exception
    //   456	462	475	java/lang/Exception
  }
  
  private void skipToEndOfRow(XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    int i;
    do {
      i = paramXmlResourceParser.next();
    } while (i != 1 && (
      i != 3 || 
      !paramXmlResourceParser.getName().equals("Row")));
  }
  
  private void parseKeyboardAttributes(Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(Xml.asAttributeSet((XmlPullParser)paramXmlResourceParser), R.styleable.Keyboard);
    int i = this.mDisplayWidth;
    this.mDefaultWidth = getDimensionOrFraction(typedArray, 0, i, i / 10);
    this.mDefaultHeight = getDimensionOrFraction(typedArray, 1, this.mDisplayHeight, 50);
    this.mDefaultHorizontalGap = getDimensionOrFraction(typedArray, 2, this.mDisplayWidth, 0);
    this.mDefaultVerticalGap = getDimensionOrFraction(typedArray, 3, this.mDisplayHeight, 0);
    this.mProximityThreshold = i = (int)(this.mDefaultWidth * SEARCH_DISTANCE);
    this.mProximityThreshold = i * i;
    typedArray.recycle();
  }
  
  static int getDimensionOrFraction(TypedArray paramTypedArray, int paramInt1, int paramInt2, int paramInt3) {
    TypedValue typedValue = paramTypedArray.peekValue(paramInt1);
    if (typedValue == null)
      return paramInt3; 
    if (typedValue.type == 5)
      return paramTypedArray.getDimensionPixelOffset(paramInt1, paramInt3); 
    if (typedValue.type == 6)
      return Math.round(paramTypedArray.getFraction(paramInt1, paramInt2, paramInt2, paramInt3)); 
    return paramInt3;
  }
}
