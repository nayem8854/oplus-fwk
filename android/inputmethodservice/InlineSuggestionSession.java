package android.inputmethodservice;

import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.autofill.AutofillId;
import android.view.inputmethod.InlineSuggestionsRequest;
import android.view.inputmethod.InlineSuggestionsResponse;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.view.IInlineSuggestionsRequestCallback;
import com.android.internal.view.IInlineSuggestionsResponseCallback;
import com.android.internal.view.InlineSuggestionsRequestInfo;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

class InlineSuggestionSession {
  static final InlineSuggestionsResponse EMPTY_RESPONSE = new InlineSuggestionsResponse(Collections.emptyList());
  
  private static final String TAG = "ImsInlineSuggestionSession";
  
  private final IInlineSuggestionsRequestCallback mCallback;
  
  private boolean mCallbackInvoked = false;
  
  private final Supplier<IBinder> mHostInputTokenSupplier;
  
  private final InlineSuggestionSessionController mInlineSuggestionSessionController;
  
  private final Handler mMainThreadHandler;
  
  private Boolean mPreviousResponseIsEmpty;
  
  private final InlineSuggestionsRequestInfo mRequestInfo;
  
  private final Function<Bundle, InlineSuggestionsRequest> mRequestSupplier;
  
  private InlineSuggestionsResponseCallbackImpl mResponseCallback;
  
  private final Consumer<InlineSuggestionsResponse> mResponseConsumer;
  
  InlineSuggestionSession(InlineSuggestionsRequestInfo paramInlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback paramIInlineSuggestionsRequestCallback, Function<Bundle, InlineSuggestionsRequest> paramFunction, Supplier<IBinder> paramSupplier, Consumer<InlineSuggestionsResponse> paramConsumer, InlineSuggestionSessionController paramInlineSuggestionSessionController, Handler paramHandler) {
    this.mRequestInfo = paramInlineSuggestionsRequestInfo;
    this.mCallback = paramIInlineSuggestionsRequestCallback;
    this.mRequestSupplier = paramFunction;
    this.mHostInputTokenSupplier = paramSupplier;
    this.mResponseConsumer = paramConsumer;
    this.mInlineSuggestionSessionController = paramInlineSuggestionSessionController;
    this.mMainThreadHandler = paramHandler;
  }
  
  InlineSuggestionsRequestInfo getRequestInfo() {
    return this.mRequestInfo;
  }
  
  IInlineSuggestionsRequestCallback getRequestCallback() {
    return this.mCallback;
  }
  
  boolean shouldSendImeStatus() {
    boolean bool;
    if (this.mResponseCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  boolean isCallbackInvoked() {
    return this.mCallbackInvoked;
  }
  
  void invalidate() {
    try {
      this.mCallback.onInlineSuggestionsSessionInvalidated();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onInlineSuggestionsSessionInvalidated() remote exception:");
      stringBuilder.append(remoteException);
      Log.w("ImsInlineSuggestionSession", stringBuilder.toString());
    } 
    if (this.mResponseCallback != null) {
      consumeInlineSuggestionsResponse(EMPTY_RESPONSE);
      this.mResponseCallback.invalidate();
      this.mResponseCallback = null;
    } 
  }
  
  void makeInlineSuggestionRequestUncheck() {
    if (this.mCallbackInvoked)
      return; 
    try {
      Function<Bundle, InlineSuggestionsRequest> function = this.mRequestSupplier;
      InlineSuggestionsRequestInfo inlineSuggestionsRequestInfo = this.mRequestInfo;
      Bundle bundle = inlineSuggestionsRequestInfo.getUiExtras();
      InlineSuggestionsRequest inlineSuggestionsRequest = function.apply(bundle);
      if (inlineSuggestionsRequest == null) {
        if (InputMethodService.DEBUG)
          Log.d("ImsInlineSuggestionSession", "onCreateInlineSuggestionsRequest() returned null request"); 
        this.mCallback.onInlineSuggestionsUnsupported();
      } else {
        inlineSuggestionsRequest.setHostInputToken(this.mHostInputTokenSupplier.get());
        inlineSuggestionsRequest.filterContentTypes();
        InlineSuggestionsResponseCallbackImpl inlineSuggestionsResponseCallbackImpl = new InlineSuggestionsResponseCallbackImpl();
        this(this);
        this.mResponseCallback = inlineSuggestionsResponseCallbackImpl;
        this.mCallback.onInlineSuggestionsRequest(inlineSuggestionsRequest, (IInlineSuggestionsResponseCallback)inlineSuggestionsResponseCallbackImpl);
      } 
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("makeInlinedSuggestionsRequest() remote exception:");
      stringBuilder.append(remoteException);
      Log.w("ImsInlineSuggestionSession", stringBuilder.toString());
    } 
    this.mCallbackInvoked = true;
  }
  
  void handleOnInlineSuggestionsResponse(AutofillId paramAutofillId, InlineSuggestionsResponse paramInlineSuggestionsResponse) {
    if (!this.mInlineSuggestionSessionController.match(paramAutofillId))
      return; 
    if (InputMethodService.DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("IME receives response: ");
      stringBuilder.append(paramInlineSuggestionsResponse.getInlineSuggestions().size());
      Log.d("ImsInlineSuggestionSession", stringBuilder.toString());
    } 
    consumeInlineSuggestionsResponse(paramInlineSuggestionsResponse);
  }
  
  void consumeInlineSuggestionsResponse(InlineSuggestionsResponse paramInlineSuggestionsResponse) {
    boolean bool = paramInlineSuggestionsResponse.getInlineSuggestions().isEmpty();
    if (bool && Boolean.TRUE.equals(this.mPreviousResponseIsEmpty))
      return; 
    this.mPreviousResponseIsEmpty = Boolean.valueOf(bool);
    this.mResponseConsumer.accept(paramInlineSuggestionsResponse);
  }
  
  class InlineSuggestionsResponseCallbackImpl extends IInlineSuggestionsResponseCallback.Stub {
    private volatile boolean mInvalid = false;
    
    private final WeakReference<InlineSuggestionSession> mSession;
    
    private InlineSuggestionsResponseCallbackImpl(InlineSuggestionSession this$0) {
      this.mSession = new WeakReference<>(InlineSuggestionSession.this);
    }
    
    void invalidate() {
      this.mInvalid = true;
    }
    
    public void onInlineSuggestionsResponse(AutofillId param1AutofillId, InlineSuggestionsResponse param1InlineSuggestionsResponse) {
      if (this.mInvalid)
        return; 
      InlineSuggestionSession inlineSuggestionSession = this.mSession.get();
      if (inlineSuggestionSession != null) {
        Handler handler = inlineSuggestionSession.mMainThreadHandler;
        -$.Lambda.gFA42jHMHyW9teqkAqcpq3JsqOY gFA42jHMHyW9teqkAqcpq3JsqOY = _$$Lambda$gFA42jHMHyW9teqkAqcpq3JsqOY.INSTANCE;
        Message message = PooledLambda.obtainMessage((TriConsumer)gFA42jHMHyW9teqkAqcpq3JsqOY, inlineSuggestionSession, param1AutofillId, param1InlineSuggestionsResponse);
        handler.sendMessage(message);
      } 
    }
  }
}
