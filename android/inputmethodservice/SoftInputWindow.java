package android.inputmethodservice;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.IBinder;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.WindowManager;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SoftInputWindow extends Dialog {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "SoftInputWindow";
  
  private final Rect mBounds = new Rect();
  
  final Callback mCallback;
  
  final KeyEvent.DispatcherState mDispatcherState;
  
  final int mGravity;
  
  final KeyEvent.Callback mKeyEventCallback;
  
  final String mName;
  
  final boolean mTakesFocus;
  
  private int mWindowState = 0;
  
  final int mWindowType;
  
  public void setToken(IBinder paramIBinder) {
    StringBuilder stringBuilder;
    int i = this.mWindowState;
    if (i != 0) {
      if (i != 1 && i != 2 && i != 3) {
        if (i == 4) {
          Log.i("SoftInputWindow", "Ignoring setToken() because window is already destroyed.");
          return;
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected state=");
        stringBuilder.append(this.mWindowState);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      throw new IllegalStateException("setToken can be called only once");
    } 
    WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
    layoutParams.token = (IBinder)stringBuilder;
    getWindow().setAttributes(layoutParams);
    updateWindowState(1);
    getWindow().getDecorView().setVisibility(4);
    show();
  }
  
  public SoftInputWindow(Context paramContext, String paramString, int paramInt1, Callback paramCallback, KeyEvent.Callback paramCallback1, KeyEvent.DispatcherState paramDispatcherState, int paramInt2, int paramInt3, boolean paramBoolean) {
    super(paramContext, paramInt1);
    this.mName = paramString;
    this.mCallback = paramCallback;
    this.mKeyEventCallback = paramCallback1;
    this.mDispatcherState = paramDispatcherState;
    this.mWindowType = paramInt2;
    this.mGravity = paramInt3;
    this.mTakesFocus = paramBoolean;
    initDockWindow();
  }
  
  public void onWindowFocusChanged(boolean paramBoolean) {
    super.onWindowFocusChanged(paramBoolean);
    this.mDispatcherState.reset();
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    getWindow().getDecorView().getHitRect(this.mBounds);
    if (paramMotionEvent.isWithinBoundsNoHistory(this.mBounds.left, this.mBounds.top, (this.mBounds.right - 1), (this.mBounds.bottom - 1)))
      return super.dispatchTouchEvent(paramMotionEvent); 
    paramMotionEvent = paramMotionEvent.clampNoHistory(this.mBounds.left, this.mBounds.top, (this.mBounds.right - 1), (this.mBounds.bottom - 1));
    boolean bool = super.dispatchTouchEvent(paramMotionEvent);
    paramMotionEvent.recycle();
    return bool;
  }
  
  public void setGravity(int paramInt) {
    WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
    layoutParams.gravity = paramInt;
    updateWidthHeight(layoutParams);
    getWindow().setAttributes(layoutParams);
  }
  
  public int getGravity() {
    return (getWindow().getAttributes()).gravity;
  }
  
  private void updateWidthHeight(WindowManager.LayoutParams paramLayoutParams) {
    if (paramLayoutParams.gravity == 48 || paramLayoutParams.gravity == 80) {
      paramLayoutParams.width = -1;
      paramLayoutParams.height = -2;
      return;
    } 
    paramLayoutParams.width = -2;
    paramLayoutParams.height = -1;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    KeyEvent.Callback callback = this.mKeyEventCallback;
    if (callback != null && callback.onKeyDown(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyDown(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent) {
    KeyEvent.Callback callback = this.mKeyEventCallback;
    if (callback != null && callback.onKeyLongPress(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyLongPress(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    KeyEvent.Callback callback = this.mKeyEventCallback;
    if (callback != null && callback.onKeyUp(paramInt, paramKeyEvent))
      return true; 
    return super.onKeyUp(paramInt, paramKeyEvent);
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    KeyEvent.Callback callback = this.mKeyEventCallback;
    if (callback != null && callback.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent))
      return true; 
    return super.onKeyMultiple(paramInt1, paramInt2, paramKeyEvent);
  }
  
  public void onBackPressed() {
    Callback callback = this.mCallback;
    if (callback != null) {
      callback.onBackPressed();
    } else {
      super.onBackPressed();
    } 
  }
  
  private void initDockWindow() {
    int j;
    WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
    layoutParams.type = this.mWindowType;
    layoutParams.setTitle(this.mName);
    layoutParams.gravity = this.mGravity;
    updateWidthHeight(layoutParams);
    getWindow().setAttributes(layoutParams);
    int i = 266;
    if (!this.mTakesFocus) {
      j = 0x100 | 0x8;
    } else {
      j = 0x100 | 0x20;
      i = 0x10A | 0x20;
    } 
    getWindow().setFlags(j, i);
  }
  
  public final void show() {
    int i = this.mWindowState;
    if (i != 0) {
      if (i != 1 && i != 2) {
        if (i != 3) {
          if (i == 4) {
            Log.i("SoftInputWindow", "Ignoring show() because the window is already destroyed.");
            return;
          } 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected state=");
          stringBuilder.append(this.mWindowState);
          throw new IllegalStateException(stringBuilder.toString());
        } 
        Log.i("SoftInputWindow", "Not trying to call show() because it was already rejected once.");
        return;
      } 
      try {
        super.show();
        updateWindowState(2);
      } catch (android.view.WindowManager.BadTokenException badTokenException) {
        Log.i("SoftInputWindow", "Probably the IME window token is already invalidated. show() does nothing.");
        updateWindowState(3);
      } 
      return;
    } 
    throw new IllegalStateException("Window token is not set yet.");
  }
  
  final void dismissForDestroyIfNecessary() {
    int i = this.mWindowState;
    if (i != 0 && i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unexpected state=");
            stringBuilder.append(this.mWindowState);
            throw new IllegalStateException(stringBuilder.toString());
          } 
          throw new IllegalStateException("dismissForDestroyIfNecessary can be called only once");
        } 
        Log.i("SoftInputWindow", "Not trying to dismiss the window because it is most likely unnecessary.");
        updateWindowState(4);
        return;
      } 
      try {
        getWindow().setWindowAnimations(0);
        dismiss();
      } catch (android.view.WindowManager.BadTokenException badTokenException) {
        Log.i("SoftInputWindow", "Probably the IME window token is already invalidated. No need to dismiss it.");
      } 
      updateWindowState(4);
      return;
    } 
    updateWindowState(4);
  }
  
  private void updateWindowState(int paramInt) {
    this.mWindowState = paramInt;
  }
  
  private static String stateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt == 4)
              return "DESTROYED"; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown state=");
            stringBuilder.append(paramInt);
            throw new IllegalStateException(stringBuilder.toString());
          } 
          return "REJECTED_AT_LEAST_ONCE";
        } 
        return "SHOWN_AT_LEAST_ONCE";
      } 
      return "TOKEN_SET";
    } 
    return "TOKEN_PENDING";
  }
  
  class Callback {
    public abstract void onBackPressed();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SoftInputWindowState implements Annotation {
    public static final int DESTROYED = 4;
    
    public static final int REJECTED_AT_LEAST_ONCE = 3;
    
    public static final int SHOWN_AT_LEAST_ONCE = 2;
    
    public static final int TOKEN_PENDING = 0;
    
    public static final int TOKEN_SET = 1;
  }
}
