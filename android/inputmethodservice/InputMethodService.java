package android.inputmethodservice;

import android.R;
import android.app.ActivityManager;
import android.app.Dialog;
import android.common.ColorFrameworkFactory;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.graphics.Rect;
import android.graphics.Region;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ResultReceiver;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.provider.Settings;
import android.text.Layout;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.util.Log;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewRootImpl;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.view.animation.AnimationUtils;
import android.view.autofill.AutofillId;
import android.view.inputmethod.CompletionInfo;
import android.view.inputmethod.CursorAnchorInfo;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.ExtractedText;
import android.view.inputmethod.ExtractedTextRequest;
import android.view.inputmethod.InlineSuggestionsRequest;
import android.view.inputmethod.InlineSuggestionsResponse;
import android.view.inputmethod.InputBinding;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputContentInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.inputmethod.InputMethodSubtype;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.window.WindowMetricsHelper;
import com.android.internal.inputmethod.IInputContentUriToken;
import com.android.internal.inputmethod.IInputMethodPrivilegedOperations;
import com.android.internal.inputmethod.InputMethodPrivilegedOperations;
import com.android.internal.inputmethod.InputMethodPrivilegedOperationsRegistry;
import com.android.internal.view.IInlineSuggestionsRequestCallback;
import com.android.internal.view.InlineSuggestionsRequestInfo;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Collections;

public class InputMethodService extends AbstractInputMethodService {
  static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private InputMethodPrivilegedOperations mPrivOps = new InputMethodPrivilegedOperations();
  
  int mTheme = 0;
  
  private Object mLock = new Object();
  
  final Insets mTmpInsets = new Insets();
  
  final int[] mTmpLocation = new int[2];
  
  final ViewTreeObserver.OnComputeInternalInsetsListener mInsetsComputer = new _$$Lambda$InputMethodService$8T9TmAUIN7vW9eU6kTg8309_d4E(this);
  
  final View.OnClickListener mActionClickListener = new _$$Lambda$InputMethodService$wp8DeVGx_WDOPw4F6an7QbwVxf0(this);
  
  public static final int BACK_DISPOSITION_ADJUST_NOTHING = 3;
  
  public static final int BACK_DISPOSITION_DEFAULT = 0;
  
  private static final int BACK_DISPOSITION_MAX = 3;
  
  private static final int BACK_DISPOSITION_MIN = 0;
  
  @Deprecated
  public static final int BACK_DISPOSITION_WILL_DISMISS = 2;
  
  @Deprecated
  public static final int BACK_DISPOSITION_WILL_NOT_DISMISS = 1;
  
  public static final int IME_ACTIVE = 1;
  
  public static final int IME_INVISIBLE = 4;
  
  public static final int IME_VISIBLE = 2;
  
  static final int MOVEMENT_DOWN = -1;
  
  static final int MOVEMENT_UP = -2;
  
  static final String TAG = "InputMethodService";
  
  private boolean mAutomotiveHideNavBarForKeyboard;
  
  int mBackDisposition;
  
  boolean mCanPreRender;
  
  FrameLayout mCandidatesFrame;
  
  boolean mCandidatesViewStarted;
  
  int mCandidatesVisibility;
  
  CompletionInfo[] mCurCompletions;
  
  private IBinder mCurHideInputToken;
  
  private IBinder mCurShowInputToken;
  
  boolean mDecorViewVisible;
  
  boolean mDecorViewWasVisible;
  
  ViewGroup mExtractAccessories;
  
  View mExtractAction;
  
  ExtractEditText mExtractEditText;
  
  FrameLayout mExtractFrame;
  
  View mExtractView;
  
  boolean mExtractViewHidden;
  
  ExtractedText mExtractedText;
  
  int mExtractedToken;
  
  boolean mFullscreenApplied;
  
  ViewGroup mFullscreenArea;
  
  InputMethodManager mImm;
  
  boolean mInShowWindow;
  
  LayoutInflater mInflater;
  
  boolean mInitialized;
  
  private InlineSuggestionSessionController mInlineSuggestionSessionController;
  
  InputBinding mInputBinding;
  
  InputConnection mInputConnection;
  
  EditorInfo mInputEditorInfo;
  
  FrameLayout mInputFrame;
  
  boolean mInputStarted;
  
  View mInputView;
  
  boolean mInputViewStarted;
  
  private boolean mIsAutomotive;
  
  boolean mIsFullscreen;
  
  boolean mIsInputViewShown;
  
  boolean mIsPreRendered;
  
  boolean mLastShowInputRequested;
  
  private boolean mNotifyUserActionSent;
  
  private IOplusInputMethodServiceUtils mOplusInputMethodServiceUtils;
  
  View mRootView;
  
  private SettingsObserver mSettingsObserver;
  
  int mShowInputFlags;
  
  boolean mShowInputRequested;
  
  private long mStartTime;
  
  InputConnection mStartedInputConnection;
  
  int mStatusIcon;
  
  TypedArray mThemeAttrs;
  
  IBinder mToken;
  
  boolean mViewsCreated;
  
  SoftInputWindow mWindow;
  
  boolean mWindowVisible;
  
  @Retention(RetentionPolicy.SOURCE)
  class BackDispositionMode implements Annotation {}
  
  class InputMethodImpl extends AbstractInputMethodService.AbstractInputMethodImpl {
    private boolean mSystemCallingHideSoftInput;
    
    private boolean mSystemCallingShowSoftInput;
    
    final InputMethodService this$0;
    
    public final void initializeInternal(IBinder param1IBinder, int param1Int, IInputMethodPrivilegedOperations param1IInputMethodPrivilegedOperations) {
      if (InputMethodPrivilegedOperationsRegistry.isRegistered(param1IBinder)) {
        Log.w("InputMethod", "The token has already registered, ignore this initialization.");
        return;
      } 
      InputMethodService.this.mPrivOps.set(param1IInputMethodPrivilegedOperations);
      InputMethodPrivilegedOperationsRegistry.put(param1IBinder, InputMethodService.this.mPrivOps);
      updateInputMethodDisplay(param1Int);
      attachToken(param1IBinder);
    }
    
    public void onCreateInlineSuggestionsRequest(InlineSuggestionsRequestInfo param1InlineSuggestionsRequestInfo, IInlineSuggestionsRequestCallback param1IInlineSuggestionsRequestCallback) {
      if (InputMethodService.DEBUG)
        Log.d("InputMethod", "InputMethodService received onCreateInlineSuggestionsRequest()"); 
      InputMethodService.this.mInlineSuggestionSessionController.onMakeInlineSuggestionsRequest(param1InlineSuggestionsRequestInfo, param1IInlineSuggestionsRequestCallback);
    }
    
    public void attachToken(IBinder param1IBinder) {
      if (InputMethodService.this.mToken == null) {
        InputMethodService.this.mToken = param1IBinder;
        InputMethodService.this.mWindow.setToken(param1IBinder);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("attachToken() must be called at most once. token=");
      stringBuilder.append(param1IBinder);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public void updateInputMethodDisplay(int param1Int) {
      InputMethodService.this.updateDisplay(param1Int);
    }
    
    public void bindInput(InputBinding param1InputBinding) {
      InputMethodService.this.mInputBinding = param1InputBinding;
      InputMethodService.this.mInputConnection = param1InputBinding.getConnection();
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("bindInput(): binding=");
        stringBuilder.append(param1InputBinding);
        stringBuilder.append(" ic=");
        stringBuilder.append(InputMethodService.this.mInputConnection);
        Log.v("InputMethod", stringBuilder.toString());
      } 
      InputMethodService.this.reportFullscreenMode();
      InputMethodService.this.initialize();
      InputMethodService.this.onBindInput();
    }
    
    public void unbindInput() {
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unbindInput(): binding=");
        stringBuilder.append(InputMethodService.this.mInputBinding);
        stringBuilder.append(" ic=");
        stringBuilder.append(InputMethodService.this.mInputConnection);
        Log.v("InputMethod", stringBuilder.toString());
      } 
      InputMethodService.this.onUnbindInput();
      InputMethodService.this.mInputBinding = null;
      InputMethodService.this.mInputConnection = null;
    }
    
    public void startInput(InputConnection param1InputConnection, EditorInfo param1EditorInfo) {
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("startInput(): editor=");
        stringBuilder.append(param1EditorInfo);
        Log.v("InputMethod", stringBuilder.toString());
      } 
      InputMethodService.this.doStartInput(param1InputConnection, param1EditorInfo, false);
    }
    
    public void restartInput(InputConnection param1InputConnection, EditorInfo param1EditorInfo) {
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("restartInput(): editor=");
        stringBuilder.append(param1EditorInfo);
        Log.v("InputMethod", stringBuilder.toString());
      } 
      InputMethodService.this.doStartInput(param1InputConnection, param1EditorInfo, true);
    }
    
    public final void dispatchStartInputWithToken(InputConnection param1InputConnection, EditorInfo param1EditorInfo, boolean param1Boolean1, IBinder param1IBinder, boolean param1Boolean2) {
      InputMethodService.this.mPrivOps.reportStartInput(param1IBinder);
      InputMethodService.this.mCanPreRender = param1Boolean2;
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Will Pre-render IME: ");
        stringBuilder.append(InputMethodService.this.mCanPreRender);
        Log.v("InputMethod", stringBuilder.toString());
      } 
      if (param1Boolean1) {
        restartInput(param1InputConnection, param1EditorInfo);
      } else {
        startInput(param1InputConnection, param1EditorInfo);
      } 
    }
    
    public void hideSoftInputWithToken(int param1Int, ResultReceiver param1ResultReceiver, IBinder param1IBinder) {
      this.mSystemCallingHideSoftInput = true;
      InputMethodService.access$302(InputMethodService.this, param1IBinder);
      hideSoftInput(param1Int, param1ResultReceiver);
      InputMethodService.access$302(InputMethodService.this, (IBinder)null);
      this.mSystemCallingHideSoftInput = false;
    }
    
    public void hideSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      boolean bool2;
      if (InputMethodService.DEBUG)
        Log.v("InputMethod", "hideSoftInput()"); 
      if (InputMethodService.this.mOplusInputMethodServiceUtils.hideImmediately(param1Int, InputMethodService.this.mWindow))
        return; 
      if ((InputMethodService.this.getApplicationInfo()).targetSdkVersion >= 30 && !this.mSystemCallingHideSoftInput) {
        Log.e("InputMethod", "IME shouldn't call hideSoftInput on itself. Use requestHideSelf(int) itself");
        return;
      } 
      boolean bool1 = InputMethodService.this.mIsPreRendered;
      boolean bool = true;
      if (bool1) {
        if (InputMethodService.this.mDecorViewVisible && InputMethodService.this.mWindowVisible) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
      } else {
        bool1 = InputMethodService.this.isInputViewShown();
      } 
      if (InputMethodService.this.mOplusInputMethodServiceUtils.skipInsetChange(param1Int))
        InputMethodService.this.applyVisibilityInInsetsConsumerIfNecessary(false); 
      if (InputMethodService.this.mIsPreRendered) {
        if (InputMethodService.DEBUG)
          Log.v("InputMethod", "Making IME window invisible"); 
        InputMethodService inputMethodService = InputMethodService.this;
        inputMethodService.setImeWindowStatus(5, inputMethodService.mBackDisposition);
        InputMethodService.this.onPreRenderedWindowVisibilityChanged(false);
      } else {
        InputMethodService.this.mShowInputFlags = 0;
        InputMethodService.this.mShowInputRequested = false;
        InputMethodService.this.doHideWindow();
      } 
      if (InputMethodService.this.mIsPreRendered) {
        if (InputMethodService.this.mDecorViewVisible && InputMethodService.this.mWindowVisible) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
      } else {
        bool2 = InputMethodService.this.isInputViewShown();
      } 
      if (bool2 != bool1) {
        param1Int = 1;
      } else {
        param1Int = 0;
      } 
      if (param1ResultReceiver != null) {
        if (param1Int != 0) {
          param1Int = 3;
        } else if (bool1) {
          param1Int = 0;
        } else {
          param1Int = bool;
        } 
        param1ResultReceiver.send(param1Int, null);
      } 
    }
    
    public void showSoftInputWithToken(int param1Int, ResultReceiver param1ResultReceiver, IBinder param1IBinder) {
      this.mSystemCallingShowSoftInput = true;
      InputMethodService.access$902(InputMethodService.this, param1IBinder);
      showSoftInput(param1Int, param1ResultReceiver);
      InputMethodService.access$902(InputMethodService.this, (IBinder)null);
      this.mSystemCallingShowSoftInput = false;
    }
    
    public void showSoftInput(int param1Int, ResultReceiver param1ResultReceiver) {
      boolean bool2;
      if (InputMethodService.DEBUG)
        Log.v("InputMethod", "showSoftInput()"); 
      if ((InputMethodService.this.getApplicationInfo()).targetSdkVersion >= 30 && !this.mSystemCallingShowSoftInput) {
        Log.e("InputMethod", " IME shouldn't call showSoftInput on itself. Use requestShowSelf(int) itself");
        return;
      } 
      boolean bool1 = InputMethodService.this.mIsPreRendered;
      boolean bool = false;
      if (bool1) {
        if (InputMethodService.this.mDecorViewVisible && InputMethodService.this.mWindowVisible) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
      } else {
        bool1 = InputMethodService.this.isInputViewShown();
      } 
      if (InputMethodService.this.dispatchOnShowInputRequested(param1Int, false)) {
        if (InputMethodService.this.mIsPreRendered) {
          if (InputMethodService.DEBUG)
            Log.v("InputMethod", "Making IME window visible"); 
          InputMethodService.this.onPreRenderedWindowVisibilityChanged(true);
        } else {
          InputMethodService.this.showWindow(true);
        } 
        InputMethodService.this.applyVisibilityInInsetsConsumerIfNecessary(true);
      } 
      InputMethodService inputMethodService = InputMethodService.this;
      inputMethodService.setImeWindowStatus(inputMethodService.mapToImeWindowStatus(), InputMethodService.this.mBackDisposition);
      if (InputMethodService.this.mIsPreRendered) {
        if (InputMethodService.this.mDecorViewVisible && InputMethodService.this.mWindowVisible) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
      } else {
        bool2 = InputMethodService.this.isInputViewShown();
      } 
      if (bool2 != bool1) {
        param1Int = 1;
      } else {
        param1Int = 0;
      } 
      if (param1ResultReceiver != null) {
        if (param1Int != 0) {
          param1Int = 2;
        } else if (bool1) {
          param1Int = bool;
        } else {
          param1Int = 1;
        } 
        param1ResultReceiver.send(param1Int, null);
      } 
    }
    
    public void changeInputMethodSubtype(InputMethodSubtype param1InputMethodSubtype) {
      InputMethodService.this.dispatchOnCurrentInputMethodSubtypeChanged(param1InputMethodSubtype);
    }
    
    public void setCurrentShowInputToken(IBinder param1IBinder) {
      InputMethodService.access$902(InputMethodService.this, param1IBinder);
    }
    
    public void setCurrentHideInputToken(IBinder param1IBinder) {
      InputMethodService.access$302(InputMethodService.this, param1IBinder);
    }
  }
  
  public InlineSuggestionsRequest onCreateInlineSuggestionsRequest(Bundle paramBundle) {
    return null;
  }
  
  public boolean onInlineSuggestionsResponse(InlineSuggestionsResponse paramInlineSuggestionsResponse) {
    return false;
  }
  
  private IBinder getHostInputToken() {
    IBinder iBinder;
    ViewRootImpl viewRootImpl = null;
    View view = this.mRootView;
    if (view != null)
      viewRootImpl = view.getViewRootImpl(); 
    if (viewRootImpl == null) {
      viewRootImpl = null;
    } else {
      iBinder = viewRootImpl.getInputToken();
    } 
    return iBinder;
  }
  
  private void notifyImeHidden() {
    requestHideSelf(0);
  }
  
  private void removeImeSurface() {
    if (!this.mShowInputRequested && !this.mWindowVisible)
      this.mWindow.hide(); 
  }
  
  private void setImeWindowStatus(int paramInt1, int paramInt2) {
    this.mPrivOps.setImeWindowStatus(paramInt1, paramInt2);
  }
  
  private void setImeExclusionRect(int paramInt) {
    View view = this.mInputFrame.getRootView();
    if (!this.mOplusInputMethodServiceUtils.shouldPreventTouch() || !this.mWindow.isShowing()) {
      view.setSystemGestureExclusionRects(Collections.EMPTY_LIST);
      return;
    } 
    int i = view.getWidth();
    Rect rect = new Rect(0, paramInt, i, view.getHeight());
    view.setSystemGestureExclusionRects(Collections.singletonList(rect));
  }
  
  class InputMethodSessionImpl extends AbstractInputMethodService.AbstractInputMethodSessionImpl {
    final InputMethodService this$0;
    
    public void finishInput() {
      if (!isEnabled())
        return; 
      if (InputMethodService.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("finishInput() in ");
        stringBuilder.append(this);
        Log.v("InputMethodService", stringBuilder.toString());
      } 
      InputMethodService.this.doFinishInput();
    }
    
    public void displayCompletions(CompletionInfo[] param1ArrayOfCompletionInfo) {
      if (!isEnabled())
        return; 
      InputMethodService.this.mCurCompletions = param1ArrayOfCompletionInfo;
      InputMethodService.this.onDisplayCompletions(param1ArrayOfCompletionInfo);
    }
    
    public void updateExtractedText(int param1Int, ExtractedText param1ExtractedText) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onUpdateExtractedText(param1Int, param1ExtractedText);
    }
    
    public void updateSelection(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onUpdateSelection(param1Int1, param1Int2, param1Int3, param1Int4, param1Int5, param1Int6);
    }
    
    public void viewClicked(boolean param1Boolean) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onViewClicked(param1Boolean);
    }
    
    public void updateCursor(Rect param1Rect) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onUpdateCursor(param1Rect);
    }
    
    public void appPrivateCommand(String param1String, Bundle param1Bundle) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onAppPrivateCommand(param1String, param1Bundle);
    }
    
    public void toggleSoftInput(int param1Int1, int param1Int2) {
      InputMethodService.this.onToggleSoftInput(param1Int1, param1Int2);
    }
    
    public void updateCursorAnchorInfo(CursorAnchorInfo param1CursorAnchorInfo) {
      if (!isEnabled())
        return; 
      InputMethodService.this.onUpdateCursorAnchorInfo(param1CursorAnchorInfo);
    }
    
    public final void notifyImeHidden() {
      InputMethodService.this.notifyImeHidden();
    }
    
    public final void removeImeSurface() {
      InputMethodService.this.removeImeSurface();
    }
  }
  
  class Insets {
    public static final int TOUCHABLE_INSETS_CONTENT = 1;
    
    public static final int TOUCHABLE_INSETS_FRAME = 0;
    
    public static final int TOUCHABLE_INSETS_REGION = 3;
    
    public static final int TOUCHABLE_INSETS_VISIBLE = 2;
    
    public int contentTopInsets;
    
    public int touchableInsets;
    
    public final Region touchableRegion;
    
    public int visibleTopInsets;
    
    public Insets() {
      this.touchableRegion = new Region();
    }
  }
  
  class SettingsObserver extends ContentObserver {
    private final InputMethodService mService;
    
    private int mShowImeWithHardKeyboard = 0;
    
    private SettingsObserver(InputMethodService this$0) {
      super(new Handler(this$0.getMainLooper()));
      this.mService = this$0;
    }
    
    public static SettingsObserver createAndRegister(InputMethodService param1InputMethodService) {
      SettingsObserver settingsObserver = new SettingsObserver(param1InputMethodService);
      ContentResolver contentResolver = param1InputMethodService.getContentResolver();
      Uri uri = Settings.Secure.getUriFor("show_ime_with_hard_keyboard");
      contentResolver.registerContentObserver(uri, false, settingsObserver);
      return settingsObserver;
    }
    
    void unregister() {
      this.mService.getContentResolver().unregisterContentObserver(this);
    }
    
    private boolean shouldShowImeWithHardKeyboard() {
      if (this.mShowImeWithHardKeyboard == 0) {
        boolean bool;
        InputMethodService inputMethodService = this.mService;
        if (Settings.Secure.getInt(inputMethodService.getContentResolver(), "show_ime_with_hard_keyboard", 0) != 0) {
          bool = true;
        } else {
          bool = true;
        } 
        this.mShowImeWithHardKeyboard = bool;
      } 
      int i = this.mShowImeWithHardKeyboard;
      if (i != 1) {
        if (i != 2) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unexpected mShowImeWithHardKeyboard=");
          stringBuilder.append(this.mShowImeWithHardKeyboard);
          Log.e("InputMethodService", stringBuilder.toString());
          return false;
        } 
        return true;
      } 
      return false;
    }
    
    public void onChange(boolean param1Boolean, Uri param1Uri) {
      Uri uri = Settings.Secure.getUriFor("show_ime_with_hard_keyboard");
      if (uri.equals(param1Uri)) {
        boolean bool;
        InputMethodService inputMethodService = this.mService;
        if (Settings.Secure.getInt(inputMethodService.getContentResolver(), "show_ime_with_hard_keyboard", 0) != 0) {
          bool = true;
        } else {
          bool = true;
        } 
        this.mShowImeWithHardKeyboard = bool;
        this.mService.resetStateForNewConfiguration();
      } 
      this.mService.onColorChange(param1Uri);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SettingsObserver{mShowImeWithHardKeyboard=");
      stringBuilder.append(this.mShowImeWithHardKeyboard);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class ShowImeWithHardKeyboardType implements Annotation {
      public static final int FALSE = 1;
      
      public static final int TRUE = 2;
      
      public static final int UNKNOWN = 0;
    }
  }
  
  public void setTheme(int paramInt) {
    if (this.mWindow == null) {
      this.mTheme = paramInt;
      return;
    } 
    throw new IllegalStateException("Must be called before onCreate()");
  }
  
  @Deprecated
  public boolean enableHardwareAcceleration() {
    if (this.mWindow == null)
      return ActivityManager.isHighEndGfx(); 
    throw new IllegalStateException("Must be called before onCreate()");
  }
  
  public void onCreate() {
    int i = this.mTheme;
    int j = (getApplicationInfo()).targetSdkVersion;
    this.mTheme = i = Resources.selectSystemTheme(i, j, 16973908, 16973951, 16974142, 16974142);
    super.setTheme(i);
    super.onCreate();
    this.mImm = (InputMethodManager)getSystemService("input_method");
    this.mSettingsObserver = SettingsObserver.createAndRegister(this);
    this.mIsAutomotive = isAutomotive();
    this.mAutomotiveHideNavBarForKeyboard = getApplicationContext().getResources().getBoolean(17891370);
    this.mInflater = (LayoutInflater)getSystemService("layout_inflater");
    SoftInputWindow softInputWindow = new SoftInputWindow((Context)this, "InputMethod", this.mTheme, null, null, this.mDispatcherState, 2011, 80, false);
    softInputWindow.getWindow().getAttributes().setFitInsetsTypes(WindowInsets.Type.statusBars() | WindowInsets.Type.navigationBars());
    this.mWindow.getWindow().getAttributes().setFitInsetsSides(WindowInsets.Side.all() & 0xFFFFFFF7);
    this.mWindow.getWindow().getAttributes().setFitInsetsIgnoringVisibility(true);
    this.mWindow.getWindow().getDecorView().setOnApplyWindowInsetsListener(new _$$Lambda$InputMethodService$TvVfWDKZ3ljQdrU87qYykg6uD_I(this));
    this.mWindow.getWindow().setFlags(-2147483648, -2147483648);
    initViews();
    this.mWindow.getWindow().setLayout(-1, -2);
    IOplusInputMethodServiceUtils iOplusInputMethodServiceUtils = (IOplusInputMethodServiceUtils)ColorFrameworkFactory.getInstance().getFeature(IOplusInputMethodServiceUtils.DEFAULT, new Object[0]);
    iOplusInputMethodServiceUtils.init((Context)this);
    this.mOplusInputMethodServiceUtils.onCreateAndRegister(this.mSettingsObserver);
    this.mInlineSuggestionSessionController = new InlineSuggestionSessionController(new _$$Lambda$hehYicAn_teGDK35tKSDBx5a_qY(this), new _$$Lambda$InputMethodService$TZAfta6HEKUpxapSrZpy7Hhlwrw(this), new _$$Lambda$_pjAZYkcDmGn7I8XQsAVO3X__e0(this));
  }
  
  public void onInitializeInterface() {}
  
  void initialize() {
    if (!this.mInitialized) {
      this.mInitialized = true;
      onInitializeInterface();
    } 
  }
  
  void initViews() {
    this.mInitialized = false;
    this.mViewsCreated = false;
    this.mShowInputRequested = false;
    this.mShowInputFlags = 0;
    this.mThemeAttrs = obtainStyledAttributes(R.styleable.InputMethodService);
    View view = this.mInflater.inflate(17367175, null);
    this.mWindow.setContentView(view);
    this.mRootView.getViewTreeObserver().removeOnComputeInternalInsetsListener(this.mInsetsComputer);
    this.mRootView.getViewTreeObserver().addOnComputeInternalInsetsListener(this.mInsetsComputer);
    if (Settings.Global.getInt(getContentResolver(), "fancy_ime_animations", 0) != 0)
      this.mWindow.getWindow().setWindowAnimations(16974587); 
    this.mFullscreenArea = (ViewGroup)this.mRootView.findViewById(16909016);
    this.mExtractViewHidden = false;
    this.mExtractFrame = (FrameLayout)this.mRootView.findViewById(16908316);
    this.mExtractView = null;
    this.mExtractEditText = null;
    this.mExtractAccessories = null;
    this.mExtractAction = null;
    this.mFullscreenApplied = false;
    this.mCandidatesFrame = (FrameLayout)this.mRootView.findViewById(16908317);
    this.mInputFrame = (FrameLayout)this.mRootView.findViewById(16908318);
    this.mInputView = null;
    this.mIsInputViewShown = false;
    this.mExtractFrame.setVisibility(8);
    int i = getCandidatesHiddenVisibility();
    this.mCandidatesFrame.setVisibility(i);
    this.mInputFrame.setVisibility(8);
  }
  
  public void onDestroy() {
    super.onDestroy();
    this.mRootView.getViewTreeObserver().removeOnComputeInternalInsetsListener(this.mInsetsComputer);
    doFinishInput();
    this.mWindow.dismissForDestroyIfNecessary();
    SettingsObserver settingsObserver = this.mSettingsObserver;
    if (settingsObserver != null) {
      settingsObserver.unregister();
      this.mSettingsObserver = null;
    } 
    IBinder iBinder = this.mToken;
    if (iBinder != null)
      InputMethodPrivilegedOperationsRegistry.remove(iBinder); 
    this.mOplusInputMethodServiceUtils.onDestroy();
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    super.onConfigurationChanged(paramConfiguration);
    resetStateForNewConfiguration();
  }
  
  private void resetStateForNewConfiguration() {
    boolean bool1 = this.mDecorViewVisible;
    int i = this.mShowInputFlags;
    boolean bool2 = this.mShowInputRequested;
    CompletionInfo[] arrayOfCompletionInfo = this.mCurCompletions;
    initViews();
    byte b = 0;
    this.mInputViewStarted = false;
    this.mCandidatesViewStarted = false;
    if (this.mInputStarted) {
      InputConnection inputConnection = getCurrentInputConnection();
      EditorInfo editorInfo = getCurrentInputEditorInfo();
      doStartInput(inputConnection, editorInfo, true);
    } 
    if (bool1) {
      if (bool2) {
        if (dispatchOnShowInputRequested(i, true)) {
          showWindow(true);
          if (arrayOfCompletionInfo != null) {
            this.mCurCompletions = arrayOfCompletionInfo;
            onDisplayCompletions(arrayOfCompletionInfo);
          } 
        } else {
          doHideWindow();
        } 
      } else if (this.mCandidatesVisibility == 0) {
        showWindow(false);
      } else {
        doHideWindow();
      } 
      bool2 = onEvaluateInputViewShown();
      if (bool2)
        b = 2; 
      setImeWindowStatus(b | 0x1, this.mBackDisposition);
    } 
    this.mOplusInputMethodServiceUtils.updateNavigationGuardColorDelay(this.mWindow);
  }
  
  public AbstractInputMethodService.AbstractInputMethodImpl onCreateInputMethodInterface() {
    return new InputMethodImpl();
  }
  
  public AbstractInputMethodService.AbstractInputMethodSessionImpl onCreateInputMethodSessionInterface() {
    return new InputMethodSessionImpl();
  }
  
  public LayoutInflater getLayoutInflater() {
    return this.mInflater;
  }
  
  public Dialog getWindow() {
    return this.mWindow;
  }
  
  public void setBackDisposition(int paramInt) {
    if (paramInt == this.mBackDisposition)
      return; 
    if (paramInt > 3 || paramInt < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid back disposition value (");
      stringBuilder.append(paramInt);
      stringBuilder.append(") specified.");
      Log.e("InputMethodService", stringBuilder.toString());
      return;
    } 
    this.mBackDisposition = paramInt;
    setImeWindowStatus(mapToImeWindowStatus(), this.mBackDisposition);
  }
  
  public int getBackDisposition() {
    return this.mBackDisposition;
  }
  
  public int getMaxWidth() {
    WindowManager windowManager = (WindowManager)getSystemService(WindowManager.class);
    WindowMetrics windowMetrics = windowManager.getCurrentWindowMetrics();
    Rect rect = WindowMetricsHelper.getBoundsExcludingNavigationBarAndCutout(windowMetrics);
    return rect.width();
  }
  
  public InputBinding getCurrentInputBinding() {
    return this.mInputBinding;
  }
  
  public InputConnection getCurrentInputConnection() {
    InputConnection inputConnection = this.mStartedInputConnection;
    if (inputConnection != null)
      return inputConnection; 
    return this.mInputConnection;
  }
  
  public final boolean switchToPreviousInputMethod() {
    return this.mPrivOps.switchToPreviousInputMethod();
  }
  
  public final boolean switchToNextInputMethod(boolean paramBoolean) {
    return this.mPrivOps.switchToNextInputMethod(paramBoolean);
  }
  
  public final boolean shouldOfferSwitchingToNextInputMethod() {
    return this.mPrivOps.shouldOfferSwitchingToNextInputMethod();
  }
  
  public boolean getCurrentInputStarted() {
    return this.mInputStarted;
  }
  
  public EditorInfo getCurrentInputEditorInfo() {
    return this.mInputEditorInfo;
  }
  
  private void reportFullscreenMode() {
    this.mPrivOps.reportFullscreenMode(this.mIsFullscreen);
  }
  
  public void updateFullscreenMode() {
    boolean bool;
    boolean bool1;
    if (this.mShowInputRequested && onEvaluateFullscreenMode()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mLastShowInputRequested != this.mShowInputRequested) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    if (this.mIsFullscreen != bool || !this.mFullscreenApplied) {
      bool1 = true;
      this.mIsFullscreen = bool;
      reportFullscreenMode();
      this.mFullscreenApplied = true;
      initialize();
      ViewGroup viewGroup = this.mFullscreenArea;
      LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams)viewGroup.getLayoutParams();
      if (bool) {
        this.mFullscreenArea.setBackgroundDrawable(this.mThemeAttrs.getDrawable(0));
        layoutParams.height = 0;
        layoutParams.weight = 1.0F;
      } else {
        this.mFullscreenArea.setBackgroundDrawable(null);
        layoutParams.height = -2;
        layoutParams.weight = 0.0F;
      } 
      ((ViewGroup)this.mFullscreenArea.getParent()).updateViewLayout((View)this.mFullscreenArea, (ViewGroup.LayoutParams)layoutParams);
      if (bool) {
        if (this.mExtractView == null) {
          View view = onCreateExtractTextView();
          if (view != null)
            setExtractView(view); 
        } 
        startExtractingText(false);
      } 
      updateExtractFrameVisibility();
    } 
    if (bool1) {
      onConfigureWindow(this.mWindow.getWindow(), bool, true ^ this.mShowInputRequested);
      this.mLastShowInputRequested = this.mShowInputRequested;
    } 
  }
  
  public void onConfigureWindow(Window paramWindow, boolean paramBoolean1, boolean paramBoolean2) {
    byte b;
    int i = (this.mWindow.getWindow().getAttributes()).height;
    if (paramBoolean1) {
      b = -1;
    } else {
      b = -2;
    } 
    if (this.mIsInputViewShown && i != b && 
      DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Window size has been changed. This may cause jankiness of resizing window: ");
      stringBuilder.append(i);
      stringBuilder.append(" -> ");
      stringBuilder.append(b);
      Log.w("InputMethodService", stringBuilder.toString());
    } 
    this.mWindow.getWindow().setLayout(-1, b);
  }
  
  public boolean isFullscreenMode() {
    return this.mIsFullscreen;
  }
  
  public boolean onEvaluateFullscreenMode() {
    Configuration configuration = getResources().getConfiguration();
    if (configuration.orientation != 2)
      return false; 
    EditorInfo editorInfo = this.mInputEditorInfo;
    if (editorInfo != null && (editorInfo.imeOptions & 0x2000000) != 0)
      return false; 
    return true;
  }
  
  public void setExtractViewShown(boolean paramBoolean) {
    if (this.mExtractViewHidden == paramBoolean) {
      this.mExtractViewHidden = paramBoolean ^ true;
      updateExtractFrameVisibility();
    } 
  }
  
  public boolean isExtractViewShown() {
    boolean bool;
    if (this.mIsFullscreen && !this.mExtractViewHidden) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void updateExtractFrameVisibility() {
    boolean bool1, bool2;
    if (isFullscreenMode()) {
      if (this.mExtractViewHidden) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      this.mExtractFrame.setVisibility(bool1);
    } else {
      bool1 = false;
      this.mExtractFrame.setVisibility(8);
    } 
    int i = this.mCandidatesVisibility, j = 1;
    if (i == 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    updateCandidatesVisibility(bool2);
    if (this.mDecorViewWasVisible && this.mFullscreenArea.getVisibility() != bool1) {
      TypedArray typedArray = this.mThemeAttrs;
      if (bool1)
        j = 2; 
      j = typedArray.getResourceId(j, 0);
      if (j != 0)
        this.mFullscreenArea.startAnimation(AnimationUtils.loadAnimation((Context)this, j)); 
    } 
    this.mFullscreenArea.setVisibility(bool1);
  }
  
  public void onComputeInsets(Insets paramInsets) {
    int[] arrayOfInt = this.mTmpLocation;
    if (this.mInputFrame.getVisibility() == 0) {
      this.mInputFrame.getLocationInWindow(arrayOfInt);
    } else {
      View view = getWindow().getWindow().getDecorView();
      arrayOfInt[1] = view.getHeight();
    } 
    if (isFullscreenMode()) {
      View view = getWindow().getWindow().getDecorView();
      paramInsets.contentTopInsets = view.getHeight();
    } else {
      paramInsets.contentTopInsets = arrayOfInt[1];
    } 
    if (this.mCandidatesFrame.getVisibility() == 0)
      this.mCandidatesFrame.getLocationInWindow(arrayOfInt); 
    paramInsets.visibleTopInsets = arrayOfInt[1];
    paramInsets.touchableInsets = 2;
    paramInsets.touchableRegion.setEmpty();
  }
  
  public void updateInputViewShown() {
    boolean bool = this.mShowInputRequested;
    byte b = 0;
    if (bool && onEvaluateInputViewShown()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mIsInputViewShown != bool && this.mDecorViewVisible) {
      this.mIsInputViewShown = bool;
      FrameLayout frameLayout = this.mInputFrame;
      if (!bool)
        b = 8; 
      frameLayout.setVisibility(b);
      if (this.mInputView == null) {
        initialize();
        View view = onCreateInputView();
        if (view != null)
          setInputView(view); 
      } 
    } 
  }
  
  public boolean isShowInputRequested() {
    this.mOplusInputMethodServiceUtils.updateNavigationGuardColor(this.mWindow);
    return this.mShowInputRequested;
  }
  
  public boolean isInputViewShown() {
    boolean bool;
    if (this.mCanPreRender) {
      bool = this.mWindowVisible;
    } else if (this.mIsInputViewShown && this.mDecorViewVisible) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean onEvaluateInputViewShown() {
    SettingsObserver settingsObserver = this.mSettingsObserver;
    boolean bool = false;
    if (settingsObserver == null) {
      Log.w("InputMethodService", "onEvaluateInputViewShown: mSettingsObserver must not be null here.");
      return false;
    } 
    if (settingsObserver.shouldShowImeWithHardKeyboard())
      return true; 
    Configuration configuration = getResources().getConfiguration();
    if (configuration.keyboard == 1 || configuration.hardKeyboardHidden == 2)
      bool = true; 
    return bool;
  }
  
  public void setCandidatesViewShown(boolean paramBoolean) {
    updateCandidatesVisibility(paramBoolean);
    if (!this.mShowInputRequested && this.mDecorViewVisible != paramBoolean)
      if (paramBoolean) {
        showWindow(false);
      } else {
        doHideWindow();
      }  
  }
  
  void updateCandidatesVisibility(boolean paramBoolean) {
    int i;
    if (paramBoolean) {
      i = 0;
    } else {
      i = getCandidatesHiddenVisibility();
    } 
    if (this.mCandidatesVisibility != i) {
      this.mCandidatesFrame.setVisibility(i);
      this.mCandidatesVisibility = i;
    } 
  }
  
  public int getCandidatesHiddenVisibility() {
    byte b;
    if (isExtractViewShown()) {
      b = 8;
    } else {
      b = 4;
    } 
    return b;
  }
  
  public void showStatusIcon(int paramInt) {
    this.mStatusIcon = paramInt;
    this.mPrivOps.updateStatusIcon(getPackageName(), paramInt);
  }
  
  public void hideStatusIcon() {
    this.mStatusIcon = 0;
    this.mPrivOps.updateStatusIcon(null, 0);
  }
  
  public void switchInputMethod(String paramString) {
    this.mPrivOps.setInputMethod(paramString);
  }
  
  public final void switchInputMethod(String paramString, InputMethodSubtype paramInputMethodSubtype) {
    this.mPrivOps.setInputMethodAndSubtype(paramString, paramInputMethodSubtype);
  }
  
  public void setExtractView(View paramView) {
    this.mExtractFrame.removeAllViews();
    this.mExtractFrame.addView(paramView, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -1));
    this.mExtractView = paramView;
    if (paramView != null) {
      ExtractEditText extractEditText = (ExtractEditText)paramView.findViewById(16908325);
      extractEditText.setIME(this);
      View view = paramView.findViewById(16909079);
      if (view != null)
        this.mExtractAccessories = (ViewGroup)paramView.findViewById(16909078); 
      startExtractingText(false);
    } else {
      this.mExtractEditText = null;
      this.mExtractAccessories = null;
      this.mExtractAction = null;
    } 
  }
  
  public void setCandidatesView(View paramView) {
    this.mCandidatesFrame.removeAllViews();
    this.mCandidatesFrame.addView(paramView, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -2));
  }
  
  public void setInputView(View paramView) {
    this.mInputFrame.removeAllViews();
    this.mInputFrame.addView(paramView, (ViewGroup.LayoutParams)new FrameLayout.LayoutParams(-1, -2));
    this.mInputView = paramView;
    this.mOplusInputMethodServiceUtils.updateNavigationGuardColor(getWindow());
  }
  
  public View onCreateExtractTextView() {
    return this.mInflater.inflate(17367176, null);
  }
  
  public View onCreateCandidatesView() {
    return null;
  }
  
  public View onCreateInputView() {
    return null;
  }
  
  public void onStartInputView(EditorInfo paramEditorInfo, boolean paramBoolean) {}
  
  public void onFinishInputView(boolean paramBoolean) {
    if (!paramBoolean) {
      InputConnection inputConnection = getCurrentInputConnection();
      if (inputConnection != null)
        inputConnection.finishComposingText(); 
    } 
  }
  
  public void onStartCandidatesView(EditorInfo paramEditorInfo, boolean paramBoolean) {}
  
  public void onFinishCandidatesView(boolean paramBoolean) {
    if (!paramBoolean) {
      InputConnection inputConnection = getCurrentInputConnection();
      if (inputConnection != null)
        inputConnection.finishComposingText(); 
    } 
  }
  
  public boolean onShowInputRequested(int paramInt, boolean paramBoolean) {
    if (!onEvaluateInputViewShown())
      return false; 
    if ((paramInt & 0x1) == 0) {
      if (!paramBoolean && onEvaluateFullscreenMode())
        return false; 
      if (!this.mSettingsObserver.shouldShowImeWithHardKeyboard() && 
        (getResources().getConfiguration()).keyboard != 1)
        return false; 
    } 
    return true;
  }
  
  private boolean dispatchOnShowInputRequested(int paramInt, boolean paramBoolean) {
    paramBoolean = onShowInputRequested(paramInt, paramBoolean);
    this.mInlineSuggestionSessionController.notifyOnShowInputRequested(paramBoolean);
    if (paramBoolean) {
      this.mShowInputFlags = paramInt;
    } else {
      this.mShowInputFlags = 0;
    } 
    return paramBoolean;
  }
  
  public void showWindow(boolean paramBoolean) {
    int j;
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Showing window: showInput=");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" mShowInputRequested=");
      stringBuilder.append(this.mShowInputRequested);
      stringBuilder.append(" mViewsCreated=");
      stringBuilder.append(this.mViewsCreated);
      stringBuilder.append(" mDecorViewVisible=");
      stringBuilder.append(this.mDecorViewVisible);
      stringBuilder.append(" mWindowVisible=");
      stringBuilder.append(this.mWindowVisible);
      stringBuilder.append(" mInputStarted=");
      stringBuilder.append(this.mInputStarted);
      stringBuilder.append(" mShowInputFlags=");
      stringBuilder.append(this.mShowInputFlags);
      Log.v("InputMethodService", stringBuilder.toString());
    } 
    if (this.mInShowWindow) {
      Log.w("InputMethodService", "Re-entrance in to showWindow");
      return;
    } 
    this.mDecorViewWasVisible = this.mDecorViewVisible;
    this.mInShowWindow = true;
    if (this.mIsPreRendered && !this.mWindowVisible) {
      j = 1;
    } else {
      j = 0;
    } 
    int k = this.mDecorViewVisible;
    if (isInputViewShown()) {
      if (j) {
        j = 4;
      } else {
        j = 2;
      } 
    } else {
      j = 0;
    } 
    int m = k | j;
    startViews(prepareWindow(paramBoolean));
    int i = mapToImeWindowStatus();
    if (m != i)
      setImeWindowStatus(i, this.mBackDisposition); 
    onWindowShown();
    this.mIsPreRendered = paramBoolean = this.mCanPreRender;
    if (paramBoolean) {
      onPreRenderedWindowVisibilityChanged(true);
    } else {
      if (DEBUG)
        Log.d("InputMethodService", "No pre-rendering supported"); 
      this.mWindowVisible = true;
    } 
    if ((m & 0x1) == 0) {
      if (DEBUG)
        Log.v("InputMethodService", "showWindow: draw decorView!"); 
      this.mWindow.show();
      this.mOplusInputMethodServiceUtils.updateNavigationGuardColor(this.mWindow);
      this.mStartTime = System.currentTimeMillis();
    } 
    maybeNotifyPreRendered();
    this.mDecorViewWasVisible = true;
    this.mInShowWindow = false;
  }
  
  private void maybeNotifyPreRendered() {
    if (!this.mCanPreRender || !this.mIsPreRendered)
      return; 
    this.mPrivOps.reportPreRendered(getCurrentInputEditorInfo());
  }
  
  private boolean prepareWindow(boolean paramBoolean) {
    boolean bool1 = false;
    this.mDecorViewVisible = true;
    boolean bool2 = bool1;
    if (!this.mShowInputRequested) {
      bool2 = bool1;
      if (this.mInputStarted) {
        bool2 = bool1;
        if (paramBoolean) {
          bool2 = true;
          this.mShowInputRequested = true;
        } 
      } 
    } 
    if (DEBUG)
      Log.v("InputMethodService", "showWindow: updating UI"); 
    initialize();
    updateFullscreenMode();
    updateInputViewShown();
    if (!this.mViewsCreated) {
      this.mViewsCreated = true;
      initialize();
      if (DEBUG)
        Log.v("InputMethodService", "CALL: onCreateCandidatesView"); 
      View view = onCreateCandidatesView();
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("showWindow: candidates=");
        stringBuilder.append(view);
        Log.v("InputMethodService", stringBuilder.toString());
      } 
      if (view != null)
        setCandidatesView(view); 
    } 
    return bool2;
  }
  
  private void startViews(boolean paramBoolean) {
    if (this.mShowInputRequested) {
      if (!this.mInputViewStarted) {
        if (DEBUG)
          Log.v("InputMethodService", "CALL: onStartInputView"); 
        this.mInputViewStarted = true;
        this.mInlineSuggestionSessionController.notifyOnStartInputView();
        onStartInputView(this.mInputEditorInfo, false);
      } 
    } else if (!this.mCandidatesViewStarted) {
      if (DEBUG)
        Log.v("InputMethodService", "CALL: onStartCandidatesView"); 
      this.mCandidatesViewStarted = true;
      onStartCandidatesView(this.mInputEditorInfo, false);
    } 
    if (paramBoolean)
      startExtractingText(false); 
  }
  
  private void onPreRenderedWindowVisibilityChanged(boolean paramBoolean) {
    boolean bool;
    this.mWindowVisible = paramBoolean;
    if (paramBoolean) {
      bool = this.mShowInputFlags;
    } else {
      bool = false;
    } 
    this.mShowInputFlags = bool;
    this.mShowInputRequested = paramBoolean;
    this.mDecorViewVisible = paramBoolean;
    if (paramBoolean)
      onWindowShown(); 
  }
  
  private void applyVisibilityInInsetsConsumerIfNecessary(boolean paramBoolean) {
    IBinder iBinder;
    if (!isVisibilityAppliedUsingInsetsConsumer())
      return; 
    InputMethodPrivilegedOperations inputMethodPrivilegedOperations = this.mPrivOps;
    if (paramBoolean) {
      iBinder = this.mCurShowInputToken;
    } else {
      iBinder = this.mCurHideInputToken;
    } 
    inputMethodPrivilegedOperations.applyImeVisibility(iBinder, paramBoolean);
  }
  
  private boolean isVisibilityAppliedUsingInsetsConsumer() {
    boolean bool;
    if (ViewRootImpl.sNewInsetsMode > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void finishViews(boolean paramBoolean) {
    if (this.mInputViewStarted) {
      if (DEBUG)
        Log.v("InputMethodService", "CALL: onFinishInputView"); 
      this.mInlineSuggestionSessionController.notifyOnFinishInputView();
      onFinishInputView(paramBoolean);
    } else if (this.mCandidatesViewStarted) {
      if (DEBUG)
        Log.v("InputMethodService", "CALL: onFinishCandidatesView"); 
      onFinishCandidatesView(paramBoolean);
    } 
    this.mInputViewStarted = false;
    this.mCandidatesViewStarted = false;
  }
  
  private void doHideWindow() {
    setImeWindowStatus(0, this.mBackDisposition);
    hideWindow();
  }
  
  public void hideWindow() {
    if (DEBUG)
      Log.v("InputMethodService", "CALL: hideWindow"); 
    this.mIsPreRendered = false;
    this.mWindowVisible = false;
    finishViews(false);
    if (this.mDecorViewVisible) {
      if (isVisibilityAppliedUsingInsetsConsumer()) {
        View view = this.mInputView;
        if (view != null)
          view.dispatchWindowVisibilityChanged(8); 
      } else {
        this.mWindow.hide();
      } 
      this.mDecorViewVisible = false;
      onWindowHidden();
      this.mDecorViewWasVisible = false;
      this.mOplusInputMethodServiceUtils.uploadData(this.mStartTime);
    } 
    updateFullscreenMode();
  }
  
  public void onWindowShown() {}
  
  public void onWindowHidden() {}
  
  public void onBindInput() {}
  
  public void onUnbindInput() {}
  
  public void onStartInput(EditorInfo paramEditorInfo, boolean paramBoolean) {}
  
  void doFinishInput() {
    if (DEBUG)
      Log.v("InputMethodService", "CALL: doFinishInput"); 
    finishViews(true);
    if (this.mInputStarted) {
      this.mInlineSuggestionSessionController.notifyOnFinishInput();
      if (DEBUG)
        Log.v("InputMethodService", "CALL: onFinishInput"); 
      onFinishInput();
    } 
    this.mInputStarted = false;
    this.mStartedInputConnection = null;
    this.mCurCompletions = null;
  }
  
  void doStartInput(InputConnection paramInputConnection, EditorInfo paramEditorInfo, boolean paramBoolean) {
    String str;
    if (!paramBoolean)
      doFinishInput(); 
    this.mInputStarted = true;
    this.mStartedInputConnection = paramInputConnection;
    this.mInputEditorInfo = paramEditorInfo;
    initialize();
    InlineSuggestionSessionController inlineSuggestionSessionController = this.mInlineSuggestionSessionController;
    AutofillId autofillId = null;
    if (paramEditorInfo == null) {
      paramInputConnection = null;
    } else {
      str = paramEditorInfo.packageName;
    } 
    if (paramEditorInfo != null)
      autofillId = paramEditorInfo.autofillId; 
    inlineSuggestionSessionController.notifyOnStartInput(str, autofillId);
    if (DEBUG)
      Log.v("InputMethodService", "CALL: onStartInput"); 
    onStartInput(paramEditorInfo, paramBoolean);
    if (this.mDecorViewVisible) {
      if (this.mShowInputRequested) {
        if (DEBUG)
          Log.v("InputMethodService", "CALL: onStartInputView"); 
        this.mInputViewStarted = true;
        this.mInlineSuggestionSessionController.notifyOnStartInputView();
        onStartInputView(this.mInputEditorInfo, paramBoolean);
        startExtractingText(true);
      } else if (this.mCandidatesVisibility == 0) {
        if (DEBUG)
          Log.v("InputMethodService", "CALL: onStartCandidatesView"); 
        this.mCandidatesViewStarted = true;
        onStartCandidatesView(this.mInputEditorInfo, paramBoolean);
      } 
    } else if (this.mCanPreRender && this.mInputEditorInfo != null && this.mStartedInputConnection != null) {
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Pre-Render IME for ");
        stringBuilder.append(this.mInputEditorInfo.fieldName);
        Log.v("InputMethodService", stringBuilder.toString());
      } 
      if (this.mInShowWindow) {
        Log.w("InputMethodService", "Re-entrance in to showWindow");
        return;
      } 
      this.mDecorViewWasVisible = this.mDecorViewVisible;
      this.mInShowWindow = true;
      startViews(prepareWindow(true));
      this.mIsPreRendered = true;
      onPreRenderedWindowVisibilityChanged(false);
      if (DEBUG)
        Log.v("InputMethodService", "showWindow: draw decorView!"); 
      this.mWindow.show();
      this.mOplusInputMethodServiceUtils.updateNavigationGuardColor(this.mWindow);
      maybeNotifyPreRendered();
      this.mDecorViewWasVisible = true;
      this.mInShowWindow = false;
    } else {
      this.mIsPreRendered = false;
    } 
  }
  
  public void onFinishInput() {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null)
      inputConnection.finishComposingText(); 
  }
  
  public void onDisplayCompletions(CompletionInfo[] paramArrayOfCompletionInfo) {}
  
  public void onUpdateExtractedText(int paramInt, ExtractedText paramExtractedText) {
    if (this.mExtractedToken != paramInt)
      return; 
    if (paramExtractedText != null) {
      ExtractEditText extractEditText = this.mExtractEditText;
      if (extractEditText != null) {
        this.mExtractedText = paramExtractedText;
        extractEditText.setExtractedText(paramExtractedText);
      } 
    } 
  }
  
  public void onUpdateSelection(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    ExtractEditText extractEditText = this.mExtractEditText;
    if (extractEditText != null && isFullscreenMode()) {
      ExtractedText extractedText = this.mExtractedText;
      if (extractedText != null) {
        paramInt1 = extractedText.startOffset;
        extractEditText.startInternalChanges();
        paramInt2 = paramInt3 - paramInt1;
        paramInt4 -= paramInt1;
        paramInt3 = extractEditText.getText().length();
        if (paramInt2 < 0) {
          paramInt1 = 0;
        } else {
          paramInt1 = paramInt2;
          if (paramInt2 > paramInt3)
            paramInt1 = paramInt3; 
        } 
        if (paramInt4 < 0) {
          paramInt2 = 0;
        } else {
          paramInt2 = paramInt4;
          if (paramInt4 > paramInt3)
            paramInt2 = paramInt3; 
        } 
        extractEditText.setSelection(paramInt1, paramInt2);
        extractEditText.finishInternalChanges();
      } 
    } 
  }
  
  @Deprecated
  public void onViewClicked(boolean paramBoolean) {}
  
  @Deprecated
  public void onUpdateCursor(Rect paramRect) {}
  
  public void onUpdateCursorAnchorInfo(CursorAnchorInfo paramCursorAnchorInfo) {}
  
  public void requestHideSelf(int paramInt) {
    this.mPrivOps.hideMySoftInput(paramInt);
  }
  
  public final void requestShowSelf(int paramInt) {
    this.mPrivOps.showMySoftInput(paramInt);
  }
  
  private boolean handleBack(boolean paramBoolean) {
    if (this.mShowInputRequested) {
      if (paramBoolean)
        requestHideSelf(0); 
      return true;
    } 
    if (this.mDecorViewVisible) {
      if (this.mCandidatesVisibility == 0) {
        if (paramBoolean)
          setCandidatesViewShown(false); 
      } else if (paramBoolean) {
        doHideWindow();
      } 
      return true;
    } 
    return false;
  }
  
  private ExtractEditText getExtractEditTextIfVisible() {
    if (!isExtractViewShown() || !isInputViewShown())
      return null; 
    return this.mExtractEditText;
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getKeyCode() == 4) {
      ExtractEditText extractEditText = getExtractEditTextIfVisible();
      if (extractEditText != null && extractEditText.handleBackInTextActionModeIfNeeded(paramKeyEvent))
        return true; 
      if (handleBack(false)) {
        paramKeyEvent.startTracking();
        return true;
      } 
      return false;
    } 
    return doMovementKey(paramInt, paramKeyEvent, -1);
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return doMovementKey(paramInt1, paramKeyEvent, paramInt2);
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if (paramKeyEvent.getKeyCode() == 4) {
      ExtractEditText extractEditText = getExtractEditTextIfVisible();
      if (extractEditText != null && extractEditText.handleBackInTextActionModeIfNeeded(paramKeyEvent))
        return true; 
      if (paramKeyEvent.isTracking() && !paramKeyEvent.isCanceled())
        return handleBack(true); 
    } 
    return doMovementKey(paramInt, paramKeyEvent, -2);
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onTrackballEvent: ");
      stringBuilder.append(paramMotionEvent);
      Log.v("InputMethodService", stringBuilder.toString());
    } 
    return false;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onGenericMotionEvent(): event ");
      stringBuilder.append(paramMotionEvent);
      Log.v("InputMethodService", stringBuilder.toString());
    } 
    return false;
  }
  
  public void onAppPrivateCommand(String paramString, Bundle paramBundle) {}
  
  private void onToggleSoftInput(int paramInt1, int paramInt2) {
    if (DEBUG)
      Log.v("InputMethodService", "toggleSoftInput()"); 
    if (isInputViewShown()) {
      requestHideSelf(paramInt2);
    } else {
      requestShowSelf(paramInt1);
    } 
  }
  
  void reportExtractedMovement(int paramInt1, int paramInt2) {
    boolean bool1 = false, bool2 = false;
    switch (paramInt1) {
      default:
        paramInt1 = bool1;
        paramInt2 = bool2;
        break;
      case 22:
        paramInt1 = paramInt2;
        paramInt2 = bool2;
        break;
      case 21:
        paramInt1 = -paramInt2;
        paramInt2 = bool2;
        break;
      case 20:
        paramInt1 = bool1;
        break;
      case 19:
        paramInt2 = -paramInt2;
        paramInt1 = bool1;
        break;
    } 
    onExtractedCursorMovement(paramInt1, paramInt2);
  }
  
  boolean doMovementKey(int paramInt1, KeyEvent paramKeyEvent, int paramInt2) {
    ExtractEditText extractEditText = getExtractEditTextIfVisible();
    if (extractEditText != null) {
      MovementMethod movementMethod = extractEditText.getMovementMethod();
      Layout layout = extractEditText.getLayout();
      if (movementMethod != null && layout != null)
        if (paramInt2 == -1) {
          if (movementMethod.onKeyDown((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, paramKeyEvent)) {
            reportExtractedMovement(paramInt1, 1);
            return true;
          } 
        } else if (paramInt2 == -2) {
          if (movementMethod.onKeyUp((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, paramKeyEvent))
            return true; 
        } else if (movementMethod.onKeyOther((TextView)extractEditText, (Spannable)extractEditText.getText(), paramKeyEvent)) {
          reportExtractedMovement(paramInt1, paramInt2);
        } else {
          KeyEvent keyEvent = KeyEvent.changeAction(paramKeyEvent, 0);
          if (movementMethod.onKeyDown((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, keyEvent)) {
            paramKeyEvent = KeyEvent.changeAction(paramKeyEvent, 1);
            movementMethod.onKeyUp((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, paramKeyEvent);
            while (--paramInt2 > 0) {
              movementMethod.onKeyDown((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, keyEvent);
              movementMethod.onKeyUp((TextView)extractEditText, (Spannable)extractEditText.getText(), paramInt1, paramKeyEvent);
            } 
            reportExtractedMovement(paramInt1, paramInt2);
          } 
        }  
      switch (paramInt1) {
        default:
          return false;
        case 19:
        case 20:
        case 21:
        case 22:
          break;
      } 
      return true;
    } 
  }
  
  public void sendDownUpKeyEvents(int paramInt) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection == null)
      return; 
    long l = SystemClock.uptimeMillis();
    inputConnection.sendKeyEvent(new KeyEvent(l, l, 0, paramInt, 0, 0, -1, 0, 6));
    inputConnection.sendKeyEvent(new KeyEvent(l, SystemClock.uptimeMillis(), 1, paramInt, 0, 0, -1, 0, 6));
  }
  
  public boolean sendDefaultEditorAction(boolean paramBoolean) {
    EditorInfo editorInfo = getCurrentInputEditorInfo();
    if (editorInfo != null && (!paramBoolean || (editorInfo.imeOptions & 0x40000000) == 0) && (editorInfo.imeOptions & 0xFF) != 1) {
      InputConnection inputConnection = getCurrentInputConnection();
      if (inputConnection != null)
        inputConnection.performEditorAction(editorInfo.imeOptions & 0xFF); 
      return true;
    } 
    return false;
  }
  
  public void sendKeyChar(char paramChar) {
    if (paramChar != '\n') {
      if (paramChar >= '0' && paramChar <= '9') {
        sendDownUpKeyEvents(paramChar - 48 + 7);
      } else {
        InputConnection inputConnection = getCurrentInputConnection();
        if (inputConnection != null)
          inputConnection.commitText(String.valueOf(paramChar), 1); 
      } 
    } else if (!sendDefaultEditorAction(true)) {
      sendDownUpKeyEvents(66);
    } 
  }
  
  public void onExtractedSelectionChanged(int paramInt1, int paramInt2) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null)
      inputConnection.setSelection(paramInt1, paramInt2); 
  }
  
  public void onExtractedDeleteText(int paramInt1, int paramInt2) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null) {
      inputConnection.finishComposingText();
      inputConnection.setSelection(paramInt1, paramInt1);
      inputConnection.deleteSurroundingText(0, paramInt2 - paramInt1);
    } 
  }
  
  public void onExtractedReplaceText(int paramInt1, int paramInt2, CharSequence paramCharSequence) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null) {
      inputConnection.setComposingRegion(paramInt1, paramInt2);
      inputConnection.commitText(paramCharSequence, 1);
    } 
  }
  
  public void onExtractedSetSpan(Object paramObject, int paramInt1, int paramInt2, int paramInt3) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null) {
      if (!inputConnection.setSelection(paramInt1, paramInt2))
        return; 
      CharSequence charSequence = inputConnection.getSelectedText(1);
      if (charSequence instanceof Spannable) {
        ((Spannable)charSequence).setSpan(paramObject, 0, charSequence.length(), paramInt3);
        inputConnection.setComposingRegion(paramInt1, paramInt2);
        inputConnection.commitText(charSequence, 1);
      } 
    } 
  }
  
  public void onExtractedTextClicked() {
    ExtractEditText extractEditText = this.mExtractEditText;
    if (extractEditText == null)
      return; 
    if (extractEditText.hasVerticalScrollBar())
      setCandidatesViewShown(false); 
  }
  
  public void onExtractedCursorMovement(int paramInt1, int paramInt2) {
    ExtractEditText extractEditText = this.mExtractEditText;
    if (extractEditText == null || paramInt2 == 0)
      return; 
    if (extractEditText.hasVerticalScrollBar())
      setCandidatesViewShown(false); 
  }
  
  public boolean onExtractTextContextMenuItem(int paramInt) {
    InputConnection inputConnection = getCurrentInputConnection();
    if (inputConnection != null)
      inputConnection.performContextMenuAction(paramInt); 
    return true;
  }
  
  public CharSequence getTextForImeAction(int paramInt) {
    switch (paramInt & 0xFF) {
      default:
        return getText(17040341);
      case 7:
        return getText(17040345);
      case 6:
        return getText(17040342);
      case 5:
        return getText(17040344);
      case 4:
        return getText(17040347);
      case 3:
        return getText(17040346);
      case 2:
        return getText(17040343);
      case 1:
        break;
    } 
    return null;
  }
  
  private int getIconForImeAction(int paramInt) {
    switch (paramInt & 0xFF) {
      default:
        return 17302454;
      case 7:
        return 17302453;
      case 6:
        return 17302450;
      case 5:
        return 17302452;
      case 4:
        return 17302456;
      case 3:
        return 17302455;
      case 2:
        break;
    } 
    return 17302451;
  }
  
  public void onUpdateExtractingVisibility(EditorInfo paramEditorInfo) {
    if (paramEditorInfo.inputType == 0 || (paramEditorInfo.imeOptions & 0x10000000) != 0) {
      setExtractViewShown(false);
      return;
    } 
    setExtractViewShown(true);
  }
  
  public void onUpdateExtractingViews(EditorInfo paramEditorInfo) {
    if (!isExtractViewShown())
      return; 
    if (this.mExtractAccessories == null)
      return; 
    CharSequence charSequence = paramEditorInfo.actionLabel;
    byte b = 1;
    int i = b;
    if (charSequence == null)
      if ((paramEditorInfo.imeOptions & 0xFF) != 1 && (paramEditorInfo.imeOptions & 0x20000000) == 0 && paramEditorInfo.inputType != 0) {
        i = b;
      } else {
        i = 0;
      }  
    if (i) {
      this.mExtractAccessories.setVisibility(0);
      View view = this.mExtractAction;
      if (view != null) {
        if (view instanceof ImageButton) {
          ImageButton imageButton = (ImageButton)view;
          i = paramEditorInfo.imeOptions;
          imageButton.setImageResource(getIconForImeAction(i));
          if (paramEditorInfo.actionLabel != null) {
            this.mExtractAction.setContentDescription(paramEditorInfo.actionLabel);
          } else {
            this.mExtractAction.setContentDescription(getTextForImeAction(paramEditorInfo.imeOptions));
          } 
        } else if (paramEditorInfo.actionLabel != null) {
          ((TextView)this.mExtractAction).setText(paramEditorInfo.actionLabel);
        } else {
          ((TextView)this.mExtractAction).setText(getTextForImeAction(paramEditorInfo.imeOptions));
        } 
        this.mExtractAction.setOnClickListener(this.mActionClickListener);
      } 
    } else {
      this.mExtractAccessories.setVisibility(8);
      View view = this.mExtractAction;
      if (view != null)
        view.setOnClickListener(null); 
    } 
  }
  
  public void onExtractingInputChanged(EditorInfo paramEditorInfo) {
    if (paramEditorInfo.inputType == 0)
      requestHideSelf(2); 
  }
  
  void startExtractingText(boolean paramBoolean) {
    ExtractEditText extractEditText = this.mExtractEditText;
    if (extractEditText != null && getCurrentInputStarted() && 
      isFullscreenMode()) {
      ExtractedText extractedText;
      this.mExtractedToken++;
      ExtractedTextRequest extractedTextRequest = new ExtractedTextRequest();
      extractedTextRequest.token = this.mExtractedToken;
      extractedTextRequest.flags = 1;
      extractedTextRequest.hintMaxLines = 10;
      extractedTextRequest.hintMaxChars = 10000;
      InputConnection inputConnection = getCurrentInputConnection();
      if (inputConnection == null) {
        extractedTextRequest = null;
      } else {
        extractedText = inputConnection.getExtractedText(extractedTextRequest, 1);
      } 
      this.mExtractedText = extractedText;
      if (extractedText == null || inputConnection == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected null in startExtractingText : mExtractedText = ");
        stringBuilder.append(this.mExtractedText);
        stringBuilder.append(", input connection = ");
        stringBuilder.append(inputConnection);
        Log.e("InputMethodService", stringBuilder.toString());
      } 
      null = getCurrentInputEditorInfo();
      try {
        extractEditText.startInternalChanges();
        onUpdateExtractingVisibility(null);
        onUpdateExtractingViews(null);
        int i = null.inputType;
        int j = i;
        if ((i & 0xF) == 1) {
          j = i;
          if ((0x40000 & i) != 0)
            j = i | 0x20000; 
        } 
        extractEditText.setInputType(j);
        extractEditText.setHint(null.hintText);
        if (this.mExtractedText != null) {
          extractEditText.setEnabled(true);
          extractEditText.setExtractedText(this.mExtractedText);
        } else {
          extractEditText.setEnabled(false);
          extractEditText.setText("");
        } 
        extractEditText.finishInternalChanges();
      } finally {
        extractEditText.finishInternalChanges();
      } 
    } 
  }
  
  private void dispatchOnCurrentInputMethodSubtypeChanged(InputMethodSubtype paramInputMethodSubtype) {
    synchronized (this.mLock) {
      this.mNotifyUserActionSent = false;
      onCurrentInputMethodSubtypeChanged(paramInputMethodSubtype);
      return;
    } 
  }
  
  protected void onCurrentInputMethodSubtypeChanged(InputMethodSubtype paramInputMethodSubtype) {
    if (DEBUG) {
      int i = paramInputMethodSubtype.getNameResId();
      String str1 = paramInputMethodSubtype.getMode();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("changeInputMethodSubtype:");
      if (i == 0) {
        str2 = "<none>";
      } else {
        str2 = getString(i);
      } 
      stringBuilder2.append(str2);
      stringBuilder2.append(",");
      stringBuilder2.append(str1);
      stringBuilder2.append(",");
      stringBuilder2.append(paramInputMethodSubtype.getLocale());
      stringBuilder2.append(",");
      stringBuilder2.append(paramInputMethodSubtype.getExtraValue());
      String str2 = stringBuilder2.toString();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("--- ");
      stringBuilder1.append(str2);
      Log.v("InputMethodService", stringBuilder1.toString());
    } 
  }
  
  @Deprecated
  public int getInputMethodWindowRecommendedHeight() {
    Log.w("InputMethodService", "getInputMethodWindowRecommendedHeight() is deprecated and now always returns 0. Do not use this method.");
    return 0;
  }
  
  public final void exposeContent(InputContentInfo paramInputContentInfo, InputConnection paramInputConnection) {
    if (paramInputConnection == null)
      return; 
    if (getCurrentInputConnection() != paramInputConnection)
      return; 
    exposeContentInternal(paramInputContentInfo, getCurrentInputEditorInfo());
  }
  
  public final void notifyUserActionIfNecessary() {
    synchronized (this.mLock) {
      if (this.mNotifyUserActionSent)
        return; 
      this.mPrivOps.notifyUserAction();
      this.mNotifyUserActionSent = true;
      return;
    } 
  }
  
  private void exposeContentInternal(InputContentInfo paramInputContentInfo, EditorInfo paramEditorInfo) {
    StringBuilder stringBuilder;
    Uri uri = paramInputContentInfo.getContentUri();
    InputMethodPrivilegedOperations inputMethodPrivilegedOperations = this.mPrivOps;
    String str = paramEditorInfo.packageName;
    IInputContentUriToken iInputContentUriToken = inputMethodPrivilegedOperations.createInputContentUriToken(uri, str);
    if (iInputContentUriToken == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("createInputContentAccessToken failed. contentUri=");
      stringBuilder.append(uri.toString());
      stringBuilder.append(" packageName=");
      stringBuilder.append(paramEditorInfo.packageName);
      Log.e("InputMethodService", stringBuilder.toString());
      return;
    } 
    stringBuilder.setUriToken(iInputContentUriToken);
  }
  
  private int mapToImeWindowStatus() {
    boolean bool = isInputViewShown();
    byte b = 2;
    if (bool) {
      if (this.mCanPreRender && !this.mWindowVisible)
        b = 4; 
    } else {
      b = 0;
    } 
    return 0x1 | b;
  }
  
  private boolean isAutomotive() {
    return getApplicationContext().getPackageManager().hasSystemFeature("android.hardware.type.automotive");
  }
  
  protected void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    PrintWriterPrinter printWriterPrinter = new PrintWriterPrinter(paramPrintWriter);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Input method service state for ");
    stringBuilder2.append(this);
    stringBuilder2.append(":");
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mViewsCreated=");
    stringBuilder2.append(this.mViewsCreated);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mDecorViewVisible=");
    stringBuilder2.append(this.mDecorViewVisible);
    stringBuilder2.append(" mDecorViewWasVisible=");
    stringBuilder2.append(this.mDecorViewWasVisible);
    stringBuilder2.append(" mWindowVisible=");
    stringBuilder2.append(this.mWindowVisible);
    stringBuilder2.append(" mInShowWindow=");
    stringBuilder2.append(this.mInShowWindow);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  Configuration=");
    stringBuilder2.append(getResources().getConfiguration());
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mToken=");
    stringBuilder2.append(this.mToken);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mInputBinding=");
    stringBuilder2.append(this.mInputBinding);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mInputConnection=");
    stringBuilder2.append(this.mInputConnection);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mStartedInputConnection=");
    stringBuilder2.append(this.mStartedInputConnection);
    printWriterPrinter.println(stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mInputStarted=");
    stringBuilder2.append(this.mInputStarted);
    stringBuilder2.append(" mInputViewStarted=");
    stringBuilder2.append(this.mInputViewStarted);
    stringBuilder2.append(" mCandidatesViewStarted=");
    stringBuilder2.append(this.mCandidatesViewStarted);
    printWriterPrinter.println(stringBuilder2.toString());
    if (this.mInputEditorInfo != null) {
      printWriterPrinter.println("  mInputEditorInfo:");
      this.mInputEditorInfo.dump((Printer)printWriterPrinter, "    ");
    } else {
      printWriterPrinter.println("  mInputEditorInfo: null");
    } 
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append("  mShowInputRequested=");
    stringBuilder2.append(this.mShowInputRequested);
    stringBuilder2.append(" mLastShowInputRequested=");
    stringBuilder2.append(this.mLastShowInputRequested);
    stringBuilder2.append(" mCanPreRender=");
    stringBuilder2.append(this.mCanPreRender);
    stringBuilder2.append(" mIsPreRendered=");
    stringBuilder2.append(this.mIsPreRendered);
    stringBuilder2.append(" mShowInputFlags=0x");
    int i = this.mShowInputFlags;
    stringBuilder2.append(Integer.toHexString(i));
    String str = stringBuilder2.toString();
    printWriterPrinter.println(str);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("  mCandidatesVisibility=");
    stringBuilder1.append(this.mCandidatesVisibility);
    stringBuilder1.append(" mFullscreenApplied=");
    stringBuilder1.append(this.mFullscreenApplied);
    stringBuilder1.append(" mIsFullscreen=");
    stringBuilder1.append(this.mIsFullscreen);
    stringBuilder1.append(" mExtractViewHidden=");
    stringBuilder1.append(this.mExtractViewHidden);
    printWriterPrinter.println(stringBuilder1.toString());
    if (this.mExtractedText != null) {
      printWriterPrinter.println("  mExtractedText:");
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("    text=");
      stringBuilder1.append(this.mExtractedText.text.length());
      stringBuilder1.append(" chars startOffset=");
      stringBuilder1.append(this.mExtractedText.startOffset);
      printWriterPrinter.println(stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("    selectionStart=");
      stringBuilder1.append(this.mExtractedText.selectionStart);
      stringBuilder1.append(" selectionEnd=");
      stringBuilder1.append(this.mExtractedText.selectionEnd);
      stringBuilder1.append(" flags=0x");
      i = this.mExtractedText.flags;
      stringBuilder1.append(Integer.toHexString(i));
      String str1 = stringBuilder1.toString();
      printWriterPrinter.println(str1);
    } else {
      printWriterPrinter.println("  mExtractedText: null");
    } 
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("  mExtractedToken=");
    stringBuilder1.append(this.mExtractedToken);
    printWriterPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("  mIsInputViewShown=");
    stringBuilder1.append(this.mIsInputViewShown);
    stringBuilder1.append(" mStatusIcon=");
    stringBuilder1.append(this.mStatusIcon);
    printWriterPrinter.println(stringBuilder1.toString());
    printWriterPrinter.println("Last computed insets:");
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("  contentTopInsets=");
    stringBuilder1.append(this.mTmpInsets.contentTopInsets);
    stringBuilder1.append(" visibleTopInsets=");
    stringBuilder1.append(this.mTmpInsets.visibleTopInsets);
    stringBuilder1.append(" touchableInsets=");
    stringBuilder1.append(this.mTmpInsets.touchableInsets);
    stringBuilder1.append(" touchableRegion=");
    stringBuilder1.append(this.mTmpInsets.touchableRegion);
    printWriterPrinter.println(stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append(" mSettingsObserver=");
    stringBuilder1.append(this.mSettingsObserver);
    printWriterPrinter.println(stringBuilder1.toString());
  }
  
  private void onColorChange(Uri paramUri) {
    this.mOplusInputMethodServiceUtils.onChange(paramUri);
  }
}
