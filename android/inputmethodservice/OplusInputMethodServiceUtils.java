package android.inputmethodservice;

import android.app.Dialog;
import android.common.IOplusCommonFeature;
import android.common.OplusFeatureCache;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.OplusSmartCutQuantizer;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Debug;
import android.os.Looper;
import android.os.MessageQueue;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.provider.Settings;
import android.util.Log;
import android.view.IOplusViewRootUtil;
import android.view.OplusBaseView;
import android.view.OplusBaseWindow;
import android.view.View;
import android.view.WindowManagerGlobal;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.statusbar.OplusStatusBarController;
import com.oplus.util.OplusNavigationBarUtil;
import com.oplus.util.OplusTypeCastingHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import oplus.util.OplusStatistics;

public class OplusInputMethodServiceUtils implements IOplusInputMethodServiceUtils {
  static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.imelog", false);
  
  static final boolean DEBUG_PANIC = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static int mNavigationBarState = 0;
  
  private ArrayList<String> mInnerList = new ArrayList<>(Arrays.asList(new String[] { "com.sohu.inputmethod.sogouoem", "com.baidu.input_oppo", "com.google.android.inputmethod.latin", "com.simeji.android.oppo" }));
  
  private int mInputMethodBottomColor = 0;
  
  Rect mTouchRect = new Rect();
  
  private boolean mIsFloatingInputMethod = false;
  
  private static final int BOTTOM_HEIGHT = 5;
  
  private static final int COLOR_SHIFT = 24;
  
  private static final int COLOR_WHITE = 16777215;
  
  private static final int CONTENT_JUDGE_WIDTH = 800;
  
  private static final int CONTENT_JUDGE_WIDTH_HALF = 400;
  
  private static final String DCS_TAG = "inputMethod";
  
  private static final int DELAY_TIME = 1000;
  
  private static final String EVENT_INPUT_METHOD_SHOW = "input_method_show";
  
  private static final String HIDE_NAVIGATIONBAR_ENABLE = "hide_navigationbar_enable";
  
  private static final int HIDE_SECURE_KEYBOARD = 20;
  
  private static final int HIDE_SECURE_KEYBOARD_ONLY = 21;
  
  private static final String KEYBOARD_PREVENT_TOUCH = "keyboard_prevent_touch";
  
  private static final int KEYBOARD_PREVENT_TOUCH_DEFAULT = -1;
  
  private static final int KEYBOARD_PREVENT_TOUCH_ON = 1;
  
  private static final String KEY_DURATION = "duration";
  
  private static final String KEY_INNER = "isInner";
  
  private static final String KEY_LABEL = "label";
  
  private static final String KEY_NAME = "packageName";
  
  private static final String KEY_OTA_VERSION = "otaVersion";
  
  private static final String KEY_START_TIME = "startTime";
  
  private static final String KEY_VERSION_CODE = "versionCode";
  
  private static final String KEY_VERSION_NAME = "versionName";
  
  public static final int MODE_NAVIGATIONBAR_GESTURE = 2;
  
  public static final int MODE_NAVIGATIONBAR_GESTURE_SIDE = 3;
  
  public static final int OPLUS_NAVIGATION_BAR_COLOR = -1;
  
  public static final int RETRY_COUNT_MAX = 3;
  
  static final String TAG = "InputMethodServiceUtils";
  
  private CacheRgbIdleHandler mCacheRgbIdleHandler;
  
  private InputMethodService mInputMethodService;
  
  private boolean mIsExpRom;
  
  private int mKeyboardPreventTouch;
  
  private String mOtaVersion;
  
  private int mReTryGetColorCount;
  
  class CacheRgbIdleHandler implements MessageQueue.IdleHandler {
    Dialog mWindow;
    
    final OplusInputMethodServiceUtils this$0;
    
    public CacheRgbIdleHandler(Dialog param1Dialog) {
      this.mWindow = param1Dialog;
    }
    
    public boolean queueIdle() {
      OplusInputMethodServiceUtils.this.getCacheRgb(this.mWindow);
      return false;
    }
  }
  
  public void init(Context paramContext) {
    this.mInputMethodService = (InputMethodService)paramContext;
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    mNavigationBarState = Settings.Secure.getInt(contentResolver, "hide_navigationbar_enable", 0);
    OplusNavigationBarUtil oplusNavigationBarUtil = OplusNavigationBarUtil.getInstance();
    if (mNavigationBarState == 2)
      bool = true; 
    oplusNavigationBarUtil.setImePackageInGestureMode(bool);
    this.mOtaVersion = SystemProperties.get("ro.build.version.ota");
    this.mKeyboardPreventTouch = Settings.Secure.getInt(this.mInputMethodService.getContentResolver(), "keyboard_prevent_touch", -1);
    this.mIsExpRom = OplusFeatureConfigManager.getInstance().hasFeature("oplus.software.inputmethod.cn") ^ true;
  }
  
  public boolean getDockSide() {
    boolean bool;
    int i = -1;
    try {
      int j = WindowManagerGlobal.getWindowManagerService().getDockedStackSide();
    } catch (RemoteException remoteException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Failed to get dock side: ");
      stringBuilder.append(remoteException);
      Log.w("InputMethodServiceUtils", stringBuilder.toString());
    } 
    if (i != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onChange(Uri paramUri) {
    Uri uri1 = Settings.Secure.getUriFor("hide_navigationbar_enable");
    Uri uri2 = Settings.Secure.getUriFor("keyboard_prevent_touch");
    if (uri1.equals(paramUri)) {
      ContentResolver contentResolver = this.mInputMethodService.getContentResolver();
      boolean bool = false;
      int i = Settings.Secure.getInt(contentResolver, "hide_navigationbar_enable", 0);
      if (i == 2 || i == 3)
        bool = true; 
      OplusNavigationBarUtil.getInstance().setImePackageInGestureMode(bool);
    } 
    if (uri2.equals(paramUri))
      this.mKeyboardPreventTouch = Settings.Secure.getInt(this.mInputMethodService.getContentResolver(), "keyboard_prevent_touch", -1); 
  }
  
  public void updateNavigationGuardColor(Dialog paramDialog) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateNavigationGuardColor: ");
      stringBuilder.append(Debug.getCallers(3));
      Log.d("InputMethodServiceUtils", stringBuilder.toString());
    } 
    this.mReTryGetColorCount = 3;
    if (paramDialog.getWindow() != null)
      paramDialog.getWindow().getDecorView().post(new _$$Lambda$OplusInputMethodServiceUtils$wLVWWEInRPhN4WF_sjxWxIVHIpU(this, paramDialog)); 
  }
  
  public void uploadData(long paramLong) {
    HashMap<Object, Object> hashMap = new HashMap<>();
    long l = System.currentTimeMillis();
    int i = (int)((l - paramLong) / 1000.0D + 0.5D);
    String str = this.mInputMethodService.getPackageName();
    hashMap.put("packageName", this.mInputMethodService.getPackageName());
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(i);
    stringBuilder2.append("");
    hashMap.put("duration", stringBuilder2.toString());
    stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramLong);
    stringBuilder2.append("");
    hashMap.put("startTime", stringBuilder2.toString());
    try {
      PackageManager packageManager = this.mInputMethodService.getPackageManager();
      PackageInfo packageInfo = packageManager.getPackageInfo(str, 0);
      hashMap.put("versionName", packageInfo.versionName);
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(packageInfo.versionCode);
      stringBuilder.append("");
      hashMap.put("versionCode", stringBuilder.toString());
      ApplicationInfo applicationInfo = packageManager.getApplicationInfo(str, 0);
      hashMap.put("label", applicationInfo.loadLabel(packageManager).toString());
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    hashMap.put("otaVersion", this.mOtaVersion);
    boolean bool = this.mInnerList.contains(str);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(bool);
    stringBuilder1.append("");
    hashMap.put("isInner", stringBuilder1.toString());
    OplusStatistics.onCommon((Context)this.mInputMethodService, "inputMethod", "input_method_show", hashMap, false);
  }
  
  private int getCacheRgb(Dialog paramDialog) {
    if (paramDialog == null || paramDialog.getWindow() == null)
      return -1; 
    OplusBaseView oplusBaseView = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, paramDialog.getWindow().getDecorView());
    OplusBaseWindow oplusBaseWindow = (OplusBaseWindow)OplusTypeCastingHelper.typeCasting(OplusBaseWindow.class, paramDialog.getWindow());
    int i = paramDialog.getWindow().getNavigationBarColor();
    if (DEBUG_PANIC) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getNavigationBarColor: ");
      stringBuilder.append(Integer.toHexString(i));
      Log.d("InputMethodServiceUtils", stringBuilder.toString());
    } 
    if (!oplusBaseWindow.isUseDefaultNavigationBarColor() && i != -16777216 && i != -1)
      return -1; 
    View view = paramDialog.findViewById(16909279);
    if (view != null) {
      Rect rect = new Rect();
      view.getBoundsOnScreen(rect, true);
      i = rect.top;
      if (rect.bottom > 5) {
        rect.top = rect.bottom - 5;
        OplusBaseView oplusBaseView1 = (OplusBaseView)OplusTypeCastingHelper.typeCasting(OplusBaseView.class, view);
        if (oplusBaseView1 != null) {
          Bitmap bitmap = oplusBaseView1.getColorCustomDrawingCache(rect, i);
        } else {
          rect = null;
        } 
        if (rect != null && !rect.isRecycled()) {
          Bitmap bitmap;
          int j = rect.getWidth();
          i = rect.getHeight();
          if (i < 5 || j <= 0) {
            rect.recycle();
            return -1;
          } 
          if (j >= 800) {
            bitmap = Bitmap.createBitmap((Bitmap)rect, j / 2 - 400, i - 5, 800, 5);
          } else {
            bitmap = Bitmap.createBitmap((Bitmap)rect, 0, i - 5, j, 5);
          } 
          if (bitmap != null) {
            i = getTransparentBitmap(bitmap);
            int k = bitmap.getWidth();
            j = bitmap.getHeight();
            int[] arrayOfInt = new int[k * j];
            bitmap.getPixels(arrayOfInt, 0, k, 0, 0, k, j);
            OplusSmartCutQuantizer oplusSmartCutQuantizer = new OplusSmartCutQuantizer(arrayOfInt);
            j = oplusSmartCutQuantizer.getDominantColor();
            if (j == 0) {
              i = i << 24 | j & 0xFFFFFF;
            } else {
              i = j & 0xFFFFFF | 0xFF000000;
            } 
            if (DEBUG_PANIC) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("getCacheRgb: ");
              stringBuilder.append(Integer.toHexString(i));
              Log.d("InputMethodServiceUtils", stringBuilder.toString());
            } 
            if (paramDialog.getWindow() != null)
              if (i == 0 && !this.mIsFloatingInputMethod) {
                oplusBaseView.updateColorNavigationGuardColor(OplusStatusBarController.getDefaultNavigationBarColor((Context)this.mInputMethodService));
                updateNavigationGuardColorDelay(paramDialog);
              } else {
                oplusBaseView.updateColorNavigationGuardColor(i);
              }  
            bitmap.recycle();
            rect.recycle();
          } 
        } 
      } else {
        return -1;
      } 
    } 
    return -1;
  }
  
  private int getTransparentBitmap(Bitmap paramBitmap) {
    int i = paramBitmap.getWidth() * paramBitmap.getHeight();
    int[] arrayOfInt = new int[i];
    paramBitmap.getPixels(arrayOfInt, 0, paramBitmap.getWidth(), 0, 0, paramBitmap.getWidth(), paramBitmap.getHeight());
    int j = 0;
    for (byte b = 0; b < arrayOfInt.length; b++)
      j += arrayOfInt[b] >>> 24; 
    return j / i;
  }
  
  public void onComputeRaise(InputMethodService.Insets paramInsets, Dialog paramDialog) {
    View view = null;
    if (paramDialog.getWindow() != null)
      view = paramDialog.getWindow().getDecorView().findViewById(16908336); 
    if (view != null) {
      int[] arrayOfInt = new int[2];
      view.getLocationInWindow(arrayOfInt);
      Rect rect = new Rect(arrayOfInt[0], arrayOfInt[1], arrayOfInt[0] + view.getWidth(), arrayOfInt[1] + view.getHeight());
      this.mTouchRect = paramInsets.touchableRegion.getBounds();
      isFloatingInputMethod(paramDialog);
      paramInsets.touchableRegion.union(rect);
    } 
  }
  
  public void updateNavigationGuardColorDelay(Dialog paramDialog) {
    if (this.mCacheRgbIdleHandler == null) {
      this.mCacheRgbIdleHandler = new CacheRgbIdleHandler(paramDialog);
    } else {
      Looper.myQueue().removeIdleHandler(this.mCacheRgbIdleHandler);
    } 
    if (this.mReTryGetColorCount > 0) {
      Looper.myQueue().addIdleHandler(this.mCacheRgbIdleHandler);
      this.mReTryGetColorCount--;
    } 
    ((IOplusViewRootUtil)OplusFeatureCache.getOrCreate((IOplusCommonFeature)IOplusViewRootUtil.DEFAULT, new Object[0])).checkGestureConfig((Context)this.mInputMethodService);
  }
  
  public void onCreateAndRegister(ContentObserver paramContentObserver) {
    ContentResolver contentResolver1 = this.mInputMethodService.getContentResolver();
    Uri uri2 = Settings.Secure.getUriFor("hide_navigationbar_enable");
    contentResolver1.registerContentObserver(uri2, false, paramContentObserver);
    ContentResolver contentResolver2 = this.mInputMethodService.getContentResolver();
    Uri uri1 = Settings.Secure.getUriFor("keyboard_prevent_touch");
    contentResolver2.registerContentObserver(uri1, false, paramContentObserver);
  }
  
  private void isFloatingInputMethod(Dialog paramDialog) {
    boolean bool1 = paramDialog.isShowing(), bool2 = false;
    if (bool1 && this.mTouchRect.width() != 0 && paramDialog.getWindow() != null) {
      if (this.mTouchRect.width() != paramDialog.getWindow().getDecorView().getWidth())
        bool2 = true; 
      if (bool2 != this.mIsFloatingInputMethod)
        this.mIsFloatingInputMethod = bool2; 
    } else {
      this.mIsFloatingInputMethod = false;
    } 
  }
  
  private void onFloatingStateChange(Dialog paramDialog) {
    if (DEBUG_PANIC) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onFloatingStateChange: ");
      stringBuilder.append(this.mIsFloatingInputMethod);
      Log.d("InputMethodServiceUtils", stringBuilder.toString());
    } 
    updateNavigationGuardColor(paramDialog);
  }
  
  public boolean hideImmediately(int paramInt, Dialog paramDialog) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("hideImmediately: ");
      stringBuilder.append(paramInt);
      Log.d("InputMethodServiceUtils", stringBuilder.toString());
    } 
    if (paramDialog != null) {
      if (paramInt == 20) {
        paramDialog.hide();
        return false;
      } 
      if (paramInt == 21) {
        paramDialog.hide();
        return true;
      } 
      return false;
    } 
    return false;
  }
  
  public int changeFlag(int paramInt) {
    return 20;
  }
  
  public boolean skipInsetChange(int paramInt) {
    boolean bool;
    if (paramInt != 20) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean shouldPreventTouch() {
    int i = this.mKeyboardPreventTouch;
    boolean bool = true;
    if (i == -1) {
      bool = this.mIsExpRom;
    } else if (i != 1) {
      bool = false;
    } 
    return bool;
  }
}
