package android.inputmethodservice;

import android.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import android.widget.PopupWindow;
import android.widget.TextView;
import com.android.internal.R;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Deprecated
public class KeyboardView extends View implements View.OnClickListener {
  private static final int[] KEY_DELETE = new int[] { -5 };
  
  private static final int[] LONG_PRESSABLE_STATE_SET = new int[] { 16843324 };
  
  private int mCurrentKeyIndex = -1;
  
  private final int[] mCoordinates = new int[2];
  
  private boolean mPreviewCentered = false;
  
  private boolean mShowPreview = true;
  
  private boolean mShowTouchPoints = true;
  
  private int mCurrentKey = -1;
  
  private int mDownKey = -1;
  
  private int[] mKeyIndices = new int[12];
  
  private int mRepeatKeyIndex = -1;
  
  private Rect mClipRegion = new Rect(0, 0, 0, 0);
  
  private SwipeTracker mSwipeTracker = new SwipeTracker();
  
  private int mOldPointerCount = 1;
  
  static {
    LONGPRESS_TIMEOUT = ViewConfiguration.getLongPressTimeout();
    MAX_NEARBY_KEYS = 12;
  }
  
  private int[] mDistances = new int[MAX_NEARBY_KEYS];
  
  private StringBuilder mPreviewLabel = new StringBuilder(1);
  
  private Rect mDirtyRect = new Rect();
  
  private static final int DEBOUNCE_TIME = 70;
  
  private static final boolean DEBUG = false;
  
  private static final int DELAY_AFTER_PREVIEW = 70;
  
  private static final int DELAY_BEFORE_PREVIEW = 0;
  
  private static final int LONGPRESS_TIMEOUT;
  
  private static int MAX_NEARBY_KEYS = 0;
  
  private static final int MSG_LONGPRESS = 4;
  
  private static final int MSG_REMOVE_PREVIEW = 2;
  
  private static final int MSG_REPEAT = 3;
  
  private static final int MSG_SHOW_PREVIEW = 1;
  
  private static final int MULTITAP_INTERVAL = 800;
  
  private static final int NOT_A_KEY = -1;
  
  private static final int REPEAT_INTERVAL = 50;
  
  private static final int REPEAT_START_DELAY = 400;
  
  private boolean mAbortKey;
  
  private AccessibilityManager mAccessibilityManager;
  
  private AudioManager mAudioManager;
  
  private float mBackgroundDimAmount;
  
  private Bitmap mBuffer;
  
  private Canvas mCanvas;
  
  private long mCurrentKeyTime;
  
  private boolean mDisambiguateSwipe;
  
  private long mDownTime;
  
  private boolean mDrawPending;
  
  private GestureDetector mGestureDetector;
  
  Handler mHandler;
  
  private boolean mHeadsetRequiredToHearPasswordsAnnounced;
  
  private boolean mInMultiTap;
  
  private Keyboard.Key mInvalidatedKey;
  
  private Drawable mKeyBackground;
  
  private int mKeyTextColor;
  
  private int mKeyTextSize;
  
  private Keyboard mKeyboard;
  
  private OnKeyboardActionListener mKeyboardActionListener;
  
  private boolean mKeyboardChanged;
  
  private Keyboard.Key[] mKeys;
  
  private int mLabelTextSize;
  
  private int mLastCodeX;
  
  private int mLastCodeY;
  
  private int mLastKey;
  
  private long mLastKeyTime;
  
  private long mLastMoveTime;
  
  private int mLastSentIndex;
  
  private long mLastTapTime;
  
  private int mLastX;
  
  private int mLastY;
  
  private KeyboardView mMiniKeyboard;
  
  private Map<Keyboard.Key, View> mMiniKeyboardCache;
  
  private View mMiniKeyboardContainer;
  
  private int mMiniKeyboardOffsetX;
  
  private int mMiniKeyboardOffsetY;
  
  private boolean mMiniKeyboardOnScreen;
  
  private float mOldPointerX;
  
  private float mOldPointerY;
  
  private Rect mPadding;
  
  private Paint mPaint;
  
  private PopupWindow mPopupKeyboard;
  
  private int mPopupLayout;
  
  private View mPopupParent;
  
  private int mPopupPreviewX;
  
  private int mPopupPreviewY;
  
  private int mPopupX;
  
  private int mPopupY;
  
  private boolean mPossiblePoly;
  
  private int mPreviewHeight;
  
  private int mPreviewOffset;
  
  private PopupWindow mPreviewPopup;
  
  private TextView mPreviewText;
  
  private int mPreviewTextSizeLarge;
  
  private boolean mProximityCorrectOn;
  
  private int mProximityThreshold;
  
  private int mShadowColor;
  
  private float mShadowRadius;
  
  private int mStartX;
  
  private int mStartY;
  
  private int mSwipeThreshold;
  
  private int mTapCount;
  
  private int mVerticalCorrection;
  
  public KeyboardView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17956963);
  }
  
  public KeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public KeyboardView(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.KeyboardView, paramInt1, paramInt2);
    LayoutInflater layoutInflater = (LayoutInflater)paramContext.getSystemService("layout_inflater");
    paramInt2 = 0;
    int i = typedArray.getIndexCount();
    for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
      int j = typedArray.getIndex(paramInt1);
      switch (j) {
        case 10:
          this.mPopupLayout = typedArray.getResourceId(j, 0);
          break;
        case 9:
          this.mVerticalCorrection = typedArray.getDimensionPixelOffset(j, 0);
          break;
        case 8:
          this.mPreviewHeight = typedArray.getDimensionPixelSize(j, 80);
          break;
        case 7:
          this.mPreviewOffset = typedArray.getDimensionPixelOffset(j, 0);
          break;
        case 6:
          paramInt2 = typedArray.getResourceId(j, 0);
          break;
        case 5:
          this.mKeyTextColor = typedArray.getColor(j, -16777216);
          break;
        case 4:
          this.mLabelTextSize = typedArray.getDimensionPixelSize(j, 14);
          break;
        case 3:
          this.mKeyTextSize = typedArray.getDimensionPixelSize(j, 18);
          break;
        case 2:
          this.mKeyBackground = typedArray.getDrawable(j);
          break;
        case 1:
          this.mShadowRadius = typedArray.getFloat(j, 0.0F);
          break;
        case 0:
          this.mShadowColor = typedArray.getColor(j, 0);
          break;
      } 
    } 
    typedArray = this.mContext.obtainStyledAttributes(R.styleable.Theme);
    this.mBackgroundDimAmount = typedArray.getFloat(2, 0.5F);
    this.mPreviewPopup = new PopupWindow(paramContext);
    if (paramInt2 != 0) {
      TextView textView = (TextView)layoutInflater.inflate(paramInt2, null);
      this.mPreviewTextSizeLarge = (int)textView.getTextSize();
      this.mPreviewPopup.setContentView((View)this.mPreviewText);
      this.mPreviewPopup.setBackgroundDrawable(null);
    } else {
      this.mShowPreview = false;
    } 
    this.mPreviewPopup.setTouchable(false);
    PopupWindow popupWindow = new PopupWindow(paramContext);
    popupWindow.setBackgroundDrawable(null);
    this.mPopupParent = this;
    Paint paint = new Paint();
    paint.setAntiAlias(true);
    this.mPaint.setTextSize(false);
    this.mPaint.setTextAlign(Paint.Align.CENTER);
    this.mPaint.setAlpha(255);
    this.mPadding = new Rect(0, 0, 0, 0);
    this.mMiniKeyboardCache = new HashMap<>();
    this.mKeyBackground.getPadding(this.mPadding);
    this.mSwipeThreshold = (int)((getResources().getDisplayMetrics()).density * 500.0F);
    this.mDisambiguateSwipe = getResources().getBoolean(17891559);
    this.mAccessibilityManager = AccessibilityManager.getInstance(paramContext);
    this.mAudioManager = (AudioManager)paramContext.getSystemService("audio");
    resetMultiTap();
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    initGestureDetector();
    if (this.mHandler == null)
      this.mHandler = (Handler)new Object(this); 
  }
  
  private void initGestureDetector() {
    if (this.mGestureDetector == null) {
      GestureDetector gestureDetector = new GestureDetector(getContext(), (GestureDetector.OnGestureListener)new Object(this));
      gestureDetector.setIsLongpressEnabled(false);
    } 
  }
  
  public void setOnKeyboardActionListener(OnKeyboardActionListener paramOnKeyboardActionListener) {
    this.mKeyboardActionListener = paramOnKeyboardActionListener;
  }
  
  protected OnKeyboardActionListener getOnKeyboardActionListener() {
    return this.mKeyboardActionListener;
  }
  
  public void setKeyboard(Keyboard paramKeyboard) {
    if (this.mKeyboard != null)
      showPreview(-1); 
    removeMessages();
    this.mKeyboard = paramKeyboard;
    List<Keyboard.Key> list = paramKeyboard.getKeys();
    this.mKeys = list.<Keyboard.Key>toArray(new Keyboard.Key[list.size()]);
    requestLayout();
    this.mKeyboardChanged = true;
    invalidateAllKeys();
    computeProximityThreshold(paramKeyboard);
    this.mMiniKeyboardCache.clear();
    this.mAbortKey = true;
  }
  
  public Keyboard getKeyboard() {
    return this.mKeyboard;
  }
  
  public boolean setShifted(boolean paramBoolean) {
    Keyboard keyboard = this.mKeyboard;
    if (keyboard != null && 
      keyboard.setShifted(paramBoolean)) {
      invalidateAllKeys();
      return true;
    } 
    return false;
  }
  
  public boolean isShifted() {
    Keyboard keyboard = this.mKeyboard;
    if (keyboard != null)
      return keyboard.isShifted(); 
    return false;
  }
  
  public void setPreviewEnabled(boolean paramBoolean) {
    this.mShowPreview = paramBoolean;
  }
  
  public boolean isPreviewEnabled() {
    return this.mShowPreview;
  }
  
  public void setVerticalCorrection(int paramInt) {}
  
  public void setPopupParent(View paramView) {
    this.mPopupParent = paramView;
  }
  
  public void setPopupOffset(int paramInt1, int paramInt2) {
    this.mMiniKeyboardOffsetX = paramInt1;
    this.mMiniKeyboardOffsetY = paramInt2;
    if (this.mPreviewPopup.isShowing())
      this.mPreviewPopup.dismiss(); 
  }
  
  public void setProximityCorrectionEnabled(boolean paramBoolean) {
    this.mProximityCorrectOn = paramBoolean;
  }
  
  public boolean isProximityCorrectionEnabled() {
    return this.mProximityCorrectOn;
  }
  
  public void onClick(View paramView) {
    dismissPopupKeyboard();
  }
  
  private CharSequence adjustCase(CharSequence paramCharSequence) {
    CharSequence charSequence = paramCharSequence;
    if (this.mKeyboard.isShifted()) {
      charSequence = paramCharSequence;
      if (paramCharSequence != null) {
        charSequence = paramCharSequence;
        if (paramCharSequence.length() < 3) {
          charSequence = paramCharSequence;
          if (Character.isLowerCase(paramCharSequence.charAt(0)))
            charSequence = paramCharSequence.toString().toUpperCase(); 
        } 
      } 
    } 
    return charSequence;
  }
  
  public void onMeasure(int paramInt1, int paramInt2) {
    Keyboard keyboard = this.mKeyboard;
    if (keyboard == null) {
      setMeasuredDimension(this.mPaddingLeft + this.mPaddingRight, this.mPaddingTop + this.mPaddingBottom);
    } else {
      int i = keyboard.getMinWidth() + this.mPaddingLeft + this.mPaddingRight;
      paramInt2 = i;
      if (View.MeasureSpec.getSize(paramInt1) < i + 10)
        paramInt2 = View.MeasureSpec.getSize(paramInt1); 
      setMeasuredDimension(paramInt2, this.mKeyboard.getHeight() + this.mPaddingTop + this.mPaddingBottom);
    } 
  }
  
  private void computeProximityThreshold(Keyboard paramKeyboard) {
    if (paramKeyboard == null)
      return; 
    Keyboard.Key[] arrayOfKey = this.mKeys;
    if (arrayOfKey == null)
      return; 
    int i = arrayOfKey.length;
    int j = 0;
    int k;
    for (k = 0; k < i; k++) {
      Keyboard.Key key = arrayOfKey[k];
      j += Math.min(key.width, key.height) + key.gap;
    } 
    if (j < 0 || i == 0)
      return; 
    this.mProximityThreshold = k = (int)(j * 1.4F / i);
    this.mProximityThreshold = k * k;
  }
  
  public void onSizeChanged(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    super.onSizeChanged(paramInt1, paramInt2, paramInt3, paramInt4);
    Keyboard keyboard = this.mKeyboard;
    if (keyboard != null)
      keyboard.resize(paramInt1, paramInt2); 
    this.mBuffer = null;
  }
  
  public void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    if (this.mDrawPending || this.mBuffer == null || this.mKeyboardChanged)
      onBufferDraw(); 
    paramCanvas.drawBitmap(this.mBuffer, 0.0F, 0.0F, null);
  }
  
  private void onBufferDraw() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBuffer : Landroid/graphics/Bitmap;
    //   4: ifnull -> 14
    //   7: aload_0
    //   8: getfield mKeyboardChanged : Z
    //   11: ifeq -> 109
    //   14: aload_0
    //   15: getfield mBuffer : Landroid/graphics/Bitmap;
    //   18: astore_1
    //   19: aload_1
    //   20: ifnull -> 55
    //   23: aload_0
    //   24: getfield mKeyboardChanged : Z
    //   27: ifeq -> 100
    //   30: aload_1
    //   31: invokevirtual getWidth : ()I
    //   34: aload_0
    //   35: invokevirtual getWidth : ()I
    //   38: if_icmpne -> 55
    //   41: aload_0
    //   42: getfield mBuffer : Landroid/graphics/Bitmap;
    //   45: invokevirtual getHeight : ()I
    //   48: aload_0
    //   49: invokevirtual getHeight : ()I
    //   52: if_icmpeq -> 100
    //   55: iconst_1
    //   56: aload_0
    //   57: invokevirtual getWidth : ()I
    //   60: invokestatic max : (II)I
    //   63: istore_2
    //   64: iconst_1
    //   65: aload_0
    //   66: invokevirtual getHeight : ()I
    //   69: invokestatic max : (II)I
    //   72: istore_3
    //   73: aload_0
    //   74: iload_2
    //   75: iload_3
    //   76: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   79: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   82: putfield mBuffer : Landroid/graphics/Bitmap;
    //   85: aload_0
    //   86: new android/graphics/Canvas
    //   89: dup
    //   90: aload_0
    //   91: getfield mBuffer : Landroid/graphics/Bitmap;
    //   94: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   97: putfield mCanvas : Landroid/graphics/Canvas;
    //   100: aload_0
    //   101: invokevirtual invalidateAllKeys : ()V
    //   104: aload_0
    //   105: iconst_0
    //   106: putfield mKeyboardChanged : Z
    //   109: aload_0
    //   110: getfield mKeyboard : Landroid/inputmethodservice/Keyboard;
    //   113: ifnonnull -> 117
    //   116: return
    //   117: aload_0
    //   118: getfield mCanvas : Landroid/graphics/Canvas;
    //   121: invokevirtual save : ()I
    //   124: pop
    //   125: aload_0
    //   126: getfield mCanvas : Landroid/graphics/Canvas;
    //   129: astore #4
    //   131: aload #4
    //   133: aload_0
    //   134: getfield mDirtyRect : Landroid/graphics/Rect;
    //   137: invokevirtual clipRect : (Landroid/graphics/Rect;)Z
    //   140: pop
    //   141: aload_0
    //   142: getfield mPaint : Landroid/graphics/Paint;
    //   145: astore #5
    //   147: aload_0
    //   148: getfield mKeyBackground : Landroid/graphics/drawable/Drawable;
    //   151: astore #6
    //   153: aload_0
    //   154: getfield mClipRegion : Landroid/graphics/Rect;
    //   157: astore #7
    //   159: aload_0
    //   160: getfield mPadding : Landroid/graphics/Rect;
    //   163: astore #8
    //   165: aload_0
    //   166: getfield mPaddingLeft : I
    //   169: istore #9
    //   171: aload_0
    //   172: getfield mPaddingTop : I
    //   175: istore #10
    //   177: aload_0
    //   178: getfield mKeys : [Landroid/inputmethodservice/Keyboard$Key;
    //   181: astore_1
    //   182: aload_0
    //   183: getfield mInvalidatedKey : Landroid/inputmethodservice/Keyboard$Key;
    //   186: astore #11
    //   188: aload #5
    //   190: aload_0
    //   191: getfield mKeyTextColor : I
    //   194: invokevirtual setColor : (I)V
    //   197: aload #11
    //   199: ifnull -> 301
    //   202: aload #4
    //   204: aload #7
    //   206: invokevirtual getClipBounds : (Landroid/graphics/Rect;)Z
    //   209: ifeq -> 301
    //   212: aload #11
    //   214: getfield x : I
    //   217: iload #9
    //   219: iadd
    //   220: iconst_1
    //   221: isub
    //   222: aload #7
    //   224: getfield left : I
    //   227: if_icmpgt -> 301
    //   230: aload #11
    //   232: getfield y : I
    //   235: iload #10
    //   237: iadd
    //   238: iconst_1
    //   239: isub
    //   240: aload #7
    //   242: getfield top : I
    //   245: if_icmpgt -> 301
    //   248: aload #11
    //   250: getfield x : I
    //   253: aload #11
    //   255: getfield width : I
    //   258: iadd
    //   259: iload #9
    //   261: iadd
    //   262: iconst_1
    //   263: iadd
    //   264: aload #7
    //   266: getfield right : I
    //   269: if_icmplt -> 301
    //   272: aload #11
    //   274: getfield y : I
    //   277: aload #11
    //   279: getfield height : I
    //   282: iadd
    //   283: iload #10
    //   285: iadd
    //   286: iconst_1
    //   287: iadd
    //   288: aload #7
    //   290: getfield bottom : I
    //   293: if_icmplt -> 301
    //   296: iconst_1
    //   297: istore_3
    //   298: goto -> 303
    //   301: iconst_0
    //   302: istore_3
    //   303: aload #4
    //   305: iconst_0
    //   306: getstatic android/graphics/PorterDuff$Mode.CLEAR : Landroid/graphics/PorterDuff$Mode;
    //   309: invokevirtual drawColor : (ILandroid/graphics/PorterDuff$Mode;)V
    //   312: aload_1
    //   313: arraylength
    //   314: istore_2
    //   315: iconst_0
    //   316: istore #12
    //   318: iload #12
    //   320: iload_2
    //   321: if_icmpge -> 881
    //   324: aload_1
    //   325: iload #12
    //   327: aaload
    //   328: astore #13
    //   330: iload_3
    //   331: ifeq -> 344
    //   334: aload #11
    //   336: aload #13
    //   338: if_acmpeq -> 344
    //   341: goto -> 875
    //   344: aload #13
    //   346: invokevirtual getCurrentDrawableState : ()[I
    //   349: astore #7
    //   351: aload #6
    //   353: aload #7
    //   355: invokevirtual setState : ([I)Z
    //   358: pop
    //   359: aload #13
    //   361: getfield label : Ljava/lang/CharSequence;
    //   364: ifnonnull -> 373
    //   367: aconst_null
    //   368: astore #7
    //   370: goto -> 389
    //   373: aload_0
    //   374: aload #13
    //   376: getfield label : Ljava/lang/CharSequence;
    //   379: invokespecial adjustCase : (Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    //   382: invokeinterface toString : ()Ljava/lang/String;
    //   387: astore #7
    //   389: aload #6
    //   391: invokevirtual getBounds : ()Landroid/graphics/Rect;
    //   394: astore #14
    //   396: aload #13
    //   398: getfield width : I
    //   401: aload #14
    //   403: getfield right : I
    //   406: if_icmpne -> 428
    //   409: aload #13
    //   411: getfield height : I
    //   414: aload #14
    //   416: getfield bottom : I
    //   419: if_icmpeq -> 425
    //   422: goto -> 428
    //   425: goto -> 445
    //   428: aload #6
    //   430: iconst_0
    //   431: iconst_0
    //   432: aload #13
    //   434: getfield width : I
    //   437: aload #13
    //   439: getfield height : I
    //   442: invokevirtual setBounds : (IIII)V
    //   445: aload #4
    //   447: aload #13
    //   449: getfield x : I
    //   452: iload #9
    //   454: iadd
    //   455: i2f
    //   456: aload #13
    //   458: getfield y : I
    //   461: iload #10
    //   463: iadd
    //   464: i2f
    //   465: invokevirtual translate : (FF)V
    //   468: aload #6
    //   470: aload #4
    //   472: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   475: aload #7
    //   477: ifnull -> 659
    //   480: aload #7
    //   482: invokevirtual length : ()I
    //   485: iconst_1
    //   486: if_icmple -> 521
    //   489: aload #13
    //   491: getfield codes : [I
    //   494: arraylength
    //   495: iconst_2
    //   496: if_icmpge -> 521
    //   499: aload #5
    //   501: aload_0
    //   502: getfield mLabelTextSize : I
    //   505: i2f
    //   506: invokevirtual setTextSize : (F)V
    //   509: aload #5
    //   511: getstatic android/graphics/Typeface.DEFAULT_BOLD : Landroid/graphics/Typeface;
    //   514: invokevirtual setTypeface : (Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    //   517: pop
    //   518: goto -> 540
    //   521: aload #5
    //   523: aload_0
    //   524: getfield mKeyTextSize : I
    //   527: i2f
    //   528: invokevirtual setTextSize : (F)V
    //   531: aload #5
    //   533: getstatic android/graphics/Typeface.DEFAULT : Landroid/graphics/Typeface;
    //   536: invokevirtual setTypeface : (Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
    //   539: pop
    //   540: aload #5
    //   542: aload_0
    //   543: getfield mShadowRadius : F
    //   546: fconst_0
    //   547: fconst_0
    //   548: aload_0
    //   549: getfield mShadowColor : I
    //   552: invokevirtual setShadowLayer : (FFFI)V
    //   555: aload #13
    //   557: getfield width : I
    //   560: aload #8
    //   562: getfield left : I
    //   565: isub
    //   566: aload #8
    //   568: getfield right : I
    //   571: isub
    //   572: iconst_2
    //   573: idiv
    //   574: aload #8
    //   576: getfield left : I
    //   579: iadd
    //   580: i2f
    //   581: fstore #15
    //   583: aload #13
    //   585: getfield height : I
    //   588: aload #8
    //   590: getfield top : I
    //   593: isub
    //   594: aload #8
    //   596: getfield bottom : I
    //   599: isub
    //   600: iconst_2
    //   601: idiv
    //   602: i2f
    //   603: fstore #16
    //   605: aload #5
    //   607: invokevirtual getTextSize : ()F
    //   610: aload #5
    //   612: invokevirtual descent : ()F
    //   615: fsub
    //   616: fconst_2
    //   617: fdiv
    //   618: fstore #17
    //   620: aload #8
    //   622: getfield top : I
    //   625: i2f
    //   626: fstore #18
    //   628: aload #4
    //   630: aload #7
    //   632: fload #15
    //   634: fload #16
    //   636: fload #17
    //   638: fadd
    //   639: fload #18
    //   641: fadd
    //   642: aload #5
    //   644: invokevirtual drawText : (Ljava/lang/String;FFLandroid/graphics/Paint;)V
    //   647: aload #5
    //   649: fconst_0
    //   650: fconst_0
    //   651: fconst_0
    //   652: iconst_0
    //   653: invokevirtual setShadowLayer : (FFFI)V
    //   656: goto -> 850
    //   659: aload #13
    //   661: getfield icon : Landroid/graphics/drawable/Drawable;
    //   664: ifnull -> 850
    //   667: aload #13
    //   669: getfield width : I
    //   672: istore #19
    //   674: aload #8
    //   676: getfield left : I
    //   679: istore #20
    //   681: aload #8
    //   683: getfield right : I
    //   686: istore #21
    //   688: aload #13
    //   690: getfield icon : Landroid/graphics/drawable/Drawable;
    //   693: astore #7
    //   695: iload #19
    //   697: iload #20
    //   699: isub
    //   700: iload #21
    //   702: isub
    //   703: aload #7
    //   705: invokevirtual getIntrinsicWidth : ()I
    //   708: isub
    //   709: iconst_2
    //   710: idiv
    //   711: aload #8
    //   713: getfield left : I
    //   716: iadd
    //   717: istore #21
    //   719: aload #13
    //   721: getfield height : I
    //   724: istore #19
    //   726: aload #8
    //   728: getfield top : I
    //   731: istore #20
    //   733: aload #8
    //   735: getfield bottom : I
    //   738: istore #22
    //   740: aload #13
    //   742: getfield icon : Landroid/graphics/drawable/Drawable;
    //   745: astore #7
    //   747: iload #19
    //   749: iload #20
    //   751: isub
    //   752: iload #22
    //   754: isub
    //   755: aload #7
    //   757: invokevirtual getIntrinsicHeight : ()I
    //   760: isub
    //   761: iconst_2
    //   762: idiv
    //   763: aload #8
    //   765: getfield top : I
    //   768: iadd
    //   769: istore #19
    //   771: aload #4
    //   773: iload #21
    //   775: i2f
    //   776: iload #19
    //   778: i2f
    //   779: invokevirtual translate : (FF)V
    //   782: aload #13
    //   784: getfield icon : Landroid/graphics/drawable/Drawable;
    //   787: astore #7
    //   789: aload #13
    //   791: getfield icon : Landroid/graphics/drawable/Drawable;
    //   794: astore #14
    //   796: aload #14
    //   798: invokevirtual getIntrinsicWidth : ()I
    //   801: istore #22
    //   803: aload #13
    //   805: getfield icon : Landroid/graphics/drawable/Drawable;
    //   808: invokevirtual getIntrinsicHeight : ()I
    //   811: istore #20
    //   813: aload #7
    //   815: iconst_0
    //   816: iconst_0
    //   817: iload #22
    //   819: iload #20
    //   821: invokevirtual setBounds : (IIII)V
    //   824: aload #13
    //   826: getfield icon : Landroid/graphics/drawable/Drawable;
    //   829: aload #4
    //   831: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   834: aload #4
    //   836: iload #21
    //   838: ineg
    //   839: i2f
    //   840: iload #19
    //   842: ineg
    //   843: i2f
    //   844: invokevirtual translate : (FF)V
    //   847: goto -> 850
    //   850: aload #4
    //   852: aload #13
    //   854: getfield x : I
    //   857: ineg
    //   858: iload #9
    //   860: isub
    //   861: i2f
    //   862: aload #13
    //   864: getfield y : I
    //   867: ineg
    //   868: iload #10
    //   870: isub
    //   871: i2f
    //   872: invokevirtual translate : (FF)V
    //   875: iinc #12, 1
    //   878: goto -> 318
    //   881: aload_0
    //   882: aconst_null
    //   883: putfield mInvalidatedKey : Landroid/inputmethodservice/Keyboard$Key;
    //   886: aload_0
    //   887: getfield mMiniKeyboardOnScreen : Z
    //   890: ifeq -> 932
    //   893: aload #5
    //   895: aload_0
    //   896: getfield mBackgroundDimAmount : F
    //   899: ldc_w 255.0
    //   902: fmul
    //   903: f2i
    //   904: bipush #24
    //   906: ishl
    //   907: invokevirtual setColor : (I)V
    //   910: aload #4
    //   912: fconst_0
    //   913: fconst_0
    //   914: aload_0
    //   915: invokevirtual getWidth : ()I
    //   918: i2f
    //   919: aload_0
    //   920: invokevirtual getHeight : ()I
    //   923: i2f
    //   924: aload #5
    //   926: invokevirtual drawRect : (FFFFLandroid/graphics/Paint;)V
    //   929: goto -> 932
    //   932: aload_0
    //   933: getfield mCanvas : Landroid/graphics/Canvas;
    //   936: invokevirtual restore : ()V
    //   939: aload_0
    //   940: iconst_0
    //   941: putfield mDrawPending : Z
    //   944: aload_0
    //   945: getfield mDirtyRect : Landroid/graphics/Rect;
    //   948: invokevirtual setEmpty : ()V
    //   951: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #660	-> 0
    //   #661	-> 14
    //   #662	-> 30
    //   #664	-> 55
    //   #665	-> 64
    //   #666	-> 73
    //   #667	-> 85
    //   #669	-> 100
    //   #670	-> 104
    //   #673	-> 109
    //   #675	-> 117
    //   #676	-> 125
    //   #677	-> 131
    //   #679	-> 141
    //   #680	-> 147
    //   #681	-> 153
    //   #682	-> 159
    //   #683	-> 165
    //   #684	-> 171
    //   #685	-> 177
    //   #686	-> 182
    //   #688	-> 188
    //   #689	-> 197
    //   #690	-> 197
    //   #692	-> 212
    //   #696	-> 296
    //   #699	-> 301
    //   #700	-> 312
    //   #701	-> 315
    //   #702	-> 324
    //   #703	-> 330
    //   #704	-> 341
    //   #706	-> 344
    //   #707	-> 351
    //   #710	-> 359
    //   #712	-> 389
    //   #713	-> 396
    //   #715	-> 428
    //   #717	-> 445
    //   #718	-> 468
    //   #720	-> 475
    //   #722	-> 480
    //   #723	-> 499
    //   #724	-> 509
    //   #726	-> 521
    //   #727	-> 531
    //   #730	-> 540
    //   #732	-> 555
    //   #736	-> 605
    //   #732	-> 628
    //   #739	-> 647
    //   #740	-> 659
    //   #741	-> 667
    //   #742	-> 695
    //   #743	-> 719
    //   #744	-> 747
    //   #745	-> 771
    //   #746	-> 782
    //   #747	-> 796
    //   #746	-> 813
    //   #748	-> 824
    //   #749	-> 834
    //   #740	-> 850
    //   #751	-> 850
    //   #701	-> 875
    //   #753	-> 881
    //   #755	-> 886
    //   #756	-> 893
    //   #757	-> 910
    //   #755	-> 932
    //   #770	-> 932
    //   #771	-> 939
    //   #772	-> 944
    //   #773	-> 951
  }
  
  private int getKeyIndices(int paramInt1, int paramInt2, int[] paramArrayOfint) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mKeys : [Landroid/inputmethodservice/Keyboard$Key;
    //   4: astore #4
    //   6: iconst_m1
    //   7: istore #5
    //   9: iconst_m1
    //   10: istore #6
    //   12: aload_0
    //   13: getfield mProximityThreshold : I
    //   16: iconst_1
    //   17: iadd
    //   18: istore #7
    //   20: aload_0
    //   21: getfield mDistances : [I
    //   24: ldc_w 2147483647
    //   27: invokestatic fill : ([II)V
    //   30: aload_0
    //   31: getfield mKeyboard : Landroid/inputmethodservice/Keyboard;
    //   34: iload_1
    //   35: iload_2
    //   36: invokevirtual getNearestKeys : (II)[I
    //   39: astore #8
    //   41: aload #8
    //   43: arraylength
    //   44: istore #9
    //   46: iconst_0
    //   47: istore #10
    //   49: iload #10
    //   51: iload #9
    //   53: if_icmpge -> 348
    //   56: aload #4
    //   58: aload #8
    //   60: iload #10
    //   62: iaload
    //   63: aaload
    //   64: astore #11
    //   66: iconst_0
    //   67: istore #12
    //   69: aload #11
    //   71: iload_1
    //   72: iload_2
    //   73: invokevirtual isInside : (II)Z
    //   76: istore #13
    //   78: iload #5
    //   80: istore #14
    //   82: iload #13
    //   84: ifeq -> 94
    //   87: aload #8
    //   89: iload #10
    //   91: iaload
    //   92: istore #14
    //   94: iload #12
    //   96: istore #5
    //   98: aload_0
    //   99: getfield mProximityCorrectOn : Z
    //   102: ifeq -> 135
    //   105: aload #11
    //   107: iload_1
    //   108: iload_2
    //   109: invokevirtual squaredDistanceFrom : (II)I
    //   112: istore #15
    //   114: iload #15
    //   116: istore #12
    //   118: iload #12
    //   120: istore #5
    //   122: iload #15
    //   124: aload_0
    //   125: getfield mProximityThreshold : I
    //   128: if_icmplt -> 140
    //   131: iload #12
    //   133: istore #5
    //   135: iload #13
    //   137: ifeq -> 330
    //   140: aload #11
    //   142: getfield codes : [I
    //   145: iconst_0
    //   146: iaload
    //   147: bipush #32
    //   149: if_icmple -> 330
    //   152: aload #11
    //   154: getfield codes : [I
    //   157: arraylength
    //   158: istore #16
    //   160: iload #6
    //   162: istore #12
    //   164: iload #7
    //   166: istore #6
    //   168: iload #5
    //   170: iload #7
    //   172: if_icmpge -> 186
    //   175: iload #5
    //   177: istore #6
    //   179: aload #8
    //   181: iload #10
    //   183: iaload
    //   184: istore #12
    //   186: aload_3
    //   187: ifnonnull -> 197
    //   190: iload #6
    //   192: istore #7
    //   194: goto -> 334
    //   197: iconst_0
    //   198: istore #7
    //   200: aload_0
    //   201: getfield mDistances : [I
    //   204: astore #17
    //   206: iload #7
    //   208: aload #17
    //   210: arraylength
    //   211: if_icmpge -> 323
    //   214: aload #17
    //   216: iload #7
    //   218: iaload
    //   219: iload #5
    //   221: if_icmple -> 317
    //   224: aload #17
    //   226: iload #7
    //   228: aload #17
    //   230: iload #7
    //   232: iload #16
    //   234: iadd
    //   235: aload #17
    //   237: arraylength
    //   238: iload #7
    //   240: isub
    //   241: iload #16
    //   243: isub
    //   244: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   247: aload_3
    //   248: iload #7
    //   250: aload_3
    //   251: iload #7
    //   253: iload #16
    //   255: iadd
    //   256: aload_3
    //   257: arraylength
    //   258: iload #7
    //   260: isub
    //   261: iload #16
    //   263: isub
    //   264: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   267: iconst_0
    //   268: istore #15
    //   270: iload #15
    //   272: iload #16
    //   274: if_icmpge -> 310
    //   277: aload_3
    //   278: iload #7
    //   280: iload #15
    //   282: iadd
    //   283: aload #11
    //   285: getfield codes : [I
    //   288: iload #15
    //   290: iaload
    //   291: iastore
    //   292: aload_0
    //   293: getfield mDistances : [I
    //   296: iload #7
    //   298: iload #15
    //   300: iadd
    //   301: iload #5
    //   303: iastore
    //   304: iinc #15, 1
    //   307: goto -> 270
    //   310: iload #6
    //   312: istore #7
    //   314: goto -> 334
    //   317: iinc #7, 1
    //   320: goto -> 200
    //   323: iload #6
    //   325: istore #7
    //   327: goto -> 334
    //   330: iload #6
    //   332: istore #12
    //   334: iinc #10, 1
    //   337: iload #14
    //   339: istore #5
    //   341: iload #12
    //   343: istore #6
    //   345: goto -> 49
    //   348: iload #5
    //   350: istore_1
    //   351: iload #5
    //   353: iconst_m1
    //   354: if_icmpne -> 360
    //   357: iload #6
    //   359: istore_1
    //   360: iload_1
    //   361: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #776	-> 0
    //   #777	-> 6
    //   #778	-> 9
    //   #779	-> 12
    //   #780	-> 20
    //   #781	-> 30
    //   #782	-> 41
    //   #783	-> 46
    //   #784	-> 56
    //   #785	-> 66
    //   #786	-> 69
    //   #787	-> 78
    //   #788	-> 87
    //   #791	-> 94
    //   #792	-> 105
    //   #796	-> 152
    //   #797	-> 160
    //   #798	-> 175
    //   #799	-> 179
    //   #802	-> 186
    //   #804	-> 197
    //   #805	-> 214
    //   #807	-> 224
    //   #809	-> 247
    //   #811	-> 267
    //   #812	-> 277
    //   #813	-> 292
    //   #811	-> 304
    //   #815	-> 310
    //   #804	-> 317
    //   #792	-> 330
    //   #783	-> 334
    //   #820	-> 348
    //   #821	-> 357
    //   #823	-> 360
  }
  
  private void detectAndSendKey(int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    if (paramInt1 != -1) {
      Keyboard.Key[] arrayOfKey = this.mKeys;
      if (paramInt1 < arrayOfKey.length) {
        Keyboard.Key key = arrayOfKey[paramInt1];
        if (key.text != null) {
          this.mKeyboardActionListener.onText(key.text);
          this.mKeyboardActionListener.onRelease(-1);
        } else {
          int i = key.codes[0];
          int[] arrayOfInt = new int[MAX_NEARBY_KEYS];
          Arrays.fill(arrayOfInt, -1);
          getKeyIndices(paramInt2, paramInt3, arrayOfInt);
          paramInt2 = i;
          if (this.mInMultiTap) {
            if (this.mTapCount != -1) {
              this.mKeyboardActionListener.onKey(-5, KEY_DELETE);
            } else {
              this.mTapCount = 0;
            } 
            paramInt2 = key.codes[this.mTapCount];
          } 
          this.mKeyboardActionListener.onKey(paramInt2, arrayOfInt);
          this.mKeyboardActionListener.onRelease(paramInt2);
        } 
        this.mLastSentIndex = paramInt1;
        this.mLastTapTime = paramLong;
      } 
    } 
  }
  
  private CharSequence getPreviewText(Keyboard.Key paramKey) {
    int[] arrayOfInt;
    if (this.mInMultiTap) {
      StringBuilder stringBuilder = this.mPreviewLabel;
      int i = 0;
      stringBuilder.setLength(0);
      stringBuilder = this.mPreviewLabel;
      arrayOfInt = paramKey.codes;
      int j = this.mTapCount;
      if (j >= 0)
        i = j; 
      stringBuilder.append((char)arrayOfInt[i]);
      return adjustCase(this.mPreviewLabel);
    } 
    return adjustCase(((Keyboard.Key)arrayOfInt).label);
  }
  
  private void showPreview(int paramInt) {
    int i = this.mCurrentKeyIndex;
    PopupWindow popupWindow = this.mPreviewPopup;
    this.mCurrentKeyIndex = paramInt;
    Keyboard.Key[] arrayOfKey = this.mKeys;
    if (i != paramInt) {
      if (i != -1 && arrayOfKey.length > i) {
        boolean bool;
        Keyboard.Key key = arrayOfKey[i];
        if (paramInt == -1) {
          bool = true;
        } else {
          bool = false;
        } 
        key.onReleased(bool);
        invalidateKey(i);
        int k = key.codes[0];
        sendAccessibilityEventForUnicodeCharacter(256, k);
        sendAccessibilityEventForUnicodeCharacter(65536, k);
      } 
      int j = this.mCurrentKeyIndex;
      if (j != -1 && arrayOfKey.length > j) {
        Keyboard.Key key = arrayOfKey[j];
        key.onPressed();
        invalidateKey(this.mCurrentKeyIndex);
        j = key.codes[0];
        sendAccessibilityEventForUnicodeCharacter(128, j);
        sendAccessibilityEventForUnicodeCharacter(32768, j);
      } 
    } 
    if (i != this.mCurrentKeyIndex && this.mShowPreview) {
      this.mHandler.removeMessages(1);
      if (popupWindow.isShowing() && 
        paramInt == -1) {
        Handler handler = this.mHandler;
        Message message = handler.obtainMessage(2);
        handler.sendMessageDelayed(message, 70L);
      } 
      if (paramInt != -1)
        if (popupWindow.isShowing() && this.mPreviewText.getVisibility() == 0) {
          showKey(paramInt);
        } else {
          Handler handler = this.mHandler;
          Message message = handler.obtainMessage(1, paramInt, 0);
          handler.sendMessageDelayed(message, 0L);
        }  
    } 
  }
  
  private void showKey(int paramInt) {
    PopupWindow popupWindow = this.mPreviewPopup;
    Keyboard.Key[] arrayOfKey = this.mKeys;
    if (paramInt < 0 || paramInt >= this.mKeys.length)
      return; 
    Keyboard.Key key = arrayOfKey[paramInt];
    if (key.icon != null) {
      Drawable drawable1;
      TextView textView1 = this.mPreviewText;
      if (key.iconPreview != null) {
        drawable1 = key.iconPreview;
      } else {
        drawable1 = key.icon;
      } 
      textView1.setCompoundDrawables(null, null, null, drawable1);
      this.mPreviewText.setText(null);
    } else {
      this.mPreviewText.setCompoundDrawables(null, null, null, null);
      this.mPreviewText.setText(getPreviewText(key));
      if (key.label.length() > 1 && key.codes.length < 2) {
        this.mPreviewText.setTextSize(0, this.mKeyTextSize);
        this.mPreviewText.setTypeface(Typeface.DEFAULT_BOLD);
      } else {
        this.mPreviewText.setTextSize(0, this.mPreviewTextSizeLarge);
        this.mPreviewText.setTypeface(Typeface.DEFAULT);
      } 
    } 
    TextView textView = this.mPreviewText;
    int i = View.MeasureSpec.makeMeasureSpec(0, 0);
    paramInt = View.MeasureSpec.makeMeasureSpec(0, 0);
    textView.measure(i, paramInt);
    int j = this.mPreviewText.getMeasuredWidth();
    i = key.width;
    textView = this.mPreviewText;
    int k = textView.getPaddingLeft();
    paramInt = this.mPreviewText.getPaddingRight();
    j = Math.max(j, i + k + paramInt);
    i = this.mPreviewHeight;
    ViewGroup.LayoutParams layoutParams = this.mPreviewText.getLayoutParams();
    if (layoutParams != null) {
      layoutParams.width = j;
      layoutParams.height = i;
    } 
    if (!this.mPreviewCentered) {
      this.mPopupPreviewX = key.x - this.mPreviewText.getPaddingLeft() + this.mPaddingLeft;
      this.mPopupPreviewY = key.y - i + this.mPreviewOffset;
    } else {
      this.mPopupPreviewX = 160 - this.mPreviewText.getMeasuredWidth() / 2;
      this.mPopupPreviewY = -this.mPreviewText.getMeasuredHeight();
    } 
    this.mHandler.removeMessages(2);
    getLocationInWindow(this.mCoordinates);
    int[] arrayOfInt = this.mCoordinates;
    arrayOfInt[0] = arrayOfInt[0] + this.mMiniKeyboardOffsetX;
    arrayOfInt[1] = arrayOfInt[1] + this.mMiniKeyboardOffsetY;
    Drawable drawable = this.mPreviewText.getBackground();
    if (key.popupResId != 0) {
      arrayOfInt = LONG_PRESSABLE_STATE_SET;
    } else {
      arrayOfInt = EMPTY_STATE_SET;
    } 
    drawable.setState(arrayOfInt);
    paramInt = this.mPopupPreviewX;
    arrayOfInt = this.mCoordinates;
    this.mPopupPreviewX = paramInt + arrayOfInt[0];
    this.mPopupPreviewY += arrayOfInt[1];
    getLocationOnScreen(arrayOfInt);
    if (this.mPopupPreviewY + this.mCoordinates[1] < 0) {
      if (key.x + key.width <= getWidth() / 2) {
        this.mPopupPreviewX += (int)(key.width * 2.5D);
      } else {
        this.mPopupPreviewX -= (int)(key.width * 2.5D);
      } 
      this.mPopupPreviewY += i;
    } 
    if (popupWindow.isShowing()) {
      popupWindow.update(this.mPopupPreviewX, this.mPopupPreviewY, j, i);
    } else {
      popupWindow.setWidth(j);
      popupWindow.setHeight(i);
      popupWindow.showAtLocation(this.mPopupParent, 0, this.mPopupPreviewX, this.mPopupPreviewY);
    } 
    this.mPreviewText.setVisibility(0);
  }
  
  private void sendAccessibilityEventForUnicodeCharacter(int paramInt1, int paramInt2) {
    if (this.mAccessibilityManager.isEnabled()) {
      String str;
      AccessibilityEvent accessibilityEvent = AccessibilityEvent.obtain(paramInt1);
      onInitializeAccessibilityEvent(accessibilityEvent);
      if (paramInt2 != 10) {
        switch (paramInt2) {
          default:
            str = String.valueOf((char)paramInt2);
            break;
          case -1:
            str = this.mContext.getString(17040377);
            break;
          case -2:
            str = this.mContext.getString(17040376);
            break;
          case -3:
            str = this.mContext.getString(17040372);
            break;
          case -4:
            str = this.mContext.getString(17040374);
            break;
          case -5:
            str = this.mContext.getString(17040373);
            break;
          case -6:
            str = this.mContext.getString(17040371);
            break;
        } 
      } else {
        str = this.mContext.getString(17040375);
      } 
      accessibilityEvent.getText().add(str);
      this.mAccessibilityManager.sendAccessibilityEvent(accessibilityEvent);
    } 
  }
  
  public void invalidateAllKeys() {
    this.mDirtyRect.union(0, 0, getWidth(), getHeight());
    this.mDrawPending = true;
    invalidate();
  }
  
  public void invalidateKey(int paramInt) {
    Keyboard.Key[] arrayOfKey = this.mKeys;
    if (arrayOfKey == null)
      return; 
    if (paramInt < 0 || paramInt >= arrayOfKey.length)
      return; 
    Keyboard.Key key = arrayOfKey[paramInt];
    this.mInvalidatedKey = key;
    this.mDirtyRect.union(key.x + this.mPaddingLeft, key.y + this.mPaddingTop, key.x + key.width + this.mPaddingLeft, key.y + key.height + this.mPaddingTop);
    onBufferDraw();
    invalidate(key.x + this.mPaddingLeft, key.y + this.mPaddingTop, key.x + key.width + this.mPaddingLeft, key.y + key.height + this.mPaddingTop);
  }
  
  private boolean openPopupIfRequired(MotionEvent paramMotionEvent) {
    if (this.mPopupLayout == 0)
      return false; 
    int i = this.mCurrentKey;
    if (i >= 0) {
      Keyboard.Key[] arrayOfKey = this.mKeys;
      if (i < arrayOfKey.length) {
        Keyboard.Key key = arrayOfKey[i];
        boolean bool = onLongPress(key);
        if (bool) {
          this.mAbortKey = true;
          showPreview(-1);
        } 
        return bool;
      } 
    } 
    return false;
  }
  
  protected boolean onLongPress(Keyboard.Key paramKey) {
    int i = paramKey.popupResId;
    if (i != 0) {
      View view = this.mMiniKeyboardCache.get(paramKey);
      if (view == null) {
        Keyboard keyboard;
        LayoutInflater layoutInflater = (LayoutInflater)getContext().getSystemService("layout_inflater");
        View view1 = layoutInflater.inflate(this.mPopupLayout, null);
        this.mMiniKeyboard = (KeyboardView)view1.findViewById(16908326);
        view1 = this.mMiniKeyboardContainer.findViewById(16908327);
        if (view1 != null)
          view1.setOnClickListener(this); 
        this.mMiniKeyboard.setOnKeyboardActionListener((OnKeyboardActionListener)new Object(this));
        if (paramKey.popupCharacters != null) {
          Context context = getContext();
          CharSequence charSequence = paramKey.popupCharacters;
          keyboard = new Keyboard(context, i, charSequence, -1, getPaddingLeft() + getPaddingRight());
        } else {
          keyboard = new Keyboard(getContext(), i);
        } 
        this.mMiniKeyboard.setKeyboard(keyboard);
        this.mMiniKeyboard.setPopupParent(this);
        view = this.mMiniKeyboardContainer;
        int m = View.MeasureSpec.makeMeasureSpec(getWidth(), -2147483648);
        i = View.MeasureSpec.makeMeasureSpec(getHeight(), -2147483648);
        view.measure(m, i);
        this.mMiniKeyboardCache.put(paramKey, this.mMiniKeyboardContainer);
      } else {
        this.mMiniKeyboard = (KeyboardView)view.findViewById(16908326);
      } 
      getLocationInWindow(this.mCoordinates);
      this.mPopupX = paramKey.x + this.mPaddingLeft;
      this.mPopupY = paramKey.y + this.mPaddingTop;
      this.mPopupX = this.mPopupX + paramKey.width - this.mMiniKeyboardContainer.getMeasuredWidth();
      this.mPopupY -= this.mMiniKeyboardContainer.getMeasuredHeight();
      int j = this.mPopupX + this.mMiniKeyboardContainer.getPaddingRight() + this.mCoordinates[0];
      int k = this.mPopupY + this.mMiniKeyboardContainer.getPaddingBottom() + this.mCoordinates[1];
      KeyboardView keyboardView = this.mMiniKeyboard;
      if (j < 0) {
        i = 0;
      } else {
        i = j;
      } 
      keyboardView.setPopupOffset(i, k);
      this.mMiniKeyboard.setShifted(isShifted());
      this.mPopupKeyboard.setContentView(this.mMiniKeyboardContainer);
      this.mPopupKeyboard.setWidth(this.mMiniKeyboardContainer.getMeasuredWidth());
      this.mPopupKeyboard.setHeight(this.mMiniKeyboardContainer.getMeasuredHeight());
      this.mPopupKeyboard.showAtLocation(this, 0, j, k);
      this.mMiniKeyboardOnScreen = true;
      invalidateAllKeys();
      return true;
    } 
    return false;
  }
  
  public boolean onHoverEvent(MotionEvent paramMotionEvent) {
    if (this.mAccessibilityManager.isTouchExplorationEnabled() && paramMotionEvent.getPointerCount() == 1) {
      int i = paramMotionEvent.getAction();
      if (i != 7) {
        if (i != 9) {
          if (i == 10)
            paramMotionEvent.setAction(1); 
        } else {
          paramMotionEvent.setAction(0);
        } 
      } else {
        paramMotionEvent.setAction(2);
      } 
      return onTouchEvent(paramMotionEvent);
    } 
    return true;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool;
    int i = paramMotionEvent.getPointerCount();
    int j = paramMotionEvent.getAction();
    long l = paramMotionEvent.getEventTime();
    if (i != this.mOldPointerCount) {
      if (i == 1) {
        float f1 = paramMotionEvent.getX(), f2 = paramMotionEvent.getY();
        int k = paramMotionEvent.getMetaState();
        MotionEvent motionEvent = MotionEvent.obtain(l, l, 0, f1, f2, k);
        bool = onModifiedTouchEvent(motionEvent, false);
        motionEvent.recycle();
        if (j == 1)
          bool = onModifiedTouchEvent(paramMotionEvent, true); 
      } else {
        float f1 = this.mOldPointerX, f2 = this.mOldPointerY;
        int k = paramMotionEvent.getMetaState();
        paramMotionEvent = MotionEvent.obtain(l, l, 1, f1, f2, k);
        bool = onModifiedTouchEvent(paramMotionEvent, true);
        paramMotionEvent.recycle();
      } 
    } else if (i == 1) {
      bool = onModifiedTouchEvent(paramMotionEvent, false);
      this.mOldPointerX = paramMotionEvent.getX();
      this.mOldPointerY = paramMotionEvent.getY();
    } else {
      bool = true;
    } 
    this.mOldPointerCount = i;
    return bool;
  }
  
  private boolean onModifiedTouchEvent(MotionEvent paramMotionEvent, boolean paramBoolean) {
    Message message;
    int i = (int)paramMotionEvent.getX() - this.mPaddingLeft;
    int j = (int)paramMotionEvent.getY() - this.mPaddingTop;
    int k = this.mVerticalCorrection, m = j;
    if (j >= -k)
      m = j + k; 
    j = paramMotionEvent.getAction();
    long l = paramMotionEvent.getEventTime();
    k = getKeyIndices(i, m, (int[])null);
    this.mPossiblePoly = paramBoolean;
    if (j == 0)
      this.mSwipeTracker.clear(); 
    this.mSwipeTracker.addMovement(paramMotionEvent);
    if (this.mAbortKey && j != 0 && j != 3)
      return true; 
    if (this.mGestureDetector.onTouchEvent(paramMotionEvent)) {
      showPreview(-1);
      this.mHandler.removeMessages(3);
      this.mHandler.removeMessages(4);
      return true;
    } 
    if (this.mMiniKeyboardOnScreen && j != 3)
      return true; 
    if (j != 0) {
      if (j != 1) {
        if (j != 2) {
          if (j == 3) {
            removeMessages();
            dismissPopupKeyboard();
            this.mAbortKey = true;
            showPreview(-1);
            invalidateKey(this.mCurrentKey);
          } 
        } else {
          if (k != -1) {
            j = this.mCurrentKey;
            if (j == -1) {
              this.mCurrentKey = k;
              this.mCurrentKeyTime = l - this.mDownTime;
            } else {
              if (k == j) {
                this.mCurrentKeyTime += l - this.mLastMoveTime;
                j = 1;
              } else {
                if (this.mRepeatKeyIndex == -1) {
                  resetMultiTap();
                  this.mLastKey = this.mCurrentKey;
                  this.mLastCodeX = this.mLastX;
                  this.mLastCodeY = this.mLastY;
                  this.mLastKeyTime = this.mCurrentKeyTime + l - this.mLastMoveTime;
                  this.mCurrentKey = k;
                  this.mCurrentKeyTime = 0L;
                } 
                j = 0;
              } 
              if (j == 0) {
                this.mHandler.removeMessages(4);
                if (k != -1) {
                  message = this.mHandler.obtainMessage(4, paramMotionEvent);
                  this.mHandler.sendMessageDelayed(message, LONGPRESS_TIMEOUT);
                } 
              } 
              showPreview(this.mCurrentKey);
              this.mLastMoveTime = l;
              this.mLastX = i;
              this.mLastY = m;
              return true;
            } 
          } 
          j = 0;
        } 
      } else {
        removeMessages();
        if (k == this.mCurrentKey) {
          this.mCurrentKeyTime += l - this.mLastMoveTime;
        } else {
          resetMultiTap();
          this.mLastKey = this.mCurrentKey;
          this.mLastKeyTime = this.mCurrentKeyTime + l - this.mLastMoveTime;
          this.mCurrentKey = k;
          this.mCurrentKeyTime = 0L;
        } 
        long l1 = this.mCurrentKeyTime;
        if (l1 < this.mLastKeyTime && l1 < 70L) {
          j = this.mLastKey;
          if (j != -1) {
            this.mCurrentKey = j;
            i = this.mLastCodeX;
            m = this.mLastCodeY;
          } 
        } 
        showPreview(-1);
        Arrays.fill(this.mKeyIndices, -1);
        if (this.mRepeatKeyIndex == -1 && !this.mMiniKeyboardOnScreen && !this.mAbortKey)
          detectAndSendKey(this.mCurrentKey, i, m, l); 
        invalidateKey(k);
        this.mRepeatKeyIndex = -1;
      } 
    } else {
      j = 0;
      this.mAbortKey = false;
      this.mStartX = i;
      this.mStartY = m;
      this.mLastCodeX = i;
      this.mLastCodeY = m;
      this.mLastKeyTime = 0L;
      this.mCurrentKeyTime = 0L;
      this.mLastKey = -1;
      this.mCurrentKey = k;
      this.mDownKey = k;
      long l1 = message.getEventTime();
      this.mLastMoveTime = l1;
      checkMultiTap(l, k);
      OnKeyboardActionListener onKeyboardActionListener = this.mKeyboardActionListener;
      if (k != -1)
        j = (this.mKeys[k]).codes[0]; 
      onKeyboardActionListener.onPress(j);
      j = this.mCurrentKey;
      if (j >= 0 && (this.mKeys[j]).repeatable) {
        this.mRepeatKeyIndex = this.mCurrentKey;
        Message message1 = this.mHandler.obtainMessage(3);
        this.mHandler.sendMessageDelayed(message1, 400L);
        repeatKey();
        if (this.mAbortKey) {
          this.mRepeatKeyIndex = -1;
          this.mLastX = i;
          this.mLastY = m;
          return true;
        } 
      } 
      if (this.mCurrentKey != -1) {
        message = this.mHandler.obtainMessage(4, message);
        this.mHandler.sendMessageDelayed(message, LONGPRESS_TIMEOUT);
      } 
      showPreview(k);
    } 
    this.mLastX = i;
    this.mLastY = m;
    return true;
  }
  
  private boolean repeatKey() {
    Keyboard.Key key = this.mKeys[this.mRepeatKeyIndex];
    detectAndSendKey(this.mCurrentKey, key.x, key.y, this.mLastTapTime);
    return true;
  }
  
  protected void swipeRight() {
    this.mKeyboardActionListener.swipeRight();
  }
  
  protected void swipeLeft() {
    this.mKeyboardActionListener.swipeLeft();
  }
  
  protected void swipeUp() {
    this.mKeyboardActionListener.swipeUp();
  }
  
  protected void swipeDown() {
    this.mKeyboardActionListener.swipeDown();
  }
  
  public void closing() {
    if (this.mPreviewPopup.isShowing())
      this.mPreviewPopup.dismiss(); 
    removeMessages();
    dismissPopupKeyboard();
    this.mBuffer = null;
    this.mCanvas = null;
    this.mMiniKeyboardCache.clear();
  }
  
  private void removeMessages() {
    Handler handler = this.mHandler;
    if (handler != null) {
      handler.removeMessages(3);
      this.mHandler.removeMessages(4);
      this.mHandler.removeMessages(1);
    } 
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    closing();
  }
  
  private void dismissPopupKeyboard() {
    if (this.mPopupKeyboard.isShowing()) {
      this.mPopupKeyboard.dismiss();
      this.mMiniKeyboardOnScreen = false;
      invalidateAllKeys();
    } 
  }
  
  public boolean handleBack() {
    if (this.mPopupKeyboard.isShowing()) {
      dismissPopupKeyboard();
      return true;
    } 
    return false;
  }
  
  private void resetMultiTap() {
    this.mLastSentIndex = -1;
    this.mTapCount = 0;
    this.mLastTapTime = -1L;
    this.mInMultiTap = false;
  }
  
  private void checkMultiTap(long paramLong, int paramInt) {
    if (paramInt == -1)
      return; 
    Keyboard.Key key = this.mKeys[paramInt];
    if (key.codes.length > 1) {
      this.mInMultiTap = true;
      if (paramLong < this.mLastTapTime + 800L && paramInt == this.mLastSentIndex) {
        this.mTapCount = (this.mTapCount + 1) % key.codes.length;
        return;
      } 
      this.mTapCount = -1;
      return;
    } 
    if (paramLong > this.mLastTapTime + 800L || paramInt != this.mLastSentIndex)
      resetMultiTap(); 
  }
  
  class OnKeyboardActionListener {
    public abstract void onKey(int param1Int, int[] param1ArrayOfint);
    
    public abstract void onPress(int param1Int);
    
    public abstract void onRelease(int param1Int);
    
    public abstract void onText(CharSequence param1CharSequence);
    
    public abstract void swipeDown();
    
    public abstract void swipeLeft();
    
    public abstract void swipeRight();
    
    public abstract void swipeUp();
  }
  
  class SwipeTracker {
    final float[] mPastX = new float[4];
    
    final float[] mPastY = new float[4];
    
    final long[] mPastTime = new long[4];
    
    static final int LONGEST_PAST_TIME = 200;
    
    static final int NUM_PAST = 4;
    
    float mXVelocity;
    
    float mYVelocity;
    
    public void clear() {
      this.mPastTime[0] = 0L;
    }
    
    public void addMovement(MotionEvent param1MotionEvent) {
      long l = param1MotionEvent.getEventTime();
      int i = param1MotionEvent.getHistorySize();
      for (byte b = 0; b < i; b++) {
        float f1 = param1MotionEvent.getHistoricalX(b), f2 = param1MotionEvent.getHistoricalY(b);
        long l1 = param1MotionEvent.getHistoricalEventTime(b);
        addPoint(f1, f2, l1);
      } 
      addPoint(param1MotionEvent.getX(), param1MotionEvent.getY(), l);
    }
    
    private void addPoint(float param1Float1, float param1Float2, long param1Long) {
      int i = -1;
      long[] arrayOfLong = this.mPastTime;
      int j;
      for (j = 0; j < 4 && 
        arrayOfLong[j] != 0L; j++) {
        if (arrayOfLong[j] < param1Long - 200L)
          i = j; 
      } 
      int k = i;
      if (j == 4) {
        k = i;
        if (i < 0)
          k = 0; 
      } 
      i = k;
      if (k == j)
        i = k - 1; 
      float[] arrayOfFloat1 = this.mPastX;
      float[] arrayOfFloat2 = this.mPastY;
      k = j;
      if (i >= 0) {
        int m = i + 1;
        k = 4 - i - 1;
        System.arraycopy(arrayOfFloat1, m, arrayOfFloat1, 0, k);
        System.arraycopy(arrayOfFloat2, m, arrayOfFloat2, 0, k);
        System.arraycopy(arrayOfLong, m, arrayOfLong, 0, k);
        k = j - i + 1;
      } 
      arrayOfFloat1[k] = param1Float1;
      arrayOfFloat2[k] = param1Float2;
      arrayOfLong[k] = param1Long;
      j = k + 1;
      if (j < 4)
        arrayOfLong[j] = 0L; 
    }
    
    public void computeCurrentVelocity(int param1Int) {
      computeCurrentVelocity(param1Int, Float.MAX_VALUE);
    }
    
    public void computeCurrentVelocity(int param1Int, float param1Float) {
      float[] arrayOfFloat1 = this.mPastX;
      float[] arrayOfFloat2 = this.mPastY;
      long[] arrayOfLong = this.mPastTime;
      float f1 = arrayOfFloat1[0];
      float f2 = arrayOfFloat2[0];
      long l = arrayOfLong[0];
      float f3 = 0.0F;
      float f4 = 0.0F;
      byte b1 = 0;
      while (b1 < 4 && 
        arrayOfLong[b1] != 0L)
        b1++; 
      for (byte b2 = 1; b2 < b1; b2++) {
        int i = (int)(arrayOfLong[b2] - l);
        if (i != 0) {
          float f = arrayOfFloat1[b2];
          f = (f - f1) / i * param1Int;
          if (f3 == 0.0F) {
            f3 = f;
          } else {
            f3 = (f3 + f) * 0.5F;
          } 
          f = arrayOfFloat2[b2];
          f = (f - f2) / i * param1Int;
          if (f4 == 0.0F) {
            f4 = f;
          } else {
            f4 = (f4 + f) * 0.5F;
          } 
        } 
      } 
      if (f3 < 0.0F) {
        f3 = Math.max(f3, -param1Float);
      } else {
        f3 = Math.min(f3, param1Float);
      } 
      this.mXVelocity = f3;
      if (f4 < 0.0F) {
        param1Float = Math.max(f4, -param1Float);
      } else {
        param1Float = Math.min(f4, param1Float);
      } 
      this.mYVelocity = param1Float;
    }
    
    public float getXVelocity() {
      return this.mXVelocity;
    }
    
    public float getYVelocity() {
      return this.mYVelocity;
    }
    
    private SwipeTracker() {}
  }
}
