package android.app;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.ColorSpace;
import android.graphics.ImageDecoder;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.DeadSystemException;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Trace;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.Display;
import android.view.WindowManagerGlobal;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import libcore.io.IoUtils;

public class WallpaperManager implements IOplusWallpaperManagerEx {
  public static final String ACTION_CHANGE_LIVE_WALLPAPER = "android.service.wallpaper.CHANGE_LIVE_WALLPAPER";
  
  public static final String ACTION_CROP_AND_SET_WALLPAPER = "android.service.wallpaper.CROP_AND_SET_WALLPAPER";
  
  public static final String ACTION_LIVE_WALLPAPER_CHOOSER = "android.service.wallpaper.LIVE_WALLPAPER_CHOOSER";
  
  public static final String COMMAND_DROP = "android.home.drop";
  
  public static final String COMMAND_REAPPLY = "android.wallpaper.reapply";
  
  public static final String COMMAND_SECONDARY_TAP = "android.wallpaper.secondaryTap";
  
  public static final String COMMAND_TAP = "android.wallpaper.tap";
  
  private static boolean DEBUG = false;
  
  public static final String EXTRA_LIVE_WALLPAPER_COMPONENT = "android.service.wallpaper.extra.LIVE_WALLPAPER_COMPONENT";
  
  public static final String EXTRA_NEW_WALLPAPER_ID = "android.service.wallpaper.extra.ID";
  
  public static final int FLAG_LOCK = 2;
  
  public static final int FLAG_SYSTEM = 1;
  
  private static final String PROP_LOCK_WALLPAPER = "ro.config.lock_wallpaper";
  
  private static final String PROP_WALLPAPER = "ro.config.wallpaper";
  
  private static final String PROP_WALLPAPER_COMPONENT = "ro.config.wallpaper_component";
  
  private static String TAG = "WallpaperManager";
  
  public static final String WALLPAPER_PREVIEW_META_DATA = "android.wallpaper.preview";
  
  private static Globals sGlobals;
  
  private static final Object sSync;
  
  private final ColorManagementProxy mCmProxy;
  
  private final Context mContext;
  
  private float mWallpaperXStep;
  
  private float mWallpaperYStep;
  
  private final boolean mWcgEnabled;
  
  static {
    DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    sSync = new Object[0];
  }
  
  class FastBitmapDrawable extends Drawable {
    private final Bitmap mBitmap;
    
    private int mDrawLeft;
    
    private int mDrawTop;
    
    private final int mHeight;
    
    private final Paint mPaint;
    
    private final int mWidth;
    
    private FastBitmapDrawable(WallpaperManager this$0) {
      this.mBitmap = (Bitmap)this$0;
      this.mWidth = this$0.getWidth();
      int i = this$0.getHeight();
      setBounds(0, 0, this.mWidth, i);
      Paint paint = new Paint();
      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC));
    }
    
    public void draw(Canvas param1Canvas) {
      param1Canvas.drawBitmap(this.mBitmap, this.mDrawLeft, this.mDrawTop, this.mPaint);
    }
    
    public int getOpacity() {
      return -1;
    }
    
    public void setBounds(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.mDrawLeft = (param1Int3 - param1Int1 - this.mWidth) / 2 + param1Int1;
      this.mDrawTop = (param1Int4 - param1Int2 - this.mHeight) / 2 + param1Int2;
    }
    
    public void setAlpha(int param1Int) {
      throw new UnsupportedOperationException("Not supported with this drawable");
    }
    
    public void setColorFilter(ColorFilter param1ColorFilter) {
      throw new UnsupportedOperationException("Not supported with this drawable");
    }
    
    public void setDither(boolean param1Boolean) {
      throw new UnsupportedOperationException("Not supported with this drawable");
    }
    
    public void setFilterBitmap(boolean param1Boolean) {
      throw new UnsupportedOperationException("Not supported with this drawable");
    }
    
    public int getIntrinsicWidth() {
      return this.mWidth;
    }
    
    public int getIntrinsicHeight() {
      return this.mHeight;
    }
    
    public int getMinimumWidth() {
      return this.mWidth;
    }
    
    public int getMinimumHeight() {
      return this.mHeight;
    }
  }
  
  class Globals extends IWallpaperManagerCallback.Stub {
    private Bitmap mCachedWallpaper;
    
    private int mCachedWallpaperUserId;
    
    private boolean mColorCallbackRegistered;
    
    private final ArrayList<Pair<WallpaperManager.OnColorsChangedListener, Handler>> mColorListeners = new ArrayList<>();
    
    private Bitmap mDefaultWallpaper;
    
    private Handler mMainLooperHandler;
    
    private final IWallpaperManager mService;
    
    Globals(WallpaperManager this$0, Looper param1Looper) {
      this.mService = (IWallpaperManager)this$0;
      this.mMainLooperHandler = new Handler(param1Looper);
      forgetLoadedWallpaper();
    }
    
    public void onWallpaperChanged() {
      forgetLoadedWallpaper();
    }
    
    public void addOnColorsChangedListener(WallpaperManager.OnColorsChangedListener param1OnColorsChangedListener, Handler param1Handler, int param1Int1, int param1Int2) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mColorCallbackRegistered : Z
      //   6: istore #5
      //   8: iload #5
      //   10: ifne -> 47
      //   13: aload_0
      //   14: getfield mService : Landroid/app/IWallpaperManager;
      //   17: aload_0
      //   18: iload_3
      //   19: iload #4
      //   21: invokeinterface registerWallpaperColorsCallback : (Landroid/app/IWallpaperManagerCallback;II)V
      //   26: aload_0
      //   27: iconst_1
      //   28: putfield mColorCallbackRegistered : Z
      //   31: goto -> 47
      //   34: astore #6
      //   36: invokestatic access$000 : ()Ljava/lang/String;
      //   39: ldc 'Can't register for color updates'
      //   41: aload #6
      //   43: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   46: pop
      //   47: aload_0
      //   48: getfield mColorListeners : Ljava/util/ArrayList;
      //   51: astore #7
      //   53: new android/util/Pair
      //   56: astore #6
      //   58: aload #6
      //   60: aload_1
      //   61: aload_2
      //   62: invokespecial <init> : (Ljava/lang/Object;Ljava/lang/Object;)V
      //   65: aload #7
      //   67: aload #6
      //   69: invokevirtual add : (Ljava/lang/Object;)Z
      //   72: pop
      //   73: aload_0
      //   74: monitorexit
      //   75: return
      //   76: astore_1
      //   77: aload_0
      //   78: monitorexit
      //   79: aload_1
      //   80: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #346	-> 0
      //   #347	-> 2
      //   #349	-> 13
      //   #350	-> 26
      //   #354	-> 31
      //   #351	-> 34
      //   #353	-> 36
      //   #356	-> 47
      //   #357	-> 73
      //   #358	-> 75
      //   #357	-> 76
      // Exception table:
      //   from	to	target	type
      //   2	8	76	finally
      //   13	26	34	android/os/RemoteException
      //   13	26	76	finally
      //   26	31	34	android/os/RemoteException
      //   26	31	76	finally
      //   36	47	76	finally
      //   47	73	76	finally
      //   73	75	76	finally
      //   77	79	76	finally
    }
    
    public void removeOnColorsChangedListener(WallpaperManager.OnColorsChangedListener param1OnColorsChangedListener, int param1Int1, int param1Int2) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mColorListeners : Ljava/util/ArrayList;
      //   6: astore #4
      //   8: new android/app/_$$Lambda$WallpaperManager$Globals$2yG7V1sbMECCnlFTLyjKWKqNoYI
      //   11: astore #5
      //   13: aload #5
      //   15: aload_1
      //   16: invokespecial <init> : (Landroid/app/WallpaperManager$OnColorsChangedListener;)V
      //   19: aload #4
      //   21: aload #5
      //   23: invokevirtual removeIf : (Ljava/util/function/Predicate;)Z
      //   26: pop
      //   27: aload_0
      //   28: getfield mColorListeners : Ljava/util/ArrayList;
      //   31: invokevirtual size : ()I
      //   34: ifne -> 76
      //   37: aload_0
      //   38: getfield mColorCallbackRegistered : Z
      //   41: ifeq -> 76
      //   44: aload_0
      //   45: iconst_0
      //   46: putfield mColorCallbackRegistered : Z
      //   49: aload_0
      //   50: getfield mService : Landroid/app/IWallpaperManager;
      //   53: aload_0
      //   54: iload_2
      //   55: iload_3
      //   56: invokeinterface unregisterWallpaperColorsCallback : (Landroid/app/IWallpaperManagerCallback;II)V
      //   61: goto -> 76
      //   64: astore_1
      //   65: invokestatic access$000 : ()Ljava/lang/String;
      //   68: ldc_w 'Can't unregister color updates'
      //   71: aload_1
      //   72: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   75: pop
      //   76: aload_0
      //   77: monitorexit
      //   78: return
      //   79: astore_1
      //   80: aload_0
      //   81: monitorexit
      //   82: aload_1
      //   83: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #369	-> 0
      //   #370	-> 2
      //   #372	-> 27
      //   #373	-> 44
      //   #375	-> 49
      //   #379	-> 61
      //   #376	-> 64
      //   #378	-> 65
      //   #381	-> 76
      //   #382	-> 78
      //   #381	-> 79
      // Exception table:
      //   from	to	target	type
      //   2	27	79	finally
      //   27	44	79	finally
      //   44	49	79	finally
      //   49	61	64	android/os/RemoteException
      //   49	61	79	finally
      //   65	76	79	finally
      //   76	78	79	finally
      //   80	82	79	finally
    }
    
    public void onWallpaperColorsChanged(WallpaperColors param1WallpaperColors, int param1Int1, int param1Int2) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mColorListeners : Ljava/util/ArrayList;
      //   6: invokevirtual iterator : ()Ljava/util/Iterator;
      //   9: astore #4
      //   11: aload #4
      //   13: invokeinterface hasNext : ()Z
      //   18: ifeq -> 87
      //   21: aload #4
      //   23: invokeinterface next : ()Ljava/lang/Object;
      //   28: checkcast android/util/Pair
      //   31: astore #5
      //   33: aload #5
      //   35: getfield second : Ljava/lang/Object;
      //   38: checkcast android/os/Handler
      //   41: astore #6
      //   43: aload #5
      //   45: getfield second : Ljava/lang/Object;
      //   48: ifnonnull -> 60
      //   51: aload_0
      //   52: getfield mMainLooperHandler : Landroid/os/Handler;
      //   55: astore #6
      //   57: goto -> 60
      //   60: new android/app/_$$Lambda$WallpaperManager$Globals$1AcnQUORvPlCjJoNqdxfQT4o4Nw
      //   63: astore #7
      //   65: aload #7
      //   67: aload_0
      //   68: aload #5
      //   70: aload_1
      //   71: iload_2
      //   72: iload_3
      //   73: invokespecial <init> : (Landroid/app/WallpaperManager$Globals;Landroid/util/Pair;Landroid/app/WallpaperColors;II)V
      //   76: aload #6
      //   78: aload #7
      //   80: invokevirtual post : (Ljava/lang/Runnable;)Z
      //   83: pop
      //   84: goto -> 11
      //   87: aload_0
      //   88: monitorexit
      //   89: return
      //   90: astore_1
      //   91: aload_0
      //   92: monitorexit
      //   93: aload_1
      //   94: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #386	-> 0
      //   #387	-> 2
      //   #388	-> 33
      //   #389	-> 43
      //   #390	-> 51
      //   #389	-> 60
      //   #392	-> 60
      //   #403	-> 84
      //   #404	-> 87
      //   #405	-> 89
      //   #404	-> 90
      // Exception table:
      //   from	to	target	type
      //   2	11	90	finally
      //   11	33	90	finally
      //   33	43	90	finally
      //   43	51	90	finally
      //   51	57	90	finally
      //   60	84	90	finally
      //   87	89	90	finally
      //   91	93	90	finally
    }
    
    WallpaperColors getWallpaperColors(int param1Int1, int param1Int2, int param1Int3) {
      if (param1Int1 == 2 || param1Int1 == 1)
        try {
          return this.mService.getWallpaperColors(param1Int1, param1Int2, param1Int3);
        } catch (RemoteException remoteException) {
          return null;
        }  
      throw new IllegalArgumentException("Must request colors for exactly one kind of wallpaper");
    }
    
    public Bitmap peekWallpaperBitmap(Context param1Context, boolean param1Boolean, int param1Int, WallpaperManager.ColorManagementProxy param1ColorManagementProxy) {
      try {
        Trace.traceBegin(8L, "WallpaperManager.Globals.peekWallpaperBitmap");
        return peekWallpaperBitmap(param1Context, param1Boolean, param1Int, param1Context.getUserId(), false, param1ColorManagementProxy);
      } finally {
        Trace.traceEnd(8L);
      } 
    }
    
    public Bitmap peekWallpaperBitmap(Context param1Context, boolean param1Boolean1, int param1Int1, int param1Int2, boolean param1Boolean2, WallpaperManager.ColorManagementProxy param1ColorManagementProxy) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mService : Landroid/app/IWallpaperManager;
      //   4: astore #7
      //   6: aload #7
      //   8: ifnull -> 40
      //   11: aload #7
      //   13: aload_1
      //   14: invokevirtual getOpPackageName : ()Ljava/lang/String;
      //   17: invokeinterface isWallpaperSupported : (Ljava/lang/String;)Z
      //   22: istore #8
      //   24: iload #8
      //   26: ifne -> 31
      //   29: aconst_null
      //   30: areturn
      //   31: goto -> 40
      //   34: astore_1
      //   35: aload_1
      //   36: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
      //   39: athrow
      //   40: aload_0
      //   41: monitorenter
      //   42: aload_0
      //   43: getfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   46: ifnull -> 81
      //   49: aload_0
      //   50: getfield mCachedWallpaperUserId : I
      //   53: iload #4
      //   55: if_icmpne -> 81
      //   58: aload_0
      //   59: getfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   62: astore #7
      //   64: aload #7
      //   66: invokevirtual isRecycled : ()Z
      //   69: ifne -> 81
      //   72: aload_0
      //   73: getfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   76: astore_1
      //   77: aload_0
      //   78: monitorexit
      //   79: aload_1
      //   80: areturn
      //   81: aload_0
      //   82: aconst_null
      //   83: putfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   86: aload_0
      //   87: iconst_0
      //   88: putfield mCachedWallpaperUserId : I
      //   91: aload_0
      //   92: aload_0
      //   93: aload_1
      //   94: iload #4
      //   96: iload #5
      //   98: aload #6
      //   100: invokespecial getCurrentWallpaperLocked : (Landroid/content/Context;IZLandroid/app/WallpaperManager$ColorManagementProxy;)Landroid/graphics/Bitmap;
      //   103: putfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   106: aload_0
      //   107: iload #4
      //   109: putfield mCachedWallpaperUserId : I
      //   112: goto -> 188
      //   115: astore #6
      //   117: aload_1
      //   118: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
      //   121: getfield targetSdkVersion : I
      //   124: bipush #27
      //   126: if_icmpge -> 141
      //   129: invokestatic access$000 : ()Ljava/lang/String;
      //   132: ldc 'No permission to access wallpaper, suppressing exception to avoid crashing legacy app.'
      //   134: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   137: pop
      //   138: goto -> 188
      //   141: aload #6
      //   143: athrow
      //   144: astore #9
      //   146: invokestatic access$000 : ()Ljava/lang/String;
      //   149: astore #7
      //   151: new java/lang/StringBuilder
      //   154: astore #6
      //   156: aload #6
      //   158: invokespecial <init> : ()V
      //   161: aload #6
      //   163: ldc 'Out of memory loading the current wallpaper: '
      //   165: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   168: pop
      //   169: aload #6
      //   171: aload #9
      //   173: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
      //   176: pop
      //   177: aload #7
      //   179: aload #6
      //   181: invokevirtual toString : ()Ljava/lang/String;
      //   184: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   187: pop
      //   188: aload_0
      //   189: getfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   192: ifnull -> 204
      //   195: aload_0
      //   196: getfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   199: astore_1
      //   200: aload_0
      //   201: monitorexit
      //   202: aload_1
      //   203: areturn
      //   204: aload_0
      //   205: monitorexit
      //   206: iload_2
      //   207: ifeq -> 254
      //   210: aload_0
      //   211: getfield mDefaultWallpaper : Landroid/graphics/Bitmap;
      //   214: astore #7
      //   216: aload #7
      //   218: astore #6
      //   220: aload #7
      //   222: ifnonnull -> 251
      //   225: aload_0
      //   226: aload_1
      //   227: iload_3
      //   228: invokespecial getDefaultWallpaper : (Landroid/content/Context;I)Landroid/graphics/Bitmap;
      //   231: astore #6
      //   233: aload_0
      //   234: monitorenter
      //   235: aload_0
      //   236: aload #6
      //   238: putfield mDefaultWallpaper : Landroid/graphics/Bitmap;
      //   241: aload_0
      //   242: monitorexit
      //   243: goto -> 251
      //   246: astore_1
      //   247: aload_0
      //   248: monitorexit
      //   249: aload_1
      //   250: athrow
      //   251: aload #6
      //   253: areturn
      //   254: aconst_null
      //   255: areturn
      //   256: astore_1
      //   257: aload_0
      //   258: monitorexit
      //   259: aload_1
      //   260: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #441	-> 0
      //   #443	-> 11
      //   #444	-> 29
      //   #448	-> 31
      //   #446	-> 34
      //   #447	-> 35
      //   #450	-> 40
      //   #451	-> 42
      //   #452	-> 64
      //   #453	-> 72
      //   #455	-> 81
      //   #456	-> 86
      //   #458	-> 91
      //   #460	-> 106
      //   #471	-> 112
      //   #463	-> 115
      //   #464	-> 117
      //   #465	-> 129
      //   #469	-> 141
      //   #461	-> 144
      //   #462	-> 146
      //   #471	-> 188
      //   #472	-> 188
      //   #473	-> 195
      //   #475	-> 204
      //   #476	-> 206
      //   #477	-> 210
      //   #478	-> 216
      //   #479	-> 225
      //   #480	-> 233
      //   #481	-> 235
      //   #482	-> 241
      //   #484	-> 251
      //   #486	-> 254
      //   #475	-> 256
      // Exception table:
      //   from	to	target	type
      //   11	24	34	android/os/RemoteException
      //   42	64	256	finally
      //   64	72	256	finally
      //   72	79	256	finally
      //   81	86	256	finally
      //   86	91	256	finally
      //   91	106	144	java/lang/OutOfMemoryError
      //   91	106	115	java/lang/SecurityException
      //   91	106	256	finally
      //   106	112	144	java/lang/OutOfMemoryError
      //   106	112	115	java/lang/SecurityException
      //   106	112	256	finally
      //   117	129	256	finally
      //   129	138	256	finally
      //   141	144	256	finally
      //   146	188	256	finally
      //   188	195	256	finally
      //   195	202	256	finally
      //   204	206	256	finally
      //   235	241	246	finally
      //   241	243	246	finally
      //   247	249	246	finally
      //   257	259	256	finally
    }
    
    void forgetLoadedWallpaper() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: aconst_null
      //   4: putfield mCachedWallpaper : Landroid/graphics/Bitmap;
      //   7: aload_0
      //   8: iconst_0
      //   9: putfield mCachedWallpaperUserId : I
      //   12: aload_0
      //   13: aconst_null
      //   14: putfield mDefaultWallpaper : Landroid/graphics/Bitmap;
      //   17: aload_0
      //   18: monitorexit
      //   19: return
      //   20: astore_1
      //   21: aload_0
      //   22: monitorexit
      //   23: aload_1
      //   24: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #490	-> 0
      //   #491	-> 2
      //   #492	-> 7
      //   #493	-> 12
      //   #494	-> 17
      //   #495	-> 19
      //   #494	-> 20
      // Exception table:
      //   from	to	target	type
      //   2	7	20	finally
      //   7	12	20	finally
      //   12	17	20	finally
      //   17	19	20	finally
      //   21	23	20	finally
    }
    
    private Bitmap getCurrentWallpaperLocked(Context param1Context, int param1Int, boolean param1Boolean, WallpaperManager.ColorManagementProxy param1ColorManagementProxy) {
      if (this.mService == null) {
        Log.w(WallpaperManager.TAG, "WallpaperService not running");
        return null;
      } 
      try {
        Bundle bundle = new Bundle();
        this();
        IWallpaperManager iWallpaperManager = this.mService;
        String str2 = param1Context.getOpPackageName(), str1 = param1Context.getAttributionTag();
        ParcelFileDescriptor parcelFileDescriptor = iWallpaperManager.getWallpaperWithFeature(str2, str1, this, 1, bundle, param1Int);
        if (parcelFileDescriptor != null)
          try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            this();
            if (param1Boolean)
              options.inPreferredConfig = Bitmap.Config.HARDWARE; 
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
            IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor);
            return bitmap;
          } catch (OutOfMemoryError outOfMemoryError) {
            Log.w(WallpaperManager.TAG, "Can't decode file", outOfMemoryError);
            IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor);
          } finally {} 
        return null;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    private Bitmap getDefaultWallpaper(Context param1Context, int param1Int) {
      InputStream inputStream = WallpaperManager.openDefaultWallpaper(param1Context, param1Int);
      if (inputStream != null)
        try {
          BitmapFactory.Options options = new BitmapFactory.Options();
          this();
          Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null, options);
          IoUtils.closeQuietly(inputStream);
          return bitmap;
        } catch (OutOfMemoryError outOfMemoryError) {
          Log.w(WallpaperManager.TAG, "Can't decode stream", outOfMemoryError);
          IoUtils.closeQuietly(inputStream);
        } finally {
          Exception exception;
        }  
      return null;
    }
  }
  
  static void initGlobals(IWallpaperManager paramIWallpaperManager, Looper paramLooper) {
    synchronized (sSync) {
      if (sGlobals == null) {
        Globals globals = new Globals();
        this(paramIWallpaperManager, paramLooper);
        sGlobals = globals;
      } 
      return;
    } 
  }
  
  WallpaperManager(IWallpaperManager paramIWallpaperManager, Context paramContext, Handler paramHandler) {
    boolean bool;
    this.mWallpaperXStep = -1.0F;
    this.mWallpaperYStep = -1.0F;
    this.mContext = paramContext;
    if (paramIWallpaperManager != null)
      initGlobals(paramIWallpaperManager, paramContext.getMainLooper()); 
    if (paramContext.getResources().getConfiguration().isScreenWideColorGamut() && 
      paramContext.getResources().getBoolean(17891454)) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mWcgEnabled = bool;
    this.mCmProxy = new ColorManagementProxy(paramContext);
  }
  
  WallpaperManager() {
    this.mWallpaperXStep = -1.0F;
    this.mWallpaperYStep = -1.0F;
    this.mContext = null;
    this.mCmProxy = null;
    this.mWcgEnabled = false;
  }
  
  public static WallpaperManager getInstance(Context paramContext) {
    return (WallpaperManager)paramContext.getSystemService("wallpaper");
  }
  
  public IWallpaperManager getIWallpaperManager() {
    return sGlobals.mService;
  }
  
  public boolean shouldEnableWideColorGamut() {
    return this.mWcgEnabled;
  }
  
  public Drawable getDrawable() {
    ColorManagementProxy colorManagementProxy = getColorManagementProxy();
    Bitmap bitmap = sGlobals.peekWallpaperBitmap(this.mContext, true, 1, colorManagementProxy);
    if (bitmap != null) {
      BitmapDrawable bitmapDrawable = new BitmapDrawable(this.mContext.getResources(), bitmap);
      bitmapDrawable.setDither(false);
      return bitmapDrawable;
    } 
    return null;
  }
  
  public Drawable getBuiltInDrawable() {
    return getBuiltInDrawable(0, 0, false, 0.0F, 0.0F, 1);
  }
  
  public Drawable getBuiltInDrawable(int paramInt) {
    return getBuiltInDrawable(0, 0, false, 0.0F, 0.0F, paramInt);
  }
  
  public Drawable getBuiltInDrawable(int paramInt1, int paramInt2, boolean paramBoolean, float paramFloat1, float paramFloat2) {
    return getBuiltInDrawable(paramInt1, paramInt2, paramBoolean, paramFloat1, paramFloat2, 1);
  }
  
  public Drawable getBuiltInDrawable(int paramInt1, int paramInt2, boolean paramBoolean, float paramFloat1, float paramFloat2, int paramInt3) {
    // Byte code:
    //   0: getstatic android/app/WallpaperManager.sGlobals : Landroid/app/WallpaperManager$Globals;
    //   3: invokestatic access$200 : (Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
    //   6: ifnull -> 1191
    //   9: iload #6
    //   11: iconst_1
    //   12: if_icmpeq -> 35
    //   15: iload #6
    //   17: iconst_2
    //   18: if_icmpne -> 24
    //   21: goto -> 35
    //   24: new java/lang/IllegalArgumentException
    //   27: dup
    //   28: ldc_w 'Must request exactly one kind of wallpaper'
    //   31: invokespecial <init> : (Ljava/lang/String;)V
    //   34: athrow
    //   35: aload_0
    //   36: getfield mContext : Landroid/content/Context;
    //   39: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   42: astore #7
    //   44: fconst_0
    //   45: fconst_1
    //   46: fload #4
    //   48: invokestatic min : (FF)F
    //   51: invokestatic max : (FF)F
    //   54: fstore #4
    //   56: fconst_0
    //   57: fconst_1
    //   58: fload #5
    //   60: invokestatic min : (FF)F
    //   63: invokestatic max : (FF)F
    //   66: fstore #8
    //   68: aload_0
    //   69: getfield mContext : Landroid/content/Context;
    //   72: iload #6
    //   74: invokestatic openDefaultWallpaper : (Landroid/content/Context;I)Ljava/io/InputStream;
    //   77: astore #9
    //   79: aload #9
    //   81: ifnonnull -> 143
    //   84: getstatic android/app/WallpaperManager.DEBUG : Z
    //   87: ifeq -> 141
    //   90: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   93: astore #10
    //   95: new java/lang/StringBuilder
    //   98: dup
    //   99: invokespecial <init> : ()V
    //   102: astore #11
    //   104: aload #11
    //   106: ldc_w 'default wallpaper stream '
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #11
    //   115: iload #6
    //   117: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload #11
    //   123: ldc_w ' is null'
    //   126: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: pop
    //   130: aload #10
    //   132: aload #11
    //   134: invokevirtual toString : ()Ljava/lang/String;
    //   137: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   140: pop
    //   141: aconst_null
    //   142: areturn
    //   143: new java/io/BufferedInputStream
    //   146: dup
    //   147: aload #9
    //   149: invokespecial <init> : (Ljava/io/InputStream;)V
    //   152: astore #12
    //   154: iload_1
    //   155: ifle -> 1137
    //   158: iload_2
    //   159: ifgt -> 165
    //   162: goto -> 1137
    //   165: new android/graphics/BitmapFactory$Options
    //   168: astore #10
    //   170: aload #10
    //   172: invokespecial <init> : ()V
    //   175: aload #10
    //   177: iconst_1
    //   178: putfield inJustDecodeBounds : Z
    //   181: aload #12
    //   183: aconst_null
    //   184: aload #10
    //   186: invokestatic decodeStream : (Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   189: pop
    //   190: aload #10
    //   192: getfield outWidth : I
    //   195: ifeq -> 1106
    //   198: aload #10
    //   200: getfield outHeight : I
    //   203: ifeq -> 1106
    //   206: aload #10
    //   208: getfield outWidth : I
    //   211: istore #13
    //   213: aload #10
    //   215: getfield outHeight : I
    //   218: istore #14
    //   220: new java/io/BufferedInputStream
    //   223: astore #11
    //   225: aload #11
    //   227: aload_0
    //   228: getfield mContext : Landroid/content/Context;
    //   231: iload #6
    //   233: invokestatic openDefaultWallpaper : (Landroid/content/Context;I)Ljava/io/InputStream;
    //   236: invokespecial <init> : (Ljava/io/InputStream;)V
    //   239: iload #13
    //   241: iload_1
    //   242: invokestatic min : (II)I
    //   245: istore_1
    //   246: iload #14
    //   248: iload_2
    //   249: invokestatic min : (II)I
    //   252: istore_2
    //   253: iload_3
    //   254: ifeq -> 296
    //   257: aload #11
    //   259: astore #10
    //   261: iload #13
    //   263: iload #14
    //   265: iload_1
    //   266: iload_2
    //   267: fload #4
    //   269: fload #8
    //   271: invokestatic getMaxCropRect : (IIIIFF)Landroid/graphics/RectF;
    //   274: astore #12
    //   276: goto -> 351
    //   279: astore #11
    //   281: aload #10
    //   283: astore #12
    //   285: aload #11
    //   287: astore #10
    //   289: aload #12
    //   291: astore #11
    //   293: goto -> 1178
    //   296: aload #11
    //   298: astore #10
    //   300: iload #13
    //   302: iload_1
    //   303: isub
    //   304: i2f
    //   305: fload #4
    //   307: fmul
    //   308: fstore #5
    //   310: iload_1
    //   311: i2f
    //   312: fstore #4
    //   314: iload #14
    //   316: iload_2
    //   317: isub
    //   318: i2f
    //   319: fload #8
    //   321: fmul
    //   322: fstore #8
    //   324: iload_2
    //   325: i2f
    //   326: fstore #15
    //   328: new android/graphics/RectF
    //   331: dup
    //   332: fload #5
    //   334: fload #8
    //   336: fload #4
    //   338: fload #5
    //   340: fadd
    //   341: fload #15
    //   343: fload #8
    //   345: fadd
    //   346: invokespecial <init> : (FFFF)V
    //   349: astore #12
    //   351: aload #11
    //   353: astore #10
    //   355: new android/graphics/Rect
    //   358: astore #16
    //   360: aload #11
    //   362: astore #10
    //   364: aload #16
    //   366: invokespecial <init> : ()V
    //   369: aload #11
    //   371: astore #10
    //   373: aload #12
    //   375: aload #16
    //   377: invokevirtual roundOut : (Landroid/graphics/Rect;)V
    //   380: aload #11
    //   382: astore #10
    //   384: aload #16
    //   386: invokevirtual width : ()I
    //   389: ifle -> 1040
    //   392: aload #11
    //   394: astore #10
    //   396: aload #16
    //   398: invokevirtual height : ()I
    //   401: ifgt -> 407
    //   404: goto -> 1040
    //   407: aload #11
    //   409: astore #10
    //   411: aload #16
    //   413: invokevirtual width : ()I
    //   416: iload_1
    //   417: idiv
    //   418: istore #14
    //   420: aload #11
    //   422: astore #10
    //   424: aload #16
    //   426: invokevirtual height : ()I
    //   429: iload_2
    //   430: idiv
    //   431: istore #13
    //   433: aload #11
    //   435: astore #10
    //   437: iload #14
    //   439: iload #13
    //   441: invokestatic min : (II)I
    //   444: istore #14
    //   446: aconst_null
    //   447: astore #17
    //   449: aload #11
    //   451: astore #10
    //   453: aload #11
    //   455: iconst_1
    //   456: invokestatic newInstance : (Ljava/io/InputStream;Z)Landroid/graphics/BitmapRegionDecoder;
    //   459: astore #12
    //   461: aload #12
    //   463: astore #17
    //   465: goto -> 484
    //   468: astore #10
    //   470: aload #11
    //   472: astore #10
    //   474: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   477: ldc_w 'cannot open region decoder for default wallpaper'
    //   480: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   483: pop
    //   484: aconst_null
    //   485: astore #12
    //   487: aload #17
    //   489: ifnull -> 551
    //   492: aload #11
    //   494: astore #10
    //   496: new android/graphics/BitmapFactory$Options
    //   499: astore #12
    //   501: aload #11
    //   503: astore #10
    //   505: aload #12
    //   507: invokespecial <init> : ()V
    //   510: iload #14
    //   512: iconst_1
    //   513: if_icmple -> 527
    //   516: aload #11
    //   518: astore #10
    //   520: aload #12
    //   522: iload #14
    //   524: putfield inSampleSize : I
    //   527: aload #11
    //   529: astore #10
    //   531: aload #17
    //   533: aload #16
    //   535: aload #12
    //   537: invokevirtual decodeRegion : (Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   540: astore #12
    //   542: aload #11
    //   544: astore #10
    //   546: aload #17
    //   548: invokevirtual recycle : ()V
    //   551: aload #12
    //   553: ifnonnull -> 710
    //   556: aload #11
    //   558: astore #10
    //   560: new java/io/BufferedInputStream
    //   563: astore #17
    //   565: aload #11
    //   567: astore #10
    //   569: aload #17
    //   571: aload_0
    //   572: getfield mContext : Landroid/content/Context;
    //   575: iload #6
    //   577: invokestatic openDefaultWallpaper : (Landroid/content/Context;I)Ljava/io/InputStream;
    //   580: invokespecial <init> : (Ljava/io/InputStream;)V
    //   583: aload #17
    //   585: astore #11
    //   587: aload #11
    //   589: astore #10
    //   591: new android/graphics/BitmapFactory$Options
    //   594: astore #17
    //   596: aload #11
    //   598: astore #10
    //   600: aload #17
    //   602: invokespecial <init> : ()V
    //   605: iload #14
    //   607: iconst_1
    //   608: if_icmple -> 622
    //   611: aload #11
    //   613: astore #10
    //   615: aload #17
    //   617: iload #14
    //   619: putfield inSampleSize : I
    //   622: aload #11
    //   624: astore #10
    //   626: aload #11
    //   628: aconst_null
    //   629: aload #17
    //   631: invokestatic decodeStream : (Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   634: astore #17
    //   636: aload #17
    //   638: ifnull -> 707
    //   641: aload #11
    //   643: astore #10
    //   645: aload #16
    //   647: getfield left : I
    //   650: istore #13
    //   652: aload #11
    //   654: astore #10
    //   656: aload #16
    //   658: getfield top : I
    //   661: istore #14
    //   663: aload #11
    //   665: astore #10
    //   667: aload #16
    //   669: invokevirtual width : ()I
    //   672: istore #18
    //   674: aload #11
    //   676: astore #10
    //   678: aload #16
    //   680: invokevirtual height : ()I
    //   683: istore #6
    //   685: aload #11
    //   687: astore #10
    //   689: aload #17
    //   691: iload #13
    //   693: iload #14
    //   695: iload #18
    //   697: iload #6
    //   699: invokestatic createBitmap : (Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;
    //   702: astore #12
    //   704: goto -> 710
    //   707: goto -> 710
    //   710: aload #12
    //   712: ifnonnull -> 741
    //   715: aload #11
    //   717: astore #10
    //   719: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   722: ldc_w 'cannot decode default wallpaper'
    //   725: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   728: pop
    //   729: aload #11
    //   731: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   734: aload #9
    //   736: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   739: aconst_null
    //   740: areturn
    //   741: iload_1
    //   742: ifle -> 1010
    //   745: iload_2
    //   746: ifle -> 1010
    //   749: aload #11
    //   751: astore #10
    //   753: aload #12
    //   755: invokevirtual getWidth : ()I
    //   758: istore #6
    //   760: iload #6
    //   762: iload_1
    //   763: if_icmpne -> 789
    //   766: aload #11
    //   768: astore #10
    //   770: aload #12
    //   772: invokevirtual getHeight : ()I
    //   775: istore #6
    //   777: iload #6
    //   779: iload_2
    //   780: if_icmpeq -> 786
    //   783: goto -> 789
    //   786: goto -> 1010
    //   789: aload #11
    //   791: astore #10
    //   793: new android/graphics/Matrix
    //   796: astore #16
    //   798: aload #11
    //   800: astore #10
    //   802: aload #16
    //   804: invokespecial <init> : ()V
    //   807: aload #11
    //   809: astore #10
    //   811: new android/graphics/RectF
    //   814: astore #19
    //   816: aload #11
    //   818: astore #10
    //   820: aload #19
    //   822: fconst_0
    //   823: fconst_0
    //   824: aload #12
    //   826: invokevirtual getWidth : ()I
    //   829: i2f
    //   830: aload #12
    //   832: invokevirtual getHeight : ()I
    //   835: i2f
    //   836: invokespecial <init> : (FFFF)V
    //   839: aload #11
    //   841: astore #10
    //   843: new android/graphics/RectF
    //   846: astore #17
    //   848: iload_1
    //   849: i2f
    //   850: fstore #4
    //   852: iload_2
    //   853: i2f
    //   854: fstore #5
    //   856: aload #11
    //   858: astore #10
    //   860: aload #17
    //   862: fconst_0
    //   863: fconst_0
    //   864: fload #4
    //   866: fload #5
    //   868: invokespecial <init> : (FFFF)V
    //   871: aload #11
    //   873: astore #10
    //   875: aload #16
    //   877: aload #19
    //   879: aload #17
    //   881: getstatic android/graphics/Matrix$ScaleToFit.FILL : Landroid/graphics/Matrix$ScaleToFit;
    //   884: invokevirtual setRectToRect : (Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z
    //   887: pop
    //   888: aload #11
    //   890: astore #10
    //   892: aload #17
    //   894: invokevirtual width : ()F
    //   897: f2i
    //   898: istore_2
    //   899: aload #11
    //   901: astore #10
    //   903: aload #17
    //   905: invokevirtual height : ()F
    //   908: f2i
    //   909: istore_1
    //   910: aload #11
    //   912: astore #10
    //   914: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   917: astore #17
    //   919: aload #11
    //   921: astore #10
    //   923: iload_2
    //   924: iload_1
    //   925: aload #17
    //   927: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   930: astore #17
    //   932: aload #17
    //   934: ifnull -> 1007
    //   937: aload #11
    //   939: astore #10
    //   941: new android/graphics/Canvas
    //   944: astore #19
    //   946: aload #11
    //   948: astore #10
    //   950: aload #19
    //   952: aload #17
    //   954: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   957: aload #11
    //   959: astore #10
    //   961: new android/graphics/Paint
    //   964: astore #20
    //   966: aload #11
    //   968: astore #10
    //   970: aload #20
    //   972: invokespecial <init> : ()V
    //   975: aload #11
    //   977: astore #10
    //   979: aload #20
    //   981: iconst_1
    //   982: invokevirtual setFilterBitmap : (Z)V
    //   985: aload #11
    //   987: astore #10
    //   989: aload #19
    //   991: aload #12
    //   993: aload #16
    //   995: aload #20
    //   997: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
    //   1000: aload #17
    //   1002: astore #12
    //   1004: goto -> 1010
    //   1007: goto -> 1010
    //   1010: aload #11
    //   1012: astore #10
    //   1014: new android/graphics/drawable/BitmapDrawable
    //   1017: dup
    //   1018: aload #7
    //   1020: aload #12
    //   1022: invokespecial <init> : (Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    //   1025: astore #12
    //   1027: aload #11
    //   1029: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1032: aload #9
    //   1034: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1037: aload #12
    //   1039: areturn
    //   1040: aload #11
    //   1042: astore #10
    //   1044: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   1047: ldc_w 'crop has bad values for full size image'
    //   1050: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1053: pop
    //   1054: aload #11
    //   1056: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1059: aload #9
    //   1061: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1064: aconst_null
    //   1065: areturn
    //   1066: astore #12
    //   1068: aload #10
    //   1070: astore #11
    //   1072: aload #12
    //   1074: astore #10
    //   1076: goto -> 1178
    //   1079: astore #11
    //   1081: aload #10
    //   1083: astore #12
    //   1085: aload #11
    //   1087: astore #10
    //   1089: aload #12
    //   1091: astore #11
    //   1093: goto -> 1178
    //   1096: astore #10
    //   1098: goto -> 1178
    //   1101: astore #10
    //   1103: goto -> 1178
    //   1106: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   1109: ldc_w 'default wallpaper dimensions are 0'
    //   1112: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1115: pop
    //   1116: aload #12
    //   1118: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1121: aload #9
    //   1123: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1126: aconst_null
    //   1127: areturn
    //   1128: astore #10
    //   1130: aload #12
    //   1132: astore #11
    //   1134: goto -> 1178
    //   1137: aload #12
    //   1139: aconst_null
    //   1140: aconst_null
    //   1141: invokestatic decodeStream : (Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   1144: astore #10
    //   1146: new android/graphics/drawable/BitmapDrawable
    //   1149: dup
    //   1150: aload #7
    //   1152: aload #10
    //   1154: invokespecial <init> : (Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    //   1157: astore #10
    //   1159: aload #12
    //   1161: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1164: aload #9
    //   1166: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1169: aload #10
    //   1171: areturn
    //   1172: astore #10
    //   1174: aload #12
    //   1176: astore #11
    //   1178: aload #11
    //   1180: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1183: aload #9
    //   1185: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   1188: aload #10
    //   1190: athrow
    //   1191: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   1194: ldc_w 'WallpaperService not running'
    //   1197: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1200: pop
    //   1201: new java/lang/RuntimeException
    //   1204: dup
    //   1205: new android/os/DeadSystemException
    //   1208: dup
    //   1209: invokespecial <init> : ()V
    //   1212: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   1215: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #711	-> 0
    //   #716	-> 9
    //   #717	-> 24
    //   #720	-> 35
    //   #721	-> 44
    //   #722	-> 56
    //   #724	-> 68
    //   #725	-> 79
    //   #726	-> 84
    //   #727	-> 90
    //   #729	-> 141
    //   #731	-> 143
    //   #736	-> 154
    //   #744	-> 165
    //   #745	-> 175
    //   #746	-> 181
    //   #747	-> 190
    //   #748	-> 206
    //   #749	-> 213
    //   #758	-> 220
    //   #762	-> 239
    //   #763	-> 246
    //   #764	-> 253
    //   #765	-> 257
    //   #850	-> 279
    //   #768	-> 296
    //   #769	-> 310
    //   #770	-> 314
    //   #771	-> 324
    //   #772	-> 328
    //   #774	-> 351
    //   #775	-> 369
    //   #777	-> 380
    //   #783	-> 407
    //   #784	-> 420
    //   #783	-> 433
    //   #787	-> 446
    //   #789	-> 449
    //   #792	-> 465
    //   #790	-> 468
    //   #791	-> 470
    //   #794	-> 484
    //   #795	-> 487
    //   #797	-> 492
    //   #798	-> 510
    //   #799	-> 516
    //   #801	-> 527
    //   #802	-> 542
    //   #805	-> 551
    //   #808	-> 556
    //   #809	-> 587
    //   #810	-> 587
    //   #811	-> 605
    //   #812	-> 611
    //   #814	-> 622
    //   #815	-> 636
    //   #816	-> 641
    //   #817	-> 663
    //   #818	-> 674
    //   #816	-> 685
    //   #815	-> 707
    //   #805	-> 710
    //   #822	-> 710
    //   #823	-> 715
    //   #824	-> 729
    //   #850	-> 729
    //   #851	-> 734
    //   #824	-> 739
    //   #828	-> 741
    //   #829	-> 749
    //   #830	-> 789
    //   #831	-> 807
    //   #832	-> 839
    //   #833	-> 871
    //   #834	-> 888
    //   #835	-> 899
    //   #834	-> 919
    //   #836	-> 932
    //   #837	-> 937
    //   #838	-> 957
    //   #839	-> 975
    //   #840	-> 985
    //   #841	-> 1000
    //   #836	-> 1007
    //   #828	-> 1010
    //   #845	-> 1010
    //   #850	-> 1027
    //   #851	-> 1032
    //   #845	-> 1037
    //   #777	-> 1040
    //   #778	-> 1040
    //   #779	-> 1054
    //   #850	-> 1054
    //   #851	-> 1059
    //   #779	-> 1064
    //   #850	-> 1066
    //   #747	-> 1106
    //   #751	-> 1106
    //   #752	-> 1116
    //   #850	-> 1116
    //   #851	-> 1121
    //   #752	-> 1126
    //   #850	-> 1128
    //   #736	-> 1137
    //   #737	-> 1137
    //   #738	-> 1146
    //   #850	-> 1159
    //   #851	-> 1164
    //   #738	-> 1169
    //   #850	-> 1172
    //   #851	-> 1183
    //   #852	-> 1188
    //   #712	-> 1191
    //   #713	-> 1201
    // Exception table:
    //   from	to	target	type
    //   165	175	1128	finally
    //   175	181	1128	finally
    //   181	190	1128	finally
    //   190	206	1128	finally
    //   206	213	1128	finally
    //   213	220	1128	finally
    //   220	239	1128	finally
    //   239	246	1101	finally
    //   246	253	1096	finally
    //   261	276	279	finally
    //   328	351	1079	finally
    //   355	360	1079	finally
    //   364	369	1079	finally
    //   373	380	1079	finally
    //   384	392	1079	finally
    //   396	404	1079	finally
    //   411	420	1079	finally
    //   424	433	1079	finally
    //   437	446	1079	finally
    //   453	461	468	java/io/IOException
    //   453	461	279	finally
    //   474	484	1079	finally
    //   496	501	279	finally
    //   505	510	279	finally
    //   520	527	279	finally
    //   531	542	279	finally
    //   546	551	279	finally
    //   560	565	279	finally
    //   569	583	279	finally
    //   591	596	279	finally
    //   600	605	279	finally
    //   615	622	279	finally
    //   626	636	279	finally
    //   645	652	279	finally
    //   656	663	279	finally
    //   667	674	279	finally
    //   678	685	279	finally
    //   689	704	279	finally
    //   719	729	279	finally
    //   753	760	1079	finally
    //   770	777	279	finally
    //   793	798	1079	finally
    //   802	807	1079	finally
    //   811	816	1079	finally
    //   820	839	1079	finally
    //   843	848	1079	finally
    //   860	871	1066	finally
    //   875	888	1066	finally
    //   892	899	1066	finally
    //   903	910	1066	finally
    //   914	919	1066	finally
    //   923	932	1066	finally
    //   941	946	1066	finally
    //   950	957	1066	finally
    //   961	966	1066	finally
    //   970	975	1066	finally
    //   979	985	1066	finally
    //   989	1000	1066	finally
    //   1014	1027	1066	finally
    //   1044	1054	1066	finally
    //   1106	1116	1172	finally
    //   1137	1146	1172	finally
    //   1146	1159	1172	finally
  }
  
  private static RectF getMaxCropRect(int paramInt1, int paramInt2, int paramInt3, int paramInt4, float paramFloat1, float paramFloat2) {
    RectF rectF = new RectF();
    if (paramInt1 / paramInt2 > paramInt3 / paramInt4) {
      rectF.top = 0.0F;
      rectF.bottom = paramInt2;
      paramFloat2 = paramInt3 * paramInt2 / paramInt4;
      rectF.left = (paramInt1 - paramFloat2) * paramFloat1;
      rectF.right = rectF.left + paramFloat2;
    } else {
      rectF.left = 0.0F;
      rectF.right = paramInt1;
      paramFloat1 = paramInt4 * paramInt1 / paramInt3;
      rectF.top = (paramInt2 - paramFloat1) * paramFloat2;
      rectF.bottom = rectF.top + paramFloat1;
    } 
    return rectF;
  }
  
  public Drawable peekDrawable() {
    ColorManagementProxy colorManagementProxy = getColorManagementProxy();
    Bitmap bitmap = sGlobals.peekWallpaperBitmap(this.mContext, false, 1, colorManagementProxy);
    if (bitmap != null) {
      BitmapDrawable bitmapDrawable = new BitmapDrawable(this.mContext.getResources(), bitmap);
      bitmapDrawable.setDither(false);
      return bitmapDrawable;
    } 
    return null;
  }
  
  public Drawable getFastDrawable() {
    ColorManagementProxy colorManagementProxy = getColorManagementProxy();
    Bitmap bitmap = sGlobals.peekWallpaperBitmap(this.mContext, true, 1, colorManagementProxy);
    if (bitmap != null)
      return new FastBitmapDrawable(); 
    return null;
  }
  
  public Drawable peekFastDrawable() {
    ColorManagementProxy colorManagementProxy = getColorManagementProxy();
    Bitmap bitmap = sGlobals.peekWallpaperBitmap(this.mContext, false, 1, colorManagementProxy);
    if (bitmap != null)
      return new FastBitmapDrawable(); 
    return null;
  }
  
  public boolean wallpaperSupportsWcg(int paramInt) {
    boolean bool = shouldEnableWideColorGamut();
    boolean bool1 = false;
    if (!bool)
      return false; 
    ColorManagementProxy colorManagementProxy = getColorManagementProxy();
    Bitmap bitmap = sGlobals.peekWallpaperBitmap(this.mContext, false, paramInt, colorManagementProxy);
    if (bitmap != null && bitmap.getColorSpace() != null && 
      bitmap.getColorSpace() != ColorSpace.get(ColorSpace.Named.SRGB) && 
      colorManagementProxy.isSupportedColorSpace(bitmap.getColorSpace()))
      bool1 = true; 
    return bool1;
  }
  
  public Bitmap getBitmap() {
    return getBitmap(false);
  }
  
  public Bitmap getBitmap(boolean paramBoolean) {
    return getBitmapAsUser(this.mContext.getUserId(), paramBoolean);
  }
  
  public Bitmap getBitmapAsUser(int paramInt, boolean paramBoolean) {
    null = getColorManagementProxy();
    try {
      Trace.traceBegin(8L, "WallpaperManager.Globals.peekWallpaperBitmap");
      return sGlobals.peekWallpaperBitmap(this.mContext, true, 1, paramInt, paramBoolean, null);
    } finally {
      Trace.traceEnd(8L);
    } 
  }
  
  public ParcelFileDescriptor getWallpaperFile(int paramInt) {
    return getWallpaperFile(paramInt, this.mContext.getUserId());
  }
  
  public void addOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener, Handler paramHandler) {
    addOnColorsChangedListener(paramOnColorsChangedListener, paramHandler, this.mContext.getUserId());
  }
  
  public void addOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener, Handler paramHandler, int paramInt) {
    sGlobals.addOnColorsChangedListener(paramOnColorsChangedListener, paramHandler, paramInt, this.mContext.getDisplayId());
  }
  
  public void removeOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener) {
    removeOnColorsChangedListener(paramOnColorsChangedListener, this.mContext.getUserId());
  }
  
  public void removeOnColorsChangedListener(OnColorsChangedListener paramOnColorsChangedListener, int paramInt) {
    sGlobals.removeOnColorsChangedListener(paramOnColorsChangedListener, paramInt, this.mContext.getDisplayId());
  }
  
  public WallpaperColors getWallpaperColors(int paramInt) {
    return getWallpaperColors(paramInt, this.mContext.getUserId());
  }
  
  public WallpaperColors getWallpaperColors(int paramInt1, int paramInt2) {
    return sGlobals.getWallpaperColors(paramInt1, paramInt2, this.mContext.getDisplayId());
  }
  
  public ParcelFileDescriptor getWallpaperFile(int paramInt1, int paramInt2) {
    if (paramInt1 == 1 || paramInt1 == 2) {
      if (sGlobals.mService != null)
        try {
          Bundle bundle = new Bundle();
          this();
          IWallpaperManager iWallpaperManager = sGlobals.mService;
          String str1 = this.mContext.getOpPackageName();
          Context context = this.mContext;
          String str2 = context.getAttributionTag();
          return iWallpaperManager.getWallpaperWithFeature(str1, str2, null, paramInt1, bundle, paramInt2);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } catch (SecurityException securityException) {
          if ((this.mContext.getApplicationInfo()).targetSdkVersion < 27) {
            Log.w(TAG, "No permission to access wallpaper, suppressing exception to avoid crashing legacy app.");
            return null;
          } 
          throw securityException;
        }  
      Log.w(TAG, "WallpaperService not running");
      throw new RuntimeException(new DeadSystemException());
    } 
    throw new IllegalArgumentException("Must request exactly one kind of wallpaper");
  }
  
  public void forgetLoadedWallpaper() {
    sGlobals.forgetLoadedWallpaper();
  }
  
  public WallpaperInfo getWallpaperInfo() {
    return getWallpaperInfo(this.mContext.getUserId());
  }
  
  public WallpaperInfo getWallpaperInfo(int paramInt) {
    try {
      if (sGlobals.mService != null)
        return sGlobals.mService.getWallpaperInfo(paramInt); 
      Log.w(TAG, "WallpaperService not running");
      RuntimeException runtimeException = new RuntimeException();
      DeadSystemException deadSystemException = new DeadSystemException();
      this();
      this((Throwable)deadSystemException);
      throw runtimeException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getWallpaperId(int paramInt) {
    return getWallpaperIdForUser(paramInt, this.mContext.getUserId());
  }
  
  public int getWallpaperIdForUser(int paramInt1, int paramInt2) {
    try {
      if (sGlobals.mService != null)
        return sGlobals.mService.getWallpaperIdForUser(paramInt1, paramInt2); 
      Log.w(TAG, "WallpaperService not running");
      RuntimeException runtimeException = new RuntimeException();
      DeadSystemException deadSystemException = new DeadSystemException();
      this();
      this((Throwable)deadSystemException);
      throw runtimeException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Intent getCropAndSetWallpaperIntent(Uri paramUri) {
    if (paramUri != null) {
      if ("content".equals(paramUri.getScheme())) {
        PackageManager packageManager = this.mContext.getPackageManager();
        Intent intent1 = new Intent("android.service.wallpaper.CROP_AND_SET_WALLPAPER", paramUri);
        intent1.addFlags(1);
        Intent intent2 = (new Intent("android.intent.action.MAIN")).addCategory("android.intent.category.HOME");
        ResolveInfo resolveInfo = packageManager.resolveActivity(intent2, 65536);
        if (resolveInfo != null) {
          intent1.setPackage(resolveInfo.activityInfo.packageName);
          List<ResolveInfo> list1 = packageManager.queryIntentActivities(intent1, 0);
          if (list1.size() > 0)
            return intent1; 
        } 
        String str = this.mContext.getString(17039977);
        intent1.setPackage(str);
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent1, 0);
        if (list.size() > 0)
          return intent1; 
        throw new IllegalArgumentException("Cannot use passed URI to set wallpaper; check that the type returned by ContentProvider matches image/*");
      } 
      throw new IllegalArgumentException("Image URI must be of the content scheme type");
    } 
    throw new IllegalArgumentException("Image URI must not be null");
  }
  
  public void setResource(int paramInt) throws IOException {
    setResource(paramInt, 3);
  }
  
  public int setResource(int paramInt1, int paramInt2) throws IOException {
    if (sGlobals.mService != null) {
      Bundle bundle = new Bundle();
      WallpaperSetCompletion wallpaperSetCompletion = new WallpaperSetCompletion();
      try {
        Resources resources = this.mContext.getResources();
        IWallpaperManager iWallpaperManager = sGlobals.mService;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("res:");
        stringBuilder.append(resources.getResourceName(paramInt1));
        String str1 = stringBuilder.toString();
        Context context1 = this.mContext;
        String str2 = context1.getOpPackageName();
        Context context2 = this.mContext;
        int i = context2.getUserId();
        ParcelFileDescriptor parcelFileDescriptor = iWallpaperManager.setWallpaper(str1, str2, null, false, bundle, paramInt2, wallpaperSetCompletion, i);
        if (parcelFileDescriptor != null) {
          ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream;
          str2 = null;
          String str = str2;
          try {
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream1 = new ParcelFileDescriptor.AutoCloseOutputStream();
            str = str2;
            this(parcelFileDescriptor);
            autoCloseOutputStream = autoCloseOutputStream1;
            copyStreamToWallpaperFile(resources.openRawResource(paramInt1), (FileOutputStream)autoCloseOutputStream1);
            autoCloseOutputStream = autoCloseOutputStream1;
            autoCloseOutputStream1.close();
            autoCloseOutputStream = autoCloseOutputStream1;
            wallpaperSetCompletion.waitForCompletion();
          } finally {
            IoUtils.closeQuietly((AutoCloseable)autoCloseOutputStream);
          } 
        } 
        return bundle.getInt("android.service.wallpaper.extra.ID", 0);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public void setBitmap(Bitmap paramBitmap) throws IOException {
    setBitmap(paramBitmap, null, true);
  }
  
  public int setBitmap(Bitmap paramBitmap, Rect paramRect, boolean paramBoolean) throws IOException {
    return setBitmap(paramBitmap, paramRect, paramBoolean, 3);
  }
  
  public int setBitmap(Bitmap paramBitmap, Rect paramRect, boolean paramBoolean, int paramInt) throws IOException {
    Context context = this.mContext;
    int i = context.getUserId();
    return setBitmap(paramBitmap, paramRect, paramBoolean, paramInt, i);
  }
  
  public int setBitmap(Bitmap paramBitmap, Rect paramRect, boolean paramBoolean, int paramInt1, int paramInt2) throws IOException {
    validateRect(paramRect);
    if (sGlobals.mService != null) {
      Bundle bundle = new Bundle();
      WallpaperSetCompletion wallpaperSetCompletion = new WallpaperSetCompletion();
      try {
        IWallpaperManager iWallpaperManager = sGlobals.mService;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        ParcelFileDescriptor parcelFileDescriptor = iWallpaperManager.setWallpaper(null, str, paramRect, paramBoolean, bundle, paramInt1, wallpaperSetCompletion, paramInt2);
        if (parcelFileDescriptor != null) {
          ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream;
          str = null;
          String str1 = str;
          try {
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream1 = new ParcelFileDescriptor.AutoCloseOutputStream();
            str1 = str;
            this(parcelFileDescriptor);
            autoCloseOutputStream = autoCloseOutputStream1;
            paramBitmap.compress(Bitmap.CompressFormat.PNG, 90, (OutputStream)autoCloseOutputStream1);
            autoCloseOutputStream = autoCloseOutputStream1;
            autoCloseOutputStream1.close();
            autoCloseOutputStream = autoCloseOutputStream1;
            wallpaperSetCompletion.waitForCompletion();
          } finally {
            IoUtils.closeQuietly((AutoCloseable)autoCloseOutputStream);
          } 
        } 
        return bundle.getInt("android.service.wallpaper.extra.ID", 0);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  private final void validateRect(Rect paramRect) {
    if (paramRect == null || !paramRect.isEmpty())
      return; 
    throw new IllegalArgumentException("visibleCrop rectangle must be valid and non-empty");
  }
  
  public void setStream(InputStream paramInputStream) throws IOException {
    setStream(paramInputStream, null, true);
  }
  
  private void copyStreamToWallpaperFile(InputStream paramInputStream, FileOutputStream paramFileOutputStream) throws IOException {
    FileUtils.copy(paramInputStream, paramFileOutputStream);
  }
  
  public int setStream(InputStream paramInputStream, Rect paramRect, boolean paramBoolean) throws IOException {
    return setStream(paramInputStream, paramRect, paramBoolean, 3);
  }
  
  public int setStream(InputStream paramInputStream, Rect paramRect, boolean paramBoolean, int paramInt) throws IOException {
    validateRect(paramRect);
    if (sGlobals.mService != null) {
      Bundle bundle = new Bundle();
      WallpaperSetCompletion wallpaperSetCompletion = new WallpaperSetCompletion();
      try {
        IWallpaperManager iWallpaperManager = sGlobals.mService;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        context = this.mContext;
        int i = context.getUserId();
        ParcelFileDescriptor parcelFileDescriptor = iWallpaperManager.setWallpaper(null, str, paramRect, paramBoolean, bundle, paramInt, wallpaperSetCompletion, i);
        if (parcelFileDescriptor != null) {
          ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream;
          iWallpaperManager = null;
          IWallpaperManager iWallpaperManager1 = iWallpaperManager;
          try {
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream2 = new ParcelFileDescriptor.AutoCloseOutputStream();
            iWallpaperManager1 = iWallpaperManager;
            this(parcelFileDescriptor);
            ParcelFileDescriptor.AutoCloseOutputStream autoCloseOutputStream1 = autoCloseOutputStream2;
            autoCloseOutputStream = autoCloseOutputStream1;
            copyStreamToWallpaperFile(paramInputStream, (FileOutputStream)autoCloseOutputStream1);
            autoCloseOutputStream = autoCloseOutputStream1;
            autoCloseOutputStream1.close();
            autoCloseOutputStream = autoCloseOutputStream1;
            wallpaperSetCompletion.waitForCompletion();
          } finally {
            IoUtils.closeQuietly((AutoCloseable)autoCloseOutputStream);
          } 
        } 
        return bundle.getInt("android.service.wallpaper.extra.ID", 0);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public boolean hasResourceWallpaper(int paramInt) {
    if (sGlobals.mService != null)
      try {
        Resources resources = this.mContext.getResources();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("res:");
        stringBuilder.append(resources.getResourceName(paramInt));
        String str = stringBuilder.toString();
        return sGlobals.mService.hasNamedWallpaper(str);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public int getDesiredMinimumWidth() {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.getWidthHint(this.mContext.getDisplayId());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public int getDesiredMinimumHeight() {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.getHeightHint(this.mContext.getDisplayId());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public void suggestDesiredDimensions(int paramInt1, int paramInt2) {
    // Byte code:
    //   0: ldc_w 'sys.max_texture_size'
    //   3: iconst_0
    //   4: invokestatic getInt : (Ljava/lang/String;I)I
    //   7: istore_3
    //   8: goto -> 20
    //   11: astore #4
    //   13: goto -> 181
    //   16: astore #4
    //   18: iconst_0
    //   19: istore_3
    //   20: iload_1
    //   21: istore #5
    //   23: iload_2
    //   24: istore #6
    //   26: iload_3
    //   27: ifle -> 93
    //   30: iload_1
    //   31: iload_3
    //   32: if_icmpgt -> 46
    //   35: iload_1
    //   36: istore #5
    //   38: iload_2
    //   39: istore #6
    //   41: iload_2
    //   42: iload_3
    //   43: if_icmple -> 93
    //   46: iload_2
    //   47: i2f
    //   48: iload_1
    //   49: i2f
    //   50: fdiv
    //   51: fstore #7
    //   53: iload_1
    //   54: iload_2
    //   55: if_icmple -> 77
    //   58: iload_3
    //   59: i2f
    //   60: fload #7
    //   62: fmul
    //   63: f2d
    //   64: ldc2_w 0.5
    //   67: dadd
    //   68: d2i
    //   69: istore #6
    //   71: iload_3
    //   72: istore #5
    //   74: goto -> 93
    //   77: iload_3
    //   78: i2f
    //   79: fload #7
    //   81: fdiv
    //   82: f2d
    //   83: ldc2_w 0.5
    //   86: dadd
    //   87: d2i
    //   88: istore #5
    //   90: iload_3
    //   91: istore #6
    //   93: getstatic android/app/WallpaperManager.sGlobals : Landroid/app/WallpaperManager$Globals;
    //   96: invokestatic access$200 : (Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
    //   99: ifnull -> 146
    //   102: getstatic android/app/WallpaperManager.sGlobals : Landroid/app/WallpaperManager$Globals;
    //   105: invokestatic access$200 : (Landroid/app/WallpaperManager$Globals;)Landroid/app/IWallpaperManager;
    //   108: astore #4
    //   110: aload_0
    //   111: getfield mContext : Landroid/content/Context;
    //   114: astore #8
    //   116: aload #8
    //   118: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   121: astore #8
    //   123: aload_0
    //   124: getfield mContext : Landroid/content/Context;
    //   127: invokevirtual getDisplayId : ()I
    //   130: istore_1
    //   131: aload #4
    //   133: iload #5
    //   135: iload #6
    //   137: aload #8
    //   139: iload_1
    //   140: invokeinterface setDimensionHints : (IILjava/lang/String;I)V
    //   145: return
    //   146: getstatic android/app/WallpaperManager.TAG : Ljava/lang/String;
    //   149: ldc_w 'WallpaperService not running'
    //   152: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   155: pop
    //   156: new java/lang/RuntimeException
    //   159: astore #4
    //   161: new android/os/DeadSystemException
    //   164: astore #8
    //   166: aload #8
    //   168: invokespecial <init> : ()V
    //   171: aload #4
    //   173: aload #8
    //   175: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   178: aload #4
    //   180: athrow
    //   181: aload #4
    //   183: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   186: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1710	-> 0
    //   #1713	-> 8
    //   #1736	-> 11
    //   #1711	-> 16
    //   #1712	-> 18
    //   #1715	-> 20
    //   #1716	-> 30
    //   #1718	-> 46
    //   #1719	-> 53
    //   #1720	-> 58
    //   #1721	-> 58
    //   #1723	-> 77
    //   #1724	-> 77
    //   #1729	-> 93
    //   #1733	-> 102
    //   #1734	-> 116
    //   #1733	-> 131
    //   #1738	-> 145
    //   #1739	-> 145
    //   #1730	-> 146
    //   #1731	-> 156
    //   #1737	-> 181
    // Exception table:
    //   from	to	target	type
    //   0	8	16	java/lang/Exception
    //   0	8	11	android/os/RemoteException
    //   93	102	11	android/os/RemoteException
    //   102	116	11	android/os/RemoteException
    //   116	131	11	android/os/RemoteException
    //   131	145	11	android/os/RemoteException
    //   146	156	11	android/os/RemoteException
    //   156	181	11	android/os/RemoteException
  }
  
  public void setDisplayPadding(Rect paramRect) {
    try {
      if (sGlobals.mService != null) {
        IWallpaperManager iWallpaperManager = sGlobals.mService;
        String str = this.mContext.getOpPackageName();
        Context context = this.mContext;
        int i = context.getDisplayId();
        iWallpaperManager.setDisplayPadding(paramRect, str, i);
        return;
      } 
      Log.w(TAG, "WallpaperService not running");
      RuntimeException runtimeException = new RuntimeException();
      DeadSystemException deadSystemException = new DeadSystemException();
      this();
      this((Throwable)deadSystemException);
      throw runtimeException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setDisplayOffset(IBinder paramIBinder, int paramInt1, int paramInt2) {
    try {
      WindowManagerGlobal.getWindowSession().setWallpaperDisplayOffset(paramIBinder, paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearWallpaper() {
    clearWallpaper(2, this.mContext.getUserId());
    clearWallpaper(1, this.mContext.getUserId());
  }
  
  @SystemApi
  public void clearWallpaper(int paramInt1, int paramInt2) {
    if (sGlobals.mService != null)
      try {
        sGlobals.mService.clearWallpaper(this.mContext.getOpPackageName(), paramInt1, paramInt2);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  @SystemApi
  public boolean setWallpaperComponent(ComponentName paramComponentName) {
    return setWallpaperComponent(paramComponentName, this.mContext.getUserId());
  }
  
  public boolean setWallpaperComponent(ComponentName paramComponentName, int paramInt) {
    if (sGlobals.mService != null)
      try {
        sGlobals.mService.setWallpaperComponentChecked(paramComponentName, this.mContext.getOpPackageName(), paramInt);
        return true;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public void setWallpaperOffsets(IBinder paramIBinder, float paramFloat1, float paramFloat2) {
    try {
      WindowManagerGlobal.getWindowSession().setWallpaperPosition(paramIBinder, paramFloat1, paramFloat2, this.mWallpaperXStep, this.mWallpaperYStep);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setWallpaperOffsetSteps(float paramFloat1, float paramFloat2) {
    this.mWallpaperXStep = paramFloat1;
    this.mWallpaperYStep = paramFloat2;
  }
  
  public void sendWallpaperCommand(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) {
    try {
      WindowManagerGlobal.getWindowSession().sendWallpaperCommand(paramIBinder, paramString, paramInt1, paramInt2, paramInt3, paramBundle, false);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setWallpaperZoomOut(IBinder paramIBinder, float paramFloat) {
    if (paramFloat >= 0.0F && paramFloat <= 1.0F)
      try {
        WindowManagerGlobal.getWindowSession().setWallpaperZoomOut(paramIBinder, paramFloat);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("zoom must be between 0 and one: ");
    stringBuilder.append(paramFloat);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean isWallpaperSupported() {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.isWallpaperSupported(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public boolean isSetWallpaperAllowed() {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.isSetWallpaperAllowed(this.mContext.getOpPackageName());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public void clearWallpaperOffsets(IBinder paramIBinder) {
    try {
      WindowManagerGlobal.getWindowSession().setWallpaperPosition(paramIBinder, -1.0F, -1.0F, -1.0F, -1.0F);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clear() throws IOException {
    setStream(openDefaultWallpaper(this.mContext, 1), null, false);
  }
  
  public void clear(int paramInt) throws IOException {
    if ((paramInt & 0x1) != 0)
      clear(); 
    if ((paramInt & 0x2) != 0)
      clearWallpaper(2, this.mContext.getUserId()); 
  }
  
  public static InputStream openDefaultWallpaper(Context paramContext, int paramInt) {
    InputStream inputStream = OplusWallpaperManagerHelper.openDefaultWallpaper(paramContext, paramInt);
    if (inputStream != null)
      return inputStream; 
    if (paramInt == 2)
      return null; 
    String str = SystemProperties.get("ro.config.wallpaper");
    if (!TextUtils.isEmpty(str)) {
      File file = new File(str);
      if (file.exists())
        try {
          return new FileInputStream(file);
        } catch (IOException iOException) {} 
    } 
    try {
      return paramContext.getResources().openRawResource(17302155);
    } catch (android.content.res.Resources.NotFoundException notFoundException) {
      return null;
    } 
  }
  
  public static ComponentName getDefaultWallpaperComponent(Context paramContext) {
    ComponentName componentName1 = null;
    String str = SystemProperties.get("ro.config.wallpaper_component");
    if (!TextUtils.isEmpty(str))
      componentName1 = ComponentName.unflattenFromString(str); 
    ComponentName componentName2 = componentName1;
    if (componentName1 == null) {
      String str1 = paramContext.getString(17040058);
      componentName2 = componentName1;
      if (!TextUtils.isEmpty(str1))
        componentName2 = ComponentName.unflattenFromString(str1); 
    } 
    componentName1 = componentName2;
    if (componentName2 != null)
      try {
        PackageManager packageManager = paramContext.getPackageManager();
        packageManager.getPackageInfo(componentName2.getPackageName(), 786432);
        componentName1 = componentName2;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        componentName1 = null;
      }  
    return componentName1;
  }
  
  public boolean setLockWallpaperCallback(IWallpaperManagerCallback paramIWallpaperManagerCallback) {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.setLockWallpaperCallback(paramIWallpaperManagerCallback);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public boolean isWallpaperBackupEligible(int paramInt) {
    if (sGlobals.mService != null)
      try {
        return sGlobals.mService.isWallpaperBackupEligible(paramInt, this.mContext.getUserId());
      } catch (RemoteException remoteException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exception querying wallpaper backup eligibility: ");
        stringBuilder.append(remoteException.getMessage());
        Log.e(str, stringBuilder.toString());
        return false;
      }  
    Log.w(TAG, "WallpaperService not running");
    throw new RuntimeException(new DeadSystemException());
  }
  
  public ColorManagementProxy getColorManagementProxy() {
    return this.mCmProxy;
  }
  
  class ColorManagementProxy {
    private final Set<ColorSpace> mSupportedColorSpaces = new HashSet<>();
    
    public ColorManagementProxy(WallpaperManager this$0) {
      Display display = this$0.getDisplayNoVerify();
      if (display != null)
        this.mSupportedColorSpaces.addAll(Arrays.asList(display.getSupportedWideColorGamut())); 
    }
    
    public Set<ColorSpace> getSupportedColorSpaces() {
      return this.mSupportedColorSpaces;
    }
    
    boolean isSupportedColorSpace(ColorSpace param1ColorSpace) {
      boolean bool;
      if (param1ColorSpace != null && (param1ColorSpace == ColorSpace.get(ColorSpace.Named.SRGB) || 
        getSupportedColorSpaces().contains(param1ColorSpace))) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    void doColorManagement(ImageDecoder param1ImageDecoder, ImageDecoder.ImageInfo param1ImageInfo) {
      if (!isSupportedColorSpace(param1ImageInfo.getColorSpace())) {
        param1ImageDecoder.setTargetColorSpace(ColorSpace.get(ColorSpace.Named.SRGB));
        String str = WallpaperManager.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Not supported color space: ");
        stringBuilder.append(param1ImageInfo.getColorSpace());
        Log.w(str, stringBuilder.toString());
      } 
    }
  }
  
  class WallpaperSetCompletion extends IWallpaperManagerCallback.Stub {
    final CountDownLatch mLatch = new CountDownLatch(1);
    
    final WallpaperManager this$0;
    
    public void waitForCompletion() {
      try {
        this.mLatch.await(30L, TimeUnit.SECONDS);
      } catch (InterruptedException interruptedException) {}
    }
    
    public void onWallpaperChanged() throws RemoteException {
      this.mLatch.countDown();
    }
    
    public void onWallpaperColorsChanged(WallpaperColors param1WallpaperColors, int param1Int1, int param1Int2) throws RemoteException {
      WallpaperManager.sGlobals.onWallpaperColorsChanged(param1WallpaperColors, param1Int1, param1Int2);
    }
  }
  
  class OnColorsChangedListener {
    public abstract void onColorsChanged(WallpaperColors param1WallpaperColors, int param1Int);
    
    public void onColorsChanged(WallpaperColors param1WallpaperColors, int param1Int1, int param1Int2) {
      onColorsChanged(param1WallpaperColors, param1Int1);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SetWallpaperFlags implements Annotation {}
}
