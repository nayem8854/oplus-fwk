package android.app.timedetector;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;

public final class TimeDetectorImpl implements TimeDetector {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "timedetector.TimeDetector";
  
  private final ITimeDetectorService mITimeDetectorService;
  
  public TimeDetectorImpl() throws ServiceManager.ServiceNotFoundException {
    IBinder iBinder = ServiceManager.getServiceOrThrow("time_detector");
    this.mITimeDetectorService = ITimeDetectorService.Stub.asInterface(iBinder);
  }
  
  public void suggestTelephonyTime(TelephonyTimeSuggestion paramTelephonyTimeSuggestion) {
    try {
      this.mITimeDetectorService.suggestTelephonyTime(paramTelephonyTimeSuggestion);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void suggestManualTime(ManualTimeSuggestion paramManualTimeSuggestion) {
    try {
      this.mITimeDetectorService.suggestManualTime(paramManualTimeSuggestion);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void suggestNetworkTime(NetworkTimeSuggestion paramNetworkTimeSuggestion) {
    try {
      this.mITimeDetectorService.suggestNetworkTime(paramNetworkTimeSuggestion);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
