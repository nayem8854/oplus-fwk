package android.app.timedetector;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.TimestampedValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class NetworkTimeSuggestion implements Parcelable {
  public static final Parcelable.Creator<NetworkTimeSuggestion> CREATOR = new Parcelable.Creator<NetworkTimeSuggestion>() {
      public NetworkTimeSuggestion createFromParcel(Parcel param1Parcel) {
        return NetworkTimeSuggestion.createFromParcel(param1Parcel);
      }
      
      public NetworkTimeSuggestion[] newArray(int param1Int) {
        return new NetworkTimeSuggestion[param1Int];
      }
    };
  
  private ArrayList<String> mDebugInfo;
  
  private final TimestampedValue<Long> mUtcTime;
  
  public NetworkTimeSuggestion(TimestampedValue<Long> paramTimestampedValue) {
    Objects.requireNonNull(paramTimestampedValue);
    this.mUtcTime = paramTimestampedValue;
    Objects.requireNonNull((Long)paramTimestampedValue.getValue());
  }
  
  private static NetworkTimeSuggestion createFromParcel(Parcel paramParcel) {
    TimestampedValue<Long> timestampedValue = (TimestampedValue)paramParcel.readParcelable(null);
    NetworkTimeSuggestion networkTimeSuggestion = new NetworkTimeSuggestion(timestampedValue);
    ArrayList<String> arrayList = paramParcel.readArrayList(null);
    networkTimeSuggestion.mDebugInfo = arrayList;
    return networkTimeSuggestion;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable((Parcelable)this.mUtcTime, 0);
    paramParcel.writeList(this.mDebugInfo);
  }
  
  public TimestampedValue<Long> getUtcTime() {
    return this.mUtcTime;
  }
  
  public List<String> getDebugInfo() {
    List<?> list = this.mDebugInfo;
    if (list == null) {
      list = Collections.emptyList();
    } else {
      list = Collections.unmodifiableList(list);
    } 
    return (List)list;
  }
  
  public void addDebugInfo(String... paramVarArgs) {
    if (this.mDebugInfo == null)
      this.mDebugInfo = new ArrayList<>(); 
    this.mDebugInfo.addAll(Arrays.asList(paramVarArgs));
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    return Objects.equals(this.mUtcTime, ((NetworkTimeSuggestion)paramObject).mUtcTime);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mUtcTime });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NetworkTimeSuggestion{mUtcTime=");
    stringBuilder.append(this.mUtcTime);
    stringBuilder.append(", mDebugInfo=");
    stringBuilder.append(this.mDebugInfo);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
