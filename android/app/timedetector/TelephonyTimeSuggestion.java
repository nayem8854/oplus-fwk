package android.app.timedetector;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.TimestampedValue;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class TelephonyTimeSuggestion implements Parcelable {
  public static final Parcelable.Creator<TelephonyTimeSuggestion> CREATOR = new Parcelable.Creator<TelephonyTimeSuggestion>() {
      public TelephonyTimeSuggestion createFromParcel(Parcel param1Parcel) {
        return TelephonyTimeSuggestion.createFromParcel(param1Parcel);
      }
      
      public TelephonyTimeSuggestion[] newArray(int param1Int) {
        return new TelephonyTimeSuggestion[param1Int];
      }
    };
  
  private ArrayList<String> mDebugInfo;
  
  private final int mSlotIndex;
  
  private final TimestampedValue<Long> mUtcTime;
  
  private TelephonyTimeSuggestion(Builder paramBuilder) {
    this.mSlotIndex = paramBuilder.mSlotIndex;
    this.mUtcTime = paramBuilder.mUtcTime;
    if (paramBuilder.mDebugInfo != null) {
      ArrayList arrayList = new ArrayList(paramBuilder.mDebugInfo);
    } else {
      paramBuilder = null;
    } 
    this.mDebugInfo = (ArrayList<String>)paramBuilder;
  }
  
  private static TelephonyTimeSuggestion createFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    Builder builder = new Builder(i);
    builder = builder.setUtcTime((TimestampedValue<Long>)paramParcel.readParcelable(null));
    TelephonyTimeSuggestion telephonyTimeSuggestion = builder.build();
    ArrayList<String> arrayList = paramParcel.readArrayList(null);
    if (arrayList != null)
      telephonyTimeSuggestion.addDebugInfo(arrayList); 
    return telephonyTimeSuggestion;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSlotIndex);
    paramParcel.writeParcelable((Parcelable)this.mUtcTime, 0);
    paramParcel.writeList(this.mDebugInfo);
  }
  
  public int getSlotIndex() {
    return this.mSlotIndex;
  }
  
  public TimestampedValue<Long> getUtcTime() {
    return this.mUtcTime;
  }
  
  public List<String> getDebugInfo() {
    List<?> list = this.mDebugInfo;
    if (list == null) {
      list = Collections.emptyList();
    } else {
      list = Collections.unmodifiableList(list);
    } 
    return (List)list;
  }
  
  public void addDebugInfo(String paramString) {
    if (this.mDebugInfo == null)
      this.mDebugInfo = new ArrayList<>(); 
    this.mDebugInfo.add(paramString);
  }
  
  public void addDebugInfo(List<String> paramList) {
    if (this.mDebugInfo == null)
      this.mDebugInfo = new ArrayList<>(paramList.size()); 
    this.mDebugInfo.addAll(paramList);
  }
  
  public boolean equals(Object<Long> paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    TelephonyTimeSuggestion telephonyTimeSuggestion = (TelephonyTimeSuggestion)paramObject;
    if (this.mSlotIndex == telephonyTimeSuggestion.mSlotIndex) {
      paramObject = (Object<Long>)this.mUtcTime;
      TimestampedValue<Long> timestampedValue = telephonyTimeSuggestion.mUtcTime;
      if (Objects.equals(paramObject, timestampedValue))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mSlotIndex), this.mUtcTime });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TelephonyTimeSuggestion{mSlotIndex='");
    stringBuilder.append(this.mSlotIndex);
    stringBuilder.append('\'');
    stringBuilder.append(", mUtcTime=");
    stringBuilder.append(this.mUtcTime);
    stringBuilder.append(", mDebugInfo=");
    stringBuilder.append(this.mDebugInfo);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  class Builder {
    private List<String> mDebugInfo;
    
    private final int mSlotIndex;
    
    private TimestampedValue<Long> mUtcTime;
    
    public Builder(TelephonyTimeSuggestion this$0) {
      this.mSlotIndex = this$0;
    }
    
    public Builder setUtcTime(TimestampedValue<Long> param1TimestampedValue) {
      if (param1TimestampedValue != null)
        Objects.requireNonNull((Long)param1TimestampedValue.getValue()); 
      this.mUtcTime = param1TimestampedValue;
      return this;
    }
    
    public Builder addDebugInfo(String param1String) {
      if (this.mDebugInfo == null)
        this.mDebugInfo = new ArrayList<>(); 
      this.mDebugInfo.add(param1String);
      return this;
    }
    
    public TelephonyTimeSuggestion build() {
      return new TelephonyTimeSuggestion(this);
    }
  }
}
