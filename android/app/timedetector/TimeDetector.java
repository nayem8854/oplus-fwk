package android.app.timedetector;

import android.os.SystemClock;
import android.os.TimestampedValue;

public interface TimeDetector {
  static ManualTimeSuggestion createManualTimeSuggestion(long paramLong, String paramString) {
    TimestampedValue<Long> timestampedValue = new TimestampedValue(SystemClock.elapsedRealtime(), Long.valueOf(paramLong));
    ManualTimeSuggestion manualTimeSuggestion = new ManualTimeSuggestion(timestampedValue);
    manualTimeSuggestion.addDebugInfo(new String[] { paramString });
    return manualTimeSuggestion;
  }
  
  void suggestManualTime(ManualTimeSuggestion paramManualTimeSuggestion);
  
  void suggestNetworkTime(NetworkTimeSuggestion paramNetworkTimeSuggestion);
  
  void suggestTelephonyTime(TelephonyTimeSuggestion paramTelephonyTimeSuggestion);
}
