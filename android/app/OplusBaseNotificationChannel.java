package android.app;

import android.os.Parcel;
import android.text.TextUtils;
import java.io.IOException;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public class OplusBaseNotificationChannel {
  private static final String ATT_BADGE_OPTION = "badge_option";
  
  private static final String ATT_CHANGEABLE_FOLD = "changeable_fold";
  
  private static final String ATT_CHANGEABLE_SHOW_ICON = "changeable_show_icon";
  
  private static final String ATT_FOLD = "fold";
  
  private static final String ATT_MAX_MESSAGES = "max_messages";
  
  private static final String ATT_OPUSH = "opush";
  
  private static final String ATT_SHOW_BANNER = "show_banner";
  
  private static final String ATT_SHOW_ICON = "show_icon";
  
  private static final String ATT_SUPPORT_NUM_BADGE = "support_num_badge";
  
  private static final String ATT_UNIMPORTANT = "unimportant";
  
  private static final int CONSTANT_HASH_CODE = 31;
  
  public static final int USER_BADGE_OPTION = 65568;
  
  public static final int USER_LOCKED_CHANGEABLE_FOLD = 65664;
  
  public static final int USER_LOCKED_CHANGEABLE_SHOW_ICON = 65792;
  
  public static final int USER_LOCKED_FOLD = 65538;
  
  public static final int USER_LOCKED_MAX_MESSAGES = 65544;
  
  public static final int USER_LOCKED_OPUSH = 65600;
  
  public static final int USER_LOCKED_SHOW_BANNER = 65537;
  
  public static final int USER_LOCKED_SHOW_ICON = 65540;
  
  public static final int USER_SUPPORT_NUM_BADGE = 65552;
  
  private int mBadgeOption;
  
  private boolean mChangeableFold;
  
  private boolean mChangeableShowIcon;
  
  private boolean mFold;
  
  private int mMaxMessages;
  
  private boolean mOpush;
  
  private boolean mShowBanner;
  
  private boolean mShowIcon;
  
  private boolean mSupportNumBadge;
  
  private boolean mUnimportant;
  
  public OplusBaseNotificationChannel(String paramString, CharSequence paramCharSequence, int paramInt) {
    boolean bool = false;
    this.mFold = false;
    this.mOpush = false;
    this.mShowBanner = false;
    this.mShowIcon = false;
    this.mMaxMessages = -1;
    this.mSupportNumBadge = false;
    this.mBadgeOption = 0;
    this.mChangeableFold = true;
    this.mChangeableShowIcon = true;
    this.mUnimportant = false;
    if (paramInt >= 4)
      bool = true; 
    this.mShowBanner = bool;
    this.mUnimportant = isUnimportantChannel(paramInt);
  }
  
  protected OplusBaseNotificationChannel(Parcel paramParcel) {
    this.mFold = false;
    this.mOpush = false;
    this.mShowBanner = false;
    this.mShowIcon = false;
    this.mMaxMessages = -1;
    this.mSupportNumBadge = false;
    this.mBadgeOption = 0;
    this.mChangeableFold = true;
    this.mChangeableShowIcon = true;
    this.mUnimportant = false;
  }
  
  protected void readFromParcel(Parcel paramParcel) {
    this.mShowBanner = paramParcel.readBoolean();
    this.mFold = paramParcel.readBoolean();
    this.mOpush = paramParcel.readBoolean();
    this.mShowIcon = paramParcel.readBoolean();
    this.mMaxMessages = paramParcel.readInt();
    this.mSupportNumBadge = paramParcel.readBoolean();
    this.mBadgeOption = paramParcel.readInt();
    this.mChangeableFold = paramParcel.readBoolean();
    this.mChangeableShowIcon = paramParcel.readBoolean();
    this.mUnimportant = paramParcel.readBoolean();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mShowBanner);
    paramParcel.writeBoolean(this.mFold);
    paramParcel.writeBoolean(this.mOpush);
    paramParcel.writeBoolean(this.mShowIcon);
    paramParcel.writeInt(this.mMaxMessages);
    paramParcel.writeBoolean(this.mSupportNumBadge);
    paramParcel.writeInt(this.mBadgeOption);
    paramParcel.writeBoolean(this.mChangeableFold);
    paramParcel.writeBoolean(this.mChangeableShowIcon);
    paramParcel.writeBoolean(this.mUnimportant);
  }
  
  public boolean canShowBanner() {
    return this.mShowBanner;
  }
  
  public void setShowBanner(boolean paramBoolean) {
    this.mShowBanner = paramBoolean;
  }
  
  public boolean isUnimportant() {
    return this.mUnimportant;
  }
  
  public void setUnimportant(boolean paramBoolean) {
    this.mUnimportant = paramBoolean;
  }
  
  public boolean isFold() {
    return this.mFold;
  }
  
  public void setFold(boolean paramBoolean) {
    this.mFold = paramBoolean;
  }
  
  public void setOpush(boolean paramBoolean) {
    this.mOpush = paramBoolean;
  }
  
  public boolean isOpush() {
    return this.mOpush;
  }
  
  public void setMaxMessage(int paramInt) {
    this.mMaxMessages = paramInt;
  }
  
  public int getMaxMessages() {
    return this.mMaxMessages;
  }
  
  public void setShowIcon(boolean paramBoolean) {
    this.mShowIcon = paramBoolean;
  }
  
  public boolean canShowIcon() {
    return this.mShowIcon;
  }
  
  public void setSupportNumBadge(boolean paramBoolean) {
    this.mSupportNumBadge = paramBoolean;
  }
  
  public boolean isSupportNumBadge() {
    return this.mSupportNumBadge;
  }
  
  public void setBadgeOption(int paramInt) {
    this.mBadgeOption = paramInt;
  }
  
  public int getBadgeOption() {
    return this.mBadgeOption;
  }
  
  public boolean isChangeableFold() {
    return this.mChangeableFold;
  }
  
  public void setChangeableFold(boolean paramBoolean) {
    this.mChangeableFold = paramBoolean;
  }
  
  public boolean isChangeableShowIcon() {
    return this.mChangeableShowIcon;
  }
  
  public void setChangeableShowIcon(boolean paramBoolean) {
    this.mChangeableShowIcon = paramBoolean;
  }
  
  protected void populateFromXml(XmlPullParser paramXmlPullParser) {
    setShowBanner(safeBool(paramXmlPullParser, "show_banner", false));
    setFold(safeBool(paramXmlPullParser, "fold", false));
    setOpush(safeBool(paramXmlPullParser, "opush", false));
    setShowIcon(safeBool(paramXmlPullParser, "show_icon", false));
    setMaxMessage(safeInt(paramXmlPullParser, "max_messages", -1));
    setSupportNumBadge(safeBool(paramXmlPullParser, "support_num_badge", false));
    setBadgeOption(safeInt(paramXmlPullParser, "badge_option", 0));
    setChangeableFold(safeBool(paramXmlPullParser, "changeable_fold", true));
    setChangeableShowIcon(safeBool(paramXmlPullParser, "changeable_show_icon", true));
    setUnimportant(safeBool(paramXmlPullParser, "unimportant", false));
  }
  
  protected void writeXml(XmlSerializer paramXmlSerializer) throws IOException {
    if (canShowBanner())
      paramXmlSerializer.attribute(null, "show_banner", Boolean.toString(canShowBanner())); 
    if (isFold())
      paramXmlSerializer.attribute(null, "fold", Boolean.toString(isFold())); 
    if (isOpush())
      paramXmlSerializer.attribute(null, "opush", Boolean.toString(isOpush())); 
    if (canShowIcon())
      paramXmlSerializer.attribute(null, "show_icon", Boolean.toString(canShowIcon())); 
    if (getMaxMessages() != -1)
      paramXmlSerializer.attribute(null, "max_messages", String.valueOf(getMaxMessages())); 
    if (isSupportNumBadge())
      paramXmlSerializer.attribute(null, "support_num_badge", Boolean.toString(isSupportNumBadge())); 
    if (getBadgeOption() != 0)
      paramXmlSerializer.attribute(null, "badge_option", String.valueOf(getBadgeOption())); 
    if (isChangeableFold())
      paramXmlSerializer.attribute(null, "changeable_fold", String.valueOf(isChangeableFold())); 
    if (isChangeableShowIcon())
      paramXmlSerializer.attribute(null, "changeable_show_icon", String.valueOf(isChangeableShowIcon())); 
    if (isUnimportant())
      paramXmlSerializer.attribute(null, "unimportant", String.valueOf(isUnimportant())); 
  }
  
  public void toJson(JSONObject paramJSONObject) throws JSONException {
    paramJSONObject.put("show_banner", canShowBanner());
    paramJSONObject.put("fold", isFold());
    paramJSONObject.put("opush", isOpush());
    paramJSONObject.put("show_icon", canShowIcon());
    paramJSONObject.put("max_messages", getMaxMessages());
    paramJSONObject.put("support_num_badge", isSupportNumBadge());
    paramJSONObject.put("badge_option", getBadgeOption());
    paramJSONObject.put("changeable_fold", isChangeableFold());
    paramJSONObject.put("changeable_show_icon", isChangeableShowIcon());
    paramJSONObject.put("unimportant", isUnimportant());
  }
  
  public boolean equals(OplusBaseNotificationChannel paramOplusBaseNotificationChannel) {
    if (this.mShowBanner != paramOplusBaseNotificationChannel.canShowBanner())
      return false; 
    if (this.mFold != paramOplusBaseNotificationChannel.isFold())
      return false; 
    if (this.mOpush != paramOplusBaseNotificationChannel.isOpush())
      return false; 
    if (this.mShowIcon != paramOplusBaseNotificationChannel.canShowIcon())
      return false; 
    if (this.mMaxMessages != paramOplusBaseNotificationChannel.getMaxMessages())
      return false; 
    if (this.mSupportNumBadge != paramOplusBaseNotificationChannel.isSupportNumBadge())
      return false; 
    if (this.mBadgeOption != paramOplusBaseNotificationChannel.getBadgeOption())
      return false; 
    if (this.mChangeableFold != paramOplusBaseNotificationChannel.isChangeableFold())
      return false; 
    if (this.mChangeableShowIcon != paramOplusBaseNotificationChannel.isChangeableShowIcon())
      return false; 
    if (this.mUnimportant != paramOplusBaseNotificationChannel.isUnimportant())
      return false; 
    return true;
  }
  
  public int hashCode(int paramInt) {
    boolean bool1 = canShowBanner();
    boolean bool2 = isFold();
    boolean bool3 = isOpush();
    boolean bool4 = canShowIcon();
    int i = getMaxMessages();
    boolean bool5 = isSupportNumBadge();
    int j = getBadgeOption();
    boolean bool6 = isChangeableFold();
    boolean bool7 = isChangeableShowIcon();
    boolean bool8 = isUnimportant();
    return (((((((((paramInt * 31 + bool1) * 31 + bool2) * 31 + bool3) * 31 + bool4) * 31 + i) * 31 + bool5) * 31 + j) * 31 + bool6) * 31 + bool7) * 31 + bool8;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(", mShowBanner=");
    stringBuilder.append(this.mShowBanner);
    stringBuilder.append(", mFold=");
    stringBuilder.append(this.mFold);
    stringBuilder.append(", mOpush=");
    stringBuilder.append(this.mOpush);
    stringBuilder.append(", mShowIcon=");
    stringBuilder.append(this.mShowIcon);
    stringBuilder.append(", mMaxMessages=");
    stringBuilder.append(this.mMaxMessages);
    stringBuilder.append(", mSupportNumBadge=");
    stringBuilder.append(this.mSupportNumBadge);
    stringBuilder.append(", mBadgeOption=");
    stringBuilder.append(this.mBadgeOption);
    stringBuilder.append(", mChangeableFold=");
    stringBuilder.append(this.mChangeableFold);
    stringBuilder.append(", mChangeableShowIcon=");
    stringBuilder.append(this.mChangeableShowIcon);
    stringBuilder.append(", mUnimportant=");
    stringBuilder.append(this.mUnimportant);
    return stringBuilder.toString();
  }
  
  private static boolean safeBool(XmlPullParser paramXmlPullParser, String paramString, boolean paramBoolean) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return paramBoolean; 
    return Boolean.parseBoolean(str);
  }
  
  private static int safeInt(XmlPullParser paramXmlPullParser, String paramString, int paramInt) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    return tryParseInt(str, paramInt);
  }
  
  private static int tryParseInt(String paramString, int paramInt) {
    if (TextUtils.isEmpty(paramString))
      return paramInt; 
    try {
      return Integer.parseInt(paramString);
    } catch (NumberFormatException numberFormatException) {
      return paramInt;
    } 
  }
  
  private boolean isUnimportantChannel(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 2)
      if (paramInt == 1) {
        bool2 = bool1;
      } else {
        bool2 = false;
      }  
    return bool2;
  }
}
