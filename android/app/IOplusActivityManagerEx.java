package android.app;

import android.content.ComponentName;

public interface IOplusActivityManagerEx {
  ComponentName getTopAppName();
}
