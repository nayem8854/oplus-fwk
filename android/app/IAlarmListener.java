package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAlarmListener extends IInterface {
  void doAlarm(IAlarmCompleteListener paramIAlarmCompleteListener) throws RemoteException;
  
  class Default implements IAlarmListener {
    public void doAlarm(IAlarmCompleteListener param1IAlarmCompleteListener) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlarmListener {
    private static final String DESCRIPTOR = "android.app.IAlarmListener";
    
    static final int TRANSACTION_doAlarm = 1;
    
    public Stub() {
      attachInterface(this, "android.app.IAlarmListener");
    }
    
    public static IAlarmListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IAlarmListener");
      if (iInterface != null && iInterface instanceof IAlarmListener)
        return (IAlarmListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "doAlarm";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.app.IAlarmListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.IAlarmListener");
      IAlarmCompleteListener iAlarmCompleteListener = IAlarmCompleteListener.Stub.asInterface(param1Parcel1.readStrongBinder());
      doAlarm(iAlarmCompleteListener);
      return true;
    }
    
    private static class Proxy implements IAlarmListener {
      public static IAlarmListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IAlarmListener";
      }
      
      public void doAlarm(IAlarmCompleteListener param2IAlarmCompleteListener) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IAlarmListener");
          if (param2IAlarmCompleteListener != null) {
            iBinder = param2IAlarmCompleteListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IAlarmListener.Stub.getDefaultImpl() != null) {
            IAlarmListener.Stub.getDefaultImpl().doAlarm(param2IAlarmCompleteListener);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlarmListener param1IAlarmListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlarmListener != null) {
          Proxy.sDefaultImpl = param1IAlarmListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlarmListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
