package android.app;

import android.os.RemoteException;

public interface IOplusStatusBarManager {
  public static final String DESCRIPTOR = "android.app.IStatusBarManager";
  
  public static final int GET_TOP_IS_FULLSCREEN = 10006;
  
  public static final int NOTIFY_CLICK_TOP = 10004;
  
  public static final int NOTIFY_MULTIWINDOW_FOCUS_CHANGED = 10010;
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int REGISTER_OPLUS_CLICK_TOP = 10003;
  
  public static final int REGISTER_OPLUS_STATUS_BAR = 10002;
  
  public static final int SET_STATUSBAR_FUNCTION = 10008;
  
  public static final int TOGGLE_SPLIT_SCREEN = 10007;
  
  public static final int TOP_IS_FULLSCREEN = 10009;
  
  public static final int UNREGISTER_OPLUS_CLICK_TOP = 10005;
  
  boolean getTopIsFullscreen() throws RemoteException;
  
  void notifyClickTop() throws RemoteException;
  
  void notifyMultiWindowFocusChanged(int paramInt) throws RemoteException;
  
  void registerOplusClickTopCallback(IOplusClickTopCallback paramIOplusClickTopCallback) throws RemoteException;
  
  void registerOplusStatusBar(IOplusStatusBar paramIOplusStatusBar) throws RemoteException;
  
  boolean setStatusBarFunction(int paramInt, String paramString) throws RemoteException;
  
  void toggleSplitScreen(int paramInt) throws RemoteException;
  
  void topIsFullscreen(boolean paramBoolean) throws RemoteException;
  
  void unregisterOplusClickTopCallback(IOplusClickTopCallback paramIOplusClickTopCallback) throws RemoteException;
}
