package android.app.admin;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface StartInstallingUpdateCallback extends IInterface {
  void onStartInstallingUpdateError(int paramInt, String paramString) throws RemoteException;
  
  class Default implements StartInstallingUpdateCallback {
    public void onStartInstallingUpdateError(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements StartInstallingUpdateCallback {
    private static final String DESCRIPTOR = "android.app.admin.StartInstallingUpdateCallback";
    
    static final int TRANSACTION_onStartInstallingUpdateError = 1;
    
    public Stub() {
      attachInterface(this, "android.app.admin.StartInstallingUpdateCallback");
    }
    
    public static StartInstallingUpdateCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.admin.StartInstallingUpdateCallback");
      if (iInterface != null && iInterface instanceof StartInstallingUpdateCallback)
        return (StartInstallingUpdateCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onStartInstallingUpdateError";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.app.admin.StartInstallingUpdateCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.admin.StartInstallingUpdateCallback");
      param1Int1 = param1Parcel1.readInt();
      String str = param1Parcel1.readString();
      onStartInstallingUpdateError(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements StartInstallingUpdateCallback {
      public static StartInstallingUpdateCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.admin.StartInstallingUpdateCallback";
      }
      
      public void onStartInstallingUpdateError(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.admin.StartInstallingUpdateCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && StartInstallingUpdateCallback.Stub.getDefaultImpl() != null) {
            StartInstallingUpdateCallback.Stub.getDefaultImpl().onStartInstallingUpdateError(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(StartInstallingUpdateCallback param1StartInstallingUpdateCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1StartInstallingUpdateCallback != null) {
          Proxy.sDefaultImpl = param1StartInstallingUpdateCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static StartInstallingUpdateCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
