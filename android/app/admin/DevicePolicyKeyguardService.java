package android.app.admin;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.Log;
import android.view.SurfaceControlViewHost;

@SystemApi
public class DevicePolicyKeyguardService extends Service {
  private final Handler mHandler = new Handler(Looper.getMainLooper());
  
  private final IKeyguardClient mClient = new IKeyguardClient.Stub() {
      final DevicePolicyKeyguardService this$0;
      
      public void onCreateKeyguardSurface(IBinder param1IBinder, IKeyguardCallback param1IKeyguardCallback) {
        DevicePolicyKeyguardService.access$002(DevicePolicyKeyguardService.this, param1IKeyguardCallback);
        DevicePolicyKeyguardService.this.mHandler.post(new _$$Lambda$DevicePolicyKeyguardService$1$fjXH0Pq_QzHSYdL9qO2BTxQE_IE(this, param1IBinder));
      }
    };
  
  private IKeyguardCallback mCallback;
  
  private static final String TAG = "DevicePolicyKeyguardService";
  
  public void onDestroy() {
    this.mHandler.removeCallbacksAndMessages(null);
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mClient.asBinder();
  }
  
  public SurfaceControlViewHost.SurfacePackage onCreateKeyguardSurface(IBinder paramIBinder) {
    return null;
  }
  
  public void dismiss() {
    IKeyguardCallback iKeyguardCallback = this.mCallback;
    if (iKeyguardCallback == null) {
      Log.w("DevicePolicyKeyguardService", "KeyguardCallback was unexpectedly null");
      return;
    } 
    try {
      iKeyguardCallback.onDismiss();
    } catch (RemoteException remoteException) {
      Log.e("DevicePolicyKeyguardService", "onDismiss failed", (Throwable)remoteException);
    } 
  }
}
