package android.app.admin;

import com.android.server.LocalServices;

public abstract class DeviceStateCache {
  public static DeviceStateCache getInstance() {
    DeviceStateCache deviceStateCache;
    DevicePolicyManagerInternal devicePolicyManagerInternal = (DevicePolicyManagerInternal)LocalServices.getService(DevicePolicyManagerInternal.class);
    if (devicePolicyManagerInternal != null) {
      deviceStateCache = devicePolicyManagerInternal.getDeviceStateCache();
    } else {
      deviceStateCache = EmptyDeviceStateCache.INSTANCE;
    } 
    return deviceStateCache;
  }
  
  public abstract boolean isDeviceProvisioned();
  
  class EmptyDeviceStateCache extends DeviceStateCache {
    private static final EmptyDeviceStateCache INSTANCE = new EmptyDeviceStateCache();
    
    public boolean isDeviceProvisioned() {
      return false;
    }
  }
}
