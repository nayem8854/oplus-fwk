package android.app.admin;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class DeviceAdminService extends Service {
  private final IDeviceAdminServiceImpl mImpl = new IDeviceAdminServiceImpl();
  
  public final IBinder onBind(Intent paramIntent) {
    return this.mImpl.asBinder();
  }
  
  private class IDeviceAdminServiceImpl extends IDeviceAdminService.Stub {
    final DeviceAdminService this$0;
    
    private IDeviceAdminServiceImpl() {}
  }
}
