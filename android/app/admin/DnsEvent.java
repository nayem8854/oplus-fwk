package android.app.admin;

import android.os.Parcel;
import android.os.Parcelable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class DnsEvent extends NetworkEvent implements Parcelable {
  public DnsEvent(String paramString1, String[] paramArrayOfString, int paramInt, String paramString2, long paramLong) {
    super(paramString2, paramLong);
    this.mHostname = paramString1;
    this.mIpAddresses = paramArrayOfString;
    this.mIpAddressesCount = paramInt;
  }
  
  private DnsEvent(Parcel paramParcel) {
    this.mHostname = paramParcel.readString();
    this.mIpAddresses = paramParcel.createStringArray();
    this.mIpAddressesCount = paramParcel.readInt();
    this.mPackageName = paramParcel.readString();
    this.mTimestamp = paramParcel.readLong();
    this.mId = paramParcel.readLong();
  }
  
  public String getHostname() {
    return this.mHostname;
  }
  
  public List<InetAddress> getInetAddresses() {
    String[] arrayOfString = this.mIpAddresses;
    if (arrayOfString == null || arrayOfString.length == 0)
      return Collections.emptyList(); 
    ArrayList<InetAddress> arrayList = new ArrayList(this.mIpAddresses.length);
    for (String str : this.mIpAddresses) {
      try {
        arrayList.add(InetAddress.getByName(str));
      } catch (UnknownHostException unknownHostException) {}
    } 
    return arrayList;
  }
  
  public int getTotalResolvedAddressCount() {
    return this.mIpAddressesCount;
  }
  
  public String toString() {
    String str2;
    long l1 = this.mId;
    String str1 = this.mHostname;
    String[] arrayOfString = this.mIpAddresses;
    if (arrayOfString == null) {
      str2 = "NONE";
    } else {
      str2 = String.join(" ", (CharSequence[])str2);
    } 
    int i = this.mIpAddressesCount;
    long l2 = this.mTimestamp;
    String str3 = this.mPackageName;
    return String.format("DnsEvent(%d, %s, %s, %d, %d, %s)", new Object[] { Long.valueOf(l1), str1, str2, Integer.valueOf(i), Long.valueOf(l2), str3 });
  }
  
  public static final Parcelable.Creator<DnsEvent> CREATOR = (Parcelable.Creator<DnsEvent>)new Object();
  
  private final String mHostname;
  
  private final String[] mIpAddresses;
  
  private final int mIpAddressesCount;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(1);
    paramParcel.writeString(this.mHostname);
    paramParcel.writeStringArray(this.mIpAddresses);
    paramParcel.writeInt(this.mIpAddressesCount);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeLong(this.mTimestamp);
    paramParcel.writeLong(this.mId);
  }
}
