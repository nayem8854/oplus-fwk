package android.app.admin;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.os.UserHandle;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class DeviceAdminReceiver extends BroadcastReceiver {
  public static final String ACTION_AFFILIATED_PROFILE_TRANSFER_OWNERSHIP_COMPLETE = "android.app.action.AFFILIATED_PROFILE_TRANSFER_OWNERSHIP_COMPLETE";
  
  public static final String ACTION_BUGREPORT_FAILED = "android.app.action.BUGREPORT_FAILED";
  
  public static final String ACTION_BUGREPORT_SHARE = "android.app.action.BUGREPORT_SHARE";
  
  public static final String ACTION_BUGREPORT_SHARING_DECLINED = "android.app.action.BUGREPORT_SHARING_DECLINED";
  
  public static final String ACTION_CHOOSE_PRIVATE_KEY_ALIAS = "android.app.action.CHOOSE_PRIVATE_KEY_ALIAS";
  
  public static final String ACTION_DEVICE_ADMIN_DISABLED = "android.app.action.DEVICE_ADMIN_DISABLED";
  
  public static final String ACTION_DEVICE_ADMIN_DISABLE_REQUESTED = "android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED";
  
  public static final String ACTION_DEVICE_ADMIN_ENABLED = "android.app.action.DEVICE_ADMIN_ENABLED";
  
  public static final String ACTION_LOCK_TASK_ENTERING = "android.app.action.LOCK_TASK_ENTERING";
  
  public static final String ACTION_LOCK_TASK_EXITING = "android.app.action.LOCK_TASK_EXITING";
  
  public static final String ACTION_NETWORK_LOGS_AVAILABLE = "android.app.action.NETWORK_LOGS_AVAILABLE";
  
  public static final String ACTION_NOTIFY_PENDING_SYSTEM_UPDATE = "android.app.action.NOTIFY_PENDING_SYSTEM_UPDATE";
  
  public static final String ACTION_PASSWORD_CHANGED = "android.app.action.ACTION_PASSWORD_CHANGED";
  
  public static final String ACTION_PASSWORD_EXPIRING = "android.app.action.ACTION_PASSWORD_EXPIRING";
  
  public static final String ACTION_PASSWORD_FAILED = "android.app.action.ACTION_PASSWORD_FAILED";
  
  public static final String ACTION_PASSWORD_SUCCEEDED = "android.app.action.ACTION_PASSWORD_SUCCEEDED";
  
  public static final String ACTION_PROFILE_PROVISIONING_COMPLETE = "android.app.action.PROFILE_PROVISIONING_COMPLETE";
  
  public static final String ACTION_SECURITY_LOGS_AVAILABLE = "android.app.action.SECURITY_LOGS_AVAILABLE";
  
  public static final String ACTION_TRANSFER_OWNERSHIP_COMPLETE = "android.app.action.TRANSFER_OWNERSHIP_COMPLETE";
  
  public static final String ACTION_USER_ADDED = "android.app.action.USER_ADDED";
  
  public static final String ACTION_USER_REMOVED = "android.app.action.USER_REMOVED";
  
  public static final String ACTION_USER_STARTED = "android.app.action.USER_STARTED";
  
  public static final String ACTION_USER_STOPPED = "android.app.action.USER_STOPPED";
  
  public static final String ACTION_USER_SWITCHED = "android.app.action.USER_SWITCHED";
  
  public static final int BUGREPORT_FAILURE_FAILED_COMPLETING = 0;
  
  public static final int BUGREPORT_FAILURE_FILE_NO_LONGER_AVAILABLE = 1;
  
  public static final String DEVICE_ADMIN_META_DATA = "android.app.device_admin";
  
  public static final String EXTRA_BUGREPORT_FAILURE_REASON = "android.app.extra.BUGREPORT_FAILURE_REASON";
  
  public static final String EXTRA_BUGREPORT_HASH = "android.app.extra.BUGREPORT_HASH";
  
  public static final String EXTRA_CHOOSE_PRIVATE_KEY_ALIAS = "android.app.extra.CHOOSE_PRIVATE_KEY_ALIAS";
  
  public static final String EXTRA_CHOOSE_PRIVATE_KEY_RESPONSE = "android.app.extra.CHOOSE_PRIVATE_KEY_RESPONSE";
  
  public static final String EXTRA_CHOOSE_PRIVATE_KEY_SENDER_UID = "android.app.extra.CHOOSE_PRIVATE_KEY_SENDER_UID";
  
  public static final String EXTRA_CHOOSE_PRIVATE_KEY_URI = "android.app.extra.CHOOSE_PRIVATE_KEY_URI";
  
  public static final String EXTRA_DISABLE_WARNING = "android.app.extra.DISABLE_WARNING";
  
  public static final String EXTRA_LOCK_TASK_PACKAGE = "android.app.extra.LOCK_TASK_PACKAGE";
  
  public static final String EXTRA_NETWORK_LOGS_COUNT = "android.app.extra.EXTRA_NETWORK_LOGS_COUNT";
  
  public static final String EXTRA_NETWORK_LOGS_TOKEN = "android.app.extra.EXTRA_NETWORK_LOGS_TOKEN";
  
  public static final String EXTRA_SYSTEM_UPDATE_RECEIVED_TIME = "android.app.extra.SYSTEM_UPDATE_RECEIVED_TIME";
  
  public static final String EXTRA_TRANSFER_OWNERSHIP_ADMIN_EXTRAS_BUNDLE = "android.app.extra.TRANSFER_OWNERSHIP_ADMIN_EXTRAS_BUNDLE";
  
  private static String TAG = "DevicePolicy";
  
  private static boolean localLOGV = false;
  
  private DevicePolicyManager mManager;
  
  private ComponentName mWho;
  
  public DevicePolicyManager getManager(Context paramContext) {
    DevicePolicyManager devicePolicyManager2 = this.mManager;
    if (devicePolicyManager2 != null)
      return devicePolicyManager2; 
    DevicePolicyManager devicePolicyManager1 = (DevicePolicyManager)paramContext.getSystemService("device_policy");
    return devicePolicyManager1;
  }
  
  public ComponentName getWho(Context paramContext) {
    ComponentName componentName2 = this.mWho;
    if (componentName2 != null)
      return componentName2; 
    ComponentName componentName1 = new ComponentName(paramContext, getClass());
    return componentName1;
  }
  
  public void onEnabled(Context paramContext, Intent paramIntent) {}
  
  public CharSequence onDisableRequested(Context paramContext, Intent paramIntent) {
    return null;
  }
  
  public void onDisabled(Context paramContext, Intent paramIntent) {}
  
  @Deprecated
  public void onPasswordChanged(Context paramContext, Intent paramIntent) {}
  
  public void onPasswordChanged(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {
    onPasswordChanged(paramContext, paramIntent);
  }
  
  @Deprecated
  public void onPasswordFailed(Context paramContext, Intent paramIntent) {}
  
  public void onPasswordFailed(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {
    onPasswordFailed(paramContext, paramIntent);
  }
  
  @Deprecated
  public void onPasswordSucceeded(Context paramContext, Intent paramIntent) {}
  
  public void onPasswordSucceeded(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {
    onPasswordSucceeded(paramContext, paramIntent);
  }
  
  @Deprecated
  public void onPasswordExpiring(Context paramContext, Intent paramIntent) {}
  
  public void onPasswordExpiring(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {
    onPasswordExpiring(paramContext, paramIntent);
  }
  
  public void onProfileProvisioningComplete(Context paramContext, Intent paramIntent) {}
  
  @Deprecated
  public void onReadyForUserInitialization(Context paramContext, Intent paramIntent) {}
  
  public void onLockTaskModeEntering(Context paramContext, Intent paramIntent, String paramString) {}
  
  public void onLockTaskModeExiting(Context paramContext, Intent paramIntent) {}
  
  public String onChoosePrivateKeyAlias(Context paramContext, Intent paramIntent, int paramInt, Uri paramUri, String paramString) {
    return null;
  }
  
  public void onSystemUpdatePending(Context paramContext, Intent paramIntent, long paramLong) {}
  
  public void onBugreportSharingDeclined(Context paramContext, Intent paramIntent) {}
  
  public void onBugreportShared(Context paramContext, Intent paramIntent, String paramString) {}
  
  public void onBugreportFailed(Context paramContext, Intent paramIntent, int paramInt) {}
  
  public void onSecurityLogsAvailable(Context paramContext, Intent paramIntent) {}
  
  public void onNetworkLogsAvailable(Context paramContext, Intent paramIntent, long paramLong, int paramInt) {}
  
  public void onUserAdded(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {}
  
  public void onUserRemoved(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {}
  
  public void onUserStarted(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {}
  
  public void onUserStopped(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {}
  
  public void onUserSwitched(Context paramContext, Intent paramIntent, UserHandle paramUserHandle) {}
  
  public void onTransferOwnershipComplete(Context paramContext, PersistableBundle paramPersistableBundle) {}
  
  public void onTransferAffiliatedProfileOwnershipComplete(Context paramContext, UserHandle paramUserHandle) {}
  
  public void onReceive(Context paramContext, Intent paramIntent) {
    String str = paramIntent.getAction();
    if ("android.app.action.ACTION_PASSWORD_CHANGED".equals(str)) {
      onPasswordChanged(paramContext, paramIntent, paramIntent.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
    } else if ("android.app.action.ACTION_PASSWORD_FAILED".equals(str)) {
      onPasswordFailed(paramContext, paramIntent, paramIntent.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
    } else if ("android.app.action.ACTION_PASSWORD_SUCCEEDED".equals(str)) {
      onPasswordSucceeded(paramContext, paramIntent, paramIntent.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
    } else if ("android.app.action.DEVICE_ADMIN_ENABLED".equals(str)) {
      onEnabled(paramContext, paramIntent);
    } else {
      CharSequence charSequence;
      Bundle bundle;
      if ("android.app.action.DEVICE_ADMIN_DISABLE_REQUESTED".equals(str)) {
        charSequence = onDisableRequested(paramContext, paramIntent);
        if (charSequence != null) {
          bundle = getResultExtras(true);
          bundle.putCharSequence("android.app.extra.DISABLE_WARNING", charSequence);
        } 
      } else if ("android.app.action.DEVICE_ADMIN_DISABLED".equals(str)) {
        onDisabled((Context)charSequence, (Intent)bundle);
      } else if ("android.app.action.ACTION_PASSWORD_EXPIRING".equals(str)) {
        onPasswordExpiring((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else if ("android.app.action.PROFILE_PROVISIONING_COMPLETE".equals(str)) {
        onProfileProvisioningComplete((Context)charSequence, (Intent)bundle);
      } else if ("android.app.action.CHOOSE_PRIVATE_KEY_ALIAS".equals(str)) {
        int i = bundle.getIntExtra("android.app.extra.CHOOSE_PRIVATE_KEY_SENDER_UID", -1);
        Uri uri = bundle.<Uri>getParcelableExtra("android.app.extra.CHOOSE_PRIVATE_KEY_URI");
        str = bundle.getStringExtra("android.app.extra.CHOOSE_PRIVATE_KEY_ALIAS");
        charSequence = onChoosePrivateKeyAlias((Context)charSequence, (Intent)bundle, i, uri, str);
        setResultData((String)charSequence);
      } else if ("android.app.action.LOCK_TASK_ENTERING".equals(str)) {
        str = bundle.getStringExtra("android.app.extra.LOCK_TASK_PACKAGE");
        onLockTaskModeEntering((Context)charSequence, (Intent)bundle, str);
      } else if ("android.app.action.LOCK_TASK_EXITING".equals(str)) {
        onLockTaskModeExiting((Context)charSequence, (Intent)bundle);
      } else if ("android.app.action.NOTIFY_PENDING_SYSTEM_UPDATE".equals(str)) {
        long l = bundle.getLongExtra("android.app.extra.SYSTEM_UPDATE_RECEIVED_TIME", -1L);
        onSystemUpdatePending((Context)charSequence, (Intent)bundle, l);
      } else if ("android.app.action.BUGREPORT_SHARING_DECLINED".equals(str)) {
        onBugreportSharingDeclined((Context)charSequence, (Intent)bundle);
      } else if ("android.app.action.BUGREPORT_SHARE".equals(str)) {
        str = bundle.getStringExtra("android.app.extra.BUGREPORT_HASH");
        onBugreportShared((Context)charSequence, (Intent)bundle, str);
      } else if ("android.app.action.BUGREPORT_FAILED".equals(str)) {
        int i = bundle.getIntExtra("android.app.extra.BUGREPORT_FAILURE_REASON", 0);
        onBugreportFailed((Context)charSequence, (Intent)bundle, i);
      } else if ("android.app.action.SECURITY_LOGS_AVAILABLE".equals(str)) {
        onSecurityLogsAvailable((Context)charSequence, (Intent)bundle);
      } else if ("android.app.action.NETWORK_LOGS_AVAILABLE".equals(str)) {
        long l = bundle.getLongExtra("android.app.extra.EXTRA_NETWORK_LOGS_TOKEN", -1L);
        int i = bundle.getIntExtra("android.app.extra.EXTRA_NETWORK_LOGS_COUNT", 0);
        onNetworkLogsAvailable((Context)charSequence, (Intent)bundle, l, i);
      } else if ("android.app.action.USER_ADDED".equals(str)) {
        onUserAdded((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else if ("android.app.action.USER_REMOVED".equals(str)) {
        onUserRemoved((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else if ("android.app.action.USER_STARTED".equals(str)) {
        onUserStarted((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else if ("android.app.action.USER_STOPPED".equals(str)) {
        onUserStopped((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else if ("android.app.action.USER_SWITCHED".equals(str)) {
        onUserSwitched((Context)charSequence, (Intent)bundle, bundle.<UserHandle>getParcelableExtra("android.intent.extra.USER"));
      } else {
        PersistableBundle persistableBundle;
        if ("android.app.action.TRANSFER_OWNERSHIP_COMPLETE".equals(str)) {
          persistableBundle = bundle.<PersistableBundle>getParcelableExtra("android.app.extra.TRANSFER_OWNERSHIP_ADMIN_EXTRAS_BUNDLE");
          onTransferOwnershipComplete((Context)charSequence, persistableBundle);
        } else if ("android.app.action.AFFILIATED_PROFILE_TRANSFER_OWNERSHIP_COMPLETE".equals(str)) {
          UserHandle userHandle = persistableBundle.<UserHandle>getParcelableExtra("android.intent.extra.USER");
          onTransferAffiliatedProfileOwnershipComplete((Context)charSequence, userHandle);
        } 
      } 
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class BugreportFailureCode implements Annotation {}
}
