package android.app.admin;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.android.internal.widget.LockscreenCredential;
import com.android.internal.widget.PasswordValidationError;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class PasswordMetrics implements Parcelable {
  public int length = 0;
  
  public int letters = 0;
  
  public int upperCase = 0;
  
  public int lowerCase = 0;
  
  public int numeric = 0;
  
  public int symbols = 0;
  
  public int nonLetter = 0;
  
  public int nonNumeric = 0;
  
  public int seqLength = Integer.MAX_VALUE;
  
  public PasswordMetrics(int paramInt) {
    this.credType = paramInt;
  }
  
  public PasswordMetrics(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, int paramInt10) {
    this.credType = paramInt1;
    this.length = paramInt2;
    this.letters = paramInt3;
    this.upperCase = paramInt4;
    this.lowerCase = paramInt5;
    this.numeric = paramInt6;
    this.symbols = paramInt7;
    this.nonLetter = paramInt8;
    this.nonNumeric = paramInt9;
    this.seqLength = paramInt10;
  }
  
  private PasswordMetrics(PasswordMetrics paramPasswordMetrics) {
    this(paramPasswordMetrics.credType, paramPasswordMetrics.length, paramPasswordMetrics.letters, paramPasswordMetrics.upperCase, paramPasswordMetrics.lowerCase, paramPasswordMetrics.numeric, paramPasswordMetrics.symbols, paramPasswordMetrics.nonLetter, paramPasswordMetrics.nonNumeric, paramPasswordMetrics.seqLength);
  }
  
  public static int sanitizeComplexityLevel(int paramInt) {
    if (paramInt != 0 && paramInt != 65536 && paramInt != 196608 && paramInt != 327680) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid password complexity used: ");
      stringBuilder.append(paramInt);
      Log.w("PasswordMetrics", stringBuilder.toString());
      return 0;
    } 
    return paramInt;
  }
  
  private static boolean hasInvalidCharacters(byte[] paramArrayOfbyte) {
    int i;
    byte b;
    for (i = paramArrayOfbyte.length, b = 0; b < i; ) {
      byte b1 = paramArrayOfbyte[b];
      char c = (char)b1;
      if (c < ' ' || c > '')
        return true; 
      b++;
    } 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.credType);
    paramParcel.writeInt(this.length);
    paramParcel.writeInt(this.letters);
    paramParcel.writeInt(this.upperCase);
    paramParcel.writeInt(this.lowerCase);
    paramParcel.writeInt(this.numeric);
    paramParcel.writeInt(this.symbols);
    paramParcel.writeInt(this.nonLetter);
    paramParcel.writeInt(this.nonNumeric);
    paramParcel.writeInt(this.seqLength);
  }
  
  public static final Parcelable.Creator<PasswordMetrics> CREATOR = new Parcelable.Creator<PasswordMetrics>() {
      public PasswordMetrics createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        int n = param1Parcel.readInt();
        int i1 = param1Parcel.readInt();
        int i2 = param1Parcel.readInt();
        int i3 = param1Parcel.readInt();
        int i4 = param1Parcel.readInt();
        int i5 = param1Parcel.readInt();
        return new PasswordMetrics(i, j, k, m, n, i1, i2, i3, i4, i5);
      }
      
      public PasswordMetrics[] newArray(int param1Int) {
        return new PasswordMetrics[param1Int];
      }
    };
  
  private static final int CHAR_DIGIT = 2;
  
  private static final int CHAR_LOWER_CASE = 0;
  
  private static final int CHAR_SYMBOL = 3;
  
  private static final int CHAR_UPPER_CASE = 1;
  
  public static final int MAX_ALLOWED_SEQUENCE = 3;
  
  private static final String TAG = "PasswordMetrics";
  
  public int credType;
  
  public static PasswordMetrics computeForCredential(LockscreenCredential paramLockscreenCredential) {
    if (paramLockscreenCredential.isPassword() || paramLockscreenCredential.isPin())
      return computeForPassword(paramLockscreenCredential.getCredential()); 
    if (paramLockscreenCredential.isPattern())
      return new PasswordMetrics(1); 
    if (paramLockscreenCredential.isNone())
      return new PasswordMetrics(-1); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown credential type ");
    stringBuilder.append(paramLockscreenCredential.getType());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public static PasswordMetrics computeForPassword(byte[] paramArrayOfbyte) {
    byte b1 = 0;
    byte b2 = 0;
    byte b3 = 0;
    byte b4 = 0;
    byte b5 = 0;
    byte b6 = 0;
    byte b7 = 0;
    int i = paramArrayOfbyte.length;
    int k;
    for (int j = paramArrayOfbyte.length; k < j; ) {
      byte b = paramArrayOfbyte[k];
      int m = categoryChar((char)b);
      if (m != 0) {
        if (m != 1) {
          if (m != 2) {
            if (m == 3) {
              b5++;
              b6++;
              b7++;
            } 
          } else {
            b4++;
            b6++;
          } 
        } else {
          b1++;
          b2++;
          b7++;
        } 
      } else {
        b1++;
        b3++;
        b7++;
      } 
      k++;
    } 
    k = maxLengthSequence(paramArrayOfbyte);
    return new PasswordMetrics(4, i, b1, b2, b3, b4, b5, b6, b7, k);
  }
  
  public static int maxLengthSequence(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte.length == 0)
      return 0; 
    char c = (char)paramArrayOfbyte[0];
    int i = categoryChar(c);
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    int i1, i2;
    for (i1 = 1, i2 = c; i1 < paramArrayOfbyte.length; i1++, k = n, n = i5) {
      int i5;
      c = (char)paramArrayOfbyte[i1];
      int i3 = categoryChar(c);
      int i4 = c - i2;
      if (i3 != i || Math.abs(i4) > maxDiffCategory(i)) {
        m = Math.max(m, i1 - n);
        i5 = i1;
        n = 0;
        i = i3;
      } else {
        i2 = m;
        i5 = n;
        if (k) {
          i2 = m;
          i5 = n;
          if (i4 != j) {
            i2 = Math.max(m, i1 - n);
            i5 = i1 - 1;
          } 
        } 
        j = i4;
        n = 1;
        m = i2;
      } 
      i2 = c;
    } 
    i1 = Math.max(m, paramArrayOfbyte.length - n);
    return i1;
  }
  
  private static int categoryChar(char paramChar) {
    if ('a' <= paramChar && paramChar <= 'z')
      return 0; 
    if ('A' <= paramChar && paramChar <= 'Z')
      return 1; 
    if ('0' <= paramChar && paramChar <= '9')
      return 2; 
    return 3;
  }
  
  private static int maxDiffCategory(int paramInt) {
    if (paramInt != 0 && paramInt != 1) {
      if (paramInt != 2)
        return 0; 
      return 10;
    } 
    return 1;
  }
  
  public static PasswordMetrics merge(List<PasswordMetrics> paramList) {
    PasswordMetrics passwordMetrics = new PasswordMetrics(-1);
    for (PasswordMetrics passwordMetrics1 : paramList)
      passwordMetrics.maxWith(passwordMetrics1); 
    return passwordMetrics;
  }
  
  public void maxWith(PasswordMetrics paramPasswordMetrics) {
    int i = Math.max(this.credType, paramPasswordMetrics.credType);
    if (i != 4)
      return; 
    this.length = Math.max(this.length, paramPasswordMetrics.length);
    this.letters = Math.max(this.letters, paramPasswordMetrics.letters);
    this.upperCase = Math.max(this.upperCase, paramPasswordMetrics.upperCase);
    this.lowerCase = Math.max(this.lowerCase, paramPasswordMetrics.lowerCase);
    this.numeric = Math.max(this.numeric, paramPasswordMetrics.numeric);
    this.symbols = Math.max(this.symbols, paramPasswordMetrics.symbols);
    this.nonLetter = Math.max(this.nonLetter, paramPasswordMetrics.nonLetter);
    this.nonNumeric = Math.max(this.nonNumeric, paramPasswordMetrics.nonNumeric);
    this.seqLength = Math.min(this.seqLength, paramPasswordMetrics.seqLength);
  }
  
  public static int complexityLevelToMinQuality(int paramInt) {
    if (paramInt != 65536) {
      if (paramInt != 196608 && paramInt != 327680)
        return 0; 
      return 196608;
    } 
    return 65536;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CharacterCatagory implements Annotation {}
  
  class ComplexityBucket extends Enum<ComplexityBucket> {
    private static final ComplexityBucket[] $VALUES;
    
    public static ComplexityBucket valueOf(String param1String) {
      return Enum.<ComplexityBucket>valueOf(ComplexityBucket.class, param1String);
    }
    
    public static ComplexityBucket[] values() {
      return (ComplexityBucket[])$VALUES.clone();
    }
    
    public static final ComplexityBucket BUCKET_HIGH = new null("BUCKET_HIGH", 0, 327680);
    
    public static final ComplexityBucket BUCKET_LOW = new null("BUCKET_LOW", 2, 65536);
    
    public static final ComplexityBucket BUCKET_MEDIUM = new null("BUCKET_MEDIUM", 1, 196608);
    
    public static final ComplexityBucket BUCKET_NONE;
    
    int mComplexityLevel;
    
    static {
      null  = new null("BUCKET_NONE", 3, 0);
      $VALUES = new ComplexityBucket[] { BUCKET_HIGH, BUCKET_MEDIUM, BUCKET_LOW,  };
    }
    
    private ComplexityBucket(PasswordMetrics this$0, int param1Int1, int param1Int2) {
      super((String)this$0, param1Int1);
      this.mComplexityLevel = param1Int2;
    }
    
    static ComplexityBucket forComplexity(int param1Int) {
      for (ComplexityBucket complexityBucket : values()) {
        if (complexityBucket.mComplexityLevel == param1Int)
          return complexityBucket; 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid complexity level: ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    abstract boolean allowsCredType(int param1Int);
    
    abstract boolean allowsNumericPassword();
    
    abstract boolean canHaveSequence();
    
    abstract int getMinimumLength(boolean param1Boolean);
  }
  
  enum null {
    boolean canHaveSequence() {
      return false;
    }
    
    int getMinimumLength(boolean param1Boolean) {
      byte b;
      if (param1Boolean) {
        b = 6;
      } else {
        b = 8;
      } 
      return b;
    }
    
    boolean allowsNumericPassword() {
      return false;
    }
    
    boolean allowsCredType(int param1Int) {
      boolean bool;
      if (param1Int == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  enum null {
    boolean canHaveSequence() {
      return false;
    }
    
    int getMinimumLength(boolean param1Boolean) {
      return 4;
    }
    
    boolean allowsNumericPassword() {
      return false;
    }
    
    boolean allowsCredType(int param1Int) {
      boolean bool;
      if (param1Int == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  enum null {
    boolean canHaveSequence() {
      return true;
    }
    
    int getMinimumLength(boolean param1Boolean) {
      return 0;
    }
    
    boolean allowsNumericPassword() {
      return true;
    }
    
    boolean allowsCredType(int param1Int) {
      boolean bool;
      if (param1Int != -1) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  enum null {
    boolean canHaveSequence() {
      return true;
    }
    
    int getMinimumLength(boolean param1Boolean) {
      return 0;
    }
    
    boolean allowsNumericPassword() {
      return true;
    }
    
    boolean allowsCredType(int param1Int) {
      return true;
    }
  }
  
  private boolean satisfiesBucket(ComplexityBucket paramComplexityBucket) {
    null = paramComplexityBucket.allowsCredType(this.credType);
    boolean bool = false;
    if (!null)
      return false; 
    if (this.credType != 4)
      return true; 
    if (paramComplexityBucket.canHaveSequence() || this.seqLength <= 3) {
      int i = this.length;
      if (this.nonNumeric > 0) {
        null = true;
      } else {
        null = false;
      } 
      if (i >= paramComplexityBucket.getMinimumLength(null))
        return true; 
    } 
    return bool;
  }
  
  public int determineComplexity() {
    for (ComplexityBucket complexityBucket : ComplexityBucket.values()) {
      if (satisfiesBucket(complexityBucket))
        return complexityBucket.mComplexityLevel; 
    } 
    throw new IllegalStateException("Failed to figure out complexity for a given metrics");
  }
  
  public static List<PasswordValidationError> validatePassword(PasswordMetrics paramPasswordMetrics, int paramInt, boolean paramBoolean, byte[] paramArrayOfbyte) {
    if (hasInvalidCharacters(paramArrayOfbyte))
      return Collections.singletonList(new PasswordValidationError(2, 0)); 
    PasswordMetrics passwordMetrics = computeForPassword(paramArrayOfbyte);
    return validatePasswordMetrics(paramPasswordMetrics, paramInt, paramBoolean, passwordMetrics);
  }
  
  public static List<PasswordValidationError> validatePasswordMetrics(PasswordMetrics paramPasswordMetrics1, int paramInt, boolean paramBoolean, PasswordMetrics paramPasswordMetrics2) {
    ComplexityBucket complexityBucket = ComplexityBucket.forComplexity(paramInt);
    paramInt = paramPasswordMetrics2.credType;
    if (paramInt < paramPasswordMetrics1.credType || 
      !complexityBucket.allowsCredType(paramInt))
      return Collections.singletonList(new PasswordValidationError(1, 0)); 
    if (paramPasswordMetrics2.credType != 4)
      return Collections.emptyList(); 
    if (paramBoolean && paramPasswordMetrics2.nonNumeric > 0)
      return Collections.singletonList(new PasswordValidationError(2, 0)); 
    ArrayList<PasswordValidationError> arrayList = new ArrayList();
    if (paramPasswordMetrics2.length > 16)
      arrayList.add(new PasswordValidationError(4, 16)); 
    paramPasswordMetrics1 = applyComplexity(paramPasswordMetrics1, paramBoolean, complexityBucket);
    paramInt = paramPasswordMetrics1.length;
    paramInt = Math.max(paramInt, 4);
    paramPasswordMetrics1.length = Math.min(16, paramInt);
    paramPasswordMetrics1.removeOverlapping();
    comparePasswordMetrics(paramPasswordMetrics1, paramPasswordMetrics2, arrayList);
    return arrayList;
  }
  
  private static void comparePasswordMetrics(PasswordMetrics paramPasswordMetrics1, PasswordMetrics paramPasswordMetrics2, ArrayList<PasswordValidationError> paramArrayList) {
    int i = paramPasswordMetrics2.length, j = paramPasswordMetrics1.length;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(3, j)); 
    i = paramPasswordMetrics2.letters;
    j = paramPasswordMetrics1.letters;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(6, j)); 
    j = paramPasswordMetrics2.upperCase;
    i = paramPasswordMetrics1.upperCase;
    if (j < i)
      paramArrayList.add(new PasswordValidationError(7, i)); 
    i = paramPasswordMetrics2.lowerCase;
    j = paramPasswordMetrics1.lowerCase;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(8, j)); 
    i = paramPasswordMetrics2.numeric;
    j = paramPasswordMetrics1.numeric;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(9, j)); 
    i = paramPasswordMetrics2.symbols;
    j = paramPasswordMetrics1.symbols;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(10, j)); 
    i = paramPasswordMetrics2.nonLetter;
    j = paramPasswordMetrics1.nonLetter;
    if (i < j)
      paramArrayList.add(new PasswordValidationError(11, j)); 
    j = paramPasswordMetrics2.nonNumeric;
    i = paramPasswordMetrics1.nonNumeric;
    if (j < i)
      paramArrayList.add(new PasswordValidationError(12, i)); 
    if (paramPasswordMetrics2.seqLength > paramPasswordMetrics1.seqLength)
      paramArrayList.add(new PasswordValidationError(5, 0)); 
  }
  
  private void removeOverlapping() {
    int i = this.upperCase + this.lowerCase;
    int j = this.numeric + this.symbols;
    int k = Math.max(this.letters, i);
    int m = this.symbols + k;
    int n = Math.max(this.nonLetter, j);
    int i1 = Math.max(this.nonNumeric, m);
    n = Math.max(k + n, this.numeric + i1);
    if (i >= this.letters)
      this.letters = 0; 
    if (j >= this.nonLetter)
      this.nonLetter = 0; 
    if (m >= this.nonNumeric)
      this.nonNumeric = 0; 
    if (n >= this.length)
      this.length = 0; 
  }
  
  public static PasswordMetrics applyComplexity(PasswordMetrics paramPasswordMetrics, boolean paramBoolean, int paramInt) {
    return applyComplexity(paramPasswordMetrics, paramBoolean, ComplexityBucket.forComplexity(paramInt));
  }
  
  private static PasswordMetrics applyComplexity(PasswordMetrics paramPasswordMetrics, boolean paramBoolean, ComplexityBucket paramComplexityBucket) {
    paramPasswordMetrics = new PasswordMetrics(paramPasswordMetrics);
    if (!paramComplexityBucket.canHaveSequence())
      paramPasswordMetrics.seqLength = Math.min(paramPasswordMetrics.seqLength, 3); 
    paramPasswordMetrics.length = Math.max(paramPasswordMetrics.length, paramComplexityBucket.getMinimumLength(paramBoolean ^ true));
    if (!paramBoolean && !paramComplexityBucket.allowsNumericPassword())
      paramPasswordMetrics.nonNumeric = Math.max(paramPasswordMetrics.nonNumeric, 1); 
    return paramPasswordMetrics;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.credType != ((PasswordMetrics)paramObject).credType || this.length != ((PasswordMetrics)paramObject).length || this.letters != ((PasswordMetrics)paramObject).letters || this.upperCase != ((PasswordMetrics)paramObject).upperCase || this.lowerCase != ((PasswordMetrics)paramObject).lowerCase || this.numeric != ((PasswordMetrics)paramObject).numeric || this.symbols != ((PasswordMetrics)paramObject).symbols || this.nonLetter != ((PasswordMetrics)paramObject).nonLetter || this.nonNumeric != ((PasswordMetrics)paramObject).nonNumeric || this.seqLength != ((PasswordMetrics)paramObject).seqLength)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = this.credType, j = this.length, k = this.letters, m = this.upperCase, n = this.lowerCase, i1 = this.numeric, i2 = this.symbols, i3 = this.nonLetter;
    int i4 = this.nonNumeric, i5 = this.seqLength;
    return Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Integer.valueOf(m), Integer.valueOf(n), Integer.valueOf(i1), Integer.valueOf(i2), Integer.valueOf(i3), Integer.valueOf(i4), Integer.valueOf(i5) });
  }
}
