package android.app.admin;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

public class DelegatedAdminReceiver extends BroadcastReceiver {
  private static final String TAG = "DelegatedAdminReceiver";
  
  public String onChoosePrivateKeyAlias(Context paramContext, Intent paramIntent, int paramInt, Uri paramUri, String paramString) {
    throw new UnsupportedOperationException("onChoosePrivateKeyAlias should be implemented");
  }
  
  public void onNetworkLogsAvailable(Context paramContext, Intent paramIntent, long paramLong, int paramInt) {
    throw new UnsupportedOperationException("onNetworkLogsAvailable should be implemented");
  }
  
  public final void onReceive(Context paramContext, Intent paramIntent) {
    String str1;
    Uri uri;
    String str2 = paramIntent.getAction();
    if ("android.app.action.CHOOSE_PRIVATE_KEY_ALIAS".equals(str2)) {
      int i = paramIntent.getIntExtra("android.app.extra.CHOOSE_PRIVATE_KEY_SENDER_UID", -1);
      uri = paramIntent.<Uri>getParcelableExtra("android.app.extra.CHOOSE_PRIVATE_KEY_URI");
      String str = paramIntent.getStringExtra("android.app.extra.CHOOSE_PRIVATE_KEY_ALIAS");
      str1 = onChoosePrivateKeyAlias(paramContext, paramIntent, i, uri, str);
      setResultData(str1);
    } else if ("android.app.action.NETWORK_LOGS_AVAILABLE".equals(uri)) {
      long l = paramIntent.getLongExtra("android.app.extra.EXTRA_NETWORK_LOGS_TOKEN", -1L);
      int i = paramIntent.getIntExtra("android.app.extra.EXTRA_NETWORK_LOGS_COUNT", 0);
      onNetworkLogsAvailable((Context)str1, paramIntent, l, i);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unhandled broadcast: ");
      stringBuilder.append((String)uri);
      Log.w("DelegatedAdminReceiver", stringBuilder.toString());
    } 
  }
}
