package android.app.admin;

import android.os.Parcel;
import android.os.Parcelable;
import java.net.InetAddress;
import java.net.UnknownHostException;

public final class ConnectEvent extends NetworkEvent implements Parcelable {
  public ConnectEvent(String paramString1, int paramInt, String paramString2, long paramLong) {
    super(paramString2, paramLong);
    this.mIpAddress = paramString1;
    this.mPort = paramInt;
  }
  
  private ConnectEvent(Parcel paramParcel) {
    this.mIpAddress = paramParcel.readString();
    this.mPort = paramParcel.readInt();
    this.mPackageName = paramParcel.readString();
    this.mTimestamp = paramParcel.readLong();
    this.mId = paramParcel.readLong();
  }
  
  public InetAddress getInetAddress() {
    try {
      return InetAddress.getByName(this.mIpAddress);
    } catch (UnknownHostException unknownHostException) {
      return InetAddress.getLoopbackAddress();
    } 
  }
  
  public int getPort() {
    return this.mPort;
  }
  
  public String toString() {
    return String.format("ConnectEvent(%d, %s, %d, %d, %s)", new Object[] { Long.valueOf(this.mId), this.mIpAddress, Integer.valueOf(this.mPort), Long.valueOf(this.mTimestamp), this.mPackageName });
  }
  
  public static final Parcelable.Creator<ConnectEvent> CREATOR = (Parcelable.Creator<ConnectEvent>)new Object();
  
  private final String mIpAddress;
  
  private final int mPort;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(2);
    paramParcel.writeString(this.mIpAddress);
    paramParcel.writeInt(this.mPort);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeLong(this.mTimestamp);
    paramParcel.writeLong(this.mId);
  }
}
