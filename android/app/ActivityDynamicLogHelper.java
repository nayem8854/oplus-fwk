package android.app;

import android.os.SystemProperties;

class ActivityDynamicLogHelper {
  static void enableDynamicalLogIfNeed() {
    boolean bool1 = "eng".equals(SystemProperties.get("ro.build.type"));
    String str = SystemProperties.get("sys.activity.thread.log");
    boolean bool2 = bool1;
    if (str != null)
      bool2 = bool1 | str.equals("true"); 
    setDynamicalLogEnable(bool2);
  }
  
  static void setDynamicalLogEnable(boolean paramBoolean) {
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.localLOGV, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_BROADCAST, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_SERVICE, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_MESSAGES, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_MEMORY_TRIM, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_BROADCAST_LIGHT, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_CONFIGURATION, paramBoolean);
    OplusMirrorActivityThread.setBooleanValue(OplusMirrorActivityThread.DEBUG_PROVIDER, paramBoolean);
    OplusMirrorTransactionExecutor.setBooleanValue(OplusMirrorTransactionExecutor.DEBUG_RESOLVER, paramBoolean);
    OplusMirrorInsetsController.setBooleanValue(OplusMirrorInsetsController.DEBUG, paramBoolean);
  }
}
