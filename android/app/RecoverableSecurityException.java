package android.app;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class RecoverableSecurityException extends SecurityException implements Parcelable {
  public RecoverableSecurityException(Parcel paramParcel) {
    this(securityException, charSequence, remoteAction);
  }
  
  public RecoverableSecurityException(Throwable paramThrowable, CharSequence paramCharSequence, RemoteAction paramRemoteAction) {
    super(paramThrowable.getMessage());
    Objects.requireNonNull(paramCharSequence);
    this.mUserMessage = paramCharSequence;
    Objects.requireNonNull(paramRemoteAction);
    this.mUserAction = paramRemoteAction;
  }
  
  public CharSequence getUserMessage() {
    return this.mUserMessage;
  }
  
  public RemoteAction getUserAction() {
    return this.mUserAction;
  }
  
  public void showAsNotification(Context paramContext, String paramString) {
    NotificationManager notificationManager = paramContext.<NotificationManager>getSystemService(NotificationManager.class);
    Notification.Builder builder2 = new Notification.Builder(paramContext, paramString);
    Notification.Builder builder3 = builder2.setSmallIcon(17302800);
    RemoteAction remoteAction2 = this.mUserAction;
    builder3 = builder3.setContentTitle(remoteAction2.getTitle());
    CharSequence charSequence = this.mUserMessage;
    builder3 = builder3.setContentText(charSequence);
    RemoteAction remoteAction1 = this.mUserAction;
    Notification.Builder builder1 = builder3.setContentIntent(remoteAction1.getActionIntent());
    builder1 = builder1.setCategory("err");
    notificationManager.notify("RecoverableSecurityException", this.mUserAction.getActionIntent().getCreatorUid(), builder1.build());
  }
  
  public void showAsDialog(Activity paramActivity) {
    LocalDialog localDialog = new LocalDialog();
    Bundle bundle = new Bundle();
    bundle.putParcelable("RecoverableSecurityException", this);
    localDialog.setArguments(bundle);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RecoverableSecurityException_");
    stringBuilder.append(this.mUserAction.getActionIntent().getCreatorUid());
    String str = stringBuilder.toString();
    FragmentManager fragmentManager = paramActivity.getFragmentManager();
    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
    Fragment fragment = fragmentManager.findFragmentByTag(str);
    if (fragment != null)
      fragmentTransaction.remove(fragment); 
    fragmentTransaction.add(localDialog, str);
    fragmentTransaction.commitAllowingStateLoss();
  }
  
  class LocalDialog extends DialogFragment {
    public Dialog onCreateDialog(Bundle param1Bundle) {
      RecoverableSecurityException recoverableSecurityException = (RecoverableSecurityException)getArguments().getParcelable("RecoverableSecurityException");
      AlertDialog.Builder builder2 = new AlertDialog.Builder((Context)getActivity());
      builder2 = builder2.setMessage(recoverableSecurityException.mUserMessage);
      AlertDialog.Builder builder1 = builder2.setPositiveButton(recoverableSecurityException.mUserAction.getTitle(), new _$$Lambda$RecoverableSecurityException$LocalDialog$r8YNkpjWIZllJsQ_8eA0q51FU5Q(recoverableSecurityException));
      builder1 = builder1.setNegativeButton(17039360, (DialogInterface.OnClickListener)null);
      return builder1.create();
    }
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(getMessage());
    paramParcel.writeCharSequence(this.mUserMessage);
    this.mUserAction.writeToParcel(paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<RecoverableSecurityException> CREATOR = new Parcelable.Creator<RecoverableSecurityException>() {
      public RecoverableSecurityException createFromParcel(Parcel param1Parcel) {
        return new RecoverableSecurityException(param1Parcel);
      }
      
      public RecoverableSecurityException[] newArray(int param1Int) {
        return new RecoverableSecurityException[param1Int];
      }
    };
  
  private static final String TAG = "RecoverableSecurityException";
  
  private final RemoteAction mUserAction;
  
  private final CharSequence mUserMessage;
}
