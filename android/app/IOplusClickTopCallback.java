package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusClickTopCallback extends IInterface {
  void onClickTopCallback() throws RemoteException;
  
  class Default implements IOplusClickTopCallback {
    public void onClickTopCallback() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusClickTopCallback {
    private static final String DESCRIPTOR = "android.app.IOplusClickTopCallback";
    
    static final int TRANSACTION_onClickTopCallback = 1;
    
    public Stub() {
      attachInterface(this, "android.app.IOplusClickTopCallback");
    }
    
    public static IOplusClickTopCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IOplusClickTopCallback");
      if (iInterface != null && iInterface instanceof IOplusClickTopCallback)
        return (IOplusClickTopCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onClickTopCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.app.IOplusClickTopCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.IOplusClickTopCallback");
      onClickTopCallback();
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusClickTopCallback {
      public static IOplusClickTopCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IOplusClickTopCallback";
      }
      
      public void onClickTopCallback() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusClickTopCallback");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusClickTopCallback.Stub.getDefaultImpl() != null) {
            IOplusClickTopCallback.Stub.getDefaultImpl().onClickTopCallback();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusClickTopCallback param1IOplusClickTopCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusClickTopCallback != null) {
          Proxy.sDefaultImpl = param1IOplusClickTopCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusClickTopCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
