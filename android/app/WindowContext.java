package android.app;

import android.content.Context;
import android.content.ContextWrapper;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.IWindowManager;
import android.view.WindowManagerGlobal;
import android.view.WindowManagerImpl;
import java.lang.ref.Reference;

public class WindowContext extends ContextWrapper {
  private boolean mOwnsToken;
  
  private final WindowTokenClient mToken;
  
  private final WindowManagerImpl mWindowManager;
  
  private final IWindowManager mWms;
  
  public WindowContext(Context paramContext, int paramInt, Bundle paramBundle) {
    super(null);
    this.mWms = WindowManagerGlobal.getWindowManagerService();
    WindowTokenClient windowTokenClient = new WindowTokenClient();
    paramContext = createBaseWindowContext(paramContext, (IBinder)windowTokenClient);
    attachBaseContext(paramContext);
    paramContext.setOuterContext(this);
    this.mToken.attachContext(this);
    WindowManagerImpl windowManagerImpl = new WindowManagerImpl(this);
    windowManagerImpl.setDefaultToken((IBinder)this.mToken);
    boolean bool = false;
    try {
      IWindowManager iWindowManager = this.mWms;
      WindowTokenClient windowTokenClient1 = this.mToken;
      int i = getDisplayId();
      String str = getPackageName();
      paramInt = iWindowManager.addWindowTokenWithOptions((IBinder)windowTokenClient1, paramInt, i, paramBundle, str);
      if (paramInt != -12) {
        if (paramInt == 0)
          bool = true; 
        this.mOwnsToken = bool;
        Reference.reachabilityFence(this);
        return;
      } 
      throw new UnsupportedOperationException("createWindowContext failed! Too many unused window contexts. Please see Context#createWindowContext documentation for detail.");
    } catch (RemoteException remoteException) {
      this.mOwnsToken = false;
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static ContextImpl createBaseWindowContext(Context paramContext, IBinder paramIBinder) {
    paramContext = ContextImpl.getImpl(paramContext);
    return paramContext.createBaseWindowContext(paramIBinder);
  }
  
  public Object getSystemService(String paramString) {
    if ("window".equals(paramString))
      return this.mWindowManager; 
    return super.getSystemService(paramString);
  }
  
  protected void finalize() throws Throwable {
    release();
    super.finalize();
  }
  
  public void release() {
    if (this.mOwnsToken)
      try {
        this.mWms.removeWindowToken((IBinder)this.mToken, getDisplayId());
        this.mOwnsToken = false;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    destroy();
  }
  
  void destroy() {
    ContextImpl contextImpl = (ContextImpl)getBaseContext();
    contextImpl.scheduleFinalCleanup(getClass().getName(), "WindowContext");
    Reference.reachabilityFence(this);
  }
}
