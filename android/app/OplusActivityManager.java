package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.GraphicBuffer;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IOplusKeyEventObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import android.util.Slog;
import android.view.Surface;
import com.oplus.app.IOplusAppStartController;
import com.oplus.app.IOplusAppSwitchObserver;
import com.oplus.app.IOplusFreeformConfigChangedListener;
import com.oplus.app.IOplusGameSpaceController;
import com.oplus.app.IOplusHansListener;
import com.oplus.app.IOplusPermissionRecordController;
import com.oplus.app.IOplusSplitScreenObserver;
import com.oplus.app.IOplusZoomWindowConfigChangedListener;
import com.oplus.app.OplusAppInfo;
import com.oplus.app.OplusAppSwitchConfig;
import com.oplus.confinemode.IOplusConfineModeObserver;
import com.oplus.darkmode.OplusDarkModeData;
import com.oplus.datasync.ISysStateChangeCallback;
import com.oplus.eap.IOplusEapDataCallback;
import com.oplus.favorite.IOplusFavoriteQueryCallback;
import com.oplus.globaldrag.OplusGlobalDragAndDropRUSConfig;
import com.oplus.lockscreen.IOplusLockScreenCallback;
import com.oplus.miragewindow.IOplusMirageSessionCallback;
import com.oplus.miragewindow.IOplusMirageWindowObserver;
import com.oplus.miragewindow.IOplusMirageWindowSession;
import com.oplus.miragewindow.OplusMirageWindowInfo;
import com.oplus.multiapp.OplusMultiAppConfig;
import com.oplus.util.OplusAccidentallyTouchData;
import com.oplus.util.OplusDisplayCompatData;
import com.oplus.util.OplusDisplayOptimizationData;
import com.oplus.util.OplusFontVariationAdaptionData;
import com.oplus.util.OplusPackageFreezeData;
import com.oplus.util.OplusProcDependData;
import com.oplus.util.OplusReflectData;
import com.oplus.util.OplusResolveData;
import com.oplus.util.OplusSecureKeyboardData;
import com.oplus.zoomwindow.IOplusZoomAppObserver;
import com.oplus.zoomwindow.IOplusZoomWindowObserver;
import com.oplus.zoomwindow.OplusZoomControlViewInfo;
import com.oplus.zoomwindow.OplusZoomInputEventInfo;
import com.oplus.zoomwindow.OplusZoomWindowInfo;
import com.oplus.zoomwindow.OplusZoomWindowRUSConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusActivityManager extends OplusBaseActivityManager implements IOplusActivityManager, IOplusActivityTaskManager, IOplusDirectActivityManager {
  private static final String TAG = "OplusActivityManager";
  
  private final OplusActivityTaskManager mOplusAtm = new OplusActivityTaskManager();
  
  public void swapDockedFullscreenStack() throws RemoteException {
    this.mOplusAtm.swapDockedFullscreenStack();
  }
  
  public void setSecureController(IActivityController paramIActivityController) throws RemoteException {
    this.mOplusAtm.setSecureController(paramIActivityController);
  }
  
  public ComponentName getTopActivityComponentName() throws RemoteException {
    return this.mOplusAtm.getTopActivityComponentName();
  }
  
  public ApplicationInfo getTopApplicationInfo() throws RemoteException {
    return this.mOplusAtm.getTopApplicationInfo();
  }
  
  public List<OplusAppInfo> getAllTopAppInfos() throws RemoteException {
    return this.mOplusAtm.getAllTopAppInfos();
  }
  
  public List<String> getFreeformConfigList(int paramInt) throws RemoteException {
    return this.mOplusAtm.getFreeformConfigList(paramInt);
  }
  
  public boolean isFreeformEnabled() throws RemoteException {
    return this.mOplusAtm.isFreeformEnabled();
  }
  
  public boolean addFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException {
    return this.mOplusAtm.addFreeformConfigChangedListener(paramIOplusFreeformConfigChangedListener);
  }
  
  public boolean removeFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException {
    return this.mOplusAtm.removeFreeformConfigChangedListener(paramIOplusFreeformConfigChangedListener);
  }
  
  public boolean registerAppSwitchObserver(String paramString, IOplusAppSwitchObserver paramIOplusAppSwitchObserver, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException {
    return this.mOplusAtm.registerAppSwitchObserver(paramString, paramIOplusAppSwitchObserver, paramOplusAppSwitchConfig);
  }
  
  public boolean unregisterAppSwitchObserver(String paramString, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException {
    return this.mOplusAtm.unregisterAppSwitchObserver(paramString, paramOplusAppSwitchConfig);
  }
  
  public int getSplitScreenState(Intent paramIntent) throws RemoteException {
    return this.mOplusAtm.getSplitScreenState(paramIntent);
  }
  
  public ComponentName getDockTopAppName() {
    return null;
  }
  
  public List<String> getAllTopPkgName() {
    return null;
  }
  
  public ApplicationInfo getFreeFormAppInfo() {
    return null;
  }
  
  public final int startActivityForFreeform(Intent paramIntent, Bundle paramBundle, int paramInt, String paramString) {
    return -1;
  }
  
  public final void exitOplusosFreeform(Bundle paramBundle) {}
  
  public int startZoomWindow(Intent paramIntent, Bundle paramBundle, int paramInt, String paramString) throws RemoteException {
    return this.mOplusAtm.startZoomWindow(paramIntent, paramBundle, paramInt, paramString);
  }
  
  public boolean registerZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException {
    return this.mOplusAtm.registerZoomWindowObserver(paramIOplusZoomWindowObserver);
  }
  
  public boolean unregisterZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException {
    return this.mOplusAtm.unregisterZoomWindowObserver(paramIOplusZoomWindowObserver);
  }
  
  public boolean registerZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException {
    return this.mOplusAtm.registerZoomAppObserver(paramIOplusZoomAppObserver);
  }
  
  public boolean unregisterZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException {
    return this.mOplusAtm.unregisterZoomAppObserver(paramIOplusZoomAppObserver);
  }
  
  public OplusZoomWindowInfo getCurrentZoomWindowState() throws RemoteException {
    return this.mOplusAtm.getCurrentZoomWindowState();
  }
  
  public void hideZoomWindow(int paramInt) throws RemoteException {
    this.mOplusAtm.hideZoomWindow(paramInt);
  }
  
  public List<String> getZoomAppConfigList(int paramInt) throws RemoteException {
    return this.mOplusAtm.getZoomAppConfigList(paramInt);
  }
  
  public void onInputEvent(OplusZoomInputEventInfo paramOplusZoomInputEventInfo) throws RemoteException {
    this.mOplusAtm.onInputEvent(paramOplusZoomInputEventInfo);
  }
  
  public void onControlViewChanged(OplusZoomControlViewInfo paramOplusZoomControlViewInfo) throws RemoteException {
    this.mOplusAtm.onControlViewChanged(paramOplusZoomControlViewInfo);
  }
  
  public boolean isSupportZoomWindowMode() throws RemoteException {
    return this.mOplusAtm.isSupportZoomWindowMode();
  }
  
  public boolean isSupportZoomMode(String paramString1, int paramInt, String paramString2, Bundle paramBundle) throws RemoteException {
    return this.mOplusAtm.isSupportZoomMode(paramString1, paramInt, paramString2, paramBundle);
  }
  
  public boolean handleShowCompatibilityToast(String paramString1, int paramInt1, String paramString2, Bundle paramBundle, int paramInt2) throws RemoteException {
    return this.mOplusAtm.handleShowCompatibilityToast(paramString1, paramInt1, paramString2, paramBundle, paramInt2);
  }
  
  public OplusZoomWindowRUSConfig getZoomWindowConfig() throws RemoteException {
    return this.mOplusAtm.getZoomWindowConfig();
  }
  
  public void setZoomWindowConfig(OplusZoomWindowRUSConfig paramOplusZoomWindowRUSConfig) throws RemoteException {
    this.mOplusAtm.setZoomWindowConfig(paramOplusZoomWindowRUSConfig);
  }
  
  public boolean addZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException {
    return this.mOplusAtm.addZoomWindowConfigChangedListener(paramIOplusZoomWindowConfigChangedListener);
  }
  
  public boolean removeZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException {
    return this.mOplusAtm.removeZoomWindowConfigChangedListener(paramIOplusZoomWindowConfigChangedListener);
  }
  
  class ITaskStackListenerWrapper {
    public abstract void onActivityPinned(String param1String, int param1Int1, int param1Int2, int param1Int3);
    
    public abstract void onActivityUnpinned();
    
    public abstract void onTaskSnapshotChanged(int param1Int, OplusActivityManager.TaskSnapshotWrapper param1TaskSnapshotWrapper);
  }
  
  class TaskSnapshotWrapper {
    ActivityManager.TaskSnapshot mTaskSnapshot;
    
    public TaskSnapshotWrapper(OplusActivityManager this$0) {
      this.mTaskSnapshot = (ActivityManager.TaskSnapshot)this$0;
    }
    
    public void destroy() {
      try {
        if (this.mTaskSnapshot != null) {
          GraphicBuffer graphicBuffer = this.mTaskSnapshot.getSnapshot();
          if (graphicBuffer != null)
            graphicBuffer.destroy(); 
        } 
      } catch (Exception exception) {
        System.gc();
      } 
    }
    
    public Bitmap getSnapshotBitmap() {
      return Bitmap.wrapHardwareBuffer(this.mTaskSnapshot.getSnapshot(), this.mTaskSnapshot.getColorSpace());
    }
  }
  
  public static void registerTaskStackListener(ITaskStackListenerWrapper paramITaskStackListenerWrapper) {
    Object object = new Object(paramITaskStackListenerWrapper);
    try {
      ActivityTaskManager.getService().registerTaskStackListener((ITaskStackListener)object);
    } catch (RemoteException remoteException) {
      Log.w("OplusActivityManager", "registerTaskStackListener failed.");
    } 
  }
  
  public static List<ActivityManager.RunningTaskInfo> getFilteredTasks(int paramInt, boolean paramBoolean) {
    List<ActivityManager.RunningTaskInfo> list = null;
    try {
      List<ActivityManager.RunningTaskInfo> list1 = ActivityTaskManager.getService().getFilteredTasks(paramInt, paramBoolean);
    } catch (RemoteException remoteException) {
      Log.w("OplusActivityManager", "getFilteredTasks failed.");
    } 
    return list;
  }
  
  public boolean writeEdgeTouchPreventParam(String paramString1, String paramString2, List<String> paramList) throws RemoteException {
    return this.mOplusAtm.writeEdgeTouchPreventParam(paramString1, paramString2, paramList);
  }
  
  public void setDefaultEdgeTouchPreventParam(String paramString, List<String> paramList) throws RemoteException {
    this.mOplusAtm.setDefaultEdgeTouchPreventParam(paramString, paramList);
  }
  
  public boolean resetDefaultEdgeTouchPreventParam(String paramString) throws RemoteException {
    return this.mOplusAtm.resetDefaultEdgeTouchPreventParam(paramString);
  }
  
  public boolean isSupportEdgeTouchPrevent() throws RemoteException {
    return this.mOplusAtm.isSupportEdgeTouchPrevent();
  }
  
  public void setEdgeTouchCallRules(String paramString, Map<String, List<String>> paramMap) throws RemoteException {
    this.mOplusAtm.setEdgeTouchCallRules(paramString, paramMap);
  }
  
  public int splitScreenForEdgePanel(Intent paramIntent, int paramInt) throws RemoteException {
    return this.mOplusAtm.splitScreenForEdgePanel(paramIntent, paramInt);
  }
  
  public boolean isAppCallRefuseMode() throws RemoteException {
    int i = getConfineMode();
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public void setAppCallRefuseMode(boolean paramBoolean) throws RemoteException {
    setConfineMode(1, paramBoolean);
  }
  
  public void setChildSpaceMode(boolean paramBoolean) throws RemoteException {
    setConfineMode(2, paramBoolean);
  }
  
  public void setAllowLaunchApps(List<String> paramList) throws RemoteException {
    setPermitList(2, 5, paramList, false);
    setPermitList(2, 5, paramList, true);
  }
  
  public void setConfineMode(int paramInt, boolean paramBoolean) throws RemoteException {
    this.mOplusAtm.setConfineMode(paramInt, paramBoolean);
  }
  
  public int getConfineMode() throws RemoteException {
    return this.mOplusAtm.getConfineMode();
  }
  
  public void setPermitList(int paramInt1, int paramInt2, List<String> paramList, boolean paramBoolean) throws RemoteException {
    this.mOplusAtm.setPermitList(paramInt1, paramInt2, paramList, paramBoolean);
  }
  
  public OplusFontVariationAdaptionData getFontVariationAdaption() throws RemoteException {
    return getFontVariationAdaptionData();
  }
  
  public void setGimbalLaunchPkg(String paramString) throws RemoteException {
    this.mOplusAtm.setGimbalLaunchPkg(paramString);
  }
  
  public void setPackagesState(Map<String, Integer> paramMap) throws RemoteException {
    this.mOplusAtm.setPackagesState(paramMap);
  }
  
  public boolean registerLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) throws RemoteException {
    return this.mOplusAtm.registerLockScreenCallback(paramIOplusLockScreenCallback);
  }
  
  public boolean unregisterLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) throws RemoteException {
    return this.mOplusAtm.unregisterLockScreenCallback(paramIOplusLockScreenCallback);
  }
  
  public void notifySplitScreenStateChanged(String paramString, Bundle paramBundle, boolean paramBoolean) throws RemoteException {
    this.mOplusAtm.notifySplitScreenStateChanged(paramString, paramBundle, paramBoolean);
  }
  
  public boolean setSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    return this.mOplusAtm.setSplitScreenObserver(paramIOplusSplitScreenObserver);
  }
  
  public boolean isInSplitScreenMode() throws RemoteException {
    return this.mOplusAtm.isInSplitScreenMode();
  }
  
  public boolean dismissSplitScreenMode(int paramInt) throws RemoteException {
    return this.mOplusAtm.dismissSplitScreenMode(paramInt);
  }
  
  public boolean registerSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    return this.mOplusAtm.registerSplitScreenObserver(paramInt, paramIOplusSplitScreenObserver);
  }
  
  public boolean unregisterSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    return this.mOplusAtm.unregisterSplitScreenObserver(paramInt, paramIOplusSplitScreenObserver);
  }
  
  public Bundle getSplitScreenStatus(String paramString) throws RemoteException {
    return this.mOplusAtm.getSplitScreenStatus(paramString);
  }
  
  public boolean splitScreenForTopApp(int paramInt) throws RemoteException {
    return this.mOplusAtm.splitScreenForTopApp(paramInt);
  }
  
  public boolean splitScreenForRecentTasks(int paramInt) throws RemoteException {
    return this.mOplusAtm.splitScreenForRecentTasks(paramInt);
  }
  
  public boolean registerKeyEventObserver(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, int paramInt) throws RemoteException {
    return this.mOplusAtm.registerKeyEventObserver(paramString, paramIOplusKeyEventObserver, paramInt);
  }
  
  public boolean unregisterKeyEventObserver(String paramString) throws RemoteException {
    return this.mOplusAtm.unregisterKeyEventObserver(paramString);
  }
  
  public boolean registerKeyEventInterceptor(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, Map<Integer, Integer> paramMap) throws RemoteException {
    return this.mOplusAtm.registerKeyEventInterceptor(paramString, paramIOplusKeyEventObserver, paramMap);
  }
  
  public boolean unregisterKeyEventInterceptor(String paramString) throws RemoteException {
    return this.mOplusAtm.unregisterKeyEventInterceptor(paramString);
  }
  
  public boolean updateAppData(String paramString, Bundle paramBundle) throws RemoteException {
    return this.mOplusAtm.updateAppData(paramString, paramBundle);
  }
  
  public boolean registerSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) throws RemoteException {
    return this.mOplusAtm.registerSysStateChangeObserver(paramString, paramISysStateChangeCallback);
  }
  
  public boolean unregisterSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) throws RemoteException {
    return this.mOplusAtm.unregisterSysStateChangeObserver(paramString, paramISysStateChangeCallback);
  }
  
  public OplusGlobalDragAndDropRUSConfig getGlobalDragAndDropConfig() throws RemoteException {
    return this.mOplusAtm.getGlobalDragAndDropConfig();
  }
  
  public void setGlobalDragAndDropConfig(OplusGlobalDragAndDropRUSConfig paramOplusGlobalDragAndDropRUSConfig) throws RemoteException {
    this.mOplusAtm.setGlobalDragAndDropConfig(paramOplusGlobalDragAndDropRUSConfig);
  }
  
  public boolean registerMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager registerMirageWindowObserver");
    return this.mOplusAtm.registerMirageWindowObserver(paramIOplusMirageWindowObserver);
  }
  
  public boolean unregisterMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException {
    return this.mOplusAtm.unregisterMirageWindowObserver(paramIOplusMirageWindowObserver);
  }
  
  public int createMirageDisplay(Surface paramSurface) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager createMirageDisplay");
    return this.mOplusAtm.createMirageDisplay(paramSurface);
  }
  
  public void startMirageWindowMode(ComponentName paramComponentName, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager startMirageWindowMode");
    this.mOplusAtm.startMirageWindowMode(paramComponentName, paramInt1, paramInt2, paramBundle);
  }
  
  public boolean isMirageWindowShow() throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager isMirageWindowShow");
    return this.mOplusAtm.isMirageWindowShow();
  }
  
  public void stopMirageWindowMode() throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager stopMirageWindowMode");
    this.mOplusAtm.stopMirageWindowMode();
  }
  
  public void expandToFullScreen() throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager expandTofullscreen");
    this.mOplusAtm.expandToFullScreen();
  }
  
  public void setMirageWindowSilent(String paramString) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager setMirageWindowSilent");
    this.mOplusAtm.setMirageWindowSilent(paramString);
  }
  
  public boolean isSupportMirageWindowMode() throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager isSupportMirageWindowMode");
    return this.mOplusAtm.isSupportMirageWindowMode();
  }
  
  public OplusMirageWindowInfo getMirageWindowInfo() throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager getMirageWindowInfo");
    return this.mOplusAtm.getMirageWindowInfo();
  }
  
  public boolean updateMirageWindowCastFlag(int paramInt, Bundle paramBundle) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager updateMirageWindowCastFlag");
    return this.mOplusAtm.updateMirageWindowCastFlag(paramInt, paramBundle);
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager updatePrivacyProtectionList");
    return this.mOplusAtm.updatePrivacyProtectionList(paramList, paramBoolean);
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager default updatePrivacyProtectionList");
    return this.mOplusAtm.updatePrivacyProtectionList(paramList, paramBoolean1, paramBoolean2, paramBundle);
  }
  
  public IOplusMirageWindowSession createMirageWindowSession(IOplusMirageSessionCallback paramIOplusMirageSessionCallback) throws RemoteException {
    Slog.i("MirageDisplayWindow", "OplusActivityManager createMirageWindowSession");
    return this.mOplusAtm.createMirageWindowSession(paramIOplusMirageSessionCallback);
  }
  
  public boolean registerConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException {
    return this.mOplusAtm.registerConfineModeObserver(paramIOplusConfineModeObserver);
  }
  
  public boolean unregisterConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException {
    return this.mOplusAtm.unregisterConfineModeObserver(paramIOplusConfineModeObserver);
  }
  
  public String readNodeFile(int paramInt) throws RemoteException {
    return this.mOplusAtm.readNodeFile(paramInt);
  }
  
  public boolean writeNodeFile(int paramInt, String paramString) throws RemoteException {
    return this.mOplusAtm.writeNodeFile(paramInt, paramString);
  }
  
  public void updatePermissionChoice(String paramString1, String paramString2, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setPermissionInterceptEnable(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mRemote.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isPermissionInterceptEnabled() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setSystemProperties(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void killPidForce(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10008, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void grantOplusPermissionByGroup(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10012, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void revokeOplusPermissionByGroup(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10013, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void handleAppForNotification(String paramString, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10014, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusAccidentallyTouchData getAccidentallyTouchData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusAccidentallyTouchData)OplusAccidentallyTouchData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setGameSpaceController(IOplusGameSpaceController paramIOplusGameSpaceController) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusGameSpaceController != null) {
        IBinder iBinder = paramIOplusGameSpaceController.asBinder();
      } else {
        paramIOplusGameSpaceController = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusGameSpaceController);
      this.mRemote.transact(10016, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public ArrayList<String> getGlobalPkgWhiteList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10019, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public ArrayList<String> getGlobalProcessWhiteList() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10020, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addStageProtectInfo(String paramString1, String paramString2, long paramLong) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeLong(paramLong);
      this.mRemote.transact(10021, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeStageProtectInfo(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10022, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusDisplayOptimizationData getDisplayOptimizationData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10023, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusDisplayOptimizationData)OplusDisplayOptimizationData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusSecureKeyboardData getSecureKeyboardData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10024, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusSecureKeyboardData)OplusSecureKeyboardData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public ArrayList<String> getStageProtectListFromPkg(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10025, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void handleAppFromControlCenter(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10026, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusDisplayCompatData getDisplayCompatData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10027, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusDisplayCompatData)OplusDisplayCompatData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean getIsSupportMultiApp() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10094, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getMultiAppList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10091, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(null);
      return null;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getMultiAppAlias(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10090, parcel1, parcel2, 0);
      parcel2.readException();
      paramString = parcel2.readString();
      return paramString;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int setMultiAppConfig(OplusMultiAppConfig paramOplusMultiAppConfig) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeParcelable((Parcelable)paramOplusMultiAppConfig, 0);
      this.mRemote.transact(10087, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusMultiAppConfig getMultiAppConfig() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10088, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusMultiAppConfig)parcel2.readParcelable(OplusMultiAppConfig.class.getClassLoader());
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int setMultiAppAlias(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10089, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int setMultiAppStatus(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10092, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getMultiAppMaxCreateNum() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10093, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void scanFileIfNeed(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mRemote.transact(10095, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addMiniProgramShare(String paramString1, String paramString2, String paramString3) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeString(paramString3);
      this.mRemote.transact(10031, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeMiniProgramShare(String paramString1, String paramString2, String paramString3) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeString(paramString3);
      this.mRemote.transact(10032, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusResolveData getResolveData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10034, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusResolveData)OplusResolveData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusReflectData getReflectData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10037, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusReflectData)OplusReflectData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addFastAppWechatPay(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10038, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeFastAppWechatPay(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10039, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<OplusPackageFreezeData> getRunningProcesses() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10086, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusPackageFreezeData.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setAppStartMonitorController(IOplusAppStartController paramIOplusAppStartController) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusAppStartController != null) {
        IBinder iBinder = paramIOplusAppStartController.asBinder();
      } else {
        paramIOplusAppStartController = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusAppStartController);
      this.mRemote.transact(10043, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addFastAppThirdLogin(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10046, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void removeFastAppThirdLogin(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10047, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void favoriteQueryRule(String paramString, IOplusFavoriteQueryCallback paramIOplusFavoriteQueryCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      if (paramIOplusFavoriteQueryCallback != null) {
        IBinder iBinder = paramIOplusFavoriteQueryCallback.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10048, parcel1, parcel2, 1);
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addBackgroundRestrictedInfo(String paramString, List<String> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeStringList(paramList);
      this.mRemote.transact(10049, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setPreventIndulgeController(IOplusAppStartController paramIOplusAppStartController) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusAppStartController != null) {
        IBinder iBinder = paramIOplusAppStartController.asBinder();
      } else {
        paramIOplusAppStartController = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusAppStartController);
      this.mRemote.transact(10050, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void addPreventIndulgeList(List<String> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeStringList(paramList);
      this.mRemote.transact(10051, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean putConfigInfo(String paramString, Bundle paramBundle, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeBundle(paramBundle);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10062, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public Bundle getConfigInfo(String paramString, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new Bundle();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10063, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBundle();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public float updateCpuTracker(long paramLong) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeLong(paramLong);
      this.mRemote.transact(10060, parcel1, parcel2, 0);
      parcel2.readException();
      return Float.valueOf(parcel2.readFloat()).floatValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getCpuWorkingStats() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10061, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void forceTrimAppMemory(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10067, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setPermissionRecordController(IOplusPermissionRecordController paramIOplusPermissionRecordController) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusPermissionRecordController != null) {
        IBinder iBinder = paramIOplusPermissionRecordController.asBinder();
      } else {
        paramIOplusPermissionRecordController = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusPermissionRecordController);
      this.mRemote.transact(10066, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isNightMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10068, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusDarkModeData getDarkModeData(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10077, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusDarkModeData)parcel2.readParcelable(OplusDarkModeData.class.getClassLoader());
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean dumpProcPerfData(Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeBundle(paramBundle);
      this.mRemote.transact(10069, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getProcCommonInfoList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10070, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(null);
      return null;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<OplusProcDependData> getProcDependency(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10071, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusProcDependData.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<OplusProcDependData> getProcDependency(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10072, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusProcDependData.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getTaskPkgList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10073, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(null);
      return null;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void syncPermissionRecord() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10074, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void updateUidCpuTracker() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10075, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getUidCpuWorkingStats() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10076, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getProcCmdline(int[] paramArrayOfint) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    ArrayList<String> arrayList = new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramArrayOfint.length);
      parcel1.writeIntArray(paramArrayOfint);
      this.mRemote.transact(10079, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void activeGc(int[] paramArrayOfint) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramArrayOfint != null) {
        parcel1.writeInt(paramArrayOfint.length);
        parcel1.writeIntArray(paramArrayOfint);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10080, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void finishNotOrderReceiver(IBinder paramIBinder, int paramInt1, int paramInt2, String paramString, Bundle paramBundle, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeStrongBinder(paramIBinder);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeString(paramString);
      parcel1.writeBundle(paramBundle);
      if (paramBoolean) {
        paramInt1 = 1;
      } else {
        paramInt1 = 0;
      } 
      parcel1.writeInt(paramInt1);
      this.mRemote.transact(10081, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void reportSkippedFrames(long paramLong1, long paramLong2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeLong(paramLong1);
      parcel1.writeLong(paramLong2);
      this.mRemote.transact(10082, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void reportSkippedFrames(long paramLong1, boolean paramBoolean1, boolean paramBoolean2, long paramLong2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool2;
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeLong(paramLong1);
      boolean bool1 = true;
      if (paramBoolean1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      parcel1.writeInt(bool2);
      if (paramBoolean2) {
        bool2 = bool1;
      } else {
        bool2 = false;
      } 
      parcel1.writeInt(bool2);
      parcel1.writeLong(paramLong2);
      this.mRemote.transact(10109, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String queryProcessNameFromPid(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10083, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readString();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyAppKillReason(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeInt(paramInt4);
      parcel1.writeString(paramString);
      this.mRemote.transact(10084, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusFontVariationAdaptionData getFontVariationAdaptionData() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10085, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusFontVariationAdaptionData)OplusFontVariationAdaptionData.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerHansListener(String paramString, IOplusHansListener paramIOplusHansListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      if (paramIOplusHansListener != null) {
        IBinder iBinder = paramIOplusHansListener.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10101, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterHansListener(String paramString, IOplusHansListener paramIOplusHansListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      if (paramIOplusHansListener != null) {
        IBinder iBinder = paramIOplusHansListener.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10102, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setAppFreeze(String paramString, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString);
      parcel1.writeBundle(paramBundle);
      this.mRemote.transact(10078, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifyAthenaOnekeyClearRunning(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10103, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void executeResPreload(String paramString1, int paramInt1, int paramInt2, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10104, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public Bundle getResPreloadInfo(int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new Bundle();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10110, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBundle();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public long getPreloadIOSize() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      this.mRemote.transact(10112, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readLong();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void enterFastFreezer(String paramString1, int[] paramArrayOfint, long paramLong, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      if (paramArrayOfint != null) {
        parcel1.writeInt(paramArrayOfint.length);
        parcel1.writeIntArray(paramArrayOfint);
      } else {
        parcel1.writeInt(0);
      } 
      parcel1.writeLong(paramLong);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10105, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void exitFastFreezer(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10106, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void registerEapDataCallback(IOplusEapDataCallback paramIOplusEapDataCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusEapDataCallback != null) {
        IBinder iBinder = paramIOplusEapDataCallback.asBinder();
      } else {
        paramIOplusEapDataCallback = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusEapDataCallback);
      this.mRemote.transact(10107, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void unregisterEapDataCallback(IOplusEapDataCallback paramIOplusEapDataCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      if (paramIOplusEapDataCallback != null) {
        IBinder iBinder = paramIOplusEapDataCallback.asBinder();
      } else {
        paramIOplusEapDataCallback = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusEapDataCallback);
      this.mRemote.transact(10108, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void clientTransactionComplete(IBinder paramIBinder, int paramInt) throws RemoteException {
    this.mOplusAtm.clientTransactionComplete(paramIBinder, paramInt);
  }
  
  public List<ActivityManager.RecentTaskInfo> getAllVisibleTasksInfo(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10111, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(ActivityManager.RecentTaskInfo.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
