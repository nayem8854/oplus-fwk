package android.app;

import android.content.ComponentName;
import android.content.pm.ParceledListSlice;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.service.notification.Adjustment;
import android.service.notification.Condition;
import android.service.notification.IConditionProvider;
import android.service.notification.INotificationListener;
import android.service.notification.StatusBarNotification;
import android.service.notification.ZenModeConfig;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public interface INotificationManager extends IInterface {
  String addAutomaticZenRule(AutomaticZenRule paramAutomaticZenRule) throws RemoteException;
  
  void allowAssistantAdjustment(String paramString) throws RemoteException;
  
  void applyAdjustmentFromAssistant(INotificationListener paramINotificationListener, Adjustment paramAdjustment) throws RemoteException;
  
  void applyAdjustmentsFromAssistant(INotificationListener paramINotificationListener, List<Adjustment> paramList) throws RemoteException;
  
  void applyEnqueuedAdjustmentFromAssistant(INotificationListener paramINotificationListener, Adjustment paramAdjustment) throws RemoteException;
  
  void applyRestore(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  boolean areBubblesAllowed(String paramString) throws RemoteException;
  
  boolean areChannelsBypassingDnd() throws RemoteException;
  
  boolean areNotificationsEnabled(String paramString) throws RemoteException;
  
  boolean areNotificationsEnabledForPackage(String paramString, int paramInt) throws RemoteException;
  
  boolean canNotifyAsPackage(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean canShowBadge(String paramString, int paramInt) throws RemoteException;
  
  void cancelAllNotifications(String paramString, int paramInt) throws RemoteException;
  
  void cancelNotificationFromListener(INotificationListener paramINotificationListener, String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void cancelNotificationWithTag(String paramString1, String paramString2, String paramString3, int paramInt1, int paramInt2) throws RemoteException;
  
  void cancelNotificationsFromListener(INotificationListener paramINotificationListener, String[] paramArrayOfString) throws RemoteException;
  
  void cancelToast(String paramString, IBinder paramIBinder) throws RemoteException;
  
  void clearData(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void clearRequestedListenerHints(INotificationListener paramINotificationListener) throws RemoteException;
  
  void createConversationNotificationChannelForPackage(String paramString1, int paramInt, String paramString2, NotificationChannel paramNotificationChannel, String paramString3) throws RemoteException;
  
  void createNotificationChannelGroups(String paramString, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void createNotificationChannels(String paramString, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void createNotificationChannelsForPackage(String paramString, int paramInt, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void deleteConversationNotificationChannels(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void deleteNotificationChannel(String paramString1, String paramString2) throws RemoteException;
  
  void deleteNotificationChannelGroup(String paramString1, String paramString2) throws RemoteException;
  
  void deleteNotificationHistoryItem(String paramString, int paramInt, long paramLong) throws RemoteException;
  
  void disallowAssistantAdjustment(String paramString) throws RemoteException;
  
  void enqueueNotificationWithTag(String paramString1, String paramString2, String paramString3, int paramInt1, Notification paramNotification, int paramInt2) throws RemoteException;
  
  void enqueueTextToast(String paramString, IBinder paramIBinder, CharSequence paramCharSequence, int paramInt1, int paramInt2, ITransientNotificationCallback paramITransientNotificationCallback) throws RemoteException;
  
  void enqueueToast(String paramString, IBinder paramIBinder, ITransientNotification paramITransientNotification, int paramInt1, int paramInt2) throws RemoteException;
  
  void finishToken(String paramString, IBinder paramIBinder) throws RemoteException;
  
  StatusBarNotification[] getActiveNotifications(String paramString) throws RemoteException;
  
  ParceledListSlice getActiveNotificationsFromListener(INotificationListener paramINotificationListener, String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  StatusBarNotification[] getActiveNotificationsWithAttribution(String paramString1, String paramString2) throws RemoteException;
  
  List<String> getAllowedAssistantAdjustments(String paramString) throws RemoteException;
  
  ComponentName getAllowedNotificationAssistant() throws RemoteException;
  
  ComponentName getAllowedNotificationAssistantForUser(int paramInt) throws RemoteException;
  
  ParceledListSlice getAppActiveNotifications(String paramString, int paramInt) throws RemoteException;
  
  int getAppsBypassingDndCount(int paramInt) throws RemoteException;
  
  AutomaticZenRule getAutomaticZenRule(String paramString) throws RemoteException;
  
  byte[] getBackupPayload(int paramInt) throws RemoteException;
  
  int getBlockedAppCount(int paramInt) throws RemoteException;
  
  int getBlockedChannelCount(String paramString, int paramInt) throws RemoteException;
  
  int getBubblePreferenceForPackage(String paramString, int paramInt) throws RemoteException;
  
  NotificationManager.Policy getConsolidatedNotificationPolicy() throws RemoteException;
  
  NotificationChannel getConversationNotificationChannel(String paramString1, int paramInt, String paramString2, String paramString3, boolean paramBoolean, String paramString4) throws RemoteException;
  
  ParceledListSlice getConversations(boolean paramBoolean) throws RemoteException;
  
  ParceledListSlice getConversationsForPackage(String paramString, int paramInt) throws RemoteException;
  
  int getDeletedChannelCount(String paramString, int paramInt) throws RemoteException;
  
  ComponentName getEffectsSuppressor() throws RemoteException;
  
  List<String> getEnabledNotificationListenerPackages() throws RemoteException;
  
  List<ComponentName> getEnabledNotificationListeners(int paramInt) throws RemoteException;
  
  int getHintsFromListener(INotificationListener paramINotificationListener) throws RemoteException;
  
  StatusBarNotification[] getHistoricalNotifications(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  StatusBarNotification[] getHistoricalNotificationsWithAttribution(String paramString1, String paramString2, int paramInt, boolean paramBoolean) throws RemoteException;
  
  int getInterruptionFilterFromListener(INotificationListener paramINotificationListener) throws RemoteException;
  
  NotificationChannel getNotificationChannel(String paramString1, int paramInt, String paramString2, String paramString3) throws RemoteException;
  
  NotificationChannel getNotificationChannelForPackage(String paramString1, int paramInt, String paramString2, String paramString3, boolean paramBoolean) throws RemoteException;
  
  NotificationChannelGroup getNotificationChannelGroup(String paramString1, String paramString2) throws RemoteException;
  
  NotificationChannelGroup getNotificationChannelGroupForPackage(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  ParceledListSlice getNotificationChannelGroups(String paramString) throws RemoteException;
  
  ParceledListSlice getNotificationChannelGroupsForPackage(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  ParceledListSlice getNotificationChannelGroupsFromPrivilegedListener(INotificationListener paramINotificationListener, String paramString, UserHandle paramUserHandle) throws RemoteException;
  
  ParceledListSlice getNotificationChannels(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  ParceledListSlice getNotificationChannelsBypassingDnd(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getNotificationChannelsForPackage(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  ParceledListSlice getNotificationChannelsFromPrivilegedListener(INotificationListener paramINotificationListener, String paramString, UserHandle paramUserHandle) throws RemoteException;
  
  String getNotificationDelegate(String paramString) throws RemoteException;
  
  NotificationHistory getNotificationHistory(String paramString1, String paramString2) throws RemoteException;
  
  NotificationManager.Policy getNotificationPolicy(String paramString) throws RemoteException;
  
  int getNumNotificationChannelsForPackage(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  int getPackageImportance(String paramString) throws RemoteException;
  
  NotificationChannelGroup getPopulatedNotificationChannelGroupForPackage(String paramString1, int paramInt, String paramString2, boolean paramBoolean) throws RemoteException;
  
  boolean getPrivateNotificationsAllowed() throws RemoteException;
  
  int getRuleInstanceCount(ComponentName paramComponentName) throws RemoteException;
  
  ParceledListSlice getSnoozedNotificationsFromListener(INotificationListener paramINotificationListener, int paramInt) throws RemoteException;
  
  int getZenMode() throws RemoteException;
  
  ZenModeConfig getZenModeConfig() throws RemoteException;
  
  List<ZenModeConfig.ZenRule> getZenRules() throws RemoteException;
  
  boolean hasSentValidMsg(String paramString, int paramInt) throws RemoteException;
  
  boolean hasUserDemotedInvalidMsgApp(String paramString, int paramInt) throws RemoteException;
  
  boolean isInInvalidMsgState(String paramString, int paramInt) throws RemoteException;
  
  boolean isNotificationAssistantAccessGranted(ComponentName paramComponentName) throws RemoteException;
  
  boolean isNotificationListenerAccessGranted(ComponentName paramComponentName) throws RemoteException;
  
  boolean isNotificationListenerAccessGrantedForUser(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean isNotificationPolicyAccessGranted(String paramString) throws RemoteException;
  
  boolean isNotificationPolicyAccessGrantedForPackage(String paramString) throws RemoteException;
  
  boolean isPackagePaused(String paramString) throws RemoteException;
  
  boolean isSystemConditionProviderEnabled(String paramString) throws RemoteException;
  
  boolean matchesCallFilter(Bundle paramBundle) throws RemoteException;
  
  void notifyConditions(String paramString, IConditionProvider paramIConditionProvider, Condition[] paramArrayOfCondition) throws RemoteException;
  
  boolean onlyHasDefaultChannel(String paramString, int paramInt) throws RemoteException;
  
  long pullStats(long paramLong, int paramInt, boolean paramBoolean, List<ParcelFileDescriptor> paramList) throws RemoteException;
  
  void registerListener(INotificationListener paramINotificationListener, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean removeAutomaticZenRule(String paramString) throws RemoteException;
  
  boolean removeAutomaticZenRules(String paramString) throws RemoteException;
  
  void requestBindListener(ComponentName paramComponentName) throws RemoteException;
  
  void requestBindProvider(ComponentName paramComponentName) throws RemoteException;
  
  void requestHintsFromListener(INotificationListener paramINotificationListener, int paramInt) throws RemoteException;
  
  void requestInterruptionFilterFromListener(INotificationListener paramINotificationListener, int paramInt) throws RemoteException;
  
  void requestUnbindListener(INotificationListener paramINotificationListener) throws RemoteException;
  
  void requestUnbindProvider(IConditionProvider paramIConditionProvider) throws RemoteException;
  
  void setAutomaticZenRuleState(String paramString, Condition paramCondition) throws RemoteException;
  
  void setBubblesAllowed(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setHideSilentStatusIcons(boolean paramBoolean) throws RemoteException;
  
  void setInterruptionFilter(String paramString, int paramInt) throws RemoteException;
  
  void setInvalidMsgAppDemoted(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationAssistantAccessGranted(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setNotificationAssistantAccessGrantedForUser(ComponentName paramComponentName, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationDelegate(String paramString1, String paramString2) throws RemoteException;
  
  void setNotificationListenerAccessGranted(ComponentName paramComponentName, boolean paramBoolean) throws RemoteException;
  
  void setNotificationListenerAccessGrantedForUser(ComponentName paramComponentName, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationPolicy(String paramString, NotificationManager.Policy paramPolicy) throws RemoteException;
  
  void setNotificationPolicyAccessGranted(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setNotificationPolicyAccessGrantedForUser(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationsEnabledForPackage(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationsEnabledWithImportanceLockForPackage(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setNotificationsShownFromListener(INotificationListener paramINotificationListener, String[] paramArrayOfString) throws RemoteException;
  
  void setOnNotificationPostedTrimFromListener(INotificationListener paramINotificationListener, int paramInt) throws RemoteException;
  
  void setPrivateNotificationsAllowed(boolean paramBoolean) throws RemoteException;
  
  void setShowBadge(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setZenMode(int paramInt, Uri paramUri, String paramString) throws RemoteException;
  
  boolean shouldHideSilentStatusIcons(String paramString) throws RemoteException;
  
  void silenceNotificationSound() throws RemoteException;
  
  void snoozeNotificationUntilContextFromListener(INotificationListener paramINotificationListener, String paramString1, String paramString2) throws RemoteException;
  
  void snoozeNotificationUntilFromListener(INotificationListener paramINotificationListener, String paramString, long paramLong) throws RemoteException;
  
  void unregisterListener(INotificationListener paramINotificationListener, int paramInt) throws RemoteException;
  
  void unsnoozeNotificationFromAssistant(INotificationListener paramINotificationListener, String paramString) throws RemoteException;
  
  void unsnoozeNotificationFromSystemListener(INotificationListener paramINotificationListener, String paramString) throws RemoteException;
  
  boolean updateAutomaticZenRule(String paramString, AutomaticZenRule paramAutomaticZenRule) throws RemoteException;
  
  void updateNotificationChannelForPackage(String paramString, int paramInt, NotificationChannel paramNotificationChannel) throws RemoteException;
  
  void updateNotificationChannelFromPrivilegedListener(INotificationListener paramINotificationListener, String paramString, UserHandle paramUserHandle, NotificationChannel paramNotificationChannel) throws RemoteException;
  
  void updateNotificationChannelGroupForPackage(String paramString, int paramInt, NotificationChannelGroup paramNotificationChannelGroup) throws RemoteException;
  
  void updateNotificationChannelGroupFromPrivilegedListener(INotificationListener paramINotificationListener, String paramString, UserHandle paramUserHandle, NotificationChannelGroup paramNotificationChannelGroup) throws RemoteException;
  
  class Default implements INotificationManager {
    public void cancelAllNotifications(String param1String, int param1Int) throws RemoteException {}
    
    public void clearData(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void enqueueTextToast(String param1String, IBinder param1IBinder, CharSequence param1CharSequence, int param1Int1, int param1Int2, ITransientNotificationCallback param1ITransientNotificationCallback) throws RemoteException {}
    
    public void enqueueToast(String param1String, IBinder param1IBinder, ITransientNotification param1ITransientNotification, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void cancelToast(String param1String, IBinder param1IBinder) throws RemoteException {}
    
    public void finishToken(String param1String, IBinder param1IBinder) throws RemoteException {}
    
    public void enqueueNotificationWithTag(String param1String1, String param1String2, String param1String3, int param1Int1, Notification param1Notification, int param1Int2) throws RemoteException {}
    
    public void cancelNotificationWithTag(String param1String1, String param1String2, String param1String3, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setShowBadge(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean canShowBadge(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasSentValidMsg(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isInInvalidMsgState(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasUserDemotedInvalidMsgApp(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setInvalidMsgAppDemoted(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationsEnabledForPackage(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationsEnabledWithImportanceLockForPackage(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public boolean areNotificationsEnabledForPackage(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean areNotificationsEnabled(String param1String) throws RemoteException {
      return false;
    }
    
    public int getPackageImportance(String param1String) throws RemoteException {
      return 0;
    }
    
    public List<String> getAllowedAssistantAdjustments(String param1String) throws RemoteException {
      return null;
    }
    
    public void allowAssistantAdjustment(String param1String) throws RemoteException {}
    
    public void disallowAssistantAdjustment(String param1String) throws RemoteException {}
    
    public boolean shouldHideSilentStatusIcons(String param1String) throws RemoteException {
      return false;
    }
    
    public void setHideSilentStatusIcons(boolean param1Boolean) throws RemoteException {}
    
    public void setBubblesAllowed(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean areBubblesAllowed(String param1String) throws RemoteException {
      return false;
    }
    
    public int getBubblePreferenceForPackage(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void createNotificationChannelGroups(String param1String, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void createNotificationChannels(String param1String, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void createNotificationChannelsForPackage(String param1String, int param1Int, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public ParceledListSlice getConversations(boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getConversationsForPackage(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getNotificationChannelGroupsForPackage(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NotificationChannelGroup getNotificationChannelGroupForPackage(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public NotificationChannelGroup getPopulatedNotificationChannelGroupForPackage(String param1String1, int param1Int, String param1String2, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void updateNotificationChannelGroupForPackage(String param1String, int param1Int, NotificationChannelGroup param1NotificationChannelGroup) throws RemoteException {}
    
    public void updateNotificationChannelForPackage(String param1String, int param1Int, NotificationChannel param1NotificationChannel) throws RemoteException {}
    
    public NotificationChannel getNotificationChannel(String param1String1, int param1Int, String param1String2, String param1String3) throws RemoteException {
      return null;
    }
    
    public NotificationChannel getConversationNotificationChannel(String param1String1, int param1Int, String param1String2, String param1String3, boolean param1Boolean, String param1String4) throws RemoteException {
      return null;
    }
    
    public void createConversationNotificationChannelForPackage(String param1String1, int param1Int, String param1String2, NotificationChannel param1NotificationChannel, String param1String3) throws RemoteException {}
    
    public NotificationChannel getNotificationChannelForPackage(String param1String1, int param1Int, String param1String2, String param1String3, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void deleteNotificationChannel(String param1String1, String param1String2) throws RemoteException {}
    
    public void deleteConversationNotificationChannels(String param1String1, int param1Int, String param1String2) throws RemoteException {}
    
    public ParceledListSlice getNotificationChannels(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getNotificationChannelsForPackage(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public int getNumNotificationChannelsForPackage(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public int getDeletedChannelCount(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getBlockedChannelCount(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void deleteNotificationChannelGroup(String param1String1, String param1String2) throws RemoteException {}
    
    public NotificationChannelGroup getNotificationChannelGroup(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getNotificationChannelGroups(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean onlyHasDefaultChannel(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public int getBlockedAppCount(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean areChannelsBypassingDnd() throws RemoteException {
      return false;
    }
    
    public int getAppsBypassingDndCount(int param1Int) throws RemoteException {
      return 0;
    }
    
    public ParceledListSlice getNotificationChannelsBypassingDnd(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isPackagePaused(String param1String) throws RemoteException {
      return false;
    }
    
    public void deleteNotificationHistoryItem(String param1String, int param1Int, long param1Long) throws RemoteException {}
    
    public void silenceNotificationSound() throws RemoteException {}
    
    public StatusBarNotification[] getActiveNotifications(String param1String) throws RemoteException {
      return null;
    }
    
    public StatusBarNotification[] getActiveNotificationsWithAttribution(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public StatusBarNotification[] getHistoricalNotifications(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public StatusBarNotification[] getHistoricalNotificationsWithAttribution(String param1String1, String param1String2, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public NotificationHistory getNotificationHistory(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void registerListener(INotificationListener param1INotificationListener, ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void unregisterListener(INotificationListener param1INotificationListener, int param1Int) throws RemoteException {}
    
    public void cancelNotificationFromListener(INotificationListener param1INotificationListener, String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void cancelNotificationsFromListener(INotificationListener param1INotificationListener, String[] param1ArrayOfString) throws RemoteException {}
    
    public void snoozeNotificationUntilContextFromListener(INotificationListener param1INotificationListener, String param1String1, String param1String2) throws RemoteException {}
    
    public void snoozeNotificationUntilFromListener(INotificationListener param1INotificationListener, String param1String, long param1Long) throws RemoteException {}
    
    public void requestBindListener(ComponentName param1ComponentName) throws RemoteException {}
    
    public void requestUnbindListener(INotificationListener param1INotificationListener) throws RemoteException {}
    
    public void requestBindProvider(ComponentName param1ComponentName) throws RemoteException {}
    
    public void requestUnbindProvider(IConditionProvider param1IConditionProvider) throws RemoteException {}
    
    public void setNotificationsShownFromListener(INotificationListener param1INotificationListener, String[] param1ArrayOfString) throws RemoteException {}
    
    public ParceledListSlice getActiveNotificationsFromListener(INotificationListener param1INotificationListener, String[] param1ArrayOfString, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getSnoozedNotificationsFromListener(INotificationListener param1INotificationListener, int param1Int) throws RemoteException {
      return null;
    }
    
    public void clearRequestedListenerHints(INotificationListener param1INotificationListener) throws RemoteException {}
    
    public void requestHintsFromListener(INotificationListener param1INotificationListener, int param1Int) throws RemoteException {}
    
    public int getHintsFromListener(INotificationListener param1INotificationListener) throws RemoteException {
      return 0;
    }
    
    public void requestInterruptionFilterFromListener(INotificationListener param1INotificationListener, int param1Int) throws RemoteException {}
    
    public int getInterruptionFilterFromListener(INotificationListener param1INotificationListener) throws RemoteException {
      return 0;
    }
    
    public void setOnNotificationPostedTrimFromListener(INotificationListener param1INotificationListener, int param1Int) throws RemoteException {}
    
    public void setInterruptionFilter(String param1String, int param1Int) throws RemoteException {}
    
    public void updateNotificationChannelGroupFromPrivilegedListener(INotificationListener param1INotificationListener, String param1String, UserHandle param1UserHandle, NotificationChannelGroup param1NotificationChannelGroup) throws RemoteException {}
    
    public void updateNotificationChannelFromPrivilegedListener(INotificationListener param1INotificationListener, String param1String, UserHandle param1UserHandle, NotificationChannel param1NotificationChannel) throws RemoteException {}
    
    public ParceledListSlice getNotificationChannelsFromPrivilegedListener(INotificationListener param1INotificationListener, String param1String, UserHandle param1UserHandle) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getNotificationChannelGroupsFromPrivilegedListener(INotificationListener param1INotificationListener, String param1String, UserHandle param1UserHandle) throws RemoteException {
      return null;
    }
    
    public void applyEnqueuedAdjustmentFromAssistant(INotificationListener param1INotificationListener, Adjustment param1Adjustment) throws RemoteException {}
    
    public void applyAdjustmentFromAssistant(INotificationListener param1INotificationListener, Adjustment param1Adjustment) throws RemoteException {}
    
    public void applyAdjustmentsFromAssistant(INotificationListener param1INotificationListener, List<Adjustment> param1List) throws RemoteException {}
    
    public void unsnoozeNotificationFromAssistant(INotificationListener param1INotificationListener, String param1String) throws RemoteException {}
    
    public void unsnoozeNotificationFromSystemListener(INotificationListener param1INotificationListener, String param1String) throws RemoteException {}
    
    public ComponentName getEffectsSuppressor() throws RemoteException {
      return null;
    }
    
    public boolean matchesCallFilter(Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public boolean isSystemConditionProviderEnabled(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isNotificationListenerAccessGranted(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean isNotificationListenerAccessGrantedForUser(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isNotificationAssistantAccessGranted(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void setNotificationListenerAccessGranted(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationAssistantAccessGranted(ComponentName param1ComponentName, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationListenerAccessGrantedForUser(ComponentName param1ComponentName, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationAssistantAccessGrantedForUser(ComponentName param1ComponentName, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public List<String> getEnabledNotificationListenerPackages() throws RemoteException {
      return null;
    }
    
    public List<ComponentName> getEnabledNotificationListeners(int param1Int) throws RemoteException {
      return null;
    }
    
    public ComponentName getAllowedNotificationAssistantForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public ComponentName getAllowedNotificationAssistant() throws RemoteException {
      return null;
    }
    
    public int getZenMode() throws RemoteException {
      return 0;
    }
    
    public ZenModeConfig getZenModeConfig() throws RemoteException {
      return null;
    }
    
    public NotificationManager.Policy getConsolidatedNotificationPolicy() throws RemoteException {
      return null;
    }
    
    public void setZenMode(int param1Int, Uri param1Uri, String param1String) throws RemoteException {}
    
    public void notifyConditions(String param1String, IConditionProvider param1IConditionProvider, Condition[] param1ArrayOfCondition) throws RemoteException {}
    
    public boolean isNotificationPolicyAccessGranted(String param1String) throws RemoteException {
      return false;
    }
    
    public NotificationManager.Policy getNotificationPolicy(String param1String) throws RemoteException {
      return null;
    }
    
    public void setNotificationPolicy(String param1String, NotificationManager.Policy param1Policy) throws RemoteException {}
    
    public boolean isNotificationPolicyAccessGrantedForPackage(String param1String) throws RemoteException {
      return false;
    }
    
    public void setNotificationPolicyAccessGranted(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void setNotificationPolicyAccessGrantedForUser(String param1String, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public AutomaticZenRule getAutomaticZenRule(String param1String) throws RemoteException {
      return null;
    }
    
    public List<ZenModeConfig.ZenRule> getZenRules() throws RemoteException {
      return null;
    }
    
    public String addAutomaticZenRule(AutomaticZenRule param1AutomaticZenRule) throws RemoteException {
      return null;
    }
    
    public boolean updateAutomaticZenRule(String param1String, AutomaticZenRule param1AutomaticZenRule) throws RemoteException {
      return false;
    }
    
    public boolean removeAutomaticZenRule(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean removeAutomaticZenRules(String param1String) throws RemoteException {
      return false;
    }
    
    public int getRuleInstanceCount(ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void setAutomaticZenRuleState(String param1String, Condition param1Condition) throws RemoteException {}
    
    public byte[] getBackupPayload(int param1Int) throws RemoteException {
      return null;
    }
    
    public void applyRestore(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public ParceledListSlice getAppActiveNotifications(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setNotificationDelegate(String param1String1, String param1String2) throws RemoteException {}
    
    public String getNotificationDelegate(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean canNotifyAsPackage(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setPrivateNotificationsAllowed(boolean param1Boolean) throws RemoteException {}
    
    public boolean getPrivateNotificationsAllowed() throws RemoteException {
      return false;
    }
    
    public long pullStats(long param1Long, int param1Int, boolean param1Boolean, List<ParcelFileDescriptor> param1List) throws RemoteException {
      return 0L;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements INotificationManager {
    private static final String DESCRIPTOR = "android.app.INotificationManager";
    
    static final int TRANSACTION_addAutomaticZenRule = 121;
    
    static final int TRANSACTION_allowAssistantAdjustment = 21;
    
    static final int TRANSACTION_applyAdjustmentFromAssistant = 90;
    
    static final int TRANSACTION_applyAdjustmentsFromAssistant = 91;
    
    static final int TRANSACTION_applyEnqueuedAdjustmentFromAssistant = 89;
    
    static final int TRANSACTION_applyRestore = 128;
    
    static final int TRANSACTION_areBubblesAllowed = 26;
    
    static final int TRANSACTION_areChannelsBypassingDnd = 54;
    
    static final int TRANSACTION_areNotificationsEnabled = 18;
    
    static final int TRANSACTION_areNotificationsEnabledForPackage = 17;
    
    static final int TRANSACTION_canNotifyAsPackage = 132;
    
    static final int TRANSACTION_canShowBadge = 10;
    
    static final int TRANSACTION_cancelAllNotifications = 1;
    
    static final int TRANSACTION_cancelNotificationFromListener = 67;
    
    static final int TRANSACTION_cancelNotificationWithTag = 8;
    
    static final int TRANSACTION_cancelNotificationsFromListener = 68;
    
    static final int TRANSACTION_cancelToast = 5;
    
    static final int TRANSACTION_clearData = 2;
    
    static final int TRANSACTION_clearRequestedListenerHints = 78;
    
    static final int TRANSACTION_createConversationNotificationChannelForPackage = 40;
    
    static final int TRANSACTION_createNotificationChannelGroups = 28;
    
    static final int TRANSACTION_createNotificationChannels = 29;
    
    static final int TRANSACTION_createNotificationChannelsForPackage = 30;
    
    static final int TRANSACTION_deleteConversationNotificationChannels = 43;
    
    static final int TRANSACTION_deleteNotificationChannel = 42;
    
    static final int TRANSACTION_deleteNotificationChannelGroup = 49;
    
    static final int TRANSACTION_deleteNotificationHistoryItem = 58;
    
    static final int TRANSACTION_disallowAssistantAdjustment = 22;
    
    static final int TRANSACTION_enqueueNotificationWithTag = 7;
    
    static final int TRANSACTION_enqueueTextToast = 3;
    
    static final int TRANSACTION_enqueueToast = 4;
    
    static final int TRANSACTION_finishToken = 6;
    
    static final int TRANSACTION_getActiveNotifications = 60;
    
    static final int TRANSACTION_getActiveNotificationsFromListener = 76;
    
    static final int TRANSACTION_getActiveNotificationsWithAttribution = 61;
    
    static final int TRANSACTION_getAllowedAssistantAdjustments = 20;
    
    static final int TRANSACTION_getAllowedNotificationAssistant = 107;
    
    static final int TRANSACTION_getAllowedNotificationAssistantForUser = 106;
    
    static final int TRANSACTION_getAppActiveNotifications = 129;
    
    static final int TRANSACTION_getAppsBypassingDndCount = 55;
    
    static final int TRANSACTION_getAutomaticZenRule = 119;
    
    static final int TRANSACTION_getBackupPayload = 127;
    
    static final int TRANSACTION_getBlockedAppCount = 53;
    
    static final int TRANSACTION_getBlockedChannelCount = 48;
    
    static final int TRANSACTION_getBubblePreferenceForPackage = 27;
    
    static final int TRANSACTION_getConsolidatedNotificationPolicy = 110;
    
    static final int TRANSACTION_getConversationNotificationChannel = 39;
    
    static final int TRANSACTION_getConversations = 31;
    
    static final int TRANSACTION_getConversationsForPackage = 32;
    
    static final int TRANSACTION_getDeletedChannelCount = 47;
    
    static final int TRANSACTION_getEffectsSuppressor = 94;
    
    static final int TRANSACTION_getEnabledNotificationListenerPackages = 104;
    
    static final int TRANSACTION_getEnabledNotificationListeners = 105;
    
    static final int TRANSACTION_getHintsFromListener = 80;
    
    static final int TRANSACTION_getHistoricalNotifications = 62;
    
    static final int TRANSACTION_getHistoricalNotificationsWithAttribution = 63;
    
    static final int TRANSACTION_getInterruptionFilterFromListener = 82;
    
    static final int TRANSACTION_getNotificationChannel = 38;
    
    static final int TRANSACTION_getNotificationChannelForPackage = 41;
    
    static final int TRANSACTION_getNotificationChannelGroup = 50;
    
    static final int TRANSACTION_getNotificationChannelGroupForPackage = 34;
    
    static final int TRANSACTION_getNotificationChannelGroups = 51;
    
    static final int TRANSACTION_getNotificationChannelGroupsForPackage = 33;
    
    static final int TRANSACTION_getNotificationChannelGroupsFromPrivilegedListener = 88;
    
    static final int TRANSACTION_getNotificationChannels = 44;
    
    static final int TRANSACTION_getNotificationChannelsBypassingDnd = 56;
    
    static final int TRANSACTION_getNotificationChannelsForPackage = 45;
    
    static final int TRANSACTION_getNotificationChannelsFromPrivilegedListener = 87;
    
    static final int TRANSACTION_getNotificationDelegate = 131;
    
    static final int TRANSACTION_getNotificationHistory = 64;
    
    static final int TRANSACTION_getNotificationPolicy = 114;
    
    static final int TRANSACTION_getNumNotificationChannelsForPackage = 46;
    
    static final int TRANSACTION_getPackageImportance = 19;
    
    static final int TRANSACTION_getPopulatedNotificationChannelGroupForPackage = 35;
    
    static final int TRANSACTION_getPrivateNotificationsAllowed = 134;
    
    static final int TRANSACTION_getRuleInstanceCount = 125;
    
    static final int TRANSACTION_getSnoozedNotificationsFromListener = 77;
    
    static final int TRANSACTION_getZenMode = 108;
    
    static final int TRANSACTION_getZenModeConfig = 109;
    
    static final int TRANSACTION_getZenRules = 120;
    
    static final int TRANSACTION_hasSentValidMsg = 11;
    
    static final int TRANSACTION_hasUserDemotedInvalidMsgApp = 13;
    
    static final int TRANSACTION_isInInvalidMsgState = 12;
    
    static final int TRANSACTION_isNotificationAssistantAccessGranted = 99;
    
    static final int TRANSACTION_isNotificationListenerAccessGranted = 97;
    
    static final int TRANSACTION_isNotificationListenerAccessGrantedForUser = 98;
    
    static final int TRANSACTION_isNotificationPolicyAccessGranted = 113;
    
    static final int TRANSACTION_isNotificationPolicyAccessGrantedForPackage = 116;
    
    static final int TRANSACTION_isPackagePaused = 57;
    
    static final int TRANSACTION_isSystemConditionProviderEnabled = 96;
    
    static final int TRANSACTION_matchesCallFilter = 95;
    
    static final int TRANSACTION_notifyConditions = 112;
    
    static final int TRANSACTION_onlyHasDefaultChannel = 52;
    
    static final int TRANSACTION_pullStats = 135;
    
    static final int TRANSACTION_registerListener = 65;
    
    static final int TRANSACTION_removeAutomaticZenRule = 123;
    
    static final int TRANSACTION_removeAutomaticZenRules = 124;
    
    static final int TRANSACTION_requestBindListener = 71;
    
    static final int TRANSACTION_requestBindProvider = 73;
    
    static final int TRANSACTION_requestHintsFromListener = 79;
    
    static final int TRANSACTION_requestInterruptionFilterFromListener = 81;
    
    static final int TRANSACTION_requestUnbindListener = 72;
    
    static final int TRANSACTION_requestUnbindProvider = 74;
    
    static final int TRANSACTION_setAutomaticZenRuleState = 126;
    
    static final int TRANSACTION_setBubblesAllowed = 25;
    
    static final int TRANSACTION_setHideSilentStatusIcons = 24;
    
    static final int TRANSACTION_setInterruptionFilter = 84;
    
    static final int TRANSACTION_setInvalidMsgAppDemoted = 14;
    
    static final int TRANSACTION_setNotificationAssistantAccessGranted = 101;
    
    static final int TRANSACTION_setNotificationAssistantAccessGrantedForUser = 103;
    
    static final int TRANSACTION_setNotificationDelegate = 130;
    
    static final int TRANSACTION_setNotificationListenerAccessGranted = 100;
    
    static final int TRANSACTION_setNotificationListenerAccessGrantedForUser = 102;
    
    static final int TRANSACTION_setNotificationPolicy = 115;
    
    static final int TRANSACTION_setNotificationPolicyAccessGranted = 117;
    
    static final int TRANSACTION_setNotificationPolicyAccessGrantedForUser = 118;
    
    static final int TRANSACTION_setNotificationsEnabledForPackage = 15;
    
    static final int TRANSACTION_setNotificationsEnabledWithImportanceLockForPackage = 16;
    
    static final int TRANSACTION_setNotificationsShownFromListener = 75;
    
    static final int TRANSACTION_setOnNotificationPostedTrimFromListener = 83;
    
    static final int TRANSACTION_setPrivateNotificationsAllowed = 133;
    
    static final int TRANSACTION_setShowBadge = 9;
    
    static final int TRANSACTION_setZenMode = 111;
    
    static final int TRANSACTION_shouldHideSilentStatusIcons = 23;
    
    static final int TRANSACTION_silenceNotificationSound = 59;
    
    static final int TRANSACTION_snoozeNotificationUntilContextFromListener = 69;
    
    static final int TRANSACTION_snoozeNotificationUntilFromListener = 70;
    
    static final int TRANSACTION_unregisterListener = 66;
    
    static final int TRANSACTION_unsnoozeNotificationFromAssistant = 92;
    
    static final int TRANSACTION_unsnoozeNotificationFromSystemListener = 93;
    
    static final int TRANSACTION_updateAutomaticZenRule = 122;
    
    static final int TRANSACTION_updateNotificationChannelForPackage = 37;
    
    static final int TRANSACTION_updateNotificationChannelFromPrivilegedListener = 86;
    
    static final int TRANSACTION_updateNotificationChannelGroupForPackage = 36;
    
    static final int TRANSACTION_updateNotificationChannelGroupFromPrivilegedListener = 85;
    
    public Stub() {
      attachInterface(this, "android.app.INotificationManager");
    }
    
    public static INotificationManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.INotificationManager");
      if (iInterface != null && iInterface instanceof INotificationManager)
        return (INotificationManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 135:
          return "pullStats";
        case 134:
          return "getPrivateNotificationsAllowed";
        case 133:
          return "setPrivateNotificationsAllowed";
        case 132:
          return "canNotifyAsPackage";
        case 131:
          return "getNotificationDelegate";
        case 130:
          return "setNotificationDelegate";
        case 129:
          return "getAppActiveNotifications";
        case 128:
          return "applyRestore";
        case 127:
          return "getBackupPayload";
        case 126:
          return "setAutomaticZenRuleState";
        case 125:
          return "getRuleInstanceCount";
        case 124:
          return "removeAutomaticZenRules";
        case 123:
          return "removeAutomaticZenRule";
        case 122:
          return "updateAutomaticZenRule";
        case 121:
          return "addAutomaticZenRule";
        case 120:
          return "getZenRules";
        case 119:
          return "getAutomaticZenRule";
        case 118:
          return "setNotificationPolicyAccessGrantedForUser";
        case 117:
          return "setNotificationPolicyAccessGranted";
        case 116:
          return "isNotificationPolicyAccessGrantedForPackage";
        case 115:
          return "setNotificationPolicy";
        case 114:
          return "getNotificationPolicy";
        case 113:
          return "isNotificationPolicyAccessGranted";
        case 112:
          return "notifyConditions";
        case 111:
          return "setZenMode";
        case 110:
          return "getConsolidatedNotificationPolicy";
        case 109:
          return "getZenModeConfig";
        case 108:
          return "getZenMode";
        case 107:
          return "getAllowedNotificationAssistant";
        case 106:
          return "getAllowedNotificationAssistantForUser";
        case 105:
          return "getEnabledNotificationListeners";
        case 104:
          return "getEnabledNotificationListenerPackages";
        case 103:
          return "setNotificationAssistantAccessGrantedForUser";
        case 102:
          return "setNotificationListenerAccessGrantedForUser";
        case 101:
          return "setNotificationAssistantAccessGranted";
        case 100:
          return "setNotificationListenerAccessGranted";
        case 99:
          return "isNotificationAssistantAccessGranted";
        case 98:
          return "isNotificationListenerAccessGrantedForUser";
        case 97:
          return "isNotificationListenerAccessGranted";
        case 96:
          return "isSystemConditionProviderEnabled";
        case 95:
          return "matchesCallFilter";
        case 94:
          return "getEffectsSuppressor";
        case 93:
          return "unsnoozeNotificationFromSystemListener";
        case 92:
          return "unsnoozeNotificationFromAssistant";
        case 91:
          return "applyAdjustmentsFromAssistant";
        case 90:
          return "applyAdjustmentFromAssistant";
        case 89:
          return "applyEnqueuedAdjustmentFromAssistant";
        case 88:
          return "getNotificationChannelGroupsFromPrivilegedListener";
        case 87:
          return "getNotificationChannelsFromPrivilegedListener";
        case 86:
          return "updateNotificationChannelFromPrivilegedListener";
        case 85:
          return "updateNotificationChannelGroupFromPrivilegedListener";
        case 84:
          return "setInterruptionFilter";
        case 83:
          return "setOnNotificationPostedTrimFromListener";
        case 82:
          return "getInterruptionFilterFromListener";
        case 81:
          return "requestInterruptionFilterFromListener";
        case 80:
          return "getHintsFromListener";
        case 79:
          return "requestHintsFromListener";
        case 78:
          return "clearRequestedListenerHints";
        case 77:
          return "getSnoozedNotificationsFromListener";
        case 76:
          return "getActiveNotificationsFromListener";
        case 75:
          return "setNotificationsShownFromListener";
        case 74:
          return "requestUnbindProvider";
        case 73:
          return "requestBindProvider";
        case 72:
          return "requestUnbindListener";
        case 71:
          return "requestBindListener";
        case 70:
          return "snoozeNotificationUntilFromListener";
        case 69:
          return "snoozeNotificationUntilContextFromListener";
        case 68:
          return "cancelNotificationsFromListener";
        case 67:
          return "cancelNotificationFromListener";
        case 66:
          return "unregisterListener";
        case 65:
          return "registerListener";
        case 64:
          return "getNotificationHistory";
        case 63:
          return "getHistoricalNotificationsWithAttribution";
        case 62:
          return "getHistoricalNotifications";
        case 61:
          return "getActiveNotificationsWithAttribution";
        case 60:
          return "getActiveNotifications";
        case 59:
          return "silenceNotificationSound";
        case 58:
          return "deleteNotificationHistoryItem";
        case 57:
          return "isPackagePaused";
        case 56:
          return "getNotificationChannelsBypassingDnd";
        case 55:
          return "getAppsBypassingDndCount";
        case 54:
          return "areChannelsBypassingDnd";
        case 53:
          return "getBlockedAppCount";
        case 52:
          return "onlyHasDefaultChannel";
        case 51:
          return "getNotificationChannelGroups";
        case 50:
          return "getNotificationChannelGroup";
        case 49:
          return "deleteNotificationChannelGroup";
        case 48:
          return "getBlockedChannelCount";
        case 47:
          return "getDeletedChannelCount";
        case 46:
          return "getNumNotificationChannelsForPackage";
        case 45:
          return "getNotificationChannelsForPackage";
        case 44:
          return "getNotificationChannels";
        case 43:
          return "deleteConversationNotificationChannels";
        case 42:
          return "deleteNotificationChannel";
        case 41:
          return "getNotificationChannelForPackage";
        case 40:
          return "createConversationNotificationChannelForPackage";
        case 39:
          return "getConversationNotificationChannel";
        case 38:
          return "getNotificationChannel";
        case 37:
          return "updateNotificationChannelForPackage";
        case 36:
          return "updateNotificationChannelGroupForPackage";
        case 35:
          return "getPopulatedNotificationChannelGroupForPackage";
        case 34:
          return "getNotificationChannelGroupForPackage";
        case 33:
          return "getNotificationChannelGroupsForPackage";
        case 32:
          return "getConversationsForPackage";
        case 31:
          return "getConversations";
        case 30:
          return "createNotificationChannelsForPackage";
        case 29:
          return "createNotificationChannels";
        case 28:
          return "createNotificationChannelGroups";
        case 27:
          return "getBubblePreferenceForPackage";
        case 26:
          return "areBubblesAllowed";
        case 25:
          return "setBubblesAllowed";
        case 24:
          return "setHideSilentStatusIcons";
        case 23:
          return "shouldHideSilentStatusIcons";
        case 22:
          return "disallowAssistantAdjustment";
        case 21:
          return "allowAssistantAdjustment";
        case 20:
          return "getAllowedAssistantAdjustments";
        case 19:
          return "getPackageImportance";
        case 18:
          return "areNotificationsEnabled";
        case 17:
          return "areNotificationsEnabledForPackage";
        case 16:
          return "setNotificationsEnabledWithImportanceLockForPackage";
        case 15:
          return "setNotificationsEnabledForPackage";
        case 14:
          return "setInvalidMsgAppDemoted";
        case 13:
          return "hasUserDemotedInvalidMsgApp";
        case 12:
          return "isInInvalidMsgState";
        case 11:
          return "hasSentValidMsg";
        case 10:
          return "canShowBadge";
        case 9:
          return "setShowBadge";
        case 8:
          return "cancelNotificationWithTag";
        case 7:
          return "enqueueNotificationWithTag";
        case 6:
          return "finishToken";
        case 5:
          return "cancelToast";
        case 4:
          return "enqueueToast";
        case 3:
          return "enqueueTextToast";
        case 2:
          return "clearData";
        case 1:
          break;
      } 
      return "cancelAllNotifications";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IConditionProvider iConditionProvider;
      if (param1Int1 != 1598968902) {
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        ArrayList<ParcelFileDescriptor> arrayList1;
        String str21;
        ParceledListSlice parceledListSlice7;
        byte[] arrayOfByte1;
        String str20;
        List<ZenModeConfig.ZenRule> list2;
        String str19;
        AutomaticZenRule automaticZenRule;
        String str18;
        NotificationManager.Policy policy2;
        String str17;
        Condition[] arrayOfCondition;
        String str16;
        NotificationManager.Policy policy1;
        ZenModeConfig zenModeConfig;
        ComponentName componentName2;
        List<ComponentName> list1;
        String str15;
        ComponentName componentName1;
        String str14;
        ArrayList<Adjustment> arrayList;
        ParceledListSlice parceledListSlice6;
        INotificationListener iNotificationListener2;
        ParceledListSlice parceledListSlice5;
        String[] arrayOfString2;
        IConditionProvider iConditionProvider1;
        INotificationListener iNotificationListener1;
        String str13, arrayOfString1[], str12;
        NotificationHistory notificationHistory;
        StatusBarNotification[] arrayOfStatusBarNotification3;
        String str11;
        StatusBarNotification[] arrayOfStatusBarNotification2;
        String str10;
        StatusBarNotification[] arrayOfStatusBarNotification1;
        String str9;
        ParceledListSlice parceledListSlice4;
        String str8;
        ParceledListSlice parceledListSlice3;
        String str7;
        NotificationChannelGroup notificationChannelGroup2;
        String str6;
        ParceledListSlice parceledListSlice2;
        String str5;
        NotificationChannel notificationChannel3;
        String str4;
        NotificationChannel notificationChannel2;
        String str3;
        NotificationChannel notificationChannel1;
        NotificationChannelGroup notificationChannelGroup1;
        ParceledListSlice parceledListSlice1;
        String str2;
        List<String> list;
        String str1;
        IBinder iBinder1;
        ITransientNotificationCallback iTransientNotificationCallback;
        long l;
        String str24;
        INotificationListener iNotificationListener4;
        String str23;
        INotificationListener iNotificationListener3;
        String str22;
        IBinder iBinder2;
        String str30;
        byte[] arrayOfByte2;
        String str29;
        INotificationListener iNotificationListener8;
        String str28;
        INotificationListener iNotificationListener7;
        String[] arrayOfString3;
        INotificationListener iNotificationListener6;
        String str27;
        INotificationListener iNotificationListener5;
        String str26;
        IBinder iBinder3;
        String str32;
        ITransientNotification iTransientNotification;
        String str31, str33;
        boolean bool18 = false, bool19 = false, bool20 = false, bool21 = false, bool22 = false, bool23 = false, bool24 = false, bool25 = false, bool26 = false, bool27 = false, bool28 = false, bool29 = false, bool30 = false, bool31 = false, bool32 = false, bool33 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 135:
            param1Parcel1.enforceInterface("android.app.INotificationManager");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            arrayList1 = new ArrayList();
            l = pullStats(l, param1Int1, bool24, arrayList1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 134:
            arrayList1.enforceInterface("android.app.INotificationManager");
            bool17 = getPrivateNotificationsAllowed();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 133:
            arrayList1.enforceInterface("android.app.INotificationManager");
            bool24 = bool33;
            if (arrayList1.readInt() != 0)
              bool24 = true; 
            setPrivateNotificationsAllowed(bool24);
            param1Parcel2.writeNoException();
            return true;
          case 132:
            arrayList1.enforceInterface("android.app.INotificationManager");
            str24 = arrayList1.readString();
            str30 = arrayList1.readString();
            i12 = arrayList1.readInt();
            bool16 = canNotifyAsPackage(str24, str30, i12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 131:
            arrayList1.enforceInterface("android.app.INotificationManager");
            str21 = arrayList1.readString();
            str21 = getNotificationDelegate(str21);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str21);
            return true;
          case 130:
            str21.enforceInterface("android.app.INotificationManager");
            str30 = str21.readString();
            str21 = str21.readString();
            setNotificationDelegate(str30, str21);
            param1Parcel2.writeNoException();
            return true;
          case 129:
            str21.enforceInterface("android.app.INotificationManager");
            str30 = str21.readString();
            i11 = str21.readInt();
            parceledListSlice7 = getAppActiveNotifications(str30, i11);
            param1Parcel2.writeNoException();
            if (parceledListSlice7 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice7.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 128:
            parceledListSlice7.enforceInterface("android.app.INotificationManager");
            arrayOfByte2 = parceledListSlice7.createByteArray();
            i11 = parceledListSlice7.readInt();
            applyRestore(arrayOfByte2, i11);
            param1Parcel2.writeNoException();
            return true;
          case 127:
            parceledListSlice7.enforceInterface("android.app.INotificationManager");
            i11 = parceledListSlice7.readInt();
            arrayOfByte1 = getBackupPayload(i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 126:
            arrayOfByte1.enforceInterface("android.app.INotificationManager");
            str29 = arrayOfByte1.readString();
            if (arrayOfByte1.readInt() != 0) {
              Condition condition = (Condition)Condition.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              arrayOfByte1 = null;
            } 
            setAutomaticZenRuleState(str29, (Condition)arrayOfByte1);
            param1Parcel2.writeNoException();
            return true;
          case 125:
            arrayOfByte1.enforceInterface("android.app.INotificationManager");
            if (arrayOfByte1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              arrayOfByte1 = null;
            } 
            i11 = getRuleInstanceCount((ComponentName)arrayOfByte1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i11);
            return true;
          case 124:
            arrayOfByte1.enforceInterface("android.app.INotificationManager");
            str20 = arrayOfByte1.readString();
            bool15 = removeAutomaticZenRules(str20);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 123:
            str20.enforceInterface("android.app.INotificationManager");
            str20 = str20.readString();
            bool15 = removeAutomaticZenRule(str20);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 122:
            str20.enforceInterface("android.app.INotificationManager");
            str29 = str20.readString();
            if (str20.readInt() != 0) {
              AutomaticZenRule automaticZenRule1 = (AutomaticZenRule)AutomaticZenRule.CREATOR.createFromParcel((Parcel)str20);
            } else {
              str20 = null;
            } 
            bool15 = updateAutomaticZenRule(str29, (AutomaticZenRule)str20);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 121:
            str20.enforceInterface("android.app.INotificationManager");
            if (str20.readInt() != 0) {
              AutomaticZenRule automaticZenRule1 = (AutomaticZenRule)AutomaticZenRule.CREATOR.createFromParcel((Parcel)str20);
            } else {
              str20 = null;
            } 
            str20 = addAutomaticZenRule((AutomaticZenRule)str20);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str20);
            return true;
          case 120:
            str20.enforceInterface("android.app.INotificationManager");
            list2 = getZenRules();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 119:
            list2.enforceInterface("android.app.INotificationManager");
            str19 = list2.readString();
            automaticZenRule = getAutomaticZenRule(str19);
            param1Parcel2.writeNoException();
            if (automaticZenRule != null) {
              param1Parcel2.writeInt(1);
              automaticZenRule.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 118:
            automaticZenRule.enforceInterface("android.app.INotificationManager");
            str29 = automaticZenRule.readString();
            i10 = automaticZenRule.readInt();
            bool24 = bool18;
            if (automaticZenRule.readInt() != 0)
              bool24 = true; 
            setNotificationPolicyAccessGrantedForUser(str29, i10, bool24);
            param1Parcel2.writeNoException();
            return true;
          case 117:
            automaticZenRule.enforceInterface("android.app.INotificationManager");
            str29 = automaticZenRule.readString();
            bool24 = bool19;
            if (automaticZenRule.readInt() != 0)
              bool24 = true; 
            setNotificationPolicyAccessGranted(str29, bool24);
            param1Parcel2.writeNoException();
            return true;
          case 116:
            automaticZenRule.enforceInterface("android.app.INotificationManager");
            str18 = automaticZenRule.readString();
            bool14 = isNotificationPolicyAccessGrantedForPackage(str18);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 115:
            str18.enforceInterface("android.app.INotificationManager");
            str29 = str18.readString();
            if (str18.readInt() != 0) {
              NotificationManager.Policy policy = (NotificationManager.Policy)NotificationManager.Policy.CREATOR.createFromParcel((Parcel)str18);
            } else {
              str18 = null;
            } 
            setNotificationPolicy(str29, (NotificationManager.Policy)str18);
            param1Parcel2.writeNoException();
            return true;
          case 114:
            str18.enforceInterface("android.app.INotificationManager");
            str18 = str18.readString();
            policy2 = getNotificationPolicy(str18);
            param1Parcel2.writeNoException();
            if (policy2 != null) {
              param1Parcel2.writeInt(1);
              policy2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 113:
            policy2.enforceInterface("android.app.INotificationManager");
            str17 = policy2.readString();
            bool14 = isNotificationPolicyAccessGranted(str17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 112:
            str17.enforceInterface("android.app.INotificationManager");
            str29 = str17.readString();
            iConditionProvider = IConditionProvider.Stub.asInterface(str17.readStrongBinder());
            arrayOfCondition = (Condition[])str17.createTypedArray(Condition.CREATOR);
            notifyConditions(str29, iConditionProvider, arrayOfCondition);
            return true;
          case 111:
            arrayOfCondition.enforceInterface("android.app.INotificationManager");
            i9 = arrayOfCondition.readInt();
            if (arrayOfCondition.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfCondition);
            } else {
              iConditionProvider = null;
            } 
            str16 = arrayOfCondition.readString();
            setZenMode(i9, (Uri)iConditionProvider, str16);
            return true;
          case 110:
            str16.enforceInterface("android.app.INotificationManager");
            policy1 = getConsolidatedNotificationPolicy();
            iConditionProvider.writeNoException();
            if (policy1 != null) {
              iConditionProvider.writeInt(1);
              policy1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 109:
            policy1.enforceInterface("android.app.INotificationManager");
            zenModeConfig = getZenModeConfig();
            iConditionProvider.writeNoException();
            if (zenModeConfig != null) {
              iConditionProvider.writeInt(1);
              zenModeConfig.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 108:
            zenModeConfig.enforceInterface("android.app.INotificationManager");
            i9 = getZenMode();
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i9);
            return true;
          case 107:
            zenModeConfig.enforceInterface("android.app.INotificationManager");
            componentName2 = getAllowedNotificationAssistant();
            iConditionProvider.writeNoException();
            if (componentName2 != null) {
              iConditionProvider.writeInt(1);
              componentName2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 106:
            componentName2.enforceInterface("android.app.INotificationManager");
            i9 = componentName2.readInt();
            componentName2 = getAllowedNotificationAssistantForUser(i9);
            iConditionProvider.writeNoException();
            if (componentName2 != null) {
              iConditionProvider.writeInt(1);
              componentName2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 105:
            componentName2.enforceInterface("android.app.INotificationManager");
            i9 = componentName2.readInt();
            list1 = getEnabledNotificationListeners(i9);
            iConditionProvider.writeNoException();
            iConditionProvider.writeTypedList(list1);
            return true;
          case 104:
            list1.enforceInterface("android.app.INotificationManager");
            list1 = (List)getEnabledNotificationListenerPackages();
            iConditionProvider.writeNoException();
            iConditionProvider.writeStringList(list1);
            return true;
          case 103:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str29 = null;
            } 
            i9 = list1.readInt();
            bool24 = bool20;
            if (list1.readInt() != 0)
              bool24 = true; 
            setNotificationAssistantAccessGrantedForUser((ComponentName)str29, i9, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 102:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str29 = null;
            } 
            i9 = list1.readInt();
            bool24 = bool21;
            if (list1.readInt() != 0)
              bool24 = true; 
            setNotificationListenerAccessGrantedForUser((ComponentName)str29, i9, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 101:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str29 = null;
            } 
            bool24 = bool22;
            if (list1.readInt() != 0)
              bool24 = true; 
            setNotificationAssistantAccessGranted((ComponentName)str29, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 100:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str29 = null;
            } 
            bool24 = bool23;
            if (list1.readInt() != 0)
              bool24 = true; 
            setNotificationListenerAccessGranted((ComponentName)str29, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 99:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            bool13 = isNotificationAssistantAccessGranted((ComponentName)list1);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool13);
            return true;
          case 98:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str29 = null;
            } 
            i8 = list1.readInt();
            bool12 = isNotificationListenerAccessGrantedForUser((ComponentName)str29, i8);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool12);
            return true;
          case 97:
            list1.enforceInterface("android.app.INotificationManager");
            if (list1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)list1);
            } else {
              list1 = null;
            } 
            bool12 = isNotificationListenerAccessGranted((ComponentName)list1);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool12);
            return true;
          case 96:
            list1.enforceInterface("android.app.INotificationManager");
            str15 = list1.readString();
            bool12 = isSystemConditionProviderEnabled(str15);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool12);
            return true;
          case 95:
            str15.enforceInterface("android.app.INotificationManager");
            if (str15.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str15);
            } else {
              str15 = null;
            } 
            bool12 = matchesCallFilter((Bundle)str15);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool12);
            return true;
          case 94:
            str15.enforceInterface("android.app.INotificationManager");
            componentName1 = getEffectsSuppressor();
            iConditionProvider.writeNoException();
            if (componentName1 != null) {
              iConditionProvider.writeInt(1);
              componentName1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 93:
            componentName1.enforceInterface("android.app.INotificationManager");
            iNotificationListener8 = INotificationListener.Stub.asInterface(componentName1.readStrongBinder());
            str14 = componentName1.readString();
            unsnoozeNotificationFromSystemListener(iNotificationListener8, str14);
            iConditionProvider.writeNoException();
            return true;
          case 92:
            str14.enforceInterface("android.app.INotificationManager");
            iNotificationListener8 = INotificationListener.Stub.asInterface(str14.readStrongBinder());
            str14 = str14.readString();
            unsnoozeNotificationFromAssistant(iNotificationListener8, str14);
            iConditionProvider.writeNoException();
            return true;
          case 91:
            str14.enforceInterface("android.app.INotificationManager");
            iNotificationListener8 = INotificationListener.Stub.asInterface(str14.readStrongBinder());
            arrayList = str14.createTypedArrayList(Adjustment.CREATOR);
            applyAdjustmentsFromAssistant(iNotificationListener8, arrayList);
            iConditionProvider.writeNoException();
            return true;
          case 90:
            arrayList.enforceInterface("android.app.INotificationManager");
            iNotificationListener8 = INotificationListener.Stub.asInterface(arrayList.readStrongBinder());
            if (arrayList.readInt() != 0) {
              Adjustment adjustment = (Adjustment)Adjustment.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            applyAdjustmentFromAssistant(iNotificationListener8, (Adjustment)arrayList);
            iConditionProvider.writeNoException();
            return true;
          case 89:
            arrayList.enforceInterface("android.app.INotificationManager");
            iNotificationListener8 = INotificationListener.Stub.asInterface(arrayList.readStrongBinder());
            if (arrayList.readInt() != 0) {
              Adjustment adjustment = (Adjustment)Adjustment.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            applyEnqueuedAdjustmentFromAssistant(iNotificationListener8, (Adjustment)arrayList);
            iConditionProvider.writeNoException();
            return true;
          case 88:
            arrayList.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(arrayList.readStrongBinder());
            str28 = arrayList.readString();
            if (arrayList.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            parceledListSlice6 = getNotificationChannelGroupsFromPrivilegedListener(iNotificationListener4, str28, (UserHandle)arrayList);
            iConditionProvider.writeNoException();
            if (parceledListSlice6 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice6.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 87:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(parceledListSlice6.readStrongBinder());
            str28 = parceledListSlice6.readString();
            if (parceledListSlice6.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)parceledListSlice6);
            } else {
              parceledListSlice6 = null;
            } 
            parceledListSlice6 = getNotificationChannelsFromPrivilegedListener(iNotificationListener4, str28, (UserHandle)parceledListSlice6);
            iConditionProvider.writeNoException();
            if (parceledListSlice6 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice6.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 86:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(parceledListSlice6.readStrongBinder());
            str32 = parceledListSlice6.readString();
            if (parceledListSlice6.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)parceledListSlice6);
            } else {
              str28 = null;
            } 
            if (parceledListSlice6.readInt() != 0) {
              NotificationChannel notificationChannel = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel((Parcel)parceledListSlice6);
            } else {
              parceledListSlice6 = null;
            } 
            updateNotificationChannelFromPrivilegedListener(iNotificationListener4, str32, (UserHandle)str28, (NotificationChannel)parceledListSlice6);
            iConditionProvider.writeNoException();
            return true;
          case 85:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(parceledListSlice6.readStrongBinder());
            str32 = parceledListSlice6.readString();
            if (parceledListSlice6.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)parceledListSlice6);
            } else {
              str28 = null;
            } 
            if (parceledListSlice6.readInt() != 0) {
              NotificationChannelGroup notificationChannelGroup = (NotificationChannelGroup)NotificationChannelGroup.CREATOR.createFromParcel((Parcel)parceledListSlice6);
            } else {
              parceledListSlice6 = null;
            } 
            updateNotificationChannelGroupFromPrivilegedListener(iNotificationListener4, str32, (UserHandle)str28, (NotificationChannelGroup)parceledListSlice6);
            iConditionProvider.writeNoException();
            return true;
          case 84:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            str28 = parceledListSlice6.readString();
            i7 = parceledListSlice6.readInt();
            setInterruptionFilter(str28, i7);
            iConditionProvider.writeNoException();
            return true;
          case 83:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            iNotificationListener7 = INotificationListener.Stub.asInterface(parceledListSlice6.readStrongBinder());
            i7 = parceledListSlice6.readInt();
            setOnNotificationPostedTrimFromListener(iNotificationListener7, i7);
            iConditionProvider.writeNoException();
            return true;
          case 82:
            parceledListSlice6.enforceInterface("android.app.INotificationManager");
            iNotificationListener2 = INotificationListener.Stub.asInterface(parceledListSlice6.readStrongBinder());
            i7 = getInterruptionFilterFromListener(iNotificationListener2);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i7);
            return true;
          case 81:
            iNotificationListener2.enforceInterface("android.app.INotificationManager");
            iNotificationListener7 = INotificationListener.Stub.asInterface(iNotificationListener2.readStrongBinder());
            i7 = iNotificationListener2.readInt();
            requestInterruptionFilterFromListener(iNotificationListener7, i7);
            iConditionProvider.writeNoException();
            return true;
          case 80:
            iNotificationListener2.enforceInterface("android.app.INotificationManager");
            iNotificationListener2 = INotificationListener.Stub.asInterface(iNotificationListener2.readStrongBinder());
            i7 = getHintsFromListener(iNotificationListener2);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i7);
            return true;
          case 79:
            iNotificationListener2.enforceInterface("android.app.INotificationManager");
            iNotificationListener7 = INotificationListener.Stub.asInterface(iNotificationListener2.readStrongBinder());
            i7 = iNotificationListener2.readInt();
            requestHintsFromListener(iNotificationListener7, i7);
            iConditionProvider.writeNoException();
            return true;
          case 78:
            iNotificationListener2.enforceInterface("android.app.INotificationManager");
            iNotificationListener2 = INotificationListener.Stub.asInterface(iNotificationListener2.readStrongBinder());
            clearRequestedListenerHints(iNotificationListener2);
            iConditionProvider.writeNoException();
            return true;
          case 77:
            iNotificationListener2.enforceInterface("android.app.INotificationManager");
            iNotificationListener7 = INotificationListener.Stub.asInterface(iNotificationListener2.readStrongBinder());
            i7 = iNotificationListener2.readInt();
            parceledListSlice5 = getSnoozedNotificationsFromListener(iNotificationListener7, i7);
            iConditionProvider.writeNoException();
            if (parceledListSlice5 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice5.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 76:
            parceledListSlice5.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(parceledListSlice5.readStrongBinder());
            arrayOfString3 = parceledListSlice5.createStringArray();
            i7 = parceledListSlice5.readInt();
            parceledListSlice5 = getActiveNotificationsFromListener(iNotificationListener4, arrayOfString3, i7);
            iConditionProvider.writeNoException();
            if (parceledListSlice5 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice5.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 75:
            parceledListSlice5.enforceInterface("android.app.INotificationManager");
            iNotificationListener6 = INotificationListener.Stub.asInterface(parceledListSlice5.readStrongBinder());
            arrayOfString2 = parceledListSlice5.createStringArray();
            setNotificationsShownFromListener(iNotificationListener6, arrayOfString2);
            iConditionProvider.writeNoException();
            return true;
          case 74:
            arrayOfString2.enforceInterface("android.app.INotificationManager");
            iConditionProvider1 = IConditionProvider.Stub.asInterface(arrayOfString2.readStrongBinder());
            requestUnbindProvider(iConditionProvider1);
            iConditionProvider.writeNoException();
            return true;
          case 73:
            iConditionProvider1.enforceInterface("android.app.INotificationManager");
            if (iConditionProvider1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iConditionProvider1);
            } else {
              iConditionProvider1 = null;
            } 
            requestBindProvider((ComponentName)iConditionProvider1);
            iConditionProvider.writeNoException();
            return true;
          case 72:
            iConditionProvider1.enforceInterface("android.app.INotificationManager");
            iNotificationListener1 = INotificationListener.Stub.asInterface(iConditionProvider1.readStrongBinder());
            requestUnbindListener(iNotificationListener1);
            iConditionProvider.writeNoException();
            return true;
          case 71:
            iNotificationListener1.enforceInterface("android.app.INotificationManager");
            if (iNotificationListener1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iNotificationListener1);
            } else {
              iNotificationListener1 = null;
            } 
            requestBindListener((ComponentName)iNotificationListener1);
            iConditionProvider.writeNoException();
            return true;
          case 70:
            iNotificationListener1.enforceInterface("android.app.INotificationManager");
            iNotificationListener4 = INotificationListener.Stub.asInterface(iNotificationListener1.readStrongBinder());
            str27 = iNotificationListener1.readString();
            l = iNotificationListener1.readLong();
            snoozeNotificationUntilFromListener(iNotificationListener4, str27, l);
            iConditionProvider.writeNoException();
            return true;
          case 69:
            iNotificationListener1.enforceInterface("android.app.INotificationManager");
            iNotificationListener5 = INotificationListener.Stub.asInterface(iNotificationListener1.readStrongBinder());
            str23 = iNotificationListener1.readString();
            str13 = iNotificationListener1.readString();
            snoozeNotificationUntilContextFromListener(iNotificationListener5, str23, str13);
            iConditionProvider.writeNoException();
            return true;
          case 68:
            str13.enforceInterface("android.app.INotificationManager");
            iNotificationListener5 = INotificationListener.Stub.asInterface(str13.readStrongBinder());
            arrayOfString1 = str13.createStringArray();
            cancelNotificationsFromListener(iNotificationListener5, arrayOfString1);
            iConditionProvider.writeNoException();
            return true;
          case 67:
            arrayOfString1.enforceInterface("android.app.INotificationManager");
            iNotificationListener5 = INotificationListener.Stub.asInterface(arrayOfString1.readStrongBinder());
            str32 = arrayOfString1.readString();
            str23 = arrayOfString1.readString();
            i7 = arrayOfString1.readInt();
            cancelNotificationFromListener(iNotificationListener5, str32, str23, i7);
            iConditionProvider.writeNoException();
            return true;
          case 66:
            arrayOfString1.enforceInterface("android.app.INotificationManager");
            iNotificationListener5 = INotificationListener.Stub.asInterface(arrayOfString1.readStrongBinder());
            i7 = arrayOfString1.readInt();
            unregisterListener(iNotificationListener5, i7);
            iConditionProvider.writeNoException();
            return true;
          case 65:
            arrayOfString1.enforceInterface("android.app.INotificationManager");
            iNotificationListener3 = INotificationListener.Stub.asInterface(arrayOfString1.readStrongBinder());
            if (arrayOfString1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iNotificationListener5 = null;
            } 
            i7 = arrayOfString1.readInt();
            registerListener(iNotificationListener3, (ComponentName)iNotificationListener5, i7);
            iConditionProvider.writeNoException();
            return true;
          case 64:
            arrayOfString1.enforceInterface("android.app.INotificationManager");
            str26 = arrayOfString1.readString();
            str12 = arrayOfString1.readString();
            notificationHistory = getNotificationHistory(str26, str12);
            iConditionProvider.writeNoException();
            if (notificationHistory != null) {
              iConditionProvider.writeInt(1);
              notificationHistory.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 63:
            notificationHistory.enforceInterface("android.app.INotificationManager");
            str26 = notificationHistory.readString();
            str22 = notificationHistory.readString();
            i7 = notificationHistory.readInt();
            if (notificationHistory.readInt() != 0)
              bool24 = true; 
            arrayOfStatusBarNotification3 = getHistoricalNotificationsWithAttribution(str26, str22, i7, bool24);
            iConditionProvider.writeNoException();
            iConditionProvider.writeTypedArray((Parcelable[])arrayOfStatusBarNotification3, 1);
            return true;
          case 62:
            arrayOfStatusBarNotification3.enforceInterface("android.app.INotificationManager");
            str26 = arrayOfStatusBarNotification3.readString();
            i7 = arrayOfStatusBarNotification3.readInt();
            bool24 = bool25;
            if (arrayOfStatusBarNotification3.readInt() != 0)
              bool24 = true; 
            arrayOfStatusBarNotification3 = getHistoricalNotifications(str26, i7, bool24);
            iConditionProvider.writeNoException();
            iConditionProvider.writeTypedArray((Parcelable[])arrayOfStatusBarNotification3, 1);
            return true;
          case 61:
            arrayOfStatusBarNotification3.enforceInterface("android.app.INotificationManager");
            str26 = arrayOfStatusBarNotification3.readString();
            str11 = arrayOfStatusBarNotification3.readString();
            arrayOfStatusBarNotification2 = getActiveNotificationsWithAttribution(str26, str11);
            iConditionProvider.writeNoException();
            iConditionProvider.writeTypedArray((Parcelable[])arrayOfStatusBarNotification2, 1);
            return true;
          case 60:
            arrayOfStatusBarNotification2.enforceInterface("android.app.INotificationManager");
            str10 = arrayOfStatusBarNotification2.readString();
            arrayOfStatusBarNotification1 = getActiveNotifications(str10);
            iConditionProvider.writeNoException();
            iConditionProvider.writeTypedArray((Parcelable[])arrayOfStatusBarNotification1, 1);
            return true;
          case 59:
            arrayOfStatusBarNotification1.enforceInterface("android.app.INotificationManager");
            silenceNotificationSound();
            iConditionProvider.writeNoException();
            return true;
          case 58:
            arrayOfStatusBarNotification1.enforceInterface("android.app.INotificationManager");
            str26 = arrayOfStatusBarNotification1.readString();
            i7 = arrayOfStatusBarNotification1.readInt();
            l = arrayOfStatusBarNotification1.readLong();
            deleteNotificationHistoryItem(str26, i7, l);
            iConditionProvider.writeNoException();
            return true;
          case 57:
            arrayOfStatusBarNotification1.enforceInterface("android.app.INotificationManager");
            str9 = arrayOfStatusBarNotification1.readString();
            bool11 = isPackagePaused(str9);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool11);
            return true;
          case 56:
            str9.enforceInterface("android.app.INotificationManager");
            str26 = str9.readString();
            i6 = str9.readInt();
            parceledListSlice4 = getNotificationChannelsBypassingDnd(str26, i6);
            iConditionProvider.writeNoException();
            if (parceledListSlice4 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice4.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 55:
            parceledListSlice4.enforceInterface("android.app.INotificationManager");
            i6 = parceledListSlice4.readInt();
            i6 = getAppsBypassingDndCount(i6);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i6);
            return true;
          case 54:
            parceledListSlice4.enforceInterface("android.app.INotificationManager");
            bool10 = areChannelsBypassingDnd();
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool10);
            return true;
          case 53:
            parceledListSlice4.enforceInterface("android.app.INotificationManager");
            i5 = parceledListSlice4.readInt();
            i5 = getBlockedAppCount(i5);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i5);
            return true;
          case 52:
            parceledListSlice4.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice4.readString();
            i5 = parceledListSlice4.readInt();
            bool9 = onlyHasDefaultChannel(str26, i5);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool9);
            return true;
          case 51:
            parceledListSlice4.enforceInterface("android.app.INotificationManager");
            str8 = parceledListSlice4.readString();
            parceledListSlice3 = getNotificationChannelGroups(str8);
            iConditionProvider.writeNoException();
            if (parceledListSlice3 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice3.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 50:
            parceledListSlice3.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice3.readString();
            str7 = parceledListSlice3.readString();
            notificationChannelGroup2 = getNotificationChannelGroup(str26, str7);
            iConditionProvider.writeNoException();
            if (notificationChannelGroup2 != null) {
              iConditionProvider.writeInt(1);
              notificationChannelGroup2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 49:
            notificationChannelGroup2.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannelGroup2.readString();
            str6 = notificationChannelGroup2.readString();
            deleteNotificationChannelGroup(str26, str6);
            iConditionProvider.writeNoException();
            return true;
          case 48:
            str6.enforceInterface("android.app.INotificationManager");
            str26 = str6.readString();
            i4 = str6.readInt();
            i4 = getBlockedChannelCount(str26, i4);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i4);
            return true;
          case 47:
            str6.enforceInterface("android.app.INotificationManager");
            str26 = str6.readString();
            i4 = str6.readInt();
            i4 = getDeletedChannelCount(str26, i4);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i4);
            return true;
          case 46:
            str6.enforceInterface("android.app.INotificationManager");
            str26 = str6.readString();
            i4 = str6.readInt();
            bool24 = bool26;
            if (str6.readInt() != 0)
              bool24 = true; 
            i4 = getNumNotificationChannelsForPackage(str26, i4, bool24);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i4);
            return true;
          case 45:
            str6.enforceInterface("android.app.INotificationManager");
            str26 = str6.readString();
            i4 = str6.readInt();
            if (str6.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            parceledListSlice2 = getNotificationChannelsForPackage(str26, i4, bool24);
            iConditionProvider.writeNoException();
            if (parceledListSlice2 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 44:
            parceledListSlice2.enforceInterface("android.app.INotificationManager");
            str22 = parceledListSlice2.readString();
            str26 = parceledListSlice2.readString();
            i4 = parceledListSlice2.readInt();
            parceledListSlice2 = getNotificationChannels(str22, str26, i4);
            iConditionProvider.writeNoException();
            if (parceledListSlice2 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 43:
            parceledListSlice2.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice2.readString();
            i4 = parceledListSlice2.readInt();
            str5 = parceledListSlice2.readString();
            deleteConversationNotificationChannels(str26, i4, str5);
            iConditionProvider.writeNoException();
            return true;
          case 42:
            str5.enforceInterface("android.app.INotificationManager");
            str26 = str5.readString();
            str5 = str5.readString();
            deleteNotificationChannel(str26, str5);
            iConditionProvider.writeNoException();
            return true;
          case 41:
            str5.enforceInterface("android.app.INotificationManager");
            str26 = str5.readString();
            i4 = str5.readInt();
            str22 = str5.readString();
            str32 = str5.readString();
            if (str5.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            notificationChannel3 = getNotificationChannelForPackage(str26, i4, str22, str32, bool24);
            iConditionProvider.writeNoException();
            if (notificationChannel3 != null) {
              iConditionProvider.writeInt(1);
              notificationChannel3.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 40:
            notificationChannel3.enforceInterface("android.app.INotificationManager");
            str32 = notificationChannel3.readString();
            i4 = notificationChannel3.readInt();
            str22 = notificationChannel3.readString();
            if (notificationChannel3.readInt() != 0) {
              NotificationChannel notificationChannel = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel((Parcel)notificationChannel3);
            } else {
              str26 = null;
            } 
            str4 = notificationChannel3.readString();
            createConversationNotificationChannelForPackage(str32, i4, str22, (NotificationChannel)str26, str4);
            iConditionProvider.writeNoException();
            return true;
          case 39:
            str4.enforceInterface("android.app.INotificationManager");
            str32 = str4.readString();
            i4 = str4.readInt();
            str26 = str4.readString();
            str22 = str4.readString();
            if (str4.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            str4 = str4.readString();
            notificationChannel2 = getConversationNotificationChannel(str32, i4, str26, str22, bool24, str4);
            iConditionProvider.writeNoException();
            if (notificationChannel2 != null) {
              iConditionProvider.writeInt(1);
              notificationChannel2.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 38:
            notificationChannel2.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannel2.readString();
            i4 = notificationChannel2.readInt();
            str22 = notificationChannel2.readString();
            str3 = notificationChannel2.readString();
            notificationChannel1 = getNotificationChannel(str26, i4, str22, str3);
            iConditionProvider.writeNoException();
            if (notificationChannel1 != null) {
              iConditionProvider.writeInt(1);
              notificationChannel1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 37:
            notificationChannel1.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannel1.readString();
            i4 = notificationChannel1.readInt();
            if (notificationChannel1.readInt() != 0) {
              notificationChannel1 = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel((Parcel)notificationChannel1);
            } else {
              notificationChannel1 = null;
            } 
            updateNotificationChannelForPackage(str26, i4, notificationChannel1);
            iConditionProvider.writeNoException();
            return true;
          case 36:
            notificationChannel1.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannel1.readString();
            i4 = notificationChannel1.readInt();
            if (notificationChannel1.readInt() != 0) {
              NotificationChannelGroup notificationChannelGroup = (NotificationChannelGroup)NotificationChannelGroup.CREATOR.createFromParcel((Parcel)notificationChannel1);
            } else {
              notificationChannel1 = null;
            } 
            updateNotificationChannelGroupForPackage(str26, i4, (NotificationChannelGroup)notificationChannel1);
            iConditionProvider.writeNoException();
            return true;
          case 35:
            notificationChannel1.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannel1.readString();
            i4 = notificationChannel1.readInt();
            str22 = notificationChannel1.readString();
            if (notificationChannel1.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            notificationChannelGroup1 = getPopulatedNotificationChannelGroupForPackage(str26, i4, str22, bool24);
            iConditionProvider.writeNoException();
            if (notificationChannelGroup1 != null) {
              iConditionProvider.writeInt(1);
              notificationChannelGroup1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 34:
            notificationChannelGroup1.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannelGroup1.readString();
            str22 = notificationChannelGroup1.readString();
            i4 = notificationChannelGroup1.readInt();
            notificationChannelGroup1 = getNotificationChannelGroupForPackage(str26, str22, i4);
            iConditionProvider.writeNoException();
            if (notificationChannelGroup1 != null) {
              iConditionProvider.writeInt(1);
              notificationChannelGroup1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 33:
            notificationChannelGroup1.enforceInterface("android.app.INotificationManager");
            str26 = notificationChannelGroup1.readString();
            i4 = notificationChannelGroup1.readInt();
            if (notificationChannelGroup1.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            parceledListSlice1 = getNotificationChannelGroupsForPackage(str26, i4, bool24);
            iConditionProvider.writeNoException();
            if (parceledListSlice1 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 32:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice1.readString();
            i4 = parceledListSlice1.readInt();
            parceledListSlice1 = getConversationsForPackage(str26, i4);
            iConditionProvider.writeNoException();
            if (parceledListSlice1 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 31:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            if (parceledListSlice1.readInt() != 0) {
              bool24 = true;
            } else {
              bool24 = false;
            } 
            parceledListSlice1 = getConversations(bool24);
            iConditionProvider.writeNoException();
            if (parceledListSlice1 != null) {
              iConditionProvider.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)iConditionProvider, 1);
            } else {
              iConditionProvider.writeInt(0);
            } 
            return true;
          case 30:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice1.readString();
            i4 = parceledListSlice1.readInt();
            if (parceledListSlice1.readInt() != 0) {
              parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              parceledListSlice1 = null;
            } 
            createNotificationChannelsForPackage(str26, i4, parceledListSlice1);
            iConditionProvider.writeNoException();
            return true;
          case 29:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice1.readString();
            if (parceledListSlice1.readInt() != 0) {
              parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              parceledListSlice1 = null;
            } 
            createNotificationChannels(str26, parceledListSlice1);
            iConditionProvider.writeNoException();
            return true;
          case 28:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice1.readString();
            if (parceledListSlice1.readInt() != 0) {
              parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              parceledListSlice1 = null;
            } 
            createNotificationChannelGroups(str26, parceledListSlice1);
            iConditionProvider.writeNoException();
            return true;
          case 27:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str26 = parceledListSlice1.readString();
            i4 = parceledListSlice1.readInt();
            i4 = getBubblePreferenceForPackage(str26, i4);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i4);
            return true;
          case 26:
            parceledListSlice1.enforceInterface("android.app.INotificationManager");
            str2 = parceledListSlice1.readString();
            bool8 = areBubblesAllowed(str2);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool8);
            return true;
          case 25:
            str2.enforceInterface("android.app.INotificationManager");
            str26 = str2.readString();
            i3 = str2.readInt();
            param1Int2 = str2.readInt();
            setBubblesAllowed(str26, i3, param1Int2);
            iConditionProvider.writeNoException();
            return true;
          case 24:
            str2.enforceInterface("android.app.INotificationManager");
            bool24 = bool27;
            if (str2.readInt() != 0)
              bool24 = true; 
            setHideSilentStatusIcons(bool24);
            iConditionProvider.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("android.app.INotificationManager");
            str2 = str2.readString();
            bool7 = shouldHideSilentStatusIcons(str2);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool7);
            return true;
          case 22:
            str2.enforceInterface("android.app.INotificationManager");
            str2 = str2.readString();
            disallowAssistantAdjustment(str2);
            iConditionProvider.writeNoException();
            return true;
          case 21:
            str2.enforceInterface("android.app.INotificationManager");
            str2 = str2.readString();
            allowAssistantAdjustment(str2);
            iConditionProvider.writeNoException();
            return true;
          case 20:
            str2.enforceInterface("android.app.INotificationManager");
            str2 = str2.readString();
            list = getAllowedAssistantAdjustments(str2);
            iConditionProvider.writeNoException();
            iConditionProvider.writeStringList(list);
            return true;
          case 19:
            list.enforceInterface("android.app.INotificationManager");
            str1 = list.readString();
            i2 = getPackageImportance(str1);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(i2);
            return true;
          case 18:
            str1.enforceInterface("android.app.INotificationManager");
            str1 = str1.readString();
            bool6 = areNotificationsEnabled(str1);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool6);
            return true;
          case 17:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            i1 = str1.readInt();
            bool5 = areNotificationsEnabledForPackage(str26, i1);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool5);
            return true;
          case 16:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            n = str1.readInt();
            bool24 = bool28;
            if (str1.readInt() != 0)
              bool24 = true; 
            setNotificationsEnabledWithImportanceLockForPackage(str26, n, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 15:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            n = str1.readInt();
            bool24 = bool29;
            if (str1.readInt() != 0)
              bool24 = true; 
            setNotificationsEnabledForPackage(str26, n, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            n = str1.readInt();
            bool24 = bool30;
            if (str1.readInt() != 0)
              bool24 = true; 
            setInvalidMsgAppDemoted(str26, n, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            n = str1.readInt();
            bool4 = hasUserDemotedInvalidMsgApp(str26, n);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool4);
            return true;
          case 12:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            m = str1.readInt();
            bool3 = isInInvalidMsgState(str26, m);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool3);
            return true;
          case 11:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            k = str1.readInt();
            bool2 = hasSentValidMsg(str26, k);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool2);
            return true;
          case 10:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            j = str1.readInt();
            bool1 = canShowBadge(str26, j);
            iConditionProvider.writeNoException();
            iConditionProvider.writeInt(bool1);
            return true;
          case 9:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            i = str1.readInt();
            bool24 = bool31;
            if (str1.readInt() != 0)
              bool24 = true; 
            setShowBadge(str26, i, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            str32 = str1.readString();
            str22 = str1.readString();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            cancelNotificationWithTag(str26, str32, str22, param1Int2, i);
            iConditionProvider.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.app.INotificationManager");
            str32 = str1.readString();
            str22 = str1.readString();
            str33 = str1.readString();
            i = str1.readInt();
            if (str1.readInt() != 0) {
              Notification notification = (Notification)Notification.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str26 = null;
            } 
            param1Int2 = str1.readInt();
            enqueueNotificationWithTag(str32, str22, str33, i, (Notification)str26, param1Int2);
            iConditionProvider.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.app.INotificationManager");
            str26 = str1.readString();
            iBinder1 = str1.readStrongBinder();
            finishToken(str26, iBinder1);
            iConditionProvider.writeNoException();
            return true;
          case 5:
            iBinder1.enforceInterface("android.app.INotificationManager");
            str26 = iBinder1.readString();
            iBinder1 = iBinder1.readStrongBinder();
            cancelToast(str26, iBinder1);
            iConditionProvider.writeNoException();
            return true;
          case 4:
            iBinder1.enforceInterface("android.app.INotificationManager");
            str22 = iBinder1.readString();
            iBinder3 = iBinder1.readStrongBinder();
            iTransientNotification = ITransientNotification.Stub.asInterface(iBinder1.readStrongBinder());
            i = iBinder1.readInt();
            param1Int2 = iBinder1.readInt();
            enqueueToast(str22, iBinder3, iTransientNotification, i, param1Int2);
            iConditionProvider.writeNoException();
            return true;
          case 3:
            iBinder1.enforceInterface("android.app.INotificationManager");
            str31 = iBinder1.readString();
            iBinder2 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder3 = null;
            } 
            param1Int2 = iBinder1.readInt();
            i = iBinder1.readInt();
            iTransientNotificationCallback = ITransientNotificationCallback.Stub.asInterface(iBinder1.readStrongBinder());
            enqueueTextToast(str31, iBinder2, (CharSequence)iBinder3, param1Int2, i, iTransientNotificationCallback);
            iConditionProvider.writeNoException();
            return true;
          case 2:
            iTransientNotificationCallback.enforceInterface("android.app.INotificationManager");
            str25 = iTransientNotificationCallback.readString();
            i = iTransientNotificationCallback.readInt();
            bool24 = bool32;
            if (iTransientNotificationCallback.readInt() != 0)
              bool24 = true; 
            clearData(str25, i, bool24);
            iConditionProvider.writeNoException();
            return true;
          case 1:
            break;
        } 
        iTransientNotificationCallback.enforceInterface("android.app.INotificationManager");
        String str25 = iTransientNotificationCallback.readString();
        int i = iTransientNotificationCallback.readInt();
        cancelAllNotifications(str25, i);
        iConditionProvider.writeNoException();
        return true;
      } 
      iConditionProvider.writeString("android.app.INotificationManager");
      return true;
    }
    
    private static class Proxy implements INotificationManager {
      public static INotificationManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.INotificationManager";
      }
      
      public void cancelAllNotifications(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().cancelAllNotifications(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearData(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().clearData(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enqueueTextToast(String param2String, IBinder param2IBinder, CharSequence param2CharSequence, int param2Int1, int param2Int2, ITransientNotificationCallback param2ITransientNotificationCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeStrongBinder(param2IBinder);
              if (param2CharSequence != null) {
                parcel1.writeInt(1);
                TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int1);
                try {
                  IBinder iBinder;
                  parcel1.writeInt(param2Int2);
                  if (param2ITransientNotificationCallback != null) {
                    iBinder = param2ITransientNotificationCallback.asBinder();
                  } else {
                    iBinder = null;
                  } 
                  parcel1.writeStrongBinder(iBinder);
                  try {
                    boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                    if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
                      INotificationManager.Stub.getDefaultImpl().enqueueTextToast(param2String, param2IBinder, param2CharSequence, param2Int1, param2Int2, param2ITransientNotificationCallback);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public void enqueueToast(String param2String, IBinder param2IBinder, ITransientNotification param2ITransientNotification, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          if (param2ITransientNotification != null) {
            iBinder = param2ITransientNotification.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().enqueueToast(param2String, param2IBinder, param2ITransientNotification, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelToast(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().cancelToast(param2String, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishToken(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().finishToken(param2String, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enqueueNotificationWithTag(String param2String1, String param2String2, String param2String3, int param2Int1, Notification param2Notification, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeInt(param2Int1);
                  if (param2Notification != null) {
                    parcel1.writeInt(1);
                    param2Notification.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  try {
                    parcel1.writeInt(param2Int2);
                    boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
                    if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
                      INotificationManager.Stub.getDefaultImpl().enqueueNotificationWithTag(param2String1, param2String2, param2String3, param2Int1, param2Notification, param2Int2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void cancelNotificationWithTag(String param2String1, String param2String2, String param2String3, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().cancelNotificationWithTag(param2String1, param2String2, param2String3, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShowBadge(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setShowBadge(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canShowBadge(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().canShowBadge(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSentValidMsg(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().hasSentValidMsg(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInInvalidMsgState(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(12, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isInInvalidMsgState(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasUserDemotedInvalidMsgApp(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(13, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().hasUserDemotedInvalidMsgApp(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInvalidMsgAppDemoted(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setInvalidMsgAppDemoted(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationsEnabledForPackage(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationsEnabledForPackage(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationsEnabledWithImportanceLockForPackage(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationsEnabledWithImportanceLockForPackage(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean areNotificationsEnabledForPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().areNotificationsEnabledForPackage(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean areNotificationsEnabled(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(18, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().areNotificationsEnabled(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPackageImportance(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getPackageImportance(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAllowedAssistantAdjustments(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getAllowedAssistantAdjustments(param2String); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void allowAssistantAdjustment(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().allowAssistantAdjustment(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disallowAssistantAdjustment(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().disallowAssistantAdjustment(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldHideSilentStatusIcons(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().shouldHideSilentStatusIcons(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHideSilentStatusIcons(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setHideSilentStatusIcons(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setBubblesAllowed(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setBubblesAllowed(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean areBubblesAllowed(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(26, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().areBubblesAllowed(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBubblePreferenceForPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getBubblePreferenceForPackage(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createNotificationChannelGroups(String param2String, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().createNotificationChannelGroups(param2String, param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createNotificationChannels(String param2String, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().createNotificationChannels(param2String, param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createNotificationChannelsForPackage(String param2String, int param2Int, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().createNotificationChannelsForPackage(param2String, param2Int, param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getConversations(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = INotificationManager.Stub.getDefaultImpl().getConversations(param2Boolean);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getConversationsForPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getConversationsForPackage(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelGroupsForPackage(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelGroupsForPackage(param2String, param2Int, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannelGroup getNotificationChannelGroupForPackage(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelGroupForPackage(param2String1, param2String2, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationChannelGroup notificationChannelGroup = (NotificationChannelGroup)NotificationChannelGroup.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationChannelGroup)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannelGroup getPopulatedNotificationChannelGroupForPackage(String param2String1, int param2Int, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getPopulatedNotificationChannelGroupForPackage(param2String1, param2Int, param2String2, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationChannelGroup notificationChannelGroup = (NotificationChannelGroup)NotificationChannelGroup.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationChannelGroup)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateNotificationChannelGroupForPackage(String param2String, int param2Int, NotificationChannelGroup param2NotificationChannelGroup) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2NotificationChannelGroup != null) {
            parcel1.writeInt(1);
            param2NotificationChannelGroup.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().updateNotificationChannelGroupForPackage(param2String, param2Int, param2NotificationChannelGroup);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateNotificationChannelForPackage(String param2String, int param2Int, NotificationChannel param2NotificationChannel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2NotificationChannel != null) {
            parcel1.writeInt(1);
            param2NotificationChannel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().updateNotificationChannelForPackage(param2String, param2Int, param2NotificationChannel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannel getNotificationChannel(String param2String1, int param2Int, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannel(param2String1, param2Int, param2String2, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationChannel notificationChannel = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationChannel)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannel getConversationNotificationChannel(String param2String1, int param2Int, String param2String2, String param2String3, boolean param2Boolean, String param2String4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeInt(param2Int);
              try {
                parcel1.writeString(param2String2);
                try {
                  boolean bool;
                  parcel1.writeString(param2String3);
                  if (param2Boolean) {
                    bool = true;
                  } else {
                    bool = false;
                  } 
                  parcel1.writeInt(bool);
                  try {
                    parcel1.writeString(param2String4);
                    try {
                      boolean bool1 = this.mRemote.transact(39, parcel1, parcel2, 0);
                      if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
                        NotificationChannel notificationChannel = INotificationManager.Stub.getDefaultImpl().getConversationNotificationChannel(param2String1, param2Int, param2String2, param2String3, param2Boolean, param2String4);
                        parcel2.recycle();
                        parcel1.recycle();
                        return notificationChannel;
                      } 
                      parcel2.readException();
                      if (parcel2.readInt() != 0) {
                        NotificationChannel notificationChannel = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel(parcel2);
                      } else {
                        param2String1 = null;
                      } 
                      parcel2.recycle();
                      parcel1.recycle();
                      return (NotificationChannel)param2String1;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void createConversationNotificationChannelForPackage(String param2String1, int param2Int, String param2String2, NotificationChannel param2NotificationChannel, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          if (param2NotificationChannel != null) {
            parcel1.writeInt(1);
            param2NotificationChannel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().createConversationNotificationChannelForPackage(param2String1, param2Int, param2String2, param2NotificationChannel, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannel getNotificationChannelForPackage(String param2String1, int param2Int, String param2String2, String param2String3, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelForPackage(param2String1, param2Int, param2String2, param2String3, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationChannel notificationChannel = (NotificationChannel)NotificationChannel.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationChannel)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteNotificationChannel(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().deleteNotificationChannel(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteConversationNotificationChannels(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().deleteConversationNotificationChannels(param2String1, param2Int, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannels(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannels(param2String1, param2String2, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParceledListSlice)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelsForPackage(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelsForPackage(param2String, param2Int, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getNumNotificationChannelsForPackage(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getNumNotificationChannelsForPackage(param2String, param2Int, param2Boolean);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDeletedChannelCount(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getDeletedChannelCount(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBlockedChannelCount(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getBlockedChannelCount(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteNotificationChannelGroup(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().deleteNotificationChannelGroup(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationChannelGroup getNotificationChannelGroup(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelGroup(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationChannelGroup notificationChannelGroup = (NotificationChannelGroup)NotificationChannelGroup.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationChannelGroup)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelGroups(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelGroups(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean onlyHasDefaultChannel(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(52, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().onlyHasDefaultChannel(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getBlockedAppCount(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getBlockedAppCount(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean areChannelsBypassingDnd() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(54, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().areChannelsBypassingDnd();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAppsBypassingDndCount(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Int = INotificationManager.Stub.getDefaultImpl().getAppsBypassingDndCount(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelsBypassingDnd(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelsBypassingDnd(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackagePaused(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(57, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isPackagePaused(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteNotificationHistoryItem(String param2String, int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().deleteNotificationHistoryItem(param2String, param2Int, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void silenceNotificationSound() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().silenceNotificationSound();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusBarNotification[] getActiveNotifications(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getActiveNotifications(param2String); 
          parcel2.readException();
          return (StatusBarNotification[])parcel2.createTypedArray(StatusBarNotification.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusBarNotification[] getActiveNotificationsWithAttribution(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getActiveNotificationsWithAttribution(param2String1, param2String2); 
          parcel2.readException();
          return (StatusBarNotification[])parcel2.createTypedArray(StatusBarNotification.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusBarNotification[] getHistoricalNotifications(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getHistoricalNotifications(param2String, param2Int, param2Boolean); 
          parcel2.readException();
          return (StatusBarNotification[])parcel2.createTypedArray(StatusBarNotification.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StatusBarNotification[] getHistoricalNotificationsWithAttribution(String param2String1, String param2String2, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getHistoricalNotificationsWithAttribution(param2String1, param2String2, param2Int, param2Boolean); 
          parcel2.readException();
          return (StatusBarNotification[])parcel2.createTypedArray(StatusBarNotification.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationHistory getNotificationHistory(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationHistory(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationHistory notificationHistory = (NotificationHistory)NotificationHistory.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (NotificationHistory)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerListener(INotificationListener param2INotificationListener, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().registerListener(param2INotificationListener, param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterListener(INotificationListener param2INotificationListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().unregisterListener(param2INotificationListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelNotificationFromListener(INotificationListener param2INotificationListener, String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().cancelNotificationFromListener(param2INotificationListener, param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelNotificationsFromListener(INotificationListener param2INotificationListener, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().cancelNotificationsFromListener(param2INotificationListener, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void snoozeNotificationUntilContextFromListener(INotificationListener param2INotificationListener, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().snoozeNotificationUntilContextFromListener(param2INotificationListener, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void snoozeNotificationUntilFromListener(INotificationListener param2INotificationListener, String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().snoozeNotificationUntilFromListener(param2INotificationListener, param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestBindListener(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestBindListener(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestUnbindListener(INotificationListener param2INotificationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestUnbindListener(param2INotificationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestBindProvider(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestBindProvider(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestUnbindProvider(IConditionProvider param2IConditionProvider) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2IConditionProvider != null) {
            iBinder = param2IConditionProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestUnbindProvider(param2IConditionProvider);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationsShownFromListener(INotificationListener param2INotificationListener, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationsShownFromListener(param2INotificationListener, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getActiveNotificationsFromListener(INotificationListener param2INotificationListener, String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getActiveNotificationsFromListener(param2INotificationListener, param2ArrayOfString, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2INotificationListener = null;
          } 
          return (ParceledListSlice)param2INotificationListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getSnoozedNotificationsFromListener(INotificationListener param2INotificationListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getSnoozedNotificationsFromListener(param2INotificationListener, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2INotificationListener = null;
          } 
          return (ParceledListSlice)param2INotificationListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearRequestedListenerHints(INotificationListener param2INotificationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().clearRequestedListenerHints(param2INotificationListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestHintsFromListener(INotificationListener param2INotificationListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestHintsFromListener(param2INotificationListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getHintsFromListener(INotificationListener param2INotificationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getHintsFromListener(param2INotificationListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestInterruptionFilterFromListener(INotificationListener param2INotificationListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().requestInterruptionFilterFromListener(param2INotificationListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInterruptionFilterFromListener(INotificationListener param2INotificationListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getInterruptionFilterFromListener(param2INotificationListener); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOnNotificationPostedTrimFromListener(INotificationListener param2INotificationListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setOnNotificationPostedTrimFromListener(param2INotificationListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInterruptionFilter(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setInterruptionFilter(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateNotificationChannelGroupFromPrivilegedListener(INotificationListener param2INotificationListener, String param2String, UserHandle param2UserHandle, NotificationChannelGroup param2NotificationChannelGroup) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2NotificationChannelGroup != null) {
            parcel1.writeInt(1);
            param2NotificationChannelGroup.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().updateNotificationChannelGroupFromPrivilegedListener(param2INotificationListener, param2String, param2UserHandle, param2NotificationChannelGroup);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateNotificationChannelFromPrivilegedListener(INotificationListener param2INotificationListener, String param2String, UserHandle param2UserHandle, NotificationChannel param2NotificationChannel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2NotificationChannel != null) {
            parcel1.writeInt(1);
            param2NotificationChannel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().updateNotificationChannelFromPrivilegedListener(param2INotificationListener, param2String, param2UserHandle, param2NotificationChannel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelsFromPrivilegedListener(INotificationListener param2INotificationListener, String param2String, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelsFromPrivilegedListener(param2INotificationListener, param2String, param2UserHandle); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2INotificationListener = null;
          } 
          return (ParceledListSlice)param2INotificationListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getNotificationChannelGroupsFromPrivilegedListener(INotificationListener param2INotificationListener, String param2String, UserHandle param2UserHandle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2UserHandle != null) {
            parcel1.writeInt(1);
            param2UserHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationChannelGroupsFromPrivilegedListener(param2INotificationListener, param2String, param2UserHandle); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2INotificationListener = null;
          } 
          return (ParceledListSlice)param2INotificationListener;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyEnqueuedAdjustmentFromAssistant(INotificationListener param2INotificationListener, Adjustment param2Adjustment) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Adjustment != null) {
            parcel1.writeInt(1);
            param2Adjustment.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().applyEnqueuedAdjustmentFromAssistant(param2INotificationListener, param2Adjustment);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyAdjustmentFromAssistant(INotificationListener param2INotificationListener, Adjustment param2Adjustment) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Adjustment != null) {
            parcel1.writeInt(1);
            param2Adjustment.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().applyAdjustmentFromAssistant(param2INotificationListener, param2Adjustment);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyAdjustmentsFromAssistant(INotificationListener param2INotificationListener, List<Adjustment> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().applyAdjustmentsFromAssistant(param2INotificationListener, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unsnoozeNotificationFromAssistant(INotificationListener param2INotificationListener, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().unsnoozeNotificationFromAssistant(param2INotificationListener, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unsnoozeNotificationFromSystemListener(INotificationListener param2INotificationListener, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2INotificationListener != null) {
            iBinder = param2INotificationListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().unsnoozeNotificationFromSystemListener(param2INotificationListener, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getEffectsSuppressor() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            componentName = INotificationManager.Stub.getDefaultImpl().getEffectsSuppressor();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean matchesCallFilter(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().matchesCallFilter(param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSystemConditionProviderEnabled(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(96, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isSystemConditionProviderEnabled(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNotificationListenerAccessGranted(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isNotificationListenerAccessGranted(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNotificationListenerAccessGrantedForUser(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isNotificationListenerAccessGrantedForUser(param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNotificationAssistantAccessGranted(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isNotificationAssistantAccessGranted(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationListenerAccessGranted(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationListenerAccessGranted(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationAssistantAccessGranted(ComponentName param2ComponentName, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationAssistantAccessGranted(param2ComponentName, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationListenerAccessGrantedForUser(ComponentName param2ComponentName, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(102, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationListenerAccessGrantedForUser(param2ComponentName, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationAssistantAccessGrantedForUser(ComponentName param2ComponentName, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationAssistantAccessGrantedForUser(param2ComponentName, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getEnabledNotificationListenerPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(104, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getEnabledNotificationListenerPackages(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ComponentName> getEnabledNotificationListeners(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(105, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getEnabledNotificationListeners(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ComponentName.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getAllowedNotificationAssistantForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            componentName = INotificationManager.Stub.getDefaultImpl().getAllowedNotificationAssistantForUser(param2Int);
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getAllowedNotificationAssistant() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            componentName = INotificationManager.Stub.getDefaultImpl().getAllowedNotificationAssistant();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getZenMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getZenMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ZenModeConfig getZenModeConfig() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ZenModeConfig zenModeConfig;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(109, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            zenModeConfig = INotificationManager.Stub.getDefaultImpl().getZenModeConfig();
            return zenModeConfig;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            zenModeConfig = (ZenModeConfig)ZenModeConfig.CREATOR.createFromParcel(parcel2);
          } else {
            zenModeConfig = null;
          } 
          return zenModeConfig;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationManager.Policy getConsolidatedNotificationPolicy() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          NotificationManager.Policy policy;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            policy = INotificationManager.Stub.getDefaultImpl().getConsolidatedNotificationPolicy();
            return policy;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            policy = (NotificationManager.Policy)NotificationManager.Policy.CREATOR.createFromParcel(parcel2);
          } else {
            policy = null;
          } 
          return policy;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setZenMode(int param2Int, Uri param2Uri, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.INotificationManager");
          parcel.writeInt(param2Int);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(111, parcel, null, 1);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setZenMode(param2Int, param2Uri, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyConditions(String param2String, IConditionProvider param2IConditionProvider, Condition[] param2ArrayOfCondition) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.INotificationManager");
          parcel.writeString(param2String);
          if (param2IConditionProvider != null) {
            iBinder = param2IConditionProvider.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeTypedArray((Parcelable[])param2ArrayOfCondition, 0);
          boolean bool = this.mRemote.transact(112, parcel, null, 1);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().notifyConditions(param2String, param2IConditionProvider, param2ArrayOfCondition);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isNotificationPolicyAccessGranted(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(113, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isNotificationPolicyAccessGranted(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public NotificationManager.Policy getNotificationPolicy(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getNotificationPolicy(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            NotificationManager.Policy policy = (NotificationManager.Policy)NotificationManager.Policy.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (NotificationManager.Policy)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationPolicy(String param2String, NotificationManager.Policy param2Policy) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          if (param2Policy != null) {
            parcel1.writeInt(1);
            param2Policy.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationPolicy(param2String, param2Policy);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isNotificationPolicyAccessGrantedForPackage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(116, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().isNotificationPolicyAccessGrantedForPackage(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationPolicyAccessGranted(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationPolicyAccessGranted(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationPolicyAccessGrantedForUser(String param2String, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationPolicyAccessGrantedForUser(param2String, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AutomaticZenRule getAutomaticZenRule(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getAutomaticZenRule(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            AutomaticZenRule automaticZenRule = (AutomaticZenRule)AutomaticZenRule.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (AutomaticZenRule)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ZenModeConfig.ZenRule> getZenRules() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          boolean bool = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getZenRules(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ZenModeConfig.ZenRule.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String addAutomaticZenRule(AutomaticZenRule param2AutomaticZenRule) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2AutomaticZenRule != null) {
            parcel1.writeInt(1);
            param2AutomaticZenRule.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().addAutomaticZenRule(param2AutomaticZenRule); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateAutomaticZenRule(String param2String, AutomaticZenRule param2AutomaticZenRule) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2AutomaticZenRule != null) {
            parcel1.writeInt(1);
            param2AutomaticZenRule.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().updateAutomaticZenRule(param2String, param2AutomaticZenRule);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeAutomaticZenRule(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(123, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().removeAutomaticZenRule(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeAutomaticZenRules(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(124, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().removeAutomaticZenRules(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRuleInstanceCount(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getRuleInstanceCount(param2ComponentName); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAutomaticZenRuleState(String param2String, Condition param2Condition) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          if (param2Condition != null) {
            parcel1.writeInt(1);
            param2Condition.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setAutomaticZenRuleState(param2String, param2Condition);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getBackupPayload(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(127, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getBackupPayload(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyRestore(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(128, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().applyRestore(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getAppActiveNotifications(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(129, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null)
            return INotificationManager.Stub.getDefaultImpl().getAppActiveNotifications(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setNotificationDelegate(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(130, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setNotificationDelegate(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNotificationDelegate(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool && INotificationManager.Stub.getDefaultImpl() != null) {
            param2String = INotificationManager.Stub.getDefaultImpl().getNotificationDelegate(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canNotifyAsPackage(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(132, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().canNotifyAsPackage(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPrivateNotificationsAllowed(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            INotificationManager.Stub.getDefaultImpl().setPrivateNotificationsAllowed(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getPrivateNotificationsAllowed() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(134, parcel1, parcel2, 0);
          if (!bool2 && INotificationManager.Stub.getDefaultImpl() != null) {
            bool1 = INotificationManager.Stub.getDefaultImpl().getPrivateNotificationsAllowed();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long pullStats(long param2Long, int param2Int, boolean param2Boolean, List<ParcelFileDescriptor> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.INotificationManager");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(135, parcel1, parcel2, 0);
          if (!bool1 && INotificationManager.Stub.getDefaultImpl() != null) {
            param2Long = INotificationManager.Stub.getDefaultImpl().pullStats(param2Long, param2Int, param2Boolean, param2List);
            return param2Long;
          } 
          parcel2.readException();
          param2Long = parcel2.readLong();
          return param2Long;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(INotificationManager param1INotificationManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1INotificationManager != null) {
          Proxy.sDefaultImpl = param1INotificationManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static INotificationManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
