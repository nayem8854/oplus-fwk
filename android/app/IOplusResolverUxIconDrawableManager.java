package android.app;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;

public interface IOplusResolverUxIconDrawableManager {
  public static final IOplusResolverUxIconDrawableManager DEFAULT = (IOplusResolverUxIconDrawableManager)new Object();
  
  default Drawable getDrawable(PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo) {
    return paramPackageManager.getDrawable(paramString, paramInt, paramApplicationInfo);
  }
}
