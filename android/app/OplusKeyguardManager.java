package android.app;

public class OplusKeyguardManager {
  private static final String TAG = "OplusKeyguardManager";
  
  public void registerKeyguardCallback(IKeyguardApp paramIKeyguardApp, String paramString) {}
  
  private boolean isSyncCommand(String paramString) {
    return false;
  }
  
  private void scheduleArriveSyncCommand(String paramString) {}
  
  private void scheduleArriveCommand(String paramString) {}
  
  private void handleCommand(String paramString) {}
  
  public void requestKeyguard(String paramString) {}
  
  public static interface IKeyguardApp {
    void onCommand(String param1String);
    
    void onSyncCommand(String param1String);
  }
}
