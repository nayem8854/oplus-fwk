package android.app;

import android.content.Intent;
import android.content.pm.IPackageInstallObserver2;
import android.os.Bundle;

public class PackageInstallObserver {
  private final IPackageInstallObserver2.Stub mBinder = (IPackageInstallObserver2.Stub)new Object(this);
  
  public IPackageInstallObserver2 getBinder() {
    return this.mBinder;
  }
  
  public void onUserActionRequired(Intent paramIntent) {}
  
  public void onPackageInstalled(String paramString1, int paramInt, String paramString2, Bundle paramBundle) {}
}
