package android.app.prediction;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class AppTargetEvent implements Parcelable {
  public static final int ACTION_DISMISS = 2;
  
  public static final int ACTION_LAUNCH = 1;
  
  public static final int ACTION_PIN = 3;
  
  public static final int ACTION_UNPIN = 4;
  
  private AppTargetEvent(AppTarget paramAppTarget, String paramString, int paramInt) {
    this.mTarget = paramAppTarget;
    this.mLocation = paramString;
    this.mAction = paramInt;
  }
  
  private AppTargetEvent(Parcel paramParcel) {
    this.mTarget = (AppTarget)paramParcel.readParcelable(null);
    this.mLocation = paramParcel.readString();
    this.mAction = paramParcel.readInt();
  }
  
  public AppTarget getTarget() {
    return this.mTarget;
  }
  
  public String getLaunchLocation() {
    return this.mLocation;
  }
  
  public int getAction() {
    return this.mAction;
  }
  
  public boolean equals(Object paramObject) {
    String str;
    Class<?> clazz = getClass();
    if (paramObject != null) {
      str = (String)paramObject.getClass();
    } else {
      str = null;
    } 
    boolean bool = clazz.equals(str);
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (this.mTarget.equals(((AppTargetEvent)paramObject).mTarget)) {
      String str1 = this.mLocation;
      str = ((AppTargetEvent)paramObject).mLocation;
      if (str1.equals(str) && this.mAction == ((AppTargetEvent)paramObject).mAction)
        bool1 = true; 
    } 
    return bool1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mTarget, 0);
    paramParcel.writeString(this.mLocation);
    paramParcel.writeInt(this.mAction);
  }
  
  public static final Parcelable.Creator<AppTargetEvent> CREATOR = new Parcelable.Creator<AppTargetEvent>() {
      public AppTargetEvent createFromParcel(Parcel param1Parcel) {
        return new AppTargetEvent(param1Parcel);
      }
      
      public AppTargetEvent[] newArray(int param1Int) {
        return new AppTargetEvent[param1Int];
      }
    };
  
  private final int mAction;
  
  private final String mLocation;
  
  private final AppTarget mTarget;
  
  @Retention(RetentionPolicy.SOURCE)
  class ActionType implements Annotation {}
  
  @SystemApi
  class Builder {
    private int mAction;
    
    private String mLocation;
    
    private AppTarget mTarget;
    
    public Builder(AppTargetEvent this$0, int param1Int) {
      this.mTarget = (AppTarget)this$0;
      this.mAction = param1Int;
    }
    
    public Builder setLaunchLocation(String param1String) {
      this.mLocation = param1String;
      return this;
    }
    
    public AppTargetEvent build() {
      return new AppTargetEvent(this.mTarget, this.mLocation, this.mAction);
    }
  }
}
