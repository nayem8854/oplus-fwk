package android.app.prediction;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class AppPredictionContext implements Parcelable {
  private AppPredictionContext(String paramString1, int paramInt, String paramString2, Bundle paramBundle) {
    this.mUiSurface = paramString1;
    this.mPredictedTargetCount = paramInt;
    this.mPackageName = paramString2;
    this.mExtras = paramBundle;
  }
  
  private AppPredictionContext(Parcel paramParcel) {
    this.mUiSurface = paramParcel.readString();
    this.mPredictedTargetCount = paramParcel.readInt();
    this.mPackageName = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
  }
  
  public String getUiSurface() {
    return this.mUiSurface;
  }
  
  public int getPredictedTargetCount() {
    return this.mPredictedTargetCount;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public boolean equals(Object paramObject) {
    String str;
    null = true;
    if (paramObject == this)
      return true; 
    Class<?> clazz = getClass();
    if (paramObject != null) {
      str = (String)paramObject.getClass();
    } else {
      str = null;
    } 
    if (!clazz.equals(str))
      return false; 
    paramObject = paramObject;
    if (this.mPredictedTargetCount == ((AppPredictionContext)paramObject).mPredictedTargetCount) {
      String str1 = this.mUiSurface;
      str = ((AppPredictionContext)paramObject).mUiSurface;
      if (str1.equals(str)) {
        str = this.mPackageName;
        paramObject = ((AppPredictionContext)paramObject).mPackageName;
        if (str.equals(paramObject))
          return null; 
      } 
    } 
    return false;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mUiSurface);
    paramParcel.writeInt(this.mPredictedTargetCount);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<AppPredictionContext> CREATOR = new Parcelable.Creator<AppPredictionContext>() {
      public AppPredictionContext createFromParcel(Parcel param1Parcel) {
        return new AppPredictionContext(param1Parcel);
      }
      
      public AppPredictionContext[] newArray(int param1Int) {
        return new AppPredictionContext[param1Int];
      }
    };
  
  private final Bundle mExtras;
  
  private final String mPackageName;
  
  private final int mPredictedTargetCount;
  
  private final String mUiSurface;
  
  @SystemApi
  class Builder {
    private Bundle mExtras;
    
    private final String mPackageName;
    
    private int mPredictedTargetCount;
    
    private String mUiSurface;
    
    @SystemApi
    public Builder(AppPredictionContext this$0) {
      this.mPackageName = this$0.getPackageName();
    }
    
    public Builder setPredictedTargetCount(int param1Int) {
      this.mPredictedTargetCount = param1Int;
      return this;
    }
    
    public Builder setUiSurface(String param1String) {
      this.mUiSurface = param1String;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public AppPredictionContext build() {
      return new AppPredictionContext(this.mUiSurface, this.mPredictedTargetCount, this.mPackageName, this.mExtras);
    }
  }
}
