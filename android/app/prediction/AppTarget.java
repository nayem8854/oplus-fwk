package android.app.prediction;

import android.annotation.SystemApi;
import android.content.pm.ShortcutInfo;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import java.util.Objects;

@SystemApi
public final class AppTarget implements Parcelable {
  @Deprecated
  public AppTarget(AppTargetId paramAppTargetId, String paramString1, String paramString2, UserHandle paramUserHandle) {
    this.mId = paramAppTargetId;
    this.mShortcutInfo = null;
    Objects.requireNonNull(paramString1);
    this.mPackageName = paramString1;
    this.mClassName = paramString2;
    Objects.requireNonNull(paramUserHandle);
    this.mUser = paramUserHandle;
    this.mRank = 0;
  }
  
  @Deprecated
  public AppTarget(AppTargetId paramAppTargetId, ShortcutInfo paramShortcutInfo, String paramString) {
    this.mId = paramAppTargetId;
    Objects.requireNonNull(paramShortcutInfo);
    ShortcutInfo shortcutInfo = paramShortcutInfo;
    this.mPackageName = shortcutInfo.getPackage();
    this.mUser = this.mShortcutInfo.getUserHandle();
    this.mClassName = paramString;
    this.mRank = 0;
  }
  
  private AppTarget(AppTargetId paramAppTargetId, String paramString1, UserHandle paramUserHandle, ShortcutInfo paramShortcutInfo, String paramString2, int paramInt) {
    this.mId = paramAppTargetId;
    this.mShortcutInfo = paramShortcutInfo;
    this.mPackageName = paramString1;
    this.mClassName = paramString2;
    this.mUser = paramUserHandle;
    this.mRank = paramInt;
  }
  
  private AppTarget(Parcel paramParcel) {
    this.mId = (AppTargetId)paramParcel.readTypedObject(AppTargetId.CREATOR);
    ShortcutInfo shortcutInfo = (ShortcutInfo)paramParcel.readTypedObject(ShortcutInfo.CREATOR);
    if (shortcutInfo == null) {
      this.mPackageName = paramParcel.readString();
      this.mUser = UserHandle.of(paramParcel.readInt());
    } else {
      this.mPackageName = shortcutInfo.getPackage();
      this.mUser = this.mShortcutInfo.getUserHandle();
    } 
    this.mClassName = paramParcel.readString();
    this.mRank = paramParcel.readInt();
  }
  
  public AppTargetId getId() {
    return this.mId;
  }
  
  public String getClassName() {
    return this.mClassName;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public UserHandle getUser() {
    return this.mUser;
  }
  
  public ShortcutInfo getShortcutInfo() {
    return this.mShortcutInfo;
  }
  
  public int getRank() {
    return this.mRank;
  }
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getClass : ()Ljava/lang/Class;
    //   4: astore_2
    //   5: aload_1
    //   6: ifnull -> 17
    //   9: aload_1
    //   10: invokevirtual getClass : ()Ljava/lang/Class;
    //   13: astore_3
    //   14: goto -> 19
    //   17: aconst_null
    //   18: astore_3
    //   19: aload_2
    //   20: aload_3
    //   21: invokevirtual equals : (Ljava/lang/Object;)Z
    //   24: istore #4
    //   26: iconst_0
    //   27: istore #5
    //   29: iload #4
    //   31: ifne -> 36
    //   34: iconst_0
    //   35: ireturn
    //   36: aload_1
    //   37: checkcast android/app/prediction/AppTarget
    //   40: astore_1
    //   41: aload_0
    //   42: getfield mClassName : Ljava/lang/String;
    //   45: ifnonnull -> 55
    //   48: aload_1
    //   49: getfield mClassName : Ljava/lang/String;
    //   52: ifnull -> 77
    //   55: aload_0
    //   56: getfield mClassName : Ljava/lang/String;
    //   59: astore_2
    //   60: aload_2
    //   61: ifnull -> 83
    //   64: aload_1
    //   65: getfield mClassName : Ljava/lang/String;
    //   68: astore_3
    //   69: aload_2
    //   70: aload_3
    //   71: invokevirtual equals : (Ljava/lang/Object;)Z
    //   74: ifeq -> 83
    //   77: iconst_1
    //   78: istore #6
    //   80: goto -> 86
    //   83: iconst_0
    //   84: istore #6
    //   86: aload_0
    //   87: getfield mShortcutInfo : Landroid/content/pm/ShortcutInfo;
    //   90: ifnonnull -> 100
    //   93: aload_1
    //   94: getfield mShortcutInfo : Landroid/content/pm/ShortcutInfo;
    //   97: ifnull -> 130
    //   100: aload_0
    //   101: getfield mShortcutInfo : Landroid/content/pm/ShortcutInfo;
    //   104: astore_3
    //   105: aload_3
    //   106: ifnull -> 136
    //   109: aload_1
    //   110: getfield mShortcutInfo : Landroid/content/pm/ShortcutInfo;
    //   113: ifnull -> 136
    //   116: aload_3
    //   117: invokevirtual getId : ()Ljava/lang/String;
    //   120: aload_1
    //   121: getfield mShortcutInfo : Landroid/content/pm/ShortcutInfo;
    //   124: invokevirtual getId : ()Ljava/lang/String;
    //   127: if_acmpne -> 136
    //   130: iconst_1
    //   131: istore #7
    //   133: goto -> 139
    //   136: iconst_0
    //   137: istore #7
    //   139: aload_0
    //   140: getfield mId : Landroid/app/prediction/AppTargetId;
    //   143: aload_1
    //   144: getfield mId : Landroid/app/prediction/AppTargetId;
    //   147: invokevirtual equals : (Ljava/lang/Object;)Z
    //   150: ifeq -> 216
    //   153: aload_0
    //   154: getfield mPackageName : Ljava/lang/String;
    //   157: astore_3
    //   158: aload_1
    //   159: getfield mPackageName : Ljava/lang/String;
    //   162: astore_2
    //   163: aload_3
    //   164: aload_2
    //   165: invokevirtual equals : (Ljava/lang/Object;)Z
    //   168: ifeq -> 216
    //   171: iload #6
    //   173: ifeq -> 216
    //   176: aload_0
    //   177: getfield mUser : Landroid/os/UserHandle;
    //   180: astore_2
    //   181: aload_1
    //   182: getfield mUser : Landroid/os/UserHandle;
    //   185: astore_3
    //   186: aload_2
    //   187: aload_3
    //   188: invokevirtual equals : (Ljava/lang/Object;)Z
    //   191: ifeq -> 216
    //   194: iload #7
    //   196: ifeq -> 216
    //   199: aload_0
    //   200: getfield mRank : I
    //   203: aload_1
    //   204: getfield mRank : I
    //   207: if_icmpne -> 216
    //   210: iconst_1
    //   211: istore #5
    //   213: goto -> 216
    //   216: iload #5
    //   218: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #155	-> 0
    //   #157	-> 36
    //   #158	-> 41
    //   #159	-> 69
    //   #160	-> 86
    //   #162	-> 116
    //   #163	-> 139
    //   #164	-> 163
    //   #166	-> 186
    //   #163	-> 216
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject(this.mId, paramInt);
    paramParcel.writeTypedObject(this.mShortcutInfo, paramInt);
    if (this.mShortcutInfo == null) {
      paramParcel.writeString(this.mPackageName);
      paramParcel.writeInt(this.mUser.getIdentifier());
    } 
    paramParcel.writeString(this.mClassName);
    paramParcel.writeInt(this.mRank);
  }
  
  @SystemApi
  class Builder {
    private String mClassName;
    
    private final AppTargetId mId;
    
    private String mPackageName;
    
    private int mRank;
    
    private ShortcutInfo mShortcutInfo;
    
    private UserHandle mUser;
    
    @SystemApi
    @Deprecated
    public Builder(AppTarget this$0) {
      this.mId = (AppTargetId)this$0;
    }
    
    @SystemApi
    public Builder(AppTarget this$0, String param1String, UserHandle param1UserHandle) {
      Objects.requireNonNull(this$0);
      this.mId = (AppTargetId)this$0;
      Objects.requireNonNull(param1String);
      this.mPackageName = param1String;
      Objects.requireNonNull(param1UserHandle);
      this.mUser = param1UserHandle;
    }
    
    @SystemApi
    public Builder(AppTarget this$0, ShortcutInfo param1ShortcutInfo) {
      Objects.requireNonNull(this$0);
      this.mId = (AppTargetId)this$0;
      Objects.requireNonNull(param1ShortcutInfo);
      this.mShortcutInfo = param1ShortcutInfo;
      this.mPackageName = param1ShortcutInfo.getPackage();
      this.mUser = param1ShortcutInfo.getUserHandle();
    }
    
    @Deprecated
    public Builder setTarget(String param1String, UserHandle param1UserHandle) {
      if (this.mPackageName == null) {
        Objects.requireNonNull(param1String);
        this.mPackageName = param1String;
        Objects.requireNonNull(param1UserHandle);
        this.mUser = param1UserHandle;
        return this;
      } 
      throw new IllegalArgumentException("Target is already set");
    }
    
    @Deprecated
    public Builder setTarget(ShortcutInfo param1ShortcutInfo) {
      setTarget(param1ShortcutInfo.getPackage(), param1ShortcutInfo.getUserHandle());
      Objects.requireNonNull(param1ShortcutInfo);
      this.mShortcutInfo = param1ShortcutInfo;
      return this;
    }
    
    public Builder setClassName(String param1String) {
      Objects.requireNonNull(param1String);
      this.mClassName = param1String;
      return this;
    }
    
    public Builder setRank(int param1Int) {
      if (param1Int >= 0) {
        this.mRank = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("rank cannot be a negative value");
    }
    
    public AppTarget build() {
      if (this.mPackageName != null)
        return new AppTarget(this.mId, this.mPackageName, this.mUser, this.mShortcutInfo, this.mClassName, this.mRank); 
      throw new IllegalStateException("No target is set");
    }
  }
  
  public static final Parcelable.Creator<AppTarget> CREATOR = new Parcelable.Creator<AppTarget>() {
      public AppTarget createFromParcel(Parcel param1Parcel) {
        return new AppTarget(param1Parcel);
      }
      
      public AppTarget[] newArray(int param1Int) {
        return new AppTarget[param1Int];
      }
    };
  
  private final String mClassName;
  
  private final AppTargetId mId;
  
  private final String mPackageName;
  
  private final int mRank;
  
  private final ShortcutInfo mShortcutInfo;
  
  private final UserHandle mUser;
}
