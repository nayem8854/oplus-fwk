package android.app.prediction;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class AppTargetId implements Parcelable {
  @SystemApi
  public AppTargetId(String paramString) {
    this.mId = paramString;
  }
  
  private AppTargetId(Parcel paramParcel) {
    this.mId = paramParcel.readString();
  }
  
  public String getId() {
    return this.mId;
  }
  
  public boolean equals(Object paramObject) {
    Object object;
    Class<?> clazz = getClass();
    if (paramObject != null) {
      object = paramObject.getClass();
    } else {
      object = null;
    } 
    if (!clazz.equals(object))
      return false; 
    paramObject = paramObject;
    return this.mId.equals(((AppTargetId)paramObject).mId);
  }
  
  public int hashCode() {
    return this.mId.hashCode();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
  }
  
  public static final Parcelable.Creator<AppTargetId> CREATOR = new Parcelable.Creator<AppTargetId>() {
      public AppTargetId createFromParcel(Parcel param1Parcel) {
        return new AppTargetId(param1Parcel);
      }
      
      public AppTargetId[] newArray(int param1Int) {
        return new AppTargetId[param1Int];
      }
    };
  
  private final String mId;
}
