package android.app;

import android.os.RemoteException;
import com.oplus.favorite.IOplusFavoriteQueryCallback;

public interface IOplusDirectActivityManager {
  void favoriteQueryRule(String paramString, IOplusFavoriteQueryCallback paramIOplusFavoriteQueryCallback) throws RemoteException;
}
