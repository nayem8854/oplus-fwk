package android.app;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

@Deprecated
public class ExpandableListActivity extends Activity implements View.OnCreateContextMenuListener, ExpandableListView.OnChildClickListener, ExpandableListView.OnGroupCollapseListener, ExpandableListView.OnGroupExpandListener {
  ExpandableListAdapter mAdapter;
  
  boolean mFinishedStart = false;
  
  ExpandableListView mList;
  
  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo) {}
  
  public boolean onChildClick(ExpandableListView paramExpandableListView, View paramView, int paramInt1, int paramInt2, long paramLong) {
    return false;
  }
  
  public void onGroupCollapse(int paramInt) {}
  
  public void onGroupExpand(int paramInt) {}
  
  protected void onRestoreInstanceState(Bundle paramBundle) {
    ensureList();
    super.onRestoreInstanceState(paramBundle);
  }
  
  public void onContentChanged() {
    super.onContentChanged();
    View view = (View)findViewById(16908292);
    ExpandableListView expandableListView = findViewById(16908298);
    if (expandableListView != null) {
      if (view != null)
        expandableListView.setEmptyView(view); 
      this.mList.setOnChildClickListener(this);
      this.mList.setOnGroupExpandListener(this);
      this.mList.setOnGroupCollapseListener(this);
      if (this.mFinishedStart)
        setListAdapter(this.mAdapter); 
      this.mFinishedStart = true;
      return;
    } 
    throw new RuntimeException("Your content must have a ExpandableListView whose id attribute is 'android.R.id.list'");
  }
  
  public void setListAdapter(ExpandableListAdapter paramExpandableListAdapter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial ensureList : ()V
    //   6: aload_0
    //   7: aload_1
    //   8: putfield mAdapter : Landroid/widget/ExpandableListAdapter;
    //   11: aload_0
    //   12: getfield mList : Landroid/widget/ExpandableListView;
    //   15: aload_1
    //   16: invokevirtual setAdapter : (Landroid/widget/ExpandableListAdapter;)V
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #246	-> 0
    //   #247	-> 2
    //   #248	-> 6
    //   #249	-> 11
    //   #250	-> 19
    //   #251	-> 21
    //   #250	-> 22
    // Exception table:
    //   from	to	target	type
    //   2	6	22	finally
    //   6	11	22	finally
    //   11	19	22	finally
    //   19	21	22	finally
    //   23	25	22	finally
  }
  
  public ExpandableListView getExpandableListView() {
    ensureList();
    return this.mList;
  }
  
  public ExpandableListAdapter getExpandableListAdapter() {
    return this.mAdapter;
  }
  
  private void ensureList() {
    if (this.mList != null)
      return; 
    setContentView(17367041);
  }
  
  public long getSelectedId() {
    return this.mList.getSelectedId();
  }
  
  public long getSelectedPosition() {
    return this.mList.getSelectedPosition();
  }
  
  public boolean setSelectedChild(int paramInt1, int paramInt2, boolean paramBoolean) {
    return this.mList.setSelectedChild(paramInt1, paramInt2, paramBoolean);
  }
  
  public void setSelectedGroup(int paramInt) {
    this.mList.setSelectedGroup(paramInt);
  }
}
