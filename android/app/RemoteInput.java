package android.app;

import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public final class RemoteInput implements Parcelable {
  private RemoteInput(String paramString, CharSequence paramCharSequence, CharSequence[] paramArrayOfCharSequence, int paramInt1, int paramInt2, Bundle paramBundle, ArraySet<String> paramArraySet) {
    this.mResultKey = paramString;
    this.mLabel = paramCharSequence;
    this.mChoices = paramArrayOfCharSequence;
    this.mFlags = paramInt1;
    this.mEditChoicesBeforeSending = paramInt2;
    this.mExtras = paramBundle;
    this.mAllowedDataTypes = paramArraySet;
    if (getEditChoicesBeforeSending() != 2 || 
      getAllowFreeFormInput())
      return; 
    throw new IllegalArgumentException("setEditChoicesBeforeSending requires setAllowFreeFormInput");
  }
  
  public String getResultKey() {
    return this.mResultKey;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public CharSequence[] getChoices() {
    return this.mChoices;
  }
  
  public Set<String> getAllowedDataTypes() {
    return (Set<String>)this.mAllowedDataTypes;
  }
  
  public boolean isDataOnly() {
    boolean bool;
    if (!getAllowFreeFormInput() && (
      getChoices() == null || (getChoices()).length == 0) && 
      !getAllowedDataTypes().isEmpty()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean getAllowFreeFormInput() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public int getEditChoicesBeforeSending() {
    return this.mEditChoicesBeforeSending;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Source implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EditChoicesBeforeSending implements Annotation {}
  
  class Builder {
    private final ArraySet<String> mAllowedDataTypes = new ArraySet();
    
    private final Bundle mExtras = new Bundle();
    
    private int mFlags = 1;
    
    private int mEditChoicesBeforeSending = 0;
    
    private CharSequence[] mChoices;
    
    private CharSequence mLabel;
    
    private final String mResultKey;
    
    public Builder(RemoteInput this$0) {
      if (this$0 != null) {
        this.mResultKey = (String)this$0;
        return;
      } 
      throw new IllegalArgumentException("Result key can't be null");
    }
    
    public Builder setLabel(CharSequence param1CharSequence) {
      this.mLabel = Notification.safeCharSequence(param1CharSequence);
      return this;
    }
    
    public Builder setChoices(CharSequence[] param1ArrayOfCharSequence) {
      if (param1ArrayOfCharSequence == null) {
        this.mChoices = null;
      } else {
        this.mChoices = new CharSequence[param1ArrayOfCharSequence.length];
        for (byte b = 0; b < param1ArrayOfCharSequence.length; b++)
          this.mChoices[b] = Notification.safeCharSequence(param1ArrayOfCharSequence[b]); 
      } 
      return this;
    }
    
    public Builder setAllowDataType(String param1String, boolean param1Boolean) {
      if (param1Boolean) {
        this.mAllowedDataTypes.add(param1String);
      } else {
        this.mAllowedDataTypes.remove(param1String);
      } 
      return this;
    }
    
    public Builder setAllowFreeFormInput(boolean param1Boolean) {
      setFlag(1, param1Boolean);
      return this;
    }
    
    public Builder setEditChoicesBeforeSending(int param1Int) {
      this.mEditChoicesBeforeSending = param1Int;
      return this;
    }
    
    public Builder addExtras(Bundle param1Bundle) {
      if (param1Bundle != null)
        this.mExtras.putAll(param1Bundle); 
      return this;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    private void setFlag(int param1Int, boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= param1Int;
      } else {
        this.mFlags &= param1Int ^ 0xFFFFFFFF;
      } 
    }
    
    public RemoteInput build() {
      return new RemoteInput(this.mResultKey, this.mLabel, this.mChoices, this.mFlags, this.mEditChoicesBeforeSending, this.mExtras, this.mAllowedDataTypes);
    }
  }
  
  private RemoteInput(Parcel paramParcel) {
    this.mResultKey = paramParcel.readString();
    this.mLabel = paramParcel.readCharSequence();
    this.mChoices = paramParcel.readCharSequenceArray();
    this.mFlags = paramParcel.readInt();
    this.mEditChoicesBeforeSending = paramParcel.readInt();
    this.mExtras = paramParcel.readBundle();
    this.mAllowedDataTypes = paramParcel.readArraySet(null);
  }
  
  public static Map<String, Uri> getDataResultsFromIntent(Intent paramIntent, String paramString) {
    Intent intent = getClipDataIntentFromIntent(paramIntent);
    HashMap hashMap1 = null;
    if (intent == null)
      return null; 
    HashMap<Object, Object> hashMap = new HashMap<>();
    Bundle bundle = intent.getExtras();
    for (String str : bundle.keySet()) {
      if (str.startsWith("android.remoteinput.dataTypeResultsData")) {
        String str2 = str.substring("android.remoteinput.dataTypeResultsData".length());
        if (str2 == null || str2.isEmpty())
          continue; 
        Bundle bundle1 = intent.getBundleExtra(str);
        String str1 = bundle1.getString(paramString);
        if (str1 == null || str1.isEmpty())
          continue; 
        hashMap.put(str2, Uri.parse(str1));
      } 
    } 
    if (hashMap.isEmpty())
      hashMap = hashMap1; 
    return (Map)hashMap;
  }
  
  public static Bundle getResultsFromIntent(Intent paramIntent) {
    paramIntent = getClipDataIntentFromIntent(paramIntent);
    if (paramIntent == null)
      return null; 
    return (Bundle)paramIntent.getExtras().getParcelable("android.remoteinput.resultsData");
  }
  
  public static void addResultsToIntent(RemoteInput[] paramArrayOfRemoteInput, Intent paramIntent, Bundle paramBundle) {
    Intent intent1 = getClipDataIntentFromIntent(paramIntent);
    Intent intent2 = intent1;
    if (intent1 == null)
      intent2 = new Intent(); 
    Bundle bundle2 = intent2.getBundleExtra("android.remoteinput.resultsData");
    Bundle bundle1 = bundle2;
    if (bundle2 == null)
      bundle1 = new Bundle(); 
    int i;
    byte b;
    for (i = paramArrayOfRemoteInput.length, b = 0; b < i; ) {
      RemoteInput remoteInput = paramArrayOfRemoteInput[b];
      Object object = paramBundle.get(remoteInput.getResultKey());
      if (object instanceof CharSequence)
        bundle1.putCharSequence(remoteInput.getResultKey(), (CharSequence)object); 
      b++;
    } 
    intent2.putExtra("android.remoteinput.resultsData", bundle1);
    paramIntent.setClipData(ClipData.newIntent("android.remoteinput.results", intent2));
  }
  
  public static void addDataResultToIntent(RemoteInput paramRemoteInput, Intent paramIntent, Map<String, Uri> paramMap) {
    Intent intent1 = getClipDataIntentFromIntent(paramIntent);
    Intent intent2 = intent1;
    if (intent1 == null)
      intent2 = new Intent(); 
    for (Map.Entry<String, Uri> entry : paramMap.entrySet()) {
      String str = (String)entry.getKey();
      Uri uri = (Uri)entry.getValue();
      if (str == null)
        continue; 
      Bundle bundle2 = intent2.getBundleExtra(getExtraResultsKeyForData(str));
      Bundle bundle1 = bundle2;
      if (bundle2 == null)
        bundle1 = new Bundle(); 
      bundle1.putString(paramRemoteInput.getResultKey(), uri.toString());
      intent2.putExtra(getExtraResultsKeyForData(str), bundle1);
    } 
    paramIntent.setClipData(ClipData.newIntent("android.remoteinput.results", intent2));
  }
  
  public static void setResultsSource(Intent paramIntent, int paramInt) {
    Intent intent1 = getClipDataIntentFromIntent(paramIntent);
    Intent intent2 = intent1;
    if (intent1 == null)
      intent2 = new Intent(); 
    intent2.putExtra("android.remoteinput.resultsSource", paramInt);
    paramIntent.setClipData(ClipData.newIntent("android.remoteinput.results", intent2));
  }
  
  public static int getResultsSource(Intent paramIntent) {
    paramIntent = getClipDataIntentFromIntent(paramIntent);
    if (paramIntent == null)
      return 0; 
    return paramIntent.getExtras().getInt("android.remoteinput.resultsSource", 0);
  }
  
  private static String getExtraResultsKeyForData(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("android.remoteinput.dataTypeResultsData");
    stringBuilder.append(paramString);
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mResultKey);
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeCharSequenceArray(this.mChoices);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mEditChoicesBeforeSending);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeArraySet(this.mAllowedDataTypes);
  }
  
  public static final Parcelable.Creator<RemoteInput> CREATOR = new Parcelable.Creator<RemoteInput>() {
      public RemoteInput createFromParcel(Parcel param1Parcel) {
        return new RemoteInput(param1Parcel);
      }
      
      public RemoteInput[] newArray(int param1Int) {
        return new RemoteInput[param1Int];
      }
    };
  
  private static final int DEFAULT_FLAGS = 1;
  
  public static final int EDIT_CHOICES_BEFORE_SENDING_AUTO = 0;
  
  public static final int EDIT_CHOICES_BEFORE_SENDING_DISABLED = 1;
  
  public static final int EDIT_CHOICES_BEFORE_SENDING_ENABLED = 2;
  
  private static final String EXTRA_DATA_TYPE_RESULTS_DATA = "android.remoteinput.dataTypeResultsData";
  
  public static final String EXTRA_RESULTS_DATA = "android.remoteinput.resultsData";
  
  private static final String EXTRA_RESULTS_SOURCE = "android.remoteinput.resultsSource";
  
  private static final int FLAG_ALLOW_FREE_FORM_INPUT = 1;
  
  public static final String RESULTS_CLIP_LABEL = "android.remoteinput.results";
  
  public static final int SOURCE_CHOICE = 1;
  
  public static final int SOURCE_FREE_FORM_INPUT = 0;
  
  private final ArraySet<String> mAllowedDataTypes;
  
  private final CharSequence[] mChoices;
  
  private final int mEditChoicesBeforeSending;
  
  private final Bundle mExtras;
  
  private final int mFlags;
  
  private final CharSequence mLabel;
  
  private final String mResultKey;
  
  private static Intent getClipDataIntentFromIntent(Intent paramIntent) {
    ClipData clipData = paramIntent.getClipData();
    if (clipData == null)
      return null; 
    ClipDescription clipDescription = clipData.getDescription();
    if (!clipDescription.hasMimeType("text/vnd.android.intent"))
      return null; 
    if (!clipDescription.getLabel().equals("android.remoteinput.results"))
      return null; 
    return clipData.getItemAt(0).getIntent();
  }
}
