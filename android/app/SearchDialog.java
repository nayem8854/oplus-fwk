package android.app;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SearchView;
import android.widget.TextView;

public class SearchDialog extends Dialog {
  private static final boolean DBG = false;
  
  private static final String IME_OPTION_NO_MICROPHONE = "nm";
  
  private static final String INSTANCE_KEY_APPDATA = "data";
  
  private static final String INSTANCE_KEY_COMPONENT = "comp";
  
  private static final String INSTANCE_KEY_USER_QUERY = "uQry";
  
  private static final String LOG_TAG = "SearchDialog";
  
  private static final int SEARCH_PLATE_LEFT_PADDING_NON_GLOBAL = 7;
  
  private Context mActivityContext;
  
  private ImageView mAppIcon;
  
  private Bundle mAppSearchData;
  
  private TextView mBadgeLabel;
  
  private View mCloseSearch;
  
  private BroadcastReceiver mConfChangeListener = (BroadcastReceiver)new Object(this);
  
  private ComponentName mLaunchComponent;
  
  private final SearchView.OnCloseListener mOnCloseListener;
  
  private final SearchView.OnQueryTextListener mOnQueryChangeListener;
  
  private final SearchView.OnSuggestionListener mOnSuggestionSelectionListener;
  
  private AutoCompleteTextView mSearchAutoComplete;
  
  private int mSearchAutoCompleteImeOptions;
  
  private View mSearchPlate;
  
  private SearchView mSearchView;
  
  private SearchableInfo mSearchable;
  
  private String mUserQuery;
  
  private final Intent mVoiceAppSearchIntent;
  
  private final Intent mVoiceWebSearchIntent;
  
  private Drawable mWorkingSpinner;
  
  static int resolveDialogTheme(Context paramContext) {
    TypedValue typedValue = new TypedValue();
    paramContext.getTheme().resolveAttribute(17957067, typedValue, true);
    return typedValue.resourceId;
  }
  
  public SearchDialog(Context paramContext, SearchManager paramSearchManager) {
    super(paramContext, resolveDialogTheme(paramContext));
    this.mOnCloseListener = (SearchView.OnCloseListener)new Object(this);
    this.mOnQueryChangeListener = (SearchView.OnQueryTextListener)new Object(this);
    this.mOnSuggestionSelectionListener = (SearchView.OnSuggestionListener)new Object(this);
    Intent intent = new Intent("android.speech.action.WEB_SEARCH");
    intent.addFlags(268435456);
    this.mVoiceWebSearchIntent.putExtra("android.speech.extra.LANGUAGE_MODEL", "web_search");
    this.mVoiceAppSearchIntent = intent = new Intent("android.speech.action.RECOGNIZE_SPEECH");
    intent.addFlags(268435456);
  }
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    Window window = getWindow();
    WindowManager.LayoutParams layoutParams = window.getAttributes();
    layoutParams.width = -1;
    layoutParams.height = -1;
    layoutParams.gravity = 55;
    layoutParams.softInputMode = 16;
    window.setAttributes(layoutParams);
    setCanceledOnTouchOutside(true);
  }
  
  private void createContentView() {
    setContentView(17367289);
    SearchView searchView = findViewById(16909399);
    searchView.setIconified(false);
    this.mSearchView.setOnCloseListener(this.mOnCloseListener);
    this.mSearchView.setOnQueryTextListener(this.mOnQueryChangeListener);
    this.mSearchView.setOnSuggestionListener(this.mOnSuggestionSelectionListener);
    this.mSearchView.onActionViewExpanded();
    this.mCloseSearch = (View)(searchView = findViewById(16908327));
    searchView.setOnClickListener((View.OnClickListener)new Object(this));
    this.mBadgeLabel = (TextView)this.mSearchView.findViewById(16909390);
    searchView = this.mSearchView;
    this.mSearchAutoComplete = (AutoCompleteTextView)searchView.findViewById(16909398);
    this.mAppIcon = findViewById(16909389);
    this.mSearchPlate = this.mSearchView.findViewById(16909397);
    this.mWorkingSpinner = getContext().getDrawable(17303464);
    setWorking(false);
    this.mBadgeLabel.setVisibility(8);
    this.mSearchAutoCompleteImeOptions = this.mSearchAutoComplete.getImeOptions();
  }
  
  public boolean show(String paramString, boolean paramBoolean, ComponentName paramComponentName, Bundle paramBundle) {
    paramBoolean = doShow(paramString, paramBoolean, paramComponentName, paramBundle);
    if (paramBoolean)
      this.mSearchAutoComplete.showDropDownAfterLayout(); 
    return paramBoolean;
  }
  
  private boolean doShow(String paramString, boolean paramBoolean, ComponentName paramComponentName, Bundle paramBundle) {
    if (!show(paramComponentName, paramBundle))
      return false; 
    setUserQuery(paramString);
    if (paramBoolean)
      this.mSearchAutoComplete.selectAll(); 
    return true;
  }
  
  private boolean show(ComponentName paramComponentName, Bundle paramBundle) {
    Context context = this.mContext;
    SearchManager searchManager = (SearchManager)context.getSystemService("search");
    SearchableInfo searchableInfo = searchManager.getSearchableInfo(paramComponentName);
    if (searchableInfo == null)
      return false; 
    this.mLaunchComponent = paramComponentName;
    this.mAppSearchData = paramBundle;
    this.mActivityContext = searchableInfo.getActivityContext(getContext());
    if (!isShowing()) {
      createContentView();
      this.mSearchView.setSearchableInfo(this.mSearchable);
      this.mSearchView.setAppSearchData(this.mAppSearchData);
      show();
    } 
    updateUI();
    return true;
  }
  
  public void onStart() {
    super.onStart();
    IntentFilter intentFilter = new IntentFilter();
    intentFilter.addAction("android.intent.action.CONFIGURATION_CHANGED");
    getContext().registerReceiver(this.mConfChangeListener, intentFilter);
  }
  
  public void onStop() {
    super.onStop();
    getContext().unregisterReceiver(this.mConfChangeListener);
    this.mLaunchComponent = null;
    this.mAppSearchData = null;
    this.mSearchable = null;
    this.mUserQuery = null;
  }
  
  public void setWorking(boolean paramBoolean) {
    boolean bool;
    Drawable drawable = this.mWorkingSpinner;
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    drawable.setAlpha(bool);
    this.mWorkingSpinner.setVisible(paramBoolean, false);
    this.mWorkingSpinner.invalidateSelf();
  }
  
  public Bundle onSaveInstanceState() {
    if (!isShowing())
      return null; 
    Bundle bundle = new Bundle();
    bundle.putParcelable("comp", this.mLaunchComponent);
    bundle.putBundle("data", this.mAppSearchData);
    bundle.putString("uQry", this.mUserQuery);
    return bundle;
  }
  
  public void onRestoreInstanceState(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    ComponentName componentName = (ComponentName)paramBundle.getParcelable("comp");
    Bundle bundle = paramBundle.getBundle("data");
    String str = paramBundle.getString("uQry");
    if (!doShow(str, false, componentName, bundle))
      return; 
  }
  
  public void onConfigurationChanged() {
    if (this.mSearchable != null && isShowing()) {
      updateSearchAppIcon();
      updateSearchBadge();
      if (isLandscapeMode(getContext())) {
        this.mSearchAutoComplete.setInputMethodMode(1);
        if (this.mSearchAutoComplete.isDropDownAlwaysVisible() || enoughToFilter())
          this.mSearchAutoComplete.showDropDown(); 
      } 
    } 
  }
  
  static boolean isLandscapeMode(Context paramContext) {
    boolean bool;
    if ((paramContext.getResources().getConfiguration()).orientation == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private boolean enoughToFilter() {
    Filterable filterable = (Filterable)this.mSearchAutoComplete.getAdapter();
    if (filterable == null || filterable.getFilter() == null)
      return false; 
    return this.mSearchAutoComplete.enoughToFilter();
  }
  
  private void updateUI() {
    if (this.mSearchable != null) {
      this.mDecor.setVisibility(0);
      updateSearchAutoComplete();
      updateSearchAppIcon();
      updateSearchBadge();
      int i = this.mSearchable.getInputType();
      int j = i;
      if ((i & 0xF) == 1) {
        i &= 0xFFFEFFFF;
        j = i;
        if (this.mSearchable.getSuggestAuthority() != null)
          j = i | 0x10000; 
      } 
      this.mSearchAutoComplete.setInputType(j);
      this.mSearchAutoCompleteImeOptions = j = this.mSearchable.getImeOptions();
      this.mSearchAutoComplete.setImeOptions(j);
      if (this.mSearchable.getVoiceSearchEnabled()) {
        this.mSearchAutoComplete.setPrivateImeOptions("nm");
      } else {
        this.mSearchAutoComplete.setPrivateImeOptions(null);
      } 
    } 
  }
  
  private void updateSearchAutoComplete() {
    this.mSearchAutoComplete.setDropDownDismissedOnCompletion(false);
    this.mSearchAutoComplete.setForceIgnoreOutsideTouch(false);
  }
  
  private void updateSearchAppIcon() {
    Drawable drawable;
    PackageManager packageManager = getContext().getPackageManager();
    try {
      ActivityInfo activityInfo = packageManager.getActivityInfo(this.mLaunchComponent, 0);
      drawable = packageManager.getApplicationIcon(activityInfo.applicationInfo);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      drawable = packageManager.getDefaultActivityIcon();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mLaunchComponent);
      stringBuilder.append(" not found, using generic app icon");
      Log.w("SearchDialog", stringBuilder.toString());
    } 
    this.mAppIcon.setImageDrawable(drawable);
    this.mAppIcon.setVisibility(0);
    View view = this.mSearchPlate;
    view.setPadding(7, view.getPaddingTop(), this.mSearchPlate.getPaddingRight(), this.mSearchPlate.getPaddingBottom());
  }
  
  private void updateSearchBadge() {
    Drawable drawable2;
    byte b = 8;
    Drawable drawable1 = null;
    String str = null;
    if (this.mSearchable.useBadgeIcon()) {
      drawable2 = this.mActivityContext.getDrawable(this.mSearchable.getIconId());
      b = 0;
    } else {
      drawable2 = drawable1;
      if (this.mSearchable.useBadgeLabel()) {
        str = this.mActivityContext.getResources().getText(this.mSearchable.getLabelId()).toString();
        b = 0;
        drawable2 = drawable1;
      } 
    } 
    this.mBadgeLabel.setCompoundDrawablesWithIntrinsicBounds(drawable2, null, null, null);
    this.mBadgeLabel.setText(str);
    this.mBadgeLabel.setVisibility(b);
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (!this.mSearchAutoComplete.isPopupShowing() && isOutOfBounds(this.mSearchPlate, paramMotionEvent)) {
      cancel();
      return true;
    } 
    return super.onTouchEvent(paramMotionEvent);
  }
  
  private boolean isOutOfBounds(View paramView, MotionEvent paramMotionEvent) {
    int i = (int)paramMotionEvent.getX();
    int j = (int)paramMotionEvent.getY();
    int k = ViewConfiguration.get(this.mContext).getScaledWindowTouchSlop();
    return (i < -k || j < -k || i > paramView.getWidth() + k || j > paramView.getHeight() + k);
  }
  
  public void hide() {
    if (!isShowing())
      return; 
    InputMethodManager inputMethodManager = getContext().<InputMethodManager>getSystemService(InputMethodManager.class);
    if (inputMethodManager != null) {
      IBinder iBinder = getWindow().getDecorView().getWindowToken();
      inputMethodManager.hideSoftInputFromWindow(iBinder, 0);
    } 
    super.hide();
  }
  
  public void launchQuerySearch() {
    launchQuerySearch(0, (String)null);
  }
  
  protected void launchQuerySearch(int paramInt, String paramString) {
    String str = this.mSearchAutoComplete.getText().toString();
    Intent intent = createIntent("android.intent.action.SEARCH", (Uri)null, (String)null, str, paramInt, paramString);
    launchIntent(intent);
  }
  
  private void launchIntent(Intent paramIntent) {
    if (paramIntent == null)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("launching ");
    stringBuilder.append(paramIntent);
    Log.d("SearchDialog", stringBuilder.toString());
    try {
      getContext().startActivity(paramIntent);
      dismiss();
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed launch activity: ");
      stringBuilder1.append(paramIntent);
      Log.e("SearchDialog", stringBuilder1.toString(), runtimeException);
    } 
  }
  
  public void setListSelection(int paramInt) {
    this.mSearchAutoComplete.setListSelection(paramInt);
  }
  
  private Intent createIntent(String paramString1, Uri paramUri, String paramString2, String paramString3, int paramInt, String paramString4) {
    Intent intent = new Intent(paramString1);
    intent.addFlags(268435456);
    if (paramUri != null)
      intent.setData(paramUri); 
    intent.putExtra("user_query", this.mUserQuery);
    if (paramString3 != null)
      intent.putExtra("query", paramString3); 
    if (paramString2 != null)
      intent.putExtra("intent_extra_data_key", paramString2); 
    Bundle bundle = this.mAppSearchData;
    if (bundle != null)
      intent.putExtra("app_data", bundle); 
    if (paramInt != 0) {
      intent.putExtra("action_key", paramInt);
      intent.putExtra("action_msg", paramString4);
    } 
    intent.setComponent(this.mSearchable.getSearchActivity());
    return intent;
  }
  
  class SearchBar extends LinearLayout {
    public SearchBar(SearchDialog this$0, AttributeSet param1AttributeSet) {
      super((Context)this$0, param1AttributeSet);
    }
    
    public SearchBar(SearchDialog this$0) {
      super((Context)this$0);
    }
    
    public ActionMode startActionModeForChild(View param1View, ActionMode.Callback param1Callback, int param1Int) {
      if (param1Int != 0)
        return super.startActionModeForChild(param1View, param1Callback, param1Int); 
      return null;
    }
  }
  
  private boolean isEmpty(AutoCompleteTextView paramAutoCompleteTextView) {
    boolean bool;
    if (TextUtils.getTrimmedLength((CharSequence)paramAutoCompleteTextView.getText()) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void onBackPressed() {
    InputMethodManager inputMethodManager = getContext().<InputMethodManager>getSystemService(InputMethodManager.class);
    if (inputMethodManager != null && inputMethodManager.isFullscreenMode() && inputMethodManager.hideSoftInputFromWindow(getWindow().getDecorView().getWindowToken(), 0))
      return; 
    cancel();
  }
  
  private boolean onClosePressed() {
    if (isEmpty(this.mSearchAutoComplete)) {
      dismiss();
      return true;
    } 
    return false;
  }
  
  private void setUserQuery(String paramString) {
    String str = paramString;
    if (paramString == null)
      str = ""; 
    this.mUserQuery = str;
    this.mSearchAutoComplete.setText(str);
    this.mSearchAutoComplete.setSelection(str.length());
  }
}
