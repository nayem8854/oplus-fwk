package android.app.backup;

import android.app.IBackupAgent;
import android.app.QueuedWork;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.pm.ApplicationInfo;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.os.UserHandle;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructStat;
import android.util.ArraySet;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParserException;

public abstract class BackupAgent extends ContextWrapper {
  private static final boolean DEBUG = false;
  
  public static final int FLAG_CLIENT_SIDE_ENCRYPTION_ENABLED = 1;
  
  public static final int FLAG_DEVICE_TO_DEVICE_TRANSFER = 2;
  
  public static final int FLAG_FAKE_CLIENT_SIDE_ENCRYPTION_ENABLED = -2147483648;
  
  public static final int RESULT_ERROR = -1;
  
  public static final int RESULT_SUCCESS = 0;
  
  private static final String TAG = "BackupAgent";
  
  public static final int TYPE_DIRECTORY = 2;
  
  public static final int TYPE_EOF = 0;
  
  public static final int TYPE_FILE = 1;
  
  public static final int TYPE_SYMLINK = 3;
  
  private final IBinder mBinder;
  
  Handler mHandler = null;
  
  private UserHandle mUser;
  
  Handler getHandler() {
    if (this.mHandler == null)
      this.mHandler = new Handler(Looper.getMainLooper()); 
    return this.mHandler;
  }
  
  class SharedPrefsSynchronizer implements Runnable {
    public final CountDownLatch mLatch = new CountDownLatch(1);
    
    final BackupAgent this$0;
    
    public void run() {
      QueuedWork.waitToFinish();
      this.mLatch.countDown();
    }
  }
  
  private void waitForSharedPrefs() {
    Handler handler = getHandler();
    SharedPrefsSynchronizer sharedPrefsSynchronizer = new SharedPrefsSynchronizer();
    handler.postAtFrontOfQueue(sharedPrefsSynchronizer);
    try {
      sharedPrefsSynchronizer.mLatch.await();
    } catch (InterruptedException interruptedException) {}
  }
  
  public BackupAgent() {
    super(null);
    this.mBinder = (new BackupServiceBinder()).asBinder();
  }
  
  public void onCreate() {}
  
  public void onCreate(UserHandle paramUserHandle) {
    onCreate();
    this.mUser = paramUserHandle;
  }
  
  public void onDestroy() {}
  
  public void onRestore(BackupDataInput paramBackupDataInput, long paramLong, ParcelFileDescriptor paramParcelFileDescriptor) throws IOException {
    onRestore(paramBackupDataInput, (int)paramLong, paramParcelFileDescriptor);
  }
  
  public void onRestore(BackupDataInput paramBackupDataInput, long paramLong, ParcelFileDescriptor paramParcelFileDescriptor, Set<String> paramSet) throws IOException {
    onRestore(paramBackupDataInput, paramLong, paramParcelFileDescriptor);
  }
  
  public void onFullBackup(FullBackupDataOutput paramFullBackupDataOutput) throws IOException {
    FullBackup.BackupScheme backupScheme = FullBackup.getBackupScheme(this);
    if (!backupScheme.isFullBackupContentEnabled())
      return; 
    try {
      Map<String, Set<FullBackup.BackupScheme.PathWithRequiredFlags>> map = backupScheme.maybeParseAndGetCanonicalIncludePaths();
      ArraySet<FullBackup.BackupScheme.PathWithRequiredFlags> arraySet = backupScheme.maybeParseAndGetCanonicalExcludePaths();
      String str1 = getPackageName();
      ApplicationInfo applicationInfo = getApplicationInfo();
      Context context1 = createCredentialProtectedStorageContext();
      String str3 = context1.getDataDir().getCanonicalPath();
      String str4 = context1.getFilesDir().getCanonicalPath();
      String str5 = context1.getNoBackupFilesDir().getCanonicalPath();
      File file1 = context1.getDatabasePath("foo").getParentFile();
      String str6 = file1.getCanonicalPath();
      File file2 = context1.getSharedPreferencesPath("foo").getParentFile();
      String str7 = file2.getCanonicalPath();
      String str8 = context1.getCacheDir().getCanonicalPath();
      String str9 = context1.getCodeCacheDir().getCanonicalPath();
      Context context2 = createDeviceProtectedStorageContext();
      String str11 = context2.getDataDir().getCanonicalPath();
      String str2 = context2.getFilesDir().getCanonicalPath();
      String str12 = context2.getNoBackupFilesDir().getCanonicalPath();
      File file3 = context2.getDatabasePath("foo").getParentFile();
      String str13 = file3.getCanonicalPath();
      File file4 = context2.getSharedPreferencesPath("foo");
      String str14 = file4.getParentFile().getCanonicalPath();
      String str15 = context2.getCacheDir().getCanonicalPath();
      String str10 = context2.getCodeCacheDir().getCanonicalPath();
      if (applicationInfo.nativeLibraryDir != null) {
        String str = (new File(applicationInfo.nativeLibraryDir)).getCanonicalPath();
      } else {
        applicationInfo = null;
      } 
      ArraySet<String> arraySet1 = new ArraySet();
      arraySet1.add(str4);
      arraySet1.add(str5);
      arraySet1.add(str6);
      arraySet1.add(str7);
      arraySet1.add(str8);
      arraySet1.add(str9);
      arraySet1.add(str2);
      arraySet1.add(str12);
      arraySet1.add(str13);
      arraySet1.add(str14);
      arraySet1.add(str15);
      arraySet1.add(str10);
      if (applicationInfo != null)
        arraySet1.add(applicationInfo); 
      applyXmlFiltersAndDoFullBackupForDomain(str1, "r", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str3);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "d_r", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str11);
      arraySet1.remove(str4);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "f", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str4);
      arraySet1.remove(str2);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "d_f", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str2);
      arraySet1.remove(str6);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "db", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str6);
      arraySet1.remove(str13);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "d_db", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str13);
      arraySet1.remove(str7);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "sp", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str7);
      arraySet1.remove(str14);
      applyXmlFiltersAndDoFullBackupForDomain(str1, "d_sp", map, arraySet, arraySet1, paramFullBackupDataOutput);
      arraySet1.add(str14);
      if (Process.myUid() != 1000) {
        File file = getExternalFilesDir(null);
        if (file != null)
          applyXmlFiltersAndDoFullBackupForDomain(str1, "ef", map, arraySet, arraySet1, paramFullBackupDataOutput); 
      } 
      return;
    } catch (IOException|XmlPullParserException iOException) {
      if (Log.isLoggable("BackupXmlParserLogging", 2))
        Log.v("BackupXmlParserLogging", "Exception trying to parse fullBackupContent xml file! Aborting full backup.", iOException); 
      return;
    } 
  }
  
  public void onQuotaExceeded(long paramLong1, long paramLong2) {}
  
  private int getBackupUserId() {
    int i;
    UserHandle userHandle = this.mUser;
    if (userHandle == null) {
      i = getUserId();
    } else {
      i = userHandle.getIdentifier();
    } 
    return i;
  }
  
  private void applyXmlFiltersAndDoFullBackupForDomain(String paramString1, String paramString2, Map<String, Set<FullBackup.BackupScheme.PathWithRequiredFlags>> paramMap, ArraySet<FullBackup.BackupScheme.PathWithRequiredFlags> paramArraySet, ArraySet<String> paramArraySet1, FullBackupDataOutput paramFullBackupDataOutput) throws IOException {
    String str;
    if (paramMap == null || paramMap.size() == 0) {
      str = FullBackup.getBackupScheme(this).tokenToDirectoryPath(paramString2);
      fullBackupFileTree(paramString1, paramString2, str, paramArraySet, paramArraySet1, paramFullBackupDataOutput);
      return;
    } 
    if (str.get(paramString2) != null)
      for (FullBackup.BackupScheme.PathWithRequiredFlags pathWithRequiredFlags : str.get(paramString2)) {
        int i = pathWithRequiredFlags.getRequiredFlags();
        int j = paramFullBackupDataOutput.getTransportFlags();
        if (areIncludeRequiredTransportFlagsSatisfied(i, j))
          fullBackupFileTree(paramString1, paramString2, pathWithRequiredFlags.getPath(), paramArraySet, paramArraySet1, paramFullBackupDataOutput); 
      }  
  }
  
  private boolean areIncludeRequiredTransportFlagsSatisfied(int paramInt1, int paramInt2) {
    boolean bool;
    if ((paramInt2 & paramInt1) == paramInt1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void fullBackupFile(File paramFile, FullBackupDataOutput paramFullBackupDataOutput) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   4: astore_3
    //   5: aload_0
    //   6: invokevirtual createCredentialProtectedStorageContext : ()Landroid/content/Context;
    //   9: astore #4
    //   11: aload #4
    //   13: invokevirtual getDataDir : ()Ljava/io/File;
    //   16: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   19: astore #5
    //   21: aload #4
    //   23: invokevirtual getFilesDir : ()Ljava/io/File;
    //   26: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   29: astore #6
    //   31: aload #4
    //   33: invokevirtual getNoBackupFilesDir : ()Ljava/io/File;
    //   36: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   39: astore #7
    //   41: aload #4
    //   43: ldc_w 'foo'
    //   46: invokevirtual getDatabasePath : (Ljava/lang/String;)Ljava/io/File;
    //   49: invokevirtual getParentFile : ()Ljava/io/File;
    //   52: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   55: astore #8
    //   57: aload #4
    //   59: ldc_w 'foo'
    //   62: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   65: invokevirtual getParentFile : ()Ljava/io/File;
    //   68: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   71: astore #9
    //   73: aload #4
    //   75: invokevirtual getCacheDir : ()Ljava/io/File;
    //   78: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   81: astore #10
    //   83: aload #4
    //   85: invokevirtual getCodeCacheDir : ()Ljava/io/File;
    //   88: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   91: astore #11
    //   93: aload_0
    //   94: invokevirtual createDeviceProtectedStorageContext : ()Landroid/content/Context;
    //   97: astore #4
    //   99: aload #4
    //   101: invokevirtual getDataDir : ()Ljava/io/File;
    //   104: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   107: astore #12
    //   109: aload #4
    //   111: invokevirtual getFilesDir : ()Ljava/io/File;
    //   114: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   117: astore #13
    //   119: aload #4
    //   121: invokevirtual getNoBackupFilesDir : ()Ljava/io/File;
    //   124: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   127: astore #14
    //   129: aload #4
    //   131: ldc_w 'foo'
    //   134: invokevirtual getDatabasePath : (Ljava/lang/String;)Ljava/io/File;
    //   137: invokevirtual getParentFile : ()Ljava/io/File;
    //   140: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   143: astore #15
    //   145: aload #4
    //   147: ldc_w 'foo'
    //   150: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   153: invokevirtual getParentFile : ()Ljava/io/File;
    //   156: astore #16
    //   158: aload #16
    //   160: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   163: astore #16
    //   165: aload #4
    //   167: invokevirtual getCacheDir : ()Ljava/io/File;
    //   170: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   173: astore #17
    //   175: aload #4
    //   177: invokevirtual getCodeCacheDir : ()Ljava/io/File;
    //   180: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   183: astore #18
    //   185: aload_3
    //   186: getfield nativeLibraryDir : Ljava/lang/String;
    //   189: ifnonnull -> 198
    //   192: aconst_null
    //   193: astore #4
    //   195: goto -> 219
    //   198: new java/io/File
    //   201: astore #4
    //   203: aload #4
    //   205: aload_3
    //   206: getfield nativeLibraryDir : Ljava/lang/String;
    //   209: invokespecial <init> : (Ljava/lang/String;)V
    //   212: aload #4
    //   214: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   217: astore #4
    //   219: invokestatic myUid : ()I
    //   222: istore #19
    //   224: iload #19
    //   226: sipush #1000
    //   229: if_icmpeq -> 254
    //   232: aload_0
    //   233: aconst_null
    //   234: invokevirtual getExternalFilesDir : (Ljava/lang/String;)Ljava/io/File;
    //   237: astore_3
    //   238: aload_3
    //   239: ifnull -> 254
    //   242: aload_3
    //   243: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   246: astore_3
    //   247: goto -> 256
    //   250: astore_1
    //   251: goto -> 603
    //   254: aconst_null
    //   255: astore_3
    //   256: aload_1
    //   257: invokevirtual getCanonicalPath : ()Ljava/lang/String;
    //   260: astore #20
    //   262: aload #20
    //   264: aload #10
    //   266: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   269: ifne -> 584
    //   272: aload #20
    //   274: aload #11
    //   276: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   279: ifne -> 581
    //   282: aload #20
    //   284: aload #7
    //   286: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   289: ifne -> 578
    //   292: aload #20
    //   294: aload #17
    //   296: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   299: ifne -> 575
    //   302: aload #20
    //   304: aload #18
    //   306: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   309: ifne -> 572
    //   312: aload #20
    //   314: aload #14
    //   316: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   319: ifne -> 569
    //   322: aload #20
    //   324: aload #4
    //   326: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   329: ifeq -> 335
    //   332: goto -> 584
    //   335: aload #20
    //   337: aload #8
    //   339: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   342: ifeq -> 355
    //   345: ldc_w 'db'
    //   348: astore_1
    //   349: aload #8
    //   351: astore_3
    //   352: goto -> 512
    //   355: aload #20
    //   357: aload #9
    //   359: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   362: ifeq -> 375
    //   365: ldc_w 'sp'
    //   368: astore_1
    //   369: aload #9
    //   371: astore_3
    //   372: goto -> 512
    //   375: aload #20
    //   377: aload #6
    //   379: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   382: ifeq -> 395
    //   385: ldc_w 'f'
    //   388: astore_1
    //   389: aload #6
    //   391: astore_3
    //   392: goto -> 512
    //   395: aload #20
    //   397: aload #5
    //   399: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   402: ifeq -> 415
    //   405: ldc_w 'r'
    //   408: astore_1
    //   409: aload #5
    //   411: astore_3
    //   412: goto -> 512
    //   415: aload #20
    //   417: aload #15
    //   419: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   422: ifeq -> 435
    //   425: ldc_w 'd_db'
    //   428: astore_1
    //   429: aload #15
    //   431: astore_3
    //   432: goto -> 512
    //   435: aload #20
    //   437: aload #16
    //   439: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   442: ifeq -> 455
    //   445: ldc_w 'd_sp'
    //   448: astore_1
    //   449: aload #16
    //   451: astore_3
    //   452: goto -> 512
    //   455: aload #20
    //   457: aload #13
    //   459: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   462: ifeq -> 475
    //   465: ldc_w 'd_f'
    //   468: astore_1
    //   469: aload #13
    //   471: astore_3
    //   472: goto -> 512
    //   475: aload #20
    //   477: aload #12
    //   479: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   482: ifeq -> 495
    //   485: ldc_w 'd_r'
    //   488: astore_1
    //   489: aload #12
    //   491: astore_3
    //   492: goto -> 512
    //   495: aload_3
    //   496: ifnull -> 527
    //   499: aload #20
    //   501: aload_3
    //   502: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   505: ifeq -> 527
    //   508: ldc_w 'ef'
    //   511: astore_1
    //   512: aload_0
    //   513: invokevirtual getPackageName : ()Ljava/lang/String;
    //   516: aload_1
    //   517: aconst_null
    //   518: aload_3
    //   519: aload #20
    //   521: aload_2
    //   522: invokestatic backupToTar : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/backup/FullBackupDataOutput;)I
    //   525: pop
    //   526: return
    //   527: new java/lang/StringBuilder
    //   530: dup
    //   531: invokespecial <init> : ()V
    //   534: astore_1
    //   535: aload_1
    //   536: ldc_w 'File '
    //   539: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   542: pop
    //   543: aload_1
    //   544: aload #20
    //   546: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   549: pop
    //   550: aload_1
    //   551: ldc_w ' is in an unsupported location; skipping'
    //   554: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   557: pop
    //   558: ldc 'BackupAgent'
    //   560: aload_1
    //   561: invokevirtual toString : ()Ljava/lang/String;
    //   564: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   567: pop
    //   568: return
    //   569: goto -> 584
    //   572: goto -> 584
    //   575: goto -> 584
    //   578: goto -> 584
    //   581: goto -> 584
    //   584: ldc 'BackupAgent'
    //   586: ldc_w 'lib, cache, code_cache, and no_backup files are not backed up'
    //   589: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   592: pop
    //   593: return
    //   594: astore_1
    //   595: goto -> 603
    //   598: astore_1
    //   599: goto -> 603
    //   602: astore_1
    //   603: ldc 'BackupAgent'
    //   605: ldc_w 'Unable to obtain canonical paths'
    //   608: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   611: pop
    //   612: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #633	-> 0
    //   #636	-> 0
    //   #641	-> 5
    //   #642	-> 11
    //   #643	-> 21
    //   #644	-> 31
    //   #645	-> 41
    //   #646	-> 57
    //   #647	-> 73
    //   #648	-> 83
    //   #650	-> 93
    //   #651	-> 99
    //   #652	-> 109
    //   #653	-> 119
    //   #654	-> 129
    //   #655	-> 145
    //   #656	-> 158
    //   #657	-> 165
    //   #658	-> 175
    //   #660	-> 185
    //   #661	-> 192
    //   #662	-> 198
    //   #665	-> 219
    //   #666	-> 232
    //   #667	-> 238
    //   #668	-> 242
    //   #676	-> 250
    //   #665	-> 254
    //   #675	-> 254
    //   #679	-> 262
    //   #681	-> 262
    //   #682	-> 272
    //   #683	-> 282
    //   #684	-> 292
    //   #685	-> 302
    //   #686	-> 312
    //   #687	-> 322
    //   #693	-> 335
    //   #694	-> 335
    //   #695	-> 345
    //   #696	-> 349
    //   #697	-> 355
    //   #698	-> 365
    //   #699	-> 369
    //   #700	-> 375
    //   #701	-> 385
    //   #702	-> 389
    //   #703	-> 395
    //   #704	-> 405
    //   #705	-> 409
    //   #706	-> 415
    //   #707	-> 425
    //   #708	-> 429
    //   #709	-> 435
    //   #710	-> 445
    //   #711	-> 449
    //   #712	-> 455
    //   #713	-> 465
    //   #714	-> 469
    //   #715	-> 475
    //   #716	-> 485
    //   #717	-> 489
    //   #718	-> 495
    //   #719	-> 508
    //   #720	-> 512
    //   #732	-> 512
    //   #733	-> 526
    //   #722	-> 527
    //   #723	-> 568
    //   #686	-> 569
    //   #685	-> 572
    //   #684	-> 575
    //   #683	-> 578
    //   #682	-> 581
    //   #681	-> 584
    //   #688	-> 584
    //   #689	-> 593
    //   #676	-> 594
    //   #677	-> 603
    //   #678	-> 612
    // Exception table:
    //   from	to	target	type
    //   5	11	602	java/io/IOException
    //   11	21	602	java/io/IOException
    //   21	31	602	java/io/IOException
    //   31	41	602	java/io/IOException
    //   41	57	602	java/io/IOException
    //   57	73	602	java/io/IOException
    //   73	83	602	java/io/IOException
    //   83	93	602	java/io/IOException
    //   93	99	602	java/io/IOException
    //   99	109	602	java/io/IOException
    //   109	119	602	java/io/IOException
    //   119	129	602	java/io/IOException
    //   129	145	602	java/io/IOException
    //   145	158	602	java/io/IOException
    //   158	165	602	java/io/IOException
    //   165	175	602	java/io/IOException
    //   175	185	602	java/io/IOException
    //   185	192	598	java/io/IOException
    //   198	219	598	java/io/IOException
    //   219	224	598	java/io/IOException
    //   232	238	250	java/io/IOException
    //   242	247	250	java/io/IOException
    //   256	262	594	java/io/IOException
  }
  
  protected final void fullBackupFileTree(String paramString1, String paramString2, String paramString3, ArraySet<FullBackup.BackupScheme.PathWithRequiredFlags> paramArraySet, ArraySet<String> paramArraySet1, FullBackupDataOutput paramFullBackupDataOutput) {
    String str = FullBackup.getBackupScheme(this).tokenToDirectoryPath(paramString2);
    if (str == null)
      return; 
    File file = new File(paramString3);
    if (file.exists()) {
      LinkedList<File> linkedList = new LinkedList();
      linkedList.add(file);
      while (linkedList.size() > 0) {
        file = linkedList.remove(0);
        try {
          StructStat structStat = Os.lstat(file.getPath());
          if (!OsConstants.S_ISREG(structStat.st_mode)) {
            int i = structStat.st_mode;
            if (!OsConstants.S_ISDIR(i))
              continue; 
          } 
          String str1 = file.getCanonicalPath();
          if (paramArraySet != null && manifestExcludesContainFilePath(paramArraySet, str1))
            continue; 
          if (paramArraySet1 != null && paramArraySet1.contains(str1))
            continue; 
          if (OsConstants.S_ISDIR(structStat.st_mode)) {
            File[] arrayOfFile = file.listFiles();
            if (arrayOfFile != null) {
              byte b;
              int i;
              for (i = arrayOfFile.length, b = 0; b < i; ) {
                File file1 = arrayOfFile[b];
                linkedList.add(0, file1);
                b++;
              } 
            } 
          } 
          FullBackup.backupToTar(paramString1, paramString2, null, str, str1, paramFullBackupDataOutput);
        } catch (IOException iOException) {
          if (Log.isLoggable("BackupXmlParserLogging", 2)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Error canonicalizing path of ");
            stringBuilder.append(file);
            Log.v("BackupXmlParserLogging", stringBuilder.toString());
          } 
        } catch (ErrnoException errnoException) {
          if (Log.isLoggable("BackupXmlParserLogging", 2)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Error scanning file ");
            stringBuilder.append(file);
            stringBuilder.append(" : ");
            stringBuilder.append(errnoException);
            Log.v("BackupXmlParserLogging", stringBuilder.toString());
          } 
        } 
      } 
    } 
  }
  
  private boolean manifestExcludesContainFilePath(ArraySet<FullBackup.BackupScheme.PathWithRequiredFlags> paramArraySet, String paramString) {
    for (FullBackup.BackupScheme.PathWithRequiredFlags pathWithRequiredFlags : paramArraySet) {
      String str = pathWithRequiredFlags.getPath();
      if (str != null && str.equals(paramString))
        return true; 
    } 
    return false;
  }
  
  public void onRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, File paramFile, int paramInt, long paramLong2, long paramLong3) throws IOException {
    boolean bool = isFileEligibleForRestore(paramFile);
    if (!bool)
      paramFile = null; 
    FullBackup.restoreFile(paramParcelFileDescriptor, paramLong1, paramInt, paramLong2, paramLong3, paramFile);
  }
  
  private boolean isFileEligibleForRestore(File paramFile) throws IOException {
    String str1;
    FullBackup.BackupScheme backupScheme = FullBackup.getBackupScheme(this);
    if (!backupScheme.isFullBackupContentEnabled()) {
      if (Log.isLoggable("BackupXmlParserLogging", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onRestoreFile \"");
        stringBuilder.append(paramFile.getCanonicalPath());
        stringBuilder.append("\" : fullBackupContent not enabled for ");
        stringBuilder.append(getPackageName());
        str1 = stringBuilder.toString();
        Log.v("BackupXmlParserLogging", str1);
      } 
      return false;
    } 
    String str2 = str1.getCanonicalPath();
    try {
      StringBuilder stringBuilder;
      Map<String, Set<FullBackup.BackupScheme.PathWithRequiredFlags>> map = backupScheme.maybeParseAndGetCanonicalIncludePaths();
      ArraySet<FullBackup.BackupScheme.PathWithRequiredFlags> arraySet = backupScheme.maybeParseAndGetCanonicalExcludePaths();
      if (arraySet != null && BackupUtils.isFileSpecifiedInPathList((File)str1, (Collection<FullBackup.BackupScheme.PathWithRequiredFlags>)arraySet)) {
        if (Log.isLoggable("BackupXmlParserLogging", 2)) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("onRestoreFile: \"");
          stringBuilder.append(str2);
          stringBuilder.append("\": listed in excludes; skipping.");
          Log.v("BackupXmlParserLogging", stringBuilder.toString());
        } 
        return false;
      } 
      if (map != null && !map.isEmpty()) {
        boolean bool2, bool1 = false;
        Iterator<Set> iterator = map.values().iterator();
        while (true) {
          bool2 = bool1;
          if (iterator.hasNext()) {
            Set<FullBackup.BackupScheme.PathWithRequiredFlags> set = iterator.next();
            bool1 |= BackupUtils.isFileSpecifiedInPathList((File)stringBuilder, set);
            if (bool1) {
              bool2 = bool1;
              break;
            } 
            continue;
          } 
          break;
        } 
        if (!bool2) {
          if (Log.isLoggable("BackupXmlParserLogging", 2)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("onRestoreFile: Trying to restore \"");
            stringBuilder.append(str2);
            stringBuilder.append("\" but it isn't specified in the included files; skipping.");
            Log.v("BackupXmlParserLogging", stringBuilder.toString());
          } 
          return false;
        } 
      } 
      return true;
    } catch (XmlPullParserException xmlPullParserException) {
      if (Log.isLoggable("BackupXmlParserLogging", 2)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("onRestoreFile \"");
        stringBuilder.append(str2);
        stringBuilder.append("\" : Exception trying to parse fullBackupContent xml file! Aborting onRestoreFile.");
        Log.v("BackupXmlParserLogging", stringBuilder.toString(), (Throwable)xmlPullParserException);
      } 
      return false;
    } 
  }
  
  protected void onRestoreFile(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong1, int paramInt, String paramString1, String paramString2, long paramLong2, long paramLong3) throws IOException {
    String str = FullBackup.getBackupScheme(this).tokenToDirectoryPath(paramString1);
    if (paramString1.equals("ef"))
      paramLong2 = -1L; 
    if (str != null) {
      File file = new File(str, paramString2);
      paramString1 = file.getCanonicalPath();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(File.separatorChar);
      if (paramString1.startsWith(stringBuilder.toString())) {
        onRestoreFile(paramParcelFileDescriptor, paramLong1, file, paramInt, paramLong2, paramLong3);
        return;
      } 
    } 
    FullBackup.restoreFile(paramParcelFileDescriptor, paramLong1, paramInt, paramLong2, paramLong3, null);
  }
  
  public void onRestoreFinished() {}
  
  public final IBinder onBind() {
    return this.mBinder;
  }
  
  public void attach(Context paramContext) {
    attachBaseContext(paramContext);
  }
  
  public abstract void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2) throws IOException;
  
  public abstract void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) throws IOException;
  
  class BackupServiceBinder extends IBackupAgent.Stub {
    private static final String TAG = "BackupServiceBinder";
    
    final BackupAgent this$0;
    
    private BackupServiceBinder() {}
    
    public void doBackup(ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2, ParcelFileDescriptor param1ParcelFileDescriptor3, long param1Long, IBackupCallback param1IBackupCallback, int param1Int) throws RemoteException {
      long l = Binder.clearCallingIdentity();
      BackupDataOutput backupDataOutput = new BackupDataOutput(param1ParcelFileDescriptor2.getFileDescriptor(), param1Long, param1Int);
      try {
        BackupAgent backupAgent = BackupAgent.this;
        try {
          backupAgent.onBackup(param1ParcelFileDescriptor1, backupDataOutput, param1ParcelFileDescriptor3);
          BackupAgent.this.waitForSharedPrefs();
          Binder.restoreCallingIdentity(l);
          try {
            param1IBackupCallback.operationComplete(0L);
          } catch (RemoteException remoteException) {}
          if (Binder.getCallingPid() != Process.myPid()) {
            IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor1);
            IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor2);
            IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor3);
          } 
          return;
        } catch (IOException iOException) {
        
        } catch (RuntimeException runtimeException1) {
        
        } finally {}
      } catch (IOException iOException) {
      
      } catch (RuntimeException runtimeException1) {
        try {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("onBackup (");
          stringBuilder1.append(BackupAgent.this.getClass().getName());
          stringBuilder1.append(") threw");
          Log.d("BackupServiceBinder", stringBuilder1.toString(), runtimeException1);
          throw runtimeException1;
        } finally {}
      } finally {}
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("onBackup (");
      stringBuilder.append(BackupAgent.this.getClass().getName());
      stringBuilder.append(") threw");
      Log.d("BackupServiceBinder", stringBuilder.toString(), (Throwable)backupDataOutput);
      RuntimeException runtimeException = new RuntimeException();
      this((Throwable)backupDataOutput);
      throw runtimeException;
    }
    
    public void doRestore(ParcelFileDescriptor param1ParcelFileDescriptor1, long param1Long, ParcelFileDescriptor param1ParcelFileDescriptor2, int param1Int, IBackupManager param1IBackupManager) throws RemoteException {
      doRestoreInternal(param1ParcelFileDescriptor1, param1Long, param1ParcelFileDescriptor2, param1Int, param1IBackupManager, null);
    }
    
    public void doRestoreWithExcludedKeys(ParcelFileDescriptor param1ParcelFileDescriptor1, long param1Long, ParcelFileDescriptor param1ParcelFileDescriptor2, int param1Int, IBackupManager param1IBackupManager, List<String> param1List) throws RemoteException {
      doRestoreInternal(param1ParcelFileDescriptor1, param1Long, param1ParcelFileDescriptor2, param1Int, param1IBackupManager, param1List);
    }
    
    private void doRestoreInternal(ParcelFileDescriptor param1ParcelFileDescriptor1, long param1Long, ParcelFileDescriptor param1ParcelFileDescriptor2, int param1Int, IBackupManager param1IBackupManager, List<String> param1List) throws RemoteException {
      // Byte code:
      //   0: invokestatic clearCallingIdentity : ()J
      //   3: lstore #8
      //   5: aload_0
      //   6: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   9: invokestatic access$100 : (Landroid/app/backup/BackupAgent;)V
      //   12: new android/app/backup/BackupDataInput
      //   15: dup
      //   16: aload_1
      //   17: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
      //   20: invokespecial <init> : (Ljava/io/FileDescriptor;)V
      //   23: astore #10
      //   25: aload_0
      //   26: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   29: astore #11
      //   31: aload #7
      //   33: ifnull -> 60
      //   36: new java/util/HashSet
      //   39: astore #12
      //   41: aload #12
      //   43: aload #7
      //   45: invokespecial <init> : (Ljava/util/Collection;)V
      //   48: aload #12
      //   50: astore #7
      //   52: goto -> 65
      //   55: astore #7
      //   57: goto -> 287
      //   60: invokestatic emptySet : ()Ljava/util/Set;
      //   63: astore #7
      //   65: aload #11
      //   67: aload #10
      //   69: lload_2
      //   70: aload #4
      //   72: aload #7
      //   74: invokevirtual onRestore : (Landroid/app/backup/BackupDataInput;JLandroid/os/ParcelFileDescriptor;Ljava/util/Set;)V
      //   77: aload_0
      //   78: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   81: invokevirtual reloadSharedPreferences : ()V
      //   84: lload #8
      //   86: invokestatic restoreCallingIdentity : (J)V
      //   89: aload #6
      //   91: aload_0
      //   92: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   95: invokestatic access$200 : (Landroid/app/backup/BackupAgent;)I
      //   98: iload #5
      //   100: lconst_0
      //   101: invokeinterface opCompleteForUser : (IIJ)V
      //   106: goto -> 111
      //   109: astore #6
      //   111: invokestatic getCallingPid : ()I
      //   114: invokestatic myPid : ()I
      //   117: if_icmpeq -> 129
      //   120: aload_1
      //   121: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
      //   124: aload #4
      //   126: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
      //   129: return
      //   130: astore #7
      //   132: goto -> 152
      //   135: astore #7
      //   137: goto -> 142
      //   140: astore #7
      //   142: goto -> 212
      //   145: astore #7
      //   147: goto -> 287
      //   150: astore #7
      //   152: new java/lang/StringBuilder
      //   155: astore #12
      //   157: aload #12
      //   159: invokespecial <init> : ()V
      //   162: aload #12
      //   164: ldc 'onRestore ('
      //   166: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   169: pop
      //   170: aload #12
      //   172: aload_0
      //   173: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   176: invokevirtual getClass : ()Ljava/lang/Class;
      //   179: invokevirtual getName : ()Ljava/lang/String;
      //   182: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   185: pop
      //   186: aload #12
      //   188: ldc ') threw'
      //   190: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   193: pop
      //   194: ldc 'BackupServiceBinder'
      //   196: aload #12
      //   198: invokevirtual toString : ()Ljava/lang/String;
      //   201: aload #7
      //   203: invokestatic d : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   206: pop
      //   207: aload #7
      //   209: athrow
      //   210: astore #7
      //   212: new java/lang/StringBuilder
      //   215: astore #12
      //   217: aload #12
      //   219: invokespecial <init> : ()V
      //   222: aload #12
      //   224: ldc 'onRestore ('
      //   226: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   229: pop
      //   230: aload #12
      //   232: aload_0
      //   233: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   236: invokevirtual getClass : ()Ljava/lang/Class;
      //   239: invokevirtual getName : ()Ljava/lang/String;
      //   242: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   245: pop
      //   246: aload #12
      //   248: ldc ') threw'
      //   250: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   253: pop
      //   254: ldc 'BackupServiceBinder'
      //   256: aload #12
      //   258: invokevirtual toString : ()Ljava/lang/String;
      //   261: aload #7
      //   263: invokestatic d : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   266: pop
      //   267: new java/lang/RuntimeException
      //   270: astore #12
      //   272: aload #12
      //   274: aload #7
      //   276: invokespecial <init> : (Ljava/lang/Throwable;)V
      //   279: aload #12
      //   281: athrow
      //   282: astore #7
      //   284: goto -> 147
      //   287: aload_0
      //   288: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   291: invokevirtual reloadSharedPreferences : ()V
      //   294: lload #8
      //   296: invokestatic restoreCallingIdentity : (J)V
      //   299: aload #6
      //   301: aload_0
      //   302: getfield this$0 : Landroid/app/backup/BackupAgent;
      //   305: invokestatic access$200 : (Landroid/app/backup/BackupAgent;)I
      //   308: iload #5
      //   310: lconst_0
      //   311: invokeinterface opCompleteForUser : (IIJ)V
      //   316: goto -> 321
      //   319: astore #6
      //   321: invokestatic getCallingPid : ()I
      //   324: invokestatic myPid : ()I
      //   327: if_icmpeq -> 339
      //   330: aload_1
      //   331: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
      //   334: aload #4
      //   336: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
      //   339: aload #7
      //   341: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1061	-> 0
      //   #1067	-> 5
      //   #1069	-> 12
      //   #1071	-> 25
      //   #1072	-> 31
      //   #1082	-> 55
      //   #1073	-> 60
      //   #1071	-> 65
      //   #1082	-> 77
      //   #1084	-> 84
      //   #1086	-> 89
      //   #1089	-> 106
      //   #1087	-> 109
      //   #1091	-> 111
      //   #1092	-> 120
      //   #1093	-> 124
      //   #1096	-> 129
      //   #1077	-> 130
      //   #1074	-> 135
      //   #1082	-> 145
      //   #1077	-> 150
      //   #1078	-> 152
      //   #1079	-> 207
      //   #1074	-> 210
      //   #1075	-> 212
      //   #1076	-> 267
      //   #1082	-> 282
      //   #1084	-> 294
      //   #1086	-> 299
      //   #1089	-> 316
      //   #1087	-> 319
      //   #1091	-> 321
      //   #1092	-> 330
      //   #1093	-> 334
      //   #1095	-> 339
      // Exception table:
      //   from	to	target	type
      //   25	31	210	java/io/IOException
      //   25	31	150	java/lang/RuntimeException
      //   25	31	145	finally
      //   36	48	210	java/io/IOException
      //   36	48	150	java/lang/RuntimeException
      //   36	48	55	finally
      //   60	65	140	java/io/IOException
      //   60	65	150	java/lang/RuntimeException
      //   60	65	145	finally
      //   65	77	135	java/io/IOException
      //   65	77	130	java/lang/RuntimeException
      //   65	77	282	finally
      //   89	106	109	android/os/RemoteException
      //   152	207	282	finally
      //   207	210	282	finally
      //   212	267	282	finally
      //   267	282	282	finally
      //   299	316	319	android/os/RemoteException
    }
    
    public void doFullBackup(ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long, int param1Int1, IBackupManager param1IBackupManager, int param1Int2) {
      long l = Binder.clearCallingIdentity();
      BackupAgent.this.waitForSharedPrefs();
      try {
        BackupAgent backupAgent = BackupAgent.this;
        FullBackupDataOutput fullBackupDataOutput = new FullBackupDataOutput();
        try {
          this(param1ParcelFileDescriptor, param1Long, param1Int2);
          backupAgent.onFullBackup(fullBackupDataOutput);
          BackupAgent.this.waitForSharedPrefs();
          try {
            FileOutputStream fileOutputStream = new FileOutputStream();
            this(param1ParcelFileDescriptor.getFileDescriptor());
            byte[] arrayOfByte = new byte[4];
            fileOutputStream.write(arrayOfByte);
          } catch (IOException null) {
            Log.e("BackupServiceBinder", "Unable to finalize backup stream!");
          } 
          Binder.restoreCallingIdentity(l);
          try {
            param1IBackupManager.opCompleteForUser(BackupAgent.this.getBackupUserId(), param1Int1, 0L);
          } catch (RemoteException remoteException) {}
          if (Binder.getCallingPid() != Process.myPid())
            IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor); 
          return;
        } catch (IOException null) {
        
        } catch (RuntimeException null) {
        
        } finally {}
      } catch (IOException null) {
      
      } catch (RuntimeException null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("onFullBackup (");
        stringBuilder1.append(BackupAgent.this.getClass().getName());
        stringBuilder1.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder1.toString(), exception);
        throw exception;
      } finally {}
      StringBuilder stringBuilder = new StringBuilder();
      this();
      Exception exception;
      stringBuilder.append("onFullBackup (");
      stringBuilder.append(BackupAgent.this.getClass().getName());
      stringBuilder.append(") threw");
      Log.d("BackupServiceBinder", stringBuilder.toString(), exception);
      RuntimeException runtimeException = new RuntimeException();
      this(exception);
      throw runtimeException;
    }
    
    public void doMeasureFullBackup(long param1Long, int param1Int1, IBackupManager param1IBackupManager, int param1Int2) {
      Exception exception;
      long l = Binder.clearCallingIdentity();
      FullBackupDataOutput fullBackupDataOutput = new FullBackupDataOutput(param1Long, param1Int2);
      BackupAgent.this.waitForSharedPrefs();
      try {
        BackupAgent.this.onFullBackup(fullBackupDataOutput);
        Binder.restoreCallingIdentity(l);
        try {
          param1Int2 = BackupAgent.this.getBackupUserId();
          param1Long = fullBackupDataOutput.getSize();
          param1IBackupManager.opCompleteForUser(param1Int2, param1Int1, param1Long);
        } catch (RemoteException remoteException) {}
        return;
      } catch (IOException null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onFullBackup[M] (");
        stringBuilder.append(BackupAgent.this.getClass().getName());
        stringBuilder.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder.toString(), exception);
        RuntimeException runtimeException = new RuntimeException();
        this(exception);
        throw runtimeException;
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onFullBackup[M] (");
        stringBuilder.append(BackupAgent.this.getClass().getName());
        stringBuilder.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder.toString(), runtimeException);
        throw runtimeException;
      } finally {}
      Binder.restoreCallingIdentity(l);
      try {
        param1Int2 = BackupAgent.this.getBackupUserId();
        param1Long = fullBackupDataOutput.getSize();
        remoteException.opCompleteForUser(param1Int2, param1Int1, param1Long);
      } catch (RemoteException remoteException1) {}
      throw exception;
    }
    
    public void doRestoreFile(ParcelFileDescriptor param1ParcelFileDescriptor, long param1Long1, int param1Int1, String param1String1, String param1String2, long param1Long2, long param1Long3, int param1Int2, IBackupManager param1IBackupManager) throws RemoteException {
      long l = Binder.clearCallingIdentity();
      try {
        BackupAgent.this.onRestoreFile(param1ParcelFileDescriptor, param1Long1, param1Int1, param1String1, param1String2, param1Long2, param1Long3);
        BackupAgent.this.waitForSharedPrefs();
        BackupAgent.this.reloadSharedPreferences();
        Binder.restoreCallingIdentity(l);
        try {
          param1IBackupManager.opCompleteForUser(BackupAgent.this.getBackupUserId(), param1Int2, 0L);
        } catch (RemoteException remoteException) {}
        if (Binder.getCallingPid() != Process.myPid())
          IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor); 
        return;
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onRestoreFile (");
        stringBuilder.append(BackupAgent.this.getClass().getName());
        stringBuilder.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder.toString(), iOException);
        RuntimeException runtimeException = new RuntimeException();
        this(iOException);
        throw runtimeException;
      } finally {}
      BackupAgent.this.waitForSharedPrefs();
      BackupAgent.this.reloadSharedPreferences();
      Binder.restoreCallingIdentity(l);
      try {
        param1IBackupManager.opCompleteForUser(BackupAgent.this.getBackupUserId(), param1Int2, 0L);
      } catch (RemoteException remoteException) {}
      if (Binder.getCallingPid() != Process.myPid())
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor); 
      throw param1String1;
    }
    
    public void doRestoreFinished(int param1Int, IBackupManager param1IBackupManager) {
      Exception exception;
      long l = Binder.clearCallingIdentity();
      try {
        BackupAgent.this.onRestoreFinished();
        BackupAgent.this.waitForSharedPrefs();
        Binder.restoreCallingIdentity(l);
        try {
          param1IBackupManager.opCompleteForUser(BackupAgent.this.getBackupUserId(), param1Int, 0L);
        } catch (RemoteException remoteException) {}
        return;
      } catch (Exception null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onRestoreFinished (");
        stringBuilder.append(BackupAgent.this.getClass().getName());
        stringBuilder.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder.toString(), exception);
        throw exception;
      } finally {}
      BackupAgent.this.waitForSharedPrefs();
      Binder.restoreCallingIdentity(l);
      try {
        remoteException.opCompleteForUser(BackupAgent.this.getBackupUserId(), param1Int, 0L);
      } catch (RemoteException remoteException1) {}
      throw exception;
    }
    
    public void fail(String param1String) {
      BackupAgent.this.getHandler().post(new BackupAgent.FailRunnable(param1String));
    }
    
    public void doQuotaExceeded(long param1Long1, long param1Long2, IBackupCallback param1IBackupCallback) {
      Exception exception;
      long l = Binder.clearCallingIdentity();
      try {
        BackupAgent.this.onQuotaExceeded(param1Long1, param1Long2);
        BackupAgent.this.waitForSharedPrefs();
        Binder.restoreCallingIdentity(l);
        try {
          param1IBackupCallback.operationComplete(0L);
        } catch (RemoteException remoteException) {}
        return;
      } catch (Exception exception1) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("onQuotaExceeded(");
        stringBuilder.append(BackupAgent.this.getClass().getName());
        stringBuilder.append(") threw");
        Log.d("BackupServiceBinder", stringBuilder.toString(), exception1);
        throw exception1;
      } finally {}
      BackupAgent.this.waitForSharedPrefs();
      Binder.restoreCallingIdentity(l);
      try {
        remoteException.operationComplete(-1L);
      } catch (RemoteException remoteException1) {}
      throw exception;
    }
  }
  
  class FailRunnable implements Runnable {
    private String mMessage;
    
    FailRunnable(BackupAgent this$0) {
      this.mMessage = (String)this$0;
    }
    
    public void run() {
      throw new IllegalStateException(this.mMessage);
    }
  }
}
