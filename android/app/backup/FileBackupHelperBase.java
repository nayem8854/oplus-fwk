package android.app.backup;

import android.content.Context;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;

class FileBackupHelperBase {
  private static final String TAG = "FileBackupHelperBase";
  
  Context mContext;
  
  boolean mExceptionLogged;
  
  long mPtr;
  
  FileBackupHelperBase(Context paramContext) {
    this.mPtr = ctor();
    this.mContext = paramContext;
  }
  
  protected void finalize() throws Throwable {
    try {
      dtor(this.mPtr);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  static void performBackup_checked(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    if (paramArrayOfString1.length == 0)
      return; 
    int j;
    for (int i = paramArrayOfString1.length; j < i; ) {
      String str = paramArrayOfString1[j];
      if (str.charAt(0) == '/') {
        j++;
        continue;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append("files must have all absolute paths: ");
      stringBuilder.append(str);
      throw new RuntimeException(stringBuilder.toString());
    } 
    if (paramArrayOfString1.length == paramArrayOfString2.length) {
      if (stringBuilder != null) {
        FileDescriptor fileDescriptor1 = stringBuilder.getFileDescriptor();
      } else {
        stringBuilder = null;
      } 
      FileDescriptor fileDescriptor = paramParcelFileDescriptor2.getFileDescriptor();
      if (fileDescriptor != null) {
        j = performBackup_native((FileDescriptor)stringBuilder, paramBackupDataOutput.mBackupWriter, fileDescriptor, paramArrayOfString1, paramArrayOfString2);
        if (j == 0)
          return; 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Backup failed 0x");
        stringBuilder.append(Integer.toHexString(j));
        throw new RuntimeException(stringBuilder.toString());
      } 
      throw null;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("files.length=");
    stringBuilder.append(paramArrayOfString1.length);
    stringBuilder.append(" keys.length=");
    stringBuilder.append(paramArrayOfString2.length);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  boolean writeFile(File paramFile, BackupDataInputStream paramBackupDataInputStream) {
    File file = paramFile.getParentFile();
    file.mkdirs();
    int i = writeFile_native(this.mPtr, paramFile.getAbsolutePath(), paramBackupDataInputStream.mData.mBackupReader);
    boolean bool = true;
    if (i != 0)
      if (!this.mExceptionLogged) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed restoring file '");
        stringBuilder.append(paramFile);
        stringBuilder.append("' for app '");
        Context context = this.mContext;
        stringBuilder.append(context.getPackageName());
        stringBuilder.append("' result=0x");
        stringBuilder.append(Integer.toHexString(i));
        String str = stringBuilder.toString();
        Log.e("FileBackupHelperBase", str);
        this.mExceptionLogged = true;
      }  
    if (i != 0)
      bool = false; 
    return bool;
  }
  
  public void writeNewStateDescription(ParcelFileDescriptor paramParcelFileDescriptor) {
    writeSnapshot_native(this.mPtr, paramParcelFileDescriptor.getFileDescriptor());
  }
  
  boolean isKeyInList(String paramString, String[] paramArrayOfString) {
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      if (str.equals(paramString))
        return true; 
      b++;
    } 
    return false;
  }
  
  private static native long ctor();
  
  private static native void dtor(long paramLong);
  
  private static native int performBackup_native(FileDescriptor paramFileDescriptor1, long paramLong, FileDescriptor paramFileDescriptor2, String[] paramArrayOfString1, String[] paramArrayOfString2);
  
  private static native int writeFile_native(long paramLong1, String paramString, long paramLong2);
  
  private static native int writeSnapshot_native(long paramLong, FileDescriptor paramFileDescriptor);
}
