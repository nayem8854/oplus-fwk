package android.app.backup;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.util.Log;
import android.util.Pair;
import java.util.List;

public class BackupManager {
  @SystemApi
  public static final int ERROR_AGENT_FAILURE = -1003;
  
  @SystemApi
  public static final int ERROR_BACKUP_CANCELLED = -2003;
  
  @SystemApi
  public static final int ERROR_BACKUP_NOT_ALLOWED = -2001;
  
  @SystemApi
  public static final int ERROR_PACKAGE_NOT_FOUND = -2002;
  
  @SystemApi
  public static final int ERROR_TRANSPORT_ABORTED = -1000;
  
  @SystemApi
  public static final int ERROR_TRANSPORT_INVALID = -2;
  
  @SystemApi
  public static final int ERROR_TRANSPORT_PACKAGE_REJECTED = -1002;
  
  @SystemApi
  public static final int ERROR_TRANSPORT_QUOTA_EXCEEDED = -1005;
  
  @SystemApi
  public static final int ERROR_TRANSPORT_UNAVAILABLE = -1;
  
  public static final String EXTRA_BACKUP_SERVICES_AVAILABLE = "backup_services_available";
  
  @SystemApi
  public static final int FLAG_NON_INCREMENTAL_BACKUP = 1;
  
  @SystemApi
  public static final String PACKAGE_MANAGER_SENTINEL = "@pm@";
  
  @SystemApi
  public static final int SUCCESS = 0;
  
  private static final String TAG = "BackupManager";
  
  private static IBackupManager sService;
  
  private Context mContext;
  
  private static void checkServiceBinder() {
    if (sService == null) {
      IBinder iBinder = ServiceManager.getService("backup");
      sService = IBackupManager.Stub.asInterface(iBinder);
    } 
  }
  
  public BackupManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public void dataChanged() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.dataChanged(this.mContext.getPackageName());
      } catch (RemoteException remoteException) {
        Log.d("BackupManager", "dataChanged() couldn't connect");
      }  
  }
  
  public static void dataChanged(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.dataChanged(paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "dataChanged(pkg) couldn't connect");
      }  
  }
  
  @Deprecated
  public int requestRestore(RestoreObserver paramRestoreObserver) {
    return requestRestore(paramRestoreObserver, null);
  }
  
  @SystemApi
  @Deprecated
  public int requestRestore(RestoreObserver paramRestoreObserver, BackupManagerMonitor paramBackupManagerMonitor) {
    Log.w("BackupManager", "requestRestore(): Since Android P app can no longer request restoring of its backup.");
    return -1;
  }
  
  @SystemApi
  public RestoreSession beginRestoreSession() {
    Context context1 = null, context2 = null;
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    Context context3 = context1;
    if (iBackupManager != null)
      try {
        context3 = this.mContext;
        IRestoreSession iRestoreSession = iBackupManager.beginRestoreSessionForUser(context3.getUserId(), null, null);
        context3 = context2;
        if (iRestoreSession != null) {
          RestoreSession restoreSession = new RestoreSession();
          this(this.mContext, iRestoreSession);
        } 
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "beginRestoreSession() couldn't connect");
        context3 = context1;
      }  
    return (RestoreSession)context3;
  }
  
  @SystemApi
  public void setBackupEnabled(boolean paramBoolean) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.setBackupEnabled(paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "setBackupEnabled() couldn't connect");
      }  
  }
  
  @SystemApi
  public boolean isBackupEnabled() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.isBackupEnabled();
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "isBackupEnabled() couldn't connect");
      }  
    return false;
  }
  
  @SystemApi
  public boolean isBackupServiceActive(UserHandle paramUserHandle) {
    this.mContext.enforceCallingOrSelfPermission("android.permission.BACKUP", "isBackupServiceActive");
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.isBackupServiceActive(paramUserHandle.getIdentifier());
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "isBackupEnabled() couldn't connect");
      }  
    return false;
  }
  
  @SystemApi
  public void setAutoRestore(boolean paramBoolean) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.setAutoRestore(paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "setAutoRestore() couldn't connect");
      }  
  }
  
  @SystemApi
  public String getCurrentTransport() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getCurrentTransport();
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getCurrentTransport() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public ComponentName getCurrentTransportComponent() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getCurrentTransportComponentForUser(this.mContext.getUserId());
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getCurrentTransportComponent() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public String[] listAllTransports() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.listAllTransports();
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "listAllTransports() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  @Deprecated
  public void updateTransportAttributes(ComponentName paramComponentName, String paramString1, Intent paramIntent1, String paramString2, Intent paramIntent2, String paramString3) {
    updateTransportAttributes(paramComponentName, paramString1, paramIntent1, paramString2, paramIntent2, paramString3);
  }
  
  @SystemApi
  public void updateTransportAttributes(ComponentName paramComponentName, String paramString1, Intent paramIntent1, String paramString2, Intent paramIntent2, CharSequence paramCharSequence) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        Context context = this.mContext;
        int i = context.getUserId();
        iBackupManager.updateTransportAttributesForUser(i, paramComponentName, paramString1, paramIntent1, paramString2, paramIntent2, paramCharSequence);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "describeTransport() couldn't connect");
      }  
  }
  
  @SystemApi
  @Deprecated
  public String selectBackupTransport(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.selectBackupTransport(paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "selectBackupTransport() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public void selectBackupTransport(ComponentName paramComponentName, SelectBackupTransportCallback paramSelectBackupTransportCallback) {
    // Byte code:
    //   0: invokestatic checkServiceBinder : ()V
    //   3: getstatic android/app/backup/BackupManager.sService : Landroid/app/backup/IBackupManager;
    //   6: ifnull -> 72
    //   9: aload_2
    //   10: ifnonnull -> 18
    //   13: aconst_null
    //   14: astore_2
    //   15: goto -> 32
    //   18: new android/app/backup/BackupManager$SelectTransportListenerWrapper
    //   21: dup
    //   22: aload_0
    //   23: aload_0
    //   24: getfield mContext : Landroid/content/Context;
    //   27: aload_2
    //   28: invokespecial <init> : (Landroid/app/backup/BackupManager;Landroid/content/Context;Landroid/app/backup/SelectBackupTransportCallback;)V
    //   31: astore_2
    //   32: getstatic android/app/backup/BackupManager.sService : Landroid/app/backup/IBackupManager;
    //   35: astore_3
    //   36: aload_0
    //   37: getfield mContext : Landroid/content/Context;
    //   40: astore #4
    //   42: aload #4
    //   44: invokevirtual getUserId : ()I
    //   47: istore #5
    //   49: aload_3
    //   50: iload #5
    //   52: aload_1
    //   53: aload_2
    //   54: invokeinterface selectBackupTransportAsyncForUser : (ILandroid/content/ComponentName;Landroid/app/backup/ISelectBackupTransportCallback;)V
    //   59: goto -> 72
    //   62: astore_1
    //   63: ldc 'BackupManager'
    //   65: ldc_w 'selectBackupTransportAsync() couldn't connect'
    //   68: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   71: pop
    //   72: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #637	-> 0
    //   #638	-> 3
    //   #640	-> 9
    //   #641	-> 13
    //   #642	-> 32
    //   #643	-> 42
    //   #642	-> 49
    //   #646	-> 59
    //   #644	-> 62
    //   #645	-> 63
    //   #648	-> 72
    // Exception table:
    //   from	to	target	type
    //   18	32	62	android/os/RemoteException
    //   32	42	62	android/os/RemoteException
    //   42	49	62	android/os/RemoteException
    //   49	59	62	android/os/RemoteException
  }
  
  @SystemApi
  public void backupNow() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.backupNow();
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "backupNow() couldn't connect");
      }  
  }
  
  @SystemApi
  public long getAvailableRestoreToken(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getAvailableRestoreTokenForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getAvailableRestoreToken() couldn't connect");
      }  
    return 0L;
  }
  
  @SystemApi
  public boolean isAppEligibleForBackup(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.isAppEligibleForBackupForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "isAppEligibleForBackup(pkg) couldn't connect");
      }  
    return false;
  }
  
  @SystemApi
  public int requestBackup(String[] paramArrayOfString, BackupObserver paramBackupObserver) {
    return requestBackup(paramArrayOfString, paramBackupObserver, null, 0);
  }
  
  @SystemApi
  public int requestBackup(String[] paramArrayOfString, BackupObserver paramBackupObserver, BackupManagerMonitor paramBackupManagerMonitor, int paramInt) {
    // Byte code:
    //   0: invokestatic checkServiceBinder : ()V
    //   3: getstatic android/app/backup/BackupManager.sService : Landroid/app/backup/IBackupManager;
    //   6: ifnull -> 83
    //   9: aconst_null
    //   10: astore #5
    //   12: aload_2
    //   13: ifnonnull -> 21
    //   16: aconst_null
    //   17: astore_2
    //   18: goto -> 35
    //   21: new android/app/backup/BackupManager$BackupObserverWrapper
    //   24: dup
    //   25: aload_0
    //   26: aload_0
    //   27: getfield mContext : Landroid/content/Context;
    //   30: aload_2
    //   31: invokespecial <init> : (Landroid/app/backup/BackupManager;Landroid/content/Context;Landroid/app/backup/BackupObserver;)V
    //   34: astore_2
    //   35: aload_3
    //   36: ifnonnull -> 45
    //   39: aload #5
    //   41: astore_3
    //   42: goto -> 55
    //   45: new android/app/backup/BackupManager$BackupManagerMonitorWrapper
    //   48: dup
    //   49: aload_0
    //   50: aload_3
    //   51: invokespecial <init> : (Landroid/app/backup/BackupManager;Landroid/app/backup/BackupManagerMonitor;)V
    //   54: astore_3
    //   55: getstatic android/app/backup/BackupManager.sService : Landroid/app/backup/IBackupManager;
    //   58: aload_1
    //   59: aload_2
    //   60: aload_3
    //   61: iload #4
    //   63: invokeinterface requestBackup : ([Ljava/lang/String;Landroid/app/backup/IBackupObserver;Landroid/app/backup/IBackupManagerMonitor;I)I
    //   68: istore #4
    //   70: iload #4
    //   72: ireturn
    //   73: astore_1
    //   74: ldc 'BackupManager'
    //   76: ldc_w 'requestBackup() couldn't connect'
    //   79: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   82: pop
    //   83: iconst_m1
    //   84: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #764	-> 0
    //   #765	-> 3
    //   #767	-> 9
    //   #768	-> 16
    //   #769	-> 21
    //   #770	-> 35
    //   #771	-> 39
    //   #772	-> 45
    //   #773	-> 55
    //   #774	-> 73
    //   #775	-> 74
    //   #778	-> 83
    // Exception table:
    //   from	to	target	type
    //   21	35	73	android/os/RemoteException
    //   45	55	73	android/os/RemoteException
    //   55	70	73	android/os/RemoteException
  }
  
  @SystemApi
  public void cancelBackups() {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.cancelBackups();
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "cancelBackups() couldn't connect.");
      }  
  }
  
  public UserHandle getUserForAncestralSerialNumber(long paramLong) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getUserForAncestralSerialNumber(paramLong);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getUserForAncestralSerialNumber() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public void setAncestralSerialNumber(long paramLong) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.setAncestralSerialNumber(paramLong);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "setAncestralSerialNumber() couldn't connect");
      }  
  }
  
  @SystemApi
  public Intent getConfigurationIntent(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getConfigurationIntentForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getConfigurationIntent() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public String getDestinationString(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getDestinationStringForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getDestinationString() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public Intent getDataManagementIntent(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getDataManagementIntentForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getDataManagementIntent() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  @Deprecated
  public String getDataManagementLabel(String paramString) {
    CharSequence charSequence = getDataManagementIntentLabel(paramString);
    if (charSequence == null) {
      charSequence = null;
    } else {
      charSequence = charSequence.toString();
    } 
    return (String)charSequence;
  }
  
  @SystemApi
  public CharSequence getDataManagementIntentLabel(String paramString) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        return iBackupManager.getDataManagementLabelForUser(this.mContext.getUserId(), paramString);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "getDataManagementIntentLabel() couldn't connect");
      }  
    return null;
  }
  
  @SystemApi
  public void excludeKeysFromRestore(String paramString, List<String> paramList) {
    checkServiceBinder();
    IBackupManager iBackupManager = sService;
    if (iBackupManager != null)
      try {
        iBackupManager.excludeKeysFromRestore(paramString, paramList);
      } catch (RemoteException remoteException) {
        Log.e("BackupManager", "excludeKeysFromRestore() couldn't connect");
      }  
  }
  
  class BackupObserverWrapper extends IBackupObserver.Stub {
    static final int MSG_FINISHED = 3;
    
    static final int MSG_RESULT = 2;
    
    static final int MSG_UPDATE = 1;
    
    final Handler mHandler;
    
    final BackupObserver mObserver;
    
    final BackupManager this$0;
    
    BackupObserverWrapper(Context param1Context, BackupObserver param1BackupObserver) {
      this.mHandler = (Handler)new Object(this, param1Context.getMainLooper(), BackupManager.this);
      this.mObserver = param1BackupObserver;
    }
    
    public void onUpdate(String param1String, BackupProgress param1BackupProgress) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(1, Pair.create(param1String, param1BackupProgress));
      handler.sendMessage(message);
    }
    
    public void onResult(String param1String, int param1Int) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(2, param1Int, 0, param1String);
      handler.sendMessage(message);
    }
    
    public void backupFinished(int param1Int) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(3, param1Int, 0);
      handler.sendMessage(message);
    }
  }
  
  class SelectTransportListenerWrapper extends ISelectBackupTransportCallback.Stub {
    private final Handler mHandler;
    
    private final SelectBackupTransportCallback mListener;
    
    final BackupManager this$0;
    
    SelectTransportListenerWrapper(Context param1Context, SelectBackupTransportCallback param1SelectBackupTransportCallback) {
      this.mHandler = new Handler(param1Context.getMainLooper());
      this.mListener = param1SelectBackupTransportCallback;
    }
    
    public void onSuccess(final String transportName) {
      this.mHandler.post(new Runnable() {
            final BackupManager.SelectTransportListenerWrapper this$1;
            
            final String val$transportName;
            
            public void run() {
              this.this$1.mListener.onSuccess(transportName);
            }
          });
    }
    
    public void onFailure(final int reason) {
      this.mHandler.post(new Runnable() {
            final BackupManager.SelectTransportListenerWrapper this$1;
            
            final int val$reason;
            
            public void run() {
              this.this$1.mListener.onFailure(reason);
            }
          });
    }
  }
  
  class null implements Runnable {
    final BackupManager.SelectTransportListenerWrapper this$1;
    
    final String val$transportName;
    
    public void run() {
      this.this$1.mListener.onSuccess(transportName);
    }
  }
  
  class null implements Runnable {
    final BackupManager.SelectTransportListenerWrapper this$1;
    
    final int val$reason;
    
    public void run() {
      this.this$1.mListener.onFailure(reason);
    }
  }
  
  class BackupManagerMonitorWrapper extends IBackupManagerMonitor.Stub {
    final BackupManagerMonitor mMonitor;
    
    final BackupManager this$0;
    
    BackupManagerMonitorWrapper(BackupManagerMonitor param1BackupManagerMonitor) {
      this.mMonitor = param1BackupManagerMonitor;
    }
    
    public void onEvent(Bundle param1Bundle) throws RemoteException {
      this.mMonitor.onEvent(param1Bundle);
    }
  }
}
