package android.app.backup;

import android.os.ParcelFileDescriptor;
import java.io.IOException;

public class BackupAgentHelper extends BackupAgent {
  static final String TAG = "BackupAgentHelper";
  
  BackupHelperDispatcher mDispatcher = new BackupHelperDispatcher();
  
  public void onBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2) throws IOException {
    this.mDispatcher.performBackup(paramParcelFileDescriptor1, paramBackupDataOutput, paramParcelFileDescriptor2);
  }
  
  public void onRestore(BackupDataInput paramBackupDataInput, int paramInt, ParcelFileDescriptor paramParcelFileDescriptor) throws IOException {
    this.mDispatcher.performRestore(paramBackupDataInput, paramInt, paramParcelFileDescriptor);
  }
  
  public BackupHelperDispatcher getDispatcher() {
    return this.mDispatcher;
  }
  
  public void addHelper(String paramString, BackupHelper paramBackupHelper) {
    this.mDispatcher.addHelper(paramString, paramBackupHelper);
  }
}
