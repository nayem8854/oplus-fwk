package android.app.backup;

import android.os.ParcelFileDescriptor;

public class FullBackupDataOutput {
  private final BackupDataOutput mData;
  
  private final long mQuota;
  
  private long mSize;
  
  private final int mTransportFlags;
  
  public long getQuota() {
    return this.mQuota;
  }
  
  public int getTransportFlags() {
    return this.mTransportFlags;
  }
  
  public FullBackupDataOutput(long paramLong) {
    this.mData = null;
    this.mQuota = paramLong;
    this.mSize = 0L;
    this.mTransportFlags = 0;
  }
  
  public FullBackupDataOutput(long paramLong, int paramInt) {
    this.mData = null;
    this.mQuota = paramLong;
    this.mSize = 0L;
    this.mTransportFlags = paramInt;
  }
  
  public FullBackupDataOutput(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong) {
    this.mData = new BackupDataOutput(paramParcelFileDescriptor.getFileDescriptor(), paramLong, 0);
    this.mQuota = paramLong;
    this.mTransportFlags = 0;
  }
  
  public FullBackupDataOutput(ParcelFileDescriptor paramParcelFileDescriptor, long paramLong, int paramInt) {
    this.mData = new BackupDataOutput(paramParcelFileDescriptor.getFileDescriptor(), paramLong, paramInt);
    this.mQuota = paramLong;
    this.mTransportFlags = paramInt;
  }
  
  public FullBackupDataOutput(ParcelFileDescriptor paramParcelFileDescriptor) {
    this(paramParcelFileDescriptor, -1L, 0);
  }
  
  public BackupDataOutput getData() {
    return this.mData;
  }
  
  public void addSize(long paramLong) {
    if (paramLong > 0L)
      this.mSize += paramLong; 
  }
  
  public long getSize() {
    return this.mSize;
  }
}
