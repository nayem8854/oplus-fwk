package android.app.backup;

import android.annotation.SystemApi;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@SystemApi
public class RestoreSession {
  static final String TAG = "RestoreSession";
  
  IRestoreSession mBinder;
  
  final Context mContext;
  
  RestoreObserverWrapper mObserver = null;
  
  public int getAvailableRestoreSets(RestoreObserver paramRestoreObserver, BackupManagerMonitor paramBackupManagerMonitor) {
    BackupManagerMonitorWrapper backupManagerMonitorWrapper;
    int i = -1;
    RestoreObserverWrapper restoreObserverWrapper = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
    if (paramBackupManagerMonitor == null) {
      paramRestoreObserver = null;
    } else {
      backupManagerMonitorWrapper = new BackupManagerMonitorWrapper(paramBackupManagerMonitor);
    } 
    try {
      int j = this.mBinder.getAvailableRestoreSets(restoreObserverWrapper, backupManagerMonitorWrapper);
    } catch (RemoteException remoteException) {
      Log.d("RestoreSession", "Can't contact server to get available sets");
    } 
    return i;
  }
  
  public int getAvailableRestoreSets(RestoreObserver paramRestoreObserver) {
    return getAvailableRestoreSets(paramRestoreObserver, null);
  }
  
  public int restoreAll(long paramLong, RestoreObserver paramRestoreObserver, BackupManagerMonitor paramBackupManagerMonitor) {
    BackupManagerMonitorWrapper backupManagerMonitorWrapper;
    int i = -1;
    if (this.mObserver != null) {
      Log.d("RestoreSession", "restoreAll() called during active restore");
      return -1;
    } 
    this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
    if (paramBackupManagerMonitor == null) {
      paramRestoreObserver = null;
    } else {
      backupManagerMonitorWrapper = new BackupManagerMonitorWrapper(paramBackupManagerMonitor);
    } 
    try {
      int j = this.mBinder.restoreAll(paramLong, this.mObserver, backupManagerMonitorWrapper);
    } catch (RemoteException remoteException) {
      Log.d("RestoreSession", "Can't contact server to restore");
    } 
    return i;
  }
  
  public int restoreAll(long paramLong, RestoreObserver paramRestoreObserver) {
    return restoreAll(paramLong, paramRestoreObserver, null);
  }
  
  public int restorePackages(long paramLong, RestoreObserver paramRestoreObserver, Set<String> paramSet, BackupManagerMonitor paramBackupManagerMonitor) {
    BackupManagerMonitorWrapper backupManagerMonitorWrapper;
    int i = -1;
    if (this.mObserver != null) {
      Log.d("RestoreSession", "restoreAll() called during active restore");
      return -1;
    } 
    this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
    if (paramBackupManagerMonitor == null) {
      paramRestoreObserver = null;
    } else {
      backupManagerMonitorWrapper = new BackupManagerMonitorWrapper(paramBackupManagerMonitor);
    } 
    try {
      int j = this.mBinder.restorePackages(paramLong, this.mObserver, paramSet.<String>toArray(new String[0]), backupManagerMonitorWrapper);
    } catch (RemoteException remoteException) {
      Log.d("RestoreSession", "Can't contact server to restore packages");
    } 
    return i;
  }
  
  public int restorePackages(long paramLong, RestoreObserver paramRestoreObserver, Set<String> paramSet) {
    return restorePackages(paramLong, paramRestoreObserver, paramSet, null);
  }
  
  @Deprecated
  public int restoreSome(long paramLong, RestoreObserver paramRestoreObserver, BackupManagerMonitor paramBackupManagerMonitor, String[] paramArrayOfString) {
    return restorePackages(paramLong, paramRestoreObserver, new HashSet<>(Arrays.asList(paramArrayOfString)), paramBackupManagerMonitor);
  }
  
  @Deprecated
  public int restoreSome(long paramLong, RestoreObserver paramRestoreObserver, String[] paramArrayOfString) {
    return restoreSome(paramLong, paramRestoreObserver, null, paramArrayOfString);
  }
  
  public int restorePackage(String paramString, RestoreObserver paramRestoreObserver, BackupManagerMonitor paramBackupManagerMonitor) {
    BackupManagerMonitorWrapper backupManagerMonitorWrapper;
    int i = -1;
    if (this.mObserver != null) {
      Log.d("RestoreSession", "restorePackage() called during active restore");
      return -1;
    } 
    this.mObserver = new RestoreObserverWrapper(this.mContext, paramRestoreObserver);
    if (paramBackupManagerMonitor == null) {
      paramRestoreObserver = null;
    } else {
      backupManagerMonitorWrapper = new BackupManagerMonitorWrapper(paramBackupManagerMonitor);
    } 
    try {
      int j = this.mBinder.restorePackage(paramString, this.mObserver, backupManagerMonitorWrapper);
    } catch (RemoteException remoteException) {
      Log.d("RestoreSession", "Can't contact server to restore package");
    } 
    return i;
  }
  
  public int restorePackage(String paramString, RestoreObserver paramRestoreObserver) {
    return restorePackage(paramString, paramRestoreObserver, null);
  }
  
  public void endRestoreSession() {
    try {
      this.mBinder.endRestoreSession();
      this.mBinder = null;
    } catch (RemoteException remoteException) {
      Log.d("RestoreSession", "Can't contact server to get available sets");
      this.mBinder = null;
    } finally {
      Exception exception;
    } 
  }
  
  RestoreSession(Context paramContext, IRestoreSession paramIRestoreSession) {
    this.mContext = paramContext;
    this.mBinder = paramIRestoreSession;
  }
  
  class RestoreObserverWrapper extends IRestoreObserver.Stub {
    static final int MSG_RESTORE_FINISHED = 3;
    
    static final int MSG_RESTORE_SETS_AVAILABLE = 4;
    
    static final int MSG_RESTORE_STARTING = 1;
    
    static final int MSG_UPDATE = 2;
    
    final RestoreObserver mAppObserver;
    
    final Handler mHandler;
    
    final RestoreSession this$0;
    
    RestoreObserverWrapper(Context param1Context, RestoreObserver param1RestoreObserver) {
      this.mHandler = (Handler)new Object(this, param1Context.getMainLooper(), RestoreSession.this);
      this.mAppObserver = param1RestoreObserver;
    }
    
    public void restoreSetsAvailable(RestoreSet[] param1ArrayOfRestoreSet) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(4, param1ArrayOfRestoreSet);
      handler.sendMessage(message);
    }
    
    public void restoreStarting(int param1Int) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(1, param1Int, 0);
      handler.sendMessage(message);
    }
    
    public void onUpdate(int param1Int, String param1String) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(2, param1Int, 0, param1String);
      handler.sendMessage(message);
    }
    
    public void restoreFinished(int param1Int) {
      Handler handler = this.mHandler;
      Message message = handler.obtainMessage(3, param1Int, 0);
      handler.sendMessage(message);
    }
  }
  
  class BackupManagerMonitorWrapper extends IBackupManagerMonitor.Stub {
    final BackupManagerMonitor mMonitor;
    
    final RestoreSession this$0;
    
    BackupManagerMonitorWrapper(BackupManagerMonitor param1BackupManagerMonitor) {
      this.mMonitor = param1BackupManagerMonitor;
    }
    
    public void onEvent(Bundle param1Bundle) throws RemoteException {
      this.mMonitor.onEvent(param1Bundle);
    }
  }
}
