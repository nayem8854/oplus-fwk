package android.app.backup;

import android.os.ParcelFileDescriptor;

public interface BackupHelper {
  void performBackup(ParcelFileDescriptor paramParcelFileDescriptor1, BackupDataOutput paramBackupDataOutput, ParcelFileDescriptor paramParcelFileDescriptor2);
  
  void restoreEntity(BackupDataInputStream paramBackupDataInputStream);
  
  void writeNewStateDescription(ParcelFileDescriptor paramParcelFileDescriptor);
}
