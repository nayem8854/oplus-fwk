package android.app.backup;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

public class BackupUtils {
  public static boolean isFileSpecifiedInPathList(File paramFile, Collection<FullBackup.BackupScheme.PathWithRequiredFlags> paramCollection) throws IOException {
    for (FullBackup.BackupScheme.PathWithRequiredFlags pathWithRequiredFlags : paramCollection) {
      String str = pathWithRequiredFlags.getPath();
      File file = new File(str);
      if (file.isDirectory()) {
        if (paramFile.isDirectory()) {
          if (paramFile.equals(file))
            return true; 
          continue;
        } 
        if (paramFile.toPath().startsWith(str))
          return true; 
        continue;
      } 
      if (paramFile.equals(file))
        return true; 
    } 
    return false;
  }
}
