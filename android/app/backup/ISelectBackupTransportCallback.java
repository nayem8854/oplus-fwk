package android.app.backup;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISelectBackupTransportCallback extends IInterface {
  void onFailure(int paramInt) throws RemoteException;
  
  void onSuccess(String paramString) throws RemoteException;
  
  class Default implements ISelectBackupTransportCallback {
    public void onSuccess(String param1String) throws RemoteException {}
    
    public void onFailure(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISelectBackupTransportCallback {
    private static final String DESCRIPTOR = "android.app.backup.ISelectBackupTransportCallback";
    
    static final int TRANSACTION_onFailure = 2;
    
    static final int TRANSACTION_onSuccess = 1;
    
    public Stub() {
      attachInterface(this, "android.app.backup.ISelectBackupTransportCallback");
    }
    
    public static ISelectBackupTransportCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.backup.ISelectBackupTransportCallback");
      if (iInterface != null && iInterface instanceof ISelectBackupTransportCallback)
        return (ISelectBackupTransportCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onFailure";
      } 
      return "onSuccess";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.app.backup.ISelectBackupTransportCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.app.backup.ISelectBackupTransportCallback");
        param1Int1 = param1Parcel1.readInt();
        onFailure(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.backup.ISelectBackupTransportCallback");
      String str = param1Parcel1.readString();
      onSuccess(str);
      return true;
    }
    
    private static class Proxy implements ISelectBackupTransportCallback {
      public static ISelectBackupTransportCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.backup.ISelectBackupTransportCallback";
      }
      
      public void onSuccess(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.backup.ISelectBackupTransportCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISelectBackupTransportCallback.Stub.getDefaultImpl() != null) {
            ISelectBackupTransportCallback.Stub.getDefaultImpl().onSuccess(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFailure(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.backup.ISelectBackupTransportCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ISelectBackupTransportCallback.Stub.getDefaultImpl() != null) {
            ISelectBackupTransportCallback.Stub.getDefaultImpl().onFailure(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISelectBackupTransportCallback param1ISelectBackupTransportCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISelectBackupTransportCallback != null) {
          Proxy.sDefaultImpl = param1ISelectBackupTransportCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISelectBackupTransportCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
