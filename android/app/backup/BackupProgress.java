package android.app.backup;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public class BackupProgress implements Parcelable {
  public BackupProgress(long paramLong1, long paramLong2) {
    this.bytesExpected = paramLong1;
    this.bytesTransferred = paramLong2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.bytesExpected);
    paramParcel.writeLong(this.bytesTransferred);
  }
  
  public static final Parcelable.Creator<BackupProgress> CREATOR = new Parcelable.Creator<BackupProgress>() {
      public BackupProgress createFromParcel(Parcel param1Parcel) {
        return new BackupProgress(param1Parcel);
      }
      
      public BackupProgress[] newArray(int param1Int) {
        return new BackupProgress[param1Int];
      }
    };
  
  public final long bytesExpected;
  
  public final long bytesTransferred;
  
  private BackupProgress(Parcel paramParcel) {
    this.bytesExpected = paramParcel.readLong();
    this.bytesTransferred = paramParcel.readLong();
  }
}
