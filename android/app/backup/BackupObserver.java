package android.app.backup;

import android.annotation.SystemApi;

@SystemApi
public abstract class BackupObserver {
  public void onUpdate(String paramString, BackupProgress paramBackupProgress) {}
  
  public void onResult(String paramString, int paramInt) {}
  
  public void backupFinished(int paramInt) {}
}
