package android.app;

public interface OnActivityPausedListener {
  void onPaused(Activity paramActivity);
}
