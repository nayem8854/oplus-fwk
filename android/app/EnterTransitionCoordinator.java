package android.app;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.util.ArrayMap;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroupOverlay;
import android.view.ViewTreeObserver;
import android.view.Window;
import com.android.internal.view.OneShotPreDrawListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

class EnterTransitionCoordinator extends ActivityTransitionCoordinator {
  private static final int MIN_ANIMATION_FRAMES = 2;
  
  private static final String TAG = "EnterTransitionCoordinator";
  
  private Activity mActivity;
  
  private boolean mAreViewsReady;
  
  private ObjectAnimator mBackgroundAnimator;
  
  private Transition mEnterViewsTransition;
  
  private boolean mHasStopped;
  
  private boolean mIsCanceled;
  
  private final boolean mIsCrossTask;
  
  private boolean mIsExitTransitionComplete;
  
  private boolean mIsReadyForTransition;
  
  private boolean mIsViewsTransitionStarted;
  
  private Runnable mOnTransitionComplete;
  
  private ArrayList<String> mPendingExitNames;
  
  private Drawable mReplacedBackground;
  
  private boolean mSharedElementTransitionStarted;
  
  private Bundle mSharedElementsBundle;
  
  private OneShotPreDrawListener mViewsReadyListener;
  
  private boolean mWasOpaque;
  
  EnterTransitionCoordinator(Activity paramActivity, ResultReceiver paramResultReceiver, ArrayList<String> paramArrayList, boolean paramBoolean1, boolean paramBoolean2) {
    super(window, paramArrayList, sharedElementCallback, paramBoolean1);
    boolean bool;
    this.mActivity = paramActivity;
    this.mIsCrossTask = paramBoolean2;
    setResultReceiver(paramResultReceiver);
    prepareEnter();
    Bundle bundle = new Bundle();
    bundle.putParcelable("android:remoteReceiver", (Parcelable)this);
    this.mResultReceiver.send(100, bundle);
    ViewGroup viewGroup = getDecor();
    if (viewGroup != null) {
      ViewTreeObserver viewTreeObserver = viewGroup.getViewTreeObserver();
      viewTreeObserver.addOnPreDrawListener((ViewTreeObserver.OnPreDrawListener)new Object(this, viewTreeObserver, (View)viewGroup));
    } 
  }
  
  boolean isCrossTask() {
    return this.mIsCrossTask;
  }
  
  public void viewInstancesReady(ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2, ArrayList<View> paramArrayList) {
    boolean bool2, bool1 = false;
    byte b = 0;
    while (true) {
      bool2 = bool1;
      if (b < paramArrayList.size()) {
        View view = paramArrayList.get(b);
        if (!TextUtils.equals(view.getTransitionName(), paramArrayList2.get(b)) || 
          !view.isAttachedToWindow()) {
          bool2 = true;
          break;
        } 
        b++;
        continue;
      } 
      break;
    } 
    if (bool2) {
      triggerViewsReady(mapNamedElements(paramArrayList1, paramArrayList2));
    } else {
      triggerViewsReady(mapSharedElements(paramArrayList1, paramArrayList));
    } 
  }
  
  public void namedViewsReady(ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2) {
    triggerViewsReady(mapNamedElements(paramArrayList1, paramArrayList2));
  }
  
  public Transition getEnterViewsTransition() {
    return this.mEnterViewsTransition;
  }
  
  protected void viewsReady(ArrayMap<String, View> paramArrayMap) {
    super.viewsReady(paramArrayMap);
    this.mIsReadyForTransition = true;
    hideViews(this.mSharedElements);
    Transition transition = getViewsTransition();
    if (transition != null && this.mTransitioningViews != null) {
      removeExcludedViews(transition, this.mTransitioningViews);
      stripOffscreenViews();
      hideViews(this.mTransitioningViews);
    } 
    if (this.mIsReturning) {
      sendSharedElementDestination();
    } else {
      moveSharedElementsToOverlay();
    } 
    if (this.mSharedElementsBundle != null)
      onTakeSharedElements(); 
  }
  
  private void triggerViewsReady(ArrayMap<String, View> paramArrayMap) {
    if (this.mAreViewsReady)
      return; 
    this.mAreViewsReady = true;
    ViewGroup viewGroup = getDecor();
    if (viewGroup == null || (viewGroup.isAttachedToWindow() && (
      paramArrayMap.isEmpty() || !((View)paramArrayMap.valueAt(0)).isLayoutRequested()))) {
      viewsReady(paramArrayMap);
      return;
    } 
    this.mViewsReadyListener = OneShotPreDrawListener.add((View)viewGroup, new _$$Lambda$EnterTransitionCoordinator$wYWFlx9zS3bxJYkN44Bpwx_EKis(this, paramArrayMap));
    viewGroup.invalidate();
  }
  
  private ArrayMap<String, View> mapNamedElements(ArrayList<String> paramArrayList1, ArrayList<String> paramArrayList2) {
    ArrayMap<String, View> arrayMap = new ArrayMap();
    ViewGroup viewGroup = getDecor();
    if (viewGroup != null)
      viewGroup.findNamedViews((Map)arrayMap); 
    if (paramArrayList1 != null)
      for (byte b = 0; b < paramArrayList2.size(); b++) {
        String str2 = paramArrayList2.get(b);
        String str1 = paramArrayList1.get(b);
        if (str2 != null && !str2.equals(str1)) {
          View view = (View)arrayMap.get(str2);
          if (view != null)
            arrayMap.put(str1, view); 
        } 
      }  
    return arrayMap;
  }
  
  private void sendSharedElementDestination() {
    Bundle bundle;
    int i;
    ViewGroup viewGroup = getDecor();
    if (allowOverlappingTransitions() && getEnterViewsTransition() != null) {
      i = 0;
    } else if (viewGroup == null) {
      i = 1;
    } else {
      int j = viewGroup.isLayoutRequested() ^ true;
      i = j;
      if (j != 0) {
        byte b = 0;
        while (true) {
          i = j;
          if (b < this.mSharedElements.size()) {
            if (((View)this.mSharedElements.get(b)).isLayoutRequested()) {
              i = 0;
              break;
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    } 
    if (i != 0) {
      if (this.mResultReceiver != null) {
        bundle = captureSharedElementState();
        moveSharedElementsToOverlay();
        this.mResultReceiver.send(107, bundle);
      } 
    } else if (bundle != null) {
      OneShotPreDrawListener.add((View)bundle, new _$$Lambda$EnterTransitionCoordinator$dV8bqDBqB_WsCnMyvajWuP4ArwA(this));
    } 
    if (allowOverlappingTransitions())
      startEnterTransitionOnly(); 
  }
  
  private static SharedElementCallback getListener(Activity paramActivity, boolean paramBoolean) {
    SharedElementCallback sharedElementCallback;
    if (paramBoolean) {
      sharedElementCallback = paramActivity.mExitTransitionListener;
    } else {
      sharedElementCallback = ((Activity)sharedElementCallback).mEnterTransitionListener;
    } 
    return sharedElementCallback;
  }
  
  protected void onReceiveResult(int paramInt, Bundle paramBundle) {
    if (paramInt != 103) {
      if (paramInt != 104) {
        if (paramInt != 106) {
          if (paramInt == 108)
            if (!this.mIsCanceled)
              this.mPendingExitNames = this.mAllSharedElementNames;  
        } else {
          cancel();
        } 
      } else if (!this.mIsCanceled) {
        this.mIsExitTransitionComplete = true;
        if (this.mSharedElementTransitionStarted)
          onRemoteExitTransitionComplete(); 
      } 
    } else if (!this.mIsCanceled) {
      this.mSharedElementsBundle = paramBundle;
      onTakeSharedElements();
    } 
  }
  
  public boolean isWaitingForRemoteExit() {
    boolean bool;
    if (this.mIsReturning && this.mResultReceiver != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ArrayList<String> getPendingExitSharedElementNames() {
    return this.mPendingExitNames;
  }
  
  public void forceViewsToAppear() {
    if (!this.mIsReturning)
      return; 
    if (!this.mIsReadyForTransition) {
      this.mIsReadyForTransition = true;
      ViewGroup viewGroup = getDecor();
      if (viewGroup != null) {
        OneShotPreDrawListener oneShotPreDrawListener = this.mViewsReadyListener;
        if (oneShotPreDrawListener != null) {
          oneShotPreDrawListener.removeListener();
          this.mViewsReadyListener = null;
        } 
      } 
      showViews(this.mTransitioningViews, true);
      setTransitioningViewsVisiblity(0, true);
      this.mSharedElements.clear();
      this.mAllSharedElementNames.clear();
      this.mTransitioningViews.clear();
      this.mIsReadyForTransition = true;
      viewsTransitionComplete();
      sharedElementTransitionComplete();
    } else {
      if (!this.mSharedElementTransitionStarted) {
        moveSharedElementsFromOverlay();
        this.mSharedElementTransitionStarted = true;
        showViews(this.mSharedElements, true);
        this.mSharedElements.clear();
        sharedElementTransitionComplete();
      } 
      if (!this.mIsViewsTransitionStarted) {
        this.mIsViewsTransitionStarted = true;
        showViews(this.mTransitioningViews, true);
        setTransitioningViewsVisiblity(0, true);
        this.mTransitioningViews.clear();
        viewsTransitionComplete();
      } 
      cancelPendingTransitions();
    } 
    this.mAreViewsReady = true;
    if (this.mResultReceiver != null) {
      this.mResultReceiver.send(106, null);
      this.mResultReceiver = null;
    } 
  }
  
  private void cancel() {
    if (!this.mIsCanceled) {
      this.mIsCanceled = true;
      if (getViewsTransition() == null || this.mIsViewsTransitionStarted) {
        showViews(this.mSharedElements, true);
      } else if (this.mTransitioningViews != null) {
        this.mTransitioningViews.addAll(this.mSharedElements);
      } 
      moveSharedElementsFromOverlay();
      this.mSharedElementNames.clear();
      this.mSharedElements.clear();
      this.mAllSharedElementNames.clear();
      startSharedElementTransition((Bundle)null);
      onRemoteExitTransitionComplete();
    } 
  }
  
  public boolean isReturning() {
    return this.mIsReturning;
  }
  
  protected void prepareEnter() {
    ViewGroup viewGroup = getDecor();
    if (this.mActivity == null || viewGroup == null)
      return; 
    if (!isCrossTask())
      this.mActivity.overridePendingTransition(0, 0); 
    if (!this.mIsReturning) {
      this.mWasOpaque = this.mActivity.convertToTranslucent(null, null);
      Drawable drawable = viewGroup.getBackground();
      if (drawable == null) {
        drawable = new ColorDrawable(0);
        this.mReplacedBackground = drawable;
      } else {
        getWindow().setBackgroundDrawable(null);
        drawable = drawable.mutate();
        drawable.setAlpha(0);
      } 
      getWindow().setBackgroundDrawable(drawable);
    } else {
      this.mActivity = null;
    } 
  }
  
  protected Transition getViewsTransition() {
    Window window = getWindow();
    if (window == null)
      return null; 
    if (this.mIsReturning)
      return window.getReenterTransition(); 
    return window.getEnterTransition();
  }
  
  protected Transition getSharedElementTransition() {
    Window window = getWindow();
    if (window == null)
      return null; 
    if (this.mIsReturning)
      return window.getSharedElementReenterTransition(); 
    return window.getSharedElementEnterTransition();
  }
  
  private void startSharedElementTransition(Bundle paramBundle) {
    ViewGroup viewGroup = getDecor();
    if (viewGroup == null)
      return; 
    ArrayList<String> arrayList = new ArrayList<>(this.mAllSharedElementNames);
    arrayList.removeAll(this.mSharedElementNames);
    arrayList = (ArrayList)createSnapshots(paramBundle, arrayList);
    if (this.mListener != null)
      this.mListener.onRejectSharedElements((List)arrayList); 
    removeNullViews((ArrayList)arrayList);
    startRejectedAnimations((ArrayList)arrayList);
    ArrayList<View> arrayList2 = createSnapshots(paramBundle, this.mSharedElementNames);
    ArrayList<View> arrayList1 = this.mSharedElements;
    boolean bool = true;
    showViews(arrayList1, true);
    scheduleSetSharedElementEnd(arrayList2);
    arrayList1 = (ArrayList)setSharedElementState(paramBundle, arrayList2);
    requestLayoutForSharedElements();
    if (!allowOverlappingTransitions() || this.mIsReturning)
      bool = false; 
    setGhostVisibility(4);
    scheduleGhostVisibilityChange(4);
    pauseInput();
    Transition transition = beginTransition(viewGroup, bool, true);
    scheduleGhostVisibilityChange(0);
    setGhostVisibility(0);
    if (bool)
      startEnterTransition(transition); 
    setOriginalSharedElementState(this.mSharedElements, (ArrayList)arrayList1);
    if (this.mResultReceiver != null)
      viewGroup.postOnAnimation((Runnable)new Object(this)); 
  }
  
  private static void removeNullViews(ArrayList<View> paramArrayList) {
    if (paramArrayList != null)
      for (int i = paramArrayList.size() - 1; i >= 0; i--) {
        if (paramArrayList.get(i) == null)
          paramArrayList.remove(i); 
      }  
  }
  
  private void onTakeSharedElements() {
    if (!this.mIsReadyForTransition || this.mSharedElementsBundle == null)
      return; 
    Bundle bundle = this.mSharedElementsBundle;
    this.mSharedElementsBundle = null;
    Object object = new Object(this, bundle);
    if (this.mListener == null) {
      object.onSharedElementsReady();
    } else {
      this.mListener.onSharedElementsArrived(this.mSharedElementNames, this.mSharedElements, (SharedElementCallback.OnSharedElementsReadyListener)object);
    } 
  }
  
  private void requestLayoutForSharedElements() {
    int i = this.mSharedElements.size();
    for (byte b = 0; b < i; b++)
      ((View)this.mSharedElements.get(b)).requestLayout(); 
  }
  
  private Transition beginTransition(ViewGroup paramViewGroup, boolean paramBoolean1, boolean paramBoolean2) {
    Transition transition1 = null, transition2 = null;
    if (paramBoolean2) {
      if (!this.mSharedElementNames.isEmpty())
        transition2 = configureTransition(getSharedElementTransition(), false); 
      if (transition2 == null) {
        sharedElementTransitionStarted();
        sharedElementTransitionComplete();
        transition1 = transition2;
      } else {
        transition2.addListener((Transition.TransitionListener)new Object(this));
        transition1 = transition2;
      } 
    } 
    transition2 = null;
    Transition transition3 = null;
    if (paramBoolean1) {
      this.mIsViewsTransitionStarted = true;
      transition2 = transition3;
      if (this.mTransitioningViews != null) {
        transition2 = transition3;
        if (!this.mTransitioningViews.isEmpty())
          transition2 = configureTransition(getViewsTransition(), true); 
      } 
      if (transition2 == null) {
        viewsTransitionComplete();
      } else {
        final ArrayList<View> transitioningViews = this.mTransitioningViews;
        transition2.addListener((Transition.TransitionListener)new ActivityTransitionCoordinator.ContinueTransitionListener() {
              final EnterTransitionCoordinator this$0;
              
              final ArrayList val$transitioningViews;
              
              public void onTransitionStart(Transition param1Transition) {
                EnterTransitionCoordinator.access$302(EnterTransitionCoordinator.this, param1Transition);
                ArrayList<View> arrayList = transitioningViews;
                if (arrayList != null)
                  EnterTransitionCoordinator.this.showViews(arrayList, false); 
                super.onTransitionStart(param1Transition);
              }
              
              public void onTransitionEnd(Transition param1Transition) {
                EnterTransitionCoordinator.access$302(EnterTransitionCoordinator.this, (Transition)null);
                param1Transition.removeListener((Transition.TransitionListener)this);
                EnterTransitionCoordinator.this.viewsTransitionComplete();
                super.onTransitionEnd(param1Transition);
              }
            });
      } 
    } 
    transition2 = mergeTransitions(transition1, transition2);
    if (transition2 != null) {
      transition2.addListener((Transition.TransitionListener)new ActivityTransitionCoordinator.ContinueTransitionListener(this));
      if (paramBoolean1)
        setTransitioningViewsVisiblity(4, false); 
      TransitionManager.beginDelayedTransition(paramViewGroup, transition2);
      if (paramBoolean1)
        setTransitioningViewsVisiblity(0, false); 
      paramViewGroup.invalidate();
    } else {
      transitionStarted();
    } 
    return transition2;
  }
  
  public void runAfterTransitionsComplete(Runnable paramRunnable) {
    if (!isTransitionRunning()) {
      onTransitionsComplete();
    } else {
      this.mOnTransitionComplete = paramRunnable;
    } 
  }
  
  protected void onTransitionsComplete() {
    moveSharedElementsFromOverlay();
    ViewGroup viewGroup = getDecor();
    if (viewGroup != null) {
      viewGroup.sendAccessibilityEvent(2048);
      Window window = getWindow();
      if (window != null && this.mReplacedBackground == viewGroup.getBackground())
        window.setBackgroundDrawable(null); 
    } 
    Runnable runnable = this.mOnTransitionComplete;
    if (runnable != null) {
      runnable.run();
      this.mOnTransitionComplete = null;
    } 
  }
  
  private void sharedElementTransitionStarted() {
    this.mSharedElementTransitionStarted = true;
    if (this.mIsExitTransitionComplete)
      send(104, null); 
  }
  
  private void startEnterTransition(Transition paramTransition) {
    ViewGroup viewGroup = getDecor();
    if (!this.mIsReturning && viewGroup != null) {
      ObjectAnimator objectAnimator;
      Drawable drawable = viewGroup.getBackground();
      if (drawable != null) {
        Drawable drawable1 = drawable.mutate();
        getWindow().setBackgroundDrawable(drawable1);
        this.mBackgroundAnimator = objectAnimator = ObjectAnimator.ofInt(drawable1, "alpha", new int[] { 255 });
        objectAnimator.setDuration(getFadeDuration());
        this.mBackgroundAnimator.addListener((Animator.AnimatorListener)new Object(this));
        this.mBackgroundAnimator.start();
      } else if (objectAnimator != null) {
        objectAnimator.addListener((Transition.TransitionListener)new Object(this));
        backgroundAnimatorComplete();
      } else {
        makeOpaque();
        backgroundAnimatorComplete();
      } 
    } else {
      backgroundAnimatorComplete();
    } 
  }
  
  public void stop() {
    ObjectAnimator objectAnimator = this.mBackgroundAnimator;
    if (objectAnimator != null) {
      objectAnimator.end();
      this.mBackgroundAnimator = null;
    } else if (this.mWasOpaque) {
      ViewGroup viewGroup = getDecor();
      if (viewGroup != null) {
        Drawable drawable = viewGroup.getBackground();
        if (drawable != null)
          drawable.setAlpha(1); 
      } 
    } 
    makeOpaque();
    this.mIsCanceled = true;
    this.mResultReceiver = null;
    this.mActivity = null;
    moveSharedElementsFromOverlay();
    if (this.mTransitioningViews != null) {
      showViews(this.mTransitioningViews, true);
      setTransitioningViewsVisiblity(0, true);
    } 
    showViews(this.mSharedElements, true);
    clearState();
  }
  
  public boolean cancelEnter() {
    setGhostVisibility(4);
    this.mHasStopped = true;
    this.mIsCanceled = true;
    clearState();
    return cancelPendingTransitions();
  }
  
  protected void clearState() {
    this.mSharedElementsBundle = null;
    this.mEnterViewsTransition = null;
    this.mResultReceiver = null;
    ObjectAnimator objectAnimator = this.mBackgroundAnimator;
    if (objectAnimator != null) {
      objectAnimator.cancel();
      this.mBackgroundAnimator = null;
    } 
    Runnable runnable = this.mOnTransitionComplete;
    if (runnable != null) {
      runnable.run();
      this.mOnTransitionComplete = null;
    } 
    super.clearState();
  }
  
  private void makeOpaque() {
    if (!this.mHasStopped) {
      Activity activity = this.mActivity;
      if (activity != null) {
        if (this.mWasOpaque)
          activity.convertFromTranslucent(); 
        this.mActivity = null;
      } 
    } 
  }
  
  private boolean allowOverlappingTransitions() {
    boolean bool;
    if (this.mIsReturning) {
      bool = getWindow().getAllowReturnTransitionOverlap();
    } else {
      bool = getWindow().getAllowEnterTransitionOverlap();
    } 
    return bool;
  }
  
  private void startRejectedAnimations(ArrayList<View> paramArrayList) {
    if (paramArrayList == null || paramArrayList.isEmpty())
      return; 
    ViewGroup viewGroup = getDecor();
    if (viewGroup != null) {
      ObjectAnimator objectAnimator;
      ViewGroupOverlay viewGroupOverlay = viewGroup.getOverlay();
      View view = null;
      int i = paramArrayList.size();
      for (byte b = 0; b < i; b++) {
        view = paramArrayList.get(b);
        viewGroupOverlay.add(view);
        objectAnimator = ObjectAnimator.ofFloat(view, View.ALPHA, new float[] { 1.0F, 0.0F });
        objectAnimator.start();
      } 
      objectAnimator.addListener((Animator.AnimatorListener)new Object(this, viewGroup, paramArrayList));
    } 
  }
  
  protected void onRemoteExitTransitionComplete() {
    if (!allowOverlappingTransitions())
      startEnterTransitionOnly(); 
  }
  
  private void startEnterTransitionOnly() {
    startTransition((Runnable)new Object(this));
  }
}
