package android.app;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.ColorSpace;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.HardwareBuffer;
import android.os.Bundle;
import android.os.Parcelable;
import android.transition.TransitionUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import java.util.List;
import java.util.Map;

public abstract class SharedElementCallback {
  private static final String BUNDLE_SNAPSHOT_BITMAP = "sharedElement:snapshot:bitmap";
  
  private static final String BUNDLE_SNAPSHOT_COLOR_SPACE = "sharedElement:snapshot:colorSpace";
  
  private static final String BUNDLE_SNAPSHOT_HARDWARE_BUFFER = "sharedElement:snapshot:hardwareBuffer";
  
  private static final String BUNDLE_SNAPSHOT_IMAGE_MATRIX = "sharedElement:snapshot:imageMatrix";
  
  private static final String BUNDLE_SNAPSHOT_IMAGE_SCALETYPE = "sharedElement:snapshot:imageScaleType";
  
  static final SharedElementCallback NULL_CALLBACK = (SharedElementCallback)new Object();
  
  private Matrix mTempMatrix;
  
  public void onSharedElementStart(List<String> paramList, List<View> paramList1, List<View> paramList2) {}
  
  public void onSharedElementEnd(List<String> paramList, List<View> paramList1, List<View> paramList2) {}
  
  public void onRejectSharedElements(List<View> paramList) {}
  
  public void onMapSharedElements(List<String> paramList, Map<String, View> paramMap) {}
  
  public Parcelable onCaptureSharedElementSnapshot(View paramView, Matrix paramMatrix, RectF paramRectF) {
    Bundle bundle;
    float[] arrayOfFloat;
    Matrix matrix1;
    if (paramView instanceof ImageView) {
      ImageView imageView = (ImageView)paramView;
      Drawable drawable1 = imageView.getDrawable();
      Drawable drawable2 = imageView.getBackground();
      if (drawable1 != null && (drawable2 == null || drawable2.getAlpha() == 0)) {
        Bitmap bitmap = TransitionUtils.createDrawableBitmap(drawable1, (View)imageView);
        if (bitmap != null) {
          bundle = new Bundle();
          if (bitmap.getConfig() != Bitmap.Config.HARDWARE) {
            bundle.putParcelable("sharedElement:snapshot:bitmap", bitmap);
          } else {
            HardwareBuffer hardwareBuffer = bitmap.getHardwareBuffer();
            bundle.putParcelable("sharedElement:snapshot:hardwareBuffer", hardwareBuffer);
            ColorSpace colorSpace = bitmap.getColorSpace();
            if (colorSpace != null)
              bundle.putInt("sharedElement:snapshot:colorSpace", colorSpace.getId()); 
          } 
          String str = imageView.getScaleType().toString();
          bundle.putString("sharedElement:snapshot:imageScaleType", str);
          if (imageView.getScaleType() == ImageView.ScaleType.MATRIX) {
            matrix1 = imageView.getImageMatrix();
            arrayOfFloat = new float[9];
            matrix1.getValues(arrayOfFloat);
            bundle.putFloatArray("sharedElement:snapshot:imageMatrix", arrayOfFloat);
          } 
          return (Parcelable)bundle;
        } 
      } 
    } 
    Matrix matrix2 = this.mTempMatrix;
    if (matrix2 == null) {
      this.mTempMatrix = new Matrix((Matrix)arrayOfFloat);
    } else {
      matrix2.set((Matrix)arrayOfFloat);
    } 
    ViewGroup viewGroup = (ViewGroup)bundle.getParent();
    return TransitionUtils.createViewBitmap((View)bundle, this.mTempMatrix, (RectF)matrix1, viewGroup);
  }
  
  public View onCreateSnapshotView(Context paramContext, Parcelable paramParcelable) {
    ImageView imageView;
    float[] arrayOfFloat;
    View view;
    Bitmap bitmap = null;
    if (paramParcelable instanceof Bundle) {
      Bitmap bitmap1;
      Bundle bundle = (Bundle)paramParcelable;
      HardwareBuffer hardwareBuffer = (HardwareBuffer)bundle.getParcelable("sharedElement:snapshot:hardwareBuffer");
      bitmap = (Bitmap)bundle.getParcelable("sharedElement:snapshot:bitmap");
      if (hardwareBuffer == null && bitmap == null)
        return null; 
      paramParcelable = bitmap;
      if (bitmap == null) {
        ColorSpace colorSpace;
        bitmap = null;
        int i = bundle.getInt("sharedElement:snapshot:colorSpace", 0);
        paramParcelable = bitmap;
        if (i >= 0) {
          paramParcelable = bitmap;
          if (i < (ColorSpace.Named.values()).length)
            colorSpace = ColorSpace.get(ColorSpace.Named.values()[i]); 
        } 
        bitmap1 = Bitmap.wrapHardwareBuffer(hardwareBuffer, colorSpace);
      } 
      ImageView imageView2 = new ImageView(paramContext);
      imageView = imageView2;
      imageView2.setImageBitmap(bitmap1);
      ImageView.ScaleType scaleType = ImageView.ScaleType.valueOf(bundle.getString("sharedElement:snapshot:imageScaleType"));
      imageView2.setScaleType(scaleType);
      ImageView imageView1 = imageView;
      if (imageView2.getScaleType() == ImageView.ScaleType.MATRIX) {
        arrayOfFloat = bundle.getFloatArray("sharedElement:snapshot:imageMatrix");
        Matrix matrix = new Matrix();
        matrix.setValues(arrayOfFloat);
        imageView2.setImageMatrix(matrix);
        ImageView imageView3 = imageView;
      } 
    } else if (arrayOfFloat instanceof Bitmap) {
      Bitmap bitmap1 = (Bitmap)arrayOfFloat;
      view = new View((Context)imageView);
      Resources resources = imageView.getResources();
      view.setBackground(new BitmapDrawable(resources, bitmap1));
    } 
    return view;
  }
  
  public void onSharedElementsArrived(List<String> paramList, List<View> paramList1, OnSharedElementsReadyListener paramOnSharedElementsReadyListener) {
    paramOnSharedElementsReadyListener.onSharedElementsReady();
  }
  
  public static interface OnSharedElementsReadyListener {
    void onSharedElementsReady();
  }
}
