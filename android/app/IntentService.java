package android.app;

import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

@Deprecated
public abstract class IntentService extends Service {
  private String mName;
  
  private boolean mRedelivery;
  
  private volatile ServiceHandler mServiceHandler;
  
  private volatile Looper mServiceLooper;
  
  class ServiceHandler extends Handler {
    final IntentService this$0;
    
    public ServiceHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      IntentService.this.onHandleIntent((Intent)param1Message.obj);
      IntentService.this.stopSelf(param1Message.arg1);
    }
  }
  
  public IntentService(String paramString) {
    this.mName = paramString;
  }
  
  public void setIntentRedelivery(boolean paramBoolean) {
    this.mRedelivery = paramBoolean;
  }
  
  public void onCreate() {
    super.onCreate();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("IntentService[");
    stringBuilder.append(this.mName);
    stringBuilder.append("]");
    HandlerThread handlerThread = new HandlerThread(stringBuilder.toString());
    handlerThread.start();
    this.mServiceLooper = handlerThread.getLooper();
    this.mServiceHandler = new ServiceHandler(this.mServiceLooper);
  }
  
  public void onStart(Intent paramIntent, int paramInt) {
    Message message = this.mServiceHandler.obtainMessage();
    message.arg1 = paramInt;
    message.obj = paramIntent;
    this.mServiceHandler.sendMessage(message);
  }
  
  public int onStartCommand(Intent paramIntent, int paramInt1, int paramInt2) {
    onStart(paramIntent, paramInt2);
    if (this.mRedelivery) {
      paramInt1 = 3;
    } else {
      paramInt1 = 2;
    } 
    return paramInt1;
  }
  
  public void onDestroy() {
    this.mServiceLooper.quit();
  }
  
  public IBinder onBind(Intent paramIntent) {
    return null;
  }
  
  protected abstract void onHandleIntent(Intent paramIntent);
}
