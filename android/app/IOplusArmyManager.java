package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IOplusArmyManager extends IInterface {
  boolean addDisallowedRunningApp(List<String> paramList) throws RemoteException;
  
  void allowToUseSdcard(boolean paramBoolean) throws RemoteException;
  
  List<String> getDisallowedRunningApp() throws RemoteException;
  
  boolean isAllowControlAppRun() throws RemoteException;
  
  boolean removeDisallowedRunningApp(List<String> paramList) throws RemoteException;
  
  void setAllowControlAppRun(boolean paramBoolean) throws RemoteException;
  
  class Default implements IOplusArmyManager {
    public boolean addDisallowedRunningApp(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public boolean removeDisallowedRunningApp(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public List<String> getDisallowedRunningApp() throws RemoteException {
      return null;
    }
    
    public void allowToUseSdcard(boolean param1Boolean) throws RemoteException {}
    
    public void setAllowControlAppRun(boolean param1Boolean) throws RemoteException {}
    
    public boolean isAllowControlAppRun() throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusArmyManager {
    private static final String DESCRIPTOR = "android.app.IOplusArmyManager";
    
    static final int TRANSACTION_addDisallowedRunningApp = 1;
    
    static final int TRANSACTION_allowToUseSdcard = 4;
    
    static final int TRANSACTION_getDisallowedRunningApp = 3;
    
    static final int TRANSACTION_isAllowControlAppRun = 6;
    
    static final int TRANSACTION_removeDisallowedRunningApp = 2;
    
    static final int TRANSACTION_setAllowControlAppRun = 5;
    
    public Stub() {
      attachInterface(this, "android.app.IOplusArmyManager");
    }
    
    public static IOplusArmyManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IOplusArmyManager");
      if (iInterface != null && iInterface instanceof IOplusArmyManager)
        return (IOplusArmyManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "isAllowControlAppRun";
        case 5:
          return "setAllowControlAppRun";
        case 4:
          return "allowToUseSdcard";
        case 3:
          return "getDisallowedRunningApp";
        case 2:
          return "removeDisallowedRunningApp";
        case 1:
          break;
      } 
      return "addDisallowedRunningApp";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.app.IOplusArmyManager");
            bool = isAllowControlAppRun();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.app.IOplusArmyManager");
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            setAllowControlAppRun(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.app.IOplusArmyManager");
            bool2 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            allowToUseSdcard(bool2);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.app.IOplusArmyManager");
            list = getDisallowedRunningApp();
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list);
            return true;
          case 2:
            list.enforceInterface("android.app.IOplusArmyManager");
            list = list.createStringArrayList();
            bool = removeDisallowedRunningApp(list);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.app.IOplusArmyManager");
        List<String> list = list.createStringArrayList();
        boolean bool = addDisallowedRunningApp(list);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.app.IOplusArmyManager");
      return true;
    }
    
    private static class Proxy implements IOplusArmyManager {
      public static IOplusArmyManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IOplusArmyManager";
      }
      
      public boolean addDisallowedRunningApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IOplusArmyManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusArmyManager.Stub.getDefaultImpl().addDisallowedRunningApp(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeDisallowedRunningApp(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IOplusArmyManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusArmyManager.Stub.getDefaultImpl().removeDisallowedRunningApp(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getDisallowedRunningApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IOplusArmyManager.Stub.getDefaultImpl() != null)
            return IOplusArmyManager.Stub.getDefaultImpl().getDisallowedRunningApp(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void allowToUseSdcard(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && IOplusArmyManager.Stub.getDefaultImpl() != null) {
            IOplusArmyManager.Stub.getDefaultImpl().allowToUseSdcard(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAllowControlAppRun(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool1 && IOplusArmyManager.Stub.getDefaultImpl() != null) {
            IOplusArmyManager.Stub.getDefaultImpl().setAllowControlAppRun(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAllowControlAppRun() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusArmyManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IOplusArmyManager.Stub.getDefaultImpl() != null) {
            bool1 = IOplusArmyManager.Stub.getDefaultImpl().isAllowControlAppRun();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusArmyManager param1IOplusArmyManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusArmyManager != null) {
          Proxy.sDefaultImpl = param1IOplusArmyManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusArmyManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
