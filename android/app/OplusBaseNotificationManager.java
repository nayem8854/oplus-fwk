package android.app;

import oplus.app.OplusCommonManager;

public class OplusBaseNotificationManager extends OplusCommonManager {
  private static final String TAG = "OplusBaseNotificationManager";
  
  public OplusBaseNotificationManager() {
    super("notification");
  }
}
