package android.app.timezonedetector;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;

public final class TimeZoneDetectorImpl implements TimeZoneDetector {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "timezonedetector.TimeZoneDetector";
  
  private final ITimeZoneDetectorService mITimeZoneDetectorService;
  
  public TimeZoneDetectorImpl() throws ServiceManager.ServiceNotFoundException {
    IBinder iBinder = ServiceManager.getServiceOrThrow("time_zone_detector");
    this.mITimeZoneDetectorService = ITimeZoneDetectorService.Stub.asInterface(iBinder);
  }
  
  public void suggestManualTimeZone(ManualTimeZoneSuggestion paramManualTimeZoneSuggestion) {
    try {
      this.mITimeZoneDetectorService.suggestManualTimeZone(paramManualTimeZoneSuggestion);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void suggestTelephonyTimeZone(TelephonyTimeZoneSuggestion paramTelephonyTimeZoneSuggestion) {
    try {
      this.mITimeZoneDetectorService.suggestTelephonyTimeZone(paramTelephonyTimeZoneSuggestion);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
