package android.app;

import java.util.List;

@Deprecated
public class FragmentManagerNonConfig {
  private final List<FragmentManagerNonConfig> mChildNonConfigs;
  
  private final List<Fragment> mFragments;
  
  FragmentManagerNonConfig(List<Fragment> paramList, List<FragmentManagerNonConfig> paramList1) {
    this.mFragments = paramList;
    this.mChildNonConfigs = paramList1;
  }
  
  List<Fragment> getFragments() {
    return this.mFragments;
  }
  
  List<FragmentManagerNonConfig> getChildNonConfigs() {
    return this.mChildNonConfigs;
  }
}
