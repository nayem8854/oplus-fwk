package android.app;

import android.app.servertransaction.TransactionExecutor;
import com.oplus.reflect.RefBoolean;
import com.oplus.reflect.RefClass;

public class OplusMirrorTransactionExecutor {
  public static RefBoolean DEBUG_RESOLVER;
  
  public static Class<?> TYPE = RefClass.load(OplusMirrorTransactionExecutor.class, TransactionExecutor.class);
  
  public static void setBooleanValue(RefBoolean paramRefBoolean, boolean paramBoolean) {
    if (paramRefBoolean != null)
      paramRefBoolean.set(null, paramBoolean); 
  }
}
