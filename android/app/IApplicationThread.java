package android.app;

import android.app.servertransaction.ClientTransaction;
import android.content.AutofillOptions;
import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ParceledListSlice;
import android.content.pm.ProviderInfo;
import android.content.pm.ProviderInfoList;
import android.content.pm.ServiceInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallback;
import android.os.RemoteException;
import com.android.internal.app.IVoiceInteractor;
import java.util.HashMap;
import java.util.Map;

public interface IApplicationThread extends IInterface {
  void attachAgent(String paramString) throws RemoteException;
  
  void attachStartupAgents(String paramString) throws RemoteException;
  
  void bindApplication(String paramString1, ApplicationInfo paramApplicationInfo, ProviderInfoList paramProviderInfoList, ComponentName paramComponentName, ProfilerInfo paramProfilerInfo, Bundle paramBundle1, IInstrumentationWatcher paramIInstrumentationWatcher, IUiAutomationConnection paramIUiAutomationConnection, int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, Map paramMap, Bundle paramBundle2, String paramString2, AutofillOptions paramAutofillOptions, ContentCaptureOptions paramContentCaptureOptions, long[] paramArrayOflong) throws RemoteException;
  
  void clearDnsCache() throws RemoteException;
  
  void dispatchPackageBroadcast(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  void dumpActivity(ParcelFileDescriptor paramParcelFileDescriptor, IBinder paramIBinder, String paramString, String[] paramArrayOfString) throws RemoteException;
  
  void dumpCacheInfo(ParcelFileDescriptor paramParcelFileDescriptor, String[] paramArrayOfString) throws RemoteException;
  
  void dumpDbInfo(ParcelFileDescriptor paramParcelFileDescriptor, String[] paramArrayOfString) throws RemoteException;
  
  void dumpGfxInfo(ParcelFileDescriptor paramParcelFileDescriptor, String[] paramArrayOfString) throws RemoteException;
  
  void dumpHeap(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString, ParcelFileDescriptor paramParcelFileDescriptor, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void dumpMemInfo(ParcelFileDescriptor paramParcelFileDescriptor, Debug.MemoryInfo paramMemoryInfo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, boolean paramBoolean5, String[] paramArrayOfString) throws RemoteException;
  
  void dumpMemInfoProto(ParcelFileDescriptor paramParcelFileDescriptor, Debug.MemoryInfo paramMemoryInfo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String[] paramArrayOfString) throws RemoteException;
  
  void dumpProvider(ParcelFileDescriptor paramParcelFileDescriptor, IBinder paramIBinder, String[] paramArrayOfString) throws RemoteException;
  
  void dumpService(ParcelFileDescriptor paramParcelFileDescriptor, IBinder paramIBinder, String[] paramArrayOfString) throws RemoteException;
  
  void getBroadcastState(int paramInt) throws RemoteException;
  
  void handleTrustStorageUpdate() throws RemoteException;
  
  void notifyCleartextNetwork(byte[] paramArrayOfbyte) throws RemoteException;
  
  void oppoScheduleReceiver(Intent paramIntent, ActivityInfo paramActivityInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt1, String paramString, Bundle paramBundle, boolean paramBoolean, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void performDirectAction(IBinder paramIBinder, String paramString, Bundle paramBundle, RemoteCallback paramRemoteCallback1, RemoteCallback paramRemoteCallback2) throws RemoteException;
  
  void processInBackground() throws RemoteException;
  
  void profilerControl(boolean paramBoolean, ProfilerInfo paramProfilerInfo, int paramInt) throws RemoteException;
  
  void requestAssistContextExtras(IBinder paramIBinder1, IBinder paramIBinder2, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void requestDirectActions(IBinder paramIBinder, IVoiceInteractor paramIVoiceInteractor, RemoteCallback paramRemoteCallback1, RemoteCallback paramRemoteCallback2) throws RemoteException;
  
  void runIsolatedEntryPoint(String paramString, String[] paramArrayOfString) throws RemoteException;
  
  void scheduleApplicationInfoChanged(ApplicationInfo paramApplicationInfo) throws RemoteException;
  
  void scheduleApplicationInfoChangedForSwitchUser(ApplicationInfo paramApplicationInfo, int paramInt) throws RemoteException;
  
  void scheduleBindService(IBinder paramIBinder, Intent paramIntent, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void scheduleCrash(String paramString) throws RemoteException;
  
  void scheduleCreateBackupAgent(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt1, int paramInt2) throws RemoteException;
  
  void scheduleCreateService(IBinder paramIBinder, ServiceInfo paramServiceInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt) throws RemoteException;
  
  void scheduleDestroyBackupAgent(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt) throws RemoteException;
  
  void scheduleEnterAnimationComplete(IBinder paramIBinder) throws RemoteException;
  
  void scheduleExit() throws RemoteException;
  
  void scheduleInstallProvider(ProviderInfo paramProviderInfo) throws RemoteException;
  
  void scheduleLocalVoiceInteractionStarted(IBinder paramIBinder, IVoiceInteractor paramIVoiceInteractor) throws RemoteException;
  
  void scheduleLowMemory() throws RemoteException;
  
  void scheduleOnNewActivityOptions(IBinder paramIBinder, Bundle paramBundle) throws RemoteException;
  
  void scheduleReceiver(Intent paramIntent, ActivityInfo paramActivityInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt1, String paramString, Bundle paramBundle, boolean paramBoolean, int paramInt2, int paramInt3) throws RemoteException;
  
  void scheduleRegisteredReceiver(IIntentReceiver paramIIntentReceiver, Intent paramIntent, int paramInt1, String paramString, Bundle paramBundle, boolean paramBoolean1, boolean paramBoolean2, int paramInt2, int paramInt3) throws RemoteException;
  
  void scheduleServiceArgs(IBinder paramIBinder, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void scheduleStopService(IBinder paramIBinder) throws RemoteException;
  
  void scheduleSuicide() throws RemoteException;
  
  void scheduleTransaction(ClientTransaction paramClientTransaction) throws RemoteException;
  
  void scheduleTranslucentConversionComplete(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void scheduleTrimMemory(int paramInt) throws RemoteException;
  
  void scheduleUnbindService(IBinder paramIBinder, Intent paramIntent) throws RemoteException;
  
  void setCoreSettings(Bundle paramBundle) throws RemoteException;
  
  void setDynamicalLogEnable(boolean paramBoolean) throws RemoteException;
  
  void setMirageWindowState(boolean paramBoolean) throws RemoteException;
  
  void setNetworkBlockSeq(long paramLong) throws RemoteException;
  
  void setProcessState(int paramInt) throws RemoteException;
  
  void setSchedulingGroup(int paramInt) throws RemoteException;
  
  void startBinderTracking() throws RemoteException;
  
  void stopBinderTrackingAndDump(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void unstableProviderDied(IBinder paramIBinder) throws RemoteException;
  
  void updateHttpProxy() throws RemoteException;
  
  void updatePackageCompatibilityInfo(String paramString, CompatibilityInfo paramCompatibilityInfo) throws RemoteException;
  
  void updateTimePrefs(int paramInt) throws RemoteException;
  
  void updateTimeZone() throws RemoteException;
  
  class Default implements IApplicationThread {
    public void scheduleReceiver(Intent param1Intent, ActivityInfo param1ActivityInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void oppoScheduleReceiver(Intent param1Intent, ActivityInfo param1ActivityInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void scheduleCreateService(IBinder param1IBinder, ServiceInfo param1ServiceInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int) throws RemoteException {}
    
    public void scheduleStopService(IBinder param1IBinder) throws RemoteException {}
    
    public void bindApplication(String param1String1, ApplicationInfo param1ApplicationInfo, ProviderInfoList param1ProviderInfoList, ComponentName param1ComponentName, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle1, IInstrumentationWatcher param1IInstrumentationWatcher, IUiAutomationConnection param1IUiAutomationConnection, int param1Int, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, Configuration param1Configuration, CompatibilityInfo param1CompatibilityInfo, Map param1Map, Bundle param1Bundle2, String param1String2, AutofillOptions param1AutofillOptions, ContentCaptureOptions param1ContentCaptureOptions, long[] param1ArrayOflong) throws RemoteException {}
    
    public void runIsolatedEntryPoint(String param1String, String[] param1ArrayOfString) throws RemoteException {}
    
    public void scheduleExit() throws RemoteException {}
    
    public void scheduleServiceArgs(IBinder param1IBinder, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void updateTimeZone() throws RemoteException {}
    
    public void processInBackground() throws RemoteException {}
    
    public void scheduleBindService(IBinder param1IBinder, Intent param1Intent, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void scheduleUnbindService(IBinder param1IBinder, Intent param1Intent) throws RemoteException {}
    
    public void dumpService(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String[] param1ArrayOfString) throws RemoteException {}
    
    public void scheduleRegisteredReceiver(IIntentReceiver param1IIntentReceiver, Intent param1Intent, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean1, boolean param1Boolean2, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void scheduleLowMemory() throws RemoteException {}
    
    public void profilerControl(boolean param1Boolean, ProfilerInfo param1ProfilerInfo, int param1Int) throws RemoteException {}
    
    public void setSchedulingGroup(int param1Int) throws RemoteException {}
    
    public void scheduleCreateBackupAgent(ApplicationInfo param1ApplicationInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void scheduleDestroyBackupAgent(ApplicationInfo param1ApplicationInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int) throws RemoteException {}
    
    public void scheduleOnNewActivityOptions(IBinder param1IBinder, Bundle param1Bundle) throws RemoteException {}
    
    public void scheduleSuicide() throws RemoteException {}
    
    public void dispatchPackageBroadcast(int param1Int, String[] param1ArrayOfString) throws RemoteException {}
    
    public void scheduleCrash(String param1String) throws RemoteException {}
    
    public void dumpHeap(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, String param1String, ParcelFileDescriptor param1ParcelFileDescriptor, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void dumpActivity(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String param1String, String[] param1ArrayOfString) throws RemoteException {}
    
    public void clearDnsCache() throws RemoteException {}
    
    public void updateHttpProxy() throws RemoteException {}
    
    public void setCoreSettings(Bundle param1Bundle) throws RemoteException {}
    
    public void updatePackageCompatibilityInfo(String param1String, CompatibilityInfo param1CompatibilityInfo) throws RemoteException {}
    
    public void scheduleTrimMemory(int param1Int) throws RemoteException {}
    
    public void dumpMemInfo(ParcelFileDescriptor param1ParcelFileDescriptor, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dumpMemInfoProto(ParcelFileDescriptor param1ParcelFileDescriptor, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dumpGfxInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dumpCacheInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dumpProvider(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dumpDbInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) throws RemoteException {}
    
    public void unstableProviderDied(IBinder param1IBinder) throws RemoteException {}
    
    public void requestAssistContextExtras(IBinder param1IBinder1, IBinder param1IBinder2, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void scheduleTranslucentConversionComplete(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void setProcessState(int param1Int) throws RemoteException {}
    
    public void scheduleInstallProvider(ProviderInfo param1ProviderInfo) throws RemoteException {}
    
    public void updateTimePrefs(int param1Int) throws RemoteException {}
    
    public void scheduleEnterAnimationComplete(IBinder param1IBinder) throws RemoteException {}
    
    public void notifyCleartextNetwork(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void startBinderTracking() throws RemoteException {}
    
    public void stopBinderTrackingAndDump(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {}
    
    public void scheduleLocalVoiceInteractionStarted(IBinder param1IBinder, IVoiceInteractor param1IVoiceInteractor) throws RemoteException {}
    
    public void handleTrustStorageUpdate() throws RemoteException {}
    
    public void attachAgent(String param1String) throws RemoteException {}
    
    public void attachStartupAgents(String param1String) throws RemoteException {}
    
    public void scheduleApplicationInfoChanged(ApplicationInfo param1ApplicationInfo) throws RemoteException {}
    
    public void setNetworkBlockSeq(long param1Long) throws RemoteException {}
    
    public void scheduleTransaction(ClientTransaction param1ClientTransaction) throws RemoteException {}
    
    public void requestDirectActions(IBinder param1IBinder, IVoiceInteractor param1IVoiceInteractor, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) throws RemoteException {}
    
    public void performDirectAction(IBinder param1IBinder, String param1String, Bundle param1Bundle, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) throws RemoteException {}
    
    public void setDynamicalLogEnable(boolean param1Boolean) throws RemoteException {}
    
    public void getBroadcastState(int param1Int) throws RemoteException {}
    
    public void setMirageWindowState(boolean param1Boolean) throws RemoteException {}
    
    public void scheduleApplicationInfoChangedForSwitchUser(ApplicationInfo param1ApplicationInfo, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IApplicationThread {
    private static final String DESCRIPTOR = "android.app.IApplicationThread";
    
    static final int TRANSACTION_attachAgent = 49;
    
    static final int TRANSACTION_attachStartupAgents = 50;
    
    static final int TRANSACTION_bindApplication = 5;
    
    static final int TRANSACTION_clearDnsCache = 26;
    
    static final int TRANSACTION_dispatchPackageBroadcast = 22;
    
    static final int TRANSACTION_dumpActivity = 25;
    
    static final int TRANSACTION_dumpCacheInfo = 34;
    
    static final int TRANSACTION_dumpDbInfo = 36;
    
    static final int TRANSACTION_dumpGfxInfo = 33;
    
    static final int TRANSACTION_dumpHeap = 24;
    
    static final int TRANSACTION_dumpMemInfo = 31;
    
    static final int TRANSACTION_dumpMemInfoProto = 32;
    
    static final int TRANSACTION_dumpProvider = 35;
    
    static final int TRANSACTION_dumpService = 13;
    
    static final int TRANSACTION_getBroadcastState = 57;
    
    static final int TRANSACTION_handleTrustStorageUpdate = 48;
    
    static final int TRANSACTION_notifyCleartextNetwork = 44;
    
    static final int TRANSACTION_oppoScheduleReceiver = 2;
    
    static final int TRANSACTION_performDirectAction = 55;
    
    static final int TRANSACTION_processInBackground = 10;
    
    static final int TRANSACTION_profilerControl = 16;
    
    static final int TRANSACTION_requestAssistContextExtras = 38;
    
    static final int TRANSACTION_requestDirectActions = 54;
    
    static final int TRANSACTION_runIsolatedEntryPoint = 6;
    
    static final int TRANSACTION_scheduleApplicationInfoChanged = 51;
    
    static final int TRANSACTION_scheduleApplicationInfoChangedForSwitchUser = 59;
    
    static final int TRANSACTION_scheduleBindService = 11;
    
    static final int TRANSACTION_scheduleCrash = 23;
    
    static final int TRANSACTION_scheduleCreateBackupAgent = 18;
    
    static final int TRANSACTION_scheduleCreateService = 3;
    
    static final int TRANSACTION_scheduleDestroyBackupAgent = 19;
    
    static final int TRANSACTION_scheduleEnterAnimationComplete = 43;
    
    static final int TRANSACTION_scheduleExit = 7;
    
    static final int TRANSACTION_scheduleInstallProvider = 41;
    
    static final int TRANSACTION_scheduleLocalVoiceInteractionStarted = 47;
    
    static final int TRANSACTION_scheduleLowMemory = 15;
    
    static final int TRANSACTION_scheduleOnNewActivityOptions = 20;
    
    static final int TRANSACTION_scheduleReceiver = 1;
    
    static final int TRANSACTION_scheduleRegisteredReceiver = 14;
    
    static final int TRANSACTION_scheduleServiceArgs = 8;
    
    static final int TRANSACTION_scheduleStopService = 4;
    
    static final int TRANSACTION_scheduleSuicide = 21;
    
    static final int TRANSACTION_scheduleTransaction = 53;
    
    static final int TRANSACTION_scheduleTranslucentConversionComplete = 39;
    
    static final int TRANSACTION_scheduleTrimMemory = 30;
    
    static final int TRANSACTION_scheduleUnbindService = 12;
    
    static final int TRANSACTION_setCoreSettings = 28;
    
    static final int TRANSACTION_setDynamicalLogEnable = 56;
    
    static final int TRANSACTION_setMirageWindowState = 58;
    
    static final int TRANSACTION_setNetworkBlockSeq = 52;
    
    static final int TRANSACTION_setProcessState = 40;
    
    static final int TRANSACTION_setSchedulingGroup = 17;
    
    static final int TRANSACTION_startBinderTracking = 45;
    
    static final int TRANSACTION_stopBinderTrackingAndDump = 46;
    
    static final int TRANSACTION_unstableProviderDied = 37;
    
    static final int TRANSACTION_updateHttpProxy = 27;
    
    static final int TRANSACTION_updatePackageCompatibilityInfo = 29;
    
    static final int TRANSACTION_updateTimePrefs = 42;
    
    static final int TRANSACTION_updateTimeZone = 9;
    
    public Stub() {
      attachInterface(this, "android.app.IApplicationThread");
    }
    
    public static IApplicationThread asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IApplicationThread");
      if (iInterface != null && iInterface instanceof IApplicationThread)
        return (IApplicationThread)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 59:
          return "scheduleApplicationInfoChangedForSwitchUser";
        case 58:
          return "setMirageWindowState";
        case 57:
          return "getBroadcastState";
        case 56:
          return "setDynamicalLogEnable";
        case 55:
          return "performDirectAction";
        case 54:
          return "requestDirectActions";
        case 53:
          return "scheduleTransaction";
        case 52:
          return "setNetworkBlockSeq";
        case 51:
          return "scheduleApplicationInfoChanged";
        case 50:
          return "attachStartupAgents";
        case 49:
          return "attachAgent";
        case 48:
          return "handleTrustStorageUpdate";
        case 47:
          return "scheduleLocalVoiceInteractionStarted";
        case 46:
          return "stopBinderTrackingAndDump";
        case 45:
          return "startBinderTracking";
        case 44:
          return "notifyCleartextNetwork";
        case 43:
          return "scheduleEnterAnimationComplete";
        case 42:
          return "updateTimePrefs";
        case 41:
          return "scheduleInstallProvider";
        case 40:
          return "setProcessState";
        case 39:
          return "scheduleTranslucentConversionComplete";
        case 38:
          return "requestAssistContextExtras";
        case 37:
          return "unstableProviderDied";
        case 36:
          return "dumpDbInfo";
        case 35:
          return "dumpProvider";
        case 34:
          return "dumpCacheInfo";
        case 33:
          return "dumpGfxInfo";
        case 32:
          return "dumpMemInfoProto";
        case 31:
          return "dumpMemInfo";
        case 30:
          return "scheduleTrimMemory";
        case 29:
          return "updatePackageCompatibilityInfo";
        case 28:
          return "setCoreSettings";
        case 27:
          return "updateHttpProxy";
        case 26:
          return "clearDnsCache";
        case 25:
          return "dumpActivity";
        case 24:
          return "dumpHeap";
        case 23:
          return "scheduleCrash";
        case 22:
          return "dispatchPackageBroadcast";
        case 21:
          return "scheduleSuicide";
        case 20:
          return "scheduleOnNewActivityOptions";
        case 19:
          return "scheduleDestroyBackupAgent";
        case 18:
          return "scheduleCreateBackupAgent";
        case 17:
          return "setSchedulingGroup";
        case 16:
          return "profilerControl";
        case 15:
          return "scheduleLowMemory";
        case 14:
          return "scheduleRegisteredReceiver";
        case 13:
          return "dumpService";
        case 12:
          return "scheduleUnbindService";
        case 11:
          return "scheduleBindService";
        case 10:
          return "processInBackground";
        case 9:
          return "updateTimeZone";
        case 8:
          return "scheduleServiceArgs";
        case 7:
          return "scheduleExit";
        case 6:
          return "runIsolatedEntryPoint";
        case 5:
          return "bindApplication";
        case 4:
          return "scheduleStopService";
        case 3:
          return "scheduleCreateService";
        case 2:
          return "oppoScheduleReceiver";
        case 1:
          break;
      } 
      return "scheduleReceiver";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str2;
        IVoiceInteractor iVoiceInteractor1;
        byte[] arrayOfByte;
        IBinder iBinder2;
        String arrayOfString2[], str1, arrayOfString1[];
        long[] arrayOfLong;
        IBinder iBinder1, iBinder4;
        String str3;
        IBinder iBinder3, iBinder5;
        String str4, str6;
        IVoiceInteractor iVoiceInteractor2;
        String str5;
        IIntentReceiver iIntentReceiver;
        IBinder iBinder6;
        RemoteCallback remoteCallback;
        IBinder iBinder8;
        String str7;
        IBinder iBinder7;
        long l;
        String str8;
        Bundle bundle;
        IInstrumentationWatcher iInstrumentationWatcher;
        IUiAutomationConnection iUiAutomationConnection;
        Configuration configuration;
        CompatibilityInfo compatibilityInfo;
        ClassLoader classLoader;
        HashMap hashMap;
        String str10;
        AutofillOptions autofillOptions;
        ContentCaptureOptions contentCaptureOptions;
        int j;
        boolean bool1 = false, bool2 = false, bool3 = false, bool4 = false, bool5 = false, bool6 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 59:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            if (param1Parcel1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            scheduleApplicationInfoChangedForSwitchUser((ApplicationInfo)param1Parcel2, param1Int1);
            return true;
          case 58:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            setMirageWindowState(bool6);
            return true;
          case 57:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            param1Int1 = param1Parcel1.readInt();
            getBroadcastState(param1Int1);
            return true;
          case 56:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            bool6 = bool1;
            if (param1Parcel1.readInt() != 0)
              bool6 = true; 
            setDynamicalLogEnable(bool6);
            return true;
          case 55:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            iBinder5 = param1Parcel1.readStrongBinder();
            str6 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              remoteCallback = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            performDirectAction(iBinder5, str6, (Bundle)param1Parcel2, remoteCallback, (RemoteCallback)param1Parcel1);
            return true;
          case 54:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            iBinder8 = param1Parcel1.readStrongBinder();
            iVoiceInteractor2 = IVoiceInteractor.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            requestDirectActions(iBinder8, iVoiceInteractor2, (RemoteCallback)param1Parcel2, (RemoteCallback)param1Parcel1);
            return true;
          case 53:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            if (param1Parcel1.readInt() != 0) {
              ClientTransaction clientTransaction = (ClientTransaction)ClientTransaction.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            scheduleTransaction((ClientTransaction)param1Parcel1);
            return true;
          case 52:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            l = param1Parcel1.readLong();
            setNetworkBlockSeq(l);
            return true;
          case 51:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            if (param1Parcel1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            scheduleApplicationInfoChanged((ApplicationInfo)param1Parcel1);
            return true;
          case 50:
            param1Parcel1.enforceInterface("android.app.IApplicationThread");
            str2 = param1Parcel1.readString();
            attachStartupAgents(str2);
            return true;
          case 49:
            str2.enforceInterface("android.app.IApplicationThread");
            str2 = str2.readString();
            attachAgent(str2);
            return true;
          case 48:
            str2.enforceInterface("android.app.IApplicationThread");
            handleTrustStorageUpdate();
            return true;
          case 47:
            str2.enforceInterface("android.app.IApplicationThread");
            iBinder4 = str2.readStrongBinder();
            iVoiceInteractor1 = IVoiceInteractor.Stub.asInterface(str2.readStrongBinder());
            scheduleLocalVoiceInteractionStarted(iBinder4, iVoiceInteractor1);
            return true;
          case 46:
            iVoiceInteractor1.enforceInterface("android.app.IApplicationThread");
            if (iVoiceInteractor1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iVoiceInteractor1);
            } else {
              iVoiceInteractor1 = null;
            } 
            stopBinderTrackingAndDump((ParcelFileDescriptor)iVoiceInteractor1);
            return true;
          case 45:
            iVoiceInteractor1.enforceInterface("android.app.IApplicationThread");
            startBinderTracking();
            return true;
          case 44:
            iVoiceInteractor1.enforceInterface("android.app.IApplicationThread");
            arrayOfByte = iVoiceInteractor1.createByteArray();
            notifyCleartextNetwork(arrayOfByte);
            return true;
          case 43:
            arrayOfByte.enforceInterface("android.app.IApplicationThread");
            iBinder2 = arrayOfByte.readStrongBinder();
            scheduleEnterAnimationComplete(iBinder2);
            return true;
          case 42:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            param1Int1 = iBinder2.readInt();
            updateTimePrefs(param1Int1);
            return true;
          case 41:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            if (iBinder2.readInt() != 0) {
              ProviderInfo providerInfo = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder2 = null;
            } 
            scheduleInstallProvider((ProviderInfo)iBinder2);
            return true;
          case 40:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            param1Int1 = iBinder2.readInt();
            setProcessState(param1Int1);
            return true;
          case 39:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            iBinder4 = iBinder2.readStrongBinder();
            bool6 = bool2;
            if (iBinder2.readInt() != 0)
              bool6 = true; 
            scheduleTranslucentConversionComplete(iBinder4, bool6);
            return true;
          case 38:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            iBinder8 = iBinder2.readStrongBinder();
            iBinder4 = iBinder2.readStrongBinder();
            param1Int2 = iBinder2.readInt();
            i = iBinder2.readInt();
            param1Int1 = iBinder2.readInt();
            requestAssistContextExtras(iBinder8, iBinder4, param1Int2, i, param1Int1);
            return true;
          case 37:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            iBinder2 = iBinder2.readStrongBinder();
            unstableProviderDied(iBinder2);
            return true;
          case 36:
            iBinder2.enforceInterface("android.app.IApplicationThread");
            if (iBinder2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              iBinder4 = null;
            } 
            arrayOfString2 = iBinder2.createStringArray();
            dumpDbInfo((ParcelFileDescriptor)iBinder4, arrayOfString2);
            return true;
          case 35:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder4 = null;
            } 
            iBinder8 = arrayOfString2.readStrongBinder();
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpProvider((ParcelFileDescriptor)iBinder4, iBinder8, arrayOfString2);
            return true;
          case 34:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder4 = null;
            } 
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpCacheInfo((ParcelFileDescriptor)iBinder4, arrayOfString2);
            return true;
          case 33:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder4 = null;
            } 
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpGfxInfo((ParcelFileDescriptor)iBinder4, arrayOfString2);
            return true;
          case 32:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder4 = null;
            } 
            if (arrayOfString2.readInt() != 0) {
              Debug.MemoryInfo memoryInfo = (Debug.MemoryInfo)Debug.MemoryInfo.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder8 = null;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool5 = true;
            } else {
              bool5 = false;
            } 
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpMemInfoProto((ParcelFileDescriptor)iBinder4, (Debug.MemoryInfo)iBinder8, bool6, bool2, bool4, bool5, arrayOfString2);
            return true;
          case 31:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder4 = null;
            } 
            if (arrayOfString2.readInt() != 0) {
              Debug.MemoryInfo memoryInfo = (Debug.MemoryInfo)Debug.MemoryInfo.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              iBinder8 = null;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool5 = true;
            } else {
              bool5 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpMemInfo((ParcelFileDescriptor)iBinder4, (Debug.MemoryInfo)iBinder8, bool6, bool2, bool4, bool5, bool1, arrayOfString2);
            return true;
          case 30:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            param1Int1 = arrayOfString2.readInt();
            scheduleTrimMemory(param1Int1);
            return true;
          case 29:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            str3 = arrayOfString2.readString();
            if (arrayOfString2.readInt() != 0) {
              CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              arrayOfString2 = null;
            } 
            updatePackageCompatibilityInfo(str3, (CompatibilityInfo)arrayOfString2);
            return true;
          case 28:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              arrayOfString2 = null;
            } 
            setCoreSettings((Bundle)arrayOfString2);
            return true;
          case 27:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            updateHttpProxy();
            return true;
          case 26:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            clearDnsCache();
            return true;
          case 25:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              str3 = null;
            } 
            iBinder8 = arrayOfString2.readStrongBinder();
            str5 = arrayOfString2.readString();
            arrayOfString2 = arrayOfString2.createStringArray();
            dumpActivity((ParcelFileDescriptor)str3, iBinder8, str5, arrayOfString2);
            return true;
          case 24:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString2.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (arrayOfString2.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            str7 = arrayOfString2.readString();
            if (arrayOfString2.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              str3 = null;
            } 
            if (arrayOfString2.readInt() != 0) {
              RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)arrayOfString2);
            } else {
              arrayOfString2 = null;
            } 
            dumpHeap(bool6, bool2, bool4, str7, (ParcelFileDescriptor)str3, (RemoteCallback)arrayOfString2);
            return true;
          case 23:
            arrayOfString2.enforceInterface("android.app.IApplicationThread");
            str1 = arrayOfString2.readString();
            scheduleCrash(str1);
            return true;
          case 22:
            str1.enforceInterface("android.app.IApplicationThread");
            param1Int1 = str1.readInt();
            arrayOfString1 = str1.createStringArray();
            dispatchPackageBroadcast(param1Int1, arrayOfString1);
            return true;
          case 21:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            scheduleSuicide();
            return true;
          case 20:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            iBinder3 = arrayOfString1.readStrongBinder();
            if (arrayOfString1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            scheduleOnNewActivityOptions(iBinder3, (Bundle)arrayOfString1);
            return true;
          case 19:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str7 = null;
            } 
            param1Int1 = arrayOfString1.readInt();
            scheduleDestroyBackupAgent((ApplicationInfo)iBinder3, (CompatibilityInfo)str7, param1Int1);
            return true;
          case 18:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str7 = null;
            } 
            param1Int1 = arrayOfString1.readInt();
            param1Int2 = arrayOfString1.readInt();
            scheduleCreateBackupAgent((ApplicationInfo)iBinder3, (CompatibilityInfo)str7, param1Int1, param1Int2);
            return true;
          case 17:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            param1Int1 = arrayOfString1.readInt();
            setSchedulingGroup(param1Int1);
            return true;
          case 16:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            bool6 = bool3;
            if (arrayOfString1.readInt() != 0)
              bool6 = true; 
            if (arrayOfString1.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            param1Int1 = arrayOfString1.readInt();
            profilerControl(bool6, (ProfilerInfo)iBinder3, param1Int1);
            return true;
          case 15:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            scheduleLowMemory();
            return true;
          case 14:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            iIntentReceiver = IIntentReceiver.Stub.asInterface(arrayOfString1.readStrongBinder());
            if (arrayOfString1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            param1Int2 = arrayOfString1.readInt();
            str4 = arrayOfString1.readString();
            if (arrayOfString1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str7 = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            if (arrayOfString1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            param1Int1 = arrayOfString1.readInt();
            i = arrayOfString1.readInt();
            scheduleRegisteredReceiver(iIntentReceiver, (Intent)iBinder3, param1Int2, str4, (Bundle)str7, bool6, bool2, param1Int1, i);
            return true;
          case 13:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            if (arrayOfString1.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            iBinder7 = arrayOfString1.readStrongBinder();
            arrayOfString1 = arrayOfString1.createStringArray();
            dumpService((ParcelFileDescriptor)iBinder3, iBinder7, arrayOfString1);
            return true;
          case 12:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            iBinder3 = arrayOfString1.readStrongBinder();
            if (arrayOfString1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            scheduleUnbindService(iBinder3, (Intent)arrayOfString1);
            return true;
          case 11:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            iBinder7 = arrayOfString1.readStrongBinder();
            if (arrayOfString1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder3 = null;
            } 
            bool6 = bool4;
            if (arrayOfString1.readInt() != 0)
              bool6 = true; 
            param1Int1 = arrayOfString1.readInt();
            scheduleBindService(iBinder7, (Intent)iBinder3, bool6, param1Int1);
            return true;
          case 10:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            processInBackground();
            return true;
          case 9:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            updateTimeZone();
            return true;
          case 8:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            iBinder3 = arrayOfString1.readStrongBinder();
            if (arrayOfString1.readInt() != 0) {
              ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            scheduleServiceArgs(iBinder3, (ParceledListSlice)arrayOfString1);
            return true;
          case 7:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            scheduleExit();
            return true;
          case 6:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            str = arrayOfString1.readString();
            arrayOfString1 = arrayOfString1.createStringArray();
            runIsolatedEntryPoint(str, arrayOfString1);
            return true;
          case 5:
            arrayOfString1.enforceInterface("android.app.IApplicationThread");
            str8 = arrayOfString1.readString();
            if (arrayOfString1.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              ProviderInfoList providerInfoList = (ProviderInfoList)ProviderInfoList.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iBinder7 = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              iIntentReceiver = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              str4 = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              bundle = null;
            } 
            iInstrumentationWatcher = IInstrumentationWatcher.Stub.asInterface(arrayOfString1.readStrongBinder());
            iUiAutomationConnection = IUiAutomationConnection.Stub.asInterface(arrayOfString1.readStrongBinder());
            param1Int1 = arrayOfString1.readInt();
            if (arrayOfString1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            if (arrayOfString1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (arrayOfString1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            if (arrayOfString1.readInt() != 0)
              bool5 = true; 
            if (arrayOfString1.readInt() != 0) {
              configuration = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              configuration = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              compatibilityInfo = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              compatibilityInfo = null;
            } 
            classLoader = getClass().getClassLoader();
            hashMap = arrayOfString1.readHashMap(classLoader);
            if (arrayOfString1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              classLoader = null;
            } 
            str10 = arrayOfString1.readString();
            if (arrayOfString1.readInt() != 0) {
              autofillOptions = (AutofillOptions)AutofillOptions.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              autofillOptions = null;
            } 
            if (arrayOfString1.readInt() != 0) {
              contentCaptureOptions = (ContentCaptureOptions)ContentCaptureOptions.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              contentCaptureOptions = null;
            } 
            arrayOfLong = arrayOfString1.createLongArray();
            bindApplication(str8, (ApplicationInfo)str, (ProviderInfoList)iBinder7, (ComponentName)iIntentReceiver, (ProfilerInfo)str4, bundle, iInstrumentationWatcher, iUiAutomationConnection, param1Int1, bool6, bool2, bool4, bool5, configuration, compatibilityInfo, hashMap, (Bundle)classLoader, str10, autofillOptions, contentCaptureOptions, arrayOfLong);
            return true;
          case 4:
            arrayOfLong.enforceInterface("android.app.IApplicationThread");
            iBinder1 = arrayOfLong.readStrongBinder();
            scheduleStopService(iBinder1);
            return true;
          case 3:
            iBinder1.enforceInterface("android.app.IApplicationThread");
            iBinder6 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              ServiceInfo serviceInfo = (ServiceInfo)ServiceInfo.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              str = null;
            } 
            if (iBinder1.readInt() != 0) {
              CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder7 = null;
            } 
            param1Int1 = iBinder1.readInt();
            scheduleCreateService(iBinder6, (ServiceInfo)str, (CompatibilityInfo)iBinder7, param1Int1);
            return true;
          case 2:
            iBinder1.enforceInterface("android.app.IApplicationThread");
            if (iBinder1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              str = null;
            } 
            if (iBinder1.readInt() != 0) {
              ActivityInfo activityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder7 = null;
            } 
            if (iBinder1.readInt() != 0) {
              CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder6 = null;
            } 
            param1Int2 = iBinder1.readInt();
            str9 = iBinder1.readString();
            if (iBinder1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              str4 = null;
            } 
            if (iBinder1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            j = iBinder1.readInt();
            i = iBinder1.readInt();
            param1Int1 = iBinder1.readInt();
            oppoScheduleReceiver((Intent)str, (ActivityInfo)iBinder7, (CompatibilityInfo)iBinder6, param1Int2, str9, (Bundle)str4, bool6, j, i, param1Int1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.app.IApplicationThread");
        if (iBinder1.readInt() != 0) {
          Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          str = null;
        } 
        if (iBinder1.readInt() != 0) {
          ActivityInfo activityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          iBinder7 = null;
        } 
        if (iBinder1.readInt() != 0) {
          CompatibilityInfo compatibilityInfo1 = (CompatibilityInfo)CompatibilityInfo.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          iBinder6 = null;
        } 
        int i = iBinder1.readInt();
        String str9 = iBinder1.readString();
        if (iBinder1.readInt() != 0) {
          Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          str4 = null;
        } 
        if (iBinder1.readInt() != 0) {
          bool6 = true;
        } else {
          bool6 = false;
        } 
        param1Int2 = iBinder1.readInt();
        param1Int1 = iBinder1.readInt();
        scheduleReceiver((Intent)str, (ActivityInfo)iBinder7, (CompatibilityInfo)iBinder6, i, str9, (Bundle)str4, bool6, param1Int2, param1Int1);
        return true;
      } 
      str.writeString("android.app.IApplicationThread");
      return true;
    }
    
    private static class Proxy implements IApplicationThread {
      public static IApplicationThread sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IApplicationThread";
      }
      
      public void scheduleReceiver(Intent param2Intent, ActivityInfo param2ActivityInfo, CompatibilityInfo param2CompatibilityInfo, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = false;
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ActivityInfo != null) {
            parcel.writeInt(1);
            param2ActivityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleReceiver(param2Intent, param2ActivityInfo, param2CompatibilityInfo, param2Int1, param2String, param2Bundle, param2Boolean, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void oppoScheduleReceiver(Intent param2Intent, ActivityInfo param2ActivityInfo, CompatibilityInfo param2CompatibilityInfo, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = false;
          if (param2Intent != null) {
            try {
              parcel.writeInt(1);
              param2Intent.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          if (param2ActivityInfo != null) {
            parcel.writeInt(1);
            param2ActivityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool1 = this.mRemote.transact(2, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread iApplicationThread = IApplicationThread.Stub.getDefaultImpl();
            try {
              iApplicationThread.oppoScheduleReceiver(param2Intent, param2ActivityInfo, param2CompatibilityInfo, param2Int1, param2String, param2Bundle, param2Boolean, param2Int2, param2Int3, param2Int4);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2Intent;
      }
      
      public void scheduleCreateService(IBinder param2IBinder, ServiceInfo param2ServiceInfo, CompatibilityInfo param2CompatibilityInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2ServiceInfo != null) {
            parcel.writeInt(1);
            param2ServiceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleCreateService(param2IBinder, param2ServiceInfo, param2CompatibilityInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleStopService(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleStopService(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void bindApplication(String param2String1, ApplicationInfo param2ApplicationInfo, ProviderInfoList param2ProviderInfoList, ComponentName param2ComponentName, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle1, IInstrumentationWatcher param2IInstrumentationWatcher, IUiAutomationConnection param2IUiAutomationConnection, int param2Int, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, boolean param2Boolean4, Configuration param2Configuration, CompatibilityInfo param2CompatibilityInfo, Map param2Map, Bundle param2Bundle2, String param2String2, AutofillOptions param2AutofillOptions, ContentCaptureOptions param2ContentCaptureOptions, long[] param2ArrayOflong) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String1);
          if (param2ApplicationInfo != null) {
            try {
              parcel.writeInt(1);
              param2ApplicationInfo.writeToParcel(parcel, 0);
            } finally {}
          } else {
            parcel.writeInt(0);
          } 
          if (param2ProviderInfoList != null) {
            parcel.writeInt(1);
            param2ProviderInfoList.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ComponentName != null) {
            parcel.writeInt(1);
            param2ComponentName.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ProfilerInfo != null) {
            parcel.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle1 != null) {
            parcel.writeInt(1);
            param2Bundle1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IInstrumentationWatcher != null) {
            iBinder = param2IInstrumentationWatcher.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2IUiAutomationConnection != null) {
            iBinder = param2IUiAutomationConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeInt(param2Int);
          if (param2Boolean1) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean2) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean3) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean4) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Configuration != null) {
            parcel.writeInt(1);
            param2Configuration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeMap(param2Map);
          if (param2Bundle2 != null) {
            parcel.writeInt(1);
            param2Bundle2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String2);
          if (param2AutofillOptions != null) {
            parcel.writeInt(1);
            param2AutofillOptions.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ContentCaptureOptions != null) {
            parcel.writeInt(1);
            param2ContentCaptureOptions.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeLongArray(param2ArrayOflong);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread iApplicationThread = IApplicationThread.Stub.getDefaultImpl();
            try {
              iApplicationThread.bindApplication(param2String1, param2ApplicationInfo, param2ProviderInfoList, param2ComponentName, param2ProfilerInfo, param2Bundle1, param2IInstrumentationWatcher, param2IUiAutomationConnection, param2Int, param2Boolean1, param2Boolean2, param2Boolean3, param2Boolean4, param2Configuration, param2CompatibilityInfo, param2Map, param2Bundle2, param2String2, param2AutofillOptions, param2ContentCaptureOptions, param2ArrayOflong);
              parcel.recycle();
              return;
            } finally {}
          } else {
            parcel.recycle();
            return;
          } 
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void runIsolatedEntryPoint(String param2String, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().runIsolatedEntryPoint(param2String, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleExit() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleExit();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleServiceArgs(IBinder param2IBinder, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleServiceArgs(param2IBinder, param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateTimeZone() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(9, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().updateTimeZone();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void processInBackground() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(10, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().processInBackground();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleBindService(IBinder param2IBinder, Intent param2Intent, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = false;
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(11, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleBindService(param2IBinder, param2Intent, param2Boolean, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleUnbindService(IBinder param2IBinder, Intent param2Intent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleUnbindService(param2IBinder, param2Intent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpService(ParcelFileDescriptor param2ParcelFileDescriptor, IBinder param2IBinder, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(13, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpService(param2ParcelFileDescriptor, param2IBinder, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleRegisteredReceiver(IIntentReceiver param2IIntentReceiver, Intent param2Intent, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean1, boolean param2Boolean2, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2IIntentReceiver != null) {
            iBinder = param2IIntentReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = false;
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          try {
            parcel.writeInt(param2Int1);
            try {
              parcel.writeString(param2String);
              if (param2Bundle != null) {
                parcel.writeInt(1);
                param2Bundle.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              if (param2Boolean1) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
              parcel.writeInt(bool1);
              boolean bool1 = bool;
              if (param2Boolean2)
                bool1 = true; 
              parcel.writeInt(bool1);
              parcel.writeInt(param2Int2);
              parcel.writeInt(param2Int3);
              boolean bool2 = this.mRemote.transact(14, parcel, null, 1);
              if (!bool2 && IApplicationThread.Stub.getDefaultImpl() != null) {
                IApplicationThread.Stub.getDefaultImpl().scheduleRegisteredReceiver(param2IIntentReceiver, param2Intent, param2Int1, param2String, param2Bundle, param2Boolean1, param2Boolean2, param2Int2, param2Int3);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IIntentReceiver;
      }
      
      public void scheduleLowMemory() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(15, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleLowMemory();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void profilerControl(boolean param2Boolean, ProfilerInfo param2ProfilerInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2ProfilerInfo != null) {
            parcel.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(16, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().profilerControl(param2Boolean, param2ProfilerInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSchedulingGroup(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setSchedulingGroup(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleCreateBackupAgent(ApplicationInfo param2ApplicationInfo, CompatibilityInfo param2CompatibilityInfo, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ApplicationInfo != null) {
            parcel.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleCreateBackupAgent(param2ApplicationInfo, param2CompatibilityInfo, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleDestroyBackupAgent(ApplicationInfo param2ApplicationInfo, CompatibilityInfo param2CompatibilityInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ApplicationInfo != null) {
            parcel.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleDestroyBackupAgent(param2ApplicationInfo, param2CompatibilityInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleOnNewActivityOptions(IBinder param2IBinder, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleOnNewActivityOptions(param2IBinder, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleSuicide() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(21, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleSuicide();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchPackageBroadcast(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(22, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dispatchPackageBroadcast(param2Int, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleCrash(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleCrash(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpHeap(boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, String param2String, ParcelFileDescriptor param2ParcelFileDescriptor, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2Boolean1) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean2) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2Boolean3) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          try {
            parcel.writeString(param2String);
            if (param2ParcelFileDescriptor != null) {
              parcel.writeInt(1);
              param2ParcelFileDescriptor.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2RemoteCallback != null) {
              parcel.writeInt(1);
              param2RemoteCallback.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              boolean bool1 = this.mRemote.transact(24, parcel, null, 1);
              if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
                IApplicationThread.Stub.getDefaultImpl().dumpHeap(param2Boolean1, param2Boolean2, param2Boolean3, param2String, param2ParcelFileDescriptor, param2RemoteCallback);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String;
      }
      
      public void dumpActivity(ParcelFileDescriptor param2ParcelFileDescriptor, IBinder param2IBinder, String param2String, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(25, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpActivity(param2ParcelFileDescriptor, param2IBinder, param2String, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clearDnsCache() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(26, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().clearDnsCache();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateHttpProxy() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(27, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().updateHttpProxy();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCoreSettings(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setCoreSettings(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updatePackageCompatibilityInfo(String param2String, CompatibilityInfo param2CompatibilityInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String);
          if (param2CompatibilityInfo != null) {
            parcel.writeInt(1);
            param2CompatibilityInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().updatePackageCompatibilityInfo(param2String, param2CompatibilityInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleTrimMemory(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleTrimMemory(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpMemInfo(ParcelFileDescriptor param2ParcelFileDescriptor, Debug.MemoryInfo param2MemoryInfo, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, boolean param2Boolean4, boolean param2Boolean5, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool1 = false;
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2MemoryInfo != null) {
            parcel.writeInt(1);
            param2MemoryInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          if (param2Boolean3) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          if (param2Boolean4) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean5)
            bool2 = true; 
          parcel.writeInt(bool2);
          try {
            parcel.writeStringArray(param2ArrayOfString);
            try {
              boolean bool = this.mRemote.transact(31, parcel, null, 1);
              if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
                IApplicationThread.Stub.getDefaultImpl().dumpMemInfo(param2ParcelFileDescriptor, param2MemoryInfo, param2Boolean1, param2Boolean2, param2Boolean3, param2Boolean4, param2Boolean5, param2ArrayOfString);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2ParcelFileDescriptor;
      }
      
      public void dumpMemInfoProto(ParcelFileDescriptor param2ParcelFileDescriptor, Debug.MemoryInfo param2MemoryInfo, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, boolean param2Boolean4, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool1 = false;
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2MemoryInfo != null) {
            parcel.writeInt(1);
            param2MemoryInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          if (param2Boolean3) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel.writeInt(bool2);
          boolean bool2 = bool1;
          if (param2Boolean4)
            bool2 = true; 
          parcel.writeInt(bool2);
          try {
            parcel.writeStringArray(param2ArrayOfString);
            try {
              boolean bool = this.mRemote.transact(32, parcel, null, 1);
              if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
                IApplicationThread.Stub.getDefaultImpl().dumpMemInfoProto(param2ParcelFileDescriptor, param2MemoryInfo, param2Boolean1, param2Boolean2, param2Boolean3, param2Boolean4, param2ArrayOfString);
                parcel.recycle();
                return;
              } 
              parcel.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2ParcelFileDescriptor;
      }
      
      public void dumpGfxInfo(ParcelFileDescriptor param2ParcelFileDescriptor, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(33, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpGfxInfo(param2ParcelFileDescriptor, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpCacheInfo(ParcelFileDescriptor param2ParcelFileDescriptor, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(34, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpCacheInfo(param2ParcelFileDescriptor, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpProvider(ParcelFileDescriptor param2ParcelFileDescriptor, IBinder param2IBinder, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(35, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpProvider(param2ParcelFileDescriptor, param2IBinder, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dumpDbInfo(ParcelFileDescriptor param2ParcelFileDescriptor, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(36, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().dumpDbInfo(param2ParcelFileDescriptor, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unstableProviderDied(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(37, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().unstableProviderDied(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestAssistContextExtras(IBinder param2IBinder1, IBinder param2IBinder2, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder1);
          parcel.writeStrongBinder(param2IBinder2);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(38, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().requestAssistContextExtras(param2IBinder1, param2IBinder2, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleTranslucentConversionComplete(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(39, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleTranslucentConversionComplete(param2IBinder, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setProcessState(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(40, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setProcessState(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleInstallProvider(ProviderInfo param2ProviderInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ProviderInfo != null) {
            parcel.writeInt(1);
            param2ProviderInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(41, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleInstallProvider(param2ProviderInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateTimePrefs(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(42, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().updateTimePrefs(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleEnterAnimationComplete(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(43, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleEnterAnimationComplete(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyCleartextNetwork(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(44, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().notifyCleartextNetwork(param2ArrayOfbyte);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startBinderTracking() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(45, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().startBinderTracking();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopBinderTrackingAndDump(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ParcelFileDescriptor != null) {
            parcel.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(46, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().stopBinderTrackingAndDump(param2ParcelFileDescriptor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleLocalVoiceInteractionStarted(IBinder param2IBinder, IVoiceInteractor param2IVoiceInteractor) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2IVoiceInteractor != null) {
            iBinder = param2IVoiceInteractor.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(47, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleLocalVoiceInteractionStarted(param2IBinder, param2IVoiceInteractor);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleTrustStorageUpdate() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          boolean bool = this.mRemote.transact(48, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().handleTrustStorageUpdate();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void attachAgent(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(49, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().attachAgent(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void attachStartupAgents(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(50, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().attachStartupAgents(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleApplicationInfoChanged(ApplicationInfo param2ApplicationInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ApplicationInfo != null) {
            parcel.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(51, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleApplicationInfoChanged(param2ApplicationInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setNetworkBlockSeq(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(52, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setNetworkBlockSeq(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleTransaction(ClientTransaction param2ClientTransaction) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ClientTransaction != null) {
            parcel.writeInt(1);
            param2ClientTransaction.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(53, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleTransaction(param2ClientTransaction);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void requestDirectActions(IBinder param2IBinder, IVoiceInteractor param2IVoiceInteractor, RemoteCallback param2RemoteCallback1, RemoteCallback param2RemoteCallback2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          if (param2IVoiceInteractor != null) {
            iBinder = param2IVoiceInteractor.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          if (param2RemoteCallback1 != null) {
            parcel.writeInt(1);
            param2RemoteCallback1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RemoteCallback2 != null) {
            parcel.writeInt(1);
            param2RemoteCallback2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(54, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().requestDirectActions(param2IBinder, param2IVoiceInteractor, param2RemoteCallback1, param2RemoteCallback2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void performDirectAction(IBinder param2IBinder, String param2String, Bundle param2Bundle, RemoteCallback param2RemoteCallback1, RemoteCallback param2RemoteCallback2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RemoteCallback1 != null) {
            parcel.writeInt(1);
            param2RemoteCallback1.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2RemoteCallback2 != null) {
            parcel.writeInt(1);
            param2RemoteCallback2.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(55, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().performDirectAction(param2IBinder, param2String, param2Bundle, param2RemoteCallback1, param2RemoteCallback2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDynamicalLogEnable(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(56, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setDynamicalLogEnable(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getBroadcastState(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(57, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().getBroadcastState(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setMirageWindowState(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(58, parcel, null, 1);
          if (!bool1 && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().setMirageWindowState(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void scheduleApplicationInfoChangedForSwitchUser(ApplicationInfo param2ApplicationInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IApplicationThread");
          if (param2ApplicationInfo != null) {
            parcel.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(59, parcel, null, 1);
          if (!bool && IApplicationThread.Stub.getDefaultImpl() != null) {
            IApplicationThread.Stub.getDefaultImpl().scheduleApplicationInfoChangedForSwitchUser(param2ApplicationInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IApplicationThread param1IApplicationThread) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IApplicationThread != null) {
          Proxy.sDefaultImpl = param1IApplicationThread;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IApplicationThread getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
