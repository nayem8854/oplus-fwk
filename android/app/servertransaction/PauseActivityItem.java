package android.app.servertransaction;

import android.app.ActivityTaskManager;
import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.Trace;

public class PauseActivityItem extends ActivityLifecycleItem {
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    Trace.traceBegin(64L, "activityPause");
    paramClientTransactionHandler.handlePauseActivity(paramIBinder, this.mFinished, this.mUserLeaving, this.mConfigChanges, paramPendingTransactionActions, "PAUSE_ACTIVITY_ITEM");
    Trace.traceEnd(64L);
  }
  
  public int getTargetState() {
    return 4;
  }
  
  public void postExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    if (this.mDontReport)
      return; 
    try {
      ActivityTaskManager.getService().activityPaused(paramIBinder);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private PauseActivityItem() {}
  
  public static PauseActivityItem obtain(boolean paramBoolean1, boolean paramBoolean2, int paramInt, boolean paramBoolean3) {
    PauseActivityItem pauseActivityItem1 = ObjectPool.<PauseActivityItem>obtain(PauseActivityItem.class);
    PauseActivityItem pauseActivityItem2 = pauseActivityItem1;
    if (pauseActivityItem1 == null)
      pauseActivityItem2 = new PauseActivityItem(); 
    pauseActivityItem2.mFinished = paramBoolean1;
    pauseActivityItem2.mUserLeaving = paramBoolean2;
    pauseActivityItem2.mConfigChanges = paramInt;
    pauseActivityItem2.mDontReport = paramBoolean3;
    return pauseActivityItem2;
  }
  
  public static PauseActivityItem obtain() {
    PauseActivityItem pauseActivityItem1 = ObjectPool.<PauseActivityItem>obtain(PauseActivityItem.class);
    PauseActivityItem pauseActivityItem2 = pauseActivityItem1;
    if (pauseActivityItem1 == null)
      pauseActivityItem2 = new PauseActivityItem(); 
    pauseActivityItem2.mFinished = false;
    pauseActivityItem2.mUserLeaving = false;
    pauseActivityItem2.mConfigChanges = 0;
    pauseActivityItem2.mDontReport = true;
    return pauseActivityItem2;
  }
  
  public void recycle() {
    super.recycle();
    this.mFinished = false;
    this.mUserLeaving = false;
    this.mConfigChanges = 0;
    this.mDontReport = false;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeBoolean(this.mFinished);
    paramParcel.writeBoolean(this.mUserLeaving);
    paramParcel.writeInt(this.mConfigChanges);
    paramParcel.writeBoolean(this.mDontReport);
  }
  
  private PauseActivityItem(Parcel paramParcel) {
    this.mFinished = paramParcel.readBoolean();
    this.mUserLeaving = paramParcel.readBoolean();
    this.mConfigChanges = paramParcel.readInt();
    this.mDontReport = paramParcel.readBoolean();
  }
  
  public static final Parcelable.Creator<PauseActivityItem> CREATOR = (Parcelable.Creator<PauseActivityItem>)new Object();
  
  private static final String TAG = "PauseActivityItem";
  
  private int mConfigChanges;
  
  private boolean mDontReport;
  
  private boolean mFinished;
  
  private boolean mUserLeaving;
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mFinished != ((PauseActivityItem)paramObject).mFinished || this.mUserLeaving != ((PauseActivityItem)paramObject).mUserLeaving || this.mConfigChanges != ((PauseActivityItem)paramObject).mConfigChanges || this.mDontReport != ((PauseActivityItem)paramObject).mDontReport)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    boolean bool1 = this.mFinished;
    boolean bool2 = this.mUserLeaving;
    int i = this.mConfigChanges;
    boolean bool3 = this.mDontReport;
    return (((17 * 31 + bool1) * 31 + bool2) * 31 + i) * 31 + bool3;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PauseActivityItem{finished=");
    stringBuilder.append(this.mFinished);
    stringBuilder.append(",userLeaving=");
    stringBuilder.append(this.mUserLeaving);
    stringBuilder.append(",configChanges=");
    stringBuilder.append(this.mConfigChanges);
    stringBuilder.append(",dontReport=");
    stringBuilder.append(this.mDontReport);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
