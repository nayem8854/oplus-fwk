package android.app.servertransaction;

import android.app.ActivityThread;
import android.app.ClientTransactionHandler;
import android.app.ResultInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Trace;
import android.util.MergedConfiguration;
import android.util.Slog;
import com.android.internal.content.ReferrerIntent;
import java.util.List;
import java.util.Objects;

public class ActivityRelaunchItem extends ClientTransactionItem {
  public void preExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder) {
    this.mActivityClientRecord = paramClientTransactionHandler.prepareRelaunchActivity(paramIBinder, this.mPendingResults, this.mPendingNewIntents, this.mConfigChanges, this.mConfig, this.mPreserveWindow);
  }
  
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    if (this.mActivityClientRecord == null) {
      if (ActivityThread.DEBUG_ORDER)
        Slog.d("ActivityRelaunchItem", "Activity relaunch cancelled"); 
      return;
    } 
    Trace.traceBegin(64L, "activityRestart");
    paramClientTransactionHandler.handleRelaunchActivity(this.mActivityClientRecord, paramPendingTransactionActions);
    Trace.traceEnd(64L);
  }
  
  public void postExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    paramClientTransactionHandler.reportRelaunch(paramIBinder, paramPendingTransactionActions);
  }
  
  private ActivityRelaunchItem() {}
  
  public static ActivityRelaunchItem obtain(List<ResultInfo> paramList, List<ReferrerIntent> paramList1, int paramInt, MergedConfiguration paramMergedConfiguration, boolean paramBoolean) {
    ActivityRelaunchItem activityRelaunchItem1 = ObjectPool.<ActivityRelaunchItem>obtain(ActivityRelaunchItem.class);
    ActivityRelaunchItem activityRelaunchItem2 = activityRelaunchItem1;
    if (activityRelaunchItem1 == null)
      activityRelaunchItem2 = new ActivityRelaunchItem(); 
    activityRelaunchItem2.mPendingResults = paramList;
    activityRelaunchItem2.mPendingNewIntents = paramList1;
    activityRelaunchItem2.mConfigChanges = paramInt;
    activityRelaunchItem2.mConfig = paramMergedConfiguration;
    activityRelaunchItem2.mPreserveWindow = paramBoolean;
    return activityRelaunchItem2;
  }
  
  public void recycle() {
    this.mPendingResults = null;
    this.mPendingNewIntents = null;
    this.mConfigChanges = 0;
    this.mConfig = null;
    this.mPreserveWindow = false;
    this.mActivityClientRecord = null;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mPendingResults, paramInt);
    paramParcel.writeTypedList(this.mPendingNewIntents, paramInt);
    paramParcel.writeInt(this.mConfigChanges);
    paramParcel.writeTypedObject((Parcelable)this.mConfig, paramInt);
    paramParcel.writeBoolean(this.mPreserveWindow);
  }
  
  private ActivityRelaunchItem(Parcel paramParcel) {
    this.mPendingResults = paramParcel.createTypedArrayList(ResultInfo.CREATOR);
    this.mPendingNewIntents = paramParcel.createTypedArrayList(ReferrerIntent.CREATOR);
    this.mConfigChanges = paramParcel.readInt();
    this.mConfig = (MergedConfiguration)paramParcel.readTypedObject(MergedConfiguration.CREATOR);
    this.mPreserveWindow = paramParcel.readBoolean();
  }
  
  public static final Parcelable.Creator<ActivityRelaunchItem> CREATOR = (Parcelable.Creator<ActivityRelaunchItem>)new Object();
  
  private static final String TAG = "ActivityRelaunchItem";
  
  private ActivityThread.ActivityClientRecord mActivityClientRecord;
  
  private MergedConfiguration mConfig;
  
  private int mConfigChanges;
  
  private List<ReferrerIntent> mPendingNewIntents;
  
  private List<ResultInfo> mPendingResults;
  
  private boolean mPreserveWindow;
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mPendingResults, ((ActivityRelaunchItem)paramObject).mPendingResults)) {
      List<ReferrerIntent> list1 = this.mPendingNewIntents, list2 = ((ActivityRelaunchItem)paramObject).mPendingNewIntents;
      if (Objects.equals(list1, list2) && this.mConfigChanges == ((ActivityRelaunchItem)paramObject).mConfigChanges) {
        MergedConfiguration mergedConfiguration2 = this.mConfig, mergedConfiguration1 = ((ActivityRelaunchItem)paramObject).mConfig;
        if (Objects.equals(mergedConfiguration2, mergedConfiguration1) && this.mPreserveWindow == ((ActivityRelaunchItem)paramObject).mPreserveWindow)
          return null; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mPendingResults);
    int j = Objects.hashCode(this.mPendingNewIntents);
    int k = this.mConfigChanges;
    int m = Objects.hashCode(this.mConfig);
    boolean bool = this.mPreserveWindow;
    return ((((17 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ActivityRelaunchItem{pendingResults=");
    stringBuilder.append(this.mPendingResults);
    stringBuilder.append(",pendingNewIntents=");
    stringBuilder.append(this.mPendingNewIntents);
    stringBuilder.append(",configChanges=");
    stringBuilder.append(this.mConfigChanges);
    stringBuilder.append(",config=");
    stringBuilder.append(this.mConfig);
    stringBuilder.append(",preserveWindow");
    stringBuilder.append(this.mPreserveWindow);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
