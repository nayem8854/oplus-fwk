package android.app.servertransaction;

import android.app.ActivityThread;
import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.util.IntArray;
import android.util.Slog;
import java.util.List;
import java.util.Map;

public class TransactionExecutor {
  public static boolean DEBUG_RESOLVER = false;
  
  private PendingTransactionActions mPendingActions = new PendingTransactionActions();
  
  private TransactionExecutorHelper mHelper = new TransactionExecutorHelper();
  
  private static final String TAG = "TransactionExecutor";
  
  private ClientTransactionHandler mTransactionHandler;
  
  public TransactionExecutor(ClientTransactionHandler paramClientTransactionHandler) {
    this.mTransactionHandler = paramClientTransactionHandler;
  }
  
  public void execute(ClientTransaction paramClientTransaction) {
    String str;
    if (DEBUG_RESOLVER) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
      stringBuilder.append("Start resolving transaction");
      Slog.d("TransactionExecutor", stringBuilder.toString());
    } 
    IBinder iBinder = paramClientTransaction.getActivityToken();
    if (iBinder != null) {
      ClientTransactionHandler clientTransactionHandler = this.mTransactionHandler;
      Map<IBinder, ClientTransactionItem> map = clientTransactionHandler.getActivitiesToBeDestroyed();
      ClientTransactionItem clientTransactionItem = map.get(iBinder);
      if (clientTransactionItem != null) {
        if (paramClientTransaction.getLifecycleStateRequest() == clientTransactionItem)
          map.remove(iBinder); 
        if (this.mTransactionHandler.getActivityClient(iBinder) == null) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
          stringBuilder.append("Skip pre-destroyed transaction:\n");
          ClientTransactionHandler clientTransactionHandler1 = this.mTransactionHandler;
          stringBuilder.append(TransactionExecutorHelper.transactionToString(paramClientTransaction, clientTransactionHandler1));
          str = stringBuilder.toString();
          Slog.w("TransactionExecutor", str);
          return;
        } 
      } 
    } 
    if (DEBUG_RESOLVER)
      Slog.d("TransactionExecutor", TransactionExecutorHelper.transactionToString((ClientTransaction)str, this.mTransactionHandler)); 
    executeCallbacks((ClientTransaction)str);
    executeLifecycleState((ClientTransaction)str);
    this.mPendingActions.clear();
    if (DEBUG_RESOLVER) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(TransactionExecutorHelper.tId((ClientTransaction)str));
      stringBuilder.append("End resolving transaction");
      Slog.d("TransactionExecutor", stringBuilder.toString());
    } 
  }
  
  public void executeCallbacks(ClientTransaction paramClientTransaction) {
    byte b;
    List<ClientTransactionItem> list = paramClientTransaction.getCallbacks();
    if (list == null || list.isEmpty())
      return; 
    if (DEBUG_RESOLVER) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
      stringBuilder.append("Resolving callbacks in transaction");
      Slog.d("TransactionExecutor", stringBuilder.toString());
    } 
    IBinder iBinder = paramClientTransaction.getActivityToken();
    ActivityThread.ActivityClientRecord activityClientRecord = this.mTransactionHandler.getActivityClient(iBinder);
    ActivityLifecycleItem activityLifecycleItem = paramClientTransaction.getLifecycleStateRequest();
    if (activityLifecycleItem != null) {
      b = activityLifecycleItem.getTargetState();
    } else {
      b = -1;
    } 
    int i = TransactionExecutorHelper.lastCallbackRequestingState(paramClientTransaction);
    int j = list.size();
    for (byte b1 = 0; b1 < j; b1++, activityClientRecord = activityClientRecord1) {
      ClientTransactionItem clientTransactionItem = list.get(b1);
      if (DEBUG_RESOLVER) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
        stringBuilder.append("Resolving callback: ");
        stringBuilder.append(clientTransactionItem);
        Slog.d("TransactionExecutor", stringBuilder.toString());
      } 
      int k = clientTransactionItem.getPostExecutionState();
      TransactionExecutorHelper transactionExecutorHelper = this.mHelper;
      int m = clientTransactionItem.getPostExecutionState();
      m = transactionExecutorHelper.getClosestPreExecutionState(activityClientRecord, m);
      if (m != -1)
        cycleToPath(activityClientRecord, m, paramClientTransaction); 
      clientTransactionItem.execute(this.mTransactionHandler, iBinder, this.mPendingActions);
      clientTransactionItem.postExecute(this.mTransactionHandler, iBinder, this.mPendingActions);
      ActivityThread.ActivityClientRecord activityClientRecord1 = activityClientRecord;
      if (activityClientRecord == null)
        activityClientRecord1 = this.mTransactionHandler.getActivityClient(iBinder); 
      if (k != -1 && activityClientRecord1 != null) {
        boolean bool;
        if (b1 == i && b == k) {
          bool = true;
        } else {
          bool = false;
        } 
        cycleToPath(activityClientRecord1, k, bool, paramClientTransaction);
      } 
    } 
  }
  
  private void executeLifecycleState(ClientTransaction paramClientTransaction) {
    StringBuilder stringBuilder;
    ActivityLifecycleItem activityLifecycleItem = paramClientTransaction.getLifecycleStateRequest();
    if (activityLifecycleItem == null)
      return; 
    IBinder iBinder = paramClientTransaction.getActivityToken();
    ActivityThread.ActivityClientRecord activityClientRecord = this.mTransactionHandler.getActivityClient(iBinder);
    if (DEBUG_RESOLVER) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(TransactionExecutorHelper.tId(paramClientTransaction));
      stringBuilder1.append("Resolving lifecycle state: ");
      stringBuilder1.append(activityLifecycleItem);
      stringBuilder1.append(" for activity: ");
      ClientTransactionHandler clientTransactionHandler = this.mTransactionHandler;
      stringBuilder1.append(TransactionExecutorHelper.getShortActivityName(iBinder, clientTransactionHandler));
      String str = stringBuilder1.toString();
      Slog.d("TransactionExecutor", str);
    } 
    if (activityClientRecord == null) {
      if (3 == activityLifecycleItem.getTargetState()) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("no activity for token=");
        stringBuilder.append(iBinder);
        stringBuilder.append(",lifecycleItem= ");
        stringBuilder.append(activityLifecycleItem);
        Slog.e("TransactionExecutor", stringBuilder.toString());
      } 
      return;
    } 
    cycleToPath(activityClientRecord, activityLifecycleItem.getTargetState(), true, (ClientTransaction)stringBuilder);
    activityLifecycleItem.execute(this.mTransactionHandler, iBinder, this.mPendingActions);
    activityLifecycleItem.postExecute(this.mTransactionHandler, iBinder, this.mPendingActions);
  }
  
  public void cycleToPath(ActivityThread.ActivityClientRecord paramActivityClientRecord, int paramInt, ClientTransaction paramClientTransaction) {
    cycleToPath(paramActivityClientRecord, paramInt, false, paramClientTransaction);
  }
  
  private void cycleToPath(ActivityThread.ActivityClientRecord paramActivityClientRecord, int paramInt, boolean paramBoolean, ClientTransaction paramClientTransaction) {
    int i = paramActivityClientRecord.getLifecycleState();
    if (DEBUG_RESOLVER) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
      stringBuilder.append("Cycle activity: ");
      IBinder iBinder = paramActivityClientRecord.token;
      ClientTransactionHandler clientTransactionHandler = this.mTransactionHandler;
      stringBuilder.append(TransactionExecutorHelper.getShortActivityName(iBinder, clientTransactionHandler));
      stringBuilder.append(" from: ");
      stringBuilder.append(TransactionExecutorHelper.getStateName(i));
      stringBuilder.append(" to: ");
      stringBuilder.append(TransactionExecutorHelper.getStateName(paramInt));
      stringBuilder.append(" excludeLastState: ");
      stringBuilder.append(paramBoolean);
      String str = stringBuilder.toString();
      Slog.d("TransactionExecutor", str);
    } 
    IntArray intArray = this.mHelper.getLifecyclePath(i, paramInt, paramBoolean);
    performLifecycleSequence(paramActivityClientRecord, intArray, paramClientTransaction);
  }
  
  private void performLifecycleSequence(ActivityThread.ActivityClientRecord paramActivityClientRecord, IntArray paramIntArray, ClientTransaction paramClientTransaction) {
    int i = paramIntArray.size();
    for (byte b = 0; b < i; b++) {
      StringBuilder stringBuilder1;
      ClientTransactionHandler clientTransactionHandler;
      StringBuilder stringBuilder2;
      String str;
      IBinder iBinder;
      int j = paramIntArray.get(b);
      if (DEBUG_RESOLVER) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(TransactionExecutorHelper.tId(paramClientTransaction));
        stringBuilder.append("Transitioning activity: ");
        IBinder iBinder1 = paramActivityClientRecord.token;
        ClientTransactionHandler clientTransactionHandler1 = this.mTransactionHandler;
        stringBuilder.append(TransactionExecutorHelper.getShortActivityName(iBinder1, clientTransactionHandler1));
        stringBuilder.append(" to state: ");
        stringBuilder.append(TransactionExecutorHelper.getStateName(j));
        String str1 = stringBuilder.toString();
        Slog.d("TransactionExecutor", str1);
      } 
      switch (j) {
        default:
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unexpected lifecycle state: ");
          stringBuilder1.append(j);
          throw new IllegalArgumentException(stringBuilder1.toString());
        case 7:
          this.mTransactionHandler.performRestartActivity(((ActivityThread.ActivityClientRecord)stringBuilder1).token, false);
          break;
        case 6:
          clientTransactionHandler = this.mTransactionHandler;
          iBinder = ((ActivityThread.ActivityClientRecord)stringBuilder1).token;
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("performLifecycleSequence. cycling to:");
          stringBuilder2.append(paramIntArray.get(i - 1));
          str = stringBuilder2.toString();
          clientTransactionHandler.handleDestroyActivity(iBinder, false, 0, false, str);
          break;
        case 5:
          this.mTransactionHandler.handleStopActivity(((ActivityThread.ActivityClientRecord)stringBuilder1).token, 0, this.mPendingActions, false, "LIFECYCLER_STOP_ACTIVITY");
          break;
        case 4:
          this.mTransactionHandler.handlePauseActivity(((ActivityThread.ActivityClientRecord)stringBuilder1).token, false, false, 0, this.mPendingActions, "LIFECYCLER_PAUSE_ACTIVITY");
          break;
        case 3:
          this.mTransactionHandler.handleResumeActivity(((ActivityThread.ActivityClientRecord)stringBuilder1).token, false, ((ActivityThread.ActivityClientRecord)stringBuilder1).isForward, "LIFECYCLER_RESUME_ACTIVITY");
          break;
        case 2:
          this.mTransactionHandler.handleStartActivity(((ActivityThread.ActivityClientRecord)stringBuilder1).token, this.mPendingActions);
          break;
        case 1:
          this.mTransactionHandler.handleLaunchActivity((ActivityThread.ActivityClientRecord)stringBuilder1, this.mPendingActions, null);
          break;
      } 
    } 
  }
}
