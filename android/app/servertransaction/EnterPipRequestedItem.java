package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class EnterPipRequestedItem extends ClientTransactionItem {
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    paramClientTransactionHandler.handlePictureInPictureRequested(paramIBinder);
  }
  
  private EnterPipRequestedItem() {}
  
  public static EnterPipRequestedItem obtain() {
    EnterPipRequestedItem enterPipRequestedItem1 = ObjectPool.<EnterPipRequestedItem>obtain(EnterPipRequestedItem.class);
    EnterPipRequestedItem enterPipRequestedItem2 = enterPipRequestedItem1;
    if (enterPipRequestedItem1 == null)
      enterPipRequestedItem2 = new EnterPipRequestedItem(); 
    return enterPipRequestedItem2;
  }
  
  public void recycle() {
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {}
  
  public static final Parcelable.Creator<EnterPipRequestedItem> CREATOR = (Parcelable.Creator<EnterPipRequestedItem>)new Object();
  
  public boolean equals(Object paramObject) {
    boolean bool;
    if (this == paramObject) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    return "EnterPipRequestedItem{}";
  }
}
