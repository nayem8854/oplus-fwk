package android.app.servertransaction;

import android.app.ActivityThread;
import android.app.ClientTransactionHandler;
import android.app.ProfilerInfo;
import android.app.ResultInfo;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.Trace;
import android.view.DisplayAdjustments;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.content.ReferrerIntent;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LaunchActivityItem extends ClientTransactionItem {
  public void preExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder) {
    paramClientTransactionHandler.countLaunchingActivities(1);
    paramClientTransactionHandler.updateProcessState(this.mProcState, false);
    paramClientTransactionHandler.updatePendingConfiguration(this.mCurConfig);
  }
  
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    Trace.traceBegin(64L, "activityStart");
    ActivityThread.ActivityClientRecord activityClientRecord = new ActivityThread.ActivityClientRecord(paramIBinder, this.mIntent, this.mIdent, this.mInfo, this.mOverrideConfig, this.mCompatInfo, this.mReferrer, this.mVoiceInteractor, this.mState, this.mPersistentState, this.mPendingResults, this.mPendingNewIntents, this.mIsForward, this.mProfilerInfo, paramClientTransactionHandler, this.mAssistToken, this.mFixedRotationAdjustments);
    paramClientTransactionHandler.handleLaunchActivity(activityClientRecord, paramPendingTransactionActions, null);
    Trace.traceEnd(64L);
  }
  
  public void postExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    paramClientTransactionHandler.countLaunchingActivities(-1);
  }
  
  private LaunchActivityItem() {}
  
  public static LaunchActivityItem obtain(Intent paramIntent, int paramInt1, ActivityInfo paramActivityInfo, Configuration paramConfiguration1, Configuration paramConfiguration2, CompatibilityInfo paramCompatibilityInfo, String paramString, IVoiceInteractor paramIVoiceInteractor, int paramInt2, Bundle paramBundle, PersistableBundle paramPersistableBundle, List<ResultInfo> paramList, List<ReferrerIntent> paramList1, boolean paramBoolean, ProfilerInfo paramProfilerInfo, IBinder paramIBinder, DisplayAdjustments.FixedRotationAdjustments paramFixedRotationAdjustments) {
    LaunchActivityItem launchActivityItem1 = ObjectPool.<LaunchActivityItem>obtain(LaunchActivityItem.class);
    LaunchActivityItem launchActivityItem2 = launchActivityItem1;
    if (launchActivityItem1 == null)
      launchActivityItem2 = new LaunchActivityItem(); 
    setValues(launchActivityItem2, paramIntent, paramInt1, paramActivityInfo, paramConfiguration1, paramConfiguration2, paramCompatibilityInfo, paramString, paramIVoiceInteractor, paramInt2, paramBundle, paramPersistableBundle, paramList, paramList1, paramBoolean, paramProfilerInfo, paramIBinder, paramFixedRotationAdjustments);
    return launchActivityItem2;
  }
  
  public void recycle() {
    setValues(this, null, 0, null, null, null, null, null, null, 0, null, null, null, null, false, null, null, null);
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedObject(this.mIntent, paramInt);
    paramParcel.writeInt(this.mIdent);
    paramParcel.writeTypedObject(this.mInfo, paramInt);
    paramParcel.writeTypedObject(this.mCurConfig, paramInt);
    paramParcel.writeTypedObject(this.mOverrideConfig, paramInt);
    paramParcel.writeTypedObject(this.mCompatInfo, paramInt);
    paramParcel.writeString(this.mReferrer);
    paramParcel.writeStrongInterface((IInterface)this.mVoiceInteractor);
    paramParcel.writeInt(this.mProcState);
    paramParcel.writeBundle(this.mState);
    paramParcel.writePersistableBundle(this.mPersistentState);
    paramParcel.writeTypedList(this.mPendingResults, paramInt);
    paramParcel.writeTypedList(this.mPendingNewIntents, paramInt);
    paramParcel.writeBoolean(this.mIsForward);
    paramParcel.writeTypedObject(this.mProfilerInfo, paramInt);
    paramParcel.writeStrongBinder(this.mAssistToken);
    paramParcel.writeTypedObject((Parcelable)this.mFixedRotationAdjustments, paramInt);
  }
  
  private LaunchActivityItem(Parcel paramParcel) {
    Intent intent = (Intent)paramParcel.readTypedObject(Intent.CREATOR);
    int i = paramParcel.readInt();
    Parcelable.Creator<ActivityInfo> creator = ActivityInfo.CREATOR;
    ActivityInfo activityInfo = (ActivityInfo)paramParcel.readTypedObject(creator);
    Configuration configuration1 = (Configuration)paramParcel.readTypedObject(Configuration.CREATOR);
    Parcelable.Creator<Configuration> creator1 = Configuration.CREATOR;
    Configuration configuration2 = (Configuration)paramParcel.readTypedObject(creator1);
    Parcelable.Creator<CompatibilityInfo> creator2 = CompatibilityInfo.CREATOR;
    CompatibilityInfo compatibilityInfo = (CompatibilityInfo)paramParcel.readTypedObject(creator2);
    String str = paramParcel.readString();
    IVoiceInteractor iVoiceInteractor = IVoiceInteractor.Stub.asInterface(paramParcel.readStrongBinder());
    int j = paramParcel.readInt();
    Bundle bundle = paramParcel.readBundle(getClass().getClassLoader());
    PersistableBundle persistableBundle = paramParcel.readPersistableBundle(getClass().getClassLoader());
    Parcelable.Creator<ResultInfo> creator3 = ResultInfo.CREATOR;
    ArrayList<ResultInfo> arrayList = paramParcel.createTypedArrayList(creator3);
    Parcelable.Creator creator4 = ReferrerIntent.CREATOR;
    ArrayList<ReferrerIntent> arrayList1 = paramParcel.createTypedArrayList(creator4);
    boolean bool = paramParcel.readBoolean();
    Parcelable.Creator<ProfilerInfo> creator5 = ProfilerInfo.CREATOR;
    ProfilerInfo profilerInfo = (ProfilerInfo)paramParcel.readTypedObject(creator5);
    IBinder iBinder = paramParcel.readStrongBinder();
    Parcelable.Creator creator6 = DisplayAdjustments.FixedRotationAdjustments.CREATOR;
    DisplayAdjustments.FixedRotationAdjustments fixedRotationAdjustments = (DisplayAdjustments.FixedRotationAdjustments)paramParcel.readTypedObject(creator6);
    setValues(this, intent, i, activityInfo, configuration1, configuration2, compatibilityInfo, str, iVoiceInteractor, j, bundle, persistableBundle, arrayList, arrayList1, bool, profilerInfo, iBinder, fixedRotationAdjustments);
  }
  
  public static final Parcelable.Creator<LaunchActivityItem> CREATOR = (Parcelable.Creator<LaunchActivityItem>)new Object();
  
  private IBinder mAssistToken;
  
  private CompatibilityInfo mCompatInfo;
  
  private Configuration mCurConfig;
  
  private DisplayAdjustments.FixedRotationAdjustments mFixedRotationAdjustments;
  
  private int mIdent;
  
  private ActivityInfo mInfo;
  
  private Intent mIntent;
  
  private boolean mIsForward;
  
  private Configuration mOverrideConfig;
  
  private List<ReferrerIntent> mPendingNewIntents;
  
  private List<ResultInfo> mPendingResults;
  
  private PersistableBundle mPersistentState;
  
  private int mProcState;
  
  private ProfilerInfo mProfilerInfo;
  
  private String mReferrer;
  
  private Bundle mState;
  
  private IVoiceInteractor mVoiceInteractor;
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: aload_0
    //   3: aload_1
    //   4: if_acmpne -> 9
    //   7: iconst_1
    //   8: ireturn
    //   9: aload_1
    //   10: ifnull -> 353
    //   13: aload_0
    //   14: invokevirtual getClass : ()Ljava/lang/Class;
    //   17: aload_1
    //   18: invokevirtual getClass : ()Ljava/lang/Class;
    //   21: if_acmpeq -> 27
    //   24: goto -> 353
    //   27: aload_1
    //   28: checkcast android/app/servertransaction/LaunchActivityItem
    //   31: astore_1
    //   32: aload_0
    //   33: getfield mIntent : Landroid/content/Intent;
    //   36: ifnonnull -> 46
    //   39: aload_1
    //   40: getfield mIntent : Landroid/content/Intent;
    //   43: ifnull -> 70
    //   46: aload_0
    //   47: getfield mIntent : Landroid/content/Intent;
    //   50: astore_3
    //   51: aload_3
    //   52: ifnull -> 76
    //   55: aload_1
    //   56: getfield mIntent : Landroid/content/Intent;
    //   59: astore #4
    //   61: aload_3
    //   62: aload #4
    //   64: invokevirtual filterEquals : (Landroid/content/Intent;)Z
    //   67: ifeq -> 76
    //   70: iconst_1
    //   71: istore #5
    //   73: goto -> 79
    //   76: iconst_0
    //   77: istore #5
    //   79: iload #5
    //   81: ifeq -> 349
    //   84: aload_0
    //   85: getfield mIdent : I
    //   88: aload_1
    //   89: getfield mIdent : I
    //   92: if_icmpne -> 349
    //   95: aload_1
    //   96: getfield mInfo : Landroid/content/pm/ActivityInfo;
    //   99: astore #4
    //   101: aload_0
    //   102: aload #4
    //   104: invokespecial activityInfoEqual : (Landroid/content/pm/ActivityInfo;)Z
    //   107: ifeq -> 349
    //   110: aload_0
    //   111: getfield mCurConfig : Landroid/content/res/Configuration;
    //   114: aload_1
    //   115: getfield mCurConfig : Landroid/content/res/Configuration;
    //   118: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   121: ifeq -> 349
    //   124: aload_0
    //   125: getfield mOverrideConfig : Landroid/content/res/Configuration;
    //   128: astore_3
    //   129: aload_1
    //   130: getfield mOverrideConfig : Landroid/content/res/Configuration;
    //   133: astore #4
    //   135: aload_3
    //   136: aload #4
    //   138: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   141: ifeq -> 349
    //   144: aload_0
    //   145: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   148: astore #4
    //   150: aload_1
    //   151: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   154: astore_3
    //   155: aload #4
    //   157: aload_3
    //   158: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   161: ifeq -> 349
    //   164: aload_0
    //   165: getfield mReferrer : Ljava/lang/String;
    //   168: astore #4
    //   170: aload_1
    //   171: getfield mReferrer : Ljava/lang/String;
    //   174: astore_3
    //   175: aload #4
    //   177: aload_3
    //   178: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   181: ifeq -> 349
    //   184: aload_0
    //   185: getfield mProcState : I
    //   188: aload_1
    //   189: getfield mProcState : I
    //   192: if_icmpne -> 349
    //   195: aload_0
    //   196: getfield mState : Landroid/os/Bundle;
    //   199: astore #4
    //   201: aload_1
    //   202: getfield mState : Landroid/os/Bundle;
    //   205: astore_3
    //   206: aload #4
    //   208: aload_3
    //   209: invokestatic areBundlesEqualRoughly : (Landroid/os/BaseBundle;Landroid/os/BaseBundle;)Z
    //   212: ifeq -> 349
    //   215: aload_0
    //   216: getfield mPersistentState : Landroid/os/PersistableBundle;
    //   219: astore_3
    //   220: aload_1
    //   221: getfield mPersistentState : Landroid/os/PersistableBundle;
    //   224: astore #4
    //   226: aload_3
    //   227: aload #4
    //   229: invokestatic areBundlesEqualRoughly : (Landroid/os/BaseBundle;Landroid/os/BaseBundle;)Z
    //   232: ifeq -> 349
    //   235: aload_0
    //   236: getfield mPendingResults : Ljava/util/List;
    //   239: astore #4
    //   241: aload_1
    //   242: getfield mPendingResults : Ljava/util/List;
    //   245: astore_3
    //   246: aload #4
    //   248: aload_3
    //   249: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   252: ifeq -> 349
    //   255: aload_0
    //   256: getfield mPendingNewIntents : Ljava/util/List;
    //   259: astore_3
    //   260: aload_1
    //   261: getfield mPendingNewIntents : Ljava/util/List;
    //   264: astore #4
    //   266: aload_3
    //   267: aload #4
    //   269: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   272: ifeq -> 349
    //   275: aload_0
    //   276: getfield mIsForward : Z
    //   279: aload_1
    //   280: getfield mIsForward : Z
    //   283: if_icmpne -> 349
    //   286: aload_0
    //   287: getfield mProfilerInfo : Landroid/app/ProfilerInfo;
    //   290: astore #4
    //   292: aload_1
    //   293: getfield mProfilerInfo : Landroid/app/ProfilerInfo;
    //   296: astore_3
    //   297: aload #4
    //   299: aload_3
    //   300: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   303: ifeq -> 349
    //   306: aload_0
    //   307: getfield mAssistToken : Landroid/os/IBinder;
    //   310: astore_3
    //   311: aload_1
    //   312: getfield mAssistToken : Landroid/os/IBinder;
    //   315: astore #4
    //   317: aload_3
    //   318: aload #4
    //   320: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   323: ifeq -> 349
    //   326: aload_0
    //   327: getfield mFixedRotationAdjustments : Landroid/view/DisplayAdjustments$FixedRotationAdjustments;
    //   330: astore #4
    //   332: aload_1
    //   333: getfield mFixedRotationAdjustments : Landroid/view/DisplayAdjustments$FixedRotationAdjustments;
    //   336: astore_1
    //   337: aload #4
    //   339: aload_1
    //   340: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   343: ifeq -> 349
    //   346: goto -> 351
    //   349: iconst_0
    //   350: istore_2
    //   351: iload_2
    //   352: ireturn
    //   353: iconst_0
    //   354: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #179	-> 0
    //   #180	-> 7
    //   #182	-> 9
    //   #185	-> 27
    //   #186	-> 32
    //   #187	-> 61
    //   #188	-> 79
    //   #189	-> 101
    //   #190	-> 135
    //   #191	-> 155
    //   #192	-> 175
    //   #193	-> 206
    //   #194	-> 226
    //   #195	-> 246
    //   #196	-> 266
    //   #198	-> 297
    //   #199	-> 317
    //   #200	-> 337
    //   #188	-> 351
    //   #183	-> 353
  }
  
  public int hashCode() {
    int i = this.mIntent.filterHashCode();
    int j = this.mIdent;
    int k = Objects.hashCode(this.mCurConfig);
    int m = Objects.hashCode(this.mOverrideConfig);
    int n = Objects.hashCode(this.mCompatInfo);
    int i1 = Objects.hashCode(this.mReferrer);
    int i2 = Objects.hashCode(Integer.valueOf(this.mProcState));
    int i3 = getRoughBundleHashCode((BaseBundle)this.mState);
    int i4 = getRoughBundleHashCode((BaseBundle)this.mPersistentState);
    int i5 = Objects.hashCode(this.mPendingResults);
    int i6 = Objects.hashCode(this.mPendingNewIntents);
    boolean bool = this.mIsForward;
    int i7 = Objects.hashCode(this.mProfilerInfo);
    int i8 = Objects.hashCode(this.mAssistToken);
    int i9 = Objects.hashCode(this.mFixedRotationAdjustments);
    return ((((((((((((((17 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + bool) * 31 + i7) * 31 + i8) * 31 + i9;
  }
  
  private boolean activityInfoEqual(ActivityInfo paramActivityInfo) {
    ActivityInfo activityInfo = this.mInfo;
    null = true;
    boolean bool = true;
    if (activityInfo == null) {
      if (paramActivityInfo == null) {
        null = bool;
      } else {
        null = false;
      } 
      return null;
    } 
    if (paramActivityInfo != null && activityInfo.flags == paramActivityInfo.flags && this.mInfo.maxAspectRatio == paramActivityInfo.maxAspectRatio) {
      String str1 = this.mInfo.launchToken, str2 = paramActivityInfo.launchToken;
      if (Objects.equals(str1, str2)) {
        ActivityInfo activityInfo1 = this.mInfo;
        if (Objects.equals(activityInfo1.getComponentName(), paramActivityInfo.getComponentName()))
          return null; 
      } 
    } 
    return false;
  }
  
  private static int getRoughBundleHashCode(BaseBundle paramBaseBundle) {
    return (paramBaseBundle == null || paramBaseBundle.isDefinitelyEmpty()) ? 0 : 1;
  }
  
  private static boolean areBundlesEqualRoughly(BaseBundle paramBaseBundle1, BaseBundle paramBaseBundle2) {
    boolean bool;
    if (getRoughBundleHashCode(paramBaseBundle1) == getRoughBundleHashCode(paramBaseBundle2)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("LaunchActivityItem{intent=");
    stringBuilder.append(this.mIntent);
    stringBuilder.append(",ident=");
    stringBuilder.append(this.mIdent);
    stringBuilder.append(",info=");
    stringBuilder.append(this.mInfo);
    stringBuilder.append(",curConfig=");
    stringBuilder.append(this.mCurConfig);
    stringBuilder.append(",overrideConfig=");
    stringBuilder.append(this.mOverrideConfig);
    stringBuilder.append(",referrer=");
    stringBuilder.append(this.mReferrer);
    stringBuilder.append(",procState=");
    stringBuilder.append(this.mProcState);
    stringBuilder.append(",state=");
    stringBuilder.append(this.mState);
    stringBuilder.append(",persistentState=");
    stringBuilder.append(this.mPersistentState);
    stringBuilder.append(",pendingResults=");
    stringBuilder.append(this.mPendingResults);
    stringBuilder.append(",pendingNewIntents=");
    stringBuilder.append(this.mPendingNewIntents);
    stringBuilder.append(",profilerInfo=");
    stringBuilder.append(this.mProfilerInfo);
    stringBuilder.append(",assistToken=");
    stringBuilder.append(this.mAssistToken);
    stringBuilder.append(",rotationAdj=");
    stringBuilder.append(this.mFixedRotationAdjustments);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private static void setValues(LaunchActivityItem paramLaunchActivityItem, Intent paramIntent, int paramInt1, ActivityInfo paramActivityInfo, Configuration paramConfiguration1, Configuration paramConfiguration2, CompatibilityInfo paramCompatibilityInfo, String paramString, IVoiceInteractor paramIVoiceInteractor, int paramInt2, Bundle paramBundle, PersistableBundle paramPersistableBundle, List<ResultInfo> paramList, List<ReferrerIntent> paramList1, boolean paramBoolean, ProfilerInfo paramProfilerInfo, IBinder paramIBinder, DisplayAdjustments.FixedRotationAdjustments paramFixedRotationAdjustments) {
    paramLaunchActivityItem.mIntent = paramIntent;
    paramLaunchActivityItem.mIdent = paramInt1;
    paramLaunchActivityItem.mInfo = paramActivityInfo;
    paramLaunchActivityItem.mCurConfig = paramConfiguration1;
    paramLaunchActivityItem.mOverrideConfig = paramConfiguration2;
    paramLaunchActivityItem.mCompatInfo = paramCompatibilityInfo;
    paramLaunchActivityItem.mReferrer = paramString;
    paramLaunchActivityItem.mVoiceInteractor = paramIVoiceInteractor;
    paramLaunchActivityItem.mProcState = paramInt2;
    paramLaunchActivityItem.mState = paramBundle;
    paramLaunchActivityItem.mPersistentState = paramPersistableBundle;
    paramLaunchActivityItem.mPendingResults = paramList;
    paramLaunchActivityItem.mPendingNewIntents = paramList1;
    paramLaunchActivityItem.mIsForward = paramBoolean;
    paramLaunchActivityItem.mProfilerInfo = paramProfilerInfo;
    paramLaunchActivityItem.mAssistToken = paramIBinder;
    paramLaunchActivityItem.mFixedRotationAdjustments = paramFixedRotationAdjustments;
  }
}
