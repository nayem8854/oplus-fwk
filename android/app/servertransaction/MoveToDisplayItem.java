package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.content.res.Configuration;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Trace;
import java.util.Objects;

public class MoveToDisplayItem extends ClientTransactionItem {
  public void preExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder) {
    paramClientTransactionHandler.updatePendingActivityConfiguration(paramIBinder, this.mConfiguration);
  }
  
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    Trace.traceBegin(64L, "activityMovedToDisplay");
    paramClientTransactionHandler.handleActivityConfigurationChanged(paramIBinder, this.mConfiguration, this.mTargetDisplayId);
    Trace.traceEnd(64L);
  }
  
  private MoveToDisplayItem() {}
  
  public static MoveToDisplayItem obtain(int paramInt, Configuration paramConfiguration) {
    if (paramConfiguration != null) {
      MoveToDisplayItem moveToDisplayItem1 = ObjectPool.<MoveToDisplayItem>obtain(MoveToDisplayItem.class);
      MoveToDisplayItem moveToDisplayItem2 = moveToDisplayItem1;
      if (moveToDisplayItem1 == null)
        moveToDisplayItem2 = new MoveToDisplayItem(); 
      moveToDisplayItem2.mTargetDisplayId = paramInt;
      moveToDisplayItem2.mConfiguration = paramConfiguration;
      return moveToDisplayItem2;
    } 
    throw new IllegalArgumentException("Configuration must not be null");
  }
  
  public void recycle() {
    this.mTargetDisplayId = 0;
    this.mConfiguration = Configuration.EMPTY;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTargetDisplayId);
    paramParcel.writeTypedObject(this.mConfiguration, paramInt);
  }
  
  private MoveToDisplayItem(Parcel paramParcel) {
    this.mTargetDisplayId = paramParcel.readInt();
    this.mConfiguration = (Configuration)paramParcel.readTypedObject(Configuration.CREATOR);
  }
  
  public static final Parcelable.Creator<MoveToDisplayItem> CREATOR = (Parcelable.Creator<MoveToDisplayItem>)new Object();
  
  private Configuration mConfiguration;
  
  private int mTargetDisplayId;
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    MoveToDisplayItem moveToDisplayItem = (MoveToDisplayItem)paramObject;
    if (this.mTargetDisplayId == moveToDisplayItem.mTargetDisplayId) {
      paramObject = this.mConfiguration;
      Configuration configuration = moveToDisplayItem.mConfiguration;
      if (Objects.equals(paramObject, configuration))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mTargetDisplayId;
    int j = this.mConfiguration.hashCode();
    return (17 * 31 + i) * 31 + j;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MoveToDisplayItem{targetDisplayId=");
    stringBuilder.append(this.mTargetDisplayId);
    stringBuilder.append(",configuration=");
    stringBuilder.append(this.mConfiguration);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
