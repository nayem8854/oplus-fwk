package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Trace;

public class StartActivityItem extends ActivityLifecycleItem {
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    Trace.traceBegin(64L, "startActivityItem");
    paramClientTransactionHandler.handleStartActivity(paramIBinder, paramPendingTransactionActions);
    Trace.traceEnd(64L);
  }
  
  public int getTargetState() {
    return 2;
  }
  
  private StartActivityItem() {}
  
  public static StartActivityItem obtain() {
    StartActivityItem startActivityItem1 = ObjectPool.<StartActivityItem>obtain(StartActivityItem.class);
    StartActivityItem startActivityItem2 = startActivityItem1;
    if (startActivityItem1 == null)
      startActivityItem2 = new StartActivityItem(); 
    return startActivityItem2;
  }
  
  public void recycle() {
    super.recycle();
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {}
  
  private StartActivityItem(Parcel paramParcel) {}
  
  public static final Parcelable.Creator<StartActivityItem> CREATOR = (Parcelable.Creator<StartActivityItem>)new Object();
  
  private static final String TAG = "StartActivityItem";
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    return true;
  }
  
  public int hashCode() {
    return 17;
  }
  
  public String toString() {
    return "StartActivityItem{}";
  }
}
