package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.app.IApplicationThread;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ClientTransaction implements Parcelable, ObjectPoolItem {
  public IApplicationThread getClient() {
    return this.mClient;
  }
  
  public void addCallback(ClientTransactionItem paramClientTransactionItem) {
    if (this.mActivityCallbacks == null)
      this.mActivityCallbacks = new ArrayList<>(); 
    this.mActivityCallbacks.add(paramClientTransactionItem);
  }
  
  public List<ClientTransactionItem> getCallbacks() {
    return this.mActivityCallbacks;
  }
  
  public IBinder getActivityToken() {
    return this.mActivityToken;
  }
  
  public ActivityLifecycleItem getLifecycleStateRequest() {
    return this.mLifecycleStateRequest;
  }
  
  public void setLifecycleStateRequest(ActivityLifecycleItem paramActivityLifecycleItem) {
    this.mLifecycleStateRequest = paramActivityLifecycleItem;
  }
  
  public void preExecute(ClientTransactionHandler paramClientTransactionHandler) {
    List<ClientTransactionItem> list = this.mActivityCallbacks;
    if (list != null) {
      int i = list.size();
      for (byte b = 0; b < i; b++)
        ((ClientTransactionItem)this.mActivityCallbacks.get(b)).preExecute(paramClientTransactionHandler, this.mActivityToken); 
    } 
    ActivityLifecycleItem activityLifecycleItem = this.mLifecycleStateRequest;
    if (activityLifecycleItem != null)
      activityLifecycleItem.preExecute(paramClientTransactionHandler, this.mActivityToken); 
  }
  
  public void schedule() throws RemoteException {
    this.mClient.scheduleTransaction(this);
  }
  
  private ClientTransaction() {}
  
  public static ClientTransaction obtain(IApplicationThread paramIApplicationThread, IBinder paramIBinder) {
    ClientTransaction clientTransaction1 = ObjectPool.<ClientTransaction>obtain(ClientTransaction.class);
    ClientTransaction clientTransaction2 = clientTransaction1;
    if (clientTransaction1 == null)
      clientTransaction2 = new ClientTransaction(); 
    clientTransaction2.mClient = paramIApplicationThread;
    clientTransaction2.mActivityToken = paramIBinder;
    return clientTransaction2;
  }
  
  public void recycle() {
    List<ClientTransactionItem> list = this.mActivityCallbacks;
    if (list != null) {
      int i = list.size();
      for (byte b = 0; b < i; b++)
        ((ClientTransactionItem)this.mActivityCallbacks.get(b)).recycle(); 
      this.mActivityCallbacks.clear();
    } 
    ActivityLifecycleItem activityLifecycleItem = this.mLifecycleStateRequest;
    if (activityLifecycleItem != null) {
      activityLifecycleItem.recycle();
      this.mLifecycleStateRequest = null;
    } 
    this.mClient = null;
    this.mActivityToken = null;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool2;
    paramParcel.writeStrongBinder(this.mClient.asBinder());
    IBinder iBinder = this.mActivityToken;
    boolean bool1 = true;
    if (iBinder != null) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramParcel.writeBoolean(bool2);
    if (bool2)
      paramParcel.writeStrongBinder(this.mActivityToken); 
    paramParcel.writeParcelable(this.mLifecycleStateRequest, paramInt);
    if (this.mActivityCallbacks != null) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    paramParcel.writeBoolean(bool2);
    if (bool2)
      paramParcel.writeParcelableList(this.mActivityCallbacks, paramInt); 
    paramParcel.writeInt(this.seq);
  }
  
  private ClientTransaction(Parcel paramParcel) {
    this.mClient = (IApplicationThread)paramParcel.readStrongBinder();
    boolean bool = paramParcel.readBoolean();
    if (bool)
      this.mActivityToken = paramParcel.readStrongBinder(); 
    this.mLifecycleStateRequest = (ActivityLifecycleItem)paramParcel.readParcelable(getClass().getClassLoader());
    bool = paramParcel.readBoolean();
    if (bool) {
      ArrayList<ClientTransactionItem> arrayList = new ArrayList();
      paramParcel.readParcelableList(arrayList, getClass().getClassLoader());
    } 
    this.seq = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ClientTransaction> CREATOR = new Parcelable.Creator<ClientTransaction>() {
      public ClientTransaction createFromParcel(Parcel param1Parcel) {
        return new ClientTransaction(param1Parcel);
      }
      
      public ClientTransaction[] newArray(int param1Int) {
        return new ClientTransaction[param1Int];
      }
    };
  
  private List<ClientTransactionItem> mActivityCallbacks;
  
  private IBinder mActivityToken;
  
  private IApplicationThread mClient;
  
  private ActivityLifecycleItem mLifecycleStateRequest;
  
  public int seq;
  
  public int describeContents() {
    return 0;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    ClientTransaction clientTransaction = (ClientTransaction)paramObject;
    if (Objects.equals(this.mActivityCallbacks, clientTransaction.mActivityCallbacks)) {
      ActivityLifecycleItem activityLifecycleItem = this.mLifecycleStateRequest;
      paramObject = clientTransaction.mLifecycleStateRequest;
      if (Objects.equals(activityLifecycleItem, paramObject) && this.mClient == clientTransaction.mClient && this.mActivityToken == clientTransaction.mActivityToken)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mActivityCallbacks);
    int j = Objects.hashCode(this.mLifecycleStateRequest);
    return (17 * 31 + i) * 31 + j;
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    byte b;
    paramPrintWriter.append(paramString).println("ClientTransaction{");
    paramPrintWriter.append(paramString).print("  callbacks=[");
    List<ClientTransactionItem> list = this.mActivityCallbacks;
    if (list != null) {
      b = list.size();
    } else {
      b = 0;
    } 
    if (b) {
      paramPrintWriter.println();
      for (byte b1 = 0; b1 < b; b1++)
        paramPrintWriter.append(paramString).append("    ").println(((ClientTransactionItem)this.mActivityCallbacks.get(b1)).toString()); 
      paramPrintWriter.append(paramString).println("  ]");
    } else {
      paramPrintWriter.println("]");
    } 
    PrintWriter printWriter = paramPrintWriter.append(paramString).append("  stateRequest=");
    ActivityLifecycleItem activityLifecycleItem = this.mLifecycleStateRequest;
    if (activityLifecycleItem != null) {
      String str = activityLifecycleItem.toString();
    } else {
      activityLifecycleItem = null;
    } 
    printWriter.println((String)activityLifecycleItem);
    paramPrintWriter.append(paramString).println("}");
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    List<ClientTransactionItem> list = this.mActivityCallbacks;
    if (list != null)
      for (ClientTransactionItem clientTransactionItem : list) {
        stringBuilder1.append(clientTransactionItem.getClass().getSimpleName());
        stringBuilder1.append(",");
      }  
    ActivityLifecycleItem activityLifecycleItem = this.mLifecycleStateRequest;
    if (activityLifecycleItem != null)
      stringBuilder1.append(activityLifecycleItem.getClass().getSimpleName()); 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("ClientTransaction ");
    stringBuilder2.append(stringBuilder1.toString());
    stringBuilder2.append(" hashCode, mActivityToken = ");
    stringBuilder2.append(this.mActivityToken);
    stringBuilder2.append(" mLifecycleStateRequest ");
    stringBuilder2.append(this.mLifecycleStateRequest);
    stringBuilder2.append(" mActivityCallbacks ");
    stringBuilder2.append(this.mActivityCallbacks);
    return stringBuilder2.toString();
  }
}
