package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.DisplayAdjustments;
import java.util.Objects;

public class FixedRotationAdjustmentsItem extends ClientTransactionItem {
  private FixedRotationAdjustmentsItem() {}
  
  public static FixedRotationAdjustmentsItem obtain(IBinder paramIBinder, DisplayAdjustments.FixedRotationAdjustments paramFixedRotationAdjustments) {
    FixedRotationAdjustmentsItem fixedRotationAdjustmentsItem1 = ObjectPool.<FixedRotationAdjustmentsItem>obtain(FixedRotationAdjustmentsItem.class);
    FixedRotationAdjustmentsItem fixedRotationAdjustmentsItem2 = fixedRotationAdjustmentsItem1;
    if (fixedRotationAdjustmentsItem1 == null)
      fixedRotationAdjustmentsItem2 = new FixedRotationAdjustmentsItem(); 
    fixedRotationAdjustmentsItem2.mToken = paramIBinder;
    fixedRotationAdjustmentsItem2.mFixedRotationAdjustments = paramFixedRotationAdjustments;
    return fixedRotationAdjustmentsItem2;
  }
  
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    paramClientTransactionHandler.handleFixedRotationAdjustments(this.mToken, this.mFixedRotationAdjustments);
  }
  
  public void recycle() {
    this.mToken = null;
    this.mFixedRotationAdjustments = null;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStrongBinder(this.mToken);
    paramParcel.writeTypedObject((Parcelable)this.mFixedRotationAdjustments, paramInt);
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    FixedRotationAdjustmentsItem fixedRotationAdjustmentsItem = (FixedRotationAdjustmentsItem)paramObject;
    if (Objects.equals(this.mToken, fixedRotationAdjustmentsItem.mToken)) {
      paramObject = this.mFixedRotationAdjustments;
      DisplayAdjustments.FixedRotationAdjustments fixedRotationAdjustments = fixedRotationAdjustmentsItem.mFixedRotationAdjustments;
      if (Objects.equals(paramObject, fixedRotationAdjustments))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mToken);
    int j = Objects.hashCode(this.mFixedRotationAdjustments);
    return (17 * 31 + i) * 31 + j;
  }
  
  private FixedRotationAdjustmentsItem(Parcel paramParcel) {
    this.mToken = paramParcel.readStrongBinder();
    this.mFixedRotationAdjustments = (DisplayAdjustments.FixedRotationAdjustments)paramParcel.readTypedObject(DisplayAdjustments.FixedRotationAdjustments.CREATOR);
  }
  
  public static final Parcelable.Creator<FixedRotationAdjustmentsItem> CREATOR = (Parcelable.Creator<FixedRotationAdjustmentsItem>)new Object();
  
  private DisplayAdjustments.FixedRotationAdjustments mFixedRotationAdjustments;
  
  private IBinder mToken;
}
