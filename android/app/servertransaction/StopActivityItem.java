package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Trace;

public class StopActivityItem extends ActivityLifecycleItem {
  public void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    Trace.traceBegin(64L, "activityStop");
    paramClientTransactionHandler.handleStopActivity(paramIBinder, this.mConfigChanges, paramPendingTransactionActions, true, "STOP_ACTIVITY_ITEM");
    Trace.traceEnd(64L);
  }
  
  public void postExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    paramClientTransactionHandler.reportStop(paramPendingTransactionActions);
  }
  
  public int getTargetState() {
    return 5;
  }
  
  private StopActivityItem() {}
  
  public static StopActivityItem obtain(int paramInt) {
    StopActivityItem stopActivityItem1 = ObjectPool.<StopActivityItem>obtain(StopActivityItem.class);
    StopActivityItem stopActivityItem2 = stopActivityItem1;
    if (stopActivityItem1 == null)
      stopActivityItem2 = new StopActivityItem(); 
    stopActivityItem2.mConfigChanges = paramInt;
    return stopActivityItem2;
  }
  
  public void recycle() {
    super.recycle();
    this.mConfigChanges = 0;
    ObjectPool.recycle(this);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mConfigChanges);
  }
  
  private StopActivityItem(Parcel paramParcel) {
    this.mConfigChanges = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<StopActivityItem> CREATOR = (Parcelable.Creator<StopActivityItem>)new Object();
  
  private static final String TAG = "StopActivityItem";
  
  private int mConfigChanges;
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mConfigChanges != ((StopActivityItem)paramObject).mConfigChanges)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = this.mConfigChanges;
    return 17 * 31 + i;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("StopActivityItem{configChanges=");
    stringBuilder.append(this.mConfigChanges);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
