package android.app.servertransaction;

import android.app.ClientTransactionHandler;
import android.os.IBinder;

public interface BaseClientRequest extends ObjectPoolItem {
  default void preExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder) {}
  
  default void postExecute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {}
  
  void execute(ClientTransactionHandler paramClientTransactionHandler, IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions);
}
