package android.app;

import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;

public class OplusNotificationManager extends OplusBaseNotificationManager implements IOplusNotificationManager {
  private static final String TAG = "OplusNotificationManager";
  
  public boolean shouldInterceptSound(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean shouldKeepAlive(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10003, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getNavigationMode(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isDriveNavigationMode(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isNavigationMode(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String[] getEnableNavigationApps(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    null = new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      parcel2.readStringList(null);
      return (String[])null.toArray((Object[])new String[null.size()]);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSuppressedByDriveMode(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10008, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setSuppressedByDriveMode(boolean paramBoolean, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(String.valueOf(paramBoolean));
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10009, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getOpenid--pkg:");
    stringBuilder.append(paramString1);
    stringBuilder.append(",uid:");
    stringBuilder.append(paramInt);
    stringBuilder.append(",type:");
    stringBuilder.append(paramString2);
    Log.d("OplusNotificationManager", stringBuilder.toString());
    Parcel parcel2 = Parcel.obtain();
    Parcel parcel1 = Parcel.obtain();
    try {
      parcel2.writeInterfaceToken("android.app.INotificationManager");
      parcel2.writeString(paramString1);
      parcel2.writeInt(paramInt);
      parcel2.writeString(paramString2);
      this.mRemote.transact(10010, parcel2, parcel1, 0);
      parcel1.readException();
      paramString1 = parcel1.readString();
      return paramString1;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void clearOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10011, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean checkGetOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10018, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setBadgeOption(String paramString, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10012, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getBadgeOption(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10013, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isNumbadgeSupport(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10014, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setNumbadgeSupport(String paramString, int paramInt, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mRemote.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getStowOption(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10016, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = Integer.valueOf(parcel2.readString()).intValue();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setStowOption(String paramString, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeString(String.valueOf(paramInt2));
      this.mRemote.transact(10017, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String getDynamicRingtone(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10023, parcel1, parcel2, 0);
      parcel2.readException();
      paramString1 = parcel2.readString();
      return paramString1;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean getAppBanner(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10020, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setAppBanner(String paramString, int paramInt, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mRemote.transact(10019, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getAppVisibility(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10022, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = Integer.valueOf(parcel2.readString()).intValue();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setAppVisibility(String paramString, int paramInt1, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.INotificationManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeString(String.valueOf(paramInt2));
      this.mRemote.transact(10021, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
