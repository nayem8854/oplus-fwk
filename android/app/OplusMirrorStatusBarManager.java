package android.app;

import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorStatusBarManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorStatusBarManager.class, StatusBarManager.class);
  
  @MethodParams({int.class})
  public static RefMethod<Void> setShortcutsPanelState;
}
