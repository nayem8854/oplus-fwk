package android.app;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.OplusBaseResources;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class OplusUXIconLoadHelper {
  private static final String TAG = "OplusUXIconLoader";
  
  private static final OplusUXIconLoader sIconLoader;
  
  private static int sSupportUxOnline = -1;
  
  static {
    sIconLoader = OplusUXIconLoader.getLoader();
  }
  
  public static Drawable getDrawable(PackageManager paramPackageManager, String paramString1, String paramString2, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: aload_0
    //   4: checkcast android/app/OplusBaseApplicationPackageManager
    //   7: astore #6
    //   9: aload #6
    //   11: aload_1
    //   12: iload_3
    //   13: invokevirtual getCachedIconForThemeHelper : (Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    //   16: astore #7
    //   18: aload #7
    //   20: ifnull -> 37
    //   23: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   26: aload #7
    //   28: invokevirtual setDarkFilterToDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   31: ldc android/app/OplusUXIconLoadHelper
    //   33: monitorexit
    //   34: aload #7
    //   36: areturn
    //   37: ldc2_w 8192
    //   40: ldc '#UxIcon.getDrawable'
    //   42: invokestatic traceBegin : (JLjava/lang/String;)V
    //   45: iload #5
    //   47: ifne -> 59
    //   50: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   53: getstatic android/app/IOplusResolverUxIconDrawableManager.DEFAULT : Landroid/app/IOplusResolverUxIconDrawableManager;
    //   56: putfield mOplusUxIconDrawableManager : Landroid/app/IOplusResolverUxIconDrawableManager;
    //   59: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   62: aload_0
    //   63: aload_1
    //   64: aload_2
    //   65: iload_3
    //   66: aload #4
    //   68: iload #5
    //   70: invokevirtual loadUxIcon : (Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Z)Landroid/graphics/drawable/Drawable;
    //   73: astore_0
    //   74: ldc2_w 8192
    //   77: invokestatic traceEnd : (J)V
    //   80: aload_0
    //   81: ifnull -> 97
    //   84: aload #6
    //   86: aload_1
    //   87: iload_3
    //   88: aload_0
    //   89: invokevirtual putCachedIconForThemeHelper : (Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)V
    //   92: ldc android/app/OplusUXIconLoadHelper
    //   94: monitorexit
    //   95: aload_0
    //   96: areturn
    //   97: ldc android/app/OplusUXIconLoadHelper
    //   99: monitorexit
    //   100: aconst_null
    //   101: areturn
    //   102: astore_0
    //   103: ldc android/app/OplusUXIconLoadHelper
    //   105: monitorexit
    //   106: aload_0
    //   107: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #53	-> 3
    //   #54	-> 9
    //   #55	-> 18
    //   #56	-> 23
    //   #57	-> 31
    //   #60	-> 37
    //   #63	-> 45
    //   #64	-> 50
    //   #66	-> 59
    //   #69	-> 74
    //   #72	-> 80
    //   #73	-> 84
    //   #74	-> 92
    //   #76	-> 97
    //   #52	-> 102
    // Exception table:
    //   from	to	target	type
    //   3	9	102	finally
    //   9	18	102	finally
    //   23	31	102	finally
    //   37	45	102	finally
    //   50	59	102	finally
    //   59	74	102	finally
    //   74	80	102	finally
    //   84	92	102	finally
  }
  
  public static boolean supportUxIcon(PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, String paramString) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: iconst_0
    //   4: istore_3
    //   5: aload_1
    //   6: ifnonnull -> 27
    //   9: aload_2
    //   10: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   13: istore #4
    //   15: iload #4
    //   17: ifne -> 31
    //   20: goto -> 27
    //   23: astore_0
    //   24: goto -> 406
    //   27: aload_0
    //   28: ifnonnull -> 36
    //   31: ldc android/app/OplusUXIconLoadHelper
    //   33: monitorexit
    //   34: iconst_0
    //   35: ireturn
    //   36: iconst_0
    //   37: istore #4
    //   39: iconst_0
    //   40: istore #5
    //   42: aload_1
    //   43: ifnonnull -> 60
    //   46: aload_0
    //   47: aload_2
    //   48: iconst_0
    //   49: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   52: astore_1
    //   53: goto -> 60
    //   56: astore_0
    //   57: goto -> 355
    //   60: aload_1
    //   61: ifnonnull -> 69
    //   64: ldc android/app/OplusUXIconLoadHelper
    //   66: monitorexit
    //   67: iconst_0
    //   68: ireturn
    //   69: iload #5
    //   71: istore #4
    //   73: aload_0
    //   74: checkcast android/app/OplusBaseApplicationPackageManager
    //   77: astore_0
    //   78: iload #5
    //   80: istore #4
    //   82: aload_0
    //   83: aload_1
    //   84: invokevirtual getOplusBaseResourcesForThemeHelper : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/OplusBaseResources;
    //   87: astore_0
    //   88: iload #5
    //   90: istore #4
    //   92: ldc 'system'
    //   94: aload_1
    //   95: getfield packageName : Ljava/lang/String;
    //   98: invokevirtual equals : (Ljava/lang/Object;)Z
    //   101: ifeq -> 119
    //   104: iload #5
    //   106: istore #4
    //   108: aload_0
    //   109: invokevirtual getConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   112: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   115: astore_0
    //   116: goto -> 134
    //   119: iload #5
    //   121: istore #4
    //   123: aload_0
    //   124: invokevirtual getOplusBaseResImpl : ()Landroid/content/res/OplusBaseResourcesImpl;
    //   127: invokevirtual getSystemConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   130: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   133: astore_0
    //   134: aload_0
    //   135: ifnonnull -> 144
    //   138: iconst_0
    //   139: istore #6
    //   141: goto -> 154
    //   144: iload #5
    //   146: istore #4
    //   148: aload_0
    //   149: getfield mUserId : I
    //   152: istore #6
    //   154: ldc 'persist.sys.themeflag'
    //   156: astore_1
    //   157: iload #6
    //   159: ifle -> 220
    //   162: iload #5
    //   164: istore #4
    //   166: new java/lang/StringBuilder
    //   169: astore_1
    //   170: iload #5
    //   172: istore #4
    //   174: aload_1
    //   175: invokespecial <init> : ()V
    //   178: iload #5
    //   180: istore #4
    //   182: aload_1
    //   183: ldc 'persist.sys.themeflag'
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: iload #5
    //   191: istore #4
    //   193: aload_1
    //   194: ldc '.'
    //   196: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   199: pop
    //   200: iload #5
    //   202: istore #4
    //   204: aload_1
    //   205: iload #6
    //   207: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: iload #5
    //   213: istore #4
    //   215: aload_1
    //   216: invokevirtual toString : ()Ljava/lang/String;
    //   219: astore_1
    //   220: aload_0
    //   221: ifnonnull -> 238
    //   224: iload #5
    //   226: istore #4
    //   228: aload_1
    //   229: lconst_0
    //   230: invokestatic getLong : (Ljava/lang/String;J)J
    //   233: lstore #7
    //   235: goto -> 248
    //   238: iload #5
    //   240: istore #4
    //   242: aload_0
    //   243: getfield mThemeChangedFlags : J
    //   246: lstore #7
    //   248: iload #5
    //   250: istore #4
    //   252: lload #7
    //   254: invokestatic valueOf : (J)Ljava/lang/Long;
    //   257: astore_0
    //   258: iload #5
    //   260: istore #4
    //   262: aload_0
    //   263: invokevirtual longValue : ()J
    //   266: ldc2_w 16
    //   269: land
    //   270: lconst_0
    //   271: lcmp
    //   272: ifne -> 277
    //   275: iconst_1
    //   276: istore_3
    //   277: iload_3
    //   278: istore #4
    //   280: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   283: ifeq -> 351
    //   286: iload_3
    //   287: istore #4
    //   289: new java/lang/StringBuilder
    //   292: astore_1
    //   293: iload_3
    //   294: istore #4
    //   296: aload_1
    //   297: invokespecial <init> : ()V
    //   300: iload_3
    //   301: istore #4
    //   303: aload_1
    //   304: ldc 'supportUxIcon themeFlag =:'
    //   306: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   309: pop
    //   310: iload_3
    //   311: istore #4
    //   313: aload_1
    //   314: aload_0
    //   315: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   318: pop
    //   319: iload_3
    //   320: istore #4
    //   322: aload_1
    //   323: ldc '; supportUxIcon = '
    //   325: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   328: pop
    //   329: iload_3
    //   330: istore #4
    //   332: aload_1
    //   333: iload_3
    //   334: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   337: pop
    //   338: iload_3
    //   339: istore #4
    //   341: ldc 'OplusUXIconLoader'
    //   343: aload_1
    //   344: invokevirtual toString : ()Ljava/lang/String;
    //   347: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   350: pop
    //   351: goto -> 401
    //   354: astore_0
    //   355: iload #4
    //   357: istore_3
    //   358: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   361: ifeq -> 401
    //   364: new java/lang/StringBuilder
    //   367: astore_1
    //   368: aload_1
    //   369: invokespecial <init> : ()V
    //   372: aload_1
    //   373: ldc 'supportUxIcon NameNotFoundException =:'
    //   375: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   378: pop
    //   379: aload_1
    //   380: aload_0
    //   381: invokevirtual toString : ()Ljava/lang/String;
    //   384: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   387: pop
    //   388: ldc 'OplusUXIconLoader'
    //   390: aload_1
    //   391: invokevirtual toString : ()Ljava/lang/String;
    //   394: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   397: pop
    //   398: iload #4
    //   400: istore_3
    //   401: ldc android/app/OplusUXIconLoadHelper
    //   403: monitorexit
    //   404: iload_3
    //   405: ireturn
    //   406: ldc android/app/OplusUXIconLoadHelper
    //   408: monitorexit
    //   409: aload_0
    //   410: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #81	-> 3
    //   #80	-> 23
    //   #81	-> 27
    //   #82	-> 36
    //   #84	-> 42
    //   #85	-> 46
    //   #109	-> 56
    //   #84	-> 60
    //   #87	-> 60
    //   #88	-> 64
    //   #90	-> 69
    //   #91	-> 78
    //   #92	-> 88
    //   #93	-> 88
    //   #94	-> 104
    //   #96	-> 119
    //   #98	-> 134
    //   #99	-> 154
    //   #100	-> 157
    //   #101	-> 162
    //   #102	-> 189
    //   #104	-> 220
    //   #105	-> 258
    //   #106	-> 277
    //   #107	-> 286
    //   #113	-> 351
    //   #109	-> 354
    //   #110	-> 355
    //   #111	-> 364
    //   #114	-> 401
    //   #80	-> 406
    // Exception table:
    //   from	to	target	type
    //   9	15	23	finally
    //   46	53	56	android/content/pm/PackageManager$NameNotFoundException
    //   46	53	23	finally
    //   73	78	354	android/content/pm/PackageManager$NameNotFoundException
    //   73	78	23	finally
    //   82	88	354	android/content/pm/PackageManager$NameNotFoundException
    //   82	88	23	finally
    //   92	104	354	android/content/pm/PackageManager$NameNotFoundException
    //   92	104	23	finally
    //   108	116	354	android/content/pm/PackageManager$NameNotFoundException
    //   108	116	23	finally
    //   123	134	354	android/content/pm/PackageManager$NameNotFoundException
    //   123	134	23	finally
    //   148	154	354	android/content/pm/PackageManager$NameNotFoundException
    //   148	154	23	finally
    //   166	170	354	android/content/pm/PackageManager$NameNotFoundException
    //   166	170	23	finally
    //   174	178	354	android/content/pm/PackageManager$NameNotFoundException
    //   174	178	23	finally
    //   182	189	354	android/content/pm/PackageManager$NameNotFoundException
    //   182	189	23	finally
    //   193	200	354	android/content/pm/PackageManager$NameNotFoundException
    //   193	200	23	finally
    //   204	211	354	android/content/pm/PackageManager$NameNotFoundException
    //   204	211	23	finally
    //   215	220	354	android/content/pm/PackageManager$NameNotFoundException
    //   215	220	23	finally
    //   228	235	354	android/content/pm/PackageManager$NameNotFoundException
    //   228	235	23	finally
    //   242	248	354	android/content/pm/PackageManager$NameNotFoundException
    //   242	248	23	finally
    //   252	258	354	android/content/pm/PackageManager$NameNotFoundException
    //   252	258	23	finally
    //   262	275	354	android/content/pm/PackageManager$NameNotFoundException
    //   262	275	23	finally
    //   280	286	354	android/content/pm/PackageManager$NameNotFoundException
    //   280	286	23	finally
    //   289	293	354	android/content/pm/PackageManager$NameNotFoundException
    //   289	293	23	finally
    //   296	300	354	android/content/pm/PackageManager$NameNotFoundException
    //   296	300	23	finally
    //   303	310	354	android/content/pm/PackageManager$NameNotFoundException
    //   303	310	23	finally
    //   313	319	354	android/content/pm/PackageManager$NameNotFoundException
    //   313	319	23	finally
    //   322	329	354	android/content/pm/PackageManager$NameNotFoundException
    //   322	329	23	finally
    //   332	338	354	android/content/pm/PackageManager$NameNotFoundException
    //   332	338	23	finally
    //   341	351	354	android/content/pm/PackageManager$NameNotFoundException
    //   341	351	23	finally
    //   358	364	23	finally
    //   364	398	23	finally
  }
  
  public static Drawable getUxIconDrawable(Resources paramResources, OplusBaseResources paramOplusBaseResources, Drawable paramDrawable, boolean paramBoolean) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   6: aload_0
    //   7: aload_1
    //   8: aload_2
    //   9: iload_3
    //   10: invokevirtual getUxIconDrawable : (Landroid/content/res/Resources;Landroid/content/res/OplusBaseResources;Landroid/graphics/drawable/Drawable;Z)Landroid/graphics/drawable/Drawable;
    //   13: astore_0
    //   14: ldc android/app/OplusUXIconLoadHelper
    //   16: monitorexit
    //   17: aload_0
    //   18: areturn
    //   19: astore_0
    //   20: ldc android/app/OplusUXIconLoadHelper
    //   22: monitorexit
    //   23: aload_0
    //   24: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #118	-> 3
    //   #118	-> 19
    // Exception table:
    //   from	to	target	type
    //   3	14	19	finally
  }
  
  public static void updateExtraConfig(int paramInt) {
    if ((Integer.MIN_VALUE & paramInt) != 0) {
      sIconLoader.updateExtraConfig();
    } else if ((paramInt & 0x200) != 0) {
      sIconLoader.updateDarkModeConfig();
    } 
  }
  
  public static boolean supportUxOnline(PackageManager paramPackageManager, String paramString) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: getstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   6: istore_2
    //   7: iconst_0
    //   8: istore_3
    //   9: iload_2
    //   10: iconst_m1
    //   11: if_icmpne -> 330
    //   14: aload_1
    //   15: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   18: ifeq -> 25
    //   21: iconst_0
    //   22: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   25: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   28: ifeq -> 67
    //   31: new java/lang/StringBuilder
    //   34: astore #4
    //   36: aload #4
    //   38: invokespecial <init> : ()V
    //   41: aload #4
    //   43: ldc 'supportUxOnline sourcePackageName:'
    //   45: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   48: pop
    //   49: aload #4
    //   51: aload_1
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: ldc 'OplusUXIconLoader'
    //   58: aload #4
    //   60: invokevirtual toString : ()Ljava/lang/String;
    //   63: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   66: pop
    //   67: getstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   70: iconst_m1
    //   71: if_icmpne -> 85
    //   74: aload_1
    //   75: invokestatic isSystemApp : (Ljava/lang/String;)Z
    //   78: ifeq -> 85
    //   81: iconst_1
    //   82: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   85: getstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   88: iconst_m1
    //   89: if_icmpne -> 132
    //   92: aload_1
    //   93: ldc 'com.oppo.'
    //   95: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   98: ifne -> 128
    //   101: aload_1
    //   102: ldc 'com.coloros.'
    //   104: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   107: ifne -> 128
    //   110: aload_1
    //   111: ldc 'com.nearme.'
    //   113: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   116: ifne -> 128
    //   119: aload_1
    //   120: ldc 'com.heytap.'
    //   122: invokevirtual startsWith : (Ljava/lang/String;)Z
    //   125: ifeq -> 132
    //   128: iconst_1
    //   129: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   132: getstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   135: istore_2
    //   136: iload_2
    //   137: iconst_m1
    //   138: if_icmpne -> 330
    //   141: aconst_null
    //   142: astore #4
    //   144: aload_0
    //   145: aload_1
    //   146: sipush #128
    //   149: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   152: astore_0
    //   153: goto -> 194
    //   156: astore_0
    //   157: new java/lang/StringBuilder
    //   160: astore_1
    //   161: aload_1
    //   162: invokespecial <init> : ()V
    //   165: aload_1
    //   166: ldc 'supportUxOnline ex :'
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload_1
    //   173: aload_0
    //   174: invokevirtual getMessage : ()Ljava/lang/String;
    //   177: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: ldc 'OplusUXIconLoader'
    //   183: aload_1
    //   184: invokevirtual toString : ()Ljava/lang/String;
    //   187: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   190: pop
    //   191: aload #4
    //   193: astore_0
    //   194: aload_0
    //   195: ifnull -> 326
    //   198: aload_0
    //   199: getfield metaData : Landroid/os/Bundle;
    //   202: astore_1
    //   203: aload_1
    //   204: ifnull -> 326
    //   207: aload_0
    //   208: getfield metaData : Landroid/os/Bundle;
    //   211: ldc 'com.coloros.support_uxonline'
    //   213: invokevirtual get : (Ljava/lang/String;)Ljava/lang/Object;
    //   216: checkcast java/lang/Boolean
    //   219: astore_1
    //   220: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   223: ifeq -> 258
    //   226: new java/lang/StringBuilder
    //   229: astore_0
    //   230: aload_0
    //   231: invokespecial <init> : ()V
    //   234: aload_0
    //   235: ldc_w 'supportUxOnline :'
    //   238: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: aload_0
    //   243: aload_1
    //   244: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   247: pop
    //   248: ldc 'OplusUXIconLoader'
    //   250: aload_0
    //   251: invokevirtual toString : ()Ljava/lang/String;
    //   254: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   257: pop
    //   258: aload_1
    //   259: ifnull -> 274
    //   262: aload_1
    //   263: invokevirtual booleanValue : ()Z
    //   266: ifeq -> 274
    //   269: iconst_1
    //   270: istore_2
    //   271: goto -> 276
    //   274: iconst_0
    //   275: istore_2
    //   276: iload_2
    //   277: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   280: goto -> 330
    //   283: astore_1
    //   284: iconst_0
    //   285: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   288: new java/lang/StringBuilder
    //   291: astore_0
    //   292: aload_0
    //   293: invokespecial <init> : ()V
    //   296: aload_0
    //   297: ldc_w 'supportUxOnline error:'
    //   300: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   303: pop
    //   304: aload_0
    //   305: aload_1
    //   306: invokevirtual getMessage : ()Ljava/lang/String;
    //   309: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: ldc 'OplusUXIconLoader'
    //   315: aload_0
    //   316: invokevirtual toString : ()Ljava/lang/String;
    //   319: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   322: pop
    //   323: goto -> 330
    //   326: iconst_0
    //   327: putstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   330: getstatic android/app/OplusUXIconLoadHelper.sSupportUxOnline : I
    //   333: istore_2
    //   334: iload_2
    //   335: iconst_1
    //   336: if_icmpne -> 341
    //   339: iconst_1
    //   340: istore_3
    //   341: ldc android/app/OplusUXIconLoadHelper
    //   343: monitorexit
    //   344: iload_3
    //   345: ireturn
    //   346: astore_0
    //   347: ldc android/app/OplusUXIconLoadHelper
    //   349: monitorexit
    //   350: aload_0
    //   351: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #130	-> 3
    //   #131	-> 14
    //   #132	-> 21
    //   #135	-> 25
    //   #136	-> 31
    //   #139	-> 67
    //   #140	-> 74
    //   #141	-> 81
    //   #144	-> 85
    //   #145	-> 92
    //   #146	-> 101
    //   #147	-> 110
    //   #148	-> 119
    //   #149	-> 128
    //   #152	-> 132
    //   #153	-> 141
    //   #155	-> 144
    //   #158	-> 153
    //   #156	-> 156
    //   #157	-> 157
    //   #160	-> 194
    //   #162	-> 207
    //   #163	-> 220
    //   #164	-> 226
    //   #166	-> 258
    //   #170	-> 280
    //   #167	-> 283
    //   #168	-> 284
    //   #169	-> 288
    //   #170	-> 323
    //   #172	-> 326
    //   #176	-> 330
    //   #129	-> 346
    // Exception table:
    //   from	to	target	type
    //   3	7	346	finally
    //   14	21	346	finally
    //   21	25	346	finally
    //   25	31	346	finally
    //   31	67	346	finally
    //   67	74	346	finally
    //   74	81	346	finally
    //   81	85	346	finally
    //   85	92	346	finally
    //   92	101	346	finally
    //   101	110	346	finally
    //   110	119	346	finally
    //   119	128	346	finally
    //   128	132	346	finally
    //   132	136	346	finally
    //   144	153	156	android/content/pm/PackageManager$NameNotFoundException
    //   144	153	346	finally
    //   157	191	346	finally
    //   198	203	346	finally
    //   207	220	283	java/lang/Exception
    //   207	220	346	finally
    //   220	226	283	java/lang/Exception
    //   220	226	346	finally
    //   226	258	283	java/lang/Exception
    //   226	258	346	finally
    //   262	269	283	java/lang/Exception
    //   262	269	346	finally
    //   276	280	283	java/lang/Exception
    //   276	280	346	finally
    //   284	288	346	finally
    //   288	323	346	finally
    //   326	330	346	finally
    //   330	334	346	finally
  }
  
  public static Drawable loadOverlayResolverDrawable(PackageManager paramPackageManager, String paramString1, int paramInt, ApplicationInfo paramApplicationInfo, String paramString2) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   6: astore #5
    //   8: new android/app/OplusResolverUxIconDrawableImpl
    //   11: astore #6
    //   13: aload #6
    //   15: aload #4
    //   17: invokespecial <init> : (Ljava/lang/String;)V
    //   20: aload #5
    //   22: aload #6
    //   24: putfield mOplusUxIconDrawableManager : Landroid/app/IOplusResolverUxIconDrawableManager;
    //   27: aload_0
    //   28: aload_1
    //   29: aconst_null
    //   30: iload_2
    //   31: aload_3
    //   32: iconst_1
    //   33: invokestatic getDrawable : (Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Z)Landroid/graphics/drawable/Drawable;
    //   36: astore_0
    //   37: ldc android/app/OplusUXIconLoadHelper
    //   39: monitorexit
    //   40: aload_0
    //   41: areturn
    //   42: astore_0
    //   43: ldc android/app/OplusUXIconLoadHelper
    //   45: monitorexit
    //   46: aload_0
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #184	-> 3
    //   #185	-> 27
    //   #183	-> 42
    // Exception table:
    //   from	to	target	type
    //   3	27	42	finally
    //   27	37	42	finally
  }
  
  public static void setDarkFilterToDrawable(Drawable paramDrawable) {
    // Byte code:
    //   0: ldc android/app/OplusUXIconLoadHelper
    //   2: monitorenter
    //   3: getstatic android/app/OplusUXIconLoadHelper.sIconLoader : Landroid/app/OplusUXIconLoader;
    //   6: aload_0
    //   7: invokevirtual setDarkFilterToDrawable : (Landroid/graphics/drawable/Drawable;)V
    //   10: ldc android/app/OplusUXIconLoadHelper
    //   12: monitorexit
    //   13: return
    //   14: astore_0
    //   15: ldc android/app/OplusUXIconLoadHelper
    //   17: monitorexit
    //   18: aload_0
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 3
    //   #190	-> 10
    //   #188	-> 14
    // Exception table:
    //   from	to	target	type
    //   3	10	14	finally
  }
}
