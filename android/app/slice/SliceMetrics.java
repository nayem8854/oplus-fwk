package android.app.slice;

import android.content.Context;
import android.metrics.LogMaker;
import android.net.Uri;
import com.android.internal.logging.MetricsLogger;

public class SliceMetrics {
  private static final String TAG = "SliceMetrics";
  
  private LogMaker mLogMaker;
  
  private MetricsLogger mMetricsLogger = new MetricsLogger();
  
  public SliceMetrics(Context paramContext, Uri paramUri) {
    LogMaker logMaker = new LogMaker(0);
    logMaker.addTaggedData(1402, paramUri.getAuthority());
    this.mLogMaker.addTaggedData(1403, paramUri.getPath());
  }
  
  public void logVisible() {
    synchronized (this.mLogMaker) {
      LogMaker logMaker = this.mLogMaker.setCategory(1401);
      logMaker.setType(1);
      this.mMetricsLogger.write(this.mLogMaker);
      return;
    } 
  }
  
  public void logHidden() {
    synchronized (this.mLogMaker) {
      LogMaker logMaker = this.mLogMaker.setCategory(1401);
      logMaker.setType(2);
      this.mMetricsLogger.write(this.mLogMaker);
      return;
    } 
  }
  
  public void logTouch(int paramInt, Uri paramUri) {
    synchronized (this.mLogMaker) {
      LogMaker logMaker = this.mLogMaker.setCategory(1401);
      logMaker = logMaker.setType(4);
      logMaker = logMaker.addTaggedData(1404, paramUri.getAuthority());
      logMaker.addTaggedData(1405, paramUri.getPath());
      this.mMetricsLogger.write(this.mLogMaker);
      return;
    } 
  }
}
