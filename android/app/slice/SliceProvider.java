package android.app.slice;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.database.Cursor;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Handler;
import android.os.Parcelable;
import android.os.Process;
import android.os.StrictMode;
import android.util.ArraySet;
import android.util.Log;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public abstract class SliceProvider extends ContentProvider {
  private static final boolean DEBUG = false;
  
  public static final String EXTRA_BIND_URI = "slice_uri";
  
  public static final String EXTRA_INTENT = "slice_intent";
  
  public static final String EXTRA_PKG = "pkg";
  
  public static final String EXTRA_RESULT = "result";
  
  public static final String EXTRA_SLICE = "slice";
  
  public static final String EXTRA_SLICE_DESCENDANTS = "slice_descendants";
  
  public static final String EXTRA_SUPPORTED_SPECS = "supported_specs";
  
  public static final String METHOD_GET_DESCENDANTS = "get_descendants";
  
  public static final String METHOD_GET_PERMISSIONS = "get_permissions";
  
  public static final String METHOD_MAP_INTENT = "map_slice";
  
  public static final String METHOD_MAP_ONLY_INTENT = "map_only";
  
  public static final String METHOD_PIN = "pin";
  
  public static final String METHOD_SLICE = "bind_slice";
  
  public static final String METHOD_UNPIN = "unpin";
  
  private static final long SLICE_BIND_ANR = 2000L;
  
  public static final String SLICE_TYPE = "vnd.android.slice";
  
  private static final String TAG = "SliceProvider";
  
  public SliceProvider(String... paramVarArgs) {
    this.mAutoGrantPermissions = paramVarArgs;
  }
  
  public SliceProvider() {
    this.mAutoGrantPermissions = new String[0];
  }
  
  public void attachInfo(Context paramContext, ProviderInfo paramProviderInfo) {
    super.attachInfo(paramContext, paramProviderInfo);
    this.mSliceManager = paramContext.<SliceManager>getSystemService(SliceManager.class);
  }
  
  public Slice onBindSlice(Uri paramUri, Set<SliceSpec> paramSet) {
    return onBindSlice(paramUri, new ArrayList<>(paramSet));
  }
  
  @Deprecated
  public Slice onBindSlice(Uri paramUri, List<SliceSpec> paramList) {
    return null;
  }
  
  public void onSlicePinned(Uri paramUri) {}
  
  public void onSliceUnpinned(Uri paramUri) {}
  
  public Collection<Uri> onGetSliceDescendants(Uri paramUri) {
    return Collections.emptyList();
  }
  
  public Uri onMapIntentToUri(Intent paramIntent) {
    throw new UnsupportedOperationException("This provider has not implemented intent to uri mapping");
  }
  
  public PendingIntent onCreatePermissionRequest(Uri paramUri) {
    return createPermissionIntent(getContext(), paramUri, getCallingPackage());
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString) {
    return 0;
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString) {
    return 0;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    return null;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal) {
    return null;
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString, Bundle paramBundle, CancellationSignal paramCancellationSignal) {
    return null;
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues) {
    return null;
  }
  
  public final String getType(Uri paramUri) {
    return "vnd.android.slice";
  }
  
  public Bundle call(String paramString1, String paramString2, Bundle paramBundle) {
    Slice slice;
    Uri uri;
    Bundle bundle;
    ArrayList<Uri> arrayList;
    String str;
    if (paramString1.equals("bind_slice")) {
      uri = (Uri)paramBundle.getParcelable("slice_uri");
      uri = getUriWithoutUserId(validateIncomingUriOrNull(uri));
      ArrayList<SliceSpec> arrayList1 = paramBundle.getParcelableArrayList("supported_specs");
      str = getCallingPackage();
      int i = Binder.getCallingUid();
      int j = Binder.getCallingPid();
      slice = handleBindSlice(uri, arrayList1, str, i, j);
      Bundle bundle1 = new Bundle();
      bundle1.putParcelable("slice", slice);
      return bundle1;
    } 
    if (slice.equals("map_slice")) {
      Intent intent = (Intent)str.getParcelable("slice_intent");
      if (intent == null)
        return null; 
      uri = validateIncomingUriOrNull(onMapIntentToUri(intent));
      ArrayList<SliceSpec> arrayList1 = str.getParcelableArrayList("supported_specs");
      Bundle bundle1 = new Bundle();
      if (uri != null) {
        str = getCallingPackage();
        int i = Binder.getCallingUid(), j = Binder.getCallingPid();
        slice = handleBindSlice(uri, arrayList1, str, i, j);
        bundle1.putParcelable("slice", slice);
      } else {
        bundle1.putParcelable("slice", null);
      } 
      return bundle1;
    } 
    if (slice.equals("map_only")) {
      Intent intent = (Intent)str.getParcelable("slice_intent");
      if (intent == null)
        return null; 
      uri = validateIncomingUriOrNull(onMapIntentToUri(intent));
      Bundle bundle1 = new Bundle();
      bundle1.putParcelable("slice", (Parcelable)uri);
      return bundle1;
    } 
    if (uri.equals("pin")) {
      Uri uri1 = (Uri)str.getParcelable("slice_uri");
      uri1 = getUriWithoutUserId(validateIncomingUriOrNull(uri1));
      if (Binder.getCallingUid() == 1000) {
        handlePinSlice(uri1);
      } else {
        throw new SecurityException("Only the system can pin/unpin slices");
      } 
    } else if (uri.equals("unpin")) {
      Uri uri1 = (Uri)str.getParcelable("slice_uri");
      uri1 = getUriWithoutUserId(validateIncomingUriOrNull(uri1));
      if (Binder.getCallingUid() == 1000) {
        handleUnpinSlice(uri1);
      } else {
        throw new SecurityException("Only the system can pin/unpin slices");
      } 
    } else {
      if (uri.equals("get_descendants")) {
        uri = validateIncomingUriOrNull((Uri)str.getParcelable("slice_uri"));
        Uri uri1 = getUriWithoutUserId(uri);
        bundle = new Bundle();
        arrayList = new ArrayList<>(handleGetDescendants(uri1));
        bundle.putParcelableArrayList("slice_descendants", arrayList);
        return bundle;
      } 
      if (bundle.equals("get_permissions")) {
        if (Binder.getCallingUid() == 1000) {
          bundle = new Bundle();
          bundle.putStringArray("result", this.mAutoGrantPermissions);
          return bundle;
        } 
        throw new SecurityException("Only the system can get permissions");
      } 
    } 
    return super.call((String)bundle, (String)arrayList, (Bundle)str);
  }
  
  private Uri validateIncomingUriOrNull(Uri paramUri) {
    if (paramUri == null) {
      paramUri = null;
    } else {
      paramUri = validateIncomingUri(paramUri);
    } 
    return paramUri;
  }
  
  private Collection<Uri> handleGetDescendants(Uri paramUri) {
    this.mCallback = "onGetSliceDescendants";
    return onGetSliceDescendants(paramUri);
  }
  
  private void handlePinSlice(Uri paramUri) {
    this.mCallback = "onSlicePinned";
    Handler.getMain().postDelayed(this.mAnr, 2000L);
    try {
      onSlicePinned(paramUri);
      return;
    } finally {
      Handler.getMain().removeCallbacks(this.mAnr);
    } 
  }
  
  private void handleUnpinSlice(Uri paramUri) {
    this.mCallback = "onSliceUnpinned";
    Handler.getMain().postDelayed(this.mAnr, 2000L);
    try {
      onSliceUnpinned(paramUri);
      return;
    } finally {
      Handler.getMain().removeCallbacks(this.mAnr);
    } 
  }
  
  private Slice handleBindSlice(Uri paramUri, List<SliceSpec> paramList, String paramString, int paramInt1, int paramInt2) {
    if (paramString == null)
      paramString = getContext().getPackageManager().getNameForUid(paramInt1); 
    try {
      this.mSliceManager.enforceSlicePermission(paramUri, paramString, paramInt2, paramInt1, this.mAutoGrantPermissions);
      this.mCallback = "onBindSlice";
      Handler.getMain().postDelayed(this.mAnr, 2000L);
      try {
        return onBindSliceStrict(paramUri, paramList);
      } finally {
        Handler.getMain().removeCallbacks(this.mAnr);
      } 
    } catch (SecurityException securityException) {
      return createPermissionSlice(getContext(), paramUri, paramString);
    } 
  }
  
  public Slice createPermissionSlice(Context paramContext, Uri paramUri, String paramString) {
    this.mCallback = "onCreatePermissionRequest";
    Handler.getMain().postDelayed(this.mAnr, 2000L);
    try {
      PendingIntent pendingIntent = onCreatePermissionRequest(paramUri);
      Handler.getMain().removeCallbacks(this.mAnr);
      Slice.Builder builder4 = new Slice.Builder(paramUri);
      Slice.Builder builder5 = new Slice.Builder(builder4);
      Icon icon2 = Icon.createWithResource(paramContext, 17302790);
      List<?> list4 = Collections.emptyList();
      Slice.Builder builder7 = builder5.addIcon(icon2, null, (List)list4);
      Slice.Builder builder8 = builder7.addHints(Arrays.asList(new String[] { "title", "shortcut" }));
      builder7 = new Slice.Builder(builder4);
      Slice.Builder builder3 = builder8.addAction(pendingIntent, builder7.build(), null);
      TypedValue typedValue = new TypedValue();
      ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(paramContext, 16974123);
      contextThemeWrapper.getTheme().resolveAttribute(16843829, typedValue, true);
      int i = typedValue.data;
      Slice.Builder builder6 = new Slice.Builder(paramUri.buildUpon().appendPath("permission").build());
      Icon icon1 = Icon.createWithResource(paramContext, 17302305);
      List<?> list3 = Collections.emptyList();
      Slice.Builder builder2 = builder6.addIcon(icon1, null, (List)list3);
      CharSequence charSequence = getPermissionString(paramContext, paramString);
      List<?> list1 = Collections.emptyList();
      Slice.Builder builder1 = builder2.addText(charSequence, null, (List)list1);
      List<?> list2 = Collections.emptyList();
      builder1 = builder1.addInt(i, "color", (List)list2);
      builder1 = builder1.addSubSlice(builder3.build(), null);
      Slice slice = builder1.build();
      return builder4.addHints(Arrays.asList(new String[] { "permission_request" })).build();
    } finally {
      Handler.getMain().removeCallbacks(this.mAnr);
    } 
  }
  
  public static PendingIntent createPermissionIntent(Context paramContext, Uri paramUri, String paramString) {
    Intent intent = new Intent("com.android.intent.action.REQUEST_SLICE_PERMISSION");
    intent.setComponent(ComponentName.unflattenFromString(paramContext.getResources().getString(17039965)));
    intent.putExtra("slice_uri", (Parcelable)paramUri);
    intent.putExtra("pkg", paramString);
    Uri.Builder builder = paramUri.buildUpon().appendQueryParameter("package", paramString);
    Uri uri = builder.build();
    intent.setData(uri);
    return PendingIntent.getActivity(paramContext, 0, intent, 0);
  }
  
  public static CharSequence getPermissionString(Context paramContext, String paramString) {
    PackageManager packageManager = paramContext.getPackageManager();
    try {
      CharSequence charSequence1 = packageManager.getApplicationInfo(paramString, 0).loadLabel(packageManager);
      CharSequence charSequence2 = paramContext.getApplicationInfo().loadLabel(packageManager);
      return paramContext.getString(17041306, new Object[] { charSequence1, charSequence2 });
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      throw new RuntimeException("Unknown calling app", nameNotFoundException);
    } 
  }
  
  private Slice onBindSliceStrict(Uri paramUri, List<SliceSpec> paramList) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.getThreadPolicy();
    try {
      StrictMode.ThreadPolicy.Builder builder = new StrictMode.ThreadPolicy.Builder();
      this();
      builder = builder.detectAll();
      builder = builder.penaltyDeath();
      StrictMode.ThreadPolicy threadPolicy1 = builder.build();
      StrictMode.setThreadPolicy(threadPolicy1);
      ArraySet arraySet = new ArraySet();
      this(paramList);
      return onBindSlice(paramUri, (Set<SliceSpec>)arraySet);
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private final Runnable mAnr = new _$$Lambda$SliceProvider$bIgM5f4PsMvz_YYWEeFTjvTqevw(this);
  
  private final String[] mAutoGrantPermissions;
  
  private String mCallback;
  
  private SliceManager mSliceManager;
}
