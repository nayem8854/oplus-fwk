package android.app.slice;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;

public interface ISliceManager extends IInterface {
  void applyRestore(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  int checkSlicePermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, String[] paramArrayOfString) throws RemoteException;
  
  byte[] getBackupPayload(int paramInt) throws RemoteException;
  
  Uri[] getPinnedSlices(String paramString) throws RemoteException;
  
  SliceSpec[] getPinnedSpecs(Uri paramUri, String paramString) throws RemoteException;
  
  void grantPermissionFromUser(Uri paramUri, String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  void grantSlicePermission(String paramString1, String paramString2, Uri paramUri) throws RemoteException;
  
  boolean hasSliceAccess(String paramString) throws RemoteException;
  
  void pinSlice(String paramString, Uri paramUri, SliceSpec[] paramArrayOfSliceSpec, IBinder paramIBinder) throws RemoteException;
  
  void revokeSlicePermission(String paramString1, String paramString2, Uri paramUri) throws RemoteException;
  
  void unpinSlice(String paramString, Uri paramUri, IBinder paramIBinder) throws RemoteException;
  
  class Default implements ISliceManager {
    public void pinSlice(String param1String, Uri param1Uri, SliceSpec[] param1ArrayOfSliceSpec, IBinder param1IBinder) throws RemoteException {}
    
    public void unpinSlice(String param1String, Uri param1Uri, IBinder param1IBinder) throws RemoteException {}
    
    public boolean hasSliceAccess(String param1String) throws RemoteException {
      return false;
    }
    
    public SliceSpec[] getPinnedSpecs(Uri param1Uri, String param1String) throws RemoteException {
      return null;
    }
    
    public Uri[] getPinnedSlices(String param1String) throws RemoteException {
      return null;
    }
    
    public byte[] getBackupPayload(int param1Int) throws RemoteException {
      return null;
    }
    
    public void applyRestore(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public void grantSlicePermission(String param1String1, String param1String2, Uri param1Uri) throws RemoteException {}
    
    public void revokeSlicePermission(String param1String1, String param1String2, Uri param1Uri) throws RemoteException {}
    
    public int checkSlicePermission(Uri param1Uri, String param1String1, String param1String2, int param1Int1, int param1Int2, String[] param1ArrayOfString) throws RemoteException {
      return 0;
    }
    
    public void grantPermissionFromUser(Uri param1Uri, String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISliceManager {
    private static final String DESCRIPTOR = "android.app.slice.ISliceManager";
    
    static final int TRANSACTION_applyRestore = 7;
    
    static final int TRANSACTION_checkSlicePermission = 10;
    
    static final int TRANSACTION_getBackupPayload = 6;
    
    static final int TRANSACTION_getPinnedSlices = 5;
    
    static final int TRANSACTION_getPinnedSpecs = 4;
    
    static final int TRANSACTION_grantPermissionFromUser = 11;
    
    static final int TRANSACTION_grantSlicePermission = 8;
    
    static final int TRANSACTION_hasSliceAccess = 3;
    
    static final int TRANSACTION_pinSlice = 1;
    
    static final int TRANSACTION_revokeSlicePermission = 9;
    
    static final int TRANSACTION_unpinSlice = 2;
    
    public Stub() {
      attachInterface(this, "android.app.slice.ISliceManager");
    }
    
    public static ISliceManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.slice.ISliceManager");
      if (iInterface != null && iInterface instanceof ISliceManager)
        return (ISliceManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "grantPermissionFromUser";
        case 10:
          return "checkSlicePermission";
        case 9:
          return "revokeSlicePermission";
        case 8:
          return "grantSlicePermission";
        case 7:
          return "applyRestore";
        case 6:
          return "getBackupPayload";
        case 5:
          return "getPinnedSlices";
        case 4:
          return "getPinnedSpecs";
        case 3:
          return "hasSliceAccess";
        case 2:
          return "unpinSlice";
        case 1:
          break;
      } 
      return "pinSlice";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String[] arrayOfString;
        byte[] arrayOfByte1;
        String str3;
        Uri[] arrayOfUri;
        String str2;
        SliceSpec[] arrayOfSliceSpec1;
        String str1;
        Uri uri;
        String str4;
        byte[] arrayOfByte2;
        String str5;
        boolean bool1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.app.slice.ISliceManager");
            if (param1Parcel1.readInt() != 0) {
              uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            str5 = param1Parcel1.readString();
            str6 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            grantPermissionFromUser(uri, str5, str6, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.app.slice.ISliceManager");
            if (param1Parcel1.readInt() != 0) {
              uri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            str6 = param1Parcel1.readString();
            str5 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            arrayOfString = param1Parcel1.createStringArray();
            param1Int1 = checkSlicePermission(uri, str6, str5, param1Int2, param1Int1, arrayOfString);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 9:
            arrayOfString.enforceInterface("android.app.slice.ISliceManager");
            str6 = arrayOfString.readString();
            str4 = arrayOfString.readString();
            if (arrayOfString.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            revokeSlicePermission(str6, str4, (Uri)arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfString.enforceInterface("android.app.slice.ISliceManager");
            str4 = arrayOfString.readString();
            str6 = arrayOfString.readString();
            if (arrayOfString.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            grantSlicePermission(str4, str6, (Uri)arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfString.enforceInterface("android.app.slice.ISliceManager");
            arrayOfByte2 = arrayOfString.createByteArray();
            param1Int1 = arrayOfString.readInt();
            applyRestore(arrayOfByte2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayOfString.enforceInterface("android.app.slice.ISliceManager");
            param1Int1 = arrayOfString.readInt();
            arrayOfByte1 = getBackupPayload(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 5:
            arrayOfByte1.enforceInterface("android.app.slice.ISliceManager");
            str3 = arrayOfByte1.readString();
            arrayOfUri = getPinnedSlices(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfUri, 1);
            return true;
          case 4:
            arrayOfUri.enforceInterface("android.app.slice.ISliceManager");
            if (arrayOfUri.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfUri);
            } else {
              arrayOfByte2 = null;
            } 
            str2 = arrayOfUri.readString();
            arrayOfSliceSpec1 = getPinnedSpecs((Uri)arrayOfByte2, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfSliceSpec1, 1);
            return true;
          case 3:
            arrayOfSliceSpec1.enforceInterface("android.app.slice.ISliceManager");
            str1 = arrayOfSliceSpec1.readString();
            bool = hasSliceAccess(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 2:
            str1.enforceInterface("android.app.slice.ISliceManager");
            str6 = str1.readString();
            if (str1.readInt() != 0) {
              Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              arrayOfByte2 = null;
            } 
            iBinder = str1.readStrongBinder();
            unpinSlice(str6, (Uri)arrayOfByte2, iBinder);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iBinder.enforceInterface("android.app.slice.ISliceManager");
        String str6 = iBinder.readString();
        if (iBinder.readInt() != 0) {
          Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)iBinder);
        } else {
          arrayOfByte2 = null;
        } 
        SliceSpec[] arrayOfSliceSpec2 = (SliceSpec[])iBinder.createTypedArray(SliceSpec.CREATOR);
        IBinder iBinder = iBinder.readStrongBinder();
        pinSlice(str6, (Uri)arrayOfByte2, arrayOfSliceSpec2, iBinder);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.app.slice.ISliceManager");
      return true;
    }
    
    private static class Proxy implements ISliceManager {
      public static ISliceManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.slice.ISliceManager";
      }
      
      public void pinSlice(String param2String, Uri param2Uri, SliceSpec[] param2ArrayOfSliceSpec, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfSliceSpec, 0);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().pinSlice(param2String, param2Uri, param2ArrayOfSliceSpec, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unpinSlice(String param2String, Uri param2Uri, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().unpinSlice(param2String, param2Uri, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSliceAccess(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ISliceManager.Stub.getDefaultImpl() != null) {
            bool1 = ISliceManager.Stub.getDefaultImpl().hasSliceAccess(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SliceSpec[] getPinnedSpecs(Uri param2Uri, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null)
            return ISliceManager.Stub.getDefaultImpl().getPinnedSpecs(param2Uri, param2String); 
          parcel2.readException();
          return (SliceSpec[])parcel2.createTypedArray(SliceSpec.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Uri[] getPinnedSlices(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null)
            return ISliceManager.Stub.getDefaultImpl().getPinnedSlices(param2String); 
          parcel2.readException();
          return (Uri[])parcel2.createTypedArray(Uri.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getBackupPayload(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null)
            return ISliceManager.Stub.getDefaultImpl().getBackupPayload(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyRestore(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().applyRestore(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantSlicePermission(String param2String1, String param2String2, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().grantSlicePermission(param2String1, param2String2, param2Uri);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeSlicePermission(String param2String1, String param2String2, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().revokeSlicePermission(param2String1, param2String2, param2Uri);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkSlicePermission(Uri param2Uri, String param2String1, String param2String2, int param2Int1, int param2Int2, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  try {
                    parcel1.writeStringArray(param2ArrayOfString);
                    boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
                    if (!bool && ISliceManager.Stub.getDefaultImpl() != null) {
                      param2Int1 = ISliceManager.Stub.getDefaultImpl().checkSlicePermission(param2Uri, param2String1, param2String2, param2Int1, param2Int2, param2ArrayOfString);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } 
                    parcel2.readException();
                    param2Int1 = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Uri;
      }
      
      public void grantPermissionFromUser(Uri param2Uri, String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.slice.ISliceManager");
          boolean bool = true;
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool1 && ISliceManager.Stub.getDefaultImpl() != null) {
            ISliceManager.Stub.getDefaultImpl().grantPermissionFromUser(param2Uri, param2String1, param2String2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISliceManager param1ISliceManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISliceManager != null) {
          Proxy.sDefaultImpl = param1ISliceManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISliceManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
