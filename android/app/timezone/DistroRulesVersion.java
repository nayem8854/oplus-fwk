package android.app.timezone;

import android.os.Parcel;
import android.os.Parcelable;

public final class DistroRulesVersion implements Parcelable {
  public DistroRulesVersion(String paramString, int paramInt) {
    this.mRulesVersion = Utils.validateRulesVersion("rulesVersion", paramString);
    this.mRevision = Utils.validateVersion("revision", paramInt);
  }
  
  public static final Parcelable.Creator<DistroRulesVersion> CREATOR = new Parcelable.Creator<DistroRulesVersion>() {
      public DistroRulesVersion createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        int i = param1Parcel.readInt();
        return new DistroRulesVersion(str, i);
      }
      
      public DistroRulesVersion[] newArray(int param1Int) {
        return new DistroRulesVersion[param1Int];
      }
    };
  
  private final int mRevision;
  
  private final String mRulesVersion;
  
  public String getRulesVersion() {
    return this.mRulesVersion;
  }
  
  public int getRevision() {
    return this.mRevision;
  }
  
  public boolean isOlderThan(DistroRulesVersion paramDistroRulesVersion) {
    int i = this.mRulesVersion.compareTo(paramDistroRulesVersion.mRulesVersion);
    boolean bool = true;
    if (i < 0)
      return true; 
    if (i > 0)
      return false; 
    if (this.mRevision >= paramDistroRulesVersion.mRevision)
      bool = false; 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mRulesVersion);
    paramParcel.writeInt(this.mRevision);
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mRevision != ((DistroRulesVersion)paramObject).mRevision)
      return false; 
    return this.mRulesVersion.equals(((DistroRulesVersion)paramObject).mRulesVersion);
  }
  
  public int hashCode() {
    int i = this.mRulesVersion.hashCode();
    int j = this.mRevision;
    return i * 31 + j;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DistroRulesVersion{mRulesVersion='");
    stringBuilder.append(this.mRulesVersion);
    stringBuilder.append('\'');
    stringBuilder.append(", mRevision='");
    stringBuilder.append(this.mRevision);
    stringBuilder.append('\'');
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public String toDumpString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mRulesVersion);
    stringBuilder.append(",");
    stringBuilder.append(this.mRevision);
    return stringBuilder.toString();
  }
}
