package android.app.timezone;

import android.os.Parcel;
import android.os.Parcelable;

public final class DistroFormatVersion implements Parcelable {
  public DistroFormatVersion(int paramInt1, int paramInt2) {
    this.mMajorVersion = Utils.validateVersion("major", paramInt1);
    this.mMinorVersion = Utils.validateVersion("minor", paramInt2);
  }
  
  public static final Parcelable.Creator<DistroFormatVersion> CREATOR = new Parcelable.Creator<DistroFormatVersion>() {
      public DistroFormatVersion createFromParcel(Parcel param1Parcel) {
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        return new DistroFormatVersion(i, j);
      }
      
      public DistroFormatVersion[] newArray(int param1Int) {
        return new DistroFormatVersion[param1Int];
      }
    };
  
  private final int mMajorVersion;
  
  private final int mMinorVersion;
  
  public int getMajorVersion() {
    return this.mMajorVersion;
  }
  
  public int getMinorVersion() {
    return this.mMinorVersion;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMajorVersion);
    paramParcel.writeInt(this.mMinorVersion);
  }
  
  public boolean supports(DistroFormatVersion paramDistroFormatVersion) {
    boolean bool;
    if (this.mMajorVersion == paramDistroFormatVersion.mMajorVersion && this.mMinorVersion <= paramDistroFormatVersion.mMinorVersion) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mMajorVersion != ((DistroFormatVersion)paramObject).mMajorVersion)
      return false; 
    if (this.mMinorVersion != ((DistroFormatVersion)paramObject).mMinorVersion)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    int i = this.mMajorVersion;
    int j = this.mMinorVersion;
    return i * 31 + j;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DistroFormatVersion{mMajorVersion=");
    stringBuilder.append(this.mMajorVersion);
    stringBuilder.append(", mMinorVersion=");
    stringBuilder.append(this.mMinorVersion);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
