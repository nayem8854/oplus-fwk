package android.app;

import android.content.Intent;
import android.content.pm.IPackageDeleteObserver2;

public class PackageDeleteObserver {
  private final IPackageDeleteObserver2.Stub mBinder = (IPackageDeleteObserver2.Stub)new Object(this);
  
  public IPackageDeleteObserver2 getBinder() {
    return this.mBinder;
  }
  
  public void onUserActionRequired(Intent paramIntent) {}
  
  public void onPackageDeleted(String paramString1, int paramInt, String paramString2) {}
}
