package android.app;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.IAccessibilityServiceClient;
import android.accessibilityservice.IAccessibilityServiceConnection;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.display.DisplayManagerGlobal;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.os.Process;
import android.os.RemoteException;
import android.os.SystemClock;
import android.os.UserHandle;
import android.util.Log;
import android.util.SparseArray;
import android.view.InputEvent;
import android.view.WindowAnimationFrameStats;
import android.view.WindowContentFrameStats;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityInteractionClient;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;
import libcore.io.IoUtils;

public final class UiAutomation {
  private static final String LOG_TAG = UiAutomation.class.getSimpleName();
  
  private final Object mLock = new Object();
  
  private final ArrayList<AccessibilityEvent> mEventQueue = new ArrayList<>();
  
  private int mConnectionId = -1;
  
  private static final int CONNECTION_ID_UNDEFINED = -1;
  
  private static final long CONNECT_TIMEOUT_MILLIS = 5000L;
  
  private static final boolean DEBUG = false;
  
  public static final int FLAG_DONT_SUPPRESS_ACCESSIBILITY_SERVICES = 1;
  
  public static final int ROTATION_FREEZE_0 = 0;
  
  public static final int ROTATION_FREEZE_180 = 2;
  
  public static final int ROTATION_FREEZE_270 = 3;
  
  public static final int ROTATION_FREEZE_90 = 1;
  
  public static final int ROTATION_FREEZE_CURRENT = -1;
  
  public static final int ROTATION_UNFREEZE = -2;
  
  private IAccessibilityServiceClient mClient;
  
  private int mFlags;
  
  private boolean mIsConnecting;
  
  private boolean mIsDestroyed;
  
  private long mLastEventTimeMillis;
  
  private final Handler mLocalCallbackHandler;
  
  private OnAccessibilityEventListener mOnAccessibilityEventListener;
  
  private HandlerThread mRemoteCallbackThread;
  
  private final IUiAutomationConnection mUiAutomationConnection;
  
  private boolean mWaitingForEventDelivery;
  
  public UiAutomation(Looper paramLooper, IUiAutomationConnection paramIUiAutomationConnection) {
    if (paramLooper != null) {
      if (paramIUiAutomationConnection != null) {
        this.mLocalCallbackHandler = new Handler(paramLooper);
        this.mUiAutomationConnection = paramIUiAutomationConnection;
        return;
      } 
      throw new IllegalArgumentException("Connection cannot be null!");
    } 
    throw new IllegalArgumentException("Looper cannot be null!");
  }
  
  public void connect() {
    connect(0);
  }
  
  public void connect(int paramInt) {
    synchronized (this.mLock) {
      throwIfConnectedLocked();
      if (this.mIsConnecting)
        return; 
      this.mIsConnecting = true;
      HandlerThread handlerThread = new HandlerThread();
      this("UiAutomation");
      this.mRemoteCallbackThread = handlerThread;
      handlerThread.start();
      IAccessibilityServiceClientImpl iAccessibilityServiceClientImpl = new IAccessibilityServiceClientImpl();
      this(this, this.mRemoteCallbackThread.getLooper());
      this.mClient = iAccessibilityServiceClientImpl;
      try {
        this.mUiAutomationConnection.connect(iAccessibilityServiceClientImpl, paramInt);
        this.mFlags = paramInt;
        synchronized (this.mLock) {
          long l = SystemClock.uptimeMillis();
          try {
            while (true) {
              boolean bool = isConnectedLocked();
              if (bool)
                return; 
              long l1 = SystemClock.uptimeMillis();
              l1 = 5000L - l1 - l;
              if (l1 > 0L) {
                try {
                  this.mLock.wait(l1);
                } catch (InterruptedException interruptedException) {}
                continue;
              } 
              RuntimeException runtimeException = new RuntimeException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Error while connecting ");
              stringBuilder.append(this);
              this(stringBuilder.toString());
              throw runtimeException;
            } 
          } finally {
            this.mIsConnecting = false;
          } 
        } 
      } catch (RemoteException null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error while connecting ");
        stringBuilder.append(this);
        throw new RuntimeException(stringBuilder.toString(), null);
      } 
    } 
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public void disconnect() {
    synchronized (this.mLock) {
      if (!this.mIsConnecting) {
        throwIfNotConnectedLocked();
        this.mConnectionId = -1;
        try {
          this.mUiAutomationConnection.disconnect();
          this.mRemoteCallbackThread.quit();
          this.mRemoteCallbackThread = null;
          return;
        } catch (RemoteException remoteException) {
          null = new RuntimeException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Error while disconnecting ");
          stringBuilder1.append(this);
          super(stringBuilder1.toString(), (Throwable)remoteException);
          throw null;
        } finally {}
        this.mRemoteCallbackThread.quit();
        this.mRemoteCallbackThread = null;
        throw null;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Cannot call disconnect() while connecting ");
      stringBuilder.append(this);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getConnectionId() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      return this.mConnectionId;
    } 
  }
  
  public boolean isDestroyed() {
    return this.mIsDestroyed;
  }
  
  public void setOnAccessibilityEventListener(OnAccessibilityEventListener paramOnAccessibilityEventListener) {
    synchronized (this.mLock) {
      this.mOnAccessibilityEventListener = paramOnAccessibilityEventListener;
      return;
    } 
  }
  
  public void destroy() {
    disconnect();
    this.mIsDestroyed = true;
  }
  
  public void adoptShellPermissionIdentity() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        this.mUiAutomationConnection.adoptShellPermissionIdentity(Process.myUid(), null);
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error executing adopting shell permission identity!", (Throwable)null);
      } 
      return;
    } 
  }
  
  public void adoptShellPermissionIdentity(String... paramVarArgs) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        this.mUiAutomationConnection.adoptShellPermissionIdentity(Process.myUid(), paramVarArgs);
      } catch (RemoteException remoteException) {
        Log.e(LOG_TAG, "Error executing adopting shell permission identity!", (Throwable)remoteException);
      } 
      return;
    } 
  }
  
  public void dropShellPermissionIdentity() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        this.mUiAutomationConnection.dropShellPermissionIdentity();
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error executing dropping shell permission identity!", (Throwable)null);
      } 
      return;
    } 
  }
  
  public final boolean performGlobalAction(int paramInt) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      AccessibilityInteractionClient.getInstance();
      int i = this.mConnectionId;
      IAccessibilityServiceConnection iAccessibilityServiceConnection = AccessibilityInteractionClient.getConnection(i);
      if (iAccessibilityServiceConnection != null)
        try {
          return iAccessibilityServiceConnection.performGlobalAction(paramInt);
        } catch (RemoteException null) {
          Log.w(LOG_TAG, "Error while calling performGlobalAction", (Throwable)null);
        }  
      return false;
    } 
  }
  
  public AccessibilityNodeInfo findFocus(int paramInt) {
    return AccessibilityInteractionClient.getInstance().findFocus(this.mConnectionId, -2, AccessibilityNodeInfo.ROOT_NODE_ID, paramInt);
  }
  
  public final AccessibilityServiceInfo getServiceInfo() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      AccessibilityInteractionClient.getInstance();
      int i = this.mConnectionId;
      IAccessibilityServiceConnection iAccessibilityServiceConnection = AccessibilityInteractionClient.getConnection(i);
      if (iAccessibilityServiceConnection != null)
        try {
          return iAccessibilityServiceConnection.getServiceInfo();
        } catch (RemoteException null) {
          Log.w(LOG_TAG, "Error while getting AccessibilityServiceInfo", (Throwable)null);
        }  
      return null;
    } 
  }
  
  public final void setServiceInfo(AccessibilityServiceInfo paramAccessibilityServiceInfo) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      AccessibilityInteractionClient.getInstance().clearCache();
      AccessibilityInteractionClient.getInstance();
      int i = this.mConnectionId;
      IAccessibilityServiceConnection iAccessibilityServiceConnection = AccessibilityInteractionClient.getConnection(i);
      if (iAccessibilityServiceConnection != null)
        try {
          iAccessibilityServiceConnection.setServiceInfo(paramAccessibilityServiceInfo);
        } catch (RemoteException remoteException) {
          Log.w(LOG_TAG, "Error while setting AccessibilityServiceInfo", (Throwable)remoteException);
        }  
      return;
    } 
  }
  
  public List<AccessibilityWindowInfo> getWindows() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      int i = this.mConnectionId;
      null = AccessibilityInteractionClient.getInstance();
      return 
        null.getWindows(i);
    } 
  }
  
  public SparseArray<List<AccessibilityWindowInfo>> getWindowsOnAllDisplays() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      int i = this.mConnectionId;
      null = AccessibilityInteractionClient.getInstance();
      return 
        null.getWindowsOnAllDisplays(i);
    } 
  }
  
  public AccessibilityNodeInfo getRootInActiveWindow() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      int i = this.mConnectionId;
      null = AccessibilityInteractionClient.getInstance();
      return 
        null.getRootInActiveWindow(i);
    } 
  }
  
  public boolean injectInputEvent(InputEvent paramInputEvent, boolean paramBoolean) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        return this.mUiAutomationConnection.injectInputEvent(paramInputEvent, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e(LOG_TAG, "Error while injecting input event!", (Throwable)remoteException);
        return false;
      } 
    } 
  }
  
  public void syncInputTransactions() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        this.mUiAutomationConnection.syncInputTransactions();
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error while syncing input transactions!", (Throwable)null);
      } 
      return;
    } 
  }
  
  public boolean setRotation(int paramInt) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      if (paramInt == -2 || paramInt == -1 || paramInt == 0 || paramInt == 1 || paramInt == 2 || paramInt == 3)
        try {
          this.mUiAutomationConnection.setRotation(paramInt);
          return true;
        } catch (RemoteException null) {
          Log.e(LOG_TAG, "Error while setting rotation!", (Throwable)null);
          return false;
        }  
      throw new IllegalArgumentException("Invalid rotation.");
    } 
  }
  
  public AccessibilityEvent executeAndWaitForEvent(Runnable paramRunnable, AccessibilityEventFilter paramAccessibilityEventFilter, long paramLong) throws TimeoutException {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      throwIfNotConnectedLocked();
      this.mEventQueue.clear();
      this.mWaitingForEventDelivery = true;
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      long l1 = SystemClock.uptimeMillis();
      paramRunnable.run();
      object = new ArrayList();
      long l2 = l1;
      try {
        long l = SystemClock.uptimeMillis();
        while (true) {
          l2 = l1;
          ArrayList<AccessibilityEvent> arrayList = new ArrayList();
          l2 = l1;
          this();
          l2 = l1;
          object1 = this.mLock;
          l2 = l1;
          /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          try {
            arrayList.addAll(this.mEventQueue);
            this.mEventQueue.clear();
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            while (true) {
              l2 = l1;
              boolean bool = arrayList.isEmpty();
              if (!bool) {
                try {
                  AccessibilityEvent accessibilityEvent = arrayList.remove(0);
                  l2 = accessibilityEvent.getEventTime();
                  if (l2 < l1)
                    continue; 
                } finally {
                  paramRunnable = null;
                } 
                continue;
              } 
              try {
                l2 = SystemClock.uptimeMillis();
                l2 = paramLong - l2 - l;
                if (l2 > 0L)
                  try {
                  
                  } finally {
                    paramRunnable = null;
                  }  
                TimeoutException timeoutException = new TimeoutException();
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Expected event not received within: ");
                stringBuilder.append(paramLong);
                stringBuilder.append(" ms among: ");
                stringBuilder.append(object);
                this(stringBuilder.toString());
                throw timeoutException;
              } finally {
                paramRunnable = null;
              } 
            } 
          } finally {
            paramRunnable = null;
          } 
          break;
        } 
      } finally {
        paramRunnable = null;
      } 
      int i = object.size();
      for (byte b = 0; b < i; b++)
        ((AccessibilityEvent)object.get(b)).recycle(); 
    } finally {
      paramRunnable = null;
    } 
  }
  
  public void waitForIdle(long paramLong1, long paramLong2) throws TimeoutException {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      long l1 = SystemClock.uptimeMillis();
      long l2 = l1;
      if (this.mLastEventTimeMillis <= 0L) {
        this.mLastEventTimeMillis = l1;
        l2 = l1;
      } 
      while (true) {
        l1 = SystemClock.uptimeMillis();
        if (paramLong2 - l1 - l2 > 0L) {
          long l = this.mLastEventTimeMillis;
          l1 = paramLong1 - l1 - l;
          if (l1 <= 0L)
            return; 
          try {
            this.mLock.wait(l1);
          } catch (InterruptedException interruptedException) {}
          continue;
        } 
        TimeoutException timeoutException = new TimeoutException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("No idle state with idle timeout: ");
        stringBuilder.append(paramLong1);
        stringBuilder.append(" within global timeout: ");
        stringBuilder.append(paramLong2);
        this(stringBuilder.toString());
        throw timeoutException;
      } 
    } 
  }
  
  public Bitmap takeScreenshot() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      DisplayManagerGlobal displayManagerGlobal = DisplayManagerGlobal.getInstance();
      null = displayManagerGlobal.getRealDisplay(0);
      Point point = new Point();
      null.getRealSize(point);
      int i = null.getRotation();
      try {
        IUiAutomationConnection iUiAutomationConnection = this.mUiAutomationConnection;
        null = new Rect();
        super(0, 0, point.x, point.y);
        Bitmap bitmap = iUiAutomationConnection.takeScreenshot((Rect)null, i);
        if (bitmap == null)
          return null; 
        bitmap.setHasAlpha(false);
        return bitmap;
      } catch (RemoteException remoteException) {
        Log.e(LOG_TAG, "Error while taking screnshot!", (Throwable)remoteException);
        return null;
      } 
    } 
  }
  
  public void setRunAsMonkey(boolean paramBoolean) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        ActivityManager.getService().setUserIsMonkey(paramBoolean);
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error while setting run as monkey!", (Throwable)null);
      } 
      return;
    } 
  }
  
  public boolean clearWindowContentFrameStats(int paramInt) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        return this.mUiAutomationConnection.clearWindowContentFrameStats(paramInt);
      } catch (RemoteException remoteException) {
        Log.e(LOG_TAG, "Error clearing window content frame stats!", (Throwable)remoteException);
        return false;
      } 
    } 
  }
  
  public WindowContentFrameStats getWindowContentFrameStats(int paramInt) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        return this.mUiAutomationConnection.getWindowContentFrameStats(paramInt);
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error getting window content frame stats!", (Throwable)null);
        return null;
      } 
    } 
  }
  
  public void clearWindowAnimationFrameStats() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        this.mUiAutomationConnection.clearWindowAnimationFrameStats();
      } catch (RemoteException null) {
        Log.e(LOG_TAG, "Error clearing window animation frame stats!", (Throwable)null);
      } 
      return;
    } 
  }
  
  public WindowAnimationFrameStats getWindowAnimationFrameStats() {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        return this.mUiAutomationConnection.getWindowAnimationFrameStats();
      } catch (RemoteException remoteException) {
        Log.e(LOG_TAG, "Error getting window animation frame stats!", (Throwable)remoteException);
        return null;
      } 
    } 
  }
  
  public void grantRuntimePermission(String paramString1, String paramString2) {
    grantRuntimePermissionAsUser(paramString1, paramString2, Process.myUserHandle());
  }
  
  @Deprecated
  public boolean grantRuntimePermission(String paramString1, String paramString2, UserHandle paramUserHandle) {
    grantRuntimePermissionAsUser(paramString1, paramString2, paramUserHandle);
    return true;
  }
  
  public void grantRuntimePermissionAsUser(String paramString1, String paramString2, UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        null = this.mUiAutomationConnection;
        int i = paramUserHandle.getIdentifier();
        null.grantRuntimePermission(paramString1, paramString2, i);
        return;
      } catch (Exception exception) {
        throw new SecurityException("Error granting runtime permission", exception);
      } 
    } 
  }
  
  public void revokeRuntimePermission(String paramString1, String paramString2) {
    revokeRuntimePermissionAsUser(paramString1, paramString2, Process.myUserHandle());
  }
  
  @Deprecated
  public boolean revokeRuntimePermission(String paramString1, String paramString2, UserHandle paramUserHandle) {
    revokeRuntimePermissionAsUser(paramString1, paramString2, paramUserHandle);
    return true;
  }
  
  public void revokeRuntimePermissionAsUser(String paramString1, String paramString2, UserHandle paramUserHandle) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      try {
        null = this.mUiAutomationConnection;
        int i = paramUserHandle.getIdentifier();
        null.revokeRuntimePermission(paramString1, paramString2, i);
        return;
      } catch (Exception exception) {
        throw new SecurityException("Error granting runtime permission", exception);
      } 
    } 
  }
  
  public ParcelFileDescriptor executeShellCommand(String paramString) {
    synchronized (this.mLock) {
      throwIfNotConnectedLocked();
      warnIfBetterCommand(paramString);
      ParcelFileDescriptor parcelFileDescriptor1 = null, parcelFileDescriptor2 = null;
      ParcelFileDescriptor parcelFileDescriptor3 = null, parcelFileDescriptor4 = null;
      null = null;
      try {
        ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
        ParcelFileDescriptor parcelFileDescriptor6 = arrayOfParcelFileDescriptor[0];
        ParcelFileDescriptor parcelFileDescriptor5 = arrayOfParcelFileDescriptor[1];
        null = parcelFileDescriptor5;
        parcelFileDescriptor2 = parcelFileDescriptor6;
        parcelFileDescriptor3 = parcelFileDescriptor5;
        parcelFileDescriptor1 = parcelFileDescriptor6;
        parcelFileDescriptor4 = parcelFileDescriptor5;
        this.mUiAutomationConnection.executeShellCommand(paramString, parcelFileDescriptor5, null);
        parcelFileDescriptor2 = parcelFileDescriptor6;
        parcelFileDescriptor3 = parcelFileDescriptor5;
      } catch (IOException iOException) {
        null = parcelFileDescriptor4;
        Log.e(LOG_TAG, "Error executing shell command!", iOException);
        parcelFileDescriptor3 = parcelFileDescriptor4;
        parcelFileDescriptor2 = parcelFileDescriptor1;
      } catch (RemoteException remoteException) {
        null = parcelFileDescriptor3;
        Log.e(LOG_TAG, "Error executing shell command!", (Throwable)remoteException);
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor3);
      return parcelFileDescriptor2;
    } 
  }
  
  public ParcelFileDescriptor[] executeShellCommandRw(String paramString) {
    synchronized (this.mLock) {
      ParcelFileDescriptor parcelFileDescriptor10;
      throwIfNotConnectedLocked();
      warnIfBetterCommand(paramString);
      ParcelFileDescriptor parcelFileDescriptor1 = null, parcelFileDescriptor2 = null;
      ParcelFileDescriptor parcelFileDescriptor3 = null, parcelFileDescriptor4 = null;
      null = null;
      ParcelFileDescriptor arrayOfParcelFileDescriptor1[] = null, parcelFileDescriptor5 = null, parcelFileDescriptor6 = null;
      ParcelFileDescriptor parcelFileDescriptor7 = null, parcelFileDescriptor8 = null;
      ParcelFileDescriptor parcelFileDescriptor9 = parcelFileDescriptor6, arrayOfParcelFileDescriptor2[] = arrayOfParcelFileDescriptor1, parcelFileDescriptor11 = parcelFileDescriptor8, parcelFileDescriptor12 = parcelFileDescriptor5, parcelFileDescriptor13 = parcelFileDescriptor7;
      try {
        ParcelFileDescriptor[] arrayOfParcelFileDescriptor = ParcelFileDescriptor.createPipe();
        ParcelFileDescriptor parcelFileDescriptor16 = arrayOfParcelFileDescriptor[0];
        ParcelFileDescriptor parcelFileDescriptor15 = arrayOfParcelFileDescriptor[1];
        null = parcelFileDescriptor15;
        parcelFileDescriptor9 = parcelFileDescriptor6;
        parcelFileDescriptor2 = parcelFileDescriptor16;
        parcelFileDescriptor3 = parcelFileDescriptor15;
        arrayOfParcelFileDescriptor2 = arrayOfParcelFileDescriptor1;
        parcelFileDescriptor11 = parcelFileDescriptor8;
        parcelFileDescriptor1 = parcelFileDescriptor16;
        parcelFileDescriptor4 = parcelFileDescriptor15;
        parcelFileDescriptor12 = parcelFileDescriptor5;
        parcelFileDescriptor13 = parcelFileDescriptor7;
        arrayOfParcelFileDescriptor1 = ParcelFileDescriptor.createPipe();
        parcelFileDescriptor5 = arrayOfParcelFileDescriptor1[0];
        ParcelFileDescriptor parcelFileDescriptor14 = arrayOfParcelFileDescriptor1[1];
        null = parcelFileDescriptor15;
        parcelFileDescriptor9 = parcelFileDescriptor5;
        parcelFileDescriptor2 = parcelFileDescriptor16;
        parcelFileDescriptor3 = parcelFileDescriptor15;
        parcelFileDescriptor10 = parcelFileDescriptor5;
        parcelFileDescriptor11 = parcelFileDescriptor14;
        parcelFileDescriptor1 = parcelFileDescriptor16;
        parcelFileDescriptor4 = parcelFileDescriptor15;
        parcelFileDescriptor12 = parcelFileDescriptor5;
        parcelFileDescriptor13 = parcelFileDescriptor14;
        this.mUiAutomationConnection.executeShellCommand(paramString, parcelFileDescriptor15, parcelFileDescriptor5);
        parcelFileDescriptor1 = parcelFileDescriptor16;
        parcelFileDescriptor3 = parcelFileDescriptor15;
        parcelFileDescriptor10 = parcelFileDescriptor5;
        parcelFileDescriptor11 = parcelFileDescriptor14;
      } catch (IOException iOException) {
        null = parcelFileDescriptor4;
        parcelFileDescriptor9 = parcelFileDescriptor12;
        Log.e(LOG_TAG, "Error executing shell command!", iOException);
        parcelFileDescriptor11 = parcelFileDescriptor13;
        parcelFileDescriptor10 = parcelFileDescriptor12;
        parcelFileDescriptor3 = parcelFileDescriptor4;
      } catch (RemoteException remoteException) {
        null = parcelFileDescriptor3;
        parcelFileDescriptor9 = parcelFileDescriptor10;
        Log.e(LOG_TAG, "Error executing shell command!", (Throwable)remoteException);
        parcelFileDescriptor1 = parcelFileDescriptor2;
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor3);
      IoUtils.closeQuietly((AutoCloseable)parcelFileDescriptor10);
      return new ParcelFileDescriptor[] { parcelFileDescriptor1, parcelFileDescriptor11 };
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UiAutomation@");
    stringBuilder.append(Integer.toHexString(hashCode()));
    stringBuilder.append("[id=");
    stringBuilder.append(this.mConnectionId);
    stringBuilder.append(", flags=");
    stringBuilder.append(this.mFlags);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  private boolean isConnectedLocked() {
    boolean bool;
    if (this.mConnectionId != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private void throwIfConnectedLocked() {
    if (this.mConnectionId == -1)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UiAutomation not connected, ");
    stringBuilder.append(this);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void throwIfNotConnectedLocked() {
    if (isConnectedLocked())
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UiAutomation not connected, ");
    stringBuilder.append(this);
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void warnIfBetterCommand(String paramString) {
    if (paramString.startsWith("pm grant ")) {
      Log.w(LOG_TAG, "UiAutomation.grantRuntimePermission() is more robust and should be used instead of 'pm grant'");
    } else if (paramString.startsWith("pm revoke ")) {
      Log.w(LOG_TAG, "UiAutomation.revokeRuntimePermission() is more robust and should be used instead of 'pm revoke'");
    } 
  }
  
  public static interface AccessibilityEventFilter {
    boolean accept(AccessibilityEvent param1AccessibilityEvent);
  }
  
  class IAccessibilityServiceClientImpl extends AccessibilityService.IAccessibilityServiceClientWrapper {
    final UiAutomation this$0;
    
    public IAccessibilityServiceClientImpl(Looper param1Looper) {
      super(null, param1Looper, (AccessibilityService.Callbacks)new Object(UiAutomation.this));
    }
  }
  
  public static interface OnAccessibilityEventListener {
    void onAccessibilityEvent(AccessibilityEvent param1AccessibilityEvent);
  }
}
