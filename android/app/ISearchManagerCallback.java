package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISearchManagerCallback extends IInterface {
  void onCancel() throws RemoteException;
  
  void onDismiss() throws RemoteException;
  
  class Default implements ISearchManagerCallback {
    public void onDismiss() throws RemoteException {}
    
    public void onCancel() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISearchManagerCallback {
    private static final String DESCRIPTOR = "android.app.ISearchManagerCallback";
    
    static final int TRANSACTION_onCancel = 2;
    
    static final int TRANSACTION_onDismiss = 1;
    
    public Stub() {
      attachInterface(this, "android.app.ISearchManagerCallback");
    }
    
    public static ISearchManagerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.ISearchManagerCallback");
      if (iInterface != null && iInterface instanceof ISearchManagerCallback)
        return (ISearchManagerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onCancel";
      } 
      return "onDismiss";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.app.ISearchManagerCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.app.ISearchManagerCallback");
        onCancel();
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.ISearchManagerCallback");
      onDismiss();
      return true;
    }
    
    private static class Proxy implements ISearchManagerCallback {
      public static ISearchManagerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.ISearchManagerCallback";
      }
      
      public void onDismiss() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.ISearchManagerCallback");
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISearchManagerCallback.Stub.getDefaultImpl() != null) {
            ISearchManagerCallback.Stub.getDefaultImpl().onDismiss();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCancel() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.ISearchManagerCallback");
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ISearchManagerCallback.Stub.getDefaultImpl() != null) {
            ISearchManagerCallback.Stub.getDefaultImpl().onCancel();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISearchManagerCallback param1ISearchManagerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISearchManagerCallback != null) {
          Proxy.sDefaultImpl = param1ISearchManagerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISearchManagerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
