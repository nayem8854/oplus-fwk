package android.app;

import oplus.app.IOplusCommonManager;

public interface IOplusBaseActivityManager extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.app.IActivityManager";
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
