package android.app;

import android.common.OplusFeatureCache;
import android.content.ComponentCallbacks;
import android.content.ComponentCallbacks2;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.util.ArrayMap;
import android.util.Log;
import android.view.autofill.AutofillManager;
import android.view.autofill.Helper;
import com.oplus.darkmode.IOplusDarkModeManager;
import java.util.ArrayList;

public class Application extends ContextWrapper implements ComponentCallbacks2 {
  private ArrayList<ComponentCallbacks> mComponentCallbacks = new ArrayList<>();
  
  private ArrayList<ActivityLifecycleCallbacks> mActivityLifecycleCallbacks = new ArrayList<>();
  
  private ArrayList<OnProvideAssistDataListener> mAssistCallbacks = null;
  
  private static final String TAG = "Application";
  
  public LoadedApk mLoadedApk;
  
  class ActivityLifecycleCallbacks {
    public void onActivityPreCreated(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityPostCreated(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityPreStarted(Activity param1Activity) {}
    
    public void onActivityPostStarted(Activity param1Activity) {}
    
    public void onActivityPreResumed(Activity param1Activity) {}
    
    public void onActivityPostResumed(Activity param1Activity) {}
    
    public void onActivityPrePaused(Activity param1Activity) {}
    
    public void onActivityPostPaused(Activity param1Activity) {}
    
    public void onActivityPreStopped(Activity param1Activity) {}
    
    public void onActivityPostStopped(Activity param1Activity) {}
    
    public void onActivityPreSaveInstanceState(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityPostSaveInstanceState(Activity param1Activity, Bundle param1Bundle) {}
    
    public void onActivityPreDestroyed(Activity param1Activity) {}
    
    public void onActivityPostDestroyed(Activity param1Activity) {}
    
    public abstract void onActivityCreated(Activity param1Activity, Bundle param1Bundle);
    
    public abstract void onActivityDestroyed(Activity param1Activity);
    
    public abstract void onActivityPaused(Activity param1Activity);
    
    public abstract void onActivityResumed(Activity param1Activity);
    
    public abstract void onActivitySaveInstanceState(Activity param1Activity, Bundle param1Bundle);
    
    public abstract void onActivityStarted(Activity param1Activity);
    
    public abstract void onActivityStopped(Activity param1Activity);
  }
  
  public Application() {
    super(null);
  }
  
  public void onCreate() {
    ((IOplusCommonInjector)OplusFeatureCache.<IOplusCommonInjector>getOrCreate(IOplusCommonInjector.DEFAULT, new Object[0])).onCreateForApplication(this);
  }
  
  public void onTerminate() {}
  
  public void onConfigurationChanged(Configuration paramConfiguration) {
    Object[] arrayOfObject = collectComponentCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ComponentCallbacks)arrayOfObject[b]).onConfigurationChanged(paramConfiguration);  
  }
  
  public void onLowMemory() {
    Object[] arrayOfObject = collectComponentCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ComponentCallbacks)arrayOfObject[b]).onLowMemory();  
  }
  
  public void onTrimMemory(int paramInt) {
    Object[] arrayOfObject = collectComponentCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++) {
        Object object = arrayOfObject[b];
        if (object instanceof ComponentCallbacks2)
          ((ComponentCallbacks2)object).onTrimMemory(paramInt); 
      }  
  }
  
  public void registerComponentCallbacks(ComponentCallbacks paramComponentCallbacks) {
    synchronized (this.mComponentCallbacks) {
      this.mComponentCallbacks.add(paramComponentCallbacks);
      return;
    } 
  }
  
  public void unregisterComponentCallbacks(ComponentCallbacks paramComponentCallbacks) {
    synchronized (this.mComponentCallbacks) {
      this.mComponentCallbacks.remove(paramComponentCallbacks);
      return;
    } 
  }
  
  public void registerActivityLifecycleCallbacks(ActivityLifecycleCallbacks paramActivityLifecycleCallbacks) {
    synchronized (this.mActivityLifecycleCallbacks) {
      this.mActivityLifecycleCallbacks.add(paramActivityLifecycleCallbacks);
      return;
    } 
  }
  
  public void unregisterActivityLifecycleCallbacks(ActivityLifecycleCallbacks paramActivityLifecycleCallbacks) {
    synchronized (this.mActivityLifecycleCallbacks) {
      this.mActivityLifecycleCallbacks.remove(paramActivityLifecycleCallbacks);
      return;
    } 
  }
  
  public void registerOnProvideAssistDataListener(OnProvideAssistDataListener paramOnProvideAssistDataListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mAssistCallbacks : Ljava/util/ArrayList;
    //   22: aload_0
    //   23: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   26: aload_1
    //   27: invokevirtual add : (Ljava/lang/Object;)Z
    //   30: pop
    //   31: aload_0
    //   32: monitorexit
    //   33: return
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #328	-> 0
    //   #329	-> 2
    //   #330	-> 9
    //   #332	-> 22
    //   #333	-> 31
    //   #334	-> 33
    //   #333	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	9	34	finally
    //   9	22	34	finally
    //   22	31	34	finally
    //   31	33	34	finally
    //   35	37	34	finally
  }
  
  public void unregisterOnProvideAssistDataListener(OnProvideAssistDataListener paramOnProvideAssistDataListener) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   6: ifnull -> 18
    //   9: aload_0
    //   10: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   13: aload_1
    //   14: invokevirtual remove : (Ljava/lang/Object;)Z
    //   17: pop
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #337	-> 0
    //   #338	-> 2
    //   #339	-> 9
    //   #341	-> 18
    //   #342	-> 20
    //   #341	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	9	21	finally
    //   9	18	21	finally
    //   18	20	21	finally
    //   22	24	21	finally
  }
  
  public static String getProcessName() {
    return ActivityThread.currentProcessName();
  }
  
  final void attach(Context paramContext) {
    attachBaseContext(paramContext);
    this.mLoadedApk = (ContextImpl.getImpl(paramContext)).mPackageInfo;
    ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).initDarkModeStatus(this);
  }
  
  void dispatchActivityPreCreated(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreCreated(paramActivity, paramBundle);  
  }
  
  void dispatchActivityCreated(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityCreated(paramActivity, paramBundle);  
  }
  
  void dispatchActivityPostCreated(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostCreated(paramActivity, paramBundle);  
  }
  
  void dispatchActivityPreStarted(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreStarted(paramActivity);  
  }
  
  void dispatchActivityStarted(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityStarted(paramActivity);  
  }
  
  void dispatchActivityPostStarted(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostStarted(paramActivity);  
  }
  
  void dispatchActivityPreResumed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreResumed(paramActivity);  
  }
  
  void dispatchActivityResumed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityResumed(paramActivity);  
  }
  
  void dispatchActivityPostResumed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostResumed(paramActivity);  
  }
  
  void dispatchActivityPrePaused(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPrePaused(paramActivity);  
  }
  
  void dispatchActivityPaused(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPaused(paramActivity);  
  }
  
  void dispatchActivityPostPaused(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostPaused(paramActivity);  
  }
  
  void dispatchActivityPreStopped(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreStopped(paramActivity);  
  }
  
  void dispatchActivityStopped(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityStopped(paramActivity);  
  }
  
  void dispatchActivityPostStopped(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostStopped(paramActivity);  
  }
  
  void dispatchActivityPreSaveInstanceState(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreSaveInstanceState(paramActivity, paramBundle);  
  }
  
  void dispatchActivitySaveInstanceState(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivitySaveInstanceState(paramActivity, paramBundle);  
  }
  
  void dispatchActivityPostSaveInstanceState(Activity paramActivity, Bundle paramBundle) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostSaveInstanceState(paramActivity, paramBundle);  
  }
  
  void dispatchActivityPreDestroyed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPreDestroyed(paramActivity);  
  }
  
  void dispatchActivityDestroyed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityDestroyed(paramActivity);  
  }
  
  void dispatchActivityPostDestroyed(Activity paramActivity) {
    Object[] arrayOfObject = collectActivityLifecycleCallbacks();
    if (arrayOfObject != null)
      for (byte b = 0; b < arrayOfObject.length; b++)
        ((ActivityLifecycleCallbacks)arrayOfObject[b]).onActivityPostDestroyed(paramActivity);  
  }
  
  private Object[] collectComponentCallbacks() {
    null = null;
    synchronized (this.mComponentCallbacks) {
      if (this.mComponentCallbacks.size() > 0)
        null = this.mComponentCallbacks.toArray(); 
      return null;
    } 
  }
  
  private Object[] collectActivityLifecycleCallbacks() {
    null = null;
    synchronized (this.mActivityLifecycleCallbacks) {
      if (this.mActivityLifecycleCallbacks.size() > 0)
        null = this.mActivityLifecycleCallbacks.toArray(); 
      return null;
    } 
  }
  
  void dispatchOnProvideAssistData(Activity paramActivity, Bundle paramBundle) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   6: ifnonnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: aload_0
    //   13: getfield mAssistCallbacks : Ljava/util/ArrayList;
    //   16: invokevirtual toArray : ()[Ljava/lang/Object;
    //   19: astore_3
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_3
    //   23: ifnull -> 56
    //   26: iconst_0
    //   27: istore #4
    //   29: iload #4
    //   31: aload_3
    //   32: arraylength
    //   33: if_icmpge -> 56
    //   36: aload_3
    //   37: iload #4
    //   39: aaload
    //   40: checkcast android/app/Application$OnProvideAssistDataListener
    //   43: aload_1
    //   44: aload_2
    //   45: invokeinterface onProvideAssistData : (Landroid/app/Activity;Landroid/os/Bundle;)V
    //   50: iinc #4, 1
    //   53: goto -> 29
    //   56: return
    //   57: astore_1
    //   58: aload_0
    //   59: monitorexit
    //   60: aload_1
    //   61: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #615	-> 0
    //   #616	-> 2
    //   #617	-> 9
    //   #619	-> 12
    //   #620	-> 20
    //   #621	-> 22
    //   #622	-> 26
    //   #623	-> 36
    //   #622	-> 50
    //   #626	-> 56
    //   #620	-> 57
    // Exception table:
    //   from	to	target	type
    //   2	9	57	finally
    //   9	11	57	finally
    //   12	20	57	finally
    //   20	22	57	finally
    //   58	60	57	finally
  }
  
  public AutofillManager.AutofillClient getAutofillClient() {
    AutofillManager.AutofillClient autofillClient = super.getAutofillClient();
    if (autofillClient != null)
      return autofillClient; 
    if (Helper.sVerbose)
      Log.v("Application", "getAutofillClient(): null on super, trying to find activity thread"); 
    ActivityThread activityThread = ActivityThread.currentActivityThread();
    if (activityThread == null)
      return null; 
    int i = activityThread.mActivities.size();
    for (byte b = 0; b < i; b++) {
      ArrayMap<IBinder, ActivityThread.ActivityClientRecord> arrayMap = activityThread.mActivities;
      ActivityThread.ActivityClientRecord activityClientRecord = (ActivityThread.ActivityClientRecord)arrayMap.valueAt(b);
      if (activityClientRecord != null) {
        Activity activity = activityClientRecord.activity;
        if (activity != null)
          if (activity.getWindow().getDecorView().hasFocus()) {
            if (Helper.sVerbose) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("getAutofillClient(): found activity for ");
              stringBuilder.append(this);
              stringBuilder.append(": ");
              stringBuilder.append(activity);
              Log.v("Application", stringBuilder.toString());
            } 
            return activity;
          }  
      } 
    } 
    if (Helper.sVerbose) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getAutofillClient(): none of the ");
      stringBuilder.append(i);
      stringBuilder.append(" activities on ");
      stringBuilder.append(this);
      stringBuilder.append(" have focus");
      Log.v("Application", stringBuilder.toString());
    } 
    return null;
  }
  
  class OnProvideAssistDataListener {
    public abstract void onProvideAssistData(Activity param1Activity, Bundle param1Bundle);
  }
}
