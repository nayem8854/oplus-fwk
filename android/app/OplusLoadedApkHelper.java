package android.app;

import android.content.pm.ApplicationInfo;
import android.content.pm.OplusMirrorApplicationInfo;
import android.util.Slog;
import dalvik.system.VMRuntime;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.List;

public class OplusLoadedApkHelper {
  public static void addSpecialLibraries(ApplicationInfo paramApplicationInfo, List<String> paramList) {
    if (paramApplicationInfo == null)
      return; 
    Object object = null;
    try {
      Class<?> clazz = Class.forName("android.content.pm.ApplicationInfo");
      Field field = clazz.getDeclaredField("specialNativeLibraryDirs");
      field.setAccessible(true);
      Object object1 = field.get(paramApplicationInfo);
    } catch (Exception exception) {
      Slog.e("OplusLoadedApkHelper", "addSpecialLibraries failed for get specialNativeLibraryDirs!", exception);
    } 
    if (object != null && 
      object.getClass().isArray()) {
      int j = Array.getLength(object);
      if (j > 0) {
        String str;
        if (VMRuntime.getRuntime().is64Bit()) {
          str = "/system/lib64/";
        } else {
          str = "/system/lib/";
        } 
        for (byte b = 0; b < j; b++) {
          Object object1 = Array.get(object, b);
          if (object1 != null && object1 instanceof String) {
            object1 = object1;
            if (!paramList.contains(object1)) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(str);
              stringBuilder.append((String)object1);
              paramList.add(0, stringBuilder.toString());
            } 
          } 
        } 
      } 
    } 
    int i = 0;
    if (OplusMirrorApplicationInfo.oplusPrivateFlags != null)
      i = OplusMirrorApplicationInfo.oplusPrivateFlags.get(paramApplicationInfo); 
    if ((i & 0x4) != 0)
      paramList.add(System.getProperty("java.library.path")); 
  }
  
  public static void addSpecialZipPaths(ApplicationInfo paramApplicationInfo, List<String> paramList) {}
}
