package android.app;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.DebugUtils;
import android.util.Log;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.app.IVoiceInteractorCallback;
import com.android.internal.app.IVoiceInteractorRequest;
import com.android.internal.os.HandlerCaller;
import com.android.internal.util.function.pooled.PooledLambda;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.Executor;
import java.util.function.Consumer;

public final class VoiceInteractor {
  static final Request[] NO_REQUESTS = new Request[0];
  
  final HandlerCaller.Callback mHandlerCallerCallback = (HandlerCaller.Callback)new Object(this);
  
  final IVoiceInteractorCallback.Stub mCallback = (IVoiceInteractorCallback.Stub)new Object(this);
  
  final ArrayMap<IBinder, Request> mActiveRequests = new ArrayMap();
  
  final ArrayMap<Runnable, Executor> mOnDestroyCallbacks = new ArrayMap();
  
  static final boolean DEBUG = false;
  
  public static final String KEY_CANCELLATION_SIGNAL = "key_cancellation_signal";
  
  public static final String KEY_KILL_SIGNAL = "key_kill_signal";
  
  static final int MSG_ABORT_VOICE_RESULT = 4;
  
  static final int MSG_CANCEL_RESULT = 6;
  
  static final int MSG_COMMAND_RESULT = 5;
  
  static final int MSG_COMPLETE_VOICE_RESULT = 3;
  
  static final int MSG_CONFIRMATION_RESULT = 1;
  
  static final int MSG_PICK_OPTION_RESULT = 2;
  
  static final String TAG = "VoiceInteractor";
  
  Activity mActivity;
  
  Context mContext;
  
  final HandlerCaller mHandlerCaller;
  
  IVoiceInteractor mInteractor;
  
  boolean mRetaining;
  
  public static abstract class Request {
    Activity mActivity;
    
    Context mContext;
    
    String mName;
    
    IVoiceInteractorRequest mRequestInterface;
    
    public String getName() {
      return this.mName;
    }
    
    public void cancel() {
      IVoiceInteractorRequest iVoiceInteractorRequest = this.mRequestInterface;
      if (iVoiceInteractorRequest != null) {
        try {
          iVoiceInteractorRequest.cancel();
        } catch (RemoteException remoteException) {
          Log.w("VoiceInteractor", "Voice interactor has died", (Throwable)remoteException);
        } 
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Request ");
      stringBuilder.append(this);
      stringBuilder.append(" is no longer active");
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Context getContext() {
      return this.mContext;
    }
    
    public Activity getActivity() {
      return this.mActivity;
    }
    
    public void onCancel() {}
    
    public void onAttached(Activity param1Activity) {}
    
    public void onDetached() {}
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      DebugUtils.buildShortClassTag(this, stringBuilder);
      stringBuilder.append(" ");
      stringBuilder.append(getRequestTypeName());
      stringBuilder.append(" name=");
      stringBuilder.append(this.mName);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mRequestInterface=");
      param1PrintWriter.println(this.mRequestInterface.asBinder());
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mActivity=");
      param1PrintWriter.println(this.mActivity);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mName=");
      param1PrintWriter.println(this.mName);
    }
    
    String getRequestTypeName() {
      return "Request";
    }
    
    void clear() {
      this.mRequestInterface = null;
      this.mContext = null;
      this.mActivity = null;
      this.mName = null;
    }
    
    abstract IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException;
  }
  
  class ConfirmationRequest extends Request {
    final Bundle mExtras;
    
    final VoiceInteractor.Prompt mPrompt;
    
    public ConfirmationRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public ConfirmationRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      if (this$0 != null) {
        VoiceInteractor.Prompt prompt = new VoiceInteractor.Prompt((CharSequence)this$0);
      } else {
        this$0 = null;
      } 
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public void onConfirmationResult(boolean param1Boolean, Bundle param1Bundle) {}
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
      if (this.mExtras != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mExtras=");
        param1PrintWriter.println(this.mExtras);
      } 
    }
    
    String getRequestTypeName() {
      return "Confirmation";
    }
    
    IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException {
      return param1IVoiceInteractor.startConfirmation(param1String, param1IVoiceInteractorCallback, this.mPrompt, this.mExtras);
    }
  }
  
  class PickOptionRequest extends Request {
    final Bundle mExtras;
    
    final Option[] mOptions;
    
    final VoiceInteractor.Prompt mPrompt;
    
    public static final class Option implements Parcelable {
      public Option(CharSequence param2CharSequence) {
        this.mLabel = param2CharSequence;
        this.mIndex = -1;
      }
      
      public Option(CharSequence param2CharSequence, int param2Int) {
        this.mLabel = param2CharSequence;
        this.mIndex = param2Int;
      }
      
      public Option addSynonym(CharSequence param2CharSequence) {
        if (this.mSynonyms == null)
          this.mSynonyms = new ArrayList<>(); 
        this.mSynonyms.add(param2CharSequence);
        return this;
      }
      
      public CharSequence getLabel() {
        return this.mLabel;
      }
      
      public int getIndex() {
        return this.mIndex;
      }
      
      public int countSynonyms() {
        boolean bool;
        ArrayList<CharSequence> arrayList = this.mSynonyms;
        if (arrayList != null) {
          bool = arrayList.size();
        } else {
          bool = false;
        } 
        return bool;
      }
      
      public CharSequence getSynonymAt(int param2Int) {
        ArrayList<CharSequence> arrayList = this.mSynonyms;
        if (arrayList != null) {
          CharSequence charSequence = arrayList.get(param2Int);
        } else {
          arrayList = null;
        } 
        return (CharSequence)arrayList;
      }
      
      public void setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
      }
      
      public Bundle getExtras() {
        return this.mExtras;
      }
      
      Option(Parcel param2Parcel) {
        this.mLabel = param2Parcel.readCharSequence();
        this.mIndex = param2Parcel.readInt();
        this.mSynonyms = param2Parcel.readCharSequenceList();
        this.mExtras = param2Parcel.readBundle();
      }
      
      public int describeContents() {
        return 0;
      }
      
      public void writeToParcel(Parcel param2Parcel, int param2Int) {
        param2Parcel.writeCharSequence(this.mLabel);
        param2Parcel.writeInt(this.mIndex);
        param2Parcel.writeCharSequenceList(this.mSynonyms);
        param2Parcel.writeBundle(this.mExtras);
      }
      
      public static final Parcelable.Creator<Option> CREATOR = new Parcelable.Creator<Option>() {
          public VoiceInteractor.PickOptionRequest.Option createFromParcel(Parcel param3Parcel) {
            return new VoiceInteractor.PickOptionRequest.Option(param3Parcel);
          }
          
          public VoiceInteractor.PickOptionRequest.Option[] newArray(int param3Int) {
            return new VoiceInteractor.PickOptionRequest.Option[param3Int];
          }
        };
      
      Bundle mExtras;
      
      final int mIndex;
      
      final CharSequence mLabel;
      
      ArrayList<CharSequence> mSynonyms;
    }
    
    class null implements Parcelable.Creator<Option> {
      public VoiceInteractor.PickOptionRequest.Option createFromParcel(Parcel param2Parcel) {
        return new VoiceInteractor.PickOptionRequest.Option(param2Parcel);
      }
      
      public VoiceInteractor.PickOptionRequest.Option[] newArray(int param2Int) {
        return new VoiceInteractor.PickOptionRequest.Option[param2Int];
      }
    }
    
    public PickOptionRequest(VoiceInteractor this$0, Option[] param1ArrayOfOption, Bundle param1Bundle) {
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mOptions = param1ArrayOfOption;
      this.mExtras = param1Bundle;
    }
    
    public PickOptionRequest(VoiceInteractor this$0, Option[] param1ArrayOfOption, Bundle param1Bundle) {
      if (this$0 != null) {
        VoiceInteractor.Prompt prompt = new VoiceInteractor.Prompt((CharSequence)this$0);
      } else {
        this$0 = null;
      } 
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mOptions = param1ArrayOfOption;
      this.mExtras = param1Bundle;
    }
    
    public void onPickOptionResult(boolean param1Boolean, Option[] param1ArrayOfOption, Bundle param1Bundle) {}
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
      if (this.mOptions != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.println("Options:");
        byte b = 0;
        while (true) {
          Option[] arrayOfOption = this.mOptions;
          if (b < arrayOfOption.length) {
            Option option = arrayOfOption[b];
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("  #");
            param1PrintWriter.print(b);
            param1PrintWriter.println(":");
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("    mLabel=");
            param1PrintWriter.println(option.mLabel);
            param1PrintWriter.print(param1String);
            param1PrintWriter.print("    mIndex=");
            param1PrintWriter.println(option.mIndex);
            if (option.mSynonyms != null && option.mSynonyms.size() > 0) {
              param1PrintWriter.print(param1String);
              param1PrintWriter.println("    Synonyms:");
              for (byte b1 = 0; b1 < option.mSynonyms.size(); b1++) {
                param1PrintWriter.print(param1String);
                param1PrintWriter.print("      #");
                param1PrintWriter.print(b1);
                param1PrintWriter.print(": ");
                param1PrintWriter.println(option.mSynonyms.get(b1));
              } 
            } 
            if (option.mExtras != null) {
              param1PrintWriter.print(param1String);
              param1PrintWriter.print("    mExtras=");
              param1PrintWriter.println(option.mExtras);
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
      if (this.mExtras != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mExtras=");
        param1PrintWriter.println(this.mExtras);
      } 
    }
    
    String getRequestTypeName() {
      return "PickOption";
    }
    
    IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException {
      return param1IVoiceInteractor.startPickOption(param1String, param1IVoiceInteractorCallback, this.mPrompt, this.mOptions, this.mExtras);
    }
  }
  
  class CompleteVoiceRequest extends Request {
    final Bundle mExtras;
    
    final VoiceInteractor.Prompt mPrompt;
    
    public CompleteVoiceRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public CompleteVoiceRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      if (this$0 != null) {
        VoiceInteractor.Prompt prompt = new VoiceInteractor.Prompt((CharSequence)this$0);
      } else {
        this$0 = null;
      } 
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public void onCompleteResult(Bundle param1Bundle) {}
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
      if (this.mExtras != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mExtras=");
        param1PrintWriter.println(this.mExtras);
      } 
    }
    
    String getRequestTypeName() {
      return "CompleteVoice";
    }
    
    IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException {
      return param1IVoiceInteractor.startCompleteVoice(param1String, param1IVoiceInteractorCallback, this.mPrompt, this.mExtras);
    }
  }
  
  class AbortVoiceRequest extends Request {
    final Bundle mExtras;
    
    final VoiceInteractor.Prompt mPrompt;
    
    public AbortVoiceRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public AbortVoiceRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      if (this$0 != null) {
        VoiceInteractor.Prompt prompt = new VoiceInteractor.Prompt((CharSequence)this$0);
      } else {
        this$0 = null;
      } 
      this.mPrompt = (VoiceInteractor.Prompt)this$0;
      this.mExtras = param1Bundle;
    }
    
    public void onAbortResult(Bundle param1Bundle) {}
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mPrompt=");
      param1PrintWriter.println(this.mPrompt);
      if (this.mExtras != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mExtras=");
        param1PrintWriter.println(this.mExtras);
      } 
    }
    
    String getRequestTypeName() {
      return "AbortVoice";
    }
    
    IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException {
      return param1IVoiceInteractor.startAbortVoice(param1String, param1IVoiceInteractorCallback, this.mPrompt, this.mExtras);
    }
  }
  
  class CommandRequest extends Request {
    final Bundle mArgs;
    
    final String mCommand;
    
    public CommandRequest(VoiceInteractor this$0, Bundle param1Bundle) {
      this.mCommand = (String)this$0;
      this.mArgs = param1Bundle;
    }
    
    public void onCommandResult(boolean param1Boolean, Bundle param1Bundle) {}
    
    void dump(String param1String, FileDescriptor param1FileDescriptor, PrintWriter param1PrintWriter, String[] param1ArrayOfString) {
      super.dump(param1String, param1FileDescriptor, param1PrintWriter, param1ArrayOfString);
      param1PrintWriter.print(param1String);
      param1PrintWriter.print("mCommand=");
      param1PrintWriter.println(this.mCommand);
      if (this.mArgs != null) {
        param1PrintWriter.print(param1String);
        param1PrintWriter.print("mArgs=");
        param1PrintWriter.println(this.mArgs);
      } 
    }
    
    String getRequestTypeName() {
      return "Command";
    }
    
    IVoiceInteractorRequest submit(IVoiceInteractor param1IVoiceInteractor, String param1String, IVoiceInteractorCallback param1IVoiceInteractorCallback) throws RemoteException {
      return param1IVoiceInteractor.startCommand(param1String, param1IVoiceInteractorCallback, this.mCommand, this.mArgs);
    }
  }
  
  class Prompt implements Parcelable {
    public Prompt(VoiceInteractor this$0, CharSequence param1CharSequence) {
      if (this$0 != null) {
        if (this$0.length != 0) {
          if (param1CharSequence != null) {
            this.mVoicePrompts = (CharSequence[])this$0;
            this.mVisualPrompt = param1CharSequence;
            return;
          } 
          throw new NullPointerException("visualPrompt must not be null");
        } 
        throw new IllegalArgumentException("voicePrompts must not be empty");
      } 
      throw new NullPointerException("voicePrompts must not be null");
    }
    
    public Prompt(VoiceInteractor this$0) {
      this.mVoicePrompts = new CharSequence[] { (CharSequence)this$0 };
      this.mVisualPrompt = (CharSequence)this$0;
    }
    
    public CharSequence getVoicePromptAt(int param1Int) {
      return this.mVoicePrompts[param1Int];
    }
    
    public int countVoicePrompts() {
      return this.mVoicePrompts.length;
    }
    
    public CharSequence getVisualPrompt() {
      return this.mVisualPrompt;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      DebugUtils.buildShortClassTag(this, stringBuilder);
      CharSequence charSequence = this.mVisualPrompt;
      if (charSequence != null) {
        CharSequence[] arrayOfCharSequence = this.mVoicePrompts;
        if (arrayOfCharSequence != null && arrayOfCharSequence.length == 1) {
          CharSequence charSequence1 = arrayOfCharSequence[0];
          if (charSequence.equals(charSequence1)) {
            stringBuilder.append(" ");
            stringBuilder.append(this.mVisualPrompt);
            stringBuilder.append('}');
            return stringBuilder.toString();
          } 
        } 
      } 
      if (this.mVisualPrompt != null) {
        stringBuilder.append(" visual=");
        stringBuilder.append(this.mVisualPrompt);
      } 
      if (this.mVoicePrompts != null) {
        stringBuilder.append(", voice=");
        for (byte b = 0; b < this.mVoicePrompts.length; b++) {
          if (b > 0)
            stringBuilder.append(" | "); 
          stringBuilder.append(this.mVoicePrompts[b]);
        } 
      } 
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    Prompt(VoiceInteractor this$0) {
      this.mVoicePrompts = this$0.readCharSequenceArray();
      this.mVisualPrompt = this$0.readCharSequence();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeCharSequenceArray(this.mVoicePrompts);
      param1Parcel.writeCharSequence(this.mVisualPrompt);
    }
    
    public static final Parcelable.Creator<Prompt> CREATOR = (Parcelable.Creator<Prompt>)new Object();
    
    private final CharSequence mVisualPrompt;
    
    private final CharSequence[] mVoicePrompts;
  }
  
  VoiceInteractor(IVoiceInteractor paramIVoiceInteractor, Context paramContext, Activity paramActivity, Looper paramLooper) {
    this.mInteractor = paramIVoiceInteractor;
    this.mContext = paramContext;
    this.mActivity = paramActivity;
    this.mHandlerCaller = new HandlerCaller(paramContext, paramLooper, this.mHandlerCallerCallback, true);
    try {
      paramIVoiceInteractor = this.mInteractor;
      KillCallback killCallback = new KillCallback();
      this(this);
      paramIVoiceInteractor.setKillCallback((ICancellationSignal)killCallback);
    } catch (RemoteException remoteException) {}
  }
  
  Request pullRequest(IVoiceInteractorRequest paramIVoiceInteractorRequest, boolean paramBoolean) {
    synchronized (this.mActiveRequests) {
      Request request = (Request)this.mActiveRequests.get(paramIVoiceInteractorRequest.asBinder());
      if (request != null && paramBoolean)
        this.mActiveRequests.remove(paramIVoiceInteractorRequest.asBinder()); 
      return request;
    } 
  }
  
  private ArrayList<Request> makeRequestList() {
    int i = this.mActiveRequests.size();
    if (i < 1)
      return null; 
    ArrayList<Request> arrayList = new ArrayList(i);
    for (byte b = 0; b < i; b++)
      arrayList.add((Request)this.mActiveRequests.valueAt(b)); 
    return arrayList;
  }
  
  void attachActivity(Activity paramActivity) {
    this.mRetaining = false;
    if (this.mActivity == paramActivity)
      return; 
    this.mContext = (Context)paramActivity;
    this.mActivity = paramActivity;
    ArrayList<Request> arrayList = makeRequestList();
    if (arrayList != null)
      for (byte b = 0; b < arrayList.size(); b++) {
        Request request = arrayList.get(b);
        request.mContext = (Context)paramActivity;
        request.mActivity = paramActivity;
        request.onAttached(paramActivity);
      }  
  }
  
  void retainInstance() {
    this.mRetaining = true;
  }
  
  void detachActivity() {
    ArrayList<Request> arrayList = makeRequestList();
    if (arrayList != null)
      for (byte b = 0; b < arrayList.size(); b++) {
        Request request = arrayList.get(b);
        request.onDetached();
        request.mActivity = null;
        request.mContext = null;
      }  
    if (!this.mRetaining) {
      arrayList = makeRequestList();
      if (arrayList != null)
        for (byte b = 0; b < arrayList.size(); b++) {
          Request request = arrayList.get(b);
          request.cancel();
        }  
      this.mActiveRequests.clear();
    } 
    this.mContext = null;
    this.mActivity = null;
  }
  
  void destroy() {
    int i = this.mActiveRequests.size();
    for (; --i >= 0; i--) {
      Request request = (Request)this.mActiveRequests.valueAt(i);
      this.mActiveRequests.removeAt(i);
      request.cancel();
    } 
    i = this.mOnDestroyCallbacks.size();
    for (; --i >= 0; i--) {
      Runnable runnable = (Runnable)this.mOnDestroyCallbacks.keyAt(i);
      Executor executor = (Executor)this.mOnDestroyCallbacks.valueAt(i);
      executor.execute(runnable);
      this.mOnDestroyCallbacks.removeAt(i);
    } 
    this.mInteractor = null;
    Activity activity = this.mActivity;
    if (activity != null)
      activity.setVoiceInteractor(null); 
  }
  
  public boolean submitRequest(Request paramRequest) {
    return submitRequest(paramRequest, null);
  }
  
  public boolean submitRequest(Request paramRequest, String paramString) {
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return false;
    } 
    try {
      if (paramRequest.mRequestInterface == null) {
        IVoiceInteractor iVoiceInteractor = this.mInteractor;
        Context context = this.mContext;
        String str = context.getOpPackageName();
        IVoiceInteractorCallback.Stub stub = this.mCallback;
        IVoiceInteractorRequest iVoiceInteractorRequest = paramRequest.submit(iVoiceInteractor, str, (IVoiceInteractorCallback)stub);
        paramRequest.mRequestInterface = iVoiceInteractorRequest;
        paramRequest.mContext = this.mContext;
        paramRequest.mActivity = this.mActivity;
        paramRequest.mName = paramString;
        synchronized (this.mActiveRequests) {
          this.mActiveRequests.put(iVoiceInteractorRequest.asBinder(), paramRequest);
          return true;
        } 
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Given ");
      stringBuilder.append(paramRequest);
      stringBuilder.append(" is already active");
      this(stringBuilder.toString());
      throw illegalStateException;
    } catch (RemoteException remoteException) {
      Log.w("VoiceInteractor", "Remove voice interactor service died", (Throwable)remoteException);
      return false;
    } 
  }
  
  public Request[] getActiveRequests() {
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return null;
    } 
    synchronized (this.mActiveRequests) {
      int i = this.mActiveRequests.size();
      if (i <= 0)
        return NO_REQUESTS; 
      Request[] arrayOfRequest = new Request[i];
      for (byte b = 0; b < i; b++)
        arrayOfRequest[b] = (Request)this.mActiveRequests.valueAt(b); 
      return arrayOfRequest;
    } 
  }
  
  public Request getActiveRequest(String paramString) {
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return null;
    } 
    synchronized (this.mActiveRequests) {
      int i = this.mActiveRequests.size();
      for (byte b = 0; b < i; b++) {
        Request request = (Request)this.mActiveRequests.valueAt(b);
        if (paramString == request.getName() || (paramString != null && paramString.equals(request.getName())))
          return request; 
      } 
      return null;
    } 
  }
  
  public boolean[] supportsCommands(String[] paramArrayOfString) {
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return new boolean[paramArrayOfString.length];
    } 
    try {
      return this.mInteractor.supportsCommands(this.mContext.getOpPackageName(), paramArrayOfString);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Voice interactor has died", remoteException);
    } 
  }
  
  public boolean isDestroyed() {
    boolean bool;
    if (this.mInteractor == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean registerOnDestroyedCallback(Executor paramExecutor, Runnable paramRunnable) {
    Objects.requireNonNull(paramExecutor);
    Objects.requireNonNull(paramRunnable);
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return false;
    } 
    this.mOnDestroyCallbacks.put(paramRunnable, paramExecutor);
    return true;
  }
  
  public boolean unregisterOnDestroyedCallback(Runnable paramRunnable) {
    Objects.requireNonNull(paramRunnable);
    boolean bool = isDestroyed();
    boolean bool1 = false;
    if (bool) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return false;
    } 
    if (this.mOnDestroyCallbacks.remove(paramRunnable) != null)
      bool1 = true; 
    return bool1;
  }
  
  public void notifyDirectActionsChanged() {
    if (isDestroyed()) {
      Log.w("VoiceInteractor", "Cannot interact with a destroyed voice interactor");
      return;
    } 
    try {
      IVoiceInteractor iVoiceInteractor = this.mInteractor;
      int i = this.mActivity.getTaskId();
      Activity activity = this.mActivity;
      IBinder iBinder = activity.getAssistToken();
      iVoiceInteractor.notifyDirectActionsChanged(i, iBinder);
    } catch (RemoteException remoteException) {
      Log.w("VoiceInteractor", "Voice interactor has died", (Throwable)remoteException);
    } 
  }
  
  void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("    ");
    String str = stringBuilder.toString();
    if (this.mActiveRequests.size() > 0) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.println("Active voice requests:");
      for (byte b = 0; b < this.mActiveRequests.size(); b++) {
        Request request = (Request)this.mActiveRequests.valueAt(b);
        paramPrintWriter.print(paramString);
        paramPrintWriter.print("  #");
        paramPrintWriter.print(b);
        paramPrintWriter.print(": ");
        paramPrintWriter.println(request);
        request.dump(str, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
      } 
    } 
    paramPrintWriter.print(paramString);
    paramPrintWriter.println("VoiceInteractor misc state:");
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("  mInteractor=");
    paramPrintWriter.println(this.mInteractor.asBinder());
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("  mActivity=");
    paramPrintWriter.println(this.mActivity);
  }
  
  class KillCallback extends ICancellationSignal.Stub {
    private final WeakReference<VoiceInteractor> mInteractor;
    
    KillCallback(VoiceInteractor this$0) {
      this.mInteractor = new WeakReference<>(this$0);
    }
    
    public void cancel() {
      VoiceInteractor voiceInteractor = this.mInteractor.get();
      if (voiceInteractor != null) {
        Handler handler = voiceInteractor.mHandlerCaller.getHandler();
        -$.Lambda.dUWXWbBHcaaVBn031EDBP91NR7k dUWXWbBHcaaVBn031EDBP91NR7k = _$$Lambda$dUWXWbBHcaaVBn031EDBP91NR7k.INSTANCE;
        Message message = PooledLambda.obtainMessage((Consumer)dUWXWbBHcaaVBn031EDBP91NR7k, voiceInteractor);
        handler.sendMessage(message);
      } 
    }
  }
}
