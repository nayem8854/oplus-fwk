package android.app;

import android.common.OplusFeatureCache;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.PackageManager;
import android.content.pm.SharedLibraryInfo;
import android.content.pm.dex.ArtManager;
import android.content.pm.split.SplitDependencyLoader;
import android.content.res.AssetManager;
import android.content.res.CompatibilityInfo;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StrictMode;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import android.util.SparseArray;
import android.view.DisplayAdjustments;
import com.android.internal.util.ArrayUtils;
import dalvik.system.BaseDexClassLoader;
import dalvik.system.VMRuntime;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executor;

public final class LoadedApk {
  private final DisplayAdjustments mDisplayAdjustments = new DisplayAdjustments();
  
  private final ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> mReceivers = new ArrayMap();
  
  private final ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> mUnregisteredReceivers = new ArrayMap();
  
  private final ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>> mServices = new ArrayMap();
  
  private final ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>> mUnboundServices = new ArrayMap();
  
  static final boolean $assertionsDisabled = false;
  
  static final boolean DEBUG = false;
  
  private static final String PROPERTY_NAME_APPEND_NATIVE = "pi.append_native_lib_paths";
  
  static final String TAG = "LoadedApk";
  
  private final ActivityThread mActivityThread;
  
  private AppComponentFactory mAppComponentFactory;
  
  private String mAppDir;
  
  private Application mApplication;
  
  private ApplicationInfo mApplicationInfo;
  
  private final ClassLoader mBaseClassLoader;
  
  private ClassLoader mClassLoader;
  
  private File mCredentialProtectedDataDirFile;
  
  private String mDataDir;
  
  private File mDataDirFile;
  
  private ClassLoader mDefaultClassLoader;
  
  private File mDeviceProtectedDataDirFile;
  
  private final boolean mIncludeCode;
  
  private String mLibDir;
  
  private String[] mOverlayDirs;
  
  final String mPackageName;
  
  private final boolean mRegisterPackage;
  
  private String mResDir;
  
  Resources mResources;
  
  private final boolean mSecurityViolation;
  
  private final String[] mSpecialLibraries;
  
  private String[] mSplitAppDirs;
  
  private String[] mSplitClassLoaderNames;
  
  private SplitDependencyLoaderImpl mSplitLoader;
  
  private String[] mSplitNames;
  
  private String[] mSplitResDirs;
  
  Application getApplication() {
    return this.mApplication;
  }
  
  public LoadedApk(ActivityThread paramActivityThread, ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, ClassLoader paramClassLoader, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    this.mActivityThread = paramActivityThread;
    setApplicationInfo(paramApplicationInfo);
    this.mPackageName = paramApplicationInfo.packageName;
    this.mBaseClassLoader = paramClassLoader;
    this.mSecurityViolation = paramBoolean1;
    this.mIncludeCode = paramBoolean2;
    this.mRegisterPackage = paramBoolean3;
    this.mDisplayAdjustments.setCompatibilityInfo(paramCompatibilityInfo);
    this.mAppComponentFactory = createAppFactory(this.mApplicationInfo, this.mBaseClassLoader);
    this.mSpecialLibraries = paramApplicationInfo.specialNativeLibraryDirs;
  }
  
  private static ApplicationInfo adjustNativeLibraryPaths(ApplicationInfo paramApplicationInfo) {
    if (paramApplicationInfo.primaryCpuAbi != null && paramApplicationInfo.secondaryCpuAbi != null) {
      String str1 = VMRuntime.getRuntime().vmInstructionSet();
      String str2 = VMRuntime.getInstructionSet(paramApplicationInfo.secondaryCpuAbi);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ro.dalvik.vm.isa.");
      stringBuilder.append(str2);
      String str3 = SystemProperties.get(stringBuilder.toString());
      if (!str3.isEmpty())
        str2 = str3; 
      if (str1.equals(str2)) {
        paramApplicationInfo = new ApplicationInfo(paramApplicationInfo);
        paramApplicationInfo.nativeLibraryDir = paramApplicationInfo.secondaryNativeLibraryDir;
        paramApplicationInfo.primaryCpuAbi = paramApplicationInfo.secondaryCpuAbi;
        return paramApplicationInfo;
      } 
    } 
    return paramApplicationInfo;
  }
  
  LoadedApk(ActivityThread paramActivityThread) {
    this.mActivityThread = paramActivityThread;
    ApplicationInfo applicationInfo = new ApplicationInfo();
    applicationInfo.packageName = "android";
    this.mPackageName = "android";
    this.mAppDir = null;
    this.mResDir = null;
    this.mSplitAppDirs = null;
    this.mSplitResDirs = null;
    this.mSplitClassLoaderNames = null;
    this.mOverlayDirs = null;
    this.mDataDir = null;
    this.mDataDirFile = null;
    this.mDeviceProtectedDataDirFile = null;
    this.mCredentialProtectedDataDirFile = null;
    this.mLibDir = null;
    this.mBaseClassLoader = null;
    this.mSecurityViolation = false;
    this.mIncludeCode = true;
    this.mRegisterPackage = false;
    this.mResources = Resources.getSystem();
    ClassLoader classLoader = ClassLoader.getSystemClassLoader();
    AppComponentFactory appComponentFactory = createAppFactory(this.mApplicationInfo, classLoader);
    this.mClassLoader = appComponentFactory.instantiateClassLoader(this.mDefaultClassLoader, new ApplicationInfo(this.mApplicationInfo));
    this.mSpecialLibraries = null;
  }
  
  void installSystemApplicationInfo(ApplicationInfo paramApplicationInfo, ClassLoader paramClassLoader) {
    this.mApplicationInfo = paramApplicationInfo;
    this.mDefaultClassLoader = paramClassLoader;
    AppComponentFactory appComponentFactory = createAppFactory(paramApplicationInfo, paramClassLoader);
    this.mClassLoader = appComponentFactory.instantiateClassLoader(this.mDefaultClassLoader, new ApplicationInfo(this.mApplicationInfo));
  }
  
  private AppComponentFactory createAppFactory(ApplicationInfo paramApplicationInfo, ClassLoader paramClassLoader) {
    if (this.mIncludeCode && paramApplicationInfo.appComponentFactory != null && paramClassLoader != null)
      try {
        String str = paramApplicationInfo.appComponentFactory;
        return 
          (AppComponentFactory)paramClassLoader.loadClass(str).newInstance();
      } catch (InstantiationException|IllegalAccessException|ClassNotFoundException instantiationException) {
        Slog.e("LoadedApk", "Unable to instantiate appComponentFactory", instantiationException);
      }  
    return AppComponentFactory.DEFAULT;
  }
  
  public AppComponentFactory getAppFactory() {
    return this.mAppComponentFactory;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public ApplicationInfo getApplicationInfo() {
    return this.mApplicationInfo;
  }
  
  public int getTargetSdkVersion() {
    return this.mApplicationInfo.targetSdkVersion;
  }
  
  public boolean isSecurityViolation() {
    return this.mSecurityViolation;
  }
  
  public CompatibilityInfo getCompatibilityInfo() {
    return this.mDisplayAdjustments.getCompatibilityInfo();
  }
  
  public void setCompatibilityInfo(CompatibilityInfo paramCompatibilityInfo) {
    this.mDisplayAdjustments.setCompatibilityInfo(paramCompatibilityInfo);
  }
  
  private static String[] getLibrariesFor(String paramString) {
    try {
      IPackageManager iPackageManager = ActivityThread.getPackageManager();
      int i = UserHandle.myUserId();
      ApplicationInfo applicationInfo = iPackageManager.getApplicationInfo(paramString, 1024, i);
      if (applicationInfo == null)
        return null; 
      return applicationInfo.sharedLibraryFiles;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateApplicationInfo(ApplicationInfo paramApplicationInfo, List<String> paramList) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokespecial setApplicationInfo : (Landroid/content/pm/ApplicationInfo;)V
    //   5: new java/util/ArrayList
    //   8: dup
    //   9: invokespecial <init> : ()V
    //   12: astore_3
    //   13: aload_0
    //   14: getfield mActivityThread : Landroid/app/ActivityThread;
    //   17: aload_1
    //   18: aload_3
    //   19: invokestatic makePaths : (Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   22: new java/util/ArrayList
    //   25: dup
    //   26: aload_3
    //   27: invokeinterface size : ()I
    //   32: invokespecial <init> : (I)V
    //   35: astore #4
    //   37: aload_2
    //   38: ifnull -> 175
    //   41: aload_3
    //   42: invokeinterface iterator : ()Ljava/util/Iterator;
    //   47: astore #5
    //   49: aload #5
    //   51: invokeinterface hasNext : ()Z
    //   56: ifeq -> 172
    //   59: aload #5
    //   61: invokeinterface next : ()Ljava/lang/Object;
    //   66: checkcast java/lang/String
    //   69: astore #6
    //   71: aload #6
    //   73: aload #6
    //   75: getstatic java/io/File.separator : Ljava/lang/String;
    //   78: invokevirtual lastIndexOf : (Ljava/lang/String;)I
    //   81: invokevirtual substring : (I)Ljava/lang/String;
    //   84: astore #7
    //   86: iconst_0
    //   87: istore #8
    //   89: aload_2
    //   90: invokeinterface iterator : ()Ljava/util/Iterator;
    //   95: astore_3
    //   96: iload #8
    //   98: istore #9
    //   100: aload_3
    //   101: invokeinterface hasNext : ()Z
    //   106: ifeq -> 154
    //   109: aload_3
    //   110: invokeinterface next : ()Ljava/lang/Object;
    //   115: checkcast java/lang/String
    //   118: astore #10
    //   120: aload #10
    //   122: aload #10
    //   124: getstatic java/io/File.separator : Ljava/lang/String;
    //   127: invokevirtual lastIndexOf : (Ljava/lang/String;)I
    //   130: invokevirtual substring : (I)Ljava/lang/String;
    //   133: astore #10
    //   135: aload #7
    //   137: aload #10
    //   139: invokevirtual equals : (Ljava/lang/Object;)Z
    //   142: ifeq -> 151
    //   145: iconst_1
    //   146: istore #9
    //   148: goto -> 154
    //   151: goto -> 96
    //   154: iload #9
    //   156: ifne -> 169
    //   159: aload #4
    //   161: aload #6
    //   163: invokeinterface add : (Ljava/lang/Object;)Z
    //   168: pop
    //   169: goto -> 49
    //   172: goto -> 184
    //   175: aload #4
    //   177: aload_3
    //   178: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   183: pop
    //   184: aload_0
    //   185: monitorenter
    //   186: aload_0
    //   187: aload #4
    //   189: invokespecial createOrUpdateClassLoaderLocked : (Ljava/util/List;)V
    //   192: aload_0
    //   193: getfield mResources : Landroid/content/res/Resources;
    //   196: astore_2
    //   197: aload_2
    //   198: ifnull -> 309
    //   201: aload_0
    //   202: aconst_null
    //   203: invokevirtual getSplitPaths : (Ljava/lang/String;)[Ljava/lang/String;
    //   206: astore_3
    //   207: invokestatic getInstance : ()Landroid/app/ResourcesManager;
    //   210: astore #5
    //   212: aload_0
    //   213: getfield mResDir : Ljava/lang/String;
    //   216: astore #10
    //   218: aload_0
    //   219: getfield mOverlayDirs : [Ljava/lang/String;
    //   222: astore #11
    //   224: aload_0
    //   225: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   228: getfield sharedLibraryFiles : [Ljava/lang/String;
    //   231: astore #7
    //   233: aload_0
    //   234: invokevirtual getCompatibilityInfo : ()Landroid/content/res/CompatibilityInfo;
    //   237: astore #4
    //   239: aload_0
    //   240: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   243: astore #6
    //   245: aload_0
    //   246: getfield mApplication : Landroid/app/Application;
    //   249: ifnonnull -> 257
    //   252: aconst_null
    //   253: astore_2
    //   254: goto -> 268
    //   257: aload_0
    //   258: getfield mApplication : Landroid/app/Application;
    //   261: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   264: invokevirtual getLoaders : ()Ljava/util/List;
    //   267: astore_2
    //   268: aload_0
    //   269: aload #5
    //   271: aconst_null
    //   272: aload #10
    //   274: aload_3
    //   275: aload #11
    //   277: aload #7
    //   279: iconst_0
    //   280: aconst_null
    //   281: aload #4
    //   283: aload #6
    //   285: aload_2
    //   286: invokevirtual getResources : (Landroid/os/IBinder;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;Ljava/util/List;)Landroid/content/res/Resources;
    //   289: putfield mResources : Landroid/content/res/Resources;
    //   292: goto -> 309
    //   295: astore_1
    //   296: new java/lang/AssertionError
    //   299: astore_1
    //   300: aload_1
    //   301: ldc_w 'null split not found'
    //   304: invokespecial <init> : (Ljava/lang/Object;)V
    //   307: aload_1
    //   308: athrow
    //   309: aload_0
    //   310: monitorexit
    //   311: aload_0
    //   312: aload_0
    //   313: aload_1
    //   314: aload_0
    //   315: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   318: invokespecial createAppFactory : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)Landroid/app/AppComponentFactory;
    //   321: putfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   324: return
    //   325: astore_1
    //   326: aload_0
    //   327: monitorexit
    //   328: aload_1
    //   329: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #359	-> 0
    //   #361	-> 5
    //   #362	-> 13
    //   #363	-> 22
    //   #365	-> 37
    //   #366	-> 41
    //   #367	-> 71
    //   #368	-> 86
    //   #369	-> 89
    //   #370	-> 120
    //   #371	-> 135
    //   #372	-> 145
    //   #373	-> 148
    //   #375	-> 151
    //   #376	-> 154
    //   #377	-> 159
    //   #379	-> 169
    //   #381	-> 175
    //   #383	-> 184
    //   #384	-> 186
    //   #385	-> 192
    //   #388	-> 201
    //   #392	-> 207
    //   #394	-> 207
    //   #396	-> 233
    //   #397	-> 239
    //   #398	-> 257
    //   #394	-> 268
    //   #389	-> 295
    //   #391	-> 296
    //   #400	-> 309
    //   #401	-> 311
    //   #402	-> 324
    //   #400	-> 325
    // Exception table:
    //   from	to	target	type
    //   186	192	325	finally
    //   192	197	325	finally
    //   201	207	295	android/content/pm/PackageManager$NameNotFoundException
    //   201	207	325	finally
    //   207	233	325	finally
    //   233	239	325	finally
    //   239	252	325	finally
    //   257	268	325	finally
    //   268	292	325	finally
    //   296	309	325	finally
    //   309	311	325	finally
    //   326	328	325	finally
  }
  
  private void setApplicationInfo(ApplicationInfo paramApplicationInfo) {
    String str, arrayOfString[];
    int i = Process.myUid();
    ApplicationInfo applicationInfo = adjustNativeLibraryPaths(paramApplicationInfo);
    this.mApplicationInfo = applicationInfo;
    this.mAppDir = applicationInfo.sourceDir;
    if (applicationInfo.uid == i) {
      str = applicationInfo.sourceDir;
    } else {
      str = applicationInfo.publicSourceDir;
    } 
    this.mResDir = str;
    this.mOverlayDirs = applicationInfo.resourceDirs;
    this.mDataDir = applicationInfo.dataDir;
    this.mLibDir = applicationInfo.nativeLibraryDir;
    this.mDataDirFile = FileUtils.newFileOrNull(applicationInfo.dataDir);
    this.mDeviceProtectedDataDirFile = FileUtils.newFileOrNull(applicationInfo.deviceProtectedDataDir);
    this.mCredentialProtectedDataDirFile = FileUtils.newFileOrNull(applicationInfo.credentialProtectedDataDir);
    this.mSplitNames = applicationInfo.splitNames;
    this.mSplitAppDirs = applicationInfo.splitSourceDirs;
    if (applicationInfo.uid == i) {
      arrayOfString = applicationInfo.splitSourceDirs;
    } else {
      arrayOfString = applicationInfo.splitPublicSourceDirs;
    } 
    this.mSplitResDirs = arrayOfString;
    this.mSplitClassLoaderNames = applicationInfo.splitClassLoaderNames;
    if (applicationInfo.requestsIsolatedSplitLoading() && !ArrayUtils.isEmpty((Object[])this.mSplitNames))
      this.mSplitLoader = new SplitDependencyLoaderImpl(applicationInfo.splitDependencies); 
  }
  
  public static void makePaths(ActivityThread paramActivityThread, ApplicationInfo paramApplicationInfo, List<String> paramList) {
    makePaths(paramActivityThread, false, paramApplicationInfo, paramList, null);
  }
  
  private static void appendSharedLibrariesLibPathsIfNeeded(List<SharedLibraryInfo> paramList, ApplicationInfo paramApplicationInfo, Set<String> paramSet, List<String> paramList1) {
    if (paramList == null)
      return; 
    for (SharedLibraryInfo sharedLibraryInfo : paramList) {
      List<String> list1 = sharedLibraryInfo.getAllCodePaths();
      paramSet.addAll(list1);
      for (String str : list1)
        appendApkLibPathIfNeeded(str, paramApplicationInfo, paramList1); 
      List<SharedLibraryInfo> list = sharedLibraryInfo.getDependencies();
      appendSharedLibrariesLibPathsIfNeeded(list, paramApplicationInfo, paramSet, paramList1);
    } 
  }
  
  private static void addCustomMdmJarToPath(List<String> paramList) {
    paramList.add("/system_ext/framework/OplusMdmInterface.jar");
    paramList.add("/system_ext/framework/OplusMdmAdapter.jar");
  }
  
  public static boolean isPackageContainsOplusCertificates(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    IBinder iBinder = ServiceManager.getService("opluscoreappservice");
    boolean bool = false;
    if (iBinder == null)
      return false; 
    try {
      parcel1.writeInterfaceToken("com.oplus.customize.coreapp.aidl.IOplusCoreAppService");
      parcel1.writeString(paramString);
      iBinder.transact(2, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public static void makePaths(ActivityThread paramActivityThread, boolean paramBoolean, ApplicationInfo paramApplicationInfo, List<String> paramList1, List<String> paramList2) {
    // Byte code:
    //   0: aload_2
    //   1: getfield sourceDir : Ljava/lang/String;
    //   4: astore #5
    //   6: aload_2
    //   7: getfield nativeLibraryDir : Ljava/lang/String;
    //   10: astore #6
    //   12: aload_3
    //   13: invokeinterface clear : ()V
    //   18: aload_3
    //   19: aload #5
    //   21: invokeinterface add : (Ljava/lang/Object;)Z
    //   26: pop
    //   27: aload_2
    //   28: getfield splitSourceDirs : [Ljava/lang/String;
    //   31: ifnull -> 50
    //   34: aload_2
    //   35: invokevirtual requestsIsolatedSplitLoading : ()Z
    //   38: ifne -> 50
    //   41: aload_3
    //   42: aload_2
    //   43: getfield splitSourceDirs : [Ljava/lang/String;
    //   46: invokestatic addAll : (Ljava/util/Collection;[Ljava/lang/Object;)Z
    //   49: pop
    //   50: aload #4
    //   52: ifnull -> 62
    //   55: aload #4
    //   57: invokeinterface clear : ()V
    //   62: aconst_null
    //   63: astore #7
    //   65: aload #7
    //   67: astore #8
    //   69: aload_0
    //   70: ifnull -> 270
    //   73: aload_0
    //   74: getfield mInstrumentationPackageName : Ljava/lang/String;
    //   77: astore #9
    //   79: aload_0
    //   80: getfield mInstrumentationAppDir : Ljava/lang/String;
    //   83: astore #10
    //   85: aload_0
    //   86: getfield mInstrumentationSplitAppDirs : [Ljava/lang/String;
    //   89: astore #11
    //   91: aload_0
    //   92: getfield mInstrumentationLibDir : Ljava/lang/String;
    //   95: astore #12
    //   97: aload_0
    //   98: getfield mInstrumentedAppDir : Ljava/lang/String;
    //   101: astore #13
    //   103: aload_0
    //   104: getfield mInstrumentedSplitAppDirs : [Ljava/lang/String;
    //   107: astore #14
    //   109: aload_0
    //   110: getfield mInstrumentedLibDir : Ljava/lang/String;
    //   113: astore #15
    //   115: aload #5
    //   117: aload #10
    //   119: invokevirtual equals : (Ljava/lang/Object;)Z
    //   122: ifne -> 139
    //   125: aload #7
    //   127: astore #8
    //   129: aload #5
    //   131: aload #13
    //   133: invokevirtual equals : (Ljava/lang/Object;)Z
    //   136: ifeq -> 270
    //   139: aload_3
    //   140: invokeinterface clear : ()V
    //   145: aload_3
    //   146: aload #10
    //   148: invokeinterface add : (Ljava/lang/Object;)Z
    //   153: pop
    //   154: aload #10
    //   156: aload #13
    //   158: invokevirtual equals : (Ljava/lang/Object;)Z
    //   161: ifne -> 173
    //   164: aload_3
    //   165: aload #13
    //   167: invokeinterface add : (Ljava/lang/Object;)Z
    //   172: pop
    //   173: aload_2
    //   174: invokevirtual requestsIsolatedSplitLoading : ()Z
    //   177: ifne -> 214
    //   180: aload #11
    //   182: ifnull -> 192
    //   185: aload_3
    //   186: aload #11
    //   188: invokestatic addAll : (Ljava/util/Collection;[Ljava/lang/Object;)Z
    //   191: pop
    //   192: aload #10
    //   194: aload #13
    //   196: invokevirtual equals : (Ljava/lang/Object;)Z
    //   199: ifne -> 214
    //   202: aload #14
    //   204: ifnull -> 214
    //   207: aload_3
    //   208: aload #14
    //   210: invokestatic addAll : (Ljava/util/Collection;[Ljava/lang/Object;)Z
    //   213: pop
    //   214: aload #4
    //   216: ifnull -> 249
    //   219: aload #4
    //   221: aload #12
    //   223: invokeinterface add : (Ljava/lang/Object;)Z
    //   228: pop
    //   229: aload #12
    //   231: aload #15
    //   233: invokevirtual equals : (Ljava/lang/Object;)Z
    //   236: ifne -> 249
    //   239: aload #4
    //   241: aload #15
    //   243: invokeinterface add : (Ljava/lang/Object;)Z
    //   248: pop
    //   249: aload #7
    //   251: astore #8
    //   253: aload #13
    //   255: aload #10
    //   257: invokevirtual equals : (Ljava/lang/Object;)Z
    //   260: ifne -> 270
    //   263: aload #9
    //   265: invokestatic getLibrariesFor : (Ljava/lang/String;)[Ljava/lang/String;
    //   268: astore #8
    //   270: aload_0
    //   271: ifnull -> 312
    //   274: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   277: astore_0
    //   278: aload_0
    //   279: ifnull -> 312
    //   282: aload_2
    //   283: getfield packageName : Ljava/lang/String;
    //   286: invokestatic isPackageContainsOplusCertificates : (Ljava/lang/String;)Z
    //   289: ifeq -> 312
    //   292: aload_3
    //   293: invokestatic addCustomMdmJarToPath : (Ljava/util/List;)V
    //   296: goto -> 312
    //   299: astore_0
    //   300: ldc 'LoadedApk'
    //   302: ldc_w 'addCustomMdmJarToPath errror'
    //   305: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   308: pop
    //   309: goto -> 312
    //   312: aload #4
    //   314: ifnull -> 515
    //   317: aload #4
    //   319: invokeinterface isEmpty : ()Z
    //   324: ifeq -> 337
    //   327: aload #4
    //   329: aload #6
    //   331: invokeinterface add : (Ljava/lang/Object;)Z
    //   336: pop
    //   337: aload_2
    //   338: getfield primaryCpuAbi : Ljava/lang/String;
    //   341: ifnull -> 491
    //   344: aload_2
    //   345: getfield targetSdkVersion : I
    //   348: bipush #24
    //   350: if_icmpge -> 414
    //   353: new java/lang/StringBuilder
    //   356: dup
    //   357: invokespecial <init> : ()V
    //   360: astore #7
    //   362: aload #7
    //   364: ldc_w '/system/fake-libs'
    //   367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload_2
    //   372: getfield primaryCpuAbi : Ljava/lang/String;
    //   375: invokestatic is64BitAbi : (Ljava/lang/String;)Z
    //   378: ifeq -> 388
    //   381: ldc_w '64'
    //   384: astore_0
    //   385: goto -> 392
    //   388: ldc_w ''
    //   391: astore_0
    //   392: aload #7
    //   394: aload_0
    //   395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: pop
    //   399: aload #7
    //   401: invokevirtual toString : ()Ljava/lang/String;
    //   404: astore_0
    //   405: aload #4
    //   407: aload_0
    //   408: invokeinterface add : (Ljava/lang/Object;)Z
    //   413: pop
    //   414: aload_3
    //   415: invokeinterface iterator : ()Ljava/util/Iterator;
    //   420: astore #6
    //   422: aload #6
    //   424: invokeinterface hasNext : ()Z
    //   429: ifeq -> 491
    //   432: aload #6
    //   434: invokeinterface next : ()Ljava/lang/Object;
    //   439: checkcast java/lang/String
    //   442: astore #7
    //   444: new java/lang/StringBuilder
    //   447: dup
    //   448: invokespecial <init> : ()V
    //   451: astore_0
    //   452: aload_0
    //   453: aload #7
    //   455: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   458: pop
    //   459: aload_0
    //   460: ldc_w '!/lib/'
    //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   466: pop
    //   467: aload_0
    //   468: aload_2
    //   469: getfield primaryCpuAbi : Ljava/lang/String;
    //   472: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   475: pop
    //   476: aload #4
    //   478: aload_0
    //   479: invokevirtual toString : ()Ljava/lang/String;
    //   482: invokeinterface add : (Ljava/lang/Object;)Z
    //   487: pop
    //   488: goto -> 422
    //   491: iload_1
    //   492: ifeq -> 509
    //   495: aload #4
    //   497: ldc_w 'java.library.path'
    //   500: invokestatic getProperty : (Ljava/lang/String;)Ljava/lang/String;
    //   503: invokeinterface add : (Ljava/lang/Object;)Z
    //   508: pop
    //   509: aload_2
    //   510: aload #4
    //   512: invokestatic addSpecialLibraries : (Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   515: new java/util/LinkedHashSet
    //   518: dup
    //   519: invokespecial <init> : ()V
    //   522: astore_0
    //   523: aload_2
    //   524: getfield sharedLibraryInfos : Ljava/util/List;
    //   527: aload_2
    //   528: aload_0
    //   529: aload #4
    //   531: invokestatic appendSharedLibrariesLibPathsIfNeeded : (Ljava/util/List;Landroid/content/pm/ApplicationInfo;Ljava/util/Set;Ljava/util/List;)V
    //   534: aload_2
    //   535: getfield sharedLibraryFiles : [Ljava/lang/String;
    //   538: ifnull -> 636
    //   541: iconst_0
    //   542: istore #16
    //   544: aload_2
    //   545: getfield sharedLibraryFiles : [Ljava/lang/String;
    //   548: astore #7
    //   550: aload #7
    //   552: arraylength
    //   553: istore #17
    //   555: iconst_0
    //   556: istore #18
    //   558: iload #18
    //   560: iload #17
    //   562: if_icmpge -> 636
    //   565: aload #7
    //   567: iload #18
    //   569: aaload
    //   570: astore #6
    //   572: iload #16
    //   574: istore #19
    //   576: aload_0
    //   577: aload #6
    //   579: invokeinterface contains : (Ljava/lang/Object;)Z
    //   584: ifne -> 626
    //   587: iload #16
    //   589: istore #19
    //   591: aload_3
    //   592: aload #6
    //   594: invokeinterface contains : (Ljava/lang/Object;)Z
    //   599: ifne -> 626
    //   602: aload_3
    //   603: iload #16
    //   605: aload #6
    //   607: invokeinterface add : (ILjava/lang/Object;)V
    //   612: iload #16
    //   614: iconst_1
    //   615: iadd
    //   616: istore #19
    //   618: aload #6
    //   620: aload_2
    //   621: aload #4
    //   623: invokestatic appendApkLibPathIfNeeded : (Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   626: iinc #18, 1
    //   629: iload #19
    //   631: istore #16
    //   633: goto -> 558
    //   636: aload #8
    //   638: ifnull -> 693
    //   641: aload #8
    //   643: arraylength
    //   644: istore #16
    //   646: iconst_0
    //   647: istore #18
    //   649: iload #18
    //   651: iload #16
    //   653: if_icmpge -> 693
    //   656: aload #8
    //   658: iload #18
    //   660: aaload
    //   661: astore_0
    //   662: aload_3
    //   663: aload_0
    //   664: invokeinterface contains : (Ljava/lang/Object;)Z
    //   669: ifne -> 687
    //   672: aload_3
    //   673: iconst_0
    //   674: aload_0
    //   675: invokeinterface add : (ILjava/lang/Object;)V
    //   680: aload_0
    //   681: aload_2
    //   682: aload #4
    //   684: invokestatic appendApkLibPathIfNeeded : (Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   687: iinc #18, 1
    //   690: goto -> 649
    //   693: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #487	-> 0
    //   #488	-> 6
    //   #490	-> 12
    //   #491	-> 18
    //   #494	-> 27
    //   #495	-> 41
    //   #498	-> 50
    //   #499	-> 55
    //   #511	-> 62
    //   #514	-> 65
    //   #515	-> 73
    //   #516	-> 79
    //   #517	-> 85
    //   #518	-> 91
    //   #520	-> 97
    //   #521	-> 103
    //   #522	-> 109
    //   #524	-> 115
    //   #525	-> 125
    //   #526	-> 139
    //   #527	-> 145
    //   #528	-> 154
    //   #529	-> 164
    //   #533	-> 173
    //   #534	-> 180
    //   #535	-> 185
    //   #538	-> 192
    //   #539	-> 202
    //   #540	-> 207
    //   #545	-> 214
    //   #546	-> 219
    //   #547	-> 229
    //   #548	-> 239
    //   #552	-> 249
    //   #553	-> 263
    //   #560	-> 270
    //   #562	-> 274
    //   #563	-> 278
    //   #564	-> 292
    //   #567	-> 299
    //   #568	-> 300
    //   #569	-> 312
    //   #572	-> 312
    //   #573	-> 317
    //   #574	-> 327
    //   #579	-> 337
    //   #581	-> 344
    //   #582	-> 353
    //   #583	-> 371
    //   #582	-> 405
    //   #585	-> 414
    //   #586	-> 444
    //   #587	-> 488
    //   #590	-> 491
    //   #595	-> 495
    //   #600	-> 509
    //   #606	-> 515
    //   #607	-> 523
    //   #613	-> 534
    //   #614	-> 541
    //   #615	-> 544
    //   #616	-> 572
    //   #617	-> 602
    //   #618	-> 612
    //   #619	-> 618
    //   #615	-> 626
    //   #624	-> 636
    //   #625	-> 641
    //   #626	-> 662
    //   #627	-> 672
    //   #628	-> 680
    //   #625	-> 687
    //   #632	-> 693
    // Exception table:
    //   from	to	target	type
    //   274	278	299	java/lang/Exception
    //   282	292	299	java/lang/Exception
    //   292	296	299	java/lang/Exception
  }
  
  private static void appendApkLibPathIfNeeded(String paramString, ApplicationInfo paramApplicationInfo, List<String> paramList) {
    if (paramList != null && paramApplicationInfo.primaryCpuAbi != null && paramString.endsWith(".apk") && 
      paramApplicationInfo.targetSdkVersion >= 26) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("!/lib/");
      stringBuilder.append(paramApplicationInfo.primaryCpuAbi);
      paramList.add(stringBuilder.toString());
    } 
  }
  
  class SplitDependencyLoaderImpl extends SplitDependencyLoader<PackageManager.NameNotFoundException> {
    private final ClassLoader[] mCachedClassLoaders;
    
    private final String[][] mCachedResourcePaths;
    
    final LoadedApk this$0;
    
    SplitDependencyLoaderImpl(SparseArray<int[]> param1SparseArray) {
      super(param1SparseArray);
      this.mCachedResourcePaths = new String[LoadedApk.this.mSplitNames.length + 1][];
      this.mCachedClassLoaders = new ClassLoader[LoadedApk.this.mSplitNames.length + 1];
    }
    
    protected boolean isSplitCached(int param1Int) {
      boolean bool;
      if (this.mCachedClassLoaders[param1Int] != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    protected void constructSplit(int param1Int1, int[] param1ArrayOfint, int param1Int2) throws PackageManager.NameNotFoundException {
      ArrayList<String> arrayList = new ArrayList();
      int i = 0;
      if (param1Int1 == 0) {
        LoadedApk.this.createOrUpdateClassLoaderLocked(null);
        this.mCachedClassLoaders[0] = LoadedApk.this.mClassLoader;
        for (param1Int2 = param1ArrayOfint.length, param1Int1 = 0; param1Int1 < param1Int2; ) {
          i = param1ArrayOfint[param1Int1];
          arrayList.add(LoadedApk.this.mSplitResDirs[i - 1]);
          param1Int1++;
        } 
        this.mCachedResourcePaths[0] = arrayList.<String>toArray(new String[arrayList.size()]);
        return;
      } 
      ClassLoader arrayOfClassLoader[] = this.mCachedClassLoaders, classLoader = arrayOfClassLoader[param1Int2];
      ApplicationLoaders applicationLoaders = ApplicationLoaders.getDefault();
      LoadedApk loadedApk1 = LoadedApk.this;
      String str1 = loadedApk1.mSplitAppDirs[param1Int1 - 1];
      int j = LoadedApk.this.getTargetSdkVersion();
      LoadedApk loadedApk2 = LoadedApk.this;
      String str2 = loadedApk2.mSplitClassLoaderNames[param1Int1 - 1];
      arrayOfClassLoader[param1Int1] = applicationLoaders.getClassLoader(str1, j, false, null, null, classLoader, str2);
      Collections.addAll(arrayList, this.mCachedResourcePaths[param1Int2]);
      arrayList.add(LoadedApk.this.mSplitResDirs[param1Int1 - 1]);
      for (j = param1ArrayOfint.length, param1Int2 = i; param1Int2 < j; ) {
        i = param1ArrayOfint[param1Int2];
        arrayList.add(LoadedApk.this.mSplitResDirs[i - 1]);
        param1Int2++;
      } 
      this.mCachedResourcePaths[param1Int1] = arrayList.<String>toArray(new String[arrayList.size()]);
    }
    
    private int ensureSplitLoaded(String param1String) throws PackageManager.NameNotFoundException {
      int i = 0;
      if (param1String != null) {
        i = Arrays.binarySearch((Object[])LoadedApk.this.mSplitNames, param1String);
        if (i >= 0) {
          i++;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Split name '");
          stringBuilder.append(param1String);
          stringBuilder.append("' is not installed");
          throw new PackageManager.NameNotFoundException(stringBuilder.toString());
        } 
      } 
      loadDependenciesForSplit(i);
      return i;
    }
    
    ClassLoader getClassLoaderForSplit(String param1String) throws PackageManager.NameNotFoundException {
      return this.mCachedClassLoaders[ensureSplitLoaded(param1String)];
    }
    
    String[] getSplitPathsForSplit(String param1String) throws PackageManager.NameNotFoundException {
      return this.mCachedResourcePaths[ensureSplitLoaded(param1String)];
    }
  }
  
  ClassLoader getSplitClassLoader(String paramString) throws PackageManager.NameNotFoundException {
    SplitDependencyLoaderImpl splitDependencyLoaderImpl = this.mSplitLoader;
    if (splitDependencyLoaderImpl == null)
      return this.mClassLoader; 
    return splitDependencyLoaderImpl.getClassLoaderForSplit(paramString);
  }
  
  String[] getSplitPaths(String paramString) throws PackageManager.NameNotFoundException {
    SplitDependencyLoaderImpl splitDependencyLoaderImpl = this.mSplitLoader;
    if (splitDependencyLoaderImpl == null)
      return this.mSplitResDirs; 
    return splitDependencyLoaderImpl.getSplitPathsForSplit(paramString);
  }
  
  ClassLoader createSharedLibraryLoader(SharedLibraryInfo paramSharedLibraryInfo, boolean paramBoolean, String paramString1, String paramString2) {
    String str;
    List<String> list1 = paramSharedLibraryInfo.getAllCodePaths();
    List<SharedLibraryInfo> list = paramSharedLibraryInfo.getDependencies();
    List<ClassLoader> list2 = createSharedLibrariesLoaders(list, paramBoolean, paramString1, paramString2);
    if (list1.size() == 1) {
      str = list1.get(0);
    } else {
      str = TextUtils.join(File.pathSeparator, list1);
    } 
    return ApplicationLoaders.getDefault().getSharedLibraryClassLoaderWithSharedLibraries(str, this.mApplicationInfo.targetSdkVersion, paramBoolean, paramString1, paramString2, null, null, list2);
  }
  
  private List<ClassLoader> createSharedLibrariesLoaders(List<SharedLibraryInfo> paramList, boolean paramBoolean, String paramString1, String paramString2) {
    if (paramList == null)
      return null; 
    ArrayList<ClassLoader> arrayList = new ArrayList();
    for (SharedLibraryInfo sharedLibraryInfo : paramList)
      arrayList.add(createSharedLibraryLoader(sharedLibraryInfo, paramBoolean, paramString1, paramString2)); 
    return arrayList;
  }
  
  private StrictMode.ThreadPolicy allowThreadDiskReads() {
    if (this.mActivityThread == null)
      return null; 
    return StrictMode.allowThreadDiskReads();
  }
  
  private void setThreadPolicy(StrictMode.ThreadPolicy paramThreadPolicy) {
    if (this.mActivityThread != null && paramThreadPolicy != null)
      StrictMode.setThreadPolicy(paramThreadPolicy); 
  }
  
  private void createOrUpdateClassLoaderLocked(List<String> paramList) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPackageName : Ljava/lang/String;
    //   4: ldc 'android'
    //   6: invokevirtual equals : (Ljava/lang/Object;)Z
    //   9: ifeq -> 86
    //   12: aload_0
    //   13: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   16: ifnull -> 20
    //   19: return
    //   20: aload_0
    //   21: getfield mBaseClassLoader : Ljava/lang/ClassLoader;
    //   24: astore_1
    //   25: aload_1
    //   26: ifnull -> 37
    //   29: aload_0
    //   30: aload_1
    //   31: putfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   34: goto -> 44
    //   37: aload_0
    //   38: invokestatic getSystemClassLoader : ()Ljava/lang/ClassLoader;
    //   41: putfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   44: aload_0
    //   45: aload_0
    //   46: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   49: aload_0
    //   50: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   53: invokespecial createAppFactory : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)Landroid/app/AppComponentFactory;
    //   56: astore_1
    //   57: aload_0
    //   58: aload_1
    //   59: putfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   62: aload_0
    //   63: aload_1
    //   64: aload_0
    //   65: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   68: new android/content/pm/ApplicationInfo
    //   71: dup
    //   72: aload_0
    //   73: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   76: invokespecial <init> : (Landroid/content/pm/ApplicationInfo;)V
    //   79: invokevirtual instantiateClassLoader : (Ljava/lang/ClassLoader;Landroid/content/pm/ApplicationInfo;)Ljava/lang/ClassLoader;
    //   82: putfield mClassLoader : Ljava/lang/ClassLoader;
    //   85: return
    //   86: aload_0
    //   87: getfield mActivityThread : Landroid/app/ActivityThread;
    //   90: ifnull -> 138
    //   93: aload_0
    //   94: getfield mPackageName : Ljava/lang/String;
    //   97: astore_2
    //   98: aload_2
    //   99: invokestatic currentPackageName : ()Ljava/lang/String;
    //   102: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   105: ifne -> 138
    //   108: aload_0
    //   109: getfield mIncludeCode : Z
    //   112: ifeq -> 138
    //   115: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   118: aload_0
    //   119: getfield mPackageName : Ljava/lang/String;
    //   122: bipush #6
    //   124: invokeinterface notifyPackageUse : (Ljava/lang/String;I)V
    //   129: goto -> 138
    //   132: astore_1
    //   133: aload_1
    //   134: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   137: athrow
    //   138: aload_0
    //   139: getfield mRegisterPackage : Z
    //   142: ifeq -> 166
    //   145: invokestatic getService : ()Landroid/app/IActivityManager;
    //   148: aload_0
    //   149: getfield mPackageName : Ljava/lang/String;
    //   152: invokeinterface addPackageDependency : (Ljava/lang/String;)V
    //   157: goto -> 166
    //   160: astore_1
    //   161: aload_1
    //   162: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   165: athrow
    //   166: new java/util/ArrayList
    //   169: dup
    //   170: bipush #10
    //   172: invokespecial <init> : (I)V
    //   175: astore_3
    //   176: new java/util/ArrayList
    //   179: dup
    //   180: bipush #10
    //   182: invokespecial <init> : (I)V
    //   185: astore #4
    //   187: aload_0
    //   188: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   191: invokevirtual isSystemApp : ()Z
    //   194: ifeq -> 215
    //   197: aload_0
    //   198: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   201: astore_2
    //   202: aload_2
    //   203: invokevirtual isUpdatedSystemApp : ()Z
    //   206: ifne -> 215
    //   209: iconst_1
    //   210: istore #5
    //   212: goto -> 218
    //   215: iconst_0
    //   216: istore #5
    //   218: ldc_w 'java.library.path'
    //   221: invokestatic getProperty : (Ljava/lang/String;)Ljava/lang/String;
    //   224: astore #6
    //   226: aload #6
    //   228: ldc_w '/vendor/lib'
    //   231: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   234: istore #7
    //   236: iload #5
    //   238: istore #8
    //   240: aload_0
    //   241: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   244: invokevirtual getCodePath : ()Ljava/lang/String;
    //   247: ifnull -> 280
    //   250: aload_0
    //   251: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   254: astore_2
    //   255: iload #5
    //   257: istore #8
    //   259: aload_2
    //   260: invokevirtual isVendor : ()Z
    //   263: ifeq -> 280
    //   266: iload #5
    //   268: istore #8
    //   270: iload #7
    //   272: iconst_1
    //   273: ixor
    //   274: ifeq -> 280
    //   277: iconst_0
    //   278: istore #8
    //   280: aload_0
    //   281: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   284: invokevirtual getCodePath : ()Ljava/lang/String;
    //   287: ifnull -> 317
    //   290: aload_0
    //   291: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   294: astore_2
    //   295: aload_2
    //   296: invokevirtual isProduct : ()Z
    //   299: ifeq -> 317
    //   302: invokestatic product_vndk_version : ()Ljava/util/Optional;
    //   305: invokevirtual isPresent : ()Z
    //   308: ifeq -> 317
    //   311: iconst_0
    //   312: istore #5
    //   314: goto -> 321
    //   317: iload #8
    //   319: istore #5
    //   321: aload_0
    //   322: getfield mActivityThread : Landroid/app/ActivityThread;
    //   325: iload #5
    //   327: aload_0
    //   328: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   331: aload_3
    //   332: aload #4
    //   334: invokestatic makePaths : (Landroid/app/ActivityThread;ZLandroid/content/pm/ApplicationInfo;Ljava/util/List;Ljava/util/List;)V
    //   337: aload_0
    //   338: getfield mDataDir : Ljava/lang/String;
    //   341: astore_2
    //   342: aload_0
    //   343: getfield mActivityThread : Landroid/app/ActivityThread;
    //   346: ifnonnull -> 353
    //   349: ldc_w ''
    //   352: astore_2
    //   353: iload #5
    //   355: ifeq -> 458
    //   358: new java/lang/StringBuilder
    //   361: dup
    //   362: invokespecial <init> : ()V
    //   365: astore #9
    //   367: aload #9
    //   369: aload_2
    //   370: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   373: pop
    //   374: aload #9
    //   376: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   379: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   382: pop
    //   383: aload #9
    //   385: aload_0
    //   386: invokevirtual getAppDir : ()Ljava/lang/String;
    //   389: iconst_0
    //   390: anewarray java/lang/String
    //   393: invokestatic get : (Ljava/lang/String;[Ljava/lang/String;)Ljava/nio/file/Path;
    //   396: invokeinterface getParent : ()Ljava/nio/file/Path;
    //   401: invokeinterface toString : ()Ljava/lang/String;
    //   406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: aload #9
    //   412: invokevirtual toString : ()Ljava/lang/String;
    //   415: astore_2
    //   416: new java/lang/StringBuilder
    //   419: dup
    //   420: invokespecial <init> : ()V
    //   423: astore #9
    //   425: aload #9
    //   427: aload_2
    //   428: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   431: pop
    //   432: aload #9
    //   434: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   437: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   440: pop
    //   441: aload #9
    //   443: aload #6
    //   445: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: pop
    //   449: aload #9
    //   451: invokevirtual toString : ()Ljava/lang/String;
    //   454: astore_2
    //   455: goto -> 458
    //   458: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   461: aload #4
    //   463: invokestatic join : (Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   466: astore #9
    //   468: aload_0
    //   469: getfield mActivityThread : Landroid/app/ActivityThread;
    //   472: astore #6
    //   474: aload #6
    //   476: ifnull -> 685
    //   479: aload #6
    //   481: ldc_w 'gpu_debug_app'
    //   484: ldc_w ''
    //   487: invokevirtual getStringCoreSetting : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   490: astore #6
    //   492: aload #6
    //   494: invokevirtual isEmpty : ()Z
    //   497: ifne -> 685
    //   500: aload_0
    //   501: getfield mPackageName : Ljava/lang/String;
    //   504: aload #6
    //   506: invokevirtual equals : (Ljava/lang/Object;)Z
    //   509: ifeq -> 685
    //   512: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   515: astore #6
    //   517: aload_0
    //   518: getfield mPackageName : Ljava/lang/String;
    //   521: astore #10
    //   523: invokestatic myUserId : ()I
    //   526: istore #11
    //   528: aload #6
    //   530: aload #10
    //   532: sipush #128
    //   535: iload #11
    //   537: invokeinterface getApplicationInfo : (Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    //   542: astore #6
    //   544: invokestatic getInstance : ()Landroid/os/GraphicsEnvironment;
    //   547: astore #10
    //   549: aload_0
    //   550: getfield mActivityThread : Landroid/app/ActivityThread;
    //   553: astore #12
    //   555: aload #12
    //   557: invokevirtual getCoreSettings : ()Landroid/os/Bundle;
    //   560: astore #12
    //   562: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   565: astore #13
    //   567: aload_0
    //   568: getfield mPackageName : Ljava/lang/String;
    //   571: astore #14
    //   573: aload #10
    //   575: aload #12
    //   577: aload #13
    //   579: aload #14
    //   581: aload #6
    //   583: invokevirtual getDebugLayerPathsFromSettings : (Landroid/os/Bundle;Landroid/content/pm/IPackageManager;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;)Ljava/lang/String;
    //   586: astore #10
    //   588: aload_2
    //   589: astore #6
    //   591: aload #10
    //   593: ifnull -> 637
    //   596: new java/lang/StringBuilder
    //   599: astore #6
    //   601: aload #6
    //   603: invokespecial <init> : ()V
    //   606: aload #6
    //   608: aload_2
    //   609: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   612: pop
    //   613: aload #6
    //   615: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   618: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   621: pop
    //   622: aload #6
    //   624: aload #10
    //   626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   629: pop
    //   630: aload #6
    //   632: invokevirtual toString : ()Ljava/lang/String;
    //   635: astore #6
    //   637: aload #6
    //   639: astore_2
    //   640: goto -> 685
    //   643: astore #6
    //   645: new java/lang/StringBuilder
    //   648: dup
    //   649: invokespecial <init> : ()V
    //   652: astore #6
    //   654: aload #6
    //   656: ldc_w 'RemoteException when fetching debug layer paths for: '
    //   659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   662: pop
    //   663: aload #6
    //   665: aload_0
    //   666: getfield mPackageName : Ljava/lang/String;
    //   669: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   672: pop
    //   673: ldc_w 'ActivityThread'
    //   676: aload #6
    //   678: invokevirtual toString : ()Ljava/lang/String;
    //   681: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   684: pop
    //   685: aload_0
    //   686: getfield mIncludeCode : Z
    //   689: ifne -> 783
    //   692: aload_0
    //   693: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   696: ifnonnull -> 749
    //   699: aload_0
    //   700: invokespecial allowThreadDiskReads : ()Landroid/os/StrictMode$ThreadPolicy;
    //   703: astore_1
    //   704: aload_0
    //   705: invokestatic getDefault : ()Landroid/app/ApplicationLoaders;
    //   708: ldc_w ''
    //   711: aload_0
    //   712: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   715: getfield targetSdkVersion : I
    //   718: iload #5
    //   720: aload #9
    //   722: aload_2
    //   723: aload_0
    //   724: getfield mBaseClassLoader : Ljava/lang/ClassLoader;
    //   727: aconst_null
    //   728: invokevirtual getClassLoader : (Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;)Ljava/lang/ClassLoader;
    //   731: putfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   734: aload_0
    //   735: aload_1
    //   736: invokespecial setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   739: aload_0
    //   740: getstatic android/app/AppComponentFactory.DEFAULT : Landroid/app/AppComponentFactory;
    //   743: putfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   746: goto -> 749
    //   749: aload_0
    //   750: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   753: ifnonnull -> 782
    //   756: aload_0
    //   757: aload_0
    //   758: getfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   761: aload_0
    //   762: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   765: new android/content/pm/ApplicationInfo
    //   768: dup
    //   769: aload_0
    //   770: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   773: invokespecial <init> : (Landroid/content/pm/ApplicationInfo;)V
    //   776: invokevirtual instantiateClassLoader : (Ljava/lang/ClassLoader;Landroid/content/pm/ApplicationInfo;)Ljava/lang/ClassLoader;
    //   779: putfield mClassLoader : Ljava/lang/ClassLoader;
    //   782: return
    //   783: aload_3
    //   784: invokeinterface size : ()I
    //   789: iconst_1
    //   790: if_icmpne -> 808
    //   793: aload_3
    //   794: iconst_0
    //   795: invokeinterface get : (I)Ljava/lang/Object;
    //   800: checkcast java/lang/String
    //   803: astore #6
    //   805: goto -> 817
    //   808: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   811: aload_3
    //   812: invokestatic join : (Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   815: astore #6
    //   817: aload_0
    //   818: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   821: ifnonnull -> 910
    //   824: aload_0
    //   825: invokespecial allowThreadDiskReads : ()Landroid/os/StrictMode$ThreadPolicy;
    //   828: astore_3
    //   829: aload_0
    //   830: aload_0
    //   831: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   834: getfield sharedLibraryInfos : Ljava/util/List;
    //   837: iload #5
    //   839: aload #9
    //   841: aload_2
    //   842: invokespecial createSharedLibrariesLoaders : (Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;)Ljava/util/List;
    //   845: astore #10
    //   847: invokestatic getDefault : ()Landroid/app/ApplicationLoaders;
    //   850: aload #6
    //   852: aload_0
    //   853: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   856: getfield targetSdkVersion : I
    //   859: iload #5
    //   861: aload #9
    //   863: aload_2
    //   864: aload_0
    //   865: getfield mBaseClassLoader : Ljava/lang/ClassLoader;
    //   868: aload_0
    //   869: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   872: getfield classLoaderName : Ljava/lang/String;
    //   875: aload #10
    //   877: invokevirtual getClassLoaderWithSharedLibraries : (Ljava/lang/String;IZLjava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;Ljava/lang/String;Ljava/util/List;)Ljava/lang/ClassLoader;
    //   880: astore_2
    //   881: aload_0
    //   882: aload_2
    //   883: putfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   886: aload_0
    //   887: aload_0
    //   888: aload_0
    //   889: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   892: aload_2
    //   893: invokespecial createAppFactory : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)Landroid/app/AppComponentFactory;
    //   896: putfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   899: aload_0
    //   900: aload_3
    //   901: invokespecial setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   904: iconst_1
    //   905: istore #11
    //   907: goto -> 913
    //   910: iconst_0
    //   911: istore #11
    //   913: aload #4
    //   915: invokeinterface isEmpty : ()Z
    //   920: ifne -> 965
    //   923: ldc 'pi.append_native_lib_paths'
    //   925: iconst_1
    //   926: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   929: ifeq -> 965
    //   932: aload_0
    //   933: invokespecial allowThreadDiskReads : ()Landroid/os/StrictMode$ThreadPolicy;
    //   936: astore_2
    //   937: invokestatic getDefault : ()Landroid/app/ApplicationLoaders;
    //   940: aload_0
    //   941: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   944: aload #4
    //   946: invokevirtual addNative : (Ljava/lang/ClassLoader;Ljava/util/Collection;)V
    //   949: aload_0
    //   950: aload_2
    //   951: invokespecial setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   954: goto -> 965
    //   957: astore_1
    //   958: aload_0
    //   959: aload_2
    //   960: invokespecial setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   963: aload_1
    //   964: athrow
    //   965: iload #11
    //   967: istore #15
    //   969: aload_1
    //   970: ifnull -> 1008
    //   973: iload #11
    //   975: istore #15
    //   977: aload_1
    //   978: invokeinterface size : ()I
    //   983: ifle -> 1008
    //   986: getstatic java/io/File.pathSeparator : Ljava/lang/String;
    //   989: aload_1
    //   990: invokestatic join : (Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;
    //   993: astore_1
    //   994: invokestatic getDefault : ()Landroid/app/ApplicationLoaders;
    //   997: aload_0
    //   998: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   1001: aload_1
    //   1002: invokevirtual addPath : (Ljava/lang/ClassLoader;Ljava/lang/String;)V
    //   1005: iconst_1
    //   1006: istore #15
    //   1008: iload #15
    //   1010: ifeq -> 1030
    //   1013: invokestatic isSystem : ()Z
    //   1016: ifne -> 1030
    //   1019: aload_0
    //   1020: getfield mActivityThread : Landroid/app/ActivityThread;
    //   1023: ifnull -> 1030
    //   1026: aload_0
    //   1027: invokespecial setupJitProfileSupport : ()V
    //   1030: aload_0
    //   1031: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   1034: ifnonnull -> 1063
    //   1037: aload_0
    //   1038: aload_0
    //   1039: getfield mAppComponentFactory : Landroid/app/AppComponentFactory;
    //   1042: aload_0
    //   1043: getfield mDefaultClassLoader : Ljava/lang/ClassLoader;
    //   1046: new android/content/pm/ApplicationInfo
    //   1049: dup
    //   1050: aload_0
    //   1051: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   1054: invokespecial <init> : (Landroid/content/pm/ApplicationInfo;)V
    //   1057: invokevirtual instantiateClassLoader : (Ljava/lang/ClassLoader;Landroid/content/pm/ApplicationInfo;)Ljava/lang/ClassLoader;
    //   1060: putfield mClassLoader : Ljava/lang/ClassLoader;
    //   1063: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #799	-> 0
    //   #802	-> 12
    //   #804	-> 19
    //   #807	-> 20
    //   #808	-> 29
    //   #810	-> 37
    //   #812	-> 44
    //   #813	-> 62
    //   #815	-> 85
    //   #822	-> 86
    //   #823	-> 98
    //   #826	-> 115
    //   #830	-> 129
    //   #828	-> 132
    //   #829	-> 133
    //   #833	-> 138
    //   #835	-> 145
    //   #838	-> 157
    //   #836	-> 160
    //   #837	-> 161
    //   #847	-> 166
    //   #848	-> 176
    //   #850	-> 187
    //   #851	-> 202
    //   #860	-> 218
    //   #861	-> 226
    //   #862	-> 236
    //   #863	-> 255
    //   #864	-> 277
    //   #873	-> 280
    //   #874	-> 295
    //   #875	-> 302
    //   #876	-> 311
    //   #879	-> 317
    //   #881	-> 337
    //   #882	-> 342
    //   #885	-> 349
    //   #888	-> 353
    //   #895	-> 358
    //   #896	-> 383
    //   #900	-> 416
    //   #888	-> 458
    //   #903	-> 458
    //   #905	-> 468
    //   #906	-> 479
    //   #908	-> 492
    //   #913	-> 512
    //   #915	-> 523
    //   #914	-> 528
    //   #916	-> 544
    //   #917	-> 555
    //   #918	-> 562
    //   #917	-> 573
    //   #919	-> 588
    //   #920	-> 596
    //   #927	-> 637
    //   #922	-> 643
    //   #925	-> 645
    //   #936	-> 685
    //   #937	-> 692
    //   #938	-> 699
    //   #939	-> 704
    //   #943	-> 734
    //   #944	-> 739
    //   #937	-> 749
    //   #947	-> 749
    //   #948	-> 756
    //   #952	-> 782
    //   #961	-> 783
    //   #962	-> 808
    //   #967	-> 817
    //   #968	-> 817
    //   #971	-> 824
    //   #973	-> 829
    //   #977	-> 847
    //   #981	-> 886
    //   #983	-> 899
    //   #985	-> 904
    //   #968	-> 910
    //   #988	-> 913
    //   #990	-> 932
    //   #992	-> 937
    //   #994	-> 949
    //   #995	-> 954
    //   #994	-> 957
    //   #995	-> 963
    //   #998	-> 965
    //   #999	-> 986
    //   #1000	-> 994
    //   #1002	-> 1005
    //   #1016	-> 1008
    //   #1017	-> 1026
    //   #1024	-> 1030
    //   #1025	-> 1037
    //   #1028	-> 1063
    // Exception table:
    //   from	to	target	type
    //   115	129	132	android/os/RemoteException
    //   145	157	160	android/os/RemoteException
    //   512	523	643	android/os/RemoteException
    //   523	528	643	android/os/RemoteException
    //   528	544	643	android/os/RemoteException
    //   544	555	643	android/os/RemoteException
    //   555	562	643	android/os/RemoteException
    //   562	573	643	android/os/RemoteException
    //   573	588	643	android/os/RemoteException
    //   596	637	643	android/os/RemoteException
    //   937	949	957	finally
  }
  
  public ClassLoader getClassLoader() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   6: ifnonnull -> 14
    //   9: aload_0
    //   10: aconst_null
    //   11: invokespecial createOrUpdateClassLoaderLocked : (Ljava/util/List;)V
    //   14: aload_0
    //   15: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   18: astore_1
    //   19: aload_0
    //   20: monitorexit
    //   21: aload_1
    //   22: areturn
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1032	-> 0
    //   #1033	-> 2
    //   #1034	-> 9
    //   #1036	-> 14
    //   #1037	-> 23
    // Exception table:
    //   from	to	target	type
    //   2	9	23	finally
    //   9	14	23	finally
    //   14	21	23	finally
    //   24	26	23	finally
  }
  
  private void setupJitProfileSupport() {
    if (!SystemProperties.getBoolean("dalvik.vm.usejitprofiles", false))
      return; 
    BaseDexClassLoader.setReporter(DexLoadReporter.getInstance());
    if (this.mApplicationInfo.uid != Process.myUid())
      return; 
    ArrayList<String> arrayList = new ArrayList();
    if ((this.mApplicationInfo.flags & 0x4) != 0)
      arrayList.add(this.mApplicationInfo.sourceDir); 
    if (this.mApplicationInfo.splitSourceDirs != null)
      Collections.addAll(arrayList, this.mApplicationInfo.splitSourceDirs); 
    if (arrayList.isEmpty())
      return; 
    for (int i = arrayList.size() - 1; i >= 0; i--) {
      if (i == 0) {
        str1 = null;
      } else {
        str1 = this.mApplicationInfo.splitNames[i - 1];
      } 
      String str2 = this.mPackageName;
      int j = UserHandle.myUserId();
      String str1 = ArtManager.getCurrentProfilePath(str2, j, str1);
      VMRuntime.registerAppInfo(str1, new String[] { arrayList.get(i) });
    } 
    DexLoadReporter.getInstance().registerAppDataDir(this.mPackageName, this.mDataDir);
  }
  
  private void initializeJavaContextClassLoader() {
    // Byte code:
    //   0: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   3: pop
    //   4: aload_0
    //   5: getfield mPackageName : Ljava/lang/String;
    //   8: astore_1
    //   9: invokestatic myUserId : ()I
    //   12: istore_2
    //   13: aload_1
    //   14: ldc_w 268435456
    //   17: iload_2
    //   18: invokestatic getPackageInfoAsUserCached : (Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    //   21: astore_1
    //   22: aload_1
    //   23: getfield sharedUserId : Ljava/lang/String;
    //   26: astore_3
    //   27: iconst_1
    //   28: istore #4
    //   30: aload_3
    //   31: ifnull -> 39
    //   34: iconst_1
    //   35: istore_2
    //   36: goto -> 41
    //   39: iconst_0
    //   40: istore_2
    //   41: aload_1
    //   42: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   45: ifnull -> 75
    //   48: aload_0
    //   49: getfield mPackageName : Ljava/lang/String;
    //   52: astore_3
    //   53: aload_1
    //   54: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   57: getfield processName : Ljava/lang/String;
    //   60: astore_1
    //   61: aload_3
    //   62: aload_1
    //   63: invokevirtual equals : (Ljava/lang/Object;)Z
    //   66: ifne -> 75
    //   69: iconst_1
    //   70: istore #5
    //   72: goto -> 78
    //   75: iconst_0
    //   76: istore #5
    //   78: iload #4
    //   80: istore #6
    //   82: iload_2
    //   83: ifne -> 101
    //   86: iload #5
    //   88: ifeq -> 98
    //   91: iload #4
    //   93: istore #6
    //   95: goto -> 101
    //   98: iconst_0
    //   99: istore #6
    //   101: iload #6
    //   103: ifeq -> 118
    //   106: new android/app/LoadedApk$WarningContextClassLoader
    //   109: dup
    //   110: aconst_null
    //   111: invokespecial <init> : (Landroid/app/LoadedApk$1;)V
    //   114: astore_1
    //   115: goto -> 123
    //   118: aload_0
    //   119: getfield mClassLoader : Ljava/lang/ClassLoader;
    //   122: astore_1
    //   123: invokestatic currentThread : ()Ljava/lang/Thread;
    //   126: aload_1
    //   127: invokevirtual setContextClassLoader : (Ljava/lang/ClassLoader;)V
    //   130: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1113	-> 0
    //   #1114	-> 4
    //   #1118	-> 9
    //   #1115	-> 13
    //   #1131	-> 22
    //   #1132	-> 41
    //   #1134	-> 61
    //   #1135	-> 78
    //   #1137	-> 101
    //   #1138	-> 106
    //   #1139	-> 118
    //   #1140	-> 123
    //   #1141	-> 130
  }
  
  private static class WarningContextClassLoader extends ClassLoader {
    private WarningContextClassLoader() {}
    
    private static boolean warned = false;
    
    private void warn(String param1String) {
      if (warned)
        return; 
      warned = true;
      Thread.currentThread().setContextClassLoader(getParent());
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ClassLoader.");
      stringBuilder.append(param1String);
      stringBuilder.append(": The class loader returned by Thread.getContextClassLoader() may fail for processes that host multiple applications. You should explicitly specify a context class loader. For example: Thread.setContextClassLoader(getClass().getClassLoader());");
      Slog.w("ActivityThread", stringBuilder.toString());
    }
    
    public URL getResource(String param1String) {
      warn("getResource");
      return getParent().getResource(param1String);
    }
    
    public Enumeration<URL> getResources(String param1String) throws IOException {
      warn("getResources");
      return getParent().getResources(param1String);
    }
    
    public InputStream getResourceAsStream(String param1String) {
      warn("getResourceAsStream");
      return getParent().getResourceAsStream(param1String);
    }
    
    public Class<?> loadClass(String param1String) throws ClassNotFoundException {
      warn("loadClass");
      return getParent().loadClass(param1String);
    }
    
    public void setClassAssertionStatus(String param1String, boolean param1Boolean) {
      warn("setClassAssertionStatus");
      getParent().setClassAssertionStatus(param1String, param1Boolean);
    }
    
    public void setPackageAssertionStatus(String param1String, boolean param1Boolean) {
      warn("setPackageAssertionStatus");
      getParent().setPackageAssertionStatus(param1String, param1Boolean);
    }
    
    public void setDefaultAssertionStatus(boolean param1Boolean) {
      warn("setDefaultAssertionStatus");
      getParent().setDefaultAssertionStatus(param1Boolean);
    }
    
    public void clearAssertionStatus() {
      warn("clearAssertionStatus");
      getParent().clearAssertionStatus();
    }
  }
  
  public String getAppDir() {
    return this.mAppDir;
  }
  
  public String getLibDir() {
    return this.mLibDir;
  }
  
  public String getResDir() {
    return this.mResDir;
  }
  
  public String[] getSplitAppDirs() {
    return this.mSplitAppDirs;
  }
  
  public String[] getSplitResDirs() {
    return this.mSplitResDirs;
  }
  
  public String[] getOverlayDirs() {
    return this.mOverlayDirs;
  }
  
  public String getDataDir() {
    return this.mDataDir;
  }
  
  public File getDataDirFile() {
    return this.mDataDirFile;
  }
  
  public File getDeviceProtectedDataDirFile() {
    return this.mDeviceProtectedDataDirFile;
  }
  
  public File getCredentialProtectedDataDirFile() {
    return this.mCredentialProtectedDataDirFile;
  }
  
  public AssetManager getAssets() {
    return getResources().getAssets();
  }
  
  public Resources getResources() {
    if (this.mResources == null)
      try {
        String[] arrayOfString1 = getSplitPaths(null);
        ResourcesManager resourcesManager = ResourcesManager.getInstance();
        String str = this.mResDir, arrayOfString2[] = this.mOverlayDirs, arrayOfString3[] = this.mApplicationInfo.sharedLibraryFiles;
        CompatibilityInfo compatibilityInfo = getCompatibilityInfo();
        ClassLoader classLoader = getClassLoader();
        this.mResources = resourcesManager.getResources(null, str, arrayOfString1, arrayOfString2, arrayOfString3, 0, null, compatibilityInfo, classLoader, null);
        ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(this.mResources, this.mPackageName);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        throw new AssertionError("null split not found");
      }  
    return this.mResources;
  }
  
  public Application makeApplication(boolean paramBoolean, Instrumentation paramInstrumentation) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mApplication : Landroid/app/Application;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnull -> 11
    //   9: aload_3
    //   10: areturn
    //   11: ldc2_w 64
    //   14: ldc_w 'makeApplication'
    //   17: invokestatic traceBegin : (JLjava/lang/String;)V
    //   20: aconst_null
    //   21: astore #4
    //   23: aload_0
    //   24: getfield mApplicationInfo : Landroid/content/pm/ApplicationInfo;
    //   27: getfield className : Ljava/lang/String;
    //   30: astore_3
    //   31: iload_1
    //   32: ifne -> 42
    //   35: aload_3
    //   36: astore #5
    //   38: aload_3
    //   39: ifnonnull -> 47
    //   42: ldc_w 'android.app.Application'
    //   45: astore #5
    //   47: aload #4
    //   49: astore_3
    //   50: aload_0
    //   51: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   54: astore #6
    //   56: aload #4
    //   58: astore_3
    //   59: aload_0
    //   60: getfield mPackageName : Ljava/lang/String;
    //   63: ldc 'android'
    //   65: invokevirtual equals : (Ljava/lang/Object;)Z
    //   68: ifne -> 99
    //   71: aload #4
    //   73: astore_3
    //   74: ldc2_w 64
    //   77: ldc_w 'initializeJavaContextClassLoader'
    //   80: invokestatic traceBegin : (JLjava/lang/String;)V
    //   83: aload #4
    //   85: astore_3
    //   86: aload_0
    //   87: invokespecial initializeJavaContextClassLoader : ()V
    //   90: aload #4
    //   92: astore_3
    //   93: ldc2_w 64
    //   96: invokestatic traceEnd : (J)V
    //   99: aload #4
    //   101: astore_3
    //   102: aload_0
    //   103: invokevirtual getAssets : ()Landroid/content/res/AssetManager;
    //   106: iconst_0
    //   107: iconst_0
    //   108: invokevirtual getAssignedPackageIdentifiers : (ZZ)Landroid/util/SparseArray;
    //   111: astore #7
    //   113: iconst_0
    //   114: istore #8
    //   116: aload #4
    //   118: astore_3
    //   119: aload #7
    //   121: invokevirtual size : ()I
    //   124: istore #9
    //   126: iload #8
    //   128: iload #9
    //   130: if_icmpge -> 188
    //   133: aload #4
    //   135: astore_3
    //   136: aload #7
    //   138: iload #8
    //   140: invokevirtual keyAt : (I)I
    //   143: istore #10
    //   145: iload #10
    //   147: iconst_1
    //   148: if_icmpeq -> 182
    //   151: iload #10
    //   153: bipush #127
    //   155: if_icmpne -> 161
    //   158: goto -> 182
    //   161: aload #4
    //   163: astore_3
    //   164: aload_0
    //   165: aload #6
    //   167: aload #7
    //   169: iload #8
    //   171: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   174: checkcast java/lang/String
    //   177: iload #10
    //   179: invokespecial rewriteRValues : (Ljava/lang/ClassLoader;Ljava/lang/String;I)V
    //   182: iinc #8, 1
    //   185: goto -> 126
    //   188: aload #4
    //   190: astore_3
    //   191: aload_0
    //   192: getfield mActivityThread : Landroid/app/ActivityThread;
    //   195: aload_0
    //   196: invokestatic createAppContext : (Landroid/app/ActivityThread;Landroid/app/LoadedApk;)Landroid/app/ContextImpl;
    //   199: astore #7
    //   201: aload #4
    //   203: astore_3
    //   204: aload #7
    //   206: invokestatic handleNewApplication : (Landroid/content/Context;)V
    //   209: aload #4
    //   211: astore_3
    //   212: aload_0
    //   213: getfield mActivityThread : Landroid/app/ActivityThread;
    //   216: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   219: aload #6
    //   221: aload #5
    //   223: aload #7
    //   225: invokevirtual newApplication : (Ljava/lang/ClassLoader;Ljava/lang/String;Landroid/content/Context;)Landroid/app/Application;
    //   228: astore #4
    //   230: aload #4
    //   232: astore_3
    //   233: aload #7
    //   235: aload #4
    //   237: invokevirtual setOuterContext : (Landroid/content/Context;)V
    //   240: aload #4
    //   242: astore_3
    //   243: goto -> 264
    //   246: astore #4
    //   248: aload_0
    //   249: getfield mActivityThread : Landroid/app/ActivityThread;
    //   252: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   255: aload_3
    //   256: aload #4
    //   258: invokevirtual onException : (Ljava/lang/Object;Ljava/lang/Throwable;)Z
    //   261: ifeq -> 382
    //   264: aload_0
    //   265: getfield mActivityThread : Landroid/app/ActivityThread;
    //   268: getfield mAllApplications : Ljava/util/ArrayList;
    //   271: aload_3
    //   272: invokevirtual add : (Ljava/lang/Object;)Z
    //   275: pop
    //   276: aload_0
    //   277: aload_3
    //   278: putfield mApplication : Landroid/app/Application;
    //   281: aload_2
    //   282: ifnull -> 374
    //   285: aload_2
    //   286: aload_3
    //   287: invokevirtual callApplicationOnCreate : (Landroid/app/Application;)V
    //   290: goto -> 374
    //   293: astore #5
    //   295: aload_2
    //   296: aload_3
    //   297: aload #5
    //   299: invokevirtual onException : (Ljava/lang/Object;Ljava/lang/Throwable;)Z
    //   302: ifeq -> 308
    //   305: goto -> 374
    //   308: ldc2_w 64
    //   311: invokestatic traceEnd : (J)V
    //   314: new java/lang/StringBuilder
    //   317: dup
    //   318: invokespecial <init> : ()V
    //   321: astore_2
    //   322: aload_2
    //   323: ldc_w 'Unable to create application '
    //   326: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   329: pop
    //   330: aload_2
    //   331: aload_3
    //   332: invokevirtual getClass : ()Ljava/lang/Class;
    //   335: invokevirtual getName : ()Ljava/lang/String;
    //   338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload_2
    //   343: ldc_w ': '
    //   346: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   349: pop
    //   350: aload_2
    //   351: aload #5
    //   353: invokevirtual toString : ()Ljava/lang/String;
    //   356: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   359: pop
    //   360: new java/lang/RuntimeException
    //   363: dup
    //   364: aload_2
    //   365: invokevirtual toString : ()Ljava/lang/String;
    //   368: aload #5
    //   370: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   373: athrow
    //   374: ldc2_w 64
    //   377: invokestatic traceEnd : (J)V
    //   380: aload_3
    //   381: areturn
    //   382: ldc2_w 64
    //   385: invokestatic traceEnd : (J)V
    //   388: new java/lang/StringBuilder
    //   391: dup
    //   392: invokespecial <init> : ()V
    //   395: astore_2
    //   396: aload_2
    //   397: ldc_w 'Unable to instantiate application '
    //   400: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   403: pop
    //   404: aload_2
    //   405: aload #5
    //   407: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   410: pop
    //   411: aload_2
    //   412: ldc_w ': '
    //   415: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload_2
    //   420: aload #4
    //   422: invokevirtual toString : ()Ljava/lang/String;
    //   425: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   428: pop
    //   429: new java/lang/RuntimeException
    //   432: dup
    //   433: aload_2
    //   434: invokevirtual toString : ()Ljava/lang/String;
    //   437: aload #4
    //   439: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   442: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1278	-> 0
    //   #1279	-> 9
    //   #1282	-> 11
    //   #1284	-> 20
    //   #1286	-> 23
    //   #1287	-> 31
    //   #1288	-> 42
    //   #1292	-> 47
    //   #1293	-> 56
    //   #1294	-> 71
    //   #1296	-> 83
    //   #1297	-> 90
    //   #1301	-> 99
    //   #1303	-> 113
    //   #1304	-> 133
    //   #1305	-> 145
    //   #1306	-> 158
    //   #1309	-> 161
    //   #1303	-> 182
    //   #1312	-> 188
    //   #1315	-> 201
    //   #1316	-> 209
    //   #1318	-> 230
    //   #1326	-> 240
    //   #1319	-> 246
    //   #1320	-> 248
    //   #1327	-> 264
    //   #1328	-> 276
    //   #1330	-> 281
    //   #1332	-> 285
    //   #1340	-> 290
    //   #1333	-> 293
    //   #1334	-> 295
    //   #1335	-> 308
    //   #1336	-> 314
    //   #1337	-> 330
    //   #1338	-> 350
    //   #1343	-> 374
    //   #1345	-> 380
    //   #1321	-> 382
    //   #1322	-> 388
    //   #1324	-> 419
    // Exception table:
    //   from	to	target	type
    //   50	56	246	java/lang/Exception
    //   59	71	246	java/lang/Exception
    //   74	83	246	java/lang/Exception
    //   86	90	246	java/lang/Exception
    //   93	99	246	java/lang/Exception
    //   102	113	246	java/lang/Exception
    //   119	126	246	java/lang/Exception
    //   136	145	246	java/lang/Exception
    //   164	182	246	java/lang/Exception
    //   191	201	246	java/lang/Exception
    //   204	209	246	java/lang/Exception
    //   212	230	246	java/lang/Exception
    //   233	240	246	java/lang/Exception
    //   285	290	293	java/lang/Exception
  }
  
  private void rewriteRValues(ClassLoader paramClassLoader, String paramString, int paramInt) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(paramString);
      stringBuilder.append(".R");
      Class<?> clazz = paramClassLoader.loadClass(stringBuilder.toString());
      try {
        Throwable throwable;
        Method method = clazz.getMethod("onResourcesLoaded", new Class[] { int.class });
        try {
          method.invoke(null, new Object[] { Integer.valueOf(paramInt) });
          return;
        } catch (IllegalAccessException null) {
        
        } catch (InvocationTargetException invocationTargetException) {
          throwable = invocationTargetException.getCause();
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to rewrite resource references for ");
        stringBuilder.append(paramString);
        throw new RuntimeException(stringBuilder.toString(), throwable);
      } catch (NoSuchMethodException noSuchMethodException) {
        return;
      } 
    } catch (ClassNotFoundException classNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No resource references to update in package ");
      stringBuilder.append(paramString);
      Log.i("LoadedApk", stringBuilder.toString());
      return;
    } 
  }
  
  public void removeContextRegistrations(Context paramContext, String paramString1, String paramString2) {
    boolean bool = StrictMode.vmRegistrationLeaksEnabled();
    synchronized (this.mReceivers) {
      ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> arrayMap = this.mReceivers;
      arrayMap = (ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>>)arrayMap.remove(paramContext);
      if (arrayMap != null)
        for (byte b = 0; b < arrayMap.size(); ) {
          ReceiverDispatcher receiverDispatcher = (ReceiverDispatcher)arrayMap.valueAt(b);
          IntentReceiverLeaked intentReceiverLeaked = new IntentReceiverLeaked();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(paramString2);
          stringBuilder.append(" ");
          stringBuilder.append(paramString1);
          stringBuilder.append(" has leaked IntentReceiver ");
          stringBuilder.append(receiverDispatcher.getIntentReceiver());
          stringBuilder.append(" that was originally registered here. Are you missing a call to unregisterReceiver()?");
          this(stringBuilder.toString());
          intentReceiverLeaked.setStackTrace(receiverDispatcher.getLocation().getStackTrace());
          Slog.e("ActivityThread", intentReceiverLeaked.getMessage(), (Throwable)intentReceiverLeaked);
          if (bool)
            StrictMode.onIntentReceiverLeaked((Throwable)intentReceiverLeaked); 
          try {
            IActivityManager iActivityManager = ActivityManager.getService();
            IIntentReceiver iIntentReceiver = receiverDispatcher.getIIntentReceiver();
            iActivityManager.unregisterReceiver(iIntentReceiver);
            b++;
          } catch (RemoteException null) {
            throw null.rethrowFromSystemServer();
          } 
        }  
      this.mUnregisteredReceivers.remove(null);
      synchronized (this.mServices) {
        ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>> arrayMap1 = this.mServices;
        arrayMap1 = (ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>>)arrayMap1.remove(null);
        if (arrayMap1 != null)
          for (byte b = 0; b < arrayMap1.size(); ) {
            ServiceDispatcher serviceDispatcher = (ServiceDispatcher)arrayMap1.valueAt(b);
            ServiceConnectionLeaked serviceConnectionLeaked = new ServiceConnectionLeaked();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(paramString2);
            stringBuilder.append(" ");
            stringBuilder.append(paramString1);
            stringBuilder.append(" has leaked ServiceConnection ");
            stringBuilder.append(serviceDispatcher.getServiceConnection());
            stringBuilder.append(" that was originally bound here");
            this(stringBuilder.toString());
            serviceConnectionLeaked.setStackTrace(serviceDispatcher.getLocation().getStackTrace());
            Slog.e("ActivityThread", serviceConnectionLeaked.getMessage(), (Throwable)serviceConnectionLeaked);
            if (bool)
              StrictMode.onServiceConnectionLeaked((Throwable)serviceConnectionLeaked); 
            try {
              IActivityManager iActivityManager = ActivityManager.getService();
              IServiceConnection iServiceConnection = serviceDispatcher.getIServiceConnection();
              iActivityManager.unbindService(iServiceConnection);
              serviceDispatcher.doForget();
              b++;
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            } 
          }  
        this.mUnboundServices.remove(remoteException);
        return;
      } 
    } 
  }
  
  public IIntentReceiver getReceiverDispatcher(BroadcastReceiver paramBroadcastReceiver, Context paramContext, Handler paramHandler, Instrumentation paramInstrumentation, boolean paramBoolean) {
    ReceiverDispatcher receiverDispatcher1;
    ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> arrayMap = this.mReceivers;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/ArrayMap<[ObjectType{android/content/Context}, ObjectType{android/util/ArrayMap<[ObjectType{android/content/BroadcastReceiver}, InnerObjectType{ObjectType{android/app/LoadedApk}.Landroid/app/LoadedApk$ReceiverDispatcher;}]>}]>}, name=null} */
    ReceiverDispatcher receiverDispatcher2 = null;
    ArrayMap arrayMap1 = null;
    ReceiverDispatcher receiverDispatcher3 = receiverDispatcher2;
    if (paramBoolean)
      try {
        ArrayMap arrayMap2 = (ArrayMap)this.mReceivers.get(paramContext);
        receiverDispatcher3 = receiverDispatcher2;
        arrayMap1 = arrayMap2;
        if (arrayMap2 != null) {
          receiverDispatcher3 = (ReceiverDispatcher)arrayMap2.get(paramBroadcastReceiver);
          arrayMap1 = arrayMap2;
        } 
      } finally {} 
    if (receiverDispatcher3 == null) {
      receiverDispatcher3 = new ReceiverDispatcher();
      this(paramBroadcastReceiver, paramContext, paramHandler, paramInstrumentation, paramBoolean);
      receiverDispatcher1 = receiverDispatcher3;
      receiverDispatcher3 = receiverDispatcher1;
      if (paramBoolean) {
        ArrayMap arrayMap2 = arrayMap1;
        if (arrayMap1 == null) {
          arrayMap2 = new ArrayMap();
          this();
          this.mReceivers.put(paramContext, arrayMap2);
        } 
        arrayMap2.put(paramBroadcastReceiver, receiverDispatcher1);
        receiverDispatcher3 = receiverDispatcher1;
      } 
    } else {
      receiverDispatcher3.validate(paramContext, (Handler)receiverDispatcher1);
    } 
    receiverDispatcher3.mForgotten = false;
    IIntentReceiver iIntentReceiver = receiverDispatcher3.getIIntentReceiver();
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{android/util/ArrayMap<[ObjectType{android/content/Context}, ObjectType{android/util/ArrayMap<[ObjectType{android/content/BroadcastReceiver}, InnerObjectType{ObjectType{android/app/LoadedApk}.Landroid/app/LoadedApk$ReceiverDispatcher;}]>}]>}, name=null} */
    return iIntentReceiver;
  }
  
  public IIntentReceiver forgetReceiverDispatcher(Context paramContext, BroadcastReceiver paramBroadcastReceiver) {
    synchronized (this.mReceivers) {
      IIntentReceiver iIntentReceiver;
      ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> arrayMap = (ArrayMap)this.mReceivers.get(paramContext);
      if (arrayMap != null) {
        ReceiverDispatcher receiverDispatcher = (ReceiverDispatcher)arrayMap.get(paramBroadcastReceiver);
        if (receiverDispatcher != null) {
          arrayMap.remove(paramBroadcastReceiver);
          if (arrayMap.size() == 0)
            this.mReceivers.remove(paramContext); 
          if (paramBroadcastReceiver.getDebugUnregister()) {
            arrayMap = this.mUnregisteredReceivers;
            ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>> arrayMap1 = (ArrayMap)arrayMap.get(paramContext);
            arrayMap = arrayMap1;
            if (arrayMap1 == null) {
              arrayMap = new ArrayMap();
              this();
              this.mUnregisteredReceivers.put(paramContext, arrayMap);
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            this("Originally unregistered here:");
            illegalArgumentException1.fillInStackTrace();
            receiverDispatcher.setUnregisterLocation(illegalArgumentException1);
            arrayMap.put(paramBroadcastReceiver, receiverDispatcher);
          } 
          receiverDispatcher.mForgotten = true;
          iIntentReceiver = receiverDispatcher.getIIntentReceiver();
          return iIntentReceiver;
        } 
      } 
      arrayMap = this.mUnregisteredReceivers;
      arrayMap = (ArrayMap<Context, ArrayMap<BroadcastReceiver, ReceiverDispatcher>>)arrayMap.get(iIntentReceiver);
      if (arrayMap != null) {
        ReceiverDispatcher receiverDispatcher = (ReceiverDispatcher)arrayMap.get(paramBroadcastReceiver);
        if (receiverDispatcher != null) {
          RuntimeException runtimeException = receiverDispatcher.getUnregisterLocation();
          IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unregistering Receiver ");
          stringBuilder.append(paramBroadcastReceiver);
          stringBuilder.append(" that was already unregistered");
          this(stringBuilder.toString(), runtimeException);
          throw illegalArgumentException1;
        } 
      } 
      if (stringBuilder == null) {
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Unbinding Receiver ");
        stringBuilder1.append(paramBroadcastReceiver);
        stringBuilder1.append(" from Context that is no longer in use: ");
        stringBuilder1.append(stringBuilder);
        this(stringBuilder1.toString());
        throw illegalStateException;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Receiver not registered: ");
      stringBuilder.append(paramBroadcastReceiver);
      this(stringBuilder.toString());
      throw illegalArgumentException;
    } 
  }
  
  class ReceiverDispatcher {
    final Handler mActivityThread;
    
    final Context mContext;
    
    boolean mForgotten;
    
    final IIntentReceiver.Stub mIIntentReceiver;
    
    final Instrumentation mInstrumentation;
    
    final IntentReceiverLeaked mLocation;
    
    final BroadcastReceiver mReceiver;
    
    final boolean mRegistered;
    
    RuntimeException mUnregisterLocation;
    
    class InnerReceiver extends IIntentReceiver.Stub {
      final WeakReference<LoadedApk.ReceiverDispatcher> mDispatcher;
      
      final LoadedApk.ReceiverDispatcher mStrongRef;
      
      InnerReceiver(LoadedApk.ReceiverDispatcher this$0, boolean param2Boolean) {
        this.mDispatcher = new WeakReference<>(this$0);
        if (!param2Boolean)
          this$0 = null; 
        this.mStrongRef = this$0;
      }
      
      public void performReceive(Intent param2Intent, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean1, boolean param2Boolean2, int param2Int2) {
        LoadedApk.ReceiverDispatcher receiverDispatcher;
        if (param2Intent == null) {
          Log.wtf("LoadedApk", "Null intent received");
          receiverDispatcher = null;
        } else {
          receiverDispatcher = this.mDispatcher.get();
        } 
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Receiving broadcast ");
          stringBuilder.append(param2Intent.getAction());
          stringBuilder.append(" to ");
          if (receiverDispatcher != null) {
            str = (String)receiverDispatcher.mReceiver;
          } else {
            str = null;
          } 
          stringBuilder.append(str);
          String str = stringBuilder.toString();
          Slog.i("ActivityThread", str);
        } 
        if (receiverDispatcher != null) {
          receiverDispatcher.performReceive(param2Intent, param2Int1, param2String, param2Bundle, param2Boolean1, param2Boolean2, param2Int2);
        } else {
          if (ActivityThread.DEBUG_BROADCAST)
            Slog.i("ActivityThread", "Finishing broadcast to unregistered receiver"); 
          IActivityManager iActivityManager = ActivityManager.getService();
          if (param2Bundle != null)
            try {
              param2Bundle.setAllowFds(false);
              iActivityManager.finishReceiver((IBinder)this, param2Int1, param2String, param2Bundle, false, param2Intent.getFlags());
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            }  
          iActivityManager.finishReceiver((IBinder)this, param2Int1, param2String, param2Bundle, false, remoteException.getFlags());
        } 
      }
    }
    
    class Args extends BroadcastReceiver.PendingResult {
      private Intent mCurIntent;
      
      private boolean mDispatched;
      
      private final boolean mOrdered;
      
      private boolean mRunCalled;
      
      final LoadedApk.ReceiverDispatcher this$0;
      
      public Args(Intent param2Intent, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean1, boolean param2Boolean2, int param2Int2) {
        super(param2Int1, param2String, param2Bundle, b, param2Boolean1, param2Boolean2, iBinder, param2Int2, i);
        byte b;
        this.mCurIntent = param2Intent;
        this.mOrdered = param2Boolean1;
      }
      
      public final Runnable getRunnable() {
        return new _$$Lambda$LoadedApk$ReceiverDispatcher$Args$_BumDX2UKsnxLVrE6UJsJZkotuA(this);
      }
    }
    
    ReceiverDispatcher(LoadedApk this$0, Context param1Context, Handler param1Handler, Instrumentation param1Instrumentation, boolean param1Boolean) {
      if (param1Handler != null) {
        this.mIIntentReceiver = new InnerReceiver(this, param1Boolean ^ true);
        this.mReceiver = (BroadcastReceiver)this$0;
        this.mContext = param1Context;
        this.mActivityThread = param1Handler;
        this.mInstrumentation = param1Instrumentation;
        this.mRegistered = param1Boolean;
        IntentReceiverLeaked intentReceiverLeaked = new IntentReceiverLeaked(null);
        intentReceiverLeaked.fillInStackTrace();
        return;
      } 
      throw new NullPointerException("Handler must not be null");
    }
    
    void validate(Context param1Context, Handler param1Handler) {
      StringBuilder stringBuilder1;
      if (this.mContext == param1Context) {
        if (this.mActivityThread == param1Handler)
          return; 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Receiver ");
        stringBuilder1.append(this.mReceiver);
        stringBuilder1.append(" registered with differing handler (was ");
        stringBuilder1.append(this.mActivityThread);
        stringBuilder1.append(" now ");
        stringBuilder1.append(param1Handler);
        stringBuilder1.append(")");
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Receiver ");
      stringBuilder2.append(this.mReceiver);
      stringBuilder2.append(" registered with differing Context (was ");
      stringBuilder2.append(this.mContext);
      stringBuilder2.append(" now ");
      stringBuilder2.append(stringBuilder1);
      stringBuilder2.append(")");
      throw new IllegalStateException(stringBuilder2.toString());
    }
    
    IntentReceiverLeaked getLocation() {
      return this.mLocation;
    }
    
    BroadcastReceiver getIntentReceiver() {
      return this.mReceiver;
    }
    
    IIntentReceiver getIIntentReceiver() {
      return this.mIIntentReceiver;
    }
    
    void setUnregisterLocation(RuntimeException param1RuntimeException) {
      this.mUnregisterLocation = param1RuntimeException;
    }
    
    RuntimeException getUnregisterLocation() {
      return this.mUnregisterLocation;
    }
    
    public void performReceive(Intent param1Intent, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean1, boolean param1Boolean2, int param1Int2) {
      Args args = new Args(param1Intent, param1Int1, param1String, param1Bundle, param1Boolean1, param1Boolean2, param1Int2);
      if (param1Intent == null) {
        Log.wtf("LoadedApk", "Null intent received");
      } else {
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Enqueueing broadcast ");
          stringBuilder.append(param1Intent.getAction());
          stringBuilder.append(" to ");
          stringBuilder.append(this.mReceiver);
          Slog.i("ActivityThread", stringBuilder.toString());
        } 
        if (param1Intent != null && param1Boolean1) {
          param1Int1 = param1Intent.getFlags();
          args.setBroadcastState(param1Int1, 1);
        } 
      } 
      if ((param1Intent == null || !this.mActivityThread.post(args.getRunnable())) && 
        this.mRegistered && param1Boolean1) {
        IActivityManager iActivityManager = ActivityManager.getService();
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Finishing sync broadcast to ");
          stringBuilder.append(this.mReceiver);
          Slog.i("ActivityThread", stringBuilder.toString());
        } 
        args.sendFinished(iActivityManager);
      } 
    }
  }
  
  public final IServiceConnection getServiceDispatcher(ServiceConnection paramServiceConnection, Context paramContext, Handler paramHandler, int paramInt) {
    return getServiceDispatcherCommon(paramServiceConnection, paramContext, paramHandler, null, paramInt);
  }
  
  public final IServiceConnection getServiceDispatcher(ServiceConnection paramServiceConnection, Context paramContext, Executor paramExecutor, int paramInt) {
    return getServiceDispatcherCommon(paramServiceConnection, paramContext, null, paramExecutor, paramInt);
  }
  
  private IServiceConnection getServiceDispatcherCommon(ServiceConnection paramServiceConnection, Context paramContext, Handler paramHandler, Executor paramExecutor, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mServices : Landroid/util/ArrayMap;
    //   4: astore #6
    //   6: aload #6
    //   8: monitorenter
    //   9: aconst_null
    //   10: astore #7
    //   12: aload_0
    //   13: getfield mServices : Landroid/util/ArrayMap;
    //   16: aload_2
    //   17: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   20: checkcast android/util/ArrayMap
    //   23: astore #8
    //   25: aload #8
    //   27: ifnull -> 41
    //   30: aload #8
    //   32: aload_1
    //   33: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   36: checkcast android/app/LoadedApk$ServiceDispatcher
    //   39: astore #7
    //   41: aload #7
    //   43: ifnonnull -> 122
    //   46: aload #4
    //   48: ifnull -> 68
    //   51: new android/app/LoadedApk$ServiceDispatcher
    //   54: astore_3
    //   55: aload_3
    //   56: aload_1
    //   57: aload_2
    //   58: aload #4
    //   60: iload #5
    //   62: invokespecial <init> : (Landroid/content/ServiceConnection;Landroid/content/Context;Ljava/util/concurrent/Executor;I)V
    //   65: goto -> 81
    //   68: new android/app/LoadedApk$ServiceDispatcher
    //   71: dup
    //   72: aload_1
    //   73: aload_2
    //   74: aload_3
    //   75: iload #5
    //   77: invokespecial <init> : (Landroid/content/ServiceConnection;Landroid/content/Context;Landroid/os/Handler;I)V
    //   80: astore_3
    //   81: aload #8
    //   83: astore #4
    //   85: aload #8
    //   87: ifnonnull -> 111
    //   90: new android/util/ArrayMap
    //   93: astore #4
    //   95: aload #4
    //   97: invokespecial <init> : ()V
    //   100: aload_0
    //   101: getfield mServices : Landroid/util/ArrayMap;
    //   104: aload_2
    //   105: aload #4
    //   107: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   110: pop
    //   111: aload #4
    //   113: aload_1
    //   114: aload_3
    //   115: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   118: pop
    //   119: goto -> 134
    //   122: aload #7
    //   124: aload_2
    //   125: aload_3
    //   126: aload #4
    //   128: invokevirtual validate : (Landroid/content/Context;Landroid/os/Handler;Ljava/util/concurrent/Executor;)V
    //   131: aload #7
    //   133: astore_3
    //   134: aload_3
    //   135: invokevirtual getIServiceConnection : ()Landroid/app/IServiceConnection;
    //   138: astore_1
    //   139: aload #6
    //   141: monitorexit
    //   142: aload_1
    //   143: areturn
    //   144: astore_1
    //   145: aload #6
    //   147: monitorexit
    //   148: aload_1
    //   149: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1828	-> 0
    //   #1829	-> 9
    //   #1830	-> 12
    //   #1831	-> 25
    //   #1833	-> 30
    //   #1835	-> 41
    //   #1836	-> 46
    //   #1837	-> 51
    //   #1839	-> 68
    //   #1842	-> 81
    //   #1843	-> 90
    //   #1844	-> 100
    //   #1846	-> 111
    //   #1848	-> 122
    //   #1850	-> 134
    //   #1851	-> 144
    // Exception table:
    //   from	to	target	type
    //   12	25	144	finally
    //   30	41	144	finally
    //   51	65	144	finally
    //   68	81	144	finally
    //   90	100	144	finally
    //   100	111	144	finally
    //   111	119	144	finally
    //   122	131	144	finally
    //   134	142	144	finally
    //   145	148	144	finally
  }
  
  public IServiceConnection lookupServiceDispatcher(ServiceConnection paramServiceConnection, Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mServices : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aconst_null
    //   8: astore #4
    //   10: aload_0
    //   11: getfield mServices : Landroid/util/ArrayMap;
    //   14: aload_2
    //   15: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   18: checkcast android/util/ArrayMap
    //   21: astore #5
    //   23: aload #4
    //   25: astore_2
    //   26: aload #5
    //   28: ifnull -> 41
    //   31: aload #5
    //   33: aload_1
    //   34: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   37: checkcast android/app/LoadedApk$ServiceDispatcher
    //   40: astore_2
    //   41: aload_2
    //   42: ifnull -> 53
    //   45: aload_2
    //   46: invokevirtual getIServiceConnection : ()Landroid/app/IServiceConnection;
    //   49: astore_1
    //   50: goto -> 55
    //   53: aconst_null
    //   54: astore_1
    //   55: aload_3
    //   56: monitorexit
    //   57: aload_1
    //   58: areturn
    //   59: astore_1
    //   60: aload_3
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1857	-> 0
    //   #1858	-> 7
    //   #1859	-> 10
    //   #1860	-> 23
    //   #1861	-> 31
    //   #1863	-> 41
    //   #1864	-> 59
    // Exception table:
    //   from	to	target	type
    //   10	23	59	finally
    //   31	41	59	finally
    //   45	50	59	finally
    //   55	57	59	finally
    //   60	62	59	finally
  }
  
  public final IServiceConnection forgetServiceDispatcher(Context paramContext, ServiceConnection paramServiceConnection) {
    synchronized (this.mServices) {
      IServiceConnection iServiceConnection;
      StringBuilder stringBuilder1;
      ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>> arrayMap = this.mServices;
      arrayMap = (ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>>)arrayMap.get(paramContext);
      if (arrayMap != null) {
        ServiceDispatcher serviceDispatcher = (ServiceDispatcher)arrayMap.get(paramServiceConnection);
        if (serviceDispatcher != null) {
          arrayMap.remove(paramServiceConnection);
          serviceDispatcher.doForget();
          if (arrayMap.size() == 0)
            this.mServices.remove(paramContext); 
          if ((serviceDispatcher.getFlags() & 0x2) != 0) {
            arrayMap = this.mUnboundServices;
            ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>> arrayMap1 = (ArrayMap)arrayMap.get(paramContext);
            arrayMap = arrayMap1;
            if (arrayMap1 == null) {
              arrayMap = new ArrayMap();
              this();
              this.mUnboundServices.put(paramContext, arrayMap);
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            this("Originally unbound here:");
            illegalArgumentException1.fillInStackTrace();
            serviceDispatcher.setUnbindLocation(illegalArgumentException1);
            arrayMap.put(paramServiceConnection, serviceDispatcher);
          } 
          iServiceConnection = serviceDispatcher.getIServiceConnection();
          return iServiceConnection;
        } 
      } 
      arrayMap = this.mUnboundServices;
      arrayMap = (ArrayMap<Context, ArrayMap<ServiceConnection, ServiceDispatcher>>)arrayMap.get(iServiceConnection);
      if (arrayMap != null) {
        ServiceDispatcher serviceDispatcher = (ServiceDispatcher)arrayMap.get(paramServiceConnection);
        if (serviceDispatcher != null) {
          RuntimeException runtimeException = serviceDispatcher.getUnbindLocation();
          IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Unbinding Service ");
          stringBuilder1.append(paramServiceConnection);
          stringBuilder1.append(" that was already unbound");
          this(stringBuilder1.toString(), runtimeException);
          throw illegalArgumentException1;
        } 
      } 
      if (stringBuilder1 == null) {
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unbinding Service ");
        stringBuilder.append(paramServiceConnection);
        stringBuilder.append(" from Context that is no longer in use: ");
        stringBuilder.append(stringBuilder1);
        this(stringBuilder.toString());
        throw illegalStateException;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder2 = new StringBuilder();
      this();
      stringBuilder2.append("Service not registered: ");
      stringBuilder2.append(paramServiceConnection);
      this(stringBuilder2.toString());
      throw illegalArgumentException;
    } 
  }
  
  static final class ServiceDispatcher {
    private static class ConnectionInfo {
      IBinder binder;
      
      IBinder.DeathRecipient deathMonitor;
      
      private ConnectionInfo() {}
    }
    
    class InnerConnection extends IServiceConnection.Stub {
      final WeakReference<LoadedApk.ServiceDispatcher> mDispatcher;
      
      InnerConnection(LoadedApk.ServiceDispatcher this$0) {
        this.mDispatcher = new WeakReference<>(this$0);
      }
      
      public void connected(ComponentName param2ComponentName, IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        LoadedApk.ServiceDispatcher serviceDispatcher = this.mDispatcher.get();
        if (serviceDispatcher != null)
          serviceDispatcher.connected(param2ComponentName, param2IBinder, param2Boolean); 
      }
    }
    
    private final ArrayMap<ComponentName, ConnectionInfo> mActiveConnections = new ArrayMap();
    
    private final Executor mActivityExecutor;
    
    private final Handler mActivityThread;
    
    private final ServiceConnection mConnection;
    
    private final Context mContext;
    
    private final int mFlags;
    
    private boolean mForgotten;
    
    private final InnerConnection mIServiceConnection;
    
    private final ServiceConnectionLeaked mLocation;
    
    private RuntimeException mUnbindLocation;
    
    ServiceDispatcher(ServiceConnection param1ServiceConnection, Context param1Context, Handler param1Handler, int param1Int) {
      this.mIServiceConnection = new InnerConnection(this);
      this.mConnection = param1ServiceConnection;
      this.mContext = param1Context;
      this.mActivityThread = param1Handler;
      this.mActivityExecutor = null;
      ServiceConnectionLeaked serviceConnectionLeaked = new ServiceConnectionLeaked(null);
      serviceConnectionLeaked.fillInStackTrace();
      this.mFlags = param1Int;
    }
    
    ServiceDispatcher(ServiceConnection param1ServiceConnection, Context param1Context, Executor param1Executor, int param1Int) {
      this.mIServiceConnection = new InnerConnection(this);
      this.mConnection = param1ServiceConnection;
      this.mContext = param1Context;
      this.mActivityThread = null;
      this.mActivityExecutor = param1Executor;
      ServiceConnectionLeaked serviceConnectionLeaked = new ServiceConnectionLeaked(null);
      serviceConnectionLeaked.fillInStackTrace();
      this.mFlags = param1Int;
    }
    
    void validate(Context param1Context, Handler param1Handler, Executor param1Executor) {
      StringBuilder stringBuilder1;
      if (this.mContext == param1Context) {
        if (this.mActivityThread == param1Handler) {
          if (this.mActivityExecutor == param1Executor)
            return; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("ServiceConnection ");
          stringBuilder.append(this.mConnection);
          stringBuilder.append(" registered with differing executor (was ");
          stringBuilder.append(this.mActivityExecutor);
          stringBuilder.append(" now ");
          stringBuilder.append(param1Executor);
          stringBuilder.append(")");
          throw new RuntimeException(stringBuilder.toString());
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("ServiceConnection ");
        stringBuilder1.append(this.mConnection);
        stringBuilder1.append(" registered with differing handler (was ");
        stringBuilder1.append(this.mActivityThread);
        stringBuilder1.append(" now ");
        stringBuilder1.append(param1Handler);
        stringBuilder1.append(")");
        throw new RuntimeException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("ServiceConnection ");
      stringBuilder2.append(this.mConnection);
      stringBuilder2.append(" registered with differing Context (was ");
      stringBuilder2.append(this.mContext);
      stringBuilder2.append(" now ");
      stringBuilder2.append(stringBuilder1);
      stringBuilder2.append(")");
      throw new RuntimeException(stringBuilder2.toString());
    }
    
    void doForget() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: iconst_0
      //   3: istore_1
      //   4: iload_1
      //   5: aload_0
      //   6: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   9: invokevirtual size : ()I
      //   12: if_icmpge -> 48
      //   15: aload_0
      //   16: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   19: iload_1
      //   20: invokevirtual valueAt : (I)Ljava/lang/Object;
      //   23: checkcast android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
      //   26: astore_2
      //   27: aload_2
      //   28: getfield binder : Landroid/os/IBinder;
      //   31: aload_2
      //   32: getfield deathMonitor : Landroid/os/IBinder$DeathRecipient;
      //   35: iconst_0
      //   36: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
      //   41: pop
      //   42: iinc #1, 1
      //   45: goto -> 4
      //   48: aload_0
      //   49: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   52: invokevirtual clear : ()V
      //   55: aload_0
      //   56: iconst_1
      //   57: putfield mForgotten : Z
      //   60: aload_0
      //   61: monitorexit
      //   62: return
      //   63: astore_2
      //   64: aload_0
      //   65: monitorexit
      //   66: aload_2
      //   67: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2005	-> 0
      //   #2006	-> 2
      //   #2007	-> 15
      //   #2008	-> 27
      //   #2006	-> 42
      //   #2010	-> 48
      //   #2011	-> 55
      //   #2012	-> 60
      //   #2013	-> 62
      //   #2012	-> 63
      // Exception table:
      //   from	to	target	type
      //   4	15	63	finally
      //   15	27	63	finally
      //   27	42	63	finally
      //   48	55	63	finally
      //   55	60	63	finally
      //   60	62	63	finally
      //   64	66	63	finally
    }
    
    ServiceConnectionLeaked getLocation() {
      return this.mLocation;
    }
    
    ServiceConnection getServiceConnection() {
      return this.mConnection;
    }
    
    IServiceConnection getIServiceConnection() {
      return this.mIServiceConnection;
    }
    
    int getFlags() {
      return this.mFlags;
    }
    
    void setUnbindLocation(RuntimeException param1RuntimeException) {
      this.mUnbindLocation = param1RuntimeException;
    }
    
    RuntimeException getUnbindLocation() {
      return this.mUnbindLocation;
    }
    
    public void connected(ComponentName param1ComponentName, IBinder param1IBinder, boolean param1Boolean) {
      Executor executor = this.mActivityExecutor;
      if (executor != null) {
        executor.execute(new RunConnection(param1ComponentName, param1IBinder, 0, param1Boolean));
      } else if (this.mActivityThread != null) {
        if (OplusExSystemServiceHelper.getInstance().checkOplusExSystemService(this.mActivityThread, param1ComponentName.getClassName())) {
          (new RunConnection(param1ComponentName, param1IBinder, 0, param1Boolean)).run();
        } else {
          this.mActivityThread.post(new RunConnection(param1ComponentName, param1IBinder, 0, param1Boolean));
        } 
      } else {
        doConnected(param1ComponentName, param1IBinder, param1Boolean);
      } 
    }
    
    public void death(ComponentName param1ComponentName, IBinder param1IBinder) {
      Executor executor = this.mActivityExecutor;
      if (executor != null) {
        executor.execute(new RunConnection(param1ComponentName, param1IBinder, 1, false));
      } else {
        Handler handler = this.mActivityThread;
        if (handler != null) {
          handler.post(new RunConnection(param1ComponentName, param1IBinder, 1, false));
        } else {
          doDeath(param1ComponentName, param1IBinder);
        } 
      } 
    }
    
    public void doConnected(ComponentName param1ComponentName, IBinder param1IBinder, boolean param1Boolean) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mForgotten : Z
      //   6: ifeq -> 12
      //   9: aload_0
      //   10: monitorexit
      //   11: return
      //   12: aload_0
      //   13: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   16: aload_1
      //   17: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   20: checkcast android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
      //   23: astore #4
      //   25: aload #4
      //   27: ifnull -> 42
      //   30: aload #4
      //   32: getfield binder : Landroid/os/IBinder;
      //   35: aload_2
      //   36: if_acmpne -> 42
      //   39: aload_0
      //   40: monitorexit
      //   41: return
      //   42: aload_2
      //   43: ifnull -> 122
      //   46: new android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
      //   49: astore #5
      //   51: aload #5
      //   53: aconst_null
      //   54: invokespecial <init> : (Landroid/app/LoadedApk$1;)V
      //   57: aload #5
      //   59: aload_2
      //   60: putfield binder : Landroid/os/IBinder;
      //   63: new android/app/LoadedApk$ServiceDispatcher$DeathMonitor
      //   66: astore #6
      //   68: aload #6
      //   70: aload_0
      //   71: aload_1
      //   72: aload_2
      //   73: invokespecial <init> : (Landroid/app/LoadedApk$ServiceDispatcher;Landroid/content/ComponentName;Landroid/os/IBinder;)V
      //   76: aload #5
      //   78: aload #6
      //   80: putfield deathMonitor : Landroid/os/IBinder$DeathRecipient;
      //   83: aload_2
      //   84: aload #5
      //   86: getfield deathMonitor : Landroid/os/IBinder$DeathRecipient;
      //   89: iconst_0
      //   90: invokeinterface linkToDeath : (Landroid/os/IBinder$DeathRecipient;I)V
      //   95: aload_0
      //   96: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   99: aload_1
      //   100: aload #5
      //   102: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
      //   105: pop
      //   106: goto -> 131
      //   109: astore_2
      //   110: aload_0
      //   111: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   114: aload_1
      //   115: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   118: pop
      //   119: aload_0
      //   120: monitorexit
      //   121: return
      //   122: aload_0
      //   123: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   126: aload_1
      //   127: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   130: pop
      //   131: aload #4
      //   133: ifnull -> 153
      //   136: aload #4
      //   138: getfield binder : Landroid/os/IBinder;
      //   141: aload #4
      //   143: getfield deathMonitor : Landroid/os/IBinder$DeathRecipient;
      //   146: iconst_0
      //   147: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
      //   152: pop
      //   153: aload_0
      //   154: monitorexit
      //   155: aload #4
      //   157: ifnull -> 170
      //   160: aload_0
      //   161: getfield mConnection : Landroid/content/ServiceConnection;
      //   164: aload_1
      //   165: invokeinterface onServiceDisconnected : (Landroid/content/ComponentName;)V
      //   170: iload_3
      //   171: ifeq -> 184
      //   174: aload_0
      //   175: getfield mConnection : Landroid/content/ServiceConnection;
      //   178: aload_1
      //   179: invokeinterface onBindingDied : (Landroid/content/ComponentName;)V
      //   184: aload_2
      //   185: ifnull -> 202
      //   188: aload_0
      //   189: getfield mConnection : Landroid/content/ServiceConnection;
      //   192: aload_1
      //   193: aload_2
      //   194: invokeinterface onServiceConnected : (Landroid/content/ComponentName;Landroid/os/IBinder;)V
      //   199: goto -> 212
      //   202: aload_0
      //   203: getfield mConnection : Landroid/content/ServiceConnection;
      //   206: aload_1
      //   207: invokeinterface onNullBinding : (Landroid/content/ComponentName;)V
      //   212: return
      //   213: astore_1
      //   214: aload_0
      //   215: monitorexit
      //   216: aload_1
      //   217: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2073	-> 0
      //   #2074	-> 2
      //   #2077	-> 9
      //   #2079	-> 12
      //   #2080	-> 25
      //   #2082	-> 39
      //   #2085	-> 42
      //   #2087	-> 46
      //   #2088	-> 57
      //   #2089	-> 63
      //   #2091	-> 83
      //   #2092	-> 95
      //   #2098	-> 106
      //   #2093	-> 109
      //   #2096	-> 110
      //   #2097	-> 119
      //   #2102	-> 122
      //   #2105	-> 131
      //   #2106	-> 136
      //   #2108	-> 153
      //   #2111	-> 155
      //   #2112	-> 160
      //   #2114	-> 170
      //   #2115	-> 174
      //   #2118	-> 184
      //   #2119	-> 188
      //   #2122	-> 202
      //   #2124	-> 212
      //   #2108	-> 213
      // Exception table:
      //   from	to	target	type
      //   2	9	213	finally
      //   9	11	213	finally
      //   12	25	213	finally
      //   30	39	213	finally
      //   39	41	213	finally
      //   46	57	213	finally
      //   57	63	213	finally
      //   63	83	213	finally
      //   83	95	109	android/os/RemoteException
      //   83	95	213	finally
      //   95	106	109	android/os/RemoteException
      //   95	106	213	finally
      //   110	119	213	finally
      //   119	121	213	finally
      //   122	131	213	finally
      //   136	153	213	finally
      //   153	155	213	finally
      //   214	216	213	finally
    }
    
    public void doDeath(ComponentName param1ComponentName, IBinder param1IBinder) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   6: aload_1
      //   7: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   10: checkcast android/app/LoadedApk$ServiceDispatcher$ConnectionInfo
      //   13: astore_3
      //   14: aload_3
      //   15: ifnull -> 66
      //   18: aload_3
      //   19: getfield binder : Landroid/os/IBinder;
      //   22: aload_2
      //   23: if_acmpeq -> 29
      //   26: goto -> 66
      //   29: aload_0
      //   30: getfield mActiveConnections : Landroid/util/ArrayMap;
      //   33: aload_1
      //   34: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   37: pop
      //   38: aload_3
      //   39: getfield binder : Landroid/os/IBinder;
      //   42: aload_3
      //   43: getfield deathMonitor : Landroid/os/IBinder$DeathRecipient;
      //   46: iconst_0
      //   47: invokeinterface unlinkToDeath : (Landroid/os/IBinder$DeathRecipient;I)Z
      //   52: pop
      //   53: aload_0
      //   54: monitorexit
      //   55: aload_0
      //   56: getfield mConnection : Landroid/content/ServiceConnection;
      //   59: aload_1
      //   60: invokeinterface onServiceDisconnected : (Landroid/content/ComponentName;)V
      //   65: return
      //   66: aload_0
      //   67: monitorexit
      //   68: return
      //   69: astore_1
      //   70: aload_0
      //   71: monitorexit
      //   72: aload_1
      //   73: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2127	-> 0
      //   #2128	-> 2
      //   #2129	-> 14
      //   #2134	-> 29
      //   #2135	-> 38
      //   #2136	-> 53
      //   #2138	-> 55
      //   #2139	-> 65
      //   #2132	-> 66
      //   #2136	-> 69
      // Exception table:
      //   from	to	target	type
      //   2	14	69	finally
      //   18	26	69	finally
      //   29	38	69	finally
      //   38	53	69	finally
      //   53	55	69	finally
      //   66	68	69	finally
      //   70	72	69	finally
    }
    
    private final class RunConnection implements Runnable {
      final int mCommand;
      
      final boolean mDead;
      
      final ComponentName mName;
      
      final IBinder mService;
      
      final LoadedApk.ServiceDispatcher this$0;
      
      RunConnection(ComponentName param2ComponentName, IBinder param2IBinder, int param2Int, boolean param2Boolean) {
        this.mName = param2ComponentName;
        this.mService = param2IBinder;
        this.mCommand = param2Int;
        this.mDead = param2Boolean;
      }
      
      public void run() {
        int i = this.mCommand;
        if (i == 0) {
          LoadedApk.ServiceDispatcher.this.doConnected(this.mName, this.mService, this.mDead);
        } else if (i == 1) {
          LoadedApk.ServiceDispatcher.this.doDeath(this.mName, this.mService);
        } 
      }
    }
    
    class DeathMonitor implements IBinder.DeathRecipient {
      final ComponentName mName;
      
      final IBinder mService;
      
      final LoadedApk.ServiceDispatcher this$0;
      
      DeathMonitor(ComponentName param2ComponentName, IBinder param2IBinder) {
        this.mName = param2ComponentName;
        this.mService = param2IBinder;
      }
      
      public void binderDied() {
        LoadedApk.ServiceDispatcher.this.death(this.mName, this.mService);
      }
    }
  }
  
  private static class ConnectionInfo {
    IBinder binder;
    
    IBinder.DeathRecipient deathMonitor;
    
    private ConnectionInfo() {}
  }
  
  class InnerConnection extends IServiceConnection.Stub {
    final WeakReference<LoadedApk.ServiceDispatcher> mDispatcher;
    
    InnerConnection(LoadedApk this$0) {
      this.mDispatcher = new WeakReference(this$0);
    }
    
    public void connected(ComponentName param1ComponentName, IBinder param1IBinder, boolean param1Boolean) throws RemoteException {
      LoadedApk.ServiceDispatcher serviceDispatcher = this.mDispatcher.get();
      if (serviceDispatcher != null)
        serviceDispatcher.connected(param1ComponentName, param1IBinder, param1Boolean); 
    }
  }
  
  private final class RunConnection implements Runnable {
    final int mCommand;
    
    final boolean mDead;
    
    final ComponentName mName;
    
    final IBinder mService;
    
    final LoadedApk.ServiceDispatcher this$0;
    
    RunConnection(ComponentName param1ComponentName, IBinder param1IBinder, int param1Int, boolean param1Boolean) {
      this.mName = param1ComponentName;
      this.mService = param1IBinder;
      this.mCommand = param1Int;
      this.mDead = param1Boolean;
    }
    
    public void run() {
      int i = this.mCommand;
      if (i == 0) {
        this.this$0.doConnected(this.mName, this.mService, this.mDead);
      } else if (i == 1) {
        this.this$0.doDeath(this.mName, this.mService);
      } 
    }
  }
  
  class DeathMonitor implements IBinder.DeathRecipient {
    final ComponentName mName;
    
    final IBinder mService;
    
    final LoadedApk.ServiceDispatcher this$0;
    
    DeathMonitor(ComponentName param1ComponentName, IBinder param1IBinder) {
      this.mName = param1ComponentName;
      this.mService = param1IBinder;
    }
    
    public void binderDied() {
      this.this$0.death(this.mName, this.mService);
    }
  }
}
