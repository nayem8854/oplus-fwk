package android.app;

import android.content.DialogInterface;
import com.android.internal.app.OplusBaseAlertController;

public abstract class OplusBaseDialog implements DialogInterface {
  @Deprecated
  public static final int DELETE_ALERT_DIALOG_ONE = 1;
  
  @Deprecated
  public static final int DELETE_ALERT_DIALOG_THREE = 3;
  
  @Deprecated
  public static final int DELETE_ALERT_DIALOG_TWO = 2;
  
  public static final int TYPE_BOTTOM = 1;
  
  public static final int TYPE_CENTER = 0;
  
  public void setButtonIsBold(int paramInt1, int paramInt2, int paramInt3) {}
  
  public void setDeleteDialogOption(int paramInt) {}
  
  class BaseBuilder {
    public BaseBuilder setDialogType(int param1Int) {
      OplusBaseAlertController.BaseAlertParams baseAlertParams = getParams();
      baseAlertParams.mDialogType = param1Int;
      return this;
    }
    
    public BaseBuilder setMessageNeedScroll(boolean param1Boolean) {
      OplusBaseAlertController.BaseAlertParams baseAlertParams = getParams();
      baseAlertParams.mMessageNeedScroll = param1Boolean;
      return this;
    }
    
    public BaseBuilder setDeleteDialogOption(int param1Int) {
      return this;
    }
    
    public BaseBuilder setItems(CharSequence[] param1ArrayOfCharSequence1, CharSequence[] param1ArrayOfCharSequence2, DialogInterface.OnClickListener param1OnClickListener) {
      OplusBaseAlertController.BaseAlertParams baseAlertParams = getParams();
      baseAlertParams.setItems(param1ArrayOfCharSequence1);
      baseAlertParams.mSummaries = param1ArrayOfCharSequence2;
      baseAlertParams.setOnClickListener(param1OnClickListener);
      return this;
    }
    
    public BaseBuilder setItems(int param1Int1, int param1Int2, DialogInterface.OnClickListener param1OnClickListener) {
      OplusBaseAlertController.BaseAlertParams baseAlertParams = getParams();
      baseAlertParams.setItems(baseAlertParams.getContext().getResources().getTextArray(param1Int1));
      baseAlertParams.mSummaries = baseAlertParams.getContext().getResources().getTextArray(param1Int2);
      baseAlertParams.setOnClickListener(param1OnClickListener);
      return this;
    }
    
    public abstract AlertDialog create();
    
    abstract OplusBaseAlertController.BaseAlertParams getParams();
    
    public abstract AlertDialog.Builder setItems(int param1Int, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setNegativeButton(int param1Int, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setNegativeButton(CharSequence param1CharSequence, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setNeutralButton(int param1Int, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setNeutralButton(CharSequence param1CharSequence, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setOnCancelListener(DialogInterface.OnCancelListener param1OnCancelListener);
    
    public abstract AlertDialog.Builder setOnDismissListener(DialogInterface.OnDismissListener param1OnDismissListener);
    
    public abstract AlertDialog.Builder setOnKeyListener(DialogInterface.OnKeyListener param1OnKeyListener);
    
    public abstract AlertDialog.Builder setPositiveButton(int param1Int, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setPositiveButton(CharSequence param1CharSequence, DialogInterface.OnClickListener param1OnClickListener);
    
    public abstract AlertDialog.Builder setView(int param1Int);
  }
}
