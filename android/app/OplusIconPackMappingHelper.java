package android.app;

import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import com.oplus.theme.OplusThemeUtil;
import java.io.FileInputStream;
import java.io.InputStream;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

class OplusIconPackMappingHelper {
  public static final String MAPPING_NAME = "packMapping.xml";
  
  private static final String TAG = "IconPackMappingHelper";
  
  public static final ArrayMap<String, String> sMappingComponentMap = new ArrayMap();
  
  public static final ArrayMap<String, String> sMappingPackageMap = new ArrayMap();
  
  private static boolean sParsed = false;
  
  public static void parsePackMapping() {
    long l = System.currentTimeMillis();
    if (sParsed)
      return; 
    Log.d("IconPackMappingHelper", "start parsePackMapping");
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      FileInputStream fileInputStream = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      StringBuilder stringBuilder = new StringBuilder();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      stringBuilder.append(OplusThemeUtil.SYSTEM_THEME_DEFAULT_PATH);
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      stringBuilder.append("packMapping.xml");
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(stringBuilder.toString());
      fileInputStream3 = fileInputStream;
      fileInputStream4 = fileInputStream;
      sMappingComponentMap.clear();
      fileInputStream3 = fileInputStream;
      fileInputStream4 = fileInputStream;
      parseXml(fileInputStream);
      fileInputStream3 = fileInputStream;
      fileInputStream4 = fileInputStream;
      fileInputStream.close();
      try {
        fileInputStream.close();
        sParsed = true;
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("parsePackMapping-time : ");
        stringBuilder1.append(System.currentTimeMillis() - l);
        Log.e("lqc", stringBuilder1.toString());
      } catch (Exception exception) {
        Log.e("parsePackMapping", "input error");
      } 
    } catch (Exception exception) {
      fileInputStream3 = fileInputStream4;
      exception.printStackTrace();
      if (fileInputStream4 != null)
        fileInputStream4.close(); 
      sParsed = true;
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("parsePackMapping-time : ");
      stringBuilder.append(System.currentTimeMillis() - l);
      Log.e("lqc", stringBuilder.toString());
    } finally {}
  }
  
  public static void parseXml(InputStream paramInputStream) throws Exception {
    SAXParserFactory sAXParserFactory = SAXParserFactory.newInstance();
    SAXParser sAXParser = sAXParserFactory.newSAXParser();
    sAXParser.parse(paramInputStream, new MappingXmlHandler());
    paramInputStream.close();
  }
  
  static class MappingXmlHandler extends DefaultHandler {
    public void startElement(String param1String1, String param1String2, String param1String3, Attributes param1Attributes) {
      if (param1String2.equalsIgnoreCase("item")) {
        param1String2 = param1Attributes.getValue("component");
        param1String3 = param1Attributes.getValue("mapping");
        int i = param1String2.indexOf("/");
        param1String1 = "";
        if (i > 0)
          param1String1 = param1String2.substring(0, i); 
        if (!TextUtils.isEmpty(param1String2) && !TextUtils.isEmpty(param1String3)) {
          OplusIconPackMappingHelper.sMappingComponentMap.put(param1String2, param1String3);
          OplusIconPackMappingHelper.sMappingPackageMap.put(param1String1, param1String3);
        } 
      } 
    }
  }
  
  public static String getMappingComponent(String paramString1, String paramString2) {
    String str = (String)sMappingComponentMap.get(paramString1);
    paramString1 = str;
    if (TextUtils.isEmpty(str))
      paramString1 = (String)sMappingPackageMap.get(paramString2); 
    return paramString1;
  }
}
