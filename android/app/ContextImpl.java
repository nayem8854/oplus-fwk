package android.app;

import android.common.OplusFeatureCache;
import android.content.AutofillOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.IContentProvider;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.OplusPackageManager;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.CompatResources;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.content.res.loader.ResourcesLoader;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.Environment;
import android.os.FileUtils;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.permission.IPermissionManager;
import android.permission.PermissionManager;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructStat;
import android.text.TextUtils;
import android.util.AndroidRuntimeException;
import android.util.ArrayMap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Slog;
import android.view.Display;
import android.view.DisplayAdjustments;
import android.view.autofill.AutofillManager;
import com.android.internal.util.Preconditions;
import dalvik.system.BlockGuard;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteOrder;
import java.nio.file.Path;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;
import libcore.io.Memory;

class ContextImpl extends Context {
  private static final boolean DEBUG = false;
  
  static boolean DEBUG_CIL = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  static final int STATE_INITIALIZING = 1;
  
  static final int STATE_NOT_FOUND = 3;
  
  static final int STATE_READY = 2;
  
  static final int STATE_UNINITIALIZED = 0;
  
  private static final String TAG = "ContextImpl";
  
  private static final String XATTR_INODE_CACHE = "user.inode_cache";
  
  private static final String XATTR_INODE_CODE_CACHE = "user.inode_code_cache";
  
  private static ArrayMap<String, ArrayMap<File, SharedPreferencesImpl>> sSharedPrefsCache;
  
  private final String mAttributionTag;
  
  private AutofillManager.AutofillClient mAutofillClient;
  
  private AutofillOptions mAutofillOptions;
  
  private final String mBasePackageName;
  
  private File mCacheDir;
  
  private ClassLoader mClassLoader;
  
  private File mCodeCacheDir;
  
  private ContentCaptureOptions mContentCaptureOptions;
  
  private final ApplicationContentResolver mContentResolver;
  
  private File mCratesDir;
  
  private File mDatabasesDir;
  
  private Display mDisplay;
  
  private File mFilesDir;
  
  private final int mFlags;
  
  private boolean mIsAssociatedWithDisplay;
  
  private boolean mIsSystemOrSystemUiContext;
  
  private boolean mIsUiContext;
  
  final ActivityThread mMainThread;
  
  private File mNoBackupFilesDir;
  
  private final String mOpPackageName;
  
  private Context mOuterContext;
  
  final LoadedApk mPackageInfo;
  
  private PackageManager mPackageManager;
  
  private File mPreferencesDir;
  
  private Context mReceiverRestrictedContext;
  
  private Resources mResources;
  
  private final ResourcesManager mResourcesManager;
  
  final Object[] mServiceCache;
  
  final int[] mServiceInitializationStateArray;
  
  private ArrayMap<String, File> mSharedPrefsPaths;
  
  private String mSplitName;
  
  private final Object mSync;
  
  private Resources.Theme mTheme;
  
  private int mThemeResource;
  
  private final IBinder mToken;
  
  private final UserHandle mUser;
  
  static ContextImpl getImpl(Context paramContext) {
    while (paramContext instanceof ContextWrapper) {
      ContextWrapper contextWrapper = (ContextWrapper)paramContext;
      Context context = contextWrapper.getBaseContext();
      if (context != null)
        paramContext = context; 
    } 
    return (ContextImpl)paramContext;
  }
  
  public AssetManager getAssets() {
    return getResources().getAssets();
  }
  
  public Resources getResources() {
    return this.mResources;
  }
  
  public PackageManager getPackageManager() {
    PackageManager packageManager = this.mPackageManager;
    if (packageManager != null)
      return packageManager; 
    IPackageManager iPackageManager = ActivityThread.getPackageManager();
    IPermissionManager iPermissionManager = ActivityThread.getPermissionManager();
    if (iPackageManager != null && iPermissionManager != null) {
      ApplicationPackageManager applicationPackageManager = new ApplicationPackageManager(this, iPackageManager, iPermissionManager);
      return applicationPackageManager;
    } 
    return null;
  }
  
  public ContentResolver getContentResolver() {
    return this.mContentResolver;
  }
  
  public Looper getMainLooper() {
    return this.mMainThread.getLooper();
  }
  
  public Executor getMainExecutor() {
    return this.mMainThread.getExecutor();
  }
  
  public Context getApplicationContext() {
    Application application;
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null) {
      application = loadedApk.getApplication();
    } else {
      application = this.mMainThread.getApplication();
    } 
    return application;
  }
  
  public void setTheme(int paramInt) {
    synchronized (this.mSync) {
      if (this.mThemeResource != paramInt) {
        this.mThemeResource = paramInt;
        initializeTheme();
      } 
      return;
    } 
  }
  
  public int getThemeResId() {
    synchronized (this.mSync) {
      return this.mThemeResource;
    } 
  }
  
  public Resources.Theme getTheme() {
    synchronized (this.mSync) {
      if (this.mTheme != null)
        return this.mTheme; 
      int i = this.mThemeResource;
      int j = (getOuterContext().getApplicationInfo()).targetSdkVersion;
      this.mThemeResource = Resources.selectDefaultTheme(i, j);
      initializeTheme();
      return this.mTheme;
    } 
  }
  
  private void initializeTheme() {
    if (this.mTheme == null)
      this.mTheme = this.mResources.newTheme(); 
    this.mTheme.applyStyle(this.mThemeResource, true);
  }
  
  public ClassLoader getClassLoader() {
    ClassLoader classLoader = this.mClassLoader;
    if (classLoader == null) {
      LoadedApk loadedApk = this.mPackageInfo;
      if (loadedApk != null) {
        ClassLoader classLoader1 = loadedApk.getClassLoader();
      } else {
        classLoader = ClassLoader.getSystemClassLoader();
      } 
    } 
    return classLoader;
  }
  
  public String getPackageName() {
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null)
      return loadedApk.getPackageName(); 
    return "android";
  }
  
  public String getBasePackageName() {
    String str = this.mBasePackageName;
    if (str == null)
      str = getPackageName(); 
    return str;
  }
  
  public String getOpPackageName() {
    String str = this.mOpPackageName;
    if (str == null)
      str = getBasePackageName(); 
    return str;
  }
  
  public String getAttributionTag() {
    return this.mAttributionTag;
  }
  
  public ApplicationInfo getApplicationInfo() {
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null)
      return loadedApk.getApplicationInfo(); 
    throw new RuntimeException("Not supported in system context");
  }
  
  public String getPackageResourcePath() {
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null)
      return loadedApk.getResDir(); 
    throw new RuntimeException("Not supported in system context");
  }
  
  public String getPackageCodePath() {
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null)
      return loadedApk.getAppDir(); 
    throw new RuntimeException("Not supported in system context");
  }
  
  public SharedPreferences getSharedPreferences(String paramString, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: astore_3
    //   2: aload_0
    //   3: getfield mPackageInfo : Landroid/app/LoadedApk;
    //   6: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   9: getfield targetSdkVersion : I
    //   12: bipush #19
    //   14: if_icmpge -> 27
    //   17: aload_1
    //   18: astore_3
    //   19: aload_1
    //   20: ifnonnull -> 27
    //   23: ldc_w 'null'
    //   26: astore_3
    //   27: ldc android/app/ContextImpl
    //   29: monitorenter
    //   30: aload_0
    //   31: getfield mSharedPrefsPaths : Landroid/util/ArrayMap;
    //   34: ifnonnull -> 50
    //   37: new android/util/ArrayMap
    //   40: astore_1
    //   41: aload_1
    //   42: invokespecial <init> : ()V
    //   45: aload_0
    //   46: aload_1
    //   47: putfield mSharedPrefsPaths : Landroid/util/ArrayMap;
    //   50: aload_0
    //   51: getfield mSharedPrefsPaths : Landroid/util/ArrayMap;
    //   54: aload_3
    //   55: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   58: checkcast java/io/File
    //   61: astore #4
    //   63: aload #4
    //   65: astore_1
    //   66: aload #4
    //   68: ifnonnull -> 87
    //   71: aload_0
    //   72: aload_3
    //   73: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   76: astore_1
    //   77: aload_0
    //   78: getfield mSharedPrefsPaths : Landroid/util/ArrayMap;
    //   81: aload_3
    //   82: aload_1
    //   83: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   86: pop
    //   87: ldc android/app/ContextImpl
    //   89: monitorexit
    //   90: aload_0
    //   91: aload_1
    //   92: iload_2
    //   93: invokevirtual getSharedPreferences : (Ljava/io/File;I)Landroid/content/SharedPreferences;
    //   96: areturn
    //   97: astore_1
    //   98: ldc android/app/ContextImpl
    //   100: monitorexit
    //   101: aload_1
    //   102: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #480	-> 0
    //   #482	-> 17
    //   #483	-> 23
    //   #488	-> 27
    //   #489	-> 30
    //   #490	-> 37
    //   #492	-> 50
    //   #493	-> 63
    //   #494	-> 71
    //   #495	-> 77
    //   #497	-> 87
    //   #498	-> 90
    //   #497	-> 97
    // Exception table:
    //   from	to	target	type
    //   30	37	97	finally
    //   37	50	97	finally
    //   50	63	97	finally
    //   71	77	97	finally
    //   77	87	97	finally
    //   87	90	97	finally
    //   98	101	97	finally
  }
  
  public SharedPreferences getSharedPreferences(File paramFile, int paramInt) {
    // Byte code:
    //   0: ldc android/app/ContextImpl
    //   2: monitorenter
    //   3: aload_0
    //   4: invokespecial getSharedPreferencesCacheLocked : ()Landroid/util/ArrayMap;
    //   7: astore_3
    //   8: aload_3
    //   9: aload_1
    //   10: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   13: checkcast android/app/SharedPreferencesImpl
    //   16: astore #4
    //   18: aload #4
    //   20: ifnonnull -> 112
    //   23: aload_0
    //   24: iload_2
    //   25: invokespecial checkMode : (I)V
    //   28: aload_0
    //   29: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   32: getfield targetSdkVersion : I
    //   35: bipush #26
    //   37: if_icmplt -> 86
    //   40: aload_0
    //   41: invokevirtual isCredentialProtectedStorage : ()Z
    //   44: ifeq -> 86
    //   47: aload_0
    //   48: ldc_w android/os/UserManager
    //   51: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   54: checkcast android/os/UserManager
    //   57: astore #4
    //   59: aload #4
    //   61: invokestatic myUserId : ()I
    //   64: invokevirtual isUserUnlockingOrUnlocked : (I)Z
    //   67: ifeq -> 73
    //   70: goto -> 86
    //   73: new java/lang/IllegalStateException
    //   76: astore_1
    //   77: aload_1
    //   78: ldc_w 'SharedPreferences in credential encrypted storage are not available until after user is unlocked'
    //   81: invokespecial <init> : (Ljava/lang/String;)V
    //   84: aload_1
    //   85: athrow
    //   86: new android/app/SharedPreferencesImpl
    //   89: astore #4
    //   91: aload #4
    //   93: aload_1
    //   94: iload_2
    //   95: invokespecial <init> : (Ljava/io/File;I)V
    //   98: aload_3
    //   99: aload_1
    //   100: aload #4
    //   102: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   105: pop
    //   106: ldc android/app/ContextImpl
    //   108: monitorexit
    //   109: aload #4
    //   111: areturn
    //   112: ldc android/app/ContextImpl
    //   114: monitorexit
    //   115: iload_2
    //   116: iconst_4
    //   117: iand
    //   118: ifne -> 133
    //   121: aload_0
    //   122: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   125: getfield targetSdkVersion : I
    //   128: bipush #11
    //   130: if_icmpge -> 138
    //   133: aload #4
    //   135: invokevirtual startReloadIfChangedUnexpectedly : ()V
    //   138: aload #4
    //   140: areturn
    //   141: astore_1
    //   142: ldc android/app/ContextImpl
    //   144: monitorexit
    //   145: aload_1
    //   146: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #504	-> 0
    //   #505	-> 3
    //   #506	-> 8
    //   #507	-> 18
    //   #508	-> 23
    //   #509	-> 28
    //   #510	-> 40
    //   #511	-> 47
    //   #512	-> 59
    //   #513	-> 73
    //   #517	-> 86
    //   #518	-> 98
    //   #519	-> 106
    //   #521	-> 112
    //   #522	-> 115
    //   #523	-> 121
    //   #527	-> 133
    //   #529	-> 138
    //   #521	-> 141
    // Exception table:
    //   from	to	target	type
    //   3	8	141	finally
    //   8	18	141	finally
    //   23	28	141	finally
    //   28	40	141	finally
    //   40	47	141	finally
    //   47	59	141	finally
    //   59	70	141	finally
    //   73	86	141	finally
    //   86	98	141	finally
    //   98	106	141	finally
    //   106	109	141	finally
    //   112	115	141	finally
    //   142	145	141	finally
  }
  
  private ArrayMap<File, SharedPreferencesImpl> getSharedPreferencesCacheLocked() {
    if (sSharedPrefsCache == null)
      sSharedPrefsCache = new ArrayMap(); 
    String str = getPackageName();
    ArrayMap<File, SharedPreferencesImpl> arrayMap1 = (ArrayMap)sSharedPrefsCache.get(str);
    ArrayMap<File, SharedPreferencesImpl> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap();
      sSharedPrefsCache.put(str, arrayMap2);
    } 
    return arrayMap2;
  }
  
  public void reloadSharedPreferences() {
    // Byte code:
    //   0: new java/util/ArrayList
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_1
    //   8: ldc android/app/ContextImpl
    //   10: monitorenter
    //   11: aload_0
    //   12: invokespecial getSharedPreferencesCacheLocked : ()Landroid/util/ArrayMap;
    //   15: astore_2
    //   16: iconst_0
    //   17: istore_3
    //   18: iload_3
    //   19: aload_2
    //   20: invokevirtual size : ()I
    //   23: if_icmpge -> 54
    //   26: aload_2
    //   27: iload_3
    //   28: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   31: checkcast android/app/SharedPreferencesImpl
    //   34: astore #4
    //   36: aload #4
    //   38: ifnull -> 48
    //   41: aload_1
    //   42: aload #4
    //   44: invokevirtual add : (Ljava/lang/Object;)Z
    //   47: pop
    //   48: iinc #3, 1
    //   51: goto -> 18
    //   54: ldc android/app/ContextImpl
    //   56: monitorexit
    //   57: iconst_0
    //   58: istore_3
    //   59: iload_3
    //   60: aload_1
    //   61: invokevirtual size : ()I
    //   64: if_icmpge -> 84
    //   67: aload_1
    //   68: iload_3
    //   69: invokevirtual get : (I)Ljava/lang/Object;
    //   72: checkcast android/app/SharedPreferencesImpl
    //   75: invokevirtual startReloadIfChangedUnexpectedly : ()V
    //   78: iinc #3, 1
    //   81: goto -> 59
    //   84: return
    //   85: astore_1
    //   86: ldc android/app/ContextImpl
    //   88: monitorexit
    //   89: aload_1
    //   90: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #551	-> 0
    //   #552	-> 8
    //   #553	-> 11
    //   #554	-> 16
    //   #555	-> 26
    //   #556	-> 36
    //   #557	-> 41
    //   #554	-> 48
    //   #560	-> 54
    //   #563	-> 57
    //   #564	-> 67
    //   #563	-> 78
    //   #566	-> 84
    //   #560	-> 85
    // Exception table:
    //   from	to	target	type
    //   11	16	85	finally
    //   18	26	85	finally
    //   26	36	85	finally
    //   41	48	85	finally
    //   54	57	85	finally
    //   86	89	85	finally
  }
  
  private static int moveFiles(File paramFile1, File paramFile2, String paramString) {
    File[] arrayOfFile = FileUtils.listFilesOrEmpty(paramFile1, (FilenameFilter)new Object(paramString));
    int i = 0;
    int j;
    byte b;
    for (j = arrayOfFile.length, b = 0; b < j; ) {
      paramFile1 = arrayOfFile[b];
      File file = new File(paramFile2, paramFile1.getName());
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Migrating ");
      stringBuilder.append(paramFile1);
      stringBuilder.append(" to ");
      stringBuilder.append(file);
      Log.d("ContextImpl", stringBuilder.toString());
      try {
        FileUtils.copyFileOrThrow(paramFile1, file);
        FileUtils.copyPermissions(paramFile1, file);
        if (paramFile1.delete()) {
          int k = i;
          if (i != -1)
            k = i + 1; 
          i = k;
        } else {
          IOException iOException = new IOException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Failed to clean up ");
          stringBuilder1.append(paramFile1);
          this(stringBuilder1.toString());
          throw iOException;
        } 
      } catch (IOException iOException) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to migrate ");
        stringBuilder.append(paramFile1);
        stringBuilder.append(": ");
        stringBuilder.append(iOException);
        Log.w("ContextImpl", stringBuilder.toString());
        i = -1;
      } 
      b++;
    } 
    return i;
  }
  
  public boolean moveSharedPreferencesFrom(Context paramContext, String paramString) {
    // Byte code:
    //   0: ldc android/app/ContextImpl
    //   2: monitorenter
    //   3: aload_1
    //   4: aload_2
    //   5: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   8: astore_1
    //   9: aload_0
    //   10: aload_2
    //   11: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   14: astore_2
    //   15: aload_1
    //   16: invokevirtual getParentFile : ()Ljava/io/File;
    //   19: astore_3
    //   20: aload_2
    //   21: invokevirtual getParentFile : ()Ljava/io/File;
    //   24: astore #4
    //   26: aload_1
    //   27: invokevirtual getName : ()Ljava/lang/String;
    //   30: astore #5
    //   32: aload_3
    //   33: aload #4
    //   35: aload #5
    //   37: invokestatic moveFiles : (Ljava/io/File;Ljava/io/File;Ljava/lang/String;)I
    //   40: istore #6
    //   42: iload #6
    //   44: ifle -> 64
    //   47: aload_0
    //   48: invokespecial getSharedPreferencesCacheLocked : ()Landroid/util/ArrayMap;
    //   51: astore_3
    //   52: aload_3
    //   53: aload_1
    //   54: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   57: pop
    //   58: aload_3
    //   59: aload_2
    //   60: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   63: pop
    //   64: iload #6
    //   66: iconst_m1
    //   67: if_icmpeq -> 76
    //   70: iconst_1
    //   71: istore #7
    //   73: goto -> 79
    //   76: iconst_0
    //   77: istore #7
    //   79: ldc android/app/ContextImpl
    //   81: monitorexit
    //   82: iload #7
    //   84: ireturn
    //   85: astore_1
    //   86: ldc android/app/ContextImpl
    //   88: monitorexit
    //   89: aload_1
    //   90: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #605	-> 0
    //   #606	-> 3
    //   #607	-> 9
    //   #609	-> 15
    //   #610	-> 26
    //   #609	-> 32
    //   #611	-> 42
    //   #614	-> 47
    //   #615	-> 47
    //   #616	-> 52
    //   #617	-> 58
    //   #619	-> 64
    //   #620	-> 85
    // Exception table:
    //   from	to	target	type
    //   3	9	85	finally
    //   9	15	85	finally
    //   15	26	85	finally
    //   26	32	85	finally
    //   32	42	85	finally
    //   47	52	85	finally
    //   52	58	85	finally
    //   58	64	85	finally
    //   79	82	85	finally
    //   86	89	85	finally
  }
  
  public boolean deleteSharedPreferences(String paramString) {
    // Byte code:
    //   0: ldc android/app/ContextImpl
    //   2: monitorenter
    //   3: aload_0
    //   4: aload_1
    //   5: invokevirtual getSharedPreferencesPath : (Ljava/lang/String;)Ljava/io/File;
    //   8: astore_2
    //   9: aload_2
    //   10: invokestatic makeBackupFile : (Ljava/io/File;)Ljava/io/File;
    //   13: astore_3
    //   14: aload_0
    //   15: invokespecial getSharedPreferencesCacheLocked : ()Landroid/util/ArrayMap;
    //   18: astore_1
    //   19: aload_1
    //   20: aload_2
    //   21: invokevirtual remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   24: pop
    //   25: aload_2
    //   26: invokevirtual delete : ()Z
    //   29: pop
    //   30: aload_3
    //   31: invokevirtual delete : ()Z
    //   34: pop
    //   35: aload_2
    //   36: invokevirtual exists : ()Z
    //   39: ifne -> 55
    //   42: aload_3
    //   43: invokevirtual exists : ()Z
    //   46: ifne -> 55
    //   49: iconst_1
    //   50: istore #4
    //   52: goto -> 58
    //   55: iconst_0
    //   56: istore #4
    //   58: ldc android/app/ContextImpl
    //   60: monitorexit
    //   61: iload #4
    //   63: ireturn
    //   64: astore_1
    //   65: ldc android/app/ContextImpl
    //   67: monitorexit
    //   68: aload_1
    //   69: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #625	-> 0
    //   #626	-> 3
    //   #627	-> 9
    //   #630	-> 14
    //   #631	-> 19
    //   #633	-> 25
    //   #634	-> 30
    //   #637	-> 35
    //   #638	-> 64
    // Exception table:
    //   from	to	target	type
    //   3	9	64	finally
    //   9	14	64	finally
    //   14	19	64	finally
    //   19	25	64	finally
    //   25	30	64	finally
    //   30	35	64	finally
    //   35	49	64	finally
    //   58	61	64	finally
    //   65	68	64	finally
  }
  
  private File getPreferencesDir() {
    synchronized (this.mSync) {
      if (this.mPreferencesDir == null) {
        File file = new File();
        this(getDataDir(), "shared_prefs");
        this.mPreferencesDir = file;
      } 
      return ensurePrivateDirExists(this.mPreferencesDir);
    } 
  }
  
  public FileInputStream openFileInput(String paramString) throws FileNotFoundException {
    File file = makeFilename(getFilesDir(), paramString);
    return new FileInputStream(file);
  }
  
  public FileOutputStream openFileOutput(String paramString, int paramInt) throws FileNotFoundException {
    boolean bool;
    checkMode(paramInt);
    if ((0x8000 & paramInt) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    File file = makeFilename(getFilesDir(), paramString);
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(file, bool);
      setFilePermissionsFromMode(file.getPath(), paramInt, 0);
      return fileOutputStream;
    } catch (FileNotFoundException fileNotFoundException) {
      File file1 = file.getParentFile();
      file1.mkdir();
      String str = file1.getPath();
      FileUtils.setPermissions(str, 505, -1, -1);
      FileOutputStream fileOutputStream = new FileOutputStream(file, bool);
      setFilePermissionsFromMode(file.getPath(), paramInt, 0);
      return fileOutputStream;
    } 
  }
  
  public boolean deleteFile(String paramString) {
    File file = makeFilename(getFilesDir(), paramString);
    return file.delete();
  }
  
  private static File ensurePrivateDirExists(File paramFile) {
    return ensurePrivateDirExists(paramFile, 505, -1, null);
  }
  
  private static File ensurePrivateCacheDirExists(File paramFile, String paramString) {
    int i = UserHandle.getCacheAppGid(Process.myUid());
    return ensurePrivateDirExists(paramFile, 1529, i, paramString);
  }
  
  private static File ensurePrivateDirExists(File paramFile, int paramInt1, int paramInt2, String paramString) {
    if (!paramFile.exists()) {
      String str = paramFile.getAbsolutePath();
      try {
        Os.mkdir(str, paramInt1);
        Os.chmod(str, paramInt1);
        if (paramInt2 != -1)
          Os.chown(str, -1, paramInt2); 
      } catch (ErrnoException errnoException) {
        if (errnoException.errno != OsConstants.EEXIST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to ensure ");
          stringBuilder.append(paramFile);
          stringBuilder.append(": ");
          stringBuilder.append(errnoException.getMessage());
          Log.w("ContextImpl", stringBuilder.toString());
        } 
      } 
      if (paramString != null)
        try {
          StructStat structStat = Os.stat(paramFile.getAbsolutePath());
          byte[] arrayOfByte = new byte[8];
          Memory.pokeLong(arrayOfByte, 0, structStat.st_ino, ByteOrder.nativeOrder());
          Os.setxattr(paramFile.getParentFile().getAbsolutePath(), paramString, arrayOfByte, 0);
        } catch (ErrnoException errnoException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to update ");
          stringBuilder.append(paramString);
          stringBuilder.append(": ");
          stringBuilder.append(errnoException.getMessage());
          Log.w("ContextImpl", stringBuilder.toString());
        }  
    } 
    return paramFile;
  }
  
  public File getFilesDir() {
    synchronized (this.mSync) {
      if (this.mFilesDir == null) {
        File file = new File();
        this(getDataDir(), "files");
        this.mFilesDir = file;
      } 
      return ensurePrivateDirExists(this.mFilesDir);
    } 
  }
  
  public File getCrateDir(String paramString) {
    Preconditions.checkArgument(FileUtils.isValidExtFilename(paramString), "invalidated crateId");
    null = getDataDir().toPath().resolve("crates");
    Path path1 = null.resolve(paramString);
    Path path2 = path1.toAbsolutePath().normalize();
    synchronized (this.mSync) {
      if (this.mCratesDir == null)
        this.mCratesDir = null.toFile(); 
      ensurePrivateDirExists(this.mCratesDir);
      null = path2.toFile();
      return ensurePrivateDirExists((File)null);
    } 
  }
  
  public File getNoBackupFilesDir() {
    synchronized (this.mSync) {
      if (this.mNoBackupFilesDir == null) {
        File file = new File();
        this(getDataDir(), "no_backup");
        this.mNoBackupFilesDir = file;
      } 
      return ensurePrivateDirExists(this.mNoBackupFilesDir);
    } 
  }
  
  public File getExternalFilesDir(String paramString) {
    File[] arrayOfFile = getExternalFilesDirs(paramString);
    if (arrayOfFile != null && arrayOfFile.length > 0) {
      File file = arrayOfFile[0];
    } else {
      arrayOfFile = null;
    } 
    return (File)arrayOfFile;
  }
  
  public File[] getExternalFilesDirs(String paramString) {
    synchronized (this.mSync) {
      File[] arrayOfFile1 = Environment.buildExternalStorageAppFilesDirs(getPackageName());
      File[] arrayOfFile2 = arrayOfFile1;
      if (paramString != null)
        arrayOfFile2 = Environment.buildPaths(arrayOfFile1, new String[] { paramString }); 
      return ensureExternalDirsExistOrFilter(arrayOfFile2, true);
    } 
  }
  
  public File getObbDir() {
    File[] arrayOfFile = getObbDirs();
    if (arrayOfFile != null && arrayOfFile.length > 0) {
      File file = arrayOfFile[0];
    } else {
      arrayOfFile = null;
    } 
    return (File)arrayOfFile;
  }
  
  public File[] getObbDirs() {
    synchronized (this.mSync) {
      File[] arrayOfFile = Environment.buildExternalStorageAppObbDirs(getPackageName());
      arrayOfFile = ensureExternalDirsExistOrFilter(arrayOfFile, true);
      return arrayOfFile;
    } 
  }
  
  public File getCacheDir() {
    synchronized (this.mSync) {
      if (this.mCacheDir == null) {
        File file = new File();
        this(getDataDir(), "cache");
        this.mCacheDir = file;
      } 
      return ensurePrivateCacheDirExists(this.mCacheDir, "user.inode_cache");
    } 
  }
  
  public File getCodeCacheDir() {
    synchronized (this.mSync) {
      if (this.mCodeCacheDir == null)
        this.mCodeCacheDir = getCodeCacheDirBeforeBind(getDataDir()); 
      return ensurePrivateCacheDirExists(this.mCodeCacheDir, "user.inode_code_cache");
    } 
  }
  
  static File getCodeCacheDirBeforeBind(File paramFile) {
    return new File(paramFile, "code_cache");
  }
  
  public File getExternalCacheDir() {
    File[] arrayOfFile = getExternalCacheDirs();
    if (arrayOfFile != null && arrayOfFile.length > 0) {
      File file = arrayOfFile[0];
    } else {
      arrayOfFile = null;
    } 
    return (File)arrayOfFile;
  }
  
  public File[] getExternalCacheDirs() {
    synchronized (this.mSync) {
      File[] arrayOfFile = Environment.buildExternalStorageAppCacheDirs(getPackageName());
      arrayOfFile = ensureExternalDirsExistOrFilter(arrayOfFile, false);
      return arrayOfFile;
    } 
  }
  
  public File[] getExternalMediaDirs() {
    synchronized (this.mSync) {
      File[] arrayOfFile = Environment.buildExternalStorageAppMediaDirs(getPackageName());
      arrayOfFile = ensureExternalDirsExistOrFilter(arrayOfFile, true);
      return arrayOfFile;
    } 
  }
  
  public File getPreloadsFileCache() {
    return Environment.getDataPreloadsFileCacheDirectory(getPackageName());
  }
  
  public File getFileStreamPath(String paramString) {
    return makeFilename(getFilesDir(), paramString);
  }
  
  public File getSharedPreferencesPath(String paramString) {
    File file = getPreferencesDir();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(".xml");
    return makeFilename(file, stringBuilder.toString());
  }
  
  public String[] fileList() {
    return FileUtils.listOrEmpty(getFilesDir());
  }
  
  public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory) {
    return openOrCreateDatabase(paramString, paramInt, paramCursorFactory, null);
  }
  
  public SQLiteDatabase openOrCreateDatabase(String paramString, int paramInt, SQLiteDatabase.CursorFactory paramCursorFactory, DatabaseErrorHandler paramDatabaseErrorHandler) {
    checkMode(paramInt);
    File file = getDatabasePath(paramString);
    int i = 268435456;
    if ((paramInt & 0x8) != 0)
      i = 0x10000000 | 0x20000000; 
    int j = i;
    if ((paramInt & 0x10) != 0)
      j = i | 0x10; 
    SQLiteDatabase sQLiteDatabase = SQLiteDatabase.openDatabase(file.getPath(), paramCursorFactory, j, paramDatabaseErrorHandler);
    setFilePermissionsFromMode(file.getPath(), paramInt, 0);
    return sQLiteDatabase;
  }
  
  public boolean moveDatabaseFrom(Context paramContext, String paramString) {
    // Byte code:
    //   0: ldc android/app/ContextImpl
    //   2: monitorenter
    //   3: aload_1
    //   4: aload_2
    //   5: invokevirtual getDatabasePath : (Ljava/lang/String;)Ljava/io/File;
    //   8: astore_3
    //   9: aload_0
    //   10: aload_2
    //   11: invokevirtual getDatabasePath : (Ljava/lang/String;)Ljava/io/File;
    //   14: astore_2
    //   15: aload_3
    //   16: invokevirtual getParentFile : ()Ljava/io/File;
    //   19: astore_1
    //   20: aload_2
    //   21: invokevirtual getParentFile : ()Ljava/io/File;
    //   24: astore_2
    //   25: aload_3
    //   26: invokevirtual getName : ()Ljava/lang/String;
    //   29: astore_3
    //   30: aload_1
    //   31: aload_2
    //   32: aload_3
    //   33: invokestatic moveFiles : (Ljava/io/File;Ljava/io/File;Ljava/lang/String;)I
    //   36: iconst_m1
    //   37: if_icmpeq -> 46
    //   40: iconst_1
    //   41: istore #4
    //   43: goto -> 49
    //   46: iconst_0
    //   47: istore #4
    //   49: ldc android/app/ContextImpl
    //   51: monitorexit
    //   52: iload #4
    //   54: ireturn
    //   55: astore_1
    //   56: ldc android/app/ContextImpl
    //   58: monitorexit
    //   59: aload_1
    //   60: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #904	-> 0
    //   #905	-> 3
    //   #906	-> 9
    //   #907	-> 15
    //   #908	-> 25
    //   #907	-> 30
    //   #909	-> 55
    // Exception table:
    //   from	to	target	type
    //   3	9	55	finally
    //   9	15	55	finally
    //   15	25	55	finally
    //   25	30	55	finally
    //   30	40	55	finally
    //   49	52	55	finally
    //   56	59	55	finally
  }
  
  public boolean deleteDatabase(String paramString) {
    try {
      File file = getDatabasePath(paramString);
      return SQLiteDatabase.deleteDatabase(file);
    } catch (Exception exception) {
      return false;
    } 
  }
  
  public File getDatabasePath(String paramString) {
    File file;
    if (paramString.charAt(0) == File.separatorChar) {
      String str = paramString.substring(0, paramString.lastIndexOf(File.separatorChar));
      File file1 = new File(str);
      paramString = paramString.substring(paramString.lastIndexOf(File.separatorChar));
      file = new File(file1, paramString);
      if (!file1.isDirectory() && file1.mkdir())
        FileUtils.setPermissions(file1.getPath(), 505, -1, -1); 
    } else {
      File file1 = getDatabasesDir();
      file = makeFilename(file1, (String)file);
    } 
    return file;
  }
  
  public String[] databaseList() {
    return FileUtils.listOrEmpty(getDatabasesDir());
  }
  
  private File getDatabasesDir() {
    synchronized (this.mSync) {
      if (this.mDatabasesDir == null)
        if ("android".equals(getPackageName())) {
          File file = new File();
          this("/data/system");
          this.mDatabasesDir = file;
        } else {
          File file = new File();
          this(getDataDir(), "databases");
          this.mDatabasesDir = file;
        }  
      return ensurePrivateDirExists(this.mDatabasesDir);
    } 
  }
  
  @Deprecated
  public Drawable getWallpaper() {
    return getWallpaperManager().getDrawable();
  }
  
  @Deprecated
  public Drawable peekWallpaper() {
    return getWallpaperManager().peekDrawable();
  }
  
  @Deprecated
  public int getWallpaperDesiredMinimumWidth() {
    return getWallpaperManager().getDesiredMinimumWidth();
  }
  
  @Deprecated
  public int getWallpaperDesiredMinimumHeight() {
    return getWallpaperManager().getDesiredMinimumHeight();
  }
  
  @Deprecated
  public void setWallpaper(Bitmap paramBitmap) throws IOException {
    getWallpaperManager().setBitmap(paramBitmap);
  }
  
  @Deprecated
  public void setWallpaper(InputStream paramInputStream) throws IOException {
    getWallpaperManager().setStream(paramInputStream);
  }
  
  @Deprecated
  public void clearWallpaper() throws IOException {
    getWallpaperManager().clear();
  }
  
  private WallpaperManager getWallpaperManager() {
    return getSystemService(WallpaperManager.class);
  }
  
  public void startActivity(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    startActivity(paramIntent, null);
  }
  
  public void startActivityAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    startActivityAsUser(paramIntent, null, paramUserHandle);
  }
  
  public void startActivity(Intent paramIntent, Bundle paramBundle) {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial warnIfCallingFromSystemProcess : ()V
    //   4: aload_0
    //   5: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   8: getfield targetSdkVersion : I
    //   11: istore_3
    //   12: aload_1
    //   13: invokevirtual getFlags : ()I
    //   16: ldc_w 268435456
    //   19: iand
    //   20: ifne -> 97
    //   23: iload_3
    //   24: bipush #24
    //   26: if_icmplt -> 35
    //   29: iload_3
    //   30: bipush #28
    //   32: if_icmplt -> 97
    //   35: aload_2
    //   36: ifnull -> 50
    //   39: aload_2
    //   40: invokestatic fromBundle : (Landroid/os/Bundle;)Landroid/app/ActivityOptions;
    //   43: invokevirtual getLaunchTaskId : ()I
    //   46: iconst_m1
    //   47: if_icmpne -> 97
    //   50: new android/content/pm/OplusPackageManager
    //   53: dup
    //   54: aload_0
    //   55: invokespecial <init> : (Landroid/content/Context;)V
    //   58: astore #4
    //   60: aload #4
    //   62: sipush #719
    //   65: aload_0
    //   66: invokevirtual getPackageName : ()Ljava/lang/String;
    //   69: invokevirtual inCptWhiteList : (ILjava/lang/String;)Z
    //   72: ifeq -> 86
    //   75: aload_1
    //   76: ldc_w 268435456
    //   79: invokevirtual addFlags : (I)Landroid/content/Intent;
    //   82: pop
    //   83: goto -> 97
    //   86: new android/util/AndroidRuntimeException
    //   89: dup
    //   90: ldc_w 'Calling startActivity() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag. Is this really what you want?'
    //   93: invokespecial <init> : (Ljava/lang/String;)V
    //   96: athrow
    //   97: aload_0
    //   98: getfield mMainThread : Landroid/app/ActivityThread;
    //   101: invokevirtual getInstrumentation : ()Landroid/app/Instrumentation;
    //   104: astore #4
    //   106: aload_0
    //   107: invokevirtual getOuterContext : ()Landroid/content/Context;
    //   110: astore #5
    //   112: aload_0
    //   113: getfield mMainThread : Landroid/app/ActivityThread;
    //   116: invokevirtual getApplicationThread : ()Landroid/app/ActivityThread$ApplicationThread;
    //   119: astore #6
    //   121: aconst_null
    //   122: checkcast android/app/Activity
    //   125: astore #7
    //   127: aload #4
    //   129: aload #5
    //   131: aload #6
    //   133: aconst_null
    //   134: aload #7
    //   136: aload_1
    //   137: iconst_m1
    //   138: aload_2
    //   139: invokevirtual execStartActivity : (Landroid/content/Context;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/app/Activity;Landroid/content/Intent;ILandroid/os/Bundle;)Landroid/app/Instrumentation$ActivityResult;
    //   142: pop
    //   143: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1024	-> 0
    //   #1030	-> 4
    //   #1032	-> 12
    //   #1036	-> 39
    //   #1044	-> 50
    //   #1045	-> 60
    //   #1046	-> 75
    //   #1048	-> 86
    //   #1055	-> 97
    //   #1056	-> 106
    //   #1055	-> 127
    //   #1058	-> 143
  }
  
  public void startActivityAsUser(Intent paramIntent, Bundle paramBundle, UserHandle paramUserHandle) {
    try {
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      try {
        ActivityThread activityThread = this.mMainThread;
        ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
        String str2 = getBasePackageName(), str1 = getAttributionTag();
        ContentResolver contentResolver = getContentResolver();
        try {
          String str = paramIntent.resolveTypeIfNeeded(contentResolver);
          int i = paramUserHandle.getIdentifier();
          iActivityTaskManager.startActivityAsUser(applicationThread, str2, str1, paramIntent, str, null, null, 0, 268435456, null, paramBundle, i);
          return;
        } catch (RemoteException null) {}
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    throw remoteException.rethrowFromSystemServer();
  }
  
  public void startActivities(Intent[] paramArrayOfIntent) {
    warnIfCallingFromSystemProcess();
    startActivities(paramArrayOfIntent, null);
  }
  
  public int startActivitiesAsUser(Intent[] paramArrayOfIntent, Bundle paramBundle, UserHandle paramUserHandle) {
    if ((paramArrayOfIntent[0].getFlags() & 0x10000000) != 0) {
      Instrumentation instrumentation = this.mMainThread.getInstrumentation();
      Context context = getOuterContext();
      ActivityThread.ApplicationThread applicationThread = this.mMainThread.getApplicationThread();
      Activity activity = (Activity)null;
      int i = paramUserHandle.getIdentifier();
      return instrumentation.execStartActivitiesAsUser(context, (IBinder)applicationThread, null, activity, paramArrayOfIntent, paramBundle, i);
    } 
    throw new AndroidRuntimeException("Calling startActivities() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag on first Intent. Is this really what you want?");
  }
  
  public void startActivities(Intent[] paramArrayOfIntent, Bundle paramBundle) {
    warnIfCallingFromSystemProcess();
    if ((paramArrayOfIntent[0].getFlags() & 0x10000000) != 0) {
      Instrumentation instrumentation = this.mMainThread.getInstrumentation();
      Context context = getOuterContext();
      ActivityThread.ApplicationThread applicationThread = this.mMainThread.getApplicationThread();
      Activity activity = (Activity)null;
      instrumentation.execStartActivities(context, (IBinder)applicationThread, null, activity, paramArrayOfIntent, paramBundle);
      return;
    } 
    throw new AndroidRuntimeException("Calling startActivities() from outside of an Activity  context requires the FLAG_ACTIVITY_NEW_TASK flag on first Intent. Is this really what you want?");
  }
  
  public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3) throws IntentSender.SendIntentException {
    startIntentSender(paramIntentSender, paramIntent, paramInt1, paramInt2, paramInt3, null);
  }
  
  public void startIntentSender(IntentSender paramIntentSender, Intent paramIntent, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) throws IntentSender.SendIntentException {
    // Byte code:
    //   0: aconst_null
    //   1: astore #7
    //   3: aload_2
    //   4: ifnull -> 28
    //   7: aload_2
    //   8: aload_0
    //   9: invokevirtual migrateExtraStreamToClipData : (Landroid/content/Context;)Z
    //   12: pop
    //   13: aload_2
    //   14: aload_0
    //   15: invokevirtual prepareToLeaveProcess : (Landroid/content/Context;)V
    //   18: aload_2
    //   19: aload_0
    //   20: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   23: invokevirtual resolveTypeIfNeeded : (Landroid/content/ContentResolver;)Ljava/lang/String;
    //   26: astore #7
    //   28: invokestatic getService : ()Landroid/app/IActivityTaskManager;
    //   31: astore #8
    //   33: aload_0
    //   34: getfield mMainThread : Landroid/app/ActivityThread;
    //   37: astore #9
    //   39: aload #9
    //   41: invokevirtual getApplicationThread : ()Landroid/app/ActivityThread$ApplicationThread;
    //   44: astore #10
    //   46: aload_1
    //   47: ifnull -> 59
    //   50: aload_1
    //   51: invokevirtual getTarget : ()Landroid/content/IIntentSender;
    //   54: astore #9
    //   56: goto -> 62
    //   59: aconst_null
    //   60: astore #9
    //   62: aload_1
    //   63: ifnull -> 74
    //   66: aload_1
    //   67: invokevirtual getWhitelistToken : ()Landroid/os/IBinder;
    //   70: astore_1
    //   71: goto -> 76
    //   74: aconst_null
    //   75: astore_1
    //   76: aload #8
    //   78: aload #10
    //   80: aload #9
    //   82: aload_1
    //   83: aload_2
    //   84: aload #7
    //   86: aconst_null
    //   87: aconst_null
    //   88: iconst_0
    //   89: iload_3
    //   90: iload #4
    //   92: aload #6
    //   94: invokeinterface startActivityIntentSender : (Landroid/app/IApplicationThread;Landroid/content/IIntentSender;Landroid/os/IBinder;Landroid/content/Intent;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;IIILandroid/os/Bundle;)I
    //   99: istore_3
    //   100: iload_3
    //   101: bipush #-96
    //   103: if_icmpeq -> 112
    //   106: iload_3
    //   107: aconst_null
    //   108: invokestatic checkStartActivityResult : (ILjava/lang/Object;)V
    //   111: return
    //   112: new android/content/IntentSender$SendIntentException
    //   115: astore_1
    //   116: aload_1
    //   117: invokespecial <init> : ()V
    //   120: aload_1
    //   121: athrow
    //   122: astore_1
    //   123: aload_1
    //   124: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   127: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1120	-> 0
    //   #1121	-> 3
    //   #1122	-> 7
    //   #1123	-> 13
    //   #1124	-> 18
    //   #1126	-> 28
    //   #1127	-> 39
    //   #1128	-> 46
    //   #1129	-> 62
    //   #1127	-> 76
    //   #1132	-> 100
    //   #1135	-> 106
    //   #1138	-> 111
    //   #1139	-> 111
    //   #1133	-> 112
    //   #1136	-> 122
    //   #1137	-> 123
    // Exception table:
    //   from	to	target	type
    //   7	13	122	android/os/RemoteException
    //   13	18	122	android/os/RemoteException
    //   18	28	122	android/os/RemoteException
    //   28	39	122	android/os/RemoteException
    //   39	46	122	android/os/RemoteException
    //   50	56	122	android/os/RemoteException
    //   66	71	122	android/os/RemoteException
    //   76	100	122	android/os/RemoteException
    //   106	111	122	android/os/RemoteException
    //   112	122	122	android/os/RemoteException
  }
  
  public void sendBroadcast(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, null, -1, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcast(Intent paramIntent, String paramString) {
    String[] arrayOfString;
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, -1, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcastMultiplePermissions(Intent paramIntent, String[] paramArrayOfString) {
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, paramArrayOfString, -1, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcastAsUserMultiplePermissions(Intent paramIntent, UserHandle paramUserHandle, String[] paramArrayOfString) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, paramArrayOfString, -1, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcast(Intent paramIntent, String paramString, Bundle paramBundle) {
    String[] arrayOfString;
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, -1, paramBundle, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcast(Intent paramIntent, String paramString, int paramInt) {
    String[] arrayOfString;
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, paramInt, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, String paramString) {
    String[] arrayOfString;
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, -1, null, true, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, String paramString1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString2, Bundle paramBundle) {
    sendOrderedBroadcast(paramIntent, paramString1, -1, paramBroadcastReceiver, paramHandler, paramInt, paramString2, paramBundle, (Bundle)null);
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, String paramString1, Bundle paramBundle1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString2, Bundle paramBundle2) {
    sendOrderedBroadcast(paramIntent, paramString1, -1, paramBroadcastReceiver, paramHandler, paramInt, paramString2, paramBundle2, paramBundle1);
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, String paramString1, int paramInt1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt2, String paramString2, Bundle paramBundle) {
    sendOrderedBroadcast(paramIntent, paramString1, paramInt1, paramBroadcastReceiver, paramHandler, paramInt2, paramString2, paramBundle, (Bundle)null);
  }
  
  void sendOrderedBroadcast(Intent paramIntent, String paramString1, int paramInt1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt2, String paramString2, Bundle paramBundle1, Bundle paramBundle2) {
    String[] arrayOfString;
    warnIfCallingFromSystemProcess();
    if (paramBroadcastReceiver != null) {
      IIntentReceiver iIntentReceiver;
      if (this.mPackageInfo != null) {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk loadedApk = this.mPackageInfo;
        Context context = getOuterContext();
        ActivityThread activityThread = this.mMainThread;
        Instrumentation instrumentation = activityThread.getInstrumentation();
        iIntentReceiver = loadedApk.getReceiverDispatcher(paramBroadcastReceiver, context, paramHandler, instrumentation, false);
      } else {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        iIntentReceiver = (new LoadedApk.ReceiverDispatcher((BroadcastReceiver)iIntentReceiver, getOuterContext(), paramHandler, null, false)).getIIntentReceiver();
      } 
    } else {
      paramBroadcastReceiver = null;
    } 
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString1 == null) {
      paramString1 = null;
    } else {
      arrayOfString = new String[] { paramString1 };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, (IIntentReceiver)paramBroadcastReceiver, paramInt2, paramString2, paramBundle1, arrayOfString, paramInt1, paramBundle2, true, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, null, -1, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString) {
    sendBroadcastAsUser(paramIntent, paramUserHandle, paramString, -1);
  }
  
  public void sendBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString, Bundle paramBundle) {
    String arrayOfString[], str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, -1, paramBundle, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString, int paramInt) {
    String arrayOfString[], str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString == null) {
      paramString = null;
    } else {
      arrayOfString = new String[] { paramString };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, arrayOfString, paramInt, null, false, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendOrderedBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString2, Bundle paramBundle) {
    sendOrderedBroadcastAsUser(paramIntent, paramUserHandle, paramString1, -1, null, paramBroadcastReceiver, paramHandler, paramInt, paramString2, paramBundle);
  }
  
  public void sendOrderedBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString1, int paramInt1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt2, String paramString2, Bundle paramBundle) {
    sendOrderedBroadcastAsUser(paramIntent, paramUserHandle, paramString1, paramInt1, null, paramBroadcastReceiver, paramHandler, paramInt2, paramString2, paramBundle);
  }
  
  public void sendOrderedBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, String paramString1, int paramInt1, Bundle paramBundle1, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt2, String paramString2, Bundle paramBundle2) {
    String[] arrayOfString;
    if (paramBroadcastReceiver != null) {
      IIntentReceiver iIntentReceiver;
      if (this.mPackageInfo != null) {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk loadedApk = this.mPackageInfo;
        Context context = getOuterContext();
        ActivityThread activityThread = this.mMainThread;
        Instrumentation instrumentation = activityThread.getInstrumentation();
        iIntentReceiver = loadedApk.getReceiverDispatcher(paramBroadcastReceiver, context, paramHandler, instrumentation, false);
      } else {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk.ReceiverDispatcher receiverDispatcher = new LoadedApk.ReceiverDispatcher((BroadcastReceiver)iIntentReceiver, getOuterContext(), paramHandler, null, false);
        IIntentReceiver iIntentReceiver1 = receiverDispatcher.getIIntentReceiver();
      } 
    } else {
      paramBroadcastReceiver = null;
    } 
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    if (paramString1 == null) {
      paramString1 = null;
    } else {
      arrayOfString = new String[] { paramString1 };
    } 
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, (IIntentReceiver)paramBroadcastReceiver, paramInt2, paramString2, paramBundle2, arrayOfString, paramInt1, paramBundle1, true, false, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, String paramString1, String paramString2, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString3, Bundle paramBundle) {
    int i = -1;
    if (!TextUtils.isEmpty(paramString2))
      i = AppOpsManager.strOpToOp(paramString2); 
    sendOrderedBroadcastAsUser(paramIntent, getUser(), paramString1, i, paramBroadcastReceiver, paramHandler, paramInt, paramString3, paramBundle);
  }
  
  public void sendOrderedBroadcast(Intent paramIntent, int paramInt, String paramString1, String paramString2, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, String paramString3, Bundle paramBundle1, Bundle paramBundle2) {
    int i = -1;
    if (!TextUtils.isEmpty(paramString2))
      i = AppOpsManager.strOpToOp(paramString2); 
    sendOrderedBroadcastAsUser(paramIntent, getUser(), paramString1, i, paramBundle2, paramBroadcastReceiver, paramHandler, paramInt, paramString3, paramBundle1);
  }
  
  @Deprecated
  public void sendStickyBroadcast(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, null, -1, null, false, true, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void sendStickyOrderedBroadcast(Intent paramIntent, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString, Bundle paramBundle) {
    warnIfCallingFromSystemProcess();
    if (paramBroadcastReceiver != null) {
      IIntentReceiver iIntentReceiver;
      if (this.mPackageInfo != null) {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk loadedApk = this.mPackageInfo;
        Context context = getOuterContext();
        ActivityThread activityThread = this.mMainThread;
        Instrumentation instrumentation = activityThread.getInstrumentation();
        iIntentReceiver = loadedApk.getReceiverDispatcher(paramBroadcastReceiver, context, paramHandler, instrumentation, false);
      } else {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        iIntentReceiver = (new LoadedApk.ReceiverDispatcher((BroadcastReceiver)iIntentReceiver, getOuterContext(), paramHandler, null, false)).getIIntentReceiver();
      } 
    } else {
      paramBroadcastReceiver = null;
    } 
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = getUserId();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, (IIntentReceiver)paramBroadcastReceiver, paramInt, paramString, paramBundle, null, -1, null, true, true, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void removeStickyBroadcast(Intent paramIntent) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    Intent intent = paramIntent;
    if (str != null) {
      intent = new Intent(paramIntent);
      intent.setDataAndType(intent.getData(), str);
    } 
    try {
      intent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      int i = getUserId();
      iActivityManager.unbroadcastIntent(applicationThread, intent, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void sendStickyBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, null, -1, null, false, true, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void sendStickyBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, Bundle paramBundle) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, null, -1, null, null, null, -1, paramBundle, false, true, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void sendStickyOrderedBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle, BroadcastReceiver paramBroadcastReceiver, Handler paramHandler, int paramInt, String paramString, Bundle paramBundle) {
    if (paramBroadcastReceiver != null) {
      IIntentReceiver iIntentReceiver;
      if (this.mPackageInfo != null) {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk loadedApk = this.mPackageInfo;
        Context context = getOuterContext();
        ActivityThread activityThread = this.mMainThread;
        Instrumentation instrumentation = activityThread.getInstrumentation();
        iIntentReceiver = loadedApk.getReceiverDispatcher(paramBroadcastReceiver, context, paramHandler, instrumentation, false);
      } else {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        iIntentReceiver = (new LoadedApk.ReceiverDispatcher((BroadcastReceiver)iIntentReceiver, getOuterContext(), paramHandler, null, false)).getIIntentReceiver();
      } 
    } else {
      paramBroadcastReceiver = null;
    } 
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    try {
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.broadcastIntentWithFeature(applicationThread, str1, paramIntent, str, (IIntentReceiver)paramBroadcastReceiver, paramInt, paramString, paramBundle, null, -1, null, true, true, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void removeStickyBroadcastAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
    Intent intent = paramIntent;
    if (str != null) {
      intent = new Intent(paramIntent);
      intent.setDataAndType(intent.getData(), str);
    } 
    try {
      intent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      int i = paramUserHandle.getIdentifier();
      iActivityManager.unbroadcastIntent(applicationThread, intent, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter) {
    return registerReceiver(paramBroadcastReceiver, paramIntentFilter, null, null);
  }
  
  public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, int paramInt) {
    return registerReceiver(paramBroadcastReceiver, paramIntentFilter, null, null, paramInt);
  }
  
  public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler) {
    int i = getUserId();
    Context context = getOuterContext();
    return registerReceiverInternal(paramBroadcastReceiver, i, paramIntentFilter, paramString, paramHandler, context, 0);
  }
  
  public Intent registerReceiver(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler, int paramInt) {
    int i = getUserId();
    Context context = getOuterContext();
    return registerReceiverInternal(paramBroadcastReceiver, i, paramIntentFilter, paramString, paramHandler, context, paramInt);
  }
  
  public Intent registerReceiverForAllUsers(BroadcastReceiver paramBroadcastReceiver, IntentFilter paramIntentFilter, String paramString, Handler paramHandler) {
    return registerReceiverAsUser(paramBroadcastReceiver, UserHandle.ALL, paramIntentFilter, paramString, paramHandler);
  }
  
  public Intent registerReceiverAsUser(BroadcastReceiver paramBroadcastReceiver, UserHandle paramUserHandle, IntentFilter paramIntentFilter, String paramString, Handler paramHandler) {
    int i = paramUserHandle.getIdentifier();
    Context context = getOuterContext();
    return registerReceiverInternal(paramBroadcastReceiver, i, paramIntentFilter, paramString, paramHandler, context, 0);
  }
  
  private Intent registerReceiverInternal(BroadcastReceiver paramBroadcastReceiver, int paramInt1, IntentFilter paramIntentFilter, String paramString, Handler paramHandler, Context paramContext, int paramInt2) {
    if (paramBroadcastReceiver != null) {
      IIntentReceiver iIntentReceiver;
      if (this.mPackageInfo != null && paramContext != null) {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk loadedApk = this.mPackageInfo;
        ActivityThread activityThread = this.mMainThread;
        Instrumentation instrumentation = activityThread.getInstrumentation();
        iIntentReceiver = loadedApk.getReceiverDispatcher(paramBroadcastReceiver, paramContext, paramHandler, instrumentation, true);
      } else {
        if (paramHandler == null)
          paramHandler = this.mMainThread.getHandler(); 
        LoadedApk.ReceiverDispatcher receiverDispatcher = new LoadedApk.ReceiverDispatcher((BroadcastReceiver)iIntentReceiver, paramContext, paramHandler, null, true);
        IIntentReceiver iIntentReceiver1 = receiverDispatcher.getIIntentReceiver();
      } 
    } else {
      paramBroadcastReceiver = null;
    } 
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str1 = this.mBasePackageName, str2 = getAttributionTag();
      Intent intent = iActivityManager.registerReceiverWithFeature(applicationThread, str1, str2, (IIntentReceiver)paramBroadcastReceiver, paramIntentFilter, paramString, paramInt1, paramInt2);
      if (intent != null) {
        intent.setExtrasClassLoader(getClassLoader());
        intent.prepareToEnterProcess();
      } 
      return intent;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterReceiver(BroadcastReceiver paramBroadcastReceiver) {
    LoadedApk loadedApk = this.mPackageInfo;
    if (loadedApk != null) {
      Context context = getOuterContext();
      IIntentReceiver iIntentReceiver = loadedApk.forgetReceiverDispatcher(context, paramBroadcastReceiver);
      try {
        ActivityManager.getService().unregisterReceiver(iIntentReceiver);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    throw new RuntimeException("Not supported in system context");
  }
  
  private void validateServiceIntent(Intent paramIntent) {
    if (paramIntent.getComponent() == null && paramIntent.getPackage() == null) {
      String str;
      if ((getApplicationInfo()).targetSdkVersion < 21) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Implicit intents with startService are not safe: ");
        stringBuilder.append(paramIntent);
        stringBuilder.append(" ");
        stringBuilder.append(Debug.getCallers(2, 3));
        str = stringBuilder.toString();
        Log.w("ContextImpl", str);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Service Intent must be explicit: ");
        stringBuilder.append(str);
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException(stringBuilder.toString());
        throw illegalArgumentException;
      } 
    } 
  }
  
  public ComponentName startService(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    return startServiceCommon(paramIntent, false, this.mUser);
  }
  
  public ComponentName startForegroundService(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    return startServiceCommon(paramIntent, true, this.mUser);
  }
  
  public boolean stopService(Intent paramIntent) {
    warnIfCallingFromSystemProcess();
    return stopServiceCommon(paramIntent, this.mUser);
  }
  
  public ComponentName startServiceAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    return startServiceCommon(paramIntent, false, paramUserHandle);
  }
  
  public ComponentName startForegroundServiceAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    return startServiceCommon(paramIntent, true, paramUserHandle);
  }
  
  private ComponentName startServiceCommon(Intent paramIntent, boolean paramBoolean, UserHandle paramUserHandle) {
    try {
      OplusPackageManager oplusPackageManager = new OplusPackageManager();
      this(this);
      validateServiceIntent(paramIntent);
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str2 = paramIntent.resolveTypeIfNeeded(getContentResolver());
      String str1 = getOpPackageName(), str3 = getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      ComponentName componentName = iActivityManager.startService(applicationThread, paramIntent, str2, paramBoolean, str1, str3, i);
      if (componentName != null) {
        paramBoolean = componentName.getPackageName().equals("!");
        if (!paramBoolean) {
          paramBoolean = componentName.getPackageName().equals("!!");
          if (!paramBoolean) {
            if (componentName.getPackageName().equals("?")) {
              if (oplusPackageManager.inCptWhiteList(696, getPackageName()))
                return null; 
              if (paramIntent != null) {
                if (DEBUG_CIL) {
                  StringBuilder stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("The package of the service is ");
                  stringBuilder1.append(paramIntent.getPackage());
                  stringBuilder1.append(" The component of the service is ");
                  stringBuilder1.append(paramIntent.getComponent());
                  Log.e("ContextImpl", stringBuilder1.toString());
                } 
                if ((paramIntent.getComponent() != null && oplusPackageManager.inCptWhiteList(711, paramIntent.getComponent().getPackageName())) || (
                  paramIntent.getPackage() != null && oplusPackageManager.inCptWhiteList(711, paramIntent.getPackage())))
                  return null; 
              } 
              IllegalStateException illegalStateException = new IllegalStateException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Not allowed to start service ");
              stringBuilder.append(paramIntent);
              stringBuilder.append(": ");
              stringBuilder.append(componentName.getClassName());
              this(stringBuilder.toString());
              throw illegalStateException;
            } 
          } else {
            SecurityException securityException = new SecurityException();
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Unable to start service ");
            stringBuilder.append(paramIntent);
            stringBuilder.append(": ");
            stringBuilder.append(componentName.getClassName());
            this(stringBuilder.toString());
            throw securityException;
          } 
        } else {
          SecurityException securityException = new SecurityException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Not allowed to start service ");
          stringBuilder.append(paramIntent);
          stringBuilder.append(" without permission ");
          stringBuilder.append(componentName.getClassName());
          this(stringBuilder.toString());
          throw securityException;
        } 
      } 
      return componentName;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean stopServiceAsUser(Intent paramIntent, UserHandle paramUserHandle) {
    return stopServiceCommon(paramIntent, paramUserHandle);
  }
  
  private boolean stopServiceCommon(Intent paramIntent, UserHandle paramUserHandle) {
    try {
      validateServiceIntent(paramIntent);
      paramIntent.prepareToLeaveProcess(this);
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      String str = paramIntent.resolveTypeIfNeeded(getContentResolver());
      int i = paramUserHandle.getIdentifier();
      i = iActivityManager.stopService(applicationThread, paramIntent, str, i);
      if (i >= 0) {
        boolean bool;
        if (i != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      } 
      SecurityException securityException = new SecurityException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Not allowed to stop service ");
      stringBuilder.append(paramIntent);
      this(stringBuilder.toString());
      throw securityException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean bindService(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt) {
    warnIfCallingFromSystemProcess();
    Handler handler = this.mMainThread.getHandler();
    UserHandle userHandle = getUser();
    return bindServiceCommon(paramIntent, paramServiceConnection, paramInt, null, handler, null, userHandle);
  }
  
  public boolean bindService(Intent paramIntent, int paramInt, Executor paramExecutor, ServiceConnection paramServiceConnection) {
    warnIfCallingFromSystemProcess();
    return bindServiceCommon(paramIntent, paramServiceConnection, paramInt, null, null, paramExecutor, getUser());
  }
  
  public boolean bindIsolatedService(Intent paramIntent, int paramInt, String paramString, Executor paramExecutor, ServiceConnection paramServiceConnection) {
    warnIfCallingFromSystemProcess();
    if (paramString != null)
      return bindServiceCommon(paramIntent, paramServiceConnection, paramInt, paramString, null, paramExecutor, getUser()); 
    throw new NullPointerException("null instanceName");
  }
  
  public boolean bindServiceAsUser(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt, UserHandle paramUserHandle) {
    return bindServiceCommon(paramIntent, paramServiceConnection, paramInt, null, this.mMainThread.getHandler(), null, paramUserHandle);
  }
  
  public boolean bindServiceAsUser(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt, Handler paramHandler, UserHandle paramUserHandle) {
    if (paramHandler != null)
      return bindServiceCommon(paramIntent, paramServiceConnection, paramInt, null, paramHandler, null, paramUserHandle); 
    throw new IllegalArgumentException("handler must not be null.");
  }
  
  public IServiceConnection getServiceDispatcher(ServiceConnection paramServiceConnection, Handler paramHandler, int paramInt) {
    return this.mPackageInfo.getServiceDispatcher(paramServiceConnection, getOuterContext(), paramHandler, paramInt);
  }
  
  public IApplicationThread getIApplicationThread() {
    return this.mMainThread.getApplicationThread();
  }
  
  public Handler getMainThreadHandler() {
    return this.mMainThread.getHandler();
  }
  
  private boolean bindServiceCommon(Intent paramIntent, ServiceConnection paramServiceConnection, int paramInt, String paramString, Handler paramHandler, Executor paramExecutor, UserHandle paramUserHandle) {
    if (paramServiceConnection != null) {
      if (paramHandler == null || paramExecutor == null) {
        LoadedApk loadedApk = this.mPackageInfo;
        if (loadedApk != null) {
          IServiceConnection iServiceConnection;
          if (paramExecutor != null) {
            iServiceConnection = loadedApk.getServiceDispatcher(paramServiceConnection, getOuterContext(), paramExecutor, paramInt);
          } else {
            iServiceConnection = loadedApk.getServiceDispatcher((ServiceConnection)iServiceConnection, getOuterContext(), paramHandler, paramInt);
          } 
          validateServiceIntent(paramIntent);
          try {
            IBinder iBinder = getActivityToken();
            if (iBinder == null && (paramInt & 0x1) == 0 && this.mPackageInfo != null) {
              LoadedApk loadedApk1 = this.mPackageInfo;
              int i = (loadedApk1.getApplicationInfo()).targetSdkVersion;
              if (i < 14)
                paramInt |= 0x20; 
            } 
            try {
              paramIntent.prepareToLeaveProcess(this);
              IActivityManager iActivityManager = ActivityManager.getService();
              ActivityThread activityThread = this.mMainThread;
              ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
              IBinder iBinder1 = getActivityToken();
              String str1 = paramIntent.resolveTypeIfNeeded(getContentResolver());
              String str2 = getOpPackageName();
              int i = paramUserHandle.getIdentifier();
              paramInt = iActivityManager.bindIsolatedService(applicationThread, iBinder1, paramIntent, str1, iServiceConnection, paramInt, paramString, str2, i);
              if (paramInt >= 0) {
                boolean bool;
                if (paramInt != 0) {
                  bool = true;
                } else {
                  bool = false;
                } 
                return bool;
              } 
              SecurityException securityException = new SecurityException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Not allowed to bind to service ");
              stringBuilder.append(paramIntent);
              this(stringBuilder.toString());
              throw securityException;
            } catch (RemoteException null) {}
          } catch (RemoteException remoteException) {}
          throw remoteException.rethrowFromSystemServer();
        } 
        throw new RuntimeException("Not supported in system context");
      } 
      throw new IllegalArgumentException("Handler and Executor both supplied");
    } 
    throw new IllegalArgumentException("connection is null");
  }
  
  public void updateServiceGroup(ServiceConnection paramServiceConnection, int paramInt1, int paramInt2) {
    if (paramServiceConnection != null) {
      LoadedApk loadedApk = this.mPackageInfo;
      if (loadedApk != null) {
        IServiceConnection iServiceConnection = loadedApk.lookupServiceDispatcher(paramServiceConnection, getOuterContext());
        if (iServiceConnection != null)
          try {
            ActivityManager.getService().updateServiceGroup(iServiceConnection, paramInt1, paramInt2);
            return;
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ServiceConnection not currently bound: ");
        stringBuilder.append(remoteException);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new RuntimeException("Not supported in system context");
    } 
    throw new IllegalArgumentException("connection is null");
  }
  
  public void unbindService(ServiceConnection paramServiceConnection) {
    if (paramServiceConnection != null) {
      LoadedApk loadedApk = this.mPackageInfo;
      if (loadedApk != null) {
        Context context = getOuterContext();
        IServiceConnection iServiceConnection = loadedApk.forgetServiceDispatcher(context, paramServiceConnection);
        try {
          ActivityManager.getService().unbindService(iServiceConnection);
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      throw new RuntimeException("Not supported in system context");
    } 
    throw new IllegalArgumentException("connection is null");
  }
  
  public boolean startInstrumentation(ComponentName paramComponentName, String paramString, Bundle paramBundle) {
    // Byte code:
    //   0: aload_3
    //   1: ifnull -> 10
    //   4: aload_3
    //   5: iconst_0
    //   6: invokevirtual setAllowFds : (Z)Z
    //   9: pop
    //   10: invokestatic getService : ()Landroid/app/IActivityManager;
    //   13: astore #4
    //   15: aload_0
    //   16: invokevirtual getUserId : ()I
    //   19: istore #5
    //   21: aload #4
    //   23: aload_1
    //   24: aload_2
    //   25: iconst_0
    //   26: aload_3
    //   27: aconst_null
    //   28: aconst_null
    //   29: iload #5
    //   31: aconst_null
    //   32: invokeinterface startInstrumentation : (Landroid/content/ComponentName;Ljava/lang/String;ILandroid/os/Bundle;Landroid/app/IInstrumentationWatcher;Landroid/app/IUiAutomationConnection;ILjava/lang/String;)Z
    //   37: istore #6
    //   39: iload #6
    //   41: ireturn
    //   42: astore_1
    //   43: aload_1
    //   44: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   47: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1945	-> 0
    //   #1946	-> 4
    //   #1948	-> 10
    //   #1949	-> 15
    //   #1948	-> 21
    //   #1951	-> 42
    //   #1952	-> 43
    // Exception table:
    //   from	to	target	type
    //   4	10	42	android/os/RemoteException
    //   10	15	42	android/os/RemoteException
    //   15	21	42	android/os/RemoteException
    //   21	39	42	android/os/RemoteException
  }
  
  public Object getSystemService(String paramString) {
    if (StrictMode.vmIncorrectContextUseEnabled()) {
      boolean bool;
      if (isUiContext() || isOuterUiContext()) {
        bool = true;
      } else {
        bool = false;
      } 
      if (isUiComponent(paramString) && !bool) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Tried to access visual service ");
        stringBuilder1.append(SystemServiceRegistry.getSystemServiceClassName(paramString));
        stringBuilder1.append(" from a non-visual Context:");
        stringBuilder1.append(getOuterContext());
        String str = stringBuilder1.toString();
        IllegalAccessException illegalAccessException = new IllegalAccessException(str);
        StrictMode.onIncorrectContextUsed("Visual services, such as WindowManager, WallpaperService or LayoutInflater should be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen.", illegalAccessException);
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(str);
        stringBuilder2.append(" ");
        stringBuilder2.append("Visual services, such as WindowManager, WallpaperService or LayoutInflater should be accessed from Activity or other visual Context. Use an Activity or a Context created with Context#createWindowContext(int, Bundle), which are adjusted to the configuration and visual bounds of an area on screen.");
        Log.e("ContextImpl", stringBuilder2.toString(), illegalAccessException);
      } 
    } 
    return SystemServiceRegistry.getSystemService(this, paramString);
  }
  
  private boolean isOuterUiContext() {
    boolean bool;
    if (getOuterContext() != null && getOuterContext().isUiContext()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getSystemServiceName(Class<?> paramClass) {
    return SystemServiceRegistry.getSystemServiceName(paramClass);
  }
  
  public boolean isUiContext() {
    return (this.mIsSystemOrSystemUiContext || this.mIsUiContext);
  }
  
  private static boolean isSystemOrSystemUI(Context paramContext) {
    if (!ActivityThread.isSystem()) {
      int i = Binder.getCallingPid();
      int j = Binder.getCallingUid();
      return (paramContext.checkPermission("android.permission.STATUS_BAR_SERVICE", i, j) == 0);
    } 
    return true;
  }
  
  private static boolean isUiComponent(String paramString) {
    return ("window".equals(paramString) || "layout_inflater".equals(paramString) || 
      "wallpaper".equals(paramString));
  }
  
  public int checkPermission(String paramString, int paramInt1, int paramInt2) {
    if (paramString != null)
      return PermissionManager.checkPermission(paramString, paramInt1, paramInt2); 
    throw new IllegalArgumentException("permission is null");
  }
  
  public int checkPermission(String paramString, int paramInt1, int paramInt2, IBinder paramIBinder) {
    if (paramString != null)
      try {
        return ActivityManager.getService().checkPermissionWithToken(paramString, paramInt1, paramInt2, paramIBinder);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("permission is null");
  }
  
  public int checkCallingPermission(String paramString) {
    if (paramString != null) {
      int i = Binder.getCallingPid();
      if (i != Process.myPid())
        return checkPermission(paramString, i, Binder.getCallingUid()); 
      return -1;
    } 
    throw new IllegalArgumentException("permission is null");
  }
  
  public int checkCallingOrSelfPermission(String paramString) {
    if (paramString != null) {
      int i = Binder.getCallingPid();
      int j = Binder.getCallingUid();
      return checkPermission(paramString, i, j);
    } 
    throw new IllegalArgumentException("permission is null");
  }
  
  public int checkSelfPermission(String paramString) {
    if (paramString != null)
      return checkPermission(paramString, Process.myPid(), Process.myUid()); 
    throw new IllegalArgumentException("permission is null");
  }
  
  private void enforce(String paramString1, int paramInt1, boolean paramBoolean, int paramInt2, String paramString2) {
    if (paramInt1 != 0) {
      String str;
      StringBuilder stringBuilder = new StringBuilder();
      if (paramString2 != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString2);
        stringBuilder1.append(": ");
        paramString2 = stringBuilder1.toString();
      } else {
        paramString2 = "";
      } 
      stringBuilder.append(paramString2);
      if (paramBoolean) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Neither user ");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" nor current process has ");
        str = stringBuilder1.toString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("uid ");
        stringBuilder1.append(paramInt2);
        stringBuilder1.append(" does not have ");
        str = stringBuilder1.toString();
      } 
      stringBuilder.append(str);
      stringBuilder.append(paramString1);
      stringBuilder.append(".");
      throw new SecurityException(stringBuilder.toString());
    } 
  }
  
  public void enforcePermission(String paramString1, int paramInt1, int paramInt2, String paramString2) {
    paramInt1 = checkPermission(paramString1, paramInt1, paramInt2);
    enforce(paramString1, paramInt1, false, paramInt2, paramString2);
  }
  
  public void enforceCallingPermission(String paramString1, String paramString2) {
    int i = checkCallingPermission(paramString1);
    int j = Binder.getCallingUid();
    enforce(paramString1, i, false, j, paramString2);
  }
  
  public void enforceCallingOrSelfPermission(String paramString1, String paramString2) {
    int i = checkCallingOrSelfPermission(paramString1);
    int j = Binder.getCallingUid();
    enforce(paramString1, i, true, j, paramString2);
  }
  
  public void grantUriPermission(String paramString, Uri paramUri, int paramInt) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iActivityManager.grantUriPermission(applicationThread, paramString, uri, paramInt, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void revokeUriPermission(Uri paramUri, int paramInt) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iActivityManager.revokeUriPermission(applicationThread, null, uri, paramInt, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void revokeUriPermission(String paramString, Uri paramUri, int paramInt) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      ActivityThread activityThread = this.mMainThread;
      ActivityThread.ApplicationThread applicationThread = activityThread.getApplicationThread();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iActivityManager.revokeUriPermission(applicationThread, paramString, uri, paramInt, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      return iActivityManager.checkUriPermission(uri, paramInt1, paramInt2, paramInt3, i, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, IBinder paramIBinder) {
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      return iActivityManager.checkUriPermission(uri, paramInt1, paramInt2, paramInt3, i, paramIBinder);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private int resolveUserId(Uri paramUri) {
    return ContentProvider.getUserIdFromUri(paramUri, getUserId());
  }
  
  public int checkCallingUriPermission(Uri paramUri, int paramInt) {
    int i = Binder.getCallingPid();
    if (i != Process.myPid()) {
      int j = Binder.getCallingUid();
      return checkUriPermission(paramUri, i, j, paramInt);
    } 
    return -1;
  }
  
  public int checkCallingOrSelfUriPermission(Uri paramUri, int paramInt) {
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    return checkUriPermission(paramUri, i, j, paramInt);
  }
  
  public int checkUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3) {
    if ((paramInt3 & 0x1) != 0 && (
      paramString1 == null || 
      checkPermission(paramString1, paramInt1, paramInt2) == 0))
      return 0; 
    if ((paramInt3 & 0x2) != 0 && (
      paramString2 == null || 
      checkPermission(paramString2, paramInt1, paramInt2) == 0))
      return 0; 
    if (paramUri != null) {
      paramInt1 = checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3);
    } else {
      paramInt1 = -1;
    } 
    return paramInt1;
  }
  
  private String uriModeFlagToString(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    if ((paramInt & 0x1) != 0)
      stringBuilder.append("read and "); 
    if ((paramInt & 0x2) != 0)
      stringBuilder.append("write and "); 
    if ((paramInt & 0x40) != 0)
      stringBuilder.append("persistable and "); 
    if ((paramInt & 0x80) != 0)
      stringBuilder.append("prefix and "); 
    if (stringBuilder.length() > 5) {
      stringBuilder.setLength(stringBuilder.length() - 5);
      return stringBuilder.toString();
    } 
    stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown permission mode flags: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void enforceForUri(int paramInt1, int paramInt2, boolean paramBoolean, int paramInt3, Uri paramUri, String paramString) {
    if (paramInt2 != 0) {
      String str;
      StringBuilder stringBuilder = new StringBuilder();
      if (paramString != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString);
        stringBuilder1.append(": ");
        paramString = stringBuilder1.toString();
      } else {
        paramString = "";
      } 
      stringBuilder.append(paramString);
      if (paramBoolean) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Neither user ");
        stringBuilder1.append(paramInt3);
        stringBuilder1.append(" nor current process has ");
        str = stringBuilder1.toString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("User ");
        stringBuilder1.append(paramInt3);
        stringBuilder1.append(" does not have ");
        str = stringBuilder1.toString();
      } 
      stringBuilder.append(str);
      stringBuilder.append(uriModeFlagToString(paramInt1));
      stringBuilder.append(" permission on ");
      stringBuilder.append(paramUri);
      stringBuilder.append(".");
      throw new SecurityException(stringBuilder.toString());
    } 
  }
  
  public void enforceUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, String paramString) {
    paramInt1 = checkUriPermission(paramUri, paramInt1, paramInt2, paramInt3);
    enforceForUri(paramInt3, paramInt1, false, paramInt2, paramUri, paramString);
  }
  
  public void enforceCallingUriPermission(Uri paramUri, int paramInt, String paramString) {
    int i = checkCallingUriPermission(paramUri, paramInt);
    int j = Binder.getCallingUid();
    enforceForUri(paramInt, i, false, j, paramUri, paramString);
  }
  
  public void enforceCallingOrSelfUriPermission(Uri paramUri, int paramInt, String paramString) {
    int i = checkCallingOrSelfUriPermission(paramUri, paramInt);
    int j = Binder.getCallingUid();
    enforceForUri(paramInt, i, true, j, paramUri, paramString);
  }
  
  public void enforceUriPermission(Uri paramUri, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3) {
    paramInt1 = checkUriPermission(paramUri, paramString1, paramString2, paramInt1, paramInt2, paramInt3);
    enforceForUri(paramInt3, paramInt1, false, paramInt2, paramUri, paramString3);
  }
  
  private void warnIfCallingFromSystemProcess() {
    if (Process.myUid() == 1000) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Calling a method in the system process without a qualified user: ");
      stringBuilder.append(Debug.getCallers(5));
      String str = stringBuilder.toString();
      Slog.w("ContextImpl", str);
    } 
  }
  
  private static Resources createResources(IBinder paramIBinder, LoadedApk paramLoadedApk, String paramString, int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, List<ResourcesLoader> paramList) {
    try {
      String[] arrayOfString2 = paramLoadedApk.getSplitPaths(paramString);
      ClassLoader classLoader = paramLoadedApk.getSplitClassLoader(paramString);
      ResourcesManager resourcesManager = ResourcesManager.getInstance();
      String str = paramLoadedApk.getResDir();
      String[] arrayOfString3 = paramLoadedApk.getOverlayDirs();
      String[] arrayOfString1 = (paramLoadedApk.getApplicationInfo()).sharedLibraryFiles;
      Resources resources = resourcesManager.getResources(paramIBinder, str, arrayOfString2, arrayOfString3, arrayOfString1, paramInt, paramConfiguration, paramCompatibilityInfo, classLoader, paramList);
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(resources, paramLoadedApk.mPackageName);
      return resources;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      throw new RuntimeException(nameNotFoundException);
    } 
  }
  
  public Context createApplicationContext(ApplicationInfo paramApplicationInfo, int paramInt) throws PackageManager.NameNotFoundException {
    LoadedApk loadedApk = this.mMainThread.getPackageInfo(paramApplicationInfo, this.mResources.getCompatibilityInfo(), paramInt | 0x40000000);
    if (loadedApk != null) {
      ActivityThread activityThread = this.mMainThread;
      IBinder iBinder1 = this.mToken;
      int i = paramApplicationInfo.uid;
      ContextImpl contextImpl = new ContextImpl(this, activityThread, loadedApk, null, null, iBinder1, new UserHandle(UserHandle.getUserId(i)), paramInt, null, null);
      paramInt = getDisplayId();
      IBinder iBinder2 = this.mToken;
      CompatibilityInfo compatibilityInfo = getDisplayAdjustments(paramInt).getCompatibilityInfo();
      contextImpl.setResources(createResources(iBinder2, loadedApk, null, paramInt, null, compatibilityInfo, null));
      if (contextImpl.mResources != null)
        return contextImpl; 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Application package ");
    stringBuilder.append(paramApplicationInfo.packageName);
    stringBuilder.append(" not found");
    throw new PackageManager.NameNotFoundException(stringBuilder.toString());
  }
  
  public Context createPackageContext(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return createPackageContextAsUser(paramString, paramInt, this.mUser);
  }
  
  public Context createPackageContextAsUser(String paramString, int paramInt, UserHandle paramUserHandle) throws PackageManager.NameNotFoundException {
    if (paramString.equals("system") || paramString.equals("android"))
      return new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, null, this.mToken, paramUserHandle, paramInt, null, null); 
    ActivityThread activityThread = this.mMainThread;
    CompatibilityInfo compatibilityInfo = this.mResources.getCompatibilityInfo();
    int i = paramUserHandle.getIdentifier();
    LoadedApk loadedApk = activityThread.getPackageInfo(paramString, compatibilityInfo, paramInt | 0x40000000, i);
    if (loadedApk != null) {
      ContextImpl contextImpl = new ContextImpl(this, this.mMainThread, loadedApk, this.mAttributionTag, null, this.mToken, paramUserHandle, paramInt, null, null);
      paramInt = getDisplayId();
      IBinder iBinder = this.mToken;
      CompatibilityInfo compatibilityInfo1 = getDisplayAdjustments(paramInt).getCompatibilityInfo();
      contextImpl.setResources(createResources(iBinder, loadedApk, null, paramInt, null, compatibilityInfo1, null));
      if (contextImpl.mResources != null)
        return contextImpl; 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Application package ");
    stringBuilder.append(paramString);
    stringBuilder.append(" not found");
    throw new PackageManager.NameNotFoundException(stringBuilder.toString());
  }
  
  public Context createContextAsUser(UserHandle paramUserHandle, int paramInt) {
    try {
      return createPackageContextAsUser(getPackageName(), paramInt, paramUserHandle);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Own package not found: package=");
      stringBuilder.append(getPackageName());
      throw new IllegalStateException(stringBuilder.toString());
    } 
  }
  
  public Context createContextForSplit(String paramString) throws PackageManager.NameNotFoundException {
    if (!this.mPackageInfo.getApplicationInfo().requestsIsolatedSplitLoading())
      return this; 
    ClassLoader classLoader = this.mPackageInfo.getSplitClassLoader(paramString);
    String[] arrayOfString1 = this.mPackageInfo.getSplitPaths(paramString);
    ContextImpl contextImpl = new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, paramString, this.mToken, this.mUser, this.mFlags, classLoader, null);
    int i = getDisplayId();
    ResourcesManager resourcesManager = ResourcesManager.getInstance();
    IBinder iBinder = this.mToken;
    LoadedApk loadedApk1 = this.mPackageInfo;
    String str = loadedApk1.getResDir();
    LoadedApk loadedApk2 = this.mPackageInfo;
    String[] arrayOfString2 = loadedApk2.getOverlayDirs();
    LoadedApk loadedApk3 = this.mPackageInfo;
    String[] arrayOfString3 = (loadedApk3.getApplicationInfo()).sharedLibraryFiles;
    LoadedApk loadedApk4 = this.mPackageInfo;
    CompatibilityInfo compatibilityInfo = loadedApk4.getCompatibilityInfo();
    Resources resources2 = this.mResources;
    List<ResourcesLoader> list = resources2.getLoaders();
    Resources resources1 = resourcesManager.getResources(iBinder, str, arrayOfString1, arrayOfString2, arrayOfString3, i, null, compatibilityInfo, classLoader, list);
    ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(resources1, this.mPackageInfo.mPackageName);
    contextImpl.setResources(resources1);
    return contextImpl;
  }
  
  public Context createConfigurationContext(Configuration paramConfiguration) {
    if (paramConfiguration != null) {
      ContextImpl contextImpl = new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, this.mSplitName, this.mToken, this.mUser, this.mFlags, this.mClassLoader, null);
      int i = getDisplayId();
      IBinder iBinder = this.mToken;
      LoadedApk loadedApk = this.mPackageInfo;
      String str = this.mSplitName;
      CompatibilityInfo compatibilityInfo = getDisplayAdjustments(i).getCompatibilityInfo();
      Resources resources = this.mResources;
      List<ResourcesLoader> list = resources.getLoaders();
      contextImpl.setResources(createResources(iBinder, loadedApk, str, i, paramConfiguration, compatibilityInfo, list));
      if (isUiContext() || isOuterUiContext()) {
        boolean bool1 = true;
        return contextImpl;
      } 
      boolean bool = false;
      return contextImpl;
    } 
    throw new IllegalArgumentException("overrideConfiguration must not be null");
  }
  
  public Context createDisplayContext(Display paramDisplay) {
    if (paramDisplay != null) {
      ContextImpl contextImpl = new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, this.mSplitName, this.mToken, this.mUser, this.mFlags, this.mClassLoader, null);
      int i = paramDisplay.getDisplayId();
      IBinder iBinder = this.mToken;
      LoadedApk loadedApk = this.mPackageInfo;
      String str = this.mSplitName;
      CompatibilityInfo compatibilityInfo = getDisplayAdjustments(i).getCompatibilityInfo();
      Resources resources = this.mResources;
      List<ResourcesLoader> list = resources.getLoaders();
      contextImpl.setResources(createResources(iBinder, loadedApk, str, i, null, compatibilityInfo, list));
      contextImpl.mDisplay = paramDisplay;
      contextImpl.mIsAssociatedWithDisplay = true;
      return contextImpl;
    } 
    throw new IllegalArgumentException("display must not be null");
  }
  
  public WindowContext createWindowContext(int paramInt, Bundle paramBundle) {
    if (getDisplay() != null)
      return new WindowContext(this, paramInt, paramBundle); 
    throw new UnsupportedOperationException("WindowContext can only be created from other visual contexts, such as Activity or one created with Context#createDisplayContext(Display)");
  }
  
  ContextImpl createBaseWindowContext(IBinder paramIBinder) {
    ContextImpl contextImpl = new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, this.mSplitName, paramIBinder, this.mUser, this.mFlags, this.mClassLoader, null);
    contextImpl.mIsUiContext = true;
    contextImpl.mIsAssociatedWithDisplay = true;
    return contextImpl;
  }
  
  Resources createWindowContextResources() {
    CompatibilityInfo compatibilityInfo;
    String str = this.mPackageInfo.getResDir();
    String[] arrayOfString1 = this.mPackageInfo.getSplitResDirs();
    String[] arrayOfString2 = this.mPackageInfo.getOverlayDirs();
    String[] arrayOfString3 = (this.mPackageInfo.getApplicationInfo()).sharedLibraryFiles;
    int i = getDisplayId();
    if (i == 0) {
      compatibilityInfo = this.mPackageInfo.getCompatibilityInfo();
    } else {
      compatibilityInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
    } 
    List<ResourcesLoader> list = this.mResources.getLoaders();
    return this.mResourcesManager.createBaseTokenResources(this.mToken, str, arrayOfString1, arrayOfString2, arrayOfString3, i, null, compatibilityInfo, this.mClassLoader, list);
  }
  
  public Context createAttributionContext(String paramString) {
    return new ContextImpl(this, this.mMainThread, this.mPackageInfo, paramString, this.mSplitName, this.mToken, this.mUser, this.mFlags, this.mClassLoader, null);
  }
  
  public Context createDeviceProtectedStorageContext() {
    int i = this.mFlags;
    return new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, this.mSplitName, this.mToken, this.mUser, i & 0xFFFFFFEF | 0x8, this.mClassLoader, null);
  }
  
  public Context createCredentialProtectedStorageContext() {
    int i = this.mFlags;
    return new ContextImpl(this, this.mMainThread, this.mPackageInfo, this.mAttributionTag, this.mSplitName, this.mToken, this.mUser, i & 0xFFFFFFF7 | 0x10, this.mClassLoader, null);
  }
  
  public boolean isRestricted() {
    boolean bool;
    if ((this.mFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isDeviceProtectedStorage() {
    boolean bool;
    if ((this.mFlags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isCredentialProtectedStorage() {
    boolean bool;
    if ((this.mFlags & 0x10) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean canLoadUnsafeResources() {
    boolean bool = getPackageName().equals(getOpPackageName());
    boolean bool1 = true;
    if (bool)
      return true; 
    if ((this.mFlags & 0x2) == 0)
      bool1 = false; 
    return bool1;
  }
  
  public Display getDisplay() {
    if (this.mIsSystemOrSystemUiContext || this.mIsAssociatedWithDisplay)
      return getDisplayNoVerify(); 
    throw new UnsupportedOperationException("Tried to obtain display from a Context not associated with  one. Only visual Contexts (such as Activity or one created with Context#createWindowContext) or ones created with Context#createDisplayContext are associated with displays. Other types of Contexts are typically related to background entities and may return an arbitrary display.");
  }
  
  public Display getDisplayNoVerify() {
    Display display = this.mDisplay;
    if (display == null)
      return this.mResourcesManager.getAdjustedDisplay(0, this.mResources); 
    return display;
  }
  
  public int getDisplayId() {
    boolean bool;
    Display display = getDisplayNoVerify();
    if (display != null) {
      bool = display.getDisplayId();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void updateDisplay(int paramInt) {
    this.mDisplay = this.mResourcesManager.getAdjustedDisplay(paramInt, this.mResources);
    this.mIsAssociatedWithDisplay = true;
  }
  
  public DisplayAdjustments getDisplayAdjustments(int paramInt) {
    return this.mResources.getDisplayAdjustments();
  }
  
  public File getDataDir() {
    if (this.mPackageInfo != null) {
      File file;
      if (isCredentialProtectedStorage()) {
        file = this.mPackageInfo.getCredentialProtectedDataDirFile();
      } else if (isDeviceProtectedStorage()) {
        file = this.mPackageInfo.getDeviceProtectedDataDirFile();
      } else {
        file = this.mPackageInfo.getDataDirFile();
      } 
      if (file != null) {
        if (!file.exists() && Process.myUid() == 1000) {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Data directory doesn't exist for package ");
          stringBuilder2.append(getPackageName());
          Log.wtf("ContextImpl", stringBuilder2.toString(), new Throwable());
        } 
        return file;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("No data directory found for package ");
      stringBuilder1.append(getPackageName());
      throw new RuntimeException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No package details found for package ");
    stringBuilder.append(getPackageName());
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public File getDir(String paramString, int paramInt) {
    checkMode(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("app_");
    stringBuilder.append(paramString);
    paramString = stringBuilder.toString();
    File file = makeFilename(getDataDir(), paramString);
    if (!file.exists()) {
      file.mkdir();
      setFilePermissionsFromMode(file.getPath(), paramInt, 505);
    } 
    return file;
  }
  
  public UserHandle getUser() {
    return this.mUser;
  }
  
  public int getUserId() {
    return this.mUser.getIdentifier();
  }
  
  public AutofillManager.AutofillClient getAutofillClient() {
    return this.mAutofillClient;
  }
  
  public void setAutofillClient(AutofillManager.AutofillClient paramAutofillClient) {
    this.mAutofillClient = paramAutofillClient;
  }
  
  public AutofillOptions getAutofillOptions() {
    return this.mAutofillOptions;
  }
  
  public void setAutofillOptions(AutofillOptions paramAutofillOptions) {
    this.mAutofillOptions = paramAutofillOptions;
  }
  
  public ContentCaptureOptions getContentCaptureOptions() {
    return this.mContentCaptureOptions;
  }
  
  public void setContentCaptureOptions(ContentCaptureOptions paramContentCaptureOptions) {
    this.mContentCaptureOptions = paramContentCaptureOptions;
  }
  
  static ContextImpl createSystemContext(ActivityThread paramActivityThread) {
    LoadedApk loadedApk = new LoadedApk(paramActivityThread);
    ContextImpl contextImpl = new ContextImpl(null, paramActivityThread, loadedApk, null, null, null, null, 0, null, null);
    contextImpl.setResources(loadedApk.getResources());
    Resources resources = contextImpl.mResources;
    Configuration configuration = contextImpl.mResourcesManager.getConfiguration();
    ResourcesManager resourcesManager = contextImpl.mResourcesManager;
    DisplayMetrics displayMetrics = resourcesManager.getDisplayMetrics();
    resources.updateConfiguration(configuration, displayMetrics);
    contextImpl.mIsSystemOrSystemUiContext = true;
    return contextImpl;
  }
  
  static ContextImpl createSystemUiContext(ContextImpl paramContextImpl, int paramInt) {
    LoadedApk loadedApk = paramContextImpl.mPackageInfo;
    ContextImpl contextImpl = new ContextImpl(null, paramContextImpl.mMainThread, loadedApk, null, null, null, null, 0, null, null);
    CompatibilityInfo compatibilityInfo = loadedApk.getCompatibilityInfo();
    contextImpl.setResources(createResources(null, loadedApk, null, paramInt, null, compatibilityInfo, null));
    contextImpl.updateDisplay(paramInt);
    contextImpl.mIsSystemOrSystemUiContext = true;
    return contextImpl;
  }
  
  static ContextImpl createSystemUiContext(ContextImpl paramContextImpl) {
    return createSystemUiContext(paramContextImpl, 0);
  }
  
  static ContextImpl createAppContext(ActivityThread paramActivityThread, LoadedApk paramLoadedApk) {
    return createAppContext(paramActivityThread, paramLoadedApk, null);
  }
  
  static ContextImpl createAppContext(ActivityThread paramActivityThread, LoadedApk paramLoadedApk, String paramString) {
    if (paramLoadedApk != null) {
      ContextImpl contextImpl = new ContextImpl(null, paramActivityThread, paramLoadedApk, null, null, null, null, 0, null, paramString);
      contextImpl.setResources(paramLoadedApk.getResources());
      contextImpl.mIsSystemOrSystemUiContext = isSystemOrSystemUI(contextImpl);
      return contextImpl;
    } 
    throw new IllegalArgumentException("packageInfo");
  }
  
  static ContextImpl createActivityContext(ActivityThread paramActivityThread, LoadedApk paramLoadedApk, ActivityInfo paramActivityInfo, IBinder paramIBinder, int paramInt, Configuration paramConfiguration) {
    if (paramLoadedApk != null) {
      CompatibilityInfo compatibilityInfo;
      RuntimeException runtimeException;
      List<ResourcesLoader> list;
      String[] arrayOfString1 = paramLoadedApk.getSplitResDirs();
      ClassLoader classLoader = paramLoadedApk.getClassLoader();
      if (paramLoadedApk.getApplicationInfo().requestsIsolatedSplitLoading()) {
        Trace.traceBegin(8192L, "SplitDependencies");
        try {
          classLoader = paramLoadedApk.getSplitClassLoader(paramActivityInfo.splitName);
          arrayOfString1 = paramLoadedApk.getSplitPaths(paramActivityInfo.splitName);
          Trace.traceEnd(8192L);
        } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
          runtimeException = new RuntimeException();
          this((Throwable)nameNotFoundException);
          throw runtimeException;
        } finally {}
      } 
      ContextImpl contextImpl = new ContextImpl(null, paramActivityThread, (LoadedApk)runtimeException, null, paramActivityInfo.splitName, paramIBinder, null, 0, classLoader, null);
      contextImpl.mIsUiContext = true;
      contextImpl.mIsAssociatedWithDisplay = true;
      contextImpl.mIsSystemOrSystemUiContext = isSystemOrSystemUI(contextImpl);
      if (paramInt == -1)
        paramInt = 0; 
      if (paramInt == 0) {
        compatibilityInfo = runtimeException.getCompatibilityInfo();
      } else {
        compatibilityInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
      } 
      ResourcesManager resourcesManager = ResourcesManager.getInstance();
      String str = runtimeException.getResDir();
      String[] arrayOfString2 = runtimeException.getOverlayDirs();
      String[] arrayOfString3 = (runtimeException.getApplicationInfo()).sharedLibraryFiles;
      if (runtimeException.getApplication() == null) {
        paramActivityInfo = null;
      } else {
        list = runtimeException.getApplication().getResources().getLoaders();
      } 
      Resources resources = resourcesManager.createBaseTokenResources(paramIBinder, str, arrayOfString1, arrayOfString2, arrayOfString3, paramInt, paramConfiguration, compatibilityInfo, classLoader, list);
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(resources, ((LoadedApk)runtimeException).mPackageName);
      contextImpl.setResources(resources);
      resources = contextImpl.getResources();
      contextImpl.mDisplay = resourcesManager.getAdjustedDisplay(paramInt, resources);
      return contextImpl;
    } 
    throw new IllegalArgumentException("packageInfo");
  }
  
  private ContextImpl(ContextImpl paramContextImpl, ActivityThread paramActivityThread, LoadedApk paramLoadedApk, String paramString1, String paramString2, IBinder paramIBinder, UserHandle paramUserHandle, int paramInt, ClassLoader paramClassLoader, String paramString3) {
    String str1, str2;
    this.mThemeResource = 0;
    this.mTheme = null;
    this.mReceiverRestrictedContext = null;
    this.mSplitName = null;
    this.mAutofillClient = null;
    this.mContentCaptureOptions = null;
    this.mSync = new Object();
    Object[] arrayOfObject = SystemServiceRegistry.createServiceCache();
    this.mServiceInitializationStateArray = new int[arrayOfObject.length];
    this.mOuterContext = this;
    int i = paramInt;
    if ((paramInt & 0x18) == 0) {
      File file = paramLoadedApk.getDataDirFile();
      if (Objects.equals(file, paramLoadedApk.getCredentialProtectedDataDirFile())) {
        i = paramInt | 0x10;
      } else {
        i = paramInt;
        if (Objects.equals(file, paramLoadedApk.getDeviceProtectedDataDirFile()))
          i = paramInt | 0x8; 
      } 
    } 
    this.mMainThread = paramActivityThread;
    this.mToken = paramIBinder;
    this.mFlags = i;
    UserHandle userHandle = paramUserHandle;
    if (paramUserHandle == null)
      userHandle = Process.myUserHandle(); 
    this.mUser = userHandle;
    this.mPackageInfo = paramLoadedApk;
    this.mSplitName = paramString2;
    this.mClassLoader = paramClassLoader;
    this.mResourcesManager = ResourcesManager.getInstance();
    if (paramContextImpl != null) {
      this.mBasePackageName = paramContextImpl.mBasePackageName;
      str2 = paramContextImpl.mOpPackageName;
      setResources(paramContextImpl.mResources);
      this.mDisplay = paramContextImpl.mDisplay;
      this.mIsAssociatedWithDisplay = paramContextImpl.mIsAssociatedWithDisplay;
      this.mIsSystemOrSystemUiContext = paramContextImpl.mIsSystemOrSystemUiContext;
      str1 = str2;
    } else {
      this.mBasePackageName = ((LoadedApk)str2).mPackageName;
      ApplicationInfo applicationInfo = str2.getApplicationInfo();
      if (applicationInfo.uid == 1000 && applicationInfo.uid != Process.myUid()) {
        str1 = ActivityThread.currentPackageName();
      } else {
        str1 = this.mBasePackageName;
      } 
    } 
    if (paramString3 != null)
      str1 = paramString3; 
    this.mOpPackageName = str1;
    this.mAttributionTag = paramString1;
    this.mContentResolver = new ApplicationContentResolver(this, paramActivityThread);
  }
  
  void setResources(Resources paramResources) {
    if (paramResources instanceof CompatResources)
      ((CompatResources)paramResources).setContext(this); 
    this.mResources = paramResources;
  }
  
  void installSystemApplicationInfo(ApplicationInfo paramApplicationInfo, ClassLoader paramClassLoader) {
    this.mPackageInfo.installSystemApplicationInfo(paramApplicationInfo, paramClassLoader);
  }
  
  final void scheduleFinalCleanup(String paramString1, String paramString2) {
    this.mMainThread.scheduleContextCleanup(this, paramString1, paramString2);
  }
  
  final void performFinalCleanup(String paramString1, String paramString2) {
    this.mPackageInfo.removeContextRegistrations(getOuterContext(), paramString1, paramString2);
  }
  
  final Context getReceiverRestrictedContext() {
    Context context = this.mReceiverRestrictedContext;
    if (context != null)
      return context; 
    this.mReceiverRestrictedContext = context = new ReceiverRestrictedContext(getOuterContext());
    return context;
  }
  
  final void setOuterContext(Context paramContext) {
    this.mOuterContext = paramContext;
  }
  
  final Context getOuterContext() {
    return this.mOuterContext;
  }
  
  public IBinder getActivityToken() {
    return this.mToken;
  }
  
  private void checkMode(int paramInt) {
    if ((getApplicationInfo()).targetSdkVersion >= 24)
      if ((paramInt & 0x1) == 0) {
        if ((paramInt & 0x2) != 0)
          throw new SecurityException("MODE_WORLD_WRITEABLE no longer supported"); 
      } else {
        throw new SecurityException("MODE_WORLD_READABLE no longer supported");
      }  
  }
  
  static void setFilePermissionsFromMode(String paramString, int paramInt1, int paramInt2) {
    int i = paramInt2 | 0x1B0;
    paramInt2 = i;
    if ((paramInt1 & 0x1) != 0)
      paramInt2 = i | 0x4; 
    i = paramInt2;
    if ((paramInt1 & 0x2) != 0)
      i = paramInt2 | 0x2; 
    FileUtils.setPermissions(paramString, i, -1, -1);
  }
  
  private File makeFilename(File paramFile, String paramString) {
    if (paramString.indexOf(File.separatorChar) < 0) {
      paramFile = new File(paramFile, paramString);
      BlockGuard.getVmPolicy().onPathAccess(paramFile.getPath());
      return paramFile;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("File ");
    stringBuilder.append(paramString);
    stringBuilder.append(" contains a path separator");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private File[] ensureExternalDirsExistOrFilter(File[] paramArrayOfFile, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: ldc_w android/os/storage/StorageManager
    //   4: invokevirtual getSystemService : (Ljava/lang/Class;)Ljava/lang/Object;
    //   7: checkcast android/os/storage/StorageManager
    //   10: astore_3
    //   11: aload_1
    //   12: arraylength
    //   13: anewarray java/io/File
    //   16: astore #4
    //   18: iconst_0
    //   19: istore #5
    //   21: iload #5
    //   23: aload_1
    //   24: arraylength
    //   25: if_icmpge -> 151
    //   28: aload_1
    //   29: iload #5
    //   31: aaload
    //   32: astore #6
    //   34: aload #6
    //   36: astore #7
    //   38: aload #6
    //   40: invokevirtual exists : ()Z
    //   43: ifne -> 138
    //   46: iload_2
    //   47: ifeq -> 58
    //   50: aload #6
    //   52: invokevirtual mkdirs : ()Z
    //   55: ifne -> 72
    //   58: aload #6
    //   60: invokevirtual exists : ()Z
    //   63: ifne -> 72
    //   66: aload_3
    //   67: aload #6
    //   69: invokevirtual mkdirs : (Ljava/io/File;)V
    //   72: aload #6
    //   74: astore #7
    //   76: goto -> 138
    //   79: astore #7
    //   81: new java/lang/StringBuilder
    //   84: dup
    //   85: invokespecial <init> : ()V
    //   88: astore #8
    //   90: aload #8
    //   92: ldc_w 'Failed to ensure '
    //   95: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   98: pop
    //   99: aload #8
    //   101: aload #6
    //   103: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   106: pop
    //   107: aload #8
    //   109: ldc_w ': '
    //   112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: aload #8
    //   118: aload #7
    //   120: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: ldc 'ContextImpl'
    //   126: aload #8
    //   128: invokevirtual toString : ()Ljava/lang/String;
    //   131: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   134: pop
    //   135: aconst_null
    //   136: astore #7
    //   138: aload #4
    //   140: iload #5
    //   142: aload #7
    //   144: aastore
    //   145: iinc #5, 1
    //   148: goto -> 21
    //   151: aload #4
    //   153: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2979	-> 0
    //   #2980	-> 11
    //   #2981	-> 18
    //   #2982	-> 28
    //   #2983	-> 34
    //   #2985	-> 46
    //   #2987	-> 58
    //   #2990	-> 66
    //   #2996	-> 72
    //   #2993	-> 79
    //   #2994	-> 81
    //   #2995	-> 135
    //   #2998	-> 138
    //   #2981	-> 145
    //   #3000	-> 151
    // Exception table:
    //   from	to	target	type
    //   50	58	79	java/lang/Exception
    //   58	66	79	java/lang/Exception
    //   66	72	79	java/lang/Exception
  }
  
  class ApplicationContentResolver extends ContentResolver {
    private final ActivityThread mMainThread;
    
    public ApplicationContentResolver(ContextImpl this$0, ActivityThread param1ActivityThread) {
      super(this$0);
      Objects.requireNonNull(param1ActivityThread);
      this.mMainThread = param1ActivityThread;
    }
    
    protected IContentProvider acquireProvider(Context param1Context, String param1String) {
      ActivityThread activityThread = this.mMainThread;
      String str = ContentProvider.getAuthorityWithoutUserId(param1String);
      int i = resolveUserIdFromAuthority(param1String);
      return activityThread.acquireProvider(param1Context, str, i, true);
    }
    
    protected IContentProvider acquireExistingProvider(Context param1Context, String param1String) {
      ActivityThread activityThread = this.mMainThread;
      String str = ContentProvider.getAuthorityWithoutUserId(param1String);
      int i = resolveUserIdFromAuthority(param1String);
      return activityThread.acquireExistingProvider(param1Context, str, i, true);
    }
    
    public boolean releaseProvider(IContentProvider param1IContentProvider) {
      return this.mMainThread.releaseProvider(param1IContentProvider, true);
    }
    
    protected IContentProvider acquireUnstableProvider(Context param1Context, String param1String) {
      ActivityThread activityThread = this.mMainThread;
      String str = ContentProvider.getAuthorityWithoutUserId(param1String);
      int i = resolveUserIdFromAuthority(param1String);
      return activityThread.acquireProvider(param1Context, str, i, false);
    }
    
    public boolean releaseUnstableProvider(IContentProvider param1IContentProvider) {
      return this.mMainThread.releaseProvider(param1IContentProvider, false);
    }
    
    public void unstableProviderDied(IContentProvider param1IContentProvider) {
      this.mMainThread.handleUnstableProviderDied(param1IContentProvider.asBinder(), true);
    }
    
    public void appNotRespondingViaProvider(IContentProvider param1IContentProvider) {
      this.mMainThread.appNotRespondingViaProvider(param1IContentProvider.asBinder());
    }
    
    protected int resolveUserIdFromAuthority(String param1String) {
      return ContentProvider.getUserIdFromAuthority(param1String, getUserId());
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ServiceInitializationState implements Annotation {}
}
