package android.app;

import android.animation.LayoutTransition;
import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.internal.R;

@Deprecated
public class FragmentBreadCrumbs extends ViewGroup implements FragmentManager.OnBackStackChangedListener {
  private static final int DEFAULT_GRAVITY = 8388627;
  
  Activity mActivity;
  
  LinearLayout mContainer;
  
  private int mGravity;
  
  LayoutInflater mInflater;
  
  private int mLayoutResId;
  
  int mMaxVisible = -1;
  
  private OnBreadCrumbClickListener mOnBreadCrumbClickListener;
  
  private View.OnClickListener mOnClickListener;
  
  private View.OnClickListener mParentClickListener;
  
  BackStackRecord mParentEntry;
  
  private int mTextColor;
  
  BackStackRecord mTopEntry;
  
  public FragmentBreadCrumbs(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public FragmentBreadCrumbs(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 17956935);
  }
  
  public FragmentBreadCrumbs(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public FragmentBreadCrumbs(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mOnClickListener = (View.OnClickListener)new Object(this);
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.FragmentBreadCrumbs, paramInt1, paramInt2);
    this.mGravity = typedArray.getInt(0, 8388627);
    this.mLayoutResId = typedArray.getResourceId(2, 17367163);
    this.mTextColor = typedArray.getColor(1, 0);
    typedArray.recycle();
  }
  
  public void setActivity(Activity paramActivity) {
    this.mActivity = paramActivity;
    LayoutInflater layoutInflater = (LayoutInflater)paramActivity.getSystemService("layout_inflater");
    LinearLayout linearLayout = (LinearLayout)layoutInflater.inflate(17367165, this, false);
    addView((View)linearLayout);
    paramActivity.getFragmentManager().addOnBackStackChangedListener(this);
    updateCrumbs();
    setLayoutTransition(new LayoutTransition());
  }
  
  public void setMaxVisible(int paramInt) {
    if (paramInt >= 1) {
      this.mMaxVisible = paramInt;
      return;
    } 
    throw new IllegalArgumentException("visibleCrumbs must be greater than zero");
  }
  
  public void setParentTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2, View.OnClickListener paramOnClickListener) {
    this.mParentEntry = createBackStackEntry(paramCharSequence1, paramCharSequence2);
    this.mParentClickListener = paramOnClickListener;
    updateCrumbs();
  }
  
  public void setOnBreadCrumbClickListener(OnBreadCrumbClickListener paramOnBreadCrumbClickListener) {
    this.mOnBreadCrumbClickListener = paramOnBreadCrumbClickListener;
  }
  
  private BackStackRecord createBackStackEntry(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    if (paramCharSequence1 == null)
      return null; 
    Activity activity = this.mActivity;
    BackStackRecord backStackRecord = new BackStackRecord((FragmentManagerImpl)activity.getFragmentManager());
    backStackRecord.setBreadCrumbTitle(paramCharSequence1);
    backStackRecord.setBreadCrumbShortTitle(paramCharSequence2);
    return backStackRecord;
  }
  
  public void setTitle(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    this.mTopEntry = createBackStackEntry(paramCharSequence1, paramCharSequence2);
    updateCrumbs();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    paramInt1 = getChildCount();
    if (paramInt1 == 0)
      return; 
    View view = getChildAt(0);
    int i = this.mPaddingTop;
    paramInt4 = this.mPaddingTop;
    int j = view.getMeasuredHeight(), k = this.mPaddingBottom;
    paramInt2 = getLayoutDirection();
    paramInt1 = this.mGravity;
    paramInt1 = Gravity.getAbsoluteGravity(paramInt1 & 0x800007, paramInt2);
    if (paramInt1 != 1) {
      if (paramInt1 != 5) {
        paramInt2 = this.mPaddingLeft;
        paramInt1 = view.getMeasuredWidth() + paramInt2;
      } else {
        paramInt1 = this.mRight - this.mLeft - this.mPaddingRight;
        paramInt2 = paramInt1 - view.getMeasuredWidth();
      } 
    } else {
      paramInt2 = this.mPaddingLeft + (this.mRight - this.mLeft - view.getMeasuredWidth()) / 2;
      paramInt1 = view.getMeasuredWidth() + paramInt2;
    } 
    paramInt3 = paramInt2;
    if (paramInt2 < this.mPaddingLeft)
      paramInt3 = this.mPaddingLeft; 
    paramInt2 = paramInt1;
    if (paramInt1 > this.mRight - this.mLeft - this.mPaddingRight)
      paramInt2 = this.mRight - this.mLeft - this.mPaddingRight; 
    view.layout(paramInt3, i, paramInt2, paramInt4 + j - k);
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    int i = getChildCount();
    int j = 0;
    int k = 0;
    int m = 0;
    int n;
    for (n = 0; n < i; n++, j = i4, k = i5, m = i6) {
      View view = getChildAt(n);
      int i4 = j, i5 = k, i6 = m;
      if (view.getVisibility() != 8) {
        measureChild(view, paramInt1, paramInt2);
        i5 = Math.max(k, view.getMeasuredWidth());
        i4 = Math.max(j, view.getMeasuredHeight());
        k = view.getMeasuredState();
        i6 = combineMeasuredStates(m, k);
      } 
    } 
    int i3 = this.mPaddingLeft;
    n = this.mPaddingRight;
    int i2 = this.mPaddingTop, i1 = this.mPaddingBottom;
    j = Math.max(j + i2 + i1, getSuggestedMinimumHeight());
    k = Math.max(k + i3 + n, getSuggestedMinimumWidth());
    paramInt1 = resolveSizeAndState(k, paramInt1, m);
    paramInt2 = resolveSizeAndState(j, paramInt2, m << 16);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public void onBackStackChanged() {
    updateCrumbs();
  }
  
  private int getPreEntryCount() {
    byte b2;
    BackStackRecord backStackRecord = this.mTopEntry;
    byte b1 = 1;
    if (backStackRecord != null) {
      b2 = 1;
    } else {
      b2 = 0;
    } 
    if (this.mParentEntry == null)
      b1 = 0; 
    return b2 + b1;
  }
  
  private FragmentManager.BackStackEntry getPreEntry(int paramInt) {
    BackStackRecord backStackRecord = this.mParentEntry;
    if (backStackRecord != null) {
      if (paramInt != 0)
        backStackRecord = this.mTopEntry; 
      return backStackRecord;
    } 
    return this.mTopEntry;
  }
  
  void updateCrumbs() {
    FragmentManager fragmentManager = this.mActivity.getFragmentManager();
    int i = fragmentManager.getBackStackEntryCount();
    int j = getPreEntryCount();
    int k = this.mContainer.getChildCount();
    int m;
    for (m = 0; m < i + j; m++, k = n) {
      FragmentManager.BackStackEntry backStackEntry;
      if (m < j) {
        backStackEntry = getPreEntry(m);
      } else {
        backStackEntry = fragmentManager.getBackStackEntryAt(m - j);
      } 
      int n = k;
      if (m < k) {
        View view = this.mContainer.getChildAt(m);
        Object object = view.getTag();
        n = k;
        if (object != backStackEntry) {
          for (n = m; n < k; n++)
            this.mContainer.removeViewAt(m); 
          n = m;
        } 
      } 
      if (m >= n) {
        View view = this.mInflater.inflate(this.mLayoutResId, this, false);
        TextView textView = (TextView)view.findViewById(16908310);
        textView.setText(backStackEntry.getBreadCrumbTitle());
        textView.setTag(backStackEntry);
        textView.setTextColor(this.mTextColor);
        if (m == 0)
          view.findViewById(16909123).setVisibility(8); 
        this.mContainer.addView(view);
        textView.setOnClickListener(this.mOnClickListener);
      } 
    } 
    m = this.mContainer.getChildCount();
    while (m > i + j) {
      this.mContainer.removeViewAt(m - 1);
      m--;
    } 
    for (k = 0; k < m; k++) {
      boolean bool;
      View view1 = this.mContainer.getChildAt(k);
      View view2 = view1.findViewById(16908310);
      if (k < m - 1) {
        bool = true;
      } else {
        bool = false;
      } 
      view2.setEnabled(bool);
      int n = this.mMaxVisible;
      if (n > 0) {
        if (k < m - n) {
          n = 8;
        } else {
          n = 0;
        } 
        view1.setVisibility(n);
        view2 = view1.findViewById(16909123);
        if (k > m - this.mMaxVisible && k != 0) {
          n = 0;
        } else {
          n = 8;
        } 
        view2.setVisibility(n);
      } 
    } 
  }
  
  @Deprecated
  class OnBreadCrumbClickListener {
    public abstract boolean onBreadCrumbClick(FragmentManager.BackStackEntry param1BackStackEntry, int param1Int);
  }
}
