package android.app.job;

import android.os.Parcel;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusBaseJobInfo {
  public static final int LEVEL_CPU_ABNORMAL_HEAVY = 3;
  
  public static final int LEVEL_CPU_ABNORMAL_MIDDLE = 2;
  
  public static final int LEVEL_CPU_ABNORMAL_SLIGHT = 1;
  
  public static final int LEVEL_CPU_NORMAL = 0;
  
  public static final int SCENE_MODE_GAME = 4;
  
  public static final int SCENE_MODE_VIDEO = 1;
  
  public static final int SCENE_MODE_VIDEO_CALL = 2;
  
  public static final int TYPE_PROTECT_FORE_FRAME = 0;
  
  public static final int TYPE_PROTECT_FORE_NET = 1;
  
  private boolean hasCpuConstraint;
  
  private boolean hasProtectSceneConstraint;
  
  private boolean hasTemperatureConstraint;
  
  protected long intervalMillis;
  
  private boolean isNotSysApp = false;
  
  private boolean isOppoJob;
  
  protected long maxExecutionDelayMillis;
  
  private String oppoExtraStr;
  
  private int protectForeType;
  
  private int protectScene;
  
  private boolean requireBattIdle;
  
  private boolean requireProtectFore;
  
  public boolean hasTemperatureConstraint() {
    return this.hasTemperatureConstraint;
  }
  
  public boolean hasProtectSceneConstraint() {
    return this.hasProtectSceneConstraint;
  }
  
  public int getProtectScene() {
    return this.protectScene;
  }
  
  public boolean isRequireProtectFore() {
    return this.requireProtectFore;
  }
  
  public boolean hasCpuConstraint() {
    return this.hasCpuConstraint;
  }
  
  public boolean getOplusJob() {
    return this.isOppoJob;
  }
  
  public String getOplusExtraStr() {
    return this.oppoExtraStr;
  }
  
  public int getProtectForeType() {
    return this.protectForeType;
  }
  
  public void initJobInfo(Parcel paramParcel) {
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.requireBattIdle = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isOppoJob = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.requireProtectFore = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.hasCpuConstraint = bool2;
    this.oppoExtraStr = paramParcel.readString();
    this.protectForeType = paramParcel.readInt();
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isNotSysApp = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.hasTemperatureConstraint = bool2;
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.hasProtectSceneConstraint = bool2;
    this.protectScene = paramParcel.readInt();
  }
  
  public void initJobInfo(BaseBuilder paramBaseBuilder) {
    this.requireBattIdle = paramBaseBuilder.mRequiresBattIdle;
    this.isOppoJob = paramBaseBuilder.mIsOplusJob;
    this.requireProtectFore = paramBaseBuilder.mRequiresProtectFore;
    this.hasCpuConstraint = paramBaseBuilder.mHasCpuConstraint;
    this.oppoExtraStr = paramBaseBuilder.mOplusExtraStr;
    this.protectForeType = paramBaseBuilder.mProtectForeType;
    this.hasTemperatureConstraint = paramBaseBuilder.mHasTemperatureConstraint;
    this.hasProtectSceneConstraint = paramBaseBuilder.mHasProtectSceneConstraint;
    this.protectScene = paramBaseBuilder.mProtectScene;
  }
  
  public void writeToParcelJobInfo(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.requireBattIdle);
    paramParcel.writeInt(this.isOppoJob);
    paramParcel.writeInt(this.requireProtectFore);
    paramParcel.writeInt(this.hasCpuConstraint);
    paramParcel.writeString(this.oppoExtraStr);
    paramParcel.writeInt(this.protectForeType);
    paramParcel.writeInt(this.isNotSysApp);
    paramParcel.writeInt(this.hasTemperatureConstraint);
    paramParcel.writeInt(this.hasProtectSceneConstraint);
    paramParcel.writeInt(this.protectScene);
  }
  
  public static class BaseBuilder {
    public boolean mHasCpuConstraint;
    
    public boolean mHasProtectSceneConstraint;
    
    public boolean mHasTemperatureConstraint;
    
    public boolean mIsOplusJob;
    
    public String mOplusExtraStr;
    
    public int mProtectForeType;
    
    public int mProtectScene;
    
    public boolean mRequiresBattIdle;
    
    public boolean mRequiresProtectFore;
    
    public JobInfo.Builder setRequiresBattIdle(boolean param1Boolean, int param1Int) {
      this.mRequiresBattIdle = param1Boolean;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setOplusJob(boolean param1Boolean) {
      this.mIsOplusJob = param1Boolean;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setRequiresProtectFore(boolean param1Boolean) {
      setRequiresProtectFore(param1Boolean, 0);
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setRequiresProtectFore(boolean param1Boolean, int param1Int) {
      this.mRequiresProtectFore = param1Boolean;
      this.mProtectForeType = param1Int;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setHasCpuConstraint(boolean param1Boolean) {
      this.mHasCpuConstraint = param1Boolean;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setOplusExtraStr(String param1String) {
      this.mOplusExtraStr = param1String;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setHasTemperatureConstraint(boolean param1Boolean) {
      this.mHasTemperatureConstraint = param1Boolean;
      return OplusBaseJobInfo.typeCasting(this);
    }
    
    public JobInfo.Builder setRequiresProtectScene(boolean param1Boolean, int param1Int) {
      this.mHasProtectSceneConstraint = param1Boolean;
      this.mProtectScene = param1Int;
      return OplusBaseJobInfo.typeCasting(this);
    }
  }
  
  public boolean isRequireBattIdle() {
    return this.requireBattIdle;
  }
  
  public boolean isSystemApp() {
    return this.isNotSysApp ^ true;
  }
  
  public void setSysApp(boolean paramBoolean) {
    this.isNotSysApp = paramBoolean ^ true;
  }
  
  public void setMaxExecutionDelayMillis(long paramLong) {
    this.maxExecutionDelayMillis = paramLong;
  }
  
  public void setIntervalMillis(long paramLong) {
    this.intervalMillis = paramLong;
  }
  
  private static JobInfo.Builder typeCasting(BaseBuilder paramBaseBuilder) {
    return (JobInfo.Builder)OplusTypeCastingHelper.typeCasting(JobInfo.Builder.class, paramBaseBuilder);
  }
}
