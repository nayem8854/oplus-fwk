package android.app.job;

import android.content.ClipData;
import android.content.ComponentName;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.Uri;
import android.os.BaseBundle;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.util.Log;
import android.util.TimeUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class JobInfo extends OplusBaseJobInfo implements Parcelable {
  public static final int BACKOFF_POLICY_EXPONENTIAL = 1;
  
  public static final int BACKOFF_POLICY_LINEAR = 0;
  
  public static final int CONSTRAINT_FLAG_BATTERY_NOT_LOW = 2;
  
  public static final int CONSTRAINT_FLAG_CHARGING = 1;
  
  public static final int CONSTRAINT_FLAG_DEVICE_IDLE = 4;
  
  public static final int CONSTRAINT_FLAG_STORAGE_NOT_LOW = 8;
  
  public static final Parcelable.Creator<JobInfo> CREATOR;
  
  public static final int DEFAULT_BACKOFF_POLICY = 1;
  
  public static final long DEFAULT_INITIAL_BACKOFF_MILLIS = 30000L;
  
  public static final int FLAG_EXEMPT_FROM_APP_STANDBY = 8;
  
  public static final int FLAG_IMPORTANT_WHILE_FOREGROUND = 2;
  
  public static final int FLAG_PREFETCH = 4;
  
  public static final int FLAG_WILL_BE_FOREGROUND = 1;
  
  public static final long MAX_BACKOFF_DELAY_MILLIS = 18000000L;
  
  public static final long MIN_BACKOFF_MILLIS = 10000L;
  
  private static final long MIN_FLEX_MILLIS = 300000L;
  
  private static final long MIN_PERIOD_MILLIS = 900000L;
  
  public static final int NETWORK_BYTES_UNKNOWN = -1;
  
  public static final int NETWORK_TYPE_ANY = 1;
  
  public static final int NETWORK_TYPE_CELLULAR = 4;
  
  @Deprecated
  public static final int NETWORK_TYPE_METERED = 4;
  
  public static final int NETWORK_TYPE_NONE = 0;
  
  public static final int NETWORK_TYPE_NOT_ROAMING = 3;
  
  public static final int NETWORK_TYPE_UNMETERED = 2;
  
  public static final int PRIORITY_ADJ_ALWAYS_RUNNING = -80;
  
  public static final int PRIORITY_ADJ_OFTEN_RUNNING = -40;
  
  public static final int PRIORITY_BOUND_FOREGROUND_SERVICE = 30;
  
  public static final int PRIORITY_DEFAULT = 0;
  
  public static final int PRIORITY_FOREGROUND_APP = 30;
  
  public static final int PRIORITY_FOREGROUND_SERVICE = 35;
  
  public static final int PRIORITY_SYNC_EXPEDITED = 10;
  
  public static final int PRIORITY_SYNC_INITIALIZATION = 20;
  
  public static final int PRIORITY_TOP_APP = 40;
  
  private static String TAG = "JobInfo";
  
  private final int backoffPolicy;
  
  private final ClipData clipData;
  
  private final int clipGrantFlags;
  
  private final int constraintFlags;
  
  private final PersistableBundle extras;
  
  private final int flags;
  
  private final long flexMillis;
  
  private final boolean hasEarlyConstraint;
  
  private final boolean hasLateConstraint;
  
  private final long initialBackoffMillis;
  
  private final boolean isPeriodic;
  
  private final boolean isPersisted;
  
  private final int jobId;
  
  private final long minLatencyMillis;
  
  private final long networkDownloadBytes;
  
  private final NetworkRequest networkRequest;
  
  private final long networkUploadBytes;
  
  private final int priority;
  
  private final ComponentName service;
  
  private final Bundle transientExtras;
  
  private final long triggerContentMaxDelay;
  
  private final long triggerContentUpdateDelay;
  
  private final TriggerContentUri[] triggerContentUris;
  
  public static final long getMinPeriodMillis() {
    return 900000L;
  }
  
  public static final long getMinFlexMillis() {
    return 300000L;
  }
  
  public static final long getMinBackoffMillis() {
    return 10000L;
  }
  
  public int getId() {
    return this.jobId;
  }
  
  public PersistableBundle getExtras() {
    return this.extras;
  }
  
  public Bundle getTransientExtras() {
    return this.transientExtras;
  }
  
  public ClipData getClipData() {
    return this.clipData;
  }
  
  public int getClipGrantFlags() {
    return this.clipGrantFlags;
  }
  
  public ComponentName getService() {
    return this.service;
  }
  
  public int getPriority() {
    return this.priority;
  }
  
  public int getFlags() {
    return this.flags;
  }
  
  public boolean isExemptedFromAppStandby() {
    boolean bool;
    if ((this.flags & 0x8) != 0 && !isPeriodic()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRequireCharging() {
    int i = this.constraintFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isRequireBatteryNotLow() {
    boolean bool;
    if ((this.constraintFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRequireDeviceIdle() {
    boolean bool;
    if ((this.constraintFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isRequireStorageNotLow() {
    boolean bool;
    if ((this.constraintFlags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getConstraintFlags() {
    return this.constraintFlags;
  }
  
  public TriggerContentUri[] getTriggerContentUris() {
    return this.triggerContentUris;
  }
  
  public long getTriggerContentUpdateDelay() {
    return this.triggerContentUpdateDelay;
  }
  
  public long getTriggerContentMaxDelay() {
    return this.triggerContentMaxDelay;
  }
  
  @Deprecated
  public int getNetworkType() {
    NetworkRequest networkRequest = this.networkRequest;
    if (networkRequest == null)
      return 0; 
    if (networkRequest.networkCapabilities.hasCapability(11))
      return 2; 
    if (this.networkRequest.networkCapabilities.hasCapability(18))
      return 3; 
    if (this.networkRequest.networkCapabilities.hasTransport(0))
      return 4; 
    return 1;
  }
  
  public NetworkRequest getRequiredNetwork() {
    return this.networkRequest;
  }
  
  public long getEstimatedNetworkDownloadBytes() {
    return this.networkDownloadBytes;
  }
  
  public long getEstimatedNetworkUploadBytes() {
    return this.networkUploadBytes;
  }
  
  public long getMinLatencyMillis() {
    return this.minLatencyMillis;
  }
  
  public long getMaxExecutionDelayMillis() {
    return this.maxExecutionDelayMillis;
  }
  
  public boolean isPeriodic() {
    return this.isPeriodic;
  }
  
  public boolean isPersisted() {
    return this.isPersisted;
  }
  
  public long getIntervalMillis() {
    return this.intervalMillis;
  }
  
  public long getFlexMillis() {
    return this.flexMillis;
  }
  
  public long getInitialBackoffMillis() {
    return this.initialBackoffMillis;
  }
  
  public int getBackoffPolicy() {
    return this.backoffPolicy;
  }
  
  public boolean isImportantWhileForeground() {
    boolean bool;
    if ((this.flags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isPrefetch() {
    boolean bool;
    if ((this.flags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasEarlyConstraint() {
    return this.hasEarlyConstraint;
  }
  
  public boolean hasLateConstraint() {
    return this.hasLateConstraint;
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof JobInfo))
      return false; 
    paramObject = paramObject;
    if (this.jobId != ((JobInfo)paramObject).jobId)
      return false; 
    if (!BaseBundle.kindofEquals((BaseBundle)this.extras, (BaseBundle)((JobInfo)paramObject).extras))
      return false; 
    if (!BaseBundle.kindofEquals((BaseBundle)this.transientExtras, (BaseBundle)((JobInfo)paramObject).transientExtras))
      return false; 
    if (this.clipData != ((JobInfo)paramObject).clipData)
      return false; 
    if (this.clipGrantFlags != ((JobInfo)paramObject).clipGrantFlags)
      return false; 
    if (!Objects.equals(this.service, ((JobInfo)paramObject).service))
      return false; 
    if (this.constraintFlags != ((JobInfo)paramObject).constraintFlags)
      return false; 
    if (!Arrays.equals((Object[])this.triggerContentUris, (Object[])((JobInfo)paramObject).triggerContentUris))
      return false; 
    if (this.triggerContentUpdateDelay != ((JobInfo)paramObject).triggerContentUpdateDelay)
      return false; 
    if (this.triggerContentMaxDelay != ((JobInfo)paramObject).triggerContentMaxDelay)
      return false; 
    if (this.hasEarlyConstraint != ((JobInfo)paramObject).hasEarlyConstraint)
      return false; 
    if (this.hasLateConstraint != ((JobInfo)paramObject).hasLateConstraint)
      return false; 
    if (!Objects.equals(this.networkRequest, ((JobInfo)paramObject).networkRequest))
      return false; 
    if (this.networkDownloadBytes != ((JobInfo)paramObject).networkDownloadBytes)
      return false; 
    if (this.networkUploadBytes != ((JobInfo)paramObject).networkUploadBytes)
      return false; 
    if (this.minLatencyMillis != ((JobInfo)paramObject).minLatencyMillis)
      return false; 
    if (this.maxExecutionDelayMillis != ((JobInfo)paramObject).maxExecutionDelayMillis)
      return false; 
    if (this.isPeriodic != ((JobInfo)paramObject).isPeriodic)
      return false; 
    if (this.isPersisted != ((JobInfo)paramObject).isPersisted)
      return false; 
    if (this.intervalMillis != ((JobInfo)paramObject).intervalMillis)
      return false; 
    if (this.flexMillis != ((JobInfo)paramObject).flexMillis)
      return false; 
    if (this.initialBackoffMillis != ((JobInfo)paramObject).initialBackoffMillis)
      return false; 
    if (this.backoffPolicy != ((JobInfo)paramObject).backoffPolicy)
      return false; 
    if (this.priority != ((JobInfo)paramObject).priority)
      return false; 
    if (this.flags != ((JobInfo)paramObject).flags)
      return false; 
    return true;
  }
  
  public int hashCode() {
    int i = this.jobId;
    PersistableBundle persistableBundle = this.extras;
    int j = i;
    if (persistableBundle != null)
      j = i * 31 + persistableBundle.hashCode(); 
    Bundle bundle = this.transientExtras;
    i = j;
    if (bundle != null)
      i = j * 31 + bundle.hashCode(); 
    ClipData clipData = this.clipData;
    j = i;
    if (clipData != null)
      j = i * 31 + clipData.hashCode(); 
    j = j * 31 + this.clipGrantFlags;
    ComponentName componentName = this.service;
    i = j;
    if (componentName != null)
      i = j * 31 + componentName.hashCode(); 
    j = i * 31 + this.constraintFlags;
    TriggerContentUri[] arrayOfTriggerContentUri = this.triggerContentUris;
    i = j;
    if (arrayOfTriggerContentUri != null)
      i = j * 31 + Arrays.hashCode((Object[])arrayOfTriggerContentUri); 
    int k = Long.hashCode(this.triggerContentUpdateDelay);
    int m = Long.hashCode(this.triggerContentMaxDelay);
    j = Boolean.hashCode(this.hasEarlyConstraint);
    j = (((i * 31 + k) * 31 + m) * 31 + j) * 31 + Boolean.hashCode(this.hasLateConstraint);
    NetworkRequest networkRequest = this.networkRequest;
    i = j;
    if (networkRequest != null)
      i = j * 31 + networkRequest.hashCode(); 
    int n = Long.hashCode(this.networkDownloadBytes);
    int i1 = Long.hashCode(this.networkUploadBytes);
    int i2 = Long.hashCode(this.minLatencyMillis);
    int i3 = Long.hashCode(this.maxExecutionDelayMillis);
    int i4 = Boolean.hashCode(this.isPeriodic);
    int i5 = Boolean.hashCode(this.isPersisted);
    int i6 = Long.hashCode(this.intervalMillis);
    j = Long.hashCode(this.flexMillis);
    int i7 = Long.hashCode(this.initialBackoffMillis);
    k = this.backoffPolicy;
    int i8 = this.priority;
    m = this.flags;
    return (((((((((((i * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + j) * 31 + i7) * 31 + k) * 31 + i8) * 31 + m;
  }
  
  private JobInfo(Parcel paramParcel) {
    this.jobId = paramParcel.readInt();
    this.extras = paramParcel.readPersistableBundle();
    this.transientExtras = paramParcel.readBundle();
    int i = paramParcel.readInt();
    boolean bool1 = false;
    if (i != 0) {
      this.clipData = (ClipData)ClipData.CREATOR.createFromParcel(paramParcel);
      this.clipGrantFlags = paramParcel.readInt();
    } else {
      this.clipData = null;
      this.clipGrantFlags = 0;
    } 
    this.service = (ComponentName)paramParcel.readParcelable(null);
    this.constraintFlags = paramParcel.readInt();
    this.triggerContentUris = (TriggerContentUri[])paramParcel.createTypedArray(TriggerContentUri.CREATOR);
    this.triggerContentUpdateDelay = paramParcel.readLong();
    this.triggerContentMaxDelay = paramParcel.readLong();
    if (paramParcel.readInt() != 0) {
      this.networkRequest = (NetworkRequest)NetworkRequest.CREATOR.createFromParcel(paramParcel);
    } else {
      this.networkRequest = null;
    } 
    this.networkDownloadBytes = paramParcel.readLong();
    this.networkUploadBytes = paramParcel.readLong();
    this.minLatencyMillis = paramParcel.readLong();
    this.maxExecutionDelayMillis = paramParcel.readLong();
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isPeriodic = bool2;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.isPersisted = bool2;
    this.intervalMillis = paramParcel.readLong();
    this.flexMillis = paramParcel.readLong();
    this.initialBackoffMillis = paramParcel.readLong();
    this.backoffPolicy = paramParcel.readInt();
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.hasEarlyConstraint = bool2;
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.hasLateConstraint = bool2;
    this.priority = paramParcel.readInt();
    this.flags = paramParcel.readInt();
    initJobInfo(paramParcel);
  }
  
  private JobInfo(Builder paramBuilder) {
    TriggerContentUri[] arrayOfTriggerContentUri;
    this.jobId = paramBuilder.mJobId;
    this.extras = paramBuilder.mExtras.deepCopy();
    this.transientExtras = paramBuilder.mTransientExtras.deepCopy();
    this.clipData = paramBuilder.mClipData;
    this.clipGrantFlags = paramBuilder.mClipGrantFlags;
    this.service = paramBuilder.mJobService;
    this.constraintFlags = paramBuilder.mConstraintFlags;
    if (paramBuilder.mTriggerContentUris != null) {
      arrayOfTriggerContentUri = (TriggerContentUri[])paramBuilder.mTriggerContentUris.toArray((Object[])new TriggerContentUri[paramBuilder.mTriggerContentUris.size()]);
    } else {
      arrayOfTriggerContentUri = null;
    } 
    this.triggerContentUris = arrayOfTriggerContentUri;
    this.triggerContentUpdateDelay = paramBuilder.mTriggerContentUpdateDelay;
    this.triggerContentMaxDelay = paramBuilder.mTriggerContentMaxDelay;
    this.networkRequest = paramBuilder.mNetworkRequest;
    this.networkDownloadBytes = paramBuilder.mNetworkDownloadBytes;
    this.networkUploadBytes = paramBuilder.mNetworkUploadBytes;
    this.minLatencyMillis = paramBuilder.mMinLatencyMillis;
    this.maxExecutionDelayMillis = paramBuilder.mMaxExecutionDelayMillis;
    this.isPeriodic = paramBuilder.mIsPeriodic;
    this.isPersisted = paramBuilder.mIsPersisted;
    this.intervalMillis = paramBuilder.mIntervalMillis;
    this.flexMillis = paramBuilder.mFlexMillis;
    this.initialBackoffMillis = paramBuilder.mInitialBackoffMillis;
    this.backoffPolicy = paramBuilder.mBackoffPolicy;
    this.hasEarlyConstraint = paramBuilder.mHasEarlyConstraint;
    this.hasLateConstraint = paramBuilder.mHasLateConstraint;
    this.priority = paramBuilder.mPriority;
    this.flags = paramBuilder.mFlags;
    initJobInfo(paramBuilder);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.jobId);
    paramParcel.writePersistableBundle(this.extras);
    paramParcel.writeBundle(this.transientExtras);
    if (this.clipData != null) {
      paramParcel.writeInt(1);
      this.clipData.writeToParcel(paramParcel, paramInt);
      paramParcel.writeInt(this.clipGrantFlags);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeParcelable(this.service, paramInt);
    paramParcel.writeInt(this.constraintFlags);
    paramParcel.writeTypedArray((Parcelable[])this.triggerContentUris, paramInt);
    paramParcel.writeLong(this.triggerContentUpdateDelay);
    paramParcel.writeLong(this.triggerContentMaxDelay);
    if (this.networkRequest != null) {
      paramParcel.writeInt(1);
      this.networkRequest.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeLong(this.networkDownloadBytes);
    paramParcel.writeLong(this.networkUploadBytes);
    paramParcel.writeLong(this.minLatencyMillis);
    paramParcel.writeLong(this.maxExecutionDelayMillis);
    paramParcel.writeInt(this.isPeriodic);
    paramParcel.writeInt(this.isPersisted);
    paramParcel.writeLong(this.intervalMillis);
    paramParcel.writeLong(this.flexMillis);
    paramParcel.writeLong(this.initialBackoffMillis);
    paramParcel.writeInt(this.backoffPolicy);
    paramParcel.writeInt(this.hasEarlyConstraint);
    paramParcel.writeInt(this.hasLateConstraint);
    paramParcel.writeInt(this.priority);
    paramParcel.writeInt(this.flags);
    writeToParcelJobInfo(paramParcel, paramInt);
  }
  
  static {
    CREATOR = new Parcelable.Creator<JobInfo>() {
        public JobInfo createFromParcel(Parcel param1Parcel) {
          return new JobInfo(param1Parcel);
        }
        
        public JobInfo[] newArray(int param1Int) {
          return new JobInfo[param1Int];
        }
      };
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("(job:");
    stringBuilder.append(this.jobId);
    stringBuilder.append("/");
    stringBuilder.append(this.service.flattenToShortString());
    stringBuilder.append(")");
    return stringBuilder.toString();
  }
  
  public static final class TriggerContentUri implements Parcelable {
    public TriggerContentUri(Uri param1Uri, int param1Int) {
      Objects.requireNonNull(param1Uri);
      this.mUri = param1Uri;
      this.mFlags = param1Int;
    }
    
    public Uri getUri() {
      return this.mUri;
    }
    
    public int getFlags() {
      return this.mFlags;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof TriggerContentUri;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (Objects.equals(((TriggerContentUri)param1Object).mUri, this.mUri)) {
        bool = bool1;
        if (((TriggerContentUri)param1Object).mFlags == this.mFlags)
          bool = true; 
      } 
      return bool;
    }
    
    public int hashCode() {
      int i;
      Uri uri = this.mUri;
      if (uri == null) {
        i = 0;
      } else {
        i = uri.hashCode();
      } 
      return i ^ this.mFlags;
    }
    
    private TriggerContentUri(Parcel param1Parcel) {
      this.mUri = (Uri)Uri.CREATOR.createFromParcel(param1Parcel);
      this.mFlags = param1Parcel.readInt();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mUri.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.mFlags);
    }
    
    public static final Parcelable.Creator<TriggerContentUri> CREATOR = new Parcelable.Creator<TriggerContentUri>() {
        public JobInfo.TriggerContentUri createFromParcel(Parcel param2Parcel) {
          return new JobInfo.TriggerContentUri(param2Parcel);
        }
        
        public JobInfo.TriggerContentUri[] newArray(int param2Int) {
          return new JobInfo.TriggerContentUri[param2Int];
        }
      };
    
    public static final int FLAG_NOTIFY_FOR_DESCENDANTS = 1;
    
    private final int mFlags;
    
    private final Uri mUri;
    
    @Retention(RetentionPolicy.SOURCE)
    class Flags implements Annotation {}
  }
  
  class null implements Parcelable.Creator<TriggerContentUri> {
    public JobInfo.TriggerContentUri createFromParcel(Parcel param1Parcel) {
      return new JobInfo.TriggerContentUri(param1Parcel);
    }
    
    public JobInfo.TriggerContentUri[] newArray(int param1Int) {
      return new JobInfo.TriggerContentUri[param1Int];
    }
  }
  
  public static final class Builder extends OplusBaseJobInfo.BaseBuilder {
    private PersistableBundle mExtras = PersistableBundle.EMPTY;
    
    private Bundle mTransientExtras = Bundle.EMPTY;
    
    private int mPriority = 0;
    
    private long mNetworkDownloadBytes = -1L;
    
    private long mNetworkUploadBytes = -1L;
    
    private long mTriggerContentUpdateDelay = -1L;
    
    private long mTriggerContentMaxDelay = -1L;
    
    private long mInitialBackoffMillis = 30000L;
    
    private int mBackoffPolicy = 1;
    
    private boolean mBackoffPolicySet = false;
    
    private ClipData mClipData;
    
    private int mClipGrantFlags;
    
    private int mConstraintFlags;
    
    private int mFlags;
    
    private long mFlexMillis;
    
    private boolean mHasEarlyConstraint;
    
    private boolean mHasLateConstraint;
    
    private long mIntervalMillis;
    
    private boolean mIsPeriodic;
    
    private boolean mIsPersisted;
    
    private final int mJobId;
    
    private final ComponentName mJobService;
    
    private long mMaxExecutionDelayMillis;
    
    private long mMinLatencyMillis;
    
    private NetworkRequest mNetworkRequest;
    
    private ArrayList<JobInfo.TriggerContentUri> mTriggerContentUris;
    
    public Builder(int param1Int, ComponentName param1ComponentName) {
      this.mJobService = param1ComponentName;
      this.mJobId = param1Int;
    }
    
    public Builder setPriority(int param1Int) {
      this.mPriority = param1Int;
      return this;
    }
    
    public Builder setFlags(int param1Int) {
      this.mFlags = param1Int;
      return this;
    }
    
    public Builder setExtras(PersistableBundle param1PersistableBundle) {
      this.mExtras = param1PersistableBundle;
      return this;
    }
    
    public Builder setTransientExtras(Bundle param1Bundle) {
      this.mTransientExtras = param1Bundle;
      return this;
    }
    
    public Builder setClipData(ClipData param1ClipData, int param1Int) {
      this.mClipData = param1ClipData;
      this.mClipGrantFlags = param1Int;
      return this;
    }
    
    public Builder setRequiredNetworkType(int param1Int) {
      if (param1Int == 0)
        return setRequiredNetwork(null); 
      NetworkRequest.Builder builder = new NetworkRequest.Builder();
      builder.addCapability(12);
      builder.addCapability(16);
      builder.removeCapability(15);
      if (param1Int != 1)
        if (param1Int == 2) {
          builder.addCapability(11);
        } else if (param1Int == 3) {
          builder.addCapability(18);
        } else if (param1Int == 4) {
          builder.addTransportType(0);
        }  
      return setRequiredNetwork(builder.build());
    }
    
    public Builder setRequiredNetwork(NetworkRequest param1NetworkRequest) {
      this.mNetworkRequest = param1NetworkRequest;
      return this;
    }
    
    public Builder setEstimatedNetworkBytes(long param1Long1, long param1Long2) {
      this.mNetworkDownloadBytes = param1Long1;
      this.mNetworkUploadBytes = param1Long2;
      return this;
    }
    
    public Builder setRequiresCharging(boolean param1Boolean) {
      int i = this.mConstraintFlags;
      this.mConstraintFlags = i & 0xFFFFFFFE | param1Boolean;
      return this;
    }
    
    public Builder setRequiresBatteryNotLow(boolean param1Boolean) {
      boolean bool;
      int i = this.mConstraintFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mConstraintFlags = i & 0xFFFFFFFD | bool;
      return this;
    }
    
    public Builder setRequiresDeviceIdle(boolean param1Boolean) {
      boolean bool;
      int i = this.mConstraintFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mConstraintFlags = i & 0xFFFFFFFB | bool;
      return this;
    }
    
    public Builder setRequiresStorageNotLow(boolean param1Boolean) {
      boolean bool;
      int i = this.mConstraintFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mConstraintFlags = i & 0xFFFFFFF7 | bool;
      return this;
    }
    
    public Builder addTriggerContentUri(JobInfo.TriggerContentUri param1TriggerContentUri) {
      if (this.mTriggerContentUris == null)
        this.mTriggerContentUris = new ArrayList<>(); 
      this.mTriggerContentUris.add(param1TriggerContentUri);
      return this;
    }
    
    public Builder setTriggerContentUpdateDelay(long param1Long) {
      this.mTriggerContentUpdateDelay = param1Long;
      return this;
    }
    
    public Builder setTriggerContentMaxDelay(long param1Long) {
      this.mTriggerContentMaxDelay = param1Long;
      return this;
    }
    
    public Builder setPeriodic(long param1Long) {
      return setPeriodic(param1Long, param1Long);
    }
    
    public Builder setPeriodic(long param1Long1, long param1Long2) {
      long l1 = JobInfo.getMinPeriodMillis();
      long l2 = param1Long1;
      if (param1Long1 < l1) {
        String str1 = JobInfo.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Requested interval ");
        stringBuilder.append(TimeUtils.formatDuration(param1Long1));
        stringBuilder.append(" for job ");
        stringBuilder.append(this.mJobId);
        stringBuilder.append(" is too small; raising to ");
        stringBuilder.append(TimeUtils.formatDuration(l1));
        String str2 = stringBuilder.toString();
        Log.w(str1, str2);
        l2 = l1;
      } 
      param1Long1 = 5L * l2 / 100L;
      l1 = Math.max(param1Long1, JobInfo.getMinFlexMillis());
      param1Long1 = param1Long2;
      if (param1Long2 < l1) {
        String str1 = JobInfo.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Requested flex ");
        stringBuilder.append(TimeUtils.formatDuration(param1Long2));
        stringBuilder.append(" for job ");
        stringBuilder.append(this.mJobId);
        stringBuilder.append(" is too small; raising to ");
        stringBuilder.append(TimeUtils.formatDuration(l1));
        String str2 = stringBuilder.toString();
        Log.w(str1, str2);
        param1Long1 = l1;
      } 
      this.mIsPeriodic = true;
      this.mIntervalMillis = l2;
      this.mFlexMillis = param1Long1;
      this.mHasLateConstraint = true;
      this.mHasEarlyConstraint = true;
      return this;
    }
    
    public Builder setMinimumLatency(long param1Long) {
      this.mMinLatencyMillis = param1Long;
      this.mHasEarlyConstraint = true;
      return this;
    }
    
    public Builder setOverrideDeadline(long param1Long) {
      this.mMaxExecutionDelayMillis = param1Long;
      this.mHasLateConstraint = true;
      return this;
    }
    
    public Builder setBackoffCriteria(long param1Long, int param1Int) {
      long l1 = JobInfo.getMinBackoffMillis();
      long l2 = param1Long;
      if (param1Long < l1) {
        String str1 = JobInfo.TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Requested backoff ");
        stringBuilder.append(TimeUtils.formatDuration(param1Long));
        stringBuilder.append(" for job ");
        stringBuilder.append(this.mJobId);
        stringBuilder.append(" is too small; raising to ");
        stringBuilder.append(TimeUtils.formatDuration(l1));
        String str2 = stringBuilder.toString();
        Log.w(str1, str2);
        l2 = l1;
      } 
      this.mBackoffPolicySet = true;
      this.mInitialBackoffMillis = l2;
      this.mBackoffPolicy = param1Int;
      return this;
    }
    
    public Builder setImportantWhileForeground(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x2;
      } else {
        this.mFlags &= 0xFFFFFFFD;
      } 
      return this;
    }
    
    public Builder setPrefetch(boolean param1Boolean) {
      if (param1Boolean) {
        this.mFlags |= 0x4;
      } else {
        this.mFlags &= 0xFFFFFFFB;
      } 
      return this;
    }
    
    public Builder setPersisted(boolean param1Boolean) {
      this.mIsPersisted = param1Boolean;
      return this;
    }
    
    public JobInfo build() {
      if ((this.mNetworkDownloadBytes <= 0L && this.mNetworkUploadBytes <= 0L) || this.mNetworkRequest != null) {
        if (this.mIsPersisted) {
          NetworkRequest networkRequest = this.mNetworkRequest;
          if (networkRequest != null) {
            NetworkCapabilities networkCapabilities = networkRequest.networkCapabilities;
            if (networkCapabilities.getNetworkSpecifier() != null)
              throw new IllegalArgumentException("Network specifiers aren't supported for persistent jobs"); 
          } 
        } 
        if (this.mIsPeriodic)
          if (this.mMaxExecutionDelayMillis == 0L) {
            if (this.mMinLatencyMillis == 0L) {
              if (this.mTriggerContentUris != null)
                throw new IllegalArgumentException("Can't call addTriggerContentUri() on a periodic job"); 
            } else {
              throw new IllegalArgumentException("Can't call setMinimumLatency() on a periodic job");
            } 
          } else {
            throw new IllegalArgumentException("Can't call setOverrideDeadline() on a periodic job.");
          }  
        if (this.mIsPersisted)
          if (this.mTriggerContentUris == null) {
            if (this.mTransientExtras.isEmpty()) {
              if (this.mClipData != null)
                throw new IllegalArgumentException("Can't call setClipData() on a persisted job"); 
            } else {
              throw new IllegalArgumentException("Can't call setTransientExtras() on a persisted job");
            } 
          } else {
            throw new IllegalArgumentException("Can't call addTriggerContentUri() on a persisted job");
          }  
        if ((this.mFlags & 0x2) == 0 || !this.mHasEarlyConstraint) {
          if (!this.mBackoffPolicySet || (this.mConstraintFlags & 0x4) == 0)
            return new JobInfo(this); 
          throw new IllegalArgumentException("An idle mode job will not respect any back-off policy, so calling setBackoffCriteria with setRequiresDeviceIdle is an error.");
        } 
        throw new IllegalArgumentException("An important while foreground job cannot have a time delay");
      } 
      throw new IllegalArgumentException("Can't provide estimated network usage without requiring a network");
    }
    
    public String summarize() {
      String str;
      ComponentName componentName = this.mJobService;
      if (componentName != null) {
        str = componentName.flattenToShortString();
      } else {
        str = "null";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("JobInfo.Builder{job:");
      stringBuilder.append(this.mJobId);
      stringBuilder.append("/");
      stringBuilder.append(str);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  public static String getPriorityString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 10) {
        if (paramInt != 20) {
          if (paramInt != 30) {
            if (paramInt != 35) {
              if (paramInt != 40) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(paramInt);
                stringBuilder.append(" [UNKNOWN]");
                return stringBuilder.toString();
              } 
              return "40 [TOP_APP]";
            } 
            return "35 [FGS_APP]";
          } 
          return "30 [BFGS_APP]";
        } 
        return "20 [SYNC_INITIALIZATION]";
      } 
      return "10 [SYNC_EXPEDITED]";
    } 
    return "0 [DEFAULT]";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class BackoffPolicy implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class NetworkType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Flags implements Annotation {}
}
