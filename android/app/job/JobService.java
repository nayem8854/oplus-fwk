package android.app.job;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public abstract class JobService extends Service {
  public static final String PERMISSION_BIND = "android.permission.BIND_JOB_SERVICE";
  
  private static final String TAG = "JobService";
  
  private JobServiceEngine mEngine;
  
  public final IBinder onBind(Intent paramIntent) {
    if (this.mEngine == null)
      this.mEngine = (JobServiceEngine)new Object(this, this); 
    return this.mEngine.getBinder();
  }
  
  public final void jobFinished(JobParameters paramJobParameters, boolean paramBoolean) {
    this.mEngine.jobFinished(paramJobParameters, paramBoolean);
  }
  
  public boolean onUpdateJobParameters(JobParameters paramJobParameters) {
    return false;
  }
  
  public abstract boolean onStartJob(JobParameters paramJobParameters);
  
  public abstract boolean onStopJob(JobParameters paramJobParameters);
}
