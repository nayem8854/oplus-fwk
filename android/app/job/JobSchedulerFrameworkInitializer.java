package android.app.job;

import android.annotation.SystemApi;
import android.app.JobSchedulerImpl;
import android.app.SystemServiceRegistry;
import android.content.Context;
import android.os.DeviceIdleManager;
import android.os.IBinder;
import android.os.IDeviceIdleController;
import android.os.PowerWhitelistManager;

@SystemApi
public class JobSchedulerFrameworkInitializer {
  public static void registerServiceWrappers() {
    SystemServiceRegistry.registerStaticService("jobscheduler", JobScheduler.class, (SystemServiceRegistry.StaticServiceProducerWithBinder<JobScheduler>)_$$Lambda$JobSchedulerFrameworkInitializer$RHUxgww0pZFMmfQWKgaRAx0YFqA.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("deviceidle", DeviceIdleManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithBinder<DeviceIdleManager>)_$$Lambda$JobSchedulerFrameworkInitializer$PtYe8PQc1PVJQXRnpm3iSxcWTR0.INSTANCE);
    SystemServiceRegistry.registerContextAwareService("power_whitelist", PowerWhitelistManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithoutBinder<PowerWhitelistManager>)_$$Lambda$FpGlzN9oJcl8o5soW_gU_DyTvXM.INSTANCE);
  }
}
