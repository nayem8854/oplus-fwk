package android.app.job;

import android.app.Service;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import java.lang.ref.WeakReference;

public abstract class JobServiceEngine {
  private static final int MSG_EXECUTE_JOB = 0;
  
  private static final int MSG_JOB_FINISHED = 2;
  
  private static final int MSG_STOP_JOB = 1;
  
  private static final int MSG_UPDATE_OPPO_JOB = 1001;
  
  private static final String TAG = "JobServiceEngine";
  
  private final IJobService mBinder;
  
  JobHandler mHandler;
  
  class JobInterface extends IJobService.Stub {
    final WeakReference<JobServiceEngine> mService;
    
    JobInterface(JobServiceEngine this$0) {
      this.mService = new WeakReference<>(this$0);
    }
    
    public void startJob(JobParameters param1JobParameters) throws RemoteException {
      JobServiceEngine jobServiceEngine = this.mService.get();
      if (jobServiceEngine != null) {
        Message message = Message.obtain(jobServiceEngine.mHandler, 0, param1JobParameters);
        message.sendToTarget();
      } 
    }
    
    public void stopJob(JobParameters param1JobParameters) throws RemoteException {
      JobServiceEngine jobServiceEngine = this.mService.get();
      if (jobServiceEngine != null) {
        Message message = Message.obtain(jobServiceEngine.mHandler, 1, param1JobParameters);
        message.sendToTarget();
      } 
    }
    
    public void updateJobParameters(JobParameters param1JobParameters) throws RemoteException {
      JobServiceEngine jobServiceEngine = this.mService.get();
      if (jobServiceEngine != null) {
        Message message = Message.obtain(jobServiceEngine.mHandler, 1001, param1JobParameters);
        message.sendToTarget();
      } 
    }
  }
  
  class JobHandler extends Handler {
    final JobServiceEngine this$0;
    
    JobHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      JobParameters jobParameters = (JobParameters)param1Message.obj;
      int i = param1Message.what;
      if (i != 0) {
        boolean bool = true;
        if (i != 1) {
          if (i != 2) {
            if (i != 1001) {
              Log.e("JobServiceEngine", "Unrecognised message received.");
            } else {
              try {
                JobServiceEngine.this.onUpdateJobParameters(jobParameters);
              } catch (Exception exception) {
                Log.e("JobServiceEngine", "Application unable to handle onUpdateJobParameters.", exception);
                throw new RuntimeException(exception);
              } 
            } 
          } else {
            if (((Message)exception).arg2 != 1)
              bool = false; 
            IJobCallback iJobCallback = jobParameters.getCallback();
            if (iJobCallback != null) {
              try {
                iJobCallback.jobFinished(jobParameters.getJobId(), bool);
              } catch (RemoteException remoteException) {
                Log.e("JobServiceEngine", "Error reporting job finish to system: binder has goneaway.");
              } 
            } else {
              Log.e("JobServiceEngine", "finishJob() called for a nonexistent job id.");
            } 
          } 
        } else {
          try {
            bool = JobServiceEngine.this.onStopJob(jobParameters);
            ackStopMessage(jobParameters, bool);
          } catch (Exception exception) {
            Log.e("JobServiceEngine", "Application unable to handle onStopJob.", exception);
            throw new RuntimeException(exception);
          } 
        } 
      } else {
        try {
          boolean bool = JobServiceEngine.this.onStartJob(jobParameters);
          ackStartMessage(jobParameters, bool);
          return;
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error while executing job: ");
          stringBuilder.append(jobParameters.getJobId());
          Log.e("JobServiceEngine", stringBuilder.toString());
          throw new RuntimeException(exception);
        } 
      } 
    }
    
    private void ackStartMessage(JobParameters param1JobParameters, boolean param1Boolean) {
      IJobCallback iJobCallback = param1JobParameters.getCallback();
      int i = param1JobParameters.getJobId();
      if (iJobCallback != null) {
        try {
          iJobCallback.acknowledgeStartMessage(i, param1Boolean);
        } catch (RemoteException remoteException) {
          Log.e("JobServiceEngine", "System unreachable for starting job.");
        } 
      } else if (Log.isLoggable("JobServiceEngine", 3)) {
        Log.d("JobServiceEngine", "Attempting to ack a job that has already been processed.");
      } 
    }
    
    private void ackStopMessage(JobParameters param1JobParameters, boolean param1Boolean) {
      IJobCallback iJobCallback = param1JobParameters.getCallback();
      int i = param1JobParameters.getJobId();
      if (iJobCallback != null) {
        try {
          iJobCallback.acknowledgeStopMessage(i, param1Boolean);
        } catch (RemoteException remoteException) {
          Log.e("JobServiceEngine", "System unreachable for stopping job.");
        } 
      } else if (Log.isLoggable("JobServiceEngine", 3)) {
        Log.d("JobServiceEngine", "Attempting to ack a job that has already been processed.");
      } 
    }
  }
  
  public JobServiceEngine(Service paramService) {
    this.mBinder = new JobInterface(this);
    this.mHandler = new JobHandler(paramService.getMainLooper());
  }
  
  public final IBinder getBinder() {
    return this.mBinder.asBinder();
  }
  
  public void jobFinished(JobParameters paramJobParameters, boolean paramBoolean) {
    if (paramJobParameters != null) {
      Message message = Message.obtain(this.mHandler, 2, paramJobParameters);
      message.arg2 = paramBoolean;
      message.sendToTarget();
      return;
    } 
    throw new NullPointerException("params");
  }
  
  public abstract boolean onStartJob(JobParameters paramJobParameters);
  
  public abstract boolean onStopJob(JobParameters paramJobParameters);
  
  public boolean onUpdateJobParameters(JobParameters paramJobParameters) {
    return false;
  }
}
