package android.app;

import android.content.ContentProviderNative;
import android.content.IContentProvider;
import android.content.pm.ProviderInfo;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public class ContentProviderHolder implements Parcelable {
  public ContentProviderHolder(ProviderInfo paramProviderInfo) {
    this.info = paramProviderInfo;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.info.writeToParcel(paramParcel, 0);
    IContentProvider iContentProvider = this.provider;
    if (iContentProvider != null) {
      paramParcel.writeStrongBinder(iContentProvider.asBinder());
    } else {
      paramParcel.writeStrongBinder(null);
    } 
    paramParcel.writeStrongBinder(this.connection);
    paramParcel.writeInt(this.noReleaseNeeded);
  }
  
  public static final Parcelable.Creator<ContentProviderHolder> CREATOR = new Parcelable.Creator<ContentProviderHolder>() {
      public ContentProviderHolder createFromParcel(Parcel param1Parcel) {
        return new ContentProviderHolder(param1Parcel);
      }
      
      public ContentProviderHolder[] newArray(int param1Int) {
        return new ContentProviderHolder[param1Int];
      }
    };
  
  public IBinder connection;
  
  public final ProviderInfo info;
  
  public boolean noReleaseNeeded;
  
  public IContentProvider provider;
  
  private ContentProviderHolder(Parcel paramParcel) {
    boolean bool;
    this.info = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel(paramParcel);
    IBinder iBinder = paramParcel.readStrongBinder();
    this.provider = ContentProviderNative.asInterface(iBinder);
    this.connection = paramParcel.readStrongBinder();
    if (paramParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    this.noReleaseNeeded = bool;
  }
}
