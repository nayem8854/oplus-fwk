package android.app;

import android.text.TextUtils;
import com.google.android.collect.Sets;
import java.util.HashSet;
import java.util.Set;

public class OplusUxIconAppCheckUtils {
  private static final Set<String> DESK_ACTIVITY_LIST;
  
  public static final HashSet<String> PRESET_APPS_LIST = new HashSet<String>() {
    
    };
  
  private static final Set<String> SYSTEM_APP_LIST = Sets.newHashSet((Object[])new String[] { "com.android.systemui", "com.android.settings", "com.android.browser", "com.android.calculator2", "com.android.calendar", "com.android.contacts", "com.android.mms", "com.android.packageinstaller", "com.android.permissioncontroller", "com.coloros.eyeprotect" });
  
  static {
    DESK_ACTIVITY_LIST = Sets.newHashSet((Object[])new String[] { "com.heytap.market" });
  }
  
  public static boolean isDeskActivity(String paramString) {
    return DESK_ACTIVITY_LIST.contains(paramString);
  }
  
  public static boolean isPresetApp(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    return PRESET_APPS_LIST.contains(paramString);
  }
  
  public static boolean isSystemApp(String paramString) {
    if (TextUtils.isEmpty(paramString))
      return false; 
    return SYSTEM_APP_LIST.contains(paramString);
  }
}
