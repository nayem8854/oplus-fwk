package android.app;

import android.app.uxicons.CustomAdaptiveIconConfig;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.OplusBaseResources;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.OplusPalette;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.drawable.AdaptiveIconDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.OplusAdaptiveIconDrawable;
import android.os.Trace;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.util.OplusRoundRectUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import oplus.content.res.OplusExtraConfiguration;

public class OplusUXIconLoader {
  IOplusResolverUxIconDrawableManager mOplusUxIconDrawableManager = IOplusResolverUxIconDrawableManager.DEFAULT;
  
  private static volatile OplusUXIconLoader sInstance = null;
  
  static {
    sIconConfig = new OplusIconConfig();
    sAppIconResMap = new HashMap<>();
    sComponentIconResMap = new HashMap<>();
  }
  
  private boolean mHasInitConfigArray = false;
  
  private ArrayList<Integer> mCommonStyleConfigArray = new ArrayList<>();
  
  private ArrayList<Integer> mSpecialStyleConfigArray = new ArrayList<>();
  
  private float mDarkModeBrightness = 0.84F;
  
  private static final String TAG = "OplusUXIconLoader";
  
  private static String mIconPackName;
  
  private static boolean mNeedUpdateIconMap;
  
  private static HashMap<String, Integer> sAppIconResMap;
  
  private static HashMap<String, Integer> sComponentIconResMap;
  
  private static OplusIconConfig sIconConfig;
  
  private String[] mCommonStylePathArray;
  
  private String[] mCommonStylePrefixArray;
  
  private LightingColorFilter mDarkModeColorFilter;
  
  private volatile Boolean mIsExpVersion;
  
  private String[] mSpecialStylePathArray;
  
  private String[] mSpecialStylePrefixArray;
  
  public static OplusUXIconLoader getLoader() {
    // Byte code:
    //   0: getstatic android/app/OplusUXIconLoader.sInstance : Landroid/app/OplusUXIconLoader;
    //   3: ifnonnull -> 39
    //   6: ldc android/app/OplusUXIconLoader
    //   8: monitorenter
    //   9: getstatic android/app/OplusUXIconLoader.sInstance : Landroid/app/OplusUXIconLoader;
    //   12: ifnonnull -> 27
    //   15: new android/app/OplusUXIconLoader
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/app/OplusUXIconLoader.sInstance : Landroid/app/OplusUXIconLoader;
    //   27: ldc android/app/OplusUXIconLoader
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/app/OplusUXIconLoader
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/app/OplusUXIconLoader.sInstance : Landroid/app/OplusUXIconLoader;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 0
    //   #112	-> 6
    //   #113	-> 9
    //   #114	-> 15
    //   #116	-> 27
    //   #118	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void updateExtraConfig() {
    if (!sIconConfig.isEmpty())
      sIconConfig.setNeedUpdate(1); 
  }
  
  public void updateDarkModeConfig() {
    if (!sIconConfig.isEmpty())
      sIconConfig.setNeedUpdate(2); 
  }
  
  public void setDarkFilterToDrawable(Drawable paramDrawable) {
    if (paramDrawable != null && sIconConfig.isDarkMode() && sIconConfig.isEnableDarkModeIcon()) {
      paramDrawable.setColorFilter((ColorFilter)this.mDarkModeColorFilter);
    } else if (paramDrawable != null) {
      paramDrawable.setColorFilter(null);
    } 
  }
  
  public Drawable getUxIconDrawable(Resources paramResources, OplusBaseResources paramOplusBaseResources, Drawable paramDrawable, boolean paramBoolean) {
    if (sIconConfig.isEmpty() || sIconConfig.isNeedUpdate()) {
      checkConfig(paramResources, paramOplusBaseResources.getOplusBaseResImpl().getSystemConfiguration().getOplusExtraConfiguration());
      if (sIconConfig.isEmpty())
        return paramDrawable; 
    } 
    if (OplusUxIconConstants.DEBUG_UX_ICON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getUxIconDrawable sIconConfig = ");
      stringBuilder.append(sIconConfig);
      Log.v("OplusUXIconLoader", stringBuilder.toString());
    } 
    int i = this.mCommonStyleConfigArray.indexOf(Integer.valueOf(sIconConfig.getTheme()));
    if (i == 1) {
      float f = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, sIconConfig.getIconSize());
      f = f * 1.0F / paramResources.getDimensionPixelSize(201654712);
      setDarkFilterToDrawable(paramDrawable);
      OplusIconConfig oplusIconConfig = sIconConfig;
      i = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getForegroundSize());
      oplusIconConfig = sIconConfig;
      int j = (int)(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getForegroundSize()) * f);
      return buildAdaptiveIconDrawableForThirdParty(paramResources, null, paramDrawable, i, j);
    } 
    if (paramBoolean) {
      Drawable drawable = getBackgroundDrawable(paramDrawable);
      return buildAdaptiveIconDrawable(paramResources, paramDrawable, drawable, false, false);
    } 
    return buildAdaptiveIconDrawable(paramResources, null, paramDrawable, false, false);
  }
  
  public Drawable loadUxIcon(PackageManager paramPackageManager, String paramString1, String paramString2, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    // Byte code:
    //   0: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   3: ifeq -> 79
    //   6: new java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial <init> : ()V
    //   13: astore #7
    //   15: aload #7
    //   17: ldc_w 'loadIcon packageName = '
    //   20: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload #7
    //   26: aload_2
    //   27: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   30: pop
    //   31: aload #7
    //   33: ldc_w ',applicationInfo =:'
    //   36: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: aload #7
    //   42: aload #5
    //   44: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: aload #7
    //   50: ldc_w '; loadByResolver = '
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload #7
    //   59: iload #6
    //   61: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: ldc 'OplusUXIconLoader'
    //   67: aload #7
    //   69: invokevirtual toString : ()Ljava/lang/String;
    //   72: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   75: pop
    //   76: goto -> 79
    //   79: aload #5
    //   81: ifnonnull -> 98
    //   84: aload_1
    //   85: aload_2
    //   86: iconst_0
    //   87: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   90: astore #7
    //   92: goto -> 102
    //   95: astore_1
    //   96: aconst_null
    //   97: areturn
    //   98: aload #5
    //   100: astore #7
    //   102: aload #7
    //   104: ifnonnull -> 109
    //   107: aconst_null
    //   108: areturn
    //   109: aload_2
    //   110: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   113: ifeq -> 118
    //   116: aconst_null
    //   117: areturn
    //   118: aload_1
    //   119: aload #7
    //   121: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   124: astore #8
    //   126: aload_1
    //   127: checkcast android/app/OplusBaseApplicationPackageManager
    //   130: astore #5
    //   132: aload #5
    //   134: aload #7
    //   136: invokevirtual getOplusBaseResourcesForThemeHelper : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/OplusBaseResources;
    //   139: astore #5
    //   141: aload #8
    //   143: ifnull -> 430
    //   146: aload #5
    //   148: ifnonnull -> 154
    //   151: goto -> 430
    //   154: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   157: invokevirtual isEmpty : ()Z
    //   160: ifne -> 178
    //   163: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   166: invokevirtual isNeedUpdate : ()Z
    //   169: ifeq -> 175
    //   172: goto -> 178
    //   175: goto -> 233
    //   178: ldc_w 'system'
    //   181: aload_2
    //   182: invokevirtual equals : (Ljava/lang/Object;)Z
    //   185: ifeq -> 201
    //   188: aload #5
    //   190: invokevirtual getConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   193: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   196: astore #5
    //   198: goto -> 214
    //   201: aload #5
    //   203: invokevirtual getOplusBaseResImpl : ()Landroid/content/res/OplusBaseResourcesImpl;
    //   206: invokevirtual getSystemConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   209: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   212: astore #5
    //   214: aload_0
    //   215: aload #8
    //   217: aload #5
    //   219: invokespecial checkConfig : (Landroid/content/res/Resources;Loplus/content/res/OplusExtraConfiguration;)V
    //   222: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   225: invokevirtual isEmpty : ()Z
    //   228: ifeq -> 233
    //   231: aconst_null
    //   232: areturn
    //   233: iload #4
    //   235: aload #7
    //   237: getfield icon : I
    //   240: if_icmpeq -> 289
    //   243: ldc_w 'com.android.contacts'
    //   246: aload_2
    //   247: invokevirtual equals : (Ljava/lang/Object;)Z
    //   250: ifeq -> 278
    //   253: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   256: astore #5
    //   258: aload #5
    //   260: invokevirtual getTheme : ()I
    //   263: iconst_5
    //   264: if_icmpeq -> 278
    //   267: ldc_w 'dialer_'
    //   270: astore #5
    //   272: iconst_0
    //   273: istore #9
    //   275: goto -> 297
    //   278: ldc_w ''
    //   281: astore #5
    //   283: iconst_1
    //   284: istore #9
    //   286: goto -> 297
    //   289: ldc_w ''
    //   292: astore #5
    //   294: iconst_0
    //   295: istore #9
    //   297: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   300: invokevirtual getTheme : ()I
    //   303: iconst_5
    //   304: if_icmpne -> 332
    //   307: aload_0
    //   308: aload_1
    //   309: aload #7
    //   311: aload_2
    //   312: aload_3
    //   313: aload #5
    //   315: aload #8
    //   317: iload #4
    //   319: invokespecial getDrawableForIconPackTheme : (Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;
    //   322: astore_3
    //   323: aload_3
    //   324: ifnull -> 329
    //   327: aload_3
    //   328: areturn
    //   329: goto -> 363
    //   332: iload #4
    //   334: ifne -> 339
    //   337: aconst_null
    //   338: areturn
    //   339: iload #9
    //   341: ifne -> 363
    //   344: aload_0
    //   345: aload_2
    //   346: aload #8
    //   348: aload_1
    //   349: aload #5
    //   351: iload #4
    //   353: invokespecial getDrawableForUXIconTheme : (Ljava/lang/String;Landroid/content/res/Resources;Landroid/content/pm/PackageManager;Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    //   356: astore_3
    //   357: aload_3
    //   358: ifnull -> 363
    //   361: aload_3
    //   362: areturn
    //   363: getstatic android/app/OplusUxIconConstants.DEBUG_UX_ICON : Z
    //   366: ifeq -> 417
    //   369: new java/lang/StringBuilder
    //   372: dup
    //   373: invokespecial <init> : ()V
    //   376: astore_3
    //   377: aload_3
    //   378: ldc_w 'loadIcon packageName = '
    //   381: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   384: pop
    //   385: aload_3
    //   386: aload_2
    //   387: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   390: pop
    //   391: aload_3
    //   392: ldc_w ',sIconConfig =:'
    //   395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: pop
    //   399: aload_3
    //   400: getstatic android/app/OplusUXIconLoader.sIconConfig : Landroid/app/OplusIconConfig;
    //   403: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   406: pop
    //   407: ldc 'OplusUXIconLoader'
    //   409: aload_3
    //   410: invokevirtual toString : ()Ljava/lang/String;
    //   413: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   416: pop
    //   417: aload_0
    //   418: aload_2
    //   419: iload #4
    //   421: aload #7
    //   423: aload #8
    //   425: aload_1
    //   426: invokespecial getDrawableForAppIcon : (Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Landroid/content/res/Resources;Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    //   429: areturn
    //   430: aconst_null
    //   431: areturn
    //   432: astore_1
    //   433: aconst_null
    //   434: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #186	-> 0
    //   #187	-> 6
    //   #186	-> 79
    //   #192	-> 79
    //   #194	-> 84
    //   #197	-> 92
    //   #195	-> 95
    //   #196	-> 96
    //   #192	-> 98
    //   #200	-> 102
    //   #201	-> 107
    //   #204	-> 109
    //   #205	-> 116
    //   #209	-> 118
    //   #211	-> 118
    //   #212	-> 126
    //   #213	-> 132
    //   #216	-> 141
    //   #218	-> 141
    //   #223	-> 154
    //   #225	-> 178
    //   #226	-> 188
    //   #228	-> 201
    //   #230	-> 214
    //   #231	-> 222
    //   #241	-> 233
    //   #242	-> 233
    //   #243	-> 233
    //   #246	-> 243
    //   #247	-> 258
    //   #248	-> 267
    //   #250	-> 278
    //   #243	-> 289
    //   #258	-> 297
    //   #262	-> 307
    //   #264	-> 323
    //   #265	-> 327
    //   #267	-> 329
    //   #274	-> 332
    //   #275	-> 337
    //   #276	-> 339
    //   #277	-> 344
    //   #278	-> 357
    //   #279	-> 361
    //   #284	-> 363
    //   #285	-> 369
    //   #287	-> 417
    //   #219	-> 430
    //   #214	-> 432
    //   #215	-> 433
    // Exception table:
    //   from	to	target	type
    //   84	92	95	android/content/pm/PackageManager$NameNotFoundException
    //   118	126	432	android/content/pm/PackageManager$NameNotFoundException
    //   126	132	432	android/content/pm/PackageManager$NameNotFoundException
    //   132	141	432	android/content/pm/PackageManager$NameNotFoundException
  }
  
  private Drawable getDrawableForIconPackTheme(PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, String paramString1, String paramString2, String paramString3, Resources paramResources, int paramInt) {
    boolean bool1;
    String str = mIconPackName;
    if (str == null || str.equals("")) {
      if (OplusUxIconConstants.DEBUG_UX_ICON)
        Log.d("OplusUXIconLoader", "mIconPackName is null"); 
      return null;
    } 
    if (OplusUxIconConstants.DEBUG_UX_ICON)
      Log.d("OplusUXIconLoader", "mIconPackName is not null, try to load from icon pack firstly"); 
    Drawable drawable3 = loadDrawableFromIconPack(mIconPackName, paramString2, paramPackageManager, paramApplicationInfo);
    if (drawable3 != null) {
      setDarkFilterToDrawable(drawable3);
      return buildAdaptiveIconDrawableForIconPack(paramResources, null, drawable3, OplusRoundRectUtil.getInstance().getPath(new Rect(0, 0, 150, 150), 0.0F), true, false);
    } 
    boolean bool = OplusUxIconAppCheckUtils.isPresetApp(paramString1);
    if (bool) {
      bool1 = isOffUxIcon(paramPackageManager, paramString1);
    } else {
      bool1 = false;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append(paramString3);
    stringBuilder1.append(getRectFgPrefix());
    Drawable drawable2 = findAppDrawable(paramString1, stringBuilder1.toString(), paramResources, false, bool, bool1);
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(paramString3);
    stringBuilder2.append(getRectBgPrefix());
    Drawable drawable4 = findAppDrawable(paramString1, stringBuilder2.toString(), paramResources, false, bool, bool1);
    if (drawable2 != null)
      return buildAdaptiveIconDrawable(paramResources, drawable2, drawable4, true, false); 
    Drawable drawable1 = paramPackageManager.getDrawable(paramString1, paramInt, paramApplicationInfo);
    if (drawable1 == null)
      return null; 
    paramInt = (int)((paramResources.getDisplayMetrics()).density * 40.0F + 0.5F);
    if (drawable1 instanceof AdaptiveIconDrawable) {
      Drawable drawable = ((AdaptiveIconDrawable)drawable1).getForeground();
      drawable1 = ((AdaptiveIconDrawable)drawable1).getBackground();
      return buildAdaptiveIconDrawable(paramResources, drawable, drawable1, false, true);
    } 
    if (drawable1.getIntrinsicWidth() < paramInt || hasTransparentPixels(drawable1)) {
      setDarkFilterToDrawable(drawable1);
      Drawable drawable = getDefaultBackgroundDrawable();
      int i = (int)((paramResources.getDisplayMetrics()).density * 34.0F + 0.5F);
      OplusIconConfig oplusIconConfig = sIconConfig;
      paramInt = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getIconSize());
      return buildAdaptiveIconDrawableForThirdParty(paramResources, drawable1, drawable, i, paramInt);
    } 
    return buildAdaptiveIconDrawable(paramResources, null, drawable1, false, false);
  }
  
  private Drawable getDrawableForUXIconTheme(String paramString1, Resources paramResources, PackageManager paramPackageManager, String paramString2, int paramInt) {
    Drawable drawable1;
    boolean bool1;
    Drawable drawable3;
    int i = this.mCommonStyleConfigArray.indexOf(Integer.valueOf(sIconConfig.getTheme()));
    paramInt = this.mCommonStyleConfigArray.size();
    PackageManager packageManager = null;
    Drawable drawable2 = null;
    boolean bool = OplusUxIconAppCheckUtils.isPresetApp(paramString1);
    if (bool) {
      bool1 = isOffUxIcon(paramPackageManager, paramString1);
    } else {
      bool1 = false;
    } 
    if (i >= 0 && i < this.mCommonStyleConfigArray.size()) {
      if (i == 2) {
        if (bool) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString2);
          stringBuilder.append(getCommonStylePrefixExceptRect(i));
          String str = stringBuilder.toString();
          drawable1 = findAppDrawable(paramString1, str, paramResources, false, bool, bool1);
          Drawable drawable = drawable1;
          drawable3 = drawable2;
          if (drawable1 != null) {
            setDarkFilterToDrawable(drawable1);
            return buildAdaptiveIconDrawable(paramResources, null, drawable1, true, false);
          } 
        } else {
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append(paramString2);
          stringBuilder2.append(getCommonStylePrefixExceptRect(i));
          String str2 = stringBuilder2.toString();
          Drawable drawable5 = findAppDrawable((String)drawable1, str2, paramResources, false, false, bool1);
          if (drawable5 != null)
            return buildAdaptiveIconDrawable(paramResources, null, drawable5, true, false); 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(paramString2);
          stringBuilder1.append(getRectFgPrefix());
          String str1 = stringBuilder1.toString();
          Drawable drawable4 = findAppDrawable((String)drawable1, str1, paramResources, false, false, bool1);
          StringBuilder stringBuilder3 = new StringBuilder();
          stringBuilder3.append(paramString2);
          stringBuilder3.append(getRectBgPrefix());
          paramString2 = stringBuilder3.toString();
          drawable3 = findAppDrawable((String)drawable1, paramString2, paramResources, false, false, bool1);
        } 
      } else if (i == 0 || i == paramInt - 1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramString2);
        stringBuilder1.append(getRectFgPrefix());
        Drawable drawable = findAppDrawable((String)drawable1, stringBuilder1.toString(), paramResources, false, bool, bool1);
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramString2);
        stringBuilder2.append(getRectBgPrefix());
        drawable3 = findAppDrawable((String)drawable1, stringBuilder2.toString(), paramResources, false, bool, bool1);
      } else {
        paramPackageManager = packageManager;
        drawable3 = drawable2;
        if (i == 1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString2);
          stringBuilder.append(getCommonStylePrefixExceptRect(i));
          String str = stringBuilder.toString();
          drawable1 = findAppDrawable((String)drawable1, str, paramResources, false, bool, bool1);
          Drawable drawable = drawable1;
          drawable3 = drawable2;
          if (drawable1 != null) {
            setDarkFilterToDrawable(drawable1);
            return buildAdaptiveIconDrawable(paramResources, null, drawable1, true, false);
          } 
        } 
      } 
    } else {
      paramInt = this.mSpecialStyleConfigArray.indexOf(Integer.valueOf(sIconConfig.getTheme()));
      if (paramInt >= 0 && paramInt < this.mSpecialStylePrefixArray.length) {
        if (bool) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(paramString2);
          stringBuilder.append(getSpecialStylePrefix(paramInt));
          String str = stringBuilder.toString();
          drawable1 = findAppDrawable((String)drawable1, str, paramResources, true, true, bool1);
          Drawable drawable = drawable1;
          drawable3 = drawable2;
          if (drawable1 != null) {
            setDarkFilterToDrawable(drawable1);
            return drawable1;
          } 
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append(paramString2);
          stringBuilder1.append(getRectFgPrefix());
          String str = stringBuilder1.toString();
          Drawable drawable = findAppDrawable((String)drawable1, str, paramResources, true, false, bool1);
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append(paramString2);
          stringBuilder2.append(getRectBgPrefix());
          paramString2 = stringBuilder2.toString();
          drawable3 = findAppDrawable((String)drawable1, paramString2, paramResources, true, false, bool1);
        } 
      } else {
        drawable3 = drawable2;
        paramPackageManager = packageManager;
      } 
    } 
    if (paramPackageManager != null)
      return buildAdaptiveIconDrawable(paramResources, (Drawable)paramPackageManager, drawable3, true, false); 
    return null;
  }
  
  private Drawable getDrawableForAppIcon(String paramString, int paramInt, ApplicationInfo paramApplicationInfo, Resources paramResources, PackageManager paramPackageManager) {
    OplusIconConfig oplusIconConfig;
    int i = this.mCommonStyleConfigArray.indexOf(Integer.valueOf(sIconConfig.getTheme()));
    Drawable drawable2 = this.mOplusUxIconDrawableManager.getDrawable(paramPackageManager, paramString, paramInt, paramApplicationInfo);
    if (drawable2 == null)
      return null; 
    if (OplusUxIconAppCheckUtils.isDeskActivity(paramString)) {
      if (i == 1)
        return buildAdaptiveIconDrawable(paramResources, drawable2, null, false, false); 
      return buildAdaptiveIconDrawable(paramResources, null, drawable2, false, false);
    } 
    paramInt = (int)((paramResources.getDisplayMetrics()).density * 40.0F + 0.5F);
    if (i == 1) {
      float f = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, sIconConfig.getIconSize());
      f = f * 1.0F / paramResources.getDimensionPixelSize(201654712);
      if (hasTransparentPixels(drawable2)) {
        setDarkFilterToDrawable(drawable2);
        Drawable drawable = getDefaultBackgroundDrawable();
        paramInt = (int)(((paramResources.getDisplayMetrics()).density * 34.0F + 0.5F) * 1.25F);
        OplusIconConfig oplusIconConfig2 = sIconConfig;
        i = (int)(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig2.getForegroundSize()) * f);
        return buildAdaptiveIconDrawableForThirdParty(paramResources, drawable2, drawable, paramInt, i);
      } 
      setDarkFilterToDrawable(drawable2);
      OplusIconConfig oplusIconConfig1 = sIconConfig;
      i = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig1.getForegroundSize());
      oplusIconConfig1 = sIconConfig;
      paramInt = (int)(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig1.getForegroundSize()) * f);
      return buildAdaptiveIconDrawableForThirdParty(paramResources, null, drawable2, i, paramInt);
    } 
    boolean bool = false;
    Drawable drawable1 = drawable2;
    if (drawable1 instanceof AdaptiveIconDrawable) {
      Drawable drawable = ((AdaptiveIconDrawable)drawable1).getForeground();
      drawable1 = ((AdaptiveIconDrawable)drawable1).getBackground();
      bool = true;
    } else {
      if (i == 2 || i == 0) {
        if (drawable2.getIntrinsicWidth() >= paramInt) {
          drawable1 = drawable2;
          if (hasTransparentPixels(drawable1)) {
            setDarkFilterToDrawable(drawable2);
            drawable1 = getDefaultBackgroundDrawable();
            paramInt = (int)((paramResources.getDisplayMetrics()).density * 34.0F + 0.5F);
            OplusIconConfig oplusIconConfig1 = sIconConfig;
            i = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig1.getIconSize());
          } 
          bool = false;
          paramPackageManager = null;
          return buildAdaptiveIconDrawable(paramResources, (Drawable)paramPackageManager, drawable1, false, bool);
        } 
      } else {
        if (drawable1.getIntrinsicWidth() < paramInt || hasTransparentPixels(drawable1)) {
          setDarkFilterToDrawable(drawable1);
          drawable2 = getDefaultBackgroundDrawable();
          float f = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, sIconConfig.getForegroundSize());
          f = f * 1.0F / paramResources.getDimensionPixelSize(201654712);
          i = (int)(((paramResources.getDisplayMetrics()).density * 34.0F + 0.5F) * f);
          OplusIconConfig oplusIconConfig1 = sIconConfig;
          paramInt = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig1.getIconSize());
          return buildAdaptiveIconDrawableForThirdParty(paramResources, drawable1, drawable2, i, paramInt);
        } 
        paramPackageManager = null;
        return buildAdaptiveIconDrawable(paramResources, (Drawable)paramPackageManager, drawable1, false, bool);
      } 
      setDarkFilterToDrawable(drawable2);
      drawable1 = getDefaultBackgroundDrawable();
      paramInt = (int)((paramResources.getDisplayMetrics()).density * 34.0F + 0.5F);
      oplusIconConfig = sIconConfig;
      i = OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getIconSize());
    } 
    return buildAdaptiveIconDrawable(paramResources, (Drawable)oplusIconConfig, drawable1, false, bool);
  }
  
  private Drawable getDefaultBackgroundDrawable() {
    ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#FFFBFBFB"));
    setDarkFilterToDrawable((Drawable)colorDrawable);
    return (Drawable)colorDrawable;
  }
  
  private Drawable loadDrawableFromIconPack(String paramString1, String paramString2, PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo) {
    String str1 = "drawable", str2 = "";
    try {
      if (OplusUxIconConstants.DEBUG_UX_ICON) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("loadDrawableFromIconPack from ");
        stringBuilder.append(paramString1);
        Log.d("OplusUXIconLoader", stringBuilder.toString());
      } 
      String str = null;
      try {
        Resources resources = paramPackageManager.getResourcesForApplication(paramString1);
        OplusIconPackMappingHelper.parsePackMapping();
        boolean bool = mNeedUpdateIconMap;
        if (bool) {
          sAppIconResMap.clear();
          sComponentIconResMap.clear();
          int j = resources.getIdentifier("appfilter", "xml", paramString1);
          XmlResourceParser xmlResourceParser = resources.getXml(j);
          int k = xmlResourceParser.getEventType();
          int m = 0;
          String str5 = str1, str3 = str2, str4 = str;
          int n = k;
          j = m;
          if (OplusUxIconConstants.DEBUG_UX_ICON) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("start parcing xml : ");
            stringBuilder.append(System.currentTimeMillis());
            Log.d("OplusUXIconLoader", stringBuilder.toString());
            j = m;
            n = k;
            str4 = str;
            str3 = str2;
            str5 = str1;
          } 
          while (n != 1) {
            if (n == 2) {
              if (xmlResourceParser.getName().equals("item")) {
                m = -1;
                str = str3;
                str1 = str3;
                for (n = 0, str2 = str3; n < xmlResourceParser.getAttributeCount(); n++, j = k, str = str3, str1 = str10) {
                  String str8 = xmlResourceParser.getAttributeName(n);
                  String str9 = xmlResourceParser.getAttributeValue(n);
                  if (str8.equals(str5)) {
                    int i1 = resources.getIdentifier(str9, str5, paramString1);
                    k = j;
                    m = i1;
                    if (i1 > 0) {
                      k = j + 1;
                      m = i1;
                    } 
                  } else {
                    k = j;
                  } 
                  str3 = str;
                  String str10 = str1;
                  if (str8.equals("component")) {
                    j = str9.indexOf("{");
                    int i1 = str9.indexOf("/");
                    if (j > 0 && i1 > j) {
                      str3 = str9.substring(j + 1, i1);
                    } else {
                      str3 = str9;
                    } 
                    i1 = str9.indexOf("}");
                    if (j > 0 && i1 > j) {
                      str10 = str9.substring(j + 1, i1);
                    } else {
                      str10 = str1;
                    } 
                  } 
                } 
                String str7 = str5, str6 = str4;
                k = j;
                str5 = str2;
                str4 = str6;
                str3 = str7;
                if (m > 0) {
                  sAppIconResMap.put(str, Integer.valueOf(m));
                  sComponentIconResMap.put(str1, Integer.valueOf(m));
                  k = j;
                  str5 = str2;
                  str4 = str6;
                  str3 = str7;
                } 
              } else {
                str2 = str5;
                k = j;
                str5 = str3;
                str3 = str2;
              } 
            } else {
              str2 = str5;
              str5 = str3;
              str3 = str2;
              k = j;
            } 
            n = xmlResourceParser.next();
            str2 = str5;
            str5 = str3;
            str3 = str2;
            j = k;
          } 
          mNeedUpdateIconMap = false;
          paramString1 = str4;
          if (OplusUxIconConstants.DEBUG_UX_ICON) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("end parcing xml : ");
            stringBuilder.append(System.currentTimeMillis());
            stringBuilder.append(", item count = ");
            stringBuilder.append(j);
            Log.d("OplusUXIconLoader", stringBuilder.toString());
            String str6 = str4;
          } 
        } else {
          paramString1 = null;
        } 
        int i = 0;
        try {
          String str3 = paramApplicationInfo.packageName;
          if (!TextUtils.isEmpty(paramString2)) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append(str3);
            stringBuilder.append("/");
            try {
              Drawable drawable;
              stringBuilder.append(paramString2);
              String str4 = stringBuilder.toString();
              Integer integer2 = sComponentIconResMap.get(str4);
              Integer integer1 = integer2;
              if (integer2 == null) {
                String str5 = OplusIconPackMappingHelper.getMappingComponent(str4, str3);
                integer2 = sComponentIconResMap.get(str5);
                integer1 = integer2;
                if (integer2 == null)
                  integer1 = sAppIconResMap.get(str3); 
              } 
              if (integer1 != null)
                i = integer1.intValue(); 
              if (i > 0)
                drawable = resources.getDrawable(i); 
              if (drawable != null) {
                if (OplusUxIconConstants.DEBUG_UX_ICON) {
                  StringBuilder stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("OplusUXIconLoader get drawable ");
                  stringBuilder1.append((String)null);
                  stringBuilder1.append(" for package ");
                  stringBuilder1.append(str3);
                  Log.d("OplusUXIconLoader", stringBuilder1.toString());
                } 
                return drawable;
              } 
            } catch (Exception exception) {
              Log.e("OplusUXIconLoader", exception.getMessage(), exception);
            } 
          } else {
            Drawable drawable;
            if (sAppIconResMap.get(str3) != null) {
              i = ((Integer)sAppIconResMap.get(str3)).intValue();
              drawable = resources.getDrawable(i);
            } 
            if (drawable != null) {
              if (OplusUxIconConstants.DEBUG_UX_ICON) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("OplusUXIconLoader get drawable ");
                stringBuilder.append((String)null);
                stringBuilder.append(" for package ");
                stringBuilder.append(str3);
                Log.d("OplusUXIconLoader", stringBuilder.toString());
              } 
              return drawable;
            } 
          } 
        } catch (Exception exception) {
          Log.e("OplusUXIconLoader", exception.getMessage(), exception);
        } 
      } catch (Exception exception) {
        Log.e("OplusUXIconLoader", exception.getMessage(), exception);
      } 
    } catch (Exception exception) {
      Log.e("OplusUXIconLoader", exception.getMessage(), exception);
    } 
    return null;
  }
  
  private void checkConfig(Resources paramResources, OplusExtraConfiguration paramOplusExtraConfiguration) {
    if (!this.mHasInitConfigArray) {
      boolean bool;
      this.mHasInitConfigArray = true;
      initConfigArray(paramResources);
      int j = getDarkModeColorInCurrentContrast(this.mDarkModeBrightness);
      this.mDarkModeColorFilter = new LightingColorFilter(j, 0);
      if (((paramResources.getConfiguration()).uiMode & 0x30) == 32) {
        bool = true;
      } else {
        bool = false;
      } 
      sIconConfig.setIsDarkMode(bool);
      OplusUxIconConfigParser.parseConfig(sIconConfig, paramOplusExtraConfiguration, paramResources, this.mCommonStyleConfigArray, this.mSpecialStyleConfigArray, this.mCommonStylePathArray, this.mSpecialStylePathArray);
    } 
    int i = sIconConfig.getNeedUpdateConfig();
    if (i != 1) {
      if (i == 2) {
        boolean bool;
        if (((paramResources.getConfiguration()).uiMode & 0x30) == 32) {
          bool = true;
        } else {
          bool = false;
        } 
        sIconConfig.setIsDarkMode(bool);
      } 
    } else {
      OplusUxIconConfigParser.parseConfig(sIconConfig, paramOplusExtraConfiguration, paramResources, this.mCommonStyleConfigArray, this.mSpecialStyleConfigArray, this.mCommonStylePathArray, this.mSpecialStylePathArray);
    } 
    if (!paramOplusExtraConfiguration.mIconPackName.equals(mIconPackName)) {
      if (OplusUxIconConstants.DEBUG_UX_ICON)
        Log.d("OplusUXIconLoader", "checkConfig set mNeedUpdateIconMap true"); 
      mNeedUpdateIconMap = true;
      mIconPackName = paramOplusExtraConfiguration.mIconPackName;
    } 
    sIconConfig.setNeedUpdate(0);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("checkConfig sIconConfig =:");
    stringBuilder.append(sIconConfig);
    Log.v("OplusUXIconLoader", stringBuilder.toString());
  }
  
  private boolean isExpVersion() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mIsExpVersion : Ljava/lang/Boolean;
    //   4: ifnonnull -> 45
    //   7: ldc android/app/OplusUXIconLoader
    //   9: monitorenter
    //   10: aload_0
    //   11: getfield mIsExpVersion : Ljava/lang/Boolean;
    //   14: ifnonnull -> 33
    //   17: aload_0
    //   18: invokespecial initIsExpVersionValues : ()V
    //   21: aload_0
    //   22: getfield mIsExpVersion : Ljava/lang/Boolean;
    //   25: ifnonnull -> 33
    //   28: ldc android/app/OplusUXIconLoader
    //   30: monitorexit
    //   31: iconst_0
    //   32: ireturn
    //   33: ldc android/app/OplusUXIconLoader
    //   35: monitorexit
    //   36: goto -> 45
    //   39: astore_1
    //   40: ldc android/app/OplusUXIconLoader
    //   42: monitorexit
    //   43: aload_1
    //   44: athrow
    //   45: aload_0
    //   46: getfield mIsExpVersion : Ljava/lang/Boolean;
    //   49: invokevirtual booleanValue : ()Z
    //   52: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #690	-> 0
    //   #691	-> 7
    //   #692	-> 10
    //   #693	-> 17
    //   #694	-> 21
    //   #695	-> 28
    //   #698	-> 33
    //   #700	-> 45
    // Exception table:
    //   from	to	target	type
    //   10	17	39	finally
    //   17	21	39	finally
    //   21	28	39	finally
    //   28	31	39	finally
    //   33	36	39	finally
    //   40	43	39	finally
  }
  
  private void initIsExpVersionValues() {
    try {
      OplusFeatureConfigManager oplusFeatureConfigManager = OplusFeatureConfigManager.getInstacne();
      boolean bool = oplusFeatureConfigManager.hasFeature("oplus.software.uxicon_exp");
      this.mIsExpVersion = Boolean.valueOf(bool);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteException --> ");
      stringBuilder.append(exception.getMessage());
      Log.e("OplusUXIconLoader", stringBuilder.toString());
    } 
    if (OplusUxIconConstants.DEBUG_UX_ICON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("mIsExpVersion = ");
      stringBuilder.append(this.mIsExpVersion);
      Log.v("OplusUXIconLoader", stringBuilder.toString());
    } 
  }
  
  private void initConfigArray(Resources paramResources) {
    int[] arrayOfInt = paramResources.getIntArray(201785352);
    byte b;
    for (b = 0; b < arrayOfInt.length; b++)
      this.mCommonStyleConfigArray.add(Integer.valueOf(arrayOfInt[b])); 
    arrayOfInt = paramResources.getIntArray(201785356);
    for (b = 0; b < arrayOfInt.length; b++)
      this.mSpecialStyleConfigArray.add(Integer.valueOf(arrayOfInt[b])); 
    this.mCommonStylePrefixArray = paramResources.getStringArray(201785355);
    this.mSpecialStylePrefixArray = paramResources.getStringArray(201785359);
    if (isExpVersion()) {
      this.mCommonStylePathArray = paramResources.getStringArray(201785349);
    } else {
      this.mCommonStylePathArray = paramResources.getStringArray(201785354);
    } 
    this.mSpecialStylePathArray = paramResources.getStringArray(201785358);
  }
  
  private boolean isOffUxIcon(PackageManager paramPackageManager, String paramString) {
    if (paramPackageManager == null)
      return false; 
    PackageManager.NameNotFoundException nameNotFoundException2 = null;
    try {
      ApplicationInfo applicationInfo = paramPackageManager.getApplicationInfo(paramString, 128);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException1) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("is off uxicon error: ");
      stringBuilder.append(nameNotFoundException1.getMessage());
      Log.e("OplusUXIconLoader", stringBuilder.toString());
      nameNotFoundException1 = nameNotFoundException2;
    } 
    if (nameNotFoundException1 != null && ((ApplicationInfo)nameNotFoundException1).metaData != null) {
      Boolean bool = (Boolean)((ApplicationInfo)nameNotFoundException1).metaData.get("com.coloros.off_uxicon");
      if (bool != null && bool.booleanValue())
        return true; 
      return false;
    } 
    return false;
  }
  
  private String buildIconPath(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder(paramString1);
    stringBuilder.append(paramString2);
    stringBuilder.append("/");
    stringBuilder.append(paramString3);
    stringBuilder = stringBuilder.append(".png");
    paramString3 = stringBuilder.toString();
    if (OplusUxIconConstants.DEBUG_UX_ICON) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("loadUXIconByPath iconPath =:");
      stringBuilder.append(paramString2);
      stringBuilder.append(",iconPath =:");
      stringBuilder.append(paramString3);
      Log.v("OplusUXIconLoader", stringBuilder.toString());
    } 
    return paramString3;
  }
  
  Drawable findAppDrawable(String paramString1, String paramString2, Resources paramResources, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    Trace.traceBegin(8192L, "#UxIcon.getDrawable.findAppDrawable");
    Drawable drawable = null;
    if (paramBoolean2) {
      Drawable drawable1 = loadUXIconByPath(buildIconPath("/data/oppo/uxicons/", paramString1, paramString2), paramResources);
      drawable = drawable1;
      if (drawable1 == null) {
        drawable = drawable1;
        if (!paramBoolean3) {
          paramResources.getDisplayMetrics();
          String str2 = OplusUxIconConstants.IconLoader.getDensityName(DisplayMetrics.DENSITY_DEVICE_STABLE);
          StringBuilder stringBuilder = new StringBuilder();
          if ((new File(OplusUxIconConstants.IconLoader.MY_COUNTRY_DEFAULT_THEME_FILE_PATH)).exists()) {
            str1 = OplusUxIconConstants.IconLoader.MY_COUNTRY_DEFAULT_THEME_FILE_PATH;
          } else {
            str1 = OplusUxIconConstants.IconLoader.MY_CARRIER_DEFAULT_THEME_FILE_PATH;
          } 
          stringBuilder.append(str1);
          stringBuilder.append(str2);
          stringBuilder.append(File.separator);
          String str1 = stringBuilder.toString();
          if (paramBoolean1)
            str1 = OplusUxIconConstants.IconLoader.BASE_PRODUCT_DEFAULT_THEME_FILE_PATH; 
          Drawable drawable2 = loadUXIconByPath(buildIconPath(str1, paramString1, paramString2), paramResources);
        } 
      } 
    } else {
      paramBoolean1 = sIconConfig.isArtPlusOn();
      if (paramBoolean1) {
        Drawable drawable1 = loadUXIconByPath(buildIconPath("/data/oppo/uxicons/", paramString1, paramString2), paramResources);
        drawable = drawable1;
        if (drawable1 == null) {
          paramResources.getDisplayMetrics();
          String str2 = OplusUxIconConstants.IconLoader.getDensityName(DisplayMetrics.DENSITY_DEVICE_STABLE);
          StringBuilder stringBuilder = new StringBuilder();
          if ((new File(OplusUxIconConstants.IconLoader.MY_COUNTRY_DEFAULT_THEME_FILE_PATH)).exists()) {
            str1 = OplusUxIconConstants.IconLoader.MY_COUNTRY_DEFAULT_THEME_FILE_PATH;
          } else {
            str1 = OplusUxIconConstants.IconLoader.MY_CARRIER_DEFAULT_THEME_FILE_PATH;
          } 
          stringBuilder.append(str1);
          stringBuilder.append(str2);
          stringBuilder.append(File.separator);
          String str1 = stringBuilder.toString();
          drawable = loadUXIconByPath(buildIconPath(str1, paramString1, paramString2), paramResources);
        } 
      } 
    } 
    Trace.traceEnd(8192L);
    return drawable;
  }
  
  private Drawable buildAdaptiveIconDrawable(Resources paramResources, Drawable paramDrawable1, Drawable paramDrawable2, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramDrawable1 != null || paramDrawable2 != null) {
      setDarkFilterToDrawable(paramDrawable1);
      setDarkFilterToDrawable(paramDrawable2);
      CustomAdaptiveIconConfig.Builder builder2 = new CustomAdaptiveIconConfig.Builder(paramResources);
      OplusIconConfig oplusIconConfig = sIconConfig;
      builder2 = builder2.setCustomIconSize(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getIconSize()));
      oplusIconConfig = sIconConfig;
      CustomAdaptiveIconConfig.Builder builder1 = builder2.setCustomIconFgSize(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getForegroundSize()));
      oplusIconConfig = sIconConfig;
      builder1 = builder1.setCustomMask(new Path(oplusIconConfig.getShapePath()));
      builder1 = builder1.setIsPlatformDrawable(paramBoolean1);
      builder1 = builder1.setIsAdaptiveIconDrawable(paramBoolean2);
      CustomAdaptiveIconConfig customAdaptiveIconConfig = builder1.create();
      OplusAdaptiveIconDrawable oplusAdaptiveIconDrawable = new OplusAdaptiveIconDrawable(paramDrawable2, paramDrawable1, customAdaptiveIconConfig);
      if (OplusUxIconConstants.DEBUG_UX_ICON) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buildAdaptiveIconDrawable foreground =:");
        stringBuilder.append(paramDrawable1);
        stringBuilder.append(",background =:");
        stringBuilder.append(paramDrawable2);
        stringBuilder.append(",config =:");
        stringBuilder.append(customAdaptiveIconConfig);
        Log.v("OplusUXIconLoader", stringBuilder.toString());
      } 
      return (Drawable)oplusAdaptiveIconDrawable;
    } 
    return null;
  }
  
  private Drawable buildAdaptiveIconDrawableForThirdParty(Resources paramResources, Drawable paramDrawable1, Drawable paramDrawable2, int paramInt1, int paramInt2) {
    if (paramDrawable1 != null || paramDrawable2 != null) {
      CustomAdaptiveIconConfig.Builder builder2 = new CustomAdaptiveIconConfig.Builder(paramResources);
      builder2 = builder2.setCustomIconSize(paramInt2);
      CustomAdaptiveIconConfig.Builder builder3 = builder2.setCustomIconFgSize(paramInt1);
      OplusIconConfig oplusIconConfig = sIconConfig;
      CustomAdaptiveIconConfig.Builder builder1 = builder3.setCustomMask(new Path(oplusIconConfig.getShapePath()));
      builder1 = builder1.setIsPlatformDrawable(false);
      builder1 = builder1.setIsAdaptiveIconDrawable(false);
      CustomAdaptiveIconConfig customAdaptiveIconConfig = builder1.create();
      OplusAdaptiveIconDrawable oplusAdaptiveIconDrawable = new OplusAdaptiveIconDrawable(paramDrawable2, paramDrawable1, customAdaptiveIconConfig);
      if (OplusUxIconConstants.DEBUG_UX_ICON) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buildAdaptiveIconDrawable foreground =:");
        stringBuilder.append(paramDrawable1);
        stringBuilder.append(",background =:");
        stringBuilder.append(paramDrawable2);
        stringBuilder.append(",config =:");
        stringBuilder.append(customAdaptiveIconConfig);
        Log.v("OplusUXIconLoader", stringBuilder.toString());
      } 
      return (Drawable)oplusAdaptiveIconDrawable;
    } 
    return null;
  }
  
  private Drawable buildAdaptiveIconDrawableForIconPack(Resources paramResources, Drawable paramDrawable1, Drawable paramDrawable2, Path paramPath, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramDrawable1 != null || paramDrawable2 != null) {
      CustomAdaptiveIconConfig.Builder builder2 = new CustomAdaptiveIconConfig.Builder(paramResources);
      OplusIconConfig oplusIconConfig = sIconConfig;
      builder2 = builder2.setCustomIconSize(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getIconSize()));
      oplusIconConfig = sIconConfig;
      CustomAdaptiveIconConfig.Builder builder1 = builder2.setCustomIconFgSize(OplusUxIconConfigParser.getPxFromIconConfigDp(paramResources, oplusIconConfig.getForegroundSize()));
      builder1 = builder1.setCustomMask(paramPath);
      builder1 = builder1.setIsPlatformDrawable(paramBoolean1);
      builder1 = builder1.setIsAdaptiveIconDrawable(paramBoolean2);
      CustomAdaptiveIconConfig customAdaptiveIconConfig = builder1.create();
      OplusAdaptiveIconDrawable oplusAdaptiveIconDrawable = new OplusAdaptiveIconDrawable(paramDrawable2, paramDrawable1, customAdaptiveIconConfig);
      if (OplusUxIconConstants.DEBUG_UX_ICON) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("buildAdaptiveIconDrawable foreground =:");
        stringBuilder.append(paramDrawable1);
        stringBuilder.append(",background =:");
        stringBuilder.append(paramDrawable2);
        stringBuilder.append(",config =:");
        stringBuilder.append(customAdaptiveIconConfig);
        Log.v("OplusUXIconLoader", stringBuilder.toString());
      } 
      return (Drawable)oplusAdaptiveIconDrawable;
    } 
    return null;
  }
  
  private Drawable loadUXIconByPath(String paramString, Resources paramResources) {
    if (TextUtils.isEmpty(paramString)) {
      if (OplusUxIconConstants.DEBUG_UX_ICON)
        Log.v("OplusUXIconLoader", "loadUXIconByPath isEmpty(path)."); 
      return null;
    } 
    if (OplusUxIconConstants.DEBUG_UX_ICON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("loadUXIconByPath path = ");
      stringBuilder.append(paramString);
      Log.v("OplusUXIconLoader", stringBuilder.toString());
    } 
    return getDrawableFromPath(paramString, paramResources);
  }
  
  private Drawable getBackgroundDrawable(Drawable paramDrawable) {
    ColorDrawable colorDrawable;
    Bitmap bitmap = getBitmapFromDrawable(paramDrawable);
    int i = OplusPalette.from(bitmap).generateEdageWithStep(10, 20).getTransMaxColor(-1);
    if (i == -1) {
      paramDrawable = getDefaultBackgroundDrawable();
    } else {
      colorDrawable = new ColorDrawable(i);
      bitmap.recycle();
    } 
    return (Drawable)colorDrawable;
  }
  
  private Bitmap getBitmapFromDrawable(Drawable paramDrawable) {
    if (paramDrawable instanceof BitmapDrawable)
      return ((BitmapDrawable)paramDrawable).getBitmap(); 
    int i = paramDrawable.getIntrinsicWidth();
    int j = paramDrawable.getIntrinsicHeight();
    Bitmap.Config config = Bitmap.Config.ARGB_8888;
    Bitmap bitmap = Bitmap.createBitmap(i, j, config);
    Canvas canvas = new Canvas(bitmap);
    paramDrawable.setBounds(0, 0, i, j);
    paramDrawable.draw(canvas);
    return bitmap;
  }
  
  private String getSpecialStylePrefix(int paramInt) {
    String[] arrayOfString = this.mSpecialStylePrefixArray;
    if (paramInt < arrayOfString.length)
      return arrayOfString[paramInt]; 
    return "";
  }
  
  private String getCommonStylePrefixExceptRect(int paramInt) {
    String[] arrayOfString = this.mCommonStylePrefixArray;
    if (paramInt + 1 < arrayOfString.length)
      return arrayOfString[paramInt + 1]; 
    return "";
  }
  
  private String getRectFgPrefix() {
    return this.mCommonStylePrefixArray[0];
  }
  
  private String getRectBgPrefix() {
    return this.mCommonStylePrefixArray[1];
  }
  
  private Drawable getDrawableFromPath(String paramString, Resources paramResources) {
    if (paramString == null || paramResources == null)
      return null; 
    Trace.traceBegin(8192L, paramString);
    try {
      FileInputStream fileInputStream = new FileInputStream();
      this(paramString);
    } catch (IOException iOException) {
      return null;
    } finally {
      Trace.traceEnd(8192L);
    } 
  }
  
  private boolean hasTransparentPixels(Drawable paramDrawable) {
    boolean bool;
    int n;
    Bitmap bitmap = getBitmapFromDrawable(paramDrawable);
    int i = 0;
    int j = (int)Math.ceil((bitmap.getWidth() * 1.0F / 4.0F));
    int k = (int)Math.ceil((bitmap.getHeight() * 1.0F / 4.0F));
    int m = j;
    while (true) {
      int i1 = 0;
      bool = true;
      n = i;
      try {
        if (m < bitmap.getWidth()) {
          for (byte b = 1; b < 4; b++, i = n, i1 = i2) {
            int i2 = i1;
            n = i;
            if (Color.alpha(bitmap.getPixel(m, b)) < 220)
              i2 = i1 + 1; 
            n = i;
            if (b == 3) {
              n = i;
              if (i2 > 1)
                n = i + 1; 
            } 
          } 
          m += j;
          continue;
        } 
        m = j;
        while (true) {
          i1 = 0;
          n = i;
          if (m < bitmap.getWidth()) {
            n = i;
            int i2 = bitmap.getHeight() - 2;
            for (;; m += j) {
              n = i;
              if (i2 > bitmap.getHeight() - 4 - 1) {
                int i3 = i1;
                n = i;
                if (Color.alpha(bitmap.getPixel(m, i2)) < 220)
                  i3 = i1 + 1; 
                i1 = i;
                n = i;
                if (i2 == bitmap.getHeight() - 4) {
                  i1 = i;
                  if (i3 > 1)
                    i1 = i + 1; 
                } 
                i2--;
                i = i1;
                i1 = i3;
                continue;
              } 
            } 
          } 
          break;
        } 
        j = k;
        while (true) {
          int i2 = 0;
          n = i;
          if (j < bitmap.getHeight()) {
            for (m = 1; m < 4; m++, i = n, i2 = i3) {
              int i3 = i2;
              n = i;
              if (Color.alpha(bitmap.getPixel(m, j)) < 220)
                i3 = i2 + 1; 
              n = i;
              if (m == 3) {
                n = i;
                if (i3 > 1)
                  n = i + 1; 
              } 
            } 
            j += k;
            continue;
          } 
          break;
        } 
        j = k;
        while (true) {
          int i2 = 0;
          n = i;
          if (j < bitmap.getHeight()) {
            n = i;
            m = bitmap.getWidth() - 2;
            for (;; j += k) {
              n = i;
              if (m > bitmap.getWidth() - 4 - 1) {
                int i3 = i2;
                n = i;
                if (Color.alpha(bitmap.getPixel(m, j)) < 220)
                  i3 = i2 + 1; 
                n = i;
                i2 = bitmap.getWidth();
                n = i;
                if (m == i2 - 4) {
                  n = i;
                  if (i3 > 1)
                    n = i + 1; 
                } 
                m--;
                i = n;
                i2 = i3;
                continue;
              } 
            } 
          } 
          break;
        } 
        n = i;
      } catch (IllegalArgumentException illegalArgumentException) {}
      break;
    } 
    if (n < 6)
      bool = false; 
    return bool;
  }
  
  private int getDarkModeColorInCurrentContrast(float paramFloat) {
    int i;
    if (paramFloat == -1.0F) {
      i = 214;
    } else {
      i = (int)(255.0F * paramFloat);
    } 
    String str = Integer.toHexString(i);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append(str);
    stringBuilder.append(str);
    return Integer.parseInt(stringBuilder.toString(), 16);
  }
}
