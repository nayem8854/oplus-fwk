package android.app;

import android.common.OplusFeatureCache;
import android.content.pm.ApplicationInfo;
import android.content.res.ApkAssets;
import android.content.res.AssetManager;
import android.content.res.CompatResources;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.content.res.ResourcesImpl;
import android.content.res.ResourcesKey;
import android.content.res.loader.ResourcesLoader;
import android.os.IBinder;
import android.os.Trace;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.util.Pair;
import android.util.Slog;
import android.view.Display;
import android.view.DisplayAdjustments;
import com.android.internal.util.ArrayUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.WeakHashMap;
import java.util.function.Consumer;

public class ResourcesManager {
  private final Configuration mResConfiguration = new Configuration();
  
  private final ArrayMap<ResourcesKey, WeakReference<ResourcesImpl>> mResourceImpls = new ArrayMap();
  
  private final ArrayList<WeakReference<Resources>> mResourceReferences = new ArrayList<>();
  
  private final ReferenceQueue<Resources> mResourcesReferencesQueue = new ReferenceQueue<>();
  
  private static class ApkKey {
    public final boolean overlay;
    
    public final String path;
    
    public final boolean sharedLib;
    
    ApkKey(String param1String, boolean param1Boolean1, boolean param1Boolean2) {
      this.path = param1String;
      this.sharedLib = param1Boolean1;
      this.overlay = param1Boolean2;
    }
    
    public int hashCode() {
      int i = this.path.hashCode();
      int j = Boolean.hashCode(this.sharedLib);
      int k = Boolean.hashCode(this.overlay);
      return ((1 * 31 + i) * 31 + j) * 31 + k;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ApkKey;
      boolean bool1 = false;
      if (!bool)
        return false; 
      param1Object = param1Object;
      bool = bool1;
      if (this.path.equals(((ApkKey)param1Object).path)) {
        bool = bool1;
        if (this.sharedLib == ((ApkKey)param1Object).sharedLib) {
          bool = bool1;
          if (this.overlay == ((ApkKey)param1Object).overlay)
            bool = true; 
        } 
      } 
      return bool;
    }
  }
  
  private final LruCache<ApkKey, ApkAssets> mLoadedApkAssets = new LruCache(3);
  
  private final ArrayMap<ApkKey, WeakReference<ApkAssets>> mCachedApkAssets = new ArrayMap();
  
  private static class ActivityResources {
    private ActivityResources() {}
    
    public final Configuration overrideConfig = new Configuration();
    
    public final ArrayList<WeakReference<Resources>> activityResources = new ArrayList<>();
    
    final ReferenceQueue<Resources> activityResourcesQueue = new ReferenceQueue<>();
  }
  
  private final WeakHashMap<IBinder, ActivityResources> mActivityResourceReferences = new WeakHashMap<>();
  
  private final ArrayMap<Pair<Integer, DisplayAdjustments>, WeakReference<Display>> mAdjustedDisplays = new ArrayMap();
  
  private final UpdateHandler mUpdateCallbacks = new UpdateHandler();
  
  private static final boolean DEBUG = false;
  
  private static final boolean ENABLE_APK_ASSETS_CACHE = true;
  
  static final String TAG = "ResourcesManager";
  
  private static ResourcesManager sResourcesManager;
  
  private CompatibilityInfo mResCompatibilityInfo;
  
  public static ResourcesManager getInstance() {
    // Byte code:
    //   0: ldc android/app/ResourcesManager
    //   2: monitorenter
    //   3: getstatic android/app/ResourcesManager.sResourcesManager : Landroid/app/ResourcesManager;
    //   6: ifnonnull -> 21
    //   9: new android/app/ResourcesManager
    //   12: astore_0
    //   13: aload_0
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: putstatic android/app/ResourcesManager.sResourcesManager : Landroid/app/ResourcesManager;
    //   21: getstatic android/app/ResourcesManager.sResourcesManager : Landroid/app/ResourcesManager;
    //   24: astore_0
    //   25: ldc android/app/ResourcesManager
    //   27: monitorexit
    //   28: aload_0
    //   29: areturn
    //   30: astore_0
    //   31: ldc android/app/ResourcesManager
    //   33: monitorexit
    //   34: aload_0
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #190	-> 0
    //   #191	-> 3
    //   #192	-> 9
    //   #194	-> 21
    //   #195	-> 30
    // Exception table:
    //   from	to	target	type
    //   3	9	30	finally
    //   9	21	30	finally
    //   21	28	30	finally
    //   31	34	30	finally
  }
  
  public void invalidatePath(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iconst_0
    //   3: istore_2
    //   4: aload_0
    //   5: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   8: invokevirtual size : ()I
    //   11: iconst_1
    //   12: isub
    //   13: istore_3
    //   14: iload_3
    //   15: iflt -> 86
    //   18: aload_0
    //   19: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   22: iload_3
    //   23: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   26: checkcast android/content/res/ResourcesKey
    //   29: astore #4
    //   31: iload_2
    //   32: istore #5
    //   34: aload #4
    //   36: aload_1
    //   37: invokevirtual isPathReferenced : (Ljava/lang/String;)Z
    //   40: ifeq -> 77
    //   43: aload_0
    //   44: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   47: iload_3
    //   48: invokevirtual removeAt : (I)Ljava/lang/Object;
    //   51: checkcast java/lang/ref/WeakReference
    //   54: invokevirtual get : ()Ljava/lang/Object;
    //   57: checkcast android/content/res/ResourcesImpl
    //   60: astore #4
    //   62: aload #4
    //   64: ifnull -> 72
    //   67: aload #4
    //   69: invokevirtual flushLayoutCache : ()V
    //   72: iload_2
    //   73: iconst_1
    //   74: iadd
    //   75: istore #5
    //   77: iinc #3, -1
    //   80: iload #5
    //   82: istore_2
    //   83: goto -> 14
    //   86: new java/lang/StringBuilder
    //   89: astore #4
    //   91: aload #4
    //   93: invokespecial <init> : ()V
    //   96: aload #4
    //   98: ldc_w 'Invalidated '
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload #4
    //   107: iload_2
    //   108: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload #4
    //   114: ldc_w ' asset managers that referenced '
    //   117: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload #4
    //   123: aload_1
    //   124: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: pop
    //   128: ldc 'ResourcesManager'
    //   130: aload #4
    //   132: invokevirtual toString : ()Ljava/lang/String;
    //   135: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   138: pop
    //   139: aload_0
    //   140: getfield mCachedApkAssets : Landroid/util/ArrayMap;
    //   143: invokevirtual size : ()I
    //   146: iconst_1
    //   147: isub
    //   148: istore_3
    //   149: iload_3
    //   150: iflt -> 221
    //   153: aload_0
    //   154: getfield mCachedApkAssets : Landroid/util/ArrayMap;
    //   157: iload_3
    //   158: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   161: checkcast android/app/ResourcesManager$ApkKey
    //   164: astore #4
    //   166: aload #4
    //   168: getfield path : Ljava/lang/String;
    //   171: aload_1
    //   172: invokevirtual equals : (Ljava/lang/Object;)Z
    //   175: ifeq -> 215
    //   178: aload_0
    //   179: getfield mCachedApkAssets : Landroid/util/ArrayMap;
    //   182: iload_3
    //   183: invokevirtual removeAt : (I)Ljava/lang/Object;
    //   186: checkcast java/lang/ref/WeakReference
    //   189: astore #4
    //   191: aload #4
    //   193: ifnull -> 215
    //   196: aload #4
    //   198: invokevirtual get : ()Ljava/lang/Object;
    //   201: ifnull -> 215
    //   204: aload #4
    //   206: invokevirtual get : ()Ljava/lang/Object;
    //   209: checkcast android/content/res/ApkAssets
    //   212: invokevirtual close : ()V
    //   215: iinc #3, -1
    //   218: goto -> 149
    //   221: aload_0
    //   222: monitorexit
    //   223: return
    //   224: astore_1
    //   225: aload_0
    //   226: monitorexit
    //   227: aload_1
    //   228: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #204	-> 0
    //   #205	-> 2
    //   #207	-> 4
    //   #208	-> 18
    //   #209	-> 31
    //   #210	-> 43
    //   #211	-> 62
    //   #212	-> 67
    //   #214	-> 72
    //   #207	-> 77
    //   #218	-> 86
    //   #220	-> 139
    //   #221	-> 153
    //   #222	-> 166
    //   #223	-> 178
    //   #224	-> 191
    //   #225	-> 204
    //   #220	-> 215
    //   #229	-> 221
    //   #230	-> 223
    //   #229	-> 224
    // Exception table:
    //   from	to	target	type
    //   4	14	224	finally
    //   18	31	224	finally
    //   34	43	224	finally
    //   43	62	224	finally
    //   67	72	224	finally
    //   86	139	224	finally
    //   139	149	224	finally
    //   153	166	224	finally
    //   166	178	224	finally
    //   178	191	224	finally
    //   196	204	224	finally
    //   204	215	224	finally
    //   221	223	224	finally
    //   225	227	224	finally
  }
  
  public Configuration getConfiguration() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mResConfiguration : Landroid/content/res/Configuration;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #233	-> 0
    //   #234	-> 2
    //   #235	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  DisplayMetrics getDisplayMetrics() {
    return getDisplayMetrics(0, DisplayAdjustments.DEFAULT_DISPLAY_ADJUSTMENTS);
  }
  
  protected DisplayMetrics getDisplayMetrics(int paramInt, DisplayAdjustments paramDisplayAdjustments) {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    Display display = getAdjustedDisplay(paramInt, paramDisplayAdjustments);
    if (display != null) {
      display.getMetrics(displayMetrics);
    } else {
      displayMetrics.setToDefaults();
    } 
    return displayMetrics;
  }
  
  private static void applyNonDefaultDisplayMetricsToConfiguration(DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration) {
    paramConfiguration.touchscreen = 1;
    paramConfiguration.densityDpi = paramDisplayMetrics.densityDpi;
    paramConfiguration.screenWidthDp = (int)(paramDisplayMetrics.widthPixels / paramDisplayMetrics.density);
    paramConfiguration.screenHeightDp = (int)(paramDisplayMetrics.heightPixels / paramDisplayMetrics.density);
    int i = Configuration.resetScreenLayout(paramConfiguration.screenLayout);
    if (paramDisplayMetrics.widthPixels > paramDisplayMetrics.heightPixels) {
      paramConfiguration.orientation = 2;
      paramConfiguration.screenLayout = Configuration.reduceScreenLayout(i, paramConfiguration.screenWidthDp, paramConfiguration.screenHeightDp);
    } else {
      paramConfiguration.orientation = 1;
      paramConfiguration.screenLayout = Configuration.reduceScreenLayout(i, paramConfiguration.screenHeightDp, paramConfiguration.screenWidthDp);
    } 
    paramConfiguration.smallestScreenWidthDp = Math.min(paramConfiguration.screenWidthDp, paramConfiguration.screenHeightDp);
    paramConfiguration.compatScreenWidthDp = paramConfiguration.screenWidthDp;
    paramConfiguration.compatScreenHeightDp = paramConfiguration.screenHeightDp;
    paramConfiguration.compatSmallestScreenWidthDp = paramConfiguration.smallestScreenWidthDp;
  }
  
  public boolean applyCompatConfigurationLocked(int paramInt, Configuration paramConfiguration) {
    CompatibilityInfo compatibilityInfo = this.mResCompatibilityInfo;
    if (compatibilityInfo != null && !compatibilityInfo.supportsScreen()) {
      this.mResCompatibilityInfo.applyToConfiguration(paramInt, paramConfiguration);
      return true;
    } 
    return false;
  }
  
  private Display getAdjustedDisplay(int paramInt, DisplayAdjustments paramDisplayAdjustments) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 16
    //   4: new android/view/DisplayAdjustments
    //   7: dup
    //   8: aload_2
    //   9: invokespecial <init> : (Landroid/view/DisplayAdjustments;)V
    //   12: astore_2
    //   13: goto -> 24
    //   16: new android/view/DisplayAdjustments
    //   19: dup
    //   20: invokespecial <init> : ()V
    //   23: astore_2
    //   24: iload_1
    //   25: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   28: aload_2
    //   29: invokestatic create : (Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    //   32: astore_2
    //   33: aload_0
    //   34: monitorenter
    //   35: aload_0
    //   36: getfield mAdjustedDisplays : Landroid/util/ArrayMap;
    //   39: aload_2
    //   40: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   43: checkcast java/lang/ref/WeakReference
    //   46: astore_3
    //   47: aload_3
    //   48: ifnull -> 67
    //   51: aload_3
    //   52: invokevirtual get : ()Ljava/lang/Object;
    //   55: checkcast android/view/Display
    //   58: astore_3
    //   59: aload_3
    //   60: ifnull -> 67
    //   63: aload_0
    //   64: monitorexit
    //   65: aload_3
    //   66: areturn
    //   67: invokestatic getInstance : ()Landroid/hardware/display/DisplayManagerGlobal;
    //   70: astore_3
    //   71: aload_3
    //   72: ifnonnull -> 79
    //   75: aload_0
    //   76: monitorexit
    //   77: aconst_null
    //   78: areturn
    //   79: aload_3
    //   80: iload_1
    //   81: aload_2
    //   82: getfield second : Ljava/lang/Object;
    //   85: checkcast android/view/DisplayAdjustments
    //   88: invokevirtual getCompatibleDisplay : (ILandroid/view/DisplayAdjustments;)Landroid/view/Display;
    //   91: astore #4
    //   93: aload #4
    //   95: ifnull -> 123
    //   98: aload_0
    //   99: getfield mAdjustedDisplays : Landroid/util/ArrayMap;
    //   102: astore_3
    //   103: new java/lang/ref/WeakReference
    //   106: astore #5
    //   108: aload #5
    //   110: aload #4
    //   112: invokespecial <init> : (Ljava/lang/Object;)V
    //   115: aload_3
    //   116: aload_2
    //   117: aload #5
    //   119: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   122: pop
    //   123: aload_0
    //   124: monitorexit
    //   125: aload #4
    //   127: areturn
    //   128: astore_2
    //   129: aload_0
    //   130: monitorexit
    //   131: aload_2
    //   132: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #300	-> 0
    //   #301	-> 4
    //   #302	-> 24
    //   #303	-> 24
    //   #304	-> 33
    //   #305	-> 35
    //   #306	-> 47
    //   #307	-> 51
    //   #308	-> 59
    //   #309	-> 63
    //   #312	-> 67
    //   #313	-> 71
    //   #315	-> 75
    //   #317	-> 79
    //   #318	-> 93
    //   #319	-> 98
    //   #321	-> 123
    //   #322	-> 128
    // Exception table:
    //   from	to	target	type
    //   35	47	128	finally
    //   51	59	128	finally
    //   63	65	128	finally
    //   67	71	128	finally
    //   75	77	128	finally
    //   79	93	128	finally
    //   98	123	128	finally
    //   123	125	128	finally
    //   129	131	128	finally
  }
  
  public Display getAdjustedDisplay(int paramInt, Resources paramResources) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: invokestatic getInstance : ()Landroid/hardware/display/DisplayManagerGlobal;
    //   5: astore_3
    //   6: aload_3
    //   7: ifnonnull -> 14
    //   10: aload_0
    //   11: monitorexit
    //   12: aconst_null
    //   13: areturn
    //   14: aload_3
    //   15: iload_1
    //   16: aload_2
    //   17: invokevirtual getCompatibleDisplay : (ILandroid/content/res/Resources;)Landroid/view/Display;
    //   20: astore_2
    //   21: aload_0
    //   22: monitorexit
    //   23: aload_2
    //   24: areturn
    //   25: astore_2
    //   26: aload_0
    //   27: monitorexit
    //   28: aload_2
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #333	-> 0
    //   #334	-> 2
    //   #335	-> 6
    //   #337	-> 10
    //   #339	-> 14
    //   #340	-> 25
    // Exception table:
    //   from	to	target	type
    //   2	6	25	finally
    //   10	12	25	finally
    //   14	23	25	finally
    //   26	28	25	finally
  }
  
  private static String overlayPathToIdmapPath(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/data/resource-cache/");
    stringBuilder.append(paramString.substring(1).replace('/', '@'));
    stringBuilder.append("@idmap");
    return stringBuilder.toString();
  }
  
  private ApkAssets loadApkAssets(String paramString, boolean paramBoolean1, boolean paramBoolean2) throws IOException {
    LruCache<ApkKey, ApkAssets> lruCache1;
    ApkAssets apkAssets;
    ApkKey apkKey = new ApkKey(paramString, paramBoolean1, paramBoolean2);
    LruCache<ApkKey, ApkAssets> lruCache3 = this.mLoadedApkAssets;
    if (lruCache3 != null) {
      ApkAssets apkAssets1 = (ApkAssets)lruCache3.get(apkKey);
      if (apkAssets1 != null && apkAssets1.isUpToDate())
        return apkAssets1; 
    } 
    WeakReference<ApkAssets> weakReference = (WeakReference)this.mCachedApkAssets.get(apkKey);
    if (weakReference != null) {
      ApkAssets apkAssets1 = weakReference.get();
      if (apkAssets1 != null && apkAssets1.isUpToDate()) {
        lruCache1 = this.mLoadedApkAssets;
        if (lruCache1 != null)
          lruCache1.put(apkKey, apkAssets1); 
        return apkAssets1;
      } 
      this.mCachedApkAssets.remove(apkKey);
    } 
    byte b = 0;
    if (paramBoolean2) {
      apkAssets = ApkAssets.loadOverlayFromPath(overlayPathToIdmapPath((String)lruCache1), 0);
    } else {
      if (paramBoolean1)
        b = 2; 
      apkAssets = ApkAssets.loadFromPath((String)apkAssets, b);
    } 
    LruCache<ApkKey, ApkAssets> lruCache2 = this.mLoadedApkAssets;
    if (lruCache2 != null)
      lruCache2.put(apkKey, apkAssets); 
    this.mCachedApkAssets.put(apkKey, new WeakReference<>(apkAssets));
    return apkAssets;
  }
  
  protected AssetManager createAssetManager(ResourcesKey paramResourcesKey) {
    StringBuilder stringBuilder1, stringBuilder2;
    AssetManager.Builder builder = new AssetManager.Builder();
    str = paramResourcesKey.mResDir;
    byte b = 0;
    if (str != null)
      try {
        builder.addApkAssets(loadApkAssets(paramResourcesKey.mResDir, false, false));
      } catch (IOException iOException) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("failed to add asset path ");
        stringBuilder2.append(paramResourcesKey.mResDir);
        Log.e("ResourcesManager", stringBuilder2.toString());
        return null;
      }  
    if (paramResourcesKey.mSplitResDirs != null) {
      String[] arrayOfString;
      int i;
      byte b1;
      for (arrayOfString = paramResourcesKey.mSplitResDirs, i = arrayOfString.length, b1 = 0; b1 < i; ) {
        str = arrayOfString[b1];
        try {
          stringBuilder2.addApkAssets(loadApkAssets(str, false, false));
          b1++;
        } catch (IOException iOException) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("failed to add split asset path ");
          stringBuilder1.append(str);
          Log.e("ResourcesManager", stringBuilder1.toString());
          return null;
        } 
      } 
    } 
    if (((ResourcesKey)stringBuilder1).mLibDirs != null)
      for (String str1 : ((ResourcesKey)stringBuilder1).mLibDirs) {
        if (str1.endsWith(".apk"))
          try {
            stringBuilder2.addApkAssets(loadApkAssets(str1, true, false));
          } catch (IOException iOException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Asset path '");
            stringBuilder.append(str1);
            stringBuilder.append("' does not exist or contains no resources.");
            Log.w("ResourcesManager", stringBuilder.toString());
          }  
      }  
    if (((ResourcesKey)stringBuilder1).mOverlayDirs != null)
      for (String str : ((ResourcesKey)stringBuilder1).mOverlayDirs) {
        try {
          stringBuilder2.addApkAssets(loadApkAssets(str, false, true));
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("failed to add overlay path ");
          stringBuilder.append(str);
          Log.w("ResourcesManager", stringBuilder.toString());
        } 
      }  
    if (((ResourcesKey)stringBuilder1).mLoaders != null) {
      ResourcesLoader[] arrayOfResourcesLoader;
      int i;
      byte b1;
      for (arrayOfResourcesLoader = ((ResourcesKey)stringBuilder1).mLoaders, i = arrayOfResourcesLoader.length, b1 = b; b1 < i; ) {
        ResourcesLoader resourcesLoader = arrayOfResourcesLoader[b1];
        stringBuilder2.addLoader(resourcesLoader);
        b1++;
      } 
    } 
    return stringBuilder2.build();
  }
  
  private static <T> int countLiveReferences(Collection<WeakReference<T>> paramCollection) {
    int i = 0;
    for (WeakReference<WeakReference> weakReference : paramCollection) {
      if (weakReference != null) {
        weakReference = weakReference.get();
      } else {
        weakReference = null;
      } 
      int j = i;
      if (weakReference != null)
        j = i + 1; 
      i = j;
    } 
    return i;
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new com/android/internal/util/IndentingPrintWriter
    //   5: astore_3
    //   6: aload_3
    //   7: aload_2
    //   8: ldc_w '  '
    //   11: invokespecial <init> : (Ljava/io/Writer;Ljava/lang/String;)V
    //   14: iconst_0
    //   15: istore #4
    //   17: iload #4
    //   19: aload_1
    //   20: invokevirtual length : ()I
    //   23: iconst_2
    //   24: idiv
    //   25: if_icmpge -> 39
    //   28: aload_3
    //   29: invokevirtual increaseIndent : ()Lcom/android/internal/util/IndentingPrintWriter;
    //   32: pop
    //   33: iinc #4, 1
    //   36: goto -> 17
    //   39: aload_3
    //   40: ldc_w 'ResourcesManager:'
    //   43: invokevirtual println : (Ljava/lang/String;)V
    //   46: aload_3
    //   47: invokevirtual increaseIndent : ()Lcom/android/internal/util/IndentingPrintWriter;
    //   50: pop
    //   51: aload_0
    //   52: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   55: ifnull -> 169
    //   58: aload_3
    //   59: ldc_w 'cached apks: total='
    //   62: invokevirtual print : (Ljava/lang/String;)V
    //   65: aload_3
    //   66: aload_0
    //   67: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   70: invokevirtual size : ()I
    //   73: invokevirtual print : (I)V
    //   76: aload_3
    //   77: ldc_w ' created='
    //   80: invokevirtual print : (Ljava/lang/String;)V
    //   83: aload_3
    //   84: aload_0
    //   85: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   88: invokevirtual createCount : ()I
    //   91: invokevirtual print : (I)V
    //   94: aload_3
    //   95: ldc_w ' evicted='
    //   98: invokevirtual print : (Ljava/lang/String;)V
    //   101: aload_3
    //   102: aload_0
    //   103: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   106: invokevirtual evictionCount : ()I
    //   109: invokevirtual print : (I)V
    //   112: aload_3
    //   113: ldc_w ' hit='
    //   116: invokevirtual print : (Ljava/lang/String;)V
    //   119: aload_3
    //   120: aload_0
    //   121: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   124: invokevirtual hitCount : ()I
    //   127: invokevirtual print : (I)V
    //   130: aload_3
    //   131: ldc_w ' miss='
    //   134: invokevirtual print : (Ljava/lang/String;)V
    //   137: aload_3
    //   138: aload_0
    //   139: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   142: invokevirtual missCount : ()I
    //   145: invokevirtual print : (I)V
    //   148: aload_3
    //   149: ldc_w ' max='
    //   152: invokevirtual print : (Ljava/lang/String;)V
    //   155: aload_3
    //   156: aload_0
    //   157: getfield mLoadedApkAssets : Landroid/util/LruCache;
    //   160: invokevirtual maxSize : ()I
    //   163: invokevirtual print : (I)V
    //   166: goto -> 176
    //   169: aload_3
    //   170: ldc_w 'cached apks: 0 [cache disabled]'
    //   173: invokevirtual print : (Ljava/lang/String;)V
    //   176: aload_3
    //   177: invokevirtual println : ()V
    //   180: aload_3
    //   181: ldc_w 'total apks: '
    //   184: invokevirtual print : (Ljava/lang/String;)V
    //   187: aload_3
    //   188: aload_0
    //   189: getfield mCachedApkAssets : Landroid/util/ArrayMap;
    //   192: invokevirtual values : ()Ljava/util/Collection;
    //   195: invokestatic countLiveReferences : (Ljava/util/Collection;)I
    //   198: invokevirtual println : (I)V
    //   201: aload_3
    //   202: ldc_w 'resources: '
    //   205: invokevirtual print : (Ljava/lang/String;)V
    //   208: aload_0
    //   209: getfield mResourceReferences : Ljava/util/ArrayList;
    //   212: invokestatic countLiveReferences : (Ljava/util/Collection;)I
    //   215: istore #4
    //   217: aload_0
    //   218: getfield mActivityResourceReferences : Ljava/util/WeakHashMap;
    //   221: invokevirtual values : ()Ljava/util/Collection;
    //   224: invokeinterface iterator : ()Ljava/util/Iterator;
    //   229: astore_2
    //   230: aload_2
    //   231: invokeinterface hasNext : ()Z
    //   236: ifeq -> 264
    //   239: aload_2
    //   240: invokeinterface next : ()Ljava/lang/Object;
    //   245: checkcast android/app/ResourcesManager$ActivityResources
    //   248: astore_1
    //   249: iload #4
    //   251: aload_1
    //   252: getfield activityResources : Ljava/util/ArrayList;
    //   255: invokestatic countLiveReferences : (Ljava/util/Collection;)I
    //   258: iadd
    //   259: istore #4
    //   261: goto -> 230
    //   264: aload_3
    //   265: iload #4
    //   267: invokevirtual println : (I)V
    //   270: aload_3
    //   271: ldc_w 'resource impls: '
    //   274: invokevirtual print : (Ljava/lang/String;)V
    //   277: aload_3
    //   278: aload_0
    //   279: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   282: invokevirtual values : ()Ljava/util/Collection;
    //   285: invokestatic countLiveReferences : (Ljava/util/Collection;)I
    //   288: invokevirtual println : (I)V
    //   291: aload_0
    //   292: monitorexit
    //   293: return
    //   294: astore_1
    //   295: aload_0
    //   296: monitorexit
    //   297: aload_1
    //   298: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #482	-> 0
    //   #483	-> 2
    //   #484	-> 14
    //   #485	-> 28
    //   #484	-> 33
    //   #488	-> 39
    //   #489	-> 46
    //   #490	-> 51
    //   #491	-> 58
    //   #492	-> 65
    //   #493	-> 76
    //   #494	-> 83
    //   #495	-> 94
    //   #496	-> 101
    //   #497	-> 112
    //   #498	-> 119
    //   #499	-> 130
    //   #500	-> 137
    //   #501	-> 148
    //   #502	-> 155
    //   #504	-> 169
    //   #506	-> 176
    //   #508	-> 180
    //   #509	-> 187
    //   #511	-> 201
    //   #513	-> 208
    //   #514	-> 217
    //   #515	-> 249
    //   #516	-> 261
    //   #517	-> 264
    //   #519	-> 270
    //   #520	-> 277
    //   #521	-> 291
    //   #522	-> 293
    //   #521	-> 294
    // Exception table:
    //   from	to	target	type
    //   2	14	294	finally
    //   17	28	294	finally
    //   28	33	294	finally
    //   39	46	294	finally
    //   46	51	294	finally
    //   51	58	294	finally
    //   58	65	294	finally
    //   65	76	294	finally
    //   76	83	294	finally
    //   83	94	294	finally
    //   94	101	294	finally
    //   101	112	294	finally
    //   112	119	294	finally
    //   119	130	294	finally
    //   130	137	294	finally
    //   137	148	294	finally
    //   148	155	294	finally
    //   155	166	294	finally
    //   169	176	294	finally
    //   176	180	294	finally
    //   180	187	294	finally
    //   187	201	294	finally
    //   201	208	294	finally
    //   208	217	294	finally
    //   217	230	294	finally
    //   230	249	294	finally
    //   249	261	294	finally
    //   264	270	294	finally
    //   270	277	294	finally
    //   277	291	294	finally
    //   291	293	294	finally
    //   295	297	294	finally
  }
  
  private Configuration generateConfig(ResourcesKey paramResourcesKey, DisplayMetrics paramDisplayMetrics) {
    boolean bool;
    if (paramResourcesKey.mDisplayId == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    boolean bool1 = paramResourcesKey.hasOverrideConfiguration();
    if (!bool || bool1) {
      Configuration configuration2 = new Configuration(getConfiguration());
      if (!bool)
        applyNonDefaultDisplayMetricsToConfiguration(paramDisplayMetrics, configuration2); 
      Configuration configuration1 = configuration2;
      if (bool1) {
        configuration2.updateFrom(paramResourcesKey.mOverrideConfiguration);
        configuration1 = configuration2;
      } 
      return configuration1;
    } 
    return getConfiguration();
  }
  
  private ResourcesImpl createResourcesImpl(String paramString, ResourcesKey paramResourcesKey) {
    ResourcesImpl resourcesImpl = createResourcesImpl(paramResourcesKey);
    if (resourcesImpl != null && !TextUtils.isEmpty(paramString))
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(resourcesImpl, paramString); 
    return resourcesImpl;
  }
  
  private ResourcesImpl createResourcesImpl(ResourcesKey paramResourcesKey) {
    DisplayAdjustments displayAdjustments = new DisplayAdjustments(paramResourcesKey.mOverrideConfiguration);
    displayAdjustments.setCompatibilityInfo(paramResourcesKey.mCompatInfo);
    AssetManager assetManager = createAssetManager(paramResourcesKey);
    if (assetManager == null)
      return null; 
    DisplayMetrics displayMetrics = getDisplayMetrics(paramResourcesKey.mDisplayId, displayAdjustments);
    Configuration configuration = generateConfig(paramResourcesKey, displayMetrics);
    return new ResourcesImpl(assetManager, displayMetrics, configuration, displayAdjustments);
  }
  
  private ResourcesImpl findResourcesImplForKeyLocked(ResourcesKey paramResourcesKey) {
    WeakReference<ResourcesImpl> weakReference = (WeakReference)this.mResourceImpls.get(paramResourcesKey);
    if (weakReference != null) {
      ResourcesImpl resourcesImpl = weakReference.get();
    } else {
      weakReference = null;
    } 
    if (weakReference != null && weakReference.getAssets().isUpToDate())
      return (ResourcesImpl)weakReference; 
    return null;
  }
  
  private ResourcesImpl findOrCreateResourcesImplForKeyLocked(ResourcesKey paramResourcesKey) {
    return findOrCreateResourcesImplForKeyLocked(null, paramResourcesKey);
  }
  
  private ResourcesImpl findOrCreateResourcesImplForKeyLocked(String paramString, ResourcesKey paramResourcesKey) {
    ResourcesImpl resourcesImpl1 = findResourcesImplForKeyLocked(paramResourcesKey);
    ResourcesImpl resourcesImpl2 = resourcesImpl1;
    if (resourcesImpl1 == null) {
      ResourcesImpl resourcesImpl = createResourcesImpl(paramString, paramResourcesKey);
      resourcesImpl2 = resourcesImpl;
      if (resourcesImpl != null) {
        this.mResourceImpls.put(paramResourcesKey, new WeakReference<>(resourcesImpl));
        resourcesImpl2 = resourcesImpl;
      } 
    } 
    return resourcesImpl2;
  }
  
  private ResourcesKey findKeyForResourceImplLocked(ResourcesImpl paramResourcesImpl) {
    int i = this.mResourceImpls.size();
    byte b = 0;
    while (true) {
      ResourcesImpl resourcesImpl = null;
      if (b < i) {
        WeakReference<ResourcesImpl> weakReference = (WeakReference)this.mResourceImpls.valueAt(b);
        if (weakReference != null)
          resourcesImpl = weakReference.get(); 
        if (paramResourcesImpl == resourcesImpl)
          return (ResourcesKey)this.mResourceImpls.keyAt(b); 
        b++;
        continue;
      } 
      break;
    } 
    return null;
  }
  
  boolean isSameResourcesOverrideConfig(IBinder paramIBinder, Configuration paramConfiguration) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_1
    //   3: ifnull -> 25
    //   6: aload_0
    //   7: getfield mActivityResourceReferences : Ljava/util/WeakHashMap;
    //   10: aload_1
    //   11: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   14: checkcast android/app/ResourcesManager$ActivityResources
    //   17: astore_1
    //   18: goto -> 27
    //   21: astore_1
    //   22: goto -> 103
    //   25: aconst_null
    //   26: astore_1
    //   27: iconst_1
    //   28: istore_3
    //   29: iconst_1
    //   30: istore #4
    //   32: aload_1
    //   33: ifnonnull -> 51
    //   36: aload_2
    //   37: ifnonnull -> 43
    //   40: goto -> 46
    //   43: iconst_0
    //   44: istore #4
    //   46: aload_0
    //   47: monitorexit
    //   48: iload #4
    //   50: ireturn
    //   51: aload_1
    //   52: getfield overrideConfig : Landroid/content/res/Configuration;
    //   55: aload_2
    //   56: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   59: ifne -> 95
    //   62: aload_2
    //   63: ifnull -> 89
    //   66: aload_1
    //   67: getfield overrideConfig : Landroid/content/res/Configuration;
    //   70: ifnull -> 89
    //   73: aload_1
    //   74: getfield overrideConfig : Landroid/content/res/Configuration;
    //   77: astore_1
    //   78: aload_2
    //   79: aload_1
    //   80: invokevirtual diffPublicOnly : (Landroid/content/res/Configuration;)I
    //   83: ifne -> 89
    //   86: goto -> 95
    //   89: iconst_0
    //   90: istore #4
    //   92: goto -> 98
    //   95: iload_3
    //   96: istore #4
    //   98: aload_0
    //   99: monitorexit
    //   100: iload #4
    //   102: ireturn
    //   103: aload_0
    //   104: monitorexit
    //   105: aload_1
    //   106: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #649	-> 0
    //   #651	-> 2
    //   #662	-> 21
    //   #651	-> 25
    //   #652	-> 27
    //   #653	-> 36
    //   #657	-> 51
    //   #659	-> 78
    //   #657	-> 100
    //   #662	-> 103
    // Exception table:
    //   from	to	target	type
    //   6	18	21	finally
    //   46	48	21	finally
    //   51	62	21	finally
    //   66	78	21	finally
    //   78	86	21	finally
    //   98	100	21	finally
    //   103	105	21	finally
  }
  
  private ActivityResources getOrCreateActivityResourcesStructLocked(IBinder paramIBinder) {
    ActivityResources activityResources1 = this.mActivityResourceReferences.get(paramIBinder);
    ActivityResources activityResources2 = activityResources1;
    if (activityResources1 == null) {
      activityResources2 = new ActivityResources();
      this.mActivityResourceReferences.put(paramIBinder, activityResources2);
    } 
    return activityResources2;
  }
  
  private Resources findResourcesForActivityLocked(IBinder paramIBinder, ResourcesKey paramResourcesKey, ClassLoader paramClassLoader) {
    ActivityResources activityResources = getOrCreateActivityResourcesStructLocked(paramIBinder);
    int i = activityResources.activityResources.size();
    byte b = 0;
    while (true) {
      paramIBinder = null;
      if (b < i) {
        ResourcesKey resourcesKey;
        WeakReference<Resources> weakReference = activityResources.activityResources.get(b);
        Resources resources = weakReference.get();
        if (resources != null) {
          ResourcesImpl resourcesImpl = resources.getImpl();
          resourcesKey = findKeyForResourceImplLocked(resourcesImpl);
        } 
        if (resourcesKey != null && 
          Objects.equals(resources.getClassLoader(), paramClassLoader) && 
          Objects.equals(resourcesKey, paramResourcesKey))
          return resources; 
        b++;
        continue;
      } 
      break;
    } 
    return null;
  }
  
  private Resources createResourcesForActivityLocked(IBinder paramIBinder, ClassLoader paramClassLoader, ResourcesImpl paramResourcesImpl, CompatibilityInfo paramCompatibilityInfo) {
    Resources resources;
    ActivityResources activityResources = getOrCreateActivityResourcesStructLocked(paramIBinder);
    cleanupReferences(activityResources.activityResources, activityResources.activityResourcesQueue);
    if (paramCompatibilityInfo.needsCompatResources()) {
      resources = new CompatResources(paramClassLoader);
    } else {
      resources = new Resources(paramClassLoader);
    } 
    resources.setImpl(paramResourcesImpl);
    resources.setCallbacks(this.mUpdateCallbacks);
    activityResources.activityResources.add(new WeakReference<>(resources, activityResources.activityResourcesQueue));
    return resources;
  }
  
  private Resources createResourcesLocked(ClassLoader paramClassLoader, ResourcesImpl paramResourcesImpl, CompatibilityInfo paramCompatibilityInfo) {
    Resources resources;
    cleanupReferences(this.mResourceReferences, this.mResourcesReferencesQueue);
    if (paramCompatibilityInfo.needsCompatResources()) {
      resources = new CompatResources(paramClassLoader);
    } else {
      resources = new Resources((ClassLoader)resources);
    } 
    resources.setImpl(paramResourcesImpl);
    resources.setCallbacks(this.mUpdateCallbacks);
    this.mResourceReferences.add(new WeakReference<>(resources, this.mResourcesReferencesQueue));
    return resources;
  }
  
  public Resources createBaseTokenResources(IBinder paramIBinder, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, ClassLoader paramClassLoader, List<ResourcesLoader> paramList) {
    // Byte code:
    //   0: ldc2_w 8192
    //   3: ldc_w 'ResourcesManager#createBaseActivityResources'
    //   6: invokestatic traceBegin : (JLjava/lang/String;)V
    //   9: new android/content/res/ResourcesKey
    //   12: astore #11
    //   14: aconst_null
    //   15: astore #12
    //   17: aload #7
    //   19: ifnull -> 37
    //   22: new android/content/res/Configuration
    //   25: astore #13
    //   27: aload #13
    //   29: aload #7
    //   31: invokespecial <init> : (Landroid/content/res/Configuration;)V
    //   34: goto -> 40
    //   37: aconst_null
    //   38: astore #13
    //   40: aload #10
    //   42: ifnonnull -> 52
    //   45: aload #12
    //   47: astore #10
    //   49: goto -> 71
    //   52: aload #10
    //   54: iconst_0
    //   55: anewarray android/content/res/loader/ResourcesLoader
    //   58: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   63: checkcast [Landroid/content/res/loader/ResourcesLoader;
    //   66: astore #10
    //   68: goto -> 49
    //   71: aload #11
    //   73: aload_2
    //   74: aload_3
    //   75: aload #4
    //   77: aload #5
    //   79: iload #6
    //   81: aload #13
    //   83: aload #8
    //   85: aload #10
    //   87: invokespecial <init> : (Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;[Landroid/content/res/loader/ResourcesLoader;)V
    //   90: aload #9
    //   92: ifnull -> 101
    //   95: aload #9
    //   97: astore_2
    //   98: goto -> 105
    //   101: invokestatic getSystemClassLoader : ()Ljava/lang/ClassLoader;
    //   104: astore_2
    //   105: aload_0
    //   106: monitorenter
    //   107: aload_0
    //   108: aload_1
    //   109: invokespecial getOrCreateActivityResourcesStructLocked : (Landroid/os/IBinder;)Landroid/app/ResourcesManager$ActivityResources;
    //   112: pop
    //   113: aload_0
    //   114: monitorexit
    //   115: aload_0
    //   116: aload_1
    //   117: aload #7
    //   119: iload #6
    //   121: iconst_0
    //   122: invokevirtual updateResourcesForActivity : (Landroid/os/IBinder;Landroid/content/res/Configuration;IZ)V
    //   125: aload_0
    //   126: aload_1
    //   127: aload #11
    //   129: invokespecial rebaseKeyForActivity : (Landroid/os/IBinder;Landroid/content/res/ResourcesKey;)V
    //   132: aload_0
    //   133: monitorenter
    //   134: aload_0
    //   135: aload_1
    //   136: aload #11
    //   138: aload_2
    //   139: invokespecial findResourcesForActivityLocked : (Landroid/os/IBinder;Landroid/content/res/ResourcesKey;Ljava/lang/ClassLoader;)Landroid/content/res/Resources;
    //   142: astore_3
    //   143: aload_3
    //   144: ifnull -> 157
    //   147: aload_0
    //   148: monitorexit
    //   149: ldc2_w 8192
    //   152: invokestatic traceEnd : (J)V
    //   155: aload_3
    //   156: areturn
    //   157: aload_0
    //   158: monitorexit
    //   159: aload_0
    //   160: aload_1
    //   161: aload #11
    //   163: aload_2
    //   164: invokespecial createResources : (Landroid/os/IBinder;Landroid/content/res/ResourcesKey;Ljava/lang/ClassLoader;)Landroid/content/res/Resources;
    //   167: astore_1
    //   168: ldc2_w 8192
    //   171: invokestatic traceEnd : (J)V
    //   174: aload_1
    //   175: areturn
    //   176: astore_1
    //   177: aload_0
    //   178: monitorexit
    //   179: aload_1
    //   180: athrow
    //   181: astore_1
    //   182: aload_0
    //   183: monitorexit
    //   184: aload_1
    //   185: athrow
    //   186: astore_1
    //   187: goto -> 199
    //   190: astore_1
    //   191: goto -> 182
    //   194: astore_1
    //   195: goto -> 199
    //   198: astore_1
    //   199: ldc2_w 8192
    //   202: invokestatic traceEnd : (J)V
    //   205: aload_1
    //   206: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #766	-> 0
    //   #768	-> 9
    //   #774	-> 14
    //   #776	-> 40
    //   #777	-> 90
    //   #784	-> 105
    //   #786	-> 107
    //   #787	-> 113
    //   #790	-> 115
    //   #793	-> 125
    //   #795	-> 132
    //   #796	-> 134
    //   #798	-> 143
    //   #799	-> 147
    //   #806	-> 149
    //   #799	-> 155
    //   #801	-> 157
    //   #804	-> 159
    //   #806	-> 168
    //   #804	-> 174
    //   #801	-> 176
    //   #787	-> 181
    //   #806	-> 186
    //   #787	-> 190
    //   #806	-> 194
    //   #807	-> 205
    // Exception table:
    //   from	to	target	type
    //   0	9	198	finally
    //   9	14	198	finally
    //   22	34	198	finally
    //   52	68	198	finally
    //   71	90	198	finally
    //   101	105	198	finally
    //   105	107	194	finally
    //   107	113	181	finally
    //   113	115	181	finally
    //   115	125	186	finally
    //   125	132	186	finally
    //   132	134	186	finally
    //   134	143	176	finally
    //   147	149	176	finally
    //   157	159	176	finally
    //   159	168	186	finally
    //   177	179	176	finally
    //   179	181	186	finally
    //   182	184	190	finally
    //   184	186	186	finally
  }
  
  private void rebaseKeyForActivity(IBinder paramIBinder, ResourcesKey paramResourcesKey) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokespecial getOrCreateActivityResourcesStructLocked : (Landroid/os/IBinder;)Landroid/app/ResourcesManager$ActivityResources;
    //   7: astore_1
    //   8: aload_2
    //   9: invokevirtual hasOverrideConfiguration : ()Z
    //   12: ifeq -> 67
    //   15: aload_1
    //   16: getfield overrideConfig : Landroid/content/res/Configuration;
    //   19: astore_3
    //   20: getstatic android/content/res/Configuration.EMPTY : Landroid/content/res/Configuration;
    //   23: astore #4
    //   25: aload_3
    //   26: aload #4
    //   28: invokevirtual equals : (Landroid/content/res/Configuration;)Z
    //   31: ifne -> 67
    //   34: new android/content/res/Configuration
    //   37: astore #4
    //   39: aload #4
    //   41: aload_1
    //   42: getfield overrideConfig : Landroid/content/res/Configuration;
    //   45: invokespecial <init> : (Landroid/content/res/Configuration;)V
    //   48: aload #4
    //   50: aload_2
    //   51: getfield mOverrideConfiguration : Landroid/content/res/Configuration;
    //   54: invokevirtual updateFrom : (Landroid/content/res/Configuration;)I
    //   57: pop
    //   58: aload_2
    //   59: getfield mOverrideConfiguration : Landroid/content/res/Configuration;
    //   62: aload #4
    //   64: invokevirtual setTo : (Landroid/content/res/Configuration;)V
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: astore_1
    //   71: aload_0
    //   72: monitorexit
    //   73: aload_1
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #814	-> 0
    //   #815	-> 2
    //   #816	-> 2
    //   #819	-> 8
    //   #820	-> 25
    //   #821	-> 34
    //   #822	-> 48
    //   #823	-> 58
    //   #825	-> 67
    //   #826	-> 69
    //   #825	-> 70
    // Exception table:
    //   from	to	target	type
    //   2	8	70	finally
    //   8	25	70	finally
    //   25	34	70	finally
    //   34	48	70	finally
    //   48	58	70	finally
    //   58	67	70	finally
    //   67	69	70	finally
    //   71	73	70	finally
  }
  
  private static <T> void cleanupReferences(ArrayList<WeakReference<T>> paramArrayList, ReferenceQueue<T> paramReferenceQueue) {
    Reference<? extends T> reference = paramReferenceQueue.poll();
    if (reference == null)
      return; 
    HashSet<Reference<? extends T>> hashSet = new HashSet();
    for (; reference != null; reference = paramReferenceQueue.poll())
      hashSet.add(reference); 
    ArrayUtils.unstableRemoveIf(paramArrayList, new _$$Lambda$ResourcesManager$JPMYJ3O5qlFN_c1356pr2ximEb0(hashSet));
  }
  
  private Resources createResources(IBinder paramIBinder, ResourcesKey paramResourcesKey, ClassLoader paramClassLoader) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_2
    //   4: invokespecial findOrCreateResourcesImplForKeyLocked : (Landroid/content/res/ResourcesKey;)Landroid/content/res/ResourcesImpl;
    //   7: astore #4
    //   9: aload #4
    //   11: ifnonnull -> 18
    //   14: aload_0
    //   15: monitorexit
    //   16: aconst_null
    //   17: areturn
    //   18: aload_1
    //   19: ifnull -> 39
    //   22: aload_0
    //   23: aload_1
    //   24: aload_3
    //   25: aload #4
    //   27: aload_2
    //   28: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   31: invokespecial createResourcesForActivityLocked : (Landroid/os/IBinder;Ljava/lang/ClassLoader;Landroid/content/res/ResourcesImpl;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: areturn
    //   39: aload_0
    //   40: aload_3
    //   41: aload #4
    //   43: aload_2
    //   44: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   47: invokespecial createResourcesLocked : (Ljava/lang/ClassLoader;Landroid/content/res/ResourcesImpl;Landroid/content/res/CompatibilityInfo;)Landroid/content/res/Resources;
    //   50: astore_1
    //   51: aload_0
    //   52: monitorexit
    //   53: aload_1
    //   54: areturn
    //   55: astore_1
    //   56: aload_0
    //   57: monitorexit
    //   58: aload_1
    //   59: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #860	-> 0
    //   #867	-> 2
    //   #868	-> 9
    //   #869	-> 14
    //   #872	-> 18
    //   #873	-> 22
    //   #876	-> 39
    //   #878	-> 55
    // Exception table:
    //   from	to	target	type
    //   2	9	55	finally
    //   14	16	55	finally
    //   22	37	55	finally
    //   39	53	55	finally
    //   56	58	55	finally
  }
  
  public Resources getResources(IBinder paramIBinder, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, ClassLoader paramClassLoader, List<ResourcesLoader> paramList) {
    try {
      ClassLoader classLoader;
      ResourcesLoader[] arrayOfResourcesLoader;
      Trace.traceBegin(8192L, "ResourcesManager#getResources");
      ResourcesKey resourcesKey = new ResourcesKey();
      List list = null;
      if (paramConfiguration != null) {
        Configuration configuration = new Configuration();
        this(paramConfiguration);
        paramConfiguration = configuration;
      } else {
        paramConfiguration = null;
      } 
      if (paramList == null) {
        paramList = list;
      } else {
        arrayOfResourcesLoader = paramList.<ResourcesLoader>toArray(new ResourcesLoader[0]);
      } 
      this(paramString, paramArrayOfString1, paramArrayOfString2, paramArrayOfString3, paramInt, paramConfiguration, paramCompatibilityInfo, arrayOfResourcesLoader);
      if (paramClassLoader != null) {
        classLoader = paramClassLoader;
      } else {
        classLoader = ClassLoader.getSystemClassLoader();
      } 
      if (paramIBinder != null) {
        try {
          rebaseKeyForActivity(paramIBinder, resourcesKey);
          Resources resources1 = createResources(paramIBinder, resourcesKey, classLoader);
          Trace.traceEnd(8192L);
          return resources1;
        } finally {}
        Trace.traceEnd(8192L);
        throw paramIBinder;
      } 
      Resources resources = createResources(paramIBinder, resourcesKey, classLoader);
      Trace.traceEnd(8192L);
      return resources;
    } finally {}
    Trace.traceEnd(8192L);
    throw paramIBinder;
  }
  
  public void updateResourcesForActivity(IBinder paramIBinder, Configuration paramConfiguration, int paramInt, boolean paramBoolean) {
    updateResourcesForActivity(null, paramIBinder, paramConfiguration, paramInt, paramBoolean);
  }
  
  public void updateResourcesForActivity(String paramString, IBinder paramIBinder, Configuration paramConfiguration, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: ldc2_w 8192
    //   3: ldc_w 'ResourcesManager#updateResourcesForActivity'
    //   6: invokestatic traceBegin : (JLjava/lang/String;)V
    //   9: aload_0
    //   10: monitorenter
    //   11: aload_0
    //   12: aload_2
    //   13: invokespecial getOrCreateActivityResourcesStructLocked : (Landroid/os/IBinder;)Landroid/app/ResourcesManager$ActivityResources;
    //   16: astore_2
    //   17: aload_2
    //   18: getfield overrideConfig : Landroid/content/res/Configuration;
    //   21: aload_3
    //   22: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   25: ifeq -> 42
    //   28: iload #5
    //   30: ifne -> 42
    //   33: aload_0
    //   34: monitorexit
    //   35: ldc2_w 8192
    //   38: invokestatic traceEnd : (J)V
    //   41: return
    //   42: new android/content/res/Configuration
    //   45: astore #6
    //   47: aload #6
    //   49: aload_2
    //   50: getfield overrideConfig : Landroid/content/res/Configuration;
    //   53: invokespecial <init> : (Landroid/content/res/Configuration;)V
    //   56: aload_3
    //   57: ifnull -> 71
    //   60: aload_2
    //   61: getfield overrideConfig : Landroid/content/res/Configuration;
    //   64: aload_3
    //   65: invokevirtual setTo : (Landroid/content/res/Configuration;)V
    //   68: goto -> 78
    //   71: aload_2
    //   72: getfield overrideConfig : Landroid/content/res/Configuration;
    //   75: invokevirtual unset : ()V
    //   78: aload_2
    //   79: getfield activityResources : Ljava/util/ArrayList;
    //   82: invokevirtual size : ()I
    //   85: istore #7
    //   87: iconst_0
    //   88: istore #8
    //   90: iload #8
    //   92: iload #7
    //   94: if_icmpge -> 167
    //   97: aload_2
    //   98: getfield activityResources : Ljava/util/ArrayList;
    //   101: astore #9
    //   103: aload #9
    //   105: iload #8
    //   107: invokevirtual get : (I)Ljava/lang/Object;
    //   110: checkcast java/lang/ref/WeakReference
    //   113: astore #9
    //   115: aload #9
    //   117: invokevirtual get : ()Ljava/lang/Object;
    //   120: checkcast android/content/res/Resources
    //   123: astore #10
    //   125: aload #10
    //   127: ifnonnull -> 133
    //   130: goto -> 161
    //   133: aload_0
    //   134: aload #10
    //   136: aload #6
    //   138: aload_3
    //   139: iload #4
    //   141: invokespecial rebaseActivityOverrideConfig : (Landroid/content/res/Resources;Landroid/content/res/Configuration;Landroid/content/res/Configuration;I)Landroid/content/res/ResourcesKey;
    //   144: astore #9
    //   146: aload #9
    //   148: ifnull -> 161
    //   151: aload_0
    //   152: aload_1
    //   153: aload #10
    //   155: aload #9
    //   157: iconst_0
    //   158: invokespecial updateActivityResources : (Ljava/lang/String;Landroid/content/res/Resources;Landroid/content/res/ResourcesKey;Z)V
    //   161: iinc #8, 1
    //   164: goto -> 90
    //   167: aload_0
    //   168: monitorexit
    //   169: ldc2_w 8192
    //   172: invokestatic traceEnd : (J)V
    //   175: return
    //   176: astore_1
    //   177: aload_0
    //   178: monitorexit
    //   179: aload_1
    //   180: athrow
    //   181: astore_1
    //   182: ldc2_w 8192
    //   185: invokestatic traceEnd : (J)V
    //   188: aload_1
    //   189: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #968	-> 0
    //   #970	-> 9
    //   #971	-> 11
    //   #972	-> 11
    //   #974	-> 17
    //   #977	-> 33
    //   #1029	-> 35
    //   #977	-> 41
    //   #982	-> 42
    //   #985	-> 56
    //   #986	-> 60
    //   #988	-> 71
    //   #1005	-> 78
    //   #1006	-> 87
    //   #1007	-> 97
    //   #1008	-> 103
    //   #1010	-> 115
    //   #1011	-> 125
    //   #1012	-> 130
    //   #1015	-> 133
    //   #1017	-> 146
    //   #1023	-> 151
    //   #1006	-> 161
    //   #1027	-> 167
    //   #1029	-> 169
    //   #1030	-> 175
    //   #1031	-> 175
    //   #1027	-> 176
    //   #1029	-> 181
    //   #1030	-> 188
    // Exception table:
    //   from	to	target	type
    //   0	9	181	finally
    //   9	11	181	finally
    //   11	17	176	finally
    //   17	28	176	finally
    //   33	35	176	finally
    //   42	56	176	finally
    //   60	68	176	finally
    //   71	78	176	finally
    //   78	87	176	finally
    //   97	103	176	finally
    //   103	115	176	finally
    //   115	125	176	finally
    //   133	146	176	finally
    //   151	161	176	finally
    //   167	169	176	finally
    //   177	179	176	finally
    //   179	181	181	finally
  }
  
  private ResourcesKey rebaseActivityOverrideConfig(Resources paramResources, Configuration paramConfiguration1, Configuration paramConfiguration2, int paramInt) {
    StringBuilder stringBuilder;
    ResourcesKey resourcesKey = findKeyForResourceImplLocked(paramResources.getImpl());
    if (resourcesKey == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("can't find ResourcesKey for resources impl=");
      stringBuilder.append(paramResources.getImpl());
      String str = stringBuilder.toString();
      Slog.e("ResourcesManager", str);
      return null;
    } 
    Configuration configuration = new Configuration();
    if (paramConfiguration2 != null)
      configuration.setTo(paramConfiguration2); 
    boolean bool = stringBuilder.equals(Configuration.EMPTY);
    if ((bool ^ true) != 0 && resourcesKey.hasOverrideConfiguration()) {
      Configuration configuration1 = Configuration.generateDelta((Configuration)stringBuilder, resourcesKey.mOverrideConfiguration);
      configuration.updateFrom(configuration1);
    } 
    return new ResourcesKey(resourcesKey.mResDir, resourcesKey.mSplitResDirs, resourcesKey.mOverlayDirs, resourcesKey.mLibDirs, paramInt, configuration, resourcesKey.mCompatInfo, resourcesKey.mLoaders);
  }
  
  private void updateActivityResources(String paramString, Resources paramResources, ResourcesKey paramResourcesKey, boolean paramBoolean) {
    ResourcesImpl resourcesImpl;
    if (paramBoolean) {
      resourcesImpl = createResourcesImpl(paramString, paramResourcesKey);
    } else {
      resourcesImpl = findOrCreateResourcesImplForKeyLocked((String)resourcesImpl, paramResourcesKey);
    } 
    if (resourcesImpl != null && resourcesImpl != paramResources.getImpl())
      paramResources.setImpl(resourcesImpl); 
  }
  
  public final boolean applyConfigurationToResources(Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: aload_2
    //   5: invokevirtual applyConfigurationToResourcesLocked : (Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z
    //   8: istore_3
    //   9: aload_0
    //   10: monitorexit
    //   11: iload_3
    //   12: ireturn
    //   13: astore_1
    //   14: aload_0
    //   15: monitorexit
    //   16: aload_1
    //   17: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1115	-> 0
    //   #1116	-> 2
    //   #1117	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	11	13	finally
    //   14	16	13	finally
  }
  
  public final boolean applyConfigurationToResourcesLocked(Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo) {
    // Byte code:
    //   0: ldc2_w 8192
    //   3: ldc_w 'ResourcesManager#applyConfigurationToResourcesLocked'
    //   6: invokestatic traceBegin : (JLjava/lang/String;)V
    //   9: aload_0
    //   10: getfield mResConfiguration : Landroid/content/res/Configuration;
    //   13: aload_1
    //   14: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   17: istore_3
    //   18: iconst_0
    //   19: istore #4
    //   21: iload_3
    //   22: ifne -> 98
    //   25: aload_2
    //   26: ifnonnull -> 98
    //   29: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   32: ifeq -> 90
    //   35: new java/lang/StringBuilder
    //   38: astore_2
    //   39: aload_2
    //   40: invokespecial <init> : ()V
    //   43: aload_2
    //   44: ldc_w 'Skipping new config: curSeq='
    //   47: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload_2
    //   52: aload_0
    //   53: getfield mResConfiguration : Landroid/content/res/Configuration;
    //   56: getfield seq : I
    //   59: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload_2
    //   64: ldc_w ', newSeq='
    //   67: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: aload_2
    //   72: aload_1
    //   73: getfield seq : I
    //   76: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: ldc 'ResourcesManager'
    //   82: aload_2
    //   83: invokevirtual toString : ()Ljava/lang/String;
    //   86: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   89: pop
    //   90: ldc2_w 8192
    //   93: invokestatic traceEnd : (J)V
    //   96: iconst_0
    //   97: ireturn
    //   98: aload_0
    //   99: getfield mResConfiguration : Landroid/content/res/Configuration;
    //   102: aload_1
    //   103: invokevirtual updateFrom : (Landroid/content/res/Configuration;)I
    //   106: istore #5
    //   108: aload_0
    //   109: getfield mAdjustedDisplays : Landroid/util/ArrayMap;
    //   112: invokevirtual clear : ()V
    //   115: aload_0
    //   116: invokevirtual getDisplayMetrics : ()Landroid/util/DisplayMetrics;
    //   119: astore #6
    //   121: iload #5
    //   123: istore #7
    //   125: aload_2
    //   126: ifnull -> 168
    //   129: aload_0
    //   130: getfield mResCompatibilityInfo : Landroid/content/res/CompatibilityInfo;
    //   133: ifnull -> 155
    //   136: aload_0
    //   137: getfield mResCompatibilityInfo : Landroid/content/res/CompatibilityInfo;
    //   140: astore #8
    //   142: iload #5
    //   144: istore #7
    //   146: aload #8
    //   148: aload_2
    //   149: invokevirtual equals : (Ljava/lang/Object;)Z
    //   152: ifne -> 168
    //   155: aload_0
    //   156: aload_2
    //   157: putfield mResCompatibilityInfo : Landroid/content/res/CompatibilityInfo;
    //   160: iload #5
    //   162: sipush #3328
    //   165: ior
    //   166: istore #7
    //   168: getstatic android/app/IOplusCommonInjector.DEFAULT : Landroid/app/IOplusCommonInjector;
    //   171: iconst_0
    //   172: anewarray java/lang/Object
    //   175: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   178: checkcast android/app/IOplusCommonInjector
    //   181: aload_1
    //   182: iload #7
    //   184: invokeinterface applyConfigurationToResourcesForResourcesManager : (Landroid/content/res/Configuration;I)V
    //   189: aload_1
    //   190: aload #6
    //   192: aload_2
    //   193: invokestatic updateSystemConfiguration : (Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;Landroid/content/res/CompatibilityInfo;)V
    //   196: invokestatic configurationChanged : ()V
    //   199: new android/content/res/Configuration
    //   202: astore #9
    //   204: aload #9
    //   206: invokespecial <init> : ()V
    //   209: aload_0
    //   210: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   213: invokevirtual size : ()I
    //   216: iconst_1
    //   217: isub
    //   218: istore #5
    //   220: iload #5
    //   222: iflt -> 310
    //   225: aload_0
    //   226: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   229: iload #5
    //   231: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   234: checkcast android/content/res/ResourcesKey
    //   237: astore #8
    //   239: aload_0
    //   240: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   243: iload #5
    //   245: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   248: checkcast java/lang/ref/WeakReference
    //   251: astore #6
    //   253: aload #6
    //   255: ifnull -> 271
    //   258: aload #6
    //   260: invokevirtual get : ()Ljava/lang/Object;
    //   263: checkcast android/content/res/ResourcesImpl
    //   266: astore #6
    //   268: goto -> 274
    //   271: aconst_null
    //   272: astore #6
    //   274: aload #6
    //   276: ifnull -> 294
    //   279: aload_0
    //   280: aload_1
    //   281: aload_2
    //   282: aload #9
    //   284: aload #8
    //   286: aload #6
    //   288: invokespecial applyConfigurationToResourcesLocked : (Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;Landroid/content/res/Configuration;Landroid/content/res/ResourcesKey;Landroid/content/res/ResourcesImpl;)V
    //   291: goto -> 304
    //   294: aload_0
    //   295: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   298: iload #5
    //   300: invokevirtual removeAt : (I)Ljava/lang/Object;
    //   303: pop
    //   304: iinc #5, -1
    //   307: goto -> 220
    //   310: iload #7
    //   312: ifeq -> 318
    //   315: iconst_1
    //   316: istore #4
    //   318: ldc2_w 8192
    //   321: invokestatic traceEnd : (J)V
    //   324: iload #4
    //   326: ireturn
    //   327: astore_1
    //   328: ldc2_w 8192
    //   331: invokestatic traceEnd : (J)V
    //   334: aload_1
    //   335: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1123	-> 0
    //   #1126	-> 9
    //   #1127	-> 29
    //   #1129	-> 90
    //   #1168	-> 90
    //   #1129	-> 96
    //   #1131	-> 98
    //   #1133	-> 108
    //   #1135	-> 115
    //   #1137	-> 121
    //   #1138	-> 142
    //   #1139	-> 155
    //   #1140	-> 160
    //   #1146	-> 168
    //   #1148	-> 189
    //   #1150	-> 196
    //   #1153	-> 199
    //   #1155	-> 209
    //   #1156	-> 225
    //   #1157	-> 239
    //   #1158	-> 253
    //   #1159	-> 274
    //   #1160	-> 279
    //   #1162	-> 294
    //   #1155	-> 304
    //   #1166	-> 310
    //   #1168	-> 318
    //   #1166	-> 324
    //   #1168	-> 327
    //   #1169	-> 334
    // Exception table:
    //   from	to	target	type
    //   0	9	327	finally
    //   9	18	327	finally
    //   29	90	327	finally
    //   98	108	327	finally
    //   108	115	327	finally
    //   115	121	327	finally
    //   129	142	327	finally
    //   146	155	327	finally
    //   155	160	327	finally
    //   168	189	327	finally
    //   189	196	327	finally
    //   196	199	327	finally
    //   199	209	327	finally
    //   209	220	327	finally
    //   225	239	327	finally
    //   239	253	327	finally
    //   258	268	327	finally
    //   279	291	327	finally
    //   294	304	327	finally
  }
  
  private void applyConfigurationToResourcesLocked(Configuration paramConfiguration1, CompatibilityInfo paramCompatibilityInfo, Configuration paramConfiguration2, ResourcesKey paramResourcesKey, ResourcesImpl paramResourcesImpl) {
    if (ActivityThread.DEBUG_CONFIGURATION) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Changing resources ");
      stringBuilder.append(paramResourcesImpl);
      stringBuilder.append(" config to: ");
      stringBuilder.append(paramConfiguration1);
      Slog.v("ResourcesManager", stringBuilder.toString());
    } 
    paramConfiguration2.setTo(paramConfiguration1);
    boolean bool = paramResourcesKey.hasOverrideConfiguration();
    if (bool)
      paramConfiguration2.updateFrom(paramResourcesKey.mOverrideConfiguration); 
    DisplayAdjustments displayAdjustments2 = paramResourcesImpl.getDisplayAdjustments();
    DisplayAdjustments displayAdjustments1 = displayAdjustments2;
    if (paramCompatibilityInfo != null) {
      displayAdjustments1 = new DisplayAdjustments(displayAdjustments2);
      displayAdjustments1.setCompatibilityInfo(paramCompatibilityInfo);
    } 
    int i = paramResourcesKey.mDisplayId;
    if (i == 0)
      displayAdjustments1.setConfiguration(paramConfiguration2); 
    DisplayMetrics displayMetrics = getDisplayMetrics(i, displayAdjustments1);
    if (i != 0) {
      applyNonDefaultDisplayMetricsToConfiguration(displayMetrics, paramConfiguration2);
      if (bool)
        paramConfiguration2.updateFrom(paramResourcesKey.mOverrideConfiguration); 
    } 
    paramResourcesImpl.updateConfiguration(paramConfiguration2, displayMetrics, paramCompatibilityInfo);
  }
  
  public void appendLibAssetForMainAssetPath(String paramString1, String paramString2) {
    appendLibAssetsForMainAssetPath(paramString1, new String[] { paramString2 });
  }
  
  public void appendLibAssetsForMainAssetPath(String paramString, String[] paramArrayOfString) {
    appendLibAssetsForMainAssetPath(null, paramString, paramArrayOfString);
  }
  
  public void appendLibAssetsForMainAssetPath(String paramString1, String paramString2, String[] paramArrayOfString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new android/util/ArrayMap
    //   5: astore #4
    //   7: aload #4
    //   9: invokespecial <init> : ()V
    //   12: aload_0
    //   13: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   16: invokevirtual size : ()I
    //   19: istore #5
    //   21: iconst_0
    //   22: istore #6
    //   24: iload #6
    //   26: iload #5
    //   28: if_icmpge -> 239
    //   31: aload_0
    //   32: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   35: iload #6
    //   37: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   40: checkcast android/content/res/ResourcesKey
    //   43: astore #7
    //   45: aload_0
    //   46: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   49: iload #6
    //   51: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   54: checkcast java/lang/ref/WeakReference
    //   57: astore #8
    //   59: aload #8
    //   61: ifnull -> 77
    //   64: aload #8
    //   66: invokevirtual get : ()Ljava/lang/Object;
    //   69: checkcast android/content/res/ResourcesImpl
    //   72: astore #8
    //   74: goto -> 80
    //   77: aconst_null
    //   78: astore #8
    //   80: aload #8
    //   82: ifnull -> 229
    //   85: aload #7
    //   87: getfield mResDir : Ljava/lang/String;
    //   90: astore #9
    //   92: aload #9
    //   94: aload_2
    //   95: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   98: ifeq -> 229
    //   101: aload #7
    //   103: getfield mLibDirs : [Ljava/lang/String;
    //   106: astore #9
    //   108: aload_3
    //   109: arraylength
    //   110: istore #10
    //   112: iconst_0
    //   113: istore #11
    //   115: iload #11
    //   117: iload #10
    //   119: if_icmpge -> 149
    //   122: aload_3
    //   123: iload #11
    //   125: aaload
    //   126: astore #12
    //   128: ldc_w java/lang/String
    //   131: aload #9
    //   133: aload #12
    //   135: invokestatic appendElement : (Ljava/lang/Class;[Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    //   138: checkcast [Ljava/lang/String;
    //   141: astore #9
    //   143: iinc #11, 1
    //   146: goto -> 115
    //   149: aload #9
    //   151: aload #7
    //   153: getfield mLibDirs : [Ljava/lang/String;
    //   156: invokestatic equals : ([Ljava/lang/Object;[Ljava/lang/Object;)Z
    //   159: ifne -> 222
    //   162: new android/content/res/ResourcesKey
    //   165: astore #12
    //   167: aload #12
    //   169: aload #7
    //   171: getfield mResDir : Ljava/lang/String;
    //   174: aload #7
    //   176: getfield mSplitResDirs : [Ljava/lang/String;
    //   179: aload #7
    //   181: getfield mOverlayDirs : [Ljava/lang/String;
    //   184: aload #9
    //   186: aload #7
    //   188: getfield mDisplayId : I
    //   191: aload #7
    //   193: getfield mOverrideConfiguration : Landroid/content/res/Configuration;
    //   196: aload #7
    //   198: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   201: aload #7
    //   203: getfield mLoaders : [Landroid/content/res/loader/ResourcesLoader;
    //   206: invokespecial <init> : (Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;[Landroid/content/res/loader/ResourcesLoader;)V
    //   209: aload #4
    //   211: aload #8
    //   213: aload #12
    //   215: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   218: pop
    //   219: goto -> 229
    //   222: goto -> 229
    //   225: astore_1
    //   226: goto -> 236
    //   229: iinc #6, 1
    //   232: goto -> 24
    //   235: astore_1
    //   236: goto -> 250
    //   239: aload_0
    //   240: aload_1
    //   241: aload #4
    //   243: invokespecial redirectResourcesToNewImplLocked : (Ljava/lang/String;Landroid/util/ArrayMap;)V
    //   246: aload_0
    //   247: monitorexit
    //   248: return
    //   249: astore_1
    //   250: aload_0
    //   251: monitorexit
    //   252: aload_1
    //   253: athrow
    //   254: astore_1
    //   255: goto -> 250
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1245	-> 0
    //   #1248	-> 2
    //   #1250	-> 12
    //   #1251	-> 21
    //   #1252	-> 31
    //   #1253	-> 45
    //   #1254	-> 59
    //   #1255	-> 80
    //   #1256	-> 101
    //   #1257	-> 108
    //   #1258	-> 128
    //   #1259	-> 128
    //   #1257	-> 143
    //   #1262	-> 149
    //   #1263	-> 162
    //   #1262	-> 222
    //   #1284	-> 225
    //   #1255	-> 229
    //   #1251	-> 229
    //   #1284	-> 235
    //   #1251	-> 239
    //   #1281	-> 239
    //   #1284	-> 246
    //   #1285	-> 248
    //   #1284	-> 249
    // Exception table:
    //   from	to	target	type
    //   2	12	249	finally
    //   12	21	249	finally
    //   31	45	235	finally
    //   45	59	235	finally
    //   64	74	249	finally
    //   85	92	235	finally
    //   92	101	225	finally
    //   101	108	225	finally
    //   108	112	225	finally
    //   128	143	225	finally
    //   149	162	225	finally
    //   162	219	225	finally
    //   239	246	254	finally
    //   246	248	254	finally
    //   250	252	254	finally
  }
  
  final void applyNewResourceDirsLocked(ApplicationInfo paramApplicationInfo, String[] paramArrayOfString) {
    // Byte code:
    //   0: ldc2_w 8192
    //   3: ldc_w 'ResourcesManager#applyNewResourceDirsLocked'
    //   6: invokestatic traceBegin : (JLjava/lang/String;)V
    //   9: aload_1
    //   10: invokevirtual getBaseCodePath : ()Ljava/lang/String;
    //   13: astore_3
    //   14: invokestatic myUid : ()I
    //   17: istore #4
    //   19: aload_1
    //   20: getfield uid : I
    //   23: iload #4
    //   25: if_icmpne -> 37
    //   28: aload_1
    //   29: getfield splitSourceDirs : [Ljava/lang/String;
    //   32: astore #5
    //   34: goto -> 43
    //   37: aload_1
    //   38: getfield splitPublicSourceDirs : [Ljava/lang/String;
    //   41: astore #5
    //   43: aload #5
    //   45: invokestatic cloneOrNull : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   48: checkcast [Ljava/lang/String;
    //   51: astore #6
    //   53: aload_1
    //   54: getfield resourceDirs : [Ljava/lang/String;
    //   57: invokestatic cloneOrNull : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   60: checkcast [Ljava/lang/String;
    //   63: astore #7
    //   65: new android/util/ArrayMap
    //   68: astore #5
    //   70: aload #5
    //   72: invokespecial <init> : ()V
    //   75: aload_0
    //   76: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   79: invokevirtual size : ()I
    //   82: istore #4
    //   84: iconst_0
    //   85: istore #8
    //   87: aload_3
    //   88: astore_1
    //   89: iload #8
    //   91: iload #4
    //   93: if_icmpge -> 259
    //   96: aload_0
    //   97: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   100: iload #8
    //   102: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   105: checkcast android/content/res/ResourcesKey
    //   108: astore #9
    //   110: aload_0
    //   111: getfield mResourceImpls : Landroid/util/ArrayMap;
    //   114: iload #8
    //   116: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   119: checkcast java/lang/ref/WeakReference
    //   122: astore_3
    //   123: aload_3
    //   124: ifnull -> 138
    //   127: aload_3
    //   128: invokevirtual get : ()Ljava/lang/Object;
    //   131: checkcast android/content/res/ResourcesImpl
    //   134: astore_3
    //   135: goto -> 140
    //   138: aconst_null
    //   139: astore_3
    //   140: aload_3
    //   141: ifnonnull -> 147
    //   144: goto -> 253
    //   147: aload #9
    //   149: getfield mResDir : Ljava/lang/String;
    //   152: astore #10
    //   154: aload #10
    //   156: ifnull -> 204
    //   159: aload #9
    //   161: getfield mResDir : Ljava/lang/String;
    //   164: astore #10
    //   166: aload #10
    //   168: aload_1
    //   169: invokevirtual equals : (Ljava/lang/Object;)Z
    //   172: ifne -> 197
    //   175: aload #9
    //   177: getfield mResDir : Ljava/lang/String;
    //   180: astore #10
    //   182: aload_2
    //   183: aload #10
    //   185: invokestatic contains : ([Ljava/lang/Object;Ljava/lang/Object;)Z
    //   188: ifeq -> 194
    //   191: goto -> 204
    //   194: goto -> 253
    //   197: goto -> 204
    //   200: astore_1
    //   201: goto -> 273
    //   204: new android/content/res/ResourcesKey
    //   207: astore #10
    //   209: aload #10
    //   211: aload_1
    //   212: aload #6
    //   214: aload #7
    //   216: aload #9
    //   218: getfield mLibDirs : [Ljava/lang/String;
    //   221: aload #9
    //   223: getfield mDisplayId : I
    //   226: aload #9
    //   228: getfield mOverrideConfiguration : Landroid/content/res/Configuration;
    //   231: aload #9
    //   233: getfield mCompatInfo : Landroid/content/res/CompatibilityInfo;
    //   236: aload #9
    //   238: getfield mLoaders : [Landroid/content/res/loader/ResourcesLoader;
    //   241: invokespecial <init> : (Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;ILandroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;[Landroid/content/res/loader/ResourcesLoader;)V
    //   244: aload #5
    //   246: aload_3
    //   247: aload #10
    //   249: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   252: pop
    //   253: iinc #8, 1
    //   256: goto -> 89
    //   259: aload_0
    //   260: aload #5
    //   262: invokespecial redirectResourcesToNewImplLocked : (Landroid/util/ArrayMap;)V
    //   265: ldc2_w 8192
    //   268: invokestatic traceEnd : (J)V
    //   271: return
    //   272: astore_1
    //   273: ldc2_w 8192
    //   276: invokestatic traceEnd : (J)V
    //   279: aload_1
    //   280: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1291	-> 0
    //   #1294	-> 9
    //   #1296	-> 14
    //   #1297	-> 19
    //   #1298	-> 28
    //   #1299	-> 37
    //   #1302	-> 43
    //   #1303	-> 53
    //   #1305	-> 65
    //   #1306	-> 75
    //   #1307	-> 84
    //   #1308	-> 96
    //   #1309	-> 110
    //   #1310	-> 123
    //   #1312	-> 140
    //   #1313	-> 144
    //   #1316	-> 147
    //   #1317	-> 166
    //   #1318	-> 182
    //   #1317	-> 197
    //   #1334	-> 200
    //   #1316	-> 204
    //   #1319	-> 204
    //   #1307	-> 253
    //   #1332	-> 259
    //   #1334	-> 265
    //   #1335	-> 271
    //   #1336	-> 271
    //   #1334	-> 272
    //   #1335	-> 279
    // Exception table:
    //   from	to	target	type
    //   0	9	272	finally
    //   9	14	272	finally
    //   14	19	272	finally
    //   19	28	272	finally
    //   28	34	272	finally
    //   37	43	272	finally
    //   43	53	272	finally
    //   53	65	272	finally
    //   65	75	272	finally
    //   75	84	272	finally
    //   96	110	272	finally
    //   110	123	272	finally
    //   127	135	272	finally
    //   147	154	272	finally
    //   159	166	200	finally
    //   166	182	200	finally
    //   182	191	272	finally
    //   204	253	272	finally
    //   259	265	272	finally
  }
  
  private void redirectResourcesToNewImplLocked(ArrayMap<ResourcesImpl, ResourcesKey> paramArrayMap) {
    redirectResourcesToNewImplLocked(null, paramArrayMap);
  }
  
  private void redirectResourcesToNewImplLocked(String paramString, ArrayMap<ResourcesImpl, ResourcesKey> paramArrayMap) {
    if (paramArrayMap.isEmpty())
      return; 
    int i = this.mResourceReferences.size();
    byte b = 0;
    while (true) {
      Resources resources = null;
      if (b < i) {
        WeakReference<Resources> weakReference = this.mResourceReferences.get(b);
        if (weakReference != null)
          resources = weakReference.get(); 
        if (resources != null) {
          ResourcesKey resourcesKey = (ResourcesKey)paramArrayMap.get(resources.getImpl());
          if (resourcesKey != null) {
            ResourcesImpl resourcesImpl = findOrCreateResourcesImplForKeyLocked(paramString, resourcesKey);
            if (resourcesImpl != null) {
              resources.setImpl(resourcesImpl);
            } else {
              throw new Resources.NotFoundException("failed to redirect ResourcesImpl");
            } 
          } 
        } 
        b++;
        continue;
      } 
      break;
    } 
    for (ActivityResources activityResources : this.mActivityResourceReferences.values()) {
      i = activityResources.activityResources.size();
      for (b = 0; b < i; b++) {
        WeakReference<Resources> weakReference = activityResources.activityResources.get(b);
        if (weakReference != null) {
          Resources resources = weakReference.get();
        } else {
          weakReference = null;
        } 
        if (weakReference != null) {
          ResourcesKey resourcesKey = (ResourcesKey)paramArrayMap.get(weakReference.getImpl());
          if (resourcesKey != null) {
            ResourcesImpl resourcesImpl = findOrCreateResourcesImplForKeyLocked(paramString, resourcesKey);
            if (resourcesImpl != null) {
              weakReference.setImpl(resourcesImpl);
            } else {
              throw new Resources.NotFoundException("failed to redirect ResourcesImpl");
            } 
          } 
        } 
      } 
    } 
  }
  
  public boolean overrideTokenDisplayAdjustments(IBinder paramIBinder, Consumer<DisplayAdjustments> paramConsumer) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield mActivityResourceReferences : Ljava/util/WeakHashMap;
    //   8: aload_1
    //   9: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   12: checkcast android/app/ResourcesManager$ActivityResources
    //   15: astore_1
    //   16: aload_1
    //   17: ifnonnull -> 24
    //   20: aload_0
    //   21: monitorexit
    //   22: iconst_0
    //   23: ireturn
    //   24: aload_1
    //   25: getfield activityResources : Ljava/util/ArrayList;
    //   28: astore_1
    //   29: aload_1
    //   30: invokevirtual size : ()I
    //   33: iconst_1
    //   34: isub
    //   35: istore #4
    //   37: iload #4
    //   39: iflt -> 78
    //   42: aload_1
    //   43: iload #4
    //   45: invokevirtual get : (I)Ljava/lang/Object;
    //   48: checkcast java/lang/ref/WeakReference
    //   51: invokevirtual get : ()Ljava/lang/Object;
    //   54: checkcast android/content/res/Resources
    //   57: astore #5
    //   59: aload #5
    //   61: ifnull -> 72
    //   64: aload #5
    //   66: aload_2
    //   67: invokevirtual overrideDisplayAdjustments : (Ljava/util/function/Consumer;)V
    //   70: iconst_1
    //   71: istore_3
    //   72: iinc #4, -1
    //   75: goto -> 37
    //   78: aload_0
    //   79: monitorexit
    //   80: iload_3
    //   81: ireturn
    //   82: astore_1
    //   83: aload_0
    //   84: monitorexit
    //   85: aload_1
    //   86: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1418	-> 0
    //   #1419	-> 2
    //   #1420	-> 4
    //   #1421	-> 16
    //   #1422	-> 20
    //   #1424	-> 24
    //   #1426	-> 29
    //   #1427	-> 42
    //   #1428	-> 59
    //   #1429	-> 64
    //   #1430	-> 70
    //   #1426	-> 72
    //   #1433	-> 78
    //   #1434	-> 80
    //   #1433	-> 82
    // Exception table:
    //   from	to	target	type
    //   4	16	82	finally
    //   20	22	82	finally
    //   24	29	82	finally
    //   29	37	82	finally
    //   42	59	82	finally
    //   64	70	82	finally
    //   78	80	82	finally
    //   83	85	82	finally
  }
  
  class UpdateHandler implements Resources.UpdateCallbacks {
    final ResourcesManager this$0;
    
    private UpdateHandler() {}
    
    public void onLoadersChanged(Resources param1Resources, List<ResourcesLoader> param1List) {
      synchronized (ResourcesManager.this) {
        ResourcesKey resourcesKey = ResourcesManager.this.findKeyForResourceImplLocked(param1Resources.getImpl());
        if (resourcesKey != null) {
          ResourcesKey resourcesKey1 = new ResourcesKey();
          String str = resourcesKey.mResDir, arrayOfString1[] = resourcesKey.mSplitResDirs, arrayOfString2[] = resourcesKey.mOverlayDirs, arrayOfString3[] = resourcesKey.mLibDirs;
          int i = resourcesKey.mDisplayId;
          Configuration configuration = resourcesKey.mOverrideConfiguration;
          CompatibilityInfo compatibilityInfo = resourcesKey.mCompatInfo;
          this(str, arrayOfString1, arrayOfString2, arrayOfString3, i, configuration, compatibilityInfo, param1List.<ResourcesLoader>toArray(new ResourcesLoader[0]));
          ResourcesImpl resourcesImpl = ResourcesManager.this.findOrCreateResourcesImplForKeyLocked(resourcesKey1);
          param1Resources.setImpl(resourcesImpl);
          return;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Cannot modify resource loaders of ResourcesImpl not registered with ResourcesManager");
        throw illegalArgumentException;
      } 
    }
    
    public void onLoaderUpdated(ResourcesLoader param1ResourcesLoader) {
      synchronized (ResourcesManager.this) {
        ArrayMap arrayMap = new ArrayMap();
        this();
        for (int i = ResourcesManager.this.mResourceImpls.size() - 1; i >= 0; i--) {
          ResourcesKey resourcesKey = (ResourcesKey)ResourcesManager.this.mResourceImpls.keyAt(i);
          WeakReference<ResourcesImpl> weakReference = (WeakReference)ResourcesManager.this.mResourceImpls.valueAt(i);
          if (weakReference != null && weakReference.get() != null) {
            ResourcesLoader[] arrayOfResourcesLoader = resourcesKey.mLoaders;
            if (ArrayUtils.contains((Object[])arrayOfResourcesLoader, param1ResourcesLoader)) {
              ResourcesManager.this.mResourceImpls.remove(resourcesKey);
              arrayMap.put(weakReference.get(), resourcesKey);
            } 
          } 
        } 
        ResourcesManager.this.redirectResourcesToNewImplLocked(arrayMap);
        return;
      } 
    }
  }
}
