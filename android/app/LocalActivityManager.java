package android.app;

import android.app.servertransaction.PendingTransactionActions;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Window;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Deprecated
public class LocalActivityManager {
  static final int CREATED = 2;
  
  static final int DESTROYED = 5;
  
  static final int INITIALIZING = 1;
  
  static final int RESTORED = 0;
  
  static final int RESUMED = 4;
  
  static final int STARTED = 3;
  
  private static final String TAG = "LocalActivityManager";
  
  private static final boolean localLOGV = false;
  
  class LocalActivityRecord extends Binder {
    Activity activity;
    
    ActivityInfo activityInfo;
    
    int curState;
    
    final String id;
    
    Bundle instanceState;
    
    Intent intent;
    
    Window window;
    
    LocalActivityRecord(LocalActivityManager this$0, Intent param1Intent) {
      this.curState = 0;
      this.id = (String)this$0;
      this.intent = param1Intent;
    }
  }
  
  private final Map<String, LocalActivityRecord> mActivities = new HashMap<>();
  
  private final ArrayList<LocalActivityRecord> mActivityArray = new ArrayList<>();
  
  private final ActivityThread mActivityThread;
  
  private int mCurState = 1;
  
  private boolean mFinishing;
  
  private final Activity mParent;
  
  private LocalActivityRecord mResumed;
  
  private boolean mSingleMode;
  
  public LocalActivityManager(Activity paramActivity, boolean paramBoolean) {
    this.mActivityThread = ActivityThread.currentActivityThread();
    this.mParent = paramActivity;
    this.mSingleMode = paramBoolean;
  }
  
  private void moveToState(LocalActivityRecord paramLocalActivityRecord, int paramInt) {
    if (paramLocalActivityRecord.curState == 0 || paramLocalActivityRecord.curState == 5)
      return; 
    if (paramLocalActivityRecord.curState == 1) {
      Activity activity = this.mParent;
      HashMap<String, Object> hashMap = activity.getLastNonConfigurationChildInstances();
      if (hashMap != null) {
        hashMap = (HashMap<String, Object>)hashMap.get(paramLocalActivityRecord.id);
      } else {
        hashMap = null;
      } 
      if (hashMap != null) {
        Activity.NonConfigurationInstances nonConfigurationInstances2 = new Activity.NonConfigurationInstances();
        nonConfigurationInstances2.activity = hashMap;
        Activity.NonConfigurationInstances nonConfigurationInstances1 = nonConfigurationInstances2;
      } else {
        hashMap = null;
      } 
      if (paramLocalActivityRecord.activityInfo == null)
        paramLocalActivityRecord.activityInfo = this.mActivityThread.resolveActivityInfo(paramLocalActivityRecord.intent); 
      paramLocalActivityRecord.activity = this.mActivityThread.startActivityNow(this.mParent, paramLocalActivityRecord.id, paramLocalActivityRecord.intent, paramLocalActivityRecord.activityInfo, (IBinder)paramLocalActivityRecord, paramLocalActivityRecord.instanceState, (Activity.NonConfigurationInstances)hashMap, (IBinder)paramLocalActivityRecord);
      if (paramLocalActivityRecord.activity == null)
        return; 
      paramLocalActivityRecord.window = paramLocalActivityRecord.activity.getWindow();
      paramLocalActivityRecord.instanceState = null;
      ActivityThread.ActivityClientRecord activityClientRecord = this.mActivityThread.getActivityClient((IBinder)paramLocalActivityRecord);
      if (!paramLocalActivityRecord.activity.mFinished) {
        PendingTransactionActions pendingTransactionActions = new PendingTransactionActions();
        pendingTransactionActions.setOldState(activityClientRecord.state);
        pendingTransactionActions.setRestoreInstanceState(true);
        pendingTransactionActions.setCallOnPostCreate(true);
      } else {
        hashMap = null;
      } 
      this.mActivityThread.handleStartActivity((IBinder)paramLocalActivityRecord, (PendingTransactionActions)hashMap);
      paramLocalActivityRecord.curState = 3;
      if (paramInt == 4) {
        this.mActivityThread.performResumeActivity((IBinder)paramLocalActivityRecord, true, "moveToState-INITIALIZING");
        paramLocalActivityRecord.curState = 4;
      } 
      return;
    } 
    int i = paramLocalActivityRecord.curState;
    if (i != 2) {
      if (i != 3) {
        if (i != 4)
          return; 
        if (paramInt == 3) {
          performPause(paramLocalActivityRecord, this.mFinishing);
          paramLocalActivityRecord.curState = 3;
        } 
        if (paramInt == 2) {
          performPause(paramLocalActivityRecord, this.mFinishing);
          this.mActivityThread.performStopActivity((IBinder)paramLocalActivityRecord, false, "moveToState-RESUMED");
          paramLocalActivityRecord.curState = 2;
        } 
        return;
      } 
      if (paramInt == 4) {
        this.mActivityThread.performResumeActivity((IBinder)paramLocalActivityRecord, true, "moveToState-STARTED");
        paramLocalActivityRecord.instanceState = null;
        paramLocalActivityRecord.curState = 4;
      } 
      if (paramInt == 2) {
        this.mActivityThread.performStopActivity((IBinder)paramLocalActivityRecord, false, "moveToState-STARTED");
        paramLocalActivityRecord.curState = 2;
      } 
      return;
    } 
    if (paramInt == 3) {
      this.mActivityThread.performRestartActivity((IBinder)paramLocalActivityRecord, true);
      paramLocalActivityRecord.curState = 3;
    } 
    if (paramInt == 4) {
      this.mActivityThread.performRestartActivity((IBinder)paramLocalActivityRecord, true);
      this.mActivityThread.performResumeActivity((IBinder)paramLocalActivityRecord, true, "moveToState-CREATED");
      paramLocalActivityRecord.curState = 4;
    } 
  }
  
  private void performPause(LocalActivityRecord paramLocalActivityRecord, boolean paramBoolean) {
    boolean bool;
    if (paramLocalActivityRecord.instanceState == null) {
      bool = true;
    } else {
      bool = false;
    } 
    Bundle bundle = this.mActivityThread.performPauseActivity((IBinder)paramLocalActivityRecord, paramBoolean, "performPause", (PendingTransactionActions)null);
    if (bool)
      paramLocalActivityRecord.instanceState = bundle; 
  }
  
  public Window startActivity(String paramString, Intent paramIntent) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCurState : I
    //   4: iconst_1
    //   5: if_icmpeq -> 489
    //   8: iconst_0
    //   9: istore_3
    //   10: iconst_0
    //   11: istore #4
    //   13: aconst_null
    //   14: astore #5
    //   16: aload_0
    //   17: getfield mActivities : Ljava/util/Map;
    //   20: aload_1
    //   21: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   26: checkcast android/app/LocalActivityManager$LocalActivityRecord
    //   29: astore #6
    //   31: aload #6
    //   33: ifnonnull -> 57
    //   36: new android/app/LocalActivityManager$LocalActivityRecord
    //   39: dup
    //   40: aload_1
    //   41: aload_2
    //   42: invokespecial <init> : (Ljava/lang/String;Landroid/content/Intent;)V
    //   45: astore #7
    //   47: iconst_1
    //   48: istore #8
    //   50: aload #5
    //   52: astore #9
    //   54: goto -> 125
    //   57: iload_3
    //   58: istore #8
    //   60: aload #5
    //   62: astore #9
    //   64: aload #6
    //   66: astore #7
    //   68: aload #6
    //   70: getfield intent : Landroid/content/Intent;
    //   73: ifnull -> 125
    //   76: aload #6
    //   78: getfield intent : Landroid/content/Intent;
    //   81: aload_2
    //   82: invokevirtual filterEquals : (Landroid/content/Intent;)Z
    //   85: istore #10
    //   87: iload_3
    //   88: istore #8
    //   90: iload #10
    //   92: istore #4
    //   94: aload #5
    //   96: astore #9
    //   98: aload #6
    //   100: astore #7
    //   102: iload #10
    //   104: ifeq -> 125
    //   107: aload #6
    //   109: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   112: astore #9
    //   114: aload #6
    //   116: astore #7
    //   118: iload #10
    //   120: istore #4
    //   122: iload_3
    //   123: istore #8
    //   125: aload #9
    //   127: astore #6
    //   129: aload #9
    //   131: ifnonnull -> 144
    //   134: aload_0
    //   135: getfield mActivityThread : Landroid/app/ActivityThread;
    //   138: aload_2
    //   139: invokevirtual resolveActivityInfo : (Landroid/content/Intent;)Landroid/content/pm/ActivityInfo;
    //   142: astore #6
    //   144: aload_0
    //   145: getfield mSingleMode : Z
    //   148: ifeq -> 184
    //   151: aload_0
    //   152: getfield mResumed : Landroid/app/LocalActivityManager$LocalActivityRecord;
    //   155: astore #9
    //   157: aload #9
    //   159: ifnull -> 184
    //   162: aload #9
    //   164: aload #7
    //   166: if_acmpeq -> 184
    //   169: aload_0
    //   170: getfield mCurState : I
    //   173: iconst_4
    //   174: if_icmpne -> 184
    //   177: aload_0
    //   178: aload #9
    //   180: iconst_3
    //   181: invokespecial moveToState : (Landroid/app/LocalActivityManager$LocalActivityRecord;I)V
    //   184: iload #8
    //   186: ifeq -> 215
    //   189: aload_0
    //   190: getfield mActivities : Ljava/util/Map;
    //   193: aload_1
    //   194: aload #7
    //   196: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   201: pop
    //   202: aload_0
    //   203: getfield mActivityArray : Ljava/util/ArrayList;
    //   206: aload #7
    //   208: invokevirtual add : (Ljava/lang/Object;)Z
    //   211: pop
    //   212: goto -> 441
    //   215: aload #7
    //   217: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   220: ifnull -> 441
    //   223: aload #6
    //   225: aload #7
    //   227: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   230: if_acmpeq -> 283
    //   233: aload #6
    //   235: getfield name : Ljava/lang/String;
    //   238: astore_1
    //   239: aload #7
    //   241: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   244: getfield name : Ljava/lang/String;
    //   247: astore #9
    //   249: aload_1
    //   250: aload #9
    //   252: invokevirtual equals : (Ljava/lang/Object;)Z
    //   255: ifeq -> 356
    //   258: aload #6
    //   260: getfield packageName : Ljava/lang/String;
    //   263: astore #9
    //   265: aload #7
    //   267: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   270: getfield packageName : Ljava/lang/String;
    //   273: astore_1
    //   274: aload #9
    //   276: aload_1
    //   277: invokevirtual equals : (Ljava/lang/Object;)Z
    //   280: ifeq -> 356
    //   283: aload #6
    //   285: getfield launchMode : I
    //   288: ifne -> 367
    //   291: aload_2
    //   292: invokevirtual getFlags : ()I
    //   295: ldc_w 536870912
    //   298: iand
    //   299: ifeq -> 305
    //   302: goto -> 367
    //   305: iload #4
    //   307: ifeq -> 356
    //   310: aload_2
    //   311: invokevirtual getFlags : ()I
    //   314: ldc_w 67108864
    //   317: iand
    //   318: ifne -> 356
    //   321: aload #7
    //   323: aload_2
    //   324: putfield intent : Landroid/content/Intent;
    //   327: aload_0
    //   328: aload #7
    //   330: aload_0
    //   331: getfield mCurState : I
    //   334: invokespecial moveToState : (Landroid/app/LocalActivityManager$LocalActivityRecord;I)V
    //   337: aload_0
    //   338: getfield mSingleMode : Z
    //   341: ifeq -> 350
    //   344: aload_0
    //   345: aload #7
    //   347: putfield mResumed : Landroid/app/LocalActivityManager$LocalActivityRecord;
    //   350: aload #7
    //   352: getfield window : Landroid/view/Window;
    //   355: areturn
    //   356: aload_0
    //   357: aload #7
    //   359: iconst_1
    //   360: invokespecial performDestroy : (Landroid/app/LocalActivityManager$LocalActivityRecord;Z)Landroid/view/Window;
    //   363: pop
    //   364: goto -> 441
    //   367: new java/util/ArrayList
    //   370: dup
    //   371: iconst_1
    //   372: invokespecial <init> : (I)V
    //   375: astore_1
    //   376: aload_1
    //   377: new com/android/internal/content/ReferrerIntent
    //   380: dup
    //   381: aload_2
    //   382: aload_0
    //   383: getfield mParent : Landroid/app/Activity;
    //   386: invokevirtual getPackageName : ()Ljava/lang/String;
    //   389: invokespecial <init> : (Landroid/content/Intent;Ljava/lang/String;)V
    //   392: invokevirtual add : (Ljava/lang/Object;)Z
    //   395: pop
    //   396: aload_0
    //   397: getfield mActivityThread : Landroid/app/ActivityThread;
    //   400: aload #7
    //   402: aload_1
    //   403: invokevirtual handleNewIntent : (Landroid/os/IBinder;Ljava/util/List;)V
    //   406: aload #7
    //   408: aload_2
    //   409: putfield intent : Landroid/content/Intent;
    //   412: aload_0
    //   413: aload #7
    //   415: aload_0
    //   416: getfield mCurState : I
    //   419: invokespecial moveToState : (Landroid/app/LocalActivityManager$LocalActivityRecord;I)V
    //   422: aload_0
    //   423: getfield mSingleMode : Z
    //   426: ifeq -> 435
    //   429: aload_0
    //   430: aload #7
    //   432: putfield mResumed : Landroid/app/LocalActivityManager$LocalActivityRecord;
    //   435: aload #7
    //   437: getfield window : Landroid/view/Window;
    //   440: areturn
    //   441: aload #7
    //   443: aload_2
    //   444: putfield intent : Landroid/content/Intent;
    //   447: aload #7
    //   449: iconst_1
    //   450: putfield curState : I
    //   453: aload #7
    //   455: aload #6
    //   457: putfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   460: aload_0
    //   461: aload #7
    //   463: aload_0
    //   464: getfield mCurState : I
    //   467: invokespecial moveToState : (Landroid/app/LocalActivityManager$LocalActivityRecord;I)V
    //   470: aload_0
    //   471: getfield mSingleMode : Z
    //   474: ifeq -> 483
    //   477: aload_0
    //   478: aload #7
    //   480: putfield mResumed : Landroid/app/LocalActivityManager$LocalActivityRecord;
    //   483: aload #7
    //   485: getfield window : Landroid/view/Window;
    //   488: areturn
    //   489: new java/lang/IllegalStateException
    //   492: dup
    //   493: ldc_w 'Activities can't be added until the containing group has been created.'
    //   496: invokespecial <init> : (Ljava/lang/String;)V
    //   499: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #300	-> 0
    //   #305	-> 8
    //   #306	-> 10
    //   #308	-> 13
    //   #311	-> 16
    //   #312	-> 31
    //   #314	-> 36
    //   #315	-> 47
    //   #316	-> 57
    //   #317	-> 76
    //   #318	-> 87
    //   #320	-> 107
    //   #323	-> 125
    //   #324	-> 134
    //   #329	-> 144
    //   #330	-> 151
    //   #334	-> 157
    //   #335	-> 177
    //   #339	-> 184
    //   #341	-> 189
    //   #342	-> 202
    //   #343	-> 215
    //   #346	-> 223
    //   #347	-> 249
    //   #348	-> 274
    //   #349	-> 283
    //   #350	-> 291
    //   #363	-> 305
    //   #364	-> 310
    //   #367	-> 321
    //   #368	-> 327
    //   #369	-> 337
    //   #370	-> 344
    //   #372	-> 350
    //   #379	-> 356
    //   #352	-> 367
    //   #353	-> 376
    //   #355	-> 396
    //   #356	-> 406
    //   #357	-> 412
    //   #358	-> 422
    //   #359	-> 429
    //   #361	-> 435
    //   #382	-> 441
    //   #383	-> 447
    //   #384	-> 453
    //   #386	-> 460
    //   #389	-> 470
    //   #390	-> 477
    //   #392	-> 483
    //   #301	-> 489
  }
  
  private Window performDestroy(LocalActivityRecord paramLocalActivityRecord, boolean paramBoolean) {
    Window window = paramLocalActivityRecord.window;
    if (paramLocalActivityRecord.curState == 4 && !paramBoolean)
      performPause(paramLocalActivityRecord, paramBoolean); 
    this.mActivityThread.performDestroyActivity((IBinder)paramLocalActivityRecord, paramBoolean, 0, false, "LocalActivityManager::performDestroy");
    paramLocalActivityRecord.activity = null;
    paramLocalActivityRecord.window = null;
    if (paramBoolean)
      paramLocalActivityRecord.instanceState = null; 
    paramLocalActivityRecord.curState = 5;
    return window;
  }
  
  public Window destroyActivity(String paramString, boolean paramBoolean) {
    LocalActivityRecord localActivityRecord = this.mActivities.get(paramString);
    Window window = null;
    if (localActivityRecord != null) {
      Window window1 = performDestroy(localActivityRecord, paramBoolean);
      window = window1;
      if (paramBoolean) {
        this.mActivities.remove(paramString);
        this.mActivityArray.remove(localActivityRecord);
        window = window1;
      } 
    } 
    return window;
  }
  
  public Activity getCurrentActivity() {
    LocalActivityRecord localActivityRecord = this.mResumed;
    if (localActivityRecord != null) {
      Activity activity = localActivityRecord.activity;
    } else {
      localActivityRecord = null;
    } 
    return (Activity)localActivityRecord;
  }
  
  public String getCurrentId() {
    LocalActivityRecord localActivityRecord = this.mResumed;
    if (localActivityRecord != null) {
      String str = localActivityRecord.id;
    } else {
      localActivityRecord = null;
    } 
    return (String)localActivityRecord;
  }
  
  public Activity getActivity(String paramString) {
    LocalActivityRecord localActivityRecord = this.mActivities.get(paramString);
    if (localActivityRecord != null) {
      Activity activity = localActivityRecord.activity;
    } else {
      localActivityRecord = null;
    } 
    return (Activity)localActivityRecord;
  }
  
  public void dispatchCreate(Bundle paramBundle) {
    if (paramBundle != null)
      for (String str : paramBundle.keySet()) {
        try {
          Bundle bundle = paramBundle.getBundle(str);
          LocalActivityRecord localActivityRecord = this.mActivities.get(str);
          if (localActivityRecord != null) {
            localActivityRecord.instanceState = bundle;
            continue;
          } 
          localActivityRecord = new LocalActivityRecord();
          this(str, null);
          localActivityRecord.instanceState = bundle;
          this.mActivities.put(str, localActivityRecord);
          this.mActivityArray.add(localActivityRecord);
        } catch (Exception exception) {
          Log.e("LocalActivityManager", "Exception thrown when restoring LocalActivityManager state", exception);
        } 
      }  
    this.mCurState = 2;
  }
  
  public Bundle saveInstanceState() {
    Bundle bundle = null;
    int i = this.mActivityArray.size();
    for (byte b = 0; b < i; b++, bundle = bundle1) {
      LocalActivityRecord localActivityRecord = this.mActivityArray.get(b);
      Bundle bundle1 = bundle;
      if (bundle == null)
        bundle1 = new Bundle(); 
      if ((localActivityRecord.instanceState != null || localActivityRecord.curState == 4) && localActivityRecord.activity != null) {
        bundle = new Bundle();
        localActivityRecord.activity.performSaveInstanceState(bundle);
        localActivityRecord.instanceState = bundle;
      } 
      if (localActivityRecord.instanceState != null)
        bundle1.putBundle(localActivityRecord.id, localActivityRecord.instanceState); 
    } 
    return bundle;
  }
  
  public void dispatchResume() {
    this.mCurState = 4;
    if (this.mSingleMode) {
      LocalActivityRecord localActivityRecord = this.mResumed;
      if (localActivityRecord != null)
        moveToState(localActivityRecord, 4); 
    } else {
      int i = this.mActivityArray.size();
      for (byte b = 0; b < i; b++)
        moveToState(this.mActivityArray.get(b), 4); 
    } 
  }
  
  public void dispatchPause(boolean paramBoolean) {
    if (paramBoolean)
      this.mFinishing = true; 
    this.mCurState = 3;
    if (this.mSingleMode) {
      LocalActivityRecord localActivityRecord = this.mResumed;
      if (localActivityRecord != null)
        moveToState(localActivityRecord, 3); 
    } else {
      int i = this.mActivityArray.size();
      for (byte b = 0; b < i; b++) {
        LocalActivityRecord localActivityRecord = this.mActivityArray.get(b);
        if (localActivityRecord.curState == 4)
          moveToState(localActivityRecord, 3); 
      } 
    } 
  }
  
  public void dispatchStop() {
    this.mCurState = 2;
    int i = this.mActivityArray.size();
    for (byte b = 0; b < i; b++) {
      LocalActivityRecord localActivityRecord = this.mActivityArray.get(b);
      moveToState(localActivityRecord, 2);
    } 
  }
  
  public HashMap<String, Object> dispatchRetainNonConfigurationInstance() {
    HashMap<Object, Object> hashMap = null;
    int i = this.mActivityArray.size();
    for (byte b = 0; b < i; b++, hashMap = hashMap1) {
      LocalActivityRecord localActivityRecord = this.mActivityArray.get(b);
      HashMap<Object, Object> hashMap1 = hashMap;
      if (localActivityRecord != null) {
        hashMap1 = hashMap;
        if (localActivityRecord.activity != null) {
          Object object = localActivityRecord.activity.onRetainNonConfigurationInstance();
          hashMap1 = hashMap;
          if (object != null) {
            hashMap1 = hashMap;
            if (hashMap == null)
              hashMap1 = new HashMap<>(); 
            hashMap1.put(localActivityRecord.id, object);
          } 
        } 
      } 
    } 
    return (HashMap)hashMap;
  }
  
  public void removeAllActivities() {
    dispatchDestroy(true);
  }
  
  public void dispatchDestroy(boolean paramBoolean) {
    int i = this.mActivityArray.size();
    for (byte b = 0; b < i; b++) {
      LocalActivityRecord localActivityRecord = this.mActivityArray.get(b);
      this.mActivityThread.performDestroyActivity((IBinder)localActivityRecord, paramBoolean, 0, false, "LocalActivityManager::dispatchDestroy");
    } 
    this.mActivities.clear();
    this.mActivityArray.clear();
  }
}
