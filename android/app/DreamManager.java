package android.app;

import android.content.ComponentName;
import android.content.Context;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.service.dreams.IDreamManager;

public class DreamManager {
  private final Context mContext;
  
  private final IDreamManager mService;
  
  public DreamManager(Context paramContext) throws ServiceManager.ServiceNotFoundException {
    IBinder iBinder = ServiceManager.getServiceOrThrow("dreams");
    this.mService = IDreamManager.Stub.asInterface(iBinder);
    this.mContext = paramContext;
  }
  
  public void startDream(ComponentName paramComponentName) {
    try {
      this.mService.dream();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void stopDream() {
    try {
      this.mService.awaken();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setActiveDream(ComponentName paramComponentName) {
    try {
      this.mService.setDreamComponentsForUser(this.mContext.getUserId(), new ComponentName[] { paramComponentName });
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isDreaming() {
    try {
      return this.mService.isDreaming();
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
      return false;
    } 
  }
}
