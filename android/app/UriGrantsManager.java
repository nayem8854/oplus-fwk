package android.app;

import android.content.Context;
import android.content.pm.ParceledListSlice;
import android.os.Handler;
import android.os.RemoteException;
import android.util.Singleton;

public class UriGrantsManager {
  UriGrantsManager(Context paramContext, Handler paramHandler) {
    this.mContext = paramContext;
  }
  
  public static IUriGrantsManager getService() {
    return (IUriGrantsManager)IUriGrantsManagerSingleton.get();
  }
  
  private static final Singleton<IUriGrantsManager> IUriGrantsManagerSingleton = (Singleton<IUriGrantsManager>)new Object();
  
  private final Context mContext;
  
  public void clearGrantedUriPermissions(String paramString) {
    try {
      getService().clearGrantedUriPermissions(paramString, this.mContext.getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ParceledListSlice<GrantedUriPermission> getGrantedUriPermissions(String paramString) {
    try {
      IUriGrantsManager iUriGrantsManager = getService();
      Context context = this.mContext;
      return iUriGrantsManager.getGrantedUriPermissions(paramString, context.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
