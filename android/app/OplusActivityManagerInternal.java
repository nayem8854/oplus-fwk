package android.app;

import android.os.Bundle;

public abstract class OplusActivityManagerInternal {
  public abstract int startActivityAsUserEmpty(Bundle paramBundle);
}
