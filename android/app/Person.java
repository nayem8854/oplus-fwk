package android.app;

import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class Person implements Parcelable {
  private Person(Parcel paramParcel) {
    this.mName = paramParcel.readCharSequence();
    if (paramParcel.readInt() != 0)
      this.mIcon = (Icon)Icon.CREATOR.createFromParcel(paramParcel); 
    this.mUri = paramParcel.readString();
    this.mKey = paramParcel.readString();
    this.mIsImportant = paramParcel.readBoolean();
    this.mIsBot = paramParcel.readBoolean();
  }
  
  private Person(Builder paramBuilder) {
    this.mName = paramBuilder.mName;
    this.mIcon = paramBuilder.mIcon;
    this.mUri = paramBuilder.mUri;
    this.mKey = paramBuilder.mKey;
    this.mIsBot = paramBuilder.mIsBot;
    this.mIsImportant = paramBuilder.mIsImportant;
  }
  
  public Builder toBuilder() {
    return new Builder();
  }
  
  public String getUri() {
    return this.mUri;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public Icon getIcon() {
    return this.mIcon;
  }
  
  public String getKey() {
    return this.mKey;
  }
  
  public boolean isBot() {
    return this.mIsBot;
  }
  
  public boolean isImportant() {
    return this.mIsImportant;
  }
  
  public String resolveToLegacyUri() {
    String str = this.mUri;
    if (str != null)
      return str; 
    if (this.mName != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("name:");
      stringBuilder.append(this.mName);
      return stringBuilder.toString();
    } 
    return "";
  }
  
  public Uri getIconUri() {
    Icon icon = this.mIcon;
    if (icon != null)
      if (icon.getType() != 4) {
        icon = this.mIcon;
        if (icon.getType() == 6)
          return this.mIcon.getUri(); 
      } else {
        return this.mIcon.getUri();
      }  
    return null;
  }
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: instanceof android/app/Person
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: ifeq -> 145
    //   11: aload_1
    //   12: checkcast android/app/Person
    //   15: astore_1
    //   16: aload_0
    //   17: getfield mName : Ljava/lang/CharSequence;
    //   20: aload_1
    //   21: getfield mName : Ljava/lang/CharSequence;
    //   24: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   27: ifeq -> 143
    //   30: aload_0
    //   31: getfield mIcon : Landroid/graphics/drawable/Icon;
    //   34: astore #4
    //   36: aload #4
    //   38: ifnonnull -> 51
    //   41: aload_1
    //   42: getfield mIcon : Landroid/graphics/drawable/Icon;
    //   45: ifnonnull -> 143
    //   48: goto -> 72
    //   51: aload_1
    //   52: getfield mIcon : Landroid/graphics/drawable/Icon;
    //   55: astore #5
    //   57: aload #5
    //   59: ifnull -> 143
    //   62: aload #4
    //   64: aload #5
    //   66: invokevirtual sameAs : (Landroid/graphics/drawable/Icon;)Z
    //   69: ifeq -> 143
    //   72: aload_0
    //   73: getfield mUri : Ljava/lang/String;
    //   76: astore #5
    //   78: aload_1
    //   79: getfield mUri : Ljava/lang/String;
    //   82: astore #4
    //   84: aload #5
    //   86: aload #4
    //   88: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   91: ifeq -> 143
    //   94: aload_0
    //   95: getfield mKey : Ljava/lang/String;
    //   98: astore #5
    //   100: aload_1
    //   101: getfield mKey : Ljava/lang/String;
    //   104: astore #4
    //   106: aload #5
    //   108: aload #4
    //   110: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   113: ifeq -> 143
    //   116: aload_0
    //   117: getfield mIsBot : Z
    //   120: aload_1
    //   121: getfield mIsBot : Z
    //   124: if_icmpne -> 143
    //   127: aload_0
    //   128: getfield mIsImportant : Z
    //   131: aload_1
    //   132: getfield mIsImportant : Z
    //   135: if_icmpne -> 143
    //   138: iconst_1
    //   139: istore_3
    //   140: goto -> 143
    //   143: iload_3
    //   144: ireturn
    //   145: iconst_0
    //   146: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #142	-> 0
    //   #143	-> 11
    //   #144	-> 16
    //   #146	-> 62
    //   #147	-> 84
    //   #148	-> 106
    //   #144	-> 143
    //   #152	-> 145
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mName, this.mIcon, this.mUri, this.mKey, Boolean.valueOf(this.mIsBot), Boolean.valueOf(this.mIsImportant) });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mName);
    if (this.mIcon != null) {
      paramParcel.writeInt(1);
      this.mIcon.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeString(this.mUri);
    paramParcel.writeString(this.mKey);
    paramParcel.writeBoolean(this.mIsImportant);
    paramParcel.writeBoolean(this.mIsBot);
  }
  
  class Builder {
    private Icon mIcon;
    
    private boolean mIsBot;
    
    private boolean mIsImportant;
    
    private String mKey;
    
    private CharSequence mName;
    
    private String mUri;
    
    public Builder() {}
    
    private Builder(Person this$0) {
      this.mName = Person.this.mName;
      this.mIcon = Person.this.mIcon;
      this.mUri = Person.this.mUri;
      this.mKey = Person.this.mKey;
      this.mIsBot = Person.this.mIsBot;
      this.mIsImportant = Person.this.mIsImportant;
    }
    
    public Builder setName(CharSequence param1CharSequence) {
      this.mName = param1CharSequence;
      return this;
    }
    
    public Builder setIcon(Icon param1Icon) {
      this.mIcon = param1Icon;
      return this;
    }
    
    public Builder setUri(String param1String) {
      this.mUri = param1String;
      return this;
    }
    
    public Builder setKey(String param1String) {
      this.mKey = param1String;
      return this;
    }
    
    public Builder setImportant(boolean param1Boolean) {
      this.mIsImportant = param1Boolean;
      return this;
    }
    
    public Builder setBot(boolean param1Boolean) {
      this.mIsBot = param1Boolean;
      return this;
    }
    
    public Person build() {
      return new Person(this);
    }
  }
  
  public static final Parcelable.Creator<Person> CREATOR = new Parcelable.Creator<Person>() {
      public Person createFromParcel(Parcel param1Parcel) {
        return new Person(param1Parcel);
      }
      
      public Person[] newArray(int param1Int) {
        return new Person[param1Int];
      }
    };
  
  private Icon mIcon;
  
  private boolean mIsBot;
  
  private boolean mIsImportant;
  
  private String mKey;
  
  private CharSequence mName;
  
  private String mUri;
}
