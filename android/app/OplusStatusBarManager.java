package android.app;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;

public class OplusStatusBarManager extends OplusBaseStatusBarManager implements IOplusStatusBarManager {
  public static final int FLAG_INPUT_METHOD_SHOW = 4;
  
  public static final int FLAG_KEYGUARD_SHOW = 1;
  
  public static final int FLAG_SCREEN_ON = 8;
  
  public static final int FLAG_SCREEN_SHOT_SHOW = 2;
  
  public static final int TOGGLE_SPLIT_SCREEN_FROM_MENU = 2;
  
  public static final int TOGGLE_SPLIT_SCREEN_FROM_NONE = -1;
  
  public static final int TOGGLE_SPLIT_SCREEN_FROM_RECENT = 3;
  
  public static final int TOGGLE_SPLIT_SCREEN_FROM_SERVICE = 1;
  
  public void registerOplusStatusBar(IOplusStatusBar paramIOplusStatusBar) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeStrongBinder(paramIOplusStatusBar.asBinder());
      this.mRemote.transact(10002, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void registerOplusClickTopCallback(IOplusClickTopCallback paramIOplusClickTopCallback) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeStrongBinder(paramIOplusClickTopCallback.asBinder());
      this.mRemote.transact(10003, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void notifyClickTop() throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      this.mRemote.transact(10004, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void unregisterOplusClickTopCallback(IOplusClickTopCallback paramIOplusClickTopCallback) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeStrongBinder(paramIOplusClickTopCallback.asBinder());
      this.mRemote.transact(10005, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean getTopIsFullscreen() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IStatusBarManager");
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void toggleSplitScreen(int paramInt) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeInt(paramInt);
      this.mRemote.transact(10007, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public boolean setStatusBarFunction(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IStatusBarManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10008, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void topIsFullscreen(boolean paramBoolean) throws RemoteException {
    boolean bool;
    Parcel parcel = Parcel.obtain();
    if (paramBoolean) {
      bool = true;
    } else {
      bool = false;
    } 
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeInt(bool);
      this.mRemote.transact(10009, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
  
  public void notifyMultiWindowFocusChanged(int paramInt) throws RemoteException {
    Parcel parcel = Parcel.obtain();
    try {
      parcel.writeInterfaceToken("android.app.IStatusBarManager");
      parcel.writeInt(paramInt);
      this.mRemote.transact(10010, parcel, null, 1);
      return;
    } finally {
      parcel.recycle();
    } 
  }
}
