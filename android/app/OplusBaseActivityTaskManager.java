package android.app;

import oplus.app.OplusCommonManager;

public abstract class OplusBaseActivityTaskManager extends OplusCommonManager {
  public OplusBaseActivityTaskManager() {
    super("activity_task");
  }
}
