package android.app.usage;

import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IStorageStatsManager extends IInterface {
  long getCacheBytes(String paramString1, String paramString2) throws RemoteException;
  
  long getCacheQuotaBytes(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  long getFreeBytes(String paramString1, String paramString2) throws RemoteException;
  
  long getTotalBytes(String paramString1, String paramString2) throws RemoteException;
  
  boolean isQuotaSupported(String paramString1, String paramString2) throws RemoteException;
  
  boolean isReservedSupported(String paramString1, String paramString2) throws RemoteException;
  
  ParceledListSlice queryCratesForPackage(String paramString1, String paramString2, int paramInt, String paramString3) throws RemoteException;
  
  ParceledListSlice queryCratesForUid(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  ParceledListSlice queryCratesForUser(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  ExternalStorageStats queryExternalStatsForUser(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  StorageStats queryStatsForPackage(String paramString1, String paramString2, int paramInt, String paramString3) throws RemoteException;
  
  StorageStats queryStatsForUid(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  StorageStats queryStatsForUser(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  class Default implements IStorageStatsManager {
    public boolean isQuotaSupported(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean isReservedSupported(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public long getTotalBytes(String param1String1, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public long getFreeBytes(String param1String1, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public long getCacheBytes(String param1String1, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public long getCacheQuotaBytes(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return 0L;
    }
    
    public StorageStats queryStatsForPackage(String param1String1, String param1String2, int param1Int, String param1String3) throws RemoteException {
      return null;
    }
    
    public StorageStats queryStatsForUid(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public StorageStats queryStatsForUser(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public ExternalStorageStats queryExternalStatsForUser(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryCratesForPackage(String param1String1, String param1String2, int param1Int, String param1String3) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryCratesForUid(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryCratesForUser(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IStorageStatsManager {
    private static final String DESCRIPTOR = "android.app.usage.IStorageStatsManager";
    
    static final int TRANSACTION_getCacheBytes = 5;
    
    static final int TRANSACTION_getCacheQuotaBytes = 6;
    
    static final int TRANSACTION_getFreeBytes = 4;
    
    static final int TRANSACTION_getTotalBytes = 3;
    
    static final int TRANSACTION_isQuotaSupported = 1;
    
    static final int TRANSACTION_isReservedSupported = 2;
    
    static final int TRANSACTION_queryCratesForPackage = 11;
    
    static final int TRANSACTION_queryCratesForUid = 12;
    
    static final int TRANSACTION_queryCratesForUser = 13;
    
    static final int TRANSACTION_queryExternalStatsForUser = 10;
    
    static final int TRANSACTION_queryStatsForPackage = 7;
    
    static final int TRANSACTION_queryStatsForUid = 8;
    
    static final int TRANSACTION_queryStatsForUser = 9;
    
    public Stub() {
      attachInterface(this, "android.app.usage.IStorageStatsManager");
    }
    
    public static IStorageStatsManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.usage.IStorageStatsManager");
      if (iInterface != null && iInterface instanceof IStorageStatsManager)
        return (IStorageStatsManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "queryCratesForUser";
        case 12:
          return "queryCratesForUid";
        case 11:
          return "queryCratesForPackage";
        case 10:
          return "queryExternalStatsForUser";
        case 9:
          return "queryStatsForUser";
        case 8:
          return "queryStatsForUid";
        case 7:
          return "queryStatsForPackage";
        case 6:
          return "getCacheQuotaBytes";
        case 5:
          return "getCacheBytes";
        case 4:
          return "getFreeBytes";
        case 3:
          return "getTotalBytes";
        case 2:
          return "isReservedSupported";
        case 1:
          break;
      } 
      return "isQuotaSupported";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str8;
        ParceledListSlice parceledListSlice3;
        String str7;
        ParceledListSlice parceledListSlice2;
        String str6;
        ParceledListSlice parceledListSlice1;
        String str5;
        ExternalStorageStats externalStorageStats;
        String str4;
        StorageStats storageStats3;
        String str3;
        StorageStats storageStats2;
        String str2;
        StorageStats storageStats1;
        String str10;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            str8 = param1Parcel1.readString();
            parceledListSlice3 = queryCratesForUser(str9, param1Int1, str8);
            param1Parcel2.writeNoException();
            if (parceledListSlice3 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 12:
            parceledListSlice3.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = parceledListSlice3.readString();
            param1Int1 = parceledListSlice3.readInt();
            str7 = parceledListSlice3.readString();
            parceledListSlice2 = queryCratesForUid(str9, param1Int1, str7);
            param1Parcel2.writeNoException();
            if (parceledListSlice2 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 11:
            parceledListSlice2.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = parceledListSlice2.readString();
            str10 = parceledListSlice2.readString();
            param1Int1 = parceledListSlice2.readInt();
            str6 = parceledListSlice2.readString();
            parceledListSlice1 = queryCratesForPackage(str9, str10, param1Int1, str6);
            param1Parcel2.writeNoException();
            if (parceledListSlice1 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 10:
            parceledListSlice1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = parceledListSlice1.readString();
            param1Int1 = parceledListSlice1.readInt();
            str5 = parceledListSlice1.readString();
            externalStorageStats = queryExternalStatsForUser(str9, param1Int1, str5);
            param1Parcel2.writeNoException();
            if (externalStorageStats != null) {
              param1Parcel2.writeInt(1);
              externalStorageStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            externalStorageStats.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = externalStorageStats.readString();
            param1Int1 = externalStorageStats.readInt();
            str4 = externalStorageStats.readString();
            storageStats3 = queryStatsForUser(str9, param1Int1, str4);
            param1Parcel2.writeNoException();
            if (storageStats3 != null) {
              param1Parcel2.writeInt(1);
              storageStats3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            storageStats3.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = storageStats3.readString();
            param1Int1 = storageStats3.readInt();
            str3 = storageStats3.readString();
            storageStats2 = queryStatsForUid(str9, param1Int1, str3);
            param1Parcel2.writeNoException();
            if (storageStats2 != null) {
              param1Parcel2.writeInt(1);
              storageStats2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            storageStats2.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = storageStats2.readString();
            str10 = storageStats2.readString();
            param1Int1 = storageStats2.readInt();
            str2 = storageStats2.readString();
            storageStats1 = queryStatsForPackage(str9, str10, param1Int1, str2);
            param1Parcel2.writeNoException();
            if (storageStats1 != null) {
              param1Parcel2.writeInt(1);
              storageStats1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            storageStats1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = storageStats1.readString();
            param1Int1 = storageStats1.readInt();
            str1 = storageStats1.readString();
            l = getCacheQuotaBytes(str9, param1Int1, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 5:
            str1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = str1.readString();
            str1 = str1.readString();
            l = getCacheBytes(str9, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 4:
            str1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = str1.readString();
            str1 = str1.readString();
            l = getFreeBytes(str9, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 3:
            str1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = str1.readString();
            str1 = str1.readString();
            l = getTotalBytes(str9, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 2:
            str1.enforceInterface("android.app.usage.IStorageStatsManager");
            str9 = str1.readString();
            str1 = str1.readString();
            bool = isReservedSupported(str9, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.app.usage.IStorageStatsManager");
        String str9 = str1.readString();
        String str1 = str1.readString();
        boolean bool = isQuotaSupported(str9, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      param1Parcel2.writeString("android.app.usage.IStorageStatsManager");
      return true;
    }
    
    private static class Proxy implements IStorageStatsManager {
      public static IStorageStatsManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.usage.IStorageStatsManager";
      }
      
      public boolean isQuotaSupported(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IStorageStatsManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageStatsManager.Stub.getDefaultImpl().isQuotaSupported(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isReservedSupported(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IStorageStatsManager.Stub.getDefaultImpl() != null) {
            bool1 = IStorageStatsManager.Stub.getDefaultImpl().isReservedSupported(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getTotalBytes(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().getTotalBytes(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getFreeBytes(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().getFreeBytes(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCacheBytes(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().getCacheBytes(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getCacheQuotaBytes(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().getCacheQuotaBytes(param2String1, param2Int, param2String2); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StorageStats queryStatsForPackage(String param2String1, String param2String2, int param2Int, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryStatsForPackage(param2String1, param2String2, param2Int, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StorageStats storageStats = (StorageStats)StorageStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (StorageStats)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StorageStats queryStatsForUid(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryStatsForUid(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StorageStats storageStats = (StorageStats)StorageStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (StorageStats)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public StorageStats queryStatsForUser(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryStatsForUser(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            StorageStats storageStats = (StorageStats)StorageStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (StorageStats)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ExternalStorageStats queryExternalStatsForUser(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryExternalStatsForUser(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ExternalStorageStats externalStorageStats = (ExternalStorageStats)ExternalStorageStats.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ExternalStorageStats)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryCratesForPackage(String param2String1, String param2String2, int param2Int, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryCratesForPackage(param2String1, param2String2, param2Int, param2String3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParceledListSlice)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryCratesForUid(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryCratesForUid(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParceledListSlice)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryCratesForUser(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IStorageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IStorageStatsManager.Stub.getDefaultImpl() != null)
            return IStorageStatsManager.Stub.getDefaultImpl().queryCratesForUser(param2String1, param2Int, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParceledListSlice)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IStorageStatsManager param1IStorageStatsManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IStorageStatsManager != null) {
          Proxy.sDefaultImpl = param1IStorageStatsManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IStorageStatsManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
