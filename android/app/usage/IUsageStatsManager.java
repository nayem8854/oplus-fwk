package android.app.usage;

import android.app.PendingIntent;
import android.content.pm.ParceledListSlice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IUsageStatsManager extends IInterface {
  void forceUsageSourceSettingRead() throws RemoteException;
  
  int getAppStandbyBucket(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  ParceledListSlice getAppStandbyBuckets(String paramString, int paramInt) throws RemoteException;
  
  int getUsageSource() throws RemoteException;
  
  boolean isAppInactive(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void onCarrierPrivilegedAppsChanged() throws RemoteException;
  
  ParceledListSlice queryConfigurationStats(int paramInt, long paramLong1, long paramLong2, String paramString) throws RemoteException;
  
  ParceledListSlice queryEventStats(int paramInt, long paramLong1, long paramLong2, String paramString) throws RemoteException;
  
  UsageEvents queryEvents(long paramLong1, long paramLong2, String paramString) throws RemoteException;
  
  UsageEvents queryEventsForPackage(long paramLong1, long paramLong2, String paramString) throws RemoteException;
  
  UsageEvents queryEventsForPackageForUser(long paramLong1, long paramLong2, int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  UsageEvents queryEventsForUser(long paramLong1, long paramLong2, int paramInt, String paramString) throws RemoteException;
  
  ParceledListSlice queryUsageStats(int paramInt, long paramLong1, long paramLong2, String paramString) throws RemoteException;
  
  void registerAppUsageLimitObserver(int paramInt, String[] paramArrayOfString, long paramLong1, long paramLong2, PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  void registerAppUsageObserver(int paramInt, String[] paramArrayOfString, long paramLong, PendingIntent paramPendingIntent, String paramString) throws RemoteException;
  
  void registerUsageSessionObserver(int paramInt, String[] paramArrayOfString, long paramLong1, long paramLong2, PendingIntent paramPendingIntent1, PendingIntent paramPendingIntent2, String paramString) throws RemoteException;
  
  void reportChooserSelection(String paramString1, int paramInt, String paramString2, String[] paramArrayOfString, String paramString3) throws RemoteException;
  
  void reportPastUsageStart(IBinder paramIBinder, String paramString1, long paramLong, String paramString2) throws RemoteException;
  
  void reportUsageStart(IBinder paramIBinder, String paramString1, String paramString2) throws RemoteException;
  
  void reportUsageStop(IBinder paramIBinder, String paramString1, String paramString2) throws RemoteException;
  
  void setAppInactive(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setAppStandbyBucket(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setAppStandbyBuckets(ParceledListSlice paramParceledListSlice, int paramInt) throws RemoteException;
  
  void unregisterAppUsageLimitObserver(int paramInt, String paramString) throws RemoteException;
  
  void unregisterAppUsageObserver(int paramInt, String paramString) throws RemoteException;
  
  void unregisterUsageSessionObserver(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IUsageStatsManager {
    public ParceledListSlice queryUsageStats(int param1Int, long param1Long1, long param1Long2, String param1String) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryConfigurationStats(int param1Int, long param1Long1, long param1Long2, String param1String) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryEventStats(int param1Int, long param1Long1, long param1Long2, String param1String) throws RemoteException {
      return null;
    }
    
    public UsageEvents queryEvents(long param1Long1, long param1Long2, String param1String) throws RemoteException {
      return null;
    }
    
    public UsageEvents queryEventsForPackage(long param1Long1, long param1Long2, String param1String) throws RemoteException {
      return null;
    }
    
    public UsageEvents queryEventsForUser(long param1Long1, long param1Long2, int param1Int, String param1String) throws RemoteException {
      return null;
    }
    
    public UsageEvents queryEventsForPackageForUser(long param1Long1, long param1Long2, int param1Int, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void setAppInactive(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public boolean isAppInactive(String param1String1, int param1Int, String param1String2) throws RemoteException {
      return false;
    }
    
    public void onCarrierPrivilegedAppsChanged() throws RemoteException {}
    
    public void reportChooserSelection(String param1String1, int param1Int, String param1String2, String[] param1ArrayOfString, String param1String3) throws RemoteException {}
    
    public int getAppStandbyBucket(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setAppStandbyBucket(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public ParceledListSlice getAppStandbyBuckets(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setAppStandbyBuckets(ParceledListSlice param1ParceledListSlice, int param1Int) throws RemoteException {}
    
    public void registerAppUsageObserver(int param1Int, String[] param1ArrayOfString, long param1Long, PendingIntent param1PendingIntent, String param1String) throws RemoteException {}
    
    public void unregisterAppUsageObserver(int param1Int, String param1String) throws RemoteException {}
    
    public void registerUsageSessionObserver(int param1Int, String[] param1ArrayOfString, long param1Long1, long param1Long2, PendingIntent param1PendingIntent1, PendingIntent param1PendingIntent2, String param1String) throws RemoteException {}
    
    public void unregisterUsageSessionObserver(int param1Int, String param1String) throws RemoteException {}
    
    public void registerAppUsageLimitObserver(int param1Int, String[] param1ArrayOfString, long param1Long1, long param1Long2, PendingIntent param1PendingIntent, String param1String) throws RemoteException {}
    
    public void unregisterAppUsageLimitObserver(int param1Int, String param1String) throws RemoteException {}
    
    public void reportUsageStart(IBinder param1IBinder, String param1String1, String param1String2) throws RemoteException {}
    
    public void reportPastUsageStart(IBinder param1IBinder, String param1String1, long param1Long, String param1String2) throws RemoteException {}
    
    public void reportUsageStop(IBinder param1IBinder, String param1String1, String param1String2) throws RemoteException {}
    
    public int getUsageSource() throws RemoteException {
      return 0;
    }
    
    public void forceUsageSourceSettingRead() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUsageStatsManager {
    private static final String DESCRIPTOR = "android.app.usage.IUsageStatsManager";
    
    static final int TRANSACTION_forceUsageSourceSettingRead = 26;
    
    static final int TRANSACTION_getAppStandbyBucket = 12;
    
    static final int TRANSACTION_getAppStandbyBuckets = 14;
    
    static final int TRANSACTION_getUsageSource = 25;
    
    static final int TRANSACTION_isAppInactive = 9;
    
    static final int TRANSACTION_onCarrierPrivilegedAppsChanged = 10;
    
    static final int TRANSACTION_queryConfigurationStats = 2;
    
    static final int TRANSACTION_queryEventStats = 3;
    
    static final int TRANSACTION_queryEvents = 4;
    
    static final int TRANSACTION_queryEventsForPackage = 5;
    
    static final int TRANSACTION_queryEventsForPackageForUser = 7;
    
    static final int TRANSACTION_queryEventsForUser = 6;
    
    static final int TRANSACTION_queryUsageStats = 1;
    
    static final int TRANSACTION_registerAppUsageLimitObserver = 20;
    
    static final int TRANSACTION_registerAppUsageObserver = 16;
    
    static final int TRANSACTION_registerUsageSessionObserver = 18;
    
    static final int TRANSACTION_reportChooserSelection = 11;
    
    static final int TRANSACTION_reportPastUsageStart = 23;
    
    static final int TRANSACTION_reportUsageStart = 22;
    
    static final int TRANSACTION_reportUsageStop = 24;
    
    static final int TRANSACTION_setAppInactive = 8;
    
    static final int TRANSACTION_setAppStandbyBucket = 13;
    
    static final int TRANSACTION_setAppStandbyBuckets = 15;
    
    static final int TRANSACTION_unregisterAppUsageLimitObserver = 21;
    
    static final int TRANSACTION_unregisterAppUsageObserver = 17;
    
    static final int TRANSACTION_unregisterUsageSessionObserver = 19;
    
    public Stub() {
      attachInterface(this, "android.app.usage.IUsageStatsManager");
    }
    
    public static IUsageStatsManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.usage.IUsageStatsManager");
      if (iInterface != null && iInterface instanceof IUsageStatsManager)
        return (IUsageStatsManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 26:
          return "forceUsageSourceSettingRead";
        case 25:
          return "getUsageSource";
        case 24:
          return "reportUsageStop";
        case 23:
          return "reportPastUsageStart";
        case 22:
          return "reportUsageStart";
        case 21:
          return "unregisterAppUsageLimitObserver";
        case 20:
          return "registerAppUsageLimitObserver";
        case 19:
          return "unregisterUsageSessionObserver";
        case 18:
          return "registerUsageSessionObserver";
        case 17:
          return "unregisterAppUsageObserver";
        case 16:
          return "registerAppUsageObserver";
        case 15:
          return "setAppStandbyBuckets";
        case 14:
          return "getAppStandbyBuckets";
        case 13:
          return "setAppStandbyBucket";
        case 12:
          return "getAppStandbyBucket";
        case 11:
          return "reportChooserSelection";
        case 10:
          return "onCarrierPrivilegedAppsChanged";
        case 9:
          return "isAppInactive";
        case 8:
          return "setAppInactive";
        case 7:
          return "queryEventsForPackageForUser";
        case 6:
          return "queryEventsForUser";
        case 5:
          return "queryEventsForPackage";
        case 4:
          return "queryEvents";
        case 3:
          return "queryEventStats";
        case 2:
          return "queryConfigurationStats";
        case 1:
          break;
      } 
      return "queryUsageStats";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str8;
        ParceledListSlice parceledListSlice4;
        String str7;
        UsageEvents usageEvents4;
        String str6;
        UsageEvents usageEvents3;
        String str5;
        UsageEvents usageEvents2;
        String str4;
        UsageEvents usageEvents1;
        String str3;
        ParceledListSlice parceledListSlice3;
        String str2;
        ParceledListSlice parceledListSlice2;
        IBinder iBinder;
        String str9, str11, arrayOfString1[], str10, arrayOfString2[];
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 26:
            param1Parcel1.enforceInterface("android.app.usage.IUsageStatsManager");
            forceUsageSourceSettingRead();
            param1Parcel2.writeNoException();
            return true;
          case 25:
            param1Parcel1.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = getUsageSource();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 24:
            param1Parcel1.enforceInterface("android.app.usage.IUsageStatsManager");
            iBinder = param1Parcel1.readStrongBinder();
            str11 = param1Parcel1.readString();
            str8 = param1Parcel1.readString();
            reportUsageStop(iBinder, str11, str8);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            iBinder = str8.readStrongBinder();
            str11 = str8.readString();
            l1 = str8.readLong();
            str8 = str8.readString();
            reportPastUsageStart(iBinder, str11, l1, str8);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            iBinder = str8.readStrongBinder();
            str11 = str8.readString();
            str8 = str8.readString();
            reportUsageStart(iBinder, str11, str8);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            str8 = str8.readString();
            unregisterAppUsageLimitObserver(param1Int1, str8);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            arrayOfString1 = str8.createStringArray();
            l2 = str8.readLong();
            l1 = str8.readLong();
            if (str8.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              iBinder = null;
            } 
            str8 = str8.readString();
            registerAppUsageLimitObserver(param1Int1, arrayOfString1, l2, l1, (PendingIntent)iBinder, str8);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            str8 = str8.readString();
            unregisterUsageSessionObserver(param1Int1, str8);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            arrayOfString2 = str8.createStringArray();
            l2 = str8.readLong();
            l1 = str8.readLong();
            if (str8.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              iBinder = null;
            } 
            if (str8.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayOfString1 = null;
            } 
            str8 = str8.readString();
            registerUsageSessionObserver(param1Int1, arrayOfString2, l2, l1, (PendingIntent)iBinder, (PendingIntent)arrayOfString1, str8);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            str8 = str8.readString();
            unregisterAppUsageObserver(param1Int1, str8);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            param1Int1 = str8.readInt();
            arrayOfString1 = str8.createStringArray();
            l1 = str8.readLong();
            if (str8.readInt() != 0) {
              PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              iBinder = null;
            } 
            str8 = str8.readString();
            registerAppUsageObserver(param1Int1, arrayOfString1, l1, (PendingIntent)iBinder, str8);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            if (str8.readInt() != 0) {
              ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)str8);
            } else {
              iBinder = null;
            } 
            param1Int1 = str8.readInt();
            setAppStandbyBuckets((ParceledListSlice)iBinder, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str8.enforceInterface("android.app.usage.IUsageStatsManager");
            str9 = str8.readString();
            param1Int1 = str8.readInt();
            parceledListSlice4 = getAppStandbyBuckets(str9, param1Int1);
            param1Parcel2.writeNoException();
            if (parceledListSlice4 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice4.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 13:
            parceledListSlice4.enforceInterface("android.app.usage.IUsageStatsManager");
            str9 = parceledListSlice4.readString();
            param1Int1 = parceledListSlice4.readInt();
            param1Int2 = parceledListSlice4.readInt();
            setAppStandbyBucket(str9, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            parceledListSlice4.enforceInterface("android.app.usage.IUsageStatsManager");
            str9 = parceledListSlice4.readString();
            str10 = parceledListSlice4.readString();
            param1Int1 = parceledListSlice4.readInt();
            param1Int1 = getAppStandbyBucket(str9, str10, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 11:
            parceledListSlice4.enforceInterface("android.app.usage.IUsageStatsManager");
            str10 = parceledListSlice4.readString();
            param1Int1 = parceledListSlice4.readInt();
            str9 = parceledListSlice4.readString();
            arrayOfString2 = parceledListSlice4.createStringArray();
            str7 = parceledListSlice4.readString();
            reportChooserSelection(str10, param1Int1, str9, arrayOfString2, str7);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str7.enforceInterface("android.app.usage.IUsageStatsManager");
            onCarrierPrivilegedAppsChanged();
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str7.enforceInterface("android.app.usage.IUsageStatsManager");
            str9 = str7.readString();
            param1Int1 = str7.readInt();
            str7 = str7.readString();
            bool = isAppInactive(str9, param1Int1, str7);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 8:
            str7.enforceInterface("android.app.usage.IUsageStatsManager");
            str9 = str7.readString();
            if (str7.readInt() != 0)
              bool1 = true; 
            i = str7.readInt();
            setAppInactive(str9, bool1, i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str7.enforceInterface("android.app.usage.IUsageStatsManager");
            l1 = str7.readLong();
            l2 = str7.readLong();
            i = str7.readInt();
            str9 = str7.readString();
            str7 = str7.readString();
            usageEvents4 = queryEventsForPackageForUser(l1, l2, i, str9, str7);
            param1Parcel2.writeNoException();
            if (usageEvents4 != null) {
              param1Parcel2.writeInt(1);
              usageEvents4.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            usageEvents4.enforceInterface("android.app.usage.IUsageStatsManager");
            l1 = usageEvents4.readLong();
            l2 = usageEvents4.readLong();
            i = usageEvents4.readInt();
            str6 = usageEvents4.readString();
            usageEvents3 = queryEventsForUser(l1, l2, i, str6);
            param1Parcel2.writeNoException();
            if (usageEvents3 != null) {
              param1Parcel2.writeInt(1);
              usageEvents3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            usageEvents3.enforceInterface("android.app.usage.IUsageStatsManager");
            l1 = usageEvents3.readLong();
            l2 = usageEvents3.readLong();
            str5 = usageEvents3.readString();
            usageEvents2 = queryEventsForPackage(l1, l2, str5);
            param1Parcel2.writeNoException();
            if (usageEvents2 != null) {
              param1Parcel2.writeInt(1);
              usageEvents2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 4:
            usageEvents2.enforceInterface("android.app.usage.IUsageStatsManager");
            l2 = usageEvents2.readLong();
            l1 = usageEvents2.readLong();
            str4 = usageEvents2.readString();
            usageEvents1 = queryEvents(l2, l1, str4);
            param1Parcel2.writeNoException();
            if (usageEvents1 != null) {
              param1Parcel2.writeInt(1);
              usageEvents1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            usageEvents1.enforceInterface("android.app.usage.IUsageStatsManager");
            i = usageEvents1.readInt();
            l1 = usageEvents1.readLong();
            l2 = usageEvents1.readLong();
            str3 = usageEvents1.readString();
            parceledListSlice3 = queryEventStats(i, l1, l2, str3);
            param1Parcel2.writeNoException();
            if (parceledListSlice3 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            parceledListSlice3.enforceInterface("android.app.usage.IUsageStatsManager");
            i = parceledListSlice3.readInt();
            l1 = parceledListSlice3.readLong();
            l2 = parceledListSlice3.readLong();
            str2 = parceledListSlice3.readString();
            parceledListSlice2 = queryConfigurationStats(i, l1, l2, str2);
            param1Parcel2.writeNoException();
            if (parceledListSlice2 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        parceledListSlice2.enforceInterface("android.app.usage.IUsageStatsManager");
        int i = parceledListSlice2.readInt();
        long l2 = parceledListSlice2.readLong();
        long l1 = parceledListSlice2.readLong();
        String str1 = parceledListSlice2.readString();
        ParceledListSlice parceledListSlice1 = queryUsageStats(i, l2, l1, str1);
        param1Parcel2.writeNoException();
        if (parceledListSlice1 != null) {
          param1Parcel2.writeInt(1);
          parceledListSlice1.writeToParcel(param1Parcel2, 1);
        } else {
          param1Parcel2.writeInt(0);
        } 
        return true;
      } 
      param1Parcel2.writeString("android.app.usage.IUsageStatsManager");
      return true;
    }
    
    private static class Proxy implements IUsageStatsManager {
      public static IUsageStatsManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.usage.IUsageStatsManager";
      }
      
      public ParceledListSlice queryUsageStats(int param2Int, long param2Long1, long param2Long2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeLong(param2Long1);
              try {
                parcel1.writeLong(param2Long2);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
                  if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                    ParceledListSlice parceledListSlice = IUsageStatsManager.Stub.getDefaultImpl().queryUsageStats(param2Int, param2Long1, param2Long2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return parceledListSlice;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2String = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (ParceledListSlice)param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public ParceledListSlice queryConfigurationStats(int param2Int, long param2Long1, long param2Long2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeLong(param2Long1);
              try {
                parcel1.writeLong(param2Long2);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                  if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                    ParceledListSlice parceledListSlice = IUsageStatsManager.Stub.getDefaultImpl().queryConfigurationStats(param2Int, param2Long1, param2Long2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return parceledListSlice;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2String = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (ParceledListSlice)param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public ParceledListSlice queryEventStats(int param2Int, long param2Long1, long param2Long2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeLong(param2Long1);
              try {
                parcel1.writeLong(param2Long2);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
                  if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                    ParceledListSlice parceledListSlice = IUsageStatsManager.Stub.getDefaultImpl().queryEventStats(param2Int, param2Long1, param2Long2, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return parceledListSlice;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2String = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (ParceledListSlice)param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public UsageEvents queryEvents(long param2Long1, long param2Long2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null)
            return IUsageStatsManager.Stub.getDefaultImpl().queryEvents(param2Long1, param2Long2, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UsageEvents usageEvents = (UsageEvents)UsageEvents.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (UsageEvents)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UsageEvents queryEventsForPackage(long param2Long1, long param2Long2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null)
            return IUsageStatsManager.Stub.getDefaultImpl().queryEventsForPackage(param2Long1, param2Long2, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            UsageEvents usageEvents = (UsageEvents)UsageEvents.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (UsageEvents)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UsageEvents queryEventsForUser(long param2Long1, long param2Long2, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeLong(param2Long1);
            try {
              parcel1.writeLong(param2Long2);
              try {
                parcel1.writeInt(param2Int);
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
                  if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                    UsageEvents usageEvents = IUsageStatsManager.Stub.getDefaultImpl().queryEventsForUser(param2Long1, param2Long2, param2Int, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return usageEvents;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    UsageEvents usageEvents = (UsageEvents)UsageEvents.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2String = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (UsageEvents)param2String;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public UsageEvents queryEventsForPackageForUser(long param2Long1, long param2Long2, int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeLong(param2Long1);
            try {
              parcel1.writeLong(param2Long2);
              try {
                parcel1.writeInt(param2Int);
                parcel1.writeString(param2String1);
                parcel1.writeString(param2String2);
                boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
                if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                  UsageEvents usageEvents = IUsageStatsManager.Stub.getDefaultImpl().queryEventsForPackageForUser(param2Long1, param2Long2, param2Int, param2String1, param2String2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return usageEvents;
                } 
                parcel2.readException();
                if (parcel2.readInt() != 0) {
                  UsageEvents usageEvents = (UsageEvents)UsageEvents.CREATOR.createFromParcel(parcel2);
                } else {
                  param2String1 = null;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return (UsageEvents)param2String1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void setAppInactive(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().setAppInactive(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAppInactive(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(9, parcel1, parcel2, 0);
          if (!bool2 && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            bool1 = IUsageStatsManager.Stub.getDefaultImpl().isAppInactive(param2String1, param2Int, param2String2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onCarrierPrivilegedAppsChanged() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().onCarrierPrivilegedAppsChanged();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportChooserSelection(String param2String1, int param2Int, String param2String2, String[] param2ArrayOfString, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().reportChooserSelection(param2String1, param2Int, param2String2, param2ArrayOfString, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getAppStandbyBucket(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            param2Int = IUsageStatsManager.Stub.getDefaultImpl().getAppStandbyBucket(param2String1, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppStandbyBucket(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().setAppStandbyBucket(param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getAppStandbyBuckets(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null)
            return IUsageStatsManager.Stub.getDefaultImpl().getAppStandbyBuckets(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAppStandbyBuckets(ParceledListSlice param2ParceledListSlice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().setAppStandbyBuckets(param2ParceledListSlice, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerAppUsageObserver(int param2Int, String[] param2ArrayOfString, long param2Long, PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeStringArray(param2ArrayOfString);
              try {
                parcel1.writeLong(param2Long);
                if (param2PendingIntent != null) {
                  parcel1.writeInt(1);
                  param2PendingIntent.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                try {
                  parcel1.writeString(param2String);
                  boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
                  if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                    IUsageStatsManager.Stub.getDefaultImpl().registerAppUsageObserver(param2Int, param2ArrayOfString, param2Long, param2PendingIntent, param2String);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfString;
      }
      
      public void unregisterAppUsageObserver(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().unregisterAppUsageObserver(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerUsageSessionObserver(int param2Int, String[] param2ArrayOfString, long param2Long1, long param2Long2, PendingIntent param2PendingIntent1, PendingIntent param2PendingIntent2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            parcel1.writeStringArray(param2ArrayOfString);
            parcel1.writeLong(param2Long1);
            parcel1.writeLong(param2Long2);
            if (param2PendingIntent1 != null) {
              parcel1.writeInt(1);
              param2PendingIntent1.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2PendingIntent2 != null) {
              parcel1.writeInt(1);
              param2PendingIntent2.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeString(param2String);
            boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
            if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
              IUsageStatsManager.Stub.getDefaultImpl().registerUsageSessionObserver(param2Int, param2ArrayOfString, param2Long1, param2Long2, param2PendingIntent1, param2PendingIntent2, param2String);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } 
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfString;
      }
      
      public void unregisterUsageSessionObserver(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().unregisterUsageSessionObserver(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerAppUsageLimitObserver(int param2Int, String[] param2ArrayOfString, long param2Long1, long param2Long2, PendingIntent param2PendingIntent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          try {
            parcel1.writeInt(param2Int);
            try {
              parcel1.writeStringArray(param2ArrayOfString);
              parcel1.writeLong(param2Long1);
              parcel1.writeLong(param2Long2);
              if (param2PendingIntent != null) {
                parcel1.writeInt(1);
                param2PendingIntent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeString(param2String);
                boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
                if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
                  IUsageStatsManager.Stub.getDefaultImpl().registerAppUsageLimitObserver(param2Int, param2ArrayOfString, param2Long1, param2Long2, param2PendingIntent, param2String);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfString;
      }
      
      public void unregisterAppUsageLimitObserver(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().unregisterAppUsageLimitObserver(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportUsageStart(IBinder param2IBinder, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().reportUsageStart(param2IBinder, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportPastUsageStart(IBinder param2IBinder, String param2String1, long param2Long, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String1);
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().reportPastUsageStart(param2IBinder, param2String1, param2Long, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportUsageStop(IBinder param2IBinder, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().reportUsageStop(param2IBinder, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUsageSource() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null)
            return IUsageStatsManager.Stub.getDefaultImpl().getUsageSource(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceUsageSourceSettingRead() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.usage.IUsageStatsManager");
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IUsageStatsManager.Stub.getDefaultImpl() != null) {
            IUsageStatsManager.Stub.getDefaultImpl().forceUsageSourceSettingRead();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUsageStatsManager param1IUsageStatsManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUsageStatsManager != null) {
          Proxy.sDefaultImpl = param1IUsageStatsManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUsageStatsManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
