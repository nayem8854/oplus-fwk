package android.app.usage;

import java.util.ArrayList;

public class EventList {
  private final ArrayList<UsageEvents.Event> mEvents = new ArrayList<>();
  
  public int size() {
    return this.mEvents.size();
  }
  
  public void clear() {
    this.mEvents.clear();
  }
  
  public UsageEvents.Event get(int paramInt) {
    return this.mEvents.get(paramInt);
  }
  
  public void insert(UsageEvents.Event paramEvent) {
    int i = this.mEvents.size();
    if (i == 0 || paramEvent.mTimeStamp >= ((UsageEvents.Event)this.mEvents.get(i - 1)).mTimeStamp) {
      this.mEvents.add(paramEvent);
      return;
    } 
    i = firstIndexOnOrAfter(paramEvent.mTimeStamp + 1L);
    this.mEvents.add(i, paramEvent);
  }
  
  public UsageEvents.Event remove(int paramInt) {
    try {
      return this.mEvents.remove(paramInt);
    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
      return null;
    } 
  }
  
  public int firstIndexOnOrAfter(long paramLong) {
    int i = this.mEvents.size();
    int j = i;
    int k = 0;
    i--;
    while (k <= i) {
      int m = k + i >>> 1;
      long l = ((UsageEvents.Event)this.mEvents.get(m)).mTimeStamp;
      if (l >= paramLong) {
        i = m - 1;
        j = m;
        continue;
      } 
      k = m + 1;
    } 
    return j;
  }
  
  public void merge(EventList paramEventList) {
    int i = paramEventList.size();
    for (byte b = 0; b < i; b++)
      insert(paramEventList.get(b)); 
  }
}
