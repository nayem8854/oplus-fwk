package android.app;

public abstract class OplusBaseActivityOptions {
  protected static final String KEY_RP_LAUNCH_HINT = "android:activity.isRPLaunch";
  
  protected static final String KEY_ZOOM_LAUNCH_FLAGS = "android:activity.mZoomLaunchFlags";
  
  protected boolean mIsRPLaunch;
  
  protected int mZoomLaunchFlags;
  
  public boolean isRPLaunch() {
    return this.mIsRPLaunch;
  }
  
  public void setRPLaunch(boolean paramBoolean) {
    this.mIsRPLaunch = paramBoolean;
  }
  
  public int getZoomLaunchFlags() {
    return this.mZoomLaunchFlags;
  }
  
  public void setZoomLaunchFlags(int paramInt) {
    this.mZoomLaunchFlags = paramInt;
  }
}
