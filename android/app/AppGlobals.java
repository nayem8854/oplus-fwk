package android.app;

import android.content.pm.IPackageManager;
import android.permission.IPermissionManager;

public class AppGlobals {
  public static Application getInitialApplication() {
    return ActivityThread.currentApplication();
  }
  
  public static String getInitialPackage() {
    return ActivityThread.currentPackageName();
  }
  
  public static IPackageManager getPackageManager() {
    return ActivityThread.getPackageManager();
  }
  
  public static IPermissionManager getPermissionManager() {
    return ActivityThread.getPermissionManager();
  }
  
  public static int getIntCoreSetting(String paramString, int paramInt) {
    ActivityThread activityThread = ActivityThread.currentActivityThread();
    if (activityThread != null)
      return activityThread.getIntCoreSetting(paramString, paramInt); 
    return paramInt;
  }
  
  public static float getFloatCoreSetting(String paramString, float paramFloat) {
    ActivityThread activityThread = ActivityThread.currentActivityThread();
    if (activityThread != null)
      return activityThread.getFloatCoreSetting(paramString, paramFloat); 
    return paramFloat;
  }
}
