package android.app;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

@Deprecated
public class FragmentController {
  private final FragmentHostCallback<?> mHost;
  
  public static final FragmentController createController(FragmentHostCallback<?> paramFragmentHostCallback) {
    return new FragmentController(paramFragmentHostCallback);
  }
  
  private FragmentController(FragmentHostCallback<?> paramFragmentHostCallback) {
    this.mHost = paramFragmentHostCallback;
  }
  
  public FragmentManager getFragmentManager() {
    return this.mHost.getFragmentManagerImpl();
  }
  
  public LoaderManager getLoaderManager() {
    return this.mHost.getLoaderManagerImpl();
  }
  
  public Fragment findFragmentByWho(String paramString) {
    return this.mHost.mFragmentManager.findFragmentByWho(paramString);
  }
  
  public void attachHost(Fragment paramFragment) {
    FragmentManagerImpl fragmentManagerImpl = this.mHost.mFragmentManager;
    FragmentHostCallback<?> fragmentHostCallback = this.mHost;
    fragmentManagerImpl.attachController(fragmentHostCallback, fragmentHostCallback, paramFragment);
  }
  
  public View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet) {
    return this.mHost.mFragmentManager.onCreateView(paramView, paramString, paramContext, paramAttributeSet);
  }
  
  public void noteStateNotSaved() {
    this.mHost.mFragmentManager.noteStateNotSaved();
  }
  
  public Parcelable saveAllState() {
    return this.mHost.mFragmentManager.saveAllState();
  }
  
  @Deprecated
  public void restoreAllState(Parcelable paramParcelable, List<Fragment> paramList) {
    this.mHost.mFragmentManager.restoreAllState(paramParcelable, new FragmentManagerNonConfig(paramList, null));
  }
  
  public void restoreAllState(Parcelable paramParcelable, FragmentManagerNonConfig paramFragmentManagerNonConfig) {
    this.mHost.mFragmentManager.restoreAllState(paramParcelable, paramFragmentManagerNonConfig);
  }
  
  @Deprecated
  public List<Fragment> retainNonConfig() {
    return this.mHost.mFragmentManager.retainNonConfig().getFragments();
  }
  
  public FragmentManagerNonConfig retainNestedNonConfig() {
    return this.mHost.mFragmentManager.retainNonConfig();
  }
  
  public void dispatchCreate() {
    this.mHost.mFragmentManager.dispatchCreate();
  }
  
  public void dispatchActivityCreated() {
    this.mHost.mFragmentManager.dispatchActivityCreated();
  }
  
  public void dispatchStart() {
    this.mHost.mFragmentManager.dispatchStart();
  }
  
  public void dispatchResume() {
    this.mHost.mFragmentManager.dispatchResume();
  }
  
  public void dispatchPause() {
    this.mHost.mFragmentManager.dispatchPause();
  }
  
  public void dispatchStop() {
    this.mHost.mFragmentManager.dispatchStop();
  }
  
  public void dispatchDestroyView() {
    this.mHost.mFragmentManager.dispatchDestroyView();
  }
  
  public void dispatchDestroy() {
    this.mHost.mFragmentManager.dispatchDestroy();
  }
  
  @Deprecated
  public void dispatchMultiWindowModeChanged(boolean paramBoolean) {
    this.mHost.mFragmentManager.dispatchMultiWindowModeChanged(paramBoolean);
  }
  
  public void dispatchMultiWindowModeChanged(boolean paramBoolean, Configuration paramConfiguration) {
    this.mHost.mFragmentManager.dispatchMultiWindowModeChanged(paramBoolean, paramConfiguration);
  }
  
  @Deprecated
  public void dispatchPictureInPictureModeChanged(boolean paramBoolean) {
    this.mHost.mFragmentManager.dispatchPictureInPictureModeChanged(paramBoolean);
  }
  
  public void dispatchPictureInPictureModeChanged(boolean paramBoolean, Configuration paramConfiguration) {
    this.mHost.mFragmentManager.dispatchPictureInPictureModeChanged(paramBoolean, paramConfiguration);
  }
  
  public void dispatchConfigurationChanged(Configuration paramConfiguration) {
    this.mHost.mFragmentManager.dispatchConfigurationChanged(paramConfiguration);
  }
  
  public void dispatchLowMemory() {
    this.mHost.mFragmentManager.dispatchLowMemory();
  }
  
  public void dispatchTrimMemory(int paramInt) {
    this.mHost.mFragmentManager.dispatchTrimMemory(paramInt);
  }
  
  public boolean dispatchCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater) {
    return this.mHost.mFragmentManager.dispatchCreateOptionsMenu(paramMenu, paramMenuInflater);
  }
  
  public boolean dispatchPrepareOptionsMenu(Menu paramMenu) {
    return this.mHost.mFragmentManager.dispatchPrepareOptionsMenu(paramMenu);
  }
  
  public boolean dispatchOptionsItemSelected(MenuItem paramMenuItem) {
    return this.mHost.mFragmentManager.dispatchOptionsItemSelected(paramMenuItem);
  }
  
  public boolean dispatchContextItemSelected(MenuItem paramMenuItem) {
    return this.mHost.mFragmentManager.dispatchContextItemSelected(paramMenuItem);
  }
  
  public void dispatchOptionsMenuClosed(Menu paramMenu) {
    this.mHost.mFragmentManager.dispatchOptionsMenuClosed(paramMenu);
  }
  
  public boolean execPendingActions() {
    return this.mHost.mFragmentManager.execPendingActions();
  }
  
  public void doLoaderStart() {
    this.mHost.doLoaderStart();
  }
  
  public void doLoaderStop(boolean paramBoolean) {
    this.mHost.doLoaderStop(paramBoolean);
  }
  
  public void doLoaderDestroy() {
    this.mHost.doLoaderDestroy();
  }
  
  public void reportLoaderStart() {
    this.mHost.reportLoaderStart();
  }
  
  public ArrayMap<String, LoaderManager> retainLoaderNonConfig() {
    return this.mHost.retainLoaderNonConfig();
  }
  
  public void restoreLoaderNonConfig(ArrayMap<String, LoaderManager> paramArrayMap) {
    this.mHost.restoreLoaderNonConfig(paramArrayMap);
  }
  
  public void dumpLoaders(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    this.mHost.dumpLoaders(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
  }
}
