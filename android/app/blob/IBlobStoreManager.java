package android.app.blob;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.List;

public interface IBlobStoreManager extends IInterface {
  void abandonSession(long paramLong, String paramString) throws RemoteException;
  
  void acquireLease(BlobHandle paramBlobHandle, int paramInt, CharSequence paramCharSequence, long paramLong, String paramString) throws RemoteException;
  
  long createSession(BlobHandle paramBlobHandle, String paramString) throws RemoteException;
  
  void deleteBlob(long paramLong) throws RemoteException;
  
  LeaseInfo getLeaseInfo(BlobHandle paramBlobHandle, String paramString) throws RemoteException;
  
  List<BlobHandle> getLeasedBlobs(String paramString) throws RemoteException;
  
  long getRemainingLeaseQuotaBytes(String paramString) throws RemoteException;
  
  ParcelFileDescriptor openBlob(BlobHandle paramBlobHandle, String paramString) throws RemoteException;
  
  IBlobStoreSession openSession(long paramLong, String paramString) throws RemoteException;
  
  List<BlobInfo> queryBlobsForUser(int paramInt) throws RemoteException;
  
  void releaseLease(BlobHandle paramBlobHandle, String paramString) throws RemoteException;
  
  void waitForIdle(RemoteCallback paramRemoteCallback) throws RemoteException;
  
  class Default implements IBlobStoreManager {
    public long createSession(BlobHandle param1BlobHandle, String param1String) throws RemoteException {
      return 0L;
    }
    
    public IBlobStoreSession openSession(long param1Long, String param1String) throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor openBlob(BlobHandle param1BlobHandle, String param1String) throws RemoteException {
      return null;
    }
    
    public void abandonSession(long param1Long, String param1String) throws RemoteException {}
    
    public void acquireLease(BlobHandle param1BlobHandle, int param1Int, CharSequence param1CharSequence, long param1Long, String param1String) throws RemoteException {}
    
    public void releaseLease(BlobHandle param1BlobHandle, String param1String) throws RemoteException {}
    
    public long getRemainingLeaseQuotaBytes(String param1String) throws RemoteException {
      return 0L;
    }
    
    public void waitForIdle(RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public List<BlobInfo> queryBlobsForUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public void deleteBlob(long param1Long) throws RemoteException {}
    
    public List<BlobHandle> getLeasedBlobs(String param1String) throws RemoteException {
      return null;
    }
    
    public LeaseInfo getLeaseInfo(BlobHandle param1BlobHandle, String param1String) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBlobStoreManager {
    private static final String DESCRIPTOR = "android.app.blob.IBlobStoreManager";
    
    static final int TRANSACTION_abandonSession = 4;
    
    static final int TRANSACTION_acquireLease = 5;
    
    static final int TRANSACTION_createSession = 1;
    
    static final int TRANSACTION_deleteBlob = 10;
    
    static final int TRANSACTION_getLeaseInfo = 12;
    
    static final int TRANSACTION_getLeasedBlobs = 11;
    
    static final int TRANSACTION_getRemainingLeaseQuotaBytes = 7;
    
    static final int TRANSACTION_openBlob = 3;
    
    static final int TRANSACTION_openSession = 2;
    
    static final int TRANSACTION_queryBlobsForUser = 9;
    
    static final int TRANSACTION_releaseLease = 6;
    
    static final int TRANSACTION_waitForIdle = 8;
    
    public Stub() {
      attachInterface(this, "android.app.blob.IBlobStoreManager");
    }
    
    public static IBlobStoreManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.blob.IBlobStoreManager");
      if (iInterface != null && iInterface instanceof IBlobStoreManager)
        return (IBlobStoreManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 12:
          return "getLeaseInfo";
        case 11:
          return "getLeasedBlobs";
        case 10:
          return "deleteBlob";
        case 9:
          return "queryBlobsForUser";
        case 8:
          return "waitForIdle";
        case 7:
          return "getRemainingLeaseQuotaBytes";
        case 6:
          return "releaseLease";
        case 5:
          return "acquireLease";
        case 4:
          return "abandonSession";
        case 3:
          return "openBlob";
        case 2:
          return "openSession";
        case 1:
          break;
      } 
      return "createSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str5;
        LeaseInfo leaseInfo;
        String str4;
        List<BlobHandle> list;
        String str3;
        ParcelFileDescriptor parcelFileDescriptor;
        String str2;
        IBlobStoreSession iBlobStoreSession;
        BlobHandle blobHandle;
        CharSequence charSequence;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 12:
            param1Parcel1.enforceInterface("android.app.blob.IBlobStoreManager");
            if (param1Parcel1.readInt() != 0) {
              blobHandle = (BlobHandle)BlobHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              blobHandle = null;
            } 
            str5 = param1Parcel1.readString();
            leaseInfo = getLeaseInfo(blobHandle, str5);
            param1Parcel2.writeNoException();
            if (leaseInfo != null) {
              param1Parcel2.writeInt(1);
              leaseInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 11:
            leaseInfo.enforceInterface("android.app.blob.IBlobStoreManager");
            str4 = leaseInfo.readString();
            list = getLeasedBlobs(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 10:
            list.enforceInterface("android.app.blob.IBlobStoreManager");
            l = list.readLong();
            deleteBlob(l);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            list.enforceInterface("android.app.blob.IBlobStoreManager");
            param1Int1 = list.readInt();
            list = (List)queryBlobsForUser(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 8:
            list.enforceInterface("android.app.blob.IBlobStoreManager");
            if (list.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            waitForIdle((RemoteCallback)list);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list.enforceInterface("android.app.blob.IBlobStoreManager");
            str3 = list.readString();
            l = getRemainingLeaseQuotaBytes(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 6:
            str3.enforceInterface("android.app.blob.IBlobStoreManager");
            if (str3.readInt() != 0) {
              blobHandle = (BlobHandle)BlobHandle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              blobHandle = null;
            } 
            str3 = str3.readString();
            releaseLease(blobHandle, str3);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str3.enforceInterface("android.app.blob.IBlobStoreManager");
            if (str3.readInt() != 0) {
              blobHandle = (BlobHandle)BlobHandle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              blobHandle = null;
            } 
            param1Int1 = str3.readInt();
            if (str3.readInt() != 0) {
              charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str3);
            } else {
              charSequence = null;
            } 
            l = str3.readLong();
            str3 = str3.readString();
            acquireLease(blobHandle, param1Int1, charSequence, l, str3);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str3.enforceInterface("android.app.blob.IBlobStoreManager");
            l = str3.readLong();
            str3 = str3.readString();
            abandonSession(l, str3);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str3.enforceInterface("android.app.blob.IBlobStoreManager");
            if (str3.readInt() != 0) {
              blobHandle = (BlobHandle)BlobHandle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              blobHandle = null;
            } 
            str3 = str3.readString();
            parcelFileDescriptor = openBlob(blobHandle, str3);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 2:
            parcelFileDescriptor.enforceInterface("android.app.blob.IBlobStoreManager");
            l = parcelFileDescriptor.readLong();
            str2 = parcelFileDescriptor.readString();
            iBlobStoreSession = openSession(l, str2);
            param1Parcel2.writeNoException();
            if (iBlobStoreSession != null) {
              IBinder iBinder = iBlobStoreSession.asBinder();
            } else {
              iBlobStoreSession = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iBlobStoreSession);
            return true;
          case 1:
            break;
        } 
        iBlobStoreSession.enforceInterface("android.app.blob.IBlobStoreManager");
        if (iBlobStoreSession.readInt() != 0) {
          blobHandle = (BlobHandle)BlobHandle.CREATOR.createFromParcel((Parcel)iBlobStoreSession);
        } else {
          blobHandle = null;
        } 
        String str1 = iBlobStoreSession.readString();
        long l = createSession(blobHandle, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeLong(l);
        return true;
      } 
      param1Parcel2.writeString("android.app.blob.IBlobStoreManager");
      return true;
    }
    
    private static class Proxy implements IBlobStoreManager {
      public static IBlobStoreManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.blob.IBlobStoreManager";
      }
      
      public long createSession(BlobHandle param2BlobHandle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2BlobHandle != null) {
            parcel1.writeInt(1);
            param2BlobHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().createSession(param2BlobHandle, param2String); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBlobStoreSession openSession(long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().openSession(param2Long, param2String); 
          parcel2.readException();
          return IBlobStoreSession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openBlob(BlobHandle param2BlobHandle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2BlobHandle != null) {
            parcel1.writeInt(1);
            param2BlobHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().openBlob(param2BlobHandle, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2BlobHandle = null;
          } 
          return (ParcelFileDescriptor)param2BlobHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void abandonSession(long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null) {
            IBlobStoreManager.Stub.getDefaultImpl().abandonSession(param2Long, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void acquireLease(BlobHandle param2BlobHandle, int param2Int, CharSequence param2CharSequence, long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2BlobHandle != null) {
            parcel1.writeInt(1);
            param2BlobHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int);
            if (param2CharSequence != null) {
              parcel1.writeInt(1);
              TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeLong(param2Long);
              try {
                parcel1.writeString(param2String);
                boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
                if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null) {
                  IBlobStoreManager.Stub.getDefaultImpl().acquireLease(param2BlobHandle, param2Int, param2CharSequence, param2Long, param2String);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2BlobHandle;
      }
      
      public void releaseLease(BlobHandle param2BlobHandle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2BlobHandle != null) {
            parcel1.writeInt(1);
            param2BlobHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null) {
            IBlobStoreManager.Stub.getDefaultImpl().releaseLease(param2BlobHandle, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getRemainingLeaseQuotaBytes(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().getRemainingLeaseQuotaBytes(param2String); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void waitForIdle(RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null) {
            IBlobStoreManager.Stub.getDefaultImpl().waitForIdle(param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BlobInfo> queryBlobsForUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().queryBlobsForUser(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BlobInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteBlob(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null) {
            IBlobStoreManager.Stub.getDefaultImpl().deleteBlob(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<BlobHandle> getLeasedBlobs(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().getLeasedBlobs(param2String); 
          parcel2.readException();
          return parcel2.createTypedArrayList(BlobHandle.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public LeaseInfo getLeaseInfo(BlobHandle param2BlobHandle, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.blob.IBlobStoreManager");
          if (param2BlobHandle != null) {
            parcel1.writeInt(1);
            param2BlobHandle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IBlobStoreManager.Stub.getDefaultImpl() != null)
            return IBlobStoreManager.Stub.getDefaultImpl().getLeaseInfo(param2BlobHandle, param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            LeaseInfo leaseInfo = (LeaseInfo)LeaseInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2BlobHandle = null;
          } 
          return (LeaseInfo)param2BlobHandle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBlobStoreManager param1IBlobStoreManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBlobStoreManager != null) {
          Proxy.sDefaultImpl = param1IBlobStoreManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBlobStoreManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
