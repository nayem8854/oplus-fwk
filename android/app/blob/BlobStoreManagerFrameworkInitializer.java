package android.app.blob;

import android.app.SystemServiceRegistry;
import android.content.Context;
import android.os.IBinder;

public class BlobStoreManagerFrameworkInitializer {
  public static void initialize() {
    SystemServiceRegistry.registerContextAwareService("blob_store", BlobStoreManager.class, (SystemServiceRegistry.ContextAwareServiceProducerWithBinder<BlobStoreManager>)_$$Lambda$BlobStoreManagerFrameworkInitializer$WjSRSHMmxWPF4Fq_7TpX23MBh2U.INSTANCE);
  }
}
