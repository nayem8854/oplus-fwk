package android.app;

import android.content.ContentUris;
import android.database.Cursor;
import android.net.Uri;
import android.provider.Downloads;
import java.io.File;

public class OplusDownloadManagerHelper {
  public static Uri fixUriWhenGetUriForDownloadedFile(Cursor paramCursor, long paramLong) {
    int i = paramCursor.getColumnIndexOrThrow("destination");
    i = paramCursor.getInt(i);
    if (i == 1 || i == 5 || i == 3 || i == 2)
      return ContentUris.withAppendedId(Downloads.Impl.ALL_DOWNLOADS_CONTENT_URI, paramLong); 
    i = paramCursor.getColumnIndexOrThrow("local_filename");
    String str = paramCursor.getString(i);
    return Uri.fromFile(new File(str));
  }
}
