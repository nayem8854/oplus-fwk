package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IOplusKeyEventObserver;
import android.os.RemoteException;
import android.view.Surface;
import com.oplus.app.IOplusAppSwitchObserver;
import com.oplus.app.IOplusFreeformConfigChangedListener;
import com.oplus.app.IOplusSplitScreenObserver;
import com.oplus.app.IOplusZoomWindowConfigChangedListener;
import com.oplus.app.OplusAppInfo;
import com.oplus.app.OplusAppSwitchConfig;
import com.oplus.confinemode.IOplusConfineModeObserver;
import com.oplus.globaldrag.OplusGlobalDragAndDropRUSConfig;
import com.oplus.miragewindow.IOplusMirageSessionCallback;
import com.oplus.miragewindow.IOplusMirageWindowObserver;
import com.oplus.miragewindow.IOplusMirageWindowSession;
import com.oplus.miragewindow.OplusMirageWindowInfo;
import com.oplus.zoomwindow.IOplusZoomAppObserver;
import com.oplus.zoomwindow.IOplusZoomWindowObserver;
import com.oplus.zoomwindow.OplusZoomControlViewInfo;
import com.oplus.zoomwindow.OplusZoomInputEventInfo;
import com.oplus.zoomwindow.OplusZoomWindowInfo;
import com.oplus.zoomwindow.OplusZoomWindowRUSConfig;
import java.util.List;
import java.util.Map;

public interface IOplusActivityTaskManager extends IOplusBaseActivityTaskManager {
  public static final int ADD_FEEFORM_CONFIG_CHANGED_LISTENER = 10056;
  
  public static final int ADD_ZOOM_WINDOW_CONFIG_CHANGED_LISTENER = 10079;
  
  public static final int ATM_TANSACTION_DONE = 10130;
  
  public static final int CREATE_MIRAGE_DISPLAY = 10114;
  
  public static final int CREATE_MIRAGE_WINDOW_SESSION = 10137;
  
  public static final int EXPAND_TO_FULL_SCREEN = 10118;
  
  public static final int GET_ALL_TOP_APP_INFOS = 10053;
  
  public static final int GET_CONFINE_MODE = 10090;
  
  public static final int GET_FREEFORM_CONFIG_LIST = 10054;
  
  public static final int GET_MIRAGE_WINDOW_INFO = 10121;
  
  public static final int GET_SPLIT_WINDOW_STATE = 10074;
  
  public static final int GET_TOP_ACTIVITY_COMPONENTNAME_TRANSACTION = 10007;
  
  public static final int GET_TOP_APPLICATION_INFO = 10011;
  
  public static final int IS_FREEFORM_ENABLED = 10055;
  
  public static final int IS_MIRAGE_WINDOW_SHOW = 10116;
  
  public static final int IS_SHOW_ZOOM_COMPATIBILITY_TOAST = 10136;
  
  public static final int IS_SUPPORT_EDGE_TOUCH_PREVENT = 10086;
  
  public static final int IS_SUPPORT_MIRAGE_WINDOW_MODE = 10120;
  
  public static final int IS_TARGET_SUPPORT_ZOOM_MODE = 10135;
  
  public static final int IS_ZOOM_WINDOW_ENABLED = 10078;
  
  public static final int ON_ZOOM_CONTROL_VIEW_CHANGED = 10132;
  
  public static final int OPLUS_GLOBAL_DRAG_SHARE_GET_RUS_CONFIG = 10110;
  
  public static final int OPLUS_GLOBAL_DRAG_SHARE_SET_RUS_CONFIG = 10111;
  
  public static final int OPLUS_START_ZOOM_WINDOW = 10068;
  
  public static final int OPLUS_ZOOM_WINDOW_BUBBLE = 10072;
  
  public static final int OPLUS_ZOOM_WINDOW_CONFIG = 10075;
  
  public static final int OPLUS_ZOOM_WINDOW_GET_RUS_CONFIG = 10076;
  
  public static final int OPLUS_ZOOM_WINDOW_HIDE = 10073;
  
  public static final int OPLUS_ZOOM_WINDOW_INPUT = 10131;
  
  public static final int OPLUS_ZOOM_WINDOW_SET_RUS_CONFIG = 10077;
  
  public static final int OPLUS_ZOOM_WINDOW_STATE = 10071;
  
  public static final int READ_NODE_FILE = 10126;
  
  public static final int REGISTER_APP_SWITCH_OBSERVER = 10064;
  
  public static final int REGISTER_CONFINE_MODE_OBSERVER = 10122;
  
  public static final int REGISTER_DATA_SYNC_CALLBACK = 10108;
  
  public static final int REGISTER_KEY_EVENT_INTERCEPTOR = 10105;
  
  public static final int REGISTER_KEY_EVENT_OBSERVER = 10103;
  
  public static final int REGISTER_LOCKSCREEN_CALLBACK = 10094;
  
  public static final int REGISTER_MIRAGE_WINDOW_OBSERVER = 10112;
  
  public static final int REGISTER_ZOOM_APP_OBSERVER = 10133;
  
  public static final int REGISTER_ZOOM_OBSERVER = 10069;
  
  public static final int REMOVE_FREEFORM_CONFIG_CHANGED_LISTENER = 10057;
  
  public static final int REMOVE_ZOOM_WINDOW_CONFIG_CHANGED_LISTENER = 10080;
  
  public static final int RESET_DEFAULT_EDGE_TOUCH_PREVENT_PARAM = 10085;
  
  public static final int SET_CONFINE_MODE = 10089;
  
  public static final int SET_DEFAULT_EDGE_TOUCH_PREVENT_PARAM = 10084;
  
  public static final int SET_EDGE_TOUCH_CALL_RULES = 10087;
  
  public static final int SET_EDGE_TOUCH_PREVENT_PARAM = 10083;
  
  public static final int SET_GIMBAL_LAUNCH_PKG = 10092;
  
  public static final int SET_MIRAGE_WINDOW_SILENT = 10119;
  
  public static final int SET_PERMIT_LIST = 10091;
  
  public static final int SET_SECURE_CONTROLLER_TRANSACTION = 10002;
  
  public static final int SPLIT_SCREEN_DISMISS = 10099;
  
  public static final int SPLIT_SCREEN_FOR_FLOAT_ASSISTENT = 10088;
  
  public static final int SPLIT_SCREEN_FOR_RECENT_TASKS = 10129;
  
  public static final int SPLIT_SCREEN_FOR_TOP_APP = 10128;
  
  public static final int SPLIT_SCREEN_GET_DESIGNATED_STATUS = 10102;
  
  public static final int SPLIT_SCREEN_GET_MODE_STATUS = 10098;
  
  public static final int SPLIT_SCREEN_NOTIFY_STATE_CHANGED = 10096;
  
  public static final int SPLIT_SCREEN_REGISTER_OBSERVER = 10100;
  
  public static final int SPLIT_SCREEN_SET_OBSERVER = 10097;
  
  public static final int SPLIT_SCREEN_UNREGISTER_OBSERVER = 10101;
  
  public static final int START_LOCK_DEVICE_MODE_TRANSACTION = 10081;
  
  public static final int START_MIRAGE_WINDOW_MODE = 10115;
  
  public static final int STOP_LOCK_DEVICE_MODE_TRANSACTION = 10082;
  
  public static final int STOP_MIRAGE_WINDOW_MODE = 10117;
  
  public static final int SWAP_STACK = 10052;
  
  public static final int UNREGISTER_APP_SWITCH_OBSERVER = 10065;
  
  public static final int UNREGISTER_CONFINE_MODE_OBSERVER = 10123;
  
  public static final int UNREGISTER_DATA_SYNC_CALLBACK = 10109;
  
  public static final int UNREGISTER_KEY_EVENT_INTERCEPTOR = 10106;
  
  public static final int UNREGISTER_KEY_EVENT_OBSERVER = 10104;
  
  public static final int UNREGISTER_LOCKSCREEN_CALLBACK = 10095;
  
  public static final int UNREGISTER_MIRAGE_WINDOW_OBSERVER = 10113;
  
  public static final int UNREGISTER_ZOOM_APP_OBSERVER = 10134;
  
  public static final int UNREGISTER_ZOOM_OBSERVER = 10070;
  
  public static final int UPDATE_APP_STATE = 10107;
  
  public static final int UPDATE_APP_STATE_FOR_INTERCEPT = 10093;
  
  public static final int UPDATE_MIRAGE_CAST_FLAG = 10124;
  
  public static final int UPDATE_MIRAGE_PRIVACY_LIST = 10138;
  
  public static final int UPDATE_PRIVACY_PROTECTION_LIST = 10125;
  
  public static final int WRITE_NODE_FILE = 10127;
  
  boolean addFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException;
  
  boolean addZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException;
  
  void clientTransactionComplete(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  int createMirageDisplay(Surface paramSurface) throws RemoteException;
  
  IOplusMirageWindowSession createMirageWindowSession(IOplusMirageSessionCallback paramIOplusMirageSessionCallback) throws RemoteException;
  
  boolean dismissSplitScreenMode(int paramInt) throws RemoteException;
  
  void expandToFullScreen() throws RemoteException;
  
  List<OplusAppInfo> getAllTopAppInfos() throws RemoteException;
  
  int getConfineMode() throws RemoteException;
  
  OplusZoomWindowInfo getCurrentZoomWindowState() throws RemoteException;
  
  List<String> getFreeformConfigList(int paramInt) throws RemoteException;
  
  OplusGlobalDragAndDropRUSConfig getGlobalDragAndDropConfig() throws RemoteException;
  
  OplusMirageWindowInfo getMirageWindowInfo() throws RemoteException;
  
  int getSplitScreenState(Intent paramIntent) throws RemoteException;
  
  Bundle getSplitScreenStatus(String paramString) throws RemoteException;
  
  ComponentName getTopActivityComponentName() throws RemoteException;
  
  ApplicationInfo getTopApplicationInfo() throws RemoteException;
  
  List<String> getZoomAppConfigList(int paramInt) throws RemoteException;
  
  OplusZoomWindowRUSConfig getZoomWindowConfig() throws RemoteException;
  
  boolean handleShowCompatibilityToast(String paramString1, int paramInt1, String paramString2, Bundle paramBundle, int paramInt2) throws RemoteException;
  
  void hideZoomWindow(int paramInt) throws RemoteException;
  
  boolean isFreeformEnabled() throws RemoteException;
  
  boolean isInSplitScreenMode() throws RemoteException;
  
  boolean isMirageWindowShow() throws RemoteException;
  
  boolean isSupportEdgeTouchPrevent() throws RemoteException;
  
  boolean isSupportMirageWindowMode() throws RemoteException;
  
  boolean isSupportZoomMode(String paramString1, int paramInt, String paramString2, Bundle paramBundle) throws RemoteException;
  
  boolean isSupportZoomWindowMode() throws RemoteException;
  
  void notifySplitScreenStateChanged(String paramString, Bundle paramBundle, boolean paramBoolean) throws RemoteException;
  
  void onControlViewChanged(OplusZoomControlViewInfo paramOplusZoomControlViewInfo) throws RemoteException;
  
  void onInputEvent(OplusZoomInputEventInfo paramOplusZoomInputEventInfo) throws RemoteException;
  
  String readNodeFile(int paramInt) throws RemoteException;
  
  boolean registerAppSwitchObserver(String paramString, IOplusAppSwitchObserver paramIOplusAppSwitchObserver, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException;
  
  boolean registerConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException;
  
  boolean registerKeyEventInterceptor(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, Map<Integer, Integer> paramMap) throws RemoteException;
  
  boolean registerKeyEventObserver(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, int paramInt) throws RemoteException;
  
  boolean registerMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException;
  
  boolean registerSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException;
  
  boolean registerZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException;
  
  boolean registerZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException;
  
  boolean removeFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException;
  
  boolean removeZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException;
  
  boolean resetDefaultEdgeTouchPreventParam(String paramString) throws RemoteException;
  
  void setConfineMode(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setDefaultEdgeTouchPreventParam(String paramString, List<String> paramList) throws RemoteException;
  
  void setEdgeTouchCallRules(String paramString, Map<String, List<String>> paramMap) throws RemoteException;
  
  void setGimbalLaunchPkg(String paramString) throws RemoteException;
  
  void setGlobalDragAndDropConfig(OplusGlobalDragAndDropRUSConfig paramOplusGlobalDragAndDropRUSConfig) throws RemoteException;
  
  void setMirageWindowSilent(String paramString) throws RemoteException;
  
  void setPermitList(int paramInt1, int paramInt2, List<String> paramList, boolean paramBoolean) throws RemoteException;
  
  void setSecureController(IActivityController paramIActivityController) throws RemoteException;
  
  boolean setSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException;
  
  void setZoomWindowConfig(OplusZoomWindowRUSConfig paramOplusZoomWindowRUSConfig) throws RemoteException;
  
  default int splitScreenForEdgePanel(Intent paramIntent, int paramInt) throws RemoteException {
    return 0;
  }
  
  boolean splitScreenForRecentTasks(int paramInt) throws RemoteException;
  
  boolean splitScreenForTopApp(int paramInt) throws RemoteException;
  
  void startMirageWindowMode(ComponentName paramComponentName, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  int startZoomWindow(Intent paramIntent, Bundle paramBundle, int paramInt, String paramString) throws RemoteException;
  
  void stopMirageWindowMode() throws RemoteException;
  
  void swapDockedFullscreenStack() throws RemoteException;
  
  boolean unregisterAppSwitchObserver(String paramString, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException;
  
  boolean unregisterConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException;
  
  boolean unregisterKeyEventInterceptor(String paramString) throws RemoteException;
  
  boolean unregisterKeyEventObserver(String paramString) throws RemoteException;
  
  boolean unregisterMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException;
  
  boolean unregisterSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException;
  
  boolean unregisterZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException;
  
  boolean unregisterZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException;
  
  boolean updateMirageWindowCastFlag(int paramInt, Bundle paramBundle) throws RemoteException;
  
  boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean) throws RemoteException;
  
  boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) throws RemoteException;
  
  boolean writeEdgeTouchPreventParam(String paramString1, String paramString2, List<String> paramList) throws RemoteException;
  
  boolean writeNodeFile(int paramInt, String paramString) throws RemoteException;
}
