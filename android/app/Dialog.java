package android.app;

import android.common.OplusFeatureCache;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import com.android.internal.app.WindowDecorActionBar;
import com.android.internal.policy.PhoneWindow;
import com.oplus.darkmode.IOplusDarkModeManager;
import java.lang.ref.WeakReference;

public class Dialog extends OplusBaseDialog implements DialogInterface, Window.Callback, KeyEvent.Callback, View.OnCreateContextMenuListener, Window.OnWindowDismissedCallback {
  protected boolean mCancelable = true;
  
  private boolean mCreated = false;
  
  private boolean mShowing = false;
  
  private boolean mCanceled = false;
  
  private final Handler mHandler = new Handler();
  
  private int mActionModeTypeStarting = 0;
  
  private final Runnable mDismissAction = new _$$Lambda$oslF4K8Uk6v_6nTRoaEpCmfAptE(this);
  
  private static final int CANCEL = 68;
  
  private static final String DIALOG_HIERARCHY_TAG = "android:dialogHierarchy";
  
  private static final String DIALOG_SHOWING_TAG = "android:dialogShowing";
  
  private static final int DISMISS = 67;
  
  private static final int SHOW = 69;
  
  private static final String TAG = "Dialog";
  
  private ActionBar mActionBar;
  
  private ActionMode mActionMode;
  
  private String mCancelAndDismissTaken;
  
  private Message mCancelMessage;
  
  final Context mContext;
  
  View mDecor;
  
  private Message mDismissMessage;
  
  private final Handler mListenersHandler;
  
  private DialogInterface.OnKeyListener mOnKeyListener;
  
  private Activity mOwnerActivity;
  
  private SearchEvent mSearchEvent;
  
  private Message mShowMessage;
  
  final Window mWindow;
  
  private final WindowManager mWindowManager;
  
  public Dialog(Context paramContext) {
    this(paramContext, 0, true);
  }
  
  public Dialog(Context paramContext, int paramInt) {
    this(paramContext, paramInt, true);
  }
  
  Dialog(Context paramContext, int paramInt, boolean paramBoolean) {
    if (paramBoolean) {
      int i = paramInt;
      if (paramInt == 0) {
        TypedValue typedValue = new TypedValue();
        paramContext.getTheme().resolveAttribute(16843528, typedValue, true);
        i = typedValue.resourceId;
      } 
      this.mContext = (Context)new ContextThemeWrapper(paramContext, i);
    } else {
      this.mContext = paramContext;
    } 
    this.mWindowManager = (WindowManager)paramContext.getSystemService("window");
    PhoneWindow phoneWindow = new PhoneWindow(this.mContext);
    this.mWindow = (Window)phoneWindow;
    phoneWindow.setCallback(this);
    phoneWindow.setOnWindowDismissedCallback(this);
    phoneWindow.setOnWindowSwipeDismissedCallback(new _$$Lambda$Dialog$zXRzrq3I7H1_zmZ8d_W7t2CQN0I(this));
    phoneWindow.setWindowManager(this.mWindowManager, null, null);
    phoneWindow.setGravity(17);
    this.mListenersHandler = new ListenersHandler(this);
  }
  
  @Deprecated
  protected Dialog(Context paramContext, boolean paramBoolean, Message paramMessage) {
    this(paramContext);
    this.mCancelable = paramBoolean;
    this.mCancelMessage = paramMessage;
  }
  
  protected Dialog(Context paramContext, boolean paramBoolean, DialogInterface.OnCancelListener paramOnCancelListener) {
    this(paramContext);
    this.mCancelable = paramBoolean;
    setOnCancelListener(paramOnCancelListener);
  }
  
  public final Context getContext() {
    return this.mContext;
  }
  
  public ActionBar getActionBar() {
    return this.mActionBar;
  }
  
  public final void setOwnerActivity(Activity paramActivity) {
    this.mOwnerActivity = paramActivity;
    getWindow().setVolumeControlStream(this.mOwnerActivity.getVolumeControlStream());
  }
  
  public final Activity getOwnerActivity() {
    return this.mOwnerActivity;
  }
  
  public boolean isShowing() {
    View view = this.mDecor;
    boolean bool = false;
    if (view != null && view.getVisibility() == 0)
      bool = true; 
    return bool;
  }
  
  public void create() {
    if (!this.mCreated)
      dispatchOnCreate(null); 
  }
  
  public void show() {
    if (this.mShowing) {
      if (this.mDecor != null) {
        if (this.mWindow.hasFeature(8))
          this.mWindow.invalidatePanelMenu(8); 
        this.mDecor.setVisibility(0);
      } 
      return;
    } 
    this.mCanceled = false;
    if (!this.mCreated) {
      dispatchOnCreate(null);
    } else {
      Configuration configuration = this.mContext.getResources().getConfiguration();
      this.mWindow.getDecorView().dispatchConfigurationChanged(configuration);
    } 
    onStart();
    this.mDecor = this.mWindow.getDecorView();
    ((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).changeUsageForceDarkAlgorithmType(this.mDecor, 2);
    if (this.mActionBar == null && this.mWindow.hasFeature(8)) {
      ApplicationInfo applicationInfo = this.mContext.getApplicationInfo();
      this.mWindow.setDefaultIcon(applicationInfo.icon);
      this.mWindow.setDefaultLogo(applicationInfo.logo);
      this.mActionBar = (ActionBar)new WindowDecorActionBar(this);
    } 
    WindowManager.LayoutParams layoutParams = this.mWindow.getAttributes();
    boolean bool = false;
    if ((layoutParams.softInputMode & 0x100) == 0) {
      layoutParams.softInputMode |= 0x100;
      bool = true;
    } 
    this.mWindowManager.addView(this.mDecor, (ViewGroup.LayoutParams)layoutParams);
    if (bool)
      layoutParams.softInputMode &= 0xFFFFFEFF; 
    this.mShowing = true;
    sendShowMessage();
  }
  
  public void hide() {
    View view = this.mDecor;
    if (view != null)
      view.setVisibility(8); 
  }
  
  public void dismiss() {
    if (Looper.myLooper() == this.mHandler.getLooper()) {
      dismissDialog();
    } else {
      this.mHandler.post(this.mDismissAction);
    } 
  }
  
  void dismissDialog() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mDecor : Landroid/view/View;
    //   4: ifnull -> 159
    //   7: aload_0
    //   8: getfield mShowing : Z
    //   11: ifne -> 17
    //   14: goto -> 159
    //   17: aload_0
    //   18: getfield mWindow : Landroid/view/Window;
    //   21: invokevirtual isDestroyed : ()Z
    //   24: ifeq -> 37
    //   27: ldc 'Dialog'
    //   29: ldc_w 'Tried to dismissDialog() but the Dialog's window was already destroyed!'
    //   32: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   35: pop
    //   36: return
    //   37: aload_0
    //   38: getfield mWindowManager : Landroid/view/WindowManager;
    //   41: aload_0
    //   42: getfield mDecor : Landroid/view/View;
    //   45: invokeinterface removeViewImmediate : (Landroid/view/View;)V
    //   50: aload_0
    //   51: getfield mActionMode : Landroid/view/ActionMode;
    //   54: astore_1
    //   55: aload_1
    //   56: ifnull -> 63
    //   59: aload_1
    //   60: invokevirtual finish : ()V
    //   63: aload_0
    //   64: aconst_null
    //   65: putfield mDecor : Landroid/view/View;
    //   68: aload_0
    //   69: getfield mWindow : Landroid/view/Window;
    //   72: invokevirtual closeAllPanels : ()V
    //   75: aload_0
    //   76: invokevirtual onStop : ()V
    //   79: aload_0
    //   80: iconst_0
    //   81: putfield mShowing : Z
    //   84: aload_0
    //   85: invokespecial sendDismissMessage : ()V
    //   88: goto -> 118
    //   91: astore_2
    //   92: goto -> 119
    //   95: astore_1
    //   96: ldc 'Dialog'
    //   98: ldc_w 'dismissDialog failed!'
    //   101: aload_1
    //   102: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   105: pop
    //   106: aload_0
    //   107: getfield mActionMode : Landroid/view/ActionMode;
    //   110: astore_1
    //   111: aload_1
    //   112: ifnull -> 63
    //   115: goto -> 59
    //   118: return
    //   119: aload_0
    //   120: getfield mActionMode : Landroid/view/ActionMode;
    //   123: astore_1
    //   124: aload_1
    //   125: ifnull -> 132
    //   128: aload_1
    //   129: invokevirtual finish : ()V
    //   132: aload_0
    //   133: aconst_null
    //   134: putfield mDecor : Landroid/view/View;
    //   137: aload_0
    //   138: getfield mWindow : Landroid/view/Window;
    //   141: invokevirtual closeAllPanels : ()V
    //   144: aload_0
    //   145: invokevirtual onStop : ()V
    //   148: aload_0
    //   149: iconst_0
    //   150: putfield mShowing : Z
    //   153: aload_0
    //   154: invokespecial sendDismissMessage : ()V
    //   157: aload_2
    //   158: athrow
    //   159: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #390	-> 0
    //   #394	-> 17
    //   #395	-> 27
    //   #396	-> 36
    //   #400	-> 37
    //   #410	-> 50
    //   #411	-> 59
    //   #413	-> 63
    //   #414	-> 68
    //   #415	-> 75
    //   #416	-> 79
    //   #418	-> 84
    //   #419	-> 88
    //   #410	-> 91
    //   #405	-> 95
    //   #406	-> 96
    //   #410	-> 106
    //   #411	-> 115
    //   #420	-> 118
    //   #410	-> 119
    //   #411	-> 128
    //   #413	-> 132
    //   #414	-> 137
    //   #415	-> 144
    //   #416	-> 148
    //   #418	-> 153
    //   #419	-> 157
    //   #391	-> 159
    // Exception table:
    //   from	to	target	type
    //   37	50	95	java/lang/Exception
    //   37	50	91	finally
    //   96	106	91	finally
  }
  
  private void sendDismissMessage() {
    Message message = this.mDismissMessage;
    if (message != null)
      Message.obtain(message).sendToTarget(); 
  }
  
  private void sendShowMessage() {
    Message message = this.mShowMessage;
    if (message != null)
      Message.obtain(message).sendToTarget(); 
  }
  
  void dispatchOnCreate(Bundle paramBundle) {
    if (!this.mCreated) {
      onCreate(paramBundle);
      this.mCreated = true;
    } 
  }
  
  protected void onCreate(Bundle paramBundle) {}
  
  protected void onStart() {
    ActionBar actionBar = this.mActionBar;
    if (actionBar != null)
      actionBar.setShowHideAnimationEnabled(true); 
  }
  
  protected void onStop() {
    ActionBar actionBar = this.mActionBar;
    if (actionBar != null)
      actionBar.setShowHideAnimationEnabled(false); 
  }
  
  public Bundle onSaveInstanceState() {
    Bundle bundle = new Bundle();
    bundle.putBoolean("android:dialogShowing", this.mShowing);
    if (this.mCreated)
      bundle.putBundle("android:dialogHierarchy", this.mWindow.saveHierarchyState()); 
    return bundle;
  }
  
  public void onRestoreInstanceState(Bundle paramBundle) {
    Bundle bundle = paramBundle.getBundle("android:dialogHierarchy");
    if (bundle == null)
      return; 
    dispatchOnCreate(paramBundle);
    this.mWindow.restoreHierarchyState(bundle);
    if (paramBundle.getBoolean("android:dialogShowing"))
      show(); 
  }
  
  public Window getWindow() {
    return this.mWindow;
  }
  
  public View getCurrentFocus() {
    Window window = this.mWindow;
    if (window != null) {
      View view = window.getCurrentFocus();
    } else {
      window = null;
    } 
    return (View)window;
  }
  
  public <T extends View> T findViewById(int paramInt) {
    return (T)this.mWindow.findViewById(paramInt);
  }
  
  public final <T extends View> T requireViewById(int paramInt) {
    T t = (T)findViewById(paramInt);
    if (t != null)
      return t; 
    throw new IllegalArgumentException("ID does not reference a View inside this Dialog");
  }
  
  public void setContentView(int paramInt) {
    this.mWindow.setContentView(paramInt);
  }
  
  public void setContentView(View paramView) {
    this.mWindow.setContentView(paramView);
  }
  
  public void setContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    this.mWindow.setContentView(paramView, paramLayoutParams);
  }
  
  public void addContentView(View paramView, ViewGroup.LayoutParams paramLayoutParams) {
    this.mWindow.addContentView(paramView, paramLayoutParams);
  }
  
  public void setTitle(CharSequence paramCharSequence) {
    this.mWindow.setTitle(paramCharSequence);
    this.mWindow.getAttributes().setTitle(paramCharSequence);
  }
  
  public void setTitle(int paramInt) {
    setTitle(this.mContext.getText(paramInt));
  }
  
  public boolean onKeyDown(int paramInt, KeyEvent paramKeyEvent) {
    if (paramInt == 4 || paramInt == 111) {
      paramKeyEvent.startTracking();
      return true;
    } 
    return false;
  }
  
  public boolean onKeyLongPress(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onKeyUp(int paramInt, KeyEvent paramKeyEvent) {
    if ((paramInt == 4 || paramInt == 111) && 
      paramKeyEvent.isTracking() && 
      !paramKeyEvent.isCanceled()) {
      onBackPressed();
      return true;
    } 
    return false;
  }
  
  public boolean onKeyMultiple(int paramInt1, int paramInt2, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public void onBackPressed() {
    if (this.mCancelable)
      cancel(); 
  }
  
  public boolean onKeyShortcut(int paramInt, KeyEvent paramKeyEvent) {
    return false;
  }
  
  public boolean onTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mCancelable && this.mShowing && this.mWindow.shouldCloseOnTouch(this.mContext, paramMotionEvent)) {
      cancel();
      return true;
    } 
    return false;
  }
  
  public boolean onTrackballEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public boolean onGenericMotionEvent(MotionEvent paramMotionEvent) {
    return false;
  }
  
  public void onWindowAttributesChanged(WindowManager.LayoutParams paramLayoutParams) {
    View view = this.mDecor;
    if (view != null)
      this.mWindowManager.updateViewLayout(view, (ViewGroup.LayoutParams)paramLayoutParams); 
  }
  
  public void onContentChanged() {}
  
  public void onWindowFocusChanged(boolean paramBoolean) {}
  
  public void onAttachedToWindow() {}
  
  public void onDetachedFromWindow() {}
  
  public void onWindowDismissed(boolean paramBoolean1, boolean paramBoolean2) {
    dismiss();
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    DialogInterface.OnKeyListener onKeyListener = this.mOnKeyListener;
    if (onKeyListener != null && onKeyListener.onKey(this, paramKeyEvent.getKeyCode(), paramKeyEvent))
      return true; 
    if (this.mWindow.superDispatchKeyEvent(paramKeyEvent))
      return true; 
    View view = this.mDecor;
    if (view != null) {
      KeyEvent.DispatcherState dispatcherState = view.getKeyDispatcherState();
    } else {
      view = null;
    } 
    return paramKeyEvent.dispatch(this, (KeyEvent.DispatcherState)view, this);
  }
  
  public boolean dispatchKeyShortcutEvent(KeyEvent paramKeyEvent) {
    if (this.mWindow.superDispatchKeyShortcutEvent(paramKeyEvent))
      return true; 
    return onKeyShortcut(paramKeyEvent.getKeyCode(), paramKeyEvent);
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    if (this.mWindow.superDispatchTouchEvent(paramMotionEvent))
      return true; 
    return onTouchEvent(paramMotionEvent);
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    if (this.mWindow.superDispatchTrackballEvent(paramMotionEvent))
      return true; 
    return onTrackballEvent(paramMotionEvent);
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    if (this.mWindow.superDispatchGenericMotionEvent(paramMotionEvent))
      return true; 
    return onGenericMotionEvent(paramMotionEvent);
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent) {
    boolean bool;
    paramAccessibilityEvent.setClassName(getClass().getName());
    paramAccessibilityEvent.setPackageName(this.mContext.getPackageName());
    WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
    if (((ViewGroup.LayoutParams)layoutParams).width == -1 && ((ViewGroup.LayoutParams)layoutParams).height == -1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramAccessibilityEvent.setFullScreen(bool);
    return false;
  }
  
  public View onCreatePanelView(int paramInt) {
    return null;
  }
  
  public boolean onCreatePanelMenu(int paramInt, Menu paramMenu) {
    if (paramInt == 0)
      return onCreateOptionsMenu(paramMenu); 
    return false;
  }
  
  public boolean onPreparePanel(int paramInt, View paramView, Menu paramMenu) {
    boolean bool = true;
    if (paramInt == 0) {
      if (!onPrepareOptionsMenu(paramMenu) || !paramMenu.hasVisibleItems())
        bool = false; 
      return bool;
    } 
    return true;
  }
  
  public boolean onMenuOpened(int paramInt, Menu paramMenu) {
    if (paramInt == 8)
      this.mActionBar.dispatchMenuVisibilityChanged(true); 
    return true;
  }
  
  public boolean onMenuItemSelected(int paramInt, MenuItem paramMenuItem) {
    return false;
  }
  
  public void onPanelClosed(int paramInt, Menu paramMenu) {
    if (paramInt == 8)
      this.mActionBar.dispatchMenuVisibilityChanged(false); 
  }
  
  public boolean onCreateOptionsMenu(Menu paramMenu) {
    return true;
  }
  
  public boolean onPrepareOptionsMenu(Menu paramMenu) {
    return true;
  }
  
  public boolean onOptionsItemSelected(MenuItem paramMenuItem) {
    return false;
  }
  
  public void onOptionsMenuClosed(Menu paramMenu) {}
  
  public void openOptionsMenu() {
    if (this.mWindow.hasFeature(0))
      this.mWindow.openPanel(0, null); 
  }
  
  public void closeOptionsMenu() {
    if (this.mWindow.hasFeature(0))
      this.mWindow.closePanel(0); 
  }
  
  public void invalidateOptionsMenu() {
    if (this.mWindow.hasFeature(0))
      this.mWindow.invalidatePanelMenu(0); 
  }
  
  public void onCreateContextMenu(ContextMenu paramContextMenu, View paramView, ContextMenu.ContextMenuInfo paramContextMenuInfo) {}
  
  public void registerForContextMenu(View paramView) {
    paramView.setOnCreateContextMenuListener(this);
  }
  
  public void unregisterForContextMenu(View paramView) {
    paramView.setOnCreateContextMenuListener(null);
  }
  
  public void openContextMenu(View paramView) {
    paramView.showContextMenu();
  }
  
  public boolean onContextItemSelected(MenuItem paramMenuItem) {
    return false;
  }
  
  public void onContextMenuClosed(Menu paramMenu) {}
  
  public boolean onSearchRequested(SearchEvent paramSearchEvent) {
    this.mSearchEvent = paramSearchEvent;
    return onSearchRequested();
  }
  
  public boolean onSearchRequested() {
    Context context = this.mContext;
    SearchManager searchManager = (SearchManager)context.getSystemService("search");
    ComponentName componentName = getAssociatedActivity();
    if (componentName != null && searchManager.getSearchableInfo(componentName) != null) {
      searchManager.startSearch(null, false, componentName, null, false);
      dismiss();
      return true;
    } 
    return false;
  }
  
  public final SearchEvent getSearchEvent() {
    return this.mSearchEvent;
  }
  
  public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback) {
    ActionBar actionBar = this.mActionBar;
    if (actionBar != null && this.mActionModeTypeStarting == 0)
      return actionBar.startActionMode(paramCallback); 
    return null;
  }
  
  public ActionMode onWindowStartingActionMode(ActionMode.Callback paramCallback, int paramInt) {
    try {
      this.mActionModeTypeStarting = paramInt;
      return onWindowStartingActionMode(paramCallback);
    } finally {
      this.mActionModeTypeStarting = 0;
    } 
  }
  
  public void onActionModeStarted(ActionMode paramActionMode) {
    this.mActionMode = paramActionMode;
  }
  
  public void onActionModeFinished(ActionMode paramActionMode) {
    if (paramActionMode == this.mActionMode)
      this.mActionMode = null; 
  }
  
  private ComponentName getAssociatedActivity() {
    ComponentName componentName;
    Context context2;
    Activity activity = this.mOwnerActivity;
    Context context1 = getContext();
    while (true) {
      context2 = null;
      Context context = null;
      if (activity == null && context1 != null) {
        if (context1 instanceof Activity) {
          activity = (Activity)context1;
          continue;
        } 
        if (context1 instanceof ContextWrapper) {
          context1 = ((ContextWrapper)context1).getBaseContext();
          continue;
        } 
        context1 = context;
        continue;
      } 
      break;
    } 
    if (activity == null) {
      context1 = context2;
    } else {
      componentName = activity.getComponentName();
    } 
    return componentName;
  }
  
  public void takeKeyEvents(boolean paramBoolean) {
    this.mWindow.takeKeyEvents(paramBoolean);
  }
  
  public final boolean requestWindowFeature(int paramInt) {
    return getWindow().requestFeature(paramInt);
  }
  
  public final void setFeatureDrawableResource(int paramInt1, int paramInt2) {
    getWindow().setFeatureDrawableResource(paramInt1, paramInt2);
  }
  
  public final void setFeatureDrawableUri(int paramInt, Uri paramUri) {
    getWindow().setFeatureDrawableUri(paramInt, paramUri);
  }
  
  public final void setFeatureDrawable(int paramInt, Drawable paramDrawable) {
    getWindow().setFeatureDrawable(paramInt, paramDrawable);
  }
  
  public final void setFeatureDrawableAlpha(int paramInt1, int paramInt2) {
    getWindow().setFeatureDrawableAlpha(paramInt1, paramInt2);
  }
  
  public LayoutInflater getLayoutInflater() {
    return getWindow().getLayoutInflater();
  }
  
  public void setCancelable(boolean paramBoolean) {
    this.mCancelable = paramBoolean;
  }
  
  public void setCanceledOnTouchOutside(boolean paramBoolean) {
    if (paramBoolean && !this.mCancelable)
      this.mCancelable = true; 
    this.mWindow.setCloseOnTouchOutside(paramBoolean);
  }
  
  public void cancel() {
    if (!this.mCanceled) {
      Message message = this.mCancelMessage;
      if (message != null) {
        this.mCanceled = true;
        Message.obtain(message).sendToTarget();
      } 
    } 
    dismiss();
  }
  
  public void setOnCancelListener(DialogInterface.OnCancelListener paramOnCancelListener) {
    if (this.mCancelAndDismissTaken == null) {
      if (paramOnCancelListener != null) {
        this.mCancelMessage = this.mListenersHandler.obtainMessage(68, paramOnCancelListener);
      } else {
        this.mCancelMessage = null;
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OnCancelListener is already taken by ");
    stringBuilder.append(this.mCancelAndDismissTaken);
    stringBuilder.append(" and can not be replaced.");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void setCancelMessage(Message paramMessage) {
    this.mCancelMessage = paramMessage;
  }
  
  public void setOnDismissListener(DialogInterface.OnDismissListener paramOnDismissListener) {
    if (this.mCancelAndDismissTaken == null) {
      if (paramOnDismissListener != null) {
        this.mDismissMessage = this.mListenersHandler.obtainMessage(67, paramOnDismissListener);
      } else {
        this.mDismissMessage = null;
      } 
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OnDismissListener is already taken by ");
    stringBuilder.append(this.mCancelAndDismissTaken);
    stringBuilder.append(" and can not be replaced.");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void setOnShowListener(DialogInterface.OnShowListener paramOnShowListener) {
    if (paramOnShowListener != null) {
      this.mShowMessage = this.mListenersHandler.obtainMessage(69, paramOnShowListener);
    } else {
      this.mShowMessage = null;
    } 
  }
  
  public void setDismissMessage(Message paramMessage) {
    this.mDismissMessage = paramMessage;
  }
  
  public boolean takeCancelAndDismissListeners(String paramString, DialogInterface.OnCancelListener paramOnCancelListener, DialogInterface.OnDismissListener paramOnDismissListener) {
    if (this.mCancelAndDismissTaken != null) {
      this.mCancelAndDismissTaken = null;
    } else if (this.mCancelMessage != null || this.mDismissMessage != null) {
      return false;
    } 
    setOnCancelListener(paramOnCancelListener);
    setOnDismissListener(paramOnDismissListener);
    this.mCancelAndDismissTaken = paramString;
    return true;
  }
  
  public final void setVolumeControlStream(int paramInt) {
    getWindow().setVolumeControlStream(paramInt);
  }
  
  public final int getVolumeControlStream() {
    return getWindow().getVolumeControlStream();
  }
  
  public void setOnKeyListener(DialogInterface.OnKeyListener paramOnKeyListener) {
    this.mOnKeyListener = paramOnKeyListener;
  }
  
  class ListenersHandler extends Handler {
    private final WeakReference<DialogInterface> mDialog;
    
    public ListenersHandler(Dialog this$0) {
      this.mDialog = new WeakReference<>(this$0);
    }
    
    public void handleMessage(Message param1Message) {
      switch (param1Message.what) {
        default:
          return;
        case 69:
          ((DialogInterface.OnShowListener)param1Message.obj).onShow(this.mDialog.get());
        case 68:
          ((DialogInterface.OnCancelListener)param1Message.obj).onCancel(this.mDialog.get());
        case 67:
          break;
      } 
      ((DialogInterface.OnDismissListener)param1Message.obj).onDismiss(this.mDialog.get());
    }
  }
}
