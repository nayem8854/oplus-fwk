package android.app;

import android.annotation.IntRange;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import java.util.Objects;

public final class SyncNotedAppOp implements Parcelable {
  public SyncNotedAppOp(int paramInt, String paramString) {
    this.mOpCode = paramInt;
    AnnotationValidations.validate(IntRange.class, null, paramInt, "from", 0L, "to", 99L);
    this.mAttributionTag = paramString;
  }
  
  public String getOp() {
    return AppOpsManager.opToPublicName(this.mOpCode);
  }
  
  public String getAttributionTag() {
    return this.mAttributionTag;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    SyncNotedAppOp syncNotedAppOp = (SyncNotedAppOp)paramObject;
    if (this.mOpCode == syncNotedAppOp.mOpCode) {
      paramObject = this.mAttributionTag;
      String str = syncNotedAppOp.mAttributionTag;
      if (Objects.equals(paramObject, str))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mOpCode;
    int j = Objects.hashCode(this.mAttributionTag);
    return (1 * 31 + i) * 31 + j;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b = 0;
    if (this.mAttributionTag != null)
      b = (byte)(0x0 | 0x2); 
    paramParcel.writeByte(b);
    paramParcel.writeInt(this.mOpCode);
    String str = this.mAttributionTag;
    if (str != null)
      paramParcel.writeString(str); 
  }
  
  public int describeContents() {
    return 0;
  }
  
  SyncNotedAppOp(Parcel paramParcel) {
    String str;
    byte b = paramParcel.readByte();
    int i = paramParcel.readInt();
    if ((b & 0x2) == 0) {
      paramParcel = null;
    } else {
      str = paramParcel.readString();
    } 
    this.mOpCode = i;
    AnnotationValidations.validate(IntRange.class, null, i, "from", 0L, "to", 99L);
    this.mAttributionTag = str;
  }
  
  public static final Parcelable.Creator<SyncNotedAppOp> CREATOR = new Parcelable.Creator<SyncNotedAppOp>() {
      public SyncNotedAppOp[] newArray(int param1Int) {
        return new SyncNotedAppOp[param1Int];
      }
      
      public SyncNotedAppOp createFromParcel(Parcel param1Parcel) {
        return new SyncNotedAppOp(param1Parcel);
      }
    };
  
  private final String mAttributionTag;
  
  private final int mOpCode;
  
  @Deprecated
  private void __metadata() {}
}
