package android.app.role;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.RemoteCallback;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.Objects;
import java.util.function.BiConsumer;

@SystemApi
public abstract class RoleControllerService extends Service {
  public static final String SERVICE_INTERFACE = "android.app.role.RoleControllerService";
  
  private Handler mWorkerHandler;
  
  private HandlerThread mWorkerThread;
  
  public void onCreate() {
    super.onCreate();
    HandlerThread handlerThread = new HandlerThread(RoleControllerService.class.getSimpleName());
    handlerThread.start();
    this.mWorkerHandler = new Handler(this.mWorkerThread.getLooper());
  }
  
  public void onDestroy() {
    super.onDestroy();
    this.mWorkerThread.quitSafely();
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return (IBinder)new IRoleController.Stub() {
        final RoleControllerService this$0;
        
        public void grantDefaultRoles(RemoteCallback param1RemoteCallback) {
          enforceCallerSystemUid("grantDefaultRoles");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          RoleControllerService.this.mWorkerHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$RoleControllerService$1$_fmj7uDKaG3BoLl6bhtrA675gRI.INSTANCE, RoleControllerService.this, param1RemoteCallback));
        }
        
        public void onAddRoleHolder(String param1String1, String param1String2, int param1Int, RemoteCallback param1RemoteCallback) {
          enforceCallerSystemUid("onAddRoleHolder");
          Preconditions.checkStringNotEmpty(param1String1, "roleName cannot be null or empty");
          Preconditions.checkStringNotEmpty(param1String2, "packageName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          Handler handler = RoleControllerService.this.mWorkerHandler;
          -$.Lambda.RoleControllerService.null.UVI1sAWAcBnt3Enqn2IT-Lirwtk uVI1sAWAcBnt3Enqn2IT-Lirwtk = _$$Lambda$RoleControllerService$1$UVI1sAWAcBnt3Enqn2IT_Lirwtk.INSTANCE;
          RoleControllerService roleControllerService = RoleControllerService.this;
          handler.sendMessage(PooledLambda.obtainMessage((QuintConsumer)uVI1sAWAcBnt3Enqn2IT-Lirwtk, roleControllerService, param1String1, param1String2, Integer.valueOf(param1Int), param1RemoteCallback));
        }
        
        public void onRemoveRoleHolder(String param1String1, String param1String2, int param1Int, RemoteCallback param1RemoteCallback) {
          enforceCallerSystemUid("onRemoveRoleHolder");
          Preconditions.checkStringNotEmpty(param1String1, "roleName cannot be null or empty");
          Preconditions.checkStringNotEmpty(param1String2, "packageName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          Handler handler = RoleControllerService.this.mWorkerHandler;
          -$.Lambda.RoleControllerService.null.PB6H1df6VvLzUJ3hhB_75mN3u7s pB6H1df6VvLzUJ3hhB_75mN3u7s = _$$Lambda$RoleControllerService$1$PB6H1df6VvLzUJ3hhB_75mN3u7s.INSTANCE;
          RoleControllerService roleControllerService = RoleControllerService.this;
          handler.sendMessage(PooledLambda.obtainMessage((QuintConsumer)pB6H1df6VvLzUJ3hhB_75mN3u7s, roleControllerService, param1String1, param1String2, Integer.valueOf(param1Int), param1RemoteCallback));
        }
        
        public void onClearRoleHolders(String param1String, int param1Int, RemoteCallback param1RemoteCallback) {
          enforceCallerSystemUid("onClearRoleHolders");
          Preconditions.checkStringNotEmpty(param1String, "roleName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          Handler handler = RoleControllerService.this.mWorkerHandler;
          -$.Lambda.RoleControllerService.null.dBm1t_MGyEA9yMAxoOUMOhYVmPo dBm1t_MGyEA9yMAxoOUMOhYVmPo = _$$Lambda$RoleControllerService$1$dBm1t_MGyEA9yMAxoOUMOhYVmPo.INSTANCE;
          RoleControllerService roleControllerService = RoleControllerService.this;
          handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)dBm1t_MGyEA9yMAxoOUMOhYVmPo, roleControllerService, param1String, Integer.valueOf(param1Int), param1RemoteCallback));
        }
        
        private void enforceCallerSystemUid(String param1String) {
          if (Binder.getCallingUid() == 1000)
            return; 
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Only the system process can call ");
          stringBuilder.append(param1String);
          stringBuilder.append("()");
          throw new SecurityException(stringBuilder.toString());
        }
        
        public void isApplicationQualifiedForRole(String param1String1, String param1String2, RemoteCallback param1RemoteCallback) {
          Bundle bundle;
          RoleControllerService roleControllerService = RoleControllerService.this;
          String str = null;
          roleControllerService.enforceCallingPermission("android.permission.MANAGE_ROLE_HOLDERS", null);
          Preconditions.checkStringNotEmpty(param1String1, "roleName cannot be null or empty");
          Preconditions.checkStringNotEmpty(param1String2, "packageName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          boolean bool = RoleControllerService.this.onIsApplicationQualifiedForRole(param1String1, param1String2);
          param1String1 = str;
          if (bool)
            bundle = Bundle.EMPTY; 
          param1RemoteCallback.sendResult(bundle);
        }
        
        public void isApplicationVisibleForRole(String param1String1, String param1String2, RemoteCallback param1RemoteCallback) {
          Bundle bundle;
          RoleControllerService roleControllerService = RoleControllerService.this;
          String str = null;
          roleControllerService.enforceCallingPermission("android.permission.MANAGE_ROLE_HOLDERS", null);
          Preconditions.checkStringNotEmpty(param1String1, "roleName cannot be null or empty");
          Preconditions.checkStringNotEmpty(param1String2, "packageName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          boolean bool = RoleControllerService.this.onIsApplicationVisibleForRole(param1String1, param1String2);
          param1String1 = str;
          if (bool)
            bundle = Bundle.EMPTY; 
          param1RemoteCallback.sendResult(bundle);
        }
        
        public void isRoleVisible(String param1String, RemoteCallback param1RemoteCallback) {
          Bundle bundle;
          RoleControllerService roleControllerService = RoleControllerService.this;
          String str = null;
          roleControllerService.enforceCallingPermission("android.permission.MANAGE_ROLE_HOLDERS", null);
          Preconditions.checkStringNotEmpty(param1String, "roleName cannot be null or empty");
          Objects.requireNonNull(param1RemoteCallback, "callback cannot be null");
          boolean bool = RoleControllerService.this.onIsRoleVisible(param1String);
          param1String = str;
          if (bool)
            bundle = Bundle.EMPTY; 
          param1RemoteCallback.sendResult(bundle);
        }
      };
  }
  
  private void grantDefaultRoles(RemoteCallback paramRemoteCallback) {
    Bundle bundle;
    boolean bool = onGrantDefaultRoles();
    if (bool) {
      bundle = Bundle.EMPTY;
    } else {
      bundle = null;
    } 
    paramRemoteCallback.sendResult(bundle);
  }
  
  private void onAddRoleHolder(String paramString1, String paramString2, int paramInt, RemoteCallback paramRemoteCallback) {
    boolean bool = onAddRoleHolder(paramString1, paramString2, paramInt);
    if (bool) {
      Bundle bundle = Bundle.EMPTY;
    } else {
      paramString1 = null;
    } 
    paramRemoteCallback.sendResult((Bundle)paramString1);
  }
  
  private void onRemoveRoleHolder(String paramString1, String paramString2, int paramInt, RemoteCallback paramRemoteCallback) {
    boolean bool = onRemoveRoleHolder(paramString1, paramString2, paramInt);
    if (bool) {
      Bundle bundle = Bundle.EMPTY;
    } else {
      paramString1 = null;
    } 
    paramRemoteCallback.sendResult((Bundle)paramString1);
  }
  
  private void onClearRoleHolders(String paramString, int paramInt, RemoteCallback paramRemoteCallback) {
    boolean bool = onClearRoleHolders(paramString, paramInt);
    if (bool) {
      Bundle bundle = Bundle.EMPTY;
    } else {
      paramString = null;
    } 
    paramRemoteCallback.sendResult((Bundle)paramString);
  }
  
  public boolean onIsApplicationVisibleForRole(String paramString1, String paramString2) {
    return onIsApplicationQualifiedForRole(paramString1, paramString2);
  }
  
  public abstract boolean onAddRoleHolder(String paramString1, String paramString2, int paramInt);
  
  public abstract boolean onClearRoleHolders(String paramString, int paramInt);
  
  public abstract boolean onGrantDefaultRoles();
  
  public abstract boolean onIsApplicationQualifiedForRole(String paramString1, String paramString2);
  
  public abstract boolean onIsRoleVisible(String paramString);
  
  public abstract boolean onRemoveRoleHolder(String paramString1, String paramString2, int paramInt);
}
