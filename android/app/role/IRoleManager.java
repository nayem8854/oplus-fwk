package android.app.role;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteCallback;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IRoleManager extends IInterface {
  void addOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener paramIOnRoleHoldersChangedListener, int paramInt) throws RemoteException;
  
  void addRoleHolderAsUser(String paramString1, String paramString2, int paramInt1, int paramInt2, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  boolean addRoleHolderFromController(String paramString1, String paramString2) throws RemoteException;
  
  void clearRoleHoldersAsUser(String paramString, int paramInt1, int paramInt2, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  String getDefaultSmsPackage(int paramInt) throws RemoteException;
  
  List<String> getHeldRolesFromController(String paramString) throws RemoteException;
  
  List<String> getRoleHoldersAsUser(String paramString, int paramInt) throws RemoteException;
  
  boolean isRoleAvailable(String paramString) throws RemoteException;
  
  boolean isRoleHeld(String paramString1, String paramString2) throws RemoteException;
  
  void removeOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener paramIOnRoleHoldersChangedListener, int paramInt) throws RemoteException;
  
  void removeRoleHolderAsUser(String paramString1, String paramString2, int paramInt1, int paramInt2, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  boolean removeRoleHolderFromController(String paramString1, String paramString2) throws RemoteException;
  
  void setRoleNamesFromController(List<String> paramList) throws RemoteException;
  
  class Default implements IRoleManager {
    public boolean isRoleAvailable(String param1String) throws RemoteException {
      return false;
    }
    
    public boolean isRoleHeld(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public List<String> getRoleHoldersAsUser(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void addRoleHolderAsUser(String param1String1, String param1String2, int param1Int1, int param1Int2, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void removeRoleHolderAsUser(String param1String1, String param1String2, int param1Int1, int param1Int2, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void clearRoleHoldersAsUser(String param1String, int param1Int1, int param1Int2, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public void addOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener param1IOnRoleHoldersChangedListener, int param1Int) throws RemoteException {}
    
    public void removeOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener param1IOnRoleHoldersChangedListener, int param1Int) throws RemoteException {}
    
    public void setRoleNamesFromController(List<String> param1List) throws RemoteException {}
    
    public boolean addRoleHolderFromController(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean removeRoleHolderFromController(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public List<String> getHeldRolesFromController(String param1String) throws RemoteException {
      return null;
    }
    
    public String getDefaultSmsPackage(int param1Int) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRoleManager {
    private static final String DESCRIPTOR = "android.app.role.IRoleManager";
    
    static final int TRANSACTION_addOnRoleHoldersChangedListenerAsUser = 7;
    
    static final int TRANSACTION_addRoleHolderAsUser = 4;
    
    static final int TRANSACTION_addRoleHolderFromController = 10;
    
    static final int TRANSACTION_clearRoleHoldersAsUser = 6;
    
    static final int TRANSACTION_getDefaultSmsPackage = 13;
    
    static final int TRANSACTION_getHeldRolesFromController = 12;
    
    static final int TRANSACTION_getRoleHoldersAsUser = 3;
    
    static final int TRANSACTION_isRoleAvailable = 1;
    
    static final int TRANSACTION_isRoleHeld = 2;
    
    static final int TRANSACTION_removeOnRoleHoldersChangedListenerAsUser = 8;
    
    static final int TRANSACTION_removeRoleHolderAsUser = 5;
    
    static final int TRANSACTION_removeRoleHolderFromController = 11;
    
    static final int TRANSACTION_setRoleNamesFromController = 9;
    
    public Stub() {
      attachInterface(this, "android.app.role.IRoleManager");
    }
    
    public static IRoleManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.role.IRoleManager");
      if (iInterface != null && iInterface instanceof IRoleManager)
        return (IRoleManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "getDefaultSmsPackage";
        case 12:
          return "getHeldRolesFromController";
        case 11:
          return "removeRoleHolderFromController";
        case 10:
          return "addRoleHolderFromController";
        case 9:
          return "setRoleNamesFromController";
        case 8:
          return "removeOnRoleHoldersChangedListenerAsUser";
        case 7:
          return "addOnRoleHoldersChangedListenerAsUser";
        case 6:
          return "clearRoleHoldersAsUser";
        case 5:
          return "removeRoleHolderAsUser";
        case 4:
          return "addRoleHolderAsUser";
        case 3:
          return "getRoleHoldersAsUser";
        case 2:
          return "isRoleHeld";
        case 1:
          break;
      } 
      return "isRoleAvailable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int i;
        String str3;
        List<String> list2;
        String str2;
        ArrayList<String> arrayList;
        List<String> list1;
        String str5;
        IOnRoleHoldersChangedListener iOnRoleHoldersChangedListener;
        String str4, str6;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.app.role.IRoleManager");
            param1Int1 = param1Parcel1.readInt();
            str3 = getDefaultSmsPackage(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 12:
            str3.enforceInterface("android.app.role.IRoleManager");
            str3 = str3.readString();
            list2 = getHeldRolesFromController(str3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 11:
            list2.enforceInterface("android.app.role.IRoleManager");
            str5 = list2.readString();
            str2 = list2.readString();
            bool2 = removeRoleHolderFromController(str5, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 10:
            str2.enforceInterface("android.app.role.IRoleManager");
            str5 = str2.readString();
            str2 = str2.readString();
            bool2 = addRoleHolderFromController(str5, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            str2.enforceInterface("android.app.role.IRoleManager");
            arrayList = str2.createStringArrayList();
            setRoleNamesFromController(arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            iOnRoleHoldersChangedListener = IOnRoleHoldersChangedListener.Stub.asInterface(arrayList.readStrongBinder());
            i = arrayList.readInt();
            removeOnRoleHoldersChangedListenerAsUser(iOnRoleHoldersChangedListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            iOnRoleHoldersChangedListener = IOnRoleHoldersChangedListener.Stub.asInterface(arrayList.readStrongBinder());
            i = arrayList.readInt();
            addOnRoleHoldersChangedListenerAsUser(iOnRoleHoldersChangedListener, i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            str4 = arrayList.readString();
            i = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            if (arrayList.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            clearRoleHoldersAsUser(str4, i, param1Int2, (RemoteCallback)arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            str4 = arrayList.readString();
            str6 = arrayList.readString();
            i = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            if (arrayList.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            removeRoleHolderAsUser(str4, str6, i, param1Int2, (RemoteCallback)arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            str6 = arrayList.readString();
            str4 = arrayList.readString();
            param1Int2 = arrayList.readInt();
            i = arrayList.readInt();
            if (arrayList.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            addRoleHolderAsUser(str6, str4, param1Int2, i, (RemoteCallback)arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayList.enforceInterface("android.app.role.IRoleManager");
            str4 = arrayList.readString();
            i = arrayList.readInt();
            list1 = getRoleHoldersAsUser(str4, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 2:
            list1.enforceInterface("android.app.role.IRoleManager");
            str4 = list1.readString();
            str1 = list1.readString();
            bool1 = isRoleHeld(str4, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.app.role.IRoleManager");
        String str1 = str1.readString();
        boolean bool1 = isRoleAvailable(str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.app.role.IRoleManager");
      return true;
    }
    
    private static class Proxy implements IRoleManager {
      public static IRoleManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.role.IRoleManager";
      }
      
      public boolean isRoleAvailable(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IRoleManager.Stub.getDefaultImpl() != null) {
            bool1 = IRoleManager.Stub.getDefaultImpl().isRoleAvailable(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRoleHeld(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IRoleManager.Stub.getDefaultImpl() != null) {
            bool1 = IRoleManager.Stub.getDefaultImpl().isRoleHeld(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getRoleHoldersAsUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null)
            return IRoleManager.Stub.getDefaultImpl().getRoleHoldersAsUser(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addRoleHolderAsUser(String param2String1, String param2String2, int param2Int1, int param2Int2, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().addRoleHolderAsUser(param2String1, param2String2, param2Int1, param2Int2, param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeRoleHolderAsUser(String param2String1, String param2String2, int param2Int1, int param2Int2, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().removeRoleHolderAsUser(param2String1, param2String2, param2Int1, param2Int2, param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearRoleHoldersAsUser(String param2String, int param2Int1, int param2Int2, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2RemoteCallback != null) {
            parcel1.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().clearRoleHoldersAsUser(param2String, param2Int1, param2Int2, param2RemoteCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener param2IOnRoleHoldersChangedListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          if (param2IOnRoleHoldersChangedListener != null) {
            iBinder = param2IOnRoleHoldersChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().addOnRoleHoldersChangedListenerAsUser(param2IOnRoleHoldersChangedListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnRoleHoldersChangedListenerAsUser(IOnRoleHoldersChangedListener param2IOnRoleHoldersChangedListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          if (param2IOnRoleHoldersChangedListener != null) {
            iBinder = param2IOnRoleHoldersChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().removeOnRoleHoldersChangedListenerAsUser(param2IOnRoleHoldersChangedListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRoleNamesFromController(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null) {
            IRoleManager.Stub.getDefaultImpl().setRoleNamesFromController(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addRoleHolderFromController(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IRoleManager.Stub.getDefaultImpl() != null) {
            bool1 = IRoleManager.Stub.getDefaultImpl().addRoleHolderFromController(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeRoleHolderFromController(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IRoleManager.Stub.getDefaultImpl() != null) {
            bool1 = IRoleManager.Stub.getDefaultImpl().removeRoleHolderFromController(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getHeldRolesFromController(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null)
            return IRoleManager.Stub.getDefaultImpl().getHeldRolesFromController(param2String); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultSmsPackage(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.role.IRoleManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IRoleManager.Stub.getDefaultImpl() != null)
            return IRoleManager.Stub.getDefaultImpl().getDefaultSmsPackage(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRoleManager param1IRoleManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRoleManager != null) {
          Proxy.sDefaultImpl = param1IRoleManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRoleManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
