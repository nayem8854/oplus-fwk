package android.app.role;

import android.app.ActivityThread;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.RemoteCallback;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.infra.AndroidFuture;
import com.android.internal.infra.ServiceConnector;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;

public class RoleControllerManager {
  private static final String LOG_TAG = RoleControllerManager.class.getSimpleName();
  
  private static final long REQUEST_TIMEOUT_MILLIS = 15000L;
  
  private static volatile ComponentName sRemoteServiceComponentName;
  
  private static final SparseArray<ServiceConnector<IRoleController>> sRemoteServices;
  
  private static final Object sRemoteServicesLock = new Object();
  
  private final ServiceConnector<IRoleController> mRemoteService;
  
  static {
    sRemoteServices = new SparseArray();
  }
  
  public static void initializeRemoteServiceComponentName(Context paramContext) {
    sRemoteServiceComponentName = getRemoteServiceComponentName(paramContext);
  }
  
  public static RoleControllerManager createWithInitializedRemoteServiceComponentName(Handler paramHandler, Context paramContext) {
    return new RoleControllerManager(sRemoteServiceComponentName, paramHandler, paramContext);
  }
  
  private RoleControllerManager(ComponentName paramComponentName, Handler paramHandler, Context paramContext) {
    synchronized (sRemoteServicesLock) {
      int i = paramContext.getUserId();
      ServiceConnector serviceConnector = (ServiceConnector)sRemoteServices.get(i);
      Object object = serviceConnector;
      if (serviceConnector == null) {
        object = new Object();
        Application application = ActivityThread.currentApplication();
        Intent intent = new Intent();
        this("android.app.role.RoleControllerService");
        super(this, application, intent.setComponent(paramComponentName), 0, i, (Function)_$$Lambda$Z0BwIRmLFQVA4XrF_I5nxvuecWE.INSTANCE, paramHandler);
        sRemoteServices.put(i, object);
      } 
      this.mRemoteService = (ServiceConnector<IRoleController>)object;
      return;
    } 
  }
  
  public RoleControllerManager(Context paramContext) {
    this(getRemoteServiceComponentName(paramContext), paramContext.getMainThreadHandler(), paramContext);
  }
  
  private static ComponentName getRemoteServiceComponentName(Context paramContext) {
    Intent intent = new Intent("android.app.role.RoleControllerService");
    PackageManager packageManager = paramContext.getPackageManager();
    intent.setPackage(packageManager.getPermissionControllerPackageName());
    ResolveInfo resolveInfo = packageManager.resolveService(intent, 0);
    return resolveInfo.getComponentInfo().getComponentName();
  }
  
  public void grantDefaultRoles(Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync((ServiceConnector.Job)_$$Lambda$RoleControllerManager$Jsb4ev7pHUqel8_lglNSRLiUzpg.INSTANCE);
    propagateCallback(androidFuture, "grantDefaultRoles", paramExecutor, paramConsumer);
  }
  
  public void onAddRoleHolder(String paramString1, String paramString2, int paramInt, RemoteCallback paramRemoteCallback) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$GiyGeOpnMIVwipW_81KV8TogKt8(paramString1, paramString2, paramInt));
    propagateCallback(androidFuture, "onAddRoleHolder", paramRemoteCallback);
  }
  
  public void onRemoveRoleHolder(String paramString1, String paramString2, int paramInt, RemoteCallback paramRemoteCallback) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$_qXKDWwgESwB52qfoJN7JTZsDiU(paramString1, paramString2, paramInt));
    propagateCallback(androidFuture, "onRemoveRoleHolder", paramRemoteCallback);
  }
  
  public void onClearRoleHolders(String paramString, int paramInt, RemoteCallback paramRemoteCallback) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$lBbpGLk6PhAvXOnY5bjXhdddZ6Q(paramString, paramInt));
    propagateCallback(androidFuture, "onClearRoleHolders", paramRemoteCallback);
  }
  
  public void isApplicationQualifiedForRole(String paramString1, String paramString2, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$ReKCWj8qlXAul532f7t4g53Ivg0(paramString1, paramString2));
    propagateCallback(androidFuture, "isApplicationQualifiedForRole", paramExecutor, paramConsumer);
  }
  
  public void isApplicationVisibleForRole(String paramString1, String paramString2, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$aq0cHbejAFqnsX1ZigMc9Dz1D5A(paramString1, paramString2));
    propagateCallback(androidFuture, "isApplicationVisibleForRole", paramExecutor, paramConsumer);
  }
  
  public void isRoleVisible(String paramString, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    AndroidFuture<Bundle> androidFuture = this.mRemoteService.postAsync(new _$$Lambda$RoleControllerManager$VsbRaGFueP4e1AEVbN4zwbUmdZU(paramString));
    propagateCallback(androidFuture, "isRoleVisible", paramExecutor, paramConsumer);
  }
  
  private void propagateCallback(AndroidFuture<Bundle> paramAndroidFuture, String paramString, Executor paramExecutor, Consumer<Boolean> paramConsumer) {
    paramAndroidFuture = paramAndroidFuture.orTimeout(15000L, TimeUnit.MILLISECONDS);
    _$$Lambda$RoleControllerManager$hbh627Rh8mtJykW3vb1FWR34mIQ _$$Lambda$RoleControllerManager$hbh627Rh8mtJykW3vb1FWR34mIQ = new _$$Lambda$RoleControllerManager$hbh627Rh8mtJykW3vb1FWR34mIQ(paramExecutor, paramString, paramConsumer);
    paramAndroidFuture.whenComplete(_$$Lambda$RoleControllerManager$hbh627Rh8mtJykW3vb1FWR34mIQ);
  }
  
  private void propagateCallback(AndroidFuture<Bundle> paramAndroidFuture, String paramString, RemoteCallback paramRemoteCallback) {
    paramAndroidFuture = paramAndroidFuture.orTimeout(15000L, TimeUnit.MILLISECONDS);
    _$$Lambda$RoleControllerManager$1tkryWxs4VtGp5AmGDyjpvBjC28 _$$Lambda$RoleControllerManager$1tkryWxs4VtGp5AmGDyjpvBjC28 = new _$$Lambda$RoleControllerManager$1tkryWxs4VtGp5AmGDyjpvBjC28(paramString, paramRemoteCallback);
    paramAndroidFuture.whenComplete(_$$Lambda$RoleControllerManager$1tkryWxs4VtGp5AmGDyjpvBjC28);
  }
}
