package android.app;

import android.annotation.SystemApi;
import android.os.Bundle;

@SystemApi
public class BroadcastOptions {
  private long mTemporaryAppWhitelistDuration;
  
  private int mMinManifestReceiverApiLevel = 0;
  
  private int mMaxManifestReceiverApiLevel = 10000;
  
  private boolean mDontSendToRestrictedApps = false;
  
  private boolean mAllowBackgroundActivityStarts;
  
  static final String KEY_TEMPORARY_APP_WHITELIST_DURATION = "android:broadcast.temporaryAppWhitelistDuration";
  
  static final String KEY_MIN_MANIFEST_RECEIVER_API_LEVEL = "android:broadcast.minManifestReceiverApiLevel";
  
  static final String KEY_MAX_MANIFEST_RECEIVER_API_LEVEL = "android:broadcast.maxManifestReceiverApiLevel";
  
  static final String KEY_DONT_SEND_TO_RESTRICTED_APPS = "android:broadcast.dontSendToRestrictedApps";
  
  static final String KEY_ALLOW_BACKGROUND_ACTIVITY_STARTS = "android:broadcast.allowBackgroundActivityStarts";
  
  public static BroadcastOptions makeBasic() {
    return new BroadcastOptions();
  }
  
  public BroadcastOptions(Bundle paramBundle) {
    this.mTemporaryAppWhitelistDuration = paramBundle.getLong("android:broadcast.temporaryAppWhitelistDuration");
    this.mMinManifestReceiverApiLevel = paramBundle.getInt("android:broadcast.minManifestReceiverApiLevel", 0);
    this.mMaxManifestReceiverApiLevel = paramBundle.getInt("android:broadcast.maxManifestReceiverApiLevel", 10000);
    this.mDontSendToRestrictedApps = paramBundle.getBoolean("android:broadcast.dontSendToRestrictedApps", false);
    this.mAllowBackgroundActivityStarts = paramBundle.getBoolean("android:broadcast.allowBackgroundActivityStarts", false);
  }
  
  public void setTemporaryAppWhitelistDuration(long paramLong) {
    this.mTemporaryAppWhitelistDuration = paramLong;
  }
  
  public long getTemporaryAppWhitelistDuration() {
    return this.mTemporaryAppWhitelistDuration;
  }
  
  public void setMinManifestReceiverApiLevel(int paramInt) {
    this.mMinManifestReceiverApiLevel = paramInt;
  }
  
  public int getMinManifestReceiverApiLevel() {
    return this.mMinManifestReceiverApiLevel;
  }
  
  public void setMaxManifestReceiverApiLevel(int paramInt) {
    this.mMaxManifestReceiverApiLevel = paramInt;
  }
  
  public int getMaxManifestReceiverApiLevel() {
    return this.mMaxManifestReceiverApiLevel;
  }
  
  public void setDontSendToRestrictedApps(boolean paramBoolean) {
    this.mDontSendToRestrictedApps = paramBoolean;
  }
  
  public boolean isDontSendToRestrictedApps() {
    return this.mDontSendToRestrictedApps;
  }
  
  public void setBackgroundActivityStartsAllowed(boolean paramBoolean) {
    this.mAllowBackgroundActivityStarts = paramBoolean;
  }
  
  public boolean allowsBackgroundActivityStarts() {
    return this.mAllowBackgroundActivityStarts;
  }
  
  public Bundle toBundle() {
    Bundle bundle = new Bundle();
    long l = this.mTemporaryAppWhitelistDuration;
    if (l > 0L)
      bundle.putLong("android:broadcast.temporaryAppWhitelistDuration", l); 
    int i = this.mMinManifestReceiverApiLevel;
    if (i != 0)
      bundle.putInt("android:broadcast.minManifestReceiverApiLevel", i); 
    i = this.mMaxManifestReceiverApiLevel;
    if (i != 10000)
      bundle.putInt("android:broadcast.maxManifestReceiverApiLevel", i); 
    if (this.mDontSendToRestrictedApps)
      bundle.putBoolean("android:broadcast.dontSendToRestrictedApps", true); 
    if (this.mAllowBackgroundActivityStarts)
      bundle.putBoolean("android:broadcast.allowBackgroundActivityStarts", true); 
    if (bundle.isEmpty())
      bundle = null; 
    return bundle;
  }
  
  private BroadcastOptions() {}
}
