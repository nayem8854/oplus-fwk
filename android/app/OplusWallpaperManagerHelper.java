package android.app;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Environment;
import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import com.oplus.multiuser.OplusMultiUserManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.Map;

public class OplusWallpaperManagerHelper {
  private static String BASE_OPPO_PRODUCT_DEFAULT_DIR;
  
  private static final String BASE_OPPO_WALLPAPER_DEFAULT_DIR = "/decouping_wallpaper/";
  
  private static final String CUSTOM_LOCK_WALLPAPER_NAME = "default_wallpaper_lock";
  
  private static final int CUSTOM_THEME_FLAG = 256;
  
  private static final String CUSTOM_WALLPAPER = "/custom";
  
  private static final String CUSTOM_WALLPAPER_NAME = "default_wallpaper";
  
  private static final String DEFAULT_LOCK_WALLPAPER_NAME = "oppo_default_wallpaper_lock";
  
  private static final String DEFAULT_MULTI_SYS_WALLPAPER_NAME = "oppo_default_multi_sys_wallpaper";
  
  private static final String DEFAULT_PATH_THEME = "default";
  
  private static final String DEFAULT_WALLPAPER_NAME = "oppo_default_wallpaper";
  
  private static final String OPERATOR = "operator";
  
  private static final String OPLUS_MODULE_DEFAULT_WALLPAPER_DIR = "/decouping_wallpaper/default/";
  
  private static final String OPPO_CUSTOM_ROOT_PATH = "/oppo_custom";
  
  private static final String OPPO_CUSTOM_WALLPAPER;
  
  private static final String OPPO_ENGINEERING_ROOT_PATH = "/oppo_engineering";
  
  private static final String OPPO_PRODUCT_ROOT_PATH = "/oppo_product";
  
  private static final String OPPO_VERSION_ROOT_PATH = "/oppo_version";
  
  private static final String PHONE_COLOR_MAPS_FILE_NAME = "phone_color_default_theme_maps";
  
  private static final String PHONE_COLOR_MAPS_FILE_SUFFIX = ".xml";
  
  private static final String PROP_HW_PHONE_COLOR = "ro.hw.phone.color";
  
  private static String TAG = "OplusWallpaperManagerHelper";
  
  private static final String TAG_DEFAULT_THEME_LOCK = "DefaultTheme_lock";
  
  private static final String TAG_DEFAULT_THEME_SYSTEM = "DefaultTheme_system";
  
  private static final String TAG_PHONE_COLOR = "PhoneColor";
  
  private static final String WALLPAPER_CUSTOM_FILE_DIR = "/media/wallpaper/default";
  
  private static final String WALLPAPER_SUFFIX = ".png";
  
  private static final String XML_ENCODING = "UTF-8";
  
  private static Map<Integer, String> sDefaultFileNameCache;
  
  static {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getDefaultWallpaperPath().getAbsolutePath());
    stringBuilder.append("/decouping_wallpaper/default/");
    BASE_OPPO_PRODUCT_DEFAULT_DIR = stringBuilder.toString();
    sDefaultFileNameCache = (Map<Integer, String>)new ArrayMap();
    OPPO_CUSTOM_WALLPAPER = getOplusCustomDirectory().getAbsolutePath();
  }
  
  public static int getDefaultWallpaperResID(Context paramContext) {
    int i = -1;
    String str = SystemProperties.get("ro.vendor.oplus.operator", "null");
    if (!"null".equals(str)) {
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("oppo_default_wallpaper_");
      stringBuilder2.append(str.toLowerCase());
      String str1 = stringBuilder2.toString();
      Resources resources = paramContext.getResources();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("oppo:drawable/");
      stringBuilder1.append(str1);
      i = resources.getIdentifier(stringBuilder1.toString(), null, null);
    } 
    int j = i;
    if (i <= 0) {
      String str1 = SystemProperties.get("persist.sys.oppo.region", "CN");
      if ("CN".equalsIgnoreCase(str1)) {
        str1 = "oppo_default_wallpaper";
      } else {
        str1 = "oppo_default_wallpaper_exp";
      } 
      Resources resources = paramContext.getResources();
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("oppo:drawable/");
      stringBuilder.append(str1);
      j = resources.getIdentifier(stringBuilder.toString(), null, null);
    } 
    i = j;
    if (j <= 0)
      i = 201850902; 
    return i;
  }
  
  public static Bitmap generateBitmap(Context paramContext, Bitmap paramBitmap) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 15
    //   4: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   7: ldc 'generateBitmap return bm = null'
    //   9: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   12: pop
    //   13: aconst_null
    //   14: areturn
    //   15: aload_0
    //   16: ldc 'window'
    //   18: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   21: checkcast android/view/WindowManager
    //   24: astore_0
    //   25: new android/util/DisplayMetrics
    //   28: dup
    //   29: invokespecial <init> : ()V
    //   32: astore_2
    //   33: aload_0
    //   34: invokeinterface getDefaultDisplay : ()Landroid/view/Display;
    //   39: aload_2
    //   40: invokevirtual getRealMetrics : (Landroid/util/DisplayMetrics;)V
    //   43: aload_1
    //   44: aload_2
    //   45: getfield noncompatDensityDpi : I
    //   48: invokevirtual setDensity : (I)V
    //   51: aload_2
    //   52: getfield widthPixels : I
    //   55: aload_2
    //   56: getfield heightPixels : I
    //   59: invokestatic max : (II)I
    //   62: istore_3
    //   63: aload_2
    //   64: getfield widthPixels : I
    //   67: aload_2
    //   68: getfield heightPixels : I
    //   71: invokestatic min : (II)I
    //   74: istore #4
    //   76: aload_1
    //   77: invokevirtual getWidth : ()I
    //   80: istore #5
    //   82: aload_1
    //   83: invokevirtual getHeight : ()I
    //   86: istore #6
    //   88: fconst_1
    //   89: fstore #7
    //   91: iload #6
    //   93: iload_3
    //   94: if_icmpge -> 104
    //   97: iload_3
    //   98: iload #6
    //   100: idiv
    //   101: i2f
    //   102: fstore #7
    //   104: iload #5
    //   106: i2f
    //   107: fload #7
    //   109: fmul
    //   110: iload #4
    //   112: i2f
    //   113: fcmpg
    //   114: ifgt -> 120
    //   117: goto -> 126
    //   120: iload #4
    //   122: iconst_2
    //   123: imul
    //   124: istore #4
    //   126: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   129: astore_0
    //   130: new java/lang/StringBuilder
    //   133: dup
    //   134: invokespecial <init> : ()V
    //   137: astore #8
    //   139: aload #8
    //   141: ldc_w 'generateBitmap desiredWidth = '
    //   144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: pop
    //   148: aload #8
    //   150: iload #4
    //   152: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload #8
    //   158: ldc_w ' desiredHeight = '
    //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload #8
    //   167: iload_3
    //   168: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload_0
    //   173: aload #8
    //   175: invokevirtual toString : ()Ljava/lang/String;
    //   178: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   181: pop
    //   182: iload #4
    //   184: ifle -> 488
    //   187: iload_3
    //   188: ifle -> 488
    //   191: aload_1
    //   192: invokevirtual getWidth : ()I
    //   195: iload #4
    //   197: if_icmpne -> 211
    //   200: aload_1
    //   201: invokevirtual getHeight : ()I
    //   204: iload_3
    //   205: if_icmpne -> 211
    //   208: goto -> 488
    //   211: iload #4
    //   213: iload_3
    //   214: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   217: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   220: astore_0
    //   221: aload_0
    //   222: aload_2
    //   223: getfield noncompatDensityDpi : I
    //   226: invokevirtual setDensity : (I)V
    //   229: new android/graphics/Canvas
    //   232: astore #9
    //   234: aload #9
    //   236: aload_0
    //   237: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   240: new android/graphics/Rect
    //   243: astore #10
    //   245: aload #10
    //   247: invokespecial <init> : ()V
    //   250: aload #10
    //   252: aload_1
    //   253: invokevirtual getWidth : ()I
    //   256: putfield right : I
    //   259: aload #10
    //   261: aload_1
    //   262: invokevirtual getHeight : ()I
    //   265: putfield bottom : I
    //   268: iload #4
    //   270: aload #10
    //   272: getfield right : I
    //   275: isub
    //   276: istore #5
    //   278: aload #10
    //   280: getfield bottom : I
    //   283: istore #6
    //   285: iload_3
    //   286: iload #6
    //   288: isub
    //   289: istore #6
    //   291: iload #5
    //   293: ifgt -> 311
    //   296: iload #6
    //   298: ifle -> 304
    //   301: goto -> 311
    //   304: iload #6
    //   306: istore #4
    //   308: goto -> 402
    //   311: iload #4
    //   313: i2f
    //   314: fstore #7
    //   316: fload #7
    //   318: aload #10
    //   320: getfield right : I
    //   323: i2f
    //   324: fdiv
    //   325: fstore #11
    //   327: iload_3
    //   328: i2f
    //   329: aload #10
    //   331: getfield bottom : I
    //   334: i2f
    //   335: fdiv
    //   336: fstore #7
    //   338: fload #11
    //   340: fload #7
    //   342: fcmpl
    //   343: ifle -> 353
    //   346: fload #11
    //   348: fstore #7
    //   350: goto -> 353
    //   353: aload #10
    //   355: aload #10
    //   357: getfield right : I
    //   360: i2f
    //   361: fload #7
    //   363: fmul
    //   364: f2i
    //   365: putfield right : I
    //   368: aload #10
    //   370: aload #10
    //   372: getfield bottom : I
    //   375: i2f
    //   376: fload #7
    //   378: fmul
    //   379: f2i
    //   380: putfield bottom : I
    //   383: iload #4
    //   385: aload #10
    //   387: getfield right : I
    //   390: isub
    //   391: istore #5
    //   393: iload_3
    //   394: aload #10
    //   396: getfield bottom : I
    //   399: isub
    //   400: istore #4
    //   402: aload #10
    //   404: iload #5
    //   406: iconst_2
    //   407: idiv
    //   408: iload #4
    //   410: iconst_2
    //   411: idiv
    //   412: invokevirtual offset : (II)V
    //   415: new android/graphics/Paint
    //   418: astore_2
    //   419: aload_2
    //   420: invokespecial <init> : ()V
    //   423: aload_2
    //   424: iconst_1
    //   425: invokevirtual setFilterBitmap : (Z)V
    //   428: new android/graphics/PorterDuffXfermode
    //   431: astore #8
    //   433: aload #8
    //   435: getstatic android/graphics/PorterDuff$Mode.SRC : Landroid/graphics/PorterDuff$Mode;
    //   438: invokespecial <init> : (Landroid/graphics/PorterDuff$Mode;)V
    //   441: aload_2
    //   442: aload #8
    //   444: invokevirtual setXfermode : (Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;
    //   447: pop
    //   448: aload #9
    //   450: aload_1
    //   451: aconst_null
    //   452: aload #10
    //   454: aload_2
    //   455: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    //   458: aload_1
    //   459: invokevirtual recycle : ()V
    //   462: aload #9
    //   464: aconst_null
    //   465: invokevirtual setBitmap : (Landroid/graphics/Bitmap;)V
    //   468: aload_0
    //   469: areturn
    //   470: astore_0
    //   471: goto -> 475
    //   474: astore_0
    //   475: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   478: ldc_w 'Can't generate default bitmap'
    //   481: aload_0
    //   482: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   485: pop
    //   486: aload_1
    //   487: areturn
    //   488: aload_1
    //   489: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #115	-> 0
    //   #116	-> 4
    //   #117	-> 13
    //   #120	-> 15
    //   #121	-> 25
    //   #122	-> 33
    //   #123	-> 43
    //   #125	-> 51
    //   #126	-> 63
    //   #128	-> 76
    //   #129	-> 82
    //   #131	-> 88
    //   #132	-> 88
    //   #133	-> 88
    //   #134	-> 91
    //   #135	-> 97
    //   #137	-> 104
    //   #138	-> 117
    //   #140	-> 120
    //   #143	-> 126
    //   #145	-> 182
    //   #146	-> 191
    //   #152	-> 211
    //   #153	-> 221
    //   #155	-> 229
    //   #156	-> 240
    //   #157	-> 250
    //   #158	-> 259
    //   #160	-> 268
    //   #161	-> 278
    //   #163	-> 291
    //   #167	-> 311
    //   #168	-> 327
    //   #169	-> 338
    //   #170	-> 353
    //   #171	-> 368
    //   #172	-> 383
    //   #173	-> 393
    //   #176	-> 402
    //   #178	-> 415
    //   #179	-> 423
    //   #180	-> 428
    //   #181	-> 448
    //   #183	-> 458
    //   #184	-> 462
    //   #185	-> 468
    //   #186	-> 470
    //   #187	-> 475
    //   #188	-> 486
    //   #145	-> 488
    //   #147	-> 488
    // Exception table:
    //   from	to	target	type
    //   211	221	474	java/lang/Exception
    //   221	229	474	java/lang/Exception
    //   229	240	474	java/lang/Exception
    //   240	250	474	java/lang/Exception
    //   250	259	474	java/lang/Exception
    //   259	268	474	java/lang/Exception
    //   268	278	474	java/lang/Exception
    //   278	285	474	java/lang/Exception
    //   316	327	470	java/lang/Exception
    //   327	338	470	java/lang/Exception
    //   353	368	470	java/lang/Exception
    //   368	383	470	java/lang/Exception
    //   383	393	470	java/lang/Exception
    //   393	402	470	java/lang/Exception
    //   402	415	470	java/lang/Exception
    //   415	423	470	java/lang/Exception
    //   423	428	470	java/lang/Exception
    //   428	448	470	java/lang/Exception
    //   448	458	470	java/lang/Exception
    //   458	462	470	java/lang/Exception
    //   462	468	470	java/lang/Exception
  }
  
  public static InputStream openDefaultWallpaper(Context paramContext, int paramInt) {
    InputStream inputStream;
    long l1 = System.currentTimeMillis();
    String str1 = getDefaultWallpaperFileName(paramContext, paramInt);
    if (str1 != null) {
      try {
        FileInputStream fileInputStream = new FileInputStream();
        this(str1);
        inputStream = fileInputStream;
      } catch (Exception exception) {
        inputStream = openDefaultWallpaperFromApkRes((Context)inputStream, paramInt);
        String str = TAG;
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("openDefaultWallpaper failed to open Stream fileName = ");
        stringBuilder1.append(str1);
        Log.d(str, stringBuilder1.toString());
      } 
    } else {
      inputStream = openDefaultWallpaperFromApkRes((Context)inputStream, paramInt);
      Log.d(TAG, "openDefaultWallpaper no file inner ");
    } 
    long l2 = System.currentTimeMillis();
    String str2 = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("openDefaultWallpaper costTime = ");
    stringBuilder.append(l2 - l1);
    stringBuilder.append(" fileName = ");
    stringBuilder.append(str1);
    stringBuilder.append(" which = ");
    stringBuilder.append(paramInt);
    Log.d(str2, stringBuilder.toString());
    return inputStream;
  }
  
  private static void setDefaultFileNameCache(int paramInt, String paramString) {
    // Byte code:
    //   0: ldc android/app/OplusWallpaperManagerHelper
    //   2: monitorenter
    //   3: getstatic android/app/OplusWallpaperManagerHelper.sDefaultFileNameCache : Ljava/util/Map;
    //   6: iload_0
    //   7: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   10: aload_1
    //   11: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   16: pop
    //   17: ldc android/app/OplusWallpaperManagerHelper
    //   19: monitorexit
    //   20: return
    //   21: astore_1
    //   22: ldc android/app/OplusWallpaperManagerHelper
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #214	-> 3
    //   #215	-> 17
    //   #213	-> 21
    // Exception table:
    //   from	to	target	type
    //   3	17	21	finally
  }
  
  private static String getDefaultWallpaperFileName(Context paramContext, int paramInt) {
    String str1 = sDefaultFileNameCache.get(Integer.valueOf(paramInt));
    String str2 = TAG;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("getDefaultWallpaperFileName cacheFileName = ");
    stringBuilder2.append(str1);
    stringBuilder2.append(" which = ");
    stringBuilder2.append(paramInt);
    Log.d(str2, stringBuilder2.toString());
    if (!TextUtils.isEmpty(str1))
      return str1; 
    str1 = getMyEngineeringFileName(paramContext, paramInt);
    String str3 = str1;
    if (str1 == null) {
      str3 = str1;
      if (OplusMultiUserManager.getInstance().isMultiSystemUserId(paramContext.getUserId()))
        str3 = getMultiSystemFileName(paramContext, paramInt); 
    } 
    str1 = str3;
    if (str3 == null)
      str1 = getModuleWallpaperFileName(paramContext, paramInt); 
    str3 = str1;
    if (str1 == null)
      str3 = getOperatorFileName(paramContext, paramInt); 
    str1 = str3;
    if (str3 == null)
      str1 = getColorFileName(paramContext, paramInt); 
    str3 = str1;
    if (str1 == null)
      str3 = getNoColorFileName(paramContext, paramInt); 
    str1 = TAG;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("getDefaultWallpaperFileName final fileName = ");
    stringBuilder1.append(str3);
    Log.d(str1, stringBuilder1.toString());
    if (!TextUtils.isEmpty(str3))
      setDefaultFileNameCache(paramInt, str3); 
    return str3;
  }
  
  private static String getMyEngineeringFileName(Context paramContext, int paramInt) {
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(getMyEngineeringDirectory().getAbsolutePath());
    stringBuilder2.append("/media/wallpaper");
    File file1 = new File(stringBuilder2.toString());
    File file2 = new File(file1, "oppo_default_wallpaper.png");
    String str = TAG;
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("getMyEngineeringFileName. oppoEngineerWallpaper = ");
    stringBuilder1.append(file2.getAbsolutePath());
    Log.d(str, stringBuilder1.toString());
    if (file2.exists()) {
      Log.d(TAG, "getMyEngineeringFileName oppoEngineerWallpaper dir exist");
      return file2.getAbsolutePath();
    } 
    Log.d(TAG, "getMyEngineeringFileName oppoEngineerWallpaper dir not exist");
    return null;
  }
  
  private static String getMultiSystemFileName(Context paramContext, int paramInt) {
    String str;
    paramContext = null;
    if (paramInt == 1) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
      stringBuilder1.append("oppo_default_multi_sys_wallpaper");
      stringBuilder1.append(".png");
      str = stringBuilder1.toString();
      File file = new File(str);
      String str1 = TAG;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("getMultiSystemFileName default fileName = ");
      stringBuilder2.append(str);
      Log.d(str1, stringBuilder2.toString());
      if (!file.exists()) {
        Log.d(TAG, "getMultiSystemFileName default fileName not exist");
        str = null;
      } 
    } 
    return str;
  }
  
  private static String getNoColorFileName(Context paramContext, int paramInt) {
    String str1;
    if (paramInt == 2) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
      stringBuilder1.append("oppo_default_wallpaper_lock");
      stringBuilder1.append(".png");
      str1 = stringBuilder1.toString();
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
      stringBuilder1.append("oppo_default_wallpaper");
      stringBuilder1.append(".png");
      str1 = stringBuilder1.toString();
    } 
    File file = new File(str1);
    String str2 = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getNoColorFileName default fileName = ");
    stringBuilder.append(str1);
    Log.d(str2, stringBuilder.toString());
    if (!file.exists()) {
      Log.d(TAG, "getNoColorFileName default fileName not exist");
      str1 = null;
    } 
    return str1;
  }
  
  private static String getColorFileName(Context paramContext, int paramInt) {
    StringBuilder stringBuilder1, stringBuilder3, stringBuilder2 = null;
    String str1 = null;
    paramContext = null;
    String str2 = SystemProperties.get("ro.hw.phone.color");
    if (!TextUtils.isEmpty(str2)) {
      String str4, arrayOfString[] = findPhoneColorDefaultWallpaper(str2);
      if (arrayOfString != null) {
        str1 = arrayOfString[0];
        if (!TextUtils.isEmpty(str1)) {
          str1 = arrayOfString[1];
          if (!TextUtils.isEmpty(str1)) {
            String str;
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
            stringBuilder2.append(arrayOfString[1]);
            stringBuilder2.append(".png");
            str1 = stringBuilder2.toString();
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
            stringBuilder2.append(arrayOfString[0]);
            stringBuilder2.append(".png");
            str4 = stringBuilder2.toString();
            File file2 = new File(str1);
            File file1 = new File(str4);
            if (file2.exists()) {
              setDefaultFileNameCache(2, str1);
              if (paramInt == 2)
                str = str1; 
            } else {
              String str5 = TAG;
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("getColorFileName lock not exist  lock  = ");
              stringBuilder.append(str1);
              Log.d(str5, stringBuilder.toString());
            } 
            if (file1.exists()) {
              setDefaultFileNameCache(1, str4);
              if (paramInt == 1)
                str = str4; 
            } else {
              String str5 = TAG;
              stringBuilder3 = new StringBuilder();
              stringBuilder3.append("getColorFileName system not exist system =  ");
              stringBuilder3.append(str4);
              Log.d(str5, stringBuilder3.toString());
            } 
            return str;
          } 
        } 
      } 
      Log.d(TAG, "getColorFileName phoneColorDefaultTheme is empty");
      String str3 = str4;
    } else {
      Log.d(TAG, "getColorFileName hwPhoneColor is empty");
      stringBuilder1 = stringBuilder3;
    } 
    return (String)stringBuilder1;
  }
  
  private static String getOperatorFileName(Context paramContext, int paramInt) {
    String str1;
    paramContext = null;
    String str2 = SystemProperties.get("ro.vendor.oplus.operator");
    if (!TextUtils.isEmpty(str2)) {
      str1 = str2.trim();
      str1 = str1.toLowerCase();
      if (paramInt == 2) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
        stringBuilder1.append("oppo_default_wallpaper_lock");
        stringBuilder1.append("_");
        stringBuilder1.append("operator");
        stringBuilder1.append("_");
        stringBuilder1.append(str1);
        stringBuilder1.append(".png");
        str1 = stringBuilder1.toString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(BASE_OPPO_PRODUCT_DEFAULT_DIR);
        stringBuilder1.append("oppo_default_wallpaper");
        stringBuilder1.append("_");
        stringBuilder1.append("operator");
        stringBuilder1.append("_");
        stringBuilder1.append(str1);
        stringBuilder1.append(".png");
        str1 = stringBuilder1.toString();
      } 
      File file = new File(str1);
      str2 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getOperatorFileName operator fileName = ");
      stringBuilder.append(str1);
      Log.d(str2, stringBuilder.toString());
      if (!file.exists()) {
        Log.d(TAG, "getOperatorFileName operator not exist ");
        str1 = null;
      } 
    } else {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getOperatorFileName valid operator  ");
      stringBuilder.append(str2);
      Log.d(str, stringBuilder.toString());
    } 
    return str1;
  }
  
  private static String getCustomFileName(Context paramContext, int paramInt) {
    File file2 = new File("/custom/media/wallpaper/default");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(OPPO_CUSTOM_WALLPAPER);
    stringBuilder.append("/media/wallpaper/default");
    File file1 = new File(stringBuilder.toString());
    if (paramInt == 2) {
      file2 = new File(file2, "oppo_default_wallpaper_lock");
      if (file2.exists()) {
        Log.d(TAG, "getCustomFileName customWallpaperLock exist");
        return file2.getAbsolutePath();
      } 
      Log.d(TAG, "getCustomFileName customWallpaperLock not exist");
      file1 = new File(file1, "oppo_default_wallpaper_lock");
      if (file1.exists()) {
        Log.d(TAG, "getCustomFileName oppoCustomWallpaperLock exist");
        return file1.getAbsolutePath();
      } 
      Log.d(TAG, "getCustomFileName oppoCustomWallpaperLock not exist");
    } else {
      file2 = new File(file2, "oppo_default_wallpaper");
      if (file2.exists()) {
        Log.d(TAG, "getCustomFileName customWallpaperSystem exist");
        return file2.getAbsolutePath();
      } 
      Log.d(TAG, "getCustomFileName customWallpaperSystem not exist");
      file1 = new File(file1, "oppo_default_wallpaper");
      if (file1.exists()) {
        Log.d(TAG, "getCustomFileName oppoCustomWallpaperSystem exist");
        return file1.getAbsolutePath();
      } 
      Log.d(TAG, "getCustomFileName oppoCustomWallpaperSystem not exist");
    } 
    return null;
  }
  
  private static String getCompanyFileName(Context paramContext, int paramInt) {
    StringBuilder stringBuilder1;
    if (!isCurrentCustomTheme())
      return null; 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append(getMyCompanyDirectory().getAbsolutePath());
    stringBuilder2.append("/media/wallpaper");
    File file = new File(stringBuilder2.toString());
    if (paramInt == 2) {
      File file1 = new File(file, "oppo_default_wallpaper_lock.png");
      String str = TAG;
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("isCurrentCustomTheme. oppoCustomWallpaperLock = ");
      stringBuilder1.append(file1.getAbsolutePath());
      Log.d(str, stringBuilder1.toString());
      if (file1.exists()) {
        Log.d(TAG, "getCompanyFileName oppoCompanyWallpaperLock exist");
        return file1.getAbsolutePath();
      } 
      Log.d(TAG, "getCompanyFileName oppoCompanyWallpaperLock not exist");
    } else {
      File file1 = new File((File)stringBuilder1, "oppo_default_wallpaper.png");
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isCurrentCustomTheme. oppoCustomWallpaperSystem = ");
      stringBuilder.append(file1.getAbsolutePath());
      Log.d(str, stringBuilder.toString());
      if (file1.exists()) {
        Log.d(TAG, "getCompanyFileName oppoCompanyWallpaperSystem exist");
        return file1.getAbsolutePath();
      } 
      Log.d(TAG, "getCompanyFileName oppoCompanyWallpaperSystem not exist");
    } 
    return null;
  }
  
  public static boolean isCurrentCustomTheme() {
    boolean bool;
    long l = SystemProperties.getLong("persist.sys.themeflag", 0L);
    String str = TAG;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("isCurrentCustomTheme. themeFlag = ");
    stringBuilder.append(l);
    Log.d(str, stringBuilder.toString());
    if ((0x100L & l) != 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static InputStream openDefaultWallpaperFromApkRes(Context paramContext, int paramInt) {
    if (paramInt == 2)
      return null; 
    return paramContext.getResources().openRawResource(getDefaultWallpaperResID(paramContext));
  }
  
  private static String[] findPhoneColorDefaultWallpaper(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   4: ifeq -> 18
    //   7: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   10: ldc 'findPhoneColorDefaultWallpaper: The phoneColorName is empty!'
    //   12: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   15: pop
    //   16: aconst_null
    //   17: areturn
    //   18: iconst_2
    //   19: anewarray java/lang/String
    //   22: astore_1
    //   23: aconst_null
    //   24: astore_2
    //   25: aconst_null
    //   26: astore_3
    //   27: aconst_null
    //   28: astore #4
    //   30: iconst_0
    //   31: ifne -> 214
    //   34: aload_3
    //   35: astore #5
    //   37: aload_2
    //   38: astore #6
    //   40: new java/io/File
    //   43: astore #7
    //   45: aload_3
    //   46: astore #5
    //   48: aload_2
    //   49: astore #6
    //   51: new java/lang/StringBuilder
    //   54: astore #4
    //   56: aload_3
    //   57: astore #5
    //   59: aload_2
    //   60: astore #6
    //   62: aload #4
    //   64: invokespecial <init> : ()V
    //   67: aload_3
    //   68: astore #5
    //   70: aload_2
    //   71: astore #6
    //   73: aload #4
    //   75: getstatic android/app/OplusWallpaperManagerHelper.BASE_OPPO_PRODUCT_DEFAULT_DIR : Ljava/lang/String;
    //   78: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   81: pop
    //   82: aload_3
    //   83: astore #5
    //   85: aload_2
    //   86: astore #6
    //   88: aload #4
    //   90: ldc 'phone_color_default_theme_maps'
    //   92: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload_3
    //   97: astore #5
    //   99: aload_2
    //   100: astore #6
    //   102: aload #4
    //   104: ldc '.xml'
    //   106: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   109: pop
    //   110: aload_3
    //   111: astore #5
    //   113: aload_2
    //   114: astore #6
    //   116: aload #7
    //   118: aload #4
    //   120: invokevirtual toString : ()Ljava/lang/String;
    //   123: invokespecial <init> : (Ljava/lang/String;)V
    //   126: aload #7
    //   128: astore #4
    //   130: aload_3
    //   131: astore #5
    //   133: aload_2
    //   134: astore #6
    //   136: aload #7
    //   138: invokevirtual exists : ()Z
    //   141: ifne -> 214
    //   144: aload_3
    //   145: astore #5
    //   147: aload_2
    //   148: astore #6
    //   150: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   153: ldc 'findPhoneColorDefaultWallpaper: The phone color map file is not exists!'
    //   155: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   158: pop
    //   159: iconst_0
    //   160: ifeq -> 212
    //   163: new java/lang/NullPointerException
    //   166: dup
    //   167: invokespecial <init> : ()V
    //   170: athrow
    //   171: astore #4
    //   173: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   176: astore_0
    //   177: new java/lang/StringBuilder
    //   180: dup
    //   181: invokespecial <init> : ()V
    //   184: astore #5
    //   186: aload #5
    //   188: ldc 'findPhoneColorDefaultWallpaper: Closing inputStream. e = '
    //   190: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   193: pop
    //   194: aload #5
    //   196: aload #4
    //   198: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   201: pop
    //   202: aload_0
    //   203: aload #5
    //   205: invokevirtual toString : ()Ljava/lang/String;
    //   208: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   211: pop
    //   212: aconst_null
    //   213: areturn
    //   214: aload_3
    //   215: astore #5
    //   217: aload_2
    //   218: astore #6
    //   220: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   223: astore #7
    //   225: aload_3
    //   226: astore #5
    //   228: aload_2
    //   229: astore #6
    //   231: new java/lang/StringBuilder
    //   234: astore #8
    //   236: aload_3
    //   237: astore #5
    //   239: aload_2
    //   240: astore #6
    //   242: aload #8
    //   244: invokespecial <init> : ()V
    //   247: aload_3
    //   248: astore #5
    //   250: aload_2
    //   251: astore #6
    //   253: aload #8
    //   255: ldc 'findPhoneColorDefaultWallpaper: phoneColorMapFile = '
    //   257: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   260: pop
    //   261: aload_3
    //   262: astore #5
    //   264: aload_2
    //   265: astore #6
    //   267: aload #8
    //   269: aload #4
    //   271: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: aload_3
    //   276: astore #5
    //   278: aload_2
    //   279: astore #6
    //   281: aload #7
    //   283: aload #8
    //   285: invokevirtual toString : ()Ljava/lang/String;
    //   288: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   291: pop
    //   292: aload_3
    //   293: astore #5
    //   295: aload_2
    //   296: astore #6
    //   298: new java/io/FileInputStream
    //   301: astore #7
    //   303: aload_3
    //   304: astore #5
    //   306: aload_2
    //   307: astore #6
    //   309: aload #7
    //   311: aload #4
    //   313: invokespecial <init> : (Ljava/io/File;)V
    //   316: aload #7
    //   318: astore #5
    //   320: aload #7
    //   322: astore #6
    //   324: invokestatic newPullParser : ()Lorg/xmlpull/v1/XmlPullParser;
    //   327: astore_2
    //   328: aload #7
    //   330: astore #5
    //   332: aload #7
    //   334: astore #6
    //   336: aload_2
    //   337: aload #7
    //   339: ldc 'UTF-8'
    //   341: invokeinterface setInput : (Ljava/io/InputStream;Ljava/lang/String;)V
    //   346: aconst_null
    //   347: astore #4
    //   349: aload #7
    //   351: astore #5
    //   353: aload #7
    //   355: astore #6
    //   357: aload_2
    //   358: invokeinterface getEventType : ()I
    //   363: istore #9
    //   365: iload #9
    //   367: iconst_1
    //   368: if_icmpeq -> 634
    //   371: iload #9
    //   373: iconst_2
    //   374: if_icmpeq -> 383
    //   377: aload #4
    //   379: astore_3
    //   380: goto -> 571
    //   383: aload #7
    //   385: astore #5
    //   387: aload #7
    //   389: astore #6
    //   391: aload_2
    //   392: invokeinterface getName : ()Ljava/lang/String;
    //   397: astore #8
    //   399: aload #7
    //   401: astore #5
    //   403: aload #7
    //   405: astore #6
    //   407: ldc 'PhoneColor'
    //   409: aload #8
    //   411: invokevirtual equals : (Ljava/lang/Object;)Z
    //   414: ifeq -> 481
    //   417: aload #7
    //   419: astore #5
    //   421: aload #7
    //   423: astore #6
    //   425: new java/lang/String
    //   428: astore #4
    //   430: aload #7
    //   432: astore #5
    //   434: aload #7
    //   436: astore #6
    //   438: aload #4
    //   440: aload_2
    //   441: iconst_0
    //   442: invokeinterface getAttributeValue : (I)Ljava/lang/String;
    //   447: invokespecial <init> : (Ljava/lang/String;)V
    //   450: aload #7
    //   452: astore #5
    //   454: aload #7
    //   456: astore #6
    //   458: aload_0
    //   459: aload #4
    //   461: invokevirtual equals : (Ljava/lang/Object;)Z
    //   464: ifeq -> 470
    //   467: goto -> 481
    //   470: aconst_null
    //   471: astore #4
    //   473: aload_1
    //   474: iconst_0
    //   475: aconst_null
    //   476: aastore
    //   477: aload_1
    //   478: iconst_1
    //   479: aconst_null
    //   480: aastore
    //   481: aload #4
    //   483: astore_3
    //   484: aload #4
    //   486: ifnull -> 571
    //   489: aload #7
    //   491: astore #5
    //   493: aload #7
    //   495: astore #6
    //   497: ldc 'DefaultTheme_system'
    //   499: aload #8
    //   501: invokevirtual equals : (Ljava/lang/Object;)Z
    //   504: ifeq -> 530
    //   507: aload #7
    //   509: astore #5
    //   511: aload #7
    //   513: astore #6
    //   515: aload_1
    //   516: iconst_0
    //   517: aload_2
    //   518: invokeinterface nextText : ()Ljava/lang/String;
    //   523: aastore
    //   524: aload #4
    //   526: astore_3
    //   527: goto -> 571
    //   530: aload #4
    //   532: astore_3
    //   533: aload #7
    //   535: astore #5
    //   537: aload #7
    //   539: astore #6
    //   541: ldc 'DefaultTheme_lock'
    //   543: aload #8
    //   545: invokevirtual equals : (Ljava/lang/Object;)Z
    //   548: ifeq -> 571
    //   551: aload #7
    //   553: astore #5
    //   555: aload #7
    //   557: astore #6
    //   559: aload_1
    //   560: iconst_1
    //   561: aload_2
    //   562: invokeinterface nextText : ()Ljava/lang/String;
    //   567: aastore
    //   568: aload #4
    //   570: astore_3
    //   571: aload #7
    //   573: astore #5
    //   575: aload #7
    //   577: astore #6
    //   579: aload_1
    //   580: iconst_0
    //   581: aaload
    //   582: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   585: ifne -> 612
    //   588: aload_1
    //   589: iconst_1
    //   590: aaload
    //   591: astore #4
    //   593: aload #7
    //   595: astore #5
    //   597: aload #7
    //   599: astore #6
    //   601: aload #4
    //   603: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   606: ifne -> 612
    //   609: goto -> 634
    //   612: aload #7
    //   614: astore #5
    //   616: aload #7
    //   618: astore #6
    //   620: aload_2
    //   621: invokeinterface next : ()I
    //   626: istore #9
    //   628: aload_3
    //   629: astore #4
    //   631: goto -> 365
    //   634: aload #7
    //   636: invokevirtual close : ()V
    //   639: goto -> 787
    //   642: astore #5
    //   644: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   647: astore_0
    //   648: new java/lang/StringBuilder
    //   651: dup
    //   652: invokespecial <init> : ()V
    //   655: astore #4
    //   657: aload #4
    //   659: ldc 'findPhoneColorDefaultWallpaper: Closing inputStream. e = '
    //   661: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   664: pop
    //   665: aload #4
    //   667: aload #5
    //   669: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   672: pop
    //   673: aload_0
    //   674: aload #4
    //   676: invokevirtual toString : ()Ljava/lang/String;
    //   679: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   682: pop
    //   683: goto -> 639
    //   686: astore_0
    //   687: goto -> 837
    //   690: astore_0
    //   691: aload #6
    //   693: astore #5
    //   695: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   698: astore #7
    //   700: aload #6
    //   702: astore #5
    //   704: new java/lang/StringBuilder
    //   707: astore #4
    //   709: aload #6
    //   711: astore #5
    //   713: aload #4
    //   715: invokespecial <init> : ()V
    //   718: aload #6
    //   720: astore #5
    //   722: aload #4
    //   724: ldc 'findPhoneColorDefaultWallpaper: e = '
    //   726: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   729: pop
    //   730: aload #6
    //   732: astore #5
    //   734: aload #4
    //   736: aload_0
    //   737: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   740: pop
    //   741: aload #6
    //   743: astore #5
    //   745: aload #7
    //   747: aload #4
    //   749: invokevirtual toString : ()Ljava/lang/String;
    //   752: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   755: pop
    //   756: aload #6
    //   758: ifnull -> 787
    //   761: aload #6
    //   763: invokevirtual close : ()V
    //   766: goto -> 639
    //   769: astore #5
    //   771: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   774: astore_0
    //   775: new java/lang/StringBuilder
    //   778: dup
    //   779: invokespecial <init> : ()V
    //   782: astore #4
    //   784: goto -> 657
    //   787: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   790: astore_0
    //   791: new java/lang/StringBuilder
    //   794: dup
    //   795: invokespecial <init> : ()V
    //   798: astore #4
    //   800: aload #4
    //   802: ldc 'findPhoneColorDefaultWallpaper: defaultTheme = '
    //   804: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   807: pop
    //   808: aload #4
    //   810: aload_1
    //   811: iconst_0
    //   812: aaload
    //   813: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   816: pop
    //   817: aload #4
    //   819: ldc ' defaultTheme[]'
    //   821: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   824: pop
    //   825: aload_0
    //   826: aload #4
    //   828: invokevirtual toString : ()Ljava/lang/String;
    //   831: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   834: pop
    //   835: aload_1
    //   836: areturn
    //   837: aload #5
    //   839: ifnull -> 893
    //   842: aload #5
    //   844: invokevirtual close : ()V
    //   847: goto -> 893
    //   850: astore #4
    //   852: getstatic android/app/OplusWallpaperManagerHelper.TAG : Ljava/lang/String;
    //   855: astore #5
    //   857: new java/lang/StringBuilder
    //   860: dup
    //   861: invokespecial <init> : ()V
    //   864: astore #6
    //   866: aload #6
    //   868: ldc 'findPhoneColorDefaultWallpaper: Closing inputStream. e = '
    //   870: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   873: pop
    //   874: aload #6
    //   876: aload #4
    //   878: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   881: pop
    //   882: aload #5
    //   884: aload #6
    //   886: invokevirtual toString : ()Ljava/lang/String;
    //   889: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   892: pop
    //   893: aload_0
    //   894: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #458	-> 0
    //   #459	-> 7
    //   #460	-> 16
    //   #463	-> 18
    //   #464	-> 23
    //   #466	-> 27
    //   #467	-> 30
    //   #468	-> 34
    //   #469	-> 126
    //   #470	-> 144
    //   #471	-> 159
    //   #518	-> 159
    //   #520	-> 163
    //   #523	-> 171
    //   #521	-> 171
    //   #522	-> 173
    //   #471	-> 212
    //   #474	-> 214
    //   #476	-> 292
    //   #477	-> 316
    //   #478	-> 328
    //   #480	-> 346
    //   #481	-> 349
    //   #482	-> 365
    //   #483	-> 371
    //   #487	-> 383
    //   #488	-> 399
    //   #489	-> 417
    //   #490	-> 450
    //   #491	-> 467
    //   #493	-> 470
    //   #494	-> 473
    //   #495	-> 477
    //   #498	-> 481
    //   #499	-> 489
    //   #500	-> 507
    //   #501	-> 530
    //   #502	-> 551
    //   #509	-> 571
    //   #510	-> 593
    //   #511	-> 609
    //   #513	-> 612
    //   #518	-> 634
    //   #520	-> 634
    //   #523	-> 639
    //   #521	-> 642
    //   #522	-> 644
    //   #518	-> 686
    //   #515	-> 690
    //   #516	-> 691
    //   #518	-> 756
    //   #520	-> 761
    //   #521	-> 769
    //   #522	-> 771
    //   #527	-> 787
    //   #528	-> 835
    //   #518	-> 837
    //   #520	-> 842
    //   #523	-> 847
    //   #521	-> 850
    //   #522	-> 852
    //   #525	-> 893
    // Exception table:
    //   from	to	target	type
    //   40	45	690	java/lang/Exception
    //   40	45	686	finally
    //   51	56	690	java/lang/Exception
    //   51	56	686	finally
    //   62	67	690	java/lang/Exception
    //   62	67	686	finally
    //   73	82	690	java/lang/Exception
    //   73	82	686	finally
    //   88	96	690	java/lang/Exception
    //   88	96	686	finally
    //   102	110	690	java/lang/Exception
    //   102	110	686	finally
    //   116	126	690	java/lang/Exception
    //   116	126	686	finally
    //   136	144	690	java/lang/Exception
    //   136	144	686	finally
    //   150	159	690	java/lang/Exception
    //   150	159	686	finally
    //   163	171	171	java/lang/Exception
    //   220	225	690	java/lang/Exception
    //   220	225	686	finally
    //   231	236	690	java/lang/Exception
    //   231	236	686	finally
    //   242	247	690	java/lang/Exception
    //   242	247	686	finally
    //   253	261	690	java/lang/Exception
    //   253	261	686	finally
    //   267	275	690	java/lang/Exception
    //   267	275	686	finally
    //   281	292	690	java/lang/Exception
    //   281	292	686	finally
    //   298	303	690	java/lang/Exception
    //   298	303	686	finally
    //   309	316	690	java/lang/Exception
    //   309	316	686	finally
    //   324	328	690	java/lang/Exception
    //   324	328	686	finally
    //   336	346	690	java/lang/Exception
    //   336	346	686	finally
    //   357	365	690	java/lang/Exception
    //   357	365	686	finally
    //   391	399	690	java/lang/Exception
    //   391	399	686	finally
    //   407	417	690	java/lang/Exception
    //   407	417	686	finally
    //   425	430	690	java/lang/Exception
    //   425	430	686	finally
    //   438	450	690	java/lang/Exception
    //   438	450	686	finally
    //   458	467	690	java/lang/Exception
    //   458	467	686	finally
    //   497	507	690	java/lang/Exception
    //   497	507	686	finally
    //   515	524	690	java/lang/Exception
    //   515	524	686	finally
    //   541	551	690	java/lang/Exception
    //   541	551	686	finally
    //   559	568	690	java/lang/Exception
    //   559	568	686	finally
    //   579	588	690	java/lang/Exception
    //   579	588	686	finally
    //   601	609	690	java/lang/Exception
    //   601	609	686	finally
    //   620	628	690	java/lang/Exception
    //   620	628	686	finally
    //   634	639	642	java/lang/Exception
    //   695	700	686	finally
    //   704	709	686	finally
    //   713	718	686	finally
    //   722	730	686	finally
    //   734	741	686	finally
    //   745	756	686	finally
    //   761	766	769	java/lang/Exception
    //   842	847	850	java/lang/Exception
  }
  
  public static File getDefaultWallpaperPath() {
    File file1 = getOplusVersionDirectory();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(file1.getAbsolutePath());
    stringBuilder.append("/decouping_wallpaper/");
    File file2 = new File(stringBuilder.toString());
    if (!file2.exists())
      file1 = getOplusProductDirectory(); 
    return file1;
  }
  
  public static File getOplusProductDirectory() {
    try {
      Method method = Environment.class.getMethod("getOplusProductDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_product");
  }
  
  public static File getOplusCustomDirectory() {
    try {
      Method method = Environment.class.getMethod("getOplusCustomDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_custom");
  }
  
  public static File getMyCompanyDirectory() {
    try {
      Method method = Environment.class.getMethod("getMyCompanyDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_custom");
  }
  
  public static File getMyEngineeringDirectory() {
    try {
      Method method = Environment.class.getMethod("getMyEngineeringDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null) {
        Log.d(TAG, "getMyEngineeringDirectory dir is not null.");
        return (File)object;
      } 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_engineering");
  }
  
  public static File getOplusVersionDirectory() {
    try {
      Method method = Environment.class.getMethod("getOplusVersionDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_version");
  }
  
  public static File getMyCarrierDirectory() {
    try {
      Method method = Environment.class.getMethod("getMyOperatorDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_custom");
  }
  
  public static File getMyRegionDirectory() {
    try {
      Method method = Environment.class.getMethod("getMyCountryDirectory", new Class[0]);
      method.setAccessible(true);
      Object object = method.invoke(null, new Object[0]);
      if (object != null)
        return (File)object; 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return new File("/oppo_custom");
  }
  
  public static String getModuleWallpaperFileName(Context paramContext, int paramInt) {
    String arrayOfString[] = new String[3], str = getMyCompanyDirectory().getAbsolutePath();
    byte b = 0;
    arrayOfString[0] = str;
    arrayOfString[1] = getMyCarrierDirectory().getAbsolutePath();
    arrayOfString[2] = getMyRegionDirectory().getAbsolutePath();
    for (int i = arrayOfString.length; b < i; ) {
      str = arrayOfString[b];
      String str1 = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getModuleWallpaperFileName:current module: ");
      stringBuilder.append(str);
      Log.d(str1, stringBuilder.toString());
      stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append("/decouping_wallpaper/default/");
      File file = new File(stringBuilder.toString());
      if (paramInt == 2) {
        file = new File(file, "default_wallpaper_lock.png");
        if (file.exists()) {
          Log.d(TAG, "getModuleWallpaperFileName WallpaperLock exist in this module");
          return file.getAbsolutePath();
        } 
        Log.d(TAG, "getModuleWallpaperFileName WallpaperLock not exist in this module");
      } else {
        file = new File(file, "default_wallpaper.png");
        if (file.exists()) {
          Log.d(TAG, "getModuleWallpaperFileName WallpaperSystem exist in this module");
          return file.getAbsolutePath();
        } 
        Log.d(TAG, "getModuleWallpaperFileName WallpaperSystem not exist in this module");
      } 
      b++;
    } 
    return null;
  }
  
  public static boolean checkCustomizeWallpaperDir() {
    String[] arrayOfString = new String[3];
    arrayOfString[0] = getMyCompanyDirectory().getAbsolutePath();
    arrayOfString[1] = getMyCarrierDirectory().getAbsolutePath();
    arrayOfString[2] = getMyRegionDirectory().getAbsolutePath();
    int i;
    byte b;
    for (i = arrayOfString.length, b = 0; b < i; ) {
      String str1 = arrayOfString[b];
      String str2 = TAG;
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("getModuleWallpaperFileName:current module: ");
      stringBuilder2.append(str1);
      Log.d(str2, stringBuilder2.toString());
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str1);
      stringBuilder1.append("/decouping_wallpaper/default/");
      File file = new File(stringBuilder1.toString());
      if (file.exists())
        return true; 
      b++;
    } 
    return false;
  }
}
