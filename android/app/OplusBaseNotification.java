package android.app;

import android.content.Context;
import android.text.TextUtils;
import com.oplus.multiapp.OplusMultiAppManager;

public class OplusBaseNotification {
  private static final String TAG = "OplusBaseNotification";
  
  public static class Builder {
    private Context mContext;
    
    private Notification mN;
    
    public Builder(Context param1Context, Notification param1Notification) {
      this.mContext = param1Context;
      this.mN = param1Notification;
    }
    
    protected String oplusLoadMultiHeaderAppName() {
      String str = null;
      if (this.mContext.getUserId() == 999) {
        str = this.mContext.getPackageName();
        str = OplusMultiAppManager.getInstance().getMultiAppAlias(str);
      } 
      if (TextUtils.isEmpty(str))
        return null; 
      return String.valueOf(str);
    }
  }
}
