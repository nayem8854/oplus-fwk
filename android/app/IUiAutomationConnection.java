package android.app;

import android.accessibilityservice.IAccessibilityServiceClient;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.view.InputEvent;
import android.view.WindowAnimationFrameStats;
import android.view.WindowContentFrameStats;

public interface IUiAutomationConnection extends IInterface {
  void adoptShellPermissionIdentity(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  void clearWindowAnimationFrameStats() throws RemoteException;
  
  boolean clearWindowContentFrameStats(int paramInt) throws RemoteException;
  
  void connect(IAccessibilityServiceClient paramIAccessibilityServiceClient, int paramInt) throws RemoteException;
  
  void disconnect() throws RemoteException;
  
  void dropShellPermissionIdentity() throws RemoteException;
  
  void executeShellCommand(String paramString, ParcelFileDescriptor paramParcelFileDescriptor1, ParcelFileDescriptor paramParcelFileDescriptor2) throws RemoteException;
  
  WindowAnimationFrameStats getWindowAnimationFrameStats() throws RemoteException;
  
  WindowContentFrameStats getWindowContentFrameStats(int paramInt) throws RemoteException;
  
  void grantRuntimePermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean injectInputEvent(InputEvent paramInputEvent, boolean paramBoolean) throws RemoteException;
  
  void revokeRuntimePermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean setRotation(int paramInt) throws RemoteException;
  
  void shutdown() throws RemoteException;
  
  void syncInputTransactions() throws RemoteException;
  
  Bitmap takeScreenshot(Rect paramRect, int paramInt) throws RemoteException;
  
  class Default implements IUiAutomationConnection {
    public void connect(IAccessibilityServiceClient param1IAccessibilityServiceClient, int param1Int) throws RemoteException {}
    
    public void disconnect() throws RemoteException {}
    
    public boolean injectInputEvent(InputEvent param1InputEvent, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void syncInputTransactions() throws RemoteException {}
    
    public boolean setRotation(int param1Int) throws RemoteException {
      return false;
    }
    
    public Bitmap takeScreenshot(Rect param1Rect, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean clearWindowContentFrameStats(int param1Int) throws RemoteException {
      return false;
    }
    
    public WindowContentFrameStats getWindowContentFrameStats(int param1Int) throws RemoteException {
      return null;
    }
    
    public void clearWindowAnimationFrameStats() throws RemoteException {}
    
    public WindowAnimationFrameStats getWindowAnimationFrameStats() throws RemoteException {
      return null;
    }
    
    public void executeShellCommand(String param1String, ParcelFileDescriptor param1ParcelFileDescriptor1, ParcelFileDescriptor param1ParcelFileDescriptor2) throws RemoteException {}
    
    public void grantRuntimePermission(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void revokeRuntimePermission(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void adoptShellPermissionIdentity(int param1Int, String[] param1ArrayOfString) throws RemoteException {}
    
    public void dropShellPermissionIdentity() throws RemoteException {}
    
    public void shutdown() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IUiAutomationConnection {
    private static final String DESCRIPTOR = "android.app.IUiAutomationConnection";
    
    static final int TRANSACTION_adoptShellPermissionIdentity = 14;
    
    static final int TRANSACTION_clearWindowAnimationFrameStats = 9;
    
    static final int TRANSACTION_clearWindowContentFrameStats = 7;
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_disconnect = 2;
    
    static final int TRANSACTION_dropShellPermissionIdentity = 15;
    
    static final int TRANSACTION_executeShellCommand = 11;
    
    static final int TRANSACTION_getWindowAnimationFrameStats = 10;
    
    static final int TRANSACTION_getWindowContentFrameStats = 8;
    
    static final int TRANSACTION_grantRuntimePermission = 12;
    
    static final int TRANSACTION_injectInputEvent = 3;
    
    static final int TRANSACTION_revokeRuntimePermission = 13;
    
    static final int TRANSACTION_setRotation = 5;
    
    static final int TRANSACTION_shutdown = 16;
    
    static final int TRANSACTION_syncInputTransactions = 4;
    
    static final int TRANSACTION_takeScreenshot = 6;
    
    public Stub() {
      attachInterface(this, "android.app.IUiAutomationConnection");
    }
    
    public static IUiAutomationConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IUiAutomationConnection");
      if (iInterface != null && iInterface instanceof IUiAutomationConnection)
        return (IUiAutomationConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "shutdown";
        case 15:
          return "dropShellPermissionIdentity";
        case 14:
          return "adoptShellPermissionIdentity";
        case 13:
          return "revokeRuntimePermission";
        case 12:
          return "grantRuntimePermission";
        case 11:
          return "executeShellCommand";
        case 10:
          return "getWindowAnimationFrameStats";
        case 9:
          return "clearWindowAnimationFrameStats";
        case 8:
          return "getWindowContentFrameStats";
        case 7:
          return "clearWindowContentFrameStats";
        case 6:
          return "takeScreenshot";
        case 5:
          return "setRotation";
        case 4:
          return "syncInputTransactions";
        case 3:
          return "injectInputEvent";
        case 2:
          return "disconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        String[] arrayOfString;
        WindowAnimationFrameStats windowAnimationFrameStats;
        WindowContentFrameStats windowContentFrameStats;
        Bitmap bitmap;
        String str1, str2;
        boolean bool = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("android.app.IUiAutomationConnection");
            shutdown();
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.app.IUiAutomationConnection");
            dropShellPermissionIdentity();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.app.IUiAutomationConnection");
            param1Int1 = param1Parcel1.readInt();
            arrayOfString = param1Parcel1.createStringArray();
            adoptShellPermissionIdentity(param1Int1, arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            arrayOfString.enforceInterface("android.app.IUiAutomationConnection");
            str1 = arrayOfString.readString();
            str2 = arrayOfString.readString();
            param1Int1 = arrayOfString.readInt();
            revokeRuntimePermission(str1, str2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            arrayOfString.enforceInterface("android.app.IUiAutomationConnection");
            str2 = arrayOfString.readString();
            str1 = arrayOfString.readString();
            param1Int1 = arrayOfString.readInt();
            grantRuntimePermission(str2, str1, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            arrayOfString.enforceInterface("android.app.IUiAutomationConnection");
            str1 = arrayOfString.readString();
            if (arrayOfString.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              str2 = null;
            } 
            if (arrayOfString.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)arrayOfString);
            } else {
              arrayOfString = null;
            } 
            executeShellCommand(str1, (ParcelFileDescriptor)str2, (ParcelFileDescriptor)arrayOfString);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            arrayOfString.enforceInterface("android.app.IUiAutomationConnection");
            windowAnimationFrameStats = getWindowAnimationFrameStats();
            param1Parcel2.writeNoException();
            if (windowAnimationFrameStats != null) {
              param1Parcel2.writeInt(1);
              windowAnimationFrameStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            windowAnimationFrameStats.enforceInterface("android.app.IUiAutomationConnection");
            clearWindowAnimationFrameStats();
            param1Parcel2.writeNoException();
            return true;
          case 8:
            windowAnimationFrameStats.enforceInterface("android.app.IUiAutomationConnection");
            param1Int1 = windowAnimationFrameStats.readInt();
            windowContentFrameStats = getWindowContentFrameStats(param1Int1);
            param1Parcel2.writeNoException();
            if (windowContentFrameStats != null) {
              param1Parcel2.writeInt(1);
              windowContentFrameStats.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            windowContentFrameStats.enforceInterface("android.app.IUiAutomationConnection");
            param1Int1 = windowContentFrameStats.readInt();
            bool2 = clearWindowContentFrameStats(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 6:
            windowContentFrameStats.enforceInterface("android.app.IUiAutomationConnection");
            if (windowContentFrameStats.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)windowContentFrameStats);
            } else {
              str2 = null;
            } 
            j = windowContentFrameStats.readInt();
            bitmap = takeScreenshot((Rect)str2, j);
            param1Parcel2.writeNoException();
            if (bitmap != null) {
              param1Parcel2.writeInt(1);
              bitmap.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            bitmap.enforceInterface("android.app.IUiAutomationConnection");
            j = bitmap.readInt();
            bool1 = setRotation(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            bitmap.enforceInterface("android.app.IUiAutomationConnection");
            syncInputTransactions();
            param1Parcel2.writeNoException();
            return true;
          case 3:
            bitmap.enforceInterface("android.app.IUiAutomationConnection");
            if (bitmap.readInt() != 0) {
              InputEvent inputEvent = (InputEvent)InputEvent.CREATOR.createFromParcel((Parcel)bitmap);
            } else {
              str2 = null;
            } 
            if (bitmap.readInt() != 0)
              bool = true; 
            bool1 = injectInputEvent((InputEvent)str2, bool);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            bitmap.enforceInterface("android.app.IUiAutomationConnection");
            disconnect();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        bitmap.enforceInterface("android.app.IUiAutomationConnection");
        IAccessibilityServiceClient iAccessibilityServiceClient = IAccessibilityServiceClient.Stub.asInterface(bitmap.readStrongBinder());
        int i = bitmap.readInt();
        connect(iAccessibilityServiceClient, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.app.IUiAutomationConnection");
      return true;
    }
    
    class Proxy implements IUiAutomationConnection {
      public static IUiAutomationConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IUiAutomationConnection.Stub this$0) {
        this.mRemote = (IBinder)this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IUiAutomationConnection";
      }
      
      public void connect(IAccessibilityServiceClient param2IAccessibilityServiceClient, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          if (param2IAccessibilityServiceClient != null) {
            iBinder = param2IAccessibilityServiceClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().connect(param2IAccessibilityServiceClient, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disconnect() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().disconnect();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean injectInputEvent(InputEvent param2InputEvent, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = true;
          if (param2InputEvent != null) {
            parcel1.writeInt(1);
            param2InputEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool1 && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            param2Boolean = IUiAutomationConnection.Stub.getDefaultImpl().injectInputEvent(param2InputEvent, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void syncInputTransactions() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().syncInputTransactions();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRotation(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            bool1 = IUiAutomationConnection.Stub.getDefaultImpl().setRotation(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bitmap takeScreenshot(Rect param2Rect, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null)
            return IUiAutomationConnection.Stub.getDefaultImpl().takeScreenshot(param2Rect, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(parcel2);
          } else {
            param2Rect = null;
          } 
          return (Bitmap)param2Rect;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearWindowContentFrameStats(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(7, parcel1, parcel2, 0);
          if (!bool2 && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            bool1 = IUiAutomationConnection.Stub.getDefaultImpl().clearWindowContentFrameStats(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WindowContentFrameStats getWindowContentFrameStats(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          WindowContentFrameStats windowContentFrameStats;
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            windowContentFrameStats = IUiAutomationConnection.Stub.getDefaultImpl().getWindowContentFrameStats(param2Int);
            return windowContentFrameStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            windowContentFrameStats = (WindowContentFrameStats)WindowContentFrameStats.CREATOR.createFromParcel(parcel2);
          } else {
            windowContentFrameStats = null;
          } 
          return windowContentFrameStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearWindowAnimationFrameStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().clearWindowAnimationFrameStats();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public WindowAnimationFrameStats getWindowAnimationFrameStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          WindowAnimationFrameStats windowAnimationFrameStats;
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            windowAnimationFrameStats = IUiAutomationConnection.Stub.getDefaultImpl().getWindowAnimationFrameStats();
            return windowAnimationFrameStats;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            windowAnimationFrameStats = (WindowAnimationFrameStats)WindowAnimationFrameStats.CREATOR.createFromParcel(parcel2);
          } else {
            windowAnimationFrameStats = null;
          } 
          return windowAnimationFrameStats;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void executeShellCommand(String param2String, ParcelFileDescriptor param2ParcelFileDescriptor1, ParcelFileDescriptor param2ParcelFileDescriptor2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeString(param2String);
          if (param2ParcelFileDescriptor1 != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ParcelFileDescriptor2 != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().executeShellCommand(param2String, param2ParcelFileDescriptor1, param2ParcelFileDescriptor2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantRuntimePermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().grantRuntimePermission(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeRuntimePermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().revokeRuntimePermission(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void adoptShellPermissionIdentity(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().adoptShellPermissionIdentity(param2Int, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dropShellPermissionIdentity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().dropShellPermissionIdentity();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shutdown() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IUiAutomationConnection");
          boolean bool = this.mRemote.transact(16, parcel, null, 1);
          if (!bool && IUiAutomationConnection.Stub.getDefaultImpl() != null) {
            IUiAutomationConnection.Stub.getDefaultImpl().shutdown();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IUiAutomationConnection param1IUiAutomationConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IUiAutomationConnection != null) {
          Proxy.sDefaultImpl = param1IUiAutomationConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IUiAutomationConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
