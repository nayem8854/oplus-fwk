package android.app;

import android.annotation.SystemApi;
import android.app.admin.DevicePolicyManager;
import android.app.admin.PasswordMetrics;
import android.app.trust.ITrustManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.provider.Settings;
import android.service.persistentdata.IPersistentDataBlockService;
import android.util.Log;
import android.view.IOnKeyguardExitResult;
import android.view.IWindowManager;
import android.view.WindowManagerGlobal;
import com.android.internal.policy.IKeyguardDismissCallback;
import com.android.internal.widget.LockPatternUtils;
import com.android.internal.widget.LockscreenCredential;
import com.android.internal.widget.PasswordValidationError;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class KeyguardManager {
  public static final String ACTION_CONFIRM_DEVICE_CREDENTIAL = "android.app.action.CONFIRM_DEVICE_CREDENTIAL";
  
  public static final String ACTION_CONFIRM_DEVICE_CREDENTIAL_WITH_USER = "android.app.action.CONFIRM_DEVICE_CREDENTIAL_WITH_USER";
  
  public static final String ACTION_CONFIRM_FRP_CREDENTIAL = "android.app.action.CONFIRM_FRP_CREDENTIAL";
  
  public static final String EXTRA_ALTERNATE_BUTTON_LABEL = "android.app.extra.ALTERNATE_BUTTON_LABEL";
  
  public static final String EXTRA_DESCRIPTION = "android.app.extra.DESCRIPTION";
  
  public static final String EXTRA_DISALLOW_BIOMETRICS_IF_POLICY_EXISTS = "check_dpm";
  
  public static final String EXTRA_TITLE = "android.app.extra.TITLE";
  
  public static final int RESULT_ALTERNATE = 1;
  
  private static final String TAG = "KeyguardManager";
  
  private final IActivityManager mAm;
  
  private final Context mContext;
  
  private final INotificationManager mNotificationManager;
  
  private final ITrustManager mTrustManager;
  
  private final IWindowManager mWM;
  
  @Deprecated
  public Intent createConfirmDeviceCredentialIntent(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    if (!isDeviceSecure())
      return null; 
    Intent intent = new Intent("android.app.action.CONFIRM_DEVICE_CREDENTIAL");
    intent.putExtra("android.app.extra.TITLE", paramCharSequence1);
    intent.putExtra("android.app.extra.DESCRIPTION", paramCharSequence2);
    intent.setPackage(getSettingsPackageForIntent(intent));
    return intent;
  }
  
  public Intent createConfirmDeviceCredentialIntent(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt) {
    if (!isDeviceSecure(paramInt))
      return null; 
    Intent intent = new Intent("android.app.action.CONFIRM_DEVICE_CREDENTIAL_WITH_USER");
    intent.putExtra("android.app.extra.TITLE", paramCharSequence1);
    intent.putExtra("android.app.extra.DESCRIPTION", paramCharSequence2);
    intent.putExtra("android.intent.extra.USER_ID", paramInt);
    intent.setPackage(getSettingsPackageForIntent(intent));
    return intent;
  }
  
  public Intent createConfirmDeviceCredentialIntent(CharSequence paramCharSequence1, CharSequence paramCharSequence2, int paramInt, boolean paramBoolean) {
    Intent intent = createConfirmDeviceCredentialIntent(paramCharSequence1, paramCharSequence2, paramInt);
    intent.putExtra("check_dpm", paramBoolean);
    return intent;
  }
  
  @SystemApi
  public Intent createConfirmFactoryResetCredentialIntent(CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3) {
    if (LockPatternUtils.frpCredentialEnabled(this.mContext)) {
      if (Settings.Global.getInt(this.mContext.getContentResolver(), "device_provisioned", 0) == 0)
        try {
          IBinder iBinder = ServiceManager.getService("persistent_data_block");
          IPersistentDataBlockService iPersistentDataBlockService = IPersistentDataBlockService.Stub.asInterface(iBinder);
          if (iPersistentDataBlockService != null) {
            if (!iPersistentDataBlockService.hasFrpCredentialHandle()) {
              Log.i("KeyguardManager", "The persistent data block does not have a factory reset credential.");
              return null;
            } 
            Intent intent = new Intent("android.app.action.CONFIRM_FRP_CREDENTIAL");
            intent.putExtra("android.app.extra.TITLE", paramCharSequence1);
            intent.putExtra("android.app.extra.DESCRIPTION", paramCharSequence2);
            intent.putExtra("android.app.extra.ALTERNATE_BUTTON_LABEL", paramCharSequence3);
            intent.setPackage(getSettingsPackageForIntent(intent));
            return intent;
          } 
          Log.e("KeyguardManager", "No persistent data block service");
          UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
          this("not supported on this device");
          throw unsupportedOperationException;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      Log.e("KeyguardManager", "Factory reset credential cannot be verified after provisioning.");
      throw new IllegalStateException("must not be provisioned yet");
    } 
    Log.w("KeyguardManager", "Factory reset credentials not supported.");
    throw new UnsupportedOperationException("not supported on this device");
  }
  
  @SystemApi
  public void setPrivateNotificationsAllowed(boolean paramBoolean) {
    try {
      this.mNotificationManager.setPrivateNotificationsAllowed(paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean getPrivateNotificationsAllowed() {
    try {
      return this.mNotificationManager.getPrivateNotificationsAllowed();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private String getSettingsPackageForIntent(Intent paramIntent) {
    PackageManager packageManager = this.mContext.getPackageManager();
    List<ResolveInfo> list = packageManager.queryIntentActivities(paramIntent, 1048576);
    if (list.size() < 0)
      return ((ResolveInfo)list.get(0)).activityInfo.packageName; 
    return "com.android.settings";
  }
  
  @Deprecated
  public static interface OnKeyguardExitResult {
    void onKeyguardExitResult(boolean param1Boolean);
  }
  
  static @interface LockTypes {
    public static final int PASSWORD = 0;
    
    public static final int PATTERN = 2;
    
    public static final int PIN = 1;
  }
  
  @Deprecated
  public class KeyguardLock {
    private final String mTag;
    
    private final IBinder mToken = (IBinder)new Binder();
    
    final KeyguardManager this$0;
    
    KeyguardLock(String param1String) {
      this.mTag = param1String;
    }
    
    public void disableKeyguard() {
      try {
        KeyguardManager.this.mWM.disableKeyguard(this.mToken, this.mTag, KeyguardManager.this.mContext.getUserId());
      } catch (RemoteException remoteException) {}
    }
    
    public void reenableKeyguard() {
      try {
        KeyguardManager.this.mWM.reenableKeyguard(this.mToken, KeyguardManager.this.mContext.getUserId());
      } catch (RemoteException remoteException) {}
    }
  }
  
  public static abstract class KeyguardDismissCallback {
    public void onDismissError() {}
    
    public void onDismissSucceeded() {}
    
    public void onDismissCancelled() {}
  }
  
  KeyguardManager(Context paramContext) throws ServiceManager.ServiceNotFoundException {
    this.mContext = paramContext;
    this.mWM = WindowManagerGlobal.getWindowManagerService();
    this.mAm = ActivityManager.getService();
    IBinder iBinder = ServiceManager.getServiceOrThrow("trust");
    this.mTrustManager = ITrustManager.Stub.asInterface(iBinder);
    iBinder = ServiceManager.getServiceOrThrow("notification");
    this.mNotificationManager = INotificationManager.Stub.asInterface(iBinder);
  }
  
  @Deprecated
  public KeyguardLock newKeyguardLock(String paramString) {
    return new KeyguardLock(paramString);
  }
  
  public boolean isKeyguardLocked() {
    try {
      return this.mWM.isKeyguardLocked();
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean isKeyguardSecure() {
    try {
      return this.mWM.isKeyguardSecure(this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean inKeyguardRestrictedInputMode() {
    return isKeyguardLocked();
  }
  
  public boolean isDeviceLocked() {
    return isDeviceLocked(this.mContext.getUserId());
  }
  
  public boolean isDeviceLocked(int paramInt) {
    try {
      return this.mTrustManager.isDeviceLocked(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean isDeviceSecure() {
    return isDeviceSecure(this.mContext.getUserId());
  }
  
  public boolean isDeviceSecure(int paramInt) {
    try {
      return this.mTrustManager.isDeviceSecure(paramInt);
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void requestDismissKeyguard(Activity paramActivity, KeyguardDismissCallback paramKeyguardDismissCallback) {
    requestDismissKeyguard(paramActivity, null, paramKeyguardDismissCallback);
  }
  
  @SystemApi
  public void requestDismissKeyguard(Activity paramActivity, CharSequence paramCharSequence, KeyguardDismissCallback paramKeyguardDismissCallback) {
    try {
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      IBinder iBinder = paramActivity.getActivityToken();
      Object object = new Object();
      super(this, paramKeyguardDismissCallback, paramActivity);
      iActivityTaskManager.dismissKeyguard(iBinder, (IKeyguardDismissCallback)object, paramCharSequence);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void exitKeyguardSecurely(OnKeyguardExitResult paramOnKeyguardExitResult) {
    try {
      IWindowManager iWindowManager = this.mWM;
      Object object = new Object();
      super(this, paramOnKeyguardExitResult);
      iWindowManager.exitKeyguardSecurely((IOnKeyguardExitResult)object);
    } catch (RemoteException remoteException) {}
  }
  
  private boolean checkInitialLockMethodUsage() {
    if (this.mContext.checkCallingOrSelfPermission("android.permission.SET_INITIAL_LOCK") == 0) {
      if (!this.mContext.getPackageManager().hasSystemFeature("android.hardware.type.automotive"))
        return false; 
      return true;
    } 
    throw new SecurityException("Requires SET_INITIAL_LOCK permission.");
  }
  
  @SystemApi
  public boolean isValidLockPasswordComplexity(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    boolean bool = checkInitialLockMethodUsage();
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramInt2 = PasswordMetrics.sanitizeComplexityLevel(paramInt2);
    Context context1 = this.mContext;
    DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context1.getSystemService("device_policy");
    Context context2 = this.mContext;
    PasswordMetrics passwordMetrics = devicePolicyManager.getPasswordMinimumMetrics(context2.getUserId());
    if (paramInt1 != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    List<PasswordValidationError> list = PasswordMetrics.validatePassword(passwordMetrics, paramInt2, bool, paramArrayOfbyte);
    bool = bool1;
    if (list.size() == 0)
      bool = true; 
    return bool;
  }
  
  @SystemApi
  public int getMinLockLength(boolean paramBoolean, int paramInt) {
    if (!checkInitialLockMethodUsage())
      return -1; 
    paramInt = PasswordMetrics.sanitizeComplexityLevel(paramInt);
    Context context1 = this.mContext;
    DevicePolicyManager devicePolicyManager = (DevicePolicyManager)context1.getSystemService("device_policy");
    Context context2 = this.mContext;
    PasswordMetrics passwordMetrics = devicePolicyManager.getPasswordMinimumMetrics(context2.getUserId());
    passwordMetrics = PasswordMetrics.applyComplexity(passwordMetrics, paramBoolean, paramInt);
    return passwordMetrics.length;
  }
  
  @SystemApi
  public boolean setLock(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    if (!checkInitialLockMethodUsage())
      return false; 
    LockPatternUtils lockPatternUtils = new LockPatternUtils(this.mContext);
    int i = this.mContext.getUserId();
    if (isDeviceSecure(i)) {
      Log.e("KeyguardManager", "Password already set, rejecting call to setLock");
      return false;
    } 
    if (!isValidLockPasswordComplexity(paramInt1, paramArrayOfbyte, paramInt2)) {
      Log.e("KeyguardManager", "Password is not valid, rejecting call to setLock");
      return false;
    } 
    boolean bool = false;
    if (paramInt1 != 0) {
      if (paramInt1 != 1) {
        if (paramInt1 != 2) {
          try {
            Log.e("KeyguardManager", "Unknown lock type, returning a failure");
            Arrays.fill(paramArrayOfbyte, (byte)0);
          } catch (Exception exception) {
            Log.e("KeyguardManager", "Save lock exception", exception);
            bool = false;
            Arrays.fill(paramArrayOfbyte, (byte)0);
          } finally {}
        } else {
          List list = LockPatternUtils.byteArrayToPattern(paramArrayOfbyte);
          LockscreenCredential lockscreenCredential1 = LockscreenCredential.createPattern(list);
          LockscreenCredential lockscreenCredential2 = LockscreenCredential.createNone();
          lockPatternUtils.setLockCredential(lockscreenCredential1, lockscreenCredential2, i);
          list.clear();
          bool = true;
          Arrays.fill(paramArrayOfbyte, (byte)0);
        } 
      } else {
        String str = new String();
        this(paramArrayOfbyte);
        LockscreenCredential lockscreenCredential1 = LockscreenCredential.createPin(str);
        LockscreenCredential lockscreenCredential2 = LockscreenCredential.createNone();
        lockPatternUtils.setLockCredential(lockscreenCredential1, lockscreenCredential2, i);
        bool = true;
        Arrays.fill(paramArrayOfbyte, (byte)0);
      } 
    } else {
      String str = new String();
      this(paramArrayOfbyte, Charset.forName("UTF-8"));
      LockscreenCredential lockscreenCredential2 = LockscreenCredential.createPassword(str);
      LockscreenCredential lockscreenCredential1 = LockscreenCredential.createNone();
      lockPatternUtils.setLockCredential(lockscreenCredential2, lockscreenCredential1, i);
      bool = true;
      Arrays.fill(paramArrayOfbyte, (byte)0);
    } 
    return bool;
  }
  
  public void sendWakeUpReasonToKeyguard(String paramString) {
    try {
      this.mWM.sendWakeUpReasonToKeyguard(paramString);
    } catch (RemoteException remoteException) {
      Log.e("KeyguardManager", "[sendWakeUpReasonToKeyguard] ", (Throwable)remoteException);
    } 
  }
}
