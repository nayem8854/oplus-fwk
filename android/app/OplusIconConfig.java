package android.app;

import android.graphics.Path;

public class OplusIconConfig {
  private boolean isEmpty = true;
  
  private int mNeedUpdateConfig = 0;
  
  private boolean mIsDarkMode = false;
  
  private boolean mEnableDarkModeIcon = true;
  
  public static final int UPDATE_DARKMODE_CONFIG = 2;
  
  public static final int UPDATE_ICON_CONFIG = 1;
  
  public static final int UPDATE_NONE = 0;
  
  private boolean artPlusOn;
  
  private int foregroundSize;
  
  private int iconShape;
  
  private int iconSize;
  
  private boolean isForeign;
  
  private Path shapePath;
  
  private int theme;
  
  public int getIconShape() {
    return this.iconShape;
  }
  
  public void setIconShape(int paramInt) {
    this.iconShape = paramInt;
  }
  
  public int getTheme() {
    return this.theme;
  }
  
  public void setTheme(int paramInt) {
    this.theme = paramInt;
  }
  
  public int getIconSize() {
    return this.iconSize;
  }
  
  public void setIconSize(int paramInt) {
    this.iconSize = paramInt;
  }
  
  public int getForegroundSize() {
    return this.foregroundSize;
  }
  
  public void setForegroundSize(int paramInt) {
    this.foregroundSize = paramInt;
  }
  
  public boolean isArtPlusOn() {
    return this.artPlusOn;
  }
  
  public void setArtPlusOn(boolean paramBoolean) {
    this.artPlusOn = paramBoolean;
  }
  
  public Path getShapePath() {
    return this.shapePath;
  }
  
  public void setShapePath(Path paramPath) {
    this.shapePath = new Path(paramPath);
  }
  
  public boolean isForeign() {
    return this.isForeign;
  }
  
  public void setForeign(boolean paramBoolean) {
    this.isForeign = paramBoolean;
  }
  
  public void setEmpty(boolean paramBoolean) {
    this.isEmpty = paramBoolean;
  }
  
  public boolean isEmpty() {
    return this.isEmpty;
  }
  
  public void setNeedUpdate(int paramInt) {
    this.mNeedUpdateConfig = paramInt;
  }
  
  public boolean isNeedUpdate() {
    boolean bool;
    if (this.mNeedUpdateConfig != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int getNeedUpdateConfig() {
    return this.mNeedUpdateConfig;
  }
  
  public void setIsDarkMode(boolean paramBoolean) {
    this.mIsDarkMode = paramBoolean;
  }
  
  public boolean isDarkMode() {
    return this.mIsDarkMode;
  }
  
  public void setEnableDarkModeIcon(boolean paramBoolean) {
    this.mEnableDarkModeIcon = paramBoolean;
  }
  
  public boolean isEnableDarkModeIcon() {
    return this.mEnableDarkModeIcon;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("OplusIconConfig = [ isForeign : ");
    stringBuilder.append(this.isForeign);
    stringBuilder.append(",theme : ");
    stringBuilder.append(this.theme);
    stringBuilder.append(",iconSize : ");
    stringBuilder.append(this.iconSize);
    stringBuilder.append(",iconShape : ");
    stringBuilder.append(this.iconShape);
    stringBuilder.append(",foregroundSize : ");
    stringBuilder.append(this.foregroundSize);
    stringBuilder.append(",artPlusOn : ");
    stringBuilder.append(this.artPlusOn);
    stringBuilder.append(",shapePath ：");
    stringBuilder.append(this.shapePath);
    stringBuilder.append(", nightMode : ");
    stringBuilder.append(this.mIsDarkMode);
    stringBuilder.append(";enableDarkModeIcon = ");
    stringBuilder.append(this.mEnableDarkModeIcon);
    stringBuilder.append(" ]");
    return stringBuilder.toString();
  }
}
