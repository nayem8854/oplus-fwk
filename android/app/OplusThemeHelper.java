package android.app;

import android.content.Context;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.OplusBaseResources;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import com.oplus.theme.OplusAppIconInfo;
import com.oplus.theme.OplusConvertIcon;
import com.oplus.theme.OplusThirdPartUtil;
import oplus.content.res.OplusExtraConfiguration;

public class OplusThemeHelper {
  public static final int DARKMODE_ICON_TRANSLATE_BIT_LENGTH = 61;
  
  private static final String TAG = "OplusThemeHelper";
  
  private static boolean sPreLoading = false;
  
  public static void handleExtraConfigurationChanges(int paramInt) {
    if ((0x8000000 & paramInt) != 0) {
      Canvas.freeCaches();
    } else if ((paramInt & 0x200) != 0) {
      Canvas.freeCaches();
    } else if ((Integer.MIN_VALUE & paramInt) != 0) {
      Canvas.freeCaches();
    } 
  }
  
  public static void handleExtraConfigurationChanges(int paramInt, Configuration paramConfiguration, Context paramContext, Handler paramHandler) {
    if ((0x8000000 & paramInt) != 0 || (paramInt & 0x200) != 0 || (Integer.MIN_VALUE & paramInt) != 0)
      handleExtraConfigurationChanges(paramInt); 
  }
  
  public static Drawable getDrawable(PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo, PackageItemInfo paramPackageItemInfo, boolean paramBoolean) {
    if (!paramBoolean || paramPackageItemInfo == null || !(paramPackageManager instanceof OplusBaseApplicationPackageManager)) {
      drawable = paramPackageManager.getDrawable(paramString, paramInt, paramApplicationInfo);
      return drawable;
    } 
    Drawable drawable = getDrawable((OplusBaseApplicationPackageManager)drawable, paramString, paramInt, paramApplicationInfo, paramPackageItemInfo.name);
    return drawable;
  }
  
  private static boolean isGoogleApps(String paramString, boolean paramBoolean) {
    if (paramBoolean && !TextUtils.isEmpty(paramString) && (
      paramString.startsWith("com.google.android") || 
      paramString.equals("com.googlesuit.ggkj") || 
      paramString.equals("com.google.earth") || 
      paramString.equals("net.eeekeie.kekegdleiedec") || 
      paramString.equals("com.jsoh.GoogleService") || 
      paramString.equals("lin.wang.allspeak") || 
      paramString.equals("com.android.vending") || 
      paramString.equals("com.android.chrome")))
      return true; 
    return false;
  }
  
  public static Drawable getDrawable(OplusBaseApplicationPackageManager paramOplusBaseApplicationPackageManager, String paramString1, int paramInt, ApplicationInfo paramApplicationInfo, String paramString2) {
    // Byte code:
    //   0: ldc android/app/OplusThemeHelper
    //   2: monitorenter
    //   3: aload_0
    //   4: aload_1
    //   5: iload_2
    //   6: invokevirtual getCachedIconForThemeHelper : (Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;
    //   9: astore #5
    //   11: aload #5
    //   13: astore #4
    //   15: aload #4
    //   17: ifnull -> 26
    //   20: ldc android/app/OplusThemeHelper
    //   22: monitorexit
    //   23: aload #4
    //   25: areturn
    //   26: iconst_1
    //   27: istore #6
    //   29: iconst_0
    //   30: istore #7
    //   32: aload_3
    //   33: ifnonnull -> 53
    //   36: aload_0
    //   37: aload_1
    //   38: iconst_0
    //   39: invokevirtual getApplicationInfo : (Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    //   42: astore #8
    //   44: goto -> 56
    //   47: astore_0
    //   48: ldc android/app/OplusThemeHelper
    //   50: monitorexit
    //   51: aconst_null
    //   52: areturn
    //   53: aload_3
    //   54: astore #8
    //   56: aload_0
    //   57: aload #8
    //   59: invokevirtual getOplusBaseResourcesForThemeHelper : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/OplusBaseResources;
    //   62: astore #9
    //   64: aload_0
    //   65: aload #8
    //   67: invokevirtual getResourcesForApplication : (Landroid/content/pm/ApplicationInfo;)Landroid/content/res/Resources;
    //   70: astore #10
    //   72: aload #9
    //   74: aload #8
    //   76: getfield packageName : Ljava/lang/String;
    //   79: invokestatic getExtraConfig : (Landroid/content/res/OplusBaseResources;Ljava/lang/String;)Loplus/content/res/OplusExtraConfiguration;
    //   82: astore_3
    //   83: aload_3
    //   84: ifnonnull -> 90
    //   87: goto -> 96
    //   90: aload_3
    //   91: getfield mUserId : I
    //   94: istore #7
    //   96: getstatic android/app/OplusThemeHelper.sPreLoading : Z
    //   99: istore #11
    //   101: iload #11
    //   103: ifeq -> 205
    //   106: aload #4
    //   108: astore #12
    //   110: aload #4
    //   112: astore #13
    //   114: aload_3
    //   115: invokestatic needUpdateTheme : (Loplus/content/res/OplusExtraConfiguration;)Z
    //   118: ifeq -> 124
    //   121: goto -> 205
    //   124: aload #4
    //   126: astore #12
    //   128: aload #4
    //   130: astore #13
    //   132: invokestatic hasInit : ()Z
    //   135: ifne -> 153
    //   138: aload #4
    //   140: astore #12
    //   142: aload #4
    //   144: astore #13
    //   146: aload #10
    //   148: iload #7
    //   150: invokestatic initConvertIconForUser : (Landroid/content/res/Resources;I)V
    //   153: aload #4
    //   155: astore #12
    //   157: aload #4
    //   159: astore #13
    //   161: invokestatic getAppsNumbers : ()I
    //   164: ifgt -> 223
    //   167: aload #4
    //   169: astore #12
    //   171: aload #4
    //   173: astore #13
    //   175: iload #7
    //   177: invokestatic parseIconXmlForUser : (I)Z
    //   180: istore #6
    //   182: goto -> 223
    //   185: astore_3
    //   186: aload #12
    //   188: astore #4
    //   190: goto -> 548
    //   193: astore_3
    //   194: aload #13
    //   196: astore #4
    //   198: goto -> 607
    //   201: astore_0
    //   202: goto -> 797
    //   205: iconst_1
    //   206: putstatic android/app/OplusThemeHelper.sPreLoading : Z
    //   209: aload #10
    //   211: iload #7
    //   213: invokestatic initConvertIconForUser : (Landroid/content/res/Resources;I)V
    //   216: iload #7
    //   218: invokestatic parseIconXmlForUser : (I)Z
    //   221: istore #6
    //   223: aconst_null
    //   224: astore_3
    //   225: aload #10
    //   227: ifnull -> 544
    //   230: aload #8
    //   232: invokestatic isThirdPart : (Landroid/content/pm/ApplicationInfo;)Z
    //   235: istore #11
    //   237: iload #11
    //   239: ifne -> 449
    //   242: getstatic com/oplus/theme/OplusThirdPartUtil.mIsDefaultTheme : Z
    //   245: istore #14
    //   247: aload #10
    //   249: iload_2
    //   250: invokevirtual getResourceName : (I)Ljava/lang/String;
    //   253: astore #13
    //   255: new java/lang/StringBuffer
    //   258: astore #12
    //   260: aload #12
    //   262: aload #13
    //   264: invokespecial <init> : (Ljava/lang/String;)V
    //   267: aload #12
    //   269: ldc '/'
    //   271: invokevirtual lastIndexOf : (Ljava/lang/String;)I
    //   274: istore #7
    //   276: iload #7
    //   278: iflt -> 318
    //   281: new java/lang/StringBuilder
    //   284: astore_3
    //   285: aload_3
    //   286: invokespecial <init> : ()V
    //   289: aload_3
    //   290: aload #12
    //   292: iload #7
    //   294: iconst_1
    //   295: iadd
    //   296: invokevirtual substring : (I)Ljava/lang/String;
    //   299: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   302: pop
    //   303: aload_3
    //   304: ldc '.png'
    //   306: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   309: pop
    //   310: aload_3
    //   311: invokevirtual toString : ()Ljava/lang/String;
    //   314: astore_3
    //   315: goto -> 318
    //   318: aload_3
    //   319: ifnull -> 408
    //   322: aload_3
    //   323: invokestatic isThirdPartbyIconName : (Ljava/lang/String;)Z
    //   326: istore #15
    //   328: aload #8
    //   330: getfield packageName : Ljava/lang/String;
    //   333: astore #4
    //   335: aload #4
    //   337: invokestatic indexOfPackageName : (Ljava/lang/String;)I
    //   340: istore #7
    //   342: iload #7
    //   344: iflt -> 357
    //   347: iload #7
    //   349: invokestatic getIconName : (I)Ljava/lang/String;
    //   352: astore #4
    //   354: goto -> 360
    //   357: aconst_null
    //   358: astore #4
    //   360: aload #4
    //   362: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   365: ifne -> 396
    //   368: iload #15
    //   370: ifeq -> 396
    //   373: aload_3
    //   374: aload #4
    //   376: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   379: ifne -> 396
    //   382: aload #9
    //   384: iload_2
    //   385: aload #4
    //   387: iload #14
    //   389: invokevirtual loadIcon : (ILjava/lang/String;Z)Landroid/graphics/drawable/Drawable;
    //   392: astore_3
    //   393: goto -> 405
    //   396: aload #9
    //   398: iload_2
    //   399: iload #14
    //   401: invokevirtual loadIcon : (IZ)Landroid/graphics/drawable/Drawable;
    //   404: astore_3
    //   405: goto -> 417
    //   408: aload #9
    //   410: iload_2
    //   411: iload #14
    //   413: invokevirtual loadIcon : (IZ)Landroid/graphics/drawable/Drawable;
    //   416: astore_3
    //   417: aload_3
    //   418: astore #4
    //   420: aload_3
    //   421: ifnonnull -> 443
    //   424: aload_3
    //   425: astore #12
    //   427: aload_3
    //   428: astore #13
    //   430: aload_0
    //   431: aload_1
    //   432: iload_2
    //   433: aload #8
    //   435: invokevirtual getDrawable : (Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    //   438: astore #4
    //   440: iconst_1
    //   441: istore #11
    //   443: aload #4
    //   445: astore_3
    //   446: goto -> 458
    //   449: aload_0
    //   450: aload_1
    //   451: iload_2
    //   452: aload #8
    //   454: invokevirtual getDrawable : (Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    //   457: astore_3
    //   458: aload_3
    //   459: astore #4
    //   461: aload_3
    //   462: ifnull -> 544
    //   465: aload_3
    //   466: astore #4
    //   468: iload #6
    //   470: ifeq -> 544
    //   473: aload_3
    //   474: astore #12
    //   476: aload_3
    //   477: astore #13
    //   479: aload_3
    //   480: astore #4
    //   482: aload_3
    //   483: instanceof android/graphics/drawable/LayerDrawable
    //   486: ifne -> 544
    //   489: aload_3
    //   490: astore #12
    //   492: aload_3
    //   493: astore #13
    //   495: new android/graphics/drawable/BitmapDrawable
    //   498: astore #4
    //   500: aload_3
    //   501: astore #12
    //   503: aload_3
    //   504: astore #13
    //   506: aload #4
    //   508: aload #10
    //   510: aload_3
    //   511: aload #10
    //   513: iload #11
    //   515: invokestatic convertIconBitmap : (Landroid/graphics/drawable/Drawable;Landroid/content/res/Resources;Z)Landroid/graphics/Bitmap;
    //   518: invokespecial <init> : (Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    //   521: goto -> 544
    //   524: astore_3
    //   525: aload #5
    //   527: astore #4
    //   529: goto -> 548
    //   532: astore_3
    //   533: aload #5
    //   535: astore #4
    //   537: goto -> 607
    //   540: astore_0
    //   541: goto -> 797
    //   544: goto -> 665
    //   547: astore_3
    //   548: new java/lang/StringBuilder
    //   551: astore #12
    //   553: aload #12
    //   555: invokespecial <init> : ()V
    //   558: aload #12
    //   560: ldc 'getDrawable. Failure retrieving icon 0x'
    //   562: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   565: pop
    //   566: aload #12
    //   568: iload_2
    //   569: invokestatic toHexString : (I)Ljava/lang/String;
    //   572: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   575: pop
    //   576: aload #12
    //   578: ldc ' in package '
    //   580: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   583: pop
    //   584: aload #12
    //   586: aload_1
    //   587: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   590: pop
    //   591: ldc 'OplusThemeHelper'
    //   593: aload #12
    //   595: invokevirtual toString : ()Ljava/lang/String;
    //   598: aload_3
    //   599: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   602: pop
    //   603: goto -> 665
    //   606: astore_3
    //   607: new java/lang/StringBuilder
    //   610: astore #12
    //   612: aload #12
    //   614: invokespecial <init> : ()V
    //   617: aload #12
    //   619: ldc 'getDrawable. Failure retrieving resources for '
    //   621: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   624: pop
    //   625: aload #12
    //   627: aload #8
    //   629: getfield packageName : Ljava/lang/String;
    //   632: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   635: pop
    //   636: aload #12
    //   638: ldc ': '
    //   640: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   643: pop
    //   644: aload #12
    //   646: aload_3
    //   647: invokevirtual getMessage : ()Ljava/lang/String;
    //   650: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   653: pop
    //   654: ldc 'OplusThemeHelper'
    //   656: aload #12
    //   658: invokevirtual toString : ()Ljava/lang/String;
    //   661: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   664: pop
    //   665: aload #4
    //   667: ifnull -> 790
    //   670: aload_0
    //   671: aload_1
    //   672: iload_2
    //   673: aload #4
    //   675: invokevirtual putCachedIconForThemeHelper : (Ljava/lang/String;ILandroid/graphics/drawable/Drawable;)V
    //   678: goto -> 790
    //   681: astore_0
    //   682: new java/lang/StringBuilder
    //   685: astore_3
    //   686: aload_3
    //   687: invokespecial <init> : ()V
    //   690: aload_3
    //   691: ldc 'getDrawable. Failure retrieving icon 0x'
    //   693: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   696: pop
    //   697: aload_3
    //   698: iload_2
    //   699: invokestatic toHexString : (I)Ljava/lang/String;
    //   702: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: pop
    //   706: aload_3
    //   707: ldc ' in package '
    //   709: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   712: pop
    //   713: aload_3
    //   714: aload_1
    //   715: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   718: pop
    //   719: ldc 'OplusThemeHelper'
    //   721: aload_3
    //   722: invokevirtual toString : ()Ljava/lang/String;
    //   725: aload_0
    //   726: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   729: pop
    //   730: goto -> 785
    //   733: astore_0
    //   734: new java/lang/StringBuilder
    //   737: astore_1
    //   738: aload_1
    //   739: invokespecial <init> : ()V
    //   742: aload_1
    //   743: ldc 'getDrawable. Failure retrieving resources for '
    //   745: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   748: pop
    //   749: aload_1
    //   750: aload #8
    //   752: getfield packageName : Ljava/lang/String;
    //   755: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   758: pop
    //   759: aload_1
    //   760: ldc ': '
    //   762: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   765: pop
    //   766: aload_1
    //   767: aload_0
    //   768: invokevirtual getMessage : ()Ljava/lang/String;
    //   771: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   774: pop
    //   775: ldc 'OplusThemeHelper'
    //   777: aload_1
    //   778: invokevirtual toString : ()Ljava/lang/String;
    //   781: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   784: pop
    //   785: ldc android/app/OplusThemeHelper
    //   787: monitorexit
    //   788: aconst_null
    //   789: areturn
    //   790: ldc android/app/OplusThemeHelper
    //   792: monitorexit
    //   793: aload #4
    //   795: areturn
    //   796: astore_0
    //   797: ldc android/app/OplusThemeHelper
    //   799: monitorexit
    //   800: aconst_null
    //   801: areturn
    //   802: astore_0
    //   803: ldc android/app/OplusThemeHelper
    //   805: monitorexit
    //   806: aload_0
    //   807: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #114	-> 3
    //   #115	-> 15
    //   #116	-> 20
    //   #118	-> 26
    //   #119	-> 29
    //   #121	-> 36
    //   #124	-> 44
    //   #122	-> 47
    //   #123	-> 48
    //   #119	-> 53
    //   #128	-> 56
    //   #129	-> 64
    //   #130	-> 72
    //   #131	-> 83
    //   #132	-> 96
    //   #137	-> 124
    //   #138	-> 138
    //   #140	-> 153
    //   #141	-> 167
    //   #192	-> 185
    //   #190	-> 193
    //   #188	-> 201
    //   #133	-> 205
    //   #134	-> 209
    //   #135	-> 216
    //   #145	-> 223
    //   #146	-> 225
    //   #147	-> 230
    //   #148	-> 237
    //   #149	-> 242
    //   #150	-> 247
    //   #151	-> 255
    //   #152	-> 267
    //   #153	-> 276
    //   #154	-> 281
    //   #153	-> 318
    //   #157	-> 318
    //   #158	-> 322
    //   #159	-> 322
    //   #160	-> 328
    //   #161	-> 335
    //   #162	-> 342
    //   #163	-> 357
    //   #164	-> 360
    //   #165	-> 373
    //   #166	-> 382
    //   #164	-> 396
    //   #168	-> 396
    //   #170	-> 405
    //   #171	-> 408
    //   #174	-> 417
    //   #175	-> 424
    //   #176	-> 424
    //   #178	-> 443
    //   #179	-> 449
    //   #183	-> 458
    //   #185	-> 489
    //   #192	-> 524
    //   #190	-> 532
    //   #188	-> 540
    //   #146	-> 544
    //   #196	-> 544
    //   #192	-> 547
    //   #195	-> 548
    //   #190	-> 606
    //   #191	-> 607
    //   #196	-> 665
    //   #198	-> 665
    //   #199	-> 670
    //   #204	-> 681
    //   #207	-> 682
    //   #202	-> 733
    //   #203	-> 734
    //   #208	-> 785
    //   #209	-> 785
    //   #201	-> 790
    //   #188	-> 796
    //   #189	-> 797
    //   #113	-> 802
    // Exception table:
    //   from	to	target	type
    //   3	11	802	finally
    //   36	44	47	android/content/pm/PackageManager$NameNotFoundException
    //   36	44	802	finally
    //   56	64	796	android/content/pm/PackageManager$NameNotFoundException
    //   56	64	606	android/content/res/Resources$NotFoundException
    //   56	64	547	java/lang/RuntimeException
    //   56	64	802	finally
    //   64	72	796	android/content/pm/PackageManager$NameNotFoundException
    //   64	72	606	android/content/res/Resources$NotFoundException
    //   64	72	547	java/lang/RuntimeException
    //   64	72	802	finally
    //   72	83	796	android/content/pm/PackageManager$NameNotFoundException
    //   72	83	606	android/content/res/Resources$NotFoundException
    //   72	83	547	java/lang/RuntimeException
    //   72	83	802	finally
    //   90	96	796	android/content/pm/PackageManager$NameNotFoundException
    //   90	96	606	android/content/res/Resources$NotFoundException
    //   90	96	547	java/lang/RuntimeException
    //   90	96	802	finally
    //   96	101	796	android/content/pm/PackageManager$NameNotFoundException
    //   96	101	606	android/content/res/Resources$NotFoundException
    //   96	101	547	java/lang/RuntimeException
    //   96	101	802	finally
    //   114	121	201	android/content/pm/PackageManager$NameNotFoundException
    //   114	121	193	android/content/res/Resources$NotFoundException
    //   114	121	185	java/lang/RuntimeException
    //   114	121	802	finally
    //   132	138	201	android/content/pm/PackageManager$NameNotFoundException
    //   132	138	193	android/content/res/Resources$NotFoundException
    //   132	138	185	java/lang/RuntimeException
    //   132	138	802	finally
    //   146	153	201	android/content/pm/PackageManager$NameNotFoundException
    //   146	153	193	android/content/res/Resources$NotFoundException
    //   146	153	185	java/lang/RuntimeException
    //   146	153	802	finally
    //   161	167	201	android/content/pm/PackageManager$NameNotFoundException
    //   161	167	193	android/content/res/Resources$NotFoundException
    //   161	167	185	java/lang/RuntimeException
    //   161	167	802	finally
    //   175	182	201	android/content/pm/PackageManager$NameNotFoundException
    //   175	182	193	android/content/res/Resources$NotFoundException
    //   175	182	185	java/lang/RuntimeException
    //   175	182	802	finally
    //   205	209	796	android/content/pm/PackageManager$NameNotFoundException
    //   205	209	606	android/content/res/Resources$NotFoundException
    //   205	209	547	java/lang/RuntimeException
    //   205	209	802	finally
    //   209	216	796	android/content/pm/PackageManager$NameNotFoundException
    //   209	216	606	android/content/res/Resources$NotFoundException
    //   209	216	547	java/lang/RuntimeException
    //   209	216	802	finally
    //   216	223	796	android/content/pm/PackageManager$NameNotFoundException
    //   216	223	606	android/content/res/Resources$NotFoundException
    //   216	223	547	java/lang/RuntimeException
    //   216	223	802	finally
    //   230	237	796	android/content/pm/PackageManager$NameNotFoundException
    //   230	237	606	android/content/res/Resources$NotFoundException
    //   230	237	547	java/lang/RuntimeException
    //   230	237	802	finally
    //   242	247	796	android/content/pm/PackageManager$NameNotFoundException
    //   242	247	606	android/content/res/Resources$NotFoundException
    //   242	247	547	java/lang/RuntimeException
    //   242	247	802	finally
    //   247	255	796	android/content/pm/PackageManager$NameNotFoundException
    //   247	255	606	android/content/res/Resources$NotFoundException
    //   247	255	547	java/lang/RuntimeException
    //   247	255	802	finally
    //   255	267	796	android/content/pm/PackageManager$NameNotFoundException
    //   255	267	606	android/content/res/Resources$NotFoundException
    //   255	267	547	java/lang/RuntimeException
    //   255	267	802	finally
    //   267	276	796	android/content/pm/PackageManager$NameNotFoundException
    //   267	276	606	android/content/res/Resources$NotFoundException
    //   267	276	547	java/lang/RuntimeException
    //   267	276	802	finally
    //   281	315	540	android/content/pm/PackageManager$NameNotFoundException
    //   281	315	532	android/content/res/Resources$NotFoundException
    //   281	315	524	java/lang/RuntimeException
    //   281	315	802	finally
    //   322	328	540	android/content/pm/PackageManager$NameNotFoundException
    //   322	328	532	android/content/res/Resources$NotFoundException
    //   322	328	524	java/lang/RuntimeException
    //   322	328	802	finally
    //   328	335	540	android/content/pm/PackageManager$NameNotFoundException
    //   328	335	532	android/content/res/Resources$NotFoundException
    //   328	335	524	java/lang/RuntimeException
    //   328	335	802	finally
    //   335	342	540	android/content/pm/PackageManager$NameNotFoundException
    //   335	342	532	android/content/res/Resources$NotFoundException
    //   335	342	524	java/lang/RuntimeException
    //   335	342	802	finally
    //   347	354	540	android/content/pm/PackageManager$NameNotFoundException
    //   347	354	532	android/content/res/Resources$NotFoundException
    //   347	354	524	java/lang/RuntimeException
    //   347	354	802	finally
    //   360	368	540	android/content/pm/PackageManager$NameNotFoundException
    //   360	368	532	android/content/res/Resources$NotFoundException
    //   360	368	524	java/lang/RuntimeException
    //   360	368	802	finally
    //   373	382	540	android/content/pm/PackageManager$NameNotFoundException
    //   373	382	532	android/content/res/Resources$NotFoundException
    //   373	382	524	java/lang/RuntimeException
    //   373	382	802	finally
    //   382	393	540	android/content/pm/PackageManager$NameNotFoundException
    //   382	393	532	android/content/res/Resources$NotFoundException
    //   382	393	524	java/lang/RuntimeException
    //   382	393	802	finally
    //   396	405	540	android/content/pm/PackageManager$NameNotFoundException
    //   396	405	532	android/content/res/Resources$NotFoundException
    //   396	405	524	java/lang/RuntimeException
    //   396	405	802	finally
    //   408	417	540	android/content/pm/PackageManager$NameNotFoundException
    //   408	417	532	android/content/res/Resources$NotFoundException
    //   408	417	524	java/lang/RuntimeException
    //   408	417	802	finally
    //   430	440	201	android/content/pm/PackageManager$NameNotFoundException
    //   430	440	193	android/content/res/Resources$NotFoundException
    //   430	440	185	java/lang/RuntimeException
    //   430	440	802	finally
    //   449	458	540	android/content/pm/PackageManager$NameNotFoundException
    //   449	458	532	android/content/res/Resources$NotFoundException
    //   449	458	524	java/lang/RuntimeException
    //   449	458	802	finally
    //   482	489	201	android/content/pm/PackageManager$NameNotFoundException
    //   482	489	193	android/content/res/Resources$NotFoundException
    //   482	489	185	java/lang/RuntimeException
    //   482	489	802	finally
    //   495	500	201	android/content/pm/PackageManager$NameNotFoundException
    //   495	500	193	android/content/res/Resources$NotFoundException
    //   495	500	185	java/lang/RuntimeException
    //   495	500	802	finally
    //   506	521	201	android/content/pm/PackageManager$NameNotFoundException
    //   506	521	193	android/content/res/Resources$NotFoundException
    //   506	521	185	java/lang/RuntimeException
    //   506	521	802	finally
    //   548	603	802	finally
    //   607	665	802	finally
    //   670	678	733	android/content/res/Resources$NotFoundException
    //   670	678	681	java/lang/RuntimeException
    //   670	678	802	finally
    //   682	730	802	finally
    //   734	785	802	finally
  }
  
  public static Drawable getDrawableByConvert(OplusBaseResources paramOplusBaseResources, Resources paramResources, Drawable paramDrawable) {
    /* monitor enter TypeReferenceDotClassExpression{ObjectType{android/app/OplusThemeHelper}} */
    if (paramResources == null || paramDrawable == null) {
      /* monitor exit TypeReferenceDotClassExpression{ObjectType{android/app/OplusThemeHelper}} */
      return paramDrawable;
    } 
    boolean bool = true;
    Drawable drawable = paramDrawable;
    try {
      int i;
      OplusExtraConfiguration oplusExtraConfiguration = getExtraConfig(paramOplusBaseResources, null);
      if (oplusExtraConfiguration == null) {
        i = 0;
      } else {
        i = oplusExtraConfiguration.mUserId;
      } 
      if (!sPreLoading || needUpdateTheme(oplusExtraConfiguration)) {
        sPreLoading = true;
        OplusConvertIcon.initConvertIconForUser(paramResources, i);
        bool = OplusAppIconInfo.parseIconXmlForUser(i);
      } else {
        if (!OplusConvertIcon.hasInit())
          OplusConvertIcon.initConvertIconForUser(paramResources, i); 
        if (OplusAppIconInfo.getAppsNumbers() <= 0)
          bool = OplusAppIconInfo.parseIconXmlForUser(i); 
      } 
      Drawable drawable1 = drawable;
      if (bool) {
        drawable1 = drawable;
        if (!(paramDrawable instanceof android.graphics.drawable.LayerDrawable)) {
          drawable1 = new BitmapDrawable();
          super(paramResources, OplusConvertIcon.convertIconBitmap(paramDrawable, paramResources, true));
        } 
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getDrawableByConvert. e = ");
      stringBuilder.append(exception);
      Log.e("OplusThemeHelper", stringBuilder.toString());
      Drawable drawable1 = drawable;
    } finally {}
    /* monitor exit TypeReferenceDotClassExpression{ObjectType{android/app/OplusThemeHelper}} */
    return (Drawable)paramOplusBaseResources;
  }
  
  public static boolean isCustomizedIcon(IntentFilter paramIntentFilter) {
    return false;
  }
  
  public static void reset() {
    sPreLoading = false;
  }
  
  private static boolean needUpdateTheme(OplusExtraConfiguration paramOplusExtraConfiguration) {
    boolean bool = false;
    if (paramOplusExtraConfiguration == null)
      return false; 
    long l = paramOplusExtraConfiguration.mThemeChangedFlags;
    if ((0x1L & l) == 0L)
      bool = true; 
    return bool ^ OplusThirdPartUtil.mIsDefaultTheme;
  }
  
  private static OplusExtraConfiguration getExtraConfig(OplusBaseResources paramOplusBaseResources, String paramString) {
    OplusExtraConfiguration oplusExtraConfiguration;
    if ("system".equals(paramString)) {
      oplusExtraConfiguration = paramOplusBaseResources.getConfiguration().getOplusExtraConfiguration();
    } else {
      oplusExtraConfiguration = oplusExtraConfiguration.getOplusBaseResImpl().getSystemConfiguration().getOplusExtraConfiguration();
    } 
    return oplusExtraConfiguration;
  }
}
