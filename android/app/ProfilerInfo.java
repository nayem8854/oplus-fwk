package android.app;

import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.util.Slog;
import android.util.proto.ProtoOutputStream;
import java.io.IOException;
import java.util.Objects;

public class ProfilerInfo implements Parcelable {
  public ProfilerInfo(String paramString1, ParcelFileDescriptor paramParcelFileDescriptor, int paramInt, boolean paramBoolean1, boolean paramBoolean2, String paramString2, boolean paramBoolean3) {
    this.profileFile = paramString1;
    this.profileFd = paramParcelFileDescriptor;
    this.samplingInterval = paramInt;
    this.autoStopProfiler = paramBoolean1;
    this.streamingOutput = paramBoolean2;
    this.agent = paramString2;
    this.attachAgentDuringBind = paramBoolean3;
  }
  
  public ProfilerInfo(ProfilerInfo paramProfilerInfo) {
    this.profileFile = paramProfilerInfo.profileFile;
    this.profileFd = paramProfilerInfo.profileFd;
    this.samplingInterval = paramProfilerInfo.samplingInterval;
    this.autoStopProfiler = paramProfilerInfo.autoStopProfiler;
    this.streamingOutput = paramProfilerInfo.streamingOutput;
    this.agent = paramProfilerInfo.agent;
    this.attachAgentDuringBind = paramProfilerInfo.attachAgentDuringBind;
  }
  
  public ProfilerInfo setAgent(String paramString, boolean paramBoolean) {
    return new ProfilerInfo(this.profileFile, this.profileFd, this.samplingInterval, this.autoStopProfiler, this.streamingOutput, paramString, paramBoolean);
  }
  
  public void closeFd() {
    ParcelFileDescriptor parcelFileDescriptor = this.profileFd;
    if (parcelFileDescriptor != null) {
      try {
        parcelFileDescriptor.close();
      } catch (IOException iOException) {
        Slog.w("ProfilerInfo", "Failure closing profile fd", iOException);
      } 
      this.profileFd = null;
    } 
  }
  
  public int describeContents() {
    ParcelFileDescriptor parcelFileDescriptor = this.profileFd;
    if (parcelFileDescriptor != null)
      return parcelFileDescriptor.describeContents(); 
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.profileFile);
    if (this.profileFd != null) {
      paramParcel.writeInt(1);
      this.profileFd.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.samplingInterval);
    paramParcel.writeInt(this.autoStopProfiler);
    paramParcel.writeInt(this.streamingOutput);
    paramParcel.writeString(this.agent);
    paramParcel.writeBoolean(this.attachAgentDuringBind);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, this.profileFile);
    ParcelFileDescriptor parcelFileDescriptor = this.profileFd;
    if (parcelFileDescriptor != null)
      paramProtoOutputStream.write(1120986464258L, parcelFileDescriptor.getFd()); 
    paramProtoOutputStream.write(1120986464259L, this.samplingInterval);
    paramProtoOutputStream.write(1133871366148L, this.autoStopProfiler);
    paramProtoOutputStream.write(1133871366149L, this.streamingOutput);
    paramProtoOutputStream.write(1138166333446L, this.agent);
    paramProtoOutputStream.end(paramLong);
  }
  
  public static final Parcelable.Creator<ProfilerInfo> CREATOR = new Parcelable.Creator<ProfilerInfo>() {
      public ProfilerInfo createFromParcel(Parcel param1Parcel) {
        return new ProfilerInfo(param1Parcel);
      }
      
      public ProfilerInfo[] newArray(int param1Int) {
        return new ProfilerInfo[param1Int];
      }
    };
  
  private static final String TAG = "ProfilerInfo";
  
  public final String agent;
  
  public final boolean attachAgentDuringBind;
  
  public final boolean autoStopProfiler;
  
  public ParcelFileDescriptor profileFd;
  
  public final String profileFile;
  
  public final int samplingInterval;
  
  public final boolean streamingOutput;
  
  private ProfilerInfo(Parcel paramParcel) {
    ParcelFileDescriptor parcelFileDescriptor;
    boolean bool2;
    this.profileFile = paramParcel.readString();
    if (paramParcel.readInt() != 0) {
      parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(paramParcel);
    } else {
      parcelFileDescriptor = null;
    } 
    this.profileFd = parcelFileDescriptor;
    this.samplingInterval = paramParcel.readInt();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.autoStopProfiler = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.streamingOutput = bool2;
    this.agent = paramParcel.readString();
    this.attachAgentDuringBind = paramParcel.readBoolean();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    ProfilerInfo profilerInfo = (ProfilerInfo)paramObject;
    if (Objects.equals(this.profileFile, profilerInfo.profileFile) && this.autoStopProfiler == profilerInfo.autoStopProfiler && this.samplingInterval == profilerInfo.samplingInterval && this.streamingOutput == profilerInfo.streamingOutput) {
      paramObject = this.agent;
      String str = profilerInfo.agent;
      if (Objects.equals(paramObject, str))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.profileFile);
    int j = this.samplingInterval;
    boolean bool1 = this.autoStopProfiler;
    boolean bool2 = this.streamingOutput;
    int k = Objects.hashCode(this.agent);
    return ((((17 * 31 + i) * 31 + j) * 31 + bool1) * 31 + bool2) * 31 + k;
  }
}
