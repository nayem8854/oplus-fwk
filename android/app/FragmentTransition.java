package android.app;

import android.graphics.Rect;
import android.transition.Transition;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.util.ArrayMap;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.view.OneShotPreDrawListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

class FragmentTransition {
  private static final int[] INVERSE_OPS = new int[] { 0, 3, 0, 1, 5, 4, 7, 6, 9, 8 };
  
  static void startTransitions(FragmentManagerImpl paramFragmentManagerImpl, ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2, boolean paramBoolean) {
    if (paramFragmentManagerImpl.mCurState < 1)
      return; 
    SparseArray<FragmentContainerTransition> sparseArray = new SparseArray();
    int i;
    for (i = paramInt1; i < paramInt2; i++) {
      BackStackRecord backStackRecord = paramArrayList.get(i);
      boolean bool = ((Boolean)paramArrayList1.get(i)).booleanValue();
      if (bool) {
        calculatePopFragments(backStackRecord, sparseArray, paramBoolean);
      } else {
        calculateFragments(backStackRecord, sparseArray, paramBoolean);
      } 
    } 
    if (sparseArray.size() != 0) {
      View view = new View(paramFragmentManagerImpl.mHost.getContext());
      int j = sparseArray.size();
      for (i = 0; i < j; i++) {
        int k = sparseArray.keyAt(i);
        ArrayMap<String, String> arrayMap = calculateNameOverrides(k, paramArrayList, paramArrayList1, paramInt1, paramInt2);
        FragmentContainerTransition fragmentContainerTransition = (FragmentContainerTransition)sparseArray.valueAt(i);
        if (paramBoolean) {
          configureTransitionsReordered(paramFragmentManagerImpl, k, fragmentContainerTransition, view, arrayMap);
        } else {
          configureTransitionsOrdered(paramFragmentManagerImpl, k, fragmentContainerTransition, view, arrayMap);
        } 
      } 
    } 
  }
  
  private static ArrayMap<String, String> calculateNameOverrides(int paramInt1, ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt2, int paramInt3) {
    ArrayMap<String, String> arrayMap = new ArrayMap();
    for (; --paramInt3 >= paramInt2; paramInt3--) {
      BackStackRecord backStackRecord = paramArrayList.get(paramInt3);
      if (backStackRecord.interactsWith(paramInt1)) {
        boolean bool = ((Boolean)paramArrayList1.get(paramInt3)).booleanValue();
        if (backStackRecord.mSharedElementSourceNames != null) {
          ArrayList<String> arrayList1, arrayList2;
          int i = backStackRecord.mSharedElementSourceNames.size();
          if (bool) {
            arrayList1 = backStackRecord.mSharedElementSourceNames;
            arrayList2 = backStackRecord.mSharedElementTargetNames;
          } else {
            arrayList2 = backStackRecord.mSharedElementSourceNames;
            arrayList1 = backStackRecord.mSharedElementTargetNames;
          } 
          for (byte b = 0; b < i; b++) {
            String str1 = arrayList2.get(b);
            String str2 = arrayList1.get(b);
            String str3 = (String)arrayMap.remove(str2);
            if (str3 != null) {
              arrayMap.put(str1, str3);
            } else {
              arrayMap.put(str1, str2);
            } 
          } 
        } 
      } 
    } 
    return arrayMap;
  }
  
  private static void configureTransitionsReordered(FragmentManagerImpl paramFragmentManagerImpl, int paramInt, FragmentContainerTransition paramFragmentContainerTransition, View paramView, ArrayMap<String, String> paramArrayMap) {
    if (paramFragmentManagerImpl.mContainer.onHasView()) {
      ViewGroup viewGroup = paramFragmentManagerImpl.mContainer.<ViewGroup>onFindViewById(paramInt);
    } else {
      paramFragmentManagerImpl = null;
    } 
    if (paramFragmentManagerImpl == null)
      return; 
    Fragment fragment1 = paramFragmentContainerTransition.lastIn;
    Fragment fragment2 = paramFragmentContainerTransition.firstOut;
    boolean bool1 = paramFragmentContainerTransition.lastInIsPop;
    boolean bool2 = paramFragmentContainerTransition.firstOutIsPop;
    ArrayList<View> arrayList3 = new ArrayList();
    ArrayList<View> arrayList4 = new ArrayList();
    Transition transition2 = getEnterTransition(fragment1, bool1);
    Transition transition3 = getExitTransition(fragment2, bool2);
    TransitionSet transitionSet = configureSharedElementsReordered((ViewGroup)paramFragmentManagerImpl, paramView, paramArrayMap, paramFragmentContainerTransition, arrayList4, arrayList3, transition2, transition3);
    if (transition2 == null && transitionSet == null && transition3 == null)
      return; 
    ArrayList<View> arrayList1 = configureEnteringExitingViews(transition3, fragment2, arrayList4, paramView);
    ArrayList<View> arrayList2 = configureEnteringExitingViews(transition2, fragment1, arrayList3, paramView);
    setViewVisibility(arrayList2, 4);
    Transition transition1 = mergeTransitions(transition2, transition3, (Transition)transitionSet, fragment1, bool1);
    if (transition1 != null) {
      replaceHide(transition3, fragment2, arrayList1);
      transition1.setNameOverrides(paramArrayMap);
      scheduleRemoveTargets(transition1, transition2, arrayList2, transition3, arrayList1, transitionSet, arrayList3);
      TransitionManager.beginDelayedTransition((ViewGroup)paramFragmentManagerImpl, transition1);
      setViewVisibility(arrayList2, 0);
      if (transitionSet != null) {
        transitionSet.getTargets().clear();
        transitionSet.getTargets().addAll(arrayList3);
        replaceTargets((Transition)transitionSet, arrayList4, arrayList3);
      } 
    } 
  }
  
  private static void configureTransitionsOrdered(FragmentManagerImpl paramFragmentManagerImpl, int paramInt, FragmentContainerTransition paramFragmentContainerTransition, View paramView, ArrayMap<String, String> paramArrayMap) {
    if (paramFragmentManagerImpl.mContainer.onHasView()) {
      ViewGroup viewGroup = paramFragmentManagerImpl.mContainer.<ViewGroup>onFindViewById(paramInt);
    } else {
      paramFragmentManagerImpl = null;
    } 
    if (paramFragmentManagerImpl == null)
      return; 
    Fragment fragment1 = paramFragmentContainerTransition.lastIn;
    Fragment fragment2 = paramFragmentContainerTransition.firstOut;
    boolean bool1 = paramFragmentContainerTransition.lastInIsPop;
    boolean bool2 = paramFragmentContainerTransition.firstOutIsPop;
    Transition transition2 = getEnterTransition(fragment1, bool1);
    Transition transition3 = getExitTransition(fragment2, bool2);
    ArrayList<View> arrayList1 = new ArrayList();
    ArrayList<View> arrayList2 = new ArrayList();
    TransitionSet transitionSet = configureSharedElementsOrdered((ViewGroup)paramFragmentManagerImpl, paramView, paramArrayMap, paramFragmentContainerTransition, arrayList1, arrayList2, transition2, transition3);
    if (transition2 == null && transitionSet == null && transition3 == null)
      return; 
    arrayList1 = configureEnteringExitingViews(transition3, fragment2, arrayList1, paramView);
    if (arrayList1 == null || arrayList1.isEmpty())
      transition3 = null; 
    if (transition2 != null)
      transition2.addTarget(paramView); 
    Transition transition1 = mergeTransitions(transition2, transition3, (Transition)transitionSet, fragment1, paramFragmentContainerTransition.lastInIsPop);
    if (transition1 != null) {
      transition1.setNameOverrides(paramArrayMap);
      ArrayList<View> arrayList = new ArrayList();
      scheduleRemoveTargets(transition1, transition2, arrayList, transition3, arrayList1, transitionSet, arrayList2);
      scheduleTargetChange((ViewGroup)paramFragmentManagerImpl, fragment1, paramView, arrayList2, transition2, arrayList, transition3, arrayList1);
      TransitionManager.beginDelayedTransition((ViewGroup)paramFragmentManagerImpl, transition1);
    } 
  }
  
  private static void replaceHide(Transition paramTransition, Fragment paramFragment, ArrayList<View> paramArrayList) {
    if (paramFragment != null && paramTransition != null && paramFragment.mAdded && paramFragment.mHidden && paramFragment.mHiddenChanged) {
      paramFragment.setHideReplaced(true);
      View view = paramFragment.getView();
      OneShotPreDrawListener.add((View)paramFragment.mContainer, new _$$Lambda$FragmentTransition$PZ32bJ_FSMpbzYzBl8x73NJPidQ(paramArrayList));
      paramTransition.addListener((Transition.TransitionListener)new Object(view, paramArrayList));
    } 
  }
  
  private static void scheduleTargetChange(ViewGroup paramViewGroup, Fragment paramFragment, View paramView, ArrayList<View> paramArrayList1, Transition paramTransition1, ArrayList<View> paramArrayList2, Transition paramTransition2, ArrayList<View> paramArrayList3) {
    OneShotPreDrawListener.add((View)paramViewGroup, new _$$Lambda$FragmentTransition$8Ei4ls5jlZcfRvuLcweFAxtFBFs(paramTransition1, paramView, paramFragment, paramArrayList1, paramArrayList2, paramArrayList3, paramTransition2));
  }
  
  private static TransitionSet getSharedElementTransition(Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean) {
    Transition transition1;
    if (paramFragment1 == null || paramFragment2 == null)
      return null; 
    if (paramBoolean) {
      transition1 = paramFragment2.getSharedElementReturnTransition();
    } else {
      transition1 = transition1.getSharedElementEnterTransition();
    } 
    Transition transition2 = cloneTransition(transition1);
    if (transition2 == null)
      return null; 
    TransitionSet transitionSet = new TransitionSet();
    transitionSet.addTransition(transition2);
    return transitionSet;
  }
  
  private static Transition getEnterTransition(Fragment paramFragment, boolean paramBoolean) {
    Transition transition;
    if (paramFragment == null)
      return null; 
    if (paramBoolean) {
      transition = paramFragment.getReenterTransition();
    } else {
      transition = transition.getEnterTransition();
    } 
    return cloneTransition(transition);
  }
  
  private static Transition getExitTransition(Fragment paramFragment, boolean paramBoolean) {
    Transition transition;
    if (paramFragment == null)
      return null; 
    if (paramBoolean) {
      transition = paramFragment.getReturnTransition();
    } else {
      transition = transition.getExitTransition();
    } 
    return cloneTransition(transition);
  }
  
  private static Transition cloneTransition(Transition paramTransition) {
    Transition transition = paramTransition;
    if (paramTransition != null)
      transition = paramTransition.clone(); 
    return transition;
  }
  
  private static TransitionSet configureSharedElementsReordered(ViewGroup paramViewGroup, View paramView, ArrayMap<String, String> paramArrayMap, FragmentContainerTransition paramFragmentContainerTransition, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2, Transition paramTransition1, Transition paramTransition2) {
    TransitionSet transitionSet1, transitionSet2;
    Fragment fragment1 = paramFragmentContainerTransition.lastIn;
    Fragment fragment2 = paramFragmentContainerTransition.firstOut;
    if (fragment1 != null)
      fragment1.getView().setVisibility(0); 
    if (fragment1 == null || fragment2 == null)
      return null; 
    boolean bool = paramFragmentContainerTransition.lastInIsPop;
    if (paramArrayMap.isEmpty()) {
      transitionSet2 = null;
    } else {
      transitionSet2 = getSharedElementTransition(fragment1, fragment2, bool);
    } 
    ArrayMap<String, View> arrayMap1 = captureOutSharedElements(paramArrayMap, transitionSet2, paramFragmentContainerTransition);
    ArrayMap<String, View> arrayMap2 = captureInSharedElements(paramArrayMap, transitionSet2, paramFragmentContainerTransition);
    if (paramArrayMap.isEmpty()) {
      if (arrayMap1 != null)
        arrayMap1.clear(); 
      if (arrayMap2 != null)
        arrayMap2.clear(); 
      paramArrayMap = null;
    } else {
      Set<String> set = paramArrayMap.keySet();
      addSharedElementsWithMatchingNames(paramArrayList1, arrayMap1, set);
      Collection<String> collection = paramArrayMap.values();
      addSharedElementsWithMatchingNames(paramArrayList2, arrayMap2, collection);
      transitionSet1 = transitionSet2;
    } 
    if (paramTransition1 == null && paramTransition2 == null && transitionSet1 == null)
      return null; 
    callSharedElementStartEnd(fragment1, fragment2, bool, arrayMap1, true);
    if (transitionSet1 != null) {
      paramArrayList2.add(paramView);
      setSharedElementTargets(transitionSet1, paramView, paramArrayList1);
      boolean bool1 = paramFragmentContainerTransition.firstOutIsPop;
      BackStackRecord backStackRecord = paramFragmentContainerTransition.firstOutTransaction;
      setOutEpicenter(transitionSet1, paramTransition2, arrayMap1, bool1, backStackRecord);
      Rect rect = new Rect();
      View view = getInEpicenterView(arrayMap2, paramFragmentContainerTransition, paramTransition1, bool);
      if (view != null)
        paramTransition1.setEpicenterCallback((Transition.EpicenterCallback)new Object(rect)); 
    } else {
      paramFragmentContainerTransition = null;
      paramView = null;
    } 
    OneShotPreDrawListener.add((View)paramViewGroup, new _$$Lambda$FragmentTransition$jurn0WXuKw3bRQ_2d5zCWdeZWuI(fragment1, fragment2, bool, arrayMap2, (View)paramFragmentContainerTransition, (Rect)paramView));
    return transitionSet1;
  }
  
  private static void addSharedElementsWithMatchingNames(ArrayList<View> paramArrayList, ArrayMap<String, View> paramArrayMap, Collection<String> paramCollection) {
    for (int i = paramArrayMap.size() - 1; i >= 0; i--) {
      View view = (View)paramArrayMap.valueAt(i);
      if (view != null && paramCollection.contains(view.getTransitionName()))
        paramArrayList.add(view); 
    } 
  }
  
  private static TransitionSet configureSharedElementsOrdered(ViewGroup paramViewGroup, View paramView, ArrayMap<String, String> paramArrayMap, FragmentContainerTransition paramFragmentContainerTransition, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2, Transition paramTransition1, Transition paramTransition2) {
    TransitionSet transitionSet;
    Fragment fragment1 = paramFragmentContainerTransition.lastIn;
    Fragment fragment2 = paramFragmentContainerTransition.firstOut;
    if (fragment1 == null || fragment2 == null)
      return null; 
    boolean bool = paramFragmentContainerTransition.lastInIsPop;
    if (paramArrayMap.isEmpty()) {
      transitionSet = null;
    } else {
      transitionSet = getSharedElementTransition(fragment1, fragment2, bool);
    } 
    ArrayMap<String, View> arrayMap = captureOutSharedElements(paramArrayMap, transitionSet, paramFragmentContainerTransition);
    if (paramArrayMap.isEmpty()) {
      transitionSet = null;
    } else {
      paramArrayList1.addAll(arrayMap.values());
    } 
    if (paramTransition1 == null && paramTransition2 == null && transitionSet == null)
      return null; 
    callSharedElementStartEnd(fragment1, fragment2, bool, arrayMap, true);
    if (transitionSet != null) {
      Rect rect2 = new Rect();
      setSharedElementTargets(transitionSet, paramView, paramArrayList1);
      boolean bool1 = paramFragmentContainerTransition.firstOutIsPop;
      BackStackRecord backStackRecord = paramFragmentContainerTransition.firstOutTransaction;
      setOutEpicenter(transitionSet, paramTransition2, arrayMap, bool1, backStackRecord);
      if (paramTransition1 != null)
        paramTransition1.setEpicenterCallback((Transition.EpicenterCallback)new Object(rect2)); 
      Rect rect1 = rect2;
    } else {
      paramTransition2 = null;
    } 
    OneShotPreDrawListener.add((View)paramViewGroup, new _$$Lambda$FragmentTransition$Ip0LktADPhG_3ouNBXgzufWpFfY(paramArrayMap, transitionSet, paramFragmentContainerTransition, paramArrayList2, paramView, fragment1, fragment2, bool, paramArrayList1, paramTransition1, (Rect)paramTransition2));
    return transitionSet;
  }
  
  private static ArrayMap<String, View> captureOutSharedElements(ArrayMap<String, String> paramArrayMap, TransitionSet paramTransitionSet, FragmentContainerTransition paramFragmentContainerTransition) {
    ArrayList<String> arrayList;
    SharedElementCallback sharedElementCallback;
    if (paramArrayMap.isEmpty() || paramTransitionSet == null) {
      paramArrayMap.clear();
      return null;
    } 
    Fragment fragment = paramFragmentContainerTransition.firstOut;
    ArrayMap<String, View> arrayMap = new ArrayMap();
    fragment.getView().findNamedViews((Map)arrayMap);
    BackStackRecord backStackRecord = paramFragmentContainerTransition.firstOutTransaction;
    if (paramFragmentContainerTransition.firstOutIsPop) {
      sharedElementCallback = fragment.getEnterTransitionCallback();
      arrayList = backStackRecord.mSharedElementTargetNames;
    } else {
      sharedElementCallback = fragment.getExitTransitionCallback();
      arrayList = ((BackStackRecord)arrayList).mSharedElementSourceNames;
    } 
    arrayMap.retainAll(arrayList);
    if (sharedElementCallback != null) {
      sharedElementCallback.onMapSharedElements(arrayList, (Map<String, View>)arrayMap);
      for (int i = arrayList.size() - 1; i >= 0; i--) {
        String str = arrayList.get(i);
        View view = (View)arrayMap.get(str);
        if (view == null) {
          paramArrayMap.remove(str);
        } else if (!str.equals(view.getTransitionName())) {
          str = (String)paramArrayMap.remove(str);
          paramArrayMap.put(view.getTransitionName(), str);
        } 
      } 
    } else {
      paramArrayMap.retainAll(arrayMap.keySet());
    } 
    return arrayMap;
  }
  
  private static ArrayMap<String, View> captureInSharedElements(ArrayMap<String, String> paramArrayMap, TransitionSet paramTransitionSet, FragmentContainerTransition paramFragmentContainerTransition) {
    ArrayList<String> arrayList;
    SharedElementCallback sharedElementCallback;
    Fragment fragment = paramFragmentContainerTransition.lastIn;
    View view = fragment.getView();
    if (paramArrayMap.isEmpty() || paramTransitionSet == null || view == null) {
      paramArrayMap.clear();
      return null;
    } 
    ArrayMap<String, View> arrayMap = new ArrayMap();
    view.findNamedViews((Map)arrayMap);
    BackStackRecord backStackRecord = paramFragmentContainerTransition.lastInTransaction;
    if (paramFragmentContainerTransition.lastInIsPop) {
      sharedElementCallback = fragment.getExitTransitionCallback();
      arrayList = backStackRecord.mSharedElementSourceNames;
    } else {
      sharedElementCallback = fragment.getEnterTransitionCallback();
      arrayList = ((BackStackRecord)arrayList).mSharedElementTargetNames;
    } 
    if (arrayList != null)
      arrayMap.retainAll(arrayList); 
    if (arrayList != null && sharedElementCallback != null) {
      sharedElementCallback.onMapSharedElements(arrayList, (Map<String, View>)arrayMap);
      for (int i = arrayList.size() - 1; i >= 0; i--) {
        String str1, str2 = arrayList.get(i);
        View view1 = (View)arrayMap.get(str2);
        if (view1 == null) {
          str1 = findKeyForValue(paramArrayMap, str2);
          if (str1 != null)
            paramArrayMap.remove(str1); 
        } else if (!str2.equals(str1.getTransitionName())) {
          str2 = findKeyForValue(paramArrayMap, str2);
          if (str2 != null)
            paramArrayMap.put(str2, str1.getTransitionName()); 
        } 
      } 
    } else {
      retainValues(paramArrayMap, arrayMap);
    } 
    return arrayMap;
  }
  
  private static String findKeyForValue(ArrayMap<String, String> paramArrayMap, String paramString) {
    int i = paramArrayMap.size();
    for (byte b = 0; b < i; b++) {
      if (paramString.equals(paramArrayMap.valueAt(b)))
        return (String)paramArrayMap.keyAt(b); 
    } 
    return null;
  }
  
  private static View getInEpicenterView(ArrayMap<String, View> paramArrayMap, FragmentContainerTransition paramFragmentContainerTransition, Transition paramTransition, boolean paramBoolean) {
    BackStackRecord backStackRecord = paramFragmentContainerTransition.lastInTransaction;
    if (paramTransition != null && paramArrayMap != null && backStackRecord.mSharedElementSourceNames != null) {
      ArrayList<String> arrayList = backStackRecord.mSharedElementSourceNames;
      if (!arrayList.isEmpty()) {
        String str;
        if (paramBoolean) {
          str = backStackRecord.mSharedElementSourceNames.get(0);
        } else {
          str = ((BackStackRecord)str).mSharedElementTargetNames.get(0);
        } 
        return (View)paramArrayMap.get(str);
      } 
    } 
    return null;
  }
  
  private static void setOutEpicenter(TransitionSet paramTransitionSet, Transition paramTransition, ArrayMap<String, View> paramArrayMap, boolean paramBoolean, BackStackRecord paramBackStackRecord) {
    if (paramBackStackRecord.mSharedElementSourceNames != null) {
      ArrayList<String> arrayList = paramBackStackRecord.mSharedElementSourceNames;
      if (!arrayList.isEmpty()) {
        String str;
        if (paramBoolean) {
          str = paramBackStackRecord.mSharedElementTargetNames.get(0);
        } else {
          str = ((BackStackRecord)str).mSharedElementSourceNames.get(0);
        } 
        View view = (View)paramArrayMap.get(str);
        setEpicenter((Transition)paramTransitionSet, view);
        if (paramTransition != null)
          setEpicenter(paramTransition, view); 
      } 
    } 
  }
  
  private static void setEpicenter(Transition paramTransition, View paramView) {
    if (paramView != null) {
      Rect rect = new Rect();
      paramView.getBoundsOnScreen(rect);
      paramTransition.setEpicenterCallback((Transition.EpicenterCallback)new Object(rect));
    } 
  }
  
  private static void retainValues(ArrayMap<String, String> paramArrayMap, ArrayMap<String, View> paramArrayMap1) {
    for (int i = paramArrayMap.size() - 1; i >= 0; i--) {
      String str = (String)paramArrayMap.valueAt(i);
      if (!paramArrayMap1.containsKey(str))
        paramArrayMap.removeAt(i); 
    } 
  }
  
  private static void callSharedElementStartEnd(Fragment paramFragment1, Fragment paramFragment2, boolean paramBoolean1, ArrayMap<String, View> paramArrayMap, boolean paramBoolean2) {
    SharedElementCallback sharedElementCallback;
    if (paramBoolean1) {
      sharedElementCallback = paramFragment2.getEnterTransitionCallback();
    } else {
      sharedElementCallback = sharedElementCallback.getEnterTransitionCallback();
    } 
    if (sharedElementCallback != null) {
      int i;
      ArrayList<View> arrayList1 = new ArrayList();
      ArrayList<String> arrayList = new ArrayList();
      if (paramArrayMap == null) {
        i = 0;
      } else {
        i = paramArrayMap.size();
      } 
      for (byte b = 0; b < i; b++) {
        arrayList.add((String)paramArrayMap.keyAt(b));
        arrayList1.add((View)paramArrayMap.valueAt(b));
      } 
      if (paramBoolean2) {
        sharedElementCallback.onSharedElementStart(arrayList, arrayList1, null);
      } else {
        sharedElementCallback.onSharedElementEnd(arrayList, arrayList1, null);
      } 
    } 
  }
  
  private static void setSharedElementTargets(TransitionSet paramTransitionSet, View paramView, ArrayList<View> paramArrayList) {
    List<View> list = paramTransitionSet.getTargets();
    list.clear();
    int i = paramArrayList.size();
    for (byte b = 0; b < i; b++) {
      View view = paramArrayList.get(b);
      bfsAddViewChildren(list, view);
    } 
    list.add(paramView);
    paramArrayList.add(paramView);
    addTargets((Transition)paramTransitionSet, paramArrayList);
  }
  
  private static void bfsAddViewChildren(List<View> paramList, View paramView) {
    int i = paramList.size();
    if (containedBeforeIndex(paramList, paramView, i))
      return; 
    paramList.add(paramView);
    for (int j = i; j < paramList.size(); j++) {
      paramView = paramList.get(j);
      if (paramView instanceof ViewGroup) {
        ViewGroup viewGroup = (ViewGroup)paramView;
        int k = viewGroup.getChildCount();
        for (byte b = 0; b < k; b++) {
          View view = viewGroup.getChildAt(b);
          if (!containedBeforeIndex(paramList, view, i))
            paramList.add(view); 
        } 
      } 
    } 
  }
  
  private static boolean containedBeforeIndex(List<View> paramList, View paramView, int paramInt) {
    for (byte b = 0; b < paramInt; b++) {
      if (paramList.get(b) == paramView)
        return true; 
    } 
    return false;
  }
  
  private static void scheduleRemoveTargets(Transition paramTransition1, Transition paramTransition2, ArrayList<View> paramArrayList1, Transition paramTransition3, ArrayList<View> paramArrayList2, TransitionSet paramTransitionSet, ArrayList<View> paramArrayList3) {
    paramTransition1.addListener((Transition.TransitionListener)new Object(paramTransition2, paramArrayList1, paramTransition3, paramArrayList2, paramTransitionSet, paramArrayList3));
  }
  
  public static void replaceTargets(Transition paramTransition, ArrayList<View> paramArrayList1, ArrayList<View> paramArrayList2) {
    if (paramTransition instanceof TransitionSet) {
      TransitionSet transitionSet = (TransitionSet)paramTransition;
      int i = transitionSet.getTransitionCount();
      for (byte b = 0; b < i; b++) {
        paramTransition = transitionSet.getTransitionAt(b);
        replaceTargets(paramTransition, paramArrayList1, paramArrayList2);
      } 
    } else if (!hasSimpleTarget(paramTransition)) {
      List list = paramTransition.getTargets();
      if (list != null && list.size() == paramArrayList1.size() && 
        list.containsAll(paramArrayList1)) {
        if (paramArrayList2 == null) {
          i = 0;
        } else {
          i = paramArrayList2.size();
        } 
        for (byte b = 0; b < i; b++)
          paramTransition.addTarget(paramArrayList2.get(b)); 
        for (int i = paramArrayList1.size() - 1; i >= 0; i--)
          paramTransition.removeTarget(paramArrayList1.get(i)); 
      } 
    } 
  }
  
  public static void addTargets(Transition paramTransition, ArrayList<View> paramArrayList) {
    TransitionSet transitionSet;
    if (paramTransition == null)
      return; 
    if (paramTransition instanceof TransitionSet) {
      transitionSet = (TransitionSet)paramTransition;
      int i = transitionSet.getTransitionCount();
      for (byte b = 0; b < i; b++) {
        Transition transition = transitionSet.getTransitionAt(b);
        addTargets(transition, paramArrayList);
      } 
    } else if (!hasSimpleTarget((Transition)transitionSet)) {
      List list = transitionSet.getTargets();
      if (isNullOrEmpty(list)) {
        int i = paramArrayList.size();
        for (byte b = 0; b < i; b++)
          transitionSet.addTarget(paramArrayList.get(b)); 
      } 
    } 
  }
  
  private static boolean hasSimpleTarget(Transition paramTransition) {
    return (!isNullOrEmpty(paramTransition.getTargetIds()) || 
      !isNullOrEmpty(paramTransition.getTargetNames()) || 
      !isNullOrEmpty(paramTransition.getTargetTypes()));
  }
  
  private static boolean isNullOrEmpty(List paramList) {
    return (paramList == null || paramList.isEmpty());
  }
  
  private static ArrayList<View> configureEnteringExitingViews(Transition paramTransition, Fragment paramFragment, ArrayList<View> paramArrayList, View paramView) {
    ArrayList<View> arrayList = null;
    if (paramTransition != null) {
      ArrayList<View> arrayList1 = new ArrayList();
      View view = paramFragment.getView();
      if (view != null)
        view.captureTransitioningViews(arrayList1); 
      if (paramArrayList != null)
        arrayList1.removeAll(paramArrayList); 
      arrayList = arrayList1;
      if (!arrayList1.isEmpty()) {
        arrayList1.add(paramView);
        addTargets(paramTransition, arrayList1);
        arrayList = arrayList1;
      } 
    } 
    return arrayList;
  }
  
  private static void setViewVisibility(ArrayList<View> paramArrayList, int paramInt) {
    if (paramArrayList == null)
      return; 
    for (int i = paramArrayList.size() - 1; i >= 0; i--) {
      View view = paramArrayList.get(i);
      view.setVisibility(paramInt);
    } 
  }
  
  private static Transition mergeTransitions(Transition paramTransition1, Transition paramTransition2, Transition paramTransition3, Fragment paramFragment, boolean paramBoolean) {
    TransitionSet transitionSet;
    boolean bool1 = true;
    boolean bool2 = bool1;
    if (paramTransition1 != null) {
      bool2 = bool1;
      if (paramTransition2 != null) {
        bool2 = bool1;
        if (paramFragment != null) {
          if (paramBoolean) {
            paramBoolean = paramFragment.getAllowReturnTransitionOverlap();
          } else {
            paramBoolean = paramFragment.getAllowEnterTransitionOverlap();
          } 
          bool2 = paramBoolean;
        } 
      } 
    } 
    if (bool2) {
      TransitionSet transitionSet1 = new TransitionSet();
      if (paramTransition1 != null)
        transitionSet1.addTransition(paramTransition1); 
      if (paramTransition2 != null)
        transitionSet1.addTransition(paramTransition2); 
      if (paramTransition3 != null)
        transitionSet1.addTransition(paramTransition3); 
      transitionSet = transitionSet1;
    } else {
      TransitionSet transitionSet1, transitionSet2;
      paramFragment = null;
      if (transitionSet != null && paramTransition1 != null) {
        transitionSet2 = new TransitionSet();
        transitionSet = transitionSet2.addTransition((Transition)transitionSet);
        transitionSet1 = transitionSet.addTransition(paramTransition1);
        transitionSet = transitionSet1.setOrdering(1);
      } else if (transitionSet == null) {
        transitionSet = transitionSet2;
        if (transitionSet1 != null)
          transitionSet = transitionSet1; 
      } 
      if (paramTransition3 != null) {
        transitionSet1 = new TransitionSet();
        if (transitionSet != null)
          transitionSet1.addTransition((Transition)transitionSet); 
        transitionSet1.addTransition(paramTransition3);
        transitionSet = transitionSet1;
      } 
    } 
    return (Transition)transitionSet;
  }
  
  public static void calculateFragments(BackStackRecord paramBackStackRecord, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean) {
    int i = paramBackStackRecord.mOps.size();
    for (byte b = 0; b < i; b++) {
      BackStackRecord.Op op = paramBackStackRecord.mOps.get(b);
      addToFirstInLastOut(paramBackStackRecord, op, paramSparseArray, false, paramBoolean);
    } 
  }
  
  public static void calculatePopFragments(BackStackRecord paramBackStackRecord, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean) {
    if (!paramBackStackRecord.mManager.mContainer.onHasView())
      return; 
    int i = paramBackStackRecord.mOps.size();
    for (; --i >= 0; i--) {
      BackStackRecord.Op op = paramBackStackRecord.mOps.get(i);
      addToFirstInLastOut(paramBackStackRecord, op, paramSparseArray, true, paramBoolean);
    } 
  }
  
  private static void addToFirstInLastOut(BackStackRecord paramBackStackRecord, BackStackRecord.Op paramOp, SparseArray<FragmentContainerTransition> paramSparseArray, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_1
    //   1: getfield fragment : Landroid/app/Fragment;
    //   4: astore #5
    //   6: aload #5
    //   8: ifnonnull -> 12
    //   11: return
    //   12: aload #5
    //   14: getfield mContainerId : I
    //   17: istore #6
    //   19: iload #6
    //   21: ifne -> 25
    //   24: return
    //   25: iload_3
    //   26: ifeq -> 42
    //   29: getstatic android/app/FragmentTransition.INVERSE_OPS : [I
    //   32: aload_1
    //   33: getfield cmd : I
    //   36: iaload
    //   37: istore #7
    //   39: goto -> 48
    //   42: aload_1
    //   43: getfield cmd : I
    //   46: istore #7
    //   48: iconst_0
    //   49: istore #8
    //   51: iconst_0
    //   52: istore #9
    //   54: iconst_0
    //   55: istore #10
    //   57: iconst_0
    //   58: istore #11
    //   60: iconst_0
    //   61: istore #12
    //   63: iconst_0
    //   64: istore #13
    //   66: iload #7
    //   68: iconst_1
    //   69: if_icmpeq -> 388
    //   72: iload #7
    //   74: iconst_3
    //   75: if_icmpeq -> 279
    //   78: iload #7
    //   80: iconst_4
    //   81: if_icmpeq -> 185
    //   84: iload #7
    //   86: iconst_5
    //   87: if_icmpeq -> 119
    //   90: iload #7
    //   92: bipush #6
    //   94: if_icmpeq -> 279
    //   97: iload #7
    //   99: bipush #7
    //   101: if_icmpeq -> 388
    //   104: iconst_0
    //   105: istore #14
    //   107: iconst_0
    //   108: istore #7
    //   110: iconst_0
    //   111: istore #11
    //   113: iconst_0
    //   114: istore #8
    //   116: goto -> 439
    //   119: iload #4
    //   121: ifeq -> 166
    //   124: iload #13
    //   126: istore #14
    //   128: aload #5
    //   130: getfield mHiddenChanged : Z
    //   133: ifeq -> 163
    //   136: iload #13
    //   138: istore #14
    //   140: aload #5
    //   142: getfield mHidden : Z
    //   145: ifne -> 163
    //   148: iload #13
    //   150: istore #14
    //   152: aload #5
    //   154: getfield mAdded : Z
    //   157: ifeq -> 163
    //   160: iconst_1
    //   161: istore #14
    //   163: goto -> 173
    //   166: aload #5
    //   168: getfield mHidden : Z
    //   171: istore #14
    //   173: iconst_0
    //   174: istore #7
    //   176: iconst_0
    //   177: istore #11
    //   179: iconst_1
    //   180: istore #8
    //   182: goto -> 439
    //   185: iload #4
    //   187: ifeq -> 232
    //   190: iload #8
    //   192: istore #7
    //   194: aload #5
    //   196: getfield mHiddenChanged : Z
    //   199: ifeq -> 229
    //   202: iload #8
    //   204: istore #7
    //   206: aload #5
    //   208: getfield mAdded : Z
    //   211: ifeq -> 229
    //   214: iload #8
    //   216: istore #7
    //   218: aload #5
    //   220: getfield mHidden : Z
    //   223: ifeq -> 229
    //   226: iconst_1
    //   227: istore #7
    //   229: goto -> 259
    //   232: iload #9
    //   234: istore #7
    //   236: aload #5
    //   238: getfield mAdded : Z
    //   241: ifeq -> 259
    //   244: iload #9
    //   246: istore #7
    //   248: aload #5
    //   250: getfield mHidden : Z
    //   253: ifne -> 259
    //   256: iconst_1
    //   257: istore #7
    //   259: iconst_0
    //   260: istore #14
    //   262: iconst_1
    //   263: istore #9
    //   265: iload #7
    //   267: istore #11
    //   269: iconst_0
    //   270: istore #8
    //   272: iload #9
    //   274: istore #7
    //   276: goto -> 439
    //   279: iload #4
    //   281: ifeq -> 341
    //   284: aload #5
    //   286: getfield mAdded : Z
    //   289: ifne -> 334
    //   292: aload #5
    //   294: getfield mView : Landroid/view/View;
    //   297: ifnull -> 334
    //   300: aload #5
    //   302: getfield mView : Landroid/view/View;
    //   305: astore_1
    //   306: aload_1
    //   307: invokevirtual getVisibility : ()I
    //   310: ifne -> 334
    //   313: aload #5
    //   315: getfield mView : Landroid/view/View;
    //   318: astore_1
    //   319: aload_1
    //   320: invokevirtual getTransitionAlpha : ()F
    //   323: fconst_0
    //   324: fcmpl
    //   325: ifle -> 334
    //   328: iconst_1
    //   329: istore #7
    //   331: goto -> 338
    //   334: iload #10
    //   336: istore #7
    //   338: goto -> 368
    //   341: iload #11
    //   343: istore #7
    //   345: aload #5
    //   347: getfield mAdded : Z
    //   350: ifeq -> 368
    //   353: iload #11
    //   355: istore #7
    //   357: aload #5
    //   359: getfield mHidden : Z
    //   362: ifne -> 368
    //   365: iconst_1
    //   366: istore #7
    //   368: iconst_0
    //   369: istore #14
    //   371: iconst_1
    //   372: istore #9
    //   374: iload #7
    //   376: istore #11
    //   378: iconst_0
    //   379: istore #8
    //   381: iload #9
    //   383: istore #7
    //   385: goto -> 439
    //   388: iload #4
    //   390: ifeq -> 403
    //   393: aload #5
    //   395: getfield mIsNewlyAdded : Z
    //   398: istore #14
    //   400: goto -> 430
    //   403: iload #12
    //   405: istore #14
    //   407: aload #5
    //   409: getfield mAdded : Z
    //   412: ifne -> 430
    //   415: iload #12
    //   417: istore #14
    //   419: aload #5
    //   421: getfield mHidden : Z
    //   424: ifne -> 430
    //   427: iconst_1
    //   428: istore #14
    //   430: iconst_0
    //   431: istore #7
    //   433: iconst_0
    //   434: istore #11
    //   436: iconst_1
    //   437: istore #8
    //   439: aload_2
    //   440: iload #6
    //   442: invokevirtual get : (I)Ljava/lang/Object;
    //   445: checkcast android/app/FragmentTransition$FragmentContainerTransition
    //   448: astore_1
    //   449: iload #14
    //   451: ifeq -> 481
    //   454: aload_1
    //   455: aload_2
    //   456: iload #6
    //   458: invokestatic ensureContainer : (Landroid/app/FragmentTransition$FragmentContainerTransition;Landroid/util/SparseArray;I)Landroid/app/FragmentTransition$FragmentContainerTransition;
    //   461: astore_1
    //   462: aload_1
    //   463: aload #5
    //   465: putfield lastIn : Landroid/app/Fragment;
    //   468: aload_1
    //   469: iload_3
    //   470: putfield lastInIsPop : Z
    //   473: aload_1
    //   474: aload_0
    //   475: putfield lastInTransaction : Landroid/app/BackStackRecord;
    //   478: goto -> 481
    //   481: iload #4
    //   483: ifne -> 590
    //   486: iload #8
    //   488: ifeq -> 590
    //   491: aload_1
    //   492: ifnull -> 509
    //   495: aload_1
    //   496: getfield firstOut : Landroid/app/Fragment;
    //   499: aload #5
    //   501: if_acmpne -> 509
    //   504: aload_1
    //   505: aconst_null
    //   506: putfield firstOut : Landroid/app/Fragment;
    //   509: aload_0
    //   510: getfield mManager : Landroid/app/FragmentManagerImpl;
    //   513: astore #15
    //   515: aload #5
    //   517: getfield mState : I
    //   520: iconst_1
    //   521: if_icmpge -> 587
    //   524: aload #15
    //   526: getfield mCurState : I
    //   529: iconst_1
    //   530: if_icmplt -> 587
    //   533: aload #15
    //   535: getfield mHost : Landroid/app/FragmentHostCallback;
    //   538: astore #16
    //   540: aload #16
    //   542: invokevirtual getContext : ()Landroid/content/Context;
    //   545: invokevirtual getApplicationInfo : ()Landroid/content/pm/ApplicationInfo;
    //   548: getfield targetSdkVersion : I
    //   551: bipush #24
    //   553: if_icmplt -> 584
    //   556: aload_0
    //   557: getfield mReorderingAllowed : Z
    //   560: ifne -> 584
    //   563: aload #15
    //   565: aload #5
    //   567: invokevirtual makeActive : (Landroid/app/Fragment;)V
    //   570: aload #15
    //   572: aload #5
    //   574: iconst_1
    //   575: iconst_0
    //   576: iconst_0
    //   577: iconst_0
    //   578: invokevirtual moveToState : (Landroid/app/Fragment;IIIZ)V
    //   581: goto -> 590
    //   584: goto -> 590
    //   587: goto -> 590
    //   590: iload #11
    //   592: ifeq -> 635
    //   595: aload_1
    //   596: ifnull -> 606
    //   599: aload_1
    //   600: getfield firstOut : Landroid/app/Fragment;
    //   603: ifnonnull -> 635
    //   606: aload_1
    //   607: aload_2
    //   608: iload #6
    //   610: invokestatic ensureContainer : (Landroid/app/FragmentTransition$FragmentContainerTransition;Landroid/util/SparseArray;I)Landroid/app/FragmentTransition$FragmentContainerTransition;
    //   613: astore_1
    //   614: aload_1
    //   615: aload #5
    //   617: putfield firstOut : Landroid/app/Fragment;
    //   620: aload_1
    //   621: iload_3
    //   622: putfield firstOutIsPop : Z
    //   625: aload_1
    //   626: aload_0
    //   627: putfield firstOutTransaction : Landroid/app/BackStackRecord;
    //   630: aload_1
    //   631: astore_0
    //   632: goto -> 637
    //   635: aload_1
    //   636: astore_0
    //   637: iload #4
    //   639: ifne -> 665
    //   642: iload #7
    //   644: ifeq -> 665
    //   647: aload_0
    //   648: ifnull -> 665
    //   651: aload_0
    //   652: getfield lastIn : Landroid/app/Fragment;
    //   655: aload #5
    //   657: if_acmpne -> 665
    //   660: aload_0
    //   661: aconst_null
    //   662: putfield lastIn : Landroid/app/Fragment;
    //   665: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1249	-> 0
    //   #1250	-> 6
    //   #1251	-> 11
    //   #1253	-> 12
    //   #1254	-> 19
    //   #1255	-> 24
    //   #1257	-> 25
    //   #1258	-> 48
    //   #1259	-> 48
    //   #1260	-> 48
    //   #1261	-> 48
    //   #1262	-> 48
    //   #1264	-> 119
    //   #1265	-> 124
    //   #1268	-> 166
    //   #1270	-> 173
    //   #1271	-> 173
    //   #1282	-> 185
    //   #1283	-> 190
    //   #1286	-> 232
    //   #1288	-> 259
    //   #1289	-> 259
    //   #1292	-> 279
    //   #1293	-> 284
    //   #1294	-> 306
    //   #1295	-> 319
    //   #1297	-> 341
    //   #1299	-> 368
    //   #1274	-> 388
    //   #1275	-> 393
    //   #1277	-> 403
    //   #1279	-> 430
    //   #1280	-> 430
    //   #1302	-> 439
    //   #1303	-> 449
    //   #1304	-> 454
    //   #1305	-> 454
    //   #1306	-> 462
    //   #1307	-> 468
    //   #1308	-> 473
    //   #1303	-> 481
    //   #1310	-> 481
    //   #1311	-> 491
    //   #1312	-> 504
    //   #1319	-> 509
    //   #1320	-> 515
    //   #1321	-> 540
    //   #1323	-> 563
    //   #1324	-> 570
    //   #1321	-> 584
    //   #1320	-> 587
    //   #1310	-> 590
    //   #1327	-> 590
    //   #1328	-> 606
    //   #1329	-> 606
    //   #1330	-> 614
    //   #1331	-> 620
    //   #1332	-> 625
    //   #1327	-> 635
    //   #1335	-> 635
    //   #1337	-> 660
    //   #1339	-> 665
  }
  
  private static FragmentContainerTransition ensureContainer(FragmentContainerTransition paramFragmentContainerTransition, SparseArray<FragmentContainerTransition> paramSparseArray, int paramInt) {
    FragmentContainerTransition fragmentContainerTransition = paramFragmentContainerTransition;
    if (paramFragmentContainerTransition == null) {
      fragmentContainerTransition = new FragmentContainerTransition();
      paramSparseArray.put(paramInt, fragmentContainerTransition);
    } 
    return fragmentContainerTransition;
  }
  
  public static class FragmentContainerTransition {
    public Fragment firstOut;
    
    public boolean firstOutIsPop;
    
    public BackStackRecord firstOutTransaction;
    
    public Fragment lastIn;
    
    public boolean lastInIsPop;
    
    public BackStackRecord lastInTransaction;
  }
}
