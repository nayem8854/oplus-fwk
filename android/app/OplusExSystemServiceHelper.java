package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

public class OplusExSystemServiceHelper {
  public static final String COREAPPFEATURE_SYSTEM_SERVICE_CLASSNAME = "com.oplus.customize.coreapp.service.OplusCoreAppFeatureService";
  
  public static final String EX_SYSTEM_SERVICE_CLASSNAME = "com.coloros.exsystemservice.ColorSharedSystemService";
  
  public static final String EX_SYSTEM_SERVICE_PKGNAME = "com.coloros.exsystemservice";
  
  private static final String TAG = OplusExSystemServiceHelper.class.getSimpleName();
  
  private static volatile OplusExSystemServiceHelper sIntance;
  
  public static OplusExSystemServiceHelper getInstance() {
    // Byte code:
    //   0: getstatic android/app/OplusExSystemServiceHelper.sIntance : Landroid/app/OplusExSystemServiceHelper;
    //   3: ifnonnull -> 39
    //   6: ldc android/app/OplusExSystemServiceHelper
    //   8: monitorenter
    //   9: getstatic android/app/OplusExSystemServiceHelper.sIntance : Landroid/app/OplusExSystemServiceHelper;
    //   12: ifnonnull -> 27
    //   15: new android/app/OplusExSystemServiceHelper
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/app/OplusExSystemServiceHelper.sIntance : Landroid/app/OplusExSystemServiceHelper;
    //   27: ldc android/app/OplusExSystemServiceHelper
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/app/OplusExSystemServiceHelper
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/app/OplusExSystemServiceHelper.sIntance : Landroid/app/OplusExSystemServiceHelper;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #22	-> 0
    //   #23	-> 6
    //   #24	-> 9
    //   #25	-> 15
    //   #27	-> 27
    //   #29	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public boolean checkOplusExSystemService(boolean paramBoolean, String paramString) {
    boolean bool1 = false;
    if (TextUtils.isEmpty(paramString)) {
      Log.w(TAG, "checkOplusExSystemService className is null or empty str");
      return false;
    } 
    boolean bool2 = bool1;
    if (paramBoolean)
      try {
        if (!"com.coloros.exsystemservice.ColorSharedSystemService".equals(paramString)) {
          paramBoolean = "com.oplus.customize.coreapp.service.OplusCoreAppFeatureService".equals(paramString);
          bool2 = bool1;
          if (paramBoolean)
            bool2 = true; 
          return bool2;
        } 
        bool2 = true;
      } catch (Exception exception) {
        bool2 = false;
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("checkOplusExSystemService e = ");
        stringBuilder.append(exception);
        Log.e(str, stringBuilder.toString());
      }  
    return bool2;
  }
  
  public boolean checkOplusExSystemService(boolean paramBoolean, Intent paramIntent) {
    if (paramIntent == null) {
      Log.w(TAG, "checkOplusExSystemService intent is null");
      return false;
    } 
    if (paramIntent.getComponent() == null) {
      Log.w(TAG, "checkOplusExSystemService intent getComponent is null");
      return false;
    } 
    return checkOplusExSystemService(paramBoolean, paramIntent.getComponent().getClassName());
  }
  
  public boolean checkOplusExSystemService(Handler paramHandler, String paramString) {
    return checkOplusExSystemService(true, paramString);
  }
  
  public ComponentName getComponentName() {
    return new ComponentName("com.coloros.exsystemservice", "com.coloros.exsystemservice.ColorSharedSystemService");
  }
}
