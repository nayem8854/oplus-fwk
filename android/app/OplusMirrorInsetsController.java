package android.app;

import android.view.InsetsController;
import com.oplus.reflect.RefBoolean;
import com.oplus.reflect.RefClass;

public class OplusMirrorInsetsController {
  public static RefBoolean DEBUG;
  
  public static Class<?> TYPE = RefClass.load(OplusMirrorInsetsController.class, InsetsController.class);
  
  public static void setBooleanValue(RefBoolean paramRefBoolean, boolean paramBoolean) {
    if (paramRefBoolean != null)
      paramRefBoolean.set(null, paramBoolean); 
  }
}
