package android.app;

import android.content.Intent;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public class ResultInfo implements Parcelable {
  public ResultInfo(String paramString, int paramInt1, int paramInt2, Intent paramIntent) {
    this.mResultWho = paramString;
    this.mRequestCode = paramInt1;
    this.mResultCode = paramInt2;
    this.mData = paramIntent;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ResultInfo{who=");
    stringBuilder.append(this.mResultWho);
    stringBuilder.append(", request=");
    stringBuilder.append(this.mRequestCode);
    stringBuilder.append(", result=");
    stringBuilder.append(this.mResultCode);
    stringBuilder.append(", data=");
    stringBuilder.append(this.mData);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mResultWho);
    paramParcel.writeInt(this.mRequestCode);
    paramParcel.writeInt(this.mResultCode);
    if (this.mData != null) {
      paramParcel.writeInt(1);
      this.mData.writeToParcel(paramParcel, 0);
    } else {
      paramParcel.writeInt(0);
    } 
  }
  
  public static final Parcelable.Creator<ResultInfo> CREATOR = new Parcelable.Creator<ResultInfo>() {
      public ResultInfo createFromParcel(Parcel param1Parcel) {
        return new ResultInfo(param1Parcel);
      }
      
      public ResultInfo[] newArray(int param1Int) {
        return new ResultInfo[param1Int];
      }
    };
  
  public final Intent mData;
  
  public final int mRequestCode;
  
  public final int mResultCode;
  
  public final String mResultWho;
  
  public ResultInfo(Parcel paramParcel) {
    this.mResultWho = paramParcel.readString();
    this.mRequestCode = paramParcel.readInt();
    this.mResultCode = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      this.mData = (Intent)Intent.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mData = null;
    } 
  }
  
  public boolean equals(Object paramObject) {
    boolean bool;
    boolean bool1 = false;
    if (paramObject == null || !(paramObject instanceof ResultInfo))
      return false; 
    ResultInfo resultInfo = (ResultInfo)paramObject;
    paramObject = this.mData;
    if (paramObject == null) {
      if (resultInfo.mData == null) {
        bool = true;
      } else {
        bool = false;
      } 
    } else {
      bool = paramObject.filterEquals(resultInfo.mData);
    } 
    boolean bool2 = bool1;
    if (bool) {
      bool2 = bool1;
      if (Objects.equals(this.mResultWho, resultInfo.mResultWho)) {
        bool2 = bool1;
        if (this.mResultCode == resultInfo.mResultCode) {
          bool2 = bool1;
          if (this.mRequestCode == resultInfo.mRequestCode)
            bool2 = true; 
        } 
      } 
    } 
    return bool2;
  }
  
  public int hashCode() {
    int i = this.mRequestCode;
    int j = this.mResultCode;
    i = ((17 * 31 + i) * 31 + j) * 31 + Objects.hashCode(this.mResultWho);
    Intent intent = this.mData;
    j = i;
    if (intent != null)
      j = i * 31 + intent.filterHashCode(); 
    return j;
  }
}
