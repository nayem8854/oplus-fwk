package android.app;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import java.util.List;

public abstract class HomeVisibilityObserver {
  private ActivityManager mActivityManager;
  
  private Context mContext;
  
  boolean mIsHomeActivityVisible;
  
  void init(Context paramContext, ActivityManager paramActivityManager) {
    this.mContext = paramContext;
    this.mActivityManager = paramActivityManager;
    this.mIsHomeActivityVisible = isHomeActivityVisible();
  }
  
  IProcessObserver.Stub mObserver = (IProcessObserver.Stub)new Object(this);
  
  private boolean isHomeActivityVisible() {
    List<ActivityManager.RunningTaskInfo> list = this.mActivityManager.getRunningTasks(1);
    if (list == null || list.isEmpty())
      return false; 
    String str = ((ActivityManager.RunningTaskInfo)list.get(0)).topActivity.getPackageName();
    if (str == null)
      return false; 
    Intent intent = new Intent("android.intent.action.MAIN", null);
    intent.addCategory("android.intent.category.HOME");
    ResolveInfo resolveInfo = this.mContext.getPackageManager().resolveActivity(intent, 65536);
    if (resolveInfo != null && str.equals(resolveInfo.activityInfo.packageName))
      return true; 
    return false;
  }
  
  public abstract void onHomeVisibilityChanged(boolean paramBoolean);
}
