package android.app;

import android.os.Environment;
import android.os.OplusBaseEnvironment;
import android.os.SystemProperties;
import java.io.File;
import java.lang.reflect.Method;

public class OplusUxIconConstants {
  public static final boolean DEBUG_UX_ICON = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final boolean DEBUG_UX_ICON_TRACE = true;
  
  public static final float DEFAULT_RELATIVE_BRIGHTNESS = 0.84F;
  
  public static class IconLoader {
    public static final String BASE_PRODUCT_DEFAULT_THEME_FILE_PATH;
    
    public static final String BASE_SYSTEM_DEFAULT_THEME_FILE_PATH = "/system/media/theme/default/";
    
    public static final String BASE_UX_ICONS_FILE_PATH = "/data/oppo/uxicons/";
    
    public static final String COM_ANDROID_CONTACTS = "com.android.contacts";
    
    public static final String COM_HEYTAP_MATKET = "com.heytap.market";
    
    public static final String DEFAULT_BACKGROUND_COLOR = "#FFFBFBFB";
    
    public static final String DIALER_PREFIX = "dialer_";
    
    public static final String FILE_SEPARATOR = "/";
    
    public static final int ICON_SIZE_THRESHOLD = 40;
    
    public static final float MATERIAL_FOREGROUND_SCALE = 1.25F;
    
    public static final String MY_CARRIER_DEFAULT_THEME_FILE_PATH;
    
    public static final String MY_CARRIER_ROOT_PATH;
    
    public static final String MY_COUNTRY_DEFAULT_THEME_FILE_PATH;
    
    public static final String MY_COUNTRY_ROOT_PATH;
    
    public static final String MY_PRODUCT_ROOT_PATH;
    
    public static final int PIXEL_ALPHA_THRESHOLD = 220;
    
    public static final int PIXEL_SAMPLE = 4;
    
    public static final int PIXEL_THRESHOLD = 6;
    
    public static final String PNG_REG = ".png";
    
    public static final int TRANSPARENT_ICON_FG_SIZE_DP = 34;
    
    static {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getMyProductDirectory());
      stringBuilder.append("/media/theme/default/");
      BASE_PRODUCT_DEFAULT_THEME_FILE_PATH = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append(OplusBaseEnvironment.getMyProductDirectory().getAbsolutePath());
      stringBuilder.append("");
      MY_PRODUCT_ROOT_PATH = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append(OplusBaseEnvironment.getMyRegionDirectory().getAbsolutePath());
      stringBuilder.append("");
      MY_COUNTRY_ROOT_PATH = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append("");
      stringBuilder.append(OplusBaseEnvironment.getMyCarrierDirectory().getAbsolutePath());
      stringBuilder.append("");
      MY_CARRIER_ROOT_PATH = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(getMyCountryDirectory());
      stringBuilder.append("/media/theme/uxicons/");
      MY_COUNTRY_DEFAULT_THEME_FILE_PATH = stringBuilder.toString();
      stringBuilder = new StringBuilder();
      stringBuilder.append(getMyCarrierDirectory());
      stringBuilder.append("/media/theme/uxicons/");
      MY_CARRIER_DEFAULT_THEME_FILE_PATH = stringBuilder.toString();
    }
    
    static String getMyProductDirectory() {
      try {
        Method method = Environment.class.getMethod("getMyProductDirectory", new Class[0]);
        method.setAccessible(true);
        Object object = method.invoke(null, new Object[0]);
        if (object != null)
          return ((File)object).getAbsolutePath(); 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return MY_PRODUCT_ROOT_PATH;
    }
    
    static String getMyCountryDirectory() {
      try {
        Method method = Environment.class.getMethod("getMyCountryDirectory", new Class[0]);
        method.setAccessible(true);
        Object object = method.invoke(null, new Object[0]);
        if (object != null)
          return ((File)object).getAbsolutePath(); 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return MY_COUNTRY_ROOT_PATH;
    }
    
    static String getMyCarrierDirectory() {
      try {
        Method method = Environment.class.getMethod("getMyCarrierDirectory", new Class[0]);
        method.setAccessible(true);
        Object object = method.invoke(null, new Object[0]);
        if (object != null)
          return ((File)object).getAbsolutePath(); 
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
      return MY_CARRIER_ROOT_PATH;
    }
    
    public static String getDensityName(int param1Int) {
      String str;
      if (param1Int > 480) {
        str = "xxxhdpi";
      } else if (param1Int > 320) {
        str = "xxhdpi";
      } else {
        str = "xhdpi";
      } 
      return str;
    }
  }
  
  public static class IconTheme {
    public static final int ART_PLUS_BIT_LENGTH = 4;
    
    public static final int DARKMODE_ICON_BIT_LENGTH = 1;
    
    public static final int DARKMODE_ICON_TRANSLATE_BIT_LENGTH = 61;
    
    public static final int FOREGROUND_SIZE_BIT_LENGTH = 16;
    
    public static final int FOREIGN_BIT_LENGTH = 4;
    
    public static final int ICON_RADIUS_BIT_LENGTH = 12;
    
    public static final int ICON_RADIUS_PX_BIT_LENGTH = 1;
    
    public static final int ICON_SHAPE_BIT_LENGTH = 4;
    
    public static final int ICON_SHAPE_CUSTOM_SQUARE = 0;
    
    public static final int ICON_SHAPE_DESIGNED_LEAF = 2;
    
    public static final int ICON_SHAPE_DESIGNED_OCTAGON = 1;
    
    public static final int ICON_SHAPE_DESIGNED_STICKER = 3;
    
    public static final int ICON_SIZE_BIT_LENGTH = 16;
    
    public static final String OPLUS_OFF_UXICON_META_DATA = "com.coloros.off_uxicon";
    
    public static final String OPLUS_UXIOCN_META_DATA = "com.coloros.support_uxonline";
    
    public static final int PREFIX_MATERIAL_POS = 2;
    
    public static final int PREFIX_PEBBLE_POS = 3;
    
    public static final int PREFIX_RECTANGLE_BG_POS = 1;
    
    public static final int PREFIX_RECTANGLE_FG_POS = 0;
    
    public static final int THEME_BIT_LENGTH = 4;
    
    public static final int THEME_CUSTOM = 3;
    
    public static final int THEME_ICON_PACK = 5;
    
    public static final int THEME_MATERIAL = 1;
    
    public static final int THEME_MATERIAL_POS = 1;
    
    public static final int THEME_MATERIAL_RADIUS_PX = 8;
    
    public static final int THEME_PEBBLE = 4;
    
    public static final int THEME_PEBBLE_POS = 2;
    
    public static final int THEME_RECTANGLE = 2;
    
    public static final int THEME_RECTANGLE_POS = 0;
  }
  
  public static class SystemProperty {
    public static final String FEATURE_OPLUS_VERSION_EXP = "oppo.version.exp";
    
    public static final String FEATURE_UX_ICON_DISABLE = "oppo.uxicons.disable.uxicons";
    
    public static final String FEATURE_UX_ICON_MATERIAL = "com.oppo.feature.uxicon.theme.material";
    
    public static final String FEATURE_UX_ICON_PEBBLE = "com.oppo.feature.uxicon.theme.pebble";
    
    public static final String KEY_THEME_FLAG = "persist.sys.themeflag";
    
    public static final String KEY_UX_ICON_CONFIG = "key_ux_icon_config";
    
    public static final String KEY_UX_ICON_THEME_FLAG = "persist.sys.themeflag.uxicon";
  }
}
