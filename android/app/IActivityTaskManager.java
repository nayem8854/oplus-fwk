package android.app;

import android.app.assist.AssistContent;
import android.app.assist.AssistStructure;
import android.content.ComponentName;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.pm.ConfigurationInfo;
import android.content.pm.ParceledListSlice;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.service.voice.IVoiceInteractionSession;
import android.text.TextUtils;
import android.view.IRecentsAnimationRunner;
import android.view.RemoteAnimationAdapter;
import android.view.RemoteAnimationDefinition;
import android.window.IWindowOrganizerController;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.policy.IKeyguardDismissCallback;
import java.util.ArrayList;
import java.util.List;

public interface IActivityTaskManager extends IInterface {
  void activityDestroyed(IBinder paramIBinder) throws RemoteException;
  
  void activityIdle(IBinder paramIBinder, Configuration paramConfiguration, boolean paramBoolean) throws RemoteException;
  
  void activityPaused(IBinder paramIBinder) throws RemoteException;
  
  void activityRelaunched(IBinder paramIBinder) throws RemoteException;
  
  void activityResumed(IBinder paramIBinder) throws RemoteException;
  
  void activityStopped(IBinder paramIBinder, Bundle paramBundle, PersistableBundle paramPersistableBundle, CharSequence paramCharSequence) throws RemoteException;
  
  void activityTopResumedStateLost() throws RemoteException;
  
  int addAppTask(IBinder paramIBinder, Intent paramIntent, ActivityManager.TaskDescription paramTaskDescription, Bitmap paramBitmap) throws RemoteException;
  
  void alwaysShowUnsupportedCompileSdkWarning(ComponentName paramComponentName) throws RemoteException;
  
  void cancelRecentsAnimation(boolean paramBoolean) throws RemoteException;
  
  void cancelTaskWindowTransition(int paramInt) throws RemoteException;
  
  void clearLaunchParamsForPackages(List<String> paramList) throws RemoteException;
  
  boolean convertFromTranslucent(IBinder paramIBinder) throws RemoteException;
  
  boolean convertToTranslucent(IBinder paramIBinder, Bundle paramBundle) throws RemoteException;
  
  void dismissKeyguard(IBinder paramIBinder, IKeyguardDismissCallback paramIKeyguardDismissCallback, CharSequence paramCharSequence) throws RemoteException;
  
  boolean enterPictureInPictureMode(IBinder paramIBinder, PictureInPictureParams paramPictureInPictureParams) throws RemoteException;
  
  boolean finishActivity(IBinder paramIBinder, int paramInt1, Intent paramIntent, int paramInt2) throws RemoteException;
  
  boolean finishActivityAffinity(IBinder paramIBinder) throws RemoteException;
  
  void finishSubActivity(IBinder paramIBinder, String paramString, int paramInt) throws RemoteException;
  
  void finishVoiceTask(IVoiceInteractionSession paramIVoiceInteractionSession) throws RemoteException;
  
  ComponentName getActivityClassForToken(IBinder paramIBinder) throws RemoteException;
  
  Bundle getActivityOptions(IBinder paramIBinder) throws RemoteException;
  
  List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException;
  
  List<ActivityManager.StackInfo> getAllStackInfosOnDisplay(int paramInt) throws RemoteException;
  
  Point getAppTaskThumbnailSize() throws RemoteException;
  
  List<IBinder> getAppTasks(String paramString) throws RemoteException;
  
  Bundle getAssistContextExtras(int paramInt) throws RemoteException;
  
  ComponentName getCallingActivity(IBinder paramIBinder) throws RemoteException;
  
  String getCallingPackage(IBinder paramIBinder) throws RemoteException;
  
  ConfigurationInfo getDeviceConfigurationInfo() throws RemoteException;
  
  int getDisplayId(IBinder paramIBinder) throws RemoteException;
  
  List<ActivityManager.RunningTaskInfo> getFilteredTasks(int paramInt, boolean paramBoolean) throws RemoteException;
  
  ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException;
  
  int getFrontActivityScreenCompatMode() throws RemoteException;
  
  int getLastResumedActivityUserId() throws RemoteException;
  
  String getLaunchedFromPackage(IBinder paramIBinder) throws RemoteException;
  
  int getLaunchedFromUid(IBinder paramIBinder) throws RemoteException;
  
  int getLockTaskModeState() throws RemoteException;
  
  int getMaxNumPictureInPictureActions(IBinder paramIBinder) throws RemoteException;
  
  boolean getPackageAskScreenCompat(String paramString) throws RemoteException;
  
  String getPackageForToken(IBinder paramIBinder) throws RemoteException;
  
  int getPackageScreenCompatMode(String paramString) throws RemoteException;
  
  ParceledListSlice getRecentTasks(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  int getRequestedOrientation(IBinder paramIBinder) throws RemoteException;
  
  ActivityManager.StackInfo getStackInfo(int paramInt1, int paramInt2) throws RemoteException;
  
  ActivityManager.StackInfo getStackInfoOnDisplay(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  Rect getTaskBounds(int paramInt) throws RemoteException;
  
  ActivityManager.TaskDescription getTaskDescription(int paramInt) throws RemoteException;
  
  Bitmap getTaskDescriptionIcon(String paramString, int paramInt) throws RemoteException;
  
  int getTaskForActivity(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  ActivityManager.TaskSnapshot getTaskSnapshot(int paramInt, boolean paramBoolean) throws RemoteException;
  
  List<ActivityManager.RunningTaskInfo> getTasks(int paramInt) throws RemoteException;
  
  IBinder getUriPermissionOwnerForActivity(IBinder paramIBinder) throws RemoteException;
  
  IWindowOrganizerController getWindowOrganizerController() throws RemoteException;
  
  void invalidateHomeTaskSnapshot(IBinder paramIBinder) throws RemoteException;
  
  boolean isActivityStartAllowedOnDisplay(int paramInt1, Intent paramIntent, String paramString, int paramInt2) throws RemoteException;
  
  boolean isAssistDataAllowedOnCurrentActivity() throws RemoteException;
  
  boolean isImmersive(IBinder paramIBinder) throws RemoteException;
  
  boolean isInLockTaskMode() throws RemoteException;
  
  boolean isRootVoiceInteraction(IBinder paramIBinder) throws RemoteException;
  
  boolean isTopActivityImmersive() throws RemoteException;
  
  boolean isTopOfTask(IBinder paramIBinder) throws RemoteException;
  
  void keyguardGoingAway(int paramInt) throws RemoteException;
  
  boolean launchAssistIntent(Intent paramIntent, int paramInt1, String paramString, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  boolean moveActivityTaskToBack(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void moveStackToDisplay(int paramInt1, int paramInt2) throws RemoteException;
  
  void moveTaskToFront(IApplicationThread paramIApplicationThread, String paramString, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void moveTaskToStack(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  boolean moveTopActivityToPinnedStack(int paramInt, Rect paramRect) throws RemoteException;
  
  boolean navigateUpTo(IBinder paramIBinder, Intent paramIntent1, int paramInt, Intent paramIntent2) throws RemoteException;
  
  void notifyActivityDrawn(IBinder paramIBinder) throws RemoteException;
  
  void notifyEnterAnimationComplete(IBinder paramIBinder) throws RemoteException;
  
  void notifyLaunchTaskBehindComplete(IBinder paramIBinder) throws RemoteException;
  
  void onBackPressedOnTaskRoot(IBinder paramIBinder, IRequestFinishCallback paramIRequestFinishCallback) throws RemoteException;
  
  void overridePendingTransition(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void positionTaskInStack(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void registerRemoteAnimationForNextActivityStart(String paramString, RemoteAnimationAdapter paramRemoteAnimationAdapter) throws RemoteException;
  
  void registerRemoteAnimations(IBinder paramIBinder, RemoteAnimationDefinition paramRemoteAnimationDefinition) throws RemoteException;
  
  void registerRemoteAnimationsForDisplay(int paramInt, RemoteAnimationDefinition paramRemoteAnimationDefinition) throws RemoteException;
  
  void registerTaskStackListener(ITaskStackListener paramITaskStackListener) throws RemoteException;
  
  boolean releaseActivityInstance(IBinder paramIBinder) throws RemoteException;
  
  void releaseSomeActivities(IApplicationThread paramIApplicationThread) throws RemoteException;
  
  void removeAllVisibleRecentTasks() throws RemoteException;
  
  void removeStack(int paramInt) throws RemoteException;
  
  void removeStacksInWindowingModes(int[] paramArrayOfint) throws RemoteException;
  
  void removeStacksWithActivityTypes(int[] paramArrayOfint) throws RemoteException;
  
  boolean removeTask(int paramInt) throws RemoteException;
  
  void reportActivityFullyDrawn(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void reportAssistContextExtras(IBinder paramIBinder, Bundle paramBundle, AssistStructure paramAssistStructure, AssistContent paramAssistContent, Uri paramUri) throws RemoteException;
  
  void reportSizeConfigurations(IBinder paramIBinder, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3) throws RemoteException;
  
  boolean requestAssistContextExtras(int paramInt, IAssistDataReceiver paramIAssistDataReceiver, Bundle paramBundle, IBinder paramIBinder, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  boolean requestAutofillData(IAssistDataReceiver paramIAssistDataReceiver, Bundle paramBundle, IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void requestPictureInPictureMode(IBinder paramIBinder) throws RemoteException;
  
  IBinder requestStartActivityPermissionToken(IBinder paramIBinder) throws RemoteException;
  
  void resizeDockedStack(Rect paramRect1, Rect paramRect2, Rect paramRect3, Rect paramRect4, Rect paramRect5) throws RemoteException;
  
  boolean resizeTask(int paramInt1, Rect paramRect, int paramInt2) throws RemoteException;
  
  void restartActivityProcessIfVisible(IBinder paramIBinder) throws RemoteException;
  
  void resumeAppSwitches() throws RemoteException;
  
  void setActivityController(IActivityController paramIActivityController, boolean paramBoolean) throws RemoteException;
  
  void setDisablePreviewScreenshots(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setDisplayToSingleTaskInstance(int paramInt) throws RemoteException;
  
  void setFocusedStack(int paramInt) throws RemoteException;
  
  void setFocusedTask(int paramInt) throws RemoteException;
  
  void setFrontActivityScreenCompatMode(int paramInt) throws RemoteException;
  
  void setImmersive(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setInheritShowWhenLocked(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setLockScreenShown(boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setOppoKinectActivityController(IOplusKinectActivityController paramIOplusKinectActivityController) throws RemoteException;
  
  void setPackageAskScreenCompat(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setPackageScreenCompatMode(String paramString, int paramInt) throws RemoteException;
  
  void setPersistentVrThread(int paramInt) throws RemoteException;
  
  void setPictureInPictureParams(IBinder paramIBinder, PictureInPictureParams paramPictureInPictureParams) throws RemoteException;
  
  void setRequestedOrientation(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void setShowWhenLocked(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setSplitScreenResizing(boolean paramBoolean) throws RemoteException;
  
  void setTaskDescription(IBinder paramIBinder, ActivityManager.TaskDescription paramTaskDescription) throws RemoteException;
  
  void setTaskResizeable(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean setTaskWindowingMode(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  boolean setTaskWindowingModeSplitScreenPrimary(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setTurnScreenOn(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void setVoiceKeepAwake(IVoiceInteractionSession paramIVoiceInteractionSession, boolean paramBoolean) throws RemoteException;
  
  int setVrMode(IBinder paramIBinder, boolean paramBoolean, ComponentName paramComponentName) throws RemoteException;
  
  void setVrThread(int paramInt) throws RemoteException;
  
  boolean shouldUpRecreateTask(IBinder paramIBinder, String paramString) throws RemoteException;
  
  boolean showAssistFromActivity(IBinder paramIBinder, Bundle paramBundle) throws RemoteException;
  
  void showLockTaskEscapeMessage(IBinder paramIBinder) throws RemoteException;
  
  int startActivities(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent[] paramArrayOfIntent, String[] paramArrayOfString, IBinder paramIBinder, Bundle paramBundle, int paramInt) throws RemoteException;
  
  int startActivity(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle) throws RemoteException;
  
  WaitResult startActivityAndWait(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  int startActivityAsCaller(IApplicationThread paramIApplicationThread, String paramString1, Intent paramIntent, String paramString2, IBinder paramIBinder1, String paramString3, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle, IBinder paramIBinder2, boolean paramBoolean, int paramInt3) throws RemoteException;
  
  int startActivityAsUser(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  int startActivityFromRecents(int paramInt, Bundle paramBundle) throws RemoteException;
  
  int startActivityIntentSender(IApplicationThread paramIApplicationThread, IIntentSender paramIIntentSender, IBinder paramIBinder1, Intent paramIntent, String paramString1, IBinder paramIBinder2, String paramString2, int paramInt1, int paramInt2, int paramInt3, Bundle paramBundle) throws RemoteException;
  
  int startActivityWithConfig(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, Configuration paramConfiguration, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  int startAssistantActivity(String paramString1, String paramString2, int paramInt1, int paramInt2, Intent paramIntent, String paramString3, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  boolean startDreamActivity(Intent paramIntent) throws RemoteException;
  
  void startLocalVoiceInteraction(IBinder paramIBinder, Bundle paramBundle) throws RemoteException;
  
  void startLockTaskModeByToken(IBinder paramIBinder) throws RemoteException;
  
  boolean startNextMatchingActivity(IBinder paramIBinder, Intent paramIntent, Bundle paramBundle) throws RemoteException;
  
  void startRecentsActivity(Intent paramIntent, IAssistDataReceiver paramIAssistDataReceiver, IRecentsAnimationRunner paramIRecentsAnimationRunner) throws RemoteException;
  
  void startSystemLockTaskMode(int paramInt) throws RemoteException;
  
  int startVoiceActivity(String paramString1, String paramString2, int paramInt1, int paramInt2, Intent paramIntent, String paramString3, IVoiceInteractionSession paramIVoiceInteractionSession, IVoiceInteractor paramIVoiceInteractor, int paramInt3, ProfilerInfo paramProfilerInfo, Bundle paramBundle, int paramInt4) throws RemoteException;
  
  void stopAppSwitches() throws RemoteException;
  
  void stopLocalVoiceInteraction(IBinder paramIBinder) throws RemoteException;
  
  void stopLockTaskModeByToken(IBinder paramIBinder) throws RemoteException;
  
  void stopSystemLockTaskMode() throws RemoteException;
  
  boolean supportsLocalVoiceInteraction() throws RemoteException;
  
  void suppressResizeConfigChanges(boolean paramBoolean) throws RemoteException;
  
  void toggleFreeformWindowingMode(IBinder paramIBinder) throws RemoteException;
  
  void unhandledBack() throws RemoteException;
  
  void unregisterRemoteAnimations(IBinder paramIBinder) throws RemoteException;
  
  void unregisterTaskStackListener(ITaskStackListener paramITaskStackListener) throws RemoteException;
  
  boolean updateConfiguration(Configuration paramConfiguration) throws RemoteException;
  
  void updateLockTaskFeatures(int paramInt1, int paramInt2) throws RemoteException;
  
  void updateLockTaskPackages(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  boolean willActivityBeVisible(IBinder paramIBinder) throws RemoteException;
  
  class Default implements IActivityTaskManager {
    public int startActivity(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public int startActivities(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent[] param1ArrayOfIntent, String[] param1ArrayOfString, IBinder param1IBinder, Bundle param1Bundle, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int startActivityAsUser(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public boolean startNextMatchingActivity(IBinder param1IBinder, Intent param1Intent, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public boolean startDreamActivity(Intent param1Intent) throws RemoteException {
      return false;
    }
    
    public int startActivityIntentSender(IApplicationThread param1IApplicationThread, IIntentSender param1IIntentSender, IBinder param1IBinder1, Intent param1Intent, String param1String1, IBinder param1IBinder2, String param1String2, int param1Int1, int param1Int2, int param1Int3, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public WaitResult startActivityAndWait(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return null;
    }
    
    public int startActivityWithConfig(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, Configuration param1Configuration, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int startVoiceActivity(String param1String1, String param1String2, int param1Int1, int param1Int2, Intent param1Intent, String param1String3, IVoiceInteractionSession param1IVoiceInteractionSession, IVoiceInteractor param1IVoiceInteractor, int param1Int3, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, int param1Int4) throws RemoteException {
      return 0;
    }
    
    public int startAssistantActivity(String param1String1, String param1String2, int param1Int1, int param1Int2, Intent param1Intent, String param1String3, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public void startRecentsActivity(Intent param1Intent, IAssistDataReceiver param1IAssistDataReceiver, IRecentsAnimationRunner param1IRecentsAnimationRunner) throws RemoteException {}
    
    public int startActivityFromRecents(int param1Int, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public int startActivityAsCaller(IApplicationThread param1IApplicationThread, String param1String1, Intent param1Intent, String param1String2, IBinder param1IBinder1, String param1String3, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, IBinder param1IBinder2, boolean param1Boolean, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public boolean isActivityStartAllowedOnDisplay(int param1Int1, Intent param1Intent, String param1String, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void unhandledBack() throws RemoteException {}
    
    public boolean finishActivity(IBinder param1IBinder, int param1Int1, Intent param1Intent, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean finishActivityAffinity(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void activityIdle(IBinder param1IBinder, Configuration param1Configuration, boolean param1Boolean) throws RemoteException {}
    
    public void activityResumed(IBinder param1IBinder) throws RemoteException {}
    
    public void activityTopResumedStateLost() throws RemoteException {}
    
    public void activityPaused(IBinder param1IBinder) throws RemoteException {}
    
    public void activityStopped(IBinder param1IBinder, Bundle param1Bundle, PersistableBundle param1PersistableBundle, CharSequence param1CharSequence) throws RemoteException {}
    
    public void activityDestroyed(IBinder param1IBinder) throws RemoteException {}
    
    public void activityRelaunched(IBinder param1IBinder) throws RemoteException {}
    
    public int getFrontActivityScreenCompatMode() throws RemoteException {
      return 0;
    }
    
    public void setFrontActivityScreenCompatMode(int param1Int) throws RemoteException {}
    
    public String getCallingPackage(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public ComponentName getCallingActivity(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void setFocusedTask(int param1Int) throws RemoteException {}
    
    public boolean removeTask(int param1Int) throws RemoteException {
      return false;
    }
    
    public void removeAllVisibleRecentTasks() throws RemoteException {}
    
    public List<ActivityManager.RunningTaskInfo> getTasks(int param1Int) throws RemoteException {
      return null;
    }
    
    public List<ActivityManager.RunningTaskInfo> getFilteredTasks(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public boolean shouldUpRecreateTask(IBinder param1IBinder, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean navigateUpTo(IBinder param1IBinder, Intent param1Intent1, int param1Int, Intent param1Intent2) throws RemoteException {
      return false;
    }
    
    public void moveTaskToFront(IApplicationThread param1IApplicationThread, String param1String, int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public int getTaskForActivity(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public void finishSubActivity(IBinder param1IBinder, String param1String, int param1Int) throws RemoteException {}
    
    public ParceledListSlice getRecentTasks(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public boolean willActivityBeVisible(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void setRequestedOrientation(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public int getRequestedOrientation(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean convertFromTranslucent(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public boolean convertToTranslucent(IBinder param1IBinder, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public void notifyActivityDrawn(IBinder param1IBinder) throws RemoteException {}
    
    public void reportActivityFullyDrawn(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public int getDisplayId(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean isImmersive(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void setImmersive(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public boolean isTopActivityImmersive() throws RemoteException {
      return false;
    }
    
    public boolean moveActivityTaskToBack(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public ActivityManager.TaskDescription getTaskDescription(int param1Int) throws RemoteException {
      return null;
    }
    
    public void overridePendingTransition(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public int getLaunchedFromUid(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public String getLaunchedFromPackage(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void reportAssistContextExtras(IBinder param1IBinder, Bundle param1Bundle, AssistStructure param1AssistStructure, AssistContent param1AssistContent, Uri param1Uri) throws RemoteException {}
    
    public void setFocusedStack(int param1Int) throws RemoteException {}
    
    public ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException {
      return null;
    }
    
    public Rect getTaskBounds(int param1Int) throws RemoteException {
      return null;
    }
    
    public void cancelRecentsAnimation(boolean param1Boolean) throws RemoteException {}
    
    public void startLockTaskModeByToken(IBinder param1IBinder) throws RemoteException {}
    
    public void stopLockTaskModeByToken(IBinder param1IBinder) throws RemoteException {}
    
    public void updateLockTaskPackages(int param1Int, String[] param1ArrayOfString) throws RemoteException {}
    
    public boolean isInLockTaskMode() throws RemoteException {
      return false;
    }
    
    public int getLockTaskModeState() throws RemoteException {
      return 0;
    }
    
    public void setTaskDescription(IBinder param1IBinder, ActivityManager.TaskDescription param1TaskDescription) throws RemoteException {}
    
    public Bundle getActivityOptions(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public List<IBinder> getAppTasks(String param1String) throws RemoteException {
      return null;
    }
    
    public void startSystemLockTaskMode(int param1Int) throws RemoteException {}
    
    public void stopSystemLockTaskMode() throws RemoteException {}
    
    public void finishVoiceTask(IVoiceInteractionSession param1IVoiceInteractionSession) throws RemoteException {}
    
    public boolean isTopOfTask(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void notifyLaunchTaskBehindComplete(IBinder param1IBinder) throws RemoteException {}
    
    public void notifyEnterAnimationComplete(IBinder param1IBinder) throws RemoteException {}
    
    public int addAppTask(IBinder param1IBinder, Intent param1Intent, ActivityManager.TaskDescription param1TaskDescription, Bitmap param1Bitmap) throws RemoteException {
      return 0;
    }
    
    public Point getAppTaskThumbnailSize() throws RemoteException {
      return null;
    }
    
    public boolean releaseActivityInstance(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public IBinder requestStartActivityPermissionToken(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void releaseSomeActivities(IApplicationThread param1IApplicationThread) throws RemoteException {}
    
    public Bitmap getTaskDescriptionIcon(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerTaskStackListener(ITaskStackListener param1ITaskStackListener) throws RemoteException {}
    
    public void unregisterTaskStackListener(ITaskStackListener param1ITaskStackListener) throws RemoteException {}
    
    public void setTaskResizeable(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void toggleFreeformWindowingMode(IBinder param1IBinder) throws RemoteException {}
    
    public boolean resizeTask(int param1Int1, Rect param1Rect, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void moveStackToDisplay(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void removeStack(int param1Int) throws RemoteException {}
    
    public boolean setTaskWindowingMode(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void moveTaskToStack(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public boolean setTaskWindowingModeSplitScreenPrimary(int param1Int, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void removeStacksInWindowingModes(int[] param1ArrayOfint) throws RemoteException {}
    
    public void removeStacksWithActivityTypes(int[] param1ArrayOfint) throws RemoteException {}
    
    public List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException {
      return null;
    }
    
    public ActivityManager.StackInfo getStackInfo(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public List<ActivityManager.StackInfo> getAllStackInfosOnDisplay(int param1Int) throws RemoteException {
      return null;
    }
    
    public ActivityManager.StackInfo getStackInfoOnDisplay(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public void setLockScreenShown(boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public Bundle getAssistContextExtras(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean launchAssistIntent(Intent param1Intent, int param1Int1, String param1String, int param1Int2, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public boolean requestAssistContextExtras(int param1Int, IAssistDataReceiver param1IAssistDataReceiver, Bundle param1Bundle, IBinder param1IBinder, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {
      return false;
    }
    
    public boolean requestAutofillData(IAssistDataReceiver param1IAssistDataReceiver, Bundle param1Bundle, IBinder param1IBinder, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isAssistDataAllowedOnCurrentActivity() throws RemoteException {
      return false;
    }
    
    public boolean showAssistFromActivity(IBinder param1IBinder, Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public boolean isRootVoiceInteraction(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void showLockTaskEscapeMessage(IBinder param1IBinder) throws RemoteException {}
    
    public void keyguardGoingAway(int param1Int) throws RemoteException {}
    
    public ComponentName getActivityClassForToken(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public String getPackageForToken(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void positionTaskInStack(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void reportSizeConfigurations(IBinder param1IBinder, int[] param1ArrayOfint1, int[] param1ArrayOfint2, int[] param1ArrayOfint3) throws RemoteException {}
    
    public void suppressResizeConfigChanges(boolean param1Boolean) throws RemoteException {}
    
    public boolean moveTopActivityToPinnedStack(int param1Int, Rect param1Rect) throws RemoteException {
      return false;
    }
    
    public boolean enterPictureInPictureMode(IBinder param1IBinder, PictureInPictureParams param1PictureInPictureParams) throws RemoteException {
      return false;
    }
    
    public void setPictureInPictureParams(IBinder param1IBinder, PictureInPictureParams param1PictureInPictureParams) throws RemoteException {}
    
    public void requestPictureInPictureMode(IBinder param1IBinder) throws RemoteException {}
    
    public int getMaxNumPictureInPictureActions(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public IBinder getUriPermissionOwnerForActivity(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void resizeDockedStack(Rect param1Rect1, Rect param1Rect2, Rect param1Rect3, Rect param1Rect4, Rect param1Rect5) throws RemoteException {}
    
    public IWindowOrganizerController getWindowOrganizerController() throws RemoteException {
      return null;
    }
    
    public void setSplitScreenResizing(boolean param1Boolean) throws RemoteException {}
    
    public int setVrMode(IBinder param1IBinder, boolean param1Boolean, ComponentName param1ComponentName) throws RemoteException {
      return 0;
    }
    
    public void startLocalVoiceInteraction(IBinder param1IBinder, Bundle param1Bundle) throws RemoteException {}
    
    public void stopLocalVoiceInteraction(IBinder param1IBinder) throws RemoteException {}
    
    public boolean supportsLocalVoiceInteraction() throws RemoteException {
      return false;
    }
    
    public ConfigurationInfo getDeviceConfigurationInfo() throws RemoteException {
      return null;
    }
    
    public void dismissKeyguard(IBinder param1IBinder, IKeyguardDismissCallback param1IKeyguardDismissCallback, CharSequence param1CharSequence) throws RemoteException {}
    
    public void cancelTaskWindowTransition(int param1Int) throws RemoteException {}
    
    public ActivityManager.TaskSnapshot getTaskSnapshot(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void setDisablePreviewScreenshots(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void invalidateHomeTaskSnapshot(IBinder param1IBinder) throws RemoteException {}
    
    public int getLastResumedActivityUserId() throws RemoteException {
      return 0;
    }
    
    public boolean updateConfiguration(Configuration param1Configuration) throws RemoteException {
      return false;
    }
    
    public void updateLockTaskFeatures(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setShowWhenLocked(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void setInheritShowWhenLocked(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void setTurnScreenOn(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void registerRemoteAnimations(IBinder param1IBinder, RemoteAnimationDefinition param1RemoteAnimationDefinition) throws RemoteException {}
    
    public void unregisterRemoteAnimations(IBinder param1IBinder) throws RemoteException {}
    
    public void registerRemoteAnimationForNextActivityStart(String param1String, RemoteAnimationAdapter param1RemoteAnimationAdapter) throws RemoteException {}
    
    public void registerRemoteAnimationsForDisplay(int param1Int, RemoteAnimationDefinition param1RemoteAnimationDefinition) throws RemoteException {}
    
    public void alwaysShowUnsupportedCompileSdkWarning(ComponentName param1ComponentName) throws RemoteException {}
    
    public void setOppoKinectActivityController(IOplusKinectActivityController param1IOplusKinectActivityController) throws RemoteException {}
    
    public void setVrThread(int param1Int) throws RemoteException {}
    
    public void setPersistentVrThread(int param1Int) throws RemoteException {}
    
    public void stopAppSwitches() throws RemoteException {}
    
    public void resumeAppSwitches() throws RemoteException {}
    
    public void setActivityController(IActivityController param1IActivityController, boolean param1Boolean) throws RemoteException {}
    
    public void setVoiceKeepAwake(IVoiceInteractionSession param1IVoiceInteractionSession, boolean param1Boolean) throws RemoteException {}
    
    public int getPackageScreenCompatMode(String param1String) throws RemoteException {
      return 0;
    }
    
    public void setPackageScreenCompatMode(String param1String, int param1Int) throws RemoteException {}
    
    public boolean getPackageAskScreenCompat(String param1String) throws RemoteException {
      return false;
    }
    
    public void setPackageAskScreenCompat(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void clearLaunchParamsForPackages(List<String> param1List) throws RemoteException {}
    
    public void setDisplayToSingleTaskInstance(int param1Int) throws RemoteException {}
    
    public void restartActivityProcessIfVisible(IBinder param1IBinder) throws RemoteException {}
    
    public void onBackPressedOnTaskRoot(IBinder param1IBinder, IRequestFinishCallback param1IRequestFinishCallback) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IActivityTaskManager {
    private static final String DESCRIPTOR = "android.app.IActivityTaskManager";
    
    static final int TRANSACTION_activityDestroyed = 23;
    
    static final int TRANSACTION_activityIdle = 18;
    
    static final int TRANSACTION_activityPaused = 21;
    
    static final int TRANSACTION_activityRelaunched = 24;
    
    static final int TRANSACTION_activityResumed = 19;
    
    static final int TRANSACTION_activityStopped = 22;
    
    static final int TRANSACTION_activityTopResumedStateLost = 20;
    
    static final int TRANSACTION_addAppTask = 75;
    
    static final int TRANSACTION_alwaysShowUnsupportedCompileSdkWarning = 141;
    
    static final int TRANSACTION_cancelRecentsAnimation = 60;
    
    static final int TRANSACTION_cancelTaskWindowTransition = 127;
    
    static final int TRANSACTION_clearLaunchParamsForPackages = 153;
    
    static final int TRANSACTION_convertFromTranslucent = 43;
    
    static final int TRANSACTION_convertToTranslucent = 44;
    
    static final int TRANSACTION_dismissKeyguard = 126;
    
    static final int TRANSACTION_enterPictureInPictureMode = 113;
    
    static final int TRANSACTION_finishActivity = 16;
    
    static final int TRANSACTION_finishActivityAffinity = 17;
    
    static final int TRANSACTION_finishSubActivity = 38;
    
    static final int TRANSACTION_finishVoiceTask = 71;
    
    static final int TRANSACTION_getActivityClassForToken = 107;
    
    static final int TRANSACTION_getActivityOptions = 67;
    
    static final int TRANSACTION_getAllStackInfos = 93;
    
    static final int TRANSACTION_getAllStackInfosOnDisplay = 95;
    
    static final int TRANSACTION_getAppTaskThumbnailSize = 76;
    
    static final int TRANSACTION_getAppTasks = 68;
    
    static final int TRANSACTION_getAssistContextExtras = 98;
    
    static final int TRANSACTION_getCallingActivity = 28;
    
    static final int TRANSACTION_getCallingPackage = 27;
    
    static final int TRANSACTION_getDeviceConfigurationInfo = 125;
    
    static final int TRANSACTION_getDisplayId = 47;
    
    static final int TRANSACTION_getFilteredTasks = 33;
    
    static final int TRANSACTION_getFocusedStackInfo = 58;
    
    static final int TRANSACTION_getFrontActivityScreenCompatMode = 25;
    
    static final int TRANSACTION_getLastResumedActivityUserId = 131;
    
    static final int TRANSACTION_getLaunchedFromPackage = 55;
    
    static final int TRANSACTION_getLaunchedFromUid = 54;
    
    static final int TRANSACTION_getLockTaskModeState = 65;
    
    static final int TRANSACTION_getMaxNumPictureInPictureActions = 116;
    
    static final int TRANSACTION_getPackageAskScreenCompat = 151;
    
    static final int TRANSACTION_getPackageForToken = 108;
    
    static final int TRANSACTION_getPackageScreenCompatMode = 149;
    
    static final int TRANSACTION_getRecentTasks = 39;
    
    static final int TRANSACTION_getRequestedOrientation = 42;
    
    static final int TRANSACTION_getStackInfo = 94;
    
    static final int TRANSACTION_getStackInfoOnDisplay = 96;
    
    static final int TRANSACTION_getTaskBounds = 59;
    
    static final int TRANSACTION_getTaskDescription = 52;
    
    static final int TRANSACTION_getTaskDescriptionIcon = 80;
    
    static final int TRANSACTION_getTaskForActivity = 37;
    
    static final int TRANSACTION_getTaskSnapshot = 128;
    
    static final int TRANSACTION_getTasks = 32;
    
    static final int TRANSACTION_getUriPermissionOwnerForActivity = 117;
    
    static final int TRANSACTION_getWindowOrganizerController = 119;
    
    static final int TRANSACTION_invalidateHomeTaskSnapshot = 130;
    
    static final int TRANSACTION_isActivityStartAllowedOnDisplay = 14;
    
    static final int TRANSACTION_isAssistDataAllowedOnCurrentActivity = 102;
    
    static final int TRANSACTION_isImmersive = 48;
    
    static final int TRANSACTION_isInLockTaskMode = 64;
    
    static final int TRANSACTION_isRootVoiceInteraction = 104;
    
    static final int TRANSACTION_isTopActivityImmersive = 50;
    
    static final int TRANSACTION_isTopOfTask = 72;
    
    static final int TRANSACTION_keyguardGoingAway = 106;
    
    static final int TRANSACTION_launchAssistIntent = 99;
    
    static final int TRANSACTION_moveActivityTaskToBack = 51;
    
    static final int TRANSACTION_moveStackToDisplay = 86;
    
    static final int TRANSACTION_moveTaskToFront = 36;
    
    static final int TRANSACTION_moveTaskToStack = 89;
    
    static final int TRANSACTION_moveTopActivityToPinnedStack = 112;
    
    static final int TRANSACTION_navigateUpTo = 35;
    
    static final int TRANSACTION_notifyActivityDrawn = 45;
    
    static final int TRANSACTION_notifyEnterAnimationComplete = 74;
    
    static final int TRANSACTION_notifyLaunchTaskBehindComplete = 73;
    
    static final int TRANSACTION_onBackPressedOnTaskRoot = 156;
    
    static final int TRANSACTION_overridePendingTransition = 53;
    
    static final int TRANSACTION_positionTaskInStack = 109;
    
    static final int TRANSACTION_registerRemoteAnimationForNextActivityStart = 139;
    
    static final int TRANSACTION_registerRemoteAnimations = 137;
    
    static final int TRANSACTION_registerRemoteAnimationsForDisplay = 140;
    
    static final int TRANSACTION_registerTaskStackListener = 81;
    
    static final int TRANSACTION_releaseActivityInstance = 77;
    
    static final int TRANSACTION_releaseSomeActivities = 79;
    
    static final int TRANSACTION_removeAllVisibleRecentTasks = 31;
    
    static final int TRANSACTION_removeStack = 87;
    
    static final int TRANSACTION_removeStacksInWindowingModes = 91;
    
    static final int TRANSACTION_removeStacksWithActivityTypes = 92;
    
    static final int TRANSACTION_removeTask = 30;
    
    static final int TRANSACTION_reportActivityFullyDrawn = 46;
    
    static final int TRANSACTION_reportAssistContextExtras = 56;
    
    static final int TRANSACTION_reportSizeConfigurations = 110;
    
    static final int TRANSACTION_requestAssistContextExtras = 100;
    
    static final int TRANSACTION_requestAutofillData = 101;
    
    static final int TRANSACTION_requestPictureInPictureMode = 115;
    
    static final int TRANSACTION_requestStartActivityPermissionToken = 78;
    
    static final int TRANSACTION_resizeDockedStack = 118;
    
    static final int TRANSACTION_resizeTask = 85;
    
    static final int TRANSACTION_restartActivityProcessIfVisible = 155;
    
    static final int TRANSACTION_resumeAppSwitches = 146;
    
    static final int TRANSACTION_setActivityController = 147;
    
    static final int TRANSACTION_setDisablePreviewScreenshots = 129;
    
    static final int TRANSACTION_setDisplayToSingleTaskInstance = 154;
    
    static final int TRANSACTION_setFocusedStack = 57;
    
    static final int TRANSACTION_setFocusedTask = 29;
    
    static final int TRANSACTION_setFrontActivityScreenCompatMode = 26;
    
    static final int TRANSACTION_setImmersive = 49;
    
    static final int TRANSACTION_setInheritShowWhenLocked = 135;
    
    static final int TRANSACTION_setLockScreenShown = 97;
    
    static final int TRANSACTION_setOppoKinectActivityController = 142;
    
    static final int TRANSACTION_setPackageAskScreenCompat = 152;
    
    static final int TRANSACTION_setPackageScreenCompatMode = 150;
    
    static final int TRANSACTION_setPersistentVrThread = 144;
    
    static final int TRANSACTION_setPictureInPictureParams = 114;
    
    static final int TRANSACTION_setRequestedOrientation = 41;
    
    static final int TRANSACTION_setShowWhenLocked = 134;
    
    static final int TRANSACTION_setSplitScreenResizing = 120;
    
    static final int TRANSACTION_setTaskDescription = 66;
    
    static final int TRANSACTION_setTaskResizeable = 83;
    
    static final int TRANSACTION_setTaskWindowingMode = 88;
    
    static final int TRANSACTION_setTaskWindowingModeSplitScreenPrimary = 90;
    
    static final int TRANSACTION_setTurnScreenOn = 136;
    
    static final int TRANSACTION_setVoiceKeepAwake = 148;
    
    static final int TRANSACTION_setVrMode = 121;
    
    static final int TRANSACTION_setVrThread = 143;
    
    static final int TRANSACTION_shouldUpRecreateTask = 34;
    
    static final int TRANSACTION_showAssistFromActivity = 103;
    
    static final int TRANSACTION_showLockTaskEscapeMessage = 105;
    
    static final int TRANSACTION_startActivities = 2;
    
    static final int TRANSACTION_startActivity = 1;
    
    static final int TRANSACTION_startActivityAndWait = 7;
    
    static final int TRANSACTION_startActivityAsCaller = 13;
    
    static final int TRANSACTION_startActivityAsUser = 3;
    
    static final int TRANSACTION_startActivityFromRecents = 12;
    
    static final int TRANSACTION_startActivityIntentSender = 6;
    
    static final int TRANSACTION_startActivityWithConfig = 8;
    
    static final int TRANSACTION_startAssistantActivity = 10;
    
    static final int TRANSACTION_startDreamActivity = 5;
    
    static final int TRANSACTION_startLocalVoiceInteraction = 122;
    
    static final int TRANSACTION_startLockTaskModeByToken = 61;
    
    static final int TRANSACTION_startNextMatchingActivity = 4;
    
    static final int TRANSACTION_startRecentsActivity = 11;
    
    static final int TRANSACTION_startSystemLockTaskMode = 69;
    
    static final int TRANSACTION_startVoiceActivity = 9;
    
    static final int TRANSACTION_stopAppSwitches = 145;
    
    static final int TRANSACTION_stopLocalVoiceInteraction = 123;
    
    static final int TRANSACTION_stopLockTaskModeByToken = 62;
    
    static final int TRANSACTION_stopSystemLockTaskMode = 70;
    
    static final int TRANSACTION_supportsLocalVoiceInteraction = 124;
    
    static final int TRANSACTION_suppressResizeConfigChanges = 111;
    
    static final int TRANSACTION_toggleFreeformWindowingMode = 84;
    
    static final int TRANSACTION_unhandledBack = 15;
    
    static final int TRANSACTION_unregisterRemoteAnimations = 138;
    
    static final int TRANSACTION_unregisterTaskStackListener = 82;
    
    static final int TRANSACTION_updateConfiguration = 132;
    
    static final int TRANSACTION_updateLockTaskFeatures = 133;
    
    static final int TRANSACTION_updateLockTaskPackages = 63;
    
    static final int TRANSACTION_willActivityBeVisible = 40;
    
    public Stub() {
      attachInterface(this, "android.app.IActivityTaskManager");
    }
    
    public static IActivityTaskManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IActivityTaskManager");
      if (iInterface != null && iInterface instanceof IActivityTaskManager)
        return (IActivityTaskManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 156:
          return "onBackPressedOnTaskRoot";
        case 155:
          return "restartActivityProcessIfVisible";
        case 154:
          return "setDisplayToSingleTaskInstance";
        case 153:
          return "clearLaunchParamsForPackages";
        case 152:
          return "setPackageAskScreenCompat";
        case 151:
          return "getPackageAskScreenCompat";
        case 150:
          return "setPackageScreenCompatMode";
        case 149:
          return "getPackageScreenCompatMode";
        case 148:
          return "setVoiceKeepAwake";
        case 147:
          return "setActivityController";
        case 146:
          return "resumeAppSwitches";
        case 145:
          return "stopAppSwitches";
        case 144:
          return "setPersistentVrThread";
        case 143:
          return "setVrThread";
        case 142:
          return "setOppoKinectActivityController";
        case 141:
          return "alwaysShowUnsupportedCompileSdkWarning";
        case 140:
          return "registerRemoteAnimationsForDisplay";
        case 139:
          return "registerRemoteAnimationForNextActivityStart";
        case 138:
          return "unregisterRemoteAnimations";
        case 137:
          return "registerRemoteAnimations";
        case 136:
          return "setTurnScreenOn";
        case 135:
          return "setInheritShowWhenLocked";
        case 134:
          return "setShowWhenLocked";
        case 133:
          return "updateLockTaskFeatures";
        case 132:
          return "updateConfiguration";
        case 131:
          return "getLastResumedActivityUserId";
        case 130:
          return "invalidateHomeTaskSnapshot";
        case 129:
          return "setDisablePreviewScreenshots";
        case 128:
          return "getTaskSnapshot";
        case 127:
          return "cancelTaskWindowTransition";
        case 126:
          return "dismissKeyguard";
        case 125:
          return "getDeviceConfigurationInfo";
        case 124:
          return "supportsLocalVoiceInteraction";
        case 123:
          return "stopLocalVoiceInteraction";
        case 122:
          return "startLocalVoiceInteraction";
        case 121:
          return "setVrMode";
        case 120:
          return "setSplitScreenResizing";
        case 119:
          return "getWindowOrganizerController";
        case 118:
          return "resizeDockedStack";
        case 117:
          return "getUriPermissionOwnerForActivity";
        case 116:
          return "getMaxNumPictureInPictureActions";
        case 115:
          return "requestPictureInPictureMode";
        case 114:
          return "setPictureInPictureParams";
        case 113:
          return "enterPictureInPictureMode";
        case 112:
          return "moveTopActivityToPinnedStack";
        case 111:
          return "suppressResizeConfigChanges";
        case 110:
          return "reportSizeConfigurations";
        case 109:
          return "positionTaskInStack";
        case 108:
          return "getPackageForToken";
        case 107:
          return "getActivityClassForToken";
        case 106:
          return "keyguardGoingAway";
        case 105:
          return "showLockTaskEscapeMessage";
        case 104:
          return "isRootVoiceInteraction";
        case 103:
          return "showAssistFromActivity";
        case 102:
          return "isAssistDataAllowedOnCurrentActivity";
        case 101:
          return "requestAutofillData";
        case 100:
          return "requestAssistContextExtras";
        case 99:
          return "launchAssistIntent";
        case 98:
          return "getAssistContextExtras";
        case 97:
          return "setLockScreenShown";
        case 96:
          return "getStackInfoOnDisplay";
        case 95:
          return "getAllStackInfosOnDisplay";
        case 94:
          return "getStackInfo";
        case 93:
          return "getAllStackInfos";
        case 92:
          return "removeStacksWithActivityTypes";
        case 91:
          return "removeStacksInWindowingModes";
        case 90:
          return "setTaskWindowingModeSplitScreenPrimary";
        case 89:
          return "moveTaskToStack";
        case 88:
          return "setTaskWindowingMode";
        case 87:
          return "removeStack";
        case 86:
          return "moveStackToDisplay";
        case 85:
          return "resizeTask";
        case 84:
          return "toggleFreeformWindowingMode";
        case 83:
          return "setTaskResizeable";
        case 82:
          return "unregisterTaskStackListener";
        case 81:
          return "registerTaskStackListener";
        case 80:
          return "getTaskDescriptionIcon";
        case 79:
          return "releaseSomeActivities";
        case 78:
          return "requestStartActivityPermissionToken";
        case 77:
          return "releaseActivityInstance";
        case 76:
          return "getAppTaskThumbnailSize";
        case 75:
          return "addAppTask";
        case 74:
          return "notifyEnterAnimationComplete";
        case 73:
          return "notifyLaunchTaskBehindComplete";
        case 72:
          return "isTopOfTask";
        case 71:
          return "finishVoiceTask";
        case 70:
          return "stopSystemLockTaskMode";
        case 69:
          return "startSystemLockTaskMode";
        case 68:
          return "getAppTasks";
        case 67:
          return "getActivityOptions";
        case 66:
          return "setTaskDescription";
        case 65:
          return "getLockTaskModeState";
        case 64:
          return "isInLockTaskMode";
        case 63:
          return "updateLockTaskPackages";
        case 62:
          return "stopLockTaskModeByToken";
        case 61:
          return "startLockTaskModeByToken";
        case 60:
          return "cancelRecentsAnimation";
        case 59:
          return "getTaskBounds";
        case 58:
          return "getFocusedStackInfo";
        case 57:
          return "setFocusedStack";
        case 56:
          return "reportAssistContextExtras";
        case 55:
          return "getLaunchedFromPackage";
        case 54:
          return "getLaunchedFromUid";
        case 53:
          return "overridePendingTransition";
        case 52:
          return "getTaskDescription";
        case 51:
          return "moveActivityTaskToBack";
        case 50:
          return "isTopActivityImmersive";
        case 49:
          return "setImmersive";
        case 48:
          return "isImmersive";
        case 47:
          return "getDisplayId";
        case 46:
          return "reportActivityFullyDrawn";
        case 45:
          return "notifyActivityDrawn";
        case 44:
          return "convertToTranslucent";
        case 43:
          return "convertFromTranslucent";
        case 42:
          return "getRequestedOrientation";
        case 41:
          return "setRequestedOrientation";
        case 40:
          return "willActivityBeVisible";
        case 39:
          return "getRecentTasks";
        case 38:
          return "finishSubActivity";
        case 37:
          return "getTaskForActivity";
        case 36:
          return "moveTaskToFront";
        case 35:
          return "navigateUpTo";
        case 34:
          return "shouldUpRecreateTask";
        case 33:
          return "getFilteredTasks";
        case 32:
          return "getTasks";
        case 31:
          return "removeAllVisibleRecentTasks";
        case 30:
          return "removeTask";
        case 29:
          return "setFocusedTask";
        case 28:
          return "getCallingActivity";
        case 27:
          return "getCallingPackage";
        case 26:
          return "setFrontActivityScreenCompatMode";
        case 25:
          return "getFrontActivityScreenCompatMode";
        case 24:
          return "activityRelaunched";
        case 23:
          return "activityDestroyed";
        case 22:
          return "activityStopped";
        case 21:
          return "activityPaused";
        case 20:
          return "activityTopResumedStateLost";
        case 19:
          return "activityResumed";
        case 18:
          return "activityIdle";
        case 17:
          return "finishActivityAffinity";
        case 16:
          return "finishActivity";
        case 15:
          return "unhandledBack";
        case 14:
          return "isActivityStartAllowedOnDisplay";
        case 13:
          return "startActivityAsCaller";
        case 12:
          return "startActivityFromRecents";
        case 11:
          return "startRecentsActivity";
        case 10:
          return "startAssistantActivity";
        case 9:
          return "startVoiceActivity";
        case 8:
          return "startActivityWithConfig";
        case 7:
          return "startActivityAndWait";
        case 6:
          return "startActivityIntentSender";
        case 5:
          return "startDreamActivity";
        case 4:
          return "startNextMatchingActivity";
        case 3:
          return "startActivityAsUser";
        case 2:
          return "startActivities";
        case 1:
          break;
      } 
      return "startActivity";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool24;
        int i19;
        boolean bool23;
        int i18;
        boolean bool22;
        int i17;
        boolean bool21;
        int i16;
        boolean bool20;
        int i15;
        boolean bool19;
        int i14;
        boolean bool18;
        int i13;
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        IRequestFinishCallback iRequestFinishCallback;
        IBinder iBinder18;
        ArrayList<String> arrayList;
        String str6;
        IOplusKinectActivityController iOplusKinectActivityController;
        IBinder iBinder17;
        ActivityManager.TaskSnapshot taskSnapshot;
        ConfigurationInfo configurationInfo;
        IBinder iBinder16;
        IWindowOrganizerController iWindowOrganizerController;
        IBinder iBinder15;
        int[] arrayOfInt2;
        IBinder iBinder14;
        String str5;
        IBinder iBinder13;
        ComponentName componentName2;
        IBinder iBinder12;
        Bundle bundle2;
        ActivityManager.StackInfo stackInfo3;
        List<ActivityManager.StackInfo> list3;
        ActivityManager.StackInfo stackInfo2;
        List<ActivityManager.StackInfo> list2;
        int[] arrayOfInt1;
        IBinder iBinder11;
        ITaskStackListener iTaskStackListener;
        Bitmap bitmap;
        IApplicationThread iApplicationThread1;
        IBinder iBinder10;
        Point point;
        IBinder iBinder9;
        IVoiceInteractionSession iVoiceInteractionSession1;
        String str4;
        List<IBinder> list1;
        IBinder iBinder8;
        Bundle bundle1;
        String[] arrayOfString1;
        IBinder iBinder7;
        Rect rect1;
        ActivityManager.StackInfo stackInfo1;
        IBinder iBinder6;
        String str3;
        IBinder iBinder5;
        ActivityManager.TaskDescription taskDescription;
        IBinder iBinder4;
        ParceledListSlice parceledListSlice;
        String str2;
        List<ActivityManager.RunningTaskInfo> list;
        IBinder iBinder3;
        ComponentName componentName1;
        IBinder iBinder2;
        String str1;
        IBinder iBinder1;
        IRecentsAnimationRunner iRecentsAnimationRunner;
        WaitResult waitResult;
        IBinder iBinder23;
        String str10;
        IVoiceInteractionSession iVoiceInteractionSession2;
        IActivityController iActivityController;
        String str9;
        IBinder iBinder22;
        String str8;
        IBinder iBinder21;
        String str7;
        IBinder iBinder20;
        IApplicationThread iApplicationThread2;
        IBinder iBinder19;
        IKeyguardDismissCallback iKeyguardDismissCallback;
        int[] arrayOfInt3;
        IAssistDataReceiver iAssistDataReceiver2;
        String str13;
        IBinder iBinder26;
        String str12;
        IBinder iBinder25;
        String str11;
        IAssistDataReceiver iAssistDataReceiver1;
        IIntentSender iIntentSender;
        IBinder iBinder24;
        String[] arrayOfString2;
        Rect rect2;
        int[] arrayOfInt4;
        IBinder iBinder28;
        String str14;
        IBinder iBinder27;
        Rect rect3;
        IBinder iBinder30;
        IApplicationThread iApplicationThread5;
        String str17;
        IApplicationThread iApplicationThread4;
        String str16;
        IBinder iBinder29;
        int i20;
        String str20;
        IApplicationThread iApplicationThread7;
        String str19;
        IApplicationThread iApplicationThread6;
        String str22;
        IVoiceInteractor iVoiceInteractor;
        IBinder iBinder32;
        String str23;
        Intent[] arrayOfIntent;
        String str26;
        IVoiceInteractionSession iVoiceInteractionSession3;
        String str25;
        IBinder iBinder33, iBinder35;
        String str27;
        IBinder iBinder34;
        int i21;
        boolean bool25 = false, bool26 = false, bool27 = false, bool28 = false, bool29 = false, bool30 = false, bool31 = false, bool32 = false, bool33 = false, bool34 = false, bool35 = false, bool36 = false, bool37 = false, bool38 = false, bool39 = false, bool40 = false, bool41 = false, bool42 = false, bool43 = false, bool44 = false, bool45 = false, bool46 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 156:
            param1Parcel1.enforceInterface("android.app.IActivityTaskManager");
            iBinder23 = param1Parcel1.readStrongBinder();
            iRequestFinishCallback = IRequestFinishCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            onBackPressedOnTaskRoot(iBinder23, iRequestFinishCallback);
            param1Parcel2.writeNoException();
            return true;
          case 155:
            iRequestFinishCallback.enforceInterface("android.app.IActivityTaskManager");
            iBinder18 = iRequestFinishCallback.readStrongBinder();
            restartActivityProcessIfVisible(iBinder18);
            param1Parcel2.writeNoException();
            return true;
          case 154:
            iBinder18.enforceInterface("android.app.IActivityTaskManager");
            param1Int1 = iBinder18.readInt();
            setDisplayToSingleTaskInstance(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 153:
            iBinder18.enforceInterface("android.app.IActivityTaskManager");
            arrayList = iBinder18.createStringArrayList();
            clearLaunchParamsForPackages(arrayList);
            param1Parcel2.writeNoException();
            return true;
          case 152:
            arrayList.enforceInterface("android.app.IActivityTaskManager");
            str10 = arrayList.readString();
            bool40 = bool46;
            if (arrayList.readInt() != 0)
              bool40 = true; 
            setPackageAskScreenCompat(str10, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 151:
            arrayList.enforceInterface("android.app.IActivityTaskManager");
            str6 = arrayList.readString();
            bool24 = getPackageAskScreenCompat(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool24);
            return true;
          case 150:
            str6.enforceInterface("android.app.IActivityTaskManager");
            str10 = str6.readString();
            i19 = str6.readInt();
            setPackageScreenCompatMode(str10, i19);
            param1Parcel2.writeNoException();
            return true;
          case 149:
            str6.enforceInterface("android.app.IActivityTaskManager");
            str6 = str6.readString();
            i19 = getPackageScreenCompatMode(str6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i19);
            return true;
          case 148:
            str6.enforceInterface("android.app.IActivityTaskManager");
            iVoiceInteractionSession2 = IVoiceInteractionSession.Stub.asInterface(str6.readStrongBinder());
            bool40 = bool25;
            if (str6.readInt() != 0)
              bool40 = true; 
            setVoiceKeepAwake(iVoiceInteractionSession2, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 147:
            str6.enforceInterface("android.app.IActivityTaskManager");
            iActivityController = IActivityController.Stub.asInterface(str6.readStrongBinder());
            bool40 = bool26;
            if (str6.readInt() != 0)
              bool40 = true; 
            setActivityController(iActivityController, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 146:
            str6.enforceInterface("android.app.IActivityTaskManager");
            resumeAppSwitches();
            param1Parcel2.writeNoException();
            return true;
          case 145:
            str6.enforceInterface("android.app.IActivityTaskManager");
            stopAppSwitches();
            param1Parcel2.writeNoException();
            return true;
          case 144:
            str6.enforceInterface("android.app.IActivityTaskManager");
            i19 = str6.readInt();
            setPersistentVrThread(i19);
            param1Parcel2.writeNoException();
            return true;
          case 143:
            str6.enforceInterface("android.app.IActivityTaskManager");
            i19 = str6.readInt();
            setVrThread(i19);
            param1Parcel2.writeNoException();
            return true;
          case 142:
            str6.enforceInterface("android.app.IActivityTaskManager");
            iOplusKinectActivityController = IOplusKinectActivityController.Stub.asInterface(str6.readStrongBinder());
            setOppoKinectActivityController(iOplusKinectActivityController);
            param1Parcel2.writeNoException();
            return true;
          case 141:
            iOplusKinectActivityController.enforceInterface("android.app.IActivityTaskManager");
            if (iOplusKinectActivityController.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iOplusKinectActivityController);
            } else {
              iOplusKinectActivityController = null;
            } 
            alwaysShowUnsupportedCompileSdkWarning((ComponentName)iOplusKinectActivityController);
            param1Parcel2.writeNoException();
            return true;
          case 140:
            iOplusKinectActivityController.enforceInterface("android.app.IActivityTaskManager");
            i19 = iOplusKinectActivityController.readInt();
            if (iOplusKinectActivityController.readInt() != 0) {
              RemoteAnimationDefinition remoteAnimationDefinition = (RemoteAnimationDefinition)RemoteAnimationDefinition.CREATOR.createFromParcel((Parcel)iOplusKinectActivityController);
            } else {
              iOplusKinectActivityController = null;
            } 
            registerRemoteAnimationsForDisplay(i19, (RemoteAnimationDefinition)iOplusKinectActivityController);
            param1Parcel2.writeNoException();
            return true;
          case 139:
            iOplusKinectActivityController.enforceInterface("android.app.IActivityTaskManager");
            str9 = iOplusKinectActivityController.readString();
            if (iOplusKinectActivityController.readInt() != 0) {
              RemoteAnimationAdapter remoteAnimationAdapter = (RemoteAnimationAdapter)RemoteAnimationAdapter.CREATOR.createFromParcel((Parcel)iOplusKinectActivityController);
            } else {
              iOplusKinectActivityController = null;
            } 
            registerRemoteAnimationForNextActivityStart(str9, (RemoteAnimationAdapter)iOplusKinectActivityController);
            param1Parcel2.writeNoException();
            return true;
          case 138:
            iOplusKinectActivityController.enforceInterface("android.app.IActivityTaskManager");
            iBinder17 = iOplusKinectActivityController.readStrongBinder();
            unregisterRemoteAnimations(iBinder17);
            param1Parcel2.writeNoException();
            return true;
          case 137:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder17.readStrongBinder();
            if (iBinder17.readInt() != 0) {
              RemoteAnimationDefinition remoteAnimationDefinition = (RemoteAnimationDefinition)RemoteAnimationDefinition.CREATOR.createFromParcel((Parcel)iBinder17);
            } else {
              iBinder17 = null;
            } 
            registerRemoteAnimations(iBinder22, (RemoteAnimationDefinition)iBinder17);
            param1Parcel2.writeNoException();
            return true;
          case 136:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder17.readStrongBinder();
            bool40 = bool27;
            if (iBinder17.readInt() != 0)
              bool40 = true; 
            setTurnScreenOn(iBinder22, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 135:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder17.readStrongBinder();
            bool40 = bool28;
            if (iBinder17.readInt() != 0)
              bool40 = true; 
            setInheritShowWhenLocked(iBinder22, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 134:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder17.readStrongBinder();
            bool40 = bool29;
            if (iBinder17.readInt() != 0)
              bool40 = true; 
            setShowWhenLocked(iBinder22, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 133:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = iBinder17.readInt();
            i19 = iBinder17.readInt();
            updateLockTaskFeatures(param1Int2, i19);
            param1Parcel2.writeNoException();
            return true;
          case 132:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            if (iBinder17.readInt() != 0) {
              Configuration configuration = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)iBinder17);
            } else {
              iBinder17 = null;
            } 
            bool23 = updateConfiguration((Configuration)iBinder17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool23);
            return true;
          case 131:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            i18 = getLastResumedActivityUserId();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i18);
            return true;
          case 130:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder17 = iBinder17.readStrongBinder();
            invalidateHomeTaskSnapshot(iBinder17);
            param1Parcel2.writeNoException();
            return true;
          case 129:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder17.readStrongBinder();
            bool40 = bool30;
            if (iBinder17.readInt() != 0)
              bool40 = true; 
            setDisablePreviewScreenshots(iBinder22, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 128:
            iBinder17.enforceInterface("android.app.IActivityTaskManager");
            i18 = iBinder17.readInt();
            if (iBinder17.readInt() != 0) {
              bool40 = true;
            } else {
              bool40 = false;
            } 
            taskSnapshot = getTaskSnapshot(i18, bool40);
            param1Parcel2.writeNoException();
            if (taskSnapshot != null) {
              param1Parcel2.writeInt(1);
              taskSnapshot.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 127:
            taskSnapshot.enforceInterface("android.app.IActivityTaskManager");
            i18 = taskSnapshot.readInt();
            cancelTaskWindowTransition(i18);
            param1Parcel2.writeNoException();
            return true;
          case 126:
            taskSnapshot.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = taskSnapshot.readStrongBinder();
            iKeyguardDismissCallback = IKeyguardDismissCallback.Stub.asInterface(taskSnapshot.readStrongBinder());
            if (taskSnapshot.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)taskSnapshot);
            } else {
              taskSnapshot = null;
            } 
            dismissKeyguard(iBinder22, iKeyguardDismissCallback, (CharSequence)taskSnapshot);
            param1Parcel2.writeNoException();
            return true;
          case 125:
            taskSnapshot.enforceInterface("android.app.IActivityTaskManager");
            configurationInfo = getDeviceConfigurationInfo();
            param1Parcel2.writeNoException();
            if (configurationInfo != null) {
              param1Parcel2.writeInt(1);
              configurationInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 124:
            configurationInfo.enforceInterface("android.app.IActivityTaskManager");
            bool22 = supportsLocalVoiceInteraction();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool22);
            return true;
          case 123:
            configurationInfo.enforceInterface("android.app.IActivityTaskManager");
            iBinder16 = configurationInfo.readStrongBinder();
            stopLocalVoiceInteraction(iBinder16);
            param1Parcel2.writeNoException();
            return true;
          case 122:
            iBinder16.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder16.readStrongBinder();
            if (iBinder16.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder16);
            } else {
              iBinder16 = null;
            } 
            startLocalVoiceInteraction(iBinder22, (Bundle)iBinder16);
            param1Parcel2.writeNoException();
            return true;
          case 121:
            iBinder16.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder16.readStrongBinder();
            bool40 = bool31;
            if (iBinder16.readInt() != 0)
              bool40 = true; 
            if (iBinder16.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iBinder16);
            } else {
              iBinder16 = null;
            } 
            i17 = setVrMode(iBinder22, bool40, (ComponentName)iBinder16);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i17);
            return true;
          case 120:
            iBinder16.enforceInterface("android.app.IActivityTaskManager");
            bool40 = bool32;
            if (iBinder16.readInt() != 0)
              bool40 = true; 
            setSplitScreenResizing(bool40);
            param1Parcel2.writeNoException();
            return true;
          case 119:
            iBinder16.enforceInterface("android.app.IActivityTaskManager");
            iWindowOrganizerController = getWindowOrganizerController();
            param1Parcel2.writeNoException();
            if (iWindowOrganizerController != null) {
              IBinder iBinder = iWindowOrganizerController.asBinder();
            } else {
              iWindowOrganizerController = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iWindowOrganizerController);
            return true;
          case 118:
            iWindowOrganizerController.enforceInterface("android.app.IActivityTaskManager");
            if (iWindowOrganizerController.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowOrganizerController);
            } else {
              iBinder22 = null;
            } 
            if (iWindowOrganizerController.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowOrganizerController);
            } else {
              iKeyguardDismissCallback = null;
            } 
            if (iWindowOrganizerController.readInt() != 0) {
              rect2 = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowOrganizerController);
            } else {
              rect2 = null;
            } 
            if (iWindowOrganizerController.readInt() != 0) {
              rect3 = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowOrganizerController);
            } else {
              rect3 = null;
            } 
            if (iWindowOrganizerController.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iWindowOrganizerController);
            } else {
              iWindowOrganizerController = null;
            } 
            resizeDockedStack((Rect)iBinder22, (Rect)iKeyguardDismissCallback, rect2, rect3, (Rect)iWindowOrganizerController);
            param1Parcel2.writeNoException();
            return true;
          case 117:
            iWindowOrganizerController.enforceInterface("android.app.IActivityTaskManager");
            iBinder15 = iWindowOrganizerController.readStrongBinder();
            iBinder15 = getUriPermissionOwnerForActivity(iBinder15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder15);
            return true;
          case 116:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            iBinder15 = iBinder15.readStrongBinder();
            i17 = getMaxNumPictureInPictureActions(iBinder15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i17);
            return true;
          case 115:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            iBinder15 = iBinder15.readStrongBinder();
            requestPictureInPictureMode(iBinder15);
            param1Parcel2.writeNoException();
            return true;
          case 114:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder15.readStrongBinder();
            if (iBinder15.readInt() != 0) {
              PictureInPictureParams pictureInPictureParams = (PictureInPictureParams)PictureInPictureParams.CREATOR.createFromParcel((Parcel)iBinder15);
            } else {
              iBinder15 = null;
            } 
            setPictureInPictureParams(iBinder22, (PictureInPictureParams)iBinder15);
            param1Parcel2.writeNoException();
            return true;
          case 113:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder15.readStrongBinder();
            if (iBinder15.readInt() != 0) {
              PictureInPictureParams pictureInPictureParams = (PictureInPictureParams)PictureInPictureParams.CREATOR.createFromParcel((Parcel)iBinder15);
            } else {
              iBinder15 = null;
            } 
            bool21 = enterPictureInPictureMode(iBinder22, (PictureInPictureParams)iBinder15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool21);
            return true;
          case 112:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            i16 = iBinder15.readInt();
            if (iBinder15.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)iBinder15);
            } else {
              iBinder15 = null;
            } 
            bool20 = moveTopActivityToPinnedStack(i16, (Rect)iBinder15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 111:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            bool40 = bool33;
            if (iBinder15.readInt() != 0)
              bool40 = true; 
            suppressResizeConfigChanges(bool40);
            param1Parcel2.writeNoException();
            return true;
          case 110:
            iBinder15.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder15.readStrongBinder();
            arrayOfInt3 = iBinder15.createIntArray();
            arrayOfInt4 = iBinder15.createIntArray();
            arrayOfInt2 = iBinder15.createIntArray();
            reportSizeConfigurations(iBinder22, arrayOfInt3, arrayOfInt4, arrayOfInt2);
            param1Parcel2.writeNoException();
            return true;
          case 109:
            arrayOfInt2.enforceInterface("android.app.IActivityTaskManager");
            i20 = arrayOfInt2.readInt();
            param1Int2 = arrayOfInt2.readInt();
            i15 = arrayOfInt2.readInt();
            positionTaskInStack(i20, param1Int2, i15);
            param1Parcel2.writeNoException();
            return true;
          case 108:
            arrayOfInt2.enforceInterface("android.app.IActivityTaskManager");
            iBinder14 = arrayOfInt2.readStrongBinder();
            str5 = getPackageForToken(iBinder14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str5);
            return true;
          case 107:
            str5.enforceInterface("android.app.IActivityTaskManager");
            iBinder13 = str5.readStrongBinder();
            componentName2 = getActivityClassForToken(iBinder13);
            param1Parcel2.writeNoException();
            if (componentName2 != null) {
              param1Parcel2.writeInt(1);
              componentName2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 106:
            componentName2.enforceInterface("android.app.IActivityTaskManager");
            i15 = componentName2.readInt();
            keyguardGoingAway(i15);
            param1Parcel2.writeNoException();
            return true;
          case 105:
            componentName2.enforceInterface("android.app.IActivityTaskManager");
            iBinder12 = componentName2.readStrongBinder();
            showLockTaskEscapeMessage(iBinder12);
            return true;
          case 104:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            iBinder12 = iBinder12.readStrongBinder();
            bool19 = isRootVoiceInteraction(iBinder12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 103:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            iBinder22 = iBinder12.readStrongBinder();
            if (iBinder12.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder12);
            } else {
              iBinder12 = null;
            } 
            bool19 = showAssistFromActivity(iBinder22, (Bundle)iBinder12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 102:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            bool19 = isAssistDataAllowedOnCurrentActivity();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 101:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            iAssistDataReceiver2 = IAssistDataReceiver.Stub.asInterface(iBinder12.readStrongBinder());
            if (iBinder12.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder12);
            } else {
              iBinder22 = null;
            } 
            iBinder28 = iBinder12.readStrongBinder();
            i14 = iBinder12.readInt();
            bool18 = requestAutofillData(iAssistDataReceiver2, (Bundle)iBinder22, iBinder28, i14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 100:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            i13 = iBinder12.readInt();
            iAssistDataReceiver2 = IAssistDataReceiver.Stub.asInterface(iBinder12.readStrongBinder());
            if (iBinder12.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder12);
            } else {
              iBinder22 = null;
            } 
            iBinder28 = iBinder12.readStrongBinder();
            if (iBinder12.readInt() != 0) {
              bool40 = true;
            } else {
              bool40 = false;
            } 
            if (iBinder12.readInt() != 0) {
              bool34 = true;
            } else {
              bool34 = false;
            } 
            bool17 = requestAssistContextExtras(i13, iAssistDataReceiver2, (Bundle)iBinder22, iBinder28, bool40, bool34);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 99:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            if (iBinder12.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder12);
            } else {
              iBinder22 = null;
            } 
            param1Int2 = iBinder12.readInt();
            str13 = iBinder12.readString();
            i12 = iBinder12.readInt();
            if (iBinder12.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder12);
            } else {
              iBinder12 = null;
            } 
            bool16 = launchAssistIntent((Intent)iBinder22, param1Int2, str13, i12, (Bundle)iBinder12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 98:
            iBinder12.enforceInterface("android.app.IActivityTaskManager");
            i11 = iBinder12.readInt();
            bundle2 = getAssistContextExtras(i11);
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 97:
            bundle2.enforceInterface("android.app.IActivityTaskManager");
            if (bundle2.readInt() != 0) {
              bool40 = true;
            } else {
              bool40 = false;
            } 
            if (bundle2.readInt() != 0)
              bool34 = true; 
            setLockScreenShown(bool40, bool34);
            param1Parcel2.writeNoException();
            return true;
          case 96:
            bundle2.enforceInterface("android.app.IActivityTaskManager");
            i20 = bundle2.readInt();
            param1Int2 = bundle2.readInt();
            i11 = bundle2.readInt();
            stackInfo3 = getStackInfoOnDisplay(i20, param1Int2, i11);
            param1Parcel2.writeNoException();
            if (stackInfo3 != null) {
              param1Parcel2.writeInt(1);
              stackInfo3.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 95:
            stackInfo3.enforceInterface("android.app.IActivityTaskManager");
            i11 = stackInfo3.readInt();
            list3 = getAllStackInfosOnDisplay(i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 94:
            list3.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = list3.readInt();
            i11 = list3.readInt();
            stackInfo2 = getStackInfo(param1Int2, i11);
            param1Parcel2.writeNoException();
            if (stackInfo2 != null) {
              param1Parcel2.writeInt(1);
              stackInfo2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 93:
            stackInfo2.enforceInterface("android.app.IActivityTaskManager");
            list2 = getAllStackInfos();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 92:
            list2.enforceInterface("android.app.IActivityTaskManager");
            arrayOfInt1 = list2.createIntArray();
            removeStacksWithActivityTypes(arrayOfInt1);
            param1Parcel2.writeNoException();
            return true;
          case 91:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            arrayOfInt1 = arrayOfInt1.createIntArray();
            removeStacksInWindowingModes(arrayOfInt1);
            param1Parcel2.writeNoException();
            return true;
          case 90:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            i11 = arrayOfInt1.readInt();
            bool40 = bool35;
            if (arrayOfInt1.readInt() != 0)
              bool40 = true; 
            bool15 = setTaskWindowingModeSplitScreenPrimary(i11, bool40);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 89:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = arrayOfInt1.readInt();
            i10 = arrayOfInt1.readInt();
            bool40 = bool36;
            if (arrayOfInt1.readInt() != 0)
              bool40 = true; 
            moveTaskToStack(param1Int2, i10, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 88:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            i10 = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            bool40 = bool37;
            if (arrayOfInt1.readInt() != 0)
              bool40 = true; 
            bool14 = setTaskWindowingMode(i10, param1Int2, bool40);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 87:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            i9 = arrayOfInt1.readInt();
            removeStack(i9);
            param1Parcel2.writeNoException();
            return true;
          case 86:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            i9 = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            moveStackToDisplay(i9, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 85:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = arrayOfInt1.readInt();
            if (arrayOfInt1.readInt() != 0) {
              Rect rect = (Rect)Rect.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              iBinder22 = null;
            } 
            i9 = arrayOfInt1.readInt();
            bool13 = resizeTask(param1Int2, (Rect)iBinder22, i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 84:
            arrayOfInt1.enforceInterface("android.app.IActivityTaskManager");
            iBinder11 = arrayOfInt1.readStrongBinder();
            toggleFreeformWindowingMode(iBinder11);
            param1Parcel2.writeNoException();
            return true;
          case 83:
            iBinder11.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = iBinder11.readInt();
            i8 = iBinder11.readInt();
            setTaskResizeable(param1Int2, i8);
            param1Parcel2.writeNoException();
            return true;
          case 82:
            iBinder11.enforceInterface("android.app.IActivityTaskManager");
            iTaskStackListener = ITaskStackListener.Stub.asInterface(iBinder11.readStrongBinder());
            unregisterTaskStackListener(iTaskStackListener);
            param1Parcel2.writeNoException();
            return true;
          case 81:
            iTaskStackListener.enforceInterface("android.app.IActivityTaskManager");
            iTaskStackListener = ITaskStackListener.Stub.asInterface(iTaskStackListener.readStrongBinder());
            registerTaskStackListener(iTaskStackListener);
            param1Parcel2.writeNoException();
            return true;
          case 80:
            iTaskStackListener.enforceInterface("android.app.IActivityTaskManager");
            str8 = iTaskStackListener.readString();
            i8 = iTaskStackListener.readInt();
            bitmap = getTaskDescriptionIcon(str8, i8);
            param1Parcel2.writeNoException();
            if (bitmap != null) {
              param1Parcel2.writeInt(1);
              bitmap.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 79:
            bitmap.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread1 = IApplicationThread.Stub.asInterface(bitmap.readStrongBinder());
            releaseSomeActivities(iApplicationThread1);
            param1Parcel2.writeNoException();
            return true;
          case 78:
            iApplicationThread1.enforceInterface("android.app.IActivityTaskManager");
            iBinder10 = iApplicationThread1.readStrongBinder();
            iBinder10 = requestStartActivityPermissionToken(iBinder10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStrongBinder(iBinder10);
            return true;
          case 77:
            iBinder10.enforceInterface("android.app.IActivityTaskManager");
            iBinder10 = iBinder10.readStrongBinder();
            bool12 = releaseActivityInstance(iBinder10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 76:
            iBinder10.enforceInterface("android.app.IActivityTaskManager");
            point = getAppTaskThumbnailSize();
            param1Parcel2.writeNoException();
            if (point != null) {
              param1Parcel2.writeInt(1);
              point.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 75:
            point.enforceInterface("android.app.IActivityTaskManager");
            iBinder28 = point.readStrongBinder();
            if (point.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)point);
            } else {
              str8 = null;
            } 
            if (point.readInt() != 0) {
              ActivityManager.TaskDescription taskDescription1 = (ActivityManager.TaskDescription)ActivityManager.TaskDescription.CREATOR.createFromParcel((Parcel)point);
            } else {
              str13 = null;
            } 
            if (point.readInt() != 0) {
              Bitmap bitmap1 = (Bitmap)Bitmap.CREATOR.createFromParcel((Parcel)point);
            } else {
              point = null;
            } 
            i7 = addAppTask(iBinder28, (Intent)str8, (ActivityManager.TaskDescription)str13, (Bitmap)point);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i7);
            return true;
          case 74:
            point.enforceInterface("android.app.IActivityTaskManager");
            iBinder9 = point.readStrongBinder();
            notifyEnterAnimationComplete(iBinder9);
            param1Parcel2.writeNoException();
            return true;
          case 73:
            iBinder9.enforceInterface("android.app.IActivityTaskManager");
            iBinder9 = iBinder9.readStrongBinder();
            notifyLaunchTaskBehindComplete(iBinder9);
            param1Parcel2.writeNoException();
            return true;
          case 72:
            iBinder9.enforceInterface("android.app.IActivityTaskManager");
            iBinder9 = iBinder9.readStrongBinder();
            bool11 = isTopOfTask(iBinder9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 71:
            iBinder9.enforceInterface("android.app.IActivityTaskManager");
            iVoiceInteractionSession1 = IVoiceInteractionSession.Stub.asInterface(iBinder9.readStrongBinder());
            finishVoiceTask(iVoiceInteractionSession1);
            param1Parcel2.writeNoException();
            return true;
          case 70:
            iVoiceInteractionSession1.enforceInterface("android.app.IActivityTaskManager");
            stopSystemLockTaskMode();
            param1Parcel2.writeNoException();
            return true;
          case 69:
            iVoiceInteractionSession1.enforceInterface("android.app.IActivityTaskManager");
            i6 = iVoiceInteractionSession1.readInt();
            startSystemLockTaskMode(i6);
            param1Parcel2.writeNoException();
            return true;
          case 68:
            iVoiceInteractionSession1.enforceInterface("android.app.IActivityTaskManager");
            str4 = iVoiceInteractionSession1.readString();
            list1 = getAppTasks(str4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeBinderList(list1);
            return true;
          case 67:
            list1.enforceInterface("android.app.IActivityTaskManager");
            iBinder8 = list1.readStrongBinder();
            bundle1 = getActivityOptions(iBinder8);
            param1Parcel2.writeNoException();
            if (bundle1 != null) {
              param1Parcel2.writeInt(1);
              bundle1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 66:
            bundle1.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = bundle1.readStrongBinder();
            if (bundle1.readInt() != 0) {
              ActivityManager.TaskDescription taskDescription1 = (ActivityManager.TaskDescription)ActivityManager.TaskDescription.CREATOR.createFromParcel((Parcel)bundle1);
            } else {
              bundle1 = null;
            } 
            setTaskDescription(iBinder21, (ActivityManager.TaskDescription)bundle1);
            param1Parcel2.writeNoException();
            return true;
          case 65:
            bundle1.enforceInterface("android.app.IActivityTaskManager");
            i6 = getLockTaskModeState();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 64:
            bundle1.enforceInterface("android.app.IActivityTaskManager");
            bool10 = isInLockTaskMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 63:
            bundle1.enforceInterface("android.app.IActivityTaskManager");
            i5 = bundle1.readInt();
            arrayOfString1 = bundle1.createStringArray();
            updateLockTaskPackages(i5, arrayOfString1);
            param1Parcel2.writeNoException();
            return true;
          case 62:
            arrayOfString1.enforceInterface("android.app.IActivityTaskManager");
            iBinder7 = arrayOfString1.readStrongBinder();
            stopLockTaskModeByToken(iBinder7);
            param1Parcel2.writeNoException();
            return true;
          case 61:
            iBinder7.enforceInterface("android.app.IActivityTaskManager");
            iBinder7 = iBinder7.readStrongBinder();
            startLockTaskModeByToken(iBinder7);
            param1Parcel2.writeNoException();
            return true;
          case 60:
            iBinder7.enforceInterface("android.app.IActivityTaskManager");
            bool40 = bool38;
            if (iBinder7.readInt() != 0)
              bool40 = true; 
            cancelRecentsAnimation(bool40);
            param1Parcel2.writeNoException();
            return true;
          case 59:
            iBinder7.enforceInterface("android.app.IActivityTaskManager");
            i5 = iBinder7.readInt();
            rect1 = getTaskBounds(i5);
            param1Parcel2.writeNoException();
            if (rect1 != null) {
              param1Parcel2.writeInt(1);
              rect1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 58:
            rect1.enforceInterface("android.app.IActivityTaskManager");
            stackInfo1 = getFocusedStackInfo();
            param1Parcel2.writeNoException();
            if (stackInfo1 != null) {
              param1Parcel2.writeInt(1);
              stackInfo1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 57:
            stackInfo1.enforceInterface("android.app.IActivityTaskManager");
            i5 = stackInfo1.readInt();
            setFocusedStack(i5);
            param1Parcel2.writeNoException();
            return true;
          case 56:
            stackInfo1.enforceInterface("android.app.IActivityTaskManager");
            iBinder30 = stackInfo1.readStrongBinder();
            if (stackInfo1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)stackInfo1);
            } else {
              iBinder21 = null;
            } 
            if (stackInfo1.readInt() != 0) {
              AssistStructure assistStructure = (AssistStructure)AssistStructure.CREATOR.createFromParcel((Parcel)stackInfo1);
            } else {
              str13 = null;
            } 
            if (stackInfo1.readInt() != 0) {
              AssistContent assistContent = (AssistContent)AssistContent.CREATOR.createFromParcel((Parcel)stackInfo1);
            } else {
              iBinder28 = null;
            } 
            if (stackInfo1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)stackInfo1);
            } else {
              stackInfo1 = null;
            } 
            reportAssistContextExtras(iBinder30, (Bundle)iBinder21, (AssistStructure)str13, (AssistContent)iBinder28, (Uri)stackInfo1);
            param1Parcel2.writeNoException();
            return true;
          case 55:
            stackInfo1.enforceInterface("android.app.IActivityTaskManager");
            iBinder6 = stackInfo1.readStrongBinder();
            str3 = getLaunchedFromPackage(iBinder6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str3);
            return true;
          case 54:
            str3.enforceInterface("android.app.IActivityTaskManager");
            iBinder5 = str3.readStrongBinder();
            i5 = getLaunchedFromUid(iBinder5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 53:
            iBinder5.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = iBinder5.readStrongBinder();
            str13 = iBinder5.readString();
            param1Int2 = iBinder5.readInt();
            i5 = iBinder5.readInt();
            overridePendingTransition(iBinder21, str13, param1Int2, i5);
            param1Parcel2.writeNoException();
            return true;
          case 52:
            iBinder5.enforceInterface("android.app.IActivityTaskManager");
            i5 = iBinder5.readInt();
            taskDescription = getTaskDescription(i5);
            param1Parcel2.writeNoException();
            if (taskDescription != null) {
              param1Parcel2.writeInt(1);
              taskDescription.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 51:
            taskDescription.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = taskDescription.readStrongBinder();
            bool40 = bool39;
            if (taskDescription.readInt() != 0)
              bool40 = true; 
            bool9 = moveActivityTaskToBack(iBinder21, bool40);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 50:
            taskDescription.enforceInterface("android.app.IActivityTaskManager");
            bool9 = isTopActivityImmersive();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 49:
            taskDescription.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = taskDescription.readStrongBinder();
            if (taskDescription.readInt() != 0)
              bool40 = true; 
            setImmersive(iBinder21, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 48:
            taskDescription.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = taskDescription.readStrongBinder();
            bool9 = isImmersive(iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 47:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = iBinder4.readStrongBinder();
            i4 = getDisplayId(iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i4);
            return true;
          case 46:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = iBinder4.readStrongBinder();
            bool40 = bool41;
            if (iBinder4.readInt() != 0)
              bool40 = true; 
            reportActivityFullyDrawn(iBinder21, bool40);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = iBinder4.readStrongBinder();
            notifyActivityDrawn(iBinder4);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = iBinder4.readStrongBinder();
            if (iBinder4.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder4);
            } else {
              iBinder4 = null;
            } 
            bool8 = convertToTranslucent(iBinder21, (Bundle)iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 43:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = iBinder4.readStrongBinder();
            bool8 = convertFromTranslucent(iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool8);
            return true;
          case 42:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = iBinder4.readStrongBinder();
            i3 = getRequestedOrientation(iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i3);
            return true;
          case 41:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder21 = iBinder4.readStrongBinder();
            i3 = iBinder4.readInt();
            setRequestedOrientation(iBinder21, i3);
            param1Parcel2.writeNoException();
            return true;
          case 40:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            iBinder4 = iBinder4.readStrongBinder();
            bool7 = willActivityBeVisible(iBinder4);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 39:
            iBinder4.enforceInterface("android.app.IActivityTaskManager");
            i2 = iBinder4.readInt();
            i20 = iBinder4.readInt();
            param1Int2 = iBinder4.readInt();
            parceledListSlice = getRecentTasks(i2, i20, param1Int2);
            param1Parcel2.writeNoException();
            if (parceledListSlice != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 38:
            parceledListSlice.enforceInterface("android.app.IActivityTaskManager");
            iBinder26 = parceledListSlice.readStrongBinder();
            str7 = parceledListSlice.readString();
            i2 = parceledListSlice.readInt();
            finishSubActivity(iBinder26, str7, i2);
            param1Parcel2.writeNoException();
            return true;
          case 37:
            parceledListSlice.enforceInterface("android.app.IActivityTaskManager");
            iBinder20 = parceledListSlice.readStrongBinder();
            bool40 = bool42;
            if (parceledListSlice.readInt() != 0)
              bool40 = true; 
            i2 = getTaskForActivity(iBinder20, bool40);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i2);
            return true;
          case 36:
            parceledListSlice.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread2 = IApplicationThread.Stub.asInterface(parceledListSlice.readStrongBinder());
            str12 = parceledListSlice.readString();
            i2 = parceledListSlice.readInt();
            param1Int2 = parceledListSlice.readInt();
            if (parceledListSlice.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              parceledListSlice = null;
            } 
            moveTaskToFront(iApplicationThread2, str12, i2, param1Int2, (Bundle)parceledListSlice);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            parceledListSlice.enforceInterface("android.app.IActivityTaskManager");
            iBinder25 = parceledListSlice.readStrongBinder();
            if (parceledListSlice.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              iApplicationThread2 = null;
            } 
            i2 = parceledListSlice.readInt();
            if (parceledListSlice.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              parceledListSlice = null;
            } 
            bool6 = navigateUpTo(iBinder25, (Intent)iApplicationThread2, i2, (Intent)parceledListSlice);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 34:
            parceledListSlice.enforceInterface("android.app.IActivityTaskManager");
            iBinder19 = parceledListSlice.readStrongBinder();
            str2 = parceledListSlice.readString();
            bool6 = shouldUpRecreateTask(iBinder19, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 33:
            str2.enforceInterface("android.app.IActivityTaskManager");
            i1 = str2.readInt();
            bool40 = bool43;
            if (str2.readInt() != 0)
              bool40 = true; 
            list = getFilteredTasks(i1, bool40);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 32:
            list.enforceInterface("android.app.IActivityTaskManager");
            i1 = list.readInt();
            list = getTasks(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 31:
            list.enforceInterface("android.app.IActivityTaskManager");
            removeAllVisibleRecentTasks();
            param1Parcel2.writeNoException();
            return true;
          case 30:
            list.enforceInterface("android.app.IActivityTaskManager");
            i1 = list.readInt();
            bool5 = removeTask(i1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 29:
            list.enforceInterface("android.app.IActivityTaskManager");
            n = list.readInt();
            setFocusedTask(n);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            list.enforceInterface("android.app.IActivityTaskManager");
            iBinder3 = list.readStrongBinder();
            componentName1 = getCallingActivity(iBinder3);
            param1Parcel2.writeNoException();
            if (componentName1 != null) {
              param1Parcel2.writeInt(1);
              componentName1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 27:
            componentName1.enforceInterface("android.app.IActivityTaskManager");
            iBinder2 = componentName1.readStrongBinder();
            str1 = getCallingPackage(iBinder2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 26:
            str1.enforceInterface("android.app.IActivityTaskManager");
            n = str1.readInt();
            setFrontActivityScreenCompatMode(n);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str1.enforceInterface("android.app.IActivityTaskManager");
            n = getFrontActivityScreenCompatMode();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(n);
            return true;
          case 24:
            str1.enforceInterface("android.app.IActivityTaskManager");
            iBinder1 = str1.readStrongBinder();
            activityRelaunched(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder1 = iBinder1.readStrongBinder();
            activityDestroyed(iBinder1);
            return true;
          case 22:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder28 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder19 = null;
            } 
            if (iBinder1.readInt() != 0) {
              PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder25 = null;
            } 
            if (iBinder1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            activityStopped(iBinder28, (Bundle)iBinder19, (PersistableBundle)iBinder25, (CharSequence)iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder1 = iBinder1.readStrongBinder();
            activityPaused(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            activityTopResumedStateLost();
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder1 = iBinder1.readStrongBinder();
            activityResumed(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder19 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              Configuration configuration = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              param1Parcel2 = null;
            } 
            bool40 = bool44;
            if (iBinder1.readInt() != 0)
              bool40 = true; 
            activityIdle(iBinder19, (Configuration)param1Parcel2, bool40);
            return true;
          case 17:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder1 = iBinder1.readStrongBinder();
            bool4 = finishActivityAffinity(iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 16:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iBinder25 = iBinder1.readStrongBinder();
            param1Int2 = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder19 = null;
            } 
            m = iBinder1.readInt();
            bool3 = finishActivity(iBinder25, param1Int2, (Intent)iBinder19, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 15:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            unhandledBack();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            param1Int2 = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder19 = null;
            } 
            str11 = iBinder1.readString();
            k = iBinder1.readInt();
            bool2 = isActivityStartAllowedOnDisplay(param1Int2, (Intent)iBinder19, str11, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 13:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread5 = IApplicationThread.Stub.asInterface(iBinder1.readStrongBinder());
            str20 = iBinder1.readString();
            if (iBinder1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder19 = null;
            } 
            str22 = iBinder1.readString();
            iBinder32 = iBinder1.readStrongBinder();
            str26 = iBinder1.readString();
            param1Int2 = iBinder1.readInt();
            i20 = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              str11 = null;
            } 
            if (iBinder1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder28 = null;
            } 
            iBinder35 = iBinder1.readStrongBinder();
            bool40 = bool45;
            if (iBinder1.readInt() != 0)
              bool40 = true; 
            j = iBinder1.readInt();
            j = startActivityAsCaller(iApplicationThread5, str20, (Intent)iBinder19, str22, iBinder32, str26, param1Int2, i20, (ProfilerInfo)str11, (Bundle)iBinder28, iBinder35, bool40, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 12:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            j = iBinder1.readInt();
            if (iBinder1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            j = startActivityFromRecents(j, (Bundle)iBinder1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 11:
            iBinder1.enforceInterface("android.app.IActivityTaskManager");
            if (iBinder1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder19 = null;
            } 
            iAssistDataReceiver1 = IAssistDataReceiver.Stub.asInterface(iBinder1.readStrongBinder());
            iRecentsAnimationRunner = IRecentsAnimationRunner.Stub.asInterface(iBinder1.readStrongBinder());
            startRecentsActivity((Intent)iBinder19, iAssistDataReceiver1, iRecentsAnimationRunner);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityTaskManager");
            str17 = iRecentsAnimationRunner.readString();
            str14 = iRecentsAnimationRunner.readString();
            i20 = iRecentsAnimationRunner.readInt();
            param1Int2 = iRecentsAnimationRunner.readInt();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iBinder19 = null;
            } 
            str20 = iRecentsAnimationRunner.readString();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iAssistDataReceiver1 = null;
            } 
            j = iRecentsAnimationRunner.readInt();
            j = startAssistantActivity(str17, str14, i20, param1Int2, (Intent)iBinder19, str20, (Bundle)iAssistDataReceiver1, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 9:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityTaskManager");
            str17 = iRecentsAnimationRunner.readString();
            str20 = iRecentsAnimationRunner.readString();
            param1Int2 = iRecentsAnimationRunner.readInt();
            i20 = iRecentsAnimationRunner.readInt();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iBinder19 = null;
            } 
            str23 = iRecentsAnimationRunner.readString();
            iVoiceInteractionSession3 = IVoiceInteractionSession.Stub.asInterface(iRecentsAnimationRunner.readStrongBinder());
            iVoiceInteractor = IVoiceInteractor.Stub.asInterface(iRecentsAnimationRunner.readStrongBinder());
            i21 = iRecentsAnimationRunner.readInt();
            if (iRecentsAnimationRunner.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iAssistDataReceiver1 = null;
            } 
            if (iRecentsAnimationRunner.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              str14 = null;
            } 
            j = iRecentsAnimationRunner.readInt();
            j = startVoiceActivity(str17, str20, param1Int2, i20, (Intent)iBinder19, str23, iVoiceInteractionSession3, iVoiceInteractor, i21, (ProfilerInfo)iAssistDataReceiver1, (Bundle)str14, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 8:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread7 = IApplicationThread.Stub.asInterface(iRecentsAnimationRunner.readStrongBinder());
            str17 = iRecentsAnimationRunner.readString();
            str23 = iRecentsAnimationRunner.readString();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iBinder19 = null;
            } 
            str21 = iRecentsAnimationRunner.readString();
            iBinder35 = iRecentsAnimationRunner.readStrongBinder();
            str25 = iRecentsAnimationRunner.readString();
            i20 = iRecentsAnimationRunner.readInt();
            param1Int2 = iRecentsAnimationRunner.readInt();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Configuration configuration = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iAssistDataReceiver1 = null;
            } 
            if (iRecentsAnimationRunner.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              str14 = null;
            } 
            j = iRecentsAnimationRunner.readInt();
            j = startActivityWithConfig(iApplicationThread7, str17, str23, (Intent)iBinder19, str21, iBinder35, str25, i20, param1Int2, (Configuration)iAssistDataReceiver1, (Bundle)str14, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 7:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread7 = IApplicationThread.Stub.asInterface(iRecentsAnimationRunner.readStrongBinder());
            str17 = iRecentsAnimationRunner.readString();
            str23 = iRecentsAnimationRunner.readString();
            if (iRecentsAnimationRunner.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iBinder19 = null;
            } 
            str21 = iRecentsAnimationRunner.readString();
            iBinder33 = iRecentsAnimationRunner.readStrongBinder();
            str27 = iRecentsAnimationRunner.readString();
            j = iRecentsAnimationRunner.readInt();
            param1Int2 = iRecentsAnimationRunner.readInt();
            if (iRecentsAnimationRunner.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              iAssistDataReceiver1 = null;
            } 
            if (iRecentsAnimationRunner.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iRecentsAnimationRunner);
            } else {
              str14 = null;
            } 
            i20 = iRecentsAnimationRunner.readInt();
            waitResult = startActivityAndWait(iApplicationThread7, str17, str23, (Intent)iBinder19, str21, iBinder33, str27, j, param1Int2, (ProfilerInfo)iAssistDataReceiver1, (Bundle)str14, i20);
            param1Parcel2.writeNoException();
            if (waitResult != null) {
              param1Parcel2.writeInt(1);
              waitResult.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            waitResult.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread4 = IApplicationThread.Stub.asInterface(waitResult.readStrongBinder());
            iIntentSender = IIntentSender.Stub.asInterface(waitResult.readStrongBinder());
            iBinder27 = waitResult.readStrongBinder();
            if (waitResult.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder19 = null;
            } 
            str23 = waitResult.readString();
            iBinder33 = waitResult.readStrongBinder();
            str19 = waitResult.readString();
            param1Int2 = waitResult.readInt();
            j = waitResult.readInt();
            i20 = waitResult.readInt();
            if (waitResult.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              waitResult = null;
            } 
            j = startActivityIntentSender(iApplicationThread4, iIntentSender, iBinder27, (Intent)iBinder19, str23, iBinder33, str19, param1Int2, j, i20, (Bundle)waitResult);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 5:
            waitResult.enforceInterface("android.app.IActivityTaskManager");
            if (waitResult.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              waitResult = null;
            } 
            bool1 = startDreamActivity((Intent)waitResult);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 4:
            waitResult.enforceInterface("android.app.IActivityTaskManager");
            iBinder24 = waitResult.readStrongBinder();
            if (waitResult.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder19 = null;
            } 
            if (waitResult.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              waitResult = null;
            } 
            bool1 = startNextMatchingActivity(iBinder24, (Intent)iBinder19, (Bundle)waitResult);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 3:
            waitResult.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread6 = IApplicationThread.Stub.asInterface(waitResult.readStrongBinder());
            str23 = waitResult.readString();
            str16 = waitResult.readString();
            if (waitResult.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder19 = null;
            } 
            str24 = waitResult.readString();
            iBinder34 = waitResult.readStrongBinder();
            str21 = waitResult.readString();
            param1Int2 = waitResult.readInt();
            i20 = waitResult.readInt();
            if (waitResult.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder24 = null;
            } 
            if (waitResult.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder27 = null;
            } 
            i = waitResult.readInt();
            i = startActivityAsUser(iApplicationThread6, str23, str16, (Intent)iBinder19, str24, iBinder34, str21, param1Int2, i20, (ProfilerInfo)iBinder24, (Bundle)iBinder27, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 2:
            waitResult.enforceInterface("android.app.IActivityTaskManager");
            iApplicationThread3 = IApplicationThread.Stub.asInterface(waitResult.readStrongBinder());
            str24 = waitResult.readString();
            str18 = waitResult.readString();
            arrayOfIntent = (Intent[])waitResult.createTypedArray(Intent.CREATOR);
            arrayOfString2 = waitResult.createStringArray();
            iBinder29 = waitResult.readStrongBinder();
            if (waitResult.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)waitResult);
            } else {
              iBinder19 = null;
            } 
            i = waitResult.readInt();
            i = startActivities(iApplicationThread3, str24, str18, arrayOfIntent, arrayOfString2, iBinder29, (Bundle)iBinder19, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 1:
            break;
        } 
        waitResult.enforceInterface("android.app.IActivityTaskManager");
        IApplicationThread iApplicationThread3 = IApplicationThread.Stub.asInterface(waitResult.readStrongBinder());
        String str15 = waitResult.readString();
        String str18 = waitResult.readString();
        if (waitResult.readInt() != 0) {
          Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)waitResult);
        } else {
          iBinder19 = null;
        } 
        String str21 = waitResult.readString();
        IBinder iBinder31 = waitResult.readStrongBinder();
        String str24 = waitResult.readString();
        param1Int2 = waitResult.readInt();
        int i = waitResult.readInt();
        if (waitResult.readInt() != 0) {
          ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)waitResult);
        } else {
          arrayOfString2 = null;
        } 
        if (waitResult.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)waitResult);
        } else {
          waitResult = null;
        } 
        i = startActivity(iApplicationThread3, str15, str18, (Intent)iBinder19, str21, iBinder31, str24, param1Int2, i, (ProfilerInfo)arrayOfString2, (Bundle)waitResult);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(i);
        return true;
      } 
      param1Parcel2.writeString("android.app.IActivityTaskManager");
      return true;
    }
    
    private static class Proxy implements IActivityTaskManager {
      public static IActivityTaskManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IActivityTaskManager";
      }
      
      public int startActivity(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startActivity(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startActivities(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent[] param2ArrayOfIntent, String[] param2ArrayOfString, IBinder param2IBinder, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeTypedArray((Parcelable[])param2ArrayOfIntent, 0);
                parcel1.writeStringArray(param2ArrayOfString);
                parcel1.writeStrongBinder(param2IBinder);
                if (param2Bundle != null) {
                  parcel1.writeInt(1);
                  param2Bundle.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                parcel1.writeInt(param2Int);
                boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
                  param2Int = IActivityTaskManager.Stub.getDefaultImpl().startActivities(param2IApplicationThread, param2String1, param2String2, param2ArrayOfIntent, param2ArrayOfString, param2IBinder, param2Bundle, param2Int);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int;
                } 
                parcel2.readException();
                param2Int = parcel2.readInt();
                parcel2.recycle();
                parcel1.recycle();
                return param2Int;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startActivityAsUser(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startActivityAsUser(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public boolean startNextMatchingActivity(IBinder param2IBinder, Intent param2Intent, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().startNextMatchingActivity(param2IBinder, param2Intent, param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startDreamActivity(Intent param2Intent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().startDreamActivity(param2Intent);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivityIntentSender(IApplicationThread param2IApplicationThread, IIntentSender param2IIntentSender, IBinder param2IBinder1, Intent param2Intent, String param2String1, IBinder param2IBinder2, String param2String2, int param2Int1, int param2Int2, int param2Int3, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            try {
              iBinder2 = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IIntentSender != null)
            iBinder2 = param2IIntentSender.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeStrongBinder(param2IBinder1);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeStrongBinder(param2IBinder2);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startActivityIntentSender(param2IApplicationThread, param2IIntentSender, param2IBinder1, param2Intent, param2String1, param2IBinder2, param2String2, param2Int1, param2Int2, param2Int3, param2Bundle);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public WaitResult startActivityAndWait(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool)
            try {
              if (IActivityTaskManager.Stub.getDefaultImpl() != null) {
                IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
                try {
                  WaitResult waitResult = iActivityTaskManager.startActivityAndWait(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle, param2Int3);
                  parcel2.recycle();
                  parcel1.recycle();
                  return waitResult;
                } finally {}
                parcel2.recycle();
                parcel1.recycle();
                throw param2IApplicationThread;
              } 
            } finally {} 
          try {
            parcel2.readException();
            if (parcel2.readInt() != 0) {
              Parcelable.Creator<WaitResult> creator = WaitResult.CREATOR;
              try {
                WaitResult waitResult = (WaitResult)creator.createFromParcel(parcel2);
              } finally {}
            } else {
              param2IApplicationThread = null;
            } 
            parcel2.recycle();
            parcel1.recycle();
            return (WaitResult)param2IApplicationThread;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startActivityWithConfig(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, Configuration param2Configuration, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Configuration != null) {
            parcel1.writeInt(1);
            param2Configuration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startActivityWithConfig(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2Configuration, param2Bundle, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startVoiceActivity(String param2String1, String param2String2, int param2Int1, int param2Int2, Intent param2Intent, String param2String3, IVoiceInteractionSession param2IVoiceInteractionSession, IVoiceInteractor param2IVoiceInteractor, int param2Int3, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Intent != null) {
            try {
              parcel1.writeInt(1);
              param2Intent.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          IBinder iBinder1 = null;
          if (param2IVoiceInteractionSession != null) {
            iBinder2 = param2IVoiceInteractionSession.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IVoiceInteractor != null)
            iBinder2 = param2IVoiceInteractor.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeInt(param2Int3);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startVoiceActivity(param2String1, param2String2, param2Int1, param2Int2, param2Intent, param2String3, param2IVoiceInteractionSession, param2IVoiceInteractor, param2Int3, param2ProfilerInfo, param2Bundle, param2Int4);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public int startAssistantActivity(String param2String1, String param2String2, int param2Int1, int param2Int2, Intent param2Intent, String param2String3, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              parcel1.writeInt(param2Int1);
              parcel1.writeInt(param2Int2);
              if (param2Intent != null) {
                parcel1.writeInt(1);
                param2Intent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              parcel1.writeString(param2String3);
              if (param2Bundle != null) {
                parcel1.writeInt(1);
                param2Bundle.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              parcel1.writeInt(param2Int3);
              boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
              if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
                param2Int1 = IActivityTaskManager.Stub.getDefaultImpl().startAssistantActivity(param2String1, param2String2, param2Int1, param2Int2, param2Intent, param2String3, param2Bundle, param2Int3);
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } 
              parcel2.readException();
              param2Int1 = parcel2.readInt();
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void startRecentsActivity(Intent param2Intent, IAssistDataReceiver param2IAssistDataReceiver, IRecentsAnimationRunner param2IRecentsAnimationRunner) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          IBinder iBinder1 = null;
          if (param2IAssistDataReceiver != null) {
            iBinder2 = param2IAssistDataReceiver.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IRecentsAnimationRunner != null)
            iBinder2 = param2IRecentsAnimationRunner.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().startRecentsActivity(param2Intent, param2IAssistDataReceiver, param2IRecentsAnimationRunner);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivityFromRecents(int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityTaskManager.Stub.getDefaultImpl().startActivityFromRecents(param2Int, param2Bundle);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivityAsCaller(IApplicationThread param2IApplicationThread, String param2String1, Intent param2Intent, String param2String2, IBinder param2IBinder1, String param2String3, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, IBinder param2IBinder2, boolean param2Boolean, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          boolean bool = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          parcel1.writeStrongBinder(param2IBinder1);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder2);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int3);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager iActivityTaskManager = IActivityTaskManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityTaskManager.startActivityAsCaller(param2IApplicationThread, param2String1, param2Intent, param2String2, param2IBinder1, param2String3, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle, param2IBinder2, param2Boolean, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public boolean isActivityStartAllowedOnDisplay(int param2Int1, Intent param2Intent, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isActivityStartAllowedOnDisplay(param2Int1, param2Intent, param2String, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unhandledBack() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().unhandledBack();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean finishActivity(IBinder param2IBinder, int param2Int1, Intent param2Intent, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().finishActivity(param2IBinder, param2Int1, param2Intent, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean finishActivityAffinity(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(17, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().finishActivityAffinity(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void activityIdle(IBinder param2IBinder, Configuration param2Configuration, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = false;
          if (param2Configuration != null) {
            parcel.writeInt(1);
            param2Configuration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(18, parcel, null, 1);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityIdle(param2IBinder, param2Configuration, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void activityResumed(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityResumed(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void activityTopResumedStateLost() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityTopResumedStateLost();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void activityPaused(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityPaused(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void activityStopped(IBinder param2IBinder, Bundle param2Bundle, PersistableBundle param2PersistableBundle, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2PersistableBundle != null) {
            parcel1.writeInt(1);
            param2PersistableBundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityStopped(param2IBinder, param2Bundle, param2PersistableBundle, param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void activityDestroyed(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(23, parcel, null, 1);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityDestroyed(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void activityRelaunched(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().activityRelaunched(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFrontActivityScreenCompatMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getFrontActivityScreenCompatMode(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFrontActivityScreenCompatMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setFrontActivityScreenCompatMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getCallingPackage(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getCallingPackage(param2IBinder); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getCallingActivity(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getCallingActivity(param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (ComponentName)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFocusedTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setFocusedTask(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(30, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().removeTask(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAllVisibleRecentTasks() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().removeAllVisibleRecentTasks();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningTaskInfo> getTasks(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getTasks(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningTaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningTaskInfo> getFilteredTasks(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getFilteredTasks(param2Int, param2Boolean); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningTaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean shouldUpRecreateTask(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().shouldUpRecreateTask(param2IBinder, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean navigateUpTo(IBinder param2IBinder, Intent param2Intent1, int param2Int, Intent param2Intent2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2Intent1 != null) {
            parcel1.writeInt(1);
            param2Intent1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          if (param2Intent2 != null) {
            parcel1.writeInt(1);
            param2Intent2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().navigateUpTo(param2IBinder, param2Intent1, param2Int, param2Intent2);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveTaskToFront(IApplicationThread param2IApplicationThread, String param2String, int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().moveTaskToFront(param2IApplicationThread, param2String, param2Int1, param2Int2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTaskForActivity(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            i = IActivityTaskManager.Stub.getDefaultImpl().getTaskForActivity(param2IBinder, param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishSubActivity(IBinder param2IBinder, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().finishSubActivity(param2IBinder, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getRecentTasks(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IActivityTaskManager.Stub.getDefaultImpl().getRecentTasks(param2Int1, param2Int2, param2Int3);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean willActivityBeVisible(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(40, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().willActivityBeVisible(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRequestedOrientation(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setRequestedOrientation(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRequestedOrientation(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getRequestedOrientation(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean convertFromTranslucent(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(43, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().convertFromTranslucent(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean convertToTranslucent(IBinder param2IBinder, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().convertToTranslucent(param2IBinder, param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyActivityDrawn(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().notifyActivityDrawn(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportActivityFullyDrawn(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().reportActivityFullyDrawn(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getDisplayId(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getDisplayId(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isImmersive(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(48, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isImmersive(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setImmersive(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setImmersive(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTopActivityImmersive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(50, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isTopActivityImmersive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean moveActivityTaskToBack(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityTaskManager.Stub.getDefaultImpl().moveActivityTaskToBack(param2IBinder, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.TaskDescription getTaskDescription(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.TaskDescription taskDescription;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            taskDescription = IActivityTaskManager.Stub.getDefaultImpl().getTaskDescription(param2Int);
            return taskDescription;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            taskDescription = (ActivityManager.TaskDescription)ActivityManager.TaskDescription.CREATOR.createFromParcel(parcel2);
          } else {
            taskDescription = null;
          } 
          return taskDescription;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overridePendingTransition(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().overridePendingTransition(param2IBinder, param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLaunchedFromUid(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getLaunchedFromUid(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLaunchedFromPackage(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getLaunchedFromPackage(param2IBinder); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportAssistContextExtras(IBinder param2IBinder, Bundle param2Bundle, AssistStructure param2AssistStructure, AssistContent param2AssistContent, Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AssistStructure != null) {
            parcel1.writeInt(1);
            param2AssistStructure.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AssistContent != null) {
            parcel1.writeInt(1);
            param2AssistContent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().reportAssistContextExtras(param2IBinder, param2Bundle, param2AssistStructure, param2AssistContent, param2Uri);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFocusedStack(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setFocusedStack(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.StackInfo stackInfo;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            stackInfo = IActivityTaskManager.Stub.getDefaultImpl().getFocusedStackInfo();
            return stackInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stackInfo = (ActivityManager.StackInfo)ActivityManager.StackInfo.CREATOR.createFromParcel(parcel2);
          } else {
            stackInfo = null;
          } 
          return stackInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Rect getTaskBounds(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Rect rect;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            rect = IActivityTaskManager.Stub.getDefaultImpl().getTaskBounds(param2Int);
            return rect;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            rect = (Rect)Rect.CREATOR.createFromParcel(parcel2);
          } else {
            rect = null;
          } 
          return rect;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelRecentsAnimation(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().cancelRecentsAnimation(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startLockTaskModeByToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().startLockTaskModeByToken(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopLockTaskModeByToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().stopLockTaskModeByToken(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateLockTaskPackages(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().updateLockTaskPackages(param2Int, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInLockTaskMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(64, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isInLockTaskMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLockTaskModeState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getLockTaskModeState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTaskDescription(IBinder param2IBinder, ActivityManager.TaskDescription param2TaskDescription) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2TaskDescription != null) {
            parcel1.writeInt(1);
            param2TaskDescription.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setTaskDescription(param2IBinder, param2TaskDescription);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getActivityOptions(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getActivityOptions(param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (Bundle)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<IBinder> getAppTasks(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getAppTasks(param2String); 
          parcel2.readException();
          return parcel2.createBinderArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startSystemLockTaskMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().startSystemLockTaskMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopSystemLockTaskMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().stopSystemLockTaskMode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishVoiceTask(IVoiceInteractionSession param2IVoiceInteractionSession) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IVoiceInteractionSession != null) {
            iBinder = param2IVoiceInteractionSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().finishVoiceTask(param2IVoiceInteractionSession);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTopOfTask(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(72, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isTopOfTask(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyLaunchTaskBehindComplete(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().notifyLaunchTaskBehindComplete(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyEnterAnimationComplete(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().notifyEnterAnimationComplete(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int addAppTask(IBinder param2IBinder, Intent param2Intent, ActivityManager.TaskDescription param2TaskDescription, Bitmap param2Bitmap) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2TaskDescription != null) {
            parcel1.writeInt(1);
            param2TaskDescription.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bitmap != null) {
            parcel1.writeInt(1);
            param2Bitmap.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().addAppTask(param2IBinder, param2Intent, param2TaskDescription, param2Bitmap); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Point getAppTaskThumbnailSize() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Point point;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            point = IActivityTaskManager.Stub.getDefaultImpl().getAppTaskThumbnailSize();
            return point;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            point = (Point)Point.CREATOR.createFromParcel(parcel2);
          } else {
            point = null;
          } 
          return point;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean releaseActivityInstance(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(77, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().releaseActivityInstance(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder requestStartActivityPermissionToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2IBinder = IActivityTaskManager.Stub.getDefaultImpl().requestStartActivityPermissionToken(param2IBinder);
            return param2IBinder;
          } 
          parcel2.readException();
          param2IBinder = parcel2.readStrongBinder();
          return param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseSomeActivities(IApplicationThread param2IApplicationThread) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().releaseSomeActivities(param2IApplicationThread);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bitmap getTaskDescriptionIcon(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getTaskDescriptionIcon(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bitmap)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerTaskStackListener(ITaskStackListener param2ITaskStackListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2ITaskStackListener != null) {
            iBinder = param2ITaskStackListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().registerTaskStackListener(param2ITaskStackListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterTaskStackListener(ITaskStackListener param2ITaskStackListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2ITaskStackListener != null) {
            iBinder = param2ITaskStackListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().unregisterTaskStackListener(param2ITaskStackListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTaskResizeable(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setTaskResizeable(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void toggleFreeformWindowingMode(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().toggleFreeformWindowingMode(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean resizeTask(int param2Int1, Rect param2Rect, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          boolean bool1 = true;
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().resizeTask(param2Int1, param2Rect, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveStackToDisplay(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().moveStackToDisplay(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeStack(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().removeStack(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setTaskWindowingMode(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityTaskManager.Stub.getDefaultImpl().setTaskWindowingMode(param2Int1, param2Int2, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveTaskToStack(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().moveTaskToStack(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setTaskWindowingModeSplitScreenPrimary(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityTaskManager.Stub.getDefaultImpl().setTaskWindowingModeSplitScreenPrimary(param2Int, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeStacksInWindowingModes(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().removeStacksInWindowingModes(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeStacksWithActivityTypes(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().removeStacksWithActivityTypes(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getAllStackInfos(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.StackInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.StackInfo getStackInfo(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.StackInfo stackInfo;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(94, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            stackInfo = IActivityTaskManager.Stub.getDefaultImpl().getStackInfo(param2Int1, param2Int2);
            return stackInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stackInfo = (ActivityManager.StackInfo)ActivityManager.StackInfo.CREATOR.createFromParcel(parcel2);
          } else {
            stackInfo = null;
          } 
          return stackInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.StackInfo> getAllStackInfosOnDisplay(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getAllStackInfosOnDisplay(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.StackInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.StackInfo getStackInfoOnDisplay(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.StackInfo stackInfo;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            stackInfo = IActivityTaskManager.Stub.getDefaultImpl().getStackInfoOnDisplay(param2Int1, param2Int2, param2Int3);
            return stackInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stackInfo = (ActivityManager.StackInfo)ActivityManager.StackInfo.CREATOR.createFromParcel(parcel2);
          } else {
            stackInfo = null;
          } 
          return stackInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLockScreenShown(boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setLockScreenShown(param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getAssistContextExtras(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(98, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bundle = IActivityTaskManager.Stub.getDefaultImpl().getAssistContextExtras(param2Int);
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean launchAssistIntent(Intent param2Intent, int param2Int1, String param2String, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeString(param2String);
              try {
                parcel1.writeInt(param2Int2);
                if (param2Bundle != null) {
                  parcel1.writeInt(1);
                  param2Bundle.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                try {
                  boolean bool1 = this.mRemote.transact(99, parcel1, parcel2, 0);
                  if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
                    bool = IActivityTaskManager.Stub.getDefaultImpl().launchAssistIntent(param2Intent, param2Int1, param2String, param2Int2, param2Bundle);
                    parcel2.recycle();
                    parcel1.recycle();
                    return bool;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 == 0)
                    bool = false; 
                  parcel2.recycle();
                  parcel1.recycle();
                  return bool;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Intent;
      }
      
      public boolean requestAssistContextExtras(int param2Int, IAssistDataReceiver param2IAssistDataReceiver, Bundle param2Bundle, IBinder param2IBinder, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          try {
            IBinder iBinder;
            parcel1.writeInt(param2Int);
            if (param2IAssistDataReceiver != null) {
              iBinder = param2IAssistDataReceiver.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            boolean bool = true;
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              boolean bool1;
              parcel1.writeStrongBinder(param2IBinder);
              if (param2Boolean1) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
              parcel1.writeInt(bool1);
              if (param2Boolean2) {
                bool1 = true;
              } else {
                bool1 = false;
              } 
              parcel1.writeInt(bool1);
              try {
                boolean bool2 = this.mRemote.transact(100, parcel1, parcel2, 0);
                if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
                  param2Boolean1 = IActivityTaskManager.Stub.getDefaultImpl().requestAssistContextExtras(param2Int, param2IAssistDataReceiver, param2Bundle, param2IBinder, param2Boolean1, param2Boolean2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean1;
                } 
                parcel2.readException();
                param2Int = parcel2.readInt();
                if (param2Int != 0) {
                  param2Boolean1 = bool;
                } else {
                  param2Boolean1 = false;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return param2Boolean1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IAssistDataReceiver;
      }
      
      public boolean requestAutofillData(IAssistDataReceiver param2IAssistDataReceiver, Bundle param2Bundle, IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IAssistDataReceiver != null) {
            iBinder = param2IAssistDataReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(101, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().requestAutofillData(param2IAssistDataReceiver, param2Bundle, param2IBinder, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAssistDataAllowedOnCurrentActivity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(102, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isAssistDataAllowedOnCurrentActivity();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean showAssistFromActivity(IBinder param2IBinder, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(103, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().showAssistFromActivity(param2IBinder, param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRootVoiceInteraction(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(104, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().isRootVoiceInteraction(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showLockTaskEscapeMessage(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(105, parcel, null, 1);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().showLockTaskEscapeMessage(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void keyguardGoingAway(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(106, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().keyguardGoingAway(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getActivityClassForToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getActivityClassForToken(param2IBinder); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            param2IBinder = null;
          } 
          return (ComponentName)param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPackageForToken(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getPackageForToken(param2IBinder); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void positionTaskInStack(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(109, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().positionTaskInStack(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportSizeConfigurations(IBinder param2IBinder, int[] param2ArrayOfint1, int[] param2ArrayOfint2, int[] param2ArrayOfint3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeIntArray(param2ArrayOfint1);
          parcel1.writeIntArray(param2ArrayOfint2);
          parcel1.writeIntArray(param2ArrayOfint3);
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().reportSizeConfigurations(param2IBinder, param2ArrayOfint1, param2ArrayOfint2, param2ArrayOfint3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void suppressResizeConfigChanges(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().suppressResizeConfigChanges(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean moveTopActivityToPinnedStack(int param2Int, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(112, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().moveTopActivityToPinnedStack(param2Int, param2Rect);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean enterPictureInPictureMode(IBinder param2IBinder, PictureInPictureParams param2PictureInPictureParams) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool1 = true;
          if (param2PictureInPictureParams != null) {
            parcel1.writeInt(1);
            param2PictureInPictureParams.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().enterPictureInPictureMode(param2IBinder, param2PictureInPictureParams);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPictureInPictureParams(IBinder param2IBinder, PictureInPictureParams param2PictureInPictureParams) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2PictureInPictureParams != null) {
            parcel1.writeInt(1);
            param2PictureInPictureParams.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setPictureInPictureParams(param2IBinder, param2PictureInPictureParams);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestPictureInPictureMode(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().requestPictureInPictureMode(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxNumPictureInPictureActions(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(116, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getMaxNumPictureInPictureActions(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder getUriPermissionOwnerForActivity(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            param2IBinder = IActivityTaskManager.Stub.getDefaultImpl().getUriPermissionOwnerForActivity(param2IBinder);
            return param2IBinder;
          } 
          parcel2.readException();
          param2IBinder = parcel2.readStrongBinder();
          return param2IBinder;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resizeDockedStack(Rect param2Rect1, Rect param2Rect2, Rect param2Rect3, Rect param2Rect4, Rect param2Rect5) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2Rect1 != null) {
            parcel1.writeInt(1);
            param2Rect1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Rect2 != null) {
            parcel1.writeInt(1);
            param2Rect2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Rect3 != null) {
            parcel1.writeInt(1);
            param2Rect3.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Rect4 != null) {
            parcel1.writeInt(1);
            param2Rect4.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Rect5 != null) {
            parcel1.writeInt(1);
            param2Rect5.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().resizeDockedStack(param2Rect1, param2Rect2, param2Rect3, param2Rect4, param2Rect5);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IWindowOrganizerController getWindowOrganizerController() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getWindowOrganizerController(); 
          parcel2.readException();
          return IWindowOrganizerController.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSplitScreenResizing(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setSplitScreenResizing(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int setVrMode(IBinder param2IBinder, boolean param2Boolean, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            i = IActivityTaskManager.Stub.getDefaultImpl().setVrMode(param2IBinder, param2Boolean, param2ComponentName);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startLocalVoiceInteraction(IBinder param2IBinder, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().startLocalVoiceInteraction(param2IBinder, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopLocalVoiceInteraction(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(123, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().stopLocalVoiceInteraction(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean supportsLocalVoiceInteraction() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(124, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().supportsLocalVoiceInteraction();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ConfigurationInfo getDeviceConfigurationInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ConfigurationInfo configurationInfo;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            configurationInfo = IActivityTaskManager.Stub.getDefaultImpl().getDeviceConfigurationInfo();
            return configurationInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            configurationInfo = (ConfigurationInfo)ConfigurationInfo.CREATOR.createFromParcel(parcel2);
          } else {
            configurationInfo = null;
          } 
          return configurationInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dismissKeyguard(IBinder param2IBinder, IKeyguardDismissCallback param2IKeyguardDismissCallback, CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IKeyguardDismissCallback != null) {
            iBinder = param2IKeyguardDismissCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().dismissKeyguard(param2IBinder, param2IKeyguardDismissCallback, param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelTaskWindowTransition(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(127, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().cancelTaskWindowTransition(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.TaskSnapshot getTaskSnapshot(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          ActivityManager.TaskSnapshot taskSnapshot;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(128, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            taskSnapshot = IActivityTaskManager.Stub.getDefaultImpl().getTaskSnapshot(param2Int, param2Boolean);
            return taskSnapshot;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            taskSnapshot = (ActivityManager.TaskSnapshot)ActivityManager.TaskSnapshot.CREATOR.createFromParcel(parcel2);
          } else {
            taskSnapshot = null;
          } 
          return taskSnapshot;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisablePreviewScreenshots(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(129, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setDisablePreviewScreenshots(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void invalidateHomeTaskSnapshot(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(130, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().invalidateHomeTaskSnapshot(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLastResumedActivityUserId() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getLastResumedActivityUserId(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateConfiguration(Configuration param2Configuration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool1 = true;
          if (param2Configuration != null) {
            parcel1.writeInt(1);
            param2Configuration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(132, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().updateConfiguration(param2Configuration);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateLockTaskFeatures(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().updateLockTaskFeatures(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setShowWhenLocked(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(134, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setShowWhenLocked(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInheritShowWhenLocked(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(135, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setInheritShowWhenLocked(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTurnScreenOn(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(136, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setTurnScreenOn(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRemoteAnimations(IBinder param2IBinder, RemoteAnimationDefinition param2RemoteAnimationDefinition) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2RemoteAnimationDefinition != null) {
            parcel1.writeInt(1);
            param2RemoteAnimationDefinition.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(137, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().registerRemoteAnimations(param2IBinder, param2RemoteAnimationDefinition);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterRemoteAnimations(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(138, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().unregisterRemoteAnimations(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRemoteAnimationForNextActivityStart(String param2String, RemoteAnimationAdapter param2RemoteAnimationAdapter) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          if (param2RemoteAnimationAdapter != null) {
            parcel1.writeInt(1);
            param2RemoteAnimationAdapter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(139, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().registerRemoteAnimationForNextActivityStart(param2String, param2RemoteAnimationAdapter);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRemoteAnimationsForDisplay(int param2Int, RemoteAnimationDefinition param2RemoteAnimationDefinition) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          if (param2RemoteAnimationDefinition != null) {
            parcel1.writeInt(1);
            param2RemoteAnimationDefinition.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(140, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().registerRemoteAnimationsForDisplay(param2Int, param2RemoteAnimationDefinition);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void alwaysShowUnsupportedCompileSdkWarning(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(141, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().alwaysShowUnsupportedCompileSdkWarning(param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOppoKinectActivityController(IOplusKinectActivityController param2IOplusKinectActivityController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IOplusKinectActivityController != null) {
            iBinder = param2IOplusKinectActivityController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(142, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setOppoKinectActivityController(param2IOplusKinectActivityController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVrThread(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(143, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setVrThread(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPersistentVrThread(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(144, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setPersistentVrThread(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopAppSwitches() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(145, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().stopAppSwitches();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumeAppSwitches() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          boolean bool = this.mRemote.transact(146, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().resumeAppSwitches();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActivityController(IActivityController param2IActivityController, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IActivityController != null) {
            iBinder = param2IActivityController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(147, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setActivityController(param2IActivityController, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVoiceKeepAwake(IVoiceInteractionSession param2IVoiceInteractionSession, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          if (param2IVoiceInteractionSession != null) {
            iBinder = param2IVoiceInteractionSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(148, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setVoiceKeepAwake(param2IVoiceInteractionSession, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPackageScreenCompatMode(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(149, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null)
            return IActivityTaskManager.Stub.getDefaultImpl().getPackageScreenCompatMode(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPackageScreenCompatMode(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(150, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setPackageScreenCompatMode(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getPackageAskScreenCompat(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(151, parcel1, parcel2, 0);
          if (!bool2 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityTaskManager.Stub.getDefaultImpl().getPackageAskScreenCompat(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPackageAskScreenCompat(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(152, parcel1, parcel2, 0);
          if (!bool1 && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setPackageAskScreenCompat(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearLaunchParamsForPackages(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(153, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().clearLaunchParamsForPackages(param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDisplayToSingleTaskInstance(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(154, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().setDisplayToSingleTaskInstance(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restartActivityProcessIfVisible(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(155, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().restartActivityProcessIfVisible(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBackPressedOnTaskRoot(IBinder param2IBinder, IRequestFinishCallback param2IRequestFinishCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IRequestFinishCallback != null) {
            iBinder = param2IRequestFinishCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(156, parcel1, parcel2, 0);
          if (!bool && IActivityTaskManager.Stub.getDefaultImpl() != null) {
            IActivityTaskManager.Stub.getDefaultImpl().onBackPressedOnTaskRoot(param2IBinder, param2IRequestFinishCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IActivityTaskManager param1IActivityTaskManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IActivityTaskManager != null) {
          Proxy.sDefaultImpl = param1IActivityTaskManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IActivityTaskManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
