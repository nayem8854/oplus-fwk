package android.app;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.proto.ProtoOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlSerializer;

public final class NotificationChannelGroup implements Parcelable {
  private int mUserLockedFields;
  
  private CharSequence mName;
  
  private final String mId;
  
  private String mDescription;
  
  private List<NotificationChannel> mChannels = new ArrayList<>();
  
  private boolean mBlocked;
  
  public static final int USER_LOCKED_BLOCKED_STATE = 1;
  
  private static final String TAG_GROUP = "channelGroup";
  
  private static final int MAX_TEXT_LENGTH = 1000;
  
  public NotificationChannelGroup(String paramString, CharSequence paramCharSequence) {
    this.mId = getTrimmedString(paramString);
    if (paramCharSequence != null) {
      paramString = getTrimmedString(paramCharSequence.toString());
    } else {
      paramString = null;
    } 
    this.mName = paramString;
  }
  
  protected NotificationChannelGroup(Parcel paramParcel) {
    if (paramParcel.readByte() != 0) {
      this.mId = paramParcel.readString();
    } else {
      this.mId = null;
    } 
    this.mName = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    if (paramParcel.readByte() != 0) {
      this.mDescription = paramParcel.readString();
    } else {
      this.mDescription = null;
    } 
    paramParcel.readParcelableList(this.mChannels, NotificationChannel.class.getClassLoader());
    this.mBlocked = paramParcel.readBoolean();
    this.mUserLockedFields = paramParcel.readInt();
  }
  
  private String getTrimmedString(String paramString) {
    if (paramString != null && paramString.length() > 1000)
      return paramString.substring(0, 1000); 
    return paramString;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    if (this.mId != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeString(this.mId);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    TextUtils.writeToParcel(this.mName, paramParcel, paramInt);
    if (this.mDescription != null) {
      paramParcel.writeByte((byte)1);
      paramParcel.writeString(this.mDescription);
    } else {
      paramParcel.writeByte((byte)0);
    } 
    paramParcel.writeParcelableList(this.mChannels, paramInt);
    paramParcel.writeBoolean(this.mBlocked);
    paramParcel.writeInt(this.mUserLockedFields);
  }
  
  public String getId() {
    return this.mId;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public List<NotificationChannel> getChannels() {
    return this.mChannels;
  }
  
  public boolean isBlocked() {
    return this.mBlocked;
  }
  
  public void setDescription(String paramString) {
    this.mDescription = getTrimmedString(paramString);
  }
  
  public void setBlocked(boolean paramBoolean) {
    this.mBlocked = paramBoolean;
  }
  
  public void addChannel(NotificationChannel paramNotificationChannel) {
    this.mChannels.add(paramNotificationChannel);
  }
  
  public void setChannels(List<NotificationChannel> paramList) {
    this.mChannels = paramList;
  }
  
  public void lockFields(int paramInt) {
    this.mUserLockedFields |= paramInt;
  }
  
  public void unlockFields(int paramInt) {
    this.mUserLockedFields &= paramInt ^ 0xFFFFFFFF;
  }
  
  public int getUserLockedFields() {
    return this.mUserLockedFields;
  }
  
  public void populateFromXml(XmlPullParser paramXmlPullParser) {
    setDescription(paramXmlPullParser.getAttributeValue(null, "desc"));
    setBlocked(safeBool(paramXmlPullParser, "blocked", false));
  }
  
  private static boolean safeBool(XmlPullParser paramXmlPullParser, String paramString, boolean paramBoolean) {
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str))
      return paramBoolean; 
    return Boolean.parseBoolean(str);
  }
  
  public void writeXml(XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.startTag(null, "channelGroup");
    paramXmlSerializer.attribute(null, "id", getId());
    if (getName() != null)
      paramXmlSerializer.attribute(null, "name", getName().toString()); 
    if (getDescription() != null)
      paramXmlSerializer.attribute(null, "desc", getDescription().toString()); 
    paramXmlSerializer.attribute(null, "blocked", Boolean.toString(isBlocked()));
    paramXmlSerializer.attribute(null, "locked", Integer.toString(this.mUserLockedFields));
    paramXmlSerializer.endTag(null, "channelGroup");
  }
  
  @SystemApi
  public JSONObject toJson() throws JSONException {
    JSONObject jSONObject = new JSONObject();
    jSONObject.put("id", getId());
    jSONObject.put("name", getName());
    jSONObject.put("desc", getDescription());
    jSONObject.put("blocked", isBlocked());
    jSONObject.put("locked", this.mUserLockedFields);
    return jSONObject;
  }
  
  public static final Parcelable.Creator<NotificationChannelGroup> CREATOR = new Parcelable.Creator<NotificationChannelGroup>() {
      public NotificationChannelGroup createFromParcel(Parcel param1Parcel) {
        return new NotificationChannelGroup(param1Parcel);
      }
      
      public NotificationChannelGroup[] newArray(int param1Int) {
        return new NotificationChannelGroup[param1Int];
      }
    };
  
  private static final String ATT_USER_LOCKED = "locked";
  
  private static final String ATT_NAME = "name";
  
  private static final String ATT_ID = "id";
  
  private static final String ATT_DESC = "desc";
  
  private static final String ATT_BLOCKED = "blocked";
  
  public int describeContents() {
    return 0;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (isBlocked() == paramObject.isBlocked() && this.mUserLockedFields == ((NotificationChannelGroup)paramObject).mUserLockedFields)
      if (Objects.equals(getId(), paramObject.getId()) && 
        Objects.equals(getName(), paramObject.getName()) && 
        Objects.equals(getDescription(), paramObject.getDescription()) && 
        Objects.equals(getChannels(), paramObject.getChannels()))
        return null;  
    return false;
  }
  
  public int hashCode() {
    String str1 = getId();
    CharSequence charSequence = getName();
    String str2 = getDescription();
    boolean bool = isBlocked();
    List<NotificationChannel> list = getChannels();
    int i = this.mUserLockedFields;
    return Objects.hash(new Object[] { str1, charSequence, str2, Boolean.valueOf(bool), list, Integer.valueOf(i) });
  }
  
  public NotificationChannelGroup clone() {
    NotificationChannelGroup notificationChannelGroup = new NotificationChannelGroup(getId(), getName());
    notificationChannelGroup.setDescription(getDescription());
    notificationChannelGroup.setBlocked(isBlocked());
    notificationChannelGroup.setChannels(getChannels());
    notificationChannelGroup.lockFields(this.mUserLockedFields);
    return notificationChannelGroup;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("NotificationChannelGroup{mId='");
    stringBuilder.append(this.mId);
    stringBuilder.append('\'');
    stringBuilder.append(", mName=");
    stringBuilder.append(this.mName);
    stringBuilder.append(", mDescription=");
    if (!TextUtils.isEmpty(this.mDescription)) {
      null = "hasDescription ";
    } else {
      null = "";
    } 
    stringBuilder.append(null);
    stringBuilder.append(", mBlocked=");
    stringBuilder.append(this.mBlocked);
    stringBuilder.append(", mChannels=");
    stringBuilder.append(this.mChannels);
    stringBuilder.append(", mUserLockedFields=");
    stringBuilder.append(this.mUserLockedFields);
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, this.mId);
    paramProtoOutputStream.write(1138166333442L, this.mName.toString());
    paramProtoOutputStream.write(1138166333443L, this.mDescription);
    paramProtoOutputStream.write(1133871366148L, this.mBlocked);
    for (NotificationChannel notificationChannel : this.mChannels)
      notificationChannel.dumpDebug(paramProtoOutputStream, 2246267895813L); 
    paramProtoOutputStream.end(paramLong);
  }
}
