package android.app;

import android.content.LocusId;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.util.Objects;

public final class DirectAction implements Parcelable {
  public DirectAction(String paramString, Bundle paramBundle, LocusId paramLocusId) {
    this.mID = (String)Preconditions.checkStringNotEmpty(paramString);
    this.mExtras = paramBundle;
    this.mLocusId = paramLocusId;
  }
  
  public void setSource(int paramInt, IBinder paramIBinder) {
    this.mTaskId = paramInt;
    this.mActivityId = paramIBinder;
  }
  
  public DirectAction(DirectAction paramDirectAction) {
    this.mTaskId = paramDirectAction.mTaskId;
    this.mActivityId = paramDirectAction.mActivityId;
    this.mID = paramDirectAction.mID;
    this.mExtras = paramDirectAction.mExtras;
    this.mLocusId = paramDirectAction.mLocusId;
  }
  
  private DirectAction(Parcel paramParcel) {
    this.mTaskId = paramParcel.readInt();
    this.mActivityId = paramParcel.readStrongBinder();
    this.mID = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
    String str = paramParcel.readString();
    if (str != null) {
      LocusId locusId = new LocusId(str);
    } else {
      str = null;
    } 
    this.mLocusId = (LocusId)str;
  }
  
  public int getTaskId() {
    return this.mTaskId;
  }
  
  public IBinder getActivityId() {
    return this.mActivityId;
  }
  
  public String getId() {
    return this.mID;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public LocusId getLocusId() {
    return this.mLocusId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    return this.mID.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (paramObject == this)
      return true; 
    if (getClass() != paramObject.getClass())
      return false; 
    return this.mID.equals(((DirectAction)paramObject).mID);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTaskId);
    paramParcel.writeStrongBinder(this.mActivityId);
    paramParcel.writeString(this.mID);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeString(this.mLocusId.getId());
  }
  
  class Builder {
    private Bundle mExtras;
    
    private String mId;
    
    private LocusId mLocusId;
    
    public Builder(DirectAction this$0) {
      Objects.requireNonNull(this$0);
      this.mId = (String)this$0;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setLocusId(LocusId param1LocusId) {
      this.mLocusId = param1LocusId;
      return this;
    }
    
    public DirectAction build() {
      return new DirectAction(this.mId, this.mExtras, this.mLocusId);
    }
  }
  
  public static final Parcelable.Creator<DirectAction> CREATOR = new Parcelable.Creator<DirectAction>() {
      public DirectAction createFromParcel(Parcel param1Parcel) {
        return new DirectAction(param1Parcel);
      }
      
      public DirectAction[] newArray(int param1Int) {
        return new DirectAction[param1Int];
      }
    };
  
  public static final String KEY_ACTIONS_LIST = "actions_list";
  
  private IBinder mActivityId;
  
  private final Bundle mExtras;
  
  private final String mID;
  
  private final LocusId mLocusId;
  
  private int mTaskId;
}
