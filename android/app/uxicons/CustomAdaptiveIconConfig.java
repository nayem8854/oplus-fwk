package android.app.uxicons;

import android.content.res.Resources;
import android.graphics.Path;

public class CustomAdaptiveIconConfig {
  public static final int OPLUS_ADAPTIVE_MASK_SIZE = 150;
  
  private int mCustomIconFgSize;
  
  private int mCustomIconSize;
  
  private Path mCustomMask;
  
  private int mDefaultIconSize;
  
  private float mForegroundScalePercent;
  
  private boolean mIsAdaptiveIconDrawable;
  
  private boolean mIsPlatformDrawable;
  
  private float mScalePercent;
  
  public static class Builder {
    private CustomAdaptiveIconConfig mConfig;
    
    public Builder(Resources param1Resources) {
      this.mConfig = new CustomAdaptiveIconConfig(param1Resources);
    }
    
    public Builder setCustomIconSize(int param1Int) {
      CustomAdaptiveIconConfig.access$102(this.mConfig, param1Int);
      CustomAdaptiveIconConfig customAdaptiveIconConfig = this.mConfig;
      CustomAdaptiveIconConfig.access$202(customAdaptiveIconConfig, param1Int * 1.0F / customAdaptiveIconConfig.getDefaultIconSize());
      return this;
    }
    
    public Builder setCustomIconFgSize(int param1Int) {
      CustomAdaptiveIconConfig.access$302(this.mConfig, param1Int);
      CustomAdaptiveIconConfig customAdaptiveIconConfig = this.mConfig;
      CustomAdaptiveIconConfig.access$402(customAdaptiveIconConfig, param1Int * 1.0F / customAdaptiveIconConfig.getDefaultIconSize());
      return this;
    }
    
    public Builder setCustomMask(Path param1Path) {
      CustomAdaptiveIconConfig.access$502(this.mConfig, param1Path);
      return this;
    }
    
    public Builder setIsPlatformDrawable(boolean param1Boolean) {
      CustomAdaptiveIconConfig.access$602(this.mConfig, param1Boolean);
      return this;
    }
    
    public Builder setIsAdaptiveIconDrawable(boolean param1Boolean) {
      CustomAdaptiveIconConfig.access$702(this.mConfig, param1Boolean);
      return this;
    }
    
    public CustomAdaptiveIconConfig create() {
      return this.mConfig;
    }
  }
  
  private CustomAdaptiveIconConfig(Resources paramResources) {
    int i = paramResources.getDimensionPixelSize(201654712);
    this.mCustomIconSize = i;
    this.mCustomIconFgSize = i;
    this.mCustomMask = null;
    this.mScalePercent = 1.0F;
    this.mForegroundScalePercent = 1.0F;
    this.mIsPlatformDrawable = false;
    this.mIsAdaptiveIconDrawable = false;
  }
  
  public int getDefaultIconSize() {
    return this.mDefaultIconSize;
  }
  
  public Path getCustomMask() {
    return this.mCustomMask;
  }
  
  public int getCustomIconSize() {
    return this.mCustomIconSize;
  }
  
  public int getCustomIconFgSize() {
    return this.mCustomIconFgSize;
  }
  
  public float getScalePercent() {
    return this.mScalePercent;
  }
  
  public float getForegroundScalePercent() {
    return this.mForegroundScalePercent;
  }
  
  public boolean getIsPlatformDrawable() {
    return this.mIsPlatformDrawable;
  }
  
  public boolean getIsAdaptiveIconDrawable() {
    return this.mIsAdaptiveIconDrawable;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("CustomIconConfig:DefaultIconSize = ");
    stringBuilder.append(this.mDefaultIconSize);
    stringBuilder.append(";CustomIconSize = ");
    stringBuilder.append(this.mCustomIconSize);
    stringBuilder.append(";CustomIconFgSize = ");
    stringBuilder.append(this.mCustomIconFgSize);
    stringBuilder.append(";ScalePercent");
    stringBuilder.append(this.mScalePercent);
    stringBuilder.append(";ForegroundScalePercent = ");
    stringBuilder.append(this.mForegroundScalePercent);
    stringBuilder.append(";IsPlatformDrawable = ");
    stringBuilder.append(this.mIsPlatformDrawable);
    stringBuilder.append(";IsAdaptiveIconDrawable");
    stringBuilder.append(this.mIsAdaptiveIconDrawable);
    return stringBuilder.toString();
  }
}
