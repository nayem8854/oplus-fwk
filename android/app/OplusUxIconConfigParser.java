package android.app;

import android.content.res.Resources;
import android.graphics.Path;
import android.graphics.Rect;
import android.util.Log;
import android.util.PathParser;
import com.oplus.util.OplusRoundRectUtil;
import java.util.ArrayList;
import oplus.content.res.OplusExtraConfiguration;

public class OplusUxIconConfigParser {
  public static final int AND_NUM_FFFF = 65535;
  
  public static final int ICON_DEFAULT_SIZE_DP = 5000;
  
  public static void parseConfig(OplusIconConfig paramOplusIconConfig, OplusExtraConfiguration paramOplusExtraConfiguration, Resources paramResources, ArrayList<Integer> paramArrayList1, ArrayList<Integer> paramArrayList2, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    Path path1;
    boolean bool;
    if (paramOplusExtraConfiguration == null)
      return; 
    Long long_1 = Long.valueOf(paramOplusExtraConfiguration.mUxIconConfig);
    if (long_1.longValue() == -1L) {
      paramOplusIconConfig.setEmpty(true);
      return;
    } 
    int i = Long.valueOf(long_1.longValue() >> 61L).intValue() & 0x1;
    if (i == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramOplusIconConfig.setEnableDarkModeIcon(bool);
    if ((long_1.intValue() & 0xF) == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramOplusIconConfig.setForeign(bool);
    long_1 = Long.valueOf(long_1.longValue() >> 4L);
    int j = long_1.intValue() & 0xF;
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("theme=");
    stringBuilder2.append(j);
    stringBuilder2.append("; uxIconConfig = ");
    stringBuilder2.append(String.valueOf(long_1));
    Log.i("OplusUXIconLoader", stringBuilder2.toString());
    paramOplusIconConfig.setTheme(j);
    Long long_2 = long_1 = Long.valueOf(long_1.longValue() >> 4L);
    if ((long_1.intValue() & 0xF) == 1) {
      bool = true;
    } else {
      bool = false;
    } 
    paramOplusIconConfig.setArtPlusOn(bool);
    int k = paramResources.getDimensionPixelSize(201654712);
    k = getDpFromIconConfigPx(paramResources, k);
    paramOplusIconConfig.setIconSize(k);
    paramOplusIconConfig.setForegroundSize(k);
    int m = paramArrayList1.size() - 1;
    if (paramArrayOfString1 != null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("themeCustomPos=");
      stringBuilder.append(m);
      stringBuilder.append(";commonStylePathArray size=");
      stringBuilder.append(paramArrayOfString1.length);
      Log.i("OplusUXIconLoader", stringBuilder.toString());
      if (j == 5) {
        k = Long.valueOf(long_2.longValue() >> 8L).intValue();
        paramOplusIconConfig.setIconSize(k & 0xFFFF);
        path1 = PathParser.createPathFromPathData(paramArrayOfString1[0]);
        k = 0;
      } else {
        for (k = 0, stringBuilder = null; k < paramArrayList1.size(); k++) {
          if (((Integer)paramArrayList1.get(k)).intValue() == j && k == m) {
            long_2 = Long.valueOf(long_2.longValue() >> 4L);
            int n = long_2.intValue() & 0xF;
            paramOplusIconConfig.setIconShape(n);
            long_2 = Long.valueOf(long_2.longValue() >> 4L);
            int i1 = long_2.intValue();
            paramOplusIconConfig.setIconSize(i1 & 0xFFFF);
            Long long_ = Long.valueOf(long_2.longValue() >> 16L);
            paramOplusIconConfig.setForegroundSize(long_.intValue() & 0xFFFF);
            if (n != 0) {
              if (n != 1) {
                if (n != 2) {
                  if (n == 3)
                    path1 = PathParser.createPathFromPathData(paramResources.getString(201588996)); 
                } else {
                  path1 = PathParser.createPathFromPathData(paramResources.getString(201588994));
                } 
              } else {
                path1 = PathParser.createPathFromPathData(paramResources.getString(201588995));
              } 
            } else {
              Long long_3 = Long.valueOf(long_2.longValue() >> 16L);
              n = long_3.intValue() & 0xFFF;
              i1 = n;
              if (n > 75)
                i1 = getPxFromIconConfigDp(paramResources, n); 
              if (i1 == 75) {
                path1 = PathParser.createPathFromPathData(paramResources.getString(201588993));
              } else {
                path1 = OplusRoundRectUtil.getInstance().getPath(new Rect(0, 0, 150, 150), i1);
              } 
            } 
          } else if (((Integer)paramArrayList1.get(k)).intValue() == j && k < paramArrayOfString1.length) {
            Long long_ = Long.valueOf(long_2.longValue() >> 8L);
            int n = long_.intValue();
            paramOplusIconConfig.setIconSize(n & 0xFFFF);
            if (k == 1) {
              n = paramResources.getDimensionPixelSize(201654710);
              n = getDpFromIconConfigPx(paramResources, n);
              paramOplusIconConfig.setForegroundSize(n);
              path1 = OplusRoundRectUtil.getInstance().getPath(new Rect(0, 0, 150, 150), 8.0F);
            } else {
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append("theme=");
              stringBuilder3.append(j);
              stringBuilder3.append("; pathData =");
              stringBuilder3.append(paramArrayOfString1[k]);
              Log.i("UXIconPaser", stringBuilder3.toString());
              path1 = PathParser.createPathFromPathData(paramArrayOfString1[k]);
            } 
          } 
        } 
      } 
    } else {
      k = 0;
      long_1 = null;
    } 
    if (k == paramArrayList1.size())
      for (k = 0; k < paramArrayList2.size(); k++, path1 = path) {
        Path path;
        Long long_ = long_1;
        if (((Integer)paramArrayList2.get(k)).intValue() == j) {
          long_ = long_1;
          if (k < paramArrayOfString2.length)
            path = PathParser.createPathFromPathData(paramArrayOfString2[k]); 
        } 
      }  
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("path = ");
    stringBuilder1.append(path1);
    stringBuilder1.append("; config = ");
    stringBuilder1.append(paramOplusIconConfig.toString());
    Log.i("OplusUXIconLoader", stringBuilder1.toString());
    Path path2 = path1;
    if (path1 == null)
      path2 = PathParser.createPathFromPathData(paramResources.getString(201588996)); 
    if (path2 != null) {
      paramOplusIconConfig.setShapePath(path2);
      paramOplusIconConfig.setEmpty(false);
      paramOplusIconConfig.setNeedUpdate(0);
    } else {
      paramOplusIconConfig.setEmpty(true);
      paramOplusIconConfig.setNeedUpdate(1);
    } 
  }
  
  public static int getDpFromIconConfigPx(Resources paramResources, int paramInt) {
    return float2int(paramInt * 1.0F / (paramResources.getDisplayMetrics()).density * 100.0F);
  }
  
  public static int getPxFromIconConfigDp(Resources paramResources, int paramInt) {
    return float2int((paramResources.getDisplayMetrics()).density * paramInt * 1.0F / 100.0F);
  }
  
  public static long getDefaultIconConfig(boolean paramBoolean, Resources paramResources) {
    long l1 = (true & true);
    int i = paramResources.getDimensionPixelSize(201654711);
    i = getDpFromIconConfigPx(paramResources, i);
    long l2 = (i & 0xFFF);
    i = paramResources.getDimensionPixelSize(201654712);
    i = getDpFromIconConfigPx(paramResources, i);
    long l3 = (0xFFFF & i);
    long l4 = 2L;
    long l5 = paramBoolean;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("DefaultIconConfig[foreign =");
    stringBuilder.append(paramBoolean);
    stringBuilder.append("; isDarkModeIcon =");
    stringBuilder.append(1);
    stringBuilder.append("; defaultTheme =");
    stringBuilder.append(2);
    stringBuilder.append("]");
    Log.i("OplusUXIconLoader", stringBuilder.toString());
    return (((((((0x0L | l1) << 13L | l2) << 16L | l3) << 16L | 0x1388L) << 4L | 0x0L) << 4L | 0x0L) << 4L | l4) << 4L | l5;
  }
  
  private static int float2int(float paramFloat) {
    int i = (int)paramFloat;
    float f = i;
    int j = i;
    if (Math.abs(paramFloat - f) > 0.5D)
      j = i + 1; 
    return j;
  }
}
