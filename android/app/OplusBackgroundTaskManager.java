package android.app;

import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ServiceManager;

public class OplusBackgroundTaskManager {
  private static final String DESCRIPTOR = "android.app.IActivityTaskManager";
  
  public static final int TRANSACTION_first = 70000;
  
  public static final int TRANSACTION_isTopCanMoveToBackground = 70001;
  
  public static final int TRANSACTION_playWhenScreenOff = 70002;
  
  private static volatile OplusBackgroundTaskManager sOplusBackgroundTaskManager;
  
  private IBinder mRemote = ServiceManager.getService("activity_task");
  
  public static OplusBackgroundTaskManager getInstance() {
    // Byte code:
    //   0: getstatic android/app/OplusBackgroundTaskManager.sOplusBackgroundTaskManager : Landroid/app/OplusBackgroundTaskManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/app/OplusBackgroundTaskManager
    //   8: monitorenter
    //   9: getstatic android/app/OplusBackgroundTaskManager.sOplusBackgroundTaskManager : Landroid/app/OplusBackgroundTaskManager;
    //   12: ifnonnull -> 27
    //   15: new android/app/OplusBackgroundTaskManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/app/OplusBackgroundTaskManager.sOplusBackgroundTaskManager : Landroid/app/OplusBackgroundTaskManager;
    //   27: ldc android/app/OplusBackgroundTaskManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/app/OplusBackgroundTaskManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/app/OplusBackgroundTaskManager.sOplusBackgroundTaskManager : Landroid/app/OplusBackgroundTaskManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #41	-> 0
    //   #42	-> 6
    //   #43	-> 9
    //   #44	-> 15
    //   #46	-> 27
    //   #48	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public static boolean isSupport() {
    return true;
  }
  
  public boolean isTopCanMoveToBackground() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(70001, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void playWhenScreenOff() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(70002, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
