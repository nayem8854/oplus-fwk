package android.app;

import android.os.RemoteException;

public interface IOplusNotificationManager {
  public static final int CHECK_GET_OPENID_TRANSACTION = 10018;
  
  public static final int CLEAR_OPENID_TRANSACTION = 10011;
  
  public static final String DESCRIPTOR = "android.app.INotificationManager";
  
  public static final int GET_APP_BANNER_TRANSACTION = 10020;
  
  public static final int GET_APP_VISIBILITY_TRANSACTION = 10022;
  
  public static final int GET_BADGE_OPTION_TRANSACTION = 10013;
  
  public static final int GET_DYNAMIC_SOUND_RINGTONE_TRANSACTION = 10023;
  
  public static final int GET_ENABLE_NAVIGATION_APPS_TRANSACTION = 10007;
  
  public static final int GET_NAVIGATION_MODE_TRANSACTION = 10004;
  
  public static final int GET_OPENID_TRANSACTION = 10010;
  
  public static final int GET_STOW_OPTION_TRANSACTION = 10016;
  
  public static final int IS_DRIVE_NAVIGATION_MODE_TRANSACTION = 10005;
  
  public static final int IS_NAVIGATION_MODE_TRANSACTION = 10006;
  
  public static final int IS_NUMBADGE_SUPPORT_TRANSACTION = 10014;
  
  public static final int IS_SUPPRESSED_BY_DRIVEMODE_TRANSACTION = 10008;
  
  public static final int OPLUS_CALL_TRANSACTION_INDEX = 10000;
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
  
  public static final int SET_APP_BANNER_TRANSACTION = 10019;
  
  public static final int SET_APP_VISIBILITY_TRANSACTION = 10021;
  
  public static final int SET_BADGE_OPTION_TRANSACTION = 10012;
  
  public static final int SET_NUMBADGE_SUPPORT_TRANSACTION = 10015;
  
  public static final int SET_STOW_OPTION_TRANSACTION = 10017;
  
  public static final int SET_SUPPRESSED_BY_DRIVEMODE_TRANSACTION = 10009;
  
  public static final int SHOULD_INTERCEPT_SOUND_TRANSACTION = 10002;
  
  public static final int SHOULD_KEEP_ALIVE_TRANSACTION = 10003;
  
  boolean checkGetOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void clearOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  boolean getAppBanner(String paramString, int paramInt) throws RemoteException;
  
  int getAppVisibility(String paramString, int paramInt) throws RemoteException;
  
  int getBadgeOption(String paramString, int paramInt) throws RemoteException;
  
  String getDynamicRingtone(String paramString1, String paramString2) throws RemoteException;
  
  String[] getEnableNavigationApps(int paramInt) throws RemoteException;
  
  int getNavigationMode(String paramString, int paramInt) throws RemoteException;
  
  String getOpenid(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  int getStowOption(String paramString, int paramInt) throws RemoteException;
  
  boolean isDriveNavigationMode(String paramString, int paramInt) throws RemoteException;
  
  boolean isNavigationMode(int paramInt) throws RemoteException;
  
  boolean isNumbadgeSupport(String paramString, int paramInt) throws RemoteException;
  
  boolean isSuppressedByDriveMode(int paramInt) throws RemoteException;
  
  void setAppBanner(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setAppVisibility(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setBadgeOption(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setNumbadgeSupport(String paramString, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setStowOption(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setSuppressedByDriveMode(boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean shouldInterceptSound(String paramString, int paramInt) throws RemoteException;
  
  boolean shouldKeepAlive(String paramString, int paramInt) throws RemoteException;
}
