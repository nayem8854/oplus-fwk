package android.app;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.pm.PackageParser;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Bundle;

public interface IOplusCommonInjector extends IOplusCommonFeature {
  public static final IOplusCommonInjector DEFAULT = (IOplusCommonInjector)new Object();
  
  default IOplusCommonInjector getDefault() {
    return DEFAULT;
  }
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusCommonInjector;
  }
  
  default void onCreateForActivity(Activity paramActivity, Bundle paramBundle) {}
  
  default void onCreateForApplication(Application paramApplication) {}
  
  default void applyConfigurationToResourcesForResourcesManager(Configuration paramConfiguration, int paramInt) {}
  
  default void onConfigurationChangedForApplication(Application paramApplication, Configuration paramConfiguration) {}
  
  default void hookPreloadResources(Resources paramResources, String paramString) {}
  
  default void hookActivityAliasTheme(PackageParser.Activity paramActivity1, Resources paramResources, XmlResourceParser paramXmlResourceParser, PackageParser.Activity paramActivity2) {}
}
