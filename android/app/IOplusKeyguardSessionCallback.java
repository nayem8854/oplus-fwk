package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusKeyguardSessionCallback extends IInterface {
  void onCommand(String paramString) throws RemoteException;
  
  class Default implements IOplusKeyguardSessionCallback {
    public void onCommand(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusKeyguardSessionCallback {
    private static final String DESCRIPTOR = "android.app.IOplusKeyguardSessionCallback";
    
    static final int TRANSACTION_onCommand = 1;
    
    public Stub() {
      attachInterface(this, "android.app.IOplusKeyguardSessionCallback");
    }
    
    public static IOplusKeyguardSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IOplusKeyguardSessionCallback");
      if (iInterface != null && iInterface instanceof IOplusKeyguardSessionCallback)
        return (IOplusKeyguardSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.app.IOplusKeyguardSessionCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.IOplusKeyguardSessionCallback");
      String str = param1Parcel1.readString();
      onCommand(str);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IOplusKeyguardSessionCallback {
      public static IOplusKeyguardSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IOplusKeyguardSessionCallback";
      }
      
      public void onCommand(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IOplusKeyguardSessionCallback");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IOplusKeyguardSessionCallback.Stub.getDefaultImpl() != null) {
            IOplusKeyguardSessionCallback.Stub.getDefaultImpl().onCommand(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusKeyguardSessionCallback param1IOplusKeyguardSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusKeyguardSessionCallback != null) {
          Proxy.sDefaultImpl = param1IOplusKeyguardSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusKeyguardSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
