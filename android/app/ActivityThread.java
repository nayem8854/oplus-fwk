package android.app;

import android.app.assist.AssistStructure;
import android.app.backup.BackupAgent;
import android.app.servertransaction.ActivityLifecycleItem;
import android.app.servertransaction.ActivityRelaunchItem;
import android.app.servertransaction.ActivityResultItem;
import android.app.servertransaction.ClientTransaction;
import android.app.servertransaction.ClientTransactionItem;
import android.app.servertransaction.PauseActivityItem;
import android.app.servertransaction.PendingTransactionActions;
import android.app.servertransaction.ResumeActivityItem;
import android.app.servertransaction.TransactionExecutor;
import android.app.servertransaction.TransactionExecutorHelper;
import android.common.OplusFeatureCache;
import android.content.AutofillOptions;
import android.content.BroadcastReceiver;
import android.content.ComponentCallbacks2;
import android.content.ComponentName;
import android.content.ContentCaptureOptions;
import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.Context;
import android.content.IContentProvider;
import android.content.IIntentReceiver;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageManager;
import android.content.pm.InstrumentationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ParceledListSlice;
import android.content.pm.ProviderInfo;
import android.content.pm.ProviderInfoList;
import android.content.pm.ServiceInfo;
import android.content.res.AssetManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Configuration;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.content.res.loader.ResourcesLoader;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDebug;
import android.ddm.DdmHandleAppName;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.HardwareRenderer;
import android.hardware.SensorManager;
import android.hardware.display.DisplayManagerGlobal;
import android.net.ConnectivityManager;
import android.net.Proxy;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.Debug;
import android.os.Environment;
import android.os.FileUtils;
import android.os.GraphicsEnvironment;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.LocaleList;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.StatsFrameworkInitializer;
import android.os.StatsServiceManager;
import android.os.StrictMode;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.TelephonyServiceManager;
import android.os.Trace;
import android.os.UserHandle;
import android.os.UserManager;
import android.permission.IPermissionManager;
import android.renderscript.RenderScriptCacheDir;
import android.security.NetworkSecurityPolicy;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructStat;
import android.telephony.TelephonyFrameworkInitializer;
import android.util.AndroidRuntimeException;
import android.util.ArrayMap;
import android.util.DisplayMetrics;
import android.util.EventLog;
import android.util.Log;
import android.util.MergedConfiguration;
import android.util.Pair;
import android.util.PrintWriterPrinter;
import android.util.Printer;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.SuperNotCalledException;
import android.util.proto.ProtoOutputStream;
import android.view.Choreographer;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.DisplayAdjustments;
import android.view.ThreadedRenderer;
import android.view.View;
import android.view.ViewDebug;
import android.view.ViewGroup;
import android.view.ViewRootImpl;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManagerGlobal;
import android.webkit.WebView;
import com.android.internal.app.IVoiceInteractor;
import com.android.internal.content.ReferrerIntent;
import com.android.internal.os.BinderInternal;
import com.android.internal.os.RuntimeInit;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.FastPrintWriter;
import com.android.internal.util.Preconditions;
import com.android.internal.util.function.HexConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import com.android.internal.util.function.pooled.PooledRunnable;
import com.android.org.conscrypt.OpenSSLSocketImpl;
import com.android.org.conscrypt.TrustedCertificateStore;
import com.oplus.benchmark.OplusBenchHelper;
import com.oplus.darkmode.IOplusDarkModeManager;
import com.oplus.debug.InputLog;
import com.oplus.multiapp.OplusMultiAppManager;
import com.oplus.orms.OplusResourceManager;
import com.oplus.orms.info.OrmsSaParam;
import com.oplus.rp.bridge.IOplusRedPacketManager;
import com.oplus.screenmode.IOplusScreenModeFeature;
import com.oppo.benchmark.OppoBenchAppSwitchManager;
import dalvik.system.CloseGuard;
import dalvik.system.VMDebug;
import dalvik.system.VMRuntime;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import libcore.io.ForwardingOs;
import libcore.io.IoUtils;
import libcore.io.Os;
import libcore.net.event.NetworkEventDispatcher;

public final class ActivityThread extends ClientTransactionHandler {
  static {
    boolean bool;
  }
  
  private static final Bitmap.Config THUMBNAIL_FORMAT = Bitmap.Config.RGB_565;
  
  static boolean localLOGV = false;
  
  static {
    DEBUG_MESSAGES = false;
    DEBUG_BROADCAST = false;
    DEBUG_CONFIGURATION = false;
    DEBUG_SERVICE = false;
    DEBUG_MEMORY_TRIM = false;
    DEBUG_PROVIDER = false;
    DEBUG_ORDER = false;
    sDoFrameOptEnabled = false;
    mFgBrState = 0;
    mBgBrState = 0;
    mOppoFgBrState = 0;
    mOppoBgBrState = 0;
    if (!false) {
      bool = false;
    } else {
      bool = true;
    } 
    DEBUG_BROADCAST_LIGHT = bool;
    mRPChecked = false;
    mRPNeedNotify = false;
    mOrmsManager = null;
    sCurrentBroadcastIntent = new ThreadLocal<>();
  }
  
  private final Object mNetworkPolicyLock = new Object();
  
  private long mNetworkBlockSeq = -1L;
  
  final ApplicationThread mAppThread = new ApplicationThread();
  
  final Looper mLooper = Looper.myLooper();
  
  private static final int ACTIVITY_THREAD_CHECKIN_VERSION = 4;
  
  private static final long CONTENT_PROVIDER_RETAIN_TIME = 1000L;
  
  private static final boolean DEBUG_BACKUP = false;
  
  public static boolean DEBUG_BROADCAST = false;
  
  public static boolean DEBUG_BROADCAST_LIGHT = false;
  
  public static boolean DEBUG_CONFIGURATION = false;
  
  public static boolean DEBUG_MEMORY_TRIM = false;
  
  static boolean DEBUG_MESSAGES = false;
  
  public static boolean DEBUG_ORDER = false;
  
  static boolean DEBUG_PROVIDER = false;
  
  private static final boolean DEBUG_RESULTS = false;
  
  static boolean DEBUG_SERVICE = false;
  
  private static final String HEAP_COLUMN = "%13s %8s %8s %8s %8s %8s %8s %8s %8s";
  
  private static final String HEAP_FULL_COLUMN = "%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s";
  
  public static final long INVALID_PROC_STATE_SEQ = -1L;
  
  public static final int IN_ENQUEUEING = 1;
  
  public static final int IN_FINISHING = 3;
  
  public static final int IN_HANDLING = 2;
  
  public static final int IN_IDLE = 0;
  
  public static final String LAUNCHER_PACKAGE_NAME = "com.oppo.launcher";
  
  private static final long MIN_TIME_BETWEEN_GCS = 5000L;
  
  private static final String ONE_ALT_COUNT_COLUMN = "%21s %8s %21s %8d";
  
  private static final String ONE_COUNT_COLUMN = "%21s %8d";
  
  private static final long PENDING_TOP_PROCESS_STATE_TIMEOUT = 1000L;
  
  public static final String PROC_START_SEQ_IDENT = "seq=";
  
  private static final boolean REPORT_TO_ACTIVITY = true;
  
  public static final int SERVICE_DONE_EXECUTING_ANON = 0;
  
  public static final int SERVICE_DONE_EXECUTING_START = 1;
  
  public static final int SERVICE_DONE_EXECUTING_STOP = 2;
  
  private static final int SQLITE_MEM_RELEASED_EVENT_LOG_TAG = 75003;
  
  public static final String TAG = "ActivityThread";
  
  private static final String THREE_COUNT_COLUMNS = "%21s %8d %21s %8s %21s %8d";
  
  private static final String TWO_COUNT_COLUMNS = "%21s %8d %21s %8d";
  
  private static final String TWO_COUNT_COLUMN_HEADER = "%21s %8s %21s %8s";
  
  private static final int VM_PROCESS_STATE_JANK_IMPERCEPTIBLE = 1;
  
  private static final int VM_PROCESS_STATE_JANK_PERCEPTIBLE = 0;
  
  public static int mBgBrState;
  
  public static int mFgBrState;
  
  public static int mOppoBgBrState;
  
  public static int mOppoFgBrState;
  
  private static OplusResourceManager mOrmsManager;
  
  private static boolean mRPChecked;
  
  private static boolean mRPNeedNotify;
  
  private static volatile ActivityThread sCurrentActivityThread;
  
  private static final ThreadLocal<Intent> sCurrentBroadcastIntent;
  
  public static boolean sDoFrameOptEnabled;
  
  static volatile Handler sMainThreadHandler;
  
  static volatile IPackageManager sPackageManager;
  
  private static volatile IPermissionManager sPermissionManager;
  
  private ArrayList<Pair<IBinder, Consumer<DisplayAdjustments>>> mActiveRotationAdjustments;
  
  final ArrayMap<IBinder, ActivityClientRecord> mActivities;
  
  final Map<IBinder, ClientTransactionItem> mActivitiesToBeDestroyed;
  
  final ArrayList<Application> mAllApplications;
  
  private final SparseArray<ArrayMap<String, BackupAgent>> mBackupAgentsByUser;
  
  AppBindData mBoundApplication;
  
  Configuration mCompatConfiguration;
  
  Configuration mConfiguration;
  
  Bundle mCoreSettings;
  
  int mCurDefaultDisplayDpi;
  
  boolean mDebugLauncher;
  
  boolean mDebugOn;
  
  boolean mDensityCompatMode;
  
  final Executor mExecutor;
  
  final GcIdler mGcIdler;
  
  boolean mGcIdlerScheduled;
  
  final ArrayMap<ProviderKey, Object> mGetProviderLocks;
  
  final H mH;
  
  boolean mHasImeComponent;
  
  boolean mHiddenApiWarningShown;
  
  Application mInitialApplication;
  
  Instrumentation mInstrumentation;
  
  String mInstrumentationAppDir;
  
  String mInstrumentationLibDir;
  
  String mInstrumentationPackageName;
  
  String[] mInstrumentationSplitAppDirs;
  
  String mInstrumentedAppDir;
  
  String mInstrumentedLibDir;
  
  String[] mInstrumentedSplitAppDirs;
  
  ArrayList<WeakReference<AssistStructure>> mLastAssistStructures;
  
  private int mLastProcessState;
  
  private final Map<IBinder, Integer> mLastReportedWindowingMode;
  
  private int mLastSessionId;
  
  final ArrayMap<IBinder, ProviderClientRecord> mLocalProviders;
  
  final ArrayMap<ComponentName, ProviderClientRecord> mLocalProvidersByName;
  
  private Configuration mMainThreadConfig;
  
  ActivityClientRecord mNewActivities;
  
  private final AtomicInteger mNumLaunchingActivities;
  
  int mNumVisibleActivities;
  
  final ArrayMap<Activity, ArrayList<OnActivityPausedListener>> mOnPauseListeners;
  
  final ArrayMap<String, WeakReference<LoadedApk>> mPackages;
  
  Configuration mPendingConfiguration;
  
  private int mPendingProcessState;
  
  Profiler mProfiler;
  
  final ArrayMap<ProviderKey, ProviderClientRecord> mProviderMap;
  
  final ArrayMap<IBinder, ProviderRefCount> mProviderRefCountMap;
  
  final PurgeIdler mPurgeIdler;
  
  boolean mPurgeIdlerScheduled;
  
  final ArrayList<ActivityClientRecord> mRelaunchingActivities;
  
  private Map<SafeCancellationTransport, CancellationSignal> mRemoteCancellations;
  
  final ArrayMap<String, WeakReference<LoadedApk>> mResourcePackages;
  
  private final ResourcesManager mResourcesManager;
  
  final ArrayMap<IBinder, Service> mServices;
  
  boolean mSomeActivitiesChanged;
  
  private ContextImpl mSystemContext;
  
  boolean mSystemThread;
  
  private ContextImpl mSystemUiContext;
  
  private final TransactionExecutor mTransactionExecutor;
  
  private boolean mUpdateHttpProxyOnBind;
  
  ActivityThread() {
    H h = new H();
    this.mExecutor = (Executor)new HandlerExecutor(h);
    this.mActivities = new ArrayMap();
    ArrayMap arrayMap = new ArrayMap();
    this.mActivitiesToBeDestroyed = Collections.synchronizedMap((Map<IBinder, ClientTransactionItem>)arrayMap);
    this.mNewActivities = null;
    this.mNumVisibleActivities = 0;
    this.mNumLaunchingActivities = new AtomicInteger();
    this.mLastProcessState = -1;
    this.mPendingProcessState = -1;
    this.mLastAssistStructures = new ArrayList<>();
    this.mServices = new ArrayMap();
    this.mUpdateHttpProxyOnBind = false;
    this.mAllApplications = new ArrayList<>();
    this.mBackupAgentsByUser = new SparseArray();
    this.mInstrumentationPackageName = null;
    this.mInstrumentationAppDir = null;
    this.mInstrumentationSplitAppDirs = null;
    this.mInstrumentationLibDir = null;
    this.mInstrumentedAppDir = null;
    this.mInstrumentedSplitAppDirs = null;
    this.mInstrumentedLibDir = null;
    this.mSystemThread = false;
    this.mSomeActivitiesChanged = false;
    this.mHiddenApiWarningShown = false;
    this.mPackages = new ArrayMap();
    this.mResourcePackages = new ArrayMap();
    this.mRelaunchingActivities = new ArrayList<>();
    this.mPendingConfiguration = null;
    this.mTransactionExecutor = new TransactionExecutor(this);
    this.mLastReportedWindowingMode = Collections.synchronizedMap((Map<IBinder, Integer>)new ArrayMap());
    this.mProviderMap = new ArrayMap();
    this.mProviderRefCountMap = new ArrayMap();
    this.mLocalProviders = new ArrayMap();
    this.mLocalProvidersByName = new ArrayMap();
    this.mGetProviderLocks = new ArrayMap();
    this.mOnPauseListeners = new ArrayMap();
    this.mGcIdler = new GcIdler();
    this.mPurgeIdler = new PurgeIdler();
    this.mPurgeIdlerScheduled = false;
    this.mGcIdlerScheduled = false;
    this.mCoreSettings = null;
    this.mHasImeComponent = false;
    this.mMainThreadConfig = new Configuration();
    this.mDebugOn = false;
    this.mDebugLauncher = false;
    this.mResourcesManager = ResourcesManager.getInstance();
  }
  
  class ProviderKey {
    final String authority;
    
    final int userId;
    
    public ProviderKey(ActivityThread this$0, int param1Int) {
      this.authority = (String)this$0;
      this.userId = param1Int;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof ProviderKey;
      boolean bool1 = false;
      if (bool) {
        param1Object = param1Object;
        bool = bool1;
        if (Objects.equals(this.authority, ((ProviderKey)param1Object).authority)) {
          bool = bool1;
          if (this.userId == ((ProviderKey)param1Object).userId)
            bool = true; 
        } 
        return bool;
      } 
      return false;
    }
    
    public int hashCode() {
      boolean bool;
      String str = this.authority;
      if (str != null) {
        bool = str.hashCode();
      } else {
        bool = false;
      } 
      return bool ^ this.userId;
    }
  }
  
  class ActivityClientRecord {
    Window window;
    
    IVoiceInteractor voiceInteractor;
    
    public IBinder token;
    
    private Configuration tmpConfig = new Configuration();
    
    boolean stopped;
    
    Bundle state;
    
    boolean startsNotResumed;
    
    String referrer;
    
    ProfilerInfo profilerInfo;
    
    PersistableBundle persistentState;
    
    List<ResultInfo> pendingResults;
    
    List<ReferrerIntent> pendingIntents;
    
    int pendingConfigChanges;
    
    boolean paused;
    
    Activity parent;
    
    public LoadedApk packageInfo;
    
    Configuration overrideConfig;
    
    ActivityClientRecord nextIdle;
    
    Configuration newConfig;
    
    boolean mPreserveWindow;
    
    WindowManager mPendingRemoveWindowManager;
    
    Window mPendingRemoveWindow;
    
    private Configuration mPendingOverrideConfig;
    
    DisplayAdjustments.FixedRotationAdjustments mPendingFixedRotationAdjustments;
    
    private int mLifecycleState = 0;
    
    boolean mIsUserLeaving;
    
    boolean lastReportedTopResumedState;
    
    Activity.NonConfigurationInstances lastNonConfigurationInstances;
    
    boolean isTopResumedActivity;
    
    public final boolean isForward;
    
    Intent intent;
    
    int ident;
    
    boolean hideForNow;
    
    String embeddedID;
    
    Configuration createdConfig;
    
    ViewRootImpl.ActivityConfigCallback configCallback;
    
    CompatibilityInfo compatInfo;
    
    public IBinder assistToken;
    
    ActivityInfo activityInfo;
    
    Activity activity;
    
    public ActivityClientRecord() {
      this.isForward = false;
      init();
    }
    
    public ActivityClientRecord(ActivityThread this$0, Intent param1Intent, int param1Int, ActivityInfo param1ActivityInfo, Configuration param1Configuration, CompatibilityInfo param1CompatibilityInfo, String param1String, IVoiceInteractor param1IVoiceInteractor, Bundle param1Bundle, PersistableBundle param1PersistableBundle, List<ResultInfo> param1List, List<ReferrerIntent> param1List1, boolean param1Boolean, ProfilerInfo param1ProfilerInfo, ClientTransactionHandler param1ClientTransactionHandler, IBinder param1IBinder1, DisplayAdjustments.FixedRotationAdjustments param1FixedRotationAdjustments) {
      this.token = (IBinder)this$0;
      this.assistToken = param1IBinder1;
      this.ident = param1Int;
      this.intent = param1Intent;
      this.referrer = param1String;
      this.voiceInteractor = param1IVoiceInteractor;
      this.activityInfo = param1ActivityInfo;
      this.compatInfo = param1CompatibilityInfo;
      this.state = param1Bundle;
      this.persistentState = param1PersistableBundle;
      this.pendingResults = param1List;
      this.pendingIntents = param1List1;
      this.isForward = param1Boolean;
      this.profilerInfo = param1ProfilerInfo;
      this.overrideConfig = param1Configuration;
      this.packageInfo = param1ClientTransactionHandler.getPackageInfoNoCheck(param1ActivityInfo.applicationInfo, param1CompatibilityInfo);
      this.mPendingFixedRotationAdjustments = param1FixedRotationAdjustments;
      init();
    }
    
    private void init() {
      this.parent = null;
      this.embeddedID = null;
      this.paused = false;
      this.stopped = false;
      this.hideForNow = false;
      this.nextIdle = null;
      this.configCallback = new _$$Lambda$ActivityThread$ActivityClientRecord$HOrG1qglSjSUHSjKBn2rXtX0gGg(this);
    }
    
    public int getLifecycleState() {
      return this.mLifecycleState;
    }
    
    public void setState(int param1Int) {
      this.mLifecycleState = param1Int;
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int == 5) {
                this.paused = true;
                this.stopped = true;
              } 
            } else {
              this.paused = true;
              this.stopped = false;
            } 
          } else {
            this.paused = false;
            this.stopped = false;
          } 
        } else {
          this.paused = true;
          this.stopped = false;
        } 
      } else {
        this.paused = true;
        this.stopped = true;
      } 
    }
    
    private boolean isPreHoneycomb() {
      boolean bool;
      Activity activity = this.activity;
      if (activity != null && (activity.getApplicationInfo()).targetSdkVersion < 11) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private boolean isPreP() {
      boolean bool;
      Activity activity = this.activity;
      if (activity != null && (activity.getApplicationInfo()).targetSdkVersion < 28) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isPersistable() {
      boolean bool;
      if (this.activityInfo.persistableMode == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isVisibleFromServer() {
      boolean bool;
      Activity activity = this.activity;
      if (activity != null && activity.mVisibleFromServer) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String toString() {
      Intent intent = this.intent;
      if (intent != null) {
        ComponentName componentName = intent.getComponent();
      } else {
        intent = null;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ActivityRecord{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" token=");
      stringBuilder.append(this.token);
      stringBuilder.append(" ");
      if (intent == null) {
        null = "no component name";
      } else {
        null = null.toShortString();
      } 
      stringBuilder.append(null);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public String getStateString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ActivityClientRecord{");
      stringBuilder.append("paused=");
      stringBuilder.append(this.paused);
      stringBuilder.append(", stopped=");
      stringBuilder.append(this.stopped);
      stringBuilder.append(", hideForNow=");
      stringBuilder.append(this.hideForNow);
      stringBuilder.append(", startsNotResumed=");
      stringBuilder.append(this.startsNotResumed);
      stringBuilder.append(", isForward=");
      stringBuilder.append(this.isForward);
      stringBuilder.append(", pendingConfigChanges=");
      stringBuilder.append(this.pendingConfigChanges);
      stringBuilder.append(", preserveWindow=");
      stringBuilder.append(this.mPreserveWindow);
      if (this.activity != null) {
        stringBuilder.append(", Activity{");
        stringBuilder.append("resumed=");
        stringBuilder.append(this.activity.mResumed);
        stringBuilder.append(", stopped=");
        stringBuilder.append(this.activity.mStopped);
        stringBuilder.append(", finished=");
        stringBuilder.append(this.activity.isFinishing());
        stringBuilder.append(", destroyed=");
        stringBuilder.append(this.activity.isDestroyed());
        stringBuilder.append(", startedActivity=");
        stringBuilder.append(this.activity.mStartedActivity);
        stringBuilder.append(", changingConfigurations=");
        stringBuilder.append(this.activity.mChangingConfigurations);
        stringBuilder.append("}");
      } 
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class ProviderClientRecord {
    final ContentProviderHolder mHolder;
    
    final ContentProvider mLocalProvider;
    
    final String[] mNames;
    
    final IContentProvider mProvider;
    
    final ActivityThread this$0;
    
    ProviderClientRecord(String[] param1ArrayOfString, IContentProvider param1IContentProvider, ContentProvider param1ContentProvider, ContentProviderHolder param1ContentProviderHolder) {
      this.mNames = param1ArrayOfString;
      this.mProvider = param1IContentProvider;
      this.mLocalProvider = param1ContentProvider;
      this.mHolder = param1ContentProviderHolder;
    }
  }
  
  static final class ReceiverData extends BroadcastReceiver.PendingResult {
    CompatibilityInfo compatInfo;
    
    ActivityInfo info;
    
    Intent intent;
    
    public ReceiverData(Intent param1Intent, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean1, boolean param1Boolean2, IBinder param1IBinder, int param1Int2) {
      super(param1Int1, param1String, param1Bundle, 0, param1Boolean1, param1Boolean2, param1IBinder, param1Int2, i);
      this.intent = param1Intent;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ReceiverData{intent=");
      stringBuilder.append(this.intent);
      stringBuilder.append(" packageName=");
      stringBuilder.append(this.info.packageName);
      stringBuilder.append(" resultCode=");
      stringBuilder.append(getResultCode());
      stringBuilder.append(" resultData=");
      stringBuilder.append(getResultData());
      stringBuilder.append(" resultExtras=");
      stringBuilder.append(getResultExtras(false));
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class CreateBackupAgentData {
    ApplicationInfo appInfo;
    
    int backupMode;
    
    CompatibilityInfo compatInfo;
    
    int userId;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CreateBackupAgentData{appInfo=");
      stringBuilder.append(this.appInfo);
      stringBuilder.append(" backupAgent=");
      stringBuilder.append(this.appInfo.backupAgentName);
      stringBuilder.append(" mode=");
      stringBuilder.append(this.backupMode);
      stringBuilder.append(" userId=");
      stringBuilder.append(this.userId);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class CreateServiceData {
    CompatibilityInfo compatInfo;
    
    ServiceInfo info;
    
    Intent intent;
    
    IBinder token;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("CreateServiceData{token=");
      stringBuilder.append(this.token);
      stringBuilder.append(" className=");
      stringBuilder.append(this.info.name);
      stringBuilder.append(" packageName=");
      stringBuilder.append(this.info.packageName);
      stringBuilder.append(" intent=");
      stringBuilder.append(this.intent);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class BindServiceData {
    Intent intent;
    
    boolean rebind;
    
    IBinder token;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BindServiceData{token=");
      stringBuilder.append(this.token);
      stringBuilder.append(" intent=");
      stringBuilder.append(this.intent);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class ServiceArgsData {
    Intent args;
    
    int flags;
    
    int startId;
    
    boolean taskRemoved;
    
    IBinder token;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ServiceArgsData{token=");
      stringBuilder.append(this.token);
      stringBuilder.append(" startId=");
      stringBuilder.append(this.startId);
      stringBuilder.append(" args=");
      stringBuilder.append(this.args);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class AppBindData {
    ApplicationInfo appInfo;
    
    AutofillOptions autofillOptions;
    
    String buildSerial;
    
    CompatibilityInfo compatInfo;
    
    Configuration config;
    
    ContentCaptureOptions contentCaptureOptions;
    
    int debugMode;
    
    long[] disabledCompatChanges;
    
    boolean enableBinderTracking;
    
    LoadedApk info;
    
    ProfilerInfo initProfilerInfo;
    
    Bundle instrumentationArgs;
    
    ComponentName instrumentationName;
    
    IUiAutomationConnection instrumentationUiAutomationConnection;
    
    IInstrumentationWatcher instrumentationWatcher;
    
    boolean persistent;
    
    String processName;
    
    List<ProviderInfo> providers;
    
    boolean restrictedBackupMode;
    
    boolean trackAllocation;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("AppBindData{appInfo=");
      stringBuilder.append(this.appInfo);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  class Profiler {
    boolean autoStopProfiler;
    
    boolean handlingProfiling;
    
    ParcelFileDescriptor profileFd;
    
    String profileFile;
    
    boolean profiling;
    
    int samplingInterval;
    
    boolean streamingOutput;
    
    public void setProfiler(ProfilerInfo param1ProfilerInfo) {
      ParcelFileDescriptor parcelFileDescriptor1 = param1ProfilerInfo.profileFd;
      if (this.profiling) {
        if (parcelFileDescriptor1 != null)
          try {
            parcelFileDescriptor1.close();
          } catch (IOException iOException) {} 
        return;
      } 
      ParcelFileDescriptor parcelFileDescriptor2 = this.profileFd;
      if (parcelFileDescriptor2 != null)
        try {
          parcelFileDescriptor2.close();
        } catch (IOException iOException1) {} 
      this.profileFile = ((ProfilerInfo)iOException).profileFile;
      this.profileFd = parcelFileDescriptor1;
      this.samplingInterval = ((ProfilerInfo)iOException).samplingInterval;
      this.autoStopProfiler = ((ProfilerInfo)iOException).autoStopProfiler;
      this.streamingOutput = ((ProfilerInfo)iOException).streamingOutput;
    }
    
    public void startProfiling() {
      if (this.profileFd == null || this.profiling)
        return; 
      try {
        boolean bool;
        int i = SystemProperties.getInt("debug.traceview-buffer-size-mb", 8);
        String str = this.profileFile;
        FileDescriptor fileDescriptor = this.profileFd.getFileDescriptor();
        if (this.samplingInterval != 0) {
          bool = true;
        } else {
          bool = false;
        } 
        VMDebug.startMethodTracing(str, fileDescriptor, i * 1024 * 1024, 0, bool, this.samplingInterval, this.streamingOutput);
        this.profiling = true;
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Profiling failed on path ");
        stringBuilder.append(this.profileFile);
        Slog.w("ActivityThread", stringBuilder.toString(), runtimeException);
        try {
          this.profileFd.close();
          this.profileFd = null;
        } catch (IOException iOException) {
          Slog.w("ActivityThread", "Failure closing profile fd", iOException);
        } 
      } 
    }
    
    public void stopProfiling() {
      if (this.profiling) {
        this.profiling = false;
        Debug.stopMethodTracing();
        ParcelFileDescriptor parcelFileDescriptor = this.profileFd;
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } catch (IOException iOException) {} 
        this.profileFd = null;
        this.profileFile = null;
      } 
    }
  }
  
  class DumpComponentInfo {
    String[] args;
    
    ParcelFileDescriptor fd;
    
    String prefix;
    
    IBinder token;
  }
  
  class ContextCleanupInfo {
    ContextImpl context;
    
    String what;
    
    String who;
  }
  
  class DumpHeapData {
    ParcelFileDescriptor fd;
    
    RemoteCallback finishCallback;
    
    public boolean mallocInfo;
    
    public boolean managed;
    
    String path;
    
    public boolean runGc;
  }
  
  class UpdateCompatibilityData {
    CompatibilityInfo info;
    
    String pkg;
  }
  
  class RequestAssistContextExtras {
    IBinder activityToken;
    
    int flags;
    
    IBinder requestToken;
    
    int requestType;
    
    int sessionId;
  }
  
  class ApplicationThread extends IApplicationThread.Stub {
    private static final String DB_INFO_FORMAT = "  %8s %8s %14s %14s  %s";
    
    final ActivityThread this$0;
    
    private ApplicationThread() {}
    
    public final void scheduleReceiver(Intent param1Intent, ActivityInfo param1ActivityInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean, int param1Int2, int param1Int3) {
      ActivityThread.this.updateProcessState(param1Int3, false);
      ApplicationThread applicationThread = ActivityThread.this.mAppThread;
      ActivityThread.ReceiverData receiverData = new ActivityThread.ReceiverData(param1Intent, param1Int1, param1String, param1Bundle, param1Boolean, false, applicationThread.asBinder(), param1Int2);
      receiverData.info = param1ActivityInfo;
      receiverData.compatInfo = param1CompatibilityInfo;
      ActivityThread.this.sendMessage(113, receiverData);
    }
    
    public final void oppoScheduleReceiver(Intent param1Intent, ActivityInfo param1ActivityInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4) {
      if (param1Intent == null) {
        Slog.w("ActivityThread", "oppoScheduleReceiver: illegal intent!");
        return;
      } 
      ActivityThread.this.updateProcessState(param1Int3, false);
      ApplicationThread applicationThread = ActivityThread.this.mAppThread;
      ActivityThread.ReceiverData receiverData = new ActivityThread.ReceiverData(param1Intent, param1Int1, param1String, param1Bundle, param1Boolean, false, applicationThread.asBinder(), param1Int2);
      if (ActivityThread.DEBUG_BROADCAST) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("new ReceiverData() hasCode ");
        stringBuilder.append(param1Int4);
        Slog.i("ActivityThread", stringBuilder.toString());
      } 
      receiverData.setHascode(param1Int4);
      receiverData.info = param1ActivityInfo;
      receiverData.compatInfo = param1CompatibilityInfo;
      if (param1Boolean) {
        param1Int1 = param1Intent.getFlags();
        receiverData.setBroadcastState(param1Int1, 1);
      } 
      if (ActivityThread.DEBUG_BROADCAST_LIGHT || "com.google.android.c2dm.intent.RECEIVE".equals(param1Intent.getAction())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("scheduleReceiver info = ");
        stringBuilder.append(param1ActivityInfo);
        stringBuilder.append(" intent = ");
        stringBuilder.append(param1Intent);
        stringBuilder.append(" sync = ");
        stringBuilder.append(param1Boolean);
        stringBuilder.append(" hasCode = ");
        stringBuilder.append(param1Int4);
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      ActivityThread.this.sendMessage(113, receiverData);
    }
    
    public final void scheduleCreateBackupAgent(ApplicationInfo param1ApplicationInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int1, int param1Int2) {
      ActivityThread.CreateBackupAgentData createBackupAgentData = new ActivityThread.CreateBackupAgentData();
      createBackupAgentData.appInfo = param1ApplicationInfo;
      createBackupAgentData.compatInfo = param1CompatibilityInfo;
      createBackupAgentData.backupMode = param1Int1;
      createBackupAgentData.userId = param1Int2;
      ActivityThread.this.sendMessage(128, createBackupAgentData);
    }
    
    public final void scheduleDestroyBackupAgent(ApplicationInfo param1ApplicationInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int) {
      ActivityThread.CreateBackupAgentData createBackupAgentData = new ActivityThread.CreateBackupAgentData();
      createBackupAgentData.appInfo = param1ApplicationInfo;
      createBackupAgentData.compatInfo = param1CompatibilityInfo;
      createBackupAgentData.userId = param1Int;
      ActivityThread.this.sendMessage(129, createBackupAgentData);
    }
    
    public final void scheduleCreateService(IBinder param1IBinder, ServiceInfo param1ServiceInfo, CompatibilityInfo param1CompatibilityInfo, int param1Int) {
      ActivityThread.this.updateProcessState(param1Int, false);
      ActivityThread.CreateServiceData createServiceData = new ActivityThread.CreateServiceData();
      createServiceData.token = param1IBinder;
      createServiceData.info = param1ServiceInfo;
      createServiceData.compatInfo = param1CompatibilityInfo;
      if (OplusExSystemServiceHelper.getInstance().checkOplusExSystemService(ActivityThread.this.mSystemThread, param1ServiceInfo.name)) {
        ActivityThread.this.handleCreateService(createServiceData);
      } else {
        ActivityThread.this.sendMessage(114, createServiceData);
      } 
    }
    
    public final void scheduleBindService(IBinder param1IBinder, Intent param1Intent, boolean param1Boolean, int param1Int) {
      ActivityThread.this.updateProcessState(param1Int, false);
      ActivityThread.BindServiceData bindServiceData = new ActivityThread.BindServiceData();
      bindServiceData.token = param1IBinder;
      bindServiceData.intent = param1Intent;
      bindServiceData.rebind = param1Boolean;
      if (ActivityThread.DEBUG_SERVICE) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("scheduleBindService token=");
        stringBuilder.append(param1IBinder);
        stringBuilder.append(" intent=");
        stringBuilder.append(param1Intent);
        stringBuilder.append(" uid=");
        stringBuilder.append(Binder.getCallingUid());
        stringBuilder.append(" pid=");
        stringBuilder.append(Binder.getCallingPid());
        String str = stringBuilder.toString();
        Slog.v("ActivityThread", str);
      } 
      if (OplusExSystemServiceHelper.getInstance().checkOplusExSystemService(ActivityThread.this.mSystemThread, param1Intent)) {
        ActivityThread.this.handleBindService(bindServiceData);
      } else {
        ActivityThread.this.sendMessage(121, bindServiceData);
      } 
    }
    
    public final void scheduleUnbindService(IBinder param1IBinder, Intent param1Intent) {
      ActivityThread.BindServiceData bindServiceData = new ActivityThread.BindServiceData();
      bindServiceData.token = param1IBinder;
      bindServiceData.intent = param1Intent;
      ActivityThread.this.sendMessage(122, bindServiceData);
    }
    
    public final void scheduleServiceArgs(IBinder param1IBinder, ParceledListSlice param1ParceledListSlice) {
      List<ServiceStartArgs> list = param1ParceledListSlice.getList();
      for (byte b = 0; b < list.size(); b++) {
        ServiceStartArgs serviceStartArgs = list.get(b);
        ActivityThread.ServiceArgsData serviceArgsData = new ActivityThread.ServiceArgsData();
        serviceArgsData.token = param1IBinder;
        serviceArgsData.taskRemoved = serviceStartArgs.taskRemoved;
        serviceArgsData.startId = serviceStartArgs.startId;
        serviceArgsData.flags = serviceStartArgs.flags;
        serviceArgsData.args = serviceStartArgs.args;
        ActivityThread.this.sendMessage(115, serviceArgsData);
      } 
    }
    
    public final void scheduleStopService(IBinder param1IBinder) {
      ActivityThread.this.sendMessage(116, param1IBinder);
    }
    
    public final void bindApplication(String param1String1, ApplicationInfo param1ApplicationInfo, ProviderInfoList param1ProviderInfoList, ComponentName param1ComponentName, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle1, IInstrumentationWatcher param1IInstrumentationWatcher, IUiAutomationConnection param1IUiAutomationConnection, int param1Int, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, Configuration param1Configuration, CompatibilityInfo param1CompatibilityInfo, Map param1Map, Bundle param1Bundle2, String param1String2, AutofillOptions param1AutofillOptions, ContentCaptureOptions param1ContentCaptureOptions, long[] param1ArrayOflong) {
      if (param1Map != null)
        ServiceManager.initServiceCache(param1Map); 
      setCoreSettings(param1Bundle2);
      ActivityThread.AppBindData appBindData = new ActivityThread.AppBindData();
      appBindData.processName = param1String1;
      appBindData.appInfo = param1ApplicationInfo;
      appBindData.providers = param1ProviderInfoList.getList();
      appBindData.instrumentationName = param1ComponentName;
      appBindData.instrumentationArgs = param1Bundle1;
      appBindData.instrumentationWatcher = param1IInstrumentationWatcher;
      appBindData.instrumentationUiAutomationConnection = param1IUiAutomationConnection;
      appBindData.debugMode = param1Int;
      appBindData.enableBinderTracking = param1Boolean1;
      appBindData.trackAllocation = param1Boolean2;
      appBindData.restrictedBackupMode = param1Boolean3;
      appBindData.persistent = param1Boolean4;
      appBindData.config = param1Configuration;
      appBindData.compatInfo = param1CompatibilityInfo;
      appBindData.initProfilerInfo = param1ProfilerInfo;
      appBindData.buildSerial = param1String2;
      appBindData.autofillOptions = param1AutofillOptions;
      appBindData.contentCaptureOptions = param1ContentCaptureOptions;
      appBindData.disabledCompatChanges = param1ArrayOflong;
      ActivityThread.this.sendMessage(110, appBindData);
    }
    
    public final void runIsolatedEntryPoint(String param1String, String[] param1ArrayOfString) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1String;
      someArgs.arg2 = param1ArrayOfString;
      ActivityThread.this.sendMessage(158, someArgs);
    }
    
    public final void scheduleExit() {
      ActivityThread.this.sendMessage(111, (Object)null);
    }
    
    public final void scheduleSuicide() {
      ActivityThread.this.sendMessage(130, (Object)null);
    }
    
    public void scheduleApplicationInfoChanged(ApplicationInfo param1ApplicationInfo) {
      ActivityThread.this.mH.removeMessages(156, param1ApplicationInfo);
      ActivityThread.this.sendMessage(156, param1ApplicationInfo);
    }
    
    public void updateTimeZone() {
      TimeZone.setDefault(null);
    }
    
    public void clearDnsCache() {
      InetAddress.clearDnsCache();
      NetworkEventDispatcher.getInstance().onNetworkConfigurationChanged();
    }
    
    public void updateHttpProxy() {
      synchronized (ActivityThread.this) {
        Application application = ActivityThread.this.getApplication();
        if (application == null) {
          ActivityThread.access$302(ActivityThread.this, true);
          return;
        } 
        ActivityThread.updateHttpProxy(application);
        return;
      } 
    }
    
    public void processInBackground() {
      ActivityThread.this.mH.removeMessages(120);
      ActivityThread.this.mH.sendMessage(ActivityThread.this.mH.obtainMessage(120));
    }
    
    public void dumpService(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String[] param1ArrayOfString) {
      ActivityThread.DumpComponentInfo dumpComponentInfo = new ActivityThread.DumpComponentInfo();
      try {
        dumpComponentInfo.fd = param1ParcelFileDescriptor.dup();
        dumpComponentInfo.token = param1IBinder;
        dumpComponentInfo.args = param1ArrayOfString;
        ActivityThread.this.sendMessage(123, dumpComponentInfo, 0, 0, true);
      } catch (IOException iOException) {
        Slog.w("ActivityThread", "dumpService failed", iOException);
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
    }
    
    public void scheduleRegisteredReceiver(IIntentReceiver param1IIntentReceiver, Intent param1Intent, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean1, boolean param1Boolean2, int param1Int2, int param1Int3) throws RemoteException {
      ActivityThread.this.updateProcessState(param1Int3, false);
      if (ActivityThread.DEBUG_BROADCAST_LIGHT || (param1Intent != null && "com.google.android.c2dm.intent.RECEIVE".equals(param1Intent.getAction()))) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("scheduleRegisteredReceiver receiver= ");
        stringBuilder.append(param1IIntentReceiver);
        stringBuilder.append(" intent= ");
        stringBuilder.append(param1Intent);
        stringBuilder.append(" ordered= ");
        stringBuilder.append(param1Boolean1);
        stringBuilder.append(" sticky = ");
        stringBuilder.append(param1Boolean2);
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      param1IIntentReceiver.performReceive(param1Intent, param1Int1, param1String, param1Bundle, param1Boolean1, param1Boolean2, param1Int2);
    }
    
    public void scheduleLowMemory() {
      ActivityThread.this.sendMessage(124, (Object)null);
    }
    
    public void profilerControl(boolean param1Boolean, ProfilerInfo param1ProfilerInfo, int param1Int) {
      ActivityThread.this.sendMessage(127, param1ProfilerInfo, param1Boolean, param1Int);
    }
    
    public void dumpHeap(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, String param1String, ParcelFileDescriptor param1ParcelFileDescriptor, RemoteCallback param1RemoteCallback) {
      ActivityThread.DumpHeapData dumpHeapData = new ActivityThread.DumpHeapData();
      dumpHeapData.managed = param1Boolean1;
      dumpHeapData.mallocInfo = param1Boolean2;
      dumpHeapData.runGc = param1Boolean3;
      dumpHeapData.path = param1String;
      try {
        dumpHeapData.fd = param1ParcelFileDescriptor.dup();
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
        dumpHeapData.finishCallback = param1RemoteCallback;
        ActivityThread.this.sendMessage(135, dumpHeapData, 0, 0, true);
        return;
      } catch (IOException iOException) {
        Slog.e("ActivityThread", "Failed to duplicate heap dump file descriptor", iOException);
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
        return;
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
      throw param1String;
    }
    
    public void attachAgent(String param1String) {
      ActivityThread.this.sendMessage(155, param1String);
    }
    
    public void attachStartupAgents(String param1String) {
      ActivityThread.this.sendMessage(162, param1String);
    }
    
    public void setSchedulingGroup(int param1Int) {
      try {
        Process.setProcessGroup(Process.myPid(), param1Int);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed setting process group to ");
        stringBuilder.append(param1Int);
        Slog.w("ActivityThread", stringBuilder.toString(), exception);
      } 
    }
    
    public void dispatchPackageBroadcast(int param1Int, String[] param1ArrayOfString) {
      ActivityThread.this.sendMessage(133, param1ArrayOfString, param1Int);
    }
    
    public void scheduleCrash(String param1String) {
      ActivityThread.this.sendMessage(134, param1String);
    }
    
    public void dumpActivity(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String param1String, String[] param1ArrayOfString) {
      ActivityThread.DumpComponentInfo dumpComponentInfo = new ActivityThread.DumpComponentInfo();
      try {
        dumpComponentInfo.fd = param1ParcelFileDescriptor.dup();
        dumpComponentInfo.token = param1IBinder;
        dumpComponentInfo.prefix = param1String;
        dumpComponentInfo.args = param1ArrayOfString;
        ActivityThread.this.sendMessage(136, dumpComponentInfo, 0, 0, true);
      } catch (IOException iOException) {
        Slog.w("ActivityThread", "dumpActivity failed", iOException);
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
    }
    
    public void dumpProvider(ParcelFileDescriptor param1ParcelFileDescriptor, IBinder param1IBinder, String[] param1ArrayOfString) {
      ActivityThread.DumpComponentInfo dumpComponentInfo = new ActivityThread.DumpComponentInfo();
      try {
        dumpComponentInfo.fd = param1ParcelFileDescriptor.dup();
        dumpComponentInfo.token = param1IBinder;
        dumpComponentInfo.args = param1ArrayOfString;
        ActivityThread.this.sendMessage(141, dumpComponentInfo, 0, 0, true);
      } catch (IOException iOException) {
        Slog.w("ActivityThread", "dumpProvider failed", iOException);
      } finally {}
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
    }
    
    public void dumpMemInfo(ParcelFileDescriptor param1ParcelFileDescriptor, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5, String[] param1ArrayOfString) {
      FileOutputStream fileOutputStream = new FileOutputStream(param1ParcelFileDescriptor.getFileDescriptor());
      FastPrintWriter fastPrintWriter = new FastPrintWriter(fileOutputStream);
      try {
        dumpMemInfo((PrintWriter)fastPrintWriter, param1MemoryInfo, param1Boolean1, param1Boolean2, param1Boolean3, param1Boolean4, param1Boolean5);
        return;
      } finally {
        fastPrintWriter.flush();
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
      } 
    }
    
    private void dumpMemInfo(PrintWriter param1PrintWriter, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5) {
      // Byte code:
      //   0: invokestatic getNativeHeapSize : ()J
      //   3: ldc2_w 1024
      //   6: ldiv
      //   7: lstore #8
      //   9: invokestatic getNativeHeapAllocatedSize : ()J
      //   12: ldc2_w 1024
      //   15: ldiv
      //   16: lstore #10
      //   18: invokestatic getNativeHeapFreeSize : ()J
      //   21: ldc2_w 1024
      //   24: ldiv
      //   25: lstore #12
      //   27: invokestatic getRuntime : ()Ljava/lang/Runtime;
      //   30: astore #14
      //   32: aload #14
      //   34: invokevirtual gc : ()V
      //   37: aload #14
      //   39: invokevirtual totalMemory : ()J
      //   42: ldc2_w 1024
      //   45: ldiv
      //   46: lstore #15
      //   48: aload #14
      //   50: invokevirtual freeMemory : ()J
      //   53: ldc2_w 1024
      //   56: ldiv
      //   57: lstore #17
      //   59: iconst_0
      //   60: istore #19
      //   62: iconst_4
      //   63: anewarray java/lang/Class
      //   66: dup
      //   67: iconst_0
      //   68: ldc android/app/ContextImpl
      //   70: aastore
      //   71: dup
      //   72: iconst_1
      //   73: ldc android/app/Activity
      //   75: aastore
      //   76: dup
      //   77: iconst_2
      //   78: ldc android/webkit/WebView
      //   80: aastore
      //   81: dup
      //   82: iconst_3
      //   83: ldc com/android/org/conscrypt/OpenSSLSocketImpl
      //   85: aastore
      //   86: iconst_1
      //   87: invokestatic countInstancesOfClasses : ([Ljava/lang/Class;Z)[J
      //   90: astore #14
      //   92: aload #14
      //   94: iconst_0
      //   95: laload
      //   96: lstore #20
      //   98: aload #14
      //   100: iconst_1
      //   101: laload
      //   102: lstore #22
      //   104: aload #14
      //   106: iconst_2
      //   107: laload
      //   108: lstore #24
      //   110: aload #14
      //   112: iconst_3
      //   113: laload
      //   114: lstore #26
      //   116: invokestatic getViewInstanceCount : ()J
      //   119: lstore #28
      //   121: invokestatic getViewRootImplCount : ()J
      //   124: lstore #30
      //   126: invokestatic getGlobalAssetCount : ()I
      //   129: istore #32
      //   131: invokestatic getGlobalAssetManagerCount : ()I
      //   134: istore #33
      //   136: invokestatic getBinderLocalObjectCount : ()I
      //   139: istore #34
      //   141: invokestatic getBinderProxyObjectCount : ()I
      //   144: istore #35
      //   146: invokestatic getBinderDeathObjectCount : ()I
      //   149: istore #36
      //   151: invokestatic getGlobalAllocSize : ()J
      //   154: lstore #37
      //   156: invokestatic getGlobalAllocCount : ()J
      //   159: lstore #39
      //   161: invokestatic getDatabaseInfo : ()Landroid/database/sqlite/SQLiteDebug$PagerStats;
      //   164: astore #41
      //   166: invokestatic myPid : ()I
      //   169: istore #42
      //   171: aload_0
      //   172: getfield this$0 : Landroid/app/ActivityThread;
      //   175: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
      //   178: ifnull -> 196
      //   181: aload_0
      //   182: getfield this$0 : Landroid/app/ActivityThread;
      //   185: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
      //   188: getfield processName : Ljava/lang/String;
      //   191: astore #14
      //   193: goto -> 200
      //   196: ldc 'unknown'
      //   198: astore #14
      //   200: aload_1
      //   201: aload_2
      //   202: iload_3
      //   203: iload #4
      //   205: iload #5
      //   207: iload #6
      //   209: iload #42
      //   211: aload #14
      //   213: lload #8
      //   215: lload #10
      //   217: lload #12
      //   219: lload #15
      //   221: lload #15
      //   223: lload #17
      //   225: lsub
      //   226: lload #17
      //   228: invokestatic dumpMemInfoTable : (Ljava/io/PrintWriter;Landroid/os/Debug$MemoryInfo;ZZZZILjava/lang/String;JJJJJJ)V
      //   231: iload_3
      //   232: ifeq -> 554
      //   235: aload_1
      //   236: lload #28
      //   238: invokevirtual print : (J)V
      //   241: aload_1
      //   242: bipush #44
      //   244: invokevirtual print : (C)V
      //   247: aload_1
      //   248: lload #30
      //   250: invokevirtual print : (J)V
      //   253: aload_1
      //   254: bipush #44
      //   256: invokevirtual print : (C)V
      //   259: aload_1
      //   260: lload #20
      //   262: invokevirtual print : (J)V
      //   265: aload_1
      //   266: bipush #44
      //   268: invokevirtual print : (C)V
      //   271: aload_1
      //   272: lload #22
      //   274: invokevirtual print : (J)V
      //   277: aload_1
      //   278: bipush #44
      //   280: invokevirtual print : (C)V
      //   283: aload_1
      //   284: iload #32
      //   286: invokevirtual print : (I)V
      //   289: aload_1
      //   290: bipush #44
      //   292: invokevirtual print : (C)V
      //   295: aload_1
      //   296: iload #33
      //   298: invokevirtual print : (I)V
      //   301: aload_1
      //   302: bipush #44
      //   304: invokevirtual print : (C)V
      //   307: aload_1
      //   308: iload #34
      //   310: invokevirtual print : (I)V
      //   313: aload_1
      //   314: bipush #44
      //   316: invokevirtual print : (C)V
      //   319: aload_1
      //   320: iload #35
      //   322: invokevirtual print : (I)V
      //   325: aload_1
      //   326: bipush #44
      //   328: invokevirtual print : (C)V
      //   331: aload_1
      //   332: iload #36
      //   334: invokevirtual print : (I)V
      //   337: aload_1
      //   338: bipush #44
      //   340: invokevirtual print : (C)V
      //   343: aload_1
      //   344: lload #26
      //   346: invokevirtual print : (J)V
      //   349: aload_1
      //   350: bipush #44
      //   352: invokevirtual print : (C)V
      //   355: aload #41
      //   357: astore_2
      //   358: aload_1
      //   359: aload_2
      //   360: getfield memoryUsed : I
      //   363: sipush #1024
      //   366: idiv
      //   367: invokevirtual print : (I)V
      //   370: aload_1
      //   371: bipush #44
      //   373: invokevirtual print : (C)V
      //   376: aload_1
      //   377: aload_2
      //   378: getfield memoryUsed : I
      //   381: sipush #1024
      //   384: idiv
      //   385: invokevirtual print : (I)V
      //   388: aload_1
      //   389: bipush #44
      //   391: invokevirtual print : (C)V
      //   394: aload_1
      //   395: aload_2
      //   396: getfield pageCacheOverflow : I
      //   399: sipush #1024
      //   402: idiv
      //   403: invokevirtual print : (I)V
      //   406: aload_1
      //   407: bipush #44
      //   409: invokevirtual print : (C)V
      //   412: aload_1
      //   413: aload_2
      //   414: getfield largestMemAlloc : I
      //   417: sipush #1024
      //   420: idiv
      //   421: invokevirtual print : (I)V
      //   424: iconst_0
      //   425: istore #42
      //   427: iload #42
      //   429: aload_2
      //   430: getfield dbStats : Ljava/util/ArrayList;
      //   433: invokevirtual size : ()I
      //   436: if_icmpge -> 549
      //   439: aload_2
      //   440: getfield dbStats : Ljava/util/ArrayList;
      //   443: iload #42
      //   445: invokevirtual get : (I)Ljava/lang/Object;
      //   448: checkcast android/database/sqlite/SQLiteDebug$DbStats
      //   451: astore #14
      //   453: aload_1
      //   454: bipush #44
      //   456: invokevirtual print : (C)V
      //   459: aload_1
      //   460: aload #14
      //   462: getfield dbName : Ljava/lang/String;
      //   465: invokevirtual print : (Ljava/lang/String;)V
      //   468: aload_1
      //   469: bipush #44
      //   471: invokevirtual print : (C)V
      //   474: aload_1
      //   475: aload #14
      //   477: getfield pageSize : J
      //   480: invokevirtual print : (J)V
      //   483: aload_1
      //   484: bipush #44
      //   486: invokevirtual print : (C)V
      //   489: aload_1
      //   490: aload #14
      //   492: getfield dbSize : J
      //   495: invokevirtual print : (J)V
      //   498: aload_1
      //   499: bipush #44
      //   501: invokevirtual print : (C)V
      //   504: aload_1
      //   505: aload #14
      //   507: getfield lookaside : I
      //   510: invokevirtual print : (I)V
      //   513: aload_1
      //   514: bipush #44
      //   516: invokevirtual print : (C)V
      //   519: aload_1
      //   520: aload #14
      //   522: getfield cache : Ljava/lang/String;
      //   525: invokevirtual print : (Ljava/lang/String;)V
      //   528: aload_1
      //   529: bipush #44
      //   531: invokevirtual print : (C)V
      //   534: aload_1
      //   535: aload #14
      //   537: getfield cache : Ljava/lang/String;
      //   540: invokevirtual print : (Ljava/lang/String;)V
      //   543: iinc #42, 1
      //   546: goto -> 427
      //   549: aload_1
      //   550: invokevirtual println : ()V
      //   553: return
      //   554: ldc_w ' '
      //   557: astore_2
      //   558: aload_1
      //   559: ldc_w ' '
      //   562: invokevirtual println : (Ljava/lang/String;)V
      //   565: aload_1
      //   566: ldc_w ' Objects'
      //   569: invokevirtual println : (Ljava/lang/String;)V
      //   572: aload_1
      //   573: ldc_w '%21s %8d %21s %8d'
      //   576: iconst_4
      //   577: anewarray java/lang/Object
      //   580: dup
      //   581: iconst_0
      //   582: ldc_w 'Views:'
      //   585: aastore
      //   586: dup
      //   587: iconst_1
      //   588: lload #28
      //   590: invokestatic valueOf : (J)Ljava/lang/Long;
      //   593: aastore
      //   594: dup
      //   595: iconst_2
      //   596: ldc_w 'ViewRootImpl:'
      //   599: aastore
      //   600: dup
      //   601: iconst_3
      //   602: lload #30
      //   604: invokestatic valueOf : (J)Ljava/lang/Long;
      //   607: aastore
      //   608: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   611: aload_1
      //   612: ldc_w '%21s %8d %21s %8d'
      //   615: iconst_4
      //   616: anewarray java/lang/Object
      //   619: dup
      //   620: iconst_0
      //   621: ldc_w 'AppContexts:'
      //   624: aastore
      //   625: dup
      //   626: iconst_1
      //   627: lload #20
      //   629: invokestatic valueOf : (J)Ljava/lang/Long;
      //   632: aastore
      //   633: dup
      //   634: iconst_2
      //   635: ldc_w 'Activities:'
      //   638: aastore
      //   639: dup
      //   640: iconst_3
      //   641: lload #22
      //   643: invokestatic valueOf : (J)Ljava/lang/Long;
      //   646: aastore
      //   647: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   650: aload_1
      //   651: ldc_w '%21s %8d %21s %8d'
      //   654: iconst_4
      //   655: anewarray java/lang/Object
      //   658: dup
      //   659: iconst_0
      //   660: ldc_w 'Assets:'
      //   663: aastore
      //   664: dup
      //   665: iconst_1
      //   666: iload #32
      //   668: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   671: aastore
      //   672: dup
      //   673: iconst_2
      //   674: ldc_w 'AssetManagers:'
      //   677: aastore
      //   678: dup
      //   679: iconst_3
      //   680: iload #33
      //   682: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   685: aastore
      //   686: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   689: aload_1
      //   690: ldc_w '%21s %8d %21s %8d'
      //   693: iconst_4
      //   694: anewarray java/lang/Object
      //   697: dup
      //   698: iconst_0
      //   699: ldc_w 'Local Binders:'
      //   702: aastore
      //   703: dup
      //   704: iconst_1
      //   705: iload #34
      //   707: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   710: aastore
      //   711: dup
      //   712: iconst_2
      //   713: ldc_w 'Proxy Binders:'
      //   716: aastore
      //   717: dup
      //   718: iconst_3
      //   719: iload #35
      //   721: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   724: aastore
      //   725: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   728: lload #37
      //   730: ldc2_w 1024
      //   733: ldiv
      //   734: lstore #30
      //   736: aload_1
      //   737: ldc_w '%21s %8d %21s %8d'
      //   740: iconst_4
      //   741: anewarray java/lang/Object
      //   744: dup
      //   745: iconst_0
      //   746: ldc_w 'Parcel memory:'
      //   749: aastore
      //   750: dup
      //   751: iconst_1
      //   752: lload #30
      //   754: invokestatic valueOf : (J)Ljava/lang/Long;
      //   757: aastore
      //   758: dup
      //   759: iconst_2
      //   760: ldc_w 'Parcel count:'
      //   763: aastore
      //   764: dup
      //   765: iconst_3
      //   766: lload #39
      //   768: invokestatic valueOf : (J)Ljava/lang/Long;
      //   771: aastore
      //   772: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   775: aload_1
      //   776: ldc_w '%21s %8d %21s %8d'
      //   779: iconst_4
      //   780: anewarray java/lang/Object
      //   783: dup
      //   784: iconst_0
      //   785: ldc_w 'Death Recipients:'
      //   788: aastore
      //   789: dup
      //   790: iconst_1
      //   791: iload #36
      //   793: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   796: aastore
      //   797: dup
      //   798: iconst_2
      //   799: ldc_w 'OpenSSL Sockets:'
      //   802: aastore
      //   803: dup
      //   804: iconst_3
      //   805: lload #26
      //   807: invokestatic valueOf : (J)Ljava/lang/Long;
      //   810: aastore
      //   811: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   814: aload_1
      //   815: ldc_w '%21s %8d'
      //   818: iconst_2
      //   819: anewarray java/lang/Object
      //   822: dup
      //   823: iconst_0
      //   824: ldc_w 'WebViews:'
      //   827: aastore
      //   828: dup
      //   829: iconst_1
      //   830: lload #24
      //   832: invokestatic valueOf : (J)Ljava/lang/Long;
      //   835: aastore
      //   836: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   839: aload_1
      //   840: ldc_w ' '
      //   843: invokevirtual println : (Ljava/lang/String;)V
      //   846: aload_1
      //   847: ldc_w ' SQL'
      //   850: invokevirtual println : (Ljava/lang/String;)V
      //   853: aload_1
      //   854: ldc_w '%21s %8d'
      //   857: iconst_2
      //   858: anewarray java/lang/Object
      //   861: dup
      //   862: iconst_0
      //   863: ldc_w 'MEMORY_USED:'
      //   866: aastore
      //   867: dup
      //   868: iconst_1
      //   869: aload #41
      //   871: getfield memoryUsed : I
      //   874: sipush #1024
      //   877: idiv
      //   878: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   881: aastore
      //   882: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   885: aload #41
      //   887: getfield pageCacheOverflow : I
      //   890: sipush #1024
      //   893: idiv
      //   894: istore #34
      //   896: aload #41
      //   898: getfield largestMemAlloc : I
      //   901: sipush #1024
      //   904: idiv
      //   905: istore #42
      //   907: aload_1
      //   908: ldc_w '%21s %8d %21s %8d'
      //   911: iconst_4
      //   912: anewarray java/lang/Object
      //   915: dup
      //   916: iconst_0
      //   917: ldc_w 'PAGECACHE_OVERFLOW:'
      //   920: aastore
      //   921: dup
      //   922: iconst_1
      //   923: iload #34
      //   925: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   928: aastore
      //   929: dup
      //   930: iconst_2
      //   931: ldc_w 'MALLOC_SIZE:'
      //   934: aastore
      //   935: dup
      //   936: iconst_3
      //   937: iload #42
      //   939: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   942: aastore
      //   943: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   946: aload_1
      //   947: ldc_w ' '
      //   950: invokevirtual println : (Ljava/lang/String;)V
      //   953: aload #41
      //   955: getfield dbStats : Ljava/util/ArrayList;
      //   958: invokevirtual size : ()I
      //   961: istore #42
      //   963: iload #42
      //   965: ifle -> 1174
      //   968: aload_1
      //   969: ldc_w ' DATABASES'
      //   972: invokevirtual println : (Ljava/lang/String;)V
      //   975: aload_1
      //   976: ldc '  %8s %8s %14s %14s  %s'
      //   978: iconst_5
      //   979: anewarray java/lang/Object
      //   982: dup
      //   983: iconst_0
      //   984: ldc_w 'pgsz'
      //   987: aastore
      //   988: dup
      //   989: iconst_1
      //   990: ldc_w 'dbsz'
      //   993: aastore
      //   994: dup
      //   995: iconst_2
      //   996: ldc_w 'Lookaside(b)'
      //   999: aastore
      //   1000: dup
      //   1001: iconst_3
      //   1002: ldc_w 'cache'
      //   1005: aastore
      //   1006: dup
      //   1007: iconst_4
      //   1008: ldc_w 'Dbname'
      //   1011: aastore
      //   1012: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   1015: iconst_0
      //   1016: istore #34
      //   1018: iload #34
      //   1020: iload #42
      //   1022: if_icmpge -> 1171
      //   1025: aload #41
      //   1027: getfield dbStats : Ljava/util/ArrayList;
      //   1030: iload #34
      //   1032: invokevirtual get : (I)Ljava/lang/Object;
      //   1035: checkcast android/database/sqlite/SQLiteDebug$DbStats
      //   1038: astore #43
      //   1040: aload #43
      //   1042: getfield pageSize : J
      //   1045: lconst_0
      //   1046: lcmp
      //   1047: ifle -> 1063
      //   1050: aload #43
      //   1052: getfield pageSize : J
      //   1055: invokestatic valueOf : (J)Ljava/lang/String;
      //   1058: astore #14
      //   1060: goto -> 1066
      //   1063: aload_2
      //   1064: astore #14
      //   1066: aload #43
      //   1068: getfield dbSize : J
      //   1071: lconst_0
      //   1072: lcmp
      //   1073: ifle -> 1089
      //   1076: aload #43
      //   1078: getfield dbSize : J
      //   1081: invokestatic valueOf : (J)Ljava/lang/String;
      //   1084: astore #44
      //   1086: goto -> 1092
      //   1089: aload_2
      //   1090: astore #44
      //   1092: aload #43
      //   1094: getfield lookaside : I
      //   1097: ifle -> 1113
      //   1100: aload #43
      //   1102: getfield lookaside : I
      //   1105: invokestatic valueOf : (I)Ljava/lang/String;
      //   1108: astore #45
      //   1110: goto -> 1116
      //   1113: aload_2
      //   1114: astore #45
      //   1116: aload #43
      //   1118: getfield cache : Ljava/lang/String;
      //   1121: astore #46
      //   1123: aload #43
      //   1125: getfield dbName : Ljava/lang/String;
      //   1128: astore #43
      //   1130: aload_1
      //   1131: ldc '  %8s %8s %14s %14s  %s'
      //   1133: iconst_5
      //   1134: anewarray java/lang/Object
      //   1137: dup
      //   1138: iconst_0
      //   1139: aload #14
      //   1141: aastore
      //   1142: dup
      //   1143: iconst_1
      //   1144: aload #44
      //   1146: aastore
      //   1147: dup
      //   1148: iconst_2
      //   1149: aload #45
      //   1151: aastore
      //   1152: dup
      //   1153: iconst_3
      //   1154: aload #46
      //   1156: aastore
      //   1157: dup
      //   1158: iconst_4
      //   1159: aload #43
      //   1161: aastore
      //   1162: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
      //   1165: iinc #34, 1
      //   1168: goto -> 1018
      //   1171: goto -> 1178
      //   1174: ldc_w ' '
      //   1177: astore_2
      //   1178: invokestatic getAssetAllocations : ()Ljava/lang/String;
      //   1181: astore #14
      //   1183: aload #14
      //   1185: ifnull -> 1209
      //   1188: aload_1
      //   1189: aload_2
      //   1190: invokevirtual println : (Ljava/lang/String;)V
      //   1193: aload_1
      //   1194: ldc_w ' Asset Allocations'
      //   1197: invokevirtual println : (Ljava/lang/String;)V
      //   1200: aload_1
      //   1201: aload #14
      //   1203: invokevirtual print : (Ljava/lang/String;)V
      //   1206: goto -> 1209
      //   1209: iload #7
      //   1211: ifeq -> 1278
      //   1214: aload_0
      //   1215: getfield this$0 : Landroid/app/ActivityThread;
      //   1218: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
      //   1221: ifnull -> 1242
      //   1224: iconst_2
      //   1225: aload_0
      //   1226: getfield this$0 : Landroid/app/ActivityThread;
      //   1229: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
      //   1232: getfield appInfo : Landroid/content/pm/ApplicationInfo;
      //   1235: getfield flags : I
      //   1238: iand
      //   1239: ifne -> 1251
      //   1242: iload #19
      //   1244: istore_3
      //   1245: getstatic android/os/Build.IS_DEBUGGABLE : Z
      //   1248: ifeq -> 1253
      //   1251: iconst_1
      //   1252: istore_3
      //   1253: aload_1
      //   1254: aload_2
      //   1255: invokevirtual println : (Ljava/lang/String;)V
      //   1258: aload_1
      //   1259: ldc_w ' Unreachable memory'
      //   1262: invokevirtual println : (Ljava/lang/String;)V
      //   1265: aload_1
      //   1266: bipush #100
      //   1268: iload_3
      //   1269: invokestatic getUnreachableMemory : (IZ)Ljava/lang/String;
      //   1272: invokevirtual print : (Ljava/lang/String;)V
      //   1275: goto -> 1278
      //   1278: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1423	-> 0
      //   #1424	-> 9
      //   #1425	-> 18
      //   #1427	-> 27
      //   #1428	-> 32
      //   #1429	-> 37
      //   #1430	-> 48
      //   #1431	-> 59
      //   #1433	-> 59
      //   #1439	-> 62
      //   #1440	-> 92
      //   #1441	-> 98
      //   #1442	-> 104
      //   #1443	-> 110
      //   #1445	-> 116
      //   #1446	-> 121
      //   #1447	-> 126
      //   #1448	-> 131
      //   #1449	-> 136
      //   #1450	-> 141
      //   #1451	-> 146
      //   #1452	-> 151
      //   #1453	-> 156
      //   #1454	-> 161
      //   #1456	-> 166
      //   #1457	-> 166
      //   #1458	-> 171
      //   #1456	-> 200
      //   #1462	-> 231
      //   #1467	-> 235
      //   #1468	-> 247
      //   #1469	-> 259
      //   #1470	-> 271
      //   #1472	-> 283
      //   #1473	-> 295
      //   #1474	-> 307
      //   #1475	-> 319
      //   #1477	-> 331
      //   #1478	-> 343
      //   #1481	-> 355
      //   #1482	-> 376
      //   #1483	-> 394
      //   #1484	-> 412
      //   #1485	-> 424
      //   #1486	-> 439
      //   #1487	-> 453
      //   #1488	-> 468
      //   #1489	-> 483
      //   #1490	-> 498
      //   #1491	-> 513
      //   #1492	-> 528
      //   #1485	-> 543
      //   #1494	-> 549
      //   #1496	-> 553
      //   #1499	-> 554
      //   #1500	-> 565
      //   #1501	-> 572
      //   #1502	-> 572
      //   #1501	-> 572
      //   #1504	-> 611
      //   #1505	-> 611
      //   #1504	-> 611
      //   #1507	-> 650
      //   #1508	-> 650
      //   #1507	-> 650
      //   #1510	-> 689
      //   #1511	-> 689
      //   #1510	-> 689
      //   #1512	-> 728
      //   #1513	-> 736
      //   #1512	-> 736
      //   #1514	-> 775
      //   #1515	-> 775
      //   #1514	-> 775
      //   #1516	-> 814
      //   #1519	-> 839
      //   #1520	-> 846
      //   #1521	-> 853
      //   #1522	-> 885
      //   #1523	-> 896
      //   #1522	-> 907
      //   #1524	-> 946
      //   #1525	-> 953
      //   #1526	-> 963
      //   #1527	-> 968
      //   #1528	-> 975
      //   #1530	-> 1015
      //   #1531	-> 1025
      //   #1532	-> 1040
      //   #1533	-> 1040
      //   #1534	-> 1066
      //   #1535	-> 1092
      //   #1532	-> 1130
      //   #1530	-> 1165
      //   #1526	-> 1174
      //   #1541	-> 1178
      //   #1542	-> 1183
      //   #1543	-> 1188
      //   #1544	-> 1193
      //   #1545	-> 1200
      //   #1542	-> 1209
      //   #1549	-> 1209
      //   #1550	-> 1214
      //   #1553	-> 1253
      //   #1554	-> 1258
      //   #1555	-> 1265
      //   #1549	-> 1278
      //   #1557	-> 1278
    }
    
    public void dumpMemInfoProto(ParcelFileDescriptor param1ParcelFileDescriptor, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4, String[] param1ArrayOfString) {
      ProtoOutputStream protoOutputStream = new ProtoOutputStream(param1ParcelFileDescriptor.getFileDescriptor());
      try {
        dumpMemInfo(protoOutputStream, param1MemoryInfo, param1Boolean1, param1Boolean2, param1Boolean3, param1Boolean4);
        return;
      } finally {
        protoOutputStream.flush();
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
      } 
    }
    
    private void dumpMemInfo(ProtoOutputStream param1ProtoOutputStream, Debug.MemoryInfo param1MemoryInfo, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, boolean param1Boolean4) {
      String str2;
      long l1 = Debug.getNativeHeapSize() / 1024L;
      long l2 = Debug.getNativeHeapAllocatedSize() / 1024L;
      long l3 = Debug.getNativeHeapFreeSize() / 1024L;
      Runtime runtime = Runtime.getRuntime();
      runtime.gc();
      long l4 = runtime.totalMemory() / 1024L;
      long l5 = runtime.freeMemory() / 1024L;
      param1Boolean1 = false;
      long[] arrayOfLong = VMDebug.countInstancesOfClasses(new Class[] { ContextImpl.class, Activity.class, WebView.class, OpenSSLSocketImpl.class }, true);
      long l6 = arrayOfLong[0];
      long l7 = arrayOfLong[1];
      long l8 = arrayOfLong[2];
      long l9 = arrayOfLong[3];
      long l10 = ViewDebug.getViewInstanceCount();
      long l11 = ViewDebug.getViewRootImplCount();
      int i = AssetManager.getGlobalAssetCount();
      int j = AssetManager.getGlobalAssetManagerCount();
      int k = Debug.getBinderLocalObjectCount();
      int m = Debug.getBinderProxyObjectCount();
      int n = Debug.getBinderDeathObjectCount();
      long l12 = Parcel.getGlobalAllocSize();
      long l13 = Parcel.getGlobalAllocCount();
      SQLiteDebug.PagerStats pagerStats2 = SQLiteDebug.getDatabaseInfo();
      long l14 = param1ProtoOutputStream.start(1146756268033L);
      param1ProtoOutputStream.write(1120986464257L, Process.myPid());
      if (ActivityThread.this.mBoundApplication != null) {
        str2 = ActivityThread.this.mBoundApplication.processName;
      } else {
        str2 = "unknown";
      } 
      param1ProtoOutputStream.write(1138166333442L, str2);
      ActivityThread.dumpMemInfoTable(param1ProtoOutputStream, param1MemoryInfo, param1Boolean2, param1Boolean3, l1, l2, l3, l4, l4 - l5, l5);
      param1ProtoOutputStream.end(l14);
      l1 = param1ProtoOutputStream.start(1146756268034L);
      param1ProtoOutputStream.write(1120986464257L, l10);
      param1ProtoOutputStream.write(1120986464258L, l11);
      param1ProtoOutputStream.write(1120986464259L, l6);
      param1ProtoOutputStream.write(1120986464260L, l7);
      param1ProtoOutputStream.write(1120986464261L, i);
      param1ProtoOutputStream.write(1120986464262L, j);
      param1ProtoOutputStream.write(1120986464263L, k);
      param1ProtoOutputStream.write(1120986464264L, m);
      param1ProtoOutputStream.write(1112396529673L, l12 / 1024L);
      param1ProtoOutputStream.write(1120986464266L, l13);
      param1ProtoOutputStream.write(1120986464267L, n);
      param1ProtoOutputStream.write(1120986464268L, l9);
      param1ProtoOutputStream.write(1120986464269L, l8);
      param1ProtoOutputStream.end(l1);
      l12 = param1ProtoOutputStream.start(1146756268035L);
      SQLiteDebug.PagerStats pagerStats1 = pagerStats2;
      param1ProtoOutputStream.write(1120986464257L, pagerStats1.memoryUsed / 1024);
      param1ProtoOutputStream.write(1120986464258L, pagerStats1.pageCacheOverflow / 1024);
      param1ProtoOutputStream.write(1120986464259L, pagerStats1.largestMemAlloc / 1024);
      j = pagerStats1.dbStats.size();
      for (k = 0; k < j; k++) {
        SQLiteDebug.DbStats dbStats = pagerStats1.dbStats.get(k);
        l9 = param1ProtoOutputStream.start(2246267895812L);
        param1ProtoOutputStream.write(1138166333441L, dbStats.dbName);
        param1ProtoOutputStream.write(1120986464258L, dbStats.pageSize);
        param1ProtoOutputStream.write(1120986464259L, dbStats.dbSize);
        param1ProtoOutputStream.write(1120986464260L, dbStats.lookaside);
        param1ProtoOutputStream.write(1138166333445L, dbStats.cache);
        param1ProtoOutputStream.end(l9);
      } 
      param1ProtoOutputStream.end(l12);
      String str1 = AssetManager.getAssetAllocations();
      if (str1 != null)
        param1ProtoOutputStream.write(1138166333444L, str1); 
      if (param1Boolean4) {
        if (ActivityThread.this.mBoundApplication == null) {
          k = 0;
        } else {
          k = ActivityThread.this.mBoundApplication.appInfo.flags;
        } 
        if ((k & 0x2) != 0 || Build.IS_DEBUGGABLE)
          param1Boolean1 = true; 
        str1 = Debug.getUnreachableMemory(100, param1Boolean1);
        param1ProtoOutputStream.write(1138166333445L, str1);
      } 
    }
    
    public void dumpGfxInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) {
      ActivityThread.this.nDumpGraphicsInfo(param1ParcelFileDescriptor.getFileDescriptor());
      WindowManagerGlobal.getInstance().dumpGfxInfo(param1ParcelFileDescriptor.getFileDescriptor(), param1ArrayOfString);
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
    }
    
    public void dumpCacheInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) {
      PropertyInvalidatedCache.dumpCacheInfo(param1ParcelFileDescriptor.getFileDescriptor(), param1ArrayOfString);
      IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
    }
    
    private File getDatabasesDir(Context param1Context) {
      return param1Context.getDatabasePath("a").getParentFile();
    }
    
    private void dumpDatabaseInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString, boolean param1Boolean) {
      FastPrintWriter fastPrintWriter = new FastPrintWriter(new FileOutputStream(param1ParcelFileDescriptor.getFileDescriptor()));
      PrintWriterPrinter printWriterPrinter = new PrintWriterPrinter((PrintWriter)fastPrintWriter);
      SQLiteDebug.dump((Printer)printWriterPrinter, param1ArrayOfString, param1Boolean);
      fastPrintWriter.flush();
    }
    
    public void dumpDbInfo(ParcelFileDescriptor param1ParcelFileDescriptor, String[] param1ArrayOfString) {
      if (ActivityThread.this.mSystemThread) {
        try {
          ParcelFileDescriptor parcelFileDescriptor = param1ParcelFileDescriptor.dup();
          IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
          AsyncTask.THREAD_POOL_EXECUTOR.execute((Runnable)new Object(this, parcelFileDescriptor, param1ArrayOfString));
        } catch (IOException iOException) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Could not dup FD ");
          stringBuilder.append(param1ParcelFileDescriptor.getFileDescriptor().getInt$());
          Log.w("ActivityThread", stringBuilder.toString());
          IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
          return;
        } finally {}
      } else {
        dumpDatabaseInfo(param1ParcelFileDescriptor, param1ArrayOfString, false);
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
      } 
    }
    
    public void unstableProviderDied(IBinder param1IBinder) {
      ActivityThread.this.sendMessage(142, param1IBinder);
    }
    
    public void requestAssistContextExtras(IBinder param1IBinder1, IBinder param1IBinder2, int param1Int1, int param1Int2, int param1Int3) {
      ActivityThread.RequestAssistContextExtras requestAssistContextExtras = new ActivityThread.RequestAssistContextExtras();
      requestAssistContextExtras.activityToken = param1IBinder1;
      requestAssistContextExtras.requestToken = param1IBinder2;
      requestAssistContextExtras.requestType = param1Int1;
      requestAssistContextExtras.sessionId = param1Int2;
      requestAssistContextExtras.flags = param1Int3;
      ActivityThread.this.sendMessage(143, requestAssistContextExtras);
    }
    
    public void setCoreSettings(Bundle param1Bundle) {
      ActivityThread.this.sendMessage(138, param1Bundle);
    }
    
    public void updatePackageCompatibilityInfo(String param1String, CompatibilityInfo param1CompatibilityInfo) {
      ActivityThread.UpdateCompatibilityData updateCompatibilityData = new ActivityThread.UpdateCompatibilityData();
      updateCompatibilityData.pkg = param1String;
      updateCompatibilityData.info = param1CompatibilityInfo;
      ActivityThread.this.sendMessage(139, updateCompatibilityData);
    }
    
    public void scheduleTrimMemory(int param1Int) {
      -$.Lambda.ActivityThread.ApplicationThread.tUGFX7CUhzB4Pg5wFd5yeqOnu38 tUGFX7CUhzB4Pg5wFd5yeqOnu38 = _$$Lambda$ActivityThread$ApplicationThread$tUGFX7CUhzB4Pg5wFd5yeqOnu38.INSTANCE;
      ActivityThread activityThread = ActivityThread.this;
      PooledRunnable pooledRunnable1 = PooledLambda.obtainRunnable((BiConsumer)tUGFX7CUhzB4Pg5wFd5yeqOnu38, activityThread, Integer.valueOf(param1Int));
      PooledRunnable pooledRunnable2 = pooledRunnable1.recycleOnUse();
      Choreographer choreographer = Choreographer.getMainThreadInstance();
      if (choreographer != null) {
        choreographer.postCallback(4, (Runnable)pooledRunnable2, null);
      } else {
        ActivityThread.this.mH.post((Runnable)pooledRunnable2);
      } 
    }
    
    public void scheduleTranslucentConversionComplete(IBinder param1IBinder, boolean param1Boolean) {
      ActivityThread.this.sendMessage(144, param1IBinder, param1Boolean);
    }
    
    public void scheduleOnNewActivityOptions(IBinder param1IBinder, Bundle param1Bundle) {
      ActivityThread activityThread = ActivityThread.this;
      Pair pair = new Pair(param1IBinder, ActivityOptions.fromBundle(param1Bundle));
      activityThread.sendMessage(146, pair);
    }
    
    public void setProcessState(int param1Int) {
      ActivityThread.this.updateProcessState(param1Int, true);
    }
    
    public void setNetworkBlockSeq(long param1Long) {
      synchronized (ActivityThread.this.mNetworkPolicyLock) {
        ActivityThread.access$1002(ActivityThread.this, param1Long);
        return;
      } 
    }
    
    public void scheduleInstallProvider(ProviderInfo param1ProviderInfo) {
      ActivityThread.this.sendMessage(145, param1ProviderInfo);
    }
    
    public final void updateTimePrefs(int param1Int) {
      Boolean bool;
      if (param1Int == 0) {
        bool = Boolean.FALSE;
      } else if (param1Int == 1) {
        bool = Boolean.TRUE;
      } else {
        bool = null;
      } 
      DateFormat.set24HourTimePref(bool);
    }
    
    public void scheduleEnterAnimationComplete(IBinder param1IBinder) {
      ActivityThread.this.sendMessage(149, param1IBinder);
    }
    
    public void notifyCleartextNetwork(byte[] param1ArrayOfbyte) {
      if (StrictMode.vmCleartextNetworkEnabled())
        StrictMode.onCleartextNetworkDetected(param1ArrayOfbyte); 
    }
    
    public void startBinderTracking() {
      ActivityThread.this.sendMessage(150, (Object)null);
    }
    
    public void stopBinderTrackingAndDump(ParcelFileDescriptor param1ParcelFileDescriptor) {
      try {
        ActivityThread.this.sendMessage(151, param1ParcelFileDescriptor.dup());
      } catch (IOException iOException) {
      
      } finally {
        IoUtils.closeQuietly((AutoCloseable)param1ParcelFileDescriptor);
      } 
    }
    
    public void scheduleLocalVoiceInteractionStarted(IBinder param1IBinder, IVoiceInteractor param1IVoiceInteractor) throws RemoteException {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.arg1 = param1IBinder;
      someArgs.arg2 = param1IVoiceInteractor;
      ActivityThread.this.sendMessage(154, someArgs);
    }
    
    public void handleTrustStorageUpdate() {
      NetworkSecurityPolicy.getInstance().handleTrustStorageUpdate();
    }
    
    public void scheduleTransaction(ClientTransaction param1ClientTransaction) throws RemoteException {
      ActivityThread.this.scheduleTransaction(param1ClientTransaction);
    }
    
    public void scheduleApplicationInfoChangedForSwitchUser(ApplicationInfo param1ApplicationInfo, int param1Int) {
      ActivityThread.this.mH.removeMessages(156, param1ApplicationInfo);
      ActivityThread.this.sendMessage(156, param1ApplicationInfo, 1, param1Int);
    }
    
    public void setDynamicalLogEnable(boolean param1Boolean) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setDynamicalLogEnable on ");
      stringBuilder.append(param1Boolean);
      Slog.v("ActivityThread", stringBuilder.toString());
      ActivityDynamicLogHelper.setDynamicalLogEnable(param1Boolean);
    }
    
    public void getBroadcastState(int param1Int) {
      if ((0x80000 & param1Int) != 0) {
        if ((param1Int & 0x10000000) != 0) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getBroadcastState mOppoFgBrState ");
          stringBuilder.append(ActivityThread.mOppoFgBrState);
          Slog.v("ActivityThread", stringBuilder.toString());
          param1Int = ActivityThread.mOppoFgBrState;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("getBroadcastState mOppoBgBrState ");
          stringBuilder.append(ActivityThread.mOppoBgBrState);
          Slog.v("ActivityThread", stringBuilder.toString());
          param1Int = ActivityThread.mOppoBgBrState;
        } 
      } else if ((param1Int & 0x10000000) != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getBroadcastState mFgBrState ");
        stringBuilder.append(ActivityThread.mFgBrState);
        Slog.v("ActivityThread", stringBuilder.toString());
        param1Int = ActivityThread.mFgBrState;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getBroadcastState mBgBrState ");
        stringBuilder.append(ActivityThread.mBgBrState);
        Slog.v("ActivityThread", stringBuilder.toString());
        param1Int = ActivityThread.mBgBrState;
      } 
      if (param1Int == 1)
        try {
          Looper looper = Looper.getMainLooper();
          MessageQueue messageQueue = looper.getQueue();
          messageQueue.dumpMessage();
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failure dump msg ");
          stringBuilder.append(exception);
          Log.e("ActivityThread", stringBuilder.toString());
        }  
    }
    
    public void setMirageWindowState(boolean param1Boolean) {
      if (ActivityThread.this.getApplication() != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("setMirageWindowState inMirage: ");
        stringBuilder.append(param1Boolean);
        Log.v("ActivityThread", stringBuilder.toString());
        SensorManager sensorManager = (SensorManager)ActivityThread.this.getApplication().getSystemService("sensor");
        SensorManager.mInMirage = param1Boolean;
      } 
    }
    
    public void requestDirectActions(IBinder param1IBinder, IVoiceInteractor param1IVoiceInteractor, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) {
      CancellationSignal cancellationSignal = new CancellationSignal();
      if (param1RemoteCallback1 != null) {
        ActivityThread.SafeCancellationTransport safeCancellationTransport = ActivityThread.this.createSafeCancellationTransport(cancellationSignal);
        Bundle bundle = new Bundle();
        IBinder iBinder = safeCancellationTransport.asBinder();
        bundle.putBinder("key_cancellation_signal", iBinder);
        param1RemoteCallback1.sendResult(bundle);
      } 
      ActivityThread.this.mH.sendMessage(PooledLambda.obtainMessage((QuintConsumer)_$$Lambda$ActivityThread$ApplicationThread$uR_ee_5oPoxu4U_by7wU55jwtdU.INSTANCE, ActivityThread.this, param1IBinder, param1IVoiceInteractor, cancellationSignal, param1RemoteCallback2));
    }
    
    public void performDirectAction(IBinder param1IBinder, String param1String, Bundle param1Bundle, RemoteCallback param1RemoteCallback1, RemoteCallback param1RemoteCallback2) {
      CancellationSignal cancellationSignal = new CancellationSignal();
      if (param1RemoteCallback1 != null) {
        ActivityThread.SafeCancellationTransport safeCancellationTransport = ActivityThread.this.createSafeCancellationTransport(cancellationSignal);
        Bundle bundle = new Bundle();
        IBinder iBinder = safeCancellationTransport.asBinder();
        bundle.putBinder("key_cancellation_signal", iBinder);
        param1RemoteCallback1.sendResult(bundle);
      } 
      ActivityThread.this.mH.sendMessage(PooledLambda.obtainMessage((HexConsumer)_$$Lambda$ActivityThread$ApplicationThread$nBC_BR7B9W6ftKAxur3BC53SJYc.INSTANCE, ActivityThread.this, param1IBinder, param1String, param1Bundle, cancellationSignal, param1RemoteCallback2));
    }
  }
  
  private SafeCancellationTransport createSafeCancellationTransport(CancellationSignal paramCancellationSignal) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRemoteCancellations : Ljava/util/Map;
    //   6: ifnonnull -> 22
    //   9: new android/util/ArrayMap
    //   12: astore_2
    //   13: aload_2
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_2
    //   19: putfield mRemoteCancellations : Ljava/util/Map;
    //   22: new android/app/ActivityThread$SafeCancellationTransport
    //   25: astore_2
    //   26: aload_2
    //   27: aload_0
    //   28: aload_1
    //   29: invokespecial <init> : (Landroid/app/ActivityThread;Landroid/os/CancellationSignal;)V
    //   32: aload_0
    //   33: getfield mRemoteCancellations : Ljava/util/Map;
    //   36: aload_2
    //   37: aload_1
    //   38: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   43: pop
    //   44: aload_0
    //   45: monitorexit
    //   46: aload_2
    //   47: areturn
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1977	-> 0
    //   #1978	-> 2
    //   #1979	-> 9
    //   #1981	-> 22
    //   #1983	-> 32
    //   #1984	-> 44
    //   #1985	-> 48
    // Exception table:
    //   from	to	target	type
    //   2	9	48	finally
    //   9	22	48	finally
    //   22	32	48	finally
    //   32	44	48	finally
    //   44	46	48	finally
    //   49	51	48	finally
  }
  
  private CancellationSignal removeSafeCancellationTransport(SafeCancellationTransport paramSafeCancellationTransport) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mRemoteCancellations : Ljava/util/Map;
    //   6: aload_1
    //   7: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   12: checkcast android/os/CancellationSignal
    //   15: astore_1
    //   16: aload_0
    //   17: getfield mRemoteCancellations : Ljava/util/Map;
    //   20: invokeinterface isEmpty : ()Z
    //   25: ifeq -> 33
    //   28: aload_0
    //   29: aconst_null
    //   30: putfield mRemoteCancellations : Ljava/util/Map;
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: areturn
    //   37: astore_1
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_1
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1990	-> 0
    //   #1991	-> 2
    //   #1992	-> 16
    //   #1993	-> 28
    //   #1995	-> 33
    //   #1996	-> 37
    // Exception table:
    //   from	to	target	type
    //   2	16	37	finally
    //   16	28	37	finally
    //   28	33	37	finally
    //   33	35	37	finally
    //   38	40	37	finally
  }
  
  class SafeCancellationTransport extends ICancellationSignal.Stub {
    private final WeakReference<ActivityThread> mWeakActivityThread;
    
    SafeCancellationTransport(ActivityThread this$0, CancellationSignal param1CancellationSignal) {
      this.mWeakActivityThread = new WeakReference<>(this$0);
    }
    
    public void cancel() {
      ActivityThread activityThread = this.mWeakActivityThread.get();
      if (activityThread != null) {
        CancellationSignal cancellationSignal = activityThread.removeSafeCancellationTransport(this);
        if (cancellationSignal != null)
          cancellationSignal.cancel(); 
      } 
    }
  }
  
  class H extends Handler {
    public static final int APPLICATION_INFO_CHANGED = 156;
    
    public static final int ATTACH_AGENT = 155;
    
    public static final int ATTACH_STARTUP_AGENTS = 162;
    
    public static final int BIND_APPLICATION = 110;
    
    public static final int BIND_SERVICE = 121;
    
    public static final int CLEAN_UP_CONTEXT = 119;
    
    public static final int CONFIGURATION_CHANGED = 118;
    
    public static final int CREATE_BACKUP_AGENT = 128;
    
    public static final int CREATE_SERVICE = 114;
    
    public static final int DESTROY_BACKUP_AGENT = 129;
    
    public static final int DISPATCH_PACKAGE_BROADCAST = 133;
    
    public static final int DUMP_ACTIVITY = 136;
    
    public static final int DUMP_HEAP = 135;
    
    public static final int DUMP_PROVIDER = 141;
    
    public static final int DUMP_SERVICE = 123;
    
    public static final int ENTER_ANIMATION_COMPLETE = 149;
    
    public static final int EXECUTE_TRANSACTION = 159;
    
    public static final int EXIT_APPLICATION = 111;
    
    public static final int GC_WHEN_IDLE = 120;
    
    public static final int INSTALL_PROVIDER = 145;
    
    public static final int LOCAL_VOICE_INTERACTION_STARTED = 154;
    
    public static final int LOW_MEMORY = 124;
    
    public static final int ON_NEW_ACTIVITY_OPTIONS = 146;
    
    public static final int PROFILER_CONTROL = 127;
    
    public static final int PURGE_RESOURCES = 161;
    
    public static final int RECEIVER = 113;
    
    public static final int RELAUNCH_ACTIVITY = 160;
    
    public static final int REMOVE_PROVIDER = 131;
    
    public static final int REQUEST_ASSIST_CONTEXT_EXTRAS = 143;
    
    public static final int RUN_ISOLATED_ENTRY_POINT = 158;
    
    public static final int SCHEDULE_CRASH = 134;
    
    public static final int SERVICE_ARGS = 115;
    
    public static final int SET_CORE_SETTINGS = 138;
    
    public static final int SLEEPING = 137;
    
    public static final int START_BINDER_TRACKING = 150;
    
    public static final int START_INPUT_WATCHING = 9999;
    
    public static final int STOP_BINDER_TRACKING_AND_DUMP = 151;
    
    public static final int STOP_SERVICE = 116;
    
    public static final int SUICIDE = 130;
    
    public static final int TRANSLUCENT_CONVERSION_COMPLETE = 144;
    
    public static final int UNBIND_SERVICE = 122;
    
    public static final int UNSTABLE_PROVIDER_DIED = 142;
    
    public static final int UPDATE_PACKAGE_COMPATIBILITY_INFO = 139;
    
    final ActivityThread this$0;
    
    String codeToString(int param1Int) {
      if (ActivityThread.DEBUG_MESSAGES) {
        if (param1Int != 110) {
          if (param1Int != 111) {
            if (param1Int != 138) {
              if (param1Int != 139) {
                if (param1Int != 149) {
                  if (param1Int != 9999) {
                    switch (param1Int) {
                      default:
                        switch (param1Int) {
                          default:
                            switch (param1Int) {
                              default:
                                switch (param1Int) {
                                  default:
                                    switch (param1Int) {
                                      default:
                                        switch (param1Int) {
                                          default:
                                            switch (param1Int) {
                                              default:
                                                return Integer.toString(param1Int);
                                              case 162:
                                                return "ATTACH_STARTUP_AGENTS";
                                              case 161:
                                                return "PURGE_RESOURCES";
                                              case 160:
                                                return "RELAUNCH_ACTIVITY";
                                              case 159:
                                                return "EXECUTE_TRANSACTION";
                                              case 158:
                                                break;
                                            } 
                                            return "RUN_ISOLATED_ENTRY_POINT";
                                          case 156:
                                            return "APPLICATION_INFO_CHANGED";
                                          case 155:
                                            return "ATTACH_AGENT";
                                          case 154:
                                            break;
                                        } 
                                        return "LOCAL_VOICE_INTERACTION_STARTED";
                                      case 146:
                                        return "ON_NEW_ACTIVITY_OPTIONS";
                                      case 145:
                                        return "INSTALL_PROVIDER";
                                      case 144:
                                        return "TRANSLUCENT_CONVERSION_COMPLETE";
                                      case 143:
                                        return "REQUEST_ASSIST_CONTEXT_EXTRAS";
                                      case 142:
                                        return "UNSTABLE_PROVIDER_DIED";
                                      case 141:
                                        break;
                                    } 
                                    return "DUMP_PROVIDER";
                                  case 136:
                                    return "DUMP_ACTIVITY";
                                  case 135:
                                    return "DUMP_HEAP";
                                  case 134:
                                    return "SCHEDULE_CRASH";
                                  case 133:
                                    break;
                                } 
                                return "DISPATCH_PACKAGE_BROADCAST";
                              case 131:
                                return "REMOVE_PROVIDER";
                              case 130:
                                return "SUICIDE";
                              case 129:
                                return "DESTROY_BACKUP_AGENT";
                              case 128:
                                return "CREATE_BACKUP_AGENT";
                              case 127:
                                break;
                            } 
                            return "PROFILER_CONTROL";
                          case 124:
                            return "LOW_MEMORY";
                          case 123:
                            return "DUMP_SERVICE";
                          case 122:
                            return "UNBIND_SERVICE";
                          case 121:
                            return "BIND_SERVICE";
                          case 120:
                            return "GC_WHEN_IDLE";
                          case 119:
                            return "CLEAN_UP_CONTEXT";
                          case 118:
                            break;
                        } 
                        return "CONFIGURATION_CHANGED";
                      case 116:
                        return "STOP_SERVICE";
                      case 115:
                        return "SERVICE_ARGS";
                      case 114:
                        return "CREATE_SERVICE";
                      case 113:
                        break;
                    } 
                    return "RECEIVER";
                  } 
                  return "START_INPUT_WATCHING";
                } 
                return "ENTER_ANIMATION_COMPLETE";
              } 
              return "UPDATE_PACKAGE_COMPATIBILITY_INFO";
            } 
            return "SET_CORE_SETTINGS";
          } 
          return "EXIT_APPLICATION";
        } 
        return "BIND_APPLICATION";
      } 
    }
    
    public void handleMessage(Message param1Message) {
      if (ActivityThread.DEBUG_MESSAGES) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(">>> handling: ");
        stringBuilder.append(codeToString(param1Message.what));
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      int i = param1Message.what;
      if (i != 110) {
        if (i != 111) {
          if (i != 118) {
            if (i != 119) {
              if (i != 120) {
                if (i != 121) {
                  if (i != 122) {
                    if (i != 123) {
                      if (i != 124) {
                        if (i != 9999) {
                          if (i != 138) {
                            if (i != 139) {
                              ClientTransaction clientTransaction;
                              Application application;
                              Pair pair;
                              IBinder iBinder;
                              ActivityThread activityThread1;
                              boolean bool1;
                              boolean bool2;
                              String str;
                              ActivityThread activityThread2;
                              switch (i) {
                                default:
                                  bool1 = true;
                                  bool2 = true;
                                  switch (i) {
                                    default:
                                      switch (i) {
                                        default:
                                          switch (i) {
                                            default:
                                              switch (i) {
                                                default:
                                                  switch (i) {
                                                    default:
                                                      switch (i) {
                                                        default:
                                                          break;
                                                        case 162:
                                                          ActivityThread.handleAttachStartupAgents((String)param1Message.obj);
                                                          break;
                                                        case 161:
                                                          ActivityThread.this.schedulePurgeIdler();
                                                          break;
                                                        case 160:
                                                          ActivityThread.this.handleRelaunchActivityLocally((IBinder)param1Message.obj);
                                                          break;
                                                        case 159:
                                                          clientTransaction = (ClientTransaction)param1Message.obj;
                                                          ActivityThread.this.mTransactionExecutor.execute(clientTransaction);
                                                          if (ActivityThread.isSystem())
                                                            clientTransaction.recycle(); 
                                                          try {
                                                            OplusActivityTaskManager.getInstance().clientTransactionComplete(clientTransaction.getActivityToken(), clientTransaction.seq);
                                                          } catch (Exception exception) {}
                                                          break;
                                                        case 158:
                                                          break;
                                                      } 
                                                      ActivityThread.this.handleRunIsolatedEntryPoint((String)((SomeArgs)param1Message.obj).arg1, (String[])((SomeArgs)param1Message.obj).arg2);
                                                      break;
                                                    case 156:
                                                      ActivityThread.this.handleApplicationInfoChangedForSwitchUser((ApplicationInfo)param1Message.obj, param1Message.arg1, param1Message.arg2);
                                                      break;
                                                    case 155:
                                                      application = ActivityThread.this.getApplication();
                                                      str = (String)param1Message.obj;
                                                      if (application != null) {
                                                        LoadedApk loadedApk = application.mLoadedApk;
                                                      } else {
                                                        application = null;
                                                      } 
                                                      ActivityThread.handleAttachAgent(str, (LoadedApk)application);
                                                      break;
                                                    case 154:
                                                      break;
                                                  } 
                                                  ActivityThread.this.handleLocalVoiceInteractionStarted((IBinder)((SomeArgs)param1Message.obj).arg1, (IVoiceInteractor)((SomeArgs)param1Message.obj).arg2);
                                                  break;
                                                case 151:
                                                  ActivityThread.this.handleStopBinderTrackingAndDump((ParcelFileDescriptor)param1Message.obj);
                                                  break;
                                                case 150:
                                                  ActivityThread.this.handleStartBinderTracking();
                                                  break;
                                                case 149:
                                                  break;
                                              } 
                                              ActivityThread.this.handleEnterAnimationComplete((IBinder)param1Message.obj);
                                              break;
                                            case 146:
                                              pair = (Pair)param1Message.obj;
                                              ActivityThread.this.onNewActivityOptions((IBinder)pair.first, (ActivityOptions)pair.second);
                                              break;
                                            case 145:
                                              ActivityThread.this.handleInstallProvider((ProviderInfo)param1Message.obj);
                                              break;
                                            case 144:
                                              activityThread2 = ActivityThread.this;
                                              iBinder = (IBinder)param1Message.obj;
                                              if (param1Message.arg1 != 1)
                                                bool2 = false; 
                                              activityThread2.handleTranslucentConversionComplete(iBinder, bool2);
                                              break;
                                            case 143:
                                              ActivityThread.this.handleRequestAssistContextExtras((ActivityThread.RequestAssistContextExtras)param1Message.obj);
                                              break;
                                            case 142:
                                              ActivityThread.this.handleUnstableProviderDied((IBinder)param1Message.obj, false);
                                              break;
                                            case 141:
                                              break;
                                          } 
                                          ActivityThread.this.handleDumpProvider((ActivityThread.DumpComponentInfo)param1Message.obj);
                                          break;
                                        case 136:
                                          ActivityThread.this.handleDumpActivity((ActivityThread.DumpComponentInfo)param1Message.obj);
                                          break;
                                        case 135:
                                          ActivityThread.handleDumpHeap((ActivityThread.DumpHeapData)param1Message.obj);
                                          break;
                                        case 134:
                                          throw new RemoteServiceException((String)param1Message.obj);
                                        case 133:
                                          break;
                                      } 
                                      Trace.traceBegin(64L, "broadcastPackage");
                                      ActivityThread.this.handleDispatchPackageBroadcast(param1Message.arg1, (String[])param1Message.obj);
                                      Trace.traceEnd(64L);
                                      break;
                                    case 131:
                                      Trace.traceBegin(64L, "providerRemove");
                                      ActivityThread.this.completeRemoveProvider((ActivityThread.ProviderRefCount)param1Message.obj);
                                      Trace.traceEnd(64L);
                                      break;
                                    case 130:
                                      Process.killProcess(Process.myPid());
                                      break;
                                    case 129:
                                      Trace.traceBegin(64L, "backupDestroyAgent");
                                      ActivityThread.this.handleDestroyBackupAgent((ActivityThread.CreateBackupAgentData)param1Message.obj);
                                      Trace.traceEnd(64L);
                                      break;
                                    case 128:
                                      Trace.traceBegin(64L, "backupCreateAgent");
                                      ActivityThread.this.handleCreateBackupAgent((ActivityThread.CreateBackupAgentData)param1Message.obj);
                                      Trace.traceEnd(64L);
                                      break;
                                    case 127:
                                      break;
                                  } 
                                  activityThread1 = ActivityThread.this;
                                  if (param1Message.arg1 != 0) {
                                    bool2 = bool1;
                                  } else {
                                    bool2 = false;
                                  } 
                                  activityThread1.handleProfilerControl(bool2, (ProfilerInfo)param1Message.obj, param1Message.arg2);
                                  break;
                                case 116:
                                  Trace.traceBegin(64L, "serviceStop");
                                  ActivityThread.this.handleStopService((IBinder)param1Message.obj);
                                  ActivityThread.this.schedulePurgeIdler();
                                  Trace.traceEnd(64L);
                                  break;
                                case 115:
                                  if (Trace.isTagEnabled(64L)) {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append("serviceStart: ");
                                    Object object1 = param1Message.obj;
                                    stringBuilder.append(String.valueOf(object1));
                                    object1 = stringBuilder.toString();
                                    Trace.traceBegin(64L, (String)object1);
                                  } 
                                  ActivityThread.this.handleServiceArgs((ActivityThread.ServiceArgsData)param1Message.obj);
                                  Trace.traceEnd(64L);
                                  break;
                                case 114:
                                  if (Trace.isTagEnabled(64L)) {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append("serviceCreate: ");
                                    Object object1 = param1Message.obj;
                                    stringBuilder.append(String.valueOf(object1));
                                    String str1 = stringBuilder.toString();
                                    Trace.traceBegin(64L, str1);
                                  } 
                                  ActivityThread.this.handleCreateService((ActivityThread.CreateServiceData)param1Message.obj);
                                  Trace.traceEnd(64L);
                                  break;
                                case 113:
                                  Trace.traceBegin(64L, "broadcastReceiveComp");
                                  ActivityThread.this.handleReceiver((ActivityThread.ReceiverData)param1Message.obj);
                                  Trace.traceEnd(64L);
                                  break;
                              } 
                            } else {
                              ActivityThread.this.handleUpdatePackageCompatibilityInfo((ActivityThread.UpdateCompatibilityData)param1Message.obj);
                            } 
                          } else {
                            Trace.traceBegin(64L, "setCoreSettings");
                            ActivityThread.this.handleSetCoreSettings((Bundle)param1Message.obj);
                            Trace.traceEnd(64L);
                          } 
                        } else {
                          InputLog.startWatching();
                        } 
                      } else {
                        Trace.traceBegin(64L, "lowMemory");
                        ActivityThread.this.handleLowMemory();
                        Trace.traceEnd(64L);
                      } 
                    } else {
                      ActivityThread.this.handleDumpService((ActivityThread.DumpComponentInfo)param1Message.obj);
                    } 
                  } else {
                    Trace.traceBegin(64L, "serviceUnbind");
                    ActivityThread.this.handleUnbindService((ActivityThread.BindServiceData)param1Message.obj);
                    ActivityThread.this.schedulePurgeIdler();
                    Trace.traceEnd(64L);
                  } 
                } else {
                  Trace.traceBegin(64L, "serviceBind");
                  ActivityThread.this.handleBindService((ActivityThread.BindServiceData)param1Message.obj);
                  Trace.traceEnd(64L);
                } 
              } else {
                ActivityThread.this.scheduleGcIdler();
              } 
            } else {
              ActivityThread.ContextCleanupInfo contextCleanupInfo = (ActivityThread.ContextCleanupInfo)param1Message.obj;
              contextCleanupInfo.context.performFinalCleanup(contextCleanupInfo.who, contextCleanupInfo.what);
            } 
          } else {
            ActivityThread.this.handleConfigurationChanged((Configuration)param1Message.obj);
          } 
        } else {
          if (ActivityThread.this.mInitialApplication != null)
            ActivityThread.this.mInitialApplication.onTerminate(); 
          Looper.myLooper().quit();
        } 
      } else {
        Trace.traceBegin(64L, "bindApplication");
        ActivityThread.AppBindData appBindData = (ActivityThread.AppBindData)param1Message.obj;
        ActivityThread.this.handleBindApplication(appBindData);
        Trace.traceEnd(64L);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("BindApplication: ");
        stringBuilder.append(appBindData.processName);
        Log.p("Quality", stringBuilder.toString());
      } 
      Object object = param1Message.obj;
      if (object instanceof SomeArgs)
        ((SomeArgs)object).recycle(); 
      if (ActivityThread.DEBUG_MESSAGES) {
        object = new StringBuilder();
        object.append("<<< done: ");
        object.append(codeToString(param1Message.what));
        Slog.v("ActivityThread", object.toString());
      } 
    }
  }
  
  private class Idler implements MessageQueue.IdleHandler {
    final ActivityThread this$0;
    
    private Idler() {}
    
    public final boolean queueIdle() {
      ActivityThread.ActivityClientRecord activityClientRecord = ActivityThread.this.mNewActivities;
      boolean bool1 = false;
      boolean bool2 = bool1;
      if (ActivityThread.this.mBoundApplication != null) {
        bool2 = bool1;
        if (ActivityThread.this.mProfiler.profileFd != null) {
          bool2 = bool1;
          if (ActivityThread.this.mProfiler.autoStopProfiler)
            bool2 = true; 
        } 
      } 
      if (activityClientRecord != null) {
        ActivityThread.ActivityClientRecord activityClientRecord1;
        ActivityThread.this.mNewActivities = null;
        IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
        do {
          if (ActivityThread.localLOGV) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Reporting idle of ");
            stringBuilder.append(activityClientRecord);
            stringBuilder.append(" finished=");
            if (activityClientRecord.activity != null && activityClientRecord.activity.mFinished) {
              bool1 = true;
            } else {
              bool1 = false;
            } 
            stringBuilder.append(bool1);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          if (activityClientRecord.activity != null && !activityClientRecord.activity.mFinished)
            try {
              iActivityTaskManager.activityIdle(activityClientRecord.token, activityClientRecord.createdConfig, bool2);
              activityClientRecord.createdConfig = null;
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            }  
          activityClientRecord1 = ((ActivityThread.ActivityClientRecord)remoteException).nextIdle;
          ((ActivityThread.ActivityClientRecord)remoteException).nextIdle = null;
          ActivityThread.ActivityClientRecord activityClientRecord2 = activityClientRecord1;
        } while (activityClientRecord1 != null);
      } 
      if (bool2)
        ActivityThread.this.mProfiler.stopProfiling(); 
      ActivityThread.this.applyPendingProcessState();
      return false;
    }
  }
  
  final class GcIdler implements MessageQueue.IdleHandler {
    final ActivityThread this$0;
    
    public final boolean queueIdle() {
      ActivityThread.this.doGcIfNeeded();
      ActivityThread.this.purgePendingResources();
      return false;
    }
  }
  
  final class PurgeIdler implements MessageQueue.IdleHandler {
    final ActivityThread this$0;
    
    public boolean queueIdle() {
      ActivityThread.this.purgePendingResources();
      return false;
    }
  }
  
  public static ActivityThread currentActivityThread() {
    return sCurrentActivityThread;
  }
  
  public static boolean isSystem() {
    boolean bool;
    if (sCurrentActivityThread != null) {
      bool = sCurrentActivityThread.mSystemThread;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String currentOpPackageName() {
    ActivityThread activityThread = currentActivityThread();
    if (activityThread != null && activityThread.getApplication() != null) {
      String str = activityThread.getApplication().getOpPackageName();
    } else {
      activityThread = null;
    } 
    return (String)activityThread;
  }
  
  public static String currentPackageName() {
    null = currentActivityThread();
    if (null != null) {
      AppBindData appBindData = null.mBoundApplication;
      if (appBindData != null)
        return appBindData.appInfo.packageName; 
    } 
    return null;
  }
  
  public static String currentProcessName() {
    null = currentActivityThread();
    if (null != null) {
      AppBindData appBindData = null.mBoundApplication;
      if (appBindData != null)
        return appBindData.processName; 
    } 
    return null;
  }
  
  public static Application currentApplication() {
    ActivityThread activityThread = currentActivityThread();
    if (activityThread != null) {
      Application application = activityThread.mInitialApplication;
    } else {
      activityThread = null;
    } 
    return (Application)activityThread;
  }
  
  public static IPackageManager getPackageManager() {
    if (sPackageManager != null)
      return sPackageManager; 
    IBinder iBinder = ServiceManager.getService("package");
    sPackageManager = IPackageManager.Stub.asInterface(iBinder);
    return sPackageManager;
  }
  
  public static IPermissionManager getPermissionManager() {
    if (sPermissionManager != null)
      return sPermissionManager; 
    IBinder iBinder = ServiceManager.getService("permissionmgr");
    sPermissionManager = IPermissionManager.Stub.asInterface(iBinder);
    return sPermissionManager;
  }
  
  Configuration applyConfigCompatMainThread(int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo) {
    if (paramConfiguration == null)
      return null; 
    Configuration configuration = paramConfiguration;
    if (!paramCompatibilityInfo.supportsScreen()) {
      this.mMainThreadConfig.setTo(paramConfiguration);
      configuration = this.mMainThreadConfig;
      paramCompatibilityInfo.applyToConfiguration(paramInt, configuration);
    } 
    return configuration;
  }
  
  Resources getTopLevelResources(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, int paramInt, LoadedApk paramLoadedApk) {
    ResourcesManager resourcesManager = this.mResourcesManager;
    CompatibilityInfo compatibilityInfo = paramLoadedApk.getCompatibilityInfo();
    ClassLoader classLoader = paramLoadedApk.getClassLoader();
    return resourcesManager.getResources(null, paramString, paramArrayOfString1, paramArrayOfString2, paramArrayOfString3, paramInt, null, compatibilityInfo, classLoader, null);
  }
  
  final Handler getHandler() {
    return this.mH;
  }
  
  public final LoadedApk getPackageInfo(String paramString, CompatibilityInfo paramCompatibilityInfo, int paramInt) {
    return getPackageInfo(paramString, paramCompatibilityInfo, paramInt, UserHandle.myUserId());
  }
  
  public final LoadedApk getPackageInfo(String paramString, CompatibilityInfo paramCompatibilityInfo, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: invokestatic myUserId : ()I
    //   3: iload #4
    //   5: if_icmpeq -> 14
    //   8: iconst_1
    //   9: istore #5
    //   11: goto -> 17
    //   14: iconst_0
    //   15: istore #5
    //   17: iload #4
    //   19: ifge -> 30
    //   22: invokestatic myUserId : ()I
    //   25: istore #4
    //   27: goto -> 30
    //   30: aload_1
    //   31: ldc_w 268436480
    //   34: iload #4
    //   36: invokestatic getApplicationInfoAsUserCached : (Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    //   39: astore #6
    //   41: aload_0
    //   42: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   45: astore #7
    //   47: aload #7
    //   49: monitorenter
    //   50: iload #5
    //   52: ifeq -> 61
    //   55: aconst_null
    //   56: astore #8
    //   58: goto -> 96
    //   61: iload_3
    //   62: iconst_1
    //   63: iand
    //   64: ifeq -> 83
    //   67: aload_0
    //   68: getfield mPackages : Landroid/util/ArrayMap;
    //   71: aload_1
    //   72: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   75: checkcast java/lang/ref/WeakReference
    //   78: astore #8
    //   80: goto -> 96
    //   83: aload_0
    //   84: getfield mResourcePackages : Landroid/util/ArrayMap;
    //   87: aload_1
    //   88: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   91: checkcast java/lang/ref/WeakReference
    //   94: astore #8
    //   96: aload #8
    //   98: ifnull -> 114
    //   101: aload #8
    //   103: invokevirtual get : ()Ljava/lang/Object;
    //   106: checkcast android/app/LoadedApk
    //   109: astore #8
    //   111: goto -> 117
    //   114: aconst_null
    //   115: astore #8
    //   117: aload #6
    //   119: ifnull -> 265
    //   122: aload #8
    //   124: ifnull -> 265
    //   127: aload #8
    //   129: aload #6
    //   131: invokestatic isLoadedApkResourceDirsUpToDate : (Landroid/app/LoadedApk;Landroid/content/pm/ApplicationInfo;)Z
    //   134: ifne -> 160
    //   137: new java/util/ArrayList
    //   140: astore_2
    //   141: aload_2
    //   142: invokespecial <init> : ()V
    //   145: aload_0
    //   146: aload #6
    //   148: aload_2
    //   149: invokestatic makePaths : (Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   152: aload #8
    //   154: aload #6
    //   156: aload_2
    //   157: invokevirtual updateApplicationInfo : (Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   160: aload #8
    //   162: invokevirtual isSecurityViolation : ()Z
    //   165: ifeq -> 259
    //   168: iload_3
    //   169: iconst_2
    //   170: iand
    //   171: ifeq -> 177
    //   174: goto -> 259
    //   177: new java/lang/SecurityException
    //   180: astore #8
    //   182: new java/lang/StringBuilder
    //   185: astore_2
    //   186: aload_2
    //   187: invokespecial <init> : ()V
    //   190: aload_2
    //   191: ldc_w 'Requesting code from '
    //   194: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   197: pop
    //   198: aload_2
    //   199: aload_1
    //   200: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_2
    //   205: ldc_w ' to be run in process '
    //   208: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   211: pop
    //   212: aload_2
    //   213: aload_0
    //   214: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   217: getfield processName : Ljava/lang/String;
    //   220: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   223: pop
    //   224: aload_2
    //   225: ldc_w '/'
    //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: pop
    //   232: aload_2
    //   233: aload_0
    //   234: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   237: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   240: getfield uid : I
    //   243: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: aload #8
    //   249: aload_2
    //   250: invokevirtual toString : ()Ljava/lang/String;
    //   253: invokespecial <init> : (Ljava/lang/String;)V
    //   256: aload #8
    //   258: athrow
    //   259: aload #7
    //   261: monitorexit
    //   262: aload #8
    //   264: areturn
    //   265: aload #7
    //   267: monitorexit
    //   268: aload #6
    //   270: ifnull -> 282
    //   273: aload_0
    //   274: aload #6
    //   276: aload_2
    //   277: iload_3
    //   278: invokevirtual getPackageInfo : (Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;I)Landroid/app/LoadedApk;
    //   281: areturn
    //   282: aconst_null
    //   283: areturn
    //   284: astore_1
    //   285: aload #7
    //   287: monitorexit
    //   288: aload_1
    //   289: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2489	-> 0
    //   #2490	-> 17
    //   #2494	-> 17
    //   #2490	-> 30
    //   #2495	-> 41
    //   #2497	-> 50
    //   #2499	-> 55
    //   #2500	-> 61
    //   #2501	-> 67
    //   #2503	-> 83
    //   #2506	-> 96
    //   #2507	-> 117
    //   #2508	-> 127
    //   #2509	-> 137
    //   #2510	-> 145
    //   #2511	-> 152
    //   #2514	-> 160
    //   #2516	-> 177
    //   #2522	-> 259
    //   #2524	-> 265
    //   #2526	-> 268
    //   #2527	-> 273
    //   #2530	-> 282
    //   #2524	-> 284
    // Exception table:
    //   from	to	target	type
    //   67	80	284	finally
    //   83	96	284	finally
    //   101	111	284	finally
    //   127	137	284	finally
    //   137	145	284	finally
    //   145	152	284	finally
    //   152	160	284	finally
    //   160	168	284	finally
    //   177	259	284	finally
    //   259	262	284	finally
    //   265	268	284	finally
    //   285	288	284	finally
  }
  
  public final LoadedApk getPackageInfo(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, int paramInt) {
    // Byte code:
    //   0: iload_3
    //   1: iconst_1
    //   2: iand
    //   3: ifeq -> 12
    //   6: iconst_1
    //   7: istore #4
    //   9: goto -> 15
    //   12: iconst_0
    //   13: istore #4
    //   15: iload #4
    //   17: ifeq -> 78
    //   20: aload_1
    //   21: getfield uid : I
    //   24: ifeq -> 78
    //   27: aload_1
    //   28: getfield uid : I
    //   31: sipush #1000
    //   34: if_icmpeq -> 78
    //   37: aload_0
    //   38: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   41: ifnull -> 72
    //   44: aload_1
    //   45: getfield uid : I
    //   48: istore #5
    //   50: aload_0
    //   51: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   54: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   57: getfield uid : I
    //   60: istore #6
    //   62: iload #5
    //   64: iload #6
    //   66: invokestatic isSameApp : (II)Z
    //   69: ifne -> 78
    //   72: iconst_1
    //   73: istore #7
    //   75: goto -> 81
    //   78: iconst_0
    //   79: istore #7
    //   81: iload #4
    //   83: ifeq -> 100
    //   86: ldc_w 1073741824
    //   89: iload_3
    //   90: iand
    //   91: ifeq -> 100
    //   94: iconst_1
    //   95: istore #8
    //   97: goto -> 103
    //   100: iconst_0
    //   101: istore #8
    //   103: iload_3
    //   104: iconst_3
    //   105: iand
    //   106: iconst_1
    //   107: if_icmpne -> 258
    //   110: iload #7
    //   112: ifeq -> 258
    //   115: new java/lang/StringBuilder
    //   118: dup
    //   119: invokespecial <init> : ()V
    //   122: astore_2
    //   123: aload_2
    //   124: ldc_w 'Requesting code from '
    //   127: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   130: pop
    //   131: aload_2
    //   132: aload_1
    //   133: getfield packageName : Ljava/lang/String;
    //   136: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload_2
    //   141: ldc_w ' (with uid '
    //   144: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   147: pop
    //   148: aload_2
    //   149: aload_1
    //   150: getfield uid : I
    //   153: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload_2
    //   158: ldc_w ')'
    //   161: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   164: pop
    //   165: aload_2
    //   166: invokevirtual toString : ()Ljava/lang/String;
    //   169: astore_2
    //   170: aload_2
    //   171: astore_1
    //   172: aload_0
    //   173: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   176: ifnull -> 249
    //   179: new java/lang/StringBuilder
    //   182: dup
    //   183: invokespecial <init> : ()V
    //   186: astore_1
    //   187: aload_1
    //   188: aload_2
    //   189: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: aload_1
    //   194: ldc_w ' to be run in process '
    //   197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   200: pop
    //   201: aload_1
    //   202: aload_0
    //   203: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   206: getfield processName : Ljava/lang/String;
    //   209: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   212: pop
    //   213: aload_1
    //   214: ldc_w ' (with uid '
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload_1
    //   222: aload_0
    //   223: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   226: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   229: getfield uid : I
    //   232: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   235: pop
    //   236: aload_1
    //   237: ldc_w ')'
    //   240: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   243: pop
    //   244: aload_1
    //   245: invokevirtual toString : ()Ljava/lang/String;
    //   248: astore_1
    //   249: new java/lang/SecurityException
    //   252: dup
    //   253: aload_1
    //   254: invokespecial <init> : (Ljava/lang/String;)V
    //   257: athrow
    //   258: aload_0
    //   259: aload_1
    //   260: aload_2
    //   261: aconst_null
    //   262: iload #7
    //   264: iload #4
    //   266: iload #8
    //   268: invokespecial getPackageInfo : (Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZZ)Landroid/app/LoadedApk;
    //   271: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2536	-> 0
    //   #2537	-> 15
    //   #2539	-> 62
    //   #2541	-> 81
    //   #2542	-> 103
    //   #2545	-> 110
    //   #2546	-> 115
    //   #2548	-> 170
    //   #2549	-> 179
    //   #2553	-> 249
    //   #2556	-> 258
  }
  
  public final LoadedApk getPackageInfoNoCheck(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo) {
    return getPackageInfo(paramApplicationInfo, paramCompatibilityInfo, (ClassLoader)null, false, true, false);
  }
  
  public final LoadedApk peekPackageInfo(String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: iload_2
    //   8: ifeq -> 26
    //   11: aload_0
    //   12: getfield mPackages : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   19: checkcast java/lang/ref/WeakReference
    //   22: astore_1
    //   23: goto -> 38
    //   26: aload_0
    //   27: getfield mResourcePackages : Landroid/util/ArrayMap;
    //   30: aload_1
    //   31: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   34: checkcast java/lang/ref/WeakReference
    //   37: astore_1
    //   38: aload_1
    //   39: ifnull -> 53
    //   42: aload_1
    //   43: invokevirtual get : ()Ljava/lang/Object;
    //   46: checkcast android/app/LoadedApk
    //   49: astore_1
    //   50: goto -> 55
    //   53: aconst_null
    //   54: astore_1
    //   55: aload_3
    //   56: monitorexit
    //   57: aload_1
    //   58: areturn
    //   59: astore_1
    //   60: aload_3
    //   61: monitorexit
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2569	-> 0
    //   #2571	-> 7
    //   #2572	-> 11
    //   #2574	-> 26
    //   #2576	-> 38
    //   #2577	-> 59
    // Exception table:
    //   from	to	target	type
    //   11	23	59	finally
    //   26	38	59	finally
    //   42	50	59	finally
    //   55	57	59	finally
    //   60	62	59	finally
  }
  
  private LoadedApk getPackageInfo(ApplicationInfo paramApplicationInfo, CompatibilityInfo paramCompatibilityInfo, ClassLoader paramClassLoader, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    // Byte code:
    //   0: invokestatic myUserId : ()I
    //   3: aload_1
    //   4: getfield uid : I
    //   7: invokestatic getUserId : (I)I
    //   10: if_icmpeq -> 19
    //   13: iconst_1
    //   14: istore #7
    //   16: goto -> 22
    //   19: iconst_0
    //   20: istore #7
    //   22: aload_0
    //   23: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   26: astore #8
    //   28: aload #8
    //   30: monitorenter
    //   31: iload #7
    //   33: ifeq -> 42
    //   36: aconst_null
    //   37: astore #9
    //   39: goto -> 82
    //   42: iload #5
    //   44: ifeq -> 66
    //   47: aload_0
    //   48: getfield mPackages : Landroid/util/ArrayMap;
    //   51: aload_1
    //   52: getfield packageName : Ljava/lang/String;
    //   55: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   58: checkcast java/lang/ref/WeakReference
    //   61: astore #9
    //   63: goto -> 82
    //   66: aload_0
    //   67: getfield mResourcePackages : Landroid/util/ArrayMap;
    //   70: aload_1
    //   71: getfield packageName : Ljava/lang/String;
    //   74: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   77: checkcast java/lang/ref/WeakReference
    //   80: astore #9
    //   82: aconst_null
    //   83: astore #10
    //   85: aload #9
    //   87: ifnull -> 103
    //   90: aload #9
    //   92: invokevirtual get : ()Ljava/lang/Object;
    //   95: checkcast android/app/LoadedApk
    //   98: astore #9
    //   100: goto -> 106
    //   103: aconst_null
    //   104: astore #9
    //   106: aload #9
    //   108: ifnull -> 147
    //   111: aload #9
    //   113: aload_1
    //   114: invokestatic isLoadedApkResourceDirsUpToDate : (Landroid/app/LoadedApk;Landroid/content/pm/ApplicationInfo;)Z
    //   117: ifne -> 141
    //   120: new java/util/ArrayList
    //   123: astore_2
    //   124: aload_2
    //   125: invokespecial <init> : ()V
    //   128: aload_0
    //   129: aload_1
    //   130: aload_2
    //   131: invokestatic makePaths : (Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   134: aload #9
    //   136: aload_1
    //   137: aload_2
    //   138: invokevirtual updateApplicationInfo : (Landroid/content/pm/ApplicationInfo;Ljava/util/List;)V
    //   141: aload #8
    //   143: monitorexit
    //   144: aload #9
    //   146: areturn
    //   147: getstatic android/app/ActivityThread.localLOGV : Z
    //   150: ifeq -> 263
    //   153: new java/lang/StringBuilder
    //   156: astore #11
    //   158: aload #11
    //   160: invokespecial <init> : ()V
    //   163: iload #5
    //   165: ifeq -> 176
    //   168: ldc_w 'Loading code package '
    //   171: astore #9
    //   173: goto -> 181
    //   176: ldc_w 'Loading resource-only package '
    //   179: astore #9
    //   181: aload #11
    //   183: aload #9
    //   185: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   188: pop
    //   189: aload #11
    //   191: aload_1
    //   192: getfield packageName : Ljava/lang/String;
    //   195: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   198: pop
    //   199: aload #11
    //   201: ldc_w ' (in '
    //   204: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   207: pop
    //   208: aload_0
    //   209: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   212: ifnull -> 227
    //   215: aload_0
    //   216: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   219: getfield processName : Ljava/lang/String;
    //   222: astore #9
    //   224: goto -> 231
    //   227: aload #10
    //   229: astore #9
    //   231: aload #11
    //   233: aload #9
    //   235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: aload #11
    //   241: ldc_w ')'
    //   244: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   247: pop
    //   248: aload #11
    //   250: invokevirtual toString : ()Ljava/lang/String;
    //   253: astore #9
    //   255: ldc 'ActivityThread'
    //   257: aload #9
    //   259: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   262: pop
    //   263: new android/app/LoadedApk
    //   266: astore #9
    //   268: iload #5
    //   270: ifeq -> 288
    //   273: aload_1
    //   274: getfield flags : I
    //   277: iconst_4
    //   278: iand
    //   279: ifeq -> 288
    //   282: iconst_1
    //   283: istore #12
    //   285: goto -> 291
    //   288: iconst_0
    //   289: istore #12
    //   291: aload #9
    //   293: aload_0
    //   294: aload_1
    //   295: aload_2
    //   296: aload_3
    //   297: iload #4
    //   299: iload #12
    //   301: iload #6
    //   303: invokespecial <init> : (Landroid/app/ActivityThread;Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZZ)V
    //   306: aload_0
    //   307: getfield mSystemThread : Z
    //   310: ifeq -> 344
    //   313: ldc_w 'android'
    //   316: aload_1
    //   317: getfield packageName : Ljava/lang/String;
    //   320: invokevirtual equals : (Ljava/lang/Object;)Z
    //   323: ifeq -> 344
    //   326: aload_0
    //   327: invokevirtual getSystemContext : ()Landroid/app/ContextImpl;
    //   330: getfield mPackageInfo : Landroid/app/LoadedApk;
    //   333: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   336: astore_2
    //   337: aload #9
    //   339: aload_1
    //   340: aload_2
    //   341: invokevirtual installSystemApplicationInfo : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)V
    //   344: iload #7
    //   346: ifeq -> 352
    //   349: goto -> 414
    //   352: iload #5
    //   354: ifeq -> 387
    //   357: aload_0
    //   358: getfield mPackages : Landroid/util/ArrayMap;
    //   361: astore_2
    //   362: aload_1
    //   363: getfield packageName : Ljava/lang/String;
    //   366: astore_3
    //   367: new java/lang/ref/WeakReference
    //   370: astore_1
    //   371: aload_1
    //   372: aload #9
    //   374: invokespecial <init> : (Ljava/lang/Object;)V
    //   377: aload_2
    //   378: aload_3
    //   379: aload_1
    //   380: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   383: pop
    //   384: goto -> 414
    //   387: aload_0
    //   388: getfield mResourcePackages : Landroid/util/ArrayMap;
    //   391: astore_2
    //   392: aload_1
    //   393: getfield packageName : Ljava/lang/String;
    //   396: astore_3
    //   397: new java/lang/ref/WeakReference
    //   400: astore_1
    //   401: aload_1
    //   402: aload #9
    //   404: invokespecial <init> : (Ljava/lang/Object;)V
    //   407: aload_2
    //   408: aload_3
    //   409: aload_1
    //   410: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   413: pop
    //   414: aload #8
    //   416: monitorexit
    //   417: aload #9
    //   419: areturn
    //   420: astore_1
    //   421: aload #8
    //   423: monitorexit
    //   424: aload_1
    //   425: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2583	-> 0
    //   #2584	-> 22
    //   #2586	-> 31
    //   #2588	-> 36
    //   #2589	-> 42
    //   #2590	-> 47
    //   #2592	-> 66
    //   #2595	-> 82
    //   #2597	-> 106
    //   #2598	-> 111
    //   #2599	-> 120
    //   #2600	-> 128
    //   #2601	-> 134
    //   #2604	-> 141
    //   #2607	-> 147
    //   #2608	-> 153
    //   #2609	-> 176
    //   #2610	-> 208
    //   #2611	-> 215
    //   #2608	-> 255
    //   #2615	-> 263
    //   #2620	-> 306
    //   #2621	-> 326
    //   #2622	-> 326
    //   #2621	-> 337
    //   #2625	-> 344
    //   #2627	-> 352
    //   #2628	-> 357
    //   #2631	-> 387
    //   #2635	-> 414
    //   #2636	-> 420
    // Exception table:
    //   from	to	target	type
    //   47	63	420	finally
    //   66	82	420	finally
    //   90	100	420	finally
    //   111	120	420	finally
    //   120	128	420	finally
    //   128	134	420	finally
    //   134	141	420	finally
    //   141	144	420	finally
    //   147	153	420	finally
    //   153	163	420	finally
    //   181	208	420	finally
    //   208	215	420	finally
    //   215	224	420	finally
    //   231	255	420	finally
    //   255	263	420	finally
    //   263	268	420	finally
    //   273	282	420	finally
    //   291	306	420	finally
    //   306	326	420	finally
    //   326	337	420	finally
    //   337	344	420	finally
    //   357	384	420	finally
    //   387	414	420	finally
    //   414	417	420	finally
    //   421	424	420	finally
  }
  
  private static boolean isLoadedApkResourceDirsUpToDate(LoadedApk paramLoadedApk, ApplicationInfo paramApplicationInfo) {
    Resources resources = paramLoadedApk.mResources;
    String[] arrayOfString1 = ArrayUtils.defeatNullable(paramLoadedApk.getOverlayDirs());
    String[] arrayOfString2 = ArrayUtils.defeatNullable(paramApplicationInfo.resourceDirs);
    if ((resources == null || resources.getAssets().isUpToDate()) && arrayOfString1.length == arrayOfString2.length)
      if (ArrayUtils.containsAll((Object[])arrayOfString1, (Object[])arrayOfString2))
        return true;  
    return false;
  }
  
  public ApplicationThread getApplicationThread() {
    return this.mAppThread;
  }
  
  public Instrumentation getInstrumentation() {
    return this.mInstrumentation;
  }
  
  public boolean isProfiling() {
    boolean bool;
    Profiler profiler = this.mProfiler;
    if (profiler != null && profiler.profileFile != null && this.mProfiler.profileFd == null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getProfileFilePath() {
    return this.mProfiler.profileFile;
  }
  
  public Looper getLooper() {
    return this.mLooper;
  }
  
  public Executor getExecutor() {
    return this.mExecutor;
  }
  
  public Application getApplication() {
    return this.mInitialApplication;
  }
  
  public String getProcessName() {
    return this.mBoundApplication.processName;
  }
  
  public ContextImpl getSystemContext() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSystemContext : Landroid/app/ContextImpl;
    //   6: ifnonnull -> 17
    //   9: aload_0
    //   10: aload_0
    //   11: invokestatic createSystemContext : (Landroid/app/ActivityThread;)Landroid/app/ContextImpl;
    //   14: putfield mSystemContext : Landroid/app/ContextImpl;
    //   17: aload_0
    //   18: getfield mSystemContext : Landroid/app/ContextImpl;
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: areturn
    //   26: astore_1
    //   27: aload_0
    //   28: monitorexit
    //   29: aload_1
    //   30: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2697	-> 0
    //   #2698	-> 2
    //   #2699	-> 9
    //   #2701	-> 17
    //   #2702	-> 26
    // Exception table:
    //   from	to	target	type
    //   2	9	26	finally
    //   9	17	26	finally
    //   17	24	26	finally
    //   27	29	26	finally
  }
  
  public ContextImpl getSystemUiContext() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mSystemUiContext : Landroid/app/ContextImpl;
    //   6: ifnonnull -> 20
    //   9: aload_0
    //   10: aload_0
    //   11: invokevirtual getSystemContext : ()Landroid/app/ContextImpl;
    //   14: invokestatic createSystemUiContext : (Landroid/app/ContextImpl;)Landroid/app/ContextImpl;
    //   17: putfield mSystemUiContext : Landroid/app/ContextImpl;
    //   20: aload_0
    //   21: getfield mSystemUiContext : Landroid/app/ContextImpl;
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: areturn
    //   29: astore_1
    //   30: aload_0
    //   31: monitorexit
    //   32: aload_1
    //   33: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2706	-> 0
    //   #2707	-> 2
    //   #2708	-> 9
    //   #2710	-> 20
    //   #2711	-> 29
    // Exception table:
    //   from	to	target	type
    //   2	9	29	finally
    //   9	20	29	finally
    //   20	27	29	finally
    //   30	32	29	finally
  }
  
  public ContextImpl createSystemUiContext(int paramInt) {
    return ContextImpl.createSystemUiContext(getSystemUiContext(), paramInt);
  }
  
  public void installSystemApplicationInfo(ApplicationInfo paramApplicationInfo, ClassLoader paramClassLoader) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual getSystemContext : ()Landroid/app/ContextImpl;
    //   6: aload_1
    //   7: aload_2
    //   8: invokevirtual installSystemApplicationInfo : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)V
    //   11: aload_0
    //   12: invokevirtual getSystemUiContext : ()Landroid/app/ContextImpl;
    //   15: aload_1
    //   16: aload_2
    //   17: invokevirtual installSystemApplicationInfo : (Landroid/content/pm/ApplicationInfo;Ljava/lang/ClassLoader;)V
    //   20: new android/app/ActivityThread$Profiler
    //   23: astore_1
    //   24: aload_1
    //   25: invokespecial <init> : ()V
    //   28: aload_0
    //   29: aload_1
    //   30: putfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2724	-> 0
    //   #2725	-> 2
    //   #2726	-> 11
    //   #2729	-> 20
    //   #2730	-> 33
    //   #2731	-> 35
    //   #2730	-> 36
    // Exception table:
    //   from	to	target	type
    //   2	11	36	finally
    //   11	20	36	finally
    //   20	33	36	finally
    //   33	35	36	finally
    //   37	39	36	finally
  }
  
  void scheduleGcIdler() {
    if (!this.mGcIdlerScheduled) {
      this.mGcIdlerScheduled = true;
      Looper.myQueue().addIdleHandler(this.mGcIdler);
    } 
    this.mH.removeMessages(120);
  }
  
  void unscheduleGcIdler() {
    if (this.mGcIdlerScheduled) {
      this.mGcIdlerScheduled = false;
      Looper.myQueue().removeIdleHandler(this.mGcIdler);
    } 
    this.mH.removeMessages(120);
  }
  
  void schedulePurgeIdler() {
    if (!this.mPurgeIdlerScheduled) {
      this.mPurgeIdlerScheduled = true;
      Looper.myQueue().addIdleHandler(this.mPurgeIdler);
    } 
    this.mH.removeMessages(161);
  }
  
  void unschedulePurgeIdler() {
    if (this.mPurgeIdlerScheduled) {
      this.mPurgeIdlerScheduled = false;
      Looper.myQueue().removeIdleHandler(this.mPurgeIdler);
    } 
    this.mH.removeMessages(161);
  }
  
  void doGcIfNeeded() {
    doGcIfNeeded("bg");
  }
  
  void doGcIfNeeded(String paramString) {
    this.mGcIdlerScheduled = false;
    long l = SystemClock.uptimeMillis();
    if (BinderInternal.getLastGcTime() + 5000L < l)
      BinderInternal.forceGc(paramString); 
  }
  
  static void printRow(PrintWriter paramPrintWriter, String paramString, Object... paramVarArgs) {
    paramPrintWriter.println(String.format(paramString, paramVarArgs));
  }
  
  public static void dumpMemInfoTable(PrintWriter paramPrintWriter, Debug.MemoryInfo paramMemoryInfo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, int paramInt, String paramString, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6) {
    // Byte code:
    //   0: iload_2
    //   1: ifeq -> 840
    //   4: aload_0
    //   5: iconst_4
    //   6: invokevirtual print : (I)V
    //   9: aload_0
    //   10: bipush #44
    //   12: invokevirtual print : (C)V
    //   15: aload_0
    //   16: iload #6
    //   18: invokevirtual print : (I)V
    //   21: aload_0
    //   22: bipush #44
    //   24: invokevirtual print : (C)V
    //   27: aload_0
    //   28: aload #7
    //   30: invokevirtual print : (Ljava/lang/String;)V
    //   33: aload_0
    //   34: bipush #44
    //   36: invokevirtual print : (C)V
    //   39: aload_0
    //   40: lload #8
    //   42: invokevirtual print : (J)V
    //   45: aload_0
    //   46: bipush #44
    //   48: invokevirtual print : (C)V
    //   51: aload_0
    //   52: lload #14
    //   54: invokevirtual print : (J)V
    //   57: aload_0
    //   58: bipush #44
    //   60: invokevirtual print : (C)V
    //   63: aload_0
    //   64: ldc_w 'N/A,'
    //   67: invokevirtual print : (Ljava/lang/String;)V
    //   70: aload_0
    //   71: lload #8
    //   73: lload #14
    //   75: ladd
    //   76: invokevirtual print : (J)V
    //   79: aload_0
    //   80: bipush #44
    //   82: invokevirtual print : (C)V
    //   85: aload_0
    //   86: lload #10
    //   88: invokevirtual print : (J)V
    //   91: aload_0
    //   92: bipush #44
    //   94: invokevirtual print : (C)V
    //   97: aload_0
    //   98: lload #16
    //   100: invokevirtual print : (J)V
    //   103: aload_0
    //   104: bipush #44
    //   106: invokevirtual print : (C)V
    //   109: aload_0
    //   110: ldc_w 'N/A,'
    //   113: invokevirtual print : (Ljava/lang/String;)V
    //   116: aload_0
    //   117: lload #10
    //   119: lload #16
    //   121: ladd
    //   122: invokevirtual print : (J)V
    //   125: aload_0
    //   126: bipush #44
    //   128: invokevirtual print : (C)V
    //   131: aload_0
    //   132: lload #12
    //   134: invokevirtual print : (J)V
    //   137: aload_0
    //   138: bipush #44
    //   140: invokevirtual print : (C)V
    //   143: aload_0
    //   144: lload #18
    //   146: invokevirtual print : (J)V
    //   149: aload_0
    //   150: bipush #44
    //   152: invokevirtual print : (C)V
    //   155: aload_0
    //   156: ldc_w 'N/A,'
    //   159: invokevirtual print : (Ljava/lang/String;)V
    //   162: aload_0
    //   163: lload #12
    //   165: lload #18
    //   167: ladd
    //   168: invokevirtual print : (J)V
    //   171: aload_0
    //   172: bipush #44
    //   174: invokevirtual print : (C)V
    //   177: aload_0
    //   178: aload_1
    //   179: getfield nativePss : I
    //   182: invokevirtual print : (I)V
    //   185: aload_0
    //   186: bipush #44
    //   188: invokevirtual print : (C)V
    //   191: aload_0
    //   192: aload_1
    //   193: getfield dalvikPss : I
    //   196: invokevirtual print : (I)V
    //   199: aload_0
    //   200: bipush #44
    //   202: invokevirtual print : (C)V
    //   205: aload_0
    //   206: aload_1
    //   207: getfield otherPss : I
    //   210: invokevirtual print : (I)V
    //   213: aload_0
    //   214: bipush #44
    //   216: invokevirtual print : (C)V
    //   219: aload_0
    //   220: aload_1
    //   221: invokevirtual getTotalPss : ()I
    //   224: invokevirtual print : (I)V
    //   227: aload_0
    //   228: bipush #44
    //   230: invokevirtual print : (C)V
    //   233: aload_0
    //   234: aload_1
    //   235: getfield nativeSwappablePss : I
    //   238: invokevirtual print : (I)V
    //   241: aload_0
    //   242: bipush #44
    //   244: invokevirtual print : (C)V
    //   247: aload_0
    //   248: aload_1
    //   249: getfield dalvikSwappablePss : I
    //   252: invokevirtual print : (I)V
    //   255: aload_0
    //   256: bipush #44
    //   258: invokevirtual print : (C)V
    //   261: aload_0
    //   262: aload_1
    //   263: getfield otherSwappablePss : I
    //   266: invokevirtual print : (I)V
    //   269: aload_0
    //   270: bipush #44
    //   272: invokevirtual print : (C)V
    //   275: aload_0
    //   276: aload_1
    //   277: invokevirtual getTotalSwappablePss : ()I
    //   280: invokevirtual print : (I)V
    //   283: aload_0
    //   284: bipush #44
    //   286: invokevirtual print : (C)V
    //   289: aload_0
    //   290: aload_1
    //   291: getfield nativeSharedDirty : I
    //   294: invokevirtual print : (I)V
    //   297: aload_0
    //   298: bipush #44
    //   300: invokevirtual print : (C)V
    //   303: aload_0
    //   304: aload_1
    //   305: getfield dalvikSharedDirty : I
    //   308: invokevirtual print : (I)V
    //   311: aload_0
    //   312: bipush #44
    //   314: invokevirtual print : (C)V
    //   317: aload_0
    //   318: aload_1
    //   319: getfield otherSharedDirty : I
    //   322: invokevirtual print : (I)V
    //   325: aload_0
    //   326: bipush #44
    //   328: invokevirtual print : (C)V
    //   331: aload_0
    //   332: aload_1
    //   333: invokevirtual getTotalSharedDirty : ()I
    //   336: invokevirtual print : (I)V
    //   339: aload_0
    //   340: bipush #44
    //   342: invokevirtual print : (C)V
    //   345: aload_0
    //   346: aload_1
    //   347: getfield nativeSharedClean : I
    //   350: invokevirtual print : (I)V
    //   353: aload_0
    //   354: bipush #44
    //   356: invokevirtual print : (C)V
    //   359: aload_0
    //   360: aload_1
    //   361: getfield dalvikSharedClean : I
    //   364: invokevirtual print : (I)V
    //   367: aload_0
    //   368: bipush #44
    //   370: invokevirtual print : (C)V
    //   373: aload_0
    //   374: aload_1
    //   375: getfield otherSharedClean : I
    //   378: invokevirtual print : (I)V
    //   381: aload_0
    //   382: bipush #44
    //   384: invokevirtual print : (C)V
    //   387: aload_0
    //   388: aload_1
    //   389: invokevirtual getTotalSharedClean : ()I
    //   392: invokevirtual print : (I)V
    //   395: aload_0
    //   396: bipush #44
    //   398: invokevirtual print : (C)V
    //   401: aload_0
    //   402: aload_1
    //   403: getfield nativePrivateDirty : I
    //   406: invokevirtual print : (I)V
    //   409: aload_0
    //   410: bipush #44
    //   412: invokevirtual print : (C)V
    //   415: aload_0
    //   416: aload_1
    //   417: getfield dalvikPrivateDirty : I
    //   420: invokevirtual print : (I)V
    //   423: aload_0
    //   424: bipush #44
    //   426: invokevirtual print : (C)V
    //   429: aload_0
    //   430: aload_1
    //   431: getfield otherPrivateDirty : I
    //   434: invokevirtual print : (I)V
    //   437: aload_0
    //   438: bipush #44
    //   440: invokevirtual print : (C)V
    //   443: aload_0
    //   444: aload_1
    //   445: invokevirtual getTotalPrivateDirty : ()I
    //   448: invokevirtual print : (I)V
    //   451: aload_0
    //   452: bipush #44
    //   454: invokevirtual print : (C)V
    //   457: aload_0
    //   458: aload_1
    //   459: getfield nativePrivateClean : I
    //   462: invokevirtual print : (I)V
    //   465: aload_0
    //   466: bipush #44
    //   468: invokevirtual print : (C)V
    //   471: aload_0
    //   472: aload_1
    //   473: getfield dalvikPrivateClean : I
    //   476: invokevirtual print : (I)V
    //   479: aload_0
    //   480: bipush #44
    //   482: invokevirtual print : (C)V
    //   485: aload_0
    //   486: aload_1
    //   487: getfield otherPrivateClean : I
    //   490: invokevirtual print : (I)V
    //   493: aload_0
    //   494: bipush #44
    //   496: invokevirtual print : (C)V
    //   499: aload_0
    //   500: aload_1
    //   501: invokevirtual getTotalPrivateClean : ()I
    //   504: invokevirtual print : (I)V
    //   507: aload_0
    //   508: bipush #44
    //   510: invokevirtual print : (C)V
    //   513: aload_0
    //   514: aload_1
    //   515: getfield nativeSwappedOut : I
    //   518: invokevirtual print : (I)V
    //   521: aload_0
    //   522: bipush #44
    //   524: invokevirtual print : (C)V
    //   527: aload_0
    //   528: aload_1
    //   529: getfield dalvikSwappedOut : I
    //   532: invokevirtual print : (I)V
    //   535: aload_0
    //   536: bipush #44
    //   538: invokevirtual print : (C)V
    //   541: aload_0
    //   542: aload_1
    //   543: getfield otherSwappedOut : I
    //   546: invokevirtual print : (I)V
    //   549: aload_0
    //   550: bipush #44
    //   552: invokevirtual print : (C)V
    //   555: aload_0
    //   556: aload_1
    //   557: invokevirtual getTotalSwappedOut : ()I
    //   560: invokevirtual print : (I)V
    //   563: aload_0
    //   564: bipush #44
    //   566: invokevirtual print : (C)V
    //   569: aload_1
    //   570: getfield hasSwappedOutPss : Z
    //   573: ifeq -> 635
    //   576: aload_0
    //   577: aload_1
    //   578: getfield nativeSwappedOutPss : I
    //   581: invokevirtual print : (I)V
    //   584: aload_0
    //   585: bipush #44
    //   587: invokevirtual print : (C)V
    //   590: aload_0
    //   591: aload_1
    //   592: getfield dalvikSwappedOutPss : I
    //   595: invokevirtual print : (I)V
    //   598: aload_0
    //   599: bipush #44
    //   601: invokevirtual print : (C)V
    //   604: aload_0
    //   605: aload_1
    //   606: getfield otherSwappedOutPss : I
    //   609: invokevirtual print : (I)V
    //   612: aload_0
    //   613: bipush #44
    //   615: invokevirtual print : (C)V
    //   618: aload_0
    //   619: aload_1
    //   620: invokevirtual getTotalSwappedOutPss : ()I
    //   623: invokevirtual print : (I)V
    //   626: aload_0
    //   627: bipush #44
    //   629: invokevirtual print : (C)V
    //   632: goto -> 663
    //   635: aload_0
    //   636: ldc_w 'N/A,'
    //   639: invokevirtual print : (Ljava/lang/String;)V
    //   642: aload_0
    //   643: ldc_w 'N/A,'
    //   646: invokevirtual print : (Ljava/lang/String;)V
    //   649: aload_0
    //   650: ldc_w 'N/A,'
    //   653: invokevirtual print : (Ljava/lang/String;)V
    //   656: aload_0
    //   657: ldc_w 'N/A,'
    //   660: invokevirtual print : (Ljava/lang/String;)V
    //   663: iconst_0
    //   664: istore #6
    //   666: iload #6
    //   668: bipush #17
    //   670: if_icmpge -> 839
    //   673: aload_0
    //   674: iload #6
    //   676: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   679: invokevirtual print : (Ljava/lang/String;)V
    //   682: aload_0
    //   683: bipush #44
    //   685: invokevirtual print : (C)V
    //   688: aload_0
    //   689: aload_1
    //   690: iload #6
    //   692: invokevirtual getOtherPss : (I)I
    //   695: invokevirtual print : (I)V
    //   698: aload_0
    //   699: bipush #44
    //   701: invokevirtual print : (C)V
    //   704: aload_0
    //   705: aload_1
    //   706: iload #6
    //   708: invokevirtual getOtherSwappablePss : (I)I
    //   711: invokevirtual print : (I)V
    //   714: aload_0
    //   715: bipush #44
    //   717: invokevirtual print : (C)V
    //   720: aload_0
    //   721: aload_1
    //   722: iload #6
    //   724: invokevirtual getOtherSharedDirty : (I)I
    //   727: invokevirtual print : (I)V
    //   730: aload_0
    //   731: bipush #44
    //   733: invokevirtual print : (C)V
    //   736: aload_0
    //   737: aload_1
    //   738: iload #6
    //   740: invokevirtual getOtherSharedClean : (I)I
    //   743: invokevirtual print : (I)V
    //   746: aload_0
    //   747: bipush #44
    //   749: invokevirtual print : (C)V
    //   752: aload_0
    //   753: aload_1
    //   754: iload #6
    //   756: invokevirtual getOtherPrivateDirty : (I)I
    //   759: invokevirtual print : (I)V
    //   762: aload_0
    //   763: bipush #44
    //   765: invokevirtual print : (C)V
    //   768: aload_0
    //   769: aload_1
    //   770: iload #6
    //   772: invokevirtual getOtherPrivateClean : (I)I
    //   775: invokevirtual print : (I)V
    //   778: aload_0
    //   779: bipush #44
    //   781: invokevirtual print : (C)V
    //   784: aload_0
    //   785: aload_1
    //   786: iload #6
    //   788: invokevirtual getOtherSwappedOut : (I)I
    //   791: invokevirtual print : (I)V
    //   794: aload_0
    //   795: bipush #44
    //   797: invokevirtual print : (C)V
    //   800: aload_1
    //   801: getfield hasSwappedOutPss : Z
    //   804: ifeq -> 826
    //   807: aload_0
    //   808: aload_1
    //   809: iload #6
    //   811: invokevirtual getOtherSwappedOutPss : (I)I
    //   814: invokevirtual print : (I)V
    //   817: aload_0
    //   818: bipush #44
    //   820: invokevirtual print : (C)V
    //   823: goto -> 833
    //   826: aload_0
    //   827: ldc_w 'N/A,'
    //   830: invokevirtual print : (Ljava/lang/String;)V
    //   833: iinc #6, 1
    //   836: goto -> 666
    //   839: return
    //   840: iload #5
    //   842: ifne -> 3485
    //   845: iload_3
    //   846: ifeq -> 1488
    //   849: aload_1
    //   850: getfield hasSwappedOutPss : Z
    //   853: ifeq -> 864
    //   856: ldc_w 'SwapPss'
    //   859: astore #7
    //   861: goto -> 869
    //   864: ldc_w 'Swap'
    //   867: astore #7
    //   869: aload_0
    //   870: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   872: bipush #12
    //   874: anewarray java/lang/Object
    //   877: dup
    //   878: iconst_0
    //   879: ldc_w ''
    //   882: aastore
    //   883: dup
    //   884: iconst_1
    //   885: ldc_w 'Pss'
    //   888: aastore
    //   889: dup
    //   890: iconst_2
    //   891: ldc_w 'Pss'
    //   894: aastore
    //   895: dup
    //   896: iconst_3
    //   897: ldc_w 'Shared'
    //   900: aastore
    //   901: dup
    //   902: iconst_4
    //   903: ldc_w 'Private'
    //   906: aastore
    //   907: dup
    //   908: iconst_5
    //   909: ldc_w 'Shared'
    //   912: aastore
    //   913: dup
    //   914: bipush #6
    //   916: ldc_w 'Private'
    //   919: aastore
    //   920: dup
    //   921: bipush #7
    //   923: aload #7
    //   925: aastore
    //   926: dup
    //   927: bipush #8
    //   929: ldc_w 'Rss'
    //   932: aastore
    //   933: dup
    //   934: bipush #9
    //   936: ldc_w 'Heap'
    //   939: aastore
    //   940: dup
    //   941: bipush #10
    //   943: ldc_w 'Heap'
    //   946: aastore
    //   947: dup
    //   948: bipush #11
    //   950: ldc_w 'Heap'
    //   953: aastore
    //   954: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   957: aload_0
    //   958: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   960: bipush #12
    //   962: anewarray java/lang/Object
    //   965: dup
    //   966: iconst_0
    //   967: ldc_w ''
    //   970: aastore
    //   971: dup
    //   972: iconst_1
    //   973: ldc_w 'Total'
    //   976: aastore
    //   977: dup
    //   978: iconst_2
    //   979: ldc_w 'Clean'
    //   982: aastore
    //   983: dup
    //   984: iconst_3
    //   985: ldc_w 'Dirty'
    //   988: aastore
    //   989: dup
    //   990: iconst_4
    //   991: ldc_w 'Dirty'
    //   994: aastore
    //   995: dup
    //   996: iconst_5
    //   997: ldc_w 'Clean'
    //   1000: aastore
    //   1001: dup
    //   1002: bipush #6
    //   1004: ldc_w 'Clean'
    //   1007: aastore
    //   1008: dup
    //   1009: bipush #7
    //   1011: ldc_w 'Dirty'
    //   1014: aastore
    //   1015: dup
    //   1016: bipush #8
    //   1018: ldc_w 'Total'
    //   1021: aastore
    //   1022: dup
    //   1023: bipush #9
    //   1025: ldc_w 'Size'
    //   1028: aastore
    //   1029: dup
    //   1030: bipush #10
    //   1032: ldc_w 'Alloc'
    //   1035: aastore
    //   1036: dup
    //   1037: bipush #11
    //   1039: ldc_w 'Free'
    //   1042: aastore
    //   1043: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1046: aload_0
    //   1047: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1049: bipush #12
    //   1051: anewarray java/lang/Object
    //   1054: dup
    //   1055: iconst_0
    //   1056: ldc_w ''
    //   1059: aastore
    //   1060: dup
    //   1061: iconst_1
    //   1062: ldc_w '------'
    //   1065: aastore
    //   1066: dup
    //   1067: iconst_2
    //   1068: ldc_w '------'
    //   1071: aastore
    //   1072: dup
    //   1073: iconst_3
    //   1074: ldc_w '------'
    //   1077: aastore
    //   1078: dup
    //   1079: iconst_4
    //   1080: ldc_w '------'
    //   1083: aastore
    //   1084: dup
    //   1085: iconst_5
    //   1086: ldc_w '------'
    //   1089: aastore
    //   1090: dup
    //   1091: bipush #6
    //   1093: ldc_w '------'
    //   1096: aastore
    //   1097: dup
    //   1098: bipush #7
    //   1100: ldc_w '------'
    //   1103: aastore
    //   1104: dup
    //   1105: bipush #8
    //   1107: ldc_w '------'
    //   1110: aastore
    //   1111: dup
    //   1112: bipush #9
    //   1114: ldc_w '------'
    //   1117: aastore
    //   1118: dup
    //   1119: bipush #10
    //   1121: ldc_w '------'
    //   1124: aastore
    //   1125: dup
    //   1126: bipush #11
    //   1128: ldc_w '------'
    //   1131: aastore
    //   1132: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1135: aload_1
    //   1136: getfield nativePss : I
    //   1139: istore #20
    //   1141: aload_1
    //   1142: getfield nativeSwappablePss : I
    //   1145: istore #21
    //   1147: aload_1
    //   1148: getfield nativeSharedDirty : I
    //   1151: istore #22
    //   1153: aload_1
    //   1154: getfield nativePrivateDirty : I
    //   1157: istore #23
    //   1159: aload_1
    //   1160: getfield nativeSharedClean : I
    //   1163: istore #24
    //   1165: aload_1
    //   1166: getfield nativePrivateClean : I
    //   1169: istore #25
    //   1171: aload_1
    //   1172: getfield hasSwappedOutPss : Z
    //   1175: ifeq -> 1187
    //   1178: aload_1
    //   1179: getfield nativeSwappedOutPss : I
    //   1182: istore #6
    //   1184: goto -> 1193
    //   1187: aload_1
    //   1188: getfield nativeSwappedOut : I
    //   1191: istore #6
    //   1193: aload_1
    //   1194: getfield nativeRss : I
    //   1197: istore #26
    //   1199: aload_0
    //   1200: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1202: bipush #12
    //   1204: anewarray java/lang/Object
    //   1207: dup
    //   1208: iconst_0
    //   1209: ldc_w 'Native Heap'
    //   1212: aastore
    //   1213: dup
    //   1214: iconst_1
    //   1215: iload #20
    //   1217: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1220: aastore
    //   1221: dup
    //   1222: iconst_2
    //   1223: iload #21
    //   1225: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1228: aastore
    //   1229: dup
    //   1230: iconst_3
    //   1231: iload #22
    //   1233: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1236: aastore
    //   1237: dup
    //   1238: iconst_4
    //   1239: iload #23
    //   1241: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1244: aastore
    //   1245: dup
    //   1246: iconst_5
    //   1247: iload #24
    //   1249: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1252: aastore
    //   1253: dup
    //   1254: bipush #6
    //   1256: iload #25
    //   1258: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1261: aastore
    //   1262: dup
    //   1263: bipush #7
    //   1265: iload #6
    //   1267: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1270: aastore
    //   1271: dup
    //   1272: bipush #8
    //   1274: iload #26
    //   1276: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1279: aastore
    //   1280: dup
    //   1281: bipush #9
    //   1283: lload #8
    //   1285: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1288: aastore
    //   1289: dup
    //   1290: bipush #10
    //   1292: lload #10
    //   1294: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1297: aastore
    //   1298: dup
    //   1299: bipush #11
    //   1301: lload #12
    //   1303: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1306: aastore
    //   1307: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1310: aload_1
    //   1311: getfield dalvikPss : I
    //   1314: istore #23
    //   1316: aload_1
    //   1317: getfield dalvikSwappablePss : I
    //   1320: istore #25
    //   1322: aload_1
    //   1323: getfield dalvikSharedDirty : I
    //   1326: istore #21
    //   1328: aload_1
    //   1329: getfield dalvikPrivateDirty : I
    //   1332: istore #24
    //   1334: aload_1
    //   1335: getfield dalvikSharedClean : I
    //   1338: istore #22
    //   1340: aload_1
    //   1341: getfield dalvikPrivateClean : I
    //   1344: istore #20
    //   1346: aload_1
    //   1347: getfield hasSwappedOutPss : Z
    //   1350: ifeq -> 1362
    //   1353: aload_1
    //   1354: getfield dalvikSwappedOutPss : I
    //   1357: istore #6
    //   1359: goto -> 1368
    //   1362: aload_1
    //   1363: getfield dalvikSwappedOut : I
    //   1366: istore #6
    //   1368: aload_1
    //   1369: getfield dalvikRss : I
    //   1372: istore #26
    //   1374: aload_0
    //   1375: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1377: bipush #12
    //   1379: anewarray java/lang/Object
    //   1382: dup
    //   1383: iconst_0
    //   1384: ldc_w 'Dalvik Heap'
    //   1387: aastore
    //   1388: dup
    //   1389: iconst_1
    //   1390: iload #23
    //   1392: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1395: aastore
    //   1396: dup
    //   1397: iconst_2
    //   1398: iload #25
    //   1400: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1403: aastore
    //   1404: dup
    //   1405: iconst_3
    //   1406: iload #21
    //   1408: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1411: aastore
    //   1412: dup
    //   1413: iconst_4
    //   1414: iload #24
    //   1416: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1419: aastore
    //   1420: dup
    //   1421: iconst_5
    //   1422: iload #22
    //   1424: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1427: aastore
    //   1428: dup
    //   1429: bipush #6
    //   1431: iload #20
    //   1433: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1436: aastore
    //   1437: dup
    //   1438: bipush #7
    //   1440: iload #6
    //   1442: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1445: aastore
    //   1446: dup
    //   1447: bipush #8
    //   1449: iload #26
    //   1451: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1454: aastore
    //   1455: dup
    //   1456: bipush #9
    //   1458: lload #14
    //   1460: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1463: aastore
    //   1464: dup
    //   1465: bipush #10
    //   1467: lload #16
    //   1469: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1472: aastore
    //   1473: dup
    //   1474: bipush #11
    //   1476: lload #18
    //   1478: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1481: aastore
    //   1482: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1485: goto -> 1978
    //   1488: aload_1
    //   1489: getfield hasSwappedOutPss : Z
    //   1492: ifeq -> 1503
    //   1495: ldc_w 'SwapPss'
    //   1498: astore #7
    //   1500: goto -> 1508
    //   1503: ldc_w 'Swap'
    //   1506: astore #7
    //   1508: aload_0
    //   1509: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1511: bipush #9
    //   1513: anewarray java/lang/Object
    //   1516: dup
    //   1517: iconst_0
    //   1518: ldc_w ''
    //   1521: aastore
    //   1522: dup
    //   1523: iconst_1
    //   1524: ldc_w 'Pss'
    //   1527: aastore
    //   1528: dup
    //   1529: iconst_2
    //   1530: ldc_w 'Private'
    //   1533: aastore
    //   1534: dup
    //   1535: iconst_3
    //   1536: ldc_w 'Private'
    //   1539: aastore
    //   1540: dup
    //   1541: iconst_4
    //   1542: aload #7
    //   1544: aastore
    //   1545: dup
    //   1546: iconst_5
    //   1547: ldc_w 'Rss'
    //   1550: aastore
    //   1551: dup
    //   1552: bipush #6
    //   1554: ldc_w 'Heap'
    //   1557: aastore
    //   1558: dup
    //   1559: bipush #7
    //   1561: ldc_w 'Heap'
    //   1564: aastore
    //   1565: dup
    //   1566: bipush #8
    //   1568: ldc_w 'Heap'
    //   1571: aastore
    //   1572: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1575: aload_0
    //   1576: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1578: bipush #9
    //   1580: anewarray java/lang/Object
    //   1583: dup
    //   1584: iconst_0
    //   1585: ldc_w ''
    //   1588: aastore
    //   1589: dup
    //   1590: iconst_1
    //   1591: ldc_w 'Total'
    //   1594: aastore
    //   1595: dup
    //   1596: iconst_2
    //   1597: ldc_w 'Dirty'
    //   1600: aastore
    //   1601: dup
    //   1602: iconst_3
    //   1603: ldc_w 'Clean'
    //   1606: aastore
    //   1607: dup
    //   1608: iconst_4
    //   1609: ldc_w 'Dirty'
    //   1612: aastore
    //   1613: dup
    //   1614: iconst_5
    //   1615: ldc_w 'Total'
    //   1618: aastore
    //   1619: dup
    //   1620: bipush #6
    //   1622: ldc_w 'Size'
    //   1625: aastore
    //   1626: dup
    //   1627: bipush #7
    //   1629: ldc_w 'Alloc'
    //   1632: aastore
    //   1633: dup
    //   1634: bipush #8
    //   1636: ldc_w 'Free'
    //   1639: aastore
    //   1640: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1643: aload_0
    //   1644: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1646: bipush #10
    //   1648: anewarray java/lang/Object
    //   1651: dup
    //   1652: iconst_0
    //   1653: ldc_w ''
    //   1656: aastore
    //   1657: dup
    //   1658: iconst_1
    //   1659: ldc_w '------'
    //   1662: aastore
    //   1663: dup
    //   1664: iconst_2
    //   1665: ldc_w '------'
    //   1668: aastore
    //   1669: dup
    //   1670: iconst_3
    //   1671: ldc_w '------'
    //   1674: aastore
    //   1675: dup
    //   1676: iconst_4
    //   1677: ldc_w '------'
    //   1680: aastore
    //   1681: dup
    //   1682: iconst_5
    //   1683: ldc_w '------'
    //   1686: aastore
    //   1687: dup
    //   1688: bipush #6
    //   1690: ldc_w '------'
    //   1693: aastore
    //   1694: dup
    //   1695: bipush #7
    //   1697: ldc_w '------'
    //   1700: aastore
    //   1701: dup
    //   1702: bipush #8
    //   1704: ldc_w '------'
    //   1707: aastore
    //   1708: dup
    //   1709: bipush #9
    //   1711: ldc_w '------'
    //   1714: aastore
    //   1715: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1718: aload_1
    //   1719: getfield nativePss : I
    //   1722: istore #22
    //   1724: aload_1
    //   1725: getfield nativePrivateDirty : I
    //   1728: istore #23
    //   1730: aload_1
    //   1731: getfield nativePrivateClean : I
    //   1734: istore #21
    //   1736: aload_1
    //   1737: getfield hasSwappedOutPss : Z
    //   1740: ifeq -> 1752
    //   1743: aload_1
    //   1744: getfield nativeSwappedOutPss : I
    //   1747: istore #6
    //   1749: goto -> 1758
    //   1752: aload_1
    //   1753: getfield nativeSwappedOut : I
    //   1756: istore #6
    //   1758: aload_1
    //   1759: getfield nativeRss : I
    //   1762: istore #25
    //   1764: aload_0
    //   1765: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1767: bipush #9
    //   1769: anewarray java/lang/Object
    //   1772: dup
    //   1773: iconst_0
    //   1774: ldc_w 'Native Heap'
    //   1777: aastore
    //   1778: dup
    //   1779: iconst_1
    //   1780: iload #22
    //   1782: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1785: aastore
    //   1786: dup
    //   1787: iconst_2
    //   1788: iload #23
    //   1790: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1793: aastore
    //   1794: dup
    //   1795: iconst_3
    //   1796: iload #21
    //   1798: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1801: aastore
    //   1802: dup
    //   1803: iconst_4
    //   1804: iload #6
    //   1806: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1809: aastore
    //   1810: dup
    //   1811: iconst_5
    //   1812: iload #25
    //   1814: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1817: aastore
    //   1818: dup
    //   1819: bipush #6
    //   1821: lload #8
    //   1823: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1826: aastore
    //   1827: dup
    //   1828: bipush #7
    //   1830: lload #10
    //   1832: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1835: aastore
    //   1836: dup
    //   1837: bipush #8
    //   1839: lload #12
    //   1841: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1844: aastore
    //   1845: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1848: aload_1
    //   1849: getfield dalvikPss : I
    //   1852: istore #23
    //   1854: aload_1
    //   1855: getfield dalvikPrivateDirty : I
    //   1858: istore #21
    //   1860: aload_1
    //   1861: getfield dalvikPrivateClean : I
    //   1864: istore #22
    //   1866: aload_1
    //   1867: getfield hasSwappedOutPss : Z
    //   1870: ifeq -> 1882
    //   1873: aload_1
    //   1874: getfield dalvikSwappedOutPss : I
    //   1877: istore #6
    //   1879: goto -> 1888
    //   1882: aload_1
    //   1883: getfield dalvikSwappedOut : I
    //   1886: istore #6
    //   1888: aload_1
    //   1889: getfield dalvikRss : I
    //   1892: istore #25
    //   1894: aload_0
    //   1895: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   1897: bipush #9
    //   1899: anewarray java/lang/Object
    //   1902: dup
    //   1903: iconst_0
    //   1904: ldc_w 'Dalvik Heap'
    //   1907: aastore
    //   1908: dup
    //   1909: iconst_1
    //   1910: iload #23
    //   1912: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1915: aastore
    //   1916: dup
    //   1917: iconst_2
    //   1918: iload #21
    //   1920: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1923: aastore
    //   1924: dup
    //   1925: iconst_3
    //   1926: iload #22
    //   1928: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1931: aastore
    //   1932: dup
    //   1933: iconst_4
    //   1934: iload #6
    //   1936: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1939: aastore
    //   1940: dup
    //   1941: iconst_5
    //   1942: iload #25
    //   1944: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1947: aastore
    //   1948: dup
    //   1949: bipush #6
    //   1951: lload #14
    //   1953: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1956: aastore
    //   1957: dup
    //   1958: bipush #7
    //   1960: lload #16
    //   1962: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1965: aastore
    //   1966: dup
    //   1967: bipush #8
    //   1969: lload #18
    //   1971: invokestatic valueOf : (J)Ljava/lang/Long;
    //   1974: aastore
    //   1975: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   1978: aload_1
    //   1979: getfield otherPss : I
    //   1982: istore #21
    //   1984: aload_1
    //   1985: getfield otherSwappablePss : I
    //   1988: istore #27
    //   1990: aload_1
    //   1991: getfield otherSharedDirty : I
    //   1994: istore #28
    //   1996: aload_1
    //   1997: getfield otherPrivateDirty : I
    //   2000: istore #26
    //   2002: aload_1
    //   2003: getfield otherSharedClean : I
    //   2006: istore #29
    //   2008: aload_1
    //   2009: getfield otherPrivateClean : I
    //   2012: istore #24
    //   2014: aload_1
    //   2015: getfield otherSwappedOut : I
    //   2018: istore #23
    //   2020: aload_1
    //   2021: getfield otherSwappedOutPss : I
    //   2024: istore #6
    //   2026: aload_1
    //   2027: getfield otherRss : I
    //   2030: istore #22
    //   2032: iconst_0
    //   2033: istore #30
    //   2035: iload #30
    //   2037: bipush #17
    //   2039: if_icmpge -> 2546
    //   2042: aload_1
    //   2043: iload #30
    //   2045: invokevirtual getOtherPss : (I)I
    //   2048: istore #31
    //   2050: aload_1
    //   2051: iload #30
    //   2053: invokevirtual getOtherSwappablePss : (I)I
    //   2056: istore #32
    //   2058: aload_1
    //   2059: iload #30
    //   2061: invokevirtual getOtherSharedDirty : (I)I
    //   2064: istore #33
    //   2066: aload_1
    //   2067: iload #30
    //   2069: invokevirtual getOtherPrivateDirty : (I)I
    //   2072: istore #34
    //   2074: aload_1
    //   2075: iload #30
    //   2077: invokevirtual getOtherSharedClean : (I)I
    //   2080: istore #35
    //   2082: aload_1
    //   2083: iload #30
    //   2085: invokevirtual getOtherPrivateClean : (I)I
    //   2088: istore #36
    //   2090: aload_1
    //   2091: iload #30
    //   2093: invokevirtual getOtherSwappedOut : (I)I
    //   2096: istore #20
    //   2098: aload_1
    //   2099: iload #30
    //   2101: invokevirtual getOtherSwappedOutPss : (I)I
    //   2104: istore #25
    //   2106: aload_1
    //   2107: iload #30
    //   2109: invokevirtual getOtherRss : (I)I
    //   2112: istore #37
    //   2114: iload #31
    //   2116: ifne -> 2203
    //   2119: iload #33
    //   2121: ifne -> 2203
    //   2124: iload #34
    //   2126: ifne -> 2203
    //   2129: iload #35
    //   2131: ifne -> 2203
    //   2134: iload #36
    //   2136: ifne -> 2203
    //   2139: iload #37
    //   2141: ifne -> 2203
    //   2144: aload_1
    //   2145: getfield hasSwappedOutPss : Z
    //   2148: ifeq -> 2158
    //   2151: iload #25
    //   2153: istore #38
    //   2155: goto -> 2162
    //   2158: iload #20
    //   2160: istore #38
    //   2162: iload #21
    //   2164: istore #39
    //   2166: iload #22
    //   2168: istore #40
    //   2170: iload #27
    //   2172: istore #41
    //   2174: iload #28
    //   2176: istore #42
    //   2178: iload #26
    //   2180: istore #43
    //   2182: iload #29
    //   2184: istore #44
    //   2186: iload #24
    //   2188: istore #45
    //   2190: iload #23
    //   2192: istore #46
    //   2194: iload #6
    //   2196: istore #47
    //   2198: iload #38
    //   2200: ifeq -> 2504
    //   2203: iload_3
    //   2204: ifeq -> 2339
    //   2207: iload #30
    //   2209: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   2212: astore #7
    //   2214: aload_1
    //   2215: getfield hasSwappedOutPss : Z
    //   2218: ifeq -> 2228
    //   2221: iload #25
    //   2223: istore #47
    //   2225: goto -> 2232
    //   2228: iload #20
    //   2230: istore #47
    //   2232: aload_0
    //   2233: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2235: bipush #12
    //   2237: anewarray java/lang/Object
    //   2240: dup
    //   2241: iconst_0
    //   2242: aload #7
    //   2244: aastore
    //   2245: dup
    //   2246: iconst_1
    //   2247: iload #31
    //   2249: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2252: aastore
    //   2253: dup
    //   2254: iconst_2
    //   2255: iload #32
    //   2257: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2260: aastore
    //   2261: dup
    //   2262: iconst_3
    //   2263: iload #33
    //   2265: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2268: aastore
    //   2269: dup
    //   2270: iconst_4
    //   2271: iload #34
    //   2273: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2276: aastore
    //   2277: dup
    //   2278: iconst_5
    //   2279: iload #35
    //   2281: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2284: aastore
    //   2285: dup
    //   2286: bipush #6
    //   2288: iload #36
    //   2290: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2293: aastore
    //   2294: dup
    //   2295: bipush #7
    //   2297: iload #47
    //   2299: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2302: aastore
    //   2303: dup
    //   2304: bipush #8
    //   2306: iload #37
    //   2308: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2311: aastore
    //   2312: dup
    //   2313: bipush #9
    //   2315: ldc_w ''
    //   2318: aastore
    //   2319: dup
    //   2320: bipush #10
    //   2322: ldc_w ''
    //   2325: aastore
    //   2326: dup
    //   2327: bipush #11
    //   2329: ldc_w ''
    //   2332: aastore
    //   2333: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   2336: goto -> 2441
    //   2339: iload #30
    //   2341: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   2344: astore #7
    //   2346: aload_1
    //   2347: getfield hasSwappedOutPss : Z
    //   2350: ifeq -> 2360
    //   2353: iload #25
    //   2355: istore #47
    //   2357: goto -> 2364
    //   2360: iload #20
    //   2362: istore #47
    //   2364: aload_0
    //   2365: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2367: bipush #9
    //   2369: anewarray java/lang/Object
    //   2372: dup
    //   2373: iconst_0
    //   2374: aload #7
    //   2376: aastore
    //   2377: dup
    //   2378: iconst_1
    //   2379: iload #31
    //   2381: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2384: aastore
    //   2385: dup
    //   2386: iconst_2
    //   2387: iload #34
    //   2389: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2392: aastore
    //   2393: dup
    //   2394: iconst_3
    //   2395: iload #36
    //   2397: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2400: aastore
    //   2401: dup
    //   2402: iconst_4
    //   2403: iload #47
    //   2405: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2408: aastore
    //   2409: dup
    //   2410: iconst_5
    //   2411: iload #37
    //   2413: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2416: aastore
    //   2417: dup
    //   2418: bipush #6
    //   2420: ldc_w ''
    //   2423: aastore
    //   2424: dup
    //   2425: bipush #7
    //   2427: ldc_w ''
    //   2430: aastore
    //   2431: dup
    //   2432: bipush #8
    //   2434: ldc_w ''
    //   2437: aastore
    //   2438: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   2441: iload #21
    //   2443: iload #31
    //   2445: isub
    //   2446: istore #39
    //   2448: iload #27
    //   2450: iload #32
    //   2452: isub
    //   2453: istore #41
    //   2455: iload #28
    //   2457: iload #33
    //   2459: isub
    //   2460: istore #42
    //   2462: iload #26
    //   2464: iload #34
    //   2466: isub
    //   2467: istore #43
    //   2469: iload #29
    //   2471: iload #35
    //   2473: isub
    //   2474: istore #44
    //   2476: iload #24
    //   2478: iload #36
    //   2480: isub
    //   2481: istore #45
    //   2483: iload #23
    //   2485: iload #20
    //   2487: isub
    //   2488: istore #46
    //   2490: iload #6
    //   2492: iload #25
    //   2494: isub
    //   2495: istore #47
    //   2497: iload #22
    //   2499: iload #37
    //   2501: isub
    //   2502: istore #40
    //   2504: iinc #30, 1
    //   2507: iload #39
    //   2509: istore #21
    //   2511: iload #40
    //   2513: istore #22
    //   2515: iload #41
    //   2517: istore #27
    //   2519: iload #42
    //   2521: istore #28
    //   2523: iload #43
    //   2525: istore #26
    //   2527: iload #44
    //   2529: istore #29
    //   2531: iload #45
    //   2533: istore #24
    //   2535: iload #46
    //   2537: istore #23
    //   2539: iload #47
    //   2541: istore #6
    //   2543: goto -> 2035
    //   2546: iload_3
    //   2547: ifeq -> 2856
    //   2550: aload_1
    //   2551: getfield hasSwappedOutPss : Z
    //   2554: ifeq -> 2560
    //   2557: goto -> 2564
    //   2560: iload #23
    //   2562: istore #6
    //   2564: aload_0
    //   2565: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2567: bipush #12
    //   2569: anewarray java/lang/Object
    //   2572: dup
    //   2573: iconst_0
    //   2574: ldc_w 'Unknown'
    //   2577: aastore
    //   2578: dup
    //   2579: iconst_1
    //   2580: iload #21
    //   2582: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2585: aastore
    //   2586: dup
    //   2587: iconst_2
    //   2588: iload #27
    //   2590: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2593: aastore
    //   2594: dup
    //   2595: iconst_3
    //   2596: iload #28
    //   2598: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2601: aastore
    //   2602: dup
    //   2603: iconst_4
    //   2604: iload #26
    //   2606: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2609: aastore
    //   2610: dup
    //   2611: iconst_5
    //   2612: iload #29
    //   2614: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2617: aastore
    //   2618: dup
    //   2619: bipush #6
    //   2621: iload #24
    //   2623: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2626: aastore
    //   2627: dup
    //   2628: bipush #7
    //   2630: iload #6
    //   2632: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2635: aastore
    //   2636: dup
    //   2637: bipush #8
    //   2639: iload #22
    //   2641: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2644: aastore
    //   2645: dup
    //   2646: bipush #9
    //   2648: ldc_w ''
    //   2651: aastore
    //   2652: dup
    //   2653: bipush #10
    //   2655: ldc_w ''
    //   2658: aastore
    //   2659: dup
    //   2660: bipush #11
    //   2662: ldc_w ''
    //   2665: aastore
    //   2666: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   2669: aload_1
    //   2670: invokevirtual getTotalPss : ()I
    //   2673: istore #23
    //   2675: aload_1
    //   2676: invokevirtual getTotalSwappablePss : ()I
    //   2679: istore #29
    //   2681: aload_1
    //   2682: invokevirtual getTotalSharedDirty : ()I
    //   2685: istore #25
    //   2687: aload_1
    //   2688: invokevirtual getTotalPrivateDirty : ()I
    //   2691: istore #26
    //   2693: aload_1
    //   2694: invokevirtual getTotalSharedClean : ()I
    //   2697: istore #24
    //   2699: aload_1
    //   2700: invokevirtual getTotalPrivateClean : ()I
    //   2703: istore #20
    //   2705: aload_1
    //   2706: getfield hasSwappedOutPss : Z
    //   2709: ifeq -> 2721
    //   2712: aload_1
    //   2713: invokevirtual getTotalSwappedOutPss : ()I
    //   2716: istore #6
    //   2718: goto -> 2727
    //   2721: aload_1
    //   2722: invokevirtual getTotalSwappedOut : ()I
    //   2725: istore #6
    //   2727: aload_1
    //   2728: invokevirtual getTotalRss : ()I
    //   2731: istore #30
    //   2733: aload_0
    //   2734: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2736: bipush #12
    //   2738: anewarray java/lang/Object
    //   2741: dup
    //   2742: iconst_0
    //   2743: ldc_w 'TOTAL'
    //   2746: aastore
    //   2747: dup
    //   2748: iconst_1
    //   2749: iload #23
    //   2751: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2754: aastore
    //   2755: dup
    //   2756: iconst_2
    //   2757: iload #29
    //   2759: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2762: aastore
    //   2763: dup
    //   2764: iconst_3
    //   2765: iload #25
    //   2767: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2770: aastore
    //   2771: dup
    //   2772: iconst_4
    //   2773: iload #26
    //   2775: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2778: aastore
    //   2779: dup
    //   2780: iconst_5
    //   2781: iload #24
    //   2783: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2786: aastore
    //   2787: dup
    //   2788: bipush #6
    //   2790: iload #20
    //   2792: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2795: aastore
    //   2796: dup
    //   2797: bipush #7
    //   2799: iload #6
    //   2801: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2804: aastore
    //   2805: dup
    //   2806: bipush #8
    //   2808: iload #30
    //   2810: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2813: aastore
    //   2814: dup
    //   2815: bipush #9
    //   2817: lload #8
    //   2819: lload #14
    //   2821: ladd
    //   2822: invokestatic valueOf : (J)Ljava/lang/Long;
    //   2825: aastore
    //   2826: dup
    //   2827: bipush #10
    //   2829: lload #10
    //   2831: lload #16
    //   2833: ladd
    //   2834: invokestatic valueOf : (J)Ljava/lang/Long;
    //   2837: aastore
    //   2838: dup
    //   2839: bipush #11
    //   2841: lload #12
    //   2843: lload #18
    //   2845: ladd
    //   2846: invokestatic valueOf : (J)Ljava/lang/Long;
    //   2849: aastore
    //   2850: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   2853: goto -> 3087
    //   2856: aload_1
    //   2857: getfield hasSwappedOutPss : Z
    //   2860: ifeq -> 2866
    //   2863: goto -> 2870
    //   2866: iload #23
    //   2868: istore #6
    //   2870: aload_0
    //   2871: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2873: bipush #9
    //   2875: anewarray java/lang/Object
    //   2878: dup
    //   2879: iconst_0
    //   2880: ldc_w 'Unknown'
    //   2883: aastore
    //   2884: dup
    //   2885: iconst_1
    //   2886: iload #21
    //   2888: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2891: aastore
    //   2892: dup
    //   2893: iconst_2
    //   2894: iload #26
    //   2896: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2899: aastore
    //   2900: dup
    //   2901: iconst_3
    //   2902: iload #24
    //   2904: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2907: aastore
    //   2908: dup
    //   2909: iconst_4
    //   2910: iload #6
    //   2912: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2915: aastore
    //   2916: dup
    //   2917: iconst_5
    //   2918: iload #22
    //   2920: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   2923: aastore
    //   2924: dup
    //   2925: bipush #6
    //   2927: ldc_w ''
    //   2930: aastore
    //   2931: dup
    //   2932: bipush #7
    //   2934: ldc_w ''
    //   2937: aastore
    //   2938: dup
    //   2939: bipush #8
    //   2941: ldc_w ''
    //   2944: aastore
    //   2945: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   2948: aload_1
    //   2949: invokevirtual getTotalPss : ()I
    //   2952: istore #23
    //   2954: aload_1
    //   2955: invokevirtual getTotalPrivateDirty : ()I
    //   2958: istore #20
    //   2960: aload_1
    //   2961: invokevirtual getTotalPrivateClean : ()I
    //   2964: istore #25
    //   2966: aload_1
    //   2967: getfield hasSwappedOutPss : Z
    //   2970: ifeq -> 2982
    //   2973: aload_1
    //   2974: invokevirtual getTotalSwappedOutPss : ()I
    //   2977: istore #6
    //   2979: goto -> 2988
    //   2982: aload_1
    //   2983: invokevirtual getTotalSwappedOut : ()I
    //   2986: istore #6
    //   2988: aload_1
    //   2989: invokevirtual getTotalPss : ()I
    //   2992: istore #24
    //   2994: aload_0
    //   2995: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   2997: bipush #9
    //   2999: anewarray java/lang/Object
    //   3002: dup
    //   3003: iconst_0
    //   3004: ldc_w 'TOTAL'
    //   3007: aastore
    //   3008: dup
    //   3009: iconst_1
    //   3010: iload #23
    //   3012: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3015: aastore
    //   3016: dup
    //   3017: iconst_2
    //   3018: iload #20
    //   3020: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3023: aastore
    //   3024: dup
    //   3025: iconst_3
    //   3026: iload #25
    //   3028: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3031: aastore
    //   3032: dup
    //   3033: iconst_4
    //   3034: iload #6
    //   3036: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3039: aastore
    //   3040: dup
    //   3041: iconst_5
    //   3042: iload #24
    //   3044: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3047: aastore
    //   3048: dup
    //   3049: bipush #6
    //   3051: lload #8
    //   3053: lload #14
    //   3055: ladd
    //   3056: invokestatic valueOf : (J)Ljava/lang/Long;
    //   3059: aastore
    //   3060: dup
    //   3061: bipush #7
    //   3063: lload #10
    //   3065: lload #16
    //   3067: ladd
    //   3068: invokestatic valueOf : (J)Ljava/lang/Long;
    //   3071: aastore
    //   3072: dup
    //   3073: bipush #8
    //   3075: lload #12
    //   3077: lload #18
    //   3079: ladd
    //   3080: invokestatic valueOf : (J)Ljava/lang/Long;
    //   3083: aastore
    //   3084: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3087: iload #4
    //   3089: ifeq -> 3482
    //   3092: aload_0
    //   3093: ldc_w ' '
    //   3096: invokevirtual println : (Ljava/lang/String;)V
    //   3099: aload_0
    //   3100: ldc_w ' Dalvik Details'
    //   3103: invokevirtual println : (Ljava/lang/String;)V
    //   3106: bipush #17
    //   3108: istore #25
    //   3110: iload #25
    //   3112: bipush #32
    //   3114: if_icmpge -> 3479
    //   3117: aload_1
    //   3118: iload #25
    //   3120: invokevirtual getOtherPss : (I)I
    //   3123: istore #30
    //   3125: aload_1
    //   3126: iload #25
    //   3128: invokevirtual getOtherSwappablePss : (I)I
    //   3131: istore #27
    //   3133: aload_1
    //   3134: iload #25
    //   3136: invokevirtual getOtherSharedDirty : (I)I
    //   3139: istore #28
    //   3141: aload_1
    //   3142: iload #25
    //   3144: invokevirtual getOtherPrivateDirty : (I)I
    //   3147: istore #29
    //   3149: aload_1
    //   3150: iload #25
    //   3152: invokevirtual getOtherSharedClean : (I)I
    //   3155: istore #47
    //   3157: aload_1
    //   3158: iload #25
    //   3160: invokevirtual getOtherPrivateClean : (I)I
    //   3163: istore #26
    //   3165: aload_1
    //   3166: iload #25
    //   3168: invokevirtual getOtherSwappedOut : (I)I
    //   3171: istore #6
    //   3173: aload_1
    //   3174: iload #25
    //   3176: invokevirtual getOtherSwappedOutPss : (I)I
    //   3179: istore #23
    //   3181: aload_1
    //   3182: iload #25
    //   3184: invokevirtual getOtherRss : (I)I
    //   3187: istore #24
    //   3189: iload #30
    //   3191: ifne -> 3243
    //   3194: iload #28
    //   3196: ifne -> 3243
    //   3199: iload #29
    //   3201: ifne -> 3243
    //   3204: iload #47
    //   3206: ifne -> 3243
    //   3209: iload #26
    //   3211: ifne -> 3243
    //   3214: aload_1
    //   3215: getfield hasSwappedOutPss : Z
    //   3218: ifeq -> 3228
    //   3221: iload #23
    //   3223: istore #20
    //   3225: goto -> 3232
    //   3228: iload #6
    //   3230: istore #20
    //   3232: iload #20
    //   3234: ifeq -> 3240
    //   3237: goto -> 3243
    //   3240: goto -> 3473
    //   3243: iload_3
    //   3244: ifeq -> 3375
    //   3247: iload #25
    //   3249: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   3252: astore #7
    //   3254: aload_1
    //   3255: getfield hasSwappedOutPss : Z
    //   3258: ifeq -> 3264
    //   3261: goto -> 3268
    //   3264: iload #6
    //   3266: istore #23
    //   3268: aload_0
    //   3269: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   3271: bipush #12
    //   3273: anewarray java/lang/Object
    //   3276: dup
    //   3277: iconst_0
    //   3278: aload #7
    //   3280: aastore
    //   3281: dup
    //   3282: iconst_1
    //   3283: iload #30
    //   3285: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3288: aastore
    //   3289: dup
    //   3290: iconst_2
    //   3291: iload #27
    //   3293: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3296: aastore
    //   3297: dup
    //   3298: iconst_3
    //   3299: iload #28
    //   3301: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3304: aastore
    //   3305: dup
    //   3306: iconst_4
    //   3307: iload #29
    //   3309: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3312: aastore
    //   3313: dup
    //   3314: iconst_5
    //   3315: iload #47
    //   3317: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3320: aastore
    //   3321: dup
    //   3322: bipush #6
    //   3324: iload #26
    //   3326: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3329: aastore
    //   3330: dup
    //   3331: bipush #7
    //   3333: iload #23
    //   3335: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3338: aastore
    //   3339: dup
    //   3340: bipush #8
    //   3342: iload #24
    //   3344: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3347: aastore
    //   3348: dup
    //   3349: bipush #9
    //   3351: ldc_w ''
    //   3354: aastore
    //   3355: dup
    //   3356: bipush #10
    //   3358: ldc_w ''
    //   3361: aastore
    //   3362: dup
    //   3363: bipush #11
    //   3365: ldc_w ''
    //   3368: aastore
    //   3369: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3372: goto -> 3473
    //   3375: iload #25
    //   3377: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   3380: astore #7
    //   3382: aload_1
    //   3383: getfield hasSwappedOutPss : Z
    //   3386: ifeq -> 3396
    //   3389: iload #23
    //   3391: istore #6
    //   3393: goto -> 3396
    //   3396: aload_0
    //   3397: ldc '%13s %8s %8s %8s %8s %8s %8s %8s %8s'
    //   3399: bipush #9
    //   3401: anewarray java/lang/Object
    //   3404: dup
    //   3405: iconst_0
    //   3406: aload #7
    //   3408: aastore
    //   3409: dup
    //   3410: iconst_1
    //   3411: iload #30
    //   3413: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3416: aastore
    //   3417: dup
    //   3418: iconst_2
    //   3419: iload #29
    //   3421: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3424: aastore
    //   3425: dup
    //   3426: iconst_3
    //   3427: iload #26
    //   3429: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3432: aastore
    //   3433: dup
    //   3434: iconst_4
    //   3435: iload #6
    //   3437: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3440: aastore
    //   3441: dup
    //   3442: iconst_5
    //   3443: iload #24
    //   3445: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3448: aastore
    //   3449: dup
    //   3450: bipush #6
    //   3452: ldc_w ''
    //   3455: aastore
    //   3456: dup
    //   3457: bipush #7
    //   3459: ldc_w ''
    //   3462: aastore
    //   3463: dup
    //   3464: bipush #8
    //   3466: ldc_w ''
    //   3469: aastore
    //   3470: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3473: iinc #25, 1
    //   3476: goto -> 3110
    //   3479: goto -> 3485
    //   3482: goto -> 3485
    //   3485: aload_0
    //   3486: ldc_w ' '
    //   3489: invokevirtual println : (Ljava/lang/String;)V
    //   3492: aload_0
    //   3493: ldc_w ' App Summary'
    //   3496: invokevirtual println : (Ljava/lang/String;)V
    //   3499: aload_0
    //   3500: ldc '%21s %8s %21s %8s'
    //   3502: iconst_4
    //   3503: anewarray java/lang/Object
    //   3506: dup
    //   3507: iconst_0
    //   3508: ldc_w ''
    //   3511: aastore
    //   3512: dup
    //   3513: iconst_1
    //   3514: ldc_w 'Pss(KB)'
    //   3517: aastore
    //   3518: dup
    //   3519: iconst_2
    //   3520: ldc_w ''
    //   3523: aastore
    //   3524: dup
    //   3525: iconst_3
    //   3526: ldc_w 'Rss(KB)'
    //   3529: aastore
    //   3530: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3533: aload_0
    //   3534: ldc '%21s %8s %21s %8s'
    //   3536: iconst_4
    //   3537: anewarray java/lang/Object
    //   3540: dup
    //   3541: iconst_0
    //   3542: ldc_w ''
    //   3545: aastore
    //   3546: dup
    //   3547: iconst_1
    //   3548: ldc_w '------'
    //   3551: aastore
    //   3552: dup
    //   3553: iconst_2
    //   3554: ldc_w ''
    //   3557: aastore
    //   3558: dup
    //   3559: iconst_3
    //   3560: ldc_w '------'
    //   3563: aastore
    //   3564: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3567: aload_1
    //   3568: invokevirtual getSummaryJavaHeap : ()I
    //   3571: istore #6
    //   3573: aload_1
    //   3574: invokevirtual getSummaryJavaHeapRss : ()I
    //   3577: istore #23
    //   3579: aload_0
    //   3580: ldc '%21s %8d %21s %8d'
    //   3582: iconst_4
    //   3583: anewarray java/lang/Object
    //   3586: dup
    //   3587: iconst_0
    //   3588: ldc_w 'Java Heap:'
    //   3591: aastore
    //   3592: dup
    //   3593: iconst_1
    //   3594: iload #6
    //   3596: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3599: aastore
    //   3600: dup
    //   3601: iconst_2
    //   3602: ldc_w ''
    //   3605: aastore
    //   3606: dup
    //   3607: iconst_3
    //   3608: iload #23
    //   3610: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3613: aastore
    //   3614: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3617: aload_1
    //   3618: invokevirtual getSummaryNativeHeap : ()I
    //   3621: istore #23
    //   3623: aload_1
    //   3624: invokevirtual getSummaryNativeHeapRss : ()I
    //   3627: istore #6
    //   3629: aload_0
    //   3630: ldc '%21s %8d %21s %8d'
    //   3632: iconst_4
    //   3633: anewarray java/lang/Object
    //   3636: dup
    //   3637: iconst_0
    //   3638: ldc_w 'Native Heap:'
    //   3641: aastore
    //   3642: dup
    //   3643: iconst_1
    //   3644: iload #23
    //   3646: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3649: aastore
    //   3650: dup
    //   3651: iconst_2
    //   3652: ldc_w ''
    //   3655: aastore
    //   3656: dup
    //   3657: iconst_3
    //   3658: iload #6
    //   3660: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3663: aastore
    //   3664: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3667: aload_1
    //   3668: invokevirtual getSummaryCode : ()I
    //   3671: istore #23
    //   3673: aload_1
    //   3674: invokevirtual getSummaryCodeRss : ()I
    //   3677: istore #6
    //   3679: aload_0
    //   3680: ldc '%21s %8d %21s %8d'
    //   3682: iconst_4
    //   3683: anewarray java/lang/Object
    //   3686: dup
    //   3687: iconst_0
    //   3688: ldc_w 'Code:'
    //   3691: aastore
    //   3692: dup
    //   3693: iconst_1
    //   3694: iload #23
    //   3696: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3699: aastore
    //   3700: dup
    //   3701: iconst_2
    //   3702: ldc_w ''
    //   3705: aastore
    //   3706: dup
    //   3707: iconst_3
    //   3708: iload #6
    //   3710: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3713: aastore
    //   3714: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3717: aload_1
    //   3718: invokevirtual getSummaryStack : ()I
    //   3721: istore #6
    //   3723: aload_1
    //   3724: invokevirtual getSummaryStackRss : ()I
    //   3727: istore #23
    //   3729: aload_0
    //   3730: ldc '%21s %8d %21s %8d'
    //   3732: iconst_4
    //   3733: anewarray java/lang/Object
    //   3736: dup
    //   3737: iconst_0
    //   3738: ldc_w 'Stack:'
    //   3741: aastore
    //   3742: dup
    //   3743: iconst_1
    //   3744: iload #6
    //   3746: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3749: aastore
    //   3750: dup
    //   3751: iconst_2
    //   3752: ldc_w ''
    //   3755: aastore
    //   3756: dup
    //   3757: iconst_3
    //   3758: iload #23
    //   3760: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3763: aastore
    //   3764: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3767: aload_1
    //   3768: invokevirtual getSummaryGraphics : ()I
    //   3771: istore #23
    //   3773: aload_1
    //   3774: invokevirtual getSummaryGraphicsRss : ()I
    //   3777: istore #6
    //   3779: aload_0
    //   3780: ldc '%21s %8d %21s %8d'
    //   3782: iconst_4
    //   3783: anewarray java/lang/Object
    //   3786: dup
    //   3787: iconst_0
    //   3788: ldc_w 'Graphics:'
    //   3791: aastore
    //   3792: dup
    //   3793: iconst_1
    //   3794: iload #23
    //   3796: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3799: aastore
    //   3800: dup
    //   3801: iconst_2
    //   3802: ldc_w ''
    //   3805: aastore
    //   3806: dup
    //   3807: iconst_3
    //   3808: iload #6
    //   3810: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3813: aastore
    //   3814: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3817: aload_1
    //   3818: invokevirtual getSummaryPrivateOther : ()I
    //   3821: istore #6
    //   3823: aload_0
    //   3824: ldc '%21s %8d'
    //   3826: iconst_2
    //   3827: anewarray java/lang/Object
    //   3830: dup
    //   3831: iconst_0
    //   3832: ldc_w 'Private Other:'
    //   3835: aastore
    //   3836: dup
    //   3837: iconst_1
    //   3838: iload #6
    //   3840: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3843: aastore
    //   3844: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3847: aload_1
    //   3848: invokevirtual getSummarySystem : ()I
    //   3851: istore #6
    //   3853: aload_0
    //   3854: ldc '%21s %8d'
    //   3856: iconst_2
    //   3857: anewarray java/lang/Object
    //   3860: dup
    //   3861: iconst_0
    //   3862: ldc_w 'System:'
    //   3865: aastore
    //   3866: dup
    //   3867: iconst_1
    //   3868: iload #6
    //   3870: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3873: aastore
    //   3874: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3877: aload_1
    //   3878: invokevirtual getSummaryUnknownRss : ()I
    //   3881: istore #6
    //   3883: aload_0
    //   3884: ldc '%21s %8s %21s %8d'
    //   3886: iconst_4
    //   3887: anewarray java/lang/Object
    //   3890: dup
    //   3891: iconst_0
    //   3892: ldc_w 'Unknown:'
    //   3895: aastore
    //   3896: dup
    //   3897: iconst_1
    //   3898: ldc_w ''
    //   3901: aastore
    //   3902: dup
    //   3903: iconst_2
    //   3904: ldc_w ''
    //   3907: aastore
    //   3908: dup
    //   3909: iconst_3
    //   3910: iload #6
    //   3912: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3915: aastore
    //   3916: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   3919: aload_0
    //   3920: ldc_w ' '
    //   3923: invokevirtual println : (Ljava/lang/String;)V
    //   3926: aload_1
    //   3927: getfield hasSwappedOutPss : Z
    //   3930: ifeq -> 4007
    //   3933: aload_1
    //   3934: invokevirtual getSummaryTotalPss : ()I
    //   3937: istore #23
    //   3939: aload_1
    //   3940: invokevirtual getTotalRss : ()I
    //   3943: istore #22
    //   3945: aload_1
    //   3946: invokevirtual getSummaryTotalSwapPss : ()I
    //   3949: istore #6
    //   3951: aload_0
    //   3952: ldc '%21s %8d %21s %8s %21s %8d'
    //   3954: bipush #6
    //   3956: anewarray java/lang/Object
    //   3959: dup
    //   3960: iconst_0
    //   3961: ldc_w 'TOTAL PSS:'
    //   3964: aastore
    //   3965: dup
    //   3966: iconst_1
    //   3967: iload #23
    //   3969: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3972: aastore
    //   3973: dup
    //   3974: iconst_2
    //   3975: ldc_w 'TOTAL RSS:'
    //   3978: aastore
    //   3979: dup
    //   3980: iconst_3
    //   3981: iload #22
    //   3983: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   3986: aastore
    //   3987: dup
    //   3988: iconst_4
    //   3989: ldc_w 'TOTAL SWAP PSS:'
    //   3992: aastore
    //   3993: dup
    //   3994: iconst_5
    //   3995: iload #6
    //   3997: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   4000: aastore
    //   4001: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   4004: goto -> 4078
    //   4007: aload_1
    //   4008: invokevirtual getSummaryTotalPss : ()I
    //   4011: istore #22
    //   4013: aload_1
    //   4014: invokevirtual getTotalRss : ()I
    //   4017: istore #6
    //   4019: aload_1
    //   4020: invokevirtual getSummaryTotalSwap : ()I
    //   4023: istore #23
    //   4025: aload_0
    //   4026: ldc '%21s %8d %21s %8s %21s %8d'
    //   4028: bipush #6
    //   4030: anewarray java/lang/Object
    //   4033: dup
    //   4034: iconst_0
    //   4035: ldc_w 'TOTAL PSS:'
    //   4038: aastore
    //   4039: dup
    //   4040: iconst_1
    //   4041: iload #22
    //   4043: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   4046: aastore
    //   4047: dup
    //   4048: iconst_2
    //   4049: ldc_w 'TOTAL RSS:'
    //   4052: aastore
    //   4053: dup
    //   4054: iconst_3
    //   4055: iload #6
    //   4057: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   4060: aastore
    //   4061: dup
    //   4062: iconst_4
    //   4063: ldc_w 'TOTAL SWAP (KB):'
    //   4066: aastore
    //   4067: dup
    //   4068: iconst_5
    //   4069: iload #23
    //   4071: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   4074: aastore
    //   4075: invokestatic printRow : (Ljava/io/PrintWriter;Ljava/lang/String;[Ljava/lang/Object;)V
    //   4078: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2805	-> 0
    //   #2810	-> 4
    //   #2811	-> 15
    //   #2812	-> 27
    //   #2815	-> 39
    //   #2816	-> 51
    //   #2817	-> 63
    //   #2818	-> 70
    //   #2821	-> 85
    //   #2822	-> 97
    //   #2823	-> 109
    //   #2824	-> 116
    //   #2827	-> 131
    //   #2828	-> 143
    //   #2829	-> 155
    //   #2830	-> 162
    //   #2833	-> 177
    //   #2834	-> 191
    //   #2835	-> 205
    //   #2836	-> 219
    //   #2839	-> 233
    //   #2840	-> 247
    //   #2841	-> 261
    //   #2842	-> 275
    //   #2845	-> 289
    //   #2846	-> 303
    //   #2847	-> 317
    //   #2848	-> 331
    //   #2851	-> 345
    //   #2852	-> 359
    //   #2853	-> 373
    //   #2854	-> 387
    //   #2857	-> 401
    //   #2858	-> 415
    //   #2859	-> 429
    //   #2860	-> 443
    //   #2863	-> 457
    //   #2864	-> 471
    //   #2865	-> 485
    //   #2866	-> 499
    //   #2869	-> 513
    //   #2870	-> 527
    //   #2871	-> 541
    //   #2872	-> 555
    //   #2875	-> 569
    //   #2876	-> 576
    //   #2877	-> 590
    //   #2878	-> 604
    //   #2879	-> 618
    //   #2881	-> 635
    //   #2882	-> 642
    //   #2883	-> 649
    //   #2884	-> 656
    //   #2888	-> 663
    //   #2889	-> 673
    //   #2890	-> 688
    //   #2891	-> 704
    //   #2892	-> 720
    //   #2893	-> 736
    //   #2894	-> 752
    //   #2895	-> 768
    //   #2896	-> 784
    //   #2897	-> 800
    //   #2898	-> 807
    //   #2900	-> 826
    //   #2888	-> 833
    //   #2903	-> 839
    //   #2906	-> 840
    //   #2907	-> 845
    //   #2908	-> 849
    //   #2909	-> 849
    //   #2908	-> 869
    //   #2911	-> 957
    //   #2914	-> 1046
    //   #2916	-> 1135
    //   #2917	-> 1147
    //   #2918	-> 1159
    //   #2919	-> 1171
    //   #2920	-> 1178
    //   #2919	-> 1193
    //   #2921	-> 1199
    //   #2916	-> 1199
    //   #2922	-> 1310
    //   #2923	-> 1322
    //   #2924	-> 1334
    //   #2925	-> 1346
    //   #2926	-> 1353
    //   #2925	-> 1368
    //   #2927	-> 1374
    //   #2922	-> 1374
    //   #2929	-> 1488
    //   #2930	-> 1488
    //   #2929	-> 1508
    //   #2932	-> 1575
    //   #2934	-> 1643
    //   #2936	-> 1718
    //   #2937	-> 1730
    //   #2938	-> 1736
    //   #2939	-> 1736
    //   #2940	-> 1752
    //   #2939	-> 1758
    //   #2940	-> 1764
    //   #2941	-> 1764
    //   #2936	-> 1764
    //   #2942	-> 1848
    //   #2943	-> 1860
    //   #2944	-> 1866
    //   #2945	-> 1866
    //   #2946	-> 1882
    //   #2945	-> 1888
    //   #2946	-> 1894
    //   #2947	-> 1894
    //   #2942	-> 1894
    //   #2950	-> 1978
    //   #2951	-> 1984
    //   #2952	-> 1990
    //   #2953	-> 1996
    //   #2954	-> 2002
    //   #2955	-> 2008
    //   #2956	-> 2014
    //   #2957	-> 2020
    //   #2958	-> 2026
    //   #2960	-> 2032
    //   #2961	-> 2042
    //   #2962	-> 2050
    //   #2963	-> 2058
    //   #2964	-> 2066
    //   #2965	-> 2074
    //   #2966	-> 2082
    //   #2967	-> 2090
    //   #2968	-> 2098
    //   #2969	-> 2106
    //   #2970	-> 2114
    //   #2972	-> 2144
    //   #2973	-> 2203
    //   #2974	-> 2207
    //   #2975	-> 2214
    //   #2976	-> 2214
    //   #2977	-> 2214
    //   #2978	-> 2232
    //   #2974	-> 2232
    //   #2980	-> 2339
    //   #2981	-> 2346
    //   #2982	-> 2346
    //   #2983	-> 2346
    //   #2984	-> 2364
    //   #2980	-> 2364
    //   #2986	-> 2441
    //   #2987	-> 2448
    //   #2988	-> 2455
    //   #2989	-> 2462
    //   #2990	-> 2469
    //   #2991	-> 2476
    //   #2992	-> 2483
    //   #2993	-> 2490
    //   #2994	-> 2497
    //   #2960	-> 2504
    //   #2998	-> 2546
    //   #2999	-> 2550
    //   #3000	-> 2550
    //   #3001	-> 2550
    //   #3002	-> 2564
    //   #2999	-> 2564
    //   #3003	-> 2669
    //   #3004	-> 2675
    //   #3005	-> 2681
    //   #3006	-> 2693
    //   #3007	-> 2705
    //   #3008	-> 2721
    //   #3007	-> 2727
    //   #3008	-> 2727
    //   #3009	-> 2733
    //   #3010	-> 2733
    //   #3003	-> 2733
    //   #3012	-> 2856
    //   #3013	-> 2856
    //   #3014	-> 2856
    //   #3015	-> 2870
    //   #3012	-> 2870
    //   #3016	-> 2948
    //   #3017	-> 2954
    //   #3018	-> 2960
    //   #3019	-> 2966
    //   #3020	-> 2982
    //   #3019	-> 2988
    //   #3020	-> 2988
    //   #3021	-> 2994
    //   #3022	-> 2994
    //   #3016	-> 2994
    //   #3025	-> 3087
    //   #3026	-> 3092
    //   #3027	-> 3099
    //   #3029	-> 3106
    //   #3030	-> 3110
    //   #3031	-> 3117
    //   #3032	-> 3125
    //   #3033	-> 3133
    //   #3034	-> 3141
    //   #3035	-> 3149
    //   #3036	-> 3157
    //   #3037	-> 3165
    //   #3038	-> 3173
    //   #3039	-> 3181
    //   #3040	-> 3189
    //   #3042	-> 3214
    //   #3040	-> 3243
    //   #3043	-> 3243
    //   #3044	-> 3247
    //   #3045	-> 3254
    //   #3046	-> 3254
    //   #3047	-> 3254
    //   #3048	-> 3268
    //   #3044	-> 3268
    //   #3050	-> 3375
    //   #3051	-> 3382
    //   #3052	-> 3382
    //   #3053	-> 3382
    //   #3054	-> 3396
    //   #3050	-> 3396
    //   #3030	-> 3473
    //   #3025	-> 3482
    //   #2906	-> 3485
    //   #3061	-> 3485
    //   #3062	-> 3492
    //   #3063	-> 3499
    //   #3064	-> 3533
    //   #3065	-> 3567
    //   #3066	-> 3567
    //   #3065	-> 3579
    //   #3067	-> 3617
    //   #3068	-> 3617
    //   #3069	-> 3623
    //   #3067	-> 3629
    //   #3070	-> 3667
    //   #3071	-> 3667
    //   #3070	-> 3679
    //   #3072	-> 3717
    //   #3073	-> 3717
    //   #3072	-> 3729
    //   #3074	-> 3767
    //   #3075	-> 3767
    //   #3074	-> 3779
    //   #3076	-> 3817
    //   #3077	-> 3817
    //   #3076	-> 3823
    //   #3078	-> 3847
    //   #3079	-> 3847
    //   #3078	-> 3853
    //   #3080	-> 3877
    //   #3081	-> 3877
    //   #3080	-> 3883
    //   #3082	-> 3919
    //   #3083	-> 3926
    //   #3084	-> 3933
    //   #3085	-> 3933
    //   #3086	-> 3939
    //   #3087	-> 3945
    //   #3084	-> 3951
    //   #3089	-> 4007
    //   #3090	-> 4007
    //   #3091	-> 4013
    //   #3092	-> 4019
    //   #3089	-> 4025
    //   #3094	-> 4078
  }
  
  private static void dumpMemoryInfo(ProtoOutputStream paramProtoOutputStream, long paramLong, String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, int paramInt7, int paramInt8, int paramInt9) {
    paramLong = paramProtoOutputStream.start(paramLong);
    paramProtoOutputStream.write(1138166333441L, paramString);
    paramProtoOutputStream.write(1120986464258L, paramInt1);
    paramProtoOutputStream.write(1120986464259L, paramInt2);
    paramProtoOutputStream.write(1120986464260L, paramInt3);
    paramProtoOutputStream.write(1120986464261L, paramInt4);
    paramProtoOutputStream.write(1120986464262L, paramInt5);
    paramProtoOutputStream.write(1120986464263L, paramInt6);
    if (paramBoolean) {
      paramProtoOutputStream.write(1120986464265L, paramInt8);
    } else {
      paramProtoOutputStream.write(1120986464264L, paramInt7);
    } 
    paramProtoOutputStream.write(1120986464266L, paramInt9);
    paramProtoOutputStream.end(paramLong);
  }
  
  public static void dumpMemInfoTable(ProtoOutputStream paramProtoOutputStream, Debug.MemoryInfo paramMemoryInfo, boolean paramBoolean1, boolean paramBoolean2, long paramLong1, long paramLong2, long paramLong3, long paramLong4, long paramLong5, long paramLong6) {
    // Byte code:
    //   0: iload_3
    //   1: ifne -> 895
    //   4: aload_0
    //   5: ldc2_w 1146756268035
    //   8: invokevirtual start : (J)J
    //   11: lstore #16
    //   13: aload_0
    //   14: ldc2_w 1146756268033
    //   17: ldc_w 'Native Heap'
    //   20: aload_1
    //   21: getfield nativePss : I
    //   24: aload_1
    //   25: getfield nativeSwappablePss : I
    //   28: aload_1
    //   29: getfield nativeSharedDirty : I
    //   32: aload_1
    //   33: getfield nativePrivateDirty : I
    //   36: aload_1
    //   37: getfield nativeSharedClean : I
    //   40: aload_1
    //   41: getfield nativePrivateClean : I
    //   44: aload_1
    //   45: getfield hasSwappedOutPss : Z
    //   48: aload_1
    //   49: getfield nativeSwappedOut : I
    //   52: aload_1
    //   53: getfield nativeSwappedOutPss : I
    //   56: aload_1
    //   57: getfield nativeRss : I
    //   60: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   63: aload_0
    //   64: ldc2_w 1120986464258
    //   67: lload #4
    //   69: invokevirtual write : (JJ)V
    //   72: aload_0
    //   73: ldc2_w 1120986464259
    //   76: lload #6
    //   78: invokevirtual write : (JJ)V
    //   81: aload_0
    //   82: ldc2_w 1120986464260
    //   85: lload #8
    //   87: invokevirtual write : (JJ)V
    //   90: aload_0
    //   91: lload #16
    //   93: invokevirtual end : (J)V
    //   96: aload_0
    //   97: ldc2_w 1146756268036
    //   100: invokevirtual start : (J)J
    //   103: lstore #16
    //   105: aload_1
    //   106: getfield dalvikPss : I
    //   109: istore #18
    //   111: aload_1
    //   112: getfield dalvikSwappablePss : I
    //   115: istore #19
    //   117: aload_1
    //   118: getfield dalvikSharedDirty : I
    //   121: istore #20
    //   123: aload_1
    //   124: getfield dalvikPrivateDirty : I
    //   127: istore #21
    //   129: aload_1
    //   130: getfield dalvikSharedClean : I
    //   133: istore #22
    //   135: aload_1
    //   136: getfield dalvikPrivateClean : I
    //   139: istore #23
    //   141: aload_1
    //   142: getfield hasSwappedOutPss : Z
    //   145: istore_3
    //   146: aload_1
    //   147: getfield dalvikSwappedOut : I
    //   150: istore #24
    //   152: aload_1
    //   153: getfield dalvikSwappedOutPss : I
    //   156: istore #25
    //   158: aload_1
    //   159: getfield dalvikRss : I
    //   162: istore #26
    //   164: aload_0
    //   165: ldc2_w 1146756268033
    //   168: ldc_w 'Dalvik Heap'
    //   171: iload #18
    //   173: iload #19
    //   175: iload #20
    //   177: iload #21
    //   179: iload #22
    //   181: iload #23
    //   183: iload_3
    //   184: iload #24
    //   186: iload #25
    //   188: iload #26
    //   190: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   193: aload_0
    //   194: ldc2_w 1120986464258
    //   197: lload #10
    //   199: invokevirtual write : (JJ)V
    //   202: aload_0
    //   203: ldc2_w 1120986464259
    //   206: lload #12
    //   208: invokevirtual write : (JJ)V
    //   211: aload_0
    //   212: ldc2_w 1120986464260
    //   215: lload #14
    //   217: invokevirtual write : (JJ)V
    //   220: aload_0
    //   221: lload #16
    //   223: invokevirtual end : (J)V
    //   226: aload_1
    //   227: getfield otherPss : I
    //   230: istore #27
    //   232: aload_1
    //   233: getfield otherSwappablePss : I
    //   236: istore #22
    //   238: aload_1
    //   239: getfield otherSharedDirty : I
    //   242: istore #25
    //   244: aload_1
    //   245: getfield otherPrivateDirty : I
    //   248: istore #18
    //   250: aload_1
    //   251: getfield otherSharedClean : I
    //   254: istore #26
    //   256: aload_1
    //   257: getfield otherPrivateClean : I
    //   260: istore #20
    //   262: aload_1
    //   263: getfield otherSwappedOut : I
    //   266: istore #21
    //   268: aload_1
    //   269: getfield otherSwappedOutPss : I
    //   272: istore #24
    //   274: aload_1
    //   275: getfield otherRss : I
    //   278: istore #23
    //   280: iconst_0
    //   281: istore #19
    //   283: iload #19
    //   285: bipush #17
    //   287: if_icmpge -> 530
    //   290: aload_1
    //   291: iload #19
    //   293: invokevirtual getOtherPss : (I)I
    //   296: istore #28
    //   298: aload_1
    //   299: iload #19
    //   301: invokevirtual getOtherSwappablePss : (I)I
    //   304: istore #29
    //   306: aload_1
    //   307: iload #19
    //   309: invokevirtual getOtherSharedDirty : (I)I
    //   312: istore #30
    //   314: aload_1
    //   315: iload #19
    //   317: invokevirtual getOtherPrivateDirty : (I)I
    //   320: istore #31
    //   322: aload_1
    //   323: iload #19
    //   325: invokevirtual getOtherSharedClean : (I)I
    //   328: istore #32
    //   330: aload_1
    //   331: iload #19
    //   333: invokevirtual getOtherPrivateClean : (I)I
    //   336: istore #33
    //   338: aload_1
    //   339: iload #19
    //   341: invokevirtual getOtherSwappedOut : (I)I
    //   344: istore #34
    //   346: aload_1
    //   347: iload #19
    //   349: invokevirtual getOtherSwappedOutPss : (I)I
    //   352: istore #35
    //   354: aload_1
    //   355: iload #19
    //   357: invokevirtual getOtherRss : (I)I
    //   360: istore #36
    //   362: iload #28
    //   364: ifne -> 421
    //   367: iload #30
    //   369: ifne -> 421
    //   372: iload #31
    //   374: ifne -> 421
    //   377: iload #32
    //   379: ifne -> 421
    //   382: iload #33
    //   384: ifne -> 421
    //   387: iload #36
    //   389: ifne -> 421
    //   392: aload_1
    //   393: getfield hasSwappedOutPss : Z
    //   396: ifeq -> 406
    //   399: iload #35
    //   401: istore #37
    //   403: goto -> 410
    //   406: iload #34
    //   408: istore #37
    //   410: iload #37
    //   412: ifeq -> 418
    //   415: goto -> 421
    //   418: goto -> 524
    //   421: iload #19
    //   423: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   426: astore #38
    //   428: aload_1
    //   429: getfield hasSwappedOutPss : Z
    //   432: istore_3
    //   433: aload_0
    //   434: ldc2_w 2246267895813
    //   437: aload #38
    //   439: iload #28
    //   441: iload #29
    //   443: iload #30
    //   445: iload #31
    //   447: iload #32
    //   449: iload #33
    //   451: iload_3
    //   452: iload #34
    //   454: iload #35
    //   456: iload #36
    //   458: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   461: iload #27
    //   463: iload #28
    //   465: isub
    //   466: istore #27
    //   468: iload #22
    //   470: iload #29
    //   472: isub
    //   473: istore #22
    //   475: iload #25
    //   477: iload #30
    //   479: isub
    //   480: istore #25
    //   482: iload #18
    //   484: iload #31
    //   486: isub
    //   487: istore #18
    //   489: iload #26
    //   491: iload #32
    //   493: isub
    //   494: istore #26
    //   496: iload #20
    //   498: iload #33
    //   500: isub
    //   501: istore #20
    //   503: iload #21
    //   505: iload #34
    //   507: isub
    //   508: istore #21
    //   510: iload #24
    //   512: iload #35
    //   514: isub
    //   515: istore #24
    //   517: iload #23
    //   519: iload #36
    //   521: isub
    //   522: istore #23
    //   524: iinc #19, 1
    //   527: goto -> 283
    //   530: aload_0
    //   531: ldc2_w 1146756268038
    //   534: ldc_w 'Unknown'
    //   537: iload #27
    //   539: iload #22
    //   541: iload #25
    //   543: iload #18
    //   545: iload #26
    //   547: iload #20
    //   549: aload_1
    //   550: getfield hasSwappedOutPss : Z
    //   553: iload #21
    //   555: iload #24
    //   557: iload #23
    //   559: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   562: aload_0
    //   563: ldc2_w 1146756268039
    //   566: invokevirtual start : (J)J
    //   569: lstore #16
    //   571: aload_1
    //   572: invokevirtual getTotalPss : ()I
    //   575: istore #22
    //   577: aload_1
    //   578: invokevirtual getTotalSwappablePss : ()I
    //   581: istore #26
    //   583: aload_1
    //   584: invokevirtual getTotalSharedDirty : ()I
    //   587: istore #18
    //   589: aload_1
    //   590: invokevirtual getTotalPrivateDirty : ()I
    //   593: istore #24
    //   595: aload_1
    //   596: invokevirtual getTotalSharedClean : ()I
    //   599: istore #21
    //   601: aload_1
    //   602: invokevirtual getTotalPrivateClean : ()I
    //   605: istore #25
    //   607: aload_1
    //   608: getfield hasSwappedOutPss : Z
    //   611: istore_3
    //   612: aload_1
    //   613: invokevirtual getTotalSwappedOut : ()I
    //   616: istore #19
    //   618: aload_1
    //   619: invokevirtual getTotalSwappedOutPss : ()I
    //   622: istore #20
    //   624: aload_1
    //   625: invokevirtual getTotalRss : ()I
    //   628: istore #23
    //   630: aload_0
    //   631: ldc2_w 1146756268033
    //   634: ldc_w 'TOTAL'
    //   637: iload #22
    //   639: iload #26
    //   641: iload #18
    //   643: iload #24
    //   645: iload #21
    //   647: iload #25
    //   649: iload_3
    //   650: iload #19
    //   652: iload #20
    //   654: iload #23
    //   656: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   659: aload_0
    //   660: ldc2_w 1120986464258
    //   663: lload #4
    //   665: lload #10
    //   667: ladd
    //   668: invokevirtual write : (JJ)V
    //   671: aload_0
    //   672: ldc2_w 1120986464259
    //   675: lload #6
    //   677: lload #12
    //   679: ladd
    //   680: invokevirtual write : (JJ)V
    //   683: aload_0
    //   684: ldc2_w 1120986464260
    //   687: lload #8
    //   689: lload #14
    //   691: ladd
    //   692: invokevirtual write : (JJ)V
    //   695: lload #16
    //   697: lstore #4
    //   699: aload_0
    //   700: lload #4
    //   702: invokevirtual end : (J)V
    //   705: iload_2
    //   706: ifeq -> 895
    //   709: bipush #17
    //   711: istore #20
    //   713: iload #20
    //   715: bipush #32
    //   717: if_icmpge -> 892
    //   720: aload_1
    //   721: iload #20
    //   723: invokevirtual getOtherPss : (I)I
    //   726: istore #21
    //   728: aload_1
    //   729: iload #20
    //   731: invokevirtual getOtherSwappablePss : (I)I
    //   734: istore #27
    //   736: aload_1
    //   737: iload #20
    //   739: invokevirtual getOtherSharedDirty : (I)I
    //   742: istore #25
    //   744: aload_1
    //   745: iload #20
    //   747: invokevirtual getOtherPrivateDirty : (I)I
    //   750: istore #22
    //   752: aload_1
    //   753: iload #20
    //   755: invokevirtual getOtherSharedClean : (I)I
    //   758: istore #19
    //   760: aload_1
    //   761: iload #20
    //   763: invokevirtual getOtherPrivateClean : (I)I
    //   766: istore #24
    //   768: aload_1
    //   769: iload #20
    //   771: invokevirtual getOtherSwappedOut : (I)I
    //   774: istore #23
    //   776: aload_1
    //   777: iload #20
    //   779: invokevirtual getOtherSwappedOutPss : (I)I
    //   782: istore #18
    //   784: aload_1
    //   785: iload #20
    //   787: invokevirtual getOtherRss : (I)I
    //   790: istore #37
    //   792: iload #21
    //   794: ifne -> 846
    //   797: iload #25
    //   799: ifne -> 846
    //   802: iload #22
    //   804: ifne -> 846
    //   807: iload #19
    //   809: ifne -> 846
    //   812: iload #24
    //   814: ifne -> 846
    //   817: aload_1
    //   818: getfield hasSwappedOutPss : Z
    //   821: ifeq -> 831
    //   824: iload #18
    //   826: istore #26
    //   828: goto -> 835
    //   831: iload #23
    //   833: istore #26
    //   835: iload #26
    //   837: ifeq -> 843
    //   840: goto -> 846
    //   843: goto -> 886
    //   846: iload #20
    //   848: invokestatic getOtherLabel : (I)Ljava/lang/String;
    //   851: astore #38
    //   853: aload_1
    //   854: getfield hasSwappedOutPss : Z
    //   857: istore_2
    //   858: aload_0
    //   859: ldc2_w 2246267895816
    //   862: aload #38
    //   864: iload #21
    //   866: iload #27
    //   868: iload #25
    //   870: iload #22
    //   872: iload #19
    //   874: iload #24
    //   876: iload_2
    //   877: iload #23
    //   879: iload #18
    //   881: iload #37
    //   883: invokestatic dumpMemoryInfo : (Landroid/util/proto/ProtoOutputStream;JLjava/lang/String;IIIIIIZIII)V
    //   886: iinc #20, 1
    //   889: goto -> 713
    //   892: goto -> 895
    //   895: aload_0
    //   896: ldc2_w 1146756268041
    //   899: invokevirtual start : (J)J
    //   902: lstore #4
    //   904: aload_1
    //   905: invokevirtual getSummaryJavaHeap : ()I
    //   908: istore #20
    //   910: aload_0
    //   911: ldc2_w 1120986464257
    //   914: iload #20
    //   916: invokevirtual write : (JI)V
    //   919: aload_1
    //   920: invokevirtual getSummaryNativeHeap : ()I
    //   923: istore #20
    //   925: aload_0
    //   926: ldc2_w 1120986464258
    //   929: iload #20
    //   931: invokevirtual write : (JI)V
    //   934: aload_1
    //   935: invokevirtual getSummaryCode : ()I
    //   938: istore #20
    //   940: aload_0
    //   941: ldc2_w 1120986464259
    //   944: iload #20
    //   946: invokevirtual write : (JI)V
    //   949: aload_1
    //   950: invokevirtual getSummaryStack : ()I
    //   953: istore #20
    //   955: aload_0
    //   956: ldc2_w 1120986464260
    //   959: iload #20
    //   961: invokevirtual write : (JI)V
    //   964: aload_1
    //   965: invokevirtual getSummaryGraphics : ()I
    //   968: istore #20
    //   970: aload_0
    //   971: ldc2_w 1120986464261
    //   974: iload #20
    //   976: invokevirtual write : (JI)V
    //   979: aload_1
    //   980: invokevirtual getSummaryPrivateOther : ()I
    //   983: istore #20
    //   985: aload_0
    //   986: ldc2_w 1120986464262
    //   989: iload #20
    //   991: invokevirtual write : (JI)V
    //   994: aload_1
    //   995: invokevirtual getSummarySystem : ()I
    //   998: istore #20
    //   1000: aload_0
    //   1001: ldc2_w 1120986464263
    //   1004: iload #20
    //   1006: invokevirtual write : (JI)V
    //   1009: aload_1
    //   1010: getfield hasSwappedOutPss : Z
    //   1013: ifeq -> 1034
    //   1016: aload_1
    //   1017: invokevirtual getSummaryTotalSwapPss : ()I
    //   1020: istore #20
    //   1022: aload_0
    //   1023: ldc2_w 1120986464264
    //   1026: iload #20
    //   1028: invokevirtual write : (JI)V
    //   1031: goto -> 1049
    //   1034: aload_1
    //   1035: invokevirtual getSummaryTotalSwap : ()I
    //   1038: istore #20
    //   1040: aload_0
    //   1041: ldc2_w 1120986464264
    //   1044: iload #20
    //   1046: invokevirtual write : (JI)V
    //   1049: aload_1
    //   1050: invokevirtual getSummaryJavaHeapRss : ()I
    //   1053: istore #20
    //   1055: aload_0
    //   1056: ldc2_w 1120986464266
    //   1059: iload #20
    //   1061: invokevirtual write : (JI)V
    //   1064: aload_1
    //   1065: invokevirtual getSummaryNativeHeapRss : ()I
    //   1068: istore #20
    //   1070: aload_0
    //   1071: ldc2_w 1120986464267
    //   1074: iload #20
    //   1076: invokevirtual write : (JI)V
    //   1079: aload_1
    //   1080: invokevirtual getSummaryCodeRss : ()I
    //   1083: istore #20
    //   1085: aload_0
    //   1086: ldc2_w 1120986464268
    //   1089: iload #20
    //   1091: invokevirtual write : (JI)V
    //   1094: aload_1
    //   1095: invokevirtual getSummaryStackRss : ()I
    //   1098: istore #20
    //   1100: aload_0
    //   1101: ldc2_w 1120986464269
    //   1104: iload #20
    //   1106: invokevirtual write : (JI)V
    //   1109: aload_1
    //   1110: invokevirtual getSummaryGraphicsRss : ()I
    //   1113: istore #20
    //   1115: aload_0
    //   1116: ldc2_w 1120986464270
    //   1119: iload #20
    //   1121: invokevirtual write : (JI)V
    //   1124: aload_1
    //   1125: invokevirtual getSummaryUnknownRss : ()I
    //   1128: istore #20
    //   1130: aload_0
    //   1131: ldc2_w 1120986464271
    //   1134: iload #20
    //   1136: invokevirtual write : (JI)V
    //   1139: aload_0
    //   1140: lload #4
    //   1142: invokevirtual end : (J)V
    //   1145: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3132	-> 0
    //   #3133	-> 4
    //   #3134	-> 13
    //   #3140	-> 63
    //   #3141	-> 72
    //   #3142	-> 81
    //   #3143	-> 90
    //   #3145	-> 96
    //   #3146	-> 105
    //   #3152	-> 193
    //   #3153	-> 202
    //   #3154	-> 211
    //   #3155	-> 220
    //   #3157	-> 226
    //   #3158	-> 232
    //   #3159	-> 238
    //   #3160	-> 244
    //   #3161	-> 250
    //   #3162	-> 256
    //   #3163	-> 262
    //   #3164	-> 268
    //   #3165	-> 274
    //   #3167	-> 280
    //   #3168	-> 290
    //   #3169	-> 298
    //   #3170	-> 306
    //   #3171	-> 314
    //   #3172	-> 322
    //   #3173	-> 330
    //   #3174	-> 338
    //   #3175	-> 346
    //   #3176	-> 354
    //   #3177	-> 362
    //   #3179	-> 392
    //   #3180	-> 421
    //   #3181	-> 421
    //   #3180	-> 433
    //   #3186	-> 461
    //   #3187	-> 468
    //   #3188	-> 475
    //   #3189	-> 482
    //   #3190	-> 489
    //   #3191	-> 496
    //   #3192	-> 503
    //   #3193	-> 510
    //   #3194	-> 517
    //   #3167	-> 524
    //   #3198	-> 530
    //   #3202	-> 562
    //   #3203	-> 571
    //   #3204	-> 571
    //   #3205	-> 583
    //   #3206	-> 595
    //   #3207	-> 612
    //   #3208	-> 618
    //   #3203	-> 630
    //   #3209	-> 659
    //   #3211	-> 671
    //   #3213	-> 683
    //   #3215	-> 695
    //   #3217	-> 705
    //   #3218	-> 709
    //   #3219	-> 713
    //   #3221	-> 720
    //   #3222	-> 728
    //   #3223	-> 736
    //   #3224	-> 744
    //   #3225	-> 752
    //   #3226	-> 760
    //   #3227	-> 768
    //   #3228	-> 776
    //   #3229	-> 784
    //   #3230	-> 792
    //   #3232	-> 817
    //   #3233	-> 846
    //   #3234	-> 846
    //   #3233	-> 858
    //   #3220	-> 886
    //   #3219	-> 892
    //   #3217	-> 895
    //   #3243	-> 895
    //   #3244	-> 904
    //   #3245	-> 904
    //   #3244	-> 910
    //   #3246	-> 919
    //   #3247	-> 919
    //   #3246	-> 925
    //   #3248	-> 934
    //   #3249	-> 934
    //   #3248	-> 940
    //   #3250	-> 949
    //   #3251	-> 949
    //   #3250	-> 955
    //   #3252	-> 964
    //   #3253	-> 964
    //   #3252	-> 970
    //   #3254	-> 979
    //   #3255	-> 979
    //   #3254	-> 985
    //   #3256	-> 994
    //   #3257	-> 994
    //   #3256	-> 1000
    //   #3258	-> 1009
    //   #3259	-> 1016
    //   #3260	-> 1016
    //   #3259	-> 1022
    //   #3262	-> 1034
    //   #3263	-> 1034
    //   #3262	-> 1040
    //   #3265	-> 1049
    //   #3266	-> 1049
    //   #3265	-> 1055
    //   #3267	-> 1064
    //   #3268	-> 1064
    //   #3267	-> 1070
    //   #3269	-> 1079
    //   #3270	-> 1079
    //   #3269	-> 1085
    //   #3271	-> 1094
    //   #3272	-> 1094
    //   #3271	-> 1100
    //   #3273	-> 1109
    //   #3274	-> 1109
    //   #3273	-> 1115
    //   #3275	-> 1124
    //   #3276	-> 1124
    //   #3275	-> 1130
    //   #3278	-> 1139
    //   #3279	-> 1145
  }
  
  public void registerOnActivityPausedListener(Activity paramActivity, OnActivityPausedListener paramOnActivityPausedListener) {
    synchronized (this.mOnPauseListeners) {
      ArrayList<OnActivityPausedListener> arrayList1 = (ArrayList)this.mOnPauseListeners.get(paramActivity);
      ArrayList<OnActivityPausedListener> arrayList2 = arrayList1;
      if (arrayList1 == null) {
        arrayList2 = new ArrayList();
        this();
        this.mOnPauseListeners.put(paramActivity, arrayList2);
      } 
      arrayList2.add(paramOnActivityPausedListener);
      return;
    } 
  }
  
  public void unregisterOnActivityPausedListener(Activity paramActivity, OnActivityPausedListener paramOnActivityPausedListener) {
    synchronized (this.mOnPauseListeners) {
      ArrayList arrayList = (ArrayList)this.mOnPauseListeners.get(paramActivity);
      if (arrayList != null)
        arrayList.remove(paramOnActivityPausedListener); 
      return;
    } 
  }
  
  public final ActivityInfo resolveActivityInfo(Intent paramIntent) {
    Application application = this.mInitialApplication;
    PackageManager packageManager = application.getPackageManager();
    ActivityInfo activityInfo = paramIntent.resolveActivityInfo(packageManager, 1024);
    if (activityInfo == null)
      Instrumentation.checkStartActivityResult(-92, paramIntent); 
    return activityInfo;
  }
  
  public final Activity startActivityNow(Activity paramActivity, String paramString, Intent paramIntent, ActivityInfo paramActivityInfo, IBinder paramIBinder1, Bundle paramBundle, Activity.NonConfigurationInstances paramNonConfigurationInstances, IBinder paramIBinder2) {
    ActivityClientRecord activityClientRecord = new ActivityClientRecord();
    activityClientRecord.token = paramIBinder1;
    activityClientRecord.assistToken = paramIBinder2;
    activityClientRecord.ident = 0;
    activityClientRecord.intent = paramIntent;
    activityClientRecord.state = paramBundle;
    activityClientRecord.parent = paramActivity;
    activityClientRecord.embeddedID = paramString;
    activityClientRecord.activityInfo = paramActivityInfo;
    activityClientRecord.lastNonConfigurationInstances = paramNonConfigurationInstances;
    if (localLOGV) {
      String str;
      ComponentName componentName = paramIntent.getComponent();
      if (componentName != null) {
        str = componentName.toShortString();
      } else {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("(Intent ");
        stringBuilder1.append(paramIntent);
        stringBuilder1.append(").getComponent() returned null");
        str = stringBuilder1.toString();
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Performing launch: action=");
      stringBuilder.append(paramIntent.getAction());
      stringBuilder.append(", comp=");
      stringBuilder.append(str);
      stringBuilder.append(", token=");
      stringBuilder.append(paramIBinder1);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    return performLaunchActivity(activityClientRecord, (Intent)null);
  }
  
  public final Activity getActivity(IBinder paramIBinder) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      Activity activity = activityClientRecord.activity;
    } else {
      activityClientRecord = null;
    } 
    return (Activity)activityClientRecord;
  }
  
  public ActivityClientRecord getActivityClient(IBinder paramIBinder) {
    return (ActivityClientRecord)this.mActivities.get(paramIBinder);
  }
  
  public Configuration getConfiguration() {
    return this.mConfiguration;
  }
  
  public void updatePendingConfiguration(Configuration paramConfiguration) {
    synchronized (this.mResourcesManager) {
      if (this.mPendingConfiguration == null || this.mPendingConfiguration.isOtherSeqNewer(paramConfiguration))
        this.mPendingConfiguration = paramConfiguration; 
      return;
    } 
  }
  
  public void updateProcessState(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAppThread : Landroid/app/ActivityThread$ApplicationThread;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mLastProcessState : I
    //   11: iload_1
    //   12: if_icmpne -> 18
    //   15: aload_3
    //   16: monitorexit
    //   17: return
    //   18: aload_0
    //   19: iload_1
    //   20: putfield mLastProcessState : I
    //   23: iload_1
    //   24: iconst_2
    //   25: if_icmpne -> 78
    //   28: aload_0
    //   29: getfield mNumLaunchingActivities : Ljava/util/concurrent/atomic/AtomicInteger;
    //   32: astore #4
    //   34: aload #4
    //   36: invokevirtual get : ()I
    //   39: ifle -> 78
    //   42: aload_0
    //   43: iload_1
    //   44: putfield mPendingProcessState : I
    //   47: aload_0
    //   48: getfield mH : Landroid/app/ActivityThread$H;
    //   51: astore #5
    //   53: new android/app/_$$Lambda$ActivityThread$A4ykhsPb8qV3ffTqpQDklHSMDJ0
    //   56: astore #4
    //   58: aload #4
    //   60: aload_0
    //   61: invokespecial <init> : (Landroid/app/ActivityThread;)V
    //   64: aload #5
    //   66: aload #4
    //   68: ldc2_w 1000
    //   71: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   74: pop
    //   75: goto -> 88
    //   78: aload_0
    //   79: iconst_m1
    //   80: putfield mPendingProcessState : I
    //   83: aload_0
    //   84: iload_1
    //   85: invokespecial updateVmProcessState : (I)V
    //   88: getstatic android/app/ActivityThread.localLOGV : Z
    //   91: ifeq -> 160
    //   94: new java/lang/StringBuilder
    //   97: astore #5
    //   99: aload #5
    //   101: invokespecial <init> : ()V
    //   104: aload #5
    //   106: ldc_w '******************* PROCESS STATE CHANGED TO: '
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #5
    //   115: iload_1
    //   116: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: iload_2
    //   121: ifeq -> 132
    //   124: ldc_w ' (from ipc'
    //   127: astore #4
    //   129: goto -> 137
    //   132: ldc_w ''
    //   135: astore #4
    //   137: aload #5
    //   139: aload #4
    //   141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload #5
    //   147: invokevirtual toString : ()Ljava/lang/String;
    //   150: astore #4
    //   152: ldc 'ActivityThread'
    //   154: aload #4
    //   156: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   159: pop
    //   160: aload_3
    //   161: monitorexit
    //   162: return
    //   163: astore #4
    //   165: aload_3
    //   166: monitorexit
    //   167: aload #4
    //   169: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3375	-> 0
    //   #3376	-> 7
    //   #3377	-> 15
    //   #3379	-> 18
    //   #3382	-> 23
    //   #3383	-> 34
    //   #3384	-> 42
    //   #3385	-> 47
    //   #3387	-> 78
    //   #3388	-> 83
    //   #3390	-> 88
    //   #3391	-> 94
    //   #3392	-> 120
    //   #3391	-> 152
    //   #3394	-> 160
    //   #3395	-> 162
    //   #3394	-> 163
    // Exception table:
    //   from	to	target	type
    //   7	15	163	finally
    //   15	17	163	finally
    //   18	23	163	finally
    //   28	34	163	finally
    //   34	42	163	finally
    //   42	47	163	finally
    //   47	75	163	finally
    //   78	83	163	finally
    //   83	88	163	finally
    //   88	94	163	finally
    //   94	120	163	finally
    //   137	152	163	finally
    //   152	160	163	finally
    //   160	162	163	finally
    //   165	167	163	finally
  }
  
  private void updateVmProcessState(int paramInt) {
    if (paramInt <= 6) {
      paramInt = 0;
    } else {
      paramInt = 1;
    } 
    VMRuntime.getRuntime().updateProcessState(paramInt);
  }
  
  private void applyPendingProcessState() {
    synchronized (this.mAppThread) {
      if (this.mPendingProcessState == -1)
        return; 
      int i = this.mPendingProcessState;
      this.mPendingProcessState = -1;
      if (i == this.mLastProcessState)
        updateVmProcessState(i); 
      return;
    } 
  }
  
  public void countLaunchingActivities(int paramInt) {
    this.mNumLaunchingActivities.getAndAdd(paramInt);
  }
  
  public final void sendActivityResult(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, Intent paramIntent) {
    ArrayList<ResultInfo> arrayList = new ArrayList();
    arrayList.add(new ResultInfo(paramString, paramInt1, paramInt2, paramIntent));
    ClientTransaction clientTransaction = ClientTransaction.obtain(this.mAppThread, paramIBinder);
    clientTransaction.addCallback(ActivityResultItem.obtain(arrayList));
    try {
      this.mAppThread.scheduleTransaction(clientTransaction);
    } catch (RemoteException remoteException) {}
  }
  
  TransactionExecutor getTransactionExecutor() {
    return this.mTransactionExecutor;
  }
  
  void sendMessage(int paramInt, Object paramObject) {
    sendMessage(paramInt, paramObject, 0, 0, false);
  }
  
  private void sendMessage(int paramInt1, Object paramObject, int paramInt2) {
    sendMessage(paramInt1, paramObject, paramInt2, 0, false);
  }
  
  private void sendMessage(int paramInt1, Object paramObject, int paramInt2, int paramInt3) {
    sendMessage(paramInt1, paramObject, paramInt2, paramInt3, false);
  }
  
  private void sendMessage(int paramInt1, Object paramObject, int paramInt2, int paramInt3, boolean paramBoolean) {
    if (DEBUG_MESSAGES) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SCHEDULE ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" ");
      stringBuilder.append(this.mH.codeToString(paramInt1));
      stringBuilder.append(": ");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" / ");
      stringBuilder.append(paramObject);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    Message message = Message.obtain();
    message.what = paramInt1;
    message.obj = paramObject;
    message.arg1 = paramInt2;
    message.arg2 = paramInt3;
    if (paramBoolean)
      message.setAsynchronous(true); 
    this.mH.sendMessage(message);
  }
  
  private void sendMessage(int paramInt1, Object paramObject, int paramInt2, int paramInt3, int paramInt4) {
    if (DEBUG_MESSAGES) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SCHEDULE ");
      H h = this.mH;
      stringBuilder.append(h.codeToString(paramInt1));
      stringBuilder.append(" arg1=");
      stringBuilder.append(paramInt2);
      stringBuilder.append(" arg2=");
      stringBuilder.append(paramInt3);
      stringBuilder.append("seq= ");
      stringBuilder.append(paramInt4);
      String str = stringBuilder.toString();
      Slog.v("ActivityThread", str);
    } 
    Message message = Message.obtain();
    message.what = paramInt1;
    SomeArgs someArgs = SomeArgs.obtain();
    someArgs.arg1 = paramObject;
    someArgs.argi1 = paramInt2;
    someArgs.argi2 = paramInt3;
    someArgs.argi3 = paramInt4;
    message.obj = someArgs;
    this.mH.sendMessage(message);
  }
  
  final void scheduleContextCleanup(ContextImpl paramContextImpl, String paramString1, String paramString2) {
    ContextCleanupInfo contextCleanupInfo = new ContextCleanupInfo();
    contextCleanupInfo.context = paramContextImpl;
    contextCleanupInfo.who = paramString1;
    contextCleanupInfo.what = paramString2;
    sendMessage(119, contextCleanupInfo);
  }
  
  public void handleFixedRotationAdjustments(IBinder paramIBinder, DisplayAdjustments.FixedRotationAdjustments paramFixedRotationAdjustments) {
    handleFixedRotationAdjustments(paramIBinder, paramFixedRotationAdjustments, (Configuration)null);
  }
  
  private void handleFixedRotationAdjustments(IBinder paramIBinder, DisplayAdjustments.FixedRotationAdjustments paramFixedRotationAdjustments, Configuration paramConfiguration) {
    DisplayAdjustments.FixedRotationAdjustments fixedRotationAdjustments;
    Consumer<DisplayAdjustments> consumer;
    Object object;
    if (paramFixedRotationAdjustments != null) {
      object = new Configuration[1];
      _$$Lambda$ActivityThread$vMpYFfxK9ah9pM_OyK4G3WmpR2s _$$Lambda$ActivityThread$vMpYFfxK9ah9pM_OyK4G3WmpR2s = new _$$Lambda$ActivityThread$vMpYFfxK9ah9pM_OyK4G3WmpR2s(paramFixedRotationAdjustments, (Configuration[])object);
    } else {
      object = null;
      paramFixedRotationAdjustments = null;
    } 
    if (!this.mResourcesManager.overrideTokenDisplayAdjustments(paramIBinder, (Consumer<DisplayAdjustments>)paramFixedRotationAdjustments))
      return; 
    Configuration configuration = paramConfiguration;
    if (paramConfiguration == null) {
      ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
      if (activityClientRecord == null)
        return; 
      configuration = activityClientRecord.overrideConfig;
    } 
    if (object != null)
      object[0] = configuration; 
    if (this.mActiveRotationAdjustments == null)
      this.mActiveRotationAdjustments = new ArrayList<>(2); 
    if (paramFixedRotationAdjustments != null) {
      this.mActiveRotationAdjustments.add(Pair.create(paramIBinder, paramFixedRotationAdjustments));
      fixedRotationAdjustments = paramFixedRotationAdjustments;
    } else {
      this.mActiveRotationAdjustments.removeIf(new _$$Lambda$ActivityThread$1aRbI_i_3uur4gKmyI8A0r9vN6o((IBinder)fixedRotationAdjustments));
      if (this.mActiveRotationAdjustments.isEmpty()) {
        fixedRotationAdjustments = null;
      } else {
        ArrayList<Pair<IBinder, Consumer<DisplayAdjustments>>> arrayList = this.mActiveRotationAdjustments;
        consumer = (Consumer)((Pair)arrayList.get(arrayList.size() - 1)).second;
      } 
    } 
    this.mInitialApplication.getResources().overrideDisplayAdjustments(consumer);
  }
  
  private Activity performLaunchActivity(ActivityClientRecord paramActivityClientRecord, Intent paramIntent) {
    StringBuilder stringBuilder;
    Activity activity;
    ActivityInfo activityInfo = paramActivityClientRecord.activityInfo;
    if (paramActivityClientRecord.packageInfo == null)
      paramActivityClientRecord.packageInfo = getPackageInfo(activityInfo.applicationInfo, paramActivityClientRecord.compatInfo, 1); 
    ComponentName componentName2 = paramActivityClientRecord.intent.getComponent();
    ComponentName componentName1 = componentName2;
    if (componentName2 == null) {
      Intent intent = paramActivityClientRecord.intent;
      Application application = this.mInitialApplication;
      PackageManager packageManager = application.getPackageManager();
      componentName1 = intent.resolveActivity(packageManager);
      paramActivityClientRecord.intent.setComponent(componentName1);
    } 
    if (paramActivityClientRecord.activityInfo.targetActivity != null) {
      componentName2 = new ComponentName(paramActivityClientRecord.activityInfo.packageName, paramActivityClientRecord.activityInfo.targetActivity);
    } else {
      componentName2 = componentName1;
    } 
    ContextImpl contextImpl = createBaseContextForActivity(paramActivityClientRecord);
    ComponentName componentName3 = null;
    componentName1 = componentName3;
    try {
      ClassLoader classLoader = contextImpl.getClassLoader();
      componentName1 = componentName3;
      Instrumentation instrumentation = this.mInstrumentation;
      componentName1 = componentName3;
      String str = componentName2.getClassName();
      componentName1 = componentName3;
      Intent intent = paramActivityClientRecord.intent;
      componentName1 = componentName3;
      Activity activity1 = instrumentation.newActivity(classLoader, str, intent);
      activity = activity1;
      StrictMode.incrementExpectedActivityCount(activity1.getClass());
      activity = activity1;
      paramActivityClientRecord.intent.setExtrasClassLoader(classLoader);
      activity = activity1;
      paramActivityClientRecord.intent.prepareToEnterProcess();
      activity = activity1;
      if (paramActivityClientRecord.state != null) {
        activity = activity1;
        paramActivityClientRecord.state.setClassLoader(classLoader);
      } 
      activity = activity1;
    } catch (Exception exception1) {
      if (!this.mInstrumentation.onException(activity, exception1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to instantiate activity ");
        stringBuilder.append(componentName2);
        stringBuilder.append(": ");
        stringBuilder.append(exception1.toString());
        throw new RuntimeException(stringBuilder.toString(), exception1);
      } 
    } 
    try {
      LoadedApk loadedApk = ((ActivityClientRecord)stringBuilder).packageInfo;
      try {
        Application application = loadedApk.makeApplication(false, this.mInstrumentation);
        boolean bool = localLOGV;
        if (bool)
          try {
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Performing launch of ");
            stringBuilder1.append(stringBuilder);
            Slog.v("ActivityThread", stringBuilder1.toString());
          } catch (SuperNotCalledException superNotCalledException) {
          
          } catch (Exception null) {} 
        bool = localLOGV;
        if (bool) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(null);
          stringBuilder1.append(": app=");
          stringBuilder1.append(application);
          stringBuilder1.append(", appName=");
          stringBuilder1.append(application.getPackageName());
          stringBuilder1.append(", pkg=");
          LoadedApk loadedApk2 = ((ActivityClientRecord)null).packageInfo;
          stringBuilder1.append(loadedApk2.getPackageName());
          stringBuilder1.append(", comp=");
          Intent intent = ((ActivityClientRecord)null).intent;
          stringBuilder1.append(intent.getComponent().toShortString());
          stringBuilder1.append(", dir=");
          LoadedApk loadedApk1 = ((ActivityClientRecord)null).packageInfo;
          stringBuilder1.append(loadedApk1.getAppDir());
          String str = stringBuilder1.toString();
          Slog.v("ActivityThread", str);
        } 
        if (activity != null) {
          CharSequence charSequence = ((ActivityClientRecord)null).activityInfo.loadLabel(contextImpl.getPackageManager());
          Configuration configuration2 = new Configuration();
          this(this.mCompatConfiguration);
          Configuration configuration1 = ((ActivityClientRecord)null).overrideConfig;
          if (configuration1 != null)
            configuration2.updateFrom(((ActivityClientRecord)null).overrideConfig); 
          bool = DEBUG_CONFIGURATION;
          if (bool) {
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Launching activity ");
            stringBuilder1.append(((ActivityClientRecord)null).activityInfo.name);
            stringBuilder1.append(" with config ");
            stringBuilder1.append(configuration2);
            Slog.v("ActivityThread", stringBuilder1.toString());
          } 
          Window window = ((ActivityClientRecord)null).mPendingRemoveWindow;
          if (window != null && ((ActivityClientRecord)null).mPreserveWindow) {
            window = ((ActivityClientRecord)null).mPendingRemoveWindow;
            ((ActivityClientRecord)null).mPendingRemoveWindow = null;
            ((ActivityClientRecord)null).mPendingRemoveWindowManager = null;
          } else {
            window = null;
          } 
          Resources resources = contextImpl.getResources();
          ResourcesLoader[] arrayOfResourcesLoader = application.getResources().getLoaders().<ResourcesLoader>toArray(new ResourcesLoader[0]);
          resources.addLoaders(arrayOfResourcesLoader);
          contextImpl.setOuterContext((Context)activity);
          Instrumentation instrumentation = getInstrumentation();
          IBinder iBinder = ((ActivityClientRecord)null).token;
          int i = ((ActivityClientRecord)null).ident;
          Intent intent = ((ActivityClientRecord)null).intent;
          ActivityInfo activityInfo1 = ((ActivityClientRecord)null).activityInfo;
          try {
            Activity activity1 = ((ActivityClientRecord)null).parent;
            try {
              String str = ((ActivityClientRecord)null).embeddedID;
              try {
                Activity.NonConfigurationInstances nonConfigurationInstances = ((ActivityClientRecord)null).lastNonConfigurationInstances;
                String str1 = ((ActivityClientRecord)null).referrer;
                IVoiceInteractor iVoiceInteractor = ((ActivityClientRecord)null).voiceInteractor;
                ViewRootImpl.ActivityConfigCallback activityConfigCallback = ((ActivityClientRecord)null).configCallback;
                IBinder iBinder1 = ((ActivityClientRecord)null).assistToken;
                try {
                  activity.attach(contextImpl, this, instrumentation, iBinder, i, application, intent, activityInfo1, charSequence, activity1, str, nonConfigurationInstances, configuration2, str1, iVoiceInteractor, window, activityConfigCallback, iBinder1);
                  if (paramIntent != null)
                    try {
                      activity.mIntent = paramIntent;
                    } catch (SuperNotCalledException superNotCalledException) {
                    
                    } catch (Exception null) {} 
                  try {
                    ((ActivityClientRecord)exception).lastNonConfigurationInstances = null;
                    checkAndBlockForNetworkAccess();
                    activity.mStartedActivity = false;
                    i = ((ActivityClientRecord)exception).activityInfo.getThemeResource();
                    if (i != 0)
                      activity.setTheme(i); 
                    activity.mCalled = false;
                    Trace.traceBegin(64L, "ActivityOnCreate");
                    bool = exception.isPersistable();
                    if (bool) {
                      try {
                        this.mInstrumentation.callActivityOnCreate(activity, ((ActivityClientRecord)exception).state, ((ActivityClientRecord)exception).persistentState);
                        Trace.traceEnd(64L);
                      } catch (SuperNotCalledException superNotCalledException) {
                      
                      } catch (Exception exception1) {}
                    } else {
                      this.mInstrumentation.callActivityOnCreate(activity, ((ActivityClientRecord)exception1).state);
                      Trace.traceEnd(64L);
                    } 
                  } catch (SuperNotCalledException superNotCalledException) {
                  
                  } catch (Exception exception1) {}
                } catch (SuperNotCalledException superNotCalledException) {
                
                } catch (Exception null) {}
              } catch (SuperNotCalledException superNotCalledException) {
              
              } catch (Exception null) {}
            } catch (SuperNotCalledException superNotCalledException) {
            
            } catch (Exception null) {}
          } catch (SuperNotCalledException superNotCalledException) {
          
          } catch (Exception null) {}
          return activity;
        } 
        null.setState(1);
        synchronized (this.mResourcesManager) {
          this.mActivities.put(((ActivityClientRecord)null).token, null);
        } 
      } catch (SuperNotCalledException superNotCalledException) {}
    } catch (SuperNotCalledException superNotCalledException) {
    
    } catch (Exception exception) {
      if (!this.mInstrumentation.onException(activity, exception)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unable to start activity ");
        stringBuilder1.append(componentName2);
        stringBuilder1.append(": ");
        stringBuilder1.append(exception.toString());
        throw new RuntimeException(stringBuilder1.toString(), exception);
      } 
      return activity;
    } 
    throw exception;
  }
  
  public void handleStartActivity(IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    Activity activity = activityClientRecord.activity;
    if (activityClientRecord.activity == null)
      return; 
    if (activityClientRecord.stopped) {
      Intent intent;
      if (activityClientRecord.activity.mFinished)
        return; 
      unscheduleGcIdler();
      activity.performStart("handleStartActivity");
      activityClientRecord.setState(2);
      if (paramPendingTransactionActions == null)
        return; 
      if (paramPendingTransactionActions.shouldRestoreInstanceState())
        if (activityClientRecord.isPersistable()) {
          if (activityClientRecord.state != null || activityClientRecord.persistentState != null)
            this.mInstrumentation.callActivityOnRestoreInstanceState(activity, activityClientRecord.state, activityClientRecord.persistentState); 
        } else if (activityClientRecord.state != null) {
          this.mInstrumentation.callActivityOnRestoreInstanceState(activity, activityClientRecord.state);
        }  
      if (paramPendingTransactionActions.shouldCallOnPostCreate()) {
        activity.mCalled = false;
        if (activityClientRecord.isPersistable()) {
          this.mInstrumentation.callActivityOnPostCreate(activity, activityClientRecord.state, activityClientRecord.persistentState);
        } else {
          this.mInstrumentation.callActivityOnPostCreate(activity, activityClientRecord.state);
        } 
        if (!activity.mCalled) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Activity ");
          intent = activityClientRecord.intent;
          stringBuilder.append(intent.getComponent().toShortString());
          stringBuilder.append(" did not call through to super.onPostCreate()");
          throw new SuperNotCalledException(stringBuilder.toString());
        } 
      } 
      updateVisibility((ActivityClientRecord)intent, true);
      this.mSomeActivitiesChanged = true;
      return;
    } 
    throw new IllegalStateException("Can't start activity that is not stopped.");
  }
  
  private void checkAndBlockForNetworkAccess() {
    synchronized (this.mNetworkPolicyLock) {
      long l = this.mNetworkBlockSeq;
      if (l != -1L)
        try {
          ActivityManager.getService().waitForNetworkStateUpdate(this.mNetworkBlockSeq);
          this.mNetworkBlockSeq = -1L;
        } catch (RemoteException remoteException) {} 
      return;
    } 
  }
  
  private ContextImpl createBaseContextForActivity(ActivityClientRecord paramActivityClientRecord) {
    try {
      int i = ActivityTaskManager.getService().getDisplayId(paramActivityClientRecord.token);
      ContextImpl contextImpl1 = ContextImpl.createActivityContext(this, paramActivityClientRecord.packageInfo, paramActivityClientRecord.activityInfo, paramActivityClientRecord.token, i, paramActivityClientRecord.overrideConfig);
      if (paramActivityClientRecord.mPendingFixedRotationAdjustments != null) {
        handleFixedRotationAdjustments(paramActivityClientRecord.token, paramActivityClientRecord.mPendingFixedRotationAdjustments, paramActivityClientRecord.overrideConfig);
        paramActivityClientRecord.mPendingFixedRotationAdjustments = null;
      } 
      DisplayManagerGlobal displayManagerGlobal = DisplayManagerGlobal.getInstance();
      String str = SystemProperties.get("debug.second-display.pkg");
      ContextImpl contextImpl2 = contextImpl1;
      if (str != null) {
        contextImpl2 = contextImpl1;
        if (!str.isEmpty()) {
          String str1 = paramActivityClientRecord.packageInfo.mPackageName;
          contextImpl2 = contextImpl1;
          if (str1.contains(str)) {
            int arrayOfInt[] = displayManagerGlobal.getDisplayIds(), j = arrayOfInt.length;
            i = 0;
            while (true) {
              contextImpl2 = contextImpl1;
              if (i < j) {
                int k = arrayOfInt[i];
                if (k != 0) {
                  Display display = displayManagerGlobal.getCompatibleDisplay(k, contextImpl1.getResources());
                  contextImpl2 = (ContextImpl)contextImpl1.createDisplayContext(display);
                  break;
                } 
                i++;
                continue;
              } 
              break;
            } 
          } 
        } 
      } 
      return contextImpl2;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void acquirePerformance() {
    if (mOrmsManager == null) {
      OplusResourceManager oplusResourceManager = OplusResourceManager.getInstance(ActivityThread.class);
      if (oplusResourceManager != null) {
        mOrmsManager.ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_ACTIVITY_START", 600));
        return;
      } 
      return;
    } 
    mOrmsManager.ormsSetSceneAction(new OrmsSaParam("", "ORMS_ACTION_ACTIVITY_START", 600));
  }
  
  public Activity handleLaunchActivity(ActivityClientRecord paramActivityClientRecord, PendingTransactionActions paramPendingTransactionActions, Intent paramIntent) {
    IOplusRedPacketManager iOplusRedPacketManager;
    unscheduleGcIdler();
    this.mSomeActivitiesChanged = true;
    acquirePerformance();
    if (paramActivityClientRecord.profilerInfo != null) {
      this.mProfiler.setProfiler(paramActivityClientRecord.profilerInfo);
      this.mProfiler.startProfiling();
    } 
    handleConfigurationChanged((Configuration)null, (CompatibilityInfo)null);
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Handling launch of ");
      stringBuilder.append(paramActivityClientRecord);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (!ThreadedRenderer.sRendererDisabled && (paramActivityClientRecord.activityInfo.flags & 0x200) != 0)
      HardwareRenderer.preload(); 
    WindowManagerGlobal.initialize();
    GraphicsEnvironment.hintActivityLaunch();
    Activity activity = performLaunchActivity(paramActivityClientRecord, paramIntent);
    if (activity != null) {
      paramActivityClientRecord.createdConfig = new Configuration(this.mConfiguration);
      reportSizeConfigurations(paramActivityClientRecord);
      if (!paramActivityClientRecord.activity.mFinished && paramPendingTransactionActions != null) {
        paramPendingTransactionActions.setOldState(paramActivityClientRecord.state);
        paramPendingTransactionActions.setRestoreInstanceState(true);
        paramPendingTransactionActions.setCallOnPostCreate(true);
      } 
      if (!mRPChecked) {
        mRPChecked = true;
        iOplusRedPacketManager = OplusFeatureCache.<IOplusRedPacketManager>getOrCreate(IOplusRedPacketManager.DEFAULT, new Object[0]);
        if (iOplusRedPacketManager.isInjectingTarget(currentPackageName()))
          mRPNeedNotify = iOplusRedPacketManager.inject(activity.getBaseContext()); 
      } 
    } else {
      try {
        IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
        IBinder iBinder = ((ActivityClientRecord)iOplusRedPacketManager).token;
        iActivityTaskManager.finishActivity(iBinder, 0, null, 0);
        return activity;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    return activity;
  }
  
  private void reportSizeConfigurations(ActivityClientRecord paramActivityClientRecord) {
    if (this.mActivitiesToBeDestroyed.containsKey(paramActivityClientRecord.token))
      return; 
    Configuration[] arrayOfConfiguration = paramActivityClientRecord.activity.getResources().getSizeConfigurations();
    if (arrayOfConfiguration == null)
      return; 
    SparseIntArray sparseIntArray1 = new SparseIntArray();
    SparseIntArray sparseIntArray2 = new SparseIntArray();
    SparseIntArray sparseIntArray3 = new SparseIntArray();
    for (int i = arrayOfConfiguration.length - 1; i >= 0; i--) {
      Configuration configuration = arrayOfConfiguration[i];
      if (configuration.screenHeightDp != 0)
        sparseIntArray2.put(configuration.screenHeightDp, 0); 
      if (configuration.screenWidthDp != 0)
        sparseIntArray1.put(configuration.screenWidthDp, 0); 
      if (configuration.smallestScreenWidthDp != 0)
        sparseIntArray3.put(configuration.smallestScreenWidthDp, 0); 
    } 
    try {
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      IBinder iBinder = paramActivityClientRecord.token;
      int[] arrayOfInt1 = sparseIntArray1.copyKeys(), arrayOfInt2 = sparseIntArray2.copyKeys(), arrayOfInt3 = sparseIntArray3.copyKeys();
      iActivityTaskManager.reportSizeConfigurations(iBinder, arrayOfInt1, arrayOfInt2, arrayOfInt3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void deliverNewIntents(ActivityClientRecord paramActivityClientRecord, List<ReferrerIntent> paramList) {
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      ReferrerIntent referrerIntent = paramList.get(b);
      referrerIntent.setExtrasClassLoader(paramActivityClientRecord.activity.getClassLoader());
      referrerIntent.prepareToEnterProcess();
      paramActivityClientRecord.activity.mFragments.noteStateNotSaved();
      this.mInstrumentation.callActivityOnNewIntent(paramActivityClientRecord.activity, referrerIntent);
    } 
  }
  
  public void handleNewIntent(IBinder paramIBinder, List<ReferrerIntent> paramList) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord == null)
      return; 
    checkAndBlockForNetworkAccess();
    deliverNewIntents(activityClientRecord, paramList);
  }
  
  public void handleRequestAssistContextExtras(RequestAssistContextExtras paramRequestAssistContextExtras) {
    // Byte code:
    //   0: aload_1
    //   1: getfield requestType : I
    //   4: istore_2
    //   5: iconst_0
    //   6: istore_3
    //   7: iload_2
    //   8: iconst_2
    //   9: if_icmpne -> 18
    //   12: iconst_1
    //   13: istore #4
    //   15: goto -> 21
    //   18: iconst_0
    //   19: istore #4
    //   21: aload_0
    //   22: getfield mLastSessionId : I
    //   25: aload_1
    //   26: getfield sessionId : I
    //   29: if_icmpeq -> 98
    //   32: aload_0
    //   33: aload_1
    //   34: getfield sessionId : I
    //   37: putfield mLastSessionId : I
    //   40: aload_0
    //   41: getfield mLastAssistStructures : Ljava/util/ArrayList;
    //   44: invokevirtual size : ()I
    //   47: iconst_1
    //   48: isub
    //   49: istore_2
    //   50: iload_2
    //   51: iflt -> 98
    //   54: aload_0
    //   55: getfield mLastAssistStructures : Ljava/util/ArrayList;
    //   58: iload_2
    //   59: invokevirtual get : (I)Ljava/lang/Object;
    //   62: checkcast java/lang/ref/WeakReference
    //   65: invokevirtual get : ()Ljava/lang/Object;
    //   68: checkcast android/app/assist/AssistStructure
    //   71: astore #5
    //   73: aload #5
    //   75: ifnull -> 83
    //   78: aload #5
    //   80: invokevirtual clearSendChannel : ()V
    //   83: aload_0
    //   84: getfield mLastAssistStructures : Ljava/util/ArrayList;
    //   87: iload_2
    //   88: invokevirtual remove : (I)Ljava/lang/Object;
    //   91: pop
    //   92: iinc #2, -1
    //   95: goto -> 50
    //   98: new android/os/Bundle
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: astore #6
    //   107: aconst_null
    //   108: astore #7
    //   110: iload #4
    //   112: ifeq -> 121
    //   115: aconst_null
    //   116: astore #8
    //   118: goto -> 130
    //   121: new android/app/assist/AssistContent
    //   124: dup
    //   125: invokespecial <init> : ()V
    //   128: astore #8
    //   130: invokestatic uptimeMillis : ()J
    //   133: lstore #9
    //   135: aload_0
    //   136: getfield mActivities : Landroid/util/ArrayMap;
    //   139: aload_1
    //   140: getfield activityToken : Landroid/os/IBinder;
    //   143: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   146: checkcast android/app/ActivityThread$ActivityClientRecord
    //   149: astore #11
    //   151: aconst_null
    //   152: astore #5
    //   154: aload #11
    //   156: ifnull -> 374
    //   159: iload #4
    //   161: ifne -> 202
    //   164: aload #11
    //   166: getfield activity : Landroid/app/Activity;
    //   169: invokevirtual getApplication : ()Landroid/app/Application;
    //   172: aload #11
    //   174: getfield activity : Landroid/app/Activity;
    //   177: aload #6
    //   179: invokevirtual dispatchOnProvideAssistData : (Landroid/app/Activity;Landroid/os/Bundle;)V
    //   182: aload #11
    //   184: getfield activity : Landroid/app/Activity;
    //   187: aload #6
    //   189: invokevirtual onProvideAssistData : (Landroid/os/Bundle;)V
    //   192: aload #11
    //   194: getfield activity : Landroid/app/Activity;
    //   197: invokevirtual onProvideReferrer : ()Landroid/net/Uri;
    //   200: astore #5
    //   202: aload_1
    //   203: getfield requestType : I
    //   206: iconst_1
    //   207: if_icmpeq -> 221
    //   210: iload #4
    //   212: ifeq -> 218
    //   215: goto -> 221
    //   218: goto -> 377
    //   221: new android/app/assist/AssistStructure
    //   224: dup
    //   225: aload #11
    //   227: getfield activity : Landroid/app/Activity;
    //   230: iload #4
    //   232: aload_1
    //   233: getfield flags : I
    //   236: invokespecial <init> : (Landroid/app/Activity;ZI)V
    //   239: astore #7
    //   241: aload #11
    //   243: getfield activity : Landroid/app/Activity;
    //   246: invokevirtual getIntent : ()Landroid/content/Intent;
    //   249: astore #12
    //   251: aload #11
    //   253: getfield window : Landroid/view/Window;
    //   256: ifnull -> 283
    //   259: aload #11
    //   261: getfield window : Landroid/view/Window;
    //   264: astore #13
    //   266: iload_3
    //   267: istore_2
    //   268: aload #13
    //   270: invokevirtual getAttributes : ()Landroid/view/WindowManager$LayoutParams;
    //   273: getfield flags : I
    //   276: sipush #8192
    //   279: iand
    //   280: ifne -> 285
    //   283: iconst_1
    //   284: istore_2
    //   285: aload #12
    //   287: ifnull -> 339
    //   290: iload_2
    //   291: ifeq -> 339
    //   294: iload #4
    //   296: ifne -> 356
    //   299: new android/content/Intent
    //   302: dup
    //   303: aload #12
    //   305: invokespecial <init> : (Landroid/content/Intent;)V
    //   308: astore #12
    //   310: aload #12
    //   312: aload #12
    //   314: invokevirtual getFlags : ()I
    //   317: bipush #-67
    //   319: iand
    //   320: invokevirtual setFlags : (I)Landroid/content/Intent;
    //   323: pop
    //   324: aload #12
    //   326: invokevirtual removeUnsafeExtras : ()V
    //   329: aload #8
    //   331: aload #12
    //   333: invokevirtual setDefaultIntent : (Landroid/content/Intent;)V
    //   336: goto -> 356
    //   339: iload #4
    //   341: ifne -> 356
    //   344: aload #8
    //   346: new android/content/Intent
    //   349: dup
    //   350: invokespecial <init> : ()V
    //   353: invokevirtual setDefaultIntent : (Landroid/content/Intent;)V
    //   356: iload #4
    //   358: ifne -> 371
    //   361: aload #11
    //   363: getfield activity : Landroid/app/Activity;
    //   366: aload #8
    //   368: invokevirtual onProvideAssistContent : (Landroid/app/assist/AssistContent;)V
    //   371: goto -> 377
    //   374: aconst_null
    //   375: astore #5
    //   377: aload #7
    //   379: ifnonnull -> 394
    //   382: new android/app/assist/AssistStructure
    //   385: dup
    //   386: invokespecial <init> : ()V
    //   389: astore #7
    //   391: goto -> 394
    //   394: aload #7
    //   396: lload #9
    //   398: invokevirtual setAcquisitionStartTime : (J)V
    //   401: aload #7
    //   403: invokestatic uptimeMillis : ()J
    //   406: invokevirtual setAcquisitionEndTime : (J)V
    //   409: aload_0
    //   410: getfield mLastAssistStructures : Ljava/util/ArrayList;
    //   413: new java/lang/ref/WeakReference
    //   416: dup
    //   417: aload #7
    //   419: invokespecial <init> : (Ljava/lang/Object;)V
    //   422: invokevirtual add : (Ljava/lang/Object;)Z
    //   425: pop
    //   426: invokestatic getService : ()Landroid/app/IActivityTaskManager;
    //   429: astore #11
    //   431: aload_1
    //   432: getfield requestToken : Landroid/os/IBinder;
    //   435: astore_1
    //   436: aload #11
    //   438: aload_1
    //   439: aload #6
    //   441: aload #7
    //   443: aload #8
    //   445: aload #5
    //   447: invokeinterface reportAssistContextExtras : (Landroid/os/IBinder;Landroid/os/Bundle;Landroid/app/assist/AssistStructure;Landroid/app/assist/AssistContent;Landroid/net/Uri;)V
    //   452: return
    //   453: astore_1
    //   454: goto -> 458
    //   457: astore_1
    //   458: aload_1
    //   459: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   462: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3963	-> 0
    //   #3966	-> 21
    //   #3968	-> 32
    //   #3969	-> 40
    //   #3970	-> 54
    //   #3971	-> 73
    //   #3972	-> 78
    //   #3974	-> 83
    //   #3969	-> 92
    //   #3978	-> 98
    //   #3979	-> 107
    //   #3980	-> 110
    //   #3981	-> 130
    //   #3982	-> 135
    //   #3983	-> 151
    //   #3984	-> 154
    //   #3985	-> 159
    //   #3986	-> 164
    //   #3987	-> 182
    //   #3988	-> 192
    //   #3990	-> 202
    //   #3991	-> 221
    //   #3992	-> 241
    //   #3993	-> 251
    //   #3994	-> 266
    //   #3996	-> 285
    //   #3997	-> 294
    //   #3998	-> 299
    //   #3999	-> 310
    //   #4001	-> 324
    //   #4002	-> 329
    //   #4003	-> 336
    //   #4005	-> 339
    //   #4006	-> 344
    //   #4009	-> 356
    //   #4010	-> 361
    //   #4015	-> 371
    //   #3984	-> 374
    //   #4015	-> 377
    //   #4016	-> 382
    //   #4015	-> 394
    //   #4021	-> 394
    //   #4022	-> 401
    //   #4024	-> 409
    //   #4025	-> 426
    //   #4027	-> 431
    //   #4030	-> 452
    //   #4031	-> 452
    //   #4028	-> 453
    //   #4029	-> 458
    // Exception table:
    //   from	to	target	type
    //   431	436	457	android/os/RemoteException
    //   436	452	453	android/os/RemoteException
  }
  
  private void handleRequestDirectActions(IBinder paramIBinder, IVoiceInteractor paramIVoiceInteractor, CancellationSignal paramCancellationSignal, RemoteCallback paramRemoteCallback) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mActivities : Landroid/util/ArrayMap;
    //   4: aload_1
    //   5: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   8: checkcast android/app/ActivityThread$ActivityClientRecord
    //   11: astore #5
    //   13: aload #5
    //   15: ifnonnull -> 57
    //   18: new java/lang/StringBuilder
    //   21: dup
    //   22: invokespecial <init> : ()V
    //   25: astore_2
    //   26: aload_2
    //   27: ldc_w 'requestDirectActions(): no activity for '
    //   30: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   33: pop
    //   34: aload_2
    //   35: aload_1
    //   36: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   39: pop
    //   40: ldc 'ActivityThread'
    //   42: aload_2
    //   43: invokevirtual toString : ()Ljava/lang/String;
    //   46: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   49: pop
    //   50: aload #4
    //   52: aconst_null
    //   53: invokevirtual sendResult : (Landroid/os/Bundle;)V
    //   56: return
    //   57: aload #5
    //   59: invokevirtual getLifecycleState : ()I
    //   62: istore #6
    //   64: iload #6
    //   66: iconst_2
    //   67: if_icmplt -> 201
    //   70: iload #6
    //   72: iconst_5
    //   73: if_icmplt -> 79
    //   76: goto -> 201
    //   79: aload #5
    //   81: getfield activity : Landroid/app/Activity;
    //   84: getfield mVoiceInteractor : Landroid/app/VoiceInteractor;
    //   87: ifnull -> 119
    //   90: aload #5
    //   92: getfield activity : Landroid/app/Activity;
    //   95: getfield mVoiceInteractor : Landroid/app/VoiceInteractor;
    //   98: getfield mInteractor : Lcom/android/internal/app/IVoiceInteractor;
    //   101: astore_1
    //   102: aload_1
    //   103: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   108: astore_1
    //   109: aload_1
    //   110: aload_2
    //   111: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   116: if_acmpeq -> 180
    //   119: aload #5
    //   121: getfield activity : Landroid/app/Activity;
    //   124: getfield mVoiceInteractor : Landroid/app/VoiceInteractor;
    //   127: ifnull -> 141
    //   130: aload #5
    //   132: getfield activity : Landroid/app/Activity;
    //   135: getfield mVoiceInteractor : Landroid/app/VoiceInteractor;
    //   138: invokevirtual destroy : ()V
    //   141: aload #5
    //   143: getfield activity : Landroid/app/Activity;
    //   146: astore_1
    //   147: aload #5
    //   149: getfield activity : Landroid/app/Activity;
    //   152: astore #7
    //   154: aload #5
    //   156: getfield activity : Landroid/app/Activity;
    //   159: astore #8
    //   161: aload_1
    //   162: new android/app/VoiceInteractor
    //   165: dup
    //   166: aload_2
    //   167: aload #7
    //   169: aload #8
    //   171: invokestatic myLooper : ()Landroid/os/Looper;
    //   174: invokespecial <init> : (Lcom/android/internal/app/IVoiceInteractor;Landroid/content/Context;Landroid/app/Activity;Landroid/os/Looper;)V
    //   177: putfield mVoiceInteractor : Landroid/app/VoiceInteractor;
    //   180: aload #5
    //   182: getfield activity : Landroid/app/Activity;
    //   185: aload_3
    //   186: new android/app/_$$Lambda$ActivityThread$kkRvsxdnIt_I5ztBPFaOyHFgais
    //   189: dup
    //   190: aload #5
    //   192: aload #4
    //   194: invokespecial <init> : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/os/RemoteCallback;)V
    //   197: invokevirtual onGetDirectActions : (Landroid/os/CancellationSignal;Ljava/util/function/Consumer;)V
    //   200: return
    //   201: new java/lang/StringBuilder
    //   204: dup
    //   205: invokespecial <init> : ()V
    //   208: astore_1
    //   209: aload_1
    //   210: ldc_w 'requestDirectActions('
    //   213: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload_1
    //   218: aload #5
    //   220: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   223: pop
    //   224: aload_1
    //   225: ldc_w '): wrong lifecycle: '
    //   228: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   231: pop
    //   232: aload_1
    //   233: iload #6
    //   235: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: ldc 'ActivityThread'
    //   241: aload_1
    //   242: invokevirtual toString : ()Ljava/lang/String;
    //   245: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   248: pop
    //   249: aload #4
    //   251: aconst_null
    //   252: invokevirtual sendResult : (Landroid/os/Bundle;)V
    //   255: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4037	-> 0
    //   #4038	-> 13
    //   #4039	-> 18
    //   #4040	-> 50
    //   #4041	-> 56
    //   #4043	-> 57
    //   #4044	-> 64
    //   #4049	-> 79
    //   #4050	-> 102
    //   #4051	-> 109
    //   #4052	-> 119
    //   #4053	-> 130
    //   #4055	-> 141
    //   #4056	-> 161
    //   #4058	-> 180
    //   #4075	-> 200
    //   #4045	-> 201
    //   #4046	-> 249
    //   #4047	-> 255
  }
  
  private void handlePerformDirectAction(IBinder paramIBinder, String paramString, Bundle paramBundle, CancellationSignal paramCancellationSignal, RemoteCallback paramRemoteCallback) {
    _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c;
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      Bundle bundle;
      int i = activityClientRecord.getLifecycleState();
      if (i < 2 || i >= 5) {
        paramRemoteCallback.sendResult(null);
        return;
      } 
      if (paramBundle != null) {
        bundle = paramBundle;
      } else {
        bundle = Bundle.EMPTY;
      } 
      Activity activity = activityClientRecord.activity;
      Objects.requireNonNull(paramRemoteCallback);
      _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c = new _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c(paramRemoteCallback);
      activity.onPerformDirectAction(paramString, bundle, paramCancellationSignal, _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c);
    } else {
      _$$Lambda$ZsFzoG2loyqNOR2cNbo_thrNK5c.sendResult(null);
    } 
  }
  
  public void handleTranslucentConversionComplete(IBinder paramIBinder, boolean paramBoolean) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null)
      activityClientRecord.activity.onTranslucentConversionComplete(paramBoolean); 
  }
  
  public void onNewActivityOptions(IBinder paramIBinder, ActivityOptions paramActivityOptions) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null)
      activityClientRecord.activity.onNewActivityOptions(paramActivityOptions); 
  }
  
  public void handleInstallProvider(ProviderInfo paramProviderInfo) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskWrites();
    try {
      installContentProviders(this.mInitialApplication, Arrays.asList(new ProviderInfo[] { paramProviderInfo }));
      return;
    } finally {
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private void handleEnterAnimationComplete(IBinder paramIBinder) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null)
      activityClientRecord.activity.dispatchEnterAnimationComplete(); 
  }
  
  private void handleStartBinderTracking() {
    Binder.enableTracing();
  }
  
  private void handleStopBinderTrackingAndDump(ParcelFileDescriptor paramParcelFileDescriptor) {
    try {
      Binder.disableTracing();
      Binder.getTransactionTracker().writeTracesToFile(paramParcelFileDescriptor);
      return;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)paramParcelFileDescriptor);
      Binder.getTransactionTracker().clearTraces();
    } 
  }
  
  public void handlePictureInPictureRequested(IBinder paramIBinder) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord == null) {
      Log.w("ActivityThread", "Activity to request PIP to no longer exists");
      return;
    } 
    boolean bool = activityClientRecord.activity.onPictureInPictureRequested();
    if (!bool)
      schedulePauseWithUserLeaveHintAndReturnToCurrentState(activityClientRecord); 
  }
  
  private void schedulePauseWithUserLeaveHintAndReturnToCurrentState(ActivityClientRecord paramActivityClientRecord) {
    int i = paramActivityClientRecord.getLifecycleState();
    if (i != 3 && i != 4)
      return; 
    if (i != 3) {
      if (i == 4) {
        scheduleResume(paramActivityClientRecord);
        schedulePauseWithUserLeavingHint(paramActivityClientRecord);
      } 
    } else {
      schedulePauseWithUserLeavingHint(paramActivityClientRecord);
      scheduleResume(paramActivityClientRecord);
    } 
  }
  
  private void schedulePauseWithUserLeavingHint(ActivityClientRecord paramActivityClientRecord) {
    ClientTransaction clientTransaction = ClientTransaction.obtain(this.mAppThread, paramActivityClientRecord.token);
    clientTransaction.setLifecycleStateRequest(PauseActivityItem.obtain(paramActivityClientRecord.activity.isFinishing(), true, paramActivityClientRecord.activity.mConfigChangeFlags, false));
    executeTransaction(clientTransaction);
  }
  
  private void scheduleResume(ActivityClientRecord paramActivityClientRecord) {
    ClientTransaction clientTransaction = ClientTransaction.obtain(this.mAppThread, paramActivityClientRecord.token);
    clientTransaction.setLifecycleStateRequest(ResumeActivityItem.obtain(false));
    executeTransaction(clientTransaction);
  }
  
  private void handleLocalVoiceInteractionStarted(IBinder paramIBinder, IVoiceInteractor paramIVoiceInteractor) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      activityClientRecord.voiceInteractor = paramIVoiceInteractor;
      activityClientRecord.activity.setVoiceInteractor(paramIVoiceInteractor);
      if (paramIVoiceInteractor == null) {
        activityClientRecord.activity.onLocalVoiceInteractionStopped();
      } else {
        activityClientRecord.activity.onLocalVoiceInteractionStarted();
      } 
    } 
  }
  
  private static boolean attemptAttachAgent(String paramString, ClassLoader paramClassLoader) {
    try {
      VMDebug.attachAgent(paramString, paramClassLoader);
      return true;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attaching agent with ");
      stringBuilder.append(paramClassLoader);
      stringBuilder.append(" failed: ");
      stringBuilder.append(paramString);
      Slog.e("ActivityThread", stringBuilder.toString());
      return false;
    } 
  }
  
  static void handleAttachAgent(String paramString, LoadedApk paramLoadedApk) {
    if (paramLoadedApk != null) {
      ClassLoader classLoader = paramLoadedApk.getClassLoader();
    } else {
      paramLoadedApk = null;
    } 
    if (attemptAttachAgent(paramString, (ClassLoader)paramLoadedApk))
      return; 
    if (paramLoadedApk != null)
      attemptAttachAgent(paramString, (ClassLoader)null); 
  }
  
  static void handleAttachStartupAgents(String paramString) {
    try {
      File file = new File();
      this(paramString);
      Path path = ContextImpl.getCodeCacheDirBeforeBind(file).toPath();
      if (!Files.exists(path, new java.nio.file.LinkOption[0]))
        return; 
      path = path.resolve("startup_agents");
      if (Files.exists(path, new java.nio.file.LinkOption[0]))
        for (Path path1 : Files.newDirectoryStream(path)) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(path1.toAbsolutePath().toString());
          stringBuilder.append("=");
          stringBuilder.append(paramString);
          String str = stringBuilder.toString();
          handleAttachAgent(str, (LoadedApk)null);
        }  
    } catch (Exception exception) {}
  }
  
  public static Intent getIntentBeingBroadcast() {
    return sCurrentBroadcastIntent.get();
  }
  
  private void handleReceiver(ReceiverData paramReceiverData) {
    unscheduleGcIdler();
    String str = paramReceiverData.intent.getComponent().getClassName();
    LoadedApk loadedApk = getPackageInfoNoCheck(paramReceiverData.info.applicationInfo, paramReceiverData.compatInfo);
    IActivityManager iActivityManager = ActivityManager.getService();
    try {
      RuntimeException runtimeException;
      Application application = loadedApk.makeApplication(false, this.mInstrumentation);
      ContextImpl contextImpl1 = (ContextImpl)application.getBaseContext();
      ContextImpl contextImpl2 = contextImpl1;
      if (paramReceiverData.info.splitName != null)
        contextImpl2 = (ContextImpl)contextImpl1.createContextForSplit(paramReceiverData.info.splitName); 
      ClassLoader classLoader = contextImpl2.getClassLoader();
      paramReceiverData.intent.setExtrasClassLoader(classLoader);
      paramReceiverData.intent.prepareToEnterProcess();
      paramReceiverData.setExtrasClassLoader(classLoader);
      AppComponentFactory appComponentFactory = loadedApk.getAppFactory();
      String str1 = paramReceiverData.info.name;
      Intent intent = paramReceiverData.intent;
      BroadcastReceiver broadcastReceiver = appComponentFactory.instantiateReceiver(classLoader, str1, intent);
      try {
        if (DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Performing receive of ");
          stringBuilder.append(paramReceiverData.intent);
          stringBuilder.append(": app=");
          stringBuilder.append(application);
          stringBuilder.append(", appName=");
          stringBuilder.append(application.getPackageName());
          stringBuilder.append(", pkg=");
          stringBuilder.append(loadedApk.getPackageName());
          stringBuilder.append(", comp=");
          Intent intent1 = paramReceiverData.intent;
          stringBuilder.append(intent1.getComponent().toShortString());
          stringBuilder.append(", dir=");
          stringBuilder.append(loadedApk.getAppDir());
          String str2 = stringBuilder.toString();
          Slog.v("ActivityThread", str2);
        } 
        boolean bool = paramReceiverData.getOrder();
        if (bool) {
          int i = paramReceiverData.intent.getFlags();
          paramReceiverData.setBroadcastState(i, 2);
        } 
        sCurrentBroadcastIntent.set(paramReceiverData.intent);
        broadcastReceiver.setPendingResult(paramReceiverData);
        if (mRPNeedNotify)
          ((IOplusRedPacketManager)OplusFeatureCache.<IOplusRedPacketManager>getOrCreate(IOplusRedPacketManager.DEFAULT, new Object[0])).checkReceiver(broadcastReceiver.getClass().getName()); 
        broadcastReceiver.onReceive(contextImpl2.getReceiverRestrictedContext(), paramReceiverData.intent);
        if (bool) {
          int i = paramReceiverData.intent.getFlags();
          paramReceiverData.setBroadcastState(i, 3);
        } 
      } catch (Exception exception) {
        if (DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Finishing failed broadcast to ");
          Intent intent1 = paramReceiverData.intent;
          stringBuilder.append(intent1.getComponent());
          String str2 = stringBuilder.toString();
          Slog.i("ActivityThread", str2);
        } 
        paramReceiverData.sendFinished(iActivityManager);
        boolean bool = this.mInstrumentation.onException(broadcastReceiver, exception);
        if (!bool) {
          runtimeException = new RuntimeException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unable to start receiver ");
          stringBuilder.append(str);
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          this(stringBuilder.toString(), exception);
          throw runtimeException;
        } 
      } finally {}
      sCurrentBroadcastIntent.set(null);
      if (runtimeException.getPendingResult() != null)
        paramReceiverData.finish(); 
      return;
    } catch (Exception exception) {
      if (DEBUG_BROADCAST) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Finishing failed broadcast to ");
        Intent intent = paramReceiverData.intent;
        stringBuilder1.append(intent.getComponent());
        String str1 = stringBuilder1.toString();
        Slog.i("ActivityThread", str1);
      } 
      paramReceiverData.sendFinished(iActivityManager);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to instantiate receiver ");
      stringBuilder.append(str);
      stringBuilder.append(": ");
      stringBuilder.append(exception.toString());
      throw new RuntimeException(stringBuilder.toString(), exception);
    } 
  }
  
  private void handleCreateBackupAgent(CreateBackupAgentData paramCreateBackupAgentData) {
    // Byte code:
    //   0: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   3: astore_2
    //   4: aload_1
    //   5: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   8: getfield packageName : Ljava/lang/String;
    //   11: astore_3
    //   12: invokestatic myUserId : ()I
    //   15: istore #4
    //   17: aload_2
    //   18: aload_3
    //   19: iconst_0
    //   20: iload #4
    //   22: invokeinterface getPackageInfo : (Ljava/lang/String;II)Landroid/content/pm/PackageInfo;
    //   27: astore_3
    //   28: aload_3
    //   29: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   32: getfield uid : I
    //   35: istore #5
    //   37: invokestatic myUid : ()I
    //   40: istore #4
    //   42: iload #5
    //   44: iload #4
    //   46: if_icmpeq -> 88
    //   49: new java/lang/StringBuilder
    //   52: astore_3
    //   53: aload_3
    //   54: invokespecial <init> : ()V
    //   57: aload_3
    //   58: ldc_w 'Asked to instantiate non-matching package '
    //   61: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload_3
    //   66: aload_1
    //   67: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   70: getfield packageName : Ljava/lang/String;
    //   73: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: pop
    //   77: ldc 'ActivityThread'
    //   79: aload_3
    //   80: invokevirtual toString : ()Ljava/lang/String;
    //   83: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   86: pop
    //   87: return
    //   88: aload_0
    //   89: invokevirtual unscheduleGcIdler : ()V
    //   92: aload_0
    //   93: aload_1
    //   94: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   97: aload_1
    //   98: getfield compatInfo : Landroid/content/res/CompatibilityInfo;
    //   101: invokevirtual getPackageInfoNoCheck : (Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;
    //   104: astore #6
    //   106: aload #6
    //   108: getfield mPackageName : Ljava/lang/String;
    //   111: astore #7
    //   113: aload #7
    //   115: ifnonnull -> 128
    //   118: ldc 'ActivityThread'
    //   120: ldc_w 'Asked to create backup agent for nonexistent package'
    //   123: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   126: pop
    //   127: return
    //   128: aload_1
    //   129: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   132: getfield backupAgentName : Ljava/lang/String;
    //   135: astore_3
    //   136: aload_3
    //   137: astore_2
    //   138: aload_3
    //   139: ifnonnull -> 164
    //   142: aload_1
    //   143: getfield backupMode : I
    //   146: iconst_1
    //   147: if_icmpeq -> 160
    //   150: aload_3
    //   151: astore_2
    //   152: aload_1
    //   153: getfield backupMode : I
    //   156: iconst_3
    //   157: if_icmpne -> 164
    //   160: ldc_w 'android.app.backup.FullBackupAgent'
    //   163: astore_2
    //   164: aconst_null
    //   165: astore #8
    //   167: aload_0
    //   168: aload_1
    //   169: getfield userId : I
    //   172: invokespecial getBackupAgentsForUser : (I)Landroid/util/ArrayMap;
    //   175: astore #9
    //   177: aload #9
    //   179: aload #7
    //   181: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   184: checkcast android/app/backup/BackupAgent
    //   187: astore_3
    //   188: aload_3
    //   189: ifnull -> 200
    //   192: aload_3
    //   193: invokevirtual onBind : ()Landroid/os/IBinder;
    //   196: astore_3
    //   197: goto -> 364
    //   200: aload #8
    //   202: astore_3
    //   203: aload #6
    //   205: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   208: astore #10
    //   210: aload #8
    //   212: astore_3
    //   213: aload #10
    //   215: aload_2
    //   216: invokevirtual loadClass : (Ljava/lang/String;)Ljava/lang/Class;
    //   219: invokevirtual newInstance : ()Ljava/lang/Object;
    //   222: checkcast android/app/backup/BackupAgent
    //   225: astore #10
    //   227: aload #8
    //   229: astore_3
    //   230: aload_0
    //   231: aload #6
    //   233: invokestatic createAppContext : (Landroid/app/ActivityThread;Landroid/app/LoadedApk;)Landroid/app/ContextImpl;
    //   236: astore #6
    //   238: aload #8
    //   240: astore_3
    //   241: aload #6
    //   243: aload #10
    //   245: invokevirtual setOuterContext : (Landroid/content/Context;)V
    //   248: aload #8
    //   250: astore_3
    //   251: aload #10
    //   253: aload #6
    //   255: invokevirtual attach : (Landroid/content/Context;)V
    //   258: aload #8
    //   260: astore_3
    //   261: aload #10
    //   263: aload_1
    //   264: getfield userId : I
    //   267: invokestatic of : (I)Landroid/os/UserHandle;
    //   270: invokevirtual onCreate : (Landroid/os/UserHandle;)V
    //   273: aload #8
    //   275: astore_3
    //   276: aload #10
    //   278: invokevirtual onBind : ()Landroid/os/IBinder;
    //   281: astore #8
    //   283: aload #8
    //   285: astore_3
    //   286: aload #9
    //   288: aload #7
    //   290: aload #10
    //   292: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   295: pop
    //   296: aload #8
    //   298: astore_3
    //   299: goto -> 364
    //   302: astore #8
    //   304: new java/lang/StringBuilder
    //   307: astore #9
    //   309: aload #9
    //   311: invokespecial <init> : ()V
    //   314: aload #9
    //   316: ldc_w 'Agent threw during creation: '
    //   319: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   322: pop
    //   323: aload #9
    //   325: aload #8
    //   327: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   330: pop
    //   331: ldc 'ActivityThread'
    //   333: aload #9
    //   335: invokevirtual toString : ()Ljava/lang/String;
    //   338: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   341: pop
    //   342: aload_1
    //   343: getfield backupMode : I
    //   346: iconst_2
    //   347: if_icmpeq -> 364
    //   350: aload_1
    //   351: getfield backupMode : I
    //   354: iconst_3
    //   355: if_icmpne -> 361
    //   358: goto -> 364
    //   361: aload #8
    //   363: athrow
    //   364: invokestatic getService : ()Landroid/app/IActivityManager;
    //   367: aload #7
    //   369: aload_3
    //   370: aload_1
    //   371: getfield userId : I
    //   374: invokeinterface backupAgentCreated : (Ljava/lang/String;Landroid/os/IBinder;I)V
    //   379: return
    //   380: astore_1
    //   381: aload_1
    //   382: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   385: athrow
    //   386: astore_3
    //   387: new java/lang/StringBuilder
    //   390: dup
    //   391: invokespecial <init> : ()V
    //   394: astore_1
    //   395: aload_1
    //   396: ldc_w 'Unable to create BackupAgent '
    //   399: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   402: pop
    //   403: aload_1
    //   404: aload_2
    //   405: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   408: pop
    //   409: aload_1
    //   410: ldc_w ': '
    //   413: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   416: pop
    //   417: aload_1
    //   418: aload_3
    //   419: invokevirtual toString : ()Ljava/lang/String;
    //   422: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: new java/lang/RuntimeException
    //   429: dup
    //   430: aload_1
    //   431: invokevirtual toString : ()Ljava/lang/String;
    //   434: aload_3
    //   435: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   438: athrow
    //   439: astore_1
    //   440: aload_1
    //   441: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   444: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4362	-> 0
    //   #4363	-> 12
    //   #4362	-> 17
    //   #4364	-> 28
    //   #4365	-> 49
    //   #4367	-> 87
    //   #4371	-> 88
    //   #4374	-> 88
    //   #4377	-> 92
    //   #4378	-> 106
    //   #4379	-> 113
    //   #4380	-> 118
    //   #4381	-> 127
    //   #4384	-> 128
    //   #4386	-> 136
    //   #4388	-> 160
    //   #4392	-> 164
    //   #4393	-> 167
    //   #4394	-> 177
    //   #4395	-> 188
    //   #4400	-> 192
    //   #4405	-> 200
    //   #4406	-> 210
    //   #4409	-> 227
    //   #4410	-> 238
    //   #4411	-> 248
    //   #4413	-> 258
    //   #4414	-> 273
    //   #4415	-> 283
    //   #4426	-> 296
    //   #4416	-> 302
    //   #4419	-> 304
    //   #4420	-> 342
    //   #4423	-> 361
    //   #4431	-> 364
    //   #4434	-> 379
    //   #4438	-> 379
    //   #4439	-> 379
    //   #4432	-> 380
    //   #4433	-> 381
    //   #4435	-> 386
    //   #4436	-> 387
    //   #4437	-> 417
    //   #4369	-> 439
    //   #4370	-> 440
    // Exception table:
    //   from	to	target	type
    //   0	12	439	android/os/RemoteException
    //   12	17	439	android/os/RemoteException
    //   17	28	439	android/os/RemoteException
    //   28	42	439	android/os/RemoteException
    //   49	87	439	android/os/RemoteException
    //   167	177	386	java/lang/Exception
    //   177	188	386	java/lang/Exception
    //   192	197	386	java/lang/Exception
    //   203	210	302	java/lang/Exception
    //   213	227	302	java/lang/Exception
    //   230	238	302	java/lang/Exception
    //   241	248	302	java/lang/Exception
    //   251	258	302	java/lang/Exception
    //   261	273	302	java/lang/Exception
    //   276	283	302	java/lang/Exception
    //   286	296	302	java/lang/Exception
    //   304	342	386	java/lang/Exception
    //   342	358	386	java/lang/Exception
    //   361	364	386	java/lang/Exception
    //   364	379	380	android/os/RemoteException
    //   364	379	386	java/lang/Exception
    //   381	386	386	java/lang/Exception
  }
  
  private void handleDestroyBackupAgent(CreateBackupAgentData paramCreateBackupAgentData) {
    LoadedApk loadedApk = getPackageInfoNoCheck(paramCreateBackupAgentData.appInfo, paramCreateBackupAgentData.compatInfo);
    String str = loadedApk.mPackageName;
    ArrayMap<String, BackupAgent> arrayMap = getBackupAgentsForUser(paramCreateBackupAgentData.userId);
    BackupAgent backupAgent = (BackupAgent)arrayMap.get(str);
    if (backupAgent != null) {
      try {
        backupAgent.onDestroy();
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exception thrown in onDestroy by backup agent of ");
        stringBuilder.append(paramCreateBackupAgentData.appInfo);
        Slog.w("ActivityThread", stringBuilder.toString());
        exception.printStackTrace();
      } 
      arrayMap.remove(str);
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Attempt to destroy unknown backup agent ");
      stringBuilder.append(paramCreateBackupAgentData);
      Slog.w("ActivityThread", stringBuilder.toString());
    } 
  }
  
  private ArrayMap<String, BackupAgent> getBackupAgentsForUser(int paramInt) {
    ArrayMap<String, BackupAgent> arrayMap1 = (ArrayMap)this.mBackupAgentsByUser.get(paramInt);
    ArrayMap<String, BackupAgent> arrayMap2 = arrayMap1;
    if (arrayMap1 == null) {
      arrayMap2 = new ArrayMap();
      this.mBackupAgentsByUser.put(paramInt, arrayMap2);
    } 
    return arrayMap2;
  }
  
  private void handleCreateService(CreateServiceData paramCreateServiceData) {
    unscheduleGcIdler();
    LoadedApk loadedApk = getPackageInfoNoCheck(paramCreateServiceData.info.applicationInfo, paramCreateServiceData.compatInfo);
    Service service1 = null;
    Service service2 = service1;
    try {
      if (localLOGV) {
        service2 = service1;
        StringBuilder stringBuilder = new StringBuilder();
        service2 = service1;
        this();
        service2 = service1;
        stringBuilder.append("Creating service ");
        service2 = service1;
        stringBuilder.append(paramCreateServiceData.info.name);
        service2 = service1;
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      service2 = service1;
      ContextImpl contextImpl = ContextImpl.createAppContext(this, loadedApk);
      service2 = service1;
      Application application = loadedApk.makeApplication(false, this.mInstrumentation);
      service2 = service1;
      ClassLoader classLoader = loadedApk.getClassLoader();
      service2 = service1;
      AppComponentFactory appComponentFactory = loadedApk.getAppFactory();
      service2 = service1;
      String str = paramCreateServiceData.info.name;
      service2 = service1;
      Intent intent = paramCreateServiceData.intent;
      service2 = service1;
      service1 = appComponentFactory.instantiateService(classLoader, str, intent);
      service2 = service1;
      Resources resources = contextImpl.getResources();
      service2 = service1;
      ResourcesLoader[] arrayOfResourcesLoader = application.getResources().getLoaders().<ResourcesLoader>toArray(new ResourcesLoader[0]);
      service2 = service1;
      resources.addLoaders(arrayOfResourcesLoader);
      service2 = service1;
      contextImpl.setOuterContext(service1);
      service2 = service1;
      str = paramCreateServiceData.info.name;
      service2 = service1;
      IBinder iBinder = paramCreateServiceData.token;
      service2 = service1;
      IActivityManager iActivityManager = ActivityManager.getService();
      service2 = service1;
      service1.attach(contextImpl, this, str, iBinder, application, iActivityManager);
      service2 = service1;
      service1.onCreate();
      service2 = service1;
      this.mServices.put(paramCreateServiceData.token, service1);
      service2 = service1;
      try {
        ActivityManager.getService().serviceDoneExecuting(paramCreateServiceData.token, 0, 0, 0);
      } catch (RemoteException remoteException) {
        service2 = service1;
        throw remoteException.rethrowFromSystemServer();
      } 
    } catch (Exception exception) {
      if (!this.mInstrumentation.onException(service2, exception)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to create service ");
        stringBuilder.append(paramCreateServiceData.info.name);
        stringBuilder.append(": ");
        stringBuilder.append(exception.toString());
        throw new RuntimeException(stringBuilder.toString(), exception);
      } 
    } 
  }
  
  private void handleBindService(BindServiceData paramBindServiceData) {
    Service service = (Service)this.mServices.get(paramBindServiceData.token);
    if (DEBUG_SERVICE) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("handleBindService s=");
      stringBuilder.append(service);
      stringBuilder.append(" rebind=");
      stringBuilder.append(paramBindServiceData.rebind);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (service != null)
      try {
        paramBindServiceData.intent.setExtrasClassLoader(service.getClassLoader());
        paramBindServiceData.intent.prepareToEnterProcess();
        try {
          if (!paramBindServiceData.rebind) {
            IBinder iBinder = service.onBind(paramBindServiceData.intent);
            ActivityManager.getService().publishService(paramBindServiceData.token, paramBindServiceData.intent, iBinder);
          } else {
            service.onRebind(paramBindServiceData.intent);
            ActivityManager.getService().serviceDoneExecuting(paramBindServiceData.token, 0, 0, 0);
          } 
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } catch (Exception exception) {
        if (!this.mInstrumentation.onException(service, exception)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to bind to service ");
          stringBuilder.append(service);
          stringBuilder.append(" with ");
          stringBuilder.append(paramBindServiceData.intent);
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          throw new RuntimeException(stringBuilder.toString(), exception);
        } 
      }  
  }
  
  private void handleUnbindService(BindServiceData paramBindServiceData) {
    Service service = (Service)this.mServices.get(paramBindServiceData.token);
    if (service != null)
      try {
        paramBindServiceData.intent.setExtrasClassLoader(service.getClassLoader());
        paramBindServiceData.intent.prepareToEnterProcess();
        boolean bool = service.onUnbind(paramBindServiceData.intent);
        if (bool) {
          try {
            ActivityManager.getService().unbindFinished(paramBindServiceData.token, paramBindServiceData.intent, bool);
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          } 
        } else {
          ActivityManager.getService().serviceDoneExecuting(paramBindServiceData.token, 0, 0, 0);
        } 
      } catch (Exception exception) {
        if (!this.mInstrumentation.onException(service, exception)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to unbind to service ");
          stringBuilder.append(service);
          stringBuilder.append(" with ");
          stringBuilder.append(paramBindServiceData.intent);
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          throw new RuntimeException(stringBuilder.toString(), exception);
        } 
      }  
  }
  
  private void handleDumpService(DumpComponentInfo paramDumpComponentInfo) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskWrites();
    try {
      Service service = (Service)this.mServices.get(paramDumpComponentInfo.token);
      if (service != null) {
        FastPrintWriter fastPrintWriter = new FastPrintWriter();
        FileOutputStream fileOutputStream = new FileOutputStream();
        ParcelFileDescriptor parcelFileDescriptor = paramDumpComponentInfo.fd;
        this(parcelFileDescriptor.getFileDescriptor());
        this(fileOutputStream);
        service.dump(paramDumpComponentInfo.fd.getFileDescriptor(), (PrintWriter)fastPrintWriter, paramDumpComponentInfo.args);
        fastPrintWriter.flush();
      } 
      return;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)paramDumpComponentInfo.fd);
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private void handleDumpActivity(DumpComponentInfo paramDumpComponentInfo) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskWrites();
    try {
      ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramDumpComponentInfo.token);
      if (activityClientRecord != null && activityClientRecord.activity != null) {
        FastPrintWriter fastPrintWriter = new FastPrintWriter();
        FileOutputStream fileOutputStream = new FileOutputStream();
        ParcelFileDescriptor parcelFileDescriptor = paramDumpComponentInfo.fd;
        this(parcelFileDescriptor.getFileDescriptor());
        this(fileOutputStream);
        activityClientRecord.activity.dump(paramDumpComponentInfo.prefix, paramDumpComponentInfo.fd.getFileDescriptor(), (PrintWriter)fastPrintWriter, paramDumpComponentInfo.args);
        fastPrintWriter.flush();
      } 
      return;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)paramDumpComponentInfo.fd);
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private void handleDumpProvider(DumpComponentInfo paramDumpComponentInfo) {
    StrictMode.ThreadPolicy threadPolicy = StrictMode.allowThreadDiskWrites();
    try {
      ProviderClientRecord providerClientRecord = (ProviderClientRecord)this.mLocalProviders.get(paramDumpComponentInfo.token);
      if (providerClientRecord != null && providerClientRecord.mLocalProvider != null) {
        FastPrintWriter fastPrintWriter = new FastPrintWriter();
        FileOutputStream fileOutputStream = new FileOutputStream();
        ParcelFileDescriptor parcelFileDescriptor = paramDumpComponentInfo.fd;
        this(parcelFileDescriptor.getFileDescriptor());
        this(fileOutputStream);
        providerClientRecord.mLocalProvider.dump(paramDumpComponentInfo.fd.getFileDescriptor(), (PrintWriter)fastPrintWriter, paramDumpComponentInfo.args);
        fastPrintWriter.flush();
      } 
      return;
    } finally {
      IoUtils.closeQuietly((AutoCloseable)paramDumpComponentInfo.fd);
      StrictMode.setThreadPolicy(threadPolicy);
    } 
  }
  
  private void handleServiceArgs(ServiceArgsData paramServiceArgsData) {
    Service service = (Service)this.mServices.get(paramServiceArgsData.token);
    if (service != null)
      try {
        char c;
        if (paramServiceArgsData.args != null) {
          paramServiceArgsData.args.setExtrasClassLoader(service.getClassLoader());
          paramServiceArgsData.args.prepareToEnterProcess();
        } 
        if (!paramServiceArgsData.taskRemoved) {
          c = service.onStartCommand(paramServiceArgsData.args, paramServiceArgsData.flags, paramServiceArgsData.startId);
        } else {
          service.onTaskRemoved(paramServiceArgsData.args);
          c = 'Ϩ';
        } 
        QueuedWork.waitToFinish();
        try {
          ActivityManager.getService().serviceDoneExecuting(paramServiceArgsData.token, 1, paramServiceArgsData.startId, c);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } catch (Exception exception) {
        if (!this.mInstrumentation.onException(service, exception)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to start service ");
          stringBuilder.append(service);
          stringBuilder.append(" with ");
          stringBuilder.append(paramServiceArgsData.args);
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          throw new RuntimeException(stringBuilder.toString(), exception);
        } 
      }  
  }
  
  private void handleStopService(IBinder paramIBinder) {
    StringBuilder stringBuilder;
    Service service = (Service)this.mServices.remove(paramIBinder);
    if (service != null) {
      try {
        if (localLOGV) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Destroying service ");
          stringBuilder1.append(service);
          Slog.v("ActivityThread", stringBuilder1.toString());
        } 
        service.onDestroy();
        service.detachAndCleanUp();
        Context context = service.getBaseContext();
        if (context instanceof ContextImpl) {
          String str = service.getClassName();
          ((ContextImpl)context).scheduleFinalCleanup(str, "Service");
        } 
        QueuedWork.waitToFinish();
        try {
          ActivityManager.getService().serviceDoneExecuting(paramIBinder, 2, 0, 0);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } catch (Exception exception) {
        StringBuilder stringBuilder1;
        if (this.mInstrumentation.onException(service, exception)) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("handleStopService: exception for ");
          stringBuilder1.append(paramIBinder);
          Slog.i("ActivityThread", stringBuilder1.toString(), exception);
        } else {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unable to stop service ");
          stringBuilder.append(stringBuilder1);
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          throw new RuntimeException(stringBuilder.toString(), exception);
        } 
      } 
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("handleStopService: token=");
      stringBuilder1.append(stringBuilder);
      stringBuilder1.append(" not found.");
      Slog.i("ActivityThread", stringBuilder1.toString());
    } 
  }
  
  public ActivityClientRecord performResumeActivity(IBinder paramIBinder, boolean paramBoolean, String paramString) {
    Intent intent;
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Performing resume of ");
      stringBuilder.append(activityClientRecord);
      stringBuilder.append(" finished= ");
      if (activityClientRecord != null) {
        Boolean bool = Boolean.valueOf(activityClientRecord.activity.mFinished);
      } else {
        paramIBinder = null;
      } 
      stringBuilder.append(paramIBinder);
      String str = stringBuilder.toString();
      Slog.v("ActivityThread", str);
    } 
    if (activityClientRecord == null || activityClientRecord.activity.mFinished)
      return null; 
    if (activityClientRecord.getLifecycleState() == 3) {
      if (!paramBoolean) {
        IllegalStateException illegalStateException = new IllegalStateException("Trying to resume activity which is already resumed");
        Slog.e("ActivityThread", illegalStateException.getMessage(), illegalStateException);
        Slog.e("ActivityThread", activityClientRecord.getStateString());
      } 
      return null;
    } 
    if (paramBoolean) {
      activityClientRecord.hideForNow = false;
      activityClientRecord.activity.mStartedActivity = false;
    } 
    try {
      activityClientRecord.activity.onStateNotSaved();
      activityClientRecord.activity.mFragments.noteStateNotSaved();
      checkAndBlockForNetworkAccess();
      if (activityClientRecord.pendingIntents != null) {
        deliverNewIntents(activityClientRecord, activityClientRecord.pendingIntents);
        activityClientRecord.pendingIntents = null;
      } 
      if (activityClientRecord.pendingResults != null) {
        deliverResults(activityClientRecord, activityClientRecord.pendingResults, paramString);
        activityClientRecord.pendingResults = null;
      } 
      Trace.traceBegin(64L, "performResume");
      activityClientRecord.activity.performResume(activityClientRecord.startsNotResumed, paramString);
      activityClientRecord.state = null;
      activityClientRecord.persistentState = null;
      activityClientRecord.setState(3);
      reportTopResumedActivityChanged(activityClientRecord, activityClientRecord.isTopResumedActivity, "topWhenResuming");
    } catch (Exception exception) {
      paramBoolean = this.mInstrumentation.onException(activityClientRecord.activity, exception);
      if (!paramBoolean) {
        RuntimeException runtimeException = new RuntimeException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unable to resume activity ");
        intent = activityClientRecord.intent;
        stringBuilder.append(intent.getComponent().toShortString());
        stringBuilder.append(": ");
        stringBuilder.append(exception.toString());
        this(stringBuilder.toString(), exception);
        throw runtimeException;
      } 
    } finally {}
    Trace.traceEnd(64L);
    return (ActivityClientRecord)intent;
  }
  
  static final void cleanUpPendingRemoveWindows(ActivityClientRecord paramActivityClientRecord, boolean paramBoolean) {
    if (paramActivityClientRecord.mPreserveWindow && !paramBoolean)
      return; 
    if (paramActivityClientRecord.mPendingRemoveWindow != null) {
      WindowManager windowManager = paramActivityClientRecord.mPendingRemoveWindowManager;
      Window window = paramActivityClientRecord.mPendingRemoveWindow;
      View view = window.getDecorView();
      windowManager.removeViewImmediate(view);
      IBinder iBinder = paramActivityClientRecord.mPendingRemoveWindow.getDecorView().getWindowToken();
      if (iBinder != null) {
        WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
        Activity activity = paramActivityClientRecord.activity;
        String str = activity.getClass().getName();
        windowManagerGlobal.closeAll(iBinder, str, "Activity");
      } 
    } 
    paramActivityClientRecord.mPendingRemoveWindow = null;
    paramActivityClientRecord.mPendingRemoveWindowManager = null;
  }
  
  public void handleResumeActivity(IBinder paramIBinder, boolean paramBoolean1, boolean paramBoolean2, String paramString) {
    boolean bool;
    boolean bool1;
    unscheduleGcIdler();
    this.mSomeActivitiesChanged = true;
    acquirePerformance();
    ActivityClientRecord activityClientRecord = performResumeActivity(paramIBinder, paramBoolean1, paramString);
    if (activityClientRecord == null)
      return; 
    if (this.mActivitiesToBeDestroyed.containsKey(paramIBinder))
      return; 
    Activity activity = activityClientRecord.activity;
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Resume ");
      stringBuilder.append(activityClientRecord);
      stringBuilder.append(" started activity: ");
      stringBuilder.append(activity.mStartedActivity);
      stringBuilder.append(", hideForNow: ");
      stringBuilder.append(activityClientRecord.hideForNow);
      stringBuilder.append(", finished: ");
      stringBuilder.append(activity.mFinished);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (paramBoolean2) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    int j = activity.mStartedActivity ^ true;
    int i = j;
    if (j == 0)
      try {
        IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
        paramIBinder = activity.getActivityToken();
        bool = iActivityTaskManager.willActivityBeVisible(paramIBinder);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    if (activityClientRecord.window == null && !activity.mFinished && bool) {
      activityClientRecord.window = activityClientRecord.activity.getWindow();
      View view = activityClientRecord.window.getDecorView();
      view.setVisibility(4);
      WindowManager windowManager = activity.getWindowManager();
      WindowManager.LayoutParams layoutParams = activityClientRecord.window.getAttributes();
      activity.mDecor = view;
      layoutParams.type = 1;
      layoutParams.softInputMode |= bool1;
      if (activityClientRecord.mPreserveWindow) {
        activity.mWindowAdded = true;
        activityClientRecord.mPreserveWindow = false;
        ViewRootImpl viewRootImpl = view.getViewRootImpl();
        if (viewRootImpl != null)
          viewRootImpl.notifyChildRebuilt(); 
      } 
      if (activity.mVisibleFromClient)
        if (!activity.mWindowAdded) {
          activity.mWindowAdded = true;
          windowManager.addView(view, (ViewGroup.LayoutParams)layoutParams);
        } else {
          activity.onWindowAttributesChanged(layoutParams);
        }  
    } else if (!bool) {
      if (localLOGV) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Launch ");
        stringBuilder.append(activityClientRecord);
        stringBuilder.append(" mStartedActivity set");
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      activityClientRecord.hideForNow = true;
    } 
    cleanUpPendingRemoveWindows(activityClientRecord, false);
    if (!activityClientRecord.activity.mFinished && bool && activityClientRecord.activity.mDecor != null && !activityClientRecord.hideForNow) {
      WindowManager.LayoutParams layoutParams;
      if (activityClientRecord.newConfig != null) {
        performConfigurationChangedForActivity(activityClientRecord, activityClientRecord.newConfig);
        if (DEBUG_CONFIGURATION) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Resuming activity ");
          stringBuilder.append(activityClientRecord.activityInfo.name);
          stringBuilder.append(" with newConfig ");
          stringBuilder.append(activityClientRecord.activity.mCurrentConfig);
          Slog.v("ActivityThread", stringBuilder.toString());
        } 
        activityClientRecord.newConfig = null;
      } 
      if (localLOGV) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Resuming ");
        stringBuilder.append(activityClientRecord);
        stringBuilder.append(" with isForward=");
        stringBuilder.append(paramBoolean2);
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      ViewRootImpl viewRootImpl = activityClientRecord.window.getDecorView().getViewRootImpl();
      if (viewRootImpl != null) {
        layoutParams = viewRootImpl.mWindowAttributes;
      } else {
        layoutParams = activityClientRecord.window.getAttributes();
      } 
      if ((layoutParams.softInputMode & 0x100) != bool1) {
        layoutParams.softInputMode = layoutParams.softInputMode & 0xFFFFFEFF | bool1;
        if (activityClientRecord.activity.mVisibleFromClient) {
          WindowManager windowManager = activity.getWindowManager();
          View view = activityClientRecord.window.getDecorView();
          windowManager.updateViewLayout(view, (ViewGroup.LayoutParams)layoutParams);
        } 
      } 
      activityClientRecord.activity.mVisibleFromServer = true;
      this.mNumVisibleActivities++;
      if (activityClientRecord.activity.mVisibleFromClient)
        activityClientRecord.activity.makeVisible(); 
    } 
    if (OplusBenchHelper.isInBenchMode() && OplusBenchHelper.isAntutuApp(activity.getPackageName()))
      OppoBenchAppSwitchManager.getInstance(this.mInitialApplication.getApplicationContext()).registerBenchAppSwitchObserver(activity.getPackageName()); 
    activityClientRecord.nextIdle = this.mNewActivities;
    this.mNewActivities = activityClientRecord;
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Scheduling idle handler for ");
      stringBuilder.append(activityClientRecord);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    Looper.myQueue().addIdleHandler(new Idler());
  }
  
  public void handleTopResumedActivityChanged(IBinder paramIBinder, boolean paramBoolean, String paramString) {
    StringBuilder stringBuilder2;
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord == null || activityClientRecord.activity == null) {
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Not found target activity to report position change for token: ");
      stringBuilder2.append(paramIBinder);
      Slog.w("ActivityThread", stringBuilder2.toString());
      return;
    } 
    if (DEBUG_ORDER) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Received position change to top: ");
      stringBuilder.append(paramBoolean);
      stringBuilder.append(" for activity: ");
      stringBuilder.append(stringBuilder2);
      Slog.d("ActivityThread", stringBuilder.toString());
    } 
    if (((ActivityClientRecord)stringBuilder2).isTopResumedActivity != paramBoolean) {
      ((ActivityClientRecord)stringBuilder2).isTopResumedActivity = paramBoolean;
      if (stringBuilder2.getLifecycleState() == 3) {
        reportTopResumedActivityChanged((ActivityClientRecord)stringBuilder2, paramBoolean, "topStateChangedWhenResumed");
      } else if (DEBUG_ORDER) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Won't deliver top position change in state=");
        stringBuilder.append(stringBuilder2.getLifecycleState());
        Slog.d("ActivityThread", stringBuilder.toString());
      } 
      return;
    } 
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Activity top position already set to onTop=");
    stringBuilder1.append(paramBoolean);
    throw new IllegalStateException(stringBuilder1.toString());
  }
  
  private void reportTopResumedActivityChanged(ActivityClientRecord paramActivityClientRecord, boolean paramBoolean, String paramString) {
    if (paramActivityClientRecord.lastReportedTopResumedState != paramBoolean) {
      paramActivityClientRecord.lastReportedTopResumedState = paramBoolean;
      paramActivityClientRecord.activity.performTopResumedActivityChanged(paramBoolean, paramString);
    } 
  }
  
  public void handlePauseActivity(IBinder paramIBinder, boolean paramBoolean1, boolean paramBoolean2, int paramInt, PendingTransactionActions paramPendingTransactionActions, String paramString) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      if (paramBoolean2)
        performUserLeavingActivity(activityClientRecord); 
      Activity activity = activityClientRecord.activity;
      activity.mConfigChangeFlags |= paramInt;
      performPauseActivity(activityClientRecord, paramBoolean1, paramString, paramPendingTransactionActions);
      if (activityClientRecord.isPreHoneycomb())
        QueuedWork.waitToFinish(); 
      this.mSomeActivitiesChanged = true;
    } 
  }
  
  final void performUserLeavingActivity(ActivityClientRecord paramActivityClientRecord) {
    this.mInstrumentation.callActivityOnPictureInPictureRequested(paramActivityClientRecord.activity);
    this.mInstrumentation.callActivityOnUserLeaving(paramActivityClientRecord.activity);
  }
  
  final Bundle performPauseActivity(IBinder paramIBinder, boolean paramBoolean, String paramString, PendingTransactionActions paramPendingTransactionActions) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      Bundle bundle = performPauseActivity(activityClientRecord, paramBoolean, paramString, paramPendingTransactionActions);
    } else {
      activityClientRecord = null;
    } 
    return (Bundle)activityClientRecord;
  }
  
  private Bundle performPauseActivity(ActivityClientRecord paramActivityClientRecord, boolean paramBoolean, String paramString, PendingTransactionActions paramPendingTransactionActions) {
    ArrayMap<Activity, ArrayList<OnActivityPausedListener>> arrayMap;
    Bundle bundle;
    boolean bool = paramActivityClientRecord.paused;
    ArrayMap arrayMap1 = null;
    if (bool) {
      if (paramActivityClientRecord.activity.mFinished)
        return null; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Performing pause of activity that is not resumed: ");
      Intent intent = paramActivityClientRecord.intent;
      stringBuilder.append(intent.getComponent().toShortString());
      RuntimeException runtimeException = new RuntimeException(stringBuilder.toString());
      Slog.e("ActivityThread", runtimeException.getMessage(), runtimeException);
    } 
    boolean bool1 = true;
    if (paramBoolean)
      paramActivityClientRecord.activity.mFinished = true; 
    paramBoolean = paramActivityClientRecord.activity.mFinished;
    int i = 0;
    if (paramBoolean || !paramActivityClientRecord.isPreHoneycomb())
      bool1 = false; 
    if (bool1)
      callActivityOnSaveInstanceState(paramActivityClientRecord); 
    performPauseActivityIfNeeded(paramActivityClientRecord, paramString);
    synchronized (this.mOnPauseListeners) {
      ArrayList<OnActivityPausedListener> arrayList = (ArrayList)this.mOnPauseListeners.remove(paramActivityClientRecord.activity);
      if (arrayList != null)
        i = arrayList.size(); 
      for (byte b = 0; b < i; b++)
        ((OnActivityPausedListener)arrayList.get(b)).onPaused(paramActivityClientRecord.activity); 
      if (paramPendingTransactionActions != null) {
        bundle = paramPendingTransactionActions.getOldState();
      } else {
        arrayMap = null;
      } 
      if (arrayMap != null)
        if (paramActivityClientRecord.isPreHoneycomb())
          paramActivityClientRecord.state = (Bundle)arrayMap;  
      arrayMap = arrayMap1;
      if (bool1)
        bundle = paramActivityClientRecord.state; 
      return bundle;
    } 
  }
  
  private void performPauseActivityIfNeeded(ActivityClientRecord paramActivityClientRecord, String paramString) {
    if (paramActivityClientRecord.paused)
      return; 
    reportTopResumedActivityChanged(paramActivityClientRecord, false, "pausing");
    try {
      paramActivityClientRecord.activity.mCalled = false;
      this.mInstrumentation.callActivityOnPause(paramActivityClientRecord.activity);
      if (!paramActivityClientRecord.activity.mCalled) {
        SuperNotCalledException superNotCalledException1 = new SuperNotCalledException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Activity ");
        stringBuilder.append(safeToComponentShortString(paramActivityClientRecord.intent));
        stringBuilder.append(" did not call through to super.onPause()");
        this(stringBuilder.toString());
        throw superNotCalledException1;
      } 
      paramActivityClientRecord.setState(4);
      return;
    } catch (SuperNotCalledException superNotCalledException) {
      throw superNotCalledException;
    } catch (Exception exception) {
      Intent intent;
      if (!this.mInstrumentation.onException(((ActivityClientRecord)superNotCalledException).activity, exception)) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to pause activity ");
        intent = ((ActivityClientRecord)superNotCalledException).intent;
        stringBuilder.append(safeToComponentShortString(intent));
        stringBuilder.append(": ");
        stringBuilder.append(exception.toString());
        throw new RuntimeException(stringBuilder.toString(), exception);
      } 
      intent.setState(4);
      return;
    } 
  }
  
  final void performStopActivity(IBinder paramIBinder, boolean paramBoolean, String paramString) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    performStopActivityInner(activityClientRecord, (PendingTransactionActions.StopInfo)null, paramBoolean, false, paramString);
  }
  
  class ProviderRefCount {
    public final ActivityThread.ProviderClientRecord client;
    
    public final ContentProviderHolder holder;
    
    public boolean removePending;
    
    public int stableCount;
    
    public int unstableCount;
    
    ProviderRefCount(ActivityThread this$0, ActivityThread.ProviderClientRecord param1ProviderClientRecord, int param1Int1, int param1Int2) {
      this.holder = (ContentProviderHolder)this$0;
      this.client = param1ProviderClientRecord;
      this.stableCount = param1Int1;
      this.unstableCount = param1Int2;
    }
  }
  
  private void performStopActivityInner(ActivityClientRecord paramActivityClientRecord, PendingTransactionActions.StopInfo paramStopInfo, boolean paramBoolean1, boolean paramBoolean2, String paramString) {
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Performing stop of ");
      stringBuilder.append(paramActivityClientRecord);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (paramActivityClientRecord != null) {
      Intent intent;
      StringBuilder stringBuilder;
      if (paramActivityClientRecord.stopped) {
        if (paramActivityClientRecord.activity.mFinished)
          return; 
        if (!paramBoolean2) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Performing stop of activity that is already stopped: ");
          Intent intent1 = paramActivityClientRecord.intent;
          stringBuilder1.append(intent1.getComponent().toShortString());
          RuntimeException runtimeException = new RuntimeException(stringBuilder1.toString());
          Slog.e("ActivityThread", runtimeException.getMessage(), runtimeException);
          Slog.e("ActivityThread", paramActivityClientRecord.getStateString());
        } 
      } 
      performPauseActivityIfNeeded(paramActivityClientRecord, paramString);
      if (paramStopInfo != null)
        try {
          paramStopInfo.setDescription(paramActivityClientRecord.activity.onCreateDescription());
        } catch (Exception exception) {
          if (!this.mInstrumentation.onException(paramActivityClientRecord.activity, exception)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unable to save state of activity ");
            intent = paramActivityClientRecord.intent;
            stringBuilder.append(intent.getComponent().toShortString());
            stringBuilder.append(": ");
            stringBuilder.append(exception.toString());
            throw new RuntimeException(stringBuilder.toString(), exception);
          } 
        }  
      callActivityOnStop((ActivityClientRecord)intent, paramBoolean1, (String)stringBuilder);
    } 
  }
  
  private void callActivityOnStop(ActivityClientRecord paramActivityClientRecord, boolean paramBoolean, String paramString) {
    // Byte code:
    //   0: iload_2
    //   1: ifeq -> 34
    //   4: aload_1
    //   5: getfield activity : Landroid/app/Activity;
    //   8: getfield mFinished : Z
    //   11: ifne -> 34
    //   14: aload_1
    //   15: getfield state : Landroid/os/Bundle;
    //   18: ifnonnull -> 34
    //   21: aload_1
    //   22: invokestatic access$3900 : (Landroid/app/ActivityThread$ActivityClientRecord;)Z
    //   25: ifne -> 34
    //   28: iconst_1
    //   29: istore #4
    //   31: goto -> 37
    //   34: iconst_0
    //   35: istore #4
    //   37: aload_1
    //   38: invokestatic access$4000 : (Landroid/app/ActivityThread$ActivityClientRecord;)Z
    //   41: istore_2
    //   42: iload #4
    //   44: ifeq -> 56
    //   47: iload_2
    //   48: ifeq -> 56
    //   51: aload_0
    //   52: aload_1
    //   53: invokespecial callActivityOnSaveInstanceState : (Landroid/app/ActivityThread$ActivityClientRecord;)V
    //   56: aload_1
    //   57: getfield activity : Landroid/app/Activity;
    //   60: aload_1
    //   61: getfield mPreserveWindow : Z
    //   64: aload_3
    //   65: invokevirtual performStop : (ZLjava/lang/String;)V
    //   68: goto -> 89
    //   71: astore #5
    //   73: aload_0
    //   74: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   77: aload_1
    //   78: getfield activity : Landroid/app/Activity;
    //   81: aload #5
    //   83: invokevirtual onException : (Ljava/lang/Object;Ljava/lang/Throwable;)Z
    //   86: ifeq -> 109
    //   89: aload_1
    //   90: iconst_5
    //   91: invokevirtual setState : (I)V
    //   94: iload #4
    //   96: ifeq -> 108
    //   99: iload_2
    //   100: ifne -> 108
    //   103: aload_0
    //   104: aload_1
    //   105: invokespecial callActivityOnSaveInstanceState : (Landroid/app/ActivityThread$ActivityClientRecord;)V
    //   108: return
    //   109: new java/lang/StringBuilder
    //   112: dup
    //   113: invokespecial <init> : ()V
    //   116: astore_3
    //   117: aload_3
    //   118: ldc_w 'Unable to stop activity '
    //   121: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   124: pop
    //   125: aload_1
    //   126: getfield intent : Landroid/content/Intent;
    //   129: astore_1
    //   130: aload_3
    //   131: aload_1
    //   132: invokevirtual getComponent : ()Landroid/content/ComponentName;
    //   135: invokevirtual toShortString : ()Ljava/lang/String;
    //   138: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   141: pop
    //   142: aload_3
    //   143: ldc_w ': '
    //   146: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   149: pop
    //   150: aload_3
    //   151: aload #5
    //   153: invokevirtual toString : ()Ljava/lang/String;
    //   156: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   159: pop
    //   160: new java/lang/RuntimeException
    //   163: dup
    //   164: aload_3
    //   165: invokevirtual toString : ()Ljava/lang/String;
    //   168: aload #5
    //   170: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   173: athrow
    //   174: astore_1
    //   175: aload_1
    //   176: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5173	-> 0
    //   #5174	-> 21
    //   #5175	-> 37
    //   #5176	-> 42
    //   #5177	-> 51
    //   #5181	-> 56
    //   #5191	-> 68
    //   #5184	-> 71
    //   #5185	-> 73
    //   #5192	-> 89
    //   #5194	-> 94
    //   #5195	-> 103
    //   #5197	-> 108
    //   #5186	-> 109
    //   #5188	-> 130
    //   #5189	-> 150
    //   #5182	-> 174
    //   #5183	-> 175
    // Exception table:
    //   from	to	target	type
    //   56	68	174	android/util/SuperNotCalledException
    //   56	68	71	java/lang/Exception
  }
  
  private void updateVisibility(ActivityClientRecord paramActivityClientRecord, boolean paramBoolean) {
    View view = paramActivityClientRecord.activity.mDecor;
    if (view != null) {
      StringBuilder stringBuilder;
      if (paramBoolean) {
        if (!paramActivityClientRecord.activity.mVisibleFromServer) {
          paramActivityClientRecord.activity.mVisibleFromServer = true;
          this.mNumVisibleActivities++;
          if (paramActivityClientRecord.activity.mVisibleFromClient)
            paramActivityClientRecord.activity.makeVisible(); 
        } 
        if (paramActivityClientRecord.newConfig != null) {
          performConfigurationChangedForActivity(paramActivityClientRecord, paramActivityClientRecord.newConfig);
          if (DEBUG_CONFIGURATION) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Updating activity vis ");
            stringBuilder.append(paramActivityClientRecord.activityInfo.name);
            stringBuilder.append(" with new config ");
            stringBuilder.append(paramActivityClientRecord.activity.mCurrentConfig);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          paramActivityClientRecord.newConfig = null;
        } 
      } else if (paramActivityClientRecord.activity.mVisibleFromServer) {
        paramActivityClientRecord.activity.mVisibleFromServer = false;
        this.mNumVisibleActivities--;
        stringBuilder.setVisibility(4);
      } 
    } 
  }
  
  public void handleStopActivity(IBinder paramIBinder, int paramInt, PendingTransactionActions paramPendingTransactionActions, boolean paramBoolean, String paramString) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    Activity activity = activityClientRecord.activity;
    activity.mConfigChangeFlags |= paramInt;
    PendingTransactionActions.StopInfo stopInfo = new PendingTransactionActions.StopInfo();
    performStopActivityInner(activityClientRecord, stopInfo, true, paramBoolean, paramString);
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Finishing stop of ");
      stringBuilder.append(activityClientRecord);
      stringBuilder.append(": win=");
      stringBuilder.append(activityClientRecord.window);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    updateVisibility(activityClientRecord, false);
    if (!activityClientRecord.isPreHoneycomb())
      QueuedWork.waitToFinish(); 
    stopInfo.setActivity(activityClientRecord);
    stopInfo.setState(activityClientRecord.state);
    stopInfo.setPersistentState(activityClientRecord.persistentState);
    paramPendingTransactionActions.setStopInfo(stopInfo);
    this.mSomeActivitiesChanged = true;
  }
  
  public void reportStop(PendingTransactionActions paramPendingTransactionActions) {
    this.mH.post(paramPendingTransactionActions.getStopInfo());
  }
  
  public void performRestartActivity(IBinder paramIBinder, boolean paramBoolean) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord.stopped) {
      activityClientRecord.activity.performRestart(paramBoolean, "performRestartActivity");
      if (paramBoolean)
        activityClientRecord.setState(2); 
    } 
  }
  
  private void handleSetCoreSettings(Bundle paramBundle) {
    synchronized (this.mResourcesManager) {
      this.mCoreSettings = paramBundle;
      onCoreSettingsChange();
      return;
    } 
  }
  
  private void onCoreSettingsChange() {
    if (updateDebugViewAttributeState())
      relaunchAllActivities(false); 
  }
  
  private boolean updateDebugViewAttributeState() {
    // Byte code:
    //   0: getstatic android/view/View.sDebugViewAttributes : Z
    //   3: istore_1
    //   4: aload_0
    //   5: getfield mCoreSettings : Landroid/os/Bundle;
    //   8: ldc_w 'debug_view_attributes_application_package'
    //   11: ldc_w ''
    //   14: invokevirtual getString : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   17: putstatic android/view/View.sDebugViewAttributesApplicationPackage : Ljava/lang/String;
    //   20: aload_0
    //   21: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnull -> 50
    //   29: aload_2
    //   30: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   33: ifnull -> 50
    //   36: aload_0
    //   37: getfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   40: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   43: getfield packageName : Ljava/lang/String;
    //   46: astore_2
    //   47: goto -> 54
    //   50: ldc_w '<unknown-app>'
    //   53: astore_2
    //   54: aload_0
    //   55: getfield mCoreSettings : Landroid/os/Bundle;
    //   58: astore_3
    //   59: iconst_0
    //   60: istore #4
    //   62: aload_3
    //   63: ldc_w 'debug_view_attributes'
    //   66: iconst_0
    //   67: invokevirtual getInt : (Ljava/lang/String;I)I
    //   70: ifne -> 94
    //   73: getstatic android/view/View.sDebugViewAttributesApplicationPackage : Ljava/lang/String;
    //   76: astore_3
    //   77: aload_3
    //   78: aload_2
    //   79: invokevirtual equals : (Ljava/lang/Object;)Z
    //   82: ifeq -> 88
    //   85: goto -> 94
    //   88: iconst_0
    //   89: istore #5
    //   91: goto -> 97
    //   94: iconst_1
    //   95: istore #5
    //   97: iload #5
    //   99: putstatic android/view/View.sDebugViewAttributes : Z
    //   102: iload #4
    //   104: istore #5
    //   106: iload_1
    //   107: getstatic android/view/View.sDebugViewAttributes : Z
    //   110: if_icmpeq -> 116
    //   113: iconst_1
    //   114: istore #5
    //   116: iload #5
    //   118: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5291	-> 0
    //   #5293	-> 4
    //   #5295	-> 20
    //   #5296	-> 36
    //   #5297	-> 54
    //   #5298	-> 59
    //   #5299	-> 77
    //   #5300	-> 102
  }
  
  private void relaunchAllActivities(boolean paramBoolean) {
    for (Map.Entry entry : this.mActivities.entrySet()) {
      ActivityClientRecord activityClientRecord = (ActivityClientRecord)entry.getValue();
      if (!activityClientRecord.activity.mFinished) {
        if (paramBoolean && activityClientRecord.window != null)
          activityClientRecord.mPreserveWindow = true; 
        scheduleRelaunchActivity((IBinder)entry.getKey());
      } 
    } 
  }
  
  private void handleUpdatePackageCompatibilityInfo(UpdateCompatibilityData paramUpdateCompatibilityData) {
    LoadedApk loadedApk = peekPackageInfo(paramUpdateCompatibilityData.pkg, false);
    if (loadedApk != null)
      loadedApk.setCompatibilityInfo(paramUpdateCompatibilityData.info); 
    loadedApk = peekPackageInfo(paramUpdateCompatibilityData.pkg, true);
    if (loadedApk != null)
      loadedApk.setCompatibilityInfo(paramUpdateCompatibilityData.info); 
    handleConfigurationChanged(this.mConfiguration, paramUpdateCompatibilityData.info);
    WindowManagerGlobal.getInstance().reportNewConfiguration(this.mConfiguration);
  }
  
  private void deliverResults(ActivityClientRecord paramActivityClientRecord, List<ResultInfo> paramList, String paramString) {
    int i = paramList.size();
    for (byte b = 0; b < i; b++) {
      ResultInfo resultInfo = paramList.get(b);
      try {
        if (resultInfo.mData != null) {
          resultInfo.mData.setExtrasClassLoader(paramActivityClientRecord.activity.getClassLoader());
          resultInfo.mData.prepareToEnterProcess();
        } 
        paramActivityClientRecord.activity.dispatchActivityResult(resultInfo.mResultWho, resultInfo.mRequestCode, resultInfo.mResultCode, resultInfo.mData, paramString);
      } catch (Exception exception) {
        if (!this.mInstrumentation.onException(paramActivityClientRecord.activity, exception)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failure delivering result ");
          stringBuilder.append(resultInfo);
          stringBuilder.append(" to activity ");
          Intent intent = paramActivityClientRecord.intent;
          stringBuilder.append(intent.getComponent().toShortString());
          stringBuilder.append(": ");
          stringBuilder.append(exception.toString());
          throw new RuntimeException(stringBuilder.toString(), exception);
        } 
      } 
    } 
  }
  
  public void handleSendResult(IBinder paramIBinder, List<ResultInfo> paramList, String paramString) {
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord != null) {
      Intent intent;
      StringBuilder stringBuilder;
      int i = activityClientRecord.paused ^ true;
      if (!activityClientRecord.activity.mFinished && activityClientRecord.activity.mDecor != null && activityClientRecord.hideForNow && i != 0)
        updateVisibility(activityClientRecord, true); 
      if (i != 0)
        try {
          activityClientRecord.activity.mCalled = false;
          this.mInstrumentation.callActivityOnPause(activityClientRecord.activity);
          if (!activityClientRecord.activity.mCalled) {
            SuperNotCalledException superNotCalledException1 = new SuperNotCalledException();
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Activity ");
            Intent intent1 = activityClientRecord.intent;
            stringBuilder1.append(intent1.getComponent().toShortString());
            stringBuilder1.append(" did not call through to super.onPause()");
            this(stringBuilder1.toString());
            throw superNotCalledException1;
          } 
        } catch (SuperNotCalledException superNotCalledException) {
          throw superNotCalledException;
        } catch (Exception exception) {
          if (!this.mInstrumentation.onException(((ActivityClientRecord)superNotCalledException).activity, exception)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unable to pause activity ");
            intent = ((ActivityClientRecord)superNotCalledException).intent;
            stringBuilder.append(intent.getComponent().toShortString());
            stringBuilder.append(": ");
            stringBuilder.append(exception.toString());
            throw new RuntimeException(stringBuilder.toString(), exception);
          } 
        }  
      checkAndBlockForNetworkAccess();
      deliverResults((ActivityClientRecord)intent, (List<ResultInfo>)stringBuilder, paramString);
      if (i != 0)
        ((ActivityClientRecord)intent).activity.performResume(false, paramString); 
    } 
  }
  
  ActivityClientRecord performDestroyActivity(IBinder paramIBinder, boolean paramBoolean1, int paramInt, boolean paramBoolean2, String paramString) {
    Intent intent;
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    paramString = null;
    if (localLOGV) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Performing finish of ");
      stringBuilder.append(activityClientRecord);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (activityClientRecord != null) {
      Class<?> clazz = activityClientRecord.activity.getClass();
      Activity activity = activityClientRecord.activity;
      activity.mConfigChangeFlags |= paramInt;
      if (paramBoolean1)
        activityClientRecord.activity.mFinished = true; 
      performPauseActivityIfNeeded(activityClientRecord, "destroy");
      if (!activityClientRecord.stopped)
        callActivityOnStop(activityClientRecord, false, "destroy"); 
      if (paramBoolean2)
        try {
          activity = activityClientRecord.activity;
          activityClientRecord.lastNonConfigurationInstances = activity.retainNonConfigurationInstances();
        } catch (Exception exception) {
          if (!this.mInstrumentation.onException(activityClientRecord.activity, exception)) {
            null = new StringBuilder();
            null.append("Unable to retain activity ");
            intent = activityClientRecord.intent;
            null.append(intent.getComponent().toShortString());
            null.append(": ");
            null.append(exception.toString());
            throw new RuntimeException(null.toString(), exception);
          } 
        }  
      try {
        activityClientRecord.activity.mCalled = false;
        this.mInstrumentation.callActivityOnDestroy(activityClientRecord.activity);
        if (activityClientRecord.activity.mCalled) {
          if (activityClientRecord.window != null)
            activityClientRecord.window.closeAllPanels(); 
        } else {
          SuperNotCalledException superNotCalledException = new SuperNotCalledException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Activity ");
          Intent intent1 = activityClientRecord.intent;
          stringBuilder.append(safeToComponentShortString(intent1));
          stringBuilder.append(" did not call through to super.onDestroy()");
          this(stringBuilder.toString());
          throw superNotCalledException;
        } 
        activityClientRecord.setState(6);
      } catch (SuperNotCalledException superNotCalledException) {
        throw superNotCalledException;
      } catch (Exception exception) {
        if (!this.mInstrumentation.onException(activityClientRecord.activity, exception)) {
          null = new StringBuilder();
          null.append("Unable to destroy activity ");
          intent = activityClientRecord.intent;
          null.append(safeToComponentShortString(intent));
          null.append(": ");
          null.append(exception.toString());
          throw new RuntimeException(null.toString(), exception);
        } 
        activityClientRecord.setState(6);
      } 
    } 
    schedulePurgeIdler();
    synchronized (this.mResourcesManager) {
      this.mActivities.remove(null);
      StrictMode.decrementExpectedActivityCount((Class)intent);
      return activityClientRecord;
    } 
  }
  
  private static String safeToComponentShortString(Intent paramIntent) {
    String str;
    ComponentName componentName = paramIntent.getComponent();
    if (componentName == null) {
      str = "[Unknown]";
    } else {
      str = str.toShortString();
    } 
    return str;
  }
  
  public Map<IBinder, ClientTransactionItem> getActivitiesToBeDestroyed() {
    return this.mActivitiesToBeDestroyed;
  }
  
  public void handleDestroyActivity(IBinder paramIBinder, boolean paramBoolean1, int paramInt, boolean paramBoolean2, String paramString) {
    ActivityClientRecord activityClientRecord = performDestroyActivity(paramIBinder, paramBoolean1, paramInt, paramBoolean2, paramString);
    if (activityClientRecord != null) {
      cleanUpPendingRemoveWindows(activityClientRecord, paramBoolean1);
      WindowManager windowManager = activityClientRecord.activity.getWindowManager();
      View view = activityClientRecord.activity.mDecor;
      if (view != null) {
        WindowManagerGlobal windowManagerGlobal;
        if (activityClientRecord.activity.mVisibleFromServer)
          this.mNumVisibleActivities--; 
        IBinder iBinder = view.getWindowToken();
        if (activityClientRecord.activity.mWindowAdded)
          if (activityClientRecord.mPreserveWindow) {
            activityClientRecord.mPendingRemoveWindow = activityClientRecord.window;
            activityClientRecord.mPendingRemoveWindowManager = windowManager;
            activityClientRecord.window.clearContentView();
          } else {
            windowManager.removeViewImmediate(view);
          }  
        if (iBinder != null && activityClientRecord.mPendingRemoveWindow == null) {
          windowManagerGlobal = WindowManagerGlobal.getInstance();
          Activity activity = activityClientRecord.activity;
          String str = activity.getClass().getName();
          windowManagerGlobal.closeAll(iBinder, str, "Activity");
        } else if (activityClientRecord.mPendingRemoveWindow != null) {
          WindowManagerGlobal windowManagerGlobal1 = WindowManagerGlobal.getInstance();
          Activity activity = activityClientRecord.activity;
          String str = activity.getClass().getName();
          windowManagerGlobal1.closeAllExceptView(paramIBinder, (View)windowManagerGlobal, str, "Activity");
        } 
        activityClientRecord.activity.mDecor = null;
      } 
      if (activityClientRecord.mPendingRemoveWindow == null) {
        WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
        Activity activity = activityClientRecord.activity;
        String str = activity.getClass().getName();
        windowManagerGlobal.closeAll(paramIBinder, str, "Activity");
      } 
      Context context = activityClientRecord.activity.getBaseContext();
      if (context instanceof ContextImpl) {
        context = context;
        Activity activity = activityClientRecord.activity;
        String str = activity.getClass().getName();
        context.scheduleFinalCleanup(str, "Activity");
      } 
    } 
    if (paramBoolean1)
      try {
        ActivityTaskManager.getService().activityDestroyed(paramIBinder);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    this.mSomeActivitiesChanged = true;
  }
  
  public ActivityClientRecord prepareRelaunchActivity(IBinder paramIBinder, List<ResultInfo> paramList, List<ReferrerIntent> paramList1, int paramInt, MergedConfiguration paramMergedConfiguration, boolean paramBoolean) {
    // Byte code:
    //   0: aconst_null
    //   1: astore #7
    //   3: iconst_0
    //   4: istore #8
    //   6: aload_0
    //   7: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   10: astore #9
    //   12: aload #9
    //   14: monitorenter
    //   15: iconst_0
    //   16: istore #10
    //   18: aload #7
    //   20: astore #11
    //   22: iload #10
    //   24: aload_0
    //   25: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   28: invokevirtual size : ()I
    //   31: if_icmpge -> 208
    //   34: aload_0
    //   35: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   38: iload #10
    //   40: invokevirtual get : (I)Ljava/lang/Object;
    //   43: checkcast android/app/ActivityThread$ActivityClientRecord
    //   46: astore #12
    //   48: getstatic android/app/ActivityThread.DEBUG_ORDER : Z
    //   51: ifeq -> 108
    //   54: new java/lang/StringBuilder
    //   57: astore #11
    //   59: aload #11
    //   61: invokespecial <init> : ()V
    //   64: aload #11
    //   66: ldc_w 'requestRelaunchActivity: '
    //   69: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload #11
    //   75: aload_0
    //   76: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload #11
    //   82: ldc_w ', trying: '
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload #11
    //   91: aload #12
    //   93: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: ldc 'ActivityThread'
    //   99: aload #11
    //   101: invokevirtual toString : ()Ljava/lang/String;
    //   104: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   107: pop
    //   108: aload #12
    //   110: getfield token : Landroid/os/IBinder;
    //   113: aload_1
    //   114: if_acmpne -> 202
    //   117: aload #12
    //   119: astore #7
    //   121: aload_2
    //   122: ifnull -> 154
    //   125: aload #12
    //   127: getfield pendingResults : Ljava/util/List;
    //   130: ifnull -> 148
    //   133: aload #12
    //   135: getfield pendingResults : Ljava/util/List;
    //   138: aload_2
    //   139: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   144: pop
    //   145: goto -> 154
    //   148: aload #12
    //   150: aload_2
    //   151: putfield pendingResults : Ljava/util/List;
    //   154: aload #7
    //   156: astore #11
    //   158: aload_3
    //   159: ifnull -> 208
    //   162: aload #12
    //   164: getfield pendingIntents : Ljava/util/List;
    //   167: ifnull -> 189
    //   170: aload #12
    //   172: getfield pendingIntents : Ljava/util/List;
    //   175: aload_3
    //   176: invokeinterface addAll : (Ljava/util/Collection;)Z
    //   181: pop
    //   182: aload #7
    //   184: astore #11
    //   186: goto -> 208
    //   189: aload #12
    //   191: aload_3
    //   192: putfield pendingIntents : Ljava/util/List;
    //   195: aload #7
    //   197: astore #11
    //   199: goto -> 208
    //   202: iinc #10, 1
    //   205: goto -> 18
    //   208: aload #11
    //   210: astore #7
    //   212: iload #8
    //   214: istore #10
    //   216: aload #11
    //   218: ifnonnull -> 284
    //   221: getstatic android/app/ActivityThread.DEBUG_ORDER : Z
    //   224: ifeq -> 236
    //   227: ldc 'ActivityThread'
    //   229: ldc_w 'requestRelaunchActivity: target is null'
    //   232: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   235: pop
    //   236: new android/app/ActivityThread$ActivityClientRecord
    //   239: astore #7
    //   241: aload #7
    //   243: invokespecial <init> : ()V
    //   246: aload #7
    //   248: aload_1
    //   249: putfield token : Landroid/os/IBinder;
    //   252: aload #7
    //   254: aload_2
    //   255: putfield pendingResults : Ljava/util/List;
    //   258: aload #7
    //   260: aload_3
    //   261: putfield pendingIntents : Ljava/util/List;
    //   264: aload #7
    //   266: iload #6
    //   268: putfield mPreserveWindow : Z
    //   271: aload_0
    //   272: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   275: aload #7
    //   277: invokevirtual add : (Ljava/lang/Object;)Z
    //   280: pop
    //   281: iconst_1
    //   282: istore #10
    //   284: aload_0
    //   285: getfield mDebugOn : Z
    //   288: ifeq -> 342
    //   291: new java/lang/StringBuilder
    //   294: astore_1
    //   295: aload_1
    //   296: invokespecial <init> : ()V
    //   299: aload_1
    //   300: ldc_w 'prepareRelaunchActivity: '
    //   303: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload_1
    //   308: aload #5
    //   310: invokevirtual getOverrideConfiguration : ()Landroid/content/res/Configuration;
    //   313: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   316: pop
    //   317: aload_1
    //   318: ldc_w ' target '
    //   321: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   324: pop
    //   325: aload_1
    //   326: aload #7
    //   328: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   331: pop
    //   332: ldc 'ActivityThread'
    //   334: aload_1
    //   335: invokevirtual toString : ()Ljava/lang/String;
    //   338: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   341: pop
    //   342: aload #7
    //   344: aload #5
    //   346: invokevirtual getGlobalConfiguration : ()Landroid/content/res/Configuration;
    //   349: putfield createdConfig : Landroid/content/res/Configuration;
    //   352: aload #7
    //   354: aload #5
    //   356: invokevirtual getOverrideConfiguration : ()Landroid/content/res/Configuration;
    //   359: putfield overrideConfig : Landroid/content/res/Configuration;
    //   362: aload #7
    //   364: aload #7
    //   366: getfield pendingConfigChanges : I
    //   369: iload #4
    //   371: ior
    //   372: putfield pendingConfigChanges : I
    //   375: aload #9
    //   377: monitorexit
    //   378: iload #10
    //   380: ifeq -> 389
    //   383: aload #7
    //   385: astore_1
    //   386: goto -> 391
    //   389: aconst_null
    //   390: astore_1
    //   391: aload_1
    //   392: areturn
    //   393: astore_1
    //   394: aload #9
    //   396: monitorexit
    //   397: aload_1
    //   398: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5544	-> 0
    //   #5545	-> 3
    //   #5547	-> 6
    //   #5548	-> 15
    //   #5549	-> 34
    //   #5550	-> 48
    //   #5551	-> 108
    //   #5552	-> 117
    //   #5553	-> 121
    //   #5554	-> 125
    //   #5555	-> 133
    //   #5557	-> 148
    //   #5560	-> 154
    //   #5561	-> 162
    //   #5562	-> 170
    //   #5564	-> 189
    //   #5548	-> 202
    //   #5571	-> 208
    //   #5572	-> 221
    //   #5573	-> 236
    //   #5574	-> 246
    //   #5575	-> 252
    //   #5576	-> 258
    //   #5577	-> 264
    //   #5578	-> 271
    //   #5579	-> 281
    //   #5583	-> 284
    //   #5586	-> 342
    //   #5587	-> 352
    //   #5588	-> 362
    //   #5589	-> 375
    //   #5591	-> 378
    //   #5589	-> 393
    // Exception table:
    //   from	to	target	type
    //   22	34	393	finally
    //   34	48	393	finally
    //   48	108	393	finally
    //   108	117	393	finally
    //   125	133	393	finally
    //   133	145	393	finally
    //   148	154	393	finally
    //   162	170	393	finally
    //   170	182	393	finally
    //   189	195	393	finally
    //   221	236	393	finally
    //   236	246	393	finally
    //   246	252	393	finally
    //   252	258	393	finally
    //   258	264	393	finally
    //   264	271	393	finally
    //   271	281	393	finally
    //   284	342	393	finally
    //   342	352	393	finally
    //   352	362	393	finally
    //   362	375	393	finally
    //   375	378	393	finally
    //   394	397	393	finally
  }
  
  public void handleRelaunchActivity(ActivityClientRecord paramActivityClientRecord, PendingTransactionActions paramPendingTransactionActions) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual unscheduleGcIdler : ()V
    //   4: aload_0
    //   5: iconst_1
    //   6: putfield mSomeActivitiesChanged : Z
    //   9: aconst_null
    //   10: astore_3
    //   11: aload_0
    //   12: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   15: astore #4
    //   17: aload #4
    //   19: monitorenter
    //   20: aload_0
    //   21: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   24: invokevirtual size : ()I
    //   27: istore #5
    //   29: aload_1
    //   30: getfield token : Landroid/os/IBinder;
    //   33: astore #6
    //   35: iconst_0
    //   36: istore #7
    //   38: iconst_0
    //   39: istore #8
    //   41: aconst_null
    //   42: astore #9
    //   44: iload #7
    //   46: iload #5
    //   48: if_icmpge -> 159
    //   51: aload_0
    //   52: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   55: iload #7
    //   57: invokevirtual get : (I)Ljava/lang/Object;
    //   60: checkcast android/app/ActivityThread$ActivityClientRecord
    //   63: astore_1
    //   64: aload_1
    //   65: getfield token : Landroid/os/IBinder;
    //   68: astore #10
    //   70: iload #5
    //   72: istore #11
    //   74: iload #7
    //   76: istore #12
    //   78: iload #8
    //   80: istore #13
    //   82: aload #10
    //   84: aload #6
    //   86: if_acmpne -> 138
    //   89: aload_1
    //   90: getfield pendingConfigChanges : I
    //   93: istore #13
    //   95: aload_0
    //   96: getfield mRelaunchingActivities : Ljava/util/ArrayList;
    //   99: iload #7
    //   101: invokevirtual remove : (I)Ljava/lang/Object;
    //   104: pop
    //   105: iload #7
    //   107: iconst_1
    //   108: isub
    //   109: istore #12
    //   111: iload #5
    //   113: iconst_1
    //   114: isub
    //   115: istore #11
    //   117: aload_1
    //   118: astore #9
    //   120: iload #13
    //   122: iload #8
    //   124: ior
    //   125: istore #13
    //   127: goto -> 138
    //   130: astore_1
    //   131: goto -> 668
    //   134: astore_1
    //   135: goto -> 668
    //   138: iload #12
    //   140: iconst_1
    //   141: iadd
    //   142: istore #7
    //   144: iload #11
    //   146: istore #5
    //   148: iload #13
    //   150: istore #8
    //   152: goto -> 44
    //   155: astore_1
    //   156: goto -> 668
    //   159: aload #9
    //   161: ifnonnull -> 183
    //   164: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   167: ifeq -> 179
    //   170: ldc 'ActivityThread'
    //   172: ldc_w 'Abort, activity not relaunching!'
    //   175: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   178: pop
    //   179: aload #4
    //   181: monitorexit
    //   182: return
    //   183: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   186: ifeq -> 245
    //   189: new java/lang/StringBuilder
    //   192: astore_1
    //   193: aload_1
    //   194: invokespecial <init> : ()V
    //   197: aload_1
    //   198: ldc_w 'Relaunching activity '
    //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   204: pop
    //   205: aload_1
    //   206: aload #9
    //   208: getfield token : Landroid/os/IBinder;
    //   211: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   214: pop
    //   215: aload_1
    //   216: ldc_w ' with configChanges=0x'
    //   219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload_1
    //   224: iload #8
    //   226: invokestatic toHexString : (I)Ljava/lang/String;
    //   229: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   232: pop
    //   233: aload_1
    //   234: invokevirtual toString : ()Ljava/lang/String;
    //   237: astore_1
    //   238: ldc 'ActivityThread'
    //   240: aload_1
    //   241: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   244: pop
    //   245: aload_3
    //   246: astore_1
    //   247: aload_0
    //   248: getfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   251: ifnull -> 264
    //   254: aload_0
    //   255: getfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   258: astore_1
    //   259: aload_0
    //   260: aconst_null
    //   261: putfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   264: aload #4
    //   266: monitorexit
    //   267: aload #9
    //   269: getfield createdConfig : Landroid/content/res/Configuration;
    //   272: ifnull -> 351
    //   275: aload_0
    //   276: getfield mConfiguration : Landroid/content/res/Configuration;
    //   279: ifnull -> 324
    //   282: aload #9
    //   284: getfield createdConfig : Landroid/content/res/Configuration;
    //   287: astore_3
    //   288: aload_0
    //   289: getfield mConfiguration : Landroid/content/res/Configuration;
    //   292: astore #4
    //   294: aload_3
    //   295: aload #4
    //   297: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   300: ifeq -> 351
    //   303: aload_0
    //   304: getfield mConfiguration : Landroid/content/res/Configuration;
    //   307: astore_3
    //   308: aload #9
    //   310: getfield createdConfig : Landroid/content/res/Configuration;
    //   313: astore #4
    //   315: aload_3
    //   316: aload #4
    //   318: invokevirtual diff : (Landroid/content/res/Configuration;)I
    //   321: ifeq -> 351
    //   324: aload_1
    //   325: ifnull -> 342
    //   328: aload #9
    //   330: getfield createdConfig : Landroid/content/res/Configuration;
    //   333: astore_3
    //   334: aload_3
    //   335: aload_1
    //   336: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   339: ifeq -> 351
    //   342: aload #9
    //   344: getfield createdConfig : Landroid/content/res/Configuration;
    //   347: astore_1
    //   348: goto -> 351
    //   351: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   354: ifeq -> 407
    //   357: new java/lang/StringBuilder
    //   360: dup
    //   361: invokespecial <init> : ()V
    //   364: astore_3
    //   365: aload_3
    //   366: ldc_w 'Relaunching activity '
    //   369: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   372: pop
    //   373: aload_3
    //   374: aload #9
    //   376: getfield token : Landroid/os/IBinder;
    //   379: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   382: pop
    //   383: aload_3
    //   384: ldc_w ': changedConfig='
    //   387: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   390: pop
    //   391: aload_3
    //   392: aload_1
    //   393: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   396: pop
    //   397: ldc 'ActivityThread'
    //   399: aload_3
    //   400: invokevirtual toString : ()Ljava/lang/String;
    //   403: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   406: pop
    //   407: aload_1
    //   408: ifnull -> 429
    //   411: aload_0
    //   412: aload_1
    //   413: getfield densityDpi : I
    //   416: putfield mCurDefaultDisplayDpi : I
    //   419: aload_0
    //   420: invokespecial updateDefaultDensity : ()V
    //   423: aload_0
    //   424: aload_1
    //   425: aconst_null
    //   426: invokespecial handleConfigurationChanged : (Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)V
    //   429: aload_0
    //   430: getfield mActivities : Landroid/util/ArrayMap;
    //   433: aload #9
    //   435: getfield token : Landroid/os/IBinder;
    //   438: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   441: checkcast android/app/ActivityThread$ActivityClientRecord
    //   444: astore_1
    //   445: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   448: ifeq -> 483
    //   451: new java/lang/StringBuilder
    //   454: dup
    //   455: invokespecial <init> : ()V
    //   458: astore_3
    //   459: aload_3
    //   460: ldc_w 'Handling relaunch of '
    //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   466: pop
    //   467: aload_3
    //   468: aload_1
    //   469: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   472: pop
    //   473: ldc 'ActivityThread'
    //   475: aload_3
    //   476: invokevirtual toString : ()Ljava/lang/String;
    //   479: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   482: pop
    //   483: aload_1
    //   484: ifnonnull -> 488
    //   487: return
    //   488: aload_1
    //   489: getfield activity : Landroid/app/Activity;
    //   492: astore_3
    //   493: aload_3
    //   494: aload_3
    //   495: getfield mConfigChangeFlags : I
    //   498: iload #8
    //   500: ior
    //   501: putfield mConfigChangeFlags : I
    //   504: aload_1
    //   505: aload #9
    //   507: getfield mPreserveWindow : Z
    //   510: putfield mPreserveWindow : Z
    //   513: aload_1
    //   514: getfield activity : Landroid/app/Activity;
    //   517: iconst_1
    //   518: putfield mChangingConfigurations : Z
    //   521: aload_1
    //   522: getfield mPreserveWindow : Z
    //   525: ifeq -> 541
    //   528: invokestatic getWindowSession : ()Landroid/view/IWindowSession;
    //   531: aload_1
    //   532: getfield token : Landroid/os/IBinder;
    //   535: iconst_1
    //   536: invokeinterface prepareToReplaceWindows : (Landroid/os/IBinder;Z)V
    //   541: aload_0
    //   542: getfield mLastReportedWindowingMode : Ljava/util/Map;
    //   545: astore_3
    //   546: aload_1
    //   547: getfield activity : Landroid/app/Activity;
    //   550: astore #4
    //   552: aload #4
    //   554: invokevirtual getActivityToken : ()Landroid/os/IBinder;
    //   557: astore #4
    //   559: aload_3
    //   560: aload #4
    //   562: iconst_0
    //   563: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   566: invokeinterface getOrDefault : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   571: checkcast java/lang/Integer
    //   574: invokevirtual intValue : ()I
    //   577: istore #7
    //   579: aload_0
    //   580: aload_1
    //   581: iload #8
    //   583: aload #9
    //   585: getfield pendingResults : Ljava/util/List;
    //   588: aload #9
    //   590: getfield pendingIntents : Ljava/util/List;
    //   593: aload_2
    //   594: aload #9
    //   596: getfield startsNotResumed : Z
    //   599: aload #9
    //   601: getfield overrideConfig : Landroid/content/res/Configuration;
    //   604: ldc_w 'handleRelaunchActivity'
    //   607: invokespecial handleRelaunchActivityInner : (Landroid/app/ActivityThread$ActivityClientRecord;ILjava/util/List;Ljava/util/List;Landroid/app/servertransaction/PendingTransactionActions;ZLandroid/content/res/Configuration;Ljava/lang/String;)V
    //   610: aload_0
    //   611: getfield mLastReportedWindowingMode : Ljava/util/Map;
    //   614: aload_1
    //   615: getfield activity : Landroid/app/Activity;
    //   618: invokevirtual getActivityToken : ()Landroid/os/IBinder;
    //   621: iload #7
    //   623: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   626: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   631: pop
    //   632: aload_0
    //   633: aload_1
    //   634: getfield activity : Landroid/app/Activity;
    //   637: aload_1
    //   638: getfield activity : Landroid/app/Activity;
    //   641: getfield mCurrentConfig : Landroid/content/res/Configuration;
    //   644: invokespecial handleWindowingModeChangeIfNeeded : (Landroid/app/Activity;Landroid/content/res/Configuration;)V
    //   647: aload_2
    //   648: ifnull -> 656
    //   651: aload_2
    //   652: iconst_1
    //   653: invokevirtual setReportRelaunchToWindowManager : (Z)V
    //   656: return
    //   657: astore_1
    //   658: aload_1
    //   659: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   662: athrow
    //   663: astore_1
    //   664: goto -> 668
    //   667: astore_1
    //   668: aload #4
    //   670: monitorexit
    //   671: aload_1
    //   672: athrow
    //   673: astore_1
    //   674: goto -> 668
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5599	-> 0
    //   #5600	-> 4
    //   #5602	-> 9
    //   #5603	-> 11
    //   #5608	-> 11
    //   #5609	-> 20
    //   #5610	-> 29
    //   #5611	-> 35
    //   #5612	-> 35
    //   #5613	-> 51
    //   #5614	-> 64
    //   #5615	-> 89
    //   #5616	-> 89
    //   #5617	-> 95
    //   #5618	-> 105
    //   #5619	-> 111
    //   #5636	-> 130
    //   #5612	-> 138
    //   #5636	-> 155
    //   #5623	-> 159
    //   #5624	-> 164
    //   #5625	-> 179
    //   #5628	-> 183
    //   #5630	-> 223
    //   #5628	-> 238
    //   #5632	-> 245
    //   #5633	-> 254
    //   #5634	-> 259
    //   #5636	-> 264
    //   #5638	-> 267
    //   #5642	-> 275
    //   #5643	-> 294
    //   #5644	-> 315
    //   #5645	-> 324
    //   #5646	-> 334
    //   #5647	-> 342
    //   #5652	-> 351
    //   #5656	-> 407
    //   #5657	-> 411
    //   #5658	-> 419
    //   #5659	-> 423
    //   #5662	-> 429
    //   #5663	-> 445
    //   #5664	-> 483
    //   #5665	-> 487
    //   #5668	-> 488
    //   #5669	-> 504
    //   #5671	-> 513
    //   #5685	-> 521
    //   #5686	-> 528
    //   #5691	-> 541
    //   #5696	-> 541
    //   #5697	-> 552
    //   #5696	-> 559
    //   #5698	-> 579
    //   #5700	-> 610
    //   #5701	-> 632
    //   #5703	-> 647
    //   #5705	-> 651
    //   #5707	-> 656
    //   #5689	-> 657
    //   #5690	-> 658
    //   #5636	-> 663
    // Exception table:
    //   from	to	target	type
    //   20	29	667	finally
    //   29	35	663	finally
    //   51	64	155	finally
    //   64	70	155	finally
    //   89	95	134	finally
    //   95	105	130	finally
    //   164	179	155	finally
    //   179	182	155	finally
    //   183	223	155	finally
    //   223	238	155	finally
    //   238	245	155	finally
    //   247	254	155	finally
    //   254	259	155	finally
    //   259	264	155	finally
    //   264	267	155	finally
    //   521	528	657	android/os/RemoteException
    //   528	541	657	android/os/RemoteException
    //   668	671	673	finally
  }
  
  void scheduleRelaunchActivity(IBinder paramIBinder) {
    this.mH.removeMessages(160, paramIBinder);
    sendMessage(160, paramIBinder);
  }
  
  private void handleRelaunchActivityLocally(IBinder paramIBinder) {
    Configuration configuration;
    ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
    if (activityClientRecord == null) {
      Log.w("ActivityThread", "Activity to relaunch no longer exists");
      return;
    } 
    int i = activityClientRecord.getLifecycleState();
    if (i < 3 || i > 5) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Activity state must be in [ON_RESUME..ON_STOP] in order to be relaunched,current state is ");
      stringBuilder.append(i);
      Log.w("ActivityThread", stringBuilder.toString());
      return;
    } 
    if (activityClientRecord.createdConfig != null) {
      configuration = activityClientRecord.createdConfig;
    } else {
      configuration = this.mConfiguration;
    } 
    MergedConfiguration mergedConfiguration = new MergedConfiguration(configuration, activityClientRecord.overrideConfig);
    ActivityRelaunchItem activityRelaunchItem = ActivityRelaunchItem.obtain(null, null, 0, mergedConfiguration, activityClientRecord.mPreserveWindow);
    ActivityLifecycleItem activityLifecycleItem = TransactionExecutorHelper.getLifecycleRequestForCurrentState(activityClientRecord);
    ClientTransaction clientTransaction = ClientTransaction.obtain(this.mAppThread, activityClientRecord.token);
    clientTransaction.addCallback(activityRelaunchItem);
    clientTransaction.setLifecycleStateRequest(activityLifecycleItem);
    executeTransaction(clientTransaction);
  }
  
  private void handleRelaunchActivityInner(ActivityClientRecord paramActivityClientRecord, int paramInt, List<ResultInfo> paramList, List<ReferrerIntent> paramList1, PendingTransactionActions paramPendingTransactionActions, boolean paramBoolean, Configuration paramConfiguration, String paramString) {
    Intent intent = paramActivityClientRecord.activity.mIntent;
    if (!paramActivityClientRecord.paused)
      performPauseActivity(paramActivityClientRecord, false, paramString, (PendingTransactionActions)null); 
    if (!paramActivityClientRecord.stopped)
      callActivityOnStop(paramActivityClientRecord, true, paramString); 
    handleDestroyActivity(paramActivityClientRecord.token, false, paramInt, true, paramString);
    paramActivityClientRecord.activity = null;
    paramActivityClientRecord.window = null;
    paramActivityClientRecord.hideForNow = false;
    paramActivityClientRecord.nextIdle = null;
    if (paramList != null)
      if (paramActivityClientRecord.pendingResults == null) {
        paramActivityClientRecord.pendingResults = paramList;
      } else {
        paramActivityClientRecord.pendingResults.addAll(paramList);
      }  
    if (paramList1 != null)
      if (paramActivityClientRecord.pendingIntents == null) {
        paramActivityClientRecord.pendingIntents = paramList1;
      } else {
        paramActivityClientRecord.pendingIntents.addAll(paramList1);
      }  
    paramActivityClientRecord.startsNotResumed = paramBoolean;
    paramActivityClientRecord.overrideConfig = paramConfiguration;
    handleLaunchActivity(paramActivityClientRecord, paramPendingTransactionActions, intent);
  }
  
  public void reportRelaunch(IBinder paramIBinder, PendingTransactionActions paramPendingTransactionActions) {
    try {
      ActivityTaskManager.getService().activityRelaunched(paramIBinder);
      ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.get(paramIBinder);
      if (paramPendingTransactionActions.shouldReportRelaunchToWindowManager() && activityClientRecord != null && activityClientRecord.window != null)
        activityClientRecord.window.reportActivityRelaunched(); 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void callActivityOnSaveInstanceState(ActivityClientRecord paramActivityClientRecord) {
    paramActivityClientRecord.state = new Bundle();
    paramActivityClientRecord.state.setAllowFds(false);
    if (paramActivityClientRecord.isPersistable()) {
      paramActivityClientRecord.persistentState = new PersistableBundle();
      this.mInstrumentation.callActivityOnSaveInstanceState(paramActivityClientRecord.activity, paramActivityClientRecord.state, paramActivityClientRecord.persistentState);
    } else {
      this.mInstrumentation.callActivityOnSaveInstanceState(paramActivityClientRecord.activity, paramActivityClientRecord.state);
    } 
  }
  
  ArrayList<ComponentCallbacks2> collectComponentCallbacks(boolean paramBoolean, Configuration paramConfiguration) {
    ArrayList<ComponentCallbacks2> arrayList = new ArrayList();
    synchronized (this.mResourcesManager) {
      int i = this.mAllApplications.size();
      byte b;
      for (b = 0; b < i; b++)
        arrayList.add(this.mAllApplications.get(b)); 
      i = this.mActivities.size();
      for (b = 0; b < i; b++) {
        ActivityClientRecord activityClientRecord = (ActivityClientRecord)this.mActivities.valueAt(b);
        Activity activity = activityClientRecord.activity;
        if (activity != null) {
          int j = this.mCurDefaultDisplayDpi;
          LoadedApk loadedApk = activityClientRecord.packageInfo;
          CompatibilityInfo compatibilityInfo = loadedApk.getCompatibilityInfo();
          Configuration configuration = applyConfigCompatMainThread(j, paramConfiguration, compatibilityInfo);
          if (!activityClientRecord.activity.mFinished && (paramBoolean || !activityClientRecord.paused)) {
            arrayList.add(activity);
          } else if (configuration != null) {
            if (DEBUG_CONFIGURATION) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Setting activity ");
              stringBuilder.append(activityClientRecord.activityInfo.name);
              stringBuilder.append(" newConfig=");
              stringBuilder.append(configuration);
              Slog.v("ActivityThread", stringBuilder.toString());
            } 
            activityClientRecord.newConfig = configuration;
          } 
        } 
      } 
      i = this.mServices.size();
      for (b = 0; b < i; b++) {
        ComponentCallbacks2 componentCallbacks2 = (ComponentCallbacks2)this.mServices.valueAt(b);
        if (componentCallbacks2 instanceof android.inputmethodservice.InputMethodService)
          this.mHasImeComponent = true; 
        arrayList.add(componentCallbacks2);
      } 
      synchronized (this.mProviderMap) {
        i = this.mLocalProviders.size();
        for (b = 0; b < i; b++)
          arrayList.add(((ProviderClientRecord)this.mLocalProviders.valueAt(b)).mLocalProvider); 
        return arrayList;
      } 
    } 
  }
  
  private void performConfigurationChangedForActivity(ActivityClientRecord paramActivityClientRecord, Configuration paramConfiguration) {
    performConfigurationChangedForActivity(paramActivityClientRecord, paramConfiguration, paramActivityClientRecord.activity.getDisplayId(), false);
  }
  
  private Configuration performConfigurationChangedForActivity(ActivityClientRecord paramActivityClientRecord, Configuration paramConfiguration, int paramInt, boolean paramBoolean) {
    paramActivityClientRecord.tmpConfig.setTo(paramConfiguration);
    if (paramActivityClientRecord.overrideConfig != null)
      paramActivityClientRecord.tmpConfig.updateFrom(paramActivityClientRecord.overrideConfig); 
    Activity activity = paramActivityClientRecord.activity;
    Configuration configuration = paramActivityClientRecord.tmpConfig;
    paramConfiguration = paramActivityClientRecord.overrideConfig;
    paramConfiguration = performActivityConfigurationChanged(activity, configuration, paramConfiguration, paramInt, paramBoolean);
    freeTextLayoutCachesIfNeeded(paramActivityClientRecord.activity.mCurrentConfig.diff(paramActivityClientRecord.tmpConfig));
    return paramConfiguration;
  }
  
  private static Configuration createNewConfigAndUpdateIfNotNull(Configuration paramConfiguration1, Configuration paramConfiguration2) {
    if (paramConfiguration2 == null)
      return paramConfiguration1; 
    paramConfiguration1 = new Configuration(paramConfiguration1);
    paramConfiguration1.updateFrom(paramConfiguration2);
    return paramConfiguration1;
  }
  
  private void performConfigurationChanged(ComponentCallbacks2 paramComponentCallbacks2, Configuration paramConfiguration) {
    Configuration configuration;
    ContextThemeWrapper contextThemeWrapper = null;
    if (paramComponentCallbacks2 instanceof ContextThemeWrapper) {
      contextThemeWrapper = (ContextThemeWrapper)paramComponentCallbacks2;
      configuration = contextThemeWrapper.getOverrideConfiguration();
    } 
    paramConfiguration = createNewConfigAndUpdateIfNotNull(paramConfiguration, configuration);
    paramComponentCallbacks2.onConfigurationChanged(paramConfiguration);
  }
  
  private Configuration performActivityConfigurationChanged(Activity paramActivity, Configuration paramConfiguration1, Configuration paramConfiguration2, int paramInt, boolean paramBoolean) {
    if (paramActivity != null) {
      IBinder iBinder = paramActivity.getActivityToken();
      if (iBinder != null) {
        StringBuilder stringBuilder;
        handleWindowingModeChangeIfNeeded(paramActivity, paramConfiguration1);
        boolean bool = false;
        if (paramActivity.mCurrentConfig == null) {
          bool = true;
        } else {
          int i = paramActivity.mCurrentConfig.diffPublicOnly(paramConfiguration1);
          int j = paramActivity.mActivityInfo.getRealConfigChanged();
          if (i == 0 && !paramBoolean) {
            ResourcesManager resourcesManager = this.mResourcesManager;
            if (resourcesManager.isSameResourcesOverrideConfig(iBinder, paramConfiguration2))
              return null; 
          } 
          if (((j ^ 0xFFFFFFFF) & i) == 0) {
            boolean bool2 = true;
            bool = bool2;
            if (((IOplusDarkModeManager)OplusFeatureCache.<IOplusDarkModeManager>getOrCreate(IOplusDarkModeManager.DEFAULT, new Object[0])).shouldInterceptConfigRelaunch(i, paramActivity.mCurrentConfig))
              bool = bool2; 
          } 
          boolean bool1 = bool;
          if (((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).shouldReportExtraConfig(i, j)) {
            bool1 = bool;
            if (!bool)
              bool1 = true; 
          } 
          if (!bool1 && (j & i) != 0 && "com.oppo.launcher".equals(paramActivity.getPackageName())) {
            bool = true;
          } else {
            bool = bool1;
          } 
        } 
        Configuration configuration = paramActivity.getOverrideConfiguration();
        paramConfiguration2 = createNewConfigAndUpdateIfNotNull(paramConfiguration2, configuration);
        this.mResourcesManager.updateResourcesForActivity(paramActivity.getPackageName(), iBinder, paramConfiguration2, paramInt, paramBoolean);
        paramActivity.mConfigChangeFlags = 0;
        paramActivity.mCurrentConfig = new Configuration(paramConfiguration1);
        paramConfiguration1 = createNewConfigAndUpdateIfNotNull(paramConfiguration1, configuration);
        if (paramBoolean)
          paramActivity.dispatchMovedToDisplay(paramInt, paramConfiguration1); 
        if (bool && true) {
          paramActivity.mCalled = false;
          paramActivity.onConfigurationChanged(paramConfiguration1);
          if (!paramActivity.mCalled) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Activity ");
            stringBuilder.append(paramActivity.getLocalClassName());
            stringBuilder.append(" did not call through to super.onConfigurationChanged()");
            throw new SuperNotCalledException(stringBuilder.toString());
          } 
        } 
        return (Configuration)stringBuilder;
      } 
      throw new IllegalArgumentException("Activity token not set. Is the activity attached?");
    } 
    throw new IllegalArgumentException("No activity provided.");
  }
  
  public final void applyConfigurationToResources(Configuration paramConfiguration) {
    synchronized (this.mResourcesManager) {
      this.mResourcesManager.applyConfigurationToResourcesLocked(paramConfiguration, null);
      return;
    } 
  }
  
  final Configuration applyCompatConfiguration(int paramInt) {
    Configuration configuration = this.mConfiguration;
    if (this.mCompatConfiguration == null)
      this.mCompatConfiguration = new Configuration(); 
    this.mCompatConfiguration.setTo(this.mConfiguration);
    if (this.mResourcesManager.applyCompatConfigurationLocked(paramInt, this.mCompatConfiguration))
      configuration = this.mCompatConfiguration; 
    return configuration;
  }
  
  public void handleConfigurationChanged(Configuration paramConfiguration) {
    Trace.traceBegin(64L, "configChanged");
    this.mCurDefaultDisplayDpi = paramConfiguration.densityDpi;
    ((IOplusScreenModeFeature)OplusFeatureCache.<IOplusScreenModeFeature>get(IOplusScreenModeFeature.DEFAULT)).updateCompatDensityIfNeed(this.mCurDefaultDisplayDpi);
    handleConfigurationChanged(paramConfiguration, (CompatibilityInfo)null);
    Trace.traceEnd(64L);
  }
  
  private void handleConfigurationChanged(Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getSystemContext : ()Landroid/app/ContextImpl;
    //   4: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   7: astore_3
    //   8: aload_0
    //   9: invokevirtual getSystemUiContext : ()Landroid/app/ContextImpl;
    //   12: invokevirtual getTheme : ()Landroid/content/res/Resources$Theme;
    //   15: astore #4
    //   17: aload_0
    //   18: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   21: astore #5
    //   23: aload #5
    //   25: monitorenter
    //   26: aload_1
    //   27: astore #6
    //   29: aload_0
    //   30: getfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   33: ifnull -> 74
    //   36: aload_1
    //   37: astore #6
    //   39: aload_0
    //   40: getfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   43: aload_1
    //   44: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   47: ifne -> 69
    //   50: aload_0
    //   51: getfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   54: astore #6
    //   56: aload_0
    //   57: aload #6
    //   59: getfield densityDpi : I
    //   62: putfield mCurDefaultDisplayDpi : I
    //   65: aload_0
    //   66: invokespecial updateDefaultDensity : ()V
    //   69: aload_0
    //   70: aconst_null
    //   71: putfield mPendingConfiguration : Landroid/content/res/Configuration;
    //   74: aload #6
    //   76: ifnonnull -> 105
    //   79: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   82: ifeq -> 101
    //   85: aload_0
    //   86: getfield mHasImeComponent : Z
    //   89: ifeq -> 101
    //   92: ldc 'ActivityThread'
    //   94: ldc_w 'handleConfigurationChanged for IME app but config is null'
    //   97: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   100: pop
    //   101: aload #5
    //   103: monitorexit
    //   104: return
    //   105: aload_0
    //   106: getfield mConfiguration : Landroid/content/res/Configuration;
    //   109: ifnull -> 130
    //   112: aload_0
    //   113: getfield mConfiguration : Landroid/content/res/Configuration;
    //   116: aload #6
    //   118: invokevirtual diffPublicOnly : (Landroid/content/res/Configuration;)I
    //   121: ifne -> 130
    //   124: iconst_1
    //   125: istore #7
    //   127: goto -> 133
    //   130: iconst_0
    //   131: istore #7
    //   133: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   136: ifeq -> 172
    //   139: new java/lang/StringBuilder
    //   142: astore_1
    //   143: aload_1
    //   144: invokespecial <init> : ()V
    //   147: aload_1
    //   148: ldc_w 'Handle configuration changed: '
    //   151: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   154: pop
    //   155: aload_1
    //   156: aload #6
    //   158: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   161: pop
    //   162: ldc 'ActivityThread'
    //   164: aload_1
    //   165: invokevirtual toString : ()Ljava/lang/String;
    //   168: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   171: pop
    //   172: aload_0
    //   173: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   176: aload #6
    //   178: aload_2
    //   179: invokevirtual applyConfigurationToResourcesLocked : (Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z
    //   182: pop
    //   183: aload_0
    //   184: getfield mInitialApplication : Landroid/app/Application;
    //   187: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   190: astore_1
    //   191: aload_0
    //   192: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   195: astore #8
    //   197: aload #8
    //   199: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   202: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   205: astore #8
    //   207: aload_0
    //   208: aload_1
    //   209: aload #8
    //   211: invokespecial updateLocaleListFromAppContext : (Landroid/content/Context;Landroid/os/LocaleList;)V
    //   214: aload_0
    //   215: getfield mConfiguration : Landroid/content/res/Configuration;
    //   218: ifnonnull -> 234
    //   221: new android/content/res/Configuration
    //   224: astore_1
    //   225: aload_1
    //   226: invokespecial <init> : ()V
    //   229: aload_0
    //   230: aload_1
    //   231: putfield mConfiguration : Landroid/content/res/Configuration;
    //   234: aload_0
    //   235: getfield mConfiguration : Landroid/content/res/Configuration;
    //   238: aload #6
    //   240: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   243: ifne -> 317
    //   246: aload_2
    //   247: ifnonnull -> 317
    //   250: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   253: ifeq -> 313
    //   256: aload_0
    //   257: getfield mHasImeComponent : Z
    //   260: ifeq -> 313
    //   263: new java/lang/StringBuilder
    //   266: astore_1
    //   267: aload_1
    //   268: invokespecial <init> : ()V
    //   271: aload_1
    //   272: ldc_w 'handleConfigurationChanged for IME app but config seq is obsolete , config='
    //   275: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: aload_1
    //   280: aload #6
    //   282: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   285: pop
    //   286: aload_1
    //   287: ldc_w ', mConfiguration='
    //   290: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   293: pop
    //   294: aload_1
    //   295: aload_0
    //   296: getfield mConfiguration : Landroid/content/res/Configuration;
    //   299: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   302: pop
    //   303: ldc 'ActivityThread'
    //   305: aload_1
    //   306: invokevirtual toString : ()Ljava/lang/String;
    //   309: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   312: pop
    //   313: aload #5
    //   315: monitorexit
    //   316: return
    //   317: aload_0
    //   318: getfield mConfiguration : Landroid/content/res/Configuration;
    //   321: aload #6
    //   323: invokevirtual updateFrom : (Landroid/content/res/Configuration;)I
    //   326: istore #9
    //   328: aload_0
    //   329: aload_0
    //   330: getfield mCurDefaultDisplayDpi : I
    //   333: invokevirtual applyCompatConfiguration : (I)Landroid/content/res/Configuration;
    //   336: astore_1
    //   337: aload_3
    //   338: invokevirtual getChangingConfigurations : ()I
    //   341: iload #9
    //   343: iand
    //   344: ifeq -> 351
    //   347: aload_3
    //   348: invokevirtual rebase : ()V
    //   351: aload #4
    //   353: invokevirtual getChangingConfigurations : ()I
    //   356: iload #9
    //   358: iand
    //   359: ifeq -> 367
    //   362: aload #4
    //   364: invokevirtual rebase : ()V
    //   367: aload #5
    //   369: monitorexit
    //   370: aload_0
    //   371: iconst_0
    //   372: aload_1
    //   373: invokevirtual collectComponentCallbacks : (ZLandroid/content/res/Configuration;)Ljava/util/ArrayList;
    //   376: astore_2
    //   377: iload #9
    //   379: invokestatic freeTextLayoutCachesIfNeeded : (I)V
    //   382: getstatic android/app/IOplusCommonInjector.DEFAULT : Landroid/app/IOplusCommonInjector;
    //   385: iconst_0
    //   386: anewarray java/lang/Object
    //   389: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   392: checkcast android/app/IOplusCommonInjector
    //   395: aload_0
    //   396: getfield mInitialApplication : Landroid/app/Application;
    //   399: aload_1
    //   400: invokeinterface onConfigurationChangedForApplication : (Landroid/app/Application;Landroid/content/res/Configuration;)V
    //   405: getstatic com/oplus/darkmode/IOplusDarkModeManager.DEFAULT : Lcom/oplus/darkmode/IOplusDarkModeManager;
    //   408: iconst_0
    //   409: anewarray java/lang/Object
    //   412: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   415: checkcast com/oplus/darkmode/IOplusDarkModeManager
    //   418: iload #9
    //   420: aload_0
    //   421: getfield mConfiguration : Landroid/content/res/Configuration;
    //   424: invokeinterface shouldInterceptConfigRelaunch : (ILandroid/content/res/Configuration;)Z
    //   429: ifeq -> 433
    //   432: return
    //   433: aload_2
    //   434: ifnull -> 593
    //   437: aload_2
    //   438: invokevirtual size : ()I
    //   441: istore #10
    //   443: iconst_0
    //   444: istore #11
    //   446: iload #11
    //   448: iload #10
    //   450: if_icmpge -> 593
    //   453: aload_2
    //   454: iload #11
    //   456: invokevirtual get : (I)Ljava/lang/Object;
    //   459: checkcast android/content/ComponentCallbacks2
    //   462: astore #6
    //   464: aload #6
    //   466: instanceof android/app/Activity
    //   469: ifeq -> 502
    //   472: aload #6
    //   474: checkcast android/app/Activity
    //   477: astore #6
    //   479: aload_0
    //   480: aload_0
    //   481: getfield mActivities : Landroid/util/ArrayMap;
    //   484: aload #6
    //   486: invokevirtual getActivityToken : ()Landroid/os/IBinder;
    //   489: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   492: checkcast android/app/ActivityThread$ActivityClientRecord
    //   495: aload_1
    //   496: invokespecial performConfigurationChangedForActivity : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/res/Configuration;)V
    //   499: goto -> 587
    //   502: iload #7
    //   504: ifne -> 517
    //   507: aload_0
    //   508: aload #6
    //   510: aload_1
    //   511: invokespecial performConfigurationChanged : (Landroid/content/ComponentCallbacks2;Landroid/content/res/Configuration;)V
    //   514: goto -> 587
    //   517: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   520: ifeq -> 587
    //   523: aload #6
    //   525: instanceof android/inputmethodservice/InputMethodService
    //   528: ifeq -> 587
    //   531: new java/lang/StringBuilder
    //   534: dup
    //   535: invokespecial <init> : ()V
    //   538: astore #6
    //   540: aload #6
    //   542: ldc_w 'performConfigurationChanged didn't callback to IME , configDiff='
    //   545: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   548: pop
    //   549: aload #6
    //   551: iload #9
    //   553: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   556: pop
    //   557: aload #6
    //   559: ldc_w ', mConfiguration='
    //   562: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   565: pop
    //   566: aload #6
    //   568: aload_0
    //   569: getfield mConfiguration : Landroid/content/res/Configuration;
    //   572: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   575: pop
    //   576: ldc 'ActivityThread'
    //   578: aload #6
    //   580: invokevirtual toString : ()Ljava/lang/String;
    //   583: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   586: pop
    //   587: iinc #11, 1
    //   590: goto -> 446
    //   593: return
    //   594: astore_1
    //   595: aload #5
    //   597: monitorexit
    //   598: aload_1
    //   599: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6128	-> 0
    //   #6129	-> 8
    //   #6131	-> 17
    //   #6132	-> 26
    //   #6133	-> 36
    //   #6134	-> 50
    //   #6135	-> 56
    //   #6136	-> 65
    //   #6138	-> 69
    //   #6141	-> 74
    //   #6143	-> 79
    //   #6144	-> 92
    //   #6146	-> 101
    //   #6154	-> 105
    //   #6156	-> 133
    //   #6159	-> 172
    //   #6160	-> 183
    //   #6161	-> 197
    //   #6160	-> 207
    //   #6163	-> 214
    //   #6164	-> 221
    //   #6166	-> 234
    //   #6168	-> 250
    //   #6169	-> 263
    //   #6173	-> 313
    //   #6176	-> 317
    //   #6177	-> 328
    //   #6179	-> 337
    //   #6180	-> 347
    //   #6183	-> 351
    //   #6184	-> 362
    //   #6186	-> 367
    //   #6188	-> 370
    //   #6190	-> 377
    //   #6194	-> 382
    //   #6199	-> 405
    //   #6203	-> 433
    //   #6204	-> 437
    //   #6205	-> 443
    //   #6206	-> 453
    //   #6207	-> 464
    //   #6210	-> 472
    //   #6211	-> 479
    //   #6213	-> 499
    //   #6214	-> 507
    //   #6217	-> 517
    //   #6218	-> 531
    //   #6205	-> 587
    //   #6225	-> 593
    //   #6186	-> 594
    // Exception table:
    //   from	to	target	type
    //   29	36	594	finally
    //   39	50	594	finally
    //   50	56	594	finally
    //   56	65	594	finally
    //   65	69	594	finally
    //   69	74	594	finally
    //   79	92	594	finally
    //   92	101	594	finally
    //   101	104	594	finally
    //   105	124	594	finally
    //   133	172	594	finally
    //   172	183	594	finally
    //   183	197	594	finally
    //   197	207	594	finally
    //   207	214	594	finally
    //   214	221	594	finally
    //   221	234	594	finally
    //   234	246	594	finally
    //   250	263	594	finally
    //   263	313	594	finally
    //   313	316	594	finally
    //   317	328	594	finally
    //   328	337	594	finally
    //   337	347	594	finally
    //   347	351	594	finally
    //   351	362	594	finally
    //   362	367	594	finally
    //   367	370	594	finally
    //   595	598	594	finally
  }
  
  private void handleWindowingModeChangeIfNeeded(Activity paramActivity, Configuration paramConfiguration) {
    int i = paramConfiguration.windowConfiguration.getWindowingMode();
    IBinder iBinder = paramActivity.getActivityToken();
    Map<IBinder, Integer> map = this.mLastReportedWindowingMode;
    int j = ((Integer)map.getOrDefault(iBinder, Integer.valueOf(0))).intValue();
    if (j == i)
      return; 
    if (i == 2) {
      paramActivity.dispatchPictureInPictureModeChanged(true, paramConfiguration);
    } else if (j == 2) {
      paramActivity.dispatchPictureInPictureModeChanged(false, paramConfiguration);
    } 
    boolean bool1 = WindowConfiguration.inMultiWindowMode(j);
    boolean bool2 = WindowConfiguration.inMultiWindowMode(i);
    if (bool1 != bool2)
      paramActivity.dispatchMultiWindowModeChanged(bool2, paramConfiguration); 
    this.mLastReportedWindowingMode.put(iBinder, Integer.valueOf(i));
  }
  
  public void handleSystemApplicationInfoChanged(ApplicationInfo paramApplicationInfo) {
    Preconditions.checkState(this.mSystemThread, "Must only be called in the system process");
    handleApplicationInfoChanged(paramApplicationInfo);
  }
  
  public void handleApplicationInfoChanged(ApplicationInfo paramApplicationInfo) {
    handleApplicationInfoChangedForSwitchUser(paramApplicationInfo, 0, 0);
  }
  
  private void handleApplicationInfoChangedForSwitchUser(ApplicationInfo paramApplicationInfo, int paramInt1, int paramInt2) {
    ResourcesManager resourcesManager;
    String[] arrayOfString;
    synchronized (this.mResourcesManager) {
      ResourcesManager resourcesManager1;
      Configuration configuration;
      WeakReference<LoadedApk> weakReference1 = (WeakReference)this.mPackages.get(paramApplicationInfo.packageName);
      if (weakReference1 != null) {
        LoadedApk loadedApk = weakReference1.get();
      } else {
        weakReference1 = null;
      } 
      WeakReference<LoadedApk> weakReference2 = (WeakReference)this.mResourcePackages.get(paramApplicationInfo.packageName);
      if (weakReference2 != null) {
        LoadedApk loadedApk = weakReference2.get();
      } else {
        weakReference2 = null;
      } 
      arrayOfString = new String[2];
      int i = 0;
      if (weakReference1 != null) {
        arrayOfString[0] = weakReference1.getResDir();
        ArrayList<String> arrayList = new ArrayList();
        LoadedApk.makePaths(this, weakReference1.getApplicationInfo(), arrayList);
        weakReference1.updateApplicationInfo(paramApplicationInfo, arrayList);
      } 
      if (weakReference2 != null) {
        arrayOfString[1] = weakReference2.getResDir();
        ArrayList<String> arrayList = new ArrayList();
        LoadedApk.makePaths(this, weakReference2.getApplicationInfo(), arrayList);
        weakReference2.updateApplicationInfo(paramApplicationInfo, arrayList);
      } 
      synchronized (this.mResourcesManager) {
        this.mResourcesManager.applyNewResourceDirsLocked(paramApplicationInfo, arrayOfString);
        ApplicationPackageManager.configurationChanged();
        configuration = new Configuration();
        Configuration configuration1 = this.mConfiguration;
        if (configuration1 != null)
          i = configuration1.assetsSeq; 
        configuration.assetsSeq = i + 1;
        handleConfigurationChanged(configuration, (CompatibilityInfo)null);
        if (paramInt1 == 1 && paramInt2 == 1 && paramApplicationInfo != null) {
          String str = paramApplicationInfo.packageName;
          if ("com.oppo.launcher".equals(str)) {
            Slog.d("ActivityThread", "relaunchAllActivitiesForSwitchUser no relaunch in switchUser");
            return;
          } 
        } 
        relaunchAllActivities(true);
        return;
      } 
    } 
  }
  
  static void freeTextLayoutCachesIfNeeded(int paramInt) {
    if (paramInt != 0) {
      if ((paramInt & 0x4) != 0) {
        paramInt = 1;
      } else {
        paramInt = 0;
      } 
      if (paramInt != 0) {
        Canvas.freeTextLayoutCaches();
        if (DEBUG_CONFIGURATION)
          Slog.v("ActivityThread", "Cleared TextLayout Caches"); 
      } 
    } 
  }
  
  public void updatePendingActivityConfiguration(IBinder paramIBinder, Configuration paramConfiguration) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mActivities : Landroid/util/ArrayMap;
    //   11: aload_1
    //   12: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   15: checkcast android/app/ActivityThread$ActivityClientRecord
    //   18: astore_1
    //   19: aload_3
    //   20: monitorexit
    //   21: aload_1
    //   22: ifnonnull -> 41
    //   25: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   28: ifeq -> 40
    //   31: ldc 'ActivityThread'
    //   33: ldc_w 'Not found target activity to update its pending config.'
    //   36: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   39: pop
    //   40: return
    //   41: aload_1
    //   42: monitorenter
    //   43: aload_1
    //   44: invokestatic access$4200 : (Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/content/res/Configuration;
    //   47: ifnull -> 121
    //   50: aload_1
    //   51: invokestatic access$4200 : (Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/content/res/Configuration;
    //   54: aload_2
    //   55: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   58: ifne -> 121
    //   61: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   64: ifeq -> 118
    //   67: new java/lang/StringBuilder
    //   70: astore_3
    //   71: aload_3
    //   72: invokespecial <init> : ()V
    //   75: aload_3
    //   76: ldc_w 'Activity has newer configuration pending so drop this transaction. overrideConfig='
    //   79: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: aload_3
    //   84: aload_2
    //   85: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: aload_3
    //   90: ldc_w ' r.mPendingOverrideConfig='
    //   93: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   96: pop
    //   97: aload_3
    //   98: aload_1
    //   99: invokestatic access$4200 : (Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/content/res/Configuration;
    //   102: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_3
    //   107: invokevirtual toString : ()Ljava/lang/String;
    //   110: astore_2
    //   111: ldc 'ActivityThread'
    //   113: aload_2
    //   114: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   117: pop
    //   118: aload_1
    //   119: monitorexit
    //   120: return
    //   121: aload_1
    //   122: aload_2
    //   123: invokestatic access$4202 : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    //   126: pop
    //   127: aload_1
    //   128: monitorexit
    //   129: return
    //   130: astore_2
    //   131: aload_1
    //   132: monitorexit
    //   133: aload_2
    //   134: athrow
    //   135: astore_1
    //   136: aload_3
    //   137: monitorexit
    //   138: aload_1
    //   139: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6358	-> 0
    //   #6359	-> 7
    //   #6360	-> 19
    //   #6362	-> 21
    //   #6363	-> 25
    //   #6364	-> 31
    //   #6366	-> 40
    //   #6369	-> 41
    //   #6370	-> 43
    //   #6371	-> 50
    //   #6372	-> 61
    //   #6373	-> 67
    //   #6375	-> 97
    //   #6373	-> 111
    //   #6377	-> 118
    //   #6379	-> 121
    //   #6380	-> 127
    //   #6381	-> 129
    //   #6380	-> 130
    //   #6360	-> 135
    // Exception table:
    //   from	to	target	type
    //   7	19	135	finally
    //   19	21	135	finally
    //   43	50	130	finally
    //   50	61	130	finally
    //   61	67	130	finally
    //   67	97	130	finally
    //   97	111	130	finally
    //   111	118	130	finally
    //   118	120	130	finally
    //   121	127	130	finally
    //   127	129	130	finally
    //   131	133	130	finally
    //   136	138	135	finally
  }
  
  public void handleActivityConfigurationChanged(IBinder paramIBinder, Configuration paramConfiguration, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mActivities : Landroid/util/ArrayMap;
    //   4: aload_1
    //   5: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   8: checkcast android/app/ActivityThread$ActivityClientRecord
    //   11: astore #4
    //   13: aload #4
    //   15: ifnull -> 498
    //   18: aload #4
    //   20: getfield activity : Landroid/app/Activity;
    //   23: ifnonnull -> 29
    //   26: goto -> 498
    //   29: iload_3
    //   30: iconst_m1
    //   31: if_icmpeq -> 54
    //   34: aload #4
    //   36: getfield activity : Landroid/app/Activity;
    //   39: astore_1
    //   40: iload_3
    //   41: aload_1
    //   42: invokevirtual getDisplayId : ()I
    //   45: if_icmpeq -> 54
    //   48: iconst_1
    //   49: istore #5
    //   51: goto -> 57
    //   54: iconst_0
    //   55: istore #5
    //   57: aload #4
    //   59: monitorenter
    //   60: aload_2
    //   61: aload #4
    //   63: invokestatic access$4200 : (Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/content/res/Configuration;
    //   66: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   69: ifeq -> 134
    //   72: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   75: ifeq -> 130
    //   78: new java/lang/StringBuilder
    //   81: astore_1
    //   82: aload_1
    //   83: invokespecial <init> : ()V
    //   86: aload_1
    //   87: ldc_w 'Activity has newer configuration pending so drop this transaction. overrideConfig='
    //   90: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   93: pop
    //   94: aload_1
    //   95: aload_2
    //   96: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   99: pop
    //   100: aload_1
    //   101: ldc_w ' r.mPendingOverrideConfig='
    //   104: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   107: pop
    //   108: aload_1
    //   109: aload #4
    //   111: invokestatic access$4200 : (Landroid/app/ActivityThread$ActivityClientRecord;)Landroid/content/res/Configuration;
    //   114: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload_1
    //   119: invokevirtual toString : ()Ljava/lang/String;
    //   122: astore_1
    //   123: ldc 'ActivityThread'
    //   125: aload_1
    //   126: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   129: pop
    //   130: aload #4
    //   132: monitorexit
    //   133: return
    //   134: aconst_null
    //   135: astore_1
    //   136: aload #4
    //   138: aconst_null
    //   139: invokestatic access$4202 : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    //   142: pop
    //   143: aload #4
    //   145: monitorexit
    //   146: getstatic android/widget/IOplusTextViewRTLUtilForUG.DEFAULT : Landroid/widget/IOplusTextViewRTLUtilForUG;
    //   149: iconst_0
    //   150: anewarray java/lang/Object
    //   153: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   156: checkcast android/widget/IOplusTextViewRTLUtilForUG
    //   159: aload #4
    //   161: getfield activity : Landroid/app/Activity;
    //   164: invokevirtual getAssets : ()Landroid/content/res/AssetManager;
    //   167: invokevirtual getNonSystemLocales : ()[Ljava/lang/String;
    //   170: aload_2
    //   171: invokeinterface updateRtlParameterForUG : ([Ljava/lang/String;Landroid/content/res/Configuration;)V
    //   176: aload #4
    //   178: getfield overrideConfig : Landroid/content/res/Configuration;
    //   181: ifnull -> 258
    //   184: aload #4
    //   186: getfield overrideConfig : Landroid/content/res/Configuration;
    //   189: aload_2
    //   190: invokevirtual isOtherSeqNewer : (Landroid/content/res/Configuration;)Z
    //   193: ifne -> 258
    //   196: iload #5
    //   198: ifne -> 258
    //   201: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   204: ifeq -> 257
    //   207: new java/lang/StringBuilder
    //   210: dup
    //   211: invokespecial <init> : ()V
    //   214: astore_1
    //   215: aload_1
    //   216: ldc_w 'Activity already handled newer configuration so drop this transaction. overrideConfig='
    //   219: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload_1
    //   224: aload_2
    //   225: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   228: pop
    //   229: aload_1
    //   230: ldc_w ' r.overrideConfig='
    //   233: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   236: pop
    //   237: aload_1
    //   238: aload #4
    //   240: getfield overrideConfig : Landroid/content/res/Configuration;
    //   243: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: ldc 'ActivityThread'
    //   249: aload_1
    //   250: invokevirtual toString : ()Ljava/lang/String;
    //   253: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   256: pop
    //   257: return
    //   258: aload #4
    //   260: aload_2
    //   261: putfield overrideConfig : Landroid/content/res/Configuration;
    //   264: aload #4
    //   266: getfield activity : Landroid/app/Activity;
    //   269: getfield mDecor : Landroid/view/View;
    //   272: ifnull -> 290
    //   275: aload #4
    //   277: getfield activity : Landroid/app/Activity;
    //   280: getfield mDecor : Landroid/view/View;
    //   283: invokevirtual getViewRootImpl : ()Landroid/view/ViewRootImpl;
    //   286: astore_1
    //   287: goto -> 290
    //   290: iload #5
    //   292: ifeq -> 402
    //   295: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   298: ifeq -> 376
    //   301: new java/lang/StringBuilder
    //   304: dup
    //   305: invokespecial <init> : ()V
    //   308: astore #6
    //   310: aload #6
    //   312: ldc_w 'Handle activity moved to display, activity:'
    //   315: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   318: pop
    //   319: aload #6
    //   321: aload #4
    //   323: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   326: getfield name : Ljava/lang/String;
    //   329: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: aload #6
    //   335: ldc_w ', displayId='
    //   338: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   341: pop
    //   342: aload #6
    //   344: iload_3
    //   345: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   348: pop
    //   349: aload #6
    //   351: ldc_w ', config='
    //   354: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   357: pop
    //   358: aload #6
    //   360: aload_2
    //   361: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   364: pop
    //   365: ldc 'ActivityThread'
    //   367: aload #6
    //   369: invokevirtual toString : ()Ljava/lang/String;
    //   372: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   375: pop
    //   376: aload_0
    //   377: aload #4
    //   379: aload_0
    //   380: getfield mCompatConfiguration : Landroid/content/res/Configuration;
    //   383: iload_3
    //   384: iconst_1
    //   385: invokespecial performConfigurationChangedForActivity : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/res/Configuration;IZ)Landroid/content/res/Configuration;
    //   388: astore_2
    //   389: aload_1
    //   390: ifnull -> 399
    //   393: aload_1
    //   394: iload_3
    //   395: aload_2
    //   396: invokevirtual onMovedToDisplay : (ILandroid/content/res/Configuration;)V
    //   399: goto -> 477
    //   402: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   405: ifeq -> 467
    //   408: new java/lang/StringBuilder
    //   411: dup
    //   412: invokespecial <init> : ()V
    //   415: astore #6
    //   417: aload #6
    //   419: ldc_w 'Handle activity config changed: '
    //   422: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   425: pop
    //   426: aload #6
    //   428: aload #4
    //   430: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   433: getfield name : Ljava/lang/String;
    //   436: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload #6
    //   442: ldc_w ', config='
    //   445: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   448: pop
    //   449: aload #6
    //   451: aload_2
    //   452: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   455: pop
    //   456: ldc 'ActivityThread'
    //   458: aload #6
    //   460: invokevirtual toString : ()Ljava/lang/String;
    //   463: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   466: pop
    //   467: aload_0
    //   468: aload #4
    //   470: aload_0
    //   471: getfield mCompatConfiguration : Landroid/content/res/Configuration;
    //   474: invokespecial performConfigurationChangedForActivity : (Landroid/app/ActivityThread$ActivityClientRecord;Landroid/content/res/Configuration;)V
    //   477: aload_1
    //   478: ifnull -> 486
    //   481: aload_1
    //   482: iload_3
    //   483: invokevirtual updateConfiguration : (I)V
    //   486: aload_0
    //   487: iconst_1
    //   488: putfield mSomeActivitiesChanged : Z
    //   491: return
    //   492: astore_1
    //   493: aload #4
    //   495: monitorexit
    //   496: aload_1
    //   497: athrow
    //   498: getstatic android/app/ActivityThread.DEBUG_CONFIGURATION : Z
    //   501: ifeq -> 537
    //   504: new java/lang/StringBuilder
    //   507: dup
    //   508: invokespecial <init> : ()V
    //   511: astore_1
    //   512: aload_1
    //   513: ldc_w 'Not found target activity to report to: '
    //   516: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   519: pop
    //   520: aload_1
    //   521: aload #4
    //   523: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   526: pop
    //   527: ldc 'ActivityThread'
    //   529: aload_1
    //   530: invokevirtual toString : ()Ljava/lang/String;
    //   533: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   536: pop
    //   537: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6396	-> 0
    //   #6398	-> 13
    //   #6402	-> 29
    //   #6403	-> 40
    //   #6405	-> 57
    //   #6406	-> 60
    //   #6407	-> 72
    //   #6408	-> 78
    //   #6410	-> 108
    //   #6408	-> 123
    //   #6412	-> 130
    //   #6414	-> 134
    //   #6415	-> 143
    //   #6418	-> 146
    //   #6420	-> 176
    //   #6422	-> 201
    //   #6423	-> 207
    //   #6427	-> 257
    //   #6431	-> 258
    //   #6432	-> 264
    //   #6433	-> 275
    //   #6435	-> 290
    //   #6436	-> 295
    //   #6440	-> 376
    //   #6442	-> 389
    //   #6443	-> 393
    //   #6445	-> 399
    //   #6446	-> 402
    //   #6448	-> 467
    //   #6452	-> 477
    //   #6453	-> 481
    //   #6455	-> 486
    //   #6456	-> 491
    //   #6415	-> 492
    //   #6399	-> 498
    //   #6400	-> 537
    // Exception table:
    //   from	to	target	type
    //   60	72	492	finally
    //   72	78	492	finally
    //   78	108	492	finally
    //   108	123	492	finally
    //   123	130	492	finally
    //   130	133	492	finally
    //   136	143	492	finally
    //   143	146	492	finally
    //   493	496	492	finally
  }
  
  final void handleProfilerControl(boolean paramBoolean, ProfilerInfo paramProfilerInfo, int paramInt) {
    if (paramBoolean) {
      try {
        this.mProfiler.setProfiler(paramProfilerInfo);
        this.mProfiler.startProfiling();
        paramProfilerInfo.closeFd();
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Profiling failed on path ");
        stringBuilder.append(paramProfilerInfo.profileFile);
        stringBuilder.append(" -- can the process access this path?");
        Slog.w("ActivityThread", stringBuilder.toString());
        paramProfilerInfo.closeFd();
      } finally {
        Exception exception;
      } 
    } else {
      this.mProfiler.stopProfiling();
    } 
  }
  
  public void stopProfiling() {
    Profiler profiler = this.mProfiler;
    if (profiler != null)
      profiler.stopProfiling(); 
  }
  
  static void handleDumpHeap(DumpHeapData paramDumpHeapData) {
    if (paramDumpHeapData.runGc) {
      System.gc();
      System.runFinalization();
      System.gc();
    } 
    try {
      ParcelFileDescriptor parcelFileDescriptor = paramDumpHeapData.fd;
      try {
        if (paramDumpHeapData.managed) {
          Debug.dumpHprofData(paramDumpHeapData.path, parcelFileDescriptor.getFileDescriptor());
        } else if (paramDumpHeapData.mallocInfo) {
          Debug.dumpNativeMallocInfo(parcelFileDescriptor.getFileDescriptor());
        } else {
          Debug.dumpNativeHeap(parcelFileDescriptor.getFileDescriptor());
        } 
      } finally {
        if (parcelFileDescriptor != null)
          try {
            parcelFileDescriptor.close();
          } finally {
            parcelFileDescriptor = null;
          }  
      } 
    } catch (IOException iOException) {
      if (paramDumpHeapData.managed) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Managed heap dump failed on path ");
        stringBuilder.append(paramDumpHeapData.path);
        stringBuilder.append(" -- can the process access this path?");
        Slog.w("ActivityThread", stringBuilder.toString(), iOException);
      } else {
        Slog.w("ActivityThread", "Failed to dump heap", iOException);
      } 
    } catch (RuntimeException runtimeException) {
      Slog.wtf("ActivityThread", "Heap dumper threw a runtime exception", runtimeException);
    } 
    try {
      ActivityManager.getService().dumpHeapFinished(paramDumpHeapData.path);
      if (paramDumpHeapData.finishCallback != null)
        paramDumpHeapData.finishCallback.sendResult(null); 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  final void handleDispatchPackageBroadcast(int paramInt, String[] paramArrayOfString) {
    boolean bool1 = false, bool2 = false;
    int i = 0;
    if (paramInt != 0 && paramInt != 2) {
      if (paramInt == 3)
        if (paramArrayOfString != null) {
          ArrayList<String> arrayList = new ArrayList();
          synchronized (this.mResourcesManager) {
            i = paramArrayOfString.length;
            bool2 = false;
            i--;
            while (true) {
              if (i >= 0) {
                String str = paramArrayOfString[i];
                try {
                  LoadedApk loadedApk;
                  WeakReference<LoadedApk> weakReference = (WeakReference)this.mPackages.get(str);
                  WeakReference weakReference1 = null;
                  if (weakReference != null) {
                    loadedApk = weakReference.get();
                  } else {
                    weakReference = null;
                  } 
                  if (weakReference != null) {
                    bool2 = true;
                  } else {
                    WeakReference<LoadedApk> weakReference2 = (WeakReference)this.mResourcePackages.get(str);
                    weakReference = weakReference1;
                    if (weakReference2 != null)
                      loadedApk = weakReference2.get(); 
                    if (loadedApk != null)
                      bool2 = true; 
                  } 
                  if (loadedApk != null)
                    try {
                      arrayList.add(str);
                      try {
                        IPackageManager iPackageManager = sPackageManager;
                        int j = UserHandle.myUserId();
                        ApplicationInfo applicationInfo = iPackageManager.getApplicationInfo(str, 1024, j);
                        if (applicationInfo != null) {
                          if (this.mActivities.size() > 0)
                            for (ActivityClientRecord activityClientRecord : this.mActivities.values()) {
                              String str1 = activityClientRecord.activityInfo.applicationInfo.packageName;
                              if (str1.equals(str)) {
                                activityClientRecord.activityInfo.applicationInfo = applicationInfo;
                                activityClientRecord.packageInfo = loadedApk;
                              } 
                            }  
                          str = loadedApk.getResDir();
                          ArrayList<String> arrayList1 = new ArrayList();
                          this();
                          LoadedApk.makePaths(this, loadedApk.getApplicationInfo(), arrayList1);
                          loadedApk.updateApplicationInfo(applicationInfo, arrayList1);
                          synchronized (this.mResourcesManager) {
                            this.mResourcesManager.applyNewResourceDirsLocked(applicationInfo, new String[] { str });
                          } 
                        } 
                      } catch (RemoteException remoteException) {}
                    } finally {} 
                  i--;
                  continue;
                } finally {}
              } else {
                try {
                  IPackageManager iPackageManager = getPackageManager();
                  String[] arrayOfString = arrayList.<String>toArray(new String[0]);
                  iPackageManager.notifyPackagesReplacedReceived(arrayOfString);
                } catch (RemoteException remoteException) {}
                bool1 = bool2;
                break;
              } 
              throw paramArrayOfString;
            } 
          } 
        }  
    } else {
      if (paramInt == 0)
        i = 1; 
      if (paramArrayOfString != null)
        synchronized (this.mResourcesManager) {
          for (int j = paramArrayOfString.length - 1; j >= 0; j--, bool1 = bool2) {
            bool2 = bool1;
            if (!bool1) {
              WeakReference weakReference = (WeakReference)this.mPackages.get(paramArrayOfString[j]);
              if (weakReference != null && weakReference.get() != null) {
                bool2 = true;
              } else {
                weakReference = (WeakReference)this.mResourcePackages.get(paramArrayOfString[j]);
                bool2 = bool1;
                if (weakReference != null) {
                  bool2 = bool1;
                  if (weakReference.get() != null)
                    bool2 = true; 
                } 
              } 
            } 
            if (i != 0) {
              this.mPackages.remove(paramArrayOfString[j]);
              this.mResourcePackages.remove(paramArrayOfString[j]);
            } 
          } 
          ApplicationPackageManager.handlePackageBroadcast(paramInt, paramArrayOfString, bool1);
          return;
        }  
    } 
    ApplicationPackageManager.handlePackageBroadcast(paramInt, paramArrayOfString, bool1);
  }
  
  final void handleLowMemory() {
    ArrayList<ComponentCallbacks2> arrayList = collectComponentCallbacks(true, (Configuration)null);
    int i = arrayList.size();
    int j;
    for (j = 0; j < i; j++)
      ((ComponentCallbacks2)arrayList.get(j)).onLowMemory(); 
    if (Process.myUid() != 1000) {
      j = SQLiteDatabase.releaseMemory();
      EventLog.writeEvent(75003, j);
    } 
    Canvas.freeCaches();
    Canvas.freeTextLayoutCaches();
    BinderInternal.forceGc("mem");
  }
  
  private void handleTrimMemory(int paramInt) {
    Trace.traceBegin(64L, "trimMemory");
    if (DEBUG_MEMORY_TRIM) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Trimming memory to level: ");
      stringBuilder.append(paramInt);
      Slog.v("ActivityThread", stringBuilder.toString());
    } 
    if (paramInt >= 80)
      for (PropertyInvalidatedCache propertyInvalidatedCache : PropertyInvalidatedCache.getActiveCaches())
        propertyInvalidatedCache.clear();  
    ArrayList<ComponentCallbacks2> arrayList = collectComponentCallbacks(true, (Configuration)null);
    int i = arrayList.size();
    for (byte b = 0; b < i; b++)
      ((ComponentCallbacks2)arrayList.get(b)).onTrimMemory(paramInt); 
    WindowManagerGlobal.getInstance().trimMemory(paramInt);
    Trace.traceEnd(64L);
    if (SystemProperties.getInt("debug.am.run_gc_trim_level", 2147483647) <= paramInt) {
      unscheduleGcIdler();
      doGcIfNeeded("tm");
    } 
    if (SystemProperties.getInt("debug.am.run_mallopt_trim_level", 2147483647) <= paramInt) {
      unschedulePurgeIdler();
      purgePendingResources();
    } 
  }
  
  private void setupGraphicsSupport(Context paramContext) {
    Trace.traceBegin(64L, "setupGraphicsSupport");
    if (!"android".equals(paramContext.getPackageName())) {
      File file1 = paramContext.getCacheDir();
      if (file1 != null) {
        System.setProperty("java.io.tmpdir", file1.getAbsolutePath());
      } else {
        Log.v("ActivityThread", "Unable to initialize \"java.io.tmpdir\" property due to missing cache directory");
      } 
      Context context = paramContext.createDeviceProtectedStorageContext();
      File file2 = context.getCodeCacheDir();
      if (file2 != null) {
        try {
          int i = Process.myUid();
          String[] arrayOfString = getPackageManager().getPackagesForUid(i);
          if (arrayOfString != null) {
            HardwareRenderer.setupDiskCache(file2);
            RenderScriptCacheDir.setupDiskCache(file2);
          } 
        } catch (RemoteException remoteException) {
          Trace.traceEnd(64L);
          throw remoteException.rethrowFromSystemServer();
        } 
      } else {
        Log.w("ActivityThread", "Unable to use shader/script cache: missing code-cache directory");
      } 
    } 
    GraphicsEnvironment.getInstance().setup((Context)remoteException, this.mCoreSettings);
    Trace.traceEnd(64L);
  }
  
  private void updateDefaultDensity() {
    int i = this.mCurDefaultDisplayDpi;
    if (!this.mDensityCompatMode && i != 0 && i != DisplayMetrics.DENSITY_DEVICE) {
      DisplayMetrics.DENSITY_DEVICE = i;
      Bitmap.setDefaultDensity(i);
    } 
    ((IOplusScreenModeFeature)OplusFeatureCache.<IOplusScreenModeFeature>get(IOplusScreenModeFeature.DEFAULT)).updateCompatDensityIfNeed(this.mCurDefaultDisplayDpi);
  }
  
  private String getInstrumentationLibrary(ApplicationInfo paramApplicationInfo, InstrumentationInfo paramInstrumentationInfo) {
    if (paramApplicationInfo.primaryCpuAbi != null && paramApplicationInfo.secondaryCpuAbi != null) {
      String str1 = paramApplicationInfo.secondaryCpuAbi, str2 = paramInstrumentationInfo.secondaryCpuAbi;
      if (str1.equals(str2)) {
        String str3 = paramApplicationInfo.secondaryCpuAbi;
        str3 = VMRuntime.getInstructionSet(str3);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("ro.dalvik.vm.isa.");
        stringBuilder.append(str3);
        String str4 = stringBuilder.toString();
        str4 = SystemProperties.get(str4);
        if (!str4.isEmpty())
          str3 = str4; 
        str4 = VMRuntime.getRuntime().vmInstructionSet();
        if (str4.equals(str3))
          return paramInstrumentationInfo.secondaryNativeLibraryDir; 
      } 
    } 
    return paramInstrumentationInfo.nativeLibraryDir;
  }
  
  private void updateLocaleListFromAppContext(Context paramContext, LocaleList paramLocaleList) {
    if (paramContext.getResources() == null) {
      Slog.e("ActivityThread", "updateLocaleListFromAppContext context.getResources() is null ,return");
      return;
    } 
    Locale locale = paramContext.getResources().getConfiguration().getLocales().get(0);
    int i = paramLocaleList.size();
    for (byte b = 0; b < i; b++) {
      if (locale.equals(paramLocaleList.get(b))) {
        LocaleList.setDefault(paramLocaleList, b);
        return;
      } 
    } 
    LocaleList.setDefault(new LocaleList(locale, paramLocaleList));
  }
  
  private void handleBindApplication(AppBindData paramAppBindData) {
    // Byte code:
    //   0: invokestatic uptimeMillis : ()J
    //   3: lstore_2
    //   4: invokestatic registerSensitiveThread : ()V
    //   7: ldc_w 'debug.allocTracker.stackDepth'
    //   10: invokestatic get : (Ljava/lang/String;)Ljava/lang/String;
    //   13: astore #4
    //   15: aload #4
    //   17: invokevirtual length : ()I
    //   20: ifeq -> 31
    //   23: aload #4
    //   25: invokestatic parseInt : (Ljava/lang/String;)I
    //   28: invokestatic setAllocTrackerStackDepth : (I)V
    //   31: aload_1
    //   32: getfield trackAllocation : Z
    //   35: ifeq -> 42
    //   38: iconst_1
    //   39: invokestatic enableRecentAllocations : (Z)V
    //   42: iconst_1
    //   43: putstatic android/app/ActivityThread.sDoFrameOptEnabled : Z
    //   46: invokestatic elapsedRealtime : ()J
    //   49: invokestatic uptimeMillis : ()J
    //   52: invokestatic setStartTimes : (JJ)V
    //   55: aload_1
    //   56: getfield disabledCompatChanges : [J
    //   59: invokestatic install : ([J)V
    //   62: aload_0
    //   63: aload_1
    //   64: putfield mBoundApplication : Landroid/app/ActivityThread$AppBindData;
    //   67: aload_0
    //   68: new android/content/res/Configuration
    //   71: dup
    //   72: aload_1
    //   73: getfield config : Landroid/content/res/Configuration;
    //   76: invokespecial <init> : (Landroid/content/res/Configuration;)V
    //   79: putfield mConfiguration : Landroid/content/res/Configuration;
    //   82: aload_0
    //   83: new android/content/res/Configuration
    //   86: dup
    //   87: aload_1
    //   88: getfield config : Landroid/content/res/Configuration;
    //   91: invokespecial <init> : (Landroid/content/res/Configuration;)V
    //   94: putfield mCompatConfiguration : Landroid/content/res/Configuration;
    //   97: aload_0
    //   98: new android/app/ActivityThread$Profiler
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: putfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   108: aload_1
    //   109: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   112: ifnull -> 207
    //   115: aload_0
    //   116: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   119: aload_1
    //   120: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   123: getfield profileFile : Ljava/lang/String;
    //   126: putfield profileFile : Ljava/lang/String;
    //   129: aload_0
    //   130: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   133: aload_1
    //   134: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   137: getfield profileFd : Landroid/os/ParcelFileDescriptor;
    //   140: putfield profileFd : Landroid/os/ParcelFileDescriptor;
    //   143: aload_0
    //   144: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   147: aload_1
    //   148: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   151: getfield samplingInterval : I
    //   154: putfield samplingInterval : I
    //   157: aload_0
    //   158: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   161: aload_1
    //   162: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   165: getfield autoStopProfiler : Z
    //   168: putfield autoStopProfiler : Z
    //   171: aload_0
    //   172: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   175: aload_1
    //   176: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   179: getfield streamingOutput : Z
    //   182: putfield streamingOutput : Z
    //   185: aload_1
    //   186: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   189: getfield attachAgentDuringBind : Z
    //   192: ifeq -> 207
    //   195: aload_1
    //   196: getfield initProfilerInfo : Landroid/app/ProfilerInfo;
    //   199: getfield agent : Ljava/lang/String;
    //   202: astore #4
    //   204: goto -> 210
    //   207: aconst_null
    //   208: astore #4
    //   210: aload_1
    //   211: getfield processName : Ljava/lang/String;
    //   214: invokestatic setArgV0 : (Ljava/lang/String;)V
    //   217: aload_1
    //   218: getfield processName : Ljava/lang/String;
    //   221: astore #5
    //   223: aload_1
    //   224: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   227: getfield packageName : Ljava/lang/String;
    //   230: astore #6
    //   232: invokestatic myUserId : ()I
    //   235: istore #7
    //   237: aload #5
    //   239: aload #6
    //   241: iload #7
    //   243: invokestatic setAppName : (Ljava/lang/String;Ljava/lang/String;I)V
    //   246: aload_1
    //   247: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   250: getfield packageName : Ljava/lang/String;
    //   253: invokestatic setProcessPackageName : (Ljava/lang/String;)V
    //   256: aload_1
    //   257: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   260: getfield dataDir : Ljava/lang/String;
    //   263: invokestatic setProcessDataDirectory : (Ljava/lang/String;)V
    //   266: aload_0
    //   267: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   270: getfield profileFd : Landroid/os/ParcelFileDescriptor;
    //   273: ifnull -> 283
    //   276: aload_0
    //   277: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   280: invokevirtual startProfiling : ()V
    //   283: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   286: iconst_0
    //   287: anewarray java/lang/Object
    //   290: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   293: pop
    //   294: aload_1
    //   295: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   298: astore #6
    //   300: aload #6
    //   302: invokestatic getOplusAppInfoExFromAppInfoRef : (Landroid/content/pm/ApplicationInfo;)Landroid/content/pm/OplusApplicationInfoEx;
    //   305: astore #6
    //   307: aload #6
    //   309: ifnull -> 416
    //   312: aload #6
    //   314: bipush #8
    //   316: invokevirtual hasPrivateFlags : (I)Z
    //   319: ifeq -> 416
    //   322: new java/lang/StringBuilder
    //   325: dup
    //   326: invokespecial <init> : ()V
    //   329: astore #6
    //   331: aload #6
    //   333: ldc_w 'Compat enabled for pkg='
    //   336: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   339: pop
    //   340: aload #6
    //   342: aload_1
    //   343: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   346: getfield packageName : Ljava/lang/String;
    //   349: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   352: pop
    //   353: aload #6
    //   355: ldc_w ' compatInfo='
    //   358: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload #6
    //   364: aload_1
    //   365: getfield compatInfo : Landroid/content/res/CompatibilityInfo;
    //   368: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   371: pop
    //   372: ldc 'ActivityThread'
    //   374: aload #6
    //   376: invokevirtual toString : ()Ljava/lang/String;
    //   379: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
    //   382: pop
    //   383: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   386: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   389: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   392: iconst_1
    //   393: invokeinterface setSupportDisplayCompat : (Z)V
    //   398: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   401: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   404: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   407: aload_1
    //   408: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   411: invokeinterface initDisplayCompat : (Landroid/content/pm/ApplicationInfo;)V
    //   416: aload_1
    //   417: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   420: getfield targetSdkVersion : I
    //   423: bipush #12
    //   425: if_icmpgt -> 434
    //   428: getstatic android/os/AsyncTask.THREAD_POOL_EXECUTOR : Ljava/util/concurrent/Executor;
    //   431: invokestatic setDefaultExecutor : (Ljava/util/concurrent/Executor;)V
    //   434: aload_1
    //   435: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   438: getfield targetSdkVersion : I
    //   441: bipush #29
    //   443: if_icmplt -> 452
    //   446: iconst_1
    //   447: istore #8
    //   449: goto -> 455
    //   452: iconst_0
    //   453: istore #8
    //   455: iload #8
    //   457: invokestatic setThrowExceptionForUpperArrayOutOfBounds : (Z)V
    //   460: aload_1
    //   461: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   464: getfield targetSdkVersion : I
    //   467: invokestatic updateCheckRecycle : (I)V
    //   470: aload_1
    //   471: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   474: getfield targetSdkVersion : I
    //   477: putstatic android/graphics/ImageDecoder.sApiLevel : I
    //   480: aconst_null
    //   481: invokestatic setDefault : (Ljava/util/TimeZone;)V
    //   484: aload_1
    //   485: getfield config : Landroid/content/res/Configuration;
    //   488: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   491: invokestatic setDefault : (Landroid/os/LocaleList;)V
    //   494: aload_0
    //   495: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   498: astore #6
    //   500: aload #6
    //   502: monitorenter
    //   503: aload_0
    //   504: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   507: aload_1
    //   508: getfield config : Landroid/content/res/Configuration;
    //   511: aload_1
    //   512: getfield compatInfo : Landroid/content/res/CompatibilityInfo;
    //   515: invokevirtual applyConfigurationToResourcesLocked : (Landroid/content/res/Configuration;Landroid/content/res/CompatibilityInfo;)Z
    //   518: pop
    //   519: aload_1
    //   520: getfield config : Landroid/content/res/Configuration;
    //   523: getfield densityDpi : I
    //   526: istore #7
    //   528: aload_0
    //   529: iload #7
    //   531: putfield mCurDefaultDisplayDpi : I
    //   534: aload_0
    //   535: iload #7
    //   537: invokevirtual applyCompatConfiguration : (I)Landroid/content/res/Configuration;
    //   540: pop
    //   541: aload #6
    //   543: monitorexit
    //   544: aload_1
    //   545: aload_0
    //   546: aload_1
    //   547: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   550: aload_1
    //   551: getfield compatInfo : Landroid/content/res/CompatibilityInfo;
    //   554: invokevirtual getPackageInfoNoCheck : (Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;)Landroid/app/LoadedApk;
    //   557: putfield info : Landroid/app/LoadedApk;
    //   560: aload #4
    //   562: ifnull -> 574
    //   565: aload #4
    //   567: aload_1
    //   568: getfield info : Landroid/app/LoadedApk;
    //   571: invokestatic handleAttachAgent : (Ljava/lang/String;Landroid/app/LoadedApk;)V
    //   574: aload_1
    //   575: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   578: getfield flags : I
    //   581: sipush #8192
    //   584: iand
    //   585: ifne -> 602
    //   588: aload_0
    //   589: iconst_1
    //   590: putfield mDensityCompatMode : Z
    //   593: sipush #160
    //   596: invokestatic setDefaultDensity : (I)V
    //   599: goto -> 681
    //   602: aload_1
    //   603: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   606: invokevirtual getOverrideDensity : ()I
    //   609: istore #7
    //   611: iload #7
    //   613: ifeq -> 681
    //   616: new java/lang/StringBuilder
    //   619: dup
    //   620: invokespecial <init> : ()V
    //   623: astore #4
    //   625: aload #4
    //   627: ldc_w 'override app density from '
    //   630: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   633: pop
    //   634: aload #4
    //   636: getstatic android/util/DisplayMetrics.DENSITY_DEVICE : I
    //   639: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   642: pop
    //   643: aload #4
    //   645: ldc_w ' to '
    //   648: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   651: pop
    //   652: aload #4
    //   654: iload #7
    //   656: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   659: pop
    //   660: ldc 'ActivityThread'
    //   662: aload #4
    //   664: invokevirtual toString : ()Ljava/lang/String;
    //   667: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   670: pop
    //   671: aload_0
    //   672: iconst_1
    //   673: putfield mDensityCompatMode : Z
    //   676: iload #7
    //   678: invokestatic setDefaultDensity : (I)V
    //   681: aload_0
    //   682: invokespecial updateDefaultDensity : ()V
    //   685: aload_0
    //   686: getfield mCoreSettings : Landroid/os/Bundle;
    //   689: ldc_w 'time_12_24'
    //   692: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
    //   695: astore #4
    //   697: aload #4
    //   699: ifnull -> 729
    //   702: ldc_w '24'
    //   705: aload #4
    //   707: invokevirtual equals : (Ljava/lang/Object;)Z
    //   710: ifeq -> 721
    //   713: getstatic java/lang/Boolean.TRUE : Ljava/lang/Boolean;
    //   716: astore #4
    //   718: goto -> 726
    //   721: getstatic java/lang/Boolean.FALSE : Ljava/lang/Boolean;
    //   724: astore #4
    //   726: goto -> 732
    //   729: aconst_null
    //   730: astore #4
    //   732: aload #4
    //   734: invokestatic set24HourTimePref : (Ljava/lang/Boolean;)V
    //   737: aload_0
    //   738: invokespecial updateDebugViewAttributeState : ()Z
    //   741: pop
    //   742: aload_1
    //   743: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   746: invokestatic initThreadDefaults : (Landroid/content/pm/ApplicationInfo;)V
    //   749: aload_1
    //   750: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   753: invokestatic initVmDefaults : (Landroid/content/pm/ApplicationInfo;)V
    //   756: new com/oppo/uamodel/UaModelUtil
    //   759: dup
    //   760: invokespecial <init> : ()V
    //   763: astore #4
    //   765: aload #4
    //   767: aload_1
    //   768: getfield info : Landroid/app/LoadedApk;
    //   771: invokevirtual getPackageName : ()Ljava/lang/String;
    //   774: invokevirtual findX2LBThemeUaModelOk : (Ljava/lang/String;)Z
    //   777: ifeq -> 785
    //   780: aload #4
    //   782: invokevirtual changeToSpecialModel : ()V
    //   785: aload #4
    //   787: aload_1
    //   788: getfield info : Landroid/app/LoadedApk;
    //   791: invokevirtual getPackageName : ()Ljava/lang/String;
    //   794: invokevirtual renoACE2EVAUaModelOk : (Ljava/lang/String;)Z
    //   797: ifeq -> 805
    //   800: aload #4
    //   802: invokevirtual changeToSpecialModel : ()V
    //   805: aload #4
    //   807: aload_1
    //   808: getfield info : Landroid/app/LoadedApk;
    //   811: invokevirtual getPackageName : ()Ljava/lang/String;
    //   814: invokevirtual barceCustomUaModelOk : (Ljava/lang/String;)Z
    //   817: ifeq -> 825
    //   820: aload #4
    //   822: invokevirtual changeToSpecialModel : ()V
    //   825: aload #4
    //   827: aload_1
    //   828: getfield info : Landroid/app/LoadedApk;
    //   831: invokevirtual getPackageName : ()Ljava/lang/String;
    //   834: invokevirtual gundamCustomUaModelOk : (Ljava/lang/String;)Z
    //   837: ifeq -> 845
    //   840: aload #4
    //   842: invokevirtual changeToSpecialModel : ()V
    //   845: new com/oppo/uamodel/UaModelUtil_Michel
    //   848: dup
    //   849: invokespecial <init> : ()V
    //   852: astore #4
    //   854: aload #4
    //   856: aload_1
    //   857: getfield info : Landroid/app/LoadedApk;
    //   860: invokevirtual getPackageName : ()Ljava/lang/String;
    //   863: invokevirtual Michel_UaModel_Ok : (Ljava/lang/String;)Z
    //   866: ifeq -> 874
    //   869: aload #4
    //   871: invokevirtual changeToSpecialModel : ()V
    //   874: aload_1
    //   875: getfield debugMode : I
    //   878: ifeq -> 1044
    //   881: sipush #8100
    //   884: invokestatic changeDebugPort : (I)V
    //   887: aload_1
    //   888: getfield debugMode : I
    //   891: iconst_2
    //   892: if_icmpne -> 993
    //   895: new java/lang/StringBuilder
    //   898: dup
    //   899: invokespecial <init> : ()V
    //   902: astore #4
    //   904: aload #4
    //   906: ldc_w 'Application '
    //   909: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   912: pop
    //   913: aload #4
    //   915: aload_1
    //   916: getfield info : Landroid/app/LoadedApk;
    //   919: invokevirtual getPackageName : ()Ljava/lang/String;
    //   922: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   925: pop
    //   926: aload #4
    //   928: ldc_w ' is waiting for the debugger on port 8100...'
    //   931: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   934: pop
    //   935: ldc 'ActivityThread'
    //   937: aload #4
    //   939: invokevirtual toString : ()Ljava/lang/String;
    //   942: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   945: pop
    //   946: invokestatic getService : ()Landroid/app/IActivityManager;
    //   949: astore #4
    //   951: aload #4
    //   953: aload_0
    //   954: getfield mAppThread : Landroid/app/ActivityThread$ApplicationThread;
    //   957: iconst_1
    //   958: invokeinterface showWaitingForDebugger : (Landroid/app/IApplicationThread;Z)V
    //   963: invokestatic waitForDebugger : ()V
    //   966: aload #4
    //   968: aload_0
    //   969: getfield mAppThread : Landroid/app/ActivityThread$ApplicationThread;
    //   972: iconst_0
    //   973: invokeinterface showWaitingForDebugger : (Landroid/app/IApplicationThread;Z)V
    //   978: goto -> 1044
    //   981: astore_1
    //   982: aload_1
    //   983: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   986: athrow
    //   987: astore_1
    //   988: aload_1
    //   989: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   992: athrow
    //   993: new java/lang/StringBuilder
    //   996: dup
    //   997: invokespecial <init> : ()V
    //   1000: astore #4
    //   1002: aload #4
    //   1004: ldc_w 'Application '
    //   1007: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1010: pop
    //   1011: aload #4
    //   1013: aload_1
    //   1014: getfield info : Landroid/app/LoadedApk;
    //   1017: invokevirtual getPackageName : ()Ljava/lang/String;
    //   1020: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1023: pop
    //   1024: aload #4
    //   1026: ldc_w ' can be debugged on port 8100...'
    //   1029: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1032: pop
    //   1033: ldc 'ActivityThread'
    //   1035: aload #4
    //   1037: invokevirtual toString : ()Ljava/lang/String;
    //   1040: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1043: pop
    //   1044: aload_1
    //   1045: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1048: invokevirtual isProfileableByShell : ()Z
    //   1051: istore #8
    //   1053: iload #8
    //   1055: invokestatic setAppTracingAllowed : (Z)V
    //   1058: iload #8
    //   1060: ifne -> 1069
    //   1063: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   1066: ifeq -> 1079
    //   1069: aload_1
    //   1070: getfield enableBinderTracking : Z
    //   1073: ifeq -> 1079
    //   1076: invokestatic enableTracing : ()V
    //   1079: iload #8
    //   1081: ifne -> 1090
    //   1084: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   1087: ifeq -> 1094
    //   1090: aload_0
    //   1091: invokespecial nInitZygoteChildHeapProfiling : ()V
    //   1094: aload_1
    //   1095: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1098: getfield flags : I
    //   1101: iconst_2
    //   1102: iand
    //   1103: ifeq -> 1112
    //   1106: iconst_1
    //   1107: istore #7
    //   1109: goto -> 1115
    //   1112: iconst_0
    //   1113: istore #7
    //   1115: iload #7
    //   1117: ifne -> 1135
    //   1120: getstatic android/os/Build.IS_DEBUGGABLE : Z
    //   1123: ifeq -> 1129
    //   1126: goto -> 1135
    //   1129: iconst_0
    //   1130: istore #8
    //   1132: goto -> 1138
    //   1135: iconst_1
    //   1136: istore #8
    //   1138: iload #8
    //   1140: invokestatic setDebuggingEnabled : (Z)V
    //   1143: aload_1
    //   1144: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1147: getfield packageName : Ljava/lang/String;
    //   1150: invokestatic setPackageName : (Ljava/lang/String;)V
    //   1153: ldc2_w 64
    //   1156: ldc_w 'Setup proxies'
    //   1159: invokestatic traceBegin : (JLjava/lang/String;)V
    //   1162: ldc_w 'connectivity'
    //   1165: invokestatic getService : (Ljava/lang/String;)Landroid/os/IBinder;
    //   1168: astore #4
    //   1170: aload #4
    //   1172: ifnull -> 1208
    //   1175: aload #4
    //   1177: invokestatic asInterface : (Landroid/os/IBinder;)Landroid/net/IConnectivityManager;
    //   1180: astore #4
    //   1182: aload #4
    //   1184: aconst_null
    //   1185: invokeinterface getProxyForNetwork : (Landroid/net/Network;)Landroid/net/ProxyInfo;
    //   1190: invokestatic setHttpProxySystemProperty : (Landroid/net/ProxyInfo;)V
    //   1193: goto -> 1208
    //   1196: astore_1
    //   1197: ldc2_w 64
    //   1200: invokestatic traceEnd : (J)V
    //   1203: aload_1
    //   1204: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   1207: athrow
    //   1208: ldc2_w 64
    //   1211: invokestatic traceEnd : (J)V
    //   1214: aload_1
    //   1215: getfield instrumentationName : Landroid/content/ComponentName;
    //   1218: ifnull -> 1577
    //   1221: new android/app/ApplicationPackageManager
    //   1224: astore #4
    //   1226: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   1229: astore #5
    //   1231: invokestatic getPermissionManager : ()Landroid/permission/IPermissionManager;
    //   1234: astore #6
    //   1236: aload #4
    //   1238: aconst_null
    //   1239: aload #5
    //   1241: aload #6
    //   1243: invokespecial <init> : (Landroid/app/ContextImpl;Landroid/content/pm/IPackageManager;Landroid/permission/IPermissionManager;)V
    //   1246: aload_1
    //   1247: getfield instrumentationName : Landroid/content/ComponentName;
    //   1250: astore #6
    //   1252: aload #4
    //   1254: aload #6
    //   1256: iconst_0
    //   1257: invokevirtual getInstrumentationInfo : (Landroid/content/ComponentName;I)Landroid/content/pm/InstrumentationInfo;
    //   1260: astore #5
    //   1262: aload_1
    //   1263: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1266: getfield primaryCpuAbi : Ljava/lang/String;
    //   1269: aload #5
    //   1271: getfield primaryCpuAbi : Ljava/lang/String;
    //   1274: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   1277: ifeq -> 1306
    //   1280: aload_1
    //   1281: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1284: getfield secondaryCpuAbi : Ljava/lang/String;
    //   1287: astore #6
    //   1289: aload #5
    //   1291: getfield secondaryCpuAbi : Ljava/lang/String;
    //   1294: astore #4
    //   1296: aload #6
    //   1298: aload #4
    //   1300: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   1303: ifne -> 1452
    //   1306: new java/lang/StringBuilder
    //   1309: dup
    //   1310: invokespecial <init> : ()V
    //   1313: astore #4
    //   1315: aload #4
    //   1317: ldc_w 'Package uses different ABI(s) than its instrumentation: package['
    //   1320: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1323: pop
    //   1324: aload #4
    //   1326: aload_1
    //   1327: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1330: getfield packageName : Ljava/lang/String;
    //   1333: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1336: pop
    //   1337: aload #4
    //   1339: ldc_w ']: '
    //   1342: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1345: pop
    //   1346: aload #4
    //   1348: aload_1
    //   1349: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1352: getfield primaryCpuAbi : Ljava/lang/String;
    //   1355: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1358: pop
    //   1359: aload #4
    //   1361: ldc_w ', '
    //   1364: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1367: pop
    //   1368: aload #4
    //   1370: aload_1
    //   1371: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1374: getfield secondaryCpuAbi : Ljava/lang/String;
    //   1377: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1380: pop
    //   1381: aload #4
    //   1383: ldc_w ' instrumentation['
    //   1386: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1389: pop
    //   1390: aload #4
    //   1392: aload #5
    //   1394: getfield packageName : Ljava/lang/String;
    //   1397: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1400: pop
    //   1401: aload #4
    //   1403: ldc_w ']: '
    //   1406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1409: pop
    //   1410: aload #4
    //   1412: aload #5
    //   1414: getfield primaryCpuAbi : Ljava/lang/String;
    //   1417: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1420: pop
    //   1421: aload #4
    //   1423: ldc_w ', '
    //   1426: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1429: pop
    //   1430: aload #4
    //   1432: aload #5
    //   1434: getfield secondaryCpuAbi : Ljava/lang/String;
    //   1437: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1440: pop
    //   1441: ldc 'ActivityThread'
    //   1443: aload #4
    //   1445: invokevirtual toString : ()Ljava/lang/String;
    //   1448: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1451: pop
    //   1452: aload_0
    //   1453: aload #5
    //   1455: getfield packageName : Ljava/lang/String;
    //   1458: putfield mInstrumentationPackageName : Ljava/lang/String;
    //   1461: aload_0
    //   1462: aload #5
    //   1464: getfield sourceDir : Ljava/lang/String;
    //   1467: putfield mInstrumentationAppDir : Ljava/lang/String;
    //   1470: aload_0
    //   1471: aload #5
    //   1473: getfield splitSourceDirs : [Ljava/lang/String;
    //   1476: putfield mInstrumentationSplitAppDirs : [Ljava/lang/String;
    //   1479: aload_0
    //   1480: aload_0
    //   1481: aload_1
    //   1482: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   1485: aload #5
    //   1487: invokespecial getInstrumentationLibrary : (Landroid/content/pm/ApplicationInfo;Landroid/content/pm/InstrumentationInfo;)Ljava/lang/String;
    //   1490: putfield mInstrumentationLibDir : Ljava/lang/String;
    //   1493: aload_0
    //   1494: aload_1
    //   1495: getfield info : Landroid/app/LoadedApk;
    //   1498: invokevirtual getAppDir : ()Ljava/lang/String;
    //   1501: putfield mInstrumentedAppDir : Ljava/lang/String;
    //   1504: aload_0
    //   1505: aload_1
    //   1506: getfield info : Landroid/app/LoadedApk;
    //   1509: invokevirtual getSplitAppDirs : ()[Ljava/lang/String;
    //   1512: putfield mInstrumentedSplitAppDirs : [Ljava/lang/String;
    //   1515: aload_0
    //   1516: aload_1
    //   1517: getfield info : Landroid/app/LoadedApk;
    //   1520: invokevirtual getLibDir : ()Ljava/lang/String;
    //   1523: putfield mInstrumentedLibDir : Ljava/lang/String;
    //   1526: goto -> 1580
    //   1529: astore #4
    //   1531: goto -> 1536
    //   1534: astore #4
    //   1536: new java/lang/StringBuilder
    //   1539: dup
    //   1540: invokespecial <init> : ()V
    //   1543: astore #4
    //   1545: aload #4
    //   1547: ldc_w 'Unable to find instrumentation info for: '
    //   1550: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1553: pop
    //   1554: aload #4
    //   1556: aload_1
    //   1557: getfield instrumentationName : Landroid/content/ComponentName;
    //   1560: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1563: pop
    //   1564: new java/lang/RuntimeException
    //   1567: dup
    //   1568: aload #4
    //   1570: invokevirtual toString : ()Ljava/lang/String;
    //   1573: invokespecial <init> : (Ljava/lang/String;)V
    //   1576: athrow
    //   1577: aconst_null
    //   1578: astore #5
    //   1580: aload_0
    //   1581: aload_1
    //   1582: getfield info : Landroid/app/LoadedApk;
    //   1585: invokestatic createAppContext : (Landroid/app/ActivityThread;Landroid/app/LoadedApk;)Landroid/app/ContextImpl;
    //   1588: astore #9
    //   1590: aload_0
    //   1591: getfield mResourcesManager : Landroid/app/ResourcesManager;
    //   1594: astore #4
    //   1596: aload #4
    //   1598: invokevirtual getConfiguration : ()Landroid/content/res/Configuration;
    //   1601: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   1604: astore #4
    //   1606: aload_0
    //   1607: aload #9
    //   1609: aload #4
    //   1611: invokespecial updateLocaleListFromAppContext : (Landroid/content/Context;Landroid/os/LocaleList;)V
    //   1614: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   1617: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   1620: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   1623: invokeinterface supportDisplayCompat : ()Z
    //   1628: ifeq -> 1647
    //   1631: getstatic com/oplus/screenmode/IOplusScreenModeFeature.DEFAULT : Lcom/oplus/screenmode/IOplusScreenModeFeature;
    //   1634: invokestatic get : (Landroid/common/IOplusCommonFeature;)Landroid/common/IOplusCommonFeature;
    //   1637: checkcast com/oplus/screenmode/IOplusScreenModeFeature
    //   1640: aload #9
    //   1642: invokeinterface init : (Landroid/content/Context;)V
    //   1647: invokestatic isIsolated : ()Z
    //   1650: ifne -> 1685
    //   1653: invokestatic allowThreadDiskWritesMask : ()I
    //   1656: istore #7
    //   1658: new android/util/BoostFramework
    //   1661: dup
    //   1662: aload #9
    //   1664: invokespecial <init> : (Landroid/content/Context;)V
    //   1667: astore #6
    //   1669: iload #7
    //   1671: invokestatic setThreadPolicyMask : (I)V
    //   1674: goto -> 1688
    //   1677: astore_1
    //   1678: iload #7
    //   1680: invokestatic setThreadPolicyMask : (I)V
    //   1683: aload_1
    //   1684: athrow
    //   1685: aconst_null
    //   1686: astore #6
    //   1688: invokestatic isIsolated : ()Z
    //   1691: ifne -> 1721
    //   1694: invokestatic allowThreadDiskWritesMask : ()I
    //   1697: istore #7
    //   1699: aload_0
    //   1700: aload #9
    //   1702: invokespecial setupGraphicsSupport : (Landroid/content/Context;)V
    //   1705: iload #7
    //   1707: invokestatic setThreadPolicyMask : (I)V
    //   1710: goto -> 1725
    //   1713: astore_1
    //   1714: iload #7
    //   1716: invokestatic setThreadPolicyMask : (I)V
    //   1719: aload_1
    //   1720: athrow
    //   1721: iconst_1
    //   1722: invokestatic setIsolatedProcess : (Z)V
    //   1725: ldc2_w 64
    //   1728: ldc_w 'NetworkSecurityConfigProvider.install'
    //   1731: invokestatic traceBegin : (JLjava/lang/String;)V
    //   1734: aload #9
    //   1736: invokestatic install : (Landroid/content/Context;)V
    //   1739: ldc2_w 64
    //   1742: invokestatic traceEnd : (J)V
    //   1745: aload #5
    //   1747: ifnull -> 2080
    //   1750: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   1753: astore #4
    //   1755: aload #5
    //   1757: getfield packageName : Ljava/lang/String;
    //   1760: astore #10
    //   1762: invokestatic myUserId : ()I
    //   1765: istore #7
    //   1767: aload #4
    //   1769: aload #10
    //   1771: iconst_0
    //   1772: iload #7
    //   1774: invokeinterface getApplicationInfo : (Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    //   1779: astore #4
    //   1781: goto -> 1789
    //   1784: astore #4
    //   1786: aconst_null
    //   1787: astore #4
    //   1789: aload #4
    //   1791: ifnonnull -> 1806
    //   1794: new android/content/pm/ApplicationInfo
    //   1797: dup
    //   1798: invokespecial <init> : ()V
    //   1801: astore #4
    //   1803: goto -> 1806
    //   1806: aload #5
    //   1808: aload #4
    //   1810: invokevirtual copyTo : (Landroid/content/pm/ApplicationInfo;)V
    //   1813: aload #4
    //   1815: invokestatic myUserId : ()I
    //   1818: invokevirtual initForUser : (I)V
    //   1821: aload_1
    //   1822: getfield compatInfo : Landroid/content/res/CompatibilityInfo;
    //   1825: astore #11
    //   1827: aload #9
    //   1829: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   1832: astore #10
    //   1834: aload_0
    //   1835: aload #4
    //   1837: aload #11
    //   1839: aload #10
    //   1841: iconst_0
    //   1842: iconst_1
    //   1843: iconst_0
    //   1844: invokespecial getPackageInfo : (Landroid/content/pm/ApplicationInfo;Landroid/content/res/CompatibilityInfo;Ljava/lang/ClassLoader;ZZZ)Landroid/app/LoadedApk;
    //   1847: astore #10
    //   1849: aload #9
    //   1851: invokevirtual getOpPackageName : ()Ljava/lang/String;
    //   1854: astore #4
    //   1856: aload_0
    //   1857: aload #10
    //   1859: aload #4
    //   1861: invokestatic createAppContext : (Landroid/app/ActivityThread;Landroid/app/LoadedApk;Ljava/lang/String;)Landroid/app/ContextImpl;
    //   1864: astore #4
    //   1866: aload #4
    //   1868: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   1871: astore #11
    //   1873: aload_1
    //   1874: getfield instrumentationName : Landroid/content/ComponentName;
    //   1877: astore #10
    //   1879: aload_0
    //   1880: aload #11
    //   1882: aload #10
    //   1884: invokevirtual getClassName : ()Ljava/lang/String;
    //   1887: invokevirtual loadClass : (Ljava/lang/String;)Ljava/lang/Class;
    //   1890: invokevirtual newInstance : ()Ljava/lang/Object;
    //   1893: checkcast android/app/Instrumentation
    //   1896: putfield mInstrumentation : Landroid/app/Instrumentation;
    //   1899: new android/content/ComponentName
    //   1902: dup
    //   1903: aload #5
    //   1905: getfield packageName : Ljava/lang/String;
    //   1908: aload #5
    //   1910: getfield name : Ljava/lang/String;
    //   1913: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   1916: astore #10
    //   1918: aload_0
    //   1919: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   1922: aload_0
    //   1923: aload #4
    //   1925: aload #9
    //   1927: aload #10
    //   1929: aload_1
    //   1930: getfield instrumentationWatcher : Landroid/app/IInstrumentationWatcher;
    //   1933: aload_1
    //   1934: getfield instrumentationUiAutomationConnection : Landroid/app/IUiAutomationConnection;
    //   1937: invokevirtual init : (Landroid/app/ActivityThread;Landroid/content/Context;Landroid/content/Context;Landroid/content/ComponentName;Landroid/app/IInstrumentationWatcher;Landroid/app/IUiAutomationConnection;)V
    //   1940: aload_0
    //   1941: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   1944: getfield profileFile : Ljava/lang/String;
    //   1947: ifnull -> 2012
    //   1950: aload #5
    //   1952: getfield handleProfiling : Z
    //   1955: ifne -> 2012
    //   1958: aload_0
    //   1959: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   1962: getfield profileFd : Landroid/os/ParcelFileDescriptor;
    //   1965: ifnonnull -> 2012
    //   1968: aload_0
    //   1969: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   1972: iconst_1
    //   1973: putfield handlingProfiling : Z
    //   1976: new java/io/File
    //   1979: dup
    //   1980: aload_0
    //   1981: getfield mProfiler : Landroid/app/ActivityThread$Profiler;
    //   1984: getfield profileFile : Ljava/lang/String;
    //   1987: invokespecial <init> : (Ljava/lang/String;)V
    //   1990: astore #4
    //   1992: aload #4
    //   1994: invokevirtual getParentFile : ()Ljava/io/File;
    //   1997: invokevirtual mkdirs : ()Z
    //   2000: pop
    //   2001: aload #4
    //   2003: invokevirtual toString : ()Ljava/lang/String;
    //   2006: ldc_w 8388608
    //   2009: invokestatic startMethodTracing : (Ljava/lang/String;I)V
    //   2012: goto -> 2101
    //   2015: astore #6
    //   2017: new java/lang/StringBuilder
    //   2020: dup
    //   2021: invokespecial <init> : ()V
    //   2024: astore #4
    //   2026: aload #4
    //   2028: ldc_w 'Unable to instantiate instrumentation '
    //   2031: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2034: pop
    //   2035: aload #4
    //   2037: aload_1
    //   2038: getfield instrumentationName : Landroid/content/ComponentName;
    //   2041: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2044: pop
    //   2045: aload #4
    //   2047: ldc_w ': '
    //   2050: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2053: pop
    //   2054: aload #4
    //   2056: aload #6
    //   2058: invokevirtual toString : ()Ljava/lang/String;
    //   2061: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2064: pop
    //   2065: new java/lang/RuntimeException
    //   2068: dup
    //   2069: aload #4
    //   2071: invokevirtual toString : ()Ljava/lang/String;
    //   2074: aload #6
    //   2076: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   2079: athrow
    //   2080: new android/app/Instrumentation
    //   2083: dup
    //   2084: invokespecial <init> : ()V
    //   2087: astore #4
    //   2089: aload_0
    //   2090: aload #4
    //   2092: putfield mInstrumentation : Landroid/app/Instrumentation;
    //   2095: aload #4
    //   2097: aload_0
    //   2098: invokevirtual basicInit : (Landroid/app/ActivityThread;)V
    //   2101: aload_1
    //   2102: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   2105: getfield flags : I
    //   2108: ldc_w 1048576
    //   2111: iand
    //   2112: ifeq -> 2124
    //   2115: invokestatic getRuntime : ()Ldalvik/system/VMRuntime;
    //   2118: invokevirtual clearGrowthLimit : ()V
    //   2121: goto -> 2130
    //   2124: invokestatic getRuntime : ()Ldalvik/system/VMRuntime;
    //   2127: invokevirtual clampGrowthLimit : ()V
    //   2130: invokestatic allowThreadDiskWrites : ()Landroid/os/StrictMode$ThreadPolicy;
    //   2133: astore #11
    //   2135: invokestatic getThreadPolicy : ()Landroid/os/StrictMode$ThreadPolicy;
    //   2138: astore #5
    //   2140: aload_1
    //   2141: getfield info : Landroid/app/LoadedApk;
    //   2144: aload_1
    //   2145: getfield restrictedBackupMode : Z
    //   2148: aconst_null
    //   2149: invokevirtual makeApplication : (ZLandroid/app/Instrumentation;)Landroid/app/Application;
    //   2152: astore #10
    //   2154: aload #10
    //   2156: aload_1
    //   2157: getfield autofillOptions : Landroid/content/AutofillOptions;
    //   2160: invokevirtual setAutofillOptions : (Landroid/content/AutofillOptions;)V
    //   2163: aload #10
    //   2165: aload_1
    //   2166: getfield contentCaptureOptions : Landroid/content/ContentCaptureOptions;
    //   2169: invokevirtual setContentCaptureOptions : (Landroid/content/ContentCaptureOptions;)V
    //   2172: aload_0
    //   2173: aload #10
    //   2175: putfield mInitialApplication : Landroid/app/Application;
    //   2178: aload_0
    //   2179: monitorenter
    //   2180: aload_0
    //   2181: getfield mUpdateHttpProxyOnBind : Z
    //   2184: istore #8
    //   2186: aload_0
    //   2187: monitorexit
    //   2188: iload #8
    //   2190: ifeq -> 2206
    //   2193: aload #10
    //   2195: invokestatic updateHttpProxy : (Landroid/content/Context;)V
    //   2198: goto -> 2206
    //   2201: astore #4
    //   2203: goto -> 2730
    //   2206: aload_1
    //   2207: getfield restrictedBackupMode : Z
    //   2210: istore #8
    //   2212: iload #8
    //   2214: ifne -> 2237
    //   2217: aload_1
    //   2218: getfield providers : Ljava/util/List;
    //   2221: invokestatic isEmpty : (Ljava/util/Collection;)Z
    //   2224: ifne -> 2237
    //   2227: aload_0
    //   2228: aload #10
    //   2230: aload_1
    //   2231: getfield providers : Ljava/util/List;
    //   2234: invokespecial installContentProviders : (Landroid/content/Context;Ljava/util/List;)V
    //   2237: aload_0
    //   2238: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   2241: aload_1
    //   2242: getfield instrumentationArgs : Landroid/os/Bundle;
    //   2245: invokevirtual onCreate : (Landroid/os/Bundle;)V
    //   2248: aload_0
    //   2249: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   2252: aload #10
    //   2254: invokevirtual callApplicationOnCreate : (Landroid/app/Application;)V
    //   2257: goto -> 2280
    //   2260: astore #4
    //   2262: aload_0
    //   2263: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   2266: aload #10
    //   2268: aload #4
    //   2270: invokevirtual onException : (Ljava/lang/Object;Ljava/lang/Throwable;)Z
    //   2273: istore #8
    //   2275: iload #8
    //   2277: ifeq -> 2567
    //   2280: aload_1
    //   2281: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   2284: getfield targetSdkVersion : I
    //   2287: bipush #27
    //   2289: if_icmplt -> 2303
    //   2292: invokestatic getThreadPolicy : ()Landroid/os/StrictMode$ThreadPolicy;
    //   2295: aload #5
    //   2297: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2300: ifeq -> 2308
    //   2303: aload #11
    //   2305: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   2308: aload #9
    //   2310: invokestatic setApplicationContextForResources : (Landroid/content/Context;)V
    //   2313: invokestatic isIsolated : ()Z
    //   2316: ifne -> 2402
    //   2319: invokestatic getPackageManager : ()Landroid/content/pm/IPackageManager;
    //   2322: astore #5
    //   2324: aload_1
    //   2325: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   2328: getfield packageName : Ljava/lang/String;
    //   2331: astore #4
    //   2333: invokestatic myUserId : ()I
    //   2336: istore #7
    //   2338: aload #5
    //   2340: aload #4
    //   2342: sipush #128
    //   2345: iload #7
    //   2347: invokeinterface getApplicationInfo : (Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    //   2352: astore #4
    //   2354: aload #4
    //   2356: getfield metaData : Landroid/os/Bundle;
    //   2359: ifnull -> 2393
    //   2362: aload #4
    //   2364: getfield metaData : Landroid/os/Bundle;
    //   2367: ldc_w 'preloaded_fonts'
    //   2370: iconst_0
    //   2371: invokevirtual getInt : (Ljava/lang/String;I)I
    //   2374: istore #7
    //   2376: iload #7
    //   2378: ifeq -> 2393
    //   2381: aload_1
    //   2382: getfield info : Landroid/app/LoadedApk;
    //   2385: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   2388: iload #7
    //   2390: invokevirtual preloadFonts : (I)V
    //   2393: goto -> 2402
    //   2396: astore_1
    //   2397: aload_1
    //   2398: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   2401: athrow
    //   2402: invokestatic uptimeMillis : ()J
    //   2405: lstore #12
    //   2407: lload #12
    //   2409: lload_2
    //   2410: lsub
    //   2411: l2i
    //   2412: istore #7
    //   2414: aload #9
    //   2416: ifnull -> 2428
    //   2419: aload #9
    //   2421: invokevirtual getPackageName : ()Ljava/lang/String;
    //   2424: astore_1
    //   2425: goto -> 2430
    //   2428: aconst_null
    //   2429: astore_1
    //   2430: aload #6
    //   2432: ifnull -> 2537
    //   2435: invokestatic isIsolated : ()Z
    //   2438: ifne -> 2537
    //   2441: aload_1
    //   2442: ifnull -> 2537
    //   2445: aconst_null
    //   2446: astore #5
    //   2448: aload #9
    //   2450: invokevirtual getPackageCodePath : ()Ljava/lang/String;
    //   2453: astore #4
    //   2455: aload #4
    //   2457: iconst_0
    //   2458: aload #4
    //   2460: bipush #47
    //   2462: invokevirtual lastIndexOf : (I)I
    //   2465: invokevirtual substring : (II)Ljava/lang/String;
    //   2468: astore #4
    //   2470: goto -> 2521
    //   2473: astore #4
    //   2475: goto -> 2480
    //   2478: astore #4
    //   2480: new java/lang/StringBuilder
    //   2483: dup
    //   2484: invokespecial <init> : ()V
    //   2487: astore #9
    //   2489: aload #9
    //   2491: ldc_w 'HeavyGameThread () : Exception_1 = '
    //   2494: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2497: pop
    //   2498: aload #9
    //   2500: aload #4
    //   2502: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2505: pop
    //   2506: ldc 'ActivityThread'
    //   2508: aload #9
    //   2510: invokevirtual toString : ()Ljava/lang/String;
    //   2513: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   2516: pop
    //   2517: aload #5
    //   2519: astore #4
    //   2521: aload #6
    //   2523: iconst_2
    //   2524: iconst_0
    //   2525: aload_1
    //   2526: iload #7
    //   2528: aload #4
    //   2530: invokevirtual perfUXEngine_events : (IILjava/lang/String;ILjava/lang/String;)I
    //   2533: pop
    //   2534: goto -> 2537
    //   2537: invokestatic getService : ()Landroid/app/IActivityManager;
    //   2540: astore_1
    //   2541: aload_1
    //   2542: aload #10
    //   2544: invokevirtual getPackageName : ()Ljava/lang/String;
    //   2547: aload #10
    //   2549: invokevirtual getUserId : ()I
    //   2552: invokestatic myPid : ()I
    //   2555: invokeinterface reportBindApplicationFinished : (Ljava/lang/String;II)V
    //   2560: return
    //   2561: astore_1
    //   2562: aload_1
    //   2563: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   2566: athrow
    //   2567: new java/lang/RuntimeException
    //   2570: astore #6
    //   2572: new java/lang/StringBuilder
    //   2575: astore #9
    //   2577: aload #9
    //   2579: invokespecial <init> : ()V
    //   2582: aload #9
    //   2584: ldc_w 'Unable to create application '
    //   2587: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2590: pop
    //   2591: aload #9
    //   2593: aload #10
    //   2595: invokevirtual getClass : ()Ljava/lang/Class;
    //   2598: invokevirtual getName : ()Ljava/lang/String;
    //   2601: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2604: pop
    //   2605: aload #9
    //   2607: ldc_w ': '
    //   2610: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2613: pop
    //   2614: aload #9
    //   2616: aload #4
    //   2618: invokevirtual toString : ()Ljava/lang/String;
    //   2621: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2624: pop
    //   2625: aload #6
    //   2627: aload #9
    //   2629: invokevirtual toString : ()Ljava/lang/String;
    //   2632: aload #4
    //   2634: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   2637: aload #6
    //   2639: athrow
    //   2640: astore #9
    //   2642: new java/lang/RuntimeException
    //   2645: astore #6
    //   2647: new java/lang/StringBuilder
    //   2650: astore #4
    //   2652: aload #4
    //   2654: invokespecial <init> : ()V
    //   2657: aload #4
    //   2659: ldc_w 'Exception thrown in onCreate() of '
    //   2662: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2665: pop
    //   2666: aload #4
    //   2668: aload_1
    //   2669: getfield instrumentationName : Landroid/content/ComponentName;
    //   2672: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   2675: pop
    //   2676: aload #4
    //   2678: ldc_w ': '
    //   2681: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2684: pop
    //   2685: aload #4
    //   2687: aload #9
    //   2689: invokevirtual toString : ()Ljava/lang/String;
    //   2692: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2695: pop
    //   2696: aload #6
    //   2698: aload #4
    //   2700: invokevirtual toString : ()Ljava/lang/String;
    //   2703: aload #9
    //   2705: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   2708: aload #6
    //   2710: athrow
    //   2711: astore #4
    //   2713: aload_0
    //   2714: monitorexit
    //   2715: aload #4
    //   2717: athrow
    //   2718: astore #4
    //   2720: goto -> 2730
    //   2723: astore #4
    //   2725: goto -> 2713
    //   2728: astore #4
    //   2730: aload_1
    //   2731: getfield appInfo : Landroid/content/pm/ApplicationInfo;
    //   2734: getfield targetSdkVersion : I
    //   2737: bipush #27
    //   2739: if_icmplt -> 2753
    //   2742: invokestatic getThreadPolicy : ()Landroid/os/StrictMode$ThreadPolicy;
    //   2745: aload #5
    //   2747: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2750: ifeq -> 2758
    //   2753: aload #11
    //   2755: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   2758: aload #4
    //   2760: athrow
    //   2761: astore_1
    //   2762: aload #6
    //   2764: monitorexit
    //   2765: aload_1
    //   2766: athrow
    //   2767: astore_1
    //   2768: goto -> 2762
    // Line number table:
    //   Java source line number -> byte code offset
    //   #6805	-> 0
    //   #6806	-> 4
    //   #6808	-> 4
    //   #6810	-> 7
    //   #6811	-> 15
    //   #6812	-> 23
    //   #6814	-> 31
    //   #6815	-> 38
    //   #6820	-> 42
    //   #6823	-> 46
    //   #6825	-> 55
    //   #6826	-> 62
    //   #6827	-> 67
    //   #6828	-> 82
    //   #6830	-> 97
    //   #6831	-> 108
    //   #6832	-> 108
    //   #6833	-> 115
    //   #6834	-> 129
    //   #6835	-> 143
    //   #6836	-> 157
    //   #6837	-> 171
    //   #6838	-> 185
    //   #6839	-> 195
    //   #6844	-> 207
    //   #6845	-> 217
    //   #6847	-> 232
    //   #6845	-> 237
    //   #6848	-> 246
    //   #6852	-> 256
    //   #6854	-> 266
    //   #6855	-> 276
    //   #6859	-> 283
    //   #6860	-> 294
    //   #6861	-> 300
    //   #6862	-> 307
    //   #6865	-> 322
    //   #6868	-> 383
    //   #6869	-> 398
    //   #6877	-> 416
    //   #6878	-> 428
    //   #6882	-> 434
    //   #6885	-> 460
    //   #6892	-> 470
    //   #6900	-> 480
    //   #6905	-> 484
    //   #6907	-> 494
    //   #6913	-> 503
    //   #6914	-> 519
    //   #6917	-> 534
    //   #6918	-> 541
    //   #6920	-> 544
    //   #6922	-> 560
    //   #6923	-> 565
    //   #6929	-> 574
    //   #6931	-> 588
    //   #6932	-> 593
    //   #6934	-> 602
    //   #6935	-> 611
    //   #6936	-> 616
    //   #6937	-> 671
    //   #6938	-> 676
    //   #6941	-> 681
    //   #6943	-> 685
    //   #6944	-> 697
    //   #6945	-> 697
    //   #6946	-> 702
    //   #6945	-> 729
    //   #6951	-> 732
    //   #6953	-> 737
    //   #6955	-> 742
    //   #6956	-> 749
    //   #6961	-> 756
    //   #6962	-> 765
    //   #6963	-> 780
    //   #6965	-> 785
    //   #6966	-> 800
    //   #6968	-> 805
    //   #6969	-> 820
    //   #6971	-> 825
    //   #6972	-> 840
    //   #6974	-> 845
    //   #6975	-> 854
    //   #6976	-> 869
    //   #6981	-> 874
    //   #6983	-> 881
    //   #6984	-> 887
    //   #6985	-> 895
    //   #6988	-> 946
    //   #6990	-> 951
    //   #6993	-> 963
    //   #6995	-> 963
    //   #6998	-> 966
    //   #7001	-> 978
    //   #7003	-> 978
    //   #6999	-> 981
    //   #7000	-> 982
    //   #6991	-> 987
    //   #6992	-> 988
    //   #7004	-> 993
    //   #7010	-> 1044
    //   #7011	-> 1053
    //   #7012	-> 1058
    //   #7013	-> 1076
    //   #7017	-> 1079
    //   #7018	-> 1090
    //   #7022	-> 1094
    //   #7023	-> 1115
    //   #7024	-> 1143
    //   #7029	-> 1153
    //   #7030	-> 1162
    //   #7031	-> 1170
    //   #7035	-> 1175
    //   #7037	-> 1182
    //   #7041	-> 1193
    //   #7038	-> 1196
    //   #7039	-> 1197
    //   #7040	-> 1203
    //   #7043	-> 1208
    //   #7048	-> 1214
    //   #7050	-> 1221
    //   #7051	-> 1226
    //   #7052	-> 1252
    //   #7056	-> 1262
    //   #7059	-> 1262
    //   #7060	-> 1296
    //   #7061	-> 1306
    //   #7068	-> 1452
    //   #7069	-> 1461
    //   #7070	-> 1470
    //   #7071	-> 1479
    //   #7072	-> 1493
    //   #7073	-> 1504
    //   #7074	-> 1515
    //   #7053	-> 1529
    //   #7054	-> 1536
    //   #7076	-> 1577
    //   #7079	-> 1580
    //   #7080	-> 1590
    //   #7081	-> 1596
    //   #7080	-> 1606
    //   #7085	-> 1614
    //   #7086	-> 1631
    //   #7089	-> 1647
    //   #7090	-> 1653
    //   #7092	-> 1658
    //   #7094	-> 1669
    //   #7095	-> 1674
    //   #7094	-> 1677
    //   #7095	-> 1683
    //   #7089	-> 1685
    //   #7098	-> 1688
    //   #7099	-> 1694
    //   #7101	-> 1699
    //   #7103	-> 1705
    //   #7104	-> 1710
    //   #7105	-> 1710
    //   #7103	-> 1713
    //   #7104	-> 1719
    //   #7106	-> 1721
    //   #7112	-> 1725
    //   #7113	-> 1734
    //   #7114	-> 1739
    //   #7117	-> 1745
    //   #7120	-> 1750
    //   #7121	-> 1762
    //   #7120	-> 1767
    //   #7124	-> 1781
    //   #7122	-> 1784
    //   #7123	-> 1786
    //   #7125	-> 1789
    //   #7126	-> 1794
    //   #7125	-> 1806
    //   #7128	-> 1806
    //   #7129	-> 1813
    //   #7130	-> 1821
    //   #7131	-> 1827
    //   #7130	-> 1834
    //   #7136	-> 1849
    //   #7137	-> 1849
    //   #7136	-> 1856
    //   #7140	-> 1866
    //   #7141	-> 1873
    //   #7142	-> 1879
    //   #7147	-> 1899
    //   #7149	-> 1899
    //   #7150	-> 1918
    //   #7153	-> 1940
    //   #7155	-> 1968
    //   #7156	-> 1976
    //   #7157	-> 1992
    //   #7158	-> 2001
    //   #7160	-> 2012
    //   #7143	-> 2015
    //   #7144	-> 2017
    //   #7146	-> 2054
    //   #7161	-> 2080
    //   #7162	-> 2095
    //   #7165	-> 2101
    //   #7166	-> 2115
    //   #7170	-> 2124
    //   #7177	-> 2130
    //   #7178	-> 2135
    //   #7182	-> 2140
    //   #7185	-> 2154
    //   #7188	-> 2163
    //   #7190	-> 2172
    //   #7192	-> 2178
    //   #7193	-> 2180
    //   #7196	-> 2186
    //   #7197	-> 2188
    //   #7198	-> 2193
    //   #7231	-> 2201
    //   #7203	-> 2206
    //   #7204	-> 2217
    //   #7205	-> 2227
    //   #7212	-> 2237
    //   #7218	-> 2248
    //   #7220	-> 2248
    //   #7227	-> 2257
    //   #7221	-> 2260
    //   #7222	-> 2262
    //   #7231	-> 2280
    //   #7232	-> 2292
    //   #7233	-> 2303
    //   #7238	-> 2308
    //   #7239	-> 2313
    //   #7242	-> 2319
    //   #7245	-> 2333
    //   #7242	-> 2338
    //   #7246	-> 2354
    //   #7247	-> 2362
    //   #7249	-> 2376
    //   #7250	-> 2381
    //   #7255	-> 2393
    //   #7253	-> 2396
    //   #7254	-> 2397
    //   #7257	-> 2402
    //   #7258	-> 2407
    //   #7259	-> 2414
    //   #7260	-> 2414
    //   #7261	-> 2419
    //   #7260	-> 2428
    //   #7263	-> 2430
    //   #7264	-> 2445
    //   #7267	-> 2448
    //   #7268	-> 2455
    //   #7273	-> 2470
    //   #7270	-> 2473
    //   #7272	-> 2480
    //   #7274	-> 2521
    //   #7263	-> 2537
    //   #7281	-> 2537
    //   #7283	-> 2541
    //   #7286	-> 2560
    //   #7288	-> 2560
    //   #7284	-> 2561
    //   #7285	-> 2562
    //   #7223	-> 2567
    //   #7224	-> 2591
    //   #7225	-> 2614
    //   #7214	-> 2640
    //   #7215	-> 2642
    //   #7217	-> 2685
    //   #7196	-> 2711
    //   #7231	-> 2718
    //   #7196	-> 2723
    //   #7231	-> 2728
    //   #7232	-> 2742
    //   #7233	-> 2753
    //   #7235	-> 2758
    //   #6918	-> 2761
    // Exception table:
    //   from	to	target	type
    //   503	519	2761	finally
    //   519	534	2761	finally
    //   534	541	2761	finally
    //   541	544	2761	finally
    //   951	963	987	android/os/RemoteException
    //   966	978	981	android/os/RemoteException
    //   1182	1193	1196	android/os/RemoteException
    //   1221	1226	1534	android/content/pm/PackageManager$NameNotFoundException
    //   1226	1236	1534	android/content/pm/PackageManager$NameNotFoundException
    //   1236	1252	1529	android/content/pm/PackageManager$NameNotFoundException
    //   1252	1262	1529	android/content/pm/PackageManager$NameNotFoundException
    //   1658	1669	1677	finally
    //   1699	1705	1713	finally
    //   1750	1762	1784	android/os/RemoteException
    //   1762	1767	1784	android/os/RemoteException
    //   1767	1781	1784	android/os/RemoteException
    //   1866	1873	2015	java/lang/Exception
    //   1873	1879	2015	java/lang/Exception
    //   1879	1899	2015	java/lang/Exception
    //   2140	2154	2728	finally
    //   2154	2163	2728	finally
    //   2163	2172	2728	finally
    //   2172	2178	2728	finally
    //   2178	2180	2728	finally
    //   2180	2186	2711	finally
    //   2186	2188	2711	finally
    //   2193	2198	2201	finally
    //   2206	2212	2728	finally
    //   2217	2227	2201	finally
    //   2227	2237	2201	finally
    //   2237	2248	2640	java/lang/Exception
    //   2237	2248	2728	finally
    //   2248	2257	2260	java/lang/Exception
    //   2248	2257	2201	finally
    //   2262	2275	2728	finally
    //   2319	2333	2396	android/os/RemoteException
    //   2333	2338	2396	android/os/RemoteException
    //   2338	2354	2396	android/os/RemoteException
    //   2354	2362	2396	android/os/RemoteException
    //   2362	2376	2396	android/os/RemoteException
    //   2381	2393	2396	android/os/RemoteException
    //   2448	2455	2478	java/lang/Exception
    //   2455	2470	2473	java/lang/Exception
    //   2541	2560	2561	android/os/RemoteException
    //   2567	2591	2718	finally
    //   2591	2614	2718	finally
    //   2614	2640	2718	finally
    //   2642	2685	2718	finally
    //   2685	2711	2718	finally
    //   2713	2715	2723	finally
    //   2715	2718	2718	finally
    //   2762	2765	2767	finally
  }
  
  final void finishInstrumentation(int paramInt, Bundle paramBundle) {
    IActivityManager iActivityManager = ActivityManager.getService();
    if (this.mProfiler.profileFile != null && this.mProfiler.handlingProfiling && this.mProfiler.profileFd == null)
      Debug.stopMethodTracing(); 
    try {
      iActivityManager.finishInstrumentation(this.mAppThread, paramInt, paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void installContentProviders(Context paramContext, List<ProviderInfo> paramList) {
    ArrayList<ContentProviderHolder> arrayList = new ArrayList();
    for (ProviderInfo providerInfo : paramList) {
      if (DEBUG_PROVIDER) {
        StringBuilder stringBuilder = new StringBuilder(128);
        stringBuilder.append("Pub ");
        stringBuilder.append(providerInfo.authority);
        stringBuilder.append(": ");
        stringBuilder.append(providerInfo.name);
        Log.i("ActivityThread", stringBuilder.toString());
      } 
      ContentProviderHolder contentProviderHolder = installProvider(paramContext, (ContentProviderHolder)null, providerInfo, false, true, true);
      if (contentProviderHolder != null) {
        contentProviderHolder.noReleaseNeeded = true;
        arrayList.add(contentProviderHolder);
      } 
    } 
    try {
      IActivityManager iActivityManager = ActivityManager.getService();
      ApplicationThread applicationThread = getApplicationThread();
      iActivityManager.publishContentProviders(applicationThread, arrayList);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final IContentProvider acquireProvider(Context paramContext, String paramString, int paramInt, boolean paramBoolean) {
    IContentProvider iContentProvider = acquireExistingProvider(paramContext, paramString, paramInt, paramBoolean);
    if (iContentProvider != null)
      return iContentProvider; 
    try {
      synchronized (getGetProviderLock(paramString, paramInt)) {
        StringBuilder stringBuilder;
        IActivityManager iActivityManager = ActivityManager.getService();
        ApplicationThread applicationThread = getApplicationThread();
        String str = paramContext.getOpPackageName();
        ContentProviderHolder contentProviderHolder2 = iActivityManager.getContentProvider(applicationThread, str, paramString, paramInt, paramBoolean);
        if (contentProviderHolder2 == null) {
          if (UserManager.get(paramContext).isUserUnlocked(paramInt)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to find provider info for ");
            stringBuilder.append(paramString);
            Slog.e("ActivityThread", stringBuilder.toString());
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Failed to find provider info for ");
            stringBuilder.append(paramString);
            stringBuilder.append(" (user not unlocked)");
            Slog.w("ActivityThread", stringBuilder.toString());
          } 
          return null;
        } 
        ContentProviderHolder contentProviderHolder1 = installProvider((Context)stringBuilder, contentProviderHolder2, contentProviderHolder2.info, true, contentProviderHolder2.noReleaseNeeded, paramBoolean);
        return contentProviderHolder1.provider;
      } 
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private Object getGetProviderLock(String paramString, int paramInt) {
    ProviderKey providerKey = new ProviderKey(paramString, paramInt);
    synchronized (this.mGetProviderLocks) {
      Object object2 = this.mGetProviderLocks.get(providerKey);
      Object object1 = object2;
      if (object2 == null) {
        object1 = providerKey;
        this.mGetProviderLocks.put(providerKey, object1);
      } 
      return object1;
    } 
  }
  
  private final void incProviderRefLocked(ProviderRefCount paramProviderRefCount, boolean paramBoolean) {
    if (paramBoolean) {
      paramProviderRefCount.stableCount++;
      if (paramProviderRefCount.stableCount == 1) {
        boolean bool;
        if (paramProviderRefCount.removePending) {
          bool = true;
          if (DEBUG_PROVIDER)
            Slog.v("ActivityThread", "incProviderRef: stable snatched provider from the jaws of death"); 
          paramProviderRefCount.removePending = false;
          this.mH.removeMessages(131, paramProviderRefCount);
        } else {
          bool = false;
        } 
        try {
          if (DEBUG_PROVIDER) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("incProviderRef Now stable - ");
            stringBuilder.append(paramProviderRefCount.holder.info.name);
            stringBuilder.append(": unstableDelta=");
            stringBuilder.append(bool);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          ActivityManager.getService().refContentProvider(paramProviderRefCount.holder.connection, 1, bool);
        } catch (RemoteException remoteException) {}
      } 
    } else {
      ((ProviderRefCount)remoteException).unstableCount++;
      if (((ProviderRefCount)remoteException).unstableCount == 1)
        if (((ProviderRefCount)remoteException).removePending) {
          if (DEBUG_PROVIDER)
            Slog.v("ActivityThread", "incProviderRef: unstable snatched provider from the jaws of death"); 
          ((ProviderRefCount)remoteException).removePending = false;
          this.mH.removeMessages(131, remoteException);
        } else {
          try {
            if (DEBUG_PROVIDER) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("incProviderRef: Now unstable - ");
              stringBuilder.append(((ProviderRefCount)remoteException).holder.info.name);
              Slog.v("ActivityThread", stringBuilder.toString());
            } 
            ActivityManager.getService().refContentProvider(((ProviderRefCount)remoteException).holder.connection, 0, 1);
          } catch (RemoteException remoteException1) {}
        }  
    } 
  }
  
  public final IContentProvider acquireExistingProvider(Context paramContext, String paramString, int paramInt, boolean paramBoolean) {
    synchronized (this.mProviderMap) {
      StringBuilder stringBuilder;
      ProviderKey providerKey = new ProviderKey();
      this(paramString, paramInt);
      ProviderClientRecord providerClientRecord = (ProviderClientRecord)this.mProviderMap.get(providerKey);
      if (providerClientRecord == null)
        return null; 
      IContentProvider iContentProvider = providerClientRecord.mProvider;
      IBinder iBinder = iContentProvider.asBinder();
      if (!iBinder.isBinderAlive()) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Acquiring provider ");
        stringBuilder.append(paramString);
        stringBuilder.append(" for user ");
        stringBuilder.append(paramInt);
        stringBuilder.append(": existing object's process dead");
        Log.i("ActivityThread", stringBuilder.toString());
        handleUnstableProviderDiedLocked(iBinder, true);
        return null;
      } 
      ProviderRefCount providerRefCount = (ProviderRefCount)this.mProviderRefCountMap.get(iBinder);
      if (providerRefCount != null)
        incProviderRefLocked(providerRefCount, paramBoolean); 
      return (IContentProvider)stringBuilder;
    } 
  }
  
  public final boolean releaseProvider(IContentProvider paramIContentProvider, boolean paramBoolean) {
    int i = 0;
    if (paramIContentProvider == null)
      return false; 
    null = paramIContentProvider.asBinder();
    synchronized (this.mProviderMap) {
      ProviderRefCount providerRefCount = (ProviderRefCount)this.mProviderRefCountMap.get(null);
      if (providerRefCount == null)
        return false; 
      boolean bool = false;
      if (paramBoolean) {
        if (providerRefCount.stableCount == 0) {
          if (DEBUG_PROVIDER)
            Slog.v("ActivityThread", "releaseProvider: stable ref count already 0, how?"); 
          return false;
        } 
        providerRefCount.stableCount--;
        paramBoolean = bool;
        if (providerRefCount.stableCount == 0) {
          int j = providerRefCount.unstableCount;
          if (j == 0) {
            paramBoolean = true;
          } else {
            paramBoolean = false;
          } 
          try {
            if (DEBUG_PROVIDER) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("releaseProvider: No longer stable w/lastRef=");
              stringBuilder.append(paramBoolean);
              stringBuilder.append(" - ");
              stringBuilder.append(providerRefCount.holder.info.name);
              Slog.v("ActivityThread", stringBuilder.toString());
            } 
            IActivityManager iActivityManager = ActivityManager.getService();
            IBinder iBinder = providerRefCount.holder.connection;
            if (paramBoolean)
              i = 1; 
            iActivityManager.refContentProvider(iBinder, -1, i);
          } catch (RemoteException remoteException) {}
        } 
      } else {
        if (providerRefCount.unstableCount == 0) {
          if (DEBUG_PROVIDER)
            Slog.v("ActivityThread", "releaseProvider: unstable ref count already 0, how?"); 
          return false;
        } 
        providerRefCount.unstableCount--;
        paramBoolean = bool;
        if (providerRefCount.unstableCount == 0) {
          i = providerRefCount.stableCount;
          if (i == 0) {
            paramBoolean = true;
          } else {
            paramBoolean = false;
          } 
          bool = paramBoolean;
          paramBoolean = bool;
          if (!bool)
            try {
              if (DEBUG_PROVIDER) {
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("releaseProvider: No longer unstable - ");
                stringBuilder.append(providerRefCount.holder.info.name);
                Slog.v("ActivityThread", stringBuilder.toString());
              } 
              ActivityManager.getService().refContentProvider(providerRefCount.holder.connection, 0, -1);
              paramBoolean = bool;
            } catch (RemoteException remoteException) {
              paramBoolean = bool;
            }  
        } 
      } 
      if (paramBoolean) {
        Message message;
        if (!providerRefCount.removePending) {
          if (DEBUG_PROVIDER) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("releaseProvider: Enqueueing pending removal - ");
            stringBuilder.append(providerRefCount.holder.info.name);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          providerRefCount.removePending = true;
          message = this.mH.obtainMessage(131, providerRefCount);
          this.mH.sendMessageDelayed(message, 1000L);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Duplicate remove pending of provider ");
          stringBuilder.append(((ProviderRefCount)message).holder.info.name);
          Slog.w("ActivityThread", stringBuilder.toString());
        } 
      } 
      return true;
    } 
  }
  
  final void completeRemoveProvider(ProviderRefCount paramProviderRefCount) {
    ArrayMap<ProviderKey, ProviderClientRecord> arrayMap;
    StringBuilder stringBuilder;
    synchronized (this.mProviderMap) {
      if (!paramProviderRefCount.removePending) {
        if (DEBUG_PROVIDER)
          Slog.v("ActivityThread", "completeRemoveProvider: lost the race, provider still in use"); 
        return;
      } 
      paramProviderRefCount.removePending = false;
      IBinder iBinder = paramProviderRefCount.holder.provider.asBinder();
      ProviderRefCount providerRefCount = (ProviderRefCount)this.mProviderRefCountMap.get(iBinder);
      if (providerRefCount == paramProviderRefCount)
        this.mProviderRefCountMap.remove(iBinder); 
      for (int i = this.mProviderMap.size() - 1; i >= 0; i--) {
        ProviderClientRecord providerClientRecord = (ProviderClientRecord)this.mProviderMap.valueAt(i);
        IBinder iBinder1 = providerClientRecord.mProvider.asBinder();
        if (iBinder1 == iBinder)
          this.mProviderMap.removeAt(i); 
      } 
      try {
        if (DEBUG_PROVIDER) {
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("removeProvider: Invoking ActivityManagerService.removeContentProvider(");
          stringBuilder.append(paramProviderRefCount.holder.info.name);
          stringBuilder.append(")");
          Slog.v("ActivityThread", stringBuilder.toString());
        } 
        ActivityManager.getService().removeContentProvider(paramProviderRefCount.holder.connection, false);
      } catch (RemoteException remoteException) {}
      return;
    } 
  }
  
  final void handleUnstableProviderDied(IBinder paramIBinder, boolean paramBoolean) {
    synchronized (this.mProviderMap) {
      handleUnstableProviderDiedLocked(paramIBinder, paramBoolean);
      return;
    } 
  }
  
  final void handleUnstableProviderDiedLocked(IBinder paramIBinder, boolean paramBoolean) {
    ProviderRefCount providerRefCount = (ProviderRefCount)this.mProviderRefCountMap.get(paramIBinder);
    if (providerRefCount != null) {
      if (DEBUG_PROVIDER) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Cleaning up dead provider ");
        stringBuilder.append(paramIBinder);
        stringBuilder.append(" ");
        stringBuilder.append(providerRefCount.holder.info.name);
        Slog.v("ActivityThread", stringBuilder.toString());
      } 
      this.mProviderRefCountMap.remove(paramIBinder);
      for (int i = this.mProviderMap.size() - 1; i >= 0; i--) {
        ProviderClientRecord providerClientRecord = (ProviderClientRecord)this.mProviderMap.valueAt(i);
        if (providerClientRecord != null && providerClientRecord.mProvider.asBinder() == paramIBinder) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Removing dead content provider:");
          stringBuilder.append(providerClientRecord.mProvider.toString());
          Slog.i("ActivityThread", stringBuilder.toString());
          this.mProviderMap.removeAt(i);
        } 
      } 
      if (paramBoolean)
        try {
          ActivityManager.getService().unstableProviderDied(providerRefCount.holder.connection);
        } catch (RemoteException remoteException) {} 
    } 
  }
  
  final void appNotRespondingViaProvider(IBinder paramIBinder) {
    synchronized (this.mProviderMap) {
      ProviderRefCount providerRefCount = (ProviderRefCount)this.mProviderRefCountMap.get(paramIBinder);
      if (providerRefCount != null)
        try {
          IActivityManager iActivityManager = ActivityManager.getService();
          IBinder iBinder = providerRefCount.holder.connection;
          iActivityManager.appNotRespondingViaProvider(iBinder);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      return;
    } 
  }
  
  private ProviderClientRecord installProviderAuthoritiesLocked(IContentProvider paramIContentProvider, ContentProvider paramContentProvider, ContentProviderHolder paramContentProviderHolder) {
    String[] arrayOfString = paramContentProviderHolder.info.authority.split(";");
    int i = UserHandle.getUserId(paramContentProviderHolder.info.applicationInfo.uid);
    byte b1 = 0;
    if (paramIContentProvider != null) {
      int k;
      byte b;
      for (k = arrayOfString.length, b = 0; b < k; ) {
        switch (arrayOfString[b]) {
          case "com.android.contacts":
          case "call_log":
          case "call_log_shadow":
          case "com.android.blockednumber":
          case "com.android.calendar":
          case "downloads":
          case "telephony":
            Binder.allowBlocking(paramIContentProvider.asBinder());
            break;
        } 
        b++;
      } 
    } 
    ProviderClientRecord providerClientRecord = new ProviderClientRecord(arrayOfString, paramIContentProvider, paramContentProvider, paramContentProviderHolder);
    int j;
    byte b2;
    for (j = arrayOfString.length, b2 = b1; b2 < j; ) {
      String str = arrayOfString[b2];
      ProviderKey providerKey = new ProviderKey(str, i);
      ProviderClientRecord providerClientRecord1 = (ProviderClientRecord)this.mProviderMap.get(providerKey);
      if (providerClientRecord1 != null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Content provider ");
        stringBuilder.append(providerClientRecord.mHolder.info.name);
        stringBuilder.append(" already published as ");
        stringBuilder.append(str);
        Slog.w("ActivityThread", stringBuilder.toString());
      } else {
        this.mProviderMap.put(providerKey, providerClientRecord);
      } 
      b2++;
    } 
    return providerClientRecord;
  }
  
  private ContentProviderHolder installProvider(Context paramContext, ContentProviderHolder paramContentProviderHolder, ProviderInfo paramProviderInfo, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 89
    //   4: aload_2
    //   5: getfield provider : Landroid/content/IContentProvider;
    //   8: ifnonnull -> 14
    //   11: goto -> 89
    //   14: aload_2
    //   15: getfield provider : Landroid/content/IContentProvider;
    //   18: astore_1
    //   19: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   22: ifeq -> 83
    //   25: new java/lang/StringBuilder
    //   28: dup
    //   29: invokespecial <init> : ()V
    //   32: astore #7
    //   34: aload #7
    //   36: ldc_w 'Installing external provider '
    //   39: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload #7
    //   45: aload_3
    //   46: getfield authority : Ljava/lang/String;
    //   49: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   52: pop
    //   53: aload #7
    //   55: ldc_w ': '
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload #7
    //   64: aload_3
    //   65: getfield name : Ljava/lang/String;
    //   68: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: ldc 'ActivityThread'
    //   74: aload #7
    //   76: invokevirtual toString : ()Ljava/lang/String;
    //   79: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   82: pop
    //   83: aconst_null
    //   84: astore #7
    //   86: goto -> 522
    //   89: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   92: ifne -> 100
    //   95: iload #4
    //   97: ifeq -> 158
    //   100: new java/lang/StringBuilder
    //   103: dup
    //   104: invokespecial <init> : ()V
    //   107: astore #7
    //   109: aload #7
    //   111: ldc_w 'Loading provider '
    //   114: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   117: pop
    //   118: aload #7
    //   120: aload_3
    //   121: getfield authority : Ljava/lang/String;
    //   124: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: pop
    //   128: aload #7
    //   130: ldc_w ': '
    //   133: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload #7
    //   139: aload_3
    //   140: getfield name : Ljava/lang/String;
    //   143: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   146: pop
    //   147: ldc 'ActivityThread'
    //   149: aload #7
    //   151: invokevirtual toString : ()Ljava/lang/String;
    //   154: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   157: pop
    //   158: aconst_null
    //   159: astore #7
    //   161: aload_3
    //   162: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   165: astore #8
    //   167: aload_1
    //   168: invokevirtual getPackageName : ()Ljava/lang/String;
    //   171: aload #8
    //   173: getfield packageName : Ljava/lang/String;
    //   176: invokevirtual equals : (Ljava/lang/Object;)Z
    //   179: ifeq -> 185
    //   182: goto -> 249
    //   185: aload_0
    //   186: getfield mInitialApplication : Landroid/app/Application;
    //   189: astore #9
    //   191: aload #9
    //   193: ifnull -> 220
    //   196: aload #9
    //   198: invokevirtual getPackageName : ()Ljava/lang/String;
    //   201: aload #8
    //   203: getfield packageName : Ljava/lang/String;
    //   206: invokevirtual equals : (Ljava/lang/Object;)Z
    //   209: ifeq -> 220
    //   212: aload_0
    //   213: getfield mInitialApplication : Landroid/app/Application;
    //   216: astore_1
    //   217: goto -> 249
    //   220: aload #8
    //   222: getfield packageName : Ljava/lang/String;
    //   225: astore #9
    //   227: aload_1
    //   228: aload #9
    //   230: iconst_1
    //   231: invokevirtual createPackageContext : (Ljava/lang/String;I)Landroid/content/Context;
    //   234: astore_1
    //   235: goto -> 249
    //   238: astore_1
    //   239: aload #7
    //   241: astore_1
    //   242: goto -> 249
    //   245: astore_1
    //   246: aload #7
    //   248: astore_1
    //   249: aload_1
    //   250: ifnonnull -> 308
    //   253: new java/lang/StringBuilder
    //   256: dup
    //   257: invokespecial <init> : ()V
    //   260: astore_1
    //   261: aload_1
    //   262: ldc_w 'Unable to get context for package '
    //   265: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   268: pop
    //   269: aload_1
    //   270: aload #8
    //   272: getfield packageName : Ljava/lang/String;
    //   275: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: aload_1
    //   280: ldc_w ' while loading content provider '
    //   283: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: pop
    //   287: aload_1
    //   288: aload_3
    //   289: getfield name : Ljava/lang/String;
    //   292: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   295: pop
    //   296: ldc 'ActivityThread'
    //   298: aload_1
    //   299: invokevirtual toString : ()Ljava/lang/String;
    //   302: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   305: pop
    //   306: aconst_null
    //   307: areturn
    //   308: aload_1
    //   309: astore #7
    //   311: aload_3
    //   312: getfield splitName : Ljava/lang/String;
    //   315: ifnull -> 341
    //   318: aload_1
    //   319: aload_3
    //   320: getfield splitName : Ljava/lang/String;
    //   323: invokevirtual createContextForSplit : (Ljava/lang/String;)Landroid/content/Context;
    //   326: astore #7
    //   328: goto -> 341
    //   331: astore_1
    //   332: new java/lang/RuntimeException
    //   335: dup
    //   336: aload_1
    //   337: invokespecial <init> : (Ljava/lang/Throwable;)V
    //   340: athrow
    //   341: aload #7
    //   343: invokevirtual getClassLoader : ()Ljava/lang/ClassLoader;
    //   346: astore #9
    //   348: aload_0
    //   349: aload #8
    //   351: getfield packageName : Ljava/lang/String;
    //   354: iconst_1
    //   355: invokevirtual peekPackageInfo : (Ljava/lang/String;Z)Landroid/app/LoadedApk;
    //   358: astore #8
    //   360: aload #8
    //   362: astore_1
    //   363: aload #8
    //   365: ifnonnull -> 376
    //   368: aload_0
    //   369: invokevirtual getSystemContext : ()Landroid/app/ContextImpl;
    //   372: getfield mPackageInfo : Landroid/app/LoadedApk;
    //   375: astore_1
    //   376: aload_1
    //   377: invokevirtual getAppFactory : ()Landroid/app/AppComponentFactory;
    //   380: astore_1
    //   381: aload_3
    //   382: getfield name : Ljava/lang/String;
    //   385: astore #8
    //   387: aload_1
    //   388: aload #9
    //   390: aload #8
    //   392: invokevirtual instantiateProvider : (Ljava/lang/ClassLoader;Ljava/lang/String;)Landroid/content/ContentProvider;
    //   395: astore #8
    //   397: aload #8
    //   399: invokevirtual getIContentProvider : ()Landroid/content/IContentProvider;
    //   402: astore_1
    //   403: aload_1
    //   404: ifnonnull -> 464
    //   407: new java/lang/StringBuilder
    //   410: astore_1
    //   411: aload_1
    //   412: invokespecial <init> : ()V
    //   415: aload_1
    //   416: ldc_w 'Failed to instantiate class '
    //   419: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   422: pop
    //   423: aload_1
    //   424: aload_3
    //   425: getfield name : Ljava/lang/String;
    //   428: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   431: pop
    //   432: aload_1
    //   433: ldc_w ' from sourceDir '
    //   436: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   439: pop
    //   440: aload_1
    //   441: aload_3
    //   442: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   445: getfield sourceDir : Ljava/lang/String;
    //   448: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   451: pop
    //   452: ldc 'ActivityThread'
    //   454: aload_1
    //   455: invokevirtual toString : ()Ljava/lang/String;
    //   458: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   461: pop
    //   462: aconst_null
    //   463: areturn
    //   464: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   467: ifeq -> 510
    //   470: new java/lang/StringBuilder
    //   473: astore #9
    //   475: aload #9
    //   477: invokespecial <init> : ()V
    //   480: aload #9
    //   482: ldc_w 'Instantiating local provider '
    //   485: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   488: pop
    //   489: aload #9
    //   491: aload_3
    //   492: getfield name : Ljava/lang/String;
    //   495: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   498: pop
    //   499: ldc 'ActivityThread'
    //   501: aload #9
    //   503: invokevirtual toString : ()Ljava/lang/String;
    //   506: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   509: pop
    //   510: aload #8
    //   512: aload #7
    //   514: aload_3
    //   515: invokevirtual attachInfo : (Landroid/content/Context;Landroid/content/pm/ProviderInfo;)V
    //   518: aload #8
    //   520: astore #7
    //   522: aload_0
    //   523: getfield mProviderMap : Landroid/util/ArrayMap;
    //   526: astore #8
    //   528: aload #8
    //   530: monitorenter
    //   531: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   534: ifeq -> 593
    //   537: new java/lang/StringBuilder
    //   540: astore #9
    //   542: aload #9
    //   544: invokespecial <init> : ()V
    //   547: aload #9
    //   549: ldc_w 'Checking to add '
    //   552: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   555: pop
    //   556: aload #9
    //   558: aload_1
    //   559: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   562: pop
    //   563: aload #9
    //   565: ldc_w ' / '
    //   568: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   571: pop
    //   572: aload #9
    //   574: aload_3
    //   575: getfield name : Ljava/lang/String;
    //   578: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   581: pop
    //   582: ldc 'ActivityThread'
    //   584: aload #9
    //   586: invokevirtual toString : ()Ljava/lang/String;
    //   589: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   592: pop
    //   593: aload_1
    //   594: invokeinterface asBinder : ()Landroid/os/IBinder;
    //   599: astore #9
    //   601: aload #7
    //   603: ifnull -> 724
    //   606: new android/content/ComponentName
    //   609: astore #10
    //   611: aload #10
    //   613: aload_3
    //   614: getfield packageName : Ljava/lang/String;
    //   617: aload_3
    //   618: getfield name : Ljava/lang/String;
    //   621: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   624: aload_0
    //   625: getfield mLocalProvidersByName : Landroid/util/ArrayMap;
    //   628: aload #10
    //   630: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   633: checkcast android/app/ActivityThread$ProviderClientRecord
    //   636: astore_2
    //   637: aload_2
    //   638: ifnull -> 666
    //   641: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   644: ifeq -> 656
    //   647: ldc 'ActivityThread'
    //   649: ldc_w 'installProvider: lost the race, using existing local provider'
    //   652: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   655: pop
    //   656: aload_2
    //   657: getfield mProvider : Landroid/content/IContentProvider;
    //   660: astore_1
    //   661: aload_2
    //   662: astore_1
    //   663: goto -> 716
    //   666: new android/app/ContentProviderHolder
    //   669: dup
    //   670: aload_3
    //   671: invokespecial <init> : (Landroid/content/pm/ProviderInfo;)V
    //   674: astore_2
    //   675: aload_2
    //   676: aload_1
    //   677: putfield provider : Landroid/content/IContentProvider;
    //   680: aload_2
    //   681: iconst_1
    //   682: putfield noReleaseNeeded : Z
    //   685: aload_0
    //   686: aload_1
    //   687: aload #7
    //   689: aload_2
    //   690: invokespecial installProviderAuthoritiesLocked : (Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/ContentProviderHolder;)Landroid/app/ActivityThread$ProviderClientRecord;
    //   693: astore_1
    //   694: aload_0
    //   695: getfield mLocalProviders : Landroid/util/ArrayMap;
    //   698: aload #9
    //   700: aload_1
    //   701: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   704: pop
    //   705: aload_0
    //   706: getfield mLocalProvidersByName : Landroid/util/ArrayMap;
    //   709: aload #10
    //   711: aload_1
    //   712: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   715: pop
    //   716: aload_1
    //   717: getfield mHolder : Landroid/app/ContentProviderHolder;
    //   720: astore_1
    //   721: goto -> 874
    //   724: aload_0
    //   725: getfield mProviderRefCountMap : Landroid/util/ArrayMap;
    //   728: aload #9
    //   730: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   733: checkcast android/app/ActivityThread$ProviderRefCount
    //   736: astore_3
    //   737: aload_3
    //   738: ifnull -> 793
    //   741: getstatic android/app/ActivityThread.DEBUG_PROVIDER : Z
    //   744: ifeq -> 756
    //   747: ldc 'ActivityThread'
    //   749: ldc_w 'installProvider: lost the race, updating ref count'
    //   752: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   755: pop
    //   756: aload_3
    //   757: astore_1
    //   758: iload #5
    //   760: ifne -> 869
    //   763: aload_0
    //   764: aload_3
    //   765: iload #6
    //   767: invokespecial incProviderRefLocked : (Landroid/app/ActivityThread$ProviderRefCount;Z)V
    //   770: invokestatic getService : ()Landroid/app/IActivityManager;
    //   773: aload_2
    //   774: getfield connection : Landroid/os/IBinder;
    //   777: iload #6
    //   779: invokeinterface removeContentProvider : (Landroid/os/IBinder;Z)V
    //   784: goto -> 788
    //   787: astore_1
    //   788: aload_3
    //   789: astore_1
    //   790: goto -> 869
    //   793: aload_0
    //   794: aload_1
    //   795: aload #7
    //   797: aload_2
    //   798: invokespecial installProviderAuthoritiesLocked : (Landroid/content/IContentProvider;Landroid/content/ContentProvider;Landroid/app/ContentProviderHolder;)Landroid/app/ActivityThread$ProviderClientRecord;
    //   801: astore_3
    //   802: iload #5
    //   804: ifeq -> 826
    //   807: new android/app/ActivityThread$ProviderRefCount
    //   810: astore_1
    //   811: aload_1
    //   812: aload_2
    //   813: aload_3
    //   814: sipush #1000
    //   817: sipush #1000
    //   820: invokespecial <init> : (Landroid/app/ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V
    //   823: goto -> 858
    //   826: iload #6
    //   828: ifeq -> 846
    //   831: new android/app/ActivityThread$ProviderRefCount
    //   834: dup
    //   835: aload_2
    //   836: aload_3
    //   837: iconst_1
    //   838: iconst_0
    //   839: invokespecial <init> : (Landroid/app/ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V
    //   842: astore_1
    //   843: goto -> 858
    //   846: new android/app/ActivityThread$ProviderRefCount
    //   849: dup
    //   850: aload_2
    //   851: aload_3
    //   852: iconst_0
    //   853: iconst_1
    //   854: invokespecial <init> : (Landroid/app/ContentProviderHolder;Landroid/app/ActivityThread$ProviderClientRecord;II)V
    //   857: astore_1
    //   858: aload_0
    //   859: getfield mProviderRefCountMap : Landroid/util/ArrayMap;
    //   862: aload #9
    //   864: aload_1
    //   865: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   868: pop
    //   869: aload_1
    //   870: getfield holder : Landroid/app/ContentProviderHolder;
    //   873: astore_1
    //   874: aload #8
    //   876: monitorexit
    //   877: aload_1
    //   878: areturn
    //   879: astore_1
    //   880: aload #8
    //   882: monitorexit
    //   883: aload_1
    //   884: athrow
    //   885: astore_1
    //   886: goto -> 880
    //   889: astore_1
    //   890: aload_0
    //   891: getfield mInstrumentation : Landroid/app/Instrumentation;
    //   894: aconst_null
    //   895: aload_1
    //   896: invokevirtual onException : (Ljava/lang/Object;Ljava/lang/Throwable;)Z
    //   899: ifeq -> 904
    //   902: aconst_null
    //   903: areturn
    //   904: new java/lang/StringBuilder
    //   907: dup
    //   908: invokespecial <init> : ()V
    //   911: astore_2
    //   912: aload_2
    //   913: ldc_w 'Unable to get provider '
    //   916: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   919: pop
    //   920: aload_2
    //   921: aload_3
    //   922: getfield name : Ljava/lang/String;
    //   925: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   928: pop
    //   929: aload_2
    //   930: ldc_w ': '
    //   933: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   936: pop
    //   937: aload_2
    //   938: aload_1
    //   939: invokevirtual toString : ()Ljava/lang/String;
    //   942: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   945: pop
    //   946: new java/lang/RuntimeException
    //   949: dup
    //   950: aload_2
    //   951: invokevirtual toString : ()Ljava/lang/String;
    //   954: aload_1
    //   955: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   958: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #7721	-> 0
    //   #7723	-> 0
    //   #7788	-> 14
    //   #7789	-> 19
    //   #7795	-> 83
    //   #7724	-> 89
    //   #7725	-> 100
    //   #7728	-> 158
    //   #7729	-> 161
    //   #7730	-> 167
    //   #7731	-> 182
    //   #7732	-> 185
    //   #7733	-> 196
    //   #7734	-> 212
    //   #7737	-> 220
    //   #7741	-> 235
    //   #7739	-> 238
    //   #7743	-> 249
    //   #7744	-> 253
    //   #7748	-> 306
    //   #7751	-> 308
    //   #7753	-> 318
    //   #7756	-> 328
    //   #7754	-> 331
    //   #7755	-> 332
    //   #7760	-> 341
    //   #7761	-> 348
    //   #7762	-> 360
    //   #7764	-> 368
    //   #7766	-> 376
    //   #7767	-> 387
    //   #7768	-> 397
    //   #7769	-> 403
    //   #7770	-> 407
    //   #7773	-> 462
    //   #7775	-> 464
    //   #7778	-> 510
    //   #7786	-> 518
    //   #7787	-> 518
    //   #7795	-> 522
    //   #7796	-> 531
    //   #7798	-> 593
    //   #7799	-> 601
    //   #7800	-> 606
    //   #7801	-> 624
    //   #7802	-> 637
    //   #7803	-> 641
    //   #7804	-> 647
    //   #7807	-> 656
    //   #7809	-> 666
    //   #7810	-> 675
    //   #7811	-> 680
    //   #7812	-> 685
    //   #7813	-> 694
    //   #7814	-> 705
    //   #7816	-> 716
    //   #7817	-> 721
    //   #7818	-> 724
    //   #7819	-> 737
    //   #7820	-> 741
    //   #7821	-> 747
    //   #7827	-> 756
    //   #7828	-> 763
    //   #7830	-> 770
    //   #7832	-> 787
    //   #7834	-> 788
    //   #7837	-> 793
    //   #7839	-> 802
    //   #7840	-> 807
    //   #7842	-> 826
    //   #7843	-> 831
    //   #7844	-> 846
    //   #7846	-> 858
    //   #7848	-> 869
    //   #7850	-> 874
    //   #7851	-> 877
    //   #7850	-> 879
    //   #7779	-> 889
    //   #7780	-> 890
    //   #7785	-> 902
    //   #7781	-> 904
    //   #7783	-> 937
    // Exception table:
    //   from	to	target	type
    //   220	227	245	android/content/pm/PackageManager$NameNotFoundException
    //   227	235	238	android/content/pm/PackageManager$NameNotFoundException
    //   318	328	331	android/content/pm/PackageManager$NameNotFoundException
    //   341	348	889	java/lang/Exception
    //   348	360	889	java/lang/Exception
    //   368	376	889	java/lang/Exception
    //   376	387	889	java/lang/Exception
    //   387	397	889	java/lang/Exception
    //   397	403	889	java/lang/Exception
    //   407	462	889	java/lang/Exception
    //   464	510	889	java/lang/Exception
    //   510	518	889	java/lang/Exception
    //   531	593	879	finally
    //   593	601	879	finally
    //   606	624	879	finally
    //   624	637	879	finally
    //   641	647	879	finally
    //   647	656	879	finally
    //   656	661	879	finally
    //   666	675	879	finally
    //   675	680	885	finally
    //   680	685	885	finally
    //   685	694	885	finally
    //   694	705	885	finally
    //   705	716	885	finally
    //   716	721	885	finally
    //   724	737	879	finally
    //   741	747	879	finally
    //   747	756	879	finally
    //   763	770	879	finally
    //   770	784	787	android/os/RemoteException
    //   770	784	879	finally
    //   793	802	879	finally
    //   807	823	879	finally
    //   831	843	879	finally
    //   846	858	879	finally
    //   858	869	879	finally
    //   869	874	879	finally
    //   874	877	885	finally
    //   880	883	885	finally
  }
  
  private void handleRunIsolatedEntryPoint(String paramString, String[] paramArrayOfString) {
    try {
      Method method = Class.forName(paramString).getMethod("main", new Class[] { String[].class });
      method.invoke(null, new Object[] { paramArrayOfString });
      System.exit(0);
      return;
    } catch (ReflectiveOperationException reflectiveOperationException) {
      throw new AndroidRuntimeException("runIsolatedEntryPoint failed", reflectiveOperationException);
    } 
  }
  
  private void attach(boolean paramBoolean, long paramLong) {
    sCurrentActivityThread = this;
    this.mSystemThread = paramBoolean;
    ActivityDynamicLogHelper.enableDynamicalLogIfNeed();
    this.mDebugOn = SystemProperties.getBoolean("persist.sys.assert.panic", false);
    if (!paramBoolean) {
      int i = UserHandle.myUserId();
      DdmHandleAppName.setAppName("<pre-initialized>", i);
      RuntimeInit.setApplicationObject(this.mAppThread.asBinder());
      IActivityManager iActivityManager = ActivityManager.getService();
      try {
        iActivityManager.attachApplication(this.mAppThread, paramLong);
        BinderInternal.addGcWatcher((Runnable)new Object(this));
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } else {
      int i = UserHandle.myUserId();
      DdmHandleAppName.setAppName("system_process", i);
      try {
        Instrumentation instrumentation = new Instrumentation();
        this();
        this.mInstrumentation = instrumentation;
        instrumentation.basicInit(this);
        LoadedApk loadedApk = (getSystemContext()).mPackageInfo;
        ContextImpl contextImpl = ContextImpl.createAppContext(this, loadedApk);
        Application application = contextImpl.mPackageInfo.makeApplication(true, null);
        application.onCreate();
        sendMessage(9999, (Object)null);
        _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y1 = new _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y(this);
        ViewRootImpl.addConfigCallback(_$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y1);
        return;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to instantiate Application():");
        stringBuilder.append(exception.toString());
        throw new RuntimeException(stringBuilder.toString(), exception);
      } 
    } 
    sendMessage(9999, (Object)null);
    _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y = new _$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y(this);
    ViewRootImpl.addConfigCallback(_$$Lambda$ActivityThread$7yWwCvhfDgOc1FXQL9FUcwCTz5Y);
  }
  
  public static ActivityThread systemMain() {
    if (!ActivityManager.isHighEndGfx()) {
      ThreadedRenderer.disable(true);
    } else {
      ThreadedRenderer.enableForegroundTrimming();
    } 
    ActivityThread activityThread = new ActivityThread();
    activityThread.attach(true, 0L);
    return activityThread;
  }
  
  public static void updateHttpProxy(Context paramContext) {
    ConnectivityManager connectivityManager = ConnectivityManager.from(paramContext);
    Proxy.setHttpProxySystemProperty(connectivityManager.getDefaultProxy());
  }
  
  public final void installSystemProviders(List<ProviderInfo> paramList) {
    if (paramList != null)
      installContentProviders(this.mInitialApplication, paramList); 
  }
  
  public Bundle getCoreSettings() {
    return this.mCoreSettings;
  }
  
  public int getIntCoreSetting(String paramString, int paramInt) {
    synchronized (this.mResourcesManager) {
      if (this.mCoreSettings != null) {
        paramInt = this.mCoreSettings.getInt(paramString, paramInt);
        return paramInt;
      } 
      return paramInt;
    } 
  }
  
  public String getStringCoreSetting(String paramString1, String paramString2) {
    synchronized (this.mResourcesManager) {
      if (this.mCoreSettings != null) {
        paramString1 = this.mCoreSettings.getString(paramString1, paramString2);
        return paramString1;
      } 
      return paramString2;
    } 
  }
  
  float getFloatCoreSetting(String paramString, float paramFloat) {
    synchronized (this.mResourcesManager) {
      if (this.mCoreSettings != null) {
        paramFloat = this.mCoreSettings.getFloat(paramString, paramFloat);
        return paramFloat;
      } 
      return paramFloat;
    } 
  }
  
  class AndroidOs extends ForwardingOs {
    public static void install() {
      Os os;
      if (!ContentResolver.DEPRECATE_DATA_COLUMNS)
        return; 
      do {
        os = Os.getDefault();
      } while (!Os.compareAndSetDefault(os, (Os)new AndroidOs(os)));
    }
    
    private AndroidOs(ActivityThread this$0) {
      super((Os)this$0);
    }
    
    private FileDescriptor openDeprecatedDataPath(String param1String, int param1Int) throws ErrnoException {
      Uri uri = ContentResolver.translateDeprecatedDataPath(param1String);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Redirecting ");
      stringBuilder.append(param1String);
      stringBuilder.append(" to ");
      stringBuilder.append(uri);
      Log.v("ActivityThread", stringBuilder.toString());
      Application application = ActivityThread.currentActivityThread().getApplication();
      ContentResolver contentResolver = application.getContentResolver();
      try {
        FileDescriptor fileDescriptor = new FileDescriptor();
        this();
        String str = FileUtils.translateModePosixToString(param1Int);
        ParcelFileDescriptor parcelFileDescriptor = contentResolver.openFileDescriptor(uri, str);
        param1Int = parcelFileDescriptor.detachFd();
        fileDescriptor.setInt$(param1Int);
        return fileDescriptor;
      } catch (SecurityException securityException) {
        throw new ErrnoException(securityException.getMessage(), OsConstants.EACCES);
      } catch (FileNotFoundException fileNotFoundException) {
        throw new ErrnoException(fileNotFoundException.getMessage(), OsConstants.ENOENT);
      } 
    }
    
    private void deleteDeprecatedDataPath(String param1String) throws ErrnoException {
      Uri uri = ContentResolver.translateDeprecatedDataPath(param1String);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Redirecting ");
      stringBuilder.append(param1String);
      stringBuilder.append(" to ");
      stringBuilder.append(uri);
      Log.v("ActivityThread", stringBuilder.toString());
      Application application = ActivityThread.currentActivityThread().getApplication();
      ContentResolver contentResolver = application.getContentResolver();
      try {
        if (contentResolver.delete(uri, null, null) != 0)
          return; 
        FileNotFoundException fileNotFoundException = new FileNotFoundException();
        this();
        throw fileNotFoundException;
      } catch (SecurityException securityException) {
        throw new ErrnoException(securityException.getMessage(), OsConstants.EACCES);
      } catch (FileNotFoundException fileNotFoundException) {
        throw new ErrnoException(fileNotFoundException.getMessage(), OsConstants.ENOENT);
      } 
    }
    
    public boolean access(String param1String, int param1Int) throws ErrnoException {
      FileDescriptor fileDescriptor;
      if (param1String != null && param1String.startsWith("/mnt/content/")) {
        fileDescriptor = openDeprecatedDataPath(param1String, FileUtils.translateModeAccessToPosix(param1Int));
        IoUtils.closeQuietly(fileDescriptor);
        return true;
      } 
      String str = OplusMultiAppManager.getSharedParalledPathIfNeeded("access", (String)fileDescriptor);
      return super.access(str, param1Int);
    }
    
    public FileDescriptor open(String param1String, int param1Int1, int param1Int2) throws ErrnoException {
      if (param1String != null && param1String.startsWith("/mnt/content/"))
        return openDeprecatedDataPath(param1String, param1Int2); 
      param1String = OplusMultiAppManager.getSharedParalledPathIfNeeded("open", param1String);
      return super.open(param1String, param1Int1, param1Int2);
    }
    
    public StructStat stat(String param1String) throws ErrnoException {
      FileDescriptor fileDescriptor;
      if (param1String != null && param1String.startsWith("/mnt/content/")) {
        fileDescriptor = openDeprecatedDataPath(param1String, OsConstants.O_RDONLY);
        try {
          return Os.fstat(fileDescriptor);
        } finally {
          IoUtils.closeQuietly(fileDescriptor);
        } 
      } 
      String str = OplusMultiAppManager.getSharedParalledPathIfNeeded("stat", (String)fileDescriptor);
      return super.stat(str);
    }
    
    public void unlink(String param1String) throws ErrnoException {
      if (param1String != null && param1String.startsWith("/mnt/content/")) {
        deleteDeprecatedDataPath(param1String);
      } else {
        super.unlink(param1String);
      } 
    }
    
    public void remove(String param1String) throws ErrnoException {
      if (param1String != null && param1String.startsWith("/mnt/content/")) {
        deleteDeprecatedDataPath(param1String);
      } else {
        super.remove(param1String);
      } 
    }
    
    public void rename(String param1String1, String param1String2) throws ErrnoException {
      try {
        super.rename(param1String1, param1String2);
      } catch (ErrnoException errnoException) {
        if (errnoException.errno == OsConstants.EXDEV && param1String1.startsWith("/storage/emulated") && param1String2.startsWith("/storage/emulated")) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Recovering failed rename ");
          stringBuilder.append(param1String1);
          stringBuilder.append(" to ");
          stringBuilder.append(param1String2);
          Log.v("ActivityThread", stringBuilder.toString());
          try {
            File file = new File();
            this(param1String1);
            Path path = file.toPath();
            file = new File();
            this(param1String2);
            Files.move(path, file.toPath(), new CopyOption[] { StandardCopyOption.REPLACE_EXISTING });
            return;
          } catch (IOException iOException) {
            Log.e("ActivityThread", "Rename recovery failed ", (Throwable)errnoException);
            throw errnoException;
          } 
        } 
      } 
    }
  }
  
  public static void main(String[] paramArrayOfString) {
    Trace.traceBegin(64L, "ActivityThreadMain");
    AndroidOs.install();
    CloseGuard.setEnabled(false);
    Environment.initForCurrentUser();
    File file = Environment.getUserConfigDirectory(UserHandle.myUserId());
    TrustedCertificateStore.setDefaultUserDirectory(file);
    initializeMainlineModules();
    Process.setArgV0("<pre-initialized>");
    Looper.prepareMainLooper();
    long l1 = 0L;
    long l2 = l1;
    if (paramArrayOfString != null) {
      int i = paramArrayOfString.length - 1;
      while (true) {
        l2 = l1;
        if (i >= 0) {
          l2 = l1;
          if (paramArrayOfString[i] != null) {
            l2 = l1;
            if (paramArrayOfString[i].startsWith("seq=")) {
              String str = paramArrayOfString[i];
              str = str.substring("seq=".length());
              l2 = Long.parseLong(str);
            } 
          } 
          i--;
          l1 = l2;
          continue;
        } 
        break;
      } 
    } 
    ActivityThread activityThread = new ActivityThread();
    activityThread.attach(false, l2);
    if (sMainThreadHandler == null)
      sMainThreadHandler = activityThread.getHandler(); 
    Trace.traceEnd(64L);
    Looper.loop();
    throw new RuntimeException("Main thread loop unexpectedly exited");
  }
  
  public static void initializeMainlineModules() {
    TelephonyFrameworkInitializer.setTelephonyServiceManager(new TelephonyServiceManager());
    StatsFrameworkInitializer.setStatsServiceManager(new StatsServiceManager());
  }
  
  private void purgePendingResources() {
    Trace.traceBegin(64L, "purgePendingResources");
    nPurgePendingResources();
    Trace.traceEnd(64L);
  }
  
  private native void nDumpGraphicsInfo(FileDescriptor paramFileDescriptor);
  
  private native void nInitZygoteChildHeapProfiling();
  
  private native void nPurgePendingResources();
}
