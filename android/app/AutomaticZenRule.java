package android.app;

import android.content.ComponentName;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.service.notification.ZenPolicy;
import java.util.Objects;

public final class AutomaticZenRule implements Parcelable {
  @Deprecated
  public AutomaticZenRule(String paramString, ComponentName paramComponentName, Uri paramUri, int paramInt, boolean paramBoolean) {
    this(paramString, paramComponentName, null, paramUri, null, paramInt, paramBoolean);
  }
  
  public AutomaticZenRule(String paramString, ComponentName paramComponentName1, ComponentName paramComponentName2, Uri paramUri, ZenPolicy paramZenPolicy, int paramInt, boolean paramBoolean) {
    this.enabled = false;
    this.mModified = false;
    this.name = paramString;
    this.owner = paramComponentName1;
    this.configurationActivity = paramComponentName2;
    this.conditionId = paramUri;
    this.interruptionFilter = paramInt;
    this.enabled = paramBoolean;
    this.mZenPolicy = paramZenPolicy;
  }
  
  public AutomaticZenRule(String paramString, ComponentName paramComponentName1, ComponentName paramComponentName2, Uri paramUri, ZenPolicy paramZenPolicy, int paramInt, boolean paramBoolean, long paramLong) {
    this(paramString, paramComponentName1, paramComponentName2, paramUri, paramZenPolicy, paramInt, paramBoolean);
    this.creationTime = paramLong;
  }
  
  public AutomaticZenRule(Parcel paramParcel) {
    boolean bool1 = false;
    this.enabled = false;
    this.mModified = false;
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.enabled = bool2;
    if (paramParcel.readInt() == 1)
      this.name = paramParcel.readString(); 
    this.interruptionFilter = paramParcel.readInt();
    this.conditionId = (Uri)paramParcel.readParcelable(null);
    this.owner = (ComponentName)paramParcel.readParcelable(null);
    this.configurationActivity = (ComponentName)paramParcel.readParcelable(null);
    this.creationTime = paramParcel.readLong();
    this.mZenPolicy = (ZenPolicy)paramParcel.readParcelable(null);
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.mModified = bool2;
  }
  
  public ComponentName getOwner() {
    return this.owner;
  }
  
  public ComponentName getConfigurationActivity() {
    return this.configurationActivity;
  }
  
  public Uri getConditionId() {
    return this.conditionId;
  }
  
  public int getInterruptionFilter() {
    return this.interruptionFilter;
  }
  
  public String getName() {
    return this.name;
  }
  
  public boolean isEnabled() {
    return this.enabled;
  }
  
  public boolean isModified() {
    return this.mModified;
  }
  
  public ZenPolicy getZenPolicy() {
    ZenPolicy zenPolicy = this.mZenPolicy;
    if (zenPolicy == null) {
      zenPolicy = null;
    } else {
      zenPolicy = zenPolicy.copy();
    } 
    return zenPolicy;
  }
  
  public long getCreationTime() {
    return this.creationTime;
  }
  
  public void setConditionId(Uri paramUri) {
    this.conditionId = paramUri;
  }
  
  public void setInterruptionFilter(int paramInt) {
    this.interruptionFilter = paramInt;
  }
  
  public void setName(String paramString) {
    this.name = paramString;
  }
  
  public void setEnabled(boolean paramBoolean) {
    this.enabled = paramBoolean;
  }
  
  public void setModified(boolean paramBoolean) {
    this.mModified = paramBoolean;
  }
  
  public void setZenPolicy(ZenPolicy paramZenPolicy) {
    if (paramZenPolicy == null) {
      paramZenPolicy = null;
    } else {
      paramZenPolicy = paramZenPolicy.copy();
    } 
    this.mZenPolicy = paramZenPolicy;
  }
  
  public void setConfigurationActivity(ComponentName paramComponentName) {
    this.configurationActivity = paramComponentName;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.enabled);
    if (this.name != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.name);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.interruptionFilter);
    paramParcel.writeParcelable((Parcelable)this.conditionId, 0);
    paramParcel.writeParcelable(this.owner, 0);
    paramParcel.writeParcelable(this.configurationActivity, 0);
    paramParcel.writeLong(this.creationTime);
    paramParcel.writeParcelable((Parcelable)this.mZenPolicy, 0);
    paramParcel.writeInt(this.mModified);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(AutomaticZenRule.class.getSimpleName());
    stringBuilder.append('[');
    stringBuilder.append("enabled=");
    stringBuilder.append(this.enabled);
    stringBuilder.append(",name=");
    stringBuilder.append(this.name);
    stringBuilder.append(",interruptionFilter=");
    stringBuilder.append(this.interruptionFilter);
    stringBuilder.append(",conditionId=");
    stringBuilder.append(this.conditionId);
    stringBuilder.append(",owner=");
    stringBuilder.append(this.owner);
    stringBuilder.append(",configActivity=");
    stringBuilder.append(this.configurationActivity);
    stringBuilder.append(",creationTime=");
    stringBuilder.append(this.creationTime);
    stringBuilder.append(",mZenPolicy=");
    stringBuilder.append(this.mZenPolicy);
    stringBuilder.append(']');
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof AutomaticZenRule;
    boolean bool1 = false;
    if (!bool)
      return false; 
    if (paramObject == this)
      return true; 
    paramObject = paramObject;
    if (((AutomaticZenRule)paramObject).enabled == this.enabled && ((AutomaticZenRule)paramObject).mModified == this.mModified) {
      String str1 = ((AutomaticZenRule)paramObject).name, str2 = this.name;
      if (Objects.equals(str1, str2) && ((AutomaticZenRule)paramObject).interruptionFilter == this.interruptionFilter) {
        Uri uri1 = ((AutomaticZenRule)paramObject).conditionId, uri2 = this.conditionId;
        if (Objects.equals(uri1, uri2)) {
          ComponentName componentName1 = ((AutomaticZenRule)paramObject).owner, componentName2 = this.owner;
          if (Objects.equals(componentName1, componentName2)) {
            ZenPolicy zenPolicy2 = ((AutomaticZenRule)paramObject).mZenPolicy, zenPolicy1 = this.mZenPolicy;
            if (Objects.equals(zenPolicy2, zenPolicy1)) {
              ComponentName componentName4 = ((AutomaticZenRule)paramObject).configurationActivity, componentName3 = this.configurationActivity;
              if (Objects.equals(componentName4, componentName3) && ((AutomaticZenRule)paramObject).creationTime == this.creationTime)
                bool1 = true; 
            } 
          } 
        } 
      } 
    } 
    return bool1;
  }
  
  public int hashCode() {
    boolean bool1 = this.enabled;
    String str = this.name;
    int i = this.interruptionFilter;
    Uri uri = this.conditionId;
    ComponentName componentName1 = this.owner, componentName2 = this.configurationActivity;
    ZenPolicy zenPolicy = this.mZenPolicy;
    boolean bool2 = this.mModified;
    long l = this.creationTime;
    return Objects.hash(new Object[] { Boolean.valueOf(bool1), str, Integer.valueOf(i), uri, componentName1, componentName2, zenPolicy, Boolean.valueOf(bool2), Long.valueOf(l) });
  }
  
  public static final Parcelable.Creator<AutomaticZenRule> CREATOR = new Parcelable.Creator<AutomaticZenRule>() {
      public AutomaticZenRule createFromParcel(Parcel param1Parcel) {
        return new AutomaticZenRule(param1Parcel);
      }
      
      public AutomaticZenRule[] newArray(int param1Int) {
        return new AutomaticZenRule[param1Int];
      }
    };
  
  private static final int DISABLED = 0;
  
  private static final int ENABLED = 1;
  
  private Uri conditionId;
  
  private ComponentName configurationActivity;
  
  private long creationTime;
  
  private boolean enabled;
  
  private int interruptionFilter;
  
  private boolean mModified;
  
  private ZenPolicy mZenPolicy;
  
  private String name;
  
  private ComponentName owner;
}
