package android.app;

import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import android.view.IWindowManager;
import com.oplus.app.IOplusFreeformConfigChangedListener;
import java.util.List;
import java.util.Map;

public class OplusFreeformManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  private static final String TAG = "OplusFreeformManager";
  
  public static final int TYPE_BLACK_PKG = 64;
  
  public static final int TYPE_FULLSCREEN_CPN = 16;
  
  public static final int TYPE_NEXT_FULLSCREEN_CPN = 32;
  
  public static final int TYPE_ROOT_PKG = 2;
  
  public static final int TYPE_SECURE_CPN = 4;
  
  public static final int TYPE_SPECIAL_CPN = 8;
  
  public static final int TYPE_SUPPORT_PKG = 1;
  
  private static OplusFreeformManager sColorFreeformManager;
  
  private final Map<OnConfigChangedListener, IOplusFreeformConfigChangedListener> mConfigListeners = (Map<OnConfigChangedListener, IOplusFreeformConfigChangedListener>)new ArrayMap();
  
  private OplusActivityManager mOAms;
  
  private IWindowManager mWms;
  
  public static OplusFreeformManager getInstance() {
    // Byte code:
    //   0: getstatic android/app/OplusFreeformManager.sColorFreeformManager : Landroid/app/OplusFreeformManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/app/OplusFreeformManager
    //   8: monitorenter
    //   9: getstatic android/app/OplusFreeformManager.sColorFreeformManager : Landroid/app/OplusFreeformManager;
    //   12: ifnonnull -> 27
    //   15: new android/app/OplusFreeformManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/app/OplusFreeformManager.sColorFreeformManager : Landroid/app/OplusFreeformManager;
    //   27: ldc android/app/OplusFreeformManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/app/OplusFreeformManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/app/OplusFreeformManager.sColorFreeformManager : Landroid/app/OplusFreeformManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #57	-> 0
    //   #58	-> 6
    //   #59	-> 9
    //   #60	-> 15
    //   #62	-> 27
    //   #64	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  private OplusFreeformManager() {
    this.mOAms = new OplusActivityManager();
    IBinder iBinder = ServiceManager.getService("window");
    this.mWms = IWindowManager.Stub.asInterface(iBinder);
  }
  
  public List<String> getFreeformConfigList(int paramInt) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getFreeformConfigList type = ");
      stringBuilder.append(paramInt);
      Log.d("OplusFreeformManager", stringBuilder.toString());
    } 
    OplusActivityManager oplusActivityManager = this.mOAms;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.getFreeformConfigList(paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusFreeformManager", "getFreeformConfigList remote exception");
        remoteException.printStackTrace();
      }  
    return null;
  }
  
  public boolean isFreeformEnabled() {
    if (DEBUG)
      Log.d("OplusFreeformManager", "isFreeformEnabled"); 
    OplusActivityManager oplusActivityManager = this.mOAms;
    if (oplusActivityManager != null)
      try {
        return oplusActivityManager.isFreeformEnabled();
      } catch (RemoteException remoteException) {
        remoteException.printStackTrace();
      }  
    return false;
  }
  
  public boolean addOnConfigChangedListener(OnConfigChangedListener paramOnConfigChangedListener) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addOnConfigChangedListener listener = ");
      stringBuilder.append(paramOnConfigChangedListener);
      Log.d("OplusFreeformManager", stringBuilder.toString());
    } 
    synchronized (this.mConfigListeners) {
      if (this.mConfigListeners.get(paramOnConfigChangedListener) != null) {
        Log.i("OplusFreeformManager", "addOnConfigChangedListener already added before");
        return false;
      } 
      OnConfigChangeListenerDelegate onConfigChangeListenerDelegate = new OnConfigChangeListenerDelegate();
      this(this, paramOnConfigChangedListener, Looper.getMainLooper());
      try {
        if (this.mOAms != null) {
          this.mConfigListeners.put(paramOnConfigChangedListener, onConfigChangeListenerDelegate);
          return this.mOAms.addFreeformConfigChangedListener((IOplusFreeformConfigChangedListener)onConfigChangeListenerDelegate);
        } 
      } catch (RemoteException remoteException) {
        Log.e("OplusFreeformManager", "addOnConfigChangedListener remoteException ");
        remoteException.printStackTrace();
      } 
      return false;
    } 
  }
  
  public boolean removeOnConfigChangedListener(OnConfigChangedListener paramOnConfigChangedListener) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("removeOnConfigChangedListener listener = ");
      stringBuilder.append(paramOnConfigChangedListener);
      Log.d("OplusFreeformManager", stringBuilder.toString());
    } 
    synchronized (this.mConfigListeners) {
      IOplusFreeformConfigChangedListener iOplusFreeformConfigChangedListener = this.mConfigListeners.get(paramOnConfigChangedListener);
      if (iOplusFreeformConfigChangedListener != null)
        try {
          if (this.mOAms != null) {
            this.mConfigListeners.remove(paramOnConfigChangedListener);
            return this.mOAms.removeFreeformConfigChangedListener(iOplusFreeformConfigChangedListener);
          } 
        } catch (RemoteException remoteException) {
          Log.e("OplusFreeformManager", "removeOnConfigChangedListener remoteException ");
          remoteException.printStackTrace();
        }  
      return false;
    } 
  }
  
  class OnConfigChangeListenerDelegate extends IOplusFreeformConfigChangedListener.Stub implements Handler.Callback {
    private static final int MSG_CONFIG_SWITCH_CHANGED = 2;
    
    private static final int MSG_CONFIG_TYPE_CHANGED = 1;
    
    private final Handler mHandler;
    
    private final OplusFreeformManager.OnConfigChangedListener mListener;
    
    final OplusFreeformManager this$0;
    
    public OnConfigChangeListenerDelegate(OplusFreeformManager.OnConfigChangedListener param1OnConfigChangedListener, Looper param1Looper) {
      this.mListener = param1OnConfigChangedListener;
      this.mHandler = new Handler(param1Looper, this);
    }
    
    public void onConfigTypeChanged(int param1Int) {
      this.mHandler.obtainMessage(1, param1Int, 0).sendToTarget();
    }
    
    public void onConfigSwitchChanged(boolean param1Boolean) {
      this.mHandler.obtainMessage(2, param1Boolean, 0).sendToTarget();
    }
    
    public boolean handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        boolean bool = false;
        if (i != 2)
          return false; 
        if (param1Message.arg1 != 0)
          bool = true; 
        this.mListener.onConfigSwitchChanged(bool);
        return true;
      } 
      i = param1Message.arg1;
      this.mListener.onConfigTypeChanged(i);
      return true;
    }
  }
  
  public static interface OnConfigChangedListener {
    void onConfigSwitchChanged(boolean param1Boolean);
    
    void onConfigTypeChanged(int param1Int);
  }
}
