package android.app.trust;

import android.hardware.biometrics.BiometricSourceType;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.util.ArrayMap;

public class TrustManager {
  private static final String DATA_FLAGS = "initiatedByUser";
  
  private static final String DATA_MESSAGE = "message";
  
  private static final int MSG_TRUST_CHANGED = 1;
  
  private static final int MSG_TRUST_ERROR = 3;
  
  private static final int MSG_TRUST_MANAGED_CHANGED = 2;
  
  private static final String TAG = "TrustManager";
  
  private final Handler mHandler;
  
  private final ITrustManager mService;
  
  private final ArrayMap<TrustListener, ITrustListener> mTrustListeners;
  
  public TrustManager(IBinder paramIBinder) {
    this.mHandler = (Handler)new Object(this, Looper.getMainLooper());
    this.mService = ITrustManager.Stub.asInterface(paramIBinder);
    this.mTrustListeners = new ArrayMap();
  }
  
  public void setDeviceLockedForUser(int paramInt, boolean paramBoolean) {
    try {
      this.mService.setDeviceLockedForUser(paramInt, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportUnlockAttempt(boolean paramBoolean, int paramInt) {
    try {
      this.mService.reportUnlockAttempt(paramBoolean, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportUnlockLockout(int paramInt1, int paramInt2) {
    try {
      this.mService.reportUnlockLockout(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportEnabledTrustAgentsChanged(int paramInt) {
    try {
      this.mService.reportEnabledTrustAgentsChanged(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportKeyguardShowingChanged() {
    try {
      this.mService.reportKeyguardShowingChanged();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerTrustListener(TrustListener paramTrustListener) {
    try {
      Object object = new Object();
      super(this, paramTrustListener);
      this.mService.registerTrustListener((ITrustListener)object);
      this.mTrustListeners.put(paramTrustListener, object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterTrustListener(TrustListener paramTrustListener) {
    ITrustListener iTrustListener = (ITrustListener)this.mTrustListeners.remove(paramTrustListener);
    if (iTrustListener != null)
      try {
        this.mService.unregisterTrustListener(iTrustListener);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public boolean isTrustUsuallyManaged(int paramInt) {
    try {
      return this.mService.isTrustUsuallyManaged(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unlockedByBiometricForUser(int paramInt, BiometricSourceType paramBiometricSourceType) {
    try {
      this.mService.unlockedByBiometricForUser(paramInt, paramBiometricSourceType);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearAllBiometricRecognized(BiometricSourceType paramBiometricSourceType) {
    try {
      this.mService.clearAllBiometricRecognized(paramBiometricSourceType);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static interface TrustListener {
    void onTrustChanged(boolean param1Boolean, int param1Int1, int param1Int2);
    
    void onTrustError(CharSequence param1CharSequence);
    
    void onTrustManagedChanged(boolean param1Boolean, int param1Int);
  }
}
