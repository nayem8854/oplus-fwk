package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOplusStatusBar extends IInterface {
  void notifyInputMethodKeyboardPosition(boolean paramBoolean) throws RemoteException;
  
  void notifyMultiWindowFocusChanged(int paramInt) throws RemoteException;
  
  void setStatusBarFunction(int paramInt, String paramString) throws RemoteException;
  
  void toggleSplitScreen(int paramInt) throws RemoteException;
  
  void topIsFullscreen(boolean paramBoolean) throws RemoteException;
  
  void updateNavBarVisibility(int paramInt) throws RemoteException;
  
  void updateNavBarVisibilityWithPkg(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IOplusStatusBar {
    public void topIsFullscreen(boolean param1Boolean) throws RemoteException {}
    
    public void notifyMultiWindowFocusChanged(int param1Int) throws RemoteException {}
    
    public void toggleSplitScreen(int param1Int) throws RemoteException {}
    
    public void setStatusBarFunction(int param1Int, String param1String) throws RemoteException {}
    
    public void updateNavBarVisibility(int param1Int) throws RemoteException {}
    
    public void updateNavBarVisibilityWithPkg(int param1Int, String param1String) throws RemoteException {}
    
    public void notifyInputMethodKeyboardPosition(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOplusStatusBar {
    private static final String DESCRIPTOR = "android.app.IOplusStatusBar";
    
    static final int TRANSACTION_notifyInputMethodKeyboardPosition = 7;
    
    static final int TRANSACTION_notifyMultiWindowFocusChanged = 2;
    
    static final int TRANSACTION_setStatusBarFunction = 4;
    
    static final int TRANSACTION_toggleSplitScreen = 3;
    
    static final int TRANSACTION_topIsFullscreen = 1;
    
    static final int TRANSACTION_updateNavBarVisibility = 5;
    
    static final int TRANSACTION_updateNavBarVisibilityWithPkg = 6;
    
    public Stub() {
      attachInterface(this, "android.app.IOplusStatusBar");
    }
    
    public static IOplusStatusBar asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IOplusStatusBar");
      if (iInterface != null && iInterface instanceof IOplusStatusBar)
        return (IOplusStatusBar)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "notifyInputMethodKeyboardPosition";
        case 6:
          return "updateNavBarVisibilityWithPkg";
        case 5:
          return "updateNavBarVisibility";
        case 4:
          return "setStatusBarFunction";
        case 3:
          return "toggleSplitScreen";
        case 2:
          return "notifyMultiWindowFocusChanged";
        case 1:
          break;
      } 
      return "topIsFullscreen";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.app.IOplusStatusBar");
            bool1 = bool2;
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            notifyInputMethodKeyboardPosition(bool1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.app.IOplusStatusBar");
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            updateNavBarVisibilityWithPkg(param1Int1, str);
            return true;
          case 5:
            str.enforceInterface("android.app.IOplusStatusBar");
            param1Int1 = str.readInt();
            updateNavBarVisibility(param1Int1);
            return true;
          case 4:
            str.enforceInterface("android.app.IOplusStatusBar");
            param1Int1 = str.readInt();
            str = str.readString();
            setStatusBarFunction(param1Int1, str);
            return true;
          case 3:
            str.enforceInterface("android.app.IOplusStatusBar");
            param1Int1 = str.readInt();
            toggleSplitScreen(param1Int1);
            return true;
          case 2:
            str.enforceInterface("android.app.IOplusStatusBar");
            param1Int1 = str.readInt();
            notifyMultiWindowFocusChanged(param1Int1);
            return true;
          case 1:
            break;
        } 
        str.enforceInterface("android.app.IOplusStatusBar");
        if (str.readInt() != 0)
          bool1 = true; 
        topIsFullscreen(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.app.IOplusStatusBar");
      return true;
    }
    
    private static class Proxy implements IOplusStatusBar {
      public static IOplusStatusBar sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IOplusStatusBar";
      }
      
      public void topIsFullscreen(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().topIsFullscreen(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyMultiWindowFocusChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().notifyMultiWindowFocusChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void toggleSplitScreen(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().toggleSplitScreen(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setStatusBarFunction(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().setStatusBarFunction(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateNavBarVisibility(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().updateNavBarVisibility(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateNavBarVisibilityWithPkg(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().updateNavBarVisibilityWithPkg(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyInputMethodKeyboardPosition(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IOplusStatusBar");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, null, 1);
          if (!bool1 && IOplusStatusBar.Stub.getDefaultImpl() != null) {
            IOplusStatusBar.Stub.getDefaultImpl().notifyInputMethodKeyboardPosition(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOplusStatusBar param1IOplusStatusBar) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOplusStatusBar != null) {
          Proxy.sDefaultImpl = param1IOplusStatusBar;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOplusStatusBar getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
