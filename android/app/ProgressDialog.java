package android.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.android.internal.R;
import java.text.NumberFormat;

@Deprecated
public class ProgressDialog extends AlertDialog {
  public static final int STYLE_HORIZONTAL = 1;
  
  public static final int STYLE_SPINNER = 0;
  
  private boolean mHasStarted;
  
  private int mIncrementBy;
  
  private int mIncrementSecondaryBy;
  
  private boolean mIndeterminate;
  
  private Drawable mIndeterminateDrawable;
  
  private int mMax;
  
  private CharSequence mMessage;
  
  private TextView mMessageView;
  
  private ProgressBar mProgress;
  
  private Drawable mProgressDrawable;
  
  private TextView mProgressNumber;
  
  private String mProgressNumberFormat;
  
  private TextView mProgressPercent;
  
  private NumberFormat mProgressPercentFormat;
  
  private int mProgressStyle = 0;
  
  private int mProgressVal;
  
  private int mSecondaryProgressVal;
  
  private Handler mViewUpdateHandler;
  
  public ProgressDialog(Context paramContext) {
    super(paramContext);
    initFormats();
  }
  
  public ProgressDialog(Context paramContext, int paramInt) {
    super(paramContext, paramInt);
    initFormats();
  }
  
  private void initFormats() {
    this.mProgressNumberFormat = "%1d/%2d";
    NumberFormat numberFormat = NumberFormat.getPercentInstance();
    numberFormat.setMaximumFractionDigits(0);
  }
  
  public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    return show(paramContext, paramCharSequence1, paramCharSequence2, false);
  }
  
  public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean) {
    return show(paramContext, paramCharSequence1, paramCharSequence2, paramBoolean, false, (DialogInterface.OnCancelListener)null);
  }
  
  public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean1, boolean paramBoolean2) {
    return show(paramContext, paramCharSequence1, paramCharSequence2, paramBoolean1, paramBoolean2, (DialogInterface.OnCancelListener)null);
  }
  
  public static ProgressDialog show(Context paramContext, CharSequence paramCharSequence1, CharSequence paramCharSequence2, boolean paramBoolean1, boolean paramBoolean2, DialogInterface.OnCancelListener paramOnCancelListener) {
    ProgressDialog progressDialog = new ProgressDialog(paramContext);
    progressDialog.setTitle(paramCharSequence1);
    progressDialog.setMessage(paramCharSequence2);
    progressDialog.setIndeterminate(paramBoolean1);
    progressDialog.setCancelable(paramBoolean2);
    progressDialog.setOnCancelListener(paramOnCancelListener);
    progressDialog.show();
    return progressDialog;
  }
  
  protected void onCreate(Bundle paramBundle) {
    View view;
    LayoutInflater layoutInflater = LayoutInflater.from(this.mContext);
    TypedArray typedArray = this.mContext.obtainStyledAttributes(null, R.styleable.AlertDialog, 16842845, 0);
    if (this.mProgressStyle == 1) {
      this.mViewUpdateHandler = (Handler)new Object(this);
      view = layoutInflater.inflate(typedArray.getResourceId(13, 17367088), null);
      this.mProgress = (ProgressBar)view.findViewById(16908301);
      this.mProgressNumber = (TextView)view.findViewById(16909327);
      this.mProgressPercent = (TextView)view.findViewById(16909328);
      setView(view);
    } else {
      view = view.inflate(typedArray.getResourceId(18, 17367264), null);
      this.mProgress = (ProgressBar)view.findViewById(16908301);
      this.mMessageView = (TextView)view.findViewById(16908299);
      setView(view);
    } 
    typedArray.recycle();
    int i = this.mMax;
    if (i > 0)
      setMax(i); 
    i = this.mProgressVal;
    if (i > 0)
      setProgress(i); 
    i = this.mSecondaryProgressVal;
    if (i > 0)
      setSecondaryProgress(i); 
    i = this.mIncrementBy;
    if (i > 0)
      incrementProgressBy(i); 
    i = this.mIncrementSecondaryBy;
    if (i > 0)
      incrementSecondaryProgressBy(i); 
    Drawable drawable = this.mProgressDrawable;
    if (drawable != null)
      setProgressDrawable(drawable); 
    drawable = this.mIndeterminateDrawable;
    if (drawable != null)
      setIndeterminateDrawable(drawable); 
    CharSequence charSequence = this.mMessage;
    if (charSequence != null)
      setMessage(charSequence); 
    setIndeterminate(this.mIndeterminate);
    onProgressChanged();
    super.onCreate(paramBundle);
  }
  
  public void onStart() {
    super.onStart();
    this.mHasStarted = true;
  }
  
  protected void onStop() {
    super.onStop();
    this.mHasStarted = false;
  }
  
  public void setProgress(int paramInt) {
    if (this.mHasStarted) {
      this.mProgress.setProgress(paramInt);
      onProgressChanged();
    } else {
      this.mProgressVal = paramInt;
    } 
  }
  
  public void setSecondaryProgress(int paramInt) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.setSecondaryProgress(paramInt);
      onProgressChanged();
    } else {
      this.mSecondaryProgressVal = paramInt;
    } 
  }
  
  public int getProgress() {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null)
      return progressBar.getProgress(); 
    return this.mProgressVal;
  }
  
  public int getSecondaryProgress() {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null)
      return progressBar.getSecondaryProgress(); 
    return this.mSecondaryProgressVal;
  }
  
  public int getMax() {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null)
      return progressBar.getMax(); 
    return this.mMax;
  }
  
  public void setMax(int paramInt) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.setMax(paramInt);
      onProgressChanged();
    } else {
      this.mMax = paramInt;
    } 
  }
  
  public void incrementProgressBy(int paramInt) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.incrementProgressBy(paramInt);
      onProgressChanged();
    } else {
      this.mIncrementBy += paramInt;
    } 
  }
  
  public void incrementSecondaryProgressBy(int paramInt) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.incrementSecondaryProgressBy(paramInt);
      onProgressChanged();
    } else {
      this.mIncrementSecondaryBy += paramInt;
    } 
  }
  
  public void setProgressDrawable(Drawable paramDrawable) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.setProgressDrawable(paramDrawable);
    } else {
      this.mProgressDrawable = paramDrawable;
    } 
  }
  
  public void setIndeterminateDrawable(Drawable paramDrawable) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.setIndeterminateDrawable(paramDrawable);
    } else {
      this.mIndeterminateDrawable = paramDrawable;
    } 
  }
  
  public void setIndeterminate(boolean paramBoolean) {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null) {
      progressBar.setIndeterminate(paramBoolean);
    } else {
      this.mIndeterminate = paramBoolean;
    } 
  }
  
  public boolean isIndeterminate() {
    ProgressBar progressBar = this.mProgress;
    if (progressBar != null)
      return progressBar.isIndeterminate(); 
    return this.mIndeterminate;
  }
  
  public void setMessage(CharSequence paramCharSequence) {
    if (this.mProgress != null) {
      if (this.mProgressStyle == 1) {
        super.setMessage(paramCharSequence);
      } else {
        this.mMessageView.setText(paramCharSequence);
      } 
    } else {
      this.mMessage = paramCharSequence;
    } 
  }
  
  public void setProgressStyle(int paramInt) {
    this.mProgressStyle = paramInt;
  }
  
  public void setProgressNumberFormat(String paramString) {
    this.mProgressNumberFormat = paramString;
    onProgressChanged();
  }
  
  public void setProgressPercentFormat(NumberFormat paramNumberFormat) {
    this.mProgressPercentFormat = paramNumberFormat;
    onProgressChanged();
  }
  
  private void onProgressChanged() {
    if (this.mProgressStyle == 1) {
      Handler handler = this.mViewUpdateHandler;
      if (handler != null && !handler.hasMessages(0))
        this.mViewUpdateHandler.sendEmptyMessage(0); 
    } 
  }
}
