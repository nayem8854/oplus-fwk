package android.app;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

@Deprecated
public class ListActivity extends Activity {
  private Handler mHandler = new Handler();
  
  private boolean mFinishedStart = false;
  
  private Runnable mRequestFocus = (Runnable)new Object(this);
  
  protected void onListItemClick(ListView paramListView, View paramView, int paramInt, long paramLong) {}
  
  protected void onRestoreInstanceState(Bundle paramBundle) {
    ensureList();
    super.onRestoreInstanceState(paramBundle);
  }
  
  protected void onDestroy() {
    this.mHandler.removeCallbacks(this.mRequestFocus);
    super.onDestroy();
  }
  
  public void onContentChanged() {
    super.onContentChanged();
    View view = (View)findViewById(16908292);
    ListView listView = findViewById(16908298);
    if (listView != null) {
      if (view != null)
        listView.setEmptyView(view); 
      this.mList.setOnItemClickListener(this.mOnClickListener);
      if (this.mFinishedStart)
        setListAdapter(this.mAdapter); 
      this.mHandler.post(this.mRequestFocus);
      this.mFinishedStart = true;
      return;
    } 
    throw new RuntimeException("Your content must have a ListView whose id attribute is 'android.R.id.list'");
  }
  
  public void setListAdapter(ListAdapter paramListAdapter) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial ensureList : ()V
    //   6: aload_0
    //   7: aload_1
    //   8: putfield mAdapter : Landroid/widget/ListAdapter;
    //   11: aload_0
    //   12: getfield mList : Landroid/widget/ListView;
    //   15: aload_1
    //   16: invokevirtual setAdapter : (Landroid/widget/ListAdapter;)V
    //   19: aload_0
    //   20: monitorexit
    //   21: return
    //   22: astore_1
    //   23: aload_0
    //   24: monitorexit
    //   25: aload_1
    //   26: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #266	-> 0
    //   #267	-> 2
    //   #268	-> 6
    //   #269	-> 11
    //   #270	-> 19
    //   #271	-> 21
    //   #270	-> 22
    // Exception table:
    //   from	to	target	type
    //   2	6	22	finally
    //   6	11	22	finally
    //   11	19	22	finally
    //   19	21	22	finally
    //   23	25	22	finally
  }
  
  public void setSelection(int paramInt) {
    this.mList.setSelection(paramInt);
  }
  
  public int getSelectedItemPosition() {
    return this.mList.getSelectedItemPosition();
  }
  
  public long getSelectedItemId() {
    return this.mList.getSelectedItemId();
  }
  
  public ListView getListView() {
    ensureList();
    return this.mList;
  }
  
  public ListAdapter getListAdapter() {
    return this.mAdapter;
  }
  
  private void ensureList() {
    if (this.mList != null)
      return; 
    setContentView(17367186);
  }
  
  private AdapterView.OnItemClickListener mOnClickListener = (AdapterView.OnItemClickListener)new Object(this);
  
  protected ListAdapter mAdapter;
  
  protected ListView mList;
}
