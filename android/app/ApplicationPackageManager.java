package android.app;

import android.common.OplusFeatureCache;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.ChangedPackages;
import android.content.pm.ComponentInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.IDexModuleRegisterCallback;
import android.content.pm.IPackageDataObserver;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageInstaller;
import android.content.pm.IPackageManager;
import android.content.pm.IPackageMoveObserver;
import android.content.pm.IPackageStatsObserver;
import android.content.pm.InstallSourceInfo;
import android.content.pm.InstantAppInfo;
import android.content.pm.InstrumentationInfo;
import android.content.pm.IntentFilterVerificationInfo;
import android.content.pm.KeySet;
import android.content.pm.ModuleInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageInstaller;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ParceledListSlice;
import android.content.pm.PermissionGroupInfo;
import android.content.pm.PermissionInfo;
import android.content.pm.ProviderInfo;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.pm.SharedLibraryInfo;
import android.content.pm.SuspendDialogInfo;
import android.content.pm.VerifierDeviceIdentity;
import android.content.pm.VersionedPackage;
import android.content.pm.dex.ArtManager;
import android.content.res.IOplusThemeManager;
import android.content.res.OplusBaseResources;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemProperties;
import android.os.UserHandle;
import android.os.UserManager;
import android.os.storage.StorageManager;
import android.os.storage.VolumeInfo;
import android.permission.IOnPermissionsChangeListener;
import android.permission.IPermissionManager;
import android.permission.PermissionManager;
import android.provider.Settings;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructStat;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.LauncherIcons;
import android.util.Log;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.UserIcons;
import com.oplus.content.OplusFeatureConfigManager;
import com.oplus.multiapp.OplusMultiAppManager;
import dalvik.system.VMRuntime;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import libcore.util.EmptyArray;

public class ApplicationPackageManager extends OplusBaseApplicationPackageManager {
  private final Object mLock = new Object();
  
  private final ArrayList<MoveCallbackDelegate> mDelegates = new ArrayList<>();
  
  UserManager getUserManager() {
    synchronized (this.mLock) {
      if (this.mUserManager == null)
        this.mUserManager = UserManager.get(this.mContext); 
      return this.mUserManager;
    } 
  }
  
  public int getUserId() {
    return this.mContext.getUserId();
  }
  
  public PackageInfo getPackageInfo(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    boolean bool;
    PackageInfo packageInfo = getPackageInfoAsUser(paramString, paramInt, getUserId());
    if (packageInfo == null) {
      bool = true;
    } else {
      bool = false;
    } 
    checkAndLogMultiApp(bool, this.mContext, paramString, "getPackageInfo");
    return packageInfo;
  }
  
  public PackageInfo getPackageInfo(VersionedPackage paramVersionedPackage, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForPackage(paramInt, i);
      PackageInfo packageInfo = iPackageManager.getPackageInfoVersioned(paramVersionedPackage, paramInt, i);
      if (packageInfo != null)
        return packageInfo; 
      throw new PackageManager.NameNotFoundException(paramVersionedPackage.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PackageInfo getPackageInfoAsUser(String paramString, int paramInt1, int paramInt2) throws PackageManager.NameNotFoundException {
    paramInt1 = updateFlagsForPackage(paramInt1, paramInt2);
    PackageInfo packageInfo = getPackageInfoAsUserCached(paramString, paramInt1, paramInt2);
    if (packageInfo != null)
      return packageInfo; 
    throw new PackageManager.NameNotFoundException(paramString);
  }
  
  public String[] currentToCanonicalPackageNames(String[] paramArrayOfString) {
    try {
      return this.mPM.currentToCanonicalPackageNames(paramArrayOfString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] canonicalToCurrentPackageNames(String[] paramArrayOfString) {
    try {
      return this.mPM.canonicalToCurrentPackageNames(paramArrayOfString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Intent getLaunchIntentForPackage(String paramString) {
    // Byte code:
    //   0: new android/content/Intent
    //   3: dup
    //   4: ldc_w 'android.intent.action.MAIN'
    //   7: invokespecial <init> : (Ljava/lang/String;)V
    //   10: astore_2
    //   11: aload_2
    //   12: ldc_w 'android.intent.category.INFO'
    //   15: invokevirtual addCategory : (Ljava/lang/String;)Landroid/content/Intent;
    //   18: pop
    //   19: aload_2
    //   20: aload_1
    //   21: invokevirtual setPackage : (Ljava/lang/String;)Landroid/content/Intent;
    //   24: pop
    //   25: aload_0
    //   26: aload_2
    //   27: iconst_0
    //   28: invokevirtual queryIntentActivities : (Landroid/content/Intent;I)Ljava/util/List;
    //   31: astore_3
    //   32: aload_3
    //   33: ifnull -> 48
    //   36: aload_3
    //   37: astore #4
    //   39: aload_3
    //   40: invokeinterface size : ()I
    //   45: ifgt -> 77
    //   48: aload_2
    //   49: ldc_w 'android.intent.category.INFO'
    //   52: invokevirtual removeCategory : (Ljava/lang/String;)V
    //   55: aload_2
    //   56: ldc_w 'android.intent.category.LAUNCHER'
    //   59: invokevirtual addCategory : (Ljava/lang/String;)Landroid/content/Intent;
    //   62: pop
    //   63: aload_2
    //   64: aload_1
    //   65: invokevirtual setPackage : (Ljava/lang/String;)Landroid/content/Intent;
    //   68: pop
    //   69: aload_0
    //   70: aload_2
    //   71: iconst_0
    //   72: invokevirtual queryIntentActivities : (Landroid/content/Intent;I)Ljava/util/List;
    //   75: astore #4
    //   77: aload #4
    //   79: ifnull -> 159
    //   82: aload #4
    //   84: invokeinterface size : ()I
    //   89: ifgt -> 95
    //   92: goto -> 159
    //   95: new android/content/Intent
    //   98: dup
    //   99: aload_2
    //   100: invokespecial <init> : (Landroid/content/Intent;)V
    //   103: astore_1
    //   104: aload_1
    //   105: ldc_w 268435456
    //   108: invokevirtual setFlags : (I)Landroid/content/Intent;
    //   111: pop
    //   112: aload #4
    //   114: iconst_0
    //   115: invokeinterface get : (I)Ljava/lang/Object;
    //   120: checkcast android/content/pm/ResolveInfo
    //   123: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   126: getfield packageName : Ljava/lang/String;
    //   129: astore_3
    //   130: aload #4
    //   132: iconst_0
    //   133: invokeinterface get : (I)Ljava/lang/Object;
    //   138: checkcast android/content/pm/ResolveInfo
    //   141: getfield activityInfo : Landroid/content/pm/ActivityInfo;
    //   144: getfield name : Ljava/lang/String;
    //   147: astore #4
    //   149: aload_1
    //   150: aload_3
    //   151: aload #4
    //   153: invokevirtual setClassName : (Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    //   156: pop
    //   157: aload_1
    //   158: areturn
    //   159: aconst_null
    //   160: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #265	-> 0
    //   #266	-> 11
    //   #267	-> 19
    //   #268	-> 25
    //   #271	-> 32
    //   #273	-> 48
    //   #274	-> 55
    //   #275	-> 63
    //   #276	-> 69
    //   #278	-> 77
    //   #281	-> 95
    //   #282	-> 104
    //   #283	-> 112
    //   #284	-> 130
    //   #283	-> 149
    //   #285	-> 157
    //   #279	-> 159
  }
  
  public Intent getLeanbackLaunchIntentForPackage(String paramString) {
    return getLaunchIntentForPackageAndCategory(paramString, "android.intent.category.LEANBACK_LAUNCHER");
  }
  
  public Intent getCarLaunchIntentForPackage(String paramString) {
    return getLaunchIntentForPackageAndCategory(paramString, "android.intent.category.CAR_LAUNCHER");
  }
  
  private Intent getLaunchIntentForPackageAndCategory(String paramString1, String paramString2) {
    Intent intent2 = new Intent("android.intent.action.MAIN");
    intent2.addCategory(paramString2);
    intent2.setPackage(paramString1);
    List<ResolveInfo> list = queryIntentActivities(intent2, 0);
    if (list == null || list.size() <= 0)
      return null; 
    Intent intent1 = new Intent(intent2);
    intent1.setFlags(268435456);
    paramString1 = ((ResolveInfo)list.get(0)).activityInfo.packageName;
    String str = ((ResolveInfo)list.get(0)).activityInfo.name;
    intent1.setClassName(paramString1, str);
    return intent1;
  }
  
  public int[] getPackageGids(String paramString) throws PackageManager.NameNotFoundException {
    return getPackageGids(paramString, 0);
  }
  
  public int[] getPackageGids(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForPackage(paramInt, i);
      int[] arrayOfInt = iPackageManager.getPackageGids(paramString, paramInt, i);
      if (arrayOfInt != null)
        return arrayOfInt; 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPackageUid(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return getPackageUidAsUser(paramString, paramInt, getUserId());
  }
  
  public int getPackageUidAsUser(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return getPackageUidAsUser(paramString, 0, paramInt);
  }
  
  public int getPackageUidAsUser(String paramString, int paramInt1, int paramInt2) throws PackageManager.NameNotFoundException {
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt1 = updateFlagsForPackage(paramInt1, paramInt2);
      paramInt1 = iPackageManager.getPackageUid(paramString, paramInt1, paramInt2);
      if (paramInt1 >= 0)
        return paramInt1; 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<PermissionGroupInfo> getAllPermissionGroups(int paramInt) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      ParceledListSlice parceledListSlice = iPermissionManager.getAllPermissionGroups(paramInt);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      PermissionGroupInfo permissionGroupInfo = iPermissionManager.getPermissionGroupInfo(paramString, paramInt);
      if (permissionGroupInfo != null)
        return permissionGroupInfo; 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PermissionInfo getPermissionInfo(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      String str = this.mContext.getOpPackageName();
      IPermissionManager iPermissionManager = this.mPermissionManager;
      PermissionInfo permissionInfo = iPermissionManager.getPermissionInfo(paramString, str, paramInt);
      if (permissionInfo != null)
        return permissionInfo; 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<PermissionInfo> queryPermissionsByGroup(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      ParceledListSlice parceledListSlice = iPermissionManager.queryPermissionsByGroup(paramString, paramInt);
      if (parceledListSlice != null) {
        List<PermissionInfo> list = parceledListSlice.getList();
        if (list != null)
          return list; 
      } 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean arePermissionsIndividuallyControlled() {
    if (OplusFeatureConfigManager.getInstacne().hasFeature("oplus.software.permission_intercept_enabled"))
      return true; 
    return this.mContext.getResources().getBoolean(17891500);
  }
  
  public boolean isWirelessConsentModeEnabled() {
    return this.mContext.getResources().getBoolean(17891601);
  }
  
  public ApplicationInfo getApplicationInfo(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return getApplicationInfoAsUser(paramString, paramInt, getUserId());
  }
  
  public ApplicationInfo getApplicationInfoAsUser(String paramString, int paramInt1, int paramInt2) throws PackageManager.NameNotFoundException {
    paramInt1 = updateFlagsForApplication(paramInt1, paramInt2);
    ApplicationInfo applicationInfo = getApplicationInfoAsUserCached(paramString, paramInt1, paramInt2);
    if (applicationInfo != null)
      return maybeAdjustApplicationInfo(applicationInfo); 
    throw new PackageManager.NameNotFoundException(paramString);
  }
  
  private static ApplicationInfo maybeAdjustApplicationInfo(ApplicationInfo paramApplicationInfo) {
    if (paramApplicationInfo.primaryCpuAbi != null && paramApplicationInfo.secondaryCpuAbi != null) {
      String str1 = VMRuntime.getRuntime().vmInstructionSet();
      String str2 = VMRuntime.getInstructionSet(paramApplicationInfo.secondaryCpuAbi);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ro.dalvik.vm.isa.");
      stringBuilder.append(str2);
      String str3 = SystemProperties.get(stringBuilder.toString());
      if (!str3.isEmpty())
        str2 = str3; 
      if (str1.equals(str2)) {
        ApplicationInfo applicationInfo = new ApplicationInfo(paramApplicationInfo);
        applicationInfo.nativeLibraryDir = paramApplicationInfo.secondaryNativeLibraryDir;
        return applicationInfo;
      } 
    } 
    return paramApplicationInfo;
  }
  
  public ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForComponent(paramInt, i, (Intent)null);
      ActivityInfo activityInfo = iPackageManager.getActivityInfo(paramComponentName, paramInt, i);
      if (activityInfo != null)
        return activityInfo; 
      checkAndLogMultiApp(true, this.mContext, paramComponentName, "getActivityInfo");
      throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForComponent(paramInt, i, (Intent)null);
      ActivityInfo activityInfo = iPackageManager.getReceiverInfo(paramComponentName, paramInt, i);
      if (activityInfo != null)
        return activityInfo; 
      checkAndLogMultiApp(true, this.mContext, paramComponentName, "getReceiverInfo");
      throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForComponent(paramInt, i, (Intent)null);
      ServiceInfo serviceInfo = iPackageManager.getServiceInfo(paramComponentName, paramInt, i);
      if (serviceInfo != null)
        return serviceInfo; 
      checkAndLogMultiApp(true, this.mContext, paramComponentName, "getServiceInfo");
      throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForComponent(paramInt, i, (Intent)null);
      ProviderInfo providerInfo = iPackageManager.getProviderInfo(paramComponentName, paramInt, i);
      if (providerInfo != null)
        return providerInfo; 
      checkAndLogMultiApp(true, this.mContext, paramComponentName, "getProviderInfo");
      throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getSystemSharedLibraryNames() {
    try {
      return this.mPM.getSystemSharedLibraryNames();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<SharedLibraryInfo> getSharedLibraries(int paramInt) {
    return getSharedLibrariesAsUser(paramInt, getUserId());
  }
  
  public List<SharedLibraryInfo> getSharedLibrariesAsUser(int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = contextImpl.getOpPackageName();
      ParceledListSlice parceledListSlice = iPackageManager.getSharedLibraries(str, paramInt1, paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<SharedLibraryInfo> getDeclaredSharedLibraries(String paramString, int paramInt) {
    try {
      List<?> list;
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      int i = contextImpl.getUserId();
      ParceledListSlice parceledListSlice = iPackageManager.getDeclaredSharedLibraries(paramString, paramInt, i);
      if (parceledListSlice != null) {
        list = parceledListSlice.getList();
      } else {
        list = Collections.emptyList();
      } 
      return (List)list;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getServicesSystemSharedLibraryPackageName() {
    try {
      return this.mPM.getServicesSystemSharedLibraryPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getSharedSystemSharedLibraryPackageName() {
    try {
      return this.mPM.getSharedSystemSharedLibraryPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ChangedPackages getChangedPackages(int paramInt) {
    try {
      return this.mPM.getChangedPackages(paramInt, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public FeatureInfo[] getSystemAvailableFeatures() {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.getSystemAvailableFeatures();
      if (parceledListSlice == null)
        return new FeatureInfo[0]; 
      List<FeatureInfo> list = parceledListSlice.getList();
      FeatureInfo[] arrayOfFeatureInfo = new FeatureInfo[list.size()];
      for (byte b = 0; b < arrayOfFeatureInfo.length; b++)
        arrayOfFeatureInfo[b] = list.get(b); 
      return arrayOfFeatureInfo;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasSystemFeature(String paramString) {
    return hasSystemFeature(paramString, 0);
  }
  
  class HasSystemFeatureQuery {
    public final String name;
    
    public final int version;
    
    public HasSystemFeatureQuery(ApplicationPackageManager this$0, int param1Int) {
      this.name = (String)this$0;
      this.version = param1Int;
    }
    
    public String toString() {
      String str = this.name;
      int i = this.version;
      return String.format("HasSystemFeatureQuery(name=\"%s\", version=%d)", new Object[] { str, Integer.valueOf(i) });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof HasSystemFeatureQuery;
      boolean bool1 = false;
      if (bool) {
        param1Object = param1Object;
        bool = bool1;
        if (Objects.equals(this.name, ((HasSystemFeatureQuery)param1Object).name)) {
          bool = bool1;
          if (this.version == ((HasSystemFeatureQuery)param1Object).version)
            bool = true; 
        } 
        return bool;
      } 
      return false;
    }
    
    public int hashCode() {
      return Objects.hashCode(this.name) * 13 + this.version;
    }
  }
  
  private static final PropertyInvalidatedCache<HasSystemFeatureQuery, Boolean> mHasSystemFeatureCache = (PropertyInvalidatedCache<HasSystemFeatureQuery, Boolean>)new Object(256, "cache_key.has_system_feature");
  
  public boolean hasSystemFeature(String paramString, int paramInt) {
    return ((Boolean)mHasSystemFeatureCache.query(new HasSystemFeatureQuery(paramString, paramInt))).booleanValue();
  }
  
  public void disableHasSystemFeatureCache() {
    mHasSystemFeatureCache.disableLocal();
  }
  
  public static void invalidateHasSystemFeatureCache() {
    mHasSystemFeatureCache.invalidateCache();
  }
  
  public int checkPermission(String paramString1, String paramString2) {
    return 
      PermissionManager.checkPackageNamePermission(paramString1, paramString2, getUserId());
  }
  
  public boolean isPermissionRevokedByPolicy(String paramString1, String paramString2) {
    try {
      return this.mPermissionManager.isPermissionRevokedByPolicy(paramString1, paramString2, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getPermissionControllerPackageName() {
    synchronized (this.mLock) {
      String str = this.mPermissionsControllerPackageName;
      if (str == null)
        try {
          this.mPermissionsControllerPackageName = this.mPM.getPermissionControllerPackageName();
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      str = this.mPermissionsControllerPackageName;
      return str;
    } 
  }
  
  public boolean addPermission(PermissionInfo paramPermissionInfo) {
    try {
      return this.mPermissionManager.addPermission(paramPermissionInfo, false);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean addPermissionAsync(PermissionInfo paramPermissionInfo) {
    try {
      return this.mPermissionManager.addPermission(paramPermissionInfo, true);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removePermission(String paramString) {
    try {
      this.mPermissionManager.removePermission(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void grantRuntimePermission(String paramString1, String paramString2, UserHandle paramUserHandle) {
    try {
      this.mPM.grantRuntimePermission(paramString1, paramString2, paramUserHandle.getIdentifier());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean shouldTraceGrant(String paramString1, String paramString2, int paramInt) {
    return false;
  }
  
  public void revokeRuntimePermission(String paramString1, String paramString2, UserHandle paramUserHandle) {
    revokeRuntimePermission(paramString1, paramString2, paramUserHandle, (String)null);
  }
  
  public void revokeRuntimePermission(String paramString1, String paramString2, UserHandle paramUserHandle, String paramString3) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      iPermissionManager.revokeRuntimePermission(paramString1, paramString2, paramUserHandle.getIdentifier(), paramString3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPermissionFlags(String paramString1, String paramString2, UserHandle paramUserHandle) {
    try {
      IPermissionManager iPermissionManager = this.mPermissionManager;
      return 
        iPermissionManager.getPermissionFlags(paramString1, paramString2, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updatePermissionFlags(String paramString1, String paramString2, int paramInt1, int paramInt2, UserHandle paramUserHandle) {
    try {
      boolean bool;
      ContextImpl contextImpl = this.mContext;
      if ((contextImpl.getApplicationInfo()).targetSdkVersion >= 29) {
        bool = true;
      } else {
        bool = false;
      } 
      IPermissionManager iPermissionManager = this.mPermissionManager;
      int i = paramUserHandle.getIdentifier();
      iPermissionManager.updatePermissionFlags(paramString1, paramString2, paramInt1, paramInt2, bool, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Set<String> getWhitelistedRestrictedPermissions(String paramString, int paramInt) {
    try {
      int i = getUserId();
      IPermissionManager iPermissionManager = this.mPermissionManager;
      List list = iPermissionManager.getWhitelistedRestrictedPermissions(paramString, paramInt, i);
      if (list != null)
        return (Set<String>)new ArraySet(list); 
      return (Set)Collections.emptySet();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean addWhitelistedRestrictedPermission(String paramString1, String paramString2, int paramInt) {
    try {
      int i = getUserId();
      IPermissionManager iPermissionManager = this.mPermissionManager;
      return 
        iPermissionManager.addWhitelistedRestrictedPermission(paramString1, paramString2, paramInt, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean setAutoRevokeWhitelisted(String paramString, boolean paramBoolean) {
    try {
      int i = getUserId();
      return this.mPermissionManager.setAutoRevokeWhitelisted(paramString, paramBoolean, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isAutoRevokeWhitelisted(String paramString) {
    try {
      int i = getUserId();
      return this.mPermissionManager.isAutoRevokeWhitelisted(paramString, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean removeWhitelistedRestrictedPermission(String paramString1, String paramString2, int paramInt) {
    try {
      int i = getUserId();
      IPermissionManager iPermissionManager = this.mPermissionManager;
      return 
        iPermissionManager.removeWhitelistedRestrictedPermission(paramString1, paramString2, paramInt, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean shouldShowRequestPermissionRationale(String paramString) {
    try {
      String str = this.mContext.getPackageName();
      IPermissionManager iPermissionManager = this.mPermissionManager;
      return 
        iPermissionManager.shouldShowRequestPermissionRationale(paramString, str, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public CharSequence getBackgroundPermissionOptionLabel() {
    try {
      String str = getPermissionControllerPackageName();
      ContextImpl contextImpl = this.mContext;
      Context context = contextImpl.createPackageContext(str, 0);
      int i = context.getResources().getIdentifier("app_permission_button_allow_always", "string", "com.android.permissioncontroller");
      if (i != 0)
        return context.getText(i); 
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      Log.e("ApplicationPackageManager", "Permission controller not found.", (Throwable)nameNotFoundException);
    } 
    return "";
  }
  
  public int checkSignatures(String paramString1, String paramString2) {
    try {
      return this.mPM.checkSignatures(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int checkSignatures(int paramInt1, int paramInt2) {
    try {
      return this.mPM.checkUidSignatures(paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasSigningCertificate(String paramString, byte[] paramArrayOfbyte, int paramInt) {
    try {
      return this.mPM.hasSigningCertificate(paramString, paramArrayOfbyte, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasSigningCertificate(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) {
    try {
      return this.mPM.hasUidSigningCertificate(paramInt1, paramArrayOfbyte, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getPackagesForUid(int paramInt) {
    try {
      return this.mPM.getPackagesForUid(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getNameForUid(int paramInt) {
    try {
      return this.mPM.getNameForUid(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getNamesForUids(int[] paramArrayOfint) {
    try {
      return this.mPM.getNamesForUids(paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getUidForSharedUser(String paramString) throws PackageManager.NameNotFoundException {
    try {
      int i = this.mPM.getUidForSharedUser(paramString);
      if (i != -1)
        return i; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No shared userid for user:");
      stringBuilder.append(paramString);
      throw new PackageManager.NameNotFoundException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ModuleInfo> getInstalledModules(int paramInt) {
    try {
      return this.mPM.getInstalledModules(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ModuleInfo getModuleInfo(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      ModuleInfo moduleInfo = this.mPM.getModuleInfo(paramString, paramInt);
      if (moduleInfo != null)
        return moduleInfo; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No module info for package: ");
      stringBuilder.append(paramString);
      throw new PackageManager.NameNotFoundException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<PackageInfo> getInstalledPackages(int paramInt) {
    return getInstalledPackagesAsUser(paramInt, getUserId());
  }
  
  public List<PackageInfo> getInstalledPackagesAsUser(int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.getInstalledPackages(updateFlagsForPackage(paramInt1, paramInt2), paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<PackageInfo> getPackagesHoldingPermissions(String[] paramArrayOfString, int paramInt) {
    int i = getUserId();
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt = updateFlagsForPackage(paramInt, i);
      ParceledListSlice parceledListSlice = iPackageManager.getPackagesHoldingPermissions(paramArrayOfString, paramInt, i);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ApplicationInfo> getInstalledApplications(int paramInt) {
    return getInstalledApplicationsAsUser(paramInt, getUserId());
  }
  
  public List<ApplicationInfo> getInstalledApplicationsAsUser(int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.getInstalledApplications(updateFlagsForApplication(paramInt1, paramInt2), paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InstantAppInfo> getInstantApps() {
    try {
      ParceledListSlice parceledListSlice = this.mPM.getInstantApps(getUserId());
      if (parceledListSlice != null)
        return parceledListSlice.getList(); 
      return (List)Collections.emptyList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Drawable getInstantAppIcon(String paramString) {
    try {
      Bitmap bitmap = this.mPM.getInstantAppIcon(paramString, getUserId());
      if (bitmap != null)
        return new BitmapDrawable(null, bitmap); 
      return null;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isInstantApp() {
    return isInstantApp(this.mContext.getPackageName());
  }
  
  public boolean isInstantApp(String paramString) {
    try {
      return this.mPM.isInstantApp(paramString, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getInstantAppCookieMaxBytes() {
    return Settings.Global.getInt(this.mContext.getContentResolver(), "ephemeral_cookie_max_size_bytes", 16384);
  }
  
  public int getInstantAppCookieMaxSize() {
    return getInstantAppCookieMaxBytes();
  }
  
  public byte[] getInstantAppCookie() {
    try {
      null = this.mPM.getInstantAppCookie(this.mContext.getPackageName(), getUserId());
      if (null != null)
        return null; 
      return EmptyArray.BYTE;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearInstantAppCookie() {
    updateInstantAppCookie((byte[])null);
  }
  
  public void updateInstantAppCookie(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null || paramArrayOfbyte.length <= getInstantAppCookieMaxBytes())
      try {
        this.mPM.setInstantAppCookie(this.mContext.getPackageName(), paramArrayOfbyte, getUserId());
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("instant cookie longer than ");
    stringBuilder.append(getInstantAppCookieMaxBytes());
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean setInstantAppCookie(byte[] paramArrayOfbyte) {
    try {
      return this.mPM.setInstantAppCookie(this.mContext.getPackageName(), paramArrayOfbyte, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ResolveInfo resolveActivity(Intent paramIntent, int paramInt) {
    return resolveActivityAsUser(paramIntent, paramInt, getUserId());
  }
  
  public ResolveInfo resolveActivityAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      return iPackageManager.resolveIntent(paramIntent, str, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryIntentActivities(Intent paramIntent, int paramInt) {
    return queryIntentActivitiesAsUser(paramIntent, paramInt, getUserId());
  }
  
  public List<ResolveInfo> queryIntentActivitiesAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      ParceledListSlice parceledListSlice = iPackageManager.queryIntentActivities(paramIntent, str, paramInt1, paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, Intent paramIntent, int paramInt) {
    int i = getUserId();
    ContentResolver contentResolver = this.mContext.getContentResolver();
    String[] arrayOfString = null;
    if (paramArrayOfIntent != null) {
      int j = paramArrayOfIntent.length;
      for (byte b = 0; b < j; b++, arrayOfString = arrayOfString1) {
        Intent intent = paramArrayOfIntent[b];
        String[] arrayOfString1 = arrayOfString;
        if (intent != null) {
          String str = intent.resolveTypeIfNeeded(contentResolver);
          arrayOfString1 = arrayOfString;
          if (str != null) {
            arrayOfString1 = arrayOfString;
            if (arrayOfString == null)
              arrayOfString1 = new String[j]; 
            arrayOfString1[b] = str;
          } 
        } 
      } 
    } else {
      arrayOfString = null;
    } 
    try {
      IPackageManager iPackageManager = this.mPM;
      String str = paramIntent.resolveTypeIfNeeded(contentResolver);
      paramInt = updateFlagsForComponent(paramInt, i, paramIntent);
      ParceledListSlice parceledListSlice = iPackageManager.queryIntentActivityOptions(paramComponentName, paramArrayOfIntent, arrayOfString, paramIntent, str, paramInt, i);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryBroadcastReceiversAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      ParceledListSlice parceledListSlice = iPackageManager.queryIntentReceivers(paramIntent, str, paramInt1, paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryBroadcastReceivers(Intent paramIntent, int paramInt) {
    return queryBroadcastReceiversAsUser(paramIntent, paramInt, getUserId());
  }
  
  public ResolveInfo resolveServiceAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      return iPackageManager.resolveService(paramIntent, str, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ResolveInfo resolveService(Intent paramIntent, int paramInt) {
    return resolveServiceAsUser(paramIntent, paramInt, getUserId());
  }
  
  public List<ResolveInfo> queryIntentServicesAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      ParceledListSlice parceledListSlice = iPackageManager.queryIntentServices(paramIntent, str, paramInt1, paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryIntentServices(Intent paramIntent, int paramInt) {
    return queryIntentServicesAsUser(paramIntent, paramInt, getUserId());
  }
  
  public List<ResolveInfo> queryIntentContentProvidersAsUser(Intent paramIntent, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = paramIntent.resolveTypeIfNeeded(contextImpl.getContentResolver());
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, paramIntent);
      ParceledListSlice parceledListSlice = iPackageManager.queryIntentContentProviders(paramIntent, str, paramInt1, paramInt2);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ResolveInfo> queryIntentContentProviders(Intent paramIntent, int paramInt) {
    return queryIntentContentProvidersAsUser(paramIntent, paramInt, getUserId());
  }
  
  public ProviderInfo resolveContentProvider(String paramString, int paramInt) {
    return resolveContentProviderAsUser(paramString, paramInt, getUserId());
  }
  
  public ProviderInfo resolveContentProviderAsUser(String paramString, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      paramInt1 = updateFlagsForComponent(paramInt1, paramInt2, (Intent)null);
      return iPackageManager.resolveContentProvider(paramString, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ProviderInfo> queryContentProviders(String paramString, int paramInt1, int paramInt2) {
    return queryContentProviders(paramString, paramInt1, paramInt2, (String)null);
  }
  
  public List<ProviderInfo> queryContentProviders(String paramString1, int paramInt1, int paramInt2, String paramString2) {
    try {
      List<?> list;
      IPackageManager iPackageManager = this.mPM;
      paramInt2 = updateFlagsForComponent(paramInt2, UserHandle.getUserId(paramInt1), (Intent)null);
      ParceledListSlice parceledListSlice = iPackageManager.queryContentProviders(paramString1, paramInt1, paramInt2, paramString2);
      if (parceledListSlice != null) {
        list = parceledListSlice.getList();
      } else {
        list = Collections.emptyList();
      } 
      return (List)list;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt) throws PackageManager.NameNotFoundException {
    try {
      InstrumentationInfo instrumentationInfo = this.mPM.getInstrumentationInfo(paramComponentName, paramInt);
      if (instrumentationInfo != null)
        return instrumentationInfo; 
      throw new PackageManager.NameNotFoundException(paramComponentName.toString());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<InstrumentationInfo> queryInstrumentation(String paramString, int paramInt) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.queryInstrumentation(paramString, paramInt);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Drawable getDrawable(String paramString, int paramInt, ApplicationInfo paramApplicationInfo) {
    ResourceName resourceName = new ResourceName(paramInt);
    Drawable drawable = getCachedIcon(resourceName);
    if (drawable != null)
      return drawable; 
    ApplicationInfo applicationInfo = paramApplicationInfo;
    if (paramApplicationInfo == null)
      try {
        applicationInfo = getApplicationInfo(paramString, 1024);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        return null;
      }  
    if (paramInt != 0) {
      String str;
      try {
        Resources resources = getResourcesForApplication(applicationInfo);
        Drawable drawable1 = resources.getDrawable(paramInt, null);
        if (drawable1 != null)
          putCachedIcon(resourceName, drawable1); 
        return drawable1;
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failure retrieving resources for ");
        stringBuilder.append(applicationInfo.packageName);
        Log.w("PackageManager", stringBuilder.toString());
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failure retrieving resources for ");
        stringBuilder.append(applicationInfo.packageName);
        stringBuilder.append(": ");
        stringBuilder.append(notFoundException.getMessage());
        str = stringBuilder.toString();
        Log.w("PackageManager", str);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failure retrieving icon 0x");
        stringBuilder.append(Integer.toHexString(paramInt));
        stringBuilder.append(" in package ");
        stringBuilder.append(str);
        str = stringBuilder.toString();
        Log.w("PackageManager", str, exception);
      } 
    } 
    return null;
  }
  
  public Drawable getActivityIcon(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    return getActivityInfo(paramComponentName, 1024).loadIcon(this);
  }
  
  public Drawable getActivityIcon(Intent paramIntent) throws PackageManager.NameNotFoundException {
    if (paramIntent.getComponent() != null)
      return getActivityIcon(paramIntent.getComponent()); 
    ResolveInfo resolveInfo = resolveActivity(paramIntent, 65536);
    if (resolveInfo != null)
      return resolveInfo.activityInfo.loadIcon(this); 
    throw new PackageManager.NameNotFoundException(paramIntent.toUri(0));
  }
  
  public Drawable getDefaultActivityIcon() {
    IOplusThemeManager iOplusThemeManager = OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0]);
    ContextImpl contextImpl = this.mContext;
    return iOplusThemeManager.getDefaultActivityIcon(contextImpl, contextImpl.getResources());
  }
  
  public Drawable getApplicationIcon(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadIcon(this);
  }
  
  public Drawable getApplicationIcon(String paramString) throws PackageManager.NameNotFoundException {
    return getApplicationIcon(getApplicationInfo(paramString, 1024));
  }
  
  public Drawable getActivityBanner(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    return getActivityInfo(paramComponentName, 1024).loadBanner(this);
  }
  
  public Drawable getActivityBanner(Intent paramIntent) throws PackageManager.NameNotFoundException {
    if (paramIntent.getComponent() != null)
      return getActivityBanner(paramIntent.getComponent()); 
    ResolveInfo resolveInfo = resolveActivity(paramIntent, 65536);
    if (resolveInfo != null)
      return resolveInfo.activityInfo.loadBanner(this); 
    throw new PackageManager.NameNotFoundException(paramIntent.toUri(0));
  }
  
  public Drawable getApplicationBanner(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadBanner(this);
  }
  
  public Drawable getApplicationBanner(String paramString) throws PackageManager.NameNotFoundException {
    return getApplicationBanner(getApplicationInfo(paramString, 1024));
  }
  
  public Drawable getActivityLogo(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    return getActivityInfo(paramComponentName, 1024).loadLogo(this);
  }
  
  public Drawable getActivityLogo(Intent paramIntent) throws PackageManager.NameNotFoundException {
    if (paramIntent.getComponent() != null)
      return getActivityLogo(paramIntent.getComponent()); 
    ResolveInfo resolveInfo = resolveActivity(paramIntent, 65536);
    if (resolveInfo != null)
      return resolveInfo.activityInfo.loadLogo(this); 
    throw new PackageManager.NameNotFoundException(paramIntent.toUri(0));
  }
  
  public Drawable getApplicationLogo(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadLogo(this);
  }
  
  public Drawable getApplicationLogo(String paramString) throws PackageManager.NameNotFoundException {
    return getApplicationLogo(getApplicationInfo(paramString, 1024));
  }
  
  public Drawable getUserBadgedIcon(Drawable paramDrawable, UserHandle paramUserHandle) {
    Drawable drawable2 = getMultiAppUserBadgedIcon(paramUserHandle);
    if (drawable2 != null)
      return getBadgedDrawable(paramDrawable, drawable2, (Rect)null, false); 
    if (!hasUserBadge(paramUserHandle.getIdentifier()))
      return paramDrawable; 
    LauncherIcons launcherIcons = new LauncherIcons(this.mContext);
    int i = getUserManager().getUserIconBadgeResId(paramUserHandle.getIdentifier());
    int j = getUserBadgeColor(paramUserHandle, false);
    Drawable drawable1 = launcherIcons.getBadgeDrawable(i, j);
    return getBadgedDrawable(paramDrawable, drawable1, (Rect)null, true);
  }
  
  public Drawable getUserBadgedDrawableForDensity(Drawable paramDrawable, UserHandle paramUserHandle, Rect paramRect, int paramInt) {
    Drawable drawable = getUserBadgeForDensity(paramUserHandle, paramInt);
    if (drawable == null)
      return paramDrawable; 
    return getBadgedDrawable(paramDrawable, drawable, paramRect, true);
  }
  
  private int getUserBadgeColor(UserHandle paramUserHandle, boolean paramBoolean) {
    if (paramBoolean && this.mContext.getResources().getConfiguration().isNightModeActive())
      return getUserManager().getUserBadgeDarkColor(paramUserHandle.getIdentifier()); 
    return getUserManager().getUserBadgeColor(paramUserHandle.getIdentifier());
  }
  
  public Drawable getUserBadgeForDensity(UserHandle paramUserHandle, int paramInt) {
    int i = getMultiAppUserBadgeId(paramUserHandle, true);
    if (-1 != i) {
      Drawable drawable = getDrawableForDensity(i, paramInt);
      if (drawable != null)
        return drawable; 
    } 
    Drawable drawable1 = getProfileIconForDensity(paramUserHandle, 17302384, paramInt);
    if (drawable1 == null)
      return null; 
    i = getUserManager().getUserBadgeResId(paramUserHandle.getIdentifier());
    Drawable drawable2 = getDrawableForDensity(i, paramInt);
    drawable2.setTint(getUserBadgeColor(paramUserHandle, false));
    return new LayerDrawable(new Drawable[] { drawable1, drawable2 });
  }
  
  public Drawable getUserBadgeForDensityNoBackground(UserHandle paramUserHandle, int paramInt) {
    int i = getMultiAppUserBadgeId(paramUserHandle, false);
    if (-1 != i) {
      Drawable drawable1 = getDrawableForDensity(i, paramInt);
      if (drawable1 != null)
        return drawable1; 
    } 
    if (!hasUserBadge(paramUserHandle.getIdentifier()))
      return null; 
    i = getUserManager().getUserBadgeNoBackgroundResId(paramUserHandle.getIdentifier());
    Drawable drawable = getDrawableForDensity(i, paramInt);
    if (drawable != null)
      drawable.setTint(getUserBadgeColor(paramUserHandle, true)); 
    return drawable;
  }
  
  private Drawable getDrawableForDensity(int paramInt1, int paramInt2) {
    int i = paramInt2;
    if (paramInt2 <= 0)
      i = (this.mContext.getResources().getDisplayMetrics()).densityDpi; 
    return this.mContext.getResources().getDrawableForDensity(paramInt1, i);
  }
  
  private Drawable getProfileIconForDensity(UserHandle paramUserHandle, int paramInt1, int paramInt2) {
    if (hasUserBadge(paramUserHandle.getIdentifier()))
      return getDrawableForDensity(paramInt1, paramInt2); 
    return null;
  }
  
  public CharSequence getUserBadgedLabel(CharSequence paramCharSequence, UserHandle paramUserHandle) {
    if (OplusMultiAppManager.getInstance().isMultiAppUserId(paramUserHandle.getIdentifier()))
      return paramCharSequence; 
    return getUserManager().getBadgedLabelForUser(paramCharSequence, paramUserHandle);
  }
  
  public Resources getResourcesForActivity(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    ApplicationInfo applicationInfo = (getActivityInfo(paramComponentName, 1024)).applicationInfo;
    return getResourcesForApplication(applicationInfo);
  }
  
  public Resources getResourcesForApplication(ApplicationInfo paramApplicationInfo) throws PackageManager.NameNotFoundException {
    boolean bool;
    String str, arrayOfString1[];
    if (paramApplicationInfo.packageName.equals("system"))
      return this.mContext.mMainThread.getSystemUiContext().getResources(); 
    if (paramApplicationInfo.uid == Process.myUid()) {
      bool = true;
    } else {
      bool = false;
    } 
    ActivityThread activityThread = this.mContext.mMainThread;
    if (bool) {
      str = paramApplicationInfo.sourceDir;
    } else {
      str = paramApplicationInfo.publicSourceDir;
    } 
    if (bool) {
      arrayOfString1 = paramApplicationInfo.splitSourceDirs;
    } else {
      arrayOfString1 = paramApplicationInfo.splitPublicSourceDirs;
    } 
    String[] arrayOfString2 = paramApplicationInfo.resourceDirs, arrayOfString3 = paramApplicationInfo.sharedLibraryFiles;
    LoadedApk loadedApk = this.mContext.mPackageInfo;
    Resources resources = activityThread.getTopLevelResources(str, arrayOfString1, arrayOfString2, arrayOfString3, 0, loadedApk);
    if (resources != null) {
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(resources, paramApplicationInfo.packageName);
      return resources;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to open ");
    stringBuilder.append(paramApplicationInfo.publicSourceDir);
    throw new PackageManager.NameNotFoundException(stringBuilder.toString());
  }
  
  public Resources getResourcesForApplication(String paramString) throws PackageManager.NameNotFoundException {
    ApplicationInfo applicationInfo = getApplicationInfo(paramString, 1024);
    return getResourcesForApplication(applicationInfo);
  }
  
  public Resources getResourcesForApplicationAsUser(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    if (paramInt >= 0) {
      if ("system".equals(paramString))
        return this.mContext.mMainThread.getSystemUiContext().getResources(); 
      try {
        ApplicationInfo applicationInfo = this.mPM.getApplicationInfo(paramString, 1024, paramInt);
        if (applicationInfo != null)
          return getResourcesForApplication(applicationInfo); 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Package ");
        stringBuilder1.append(paramString);
        stringBuilder1.append(" doesn't exist");
        throw new PackageManager.NameNotFoundException(stringBuilder1.toString());
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Call does not support special user #");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  volatile int mCachedSafeMode = -1;
  
  public boolean isSafeMode() {
    try {
      int i = this.mCachedSafeMode;
      boolean bool = true;
      if (i < 0) {
        if (this.mPM.isSafeMode()) {
          i = 1;
        } else {
          i = 0;
        } 
        this.mCachedSafeMode = i;
      } 
      i = this.mCachedSafeMode;
      if (i == 0)
        bool = false; 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addOnPermissionsChangeListener(PackageManager.OnPermissionsChangedListener paramOnPermissionsChangedListener) {
    synchronized (this.mPermissionListeners) {
      if (this.mPermissionListeners.get(paramOnPermissionsChangedListener) != null)
        return; 
      OnPermissionsChangeListenerDelegate onPermissionsChangeListenerDelegate = new OnPermissionsChangeListenerDelegate();
      this(this, paramOnPermissionsChangedListener, Looper.getMainLooper());
      try {
        this.mPermissionManager.addOnPermissionsChangeListener((IOnPermissionsChangeListener)onPermissionsChangeListenerDelegate);
        this.mPermissionListeners.put(paramOnPermissionsChangedListener, onPermissionsChangeListenerDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void removeOnPermissionsChangeListener(PackageManager.OnPermissionsChangedListener paramOnPermissionsChangedListener) {
    synchronized (this.mPermissionListeners) {
      IOnPermissionsChangeListener iOnPermissionsChangeListener = this.mPermissionListeners.get(paramOnPermissionsChangedListener);
      if (iOnPermissionsChangeListener != null)
        try {
          this.mPermissionManager.removeOnPermissionsChangeListener(iOnPermissionsChangeListener);
          this.mPermissionListeners.remove(paramOnPermissionsChangedListener);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      return;
    } 
  }
  
  static void configurationChanged() {
    synchronized (sSync) {
      sIconCache.clear();
      sStringCache.clear();
      return;
    } 
  }
  
  protected ApplicationPackageManager(ContextImpl paramContextImpl, IPackageManager paramIPackageManager) {
    this(paramContextImpl, paramIPackageManager, ActivityThread.getPermissionManager());
  }
  
  protected ApplicationPackageManager(ContextImpl paramContextImpl, IPackageManager paramIPackageManager, IPermissionManager paramIPermissionManager) {
    super(paramContextImpl, paramIPackageManager, paramIPermissionManager);
    this.mUserUnlocked = false;
    this.mPermissionListeners = (Map<PackageManager.OnPermissionsChangedListener, IOnPermissionsChangeListener>)new ArrayMap();
    this.mContext = paramContextImpl;
    this.mPM = paramIPackageManager;
    this.mPermissionManager = paramIPermissionManager;
  }
  
  private int updateFlagsForPackage(int paramInt1, int paramInt2) {
    if ((paramInt1 & 0xF) != 0)
      if ((0x100C0000 & paramInt1) == 0)
        onImplicitDirectBoot(paramInt2);  
    return paramInt1;
  }
  
  private int updateFlagsForApplication(int paramInt1, int paramInt2) {
    return updateFlagsForPackage(paramInt1, paramInt2);
  }
  
  private int updateFlagsForComponent(int paramInt1, int paramInt2, Intent paramIntent) {
    int i = paramInt1;
    if (paramIntent != null) {
      i = paramInt1;
      if ((paramIntent.getFlags() & 0x100) != 0)
        i = paramInt1 | 0x10000000; 
    } 
    if ((0x100C0000 & i) == 0)
      onImplicitDirectBoot(paramInt2); 
    return i;
  }
  
  private void onImplicitDirectBoot(int paramInt) {
    if (StrictMode.vmImplicitDirectBootEnabled())
      if (paramInt == UserHandle.myUserId()) {
        if (this.mUserUnlocked)
          return; 
        UserManager userManager = this.mContext.<UserManager>getSystemService(UserManager.class);
        if (userManager.isUserUnlockingOrUnlocked(paramInt)) {
          this.mUserUnlocked = true;
        } else {
          StrictMode.onImplicitDirectBoot();
        } 
      } else {
        UserManager userManager = this.mContext.<UserManager>getSystemService(UserManager.class);
        if (!userManager.isUserUnlockingOrUnlocked(paramInt))
          StrictMode.onImplicitDirectBoot(); 
      }  
  }
  
  private Drawable getCachedIcon(ResourceName paramResourceName) {
    synchronized (sSync) {
      WeakReference<Drawable.ConstantState> weakReference = (WeakReference)sIconCache.get(paramResourceName);
      if (weakReference != null) {
        Drawable drawable;
        Drawable.ConstantState constantState = weakReference.get();
        if (constantState != null) {
          drawable = constantState.newDrawable();
          return drawable;
        } 
        sIconCache.remove(drawable);
      } 
      return null;
    } 
  }
  
  private void putCachedIcon(ResourceName paramResourceName, Drawable paramDrawable) {
    synchronized (sSync) {
      ArrayMap<ResourceName, WeakReference<Drawable.ConstantState>> arrayMap = sIconCache;
      WeakReference weakReference = new WeakReference();
      this((T)paramDrawable.getConstantState());
      arrayMap.put(paramResourceName, weakReference);
      return;
    } 
  }
  
  static void handlePackageBroadcast(int paramInt, String[] paramArrayOfString, boolean paramBoolean) {
    boolean bool = false;
    if (paramInt == 1)
      bool = true; 
    if (paramArrayOfString != null && paramArrayOfString.length > 0) {
      paramInt = 0;
      int i;
      byte b;
      for (i = paramArrayOfString.length, b = 0; b < i; ) {
        String str = paramArrayOfString[b];
        synchronized (sSync) {
          int j;
          for (j = sIconCache.size() - 1; j >= 0; j--) {
            ResourceName resourceName = (ResourceName)sIconCache.keyAt(j);
            if (resourceName.packageName.equals(str)) {
              sIconCache.removeAt(j);
              paramInt = 1;
            } 
          } 
          for (j = sStringCache.size() - 1; j >= 0; j--) {
            ResourceName resourceName = (ResourceName)sStringCache.keyAt(j);
            if (resourceName.packageName.equals(str)) {
              sStringCache.removeAt(j);
              paramInt = 1;
            } 
          } 
          b++;
        } 
      } 
      if (paramInt != 0 || paramBoolean)
        if (bool) {
          Runtime.getRuntime().gc();
        } else {
          ActivityThread.currentActivityThread().scheduleGcIdler();
        }  
    } 
  }
  
  class ResourceName {
    final int iconId;
    
    final String packageName;
    
    ResourceName(int param1Int) {
      this.packageName = (String)this$0;
      this.iconId = param1Int;
    }
    
    ResourceName(int param1Int) {
      this(((ApplicationInfo)this$0).packageName, param1Int);
    }
    
    ResourceName(int param1Int) {
      this(((ComponentInfo)this$0).applicationInfo.packageName, param1Int);
    }
    
    ResourceName(int param1Int) {
      this(((ResolveInfo)this$0).activityInfo.applicationInfo.packageName, param1Int);
    }
    
    public boolean equals(Object param1Object) {
      String str;
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      ResourceName resourceName = (ResourceName)param1Object;
      if (this.iconId != resourceName.iconId)
        return false; 
      param1Object = this.packageName;
      if (param1Object != null) {
        str = resourceName.packageName;
        if (!param1Object.equals(str))
          bool = false; 
      } else {
        if (((ResourceName)str).packageName == null)
          return bool; 
        bool = false;
      } 
      return bool;
    }
    
    public int hashCode() {
      int i = this.packageName.hashCode();
      int j = this.iconId;
      return i * 31 + j;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{ResourceName ");
      stringBuilder.append(this.packageName);
      stringBuilder.append(" / ");
      stringBuilder.append(this.iconId);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  private CharSequence getCachedString(ResourceName paramResourceName) {
    synchronized (sSync) {
      WeakReference<CharSequence> weakReference = (WeakReference)sStringCache.get(paramResourceName);
      if (weakReference != null) {
        CharSequence charSequence = weakReference.get();
        if (charSequence != null)
          return charSequence; 
        sStringCache.remove(paramResourceName);
      } 
      return null;
    } 
  }
  
  private void putCachedString(ResourceName paramResourceName, CharSequence paramCharSequence) {
    synchronized (sSync) {
      ArrayMap<ResourceName, WeakReference<CharSequence>> arrayMap = sStringCache;
      WeakReference weakReference = new WeakReference();
      this((T)paramCharSequence);
      arrayMap.put(paramResourceName, weakReference);
      return;
    } 
  }
  
  public CharSequence getText(String paramString, int paramInt, ApplicationInfo paramApplicationInfo) {
    StringBuilder stringBuilder;
    ResourceName resourceName = new ResourceName(paramInt);
    CharSequence charSequence = getCachedString(resourceName);
    if (charSequence != null)
      return charSequence; 
    ApplicationInfo applicationInfo = paramApplicationInfo;
    if (paramApplicationInfo == null)
      try {
        applicationInfo = getApplicationInfo(paramString, 1024);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        return null;
      }  
    try {
      Resources resources = getResourcesForApplication(applicationInfo);
      CharSequence charSequence1 = resources.getText(paramInt);
      putCachedString(resourceName, charSequence1);
      return charSequence1;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failure retrieving resources for ");
      stringBuilder.append(applicationInfo.packageName);
      Log.w("PackageManager", stringBuilder.toString());
    } catch (RuntimeException runtimeException) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failure retrieving text 0x");
      stringBuilder1.append(Integer.toHexString(paramInt));
      stringBuilder1.append(" in package ");
      stringBuilder1.append((String)stringBuilder);
      String str = stringBuilder1.toString();
      Log.w("PackageManager", str, runtimeException);
    } 
    return null;
  }
  
  public XmlResourceParser getXml(String paramString, int paramInt, ApplicationInfo paramApplicationInfo) {
    StringBuilder stringBuilder;
    ApplicationInfo applicationInfo = paramApplicationInfo;
    if (paramApplicationInfo == null)
      try {
        applicationInfo = getApplicationInfo(paramString, 1024);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        return null;
      }  
    try {
      Resources resources = getResourcesForApplication(applicationInfo);
      return resources.getXml(paramInt);
    } catch (RuntimeException runtimeException) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failure retrieving xml 0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" in package ");
      stringBuilder.append((String)nameNotFoundException);
      String str = stringBuilder.toString();
      Log.w("PackageManager", str, runtimeException);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException1) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failure retrieving resources for ");
      stringBuilder1.append(((ApplicationInfo)stringBuilder).packageName);
      Log.w("PackageManager", stringBuilder1.toString());
    } 
    return null;
  }
  
  public CharSequence getApplicationLabel(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadLabel(this);
  }
  
  public int installExistingPackage(String paramString) throws PackageManager.NameNotFoundException {
    return installExistingPackage(paramString, 0);
  }
  
  public int installExistingPackage(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return installExistingPackageAsUser(paramString, paramInt, getUserId());
  }
  
  public int installExistingPackageAsUser(String paramString, int paramInt) throws PackageManager.NameNotFoundException {
    return installExistingPackageAsUser(paramString, 0, paramInt);
  }
  
  private int installExistingPackageAsUser(String paramString, int paramInt1, int paramInt2) throws PackageManager.NameNotFoundException {
    try {
      paramInt1 = this.mPM.installExistingPackageAsUser(paramString, paramInt2, 4194304, paramInt1, null);
      if (paramInt1 != -3)
        return paramInt1; 
      PackageManager.NameNotFoundException nameNotFoundException = new PackageManager.NameNotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Package ");
      stringBuilder.append(paramString);
      stringBuilder.append(" doesn't exist");
      this(stringBuilder.toString());
      throw nameNotFoundException;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void verifyPendingInstall(int paramInt1, int paramInt2) {
    try {
      this.mPM.verifyPendingInstall(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void extendVerificationTimeout(int paramInt1, int paramInt2, long paramLong) {
    try {
      this.mPM.extendVerificationTimeout(paramInt1, paramInt2, paramLong);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void verifyIntentFilter(int paramInt1, int paramInt2, List<String> paramList) {
    try {
      this.mPM.verifyIntentFilter(paramInt1, paramInt2, paramList);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getIntentVerificationStatusAsUser(String paramString, int paramInt) {
    try {
      return this.mPM.getIntentVerificationStatus(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean updateIntentVerificationStatusAsUser(String paramString, int paramInt1, int paramInt2) {
    try {
      return this.mPM.updateIntentVerificationStatus(paramString, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<IntentFilterVerificationInfo> getIntentFilterVerifications(String paramString) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.getIntentFilterVerifications(paramString);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<IntentFilter> getAllIntentFilters(String paramString) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ParceledListSlice parceledListSlice = iPackageManager.getAllIntentFilters(paramString);
      if (parceledListSlice == null)
        return Collections.emptyList(); 
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getDefaultBrowserPackageNameAsUser(int paramInt) {
    try {
      return this.mPermissionManager.getDefaultBrowser(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean setDefaultBrowserPackageNameAsUser(String paramString, int paramInt) {
    try {
      return this.mPermissionManager.setDefaultBrowser(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setInstallerPackageName(String paramString1, String paramString2) {
    try {
      this.mPM.setInstallerPackageName(paramString1, paramString2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setUpdateAvailable(String paramString, boolean paramBoolean) {
    try {
      this.mPM.setUpdateAvailable(paramString, paramBoolean);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String getInstallerPackageName(String paramString) {
    try {
      return this.mPM.getInstallerPackageName(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public InstallSourceInfo getInstallSourceInfo(String paramString) throws PackageManager.NameNotFoundException {
    try {
      InstallSourceInfo installSourceInfo = this.mPM.getInstallSourceInfo(paramString);
      if (installSourceInfo != null)
        return installSourceInfo; 
      throw new PackageManager.NameNotFoundException(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getMoveStatus(int paramInt) {
    try {
      return this.mPM.getMoveStatus(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void registerMoveCallback(PackageManager.MoveCallback paramMoveCallback, Handler paramHandler) {
    synchronized (this.mDelegates) {
      MoveCallbackDelegate moveCallbackDelegate = new MoveCallbackDelegate();
      this(paramMoveCallback, paramHandler.getLooper());
      try {
        this.mPM.registerMoveCallback(moveCallbackDelegate);
        this.mDelegates.add(moveCallbackDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  public void unregisterMoveCallback(PackageManager.MoveCallback paramMoveCallback) {
    synchronized (this.mDelegates) {
      for (Iterator<MoveCallbackDelegate> iterator = this.mDelegates.iterator(); iterator.hasNext(); ) {
        MoveCallbackDelegate moveCallbackDelegate = iterator.next();
        PackageManager.MoveCallback moveCallback = moveCallbackDelegate.mCallback;
        if (moveCallback == paramMoveCallback)
          try {
            this.mPM.unregisterMoveCallback(moveCallbackDelegate);
            iterator.remove();
          } catch (RemoteException remoteException) {
            throw remoteException.rethrowFromSystemServer();
          }  
      } 
      return;
    } 
  }
  
  public int movePackage(String paramString, VolumeInfo paramVolumeInfo) {
    try {
      String str;
      if ("private".equals(paramVolumeInfo.id)) {
        str = StorageManager.UUID_PRIVATE_INTERNAL;
      } else if (str.isPrimaryPhysical()) {
        str = "primary_physical";
      } else {
        str = ((VolumeInfo)str).fsUuid;
        Objects.requireNonNull(str);
        str = str;
      } 
      return this.mPM.movePackage(paramString, str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public VolumeInfo getPackageCurrentVolume(ApplicationInfo paramApplicationInfo) {
    StorageManager storageManager = this.mContext.<StorageManager>getSystemService(StorageManager.class);
    return getPackageCurrentVolume(paramApplicationInfo, storageManager);
  }
  
  protected VolumeInfo getPackageCurrentVolume(ApplicationInfo paramApplicationInfo, StorageManager paramStorageManager) {
    if (paramApplicationInfo.isInternal())
      return paramStorageManager.findVolumeById("private"); 
    return paramStorageManager.findVolumeByUuid(paramApplicationInfo.volumeUuid);
  }
  
  public List<VolumeInfo> getPackageCandidateVolumes(ApplicationInfo paramApplicationInfo) {
    StorageManager storageManager = this.mContext.<StorageManager>getSystemService(StorageManager.class);
    return getPackageCandidateVolumes(paramApplicationInfo, storageManager, this.mPM);
  }
  
  protected List<VolumeInfo> getPackageCandidateVolumes(ApplicationInfo paramApplicationInfo, StorageManager paramStorageManager, IPackageManager paramIPackageManager) {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: aload_2
    //   3: invokevirtual getPackageCurrentVolume : (Landroid/content/pm/ApplicationInfo;Landroid/os/storage/StorageManager;)Landroid/os/storage/VolumeInfo;
    //   6: astore #4
    //   8: aload_2
    //   9: invokevirtual getVolumes : ()Ljava/util/List;
    //   12: astore #5
    //   14: new java/util/ArrayList
    //   17: dup
    //   18: invokespecial <init> : ()V
    //   21: astore_2
    //   22: aload #5
    //   24: invokeinterface iterator : ()Ljava/util/Iterator;
    //   29: astore #6
    //   31: aload #6
    //   33: invokeinterface hasNext : ()Z
    //   38: ifeq -> 94
    //   41: aload #6
    //   43: invokeinterface next : ()Ljava/lang/Object;
    //   48: checkcast android/os/storage/VolumeInfo
    //   51: astore #7
    //   53: aload #7
    //   55: aload #4
    //   57: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   60: ifne -> 82
    //   63: aload_0
    //   64: getfield mContext : Landroid/app/ContextImpl;
    //   67: astore #5
    //   69: aload_0
    //   70: aload #5
    //   72: aload_1
    //   73: aload #7
    //   75: aload_3
    //   76: invokespecial isPackageCandidateVolume : (Landroid/app/ContextImpl;Landroid/content/pm/ApplicationInfo;Landroid/os/storage/VolumeInfo;Landroid/content/pm/IPackageManager;)Z
    //   79: ifeq -> 91
    //   82: aload_2
    //   83: aload #7
    //   85: invokeinterface add : (Ljava/lang/Object;)Z
    //   90: pop
    //   91: goto -> 31
    //   94: aload_2
    //   95: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2407	-> 0
    //   #2408	-> 8
    //   #2409	-> 14
    //   #2410	-> 22
    //   #2411	-> 53
    //   #2412	-> 69
    //   #2413	-> 82
    //   #2415	-> 91
    //   #2416	-> 94
  }
  
  protected boolean isForceAllowOnExternal(Context paramContext) {
    ContentResolver contentResolver = paramContext.getContentResolver();
    boolean bool = false;
    if (Settings.Global.getInt(contentResolver, "force_allow_on_external", 0) != 0)
      bool = true; 
    return bool;
  }
  
  protected boolean isAllow3rdPartyOnInternal(Context paramContext) {
    return paramContext.getResources().getBoolean(17891340);
  }
  
  private boolean isPackageCandidateVolume(ContextImpl paramContextImpl, ApplicationInfo paramApplicationInfo, VolumeInfo paramVolumeInfo, IPackageManager paramIPackageManager) {
    boolean bool1 = isForceAllowOnExternal(paramContextImpl);
    boolean bool2 = "private".equals(paramVolumeInfo.getId()), bool3 = true, bool4 = true;
    if (bool2) {
      bool3 = bool4;
      if (!paramApplicationInfo.isSystemApp())
        if (isAllow3rdPartyOnInternal(paramContextImpl)) {
          bool3 = bool4;
        } else {
          bool3 = false;
        }  
      return bool3;
    } 
    if (paramApplicationInfo.isSystemApp())
      return false; 
    if (!bool1 && (paramApplicationInfo.installLocation == 1 || paramApplicationInfo.installLocation == -1))
      return false; 
    if (!paramVolumeInfo.isMountedWritable())
      return false; 
    if (paramVolumeInfo.isPrimaryPhysical())
      return paramApplicationInfo.isInternal(); 
    try {
      bool4 = paramIPackageManager.isPackageDeviceAdminOnAnyUser(paramApplicationInfo.packageName);
      if (bool4)
        return false; 
      if (paramVolumeInfo.getType() != 1)
        bool3 = false; 
      return bool3;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int movePrimaryStorage(VolumeInfo paramVolumeInfo) {
    try {
      String str;
      if ("private".equals(paramVolumeInfo.id)) {
        str = StorageManager.UUID_PRIVATE_INTERNAL;
      } else if (str.isPrimaryPhysical()) {
        str = "primary_physical";
      } else {
        str = ((VolumeInfo)str).fsUuid;
        Objects.requireNonNull(str);
        str = str;
      } 
      return this.mPM.movePrimaryStorage(str);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public VolumeInfo getPrimaryStorageCurrentVolume() {
    StorageManager storageManager = this.mContext.<StorageManager>getSystemService(StorageManager.class);
    String str = storageManager.getPrimaryStorageUuid();
    return storageManager.findVolumeByQualifiedUuid(str);
  }
  
  public List<VolumeInfo> getPrimaryStorageCandidateVolumes() {
    StorageManager storageManager = this.mContext.<StorageManager>getSystemService(StorageManager.class);
    VolumeInfo volumeInfo = getPrimaryStorageCurrentVolume();
    List list = storageManager.getVolumes();
    ArrayList<VolumeInfo> arrayList = new ArrayList();
    String str = storageManager.getPrimaryStorageUuid();
    if (Objects.equals("primary_physical", str) && volumeInfo != null) {
      arrayList.add(volumeInfo);
    } else {
      for (VolumeInfo volumeInfo1 : list) {
        if (Objects.equals(volumeInfo1, volumeInfo) || isPrimaryStorageCandidateVolume(volumeInfo1))
          arrayList.add(volumeInfo1); 
      } 
    } 
    return arrayList;
  }
  
  private static boolean isPrimaryStorageCandidateVolume(VolumeInfo paramVolumeInfo) {
    boolean bool = "private".equals(paramVolumeInfo.getId());
    boolean bool1 = true;
    if (bool)
      return true; 
    if (!paramVolumeInfo.isMountedWritable())
      return false; 
    if (paramVolumeInfo.getType() != 1)
      bool1 = false; 
    return bool1;
  }
  
  public void deletePackage(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt) {
    deletePackageAsUser(paramString, paramIPackageDeleteObserver, paramInt, getUserId());
  }
  
  public void deletePackageAsUser(String paramString, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt1, int paramInt2) {
    try {
      this.mPM.deletePackageAsUser(paramString, -1, paramIPackageDeleteObserver, paramInt2, paramInt1);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver) {
    try {
      this.mPM.clearApplicationUserData(paramString, paramIPackageDataObserver, getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver) {
    try {
      this.mPM.deleteApplicationCacheFiles(paramString, paramIPackageDataObserver);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteApplicationCacheFilesAsUser(String paramString, int paramInt, IPackageDataObserver paramIPackageDataObserver) {
    try {
      this.mPM.deleteApplicationCacheFilesAsUser(paramString, paramInt, paramIPackageDataObserver);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void freeStorageAndNotify(String paramString, long paramLong, IPackageDataObserver paramIPackageDataObserver) {
    try {
      this.mPM.freeStorageAndNotify(paramString, paramLong, 0, paramIPackageDataObserver);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void freeStorage(String paramString, long paramLong, IntentSender paramIntentSender) {
    try {
      this.mPM.freeStorage(paramString, paramLong, 0, paramIntentSender);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] setDistractingPackageRestrictions(String[] paramArrayOfString, int paramInt) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      int i = contextImpl.getUserId();
      return iPackageManager.setDistractingPackageRestrictionsAsUser(paramArrayOfString, paramInt, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] setPackagesSuspended(String[] paramArrayOfString, boolean paramBoolean, PersistableBundle paramPersistableBundle1, PersistableBundle paramPersistableBundle2, String paramString) {
    if (!TextUtils.isEmpty(paramString)) {
      SuspendDialogInfo suspendDialogInfo = (new SuspendDialogInfo.Builder()).setMessage(paramString).build();
    } else {
      paramString = null;
    } 
    return setPackagesSuspended(paramArrayOfString, paramBoolean, paramPersistableBundle1, paramPersistableBundle2, (SuspendDialogInfo)paramString);
  }
  
  public String[] setPackagesSuspended(String[] paramArrayOfString, boolean paramBoolean, PersistableBundle paramPersistableBundle1, PersistableBundle paramPersistableBundle2, SuspendDialogInfo paramSuspendDialogInfo) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = contextImpl.getOpPackageName();
      int i = getUserId();
      return iPackageManager.setPackagesSuspendedAsUser(paramArrayOfString, paramBoolean, paramPersistableBundle1, paramPersistableBundle2, paramSuspendDialogInfo, str, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public String[] getUnsuspendablePackages(String[] paramArrayOfString) {
    try {
      return this.mPM.getUnsuspendablePackagesForUser(paramArrayOfString, this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Bundle getSuspendedPackageAppExtras() {
    try {
      return this.mPM.getSuspendedPackageAppExtras(this.mContext.getOpPackageName(), getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isPackageSuspendedForUser(String paramString, int paramInt) {
    try {
      return this.mPM.isPackageSuspendedForUser(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isPackageSuspended(String paramString) throws PackageManager.NameNotFoundException {
    try {
      return isPackageSuspendedForUser(paramString, getUserId());
    } catch (IllegalArgumentException illegalArgumentException) {
      throw new PackageManager.NameNotFoundException(paramString);
    } 
  }
  
  public boolean isPackageSuspended() {
    return isPackageSuspendedForUser(this.mContext.getOpPackageName(), getUserId());
  }
  
  public void setApplicationCategoryHint(String paramString, int paramInt) {
    try {
      IPackageManager iPackageManager = this.mPM;
      ContextImpl contextImpl = this.mContext;
      String str = contextImpl.getOpPackageName();
      iPackageManager.setApplicationCategoryHint(paramString, paramInt, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void getPackageSizeInfoAsUser(String paramString, int paramInt, IPackageStatsObserver paramIPackageStatsObserver) {
    if ((this.mContext.getApplicationInfo()).targetSdkVersion < 26) {
      if (paramIPackageStatsObserver != null) {
        Log.d("ApplicationPackageManager", "Shame on you for calling the hidden API getPackageSizeInfoAsUser(). Shame!");
        try {
          paramIPackageStatsObserver.onGetStatsCompleted(null, false);
        } catch (RemoteException remoteException) {}
      } 
      return;
    } 
    throw new UnsupportedOperationException("Shame on you for calling the hidden API getPackageSizeInfoAsUser(). Shame!");
  }
  
  public void addPackageToPreferred(String paramString) {
    Log.w("ApplicationPackageManager", "addPackageToPreferred() is a no-op");
  }
  
  public void removePackageFromPreferred(String paramString) {
    Log.w("ApplicationPackageManager", "removePackageFromPreferred() is a no-op");
  }
  
  public List<PackageInfo> getPreferredPackages(int paramInt) {
    Log.w("ApplicationPackageManager", "getPreferredPackages() is a no-op");
    return Collections.emptyList();
  }
  
  public void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName) {
    try {
      this.mPM.addPreferredActivity(paramIntentFilter, paramInt, paramArrayOfComponentName, paramComponentName, getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addPreferredActivityAsUser(IntentFilter paramIntentFilter, int paramInt1, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName, int paramInt2) {
    try {
      this.mPM.addPreferredActivity(paramIntentFilter, paramInt1, paramArrayOfComponentName, paramComponentName, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName) {
    try {
      this.mPM.replacePreferredActivity(paramIntentFilter, paramInt, paramArrayOfComponentName, paramComponentName, getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void replacePreferredActivityAsUser(IntentFilter paramIntentFilter, int paramInt1, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName, int paramInt2) {
    try {
      this.mPM.replacePreferredActivity(paramIntentFilter, paramInt1, paramArrayOfComponentName, paramComponentName, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearPackagePreferredActivities(String paramString) {
    try {
      this.mPM.clearPackagePreferredActivities(paramString);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString) {
    try {
      return this.mPM.getPreferredActivities(paramList, paramList1, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ComponentName getHomeActivities(List<ResolveInfo> paramList) {
    try {
      return this.mPM.getHomeActivities(paramList);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSyntheticAppDetailsActivityEnabled(String paramString, boolean paramBoolean) {
    try {
      byte b;
      ComponentName componentName = new ComponentName();
      this(paramString, APP_DETAILS_ACTIVITY_CLASS_NAME);
      IPackageManager iPackageManager = this.mPM;
      if (paramBoolean) {
        b = 0;
      } else {
        b = 2;
      } 
      int i = getUserId();
      iPackageManager.setComponentEnabledSetting(componentName, b, 1, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean getSyntheticAppDetailsActivityEnabled(String paramString) {
    try {
      ComponentName componentName = new ComponentName();
      this(paramString, APP_DETAILS_ACTIVITY_CLASS_NAME);
      int i = this.mPM.getComponentEnabledSetting(componentName, getUserId());
      boolean bool1 = true, bool2 = bool1;
      if (i != 1)
        if (i == 0) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
      return bool2;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2) {
    try {
      this.mPM.setComponentEnabledSetting(paramComponentName, paramInt1, paramInt2, getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getComponentEnabledSetting(ComponentName paramComponentName) {
    try {
      return this.mPM.getComponentEnabledSetting(paramComponentName, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setApplicationEnabledSetting(String paramString, int paramInt1, int paramInt2) {
    try {
      IPackageManager iPackageManager = this.mPM;
      int i = getUserId();
      String str = this.mContext.getOpPackageName();
      iPackageManager.setApplicationEnabledSetting(paramString, paramInt1, paramInt2, i, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getApplicationEnabledSetting(String paramString) {
    try {
      return this.mPM.getApplicationEnabledSetting(paramString, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void flushPackageRestrictionsAsUser(int paramInt) {
    try {
      this.mPM.flushPackageRestrictionsAsUser(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean setApplicationHiddenSettingAsUser(String paramString, boolean paramBoolean, UserHandle paramUserHandle) {
    try {
      IPackageManager iPackageManager = this.mPM;
      int i = paramUserHandle.getIdentifier();
      return iPackageManager.setApplicationHiddenSettingAsUser(paramString, paramBoolean, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean getApplicationHiddenSettingAsUser(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mPM.getApplicationHiddenSettingAsUser(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setSystemAppState(String paramString, int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt == 3)
            try {
              this.mPM.setSystemAppInstallState(paramString, false, getUserId());
              return;
            } catch (RemoteException remoteException) {
              throw remoteException.rethrowFromSystemServer();
            }  
        } else {
          this.mPM.setSystemAppInstallState((String)remoteException, true, getUserId());
        } 
      } else {
        this.mPM.setSystemAppHiddenUntilInstalled((String)remoteException, false);
      } 
    } else {
      this.mPM.setSystemAppHiddenUntilInstalled((String)remoteException, true);
    } 
  }
  
  public KeySet getKeySetByAlias(String paramString1, String paramString2) {
    Objects.requireNonNull(paramString1);
    Objects.requireNonNull(paramString2);
    try {
      return this.mPM.getKeySetByAlias(paramString1, paramString2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public KeySet getSigningKeySet(String paramString) {
    Objects.requireNonNull(paramString);
    try {
      return this.mPM.getSigningKeySet(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isSignedBy(String paramString, KeySet paramKeySet) {
    Objects.requireNonNull(paramString);
    Objects.requireNonNull(paramKeySet);
    try {
      return this.mPM.isPackageSignedByKeySet(paramString, paramKeySet);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isSignedByExactly(String paramString, KeySet paramKeySet) {
    Objects.requireNonNull(paramString);
    Objects.requireNonNull(paramKeySet);
    try {
      return this.mPM.isPackageSignedByKeySetExactly(paramString, paramKeySet);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public VerifierDeviceIdentity getVerifierDeviceIdentity() {
    try {
      return this.mPM.getVerifierDeviceIdentity();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isUpgrade() {
    return isDeviceUpgrading();
  }
  
  public boolean isDeviceUpgrading() {
    try {
      return this.mPM.isDeviceUpgrading();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public PackageInstaller getPackageInstaller() {
    synchronized (this.mLock) {
      PackageInstaller packageInstaller = this.mInstaller;
      if (packageInstaller == null)
        try {
          PackageInstaller packageInstaller1 = new PackageInstaller();
          IPackageInstaller iPackageInstaller = this.mPM.getPackageInstaller();
          ContextImpl contextImpl = this.mContext;
          this(iPackageInstaller, contextImpl.getPackageName(), getUserId());
          this.mInstaller = packageInstaller1;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      packageInstaller = this.mInstaller;
      return packageInstaller;
    } 
  }
  
  public boolean isPackageAvailable(String paramString) {
    try {
      return this.mPM.isPackageAvailable(paramString, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addCrossProfileIntentFilter(IntentFilter paramIntentFilter, int paramInt1, int paramInt2, int paramInt3) {
    try {
      this.mPM.addCrossProfileIntentFilter(paramIntentFilter, this.mContext.getOpPackageName(), paramInt1, paramInt2, paramInt3);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearCrossProfileIntentFilters(int paramInt) {
    try {
      this.mPM.clearCrossProfileIntentFilters(paramInt, this.mContext.getOpPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Drawable loadItemIcon(PackageItemInfo paramPackageItemInfo, ApplicationInfo paramApplicationInfo) {
    Drawable drawable = loadUnbadgedItemIcon(paramPackageItemInfo, paramApplicationInfo);
    if (paramPackageItemInfo.showUserIcon != -10000)
      return drawable; 
    return getUserBadgedIcon(drawable, new UserHandle(getUserId()));
  }
  
  public Drawable loadUnbadgedItemIcon(PackageItemInfo paramPackageItemInfo, ApplicationInfo paramApplicationInfo) {
    Resources resources;
    if (paramPackageItemInfo.showUserIcon != -10000) {
      int i = paramPackageItemInfo.showUserIcon;
      ContextImpl contextImpl = this.mContext;
      resources = contextImpl.getResources();
      return UserIcons.getDefaultUserIcon(resources, i, false);
    } 
    Drawable drawable2 = null;
    if (((PackageItemInfo)resources).packageName != null) {
      Drawable drawable;
      if (((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).isOplusIcons()) {
        drawable = ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).loadPackageItemIcon((PackageItemInfo)resources, this, paramApplicationInfo, true);
      } else {
        drawable = getDrawable(((PackageItemInfo)resources).packageName, ((PackageItemInfo)resources).icon, paramApplicationInfo);
      } 
      drawable2 = drawable;
    } 
    Drawable drawable3 = drawable2;
    if (drawable2 == null) {
      drawable3 = drawable2;
      if (resources != paramApplicationInfo) {
        drawable3 = drawable2;
        if (paramApplicationInfo != null)
          drawable3 = loadUnbadgedItemIcon(paramApplicationInfo, paramApplicationInfo); 
      } 
    } 
    Drawable drawable1 = drawable3;
    if (drawable3 == null)
      drawable1 = resources.loadDefaultIcon(this); 
    return drawable1;
  }
  
  private Drawable getBadgedDrawable(Drawable paramDrawable1, Drawable paramDrawable2, Rect paramRect, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual getIntrinsicWidth : ()I
    //   4: istore #5
    //   6: aload_1
    //   7: invokevirtual getIntrinsicHeight : ()I
    //   10: istore #6
    //   12: iload #4
    //   14: ifeq -> 47
    //   17: aload_1
    //   18: instanceof android/graphics/drawable/BitmapDrawable
    //   21: ifeq -> 47
    //   24: aload_1
    //   25: checkcast android/graphics/drawable/BitmapDrawable
    //   28: astore #7
    //   30: aload #7
    //   32: invokevirtual getBitmap : ()Landroid/graphics/Bitmap;
    //   35: invokevirtual isMutable : ()Z
    //   38: ifeq -> 47
    //   41: iconst_1
    //   42: istore #8
    //   44: goto -> 50
    //   47: iconst_0
    //   48: istore #8
    //   50: iload #8
    //   52: ifeq -> 67
    //   55: aload_1
    //   56: checkcast android/graphics/drawable/BitmapDrawable
    //   59: invokevirtual getBitmap : ()Landroid/graphics/Bitmap;
    //   62: astore #7
    //   64: goto -> 79
    //   67: iload #5
    //   69: iload #6
    //   71: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   74: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   77: astore #7
    //   79: new android/graphics/Canvas
    //   82: dup
    //   83: aload #7
    //   85: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   88: astore #9
    //   90: iload #8
    //   92: ifne -> 111
    //   95: aload_1
    //   96: iconst_0
    //   97: iconst_0
    //   98: iload #5
    //   100: iload #6
    //   102: invokevirtual setBounds : (IIII)V
    //   105: aload_1
    //   106: aload #9
    //   108: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   111: aload_3
    //   112: ifnull -> 254
    //   115: aload_3
    //   116: getfield left : I
    //   119: iflt -> 196
    //   122: aload_3
    //   123: getfield top : I
    //   126: iflt -> 196
    //   129: aload_3
    //   130: invokevirtual width : ()I
    //   133: iload #5
    //   135: if_icmpgt -> 196
    //   138: aload_3
    //   139: invokevirtual height : ()I
    //   142: iload #6
    //   144: if_icmpgt -> 196
    //   147: aload_2
    //   148: iconst_0
    //   149: iconst_0
    //   150: aload_3
    //   151: invokevirtual width : ()I
    //   154: aload_3
    //   155: invokevirtual height : ()I
    //   158: invokevirtual setBounds : (IIII)V
    //   161: aload #9
    //   163: invokevirtual save : ()I
    //   166: pop
    //   167: aload #9
    //   169: aload_3
    //   170: getfield left : I
    //   173: i2f
    //   174: aload_3
    //   175: getfield top : I
    //   178: i2f
    //   179: invokevirtual translate : (FF)V
    //   182: aload_2
    //   183: aload #9
    //   185: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   188: aload #9
    //   190: invokevirtual restore : ()V
    //   193: goto -> 270
    //   196: new java/lang/StringBuilder
    //   199: dup
    //   200: invokespecial <init> : ()V
    //   203: astore_1
    //   204: aload_1
    //   205: ldc 'Badge location '
    //   207: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload_1
    //   212: aload_3
    //   213: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   216: pop
    //   217: aload_1
    //   218: ldc ' not in badged drawable bounds '
    //   220: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   223: pop
    //   224: aload_1
    //   225: new android/graphics/Rect
    //   228: dup
    //   229: iconst_0
    //   230: iconst_0
    //   231: iload #5
    //   233: iload #6
    //   235: invokespecial <init> : (IIII)V
    //   238: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: new java/lang/IllegalArgumentException
    //   245: dup
    //   246: aload_1
    //   247: invokevirtual toString : ()Ljava/lang/String;
    //   250: invokespecial <init> : (Ljava/lang/String;)V
    //   253: athrow
    //   254: aload_2
    //   255: iconst_0
    //   256: iconst_0
    //   257: iload #5
    //   259: iload #6
    //   261: invokevirtual setBounds : (IIII)V
    //   264: aload_2
    //   265: aload #9
    //   267: invokevirtual draw : (Landroid/graphics/Canvas;)V
    //   270: iload #8
    //   272: ifne -> 317
    //   275: new android/graphics/drawable/BitmapDrawable
    //   278: dup
    //   279: aload_0
    //   280: getfield mContext : Landroid/app/ContextImpl;
    //   283: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   286: aload #7
    //   288: invokespecial <init> : (Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    //   291: astore_2
    //   292: aload_1
    //   293: instanceof android/graphics/drawable/BitmapDrawable
    //   296: ifeq -> 315
    //   299: aload_1
    //   300: checkcast android/graphics/drawable/BitmapDrawable
    //   303: astore_1
    //   304: aload_2
    //   305: aload_1
    //   306: invokevirtual getBitmap : ()Landroid/graphics/Bitmap;
    //   309: invokevirtual getDensity : ()I
    //   312: invokevirtual setTargetDensity : (I)V
    //   315: aload_2
    //   316: areturn
    //   317: aload_1
    //   318: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3074	-> 0
    //   #3075	-> 6
    //   #3076	-> 12
    //   #3078	-> 30
    //   #3081	-> 50
    //   #3082	-> 55
    //   #3084	-> 67
    //   #3086	-> 79
    //   #3088	-> 90
    //   #3089	-> 95
    //   #3090	-> 105
    //   #3093	-> 111
    //   #3094	-> 115
    //   #3095	-> 129
    //   #3100	-> 147
    //   #3102	-> 161
    //   #3103	-> 167
    //   #3104	-> 182
    //   #3105	-> 188
    //   #3096	-> 196
    //   #3107	-> 254
    //   #3108	-> 264
    //   #3111	-> 270
    //   #3112	-> 275
    //   #3114	-> 292
    //   #3115	-> 299
    //   #3116	-> 304
    //   #3119	-> 315
    //   #3122	-> 317
  }
  
  private boolean hasUserBadge(int paramInt) {
    return getUserManager().hasBadge(paramInt);
  }
  
  public int getInstallReason(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mPM.getInstallReason(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class MoveCallbackDelegate extends IPackageMoveObserver.Stub implements Handler.Callback {
    private static final int MSG_CREATED = 1;
    
    private static final int MSG_STATUS_CHANGED = 2;
    
    final PackageManager.MoveCallback mCallback;
    
    final Handler mHandler;
    
    public MoveCallbackDelegate(ApplicationPackageManager this$0, Looper param1Looper) {
      this.mCallback = (PackageManager.MoveCallback)this$0;
      this.mHandler = new Handler(param1Looper, this);
    }
    
    public boolean handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2)
          return false; 
        someArgs = (SomeArgs)param1Message.obj;
        this.mCallback.onStatusChanged(someArgs.argi1, someArgs.argi2, ((Long)someArgs.arg3).longValue());
        someArgs.recycle();
        return true;
      } 
      SomeArgs someArgs = (SomeArgs)((Message)someArgs).obj;
      this.mCallback.onCreated(someArgs.argi1, (Bundle)someArgs.arg2);
      someArgs.recycle();
      return true;
    }
    
    public void onCreated(int param1Int, Bundle param1Bundle) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.argi1 = param1Int;
      someArgs.arg2 = param1Bundle;
      this.mHandler.obtainMessage(1, someArgs).sendToTarget();
    }
    
    public void onStatusChanged(int param1Int1, int param1Int2, long param1Long) {
      SomeArgs someArgs = SomeArgs.obtain();
      someArgs.argi1 = param1Int1;
      someArgs.argi2 = param1Int2;
      someArgs.arg3 = Long.valueOf(param1Long);
      this.mHandler.obtainMessage(2, someArgs).sendToTarget();
    }
  }
  
  private static final Object sSync = new Object();
  
  public static final String APP_PERMISSION_BUTTON_ALLOW_ALWAYS = "app_permission_button_allow_always";
  
  private static final boolean DEBUG_ICONS = false;
  
  public static final boolean DEBUG_TRACE_GRANTS = false;
  
  public static final boolean DEBUG_TRACE_PERMISSION_UPDATES = false;
  
  private static final int DEFAULT_EPHEMERAL_COOKIE_MAX_SIZE_BYTES = 16384;
  
  public static final String PERMISSION_CONTROLLER_RESOURCE_PACKAGE = "com.android.permissioncontroller";
  
  private static final String TAG = "ApplicationPackageManager";
  
  private static final int sDefaultFlags = 1024;
  
  private static ArrayMap<ResourceName, WeakReference<Drawable.ConstantState>> sIconCache;
  
  private static ArrayMap<ResourceName, WeakReference<CharSequence>> sStringCache;
  
  private ArtManager mArtManager;
  
  private final ContextImpl mContext;
  
  private PackageInstaller mInstaller;
  
  private final IPackageManager mPM;
  
  private final Map<PackageManager.OnPermissionsChangedListener, IOnPermissionsChangeListener> mPermissionListeners;
  
  private final IPermissionManager mPermissionManager;
  
  private String mPermissionsControllerPackageName;
  
  private UserManager mUserManager;
  
  private volatile boolean mUserUnlocked;
  
  static {
    sIconCache = new ArrayMap();
    sStringCache = new ArrayMap();
  }
  
  class OnPermissionsChangeListenerDelegate extends IOnPermissionsChangeListener.Stub implements Handler.Callback {
    private static final int MSG_PERMISSIONS_CHANGED = 1;
    
    private final Handler mHandler;
    
    private final PackageManager.OnPermissionsChangedListener mListener;
    
    final ApplicationPackageManager this$0;
    
    public OnPermissionsChangeListenerDelegate(PackageManager.OnPermissionsChangedListener param1OnPermissionsChangedListener, Looper param1Looper) {
      this.mListener = param1OnPermissionsChangedListener;
      this.mHandler = new Handler(param1Looper, this);
    }
    
    public void onPermissionsChanged(int param1Int) {
      this.mHandler.obtainMessage(1, param1Int, 0).sendToTarget();
    }
    
    public boolean handleMessage(Message param1Message) {
      if (param1Message.what != 1)
        return false; 
      int i = param1Message.arg1;
      this.mListener.onPermissionsChanged(i);
      return true;
    }
  }
  
  public boolean canRequestPackageInstalls() {
    try {
      return this.mPM.canRequestPackageInstalls(this.mContext.getPackageName(), getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public ComponentName getInstantAppResolverSettingsComponent() {
    try {
      return this.mPM.getInstantAppResolverSettingsComponent();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public ComponentName getInstantAppInstallerComponent() {
    try {
      return this.mPM.getInstantAppInstallerComponent();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getInstantAppAndroidId(String paramString, UserHandle paramUserHandle) {
    try {
      return this.mPM.getInstantAppAndroidId(paramString, paramUserHandle.getIdentifier());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  class DexModuleRegisterResult {
    final String dexModulePath;
    
    final String message;
    
    final boolean success;
    
    private DexModuleRegisterResult(ApplicationPackageManager this$0, boolean param1Boolean, String param1String1) {
      this.dexModulePath = (String)this$0;
      this.success = param1Boolean;
      this.message = param1String1;
    }
  }
  
  class DexModuleRegisterCallbackDelegate extends IDexModuleRegisterCallback.Stub implements Handler.Callback {
    private static final int MSG_DEX_MODULE_REGISTERED = 1;
    
    private final PackageManager.DexModuleRegisterCallback callback;
    
    private final Handler mHandler;
    
    DexModuleRegisterCallbackDelegate(ApplicationPackageManager this$0) {
      this.callback = (PackageManager.DexModuleRegisterCallback)this$0;
      this.mHandler = new Handler(Looper.getMainLooper(), this);
    }
    
    public void onDexModuleRegistered(String param1String1, boolean param1Boolean, String param1String2) throws RemoteException {
      Message message = this.mHandler.obtainMessage(1, new ApplicationPackageManager.DexModuleRegisterResult(param1Boolean, param1String2));
      message.sendToTarget();
    }
    
    public boolean handleMessage(Message param1Message) {
      if (param1Message.what != 1)
        return false; 
      ApplicationPackageManager.DexModuleRegisterResult dexModuleRegisterResult = (ApplicationPackageManager.DexModuleRegisterResult)param1Message.obj;
      this.callback.onDexModuleRegistered(dexModuleRegisterResult.dexModulePath, dexModuleRegisterResult.success, dexModuleRegisterResult.message);
      return true;
    }
  }
  
  public void registerDexModule(String paramString, PackageManager.DexModuleRegisterCallback paramDexModuleRegisterCallback) {
    boolean bool = false;
    try {
      DexModuleRegisterCallbackDelegate dexModuleRegisterCallbackDelegate;
      StructStat structStat = Os.stat(paramString);
      int i = OsConstants.S_IROTH, j = structStat.st_mode;
      if ((i & j) != 0)
        bool = true; 
      structStat = null;
      if (paramDexModuleRegisterCallback != null)
        dexModuleRegisterCallbackDelegate = new DexModuleRegisterCallbackDelegate(paramDexModuleRegisterCallback); 
      try {
        this.mPM.registerDexModule(this.mContext.getPackageName(), paramString, bool, dexModuleRegisterCallbackDelegate);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowAsRuntimeException();
      } 
    } catch (ErrnoException errnoException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Could not get stat the module file: ");
      stringBuilder.append(errnoException.getMessage());
      String str = stringBuilder.toString();
      paramDexModuleRegisterCallback.onDexModuleRegistered((String)remoteException, false, str);
      return;
    } 
  }
  
  public CharSequence getHarmfulAppWarning(String paramString) {
    try {
      return this.mPM.getHarmfulAppWarning(paramString, getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void setHarmfulAppWarning(String paramString, CharSequence paramCharSequence) {
    try {
      this.mPM.setHarmfulAppWarning(paramString, paramCharSequence, getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public ArtManager getArtManager() {
    synchronized (this.mLock) {
      ArtManager artManager = this.mArtManager;
      if (artManager == null)
        try {
          artManager = new ArtManager();
          this(this.mContext, this.mPM.getArtManager());
          this.mArtManager = artManager;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      artManager = this.mArtManager;
      return artManager;
    } 
  }
  
  public String getDefaultTextClassifierPackageName() {
    try {
      return this.mPM.getDefaultTextClassifierPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getSystemTextClassifierPackageName() {
    try {
      return this.mPM.getSystemTextClassifierPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getAttentionServicePackageName() {
    try {
      return this.mPM.getAttentionServicePackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getWellbeingPackageName() {
    try {
      return this.mPM.getWellbeingPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getAppPredictionServicePackageName() {
    try {
      return this.mPM.getAppPredictionServicePackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getSystemCaptionsServicePackageName() {
    try {
      return this.mPM.getSystemCaptionsServicePackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getSetupWizardPackageName() {
    try {
      return this.mPM.getSetupWizardPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getIncidentReportApproverPackageName() {
    try {
      return this.mPM.getIncidentReportApproverPackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public String getContentCaptureServicePackageName() {
    try {
      return this.mPM.getContentCaptureServicePackageName();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean isPackageStateProtected(String paramString, int paramInt) {
    try {
      return this.mPM.isPackageStateProtected(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void sendDeviceCustomizationReadyBroadcast() {
    try {
      this.mPM.sendDeviceCustomizationReadyBroadcast();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public boolean isAutoRevokeWhitelisted() {
    try {
      return this.mPM.isAutoRevokeWhitelisted(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public void setMimeGroup(String paramString, Set<String> paramSet) {
    try {
      IPackageManager iPackageManager = this.mPM;
      String str = this.mContext.getPackageName();
      ArrayList<String> arrayList = new ArrayList();
      this((Collection)paramSet);
      iPackageManager.setMimeGroup(str, paramString, arrayList);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public Set<String> getMimeGroup(String paramString) {
    try {
      List<String> list = this.mPM.getMimeGroup(this.mContext.getPackageName(), paramString);
      return (Set<String>)new ArraySet(list);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowAsRuntimeException();
    } 
  }
  
  public Drawable getCachedIconForThemeHelper(String paramString, int paramInt) {
    ResourceName resourceName = new ResourceName(paramInt);
    return getCachedIcon(resourceName);
  }
  
  public OplusBaseResources getOplusBaseResourcesForThemeHelper(ApplicationInfo paramApplicationInfo) throws PackageManager.NameNotFoundException {
    return getResourcesForApplication(paramApplicationInfo);
  }
  
  public void putCachedIconForThemeHelper(String paramString, int paramInt, Drawable paramDrawable) {
    ResourceName resourceName = new ResourceName(paramInt);
    putCachedIcon(resourceName, paramDrawable);
  }
  
  public void clearCachedIconForActivity(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    try {
      ActivityInfo activityInfo = getActivityInfo(paramComponentName, 1024);
      if (activityInfo != null)
        synchronized (sSync) {
          StringBuilder stringBuilder;
          ResourceName resourceName = new ResourceName();
          this(activityInfo.packageName, activityInfo.getIconResource());
          WeakReference weakReference = (WeakReference)sIconCache.get(resourceName);
          if (weakReference != null) {
            sIconCache.remove(resourceName);
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("clearCachedIconForActivity name :");
            stringBuilder.append(resourceName.packageName);
            Log.i("ApplicationPackageManager", stringBuilder.toString());
          } else {
            for (int i = sIconCache.size() - 1; i >= 0; i--) {
              ResourceName resourceName1 = (ResourceName)sIconCache.keyAt(i);
              if (resourceName1.packageName.equals(((ActivityInfo)stringBuilder).packageName)) {
                sIconCache.removeAt(i);
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("clearCachedIconForActivity name :");
                stringBuilder1.append(resourceName1.packageName);
                Log.i("ApplicationPackageManager", stringBuilder1.toString());
              } 
            } 
          } 
        }  
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("clearCachedIconForActivity error :");
      stringBuilder.append(nameNotFoundException.getMessage());
      Log.e("ApplicationPackageManager", stringBuilder.toString());
    } 
  }
  
  public List<ApplicationInfo> getIconPackList() {
    return getIconPackListInner();
  }
}
