package android.app.contentsuggestions;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ContentSelection implements Parcelable {
  public ContentSelection(String paramString, Bundle paramBundle) {
    this.mSelectionId = paramString;
    this.mExtras = paramBundle;
  }
  
  public String getId() {
    return this.mSelectionId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mSelectionId);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<ContentSelection> CREATOR = new Parcelable.Creator<ContentSelection>() {
      public ContentSelection createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        return new ContentSelection(str, param1Parcel.readBundle());
      }
      
      public ContentSelection[] newArray(int param1Int) {
        return new ContentSelection[param1Int];
      }
    };
  
  private final Bundle mExtras;
  
  private final String mSelectionId;
}
