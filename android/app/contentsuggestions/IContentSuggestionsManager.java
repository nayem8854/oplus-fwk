package android.app.contentsuggestions;

import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import com.android.internal.os.IResultReceiver;

public interface IContentSuggestionsManager extends IInterface {
  void classifyContentSelections(int paramInt, ClassificationsRequest paramClassificationsRequest, IClassificationsCallback paramIClassificationsCallback) throws RemoteException;
  
  void isEnabled(int paramInt, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void notifyInteraction(int paramInt, String paramString, Bundle paramBundle) throws RemoteException;
  
  void provideContextBitmap(int paramInt, Bitmap paramBitmap, Bundle paramBundle) throws RemoteException;
  
  void provideContextImage(int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void suggestContentSelections(int paramInt, SelectionsRequest paramSelectionsRequest, ISelectionsCallback paramISelectionsCallback) throws RemoteException;
  
  class Default implements IContentSuggestionsManager {
    public void provideContextImage(int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public void provideContextBitmap(int param1Int, Bitmap param1Bitmap, Bundle param1Bundle) throws RemoteException {}
    
    public void suggestContentSelections(int param1Int, SelectionsRequest param1SelectionsRequest, ISelectionsCallback param1ISelectionsCallback) throws RemoteException {}
    
    public void classifyContentSelections(int param1Int, ClassificationsRequest param1ClassificationsRequest, IClassificationsCallback param1IClassificationsCallback) throws RemoteException {}
    
    public void notifyInteraction(int param1Int, String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void isEnabled(int param1Int, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentSuggestionsManager {
    private static final String DESCRIPTOR = "android.app.contentsuggestions.IContentSuggestionsManager";
    
    static final int TRANSACTION_classifyContentSelections = 4;
    
    static final int TRANSACTION_isEnabled = 6;
    
    static final int TRANSACTION_notifyInteraction = 5;
    
    static final int TRANSACTION_provideContextBitmap = 2;
    
    static final int TRANSACTION_provideContextImage = 1;
    
    static final int TRANSACTION_suggestContentSelections = 3;
    
    public Stub() {
      attachInterface(this, "android.app.contentsuggestions.IContentSuggestionsManager");
    }
    
    public static IContentSuggestionsManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.contentsuggestions.IContentSuggestionsManager");
      if (iInterface != null && iInterface instanceof IContentSuggestionsManager)
        return (IContentSuggestionsManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "isEnabled";
        case 5:
          return "notifyInteraction";
        case 4:
          return "classifyContentSelections";
        case 3:
          return "suggestContentSelections";
        case 2:
          return "provideContextBitmap";
        case 1:
          break;
      } 
      return "provideContextImage";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        IResultReceiver iResultReceiver;
        IClassificationsCallback iClassificationsCallback;
        ISelectionsCallback iSelectionsCallback;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
            param1Int1 = param1Parcel1.readInt();
            iResultReceiver = IResultReceiver.Stub.asInterface(param1Parcel1.readStrongBinder());
            isEnabled(param1Int1, iResultReceiver);
            return true;
          case 5:
            iResultReceiver.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
            param1Int1 = iResultReceiver.readInt();
            str = iResultReceiver.readString();
            if (iResultReceiver.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iResultReceiver);
            } else {
              iResultReceiver = null;
            } 
            notifyInteraction(param1Int1, str, (Bundle)iResultReceiver);
            return true;
          case 4:
            iResultReceiver.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
            param1Int1 = iResultReceiver.readInt();
            if (iResultReceiver.readInt() != 0) {
              ClassificationsRequest classificationsRequest = (ClassificationsRequest)ClassificationsRequest.CREATOR.createFromParcel((Parcel)iResultReceiver);
            } else {
              str = null;
            } 
            iClassificationsCallback = IClassificationsCallback.Stub.asInterface(iResultReceiver.readStrongBinder());
            classifyContentSelections(param1Int1, (ClassificationsRequest)str, iClassificationsCallback);
            return true;
          case 3:
            iClassificationsCallback.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
            param1Int1 = iClassificationsCallback.readInt();
            if (iClassificationsCallback.readInt() != 0) {
              SelectionsRequest selectionsRequest = (SelectionsRequest)SelectionsRequest.CREATOR.createFromParcel((Parcel)iClassificationsCallback);
            } else {
              str = null;
            } 
            iSelectionsCallback = ISelectionsCallback.Stub.asInterface(iClassificationsCallback.readStrongBinder());
            suggestContentSelections(param1Int1, (SelectionsRequest)str, iSelectionsCallback);
            return true;
          case 2:
            iSelectionsCallback.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
            param1Int1 = iSelectionsCallback.readInt();
            if (iSelectionsCallback.readInt() != 0) {
              Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel((Parcel)iSelectionsCallback);
            } else {
              str = null;
            } 
            if (iSelectionsCallback.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iSelectionsCallback);
            } else {
              iSelectionsCallback = null;
            } 
            provideContextBitmap(param1Int1, (Bitmap)str, (Bundle)iSelectionsCallback);
            return true;
          case 1:
            break;
        } 
        iSelectionsCallback.enforceInterface("android.app.contentsuggestions.IContentSuggestionsManager");
        param1Int2 = iSelectionsCallback.readInt();
        param1Int1 = iSelectionsCallback.readInt();
        if (iSelectionsCallback.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iSelectionsCallback);
        } else {
          iSelectionsCallback = null;
        } 
        provideContextImage(param1Int2, param1Int1, (Bundle)iSelectionsCallback);
        return true;
      } 
      str.writeString("android.app.contentsuggestions.IContentSuggestionsManager");
      return true;
    }
    
    private static class Proxy implements IContentSuggestionsManager {
      public static IContentSuggestionsManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.contentsuggestions.IContentSuggestionsManager";
      }
      
      public void provideContextImage(int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().provideContextImage(param2Int1, param2Int2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void provideContextBitmap(int param2Int, Bitmap param2Bitmap, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int);
          if (param2Bitmap != null) {
            parcel.writeInt(1);
            param2Bitmap.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().provideContextBitmap(param2Int, param2Bitmap, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void suggestContentSelections(int param2Int, SelectionsRequest param2SelectionsRequest, ISelectionsCallback param2ISelectionsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int);
          if (param2SelectionsRequest != null) {
            parcel.writeInt(1);
            param2SelectionsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ISelectionsCallback != null) {
            iBinder = param2ISelectionsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().suggestContentSelections(param2Int, param2SelectionsRequest, param2ISelectionsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void classifyContentSelections(int param2Int, ClassificationsRequest param2ClassificationsRequest, IClassificationsCallback param2IClassificationsCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int);
          if (param2ClassificationsRequest != null) {
            parcel.writeInt(1);
            param2ClassificationsRequest.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2IClassificationsCallback != null) {
            iBinder = param2IClassificationsCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().classifyContentSelections(param2Int, param2ClassificationsRequest, param2IClassificationsCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyInteraction(int param2Int, String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().notifyInteraction(param2Int, param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void isEnabled(int param2Int, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.contentsuggestions.IContentSuggestionsManager");
          parcel.writeInt(param2Int);
          if (param2IResultReceiver != null) {
            iBinder = param2IResultReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IContentSuggestionsManager.Stub.getDefaultImpl() != null) {
            IContentSuggestionsManager.Stub.getDefaultImpl().isEnabled(param2Int, param2IResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentSuggestionsManager param1IContentSuggestionsManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentSuggestionsManager != null) {
          Proxy.sDefaultImpl = param1IContentSuggestionsManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentSuggestionsManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
