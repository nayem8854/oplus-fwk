package android.app.contentsuggestions;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

@SystemApi
public final class ClassificationsRequest implements Parcelable {
  private ClassificationsRequest(List<ContentSelection> paramList, Bundle paramBundle) {
    this.mSelections = paramList;
    this.mExtras = paramBundle;
  }
  
  public List<ContentSelection> getSelections() {
    return this.mSelections;
  }
  
  public Bundle getExtras() {
    Bundle bundle1 = this.mExtras, bundle2 = bundle1;
    if (bundle1 == null)
      bundle2 = new Bundle(); 
    return bundle2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.mSelections);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<ClassificationsRequest> CREATOR = new Parcelable.Creator<ClassificationsRequest>() {
      public ClassificationsRequest createFromParcel(Parcel param1Parcel) {
        Parcelable.Creator<ContentSelection> creator = ContentSelection.CREATOR;
        ArrayList arrayList = param1Parcel.createTypedArrayList(creator);
        return new ClassificationsRequest(arrayList, param1Parcel.readBundle());
      }
      
      public ClassificationsRequest[] newArray(int param1Int) {
        return new ClassificationsRequest[param1Int];
      }
    };
  
  private final Bundle mExtras;
  
  private final List<ContentSelection> mSelections;
  
  @SystemApi
  class Builder {
    private Bundle mExtras;
    
    private final List<ContentSelection> mSelections;
    
    public Builder(ClassificationsRequest this$0) {
      this.mSelections = (List<ContentSelection>)this$0;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public ClassificationsRequest build() {
      return new ClassificationsRequest(this.mSelections, this.mExtras);
    }
  }
}
