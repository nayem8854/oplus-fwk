package android.app.contentsuggestions;

import android.annotation.SystemApi;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class SelectionsRequest implements Parcelable {
  private SelectionsRequest(int paramInt, Point paramPoint, Bundle paramBundle) {
    this.mTaskId = paramInt;
    this.mInterestPoint = paramPoint;
    this.mExtras = paramBundle;
  }
  
  public int getTaskId() {
    return this.mTaskId;
  }
  
  public Point getInterestPoint() {
    return this.mInterestPoint;
  }
  
  public Bundle getExtras() {
    Bundle bundle1 = this.mExtras, bundle2 = bundle1;
    if (bundle1 == null)
      bundle2 = new Bundle(); 
    return bundle2;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTaskId);
    paramParcel.writeTypedObject(this.mInterestPoint, paramInt);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<SelectionsRequest> CREATOR = new Parcelable.Creator<SelectionsRequest>() {
      public SelectionsRequest createFromParcel(Parcel param1Parcel) {
        return 
          new SelectionsRequest(param1Parcel.readInt(), (Point)param1Parcel.readTypedObject(Point.CREATOR), param1Parcel.readBundle());
      }
      
      public SelectionsRequest[] newArray(int param1Int) {
        return new SelectionsRequest[param1Int];
      }
    };
  
  private final Bundle mExtras;
  
  private final Point mInterestPoint;
  
  private final int mTaskId;
  
  @SystemApi
  class Builder {
    private Bundle mExtras;
    
    private Point mInterestPoint;
    
    private final int mTaskId;
    
    public Builder(SelectionsRequest this$0) {
      this.mTaskId = this$0;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setInterestPoint(Point param1Point) {
      this.mInterestPoint = param1Point;
      return this;
    }
    
    public SelectionsRequest build() {
      return new SelectionsRequest(this.mTaskId, this.mInterestPoint, this.mExtras);
    }
  }
}
