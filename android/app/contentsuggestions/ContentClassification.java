package android.app.contentsuggestions;

import android.annotation.SystemApi;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class ContentClassification implements Parcelable {
  public ContentClassification(String paramString, Bundle paramBundle) {
    this.mClassificationId = paramString;
    this.mExtras = paramBundle;
  }
  
  public String getId() {
    return this.mClassificationId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mClassificationId);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public static final Parcelable.Creator<ContentClassification> CREATOR = new Parcelable.Creator<ContentClassification>() {
      public ContentClassification createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        return new ContentClassification(str, param1Parcel.readBundle());
      }
      
      public ContentClassification[] newArray(int param1Int) {
        return new ContentClassification[param1Int];
      }
    };
  
  private final String mClassificationId;
  
  private final Bundle mExtras;
}
