package android.app.assist;

import android.annotation.SystemApi;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.BadParcelableException;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PooledStringReader;
import android.os.PooledStringWriter;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.view.ViewRootImpl;
import android.view.ViewStructure;
import android.view.WindowManagerGlobal;
import android.view.autofill.AutofillId;
import android.view.autofill.AutofillValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class AssistStructure implements Parcelable {
  private final ArrayList<WindowNode> mWindowNodes = new ArrayList<>();
  
  private final ArrayList<ViewNodeBuilder> mPendingAsyncChildren = new ArrayList<>();
  
  private Rect mTmpRect = new Rect();
  
  public AssistStructure() {
    this.mSanitizeOnWrite = false;
    this.mHaveData = true;
    this.mFlags = 0;
  }
  
  public AssistStructure(Activity paramActivity, boolean paramBoolean, int paramInt) {
    this.mSanitizeOnWrite = false;
    this.mHaveData = true;
    this.mFlags = paramInt;
    WindowManagerGlobal windowManagerGlobal = WindowManagerGlobal.getInstance();
    IBinder iBinder = paramActivity.getActivityToken();
    ArrayList<ViewRootImpl> arrayList = windowManagerGlobal.getRootViews(iBinder);
    for (byte b = 0; b < arrayList.size(); b++) {
      ViewRootImpl viewRootImpl = arrayList.get(b);
      if (viewRootImpl.getView() == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Skipping window with dettached view: ");
        stringBuilder.append(viewRootImpl.getTitle());
        Log.w("AssistStructure", stringBuilder.toString());
      } else {
        this.mWindowNodes.add(new WindowNode(this, viewRootImpl, paramBoolean, paramInt));
      } 
    } 
  }
  
  public AssistStructure(Parcel paramParcel) {
    boolean bool = false;
    this.mSanitizeOnWrite = false;
    this.mTaskId = paramParcel.readInt();
    this.mActivityComponent = ComponentName.readFromParcel(paramParcel);
    if (paramParcel.readInt() == 1)
      bool = true; 
    this.mIsHomeActivity = bool;
    this.mReceiveChannel = paramParcel.readStrongBinder();
  }
  
  public void setAcquisitionStartTime(long paramLong) {
    this.mAcquisitionStartTime = paramLong;
  }
  
  public void setAcquisitionEndTime(long paramLong) {
    this.mAcquisitionEndTime = paramLong;
  }
  
  public void setHomeActivity(boolean paramBoolean) {
    this.mIsHomeActivity = paramBoolean;
  }
  
  public long getAcquisitionStartTime() {
    ensureData();
    return this.mAcquisitionStartTime;
  }
  
  public long getAcquisitionEndTime() {
    ensureData();
    return this.mAcquisitionEndTime;
  }
  
  class SendChannel extends Binder {
    volatile AssistStructure mAssistStructure;
    
    SendChannel(AssistStructure this$0) {
      this.mAssistStructure = this$0;
    }
    
    protected boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      AssistStructure.ParcelTransferWriter parcelTransferWriter;
      StringBuilder stringBuilder;
      if (param1Int1 == 2) {
        AssistStructure assistStructure = this.mAssistStructure;
        if (assistStructure == null)
          return true; 
        param1Parcel1.enforceInterface("android.app.AssistStructure");
        IBinder iBinder = param1Parcel1.readStrongBinder();
        if (iBinder != null) {
          AssistStructure.ParcelTransferWriter parcelTransferWriter1;
          if (iBinder instanceof AssistStructure.ParcelTransferWriter) {
            parcelTransferWriter1 = (AssistStructure.ParcelTransferWriter)iBinder;
            parcelTransferWriter1.writeToParcel(assistStructure, param1Parcel2);
            return true;
          } 
          stringBuilder = new StringBuilder();
          stringBuilder.append("Caller supplied bad token type: ");
          stringBuilder.append(parcelTransferWriter1);
          Log.w("AssistStructure", stringBuilder.toString());
          return true;
        } 
        parcelTransferWriter = new AssistStructure.ParcelTransferWriter(assistStructure, (Parcel)stringBuilder);
        parcelTransferWriter.writeToParcel(assistStructure, (Parcel)stringBuilder);
        return true;
      } 
      return super.onTransact(param1Int1, (Parcel)parcelTransferWriter, (Parcel)stringBuilder, param1Int2);
    }
  }
  
  class ViewStackEntry {
    int curChild;
    
    AssistStructure.ViewNode node;
    
    int numChildren;
  }
  
  class ParcelTransferWriter extends Binder {
    final boolean mWriteStructure;
    
    final ArrayList<AssistStructure.ViewStackEntry> mViewStack = new ArrayList<>();
    
    final float[] mTmpMatrix = new float[9];
    
    final boolean mSanitizeOnWrite;
    
    int mNumWrittenWindows;
    
    int mNumWrittenViews;
    
    int mNumWindows;
    
    int mCurWindow;
    
    int mCurViewStackPos;
    
    AssistStructure.ViewStackEntry mCurViewStackEntry;
    
    ParcelTransferWriter(AssistStructure this$0, Parcel param1Parcel) {
      this.mSanitizeOnWrite = this$0.mSanitizeOnWrite;
      this.mWriteStructure = this$0.waitForReady();
      param1Parcel.writeInt(this$0.mFlags);
      param1Parcel.writeInt(this$0.mAutofillFlags);
      param1Parcel.writeLong(this$0.mAcquisitionStartTime);
      param1Parcel.writeLong(this$0.mAcquisitionEndTime);
      int i = this$0.mWindowNodes.size();
      if (this.mWriteStructure && i > 0) {
        param1Parcel.writeInt(i);
      } else {
        param1Parcel.writeInt(0);
      } 
    }
    
    void writeToParcel(AssistStructure param1AssistStructure, Parcel param1Parcel) {
      int i = param1Parcel.dataPosition();
      this.mNumWrittenWindows = 0;
      this.mNumWrittenViews = 0;
      boolean bool = writeToParcelInner(param1AssistStructure, param1Parcel);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Flattened ");
      if (bool) {
        str = "partial";
      } else {
        str = "final";
      } 
      stringBuilder.append(str);
      stringBuilder.append(" assist data: ");
      stringBuilder.append(param1Parcel.dataPosition() - i);
      stringBuilder.append(" bytes, containing ");
      stringBuilder.append(this.mNumWrittenWindows);
      stringBuilder.append(" windows, ");
      stringBuilder.append(this.mNumWrittenViews);
      stringBuilder.append(" views");
      String str = stringBuilder.toString();
      Log.i("AssistStructure", str);
    }
    
    boolean writeToParcelInner(AssistStructure param1AssistStructure, Parcel param1Parcel) {
      if (this.mNumWindows == 0)
        return false; 
      PooledStringWriter pooledStringWriter = new PooledStringWriter(param1Parcel);
      while (writeNextEntryToParcel(param1AssistStructure, param1Parcel, pooledStringWriter)) {
        if (param1Parcel.dataSize() > 65536) {
          param1Parcel.writeInt(0);
          param1Parcel.writeStrongBinder((IBinder)this);
          pooledStringWriter.finish();
          return true;
        } 
      } 
      pooledStringWriter.finish();
      this.mViewStack.clear();
      return false;
    }
    
    void pushViewStackEntry(AssistStructure.ViewNode param1ViewNode, int param1Int) {
      AssistStructure.ViewStackEntry viewStackEntry;
      if (param1Int >= this.mViewStack.size()) {
        viewStackEntry = new AssistStructure.ViewStackEntry();
        this.mViewStack.add(viewStackEntry);
      } else {
        viewStackEntry = this.mViewStack.get(param1Int);
      } 
      viewStackEntry.node = param1ViewNode;
      viewStackEntry.numChildren = param1ViewNode.getChildCount();
      viewStackEntry.curChild = 0;
      this.mCurViewStackEntry = viewStackEntry;
    }
    
    void writeView(AssistStructure.ViewNode param1ViewNode, Parcel param1Parcel, PooledStringWriter param1PooledStringWriter, int param1Int) {
      param1Parcel.writeInt(572662306);
      param1Int = param1ViewNode.writeSelfToParcel(param1Parcel, param1PooledStringWriter, this.mSanitizeOnWrite, this.mTmpMatrix);
      this.mNumWrittenViews++;
      if ((0x100000 & param1Int) != 0) {
        param1Parcel.writeInt(param1ViewNode.mChildren.length);
        this.mCurViewStackPos = param1Int = this.mCurViewStackPos + 1;
        pushViewStackEntry(param1ViewNode, param1Int);
      } 
    }
    
    boolean writeNextEntryToParcel(AssistStructure param1AssistStructure, Parcel param1Parcel, PooledStringWriter param1PooledStringWriter) {
      AssistStructure.ViewStackEntry viewStackEntry1, viewStackEntry2 = this.mCurViewStackEntry;
      if (viewStackEntry2 != null) {
        if (viewStackEntry2.curChild < this.mCurViewStackEntry.numChildren) {
          AssistStructure.ViewNode viewNode = this.mCurViewStackEntry.node.mChildren[this.mCurViewStackEntry.curChild];
          viewStackEntry1 = this.mCurViewStackEntry;
          viewStackEntry1.curChild++;
          writeView(viewNode, param1Parcel, param1PooledStringWriter, 1);
          return true;
        } 
        do {
          int j = this.mCurViewStackPos - 1;
          if (j < 0) {
            this.mCurViewStackEntry = null;
            break;
          } 
          this.mCurViewStackEntry = viewStackEntry1 = this.mViewStack.get(j);
        } while (viewStackEntry1.curChild >= this.mCurViewStackEntry.numChildren);
        return true;
      } 
      int i = this.mCurWindow;
      if (i < this.mNumWindows) {
        AssistStructure.WindowNode windowNode = ((AssistStructure)viewStackEntry1).mWindowNodes.get(i);
        this.mCurWindow++;
        param1Parcel.writeInt(286331153);
        windowNode.writeSelfToParcel(param1Parcel, param1PooledStringWriter, this.mTmpMatrix);
        this.mNumWrittenWindows++;
        AssistStructure.ViewNode viewNode = windowNode.mRoot;
        this.mCurViewStackPos = 0;
        writeView(viewNode, param1Parcel, param1PooledStringWriter, 0);
        return true;
      } 
      return false;
    }
  }
  
  class ParcelTransferReader {
    private final IBinder mChannel;
    
    private Parcel mCurParcel;
    
    int mNumReadViews;
    
    int mNumReadWindows;
    
    PooledStringReader mStringReader;
    
    final float[] mTmpMatrix = new float[9];
    
    private IBinder mTransferToken;
    
    final AssistStructure this$0;
    
    ParcelTransferReader(IBinder param1IBinder) {
      this.mChannel = param1IBinder;
    }
    
    void go() {
      fetchData();
      AssistStructure.access$102(AssistStructure.this, this.mCurParcel.readInt());
      AssistStructure.access$202(AssistStructure.this, this.mCurParcel.readInt());
      AssistStructure.access$302(AssistStructure.this, this.mCurParcel.readLong());
      AssistStructure.access$402(AssistStructure.this, this.mCurParcel.readLong());
      int i = this.mCurParcel.readInt();
      if (i > 0) {
        this.mStringReader = new PooledStringReader(this.mCurParcel);
        for (byte b = 0; b < i; b++)
          AssistStructure.this.mWindowNodes.add(new AssistStructure.WindowNode(this)); 
      } 
      this.mCurParcel.recycle();
      this.mCurParcel = null;
    }
    
    Parcel readParcel(int param1Int1, int param1Int2) {
      param1Int2 = this.mCurParcel.readInt();
      if (param1Int2 != 0) {
        if (param1Int2 == param1Int1)
          return this.mCurParcel; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Got token ");
        stringBuilder.append(Integer.toHexString(param1Int2));
        stringBuilder.append(", expected token ");
        stringBuilder.append(Integer.toHexString(param1Int1));
        throw new BadParcelableException(stringBuilder.toString());
      } 
      IBinder iBinder = this.mCurParcel.readStrongBinder();
      if (iBinder != null) {
        fetchData();
        this.mStringReader = new PooledStringReader(this.mCurParcel);
        this.mCurParcel.readInt();
        return this.mCurParcel;
      } 
      throw new IllegalStateException("Reached end of partial data without transfer token");
    }
    
    private void fetchData() {
      Parcel parcel = Parcel.obtain();
      try {
        parcel.writeInterfaceToken("android.app.AssistStructure");
        parcel.writeStrongBinder(this.mTransferToken);
        if (this.mCurParcel != null)
          this.mCurParcel.recycle(); 
        Parcel parcel1 = Parcel.obtain();
      } finally {
        parcel.recycle();
      } 
    }
  }
  
  class ViewNodeText {
    int mTextStyle;
    
    float mTextSize;
    
    int mTextSelectionStart;
    
    int mTextSelectionEnd;
    
    int mTextColor = 1;
    
    int mTextBackgroundColor = 1;
    
    CharSequence mText;
    
    int[] mLineCharOffsets;
    
    int[] mLineBaselines;
    
    String mHint;
    
    boolean isSimple() {
      int i = this.mTextBackgroundColor;
      boolean bool = true;
      if (i != 1 || this.mTextSelectionStart != 0 || this.mTextSelectionEnd != 0 || this.mLineCharOffsets != null || this.mLineBaselines != null || this.mHint != null)
        bool = false; 
      return bool;
    }
    
    ViewNodeText(AssistStructure this$0, boolean param1Boolean) {
      this.mText = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)this$0);
      this.mTextSize = this$0.readFloat();
      this.mTextStyle = this$0.readInt();
      this.mTextColor = this$0.readInt();
      if (!param1Boolean) {
        this.mTextBackgroundColor = this$0.readInt();
        this.mTextSelectionStart = this$0.readInt();
        this.mTextSelectionEnd = this$0.readInt();
        this.mLineCharOffsets = this$0.createIntArray();
        this.mLineBaselines = this$0.createIntArray();
        this.mHint = this$0.readString();
      } 
    }
    
    void writeToParcel(Parcel param1Parcel, boolean param1Boolean1, boolean param1Boolean2) {
      CharSequence charSequence;
      if (param1Boolean2) {
        charSequence = this.mText;
      } else {
        charSequence = "";
      } 
      TextUtils.writeToParcel(charSequence, param1Parcel, 0);
      param1Parcel.writeFloat(this.mTextSize);
      param1Parcel.writeInt(this.mTextStyle);
      param1Parcel.writeInt(this.mTextColor);
      if (!param1Boolean1) {
        param1Parcel.writeInt(this.mTextBackgroundColor);
        param1Parcel.writeInt(this.mTextSelectionStart);
        param1Parcel.writeInt(this.mTextSelectionEnd);
        param1Parcel.writeIntArray(this.mLineCharOffsets);
        param1Parcel.writeIntArray(this.mLineBaselines);
        param1Parcel.writeString(this.mHint);
      } 
    }
    
    ViewNodeText() {}
  }
  
  class WindowNode {
    final int mDisplayId;
    
    final int mHeight;
    
    final AssistStructure.ViewNode mRoot;
    
    final CharSequence mTitle;
    
    final int mWidth;
    
    final int mX;
    
    final int mY;
    
    WindowNode(AssistStructure this$0, ViewRootImpl param1ViewRootImpl, boolean param1Boolean, int param1Int) {
      View view = param1ViewRootImpl.getView();
      Rect rect = new Rect();
      view.getBoundsOnScreen(rect);
      this.mX = rect.left - view.getLeft();
      this.mY = rect.top - view.getTop();
      this.mWidth = rect.width();
      this.mHeight = rect.height();
      this.mTitle = param1ViewRootImpl.getTitle();
      this.mDisplayId = param1ViewRootImpl.getDisplayId();
      AssistStructure.ViewNode viewNode = new AssistStructure.ViewNode();
      AssistStructure.ViewNodeBuilder viewNodeBuilder = new AssistStructure.ViewNodeBuilder(this$0, viewNode, false);
      if ((param1ViewRootImpl.getWindowFlags() & 0x2000) != 0)
        if (param1Boolean) {
          int i = resolveViewAutofillFlags(view.getContext(), param1Int);
          view.onProvideAutofillStructure(viewNodeBuilder, i);
        } else {
          view.onProvideStructure(viewNodeBuilder);
          viewNodeBuilder.setAssistBlocked(true);
          return;
        }  
      if (param1Boolean) {
        param1Int = resolveViewAutofillFlags(view.getContext(), param1Int);
        view.dispatchProvideAutofillStructure(viewNodeBuilder, param1Int);
      } else {
        view.dispatchProvideStructure(viewNodeBuilder);
      } 
    }
    
    WindowNode(AssistStructure this$0) {
      Parcel parcel = this$0.readParcel(286331153, 0);
      ((AssistStructure.ParcelTransferReader)this$0).mNumReadWindows++;
      this.mX = parcel.readInt();
      this.mY = parcel.readInt();
      this.mWidth = parcel.readInt();
      this.mHeight = parcel.readInt();
      this.mTitle = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel);
      this.mDisplayId = parcel.readInt();
      this.mRoot = new AssistStructure.ViewNode((AssistStructure.ParcelTransferReader)this$0, 0);
    }
    
    int resolveViewAutofillFlags(Context param1Context, int param1Int) {
      return ((param1Int & 0x1) != 0 || param1Context.isAutofillCompatibilityEnabled()) ? 1 : 0;
    }
    
    void writeSelfToParcel(Parcel param1Parcel, PooledStringWriter param1PooledStringWriter, float[] param1ArrayOffloat) {
      param1Parcel.writeInt(this.mX);
      param1Parcel.writeInt(this.mY);
      param1Parcel.writeInt(this.mWidth);
      param1Parcel.writeInt(this.mHeight);
      TextUtils.writeToParcel(this.mTitle, param1Parcel, 0);
      param1Parcel.writeInt(this.mDisplayId);
    }
    
    public int getLeft() {
      return this.mX;
    }
    
    public int getTop() {
      return this.mY;
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public CharSequence getTitle() {
      return this.mTitle;
    }
    
    public int getDisplayId() {
      return this.mDisplayId;
    }
    
    public AssistStructure.ViewNode getRootViewNode() {
      return this.mRoot;
    }
  }
  
  class ViewNode {
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_HINTS = 16;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_OPTIONS = 32;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_SESSION_ID = 2048;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_TYPE = 8;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_VALUE = 4;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_VIEW_ID = 1;
    
    static final int AUTOFILL_FLAGS_HAS_AUTOFILL_VIRTUAL_VIEW_ID = 2;
    
    static final int AUTOFILL_FLAGS_HAS_HINT_ID_ENTRY = 4096;
    
    static final int AUTOFILL_FLAGS_HAS_HTML_INFO = 64;
    
    static final int AUTOFILL_FLAGS_HAS_MAX_TEXT_EMS = 512;
    
    static final int AUTOFILL_FLAGS_HAS_MAX_TEXT_LENGTH = 1024;
    
    static final int AUTOFILL_FLAGS_HAS_MIN_TEXT_EMS = 256;
    
    static final int AUTOFILL_FLAGS_HAS_TEXT_ID_ENTRY = 128;
    
    static final int FLAGS_ACCESSIBILITY_FOCUSED = 4096;
    
    static final int FLAGS_ACTIVATED = 8192;
    
    static final int FLAGS_ALL_CONTROL = -1048576;
    
    static final int FLAGS_ASSIST_BLOCKED = 128;
    
    static final int FLAGS_CHECKABLE = 256;
    
    static final int FLAGS_CHECKED = 512;
    
    static final int FLAGS_CLICKABLE = 1024;
    
    static final int FLAGS_CONTEXT_CLICKABLE = 16384;
    
    static final int FLAGS_DISABLED = 1;
    
    static final int FLAGS_FOCUSABLE = 16;
    
    static final int FLAGS_FOCUSED = 32;
    
    static final int FLAGS_HAS_ALPHA = 536870912;
    
    static final int FLAGS_HAS_CHILDREN = 1048576;
    
    static final int FLAGS_HAS_COMPLEX_TEXT = 8388608;
    
    static final int FLAGS_HAS_CONTENT_DESCRIPTION = 33554432;
    
    static final int FLAGS_HAS_ELEVATION = 268435456;
    
    static final int FLAGS_HAS_EXTRAS = 4194304;
    
    static final int FLAGS_HAS_ID = 2097152;
    
    static final int FLAGS_HAS_INPUT_TYPE = 262144;
    
    static final int FLAGS_HAS_LARGE_COORDS = 67108864;
    
    static final int FLAGS_HAS_LOCALE_LIST = 65536;
    
    static final int FLAGS_HAS_MATRIX = 1073741824;
    
    static final int FLAGS_HAS_SCROLL = 134217728;
    
    static final int FLAGS_HAS_TEXT = 16777216;
    
    static final int FLAGS_HAS_URL_DOMAIN = 524288;
    
    static final int FLAGS_HAS_URL_SCHEME = 131072;
    
    static final int FLAGS_LONG_CLICKABLE = 2048;
    
    static final int FLAGS_OPAQUE = 32768;
    
    static final int FLAGS_SELECTED = 64;
    
    static final int FLAGS_VISIBILITY_MASK = 12;
    
    public static final int TEXT_COLOR_UNDEFINED = 1;
    
    public static final int TEXT_STYLE_BOLD = 1;
    
    public static final int TEXT_STYLE_ITALIC = 2;
    
    public static final int TEXT_STYLE_STRIKE_THRU = 8;
    
    public static final int TEXT_STYLE_UNDERLINE = 4;
    
    float mAlpha;
    
    int mAutofillFlags;
    
    String[] mAutofillHints;
    
    AutofillId mAutofillId;
    
    CharSequence[] mAutofillOptions;
    
    AssistStructure.AutofillOverlay mAutofillOverlay;
    
    int mAutofillType;
    
    AutofillValue mAutofillValue;
    
    ViewNode[] mChildren;
    
    String mClassName;
    
    CharSequence mContentDescription;
    
    float mElevation;
    
    Bundle mExtras;
    
    int mFlags;
    
    int mHeight;
    
    String mHintIdEntry;
    
    ViewStructure.HtmlInfo mHtmlInfo;
    
    int mId = -1;
    
    String mIdEntry;
    
    String mIdPackage;
    
    String mIdType;
    
    int mImportantForAutofill;
    
    int mInputType;
    
    LocaleList mLocaleList;
    
    Matrix mMatrix;
    
    int mMaxEms;
    
    int mMaxLength;
    
    int mMinEms;
    
    boolean mSanitized;
    
    int mScrollX;
    
    int mScrollY;
    
    AssistStructure.ViewNodeText mText;
    
    String mTextIdEntry;
    
    String mWebDomain;
    
    String mWebScheme;
    
    int mWidth;
    
    int mX;
    
    int mY;
    
    @SystemApi
    public ViewNode() {
      this.mAutofillType = 0;
      this.mMinEms = -1;
      this.mMaxEms = -1;
      this.mMaxLength = -1;
      this.mAlpha = 1.0F;
    }
    
    ViewNode(AssistStructure this$0, int param1Int) {
      boolean bool = false;
      this.mAutofillType = 0;
      this.mMinEms = -1;
      this.mMaxEms = -1;
      this.mMaxLength = -1;
      this.mAlpha = 1.0F;
      Parcel parcel = this$0.readParcel(572662306, param1Int);
      ((AssistStructure.ParcelTransferReader)this$0).mNumReadViews++;
      PooledStringReader pooledStringReader = ((AssistStructure.ParcelTransferReader)this$0).mStringReader;
      this.mClassName = pooledStringReader.readString();
      this.mFlags = parcel.readInt();
      int i = this.mFlags;
      this.mAutofillFlags = parcel.readInt();
      int j = this.mAutofillFlags;
      if ((0x200000 & i) != 0) {
        int k = parcel.readInt();
        if (k != -1) {
          String str = pooledStringReader.readString();
          if (str != null) {
            this.mIdType = pooledStringReader.readString();
            this.mIdPackage = pooledStringReader.readString();
          } 
        } 
      } 
      if (j != 0) {
        boolean bool1;
        if (parcel.readInt() == 1) {
          bool1 = true;
        } else {
          bool1 = false;
        } 
        this.mSanitized = bool1;
        this.mImportantForAutofill = parcel.readInt();
        if ((j & 0x1) != 0) {
          int k = parcel.readInt();
          if ((j & 0x2) != 0) {
            this.mAutofillId = new AutofillId(k, parcel.readInt());
          } else {
            this.mAutofillId = new AutofillId(k);
          } 
          if ((j & 0x800) != 0)
            this.mAutofillId.setSessionId(parcel.readInt()); 
        } 
        if ((j & 0x8) != 0)
          this.mAutofillType = parcel.readInt(); 
        if ((j & 0x10) != 0)
          this.mAutofillHints = parcel.readStringArray(); 
        if ((j & 0x4) != 0)
          this.mAutofillValue = (AutofillValue)parcel.readParcelable(null); 
        if ((j & 0x20) != 0)
          this.mAutofillOptions = parcel.readCharSequenceArray(); 
        if ((j & 0x40) != 0)
          this.mHtmlInfo = (ViewStructure.HtmlInfo)parcel.readParcelable(null); 
        if ((j & 0x100) != 0)
          this.mMinEms = parcel.readInt(); 
        if ((j & 0x200) != 0)
          this.mMaxEms = parcel.readInt(); 
        if ((j & 0x400) != 0)
          this.mMaxLength = parcel.readInt(); 
        if ((j & 0x80) != 0)
          this.mTextIdEntry = pooledStringReader.readString(); 
        if ((j & 0x1000) != 0)
          this.mHintIdEntry = pooledStringReader.readString(); 
      } 
      if ((0x4000000 & i) != 0) {
        this.mX = parcel.readInt();
        this.mY = parcel.readInt();
        this.mWidth = parcel.readInt();
        this.mHeight = parcel.readInt();
      } else {
        j = parcel.readInt();
        this.mX = j & 0x7FFF;
        this.mY = j >> 16 & 0x7FFF;
        j = parcel.readInt();
        this.mWidth = j & 0x7FFF;
        this.mHeight = j >> 16 & 0x7FFF;
      } 
      if ((0x8000000 & i) != 0) {
        this.mScrollX = parcel.readInt();
        this.mScrollY = parcel.readInt();
      } 
      if ((0x40000000 & i) != 0) {
        this.mMatrix = new Matrix();
        parcel.readFloatArray(((AssistStructure.ParcelTransferReader)this$0).mTmpMatrix);
        this.mMatrix.setValues(((AssistStructure.ParcelTransferReader)this$0).mTmpMatrix);
      } 
      if ((0x10000000 & i) != 0)
        this.mElevation = parcel.readFloat(); 
      if ((0x20000000 & i) != 0)
        this.mAlpha = parcel.readFloat(); 
      if ((0x2000000 & i) != 0)
        this.mContentDescription = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel); 
      if ((0x1000000 & i) != 0) {
        boolean bool1 = bool;
        if ((0x800000 & i) == 0)
          bool1 = true; 
        this.mText = new AssistStructure.ViewNodeText(parcel, bool1);
      } 
      if ((0x40000 & i) != 0)
        this.mInputType = parcel.readInt(); 
      if ((0x20000 & i) != 0)
        this.mWebScheme = parcel.readString(); 
      if ((0x80000 & i) != 0)
        this.mWebDomain = parcel.readString(); 
      if ((0x10000 & i) != 0)
        this.mLocaleList = (LocaleList)parcel.readParcelable(null); 
      if ((0x400000 & i) != 0)
        this.mExtras = parcel.readBundle(); 
      if ((0x100000 & i) != 0) {
        j = parcel.readInt();
        this.mChildren = new ViewNode[j];
        for (i = 0; i < j; i++)
          this.mChildren[i] = new ViewNode((AssistStructure.ParcelTransferReader)this$0, param1Int + 1); 
      } 
    }
    
    int writeSelfToParcel(Parcel param1Parcel, PooledStringWriter param1PooledStringWriter, boolean param1Boolean, float[] param1ArrayOffloat) {
      // Byte code:
      //   0: iconst_1
      //   1: istore #5
      //   3: aload_0
      //   4: getfield mFlags : I
      //   7: ldc_w 1048575
      //   10: iand
      //   11: istore #6
      //   13: iconst_0
      //   14: istore #7
      //   16: iload #6
      //   18: istore #8
      //   20: aload_0
      //   21: getfield mId : I
      //   24: iconst_m1
      //   25: if_icmpeq -> 35
      //   28: iload #6
      //   30: ldc 2097152
      //   32: ior
      //   33: istore #8
      //   35: aload_0
      //   36: getfield mX : I
      //   39: sipush #-32768
      //   42: iand
      //   43: ifne -> 109
      //   46: aload_0
      //   47: getfield mY : I
      //   50: sipush #-32768
      //   53: iand
      //   54: ifne -> 109
      //   57: aload_0
      //   58: getfield mWidth : I
      //   61: sipush #-32768
      //   64: iand
      //   65: ifeq -> 74
      //   68: iconst_1
      //   69: istore #9
      //   71: goto -> 77
      //   74: iconst_0
      //   75: istore #9
      //   77: aload_0
      //   78: getfield mHeight : I
      //   81: sipush #-32768
      //   84: iand
      //   85: ifeq -> 94
      //   88: iconst_1
      //   89: istore #10
      //   91: goto -> 97
      //   94: iconst_0
      //   95: istore #10
      //   97: iload #8
      //   99: istore #6
      //   101: iload #9
      //   103: iload #10
      //   105: ior
      //   106: ifeq -> 116
      //   109: iload #8
      //   111: ldc 67108864
      //   113: ior
      //   114: istore #6
      //   116: aload_0
      //   117: getfield mScrollX : I
      //   120: ifne -> 134
      //   123: iload #6
      //   125: istore #8
      //   127: aload_0
      //   128: getfield mScrollY : I
      //   131: ifeq -> 141
      //   134: iload #6
      //   136: ldc 134217728
      //   138: ior
      //   139: istore #8
      //   141: iload #8
      //   143: istore #6
      //   145: aload_0
      //   146: getfield mMatrix : Landroid/graphics/Matrix;
      //   149: ifnull -> 159
      //   152: iload #8
      //   154: ldc 1073741824
      //   156: ior
      //   157: istore #6
      //   159: iload #6
      //   161: istore #8
      //   163: aload_0
      //   164: getfield mElevation : F
      //   167: fconst_0
      //   168: fcmpl
      //   169: ifeq -> 179
      //   172: iload #6
      //   174: ldc 268435456
      //   176: ior
      //   177: istore #8
      //   179: iload #8
      //   181: istore #9
      //   183: aload_0
      //   184: getfield mAlpha : F
      //   187: fconst_1
      //   188: fcmpl
      //   189: ifeq -> 199
      //   192: iload #8
      //   194: ldc 536870912
      //   196: ior
      //   197: istore #9
      //   199: iload #9
      //   201: istore #6
      //   203: aload_0
      //   204: getfield mContentDescription : Ljava/lang/CharSequence;
      //   207: ifnull -> 217
      //   210: iload #9
      //   212: ldc 33554432
      //   214: ior
      //   215: istore #6
      //   217: aload_0
      //   218: getfield mText : Landroid/app/assist/AssistStructure$ViewNodeText;
      //   221: astore #11
      //   223: iload #6
      //   225: istore #8
      //   227: aload #11
      //   229: ifnull -> 258
      //   232: iload #6
      //   234: ldc 16777216
      //   236: ior
      //   237: istore #6
      //   239: iload #6
      //   241: istore #8
      //   243: aload #11
      //   245: invokevirtual isSimple : ()Z
      //   248: ifne -> 258
      //   251: iload #6
      //   253: ldc 8388608
      //   255: ior
      //   256: istore #8
      //   258: iload #8
      //   260: istore #6
      //   262: aload_0
      //   263: getfield mInputType : I
      //   266: ifeq -> 276
      //   269: iload #8
      //   271: ldc 262144
      //   273: ior
      //   274: istore #6
      //   276: iload #6
      //   278: istore #8
      //   280: aload_0
      //   281: getfield mWebScheme : Ljava/lang/String;
      //   284: ifnull -> 294
      //   287: iload #6
      //   289: ldc 131072
      //   291: ior
      //   292: istore #8
      //   294: iload #8
      //   296: istore #6
      //   298: aload_0
      //   299: getfield mWebDomain : Ljava/lang/String;
      //   302: ifnull -> 312
      //   305: iload #8
      //   307: ldc 524288
      //   309: ior
      //   310: istore #6
      //   312: iload #6
      //   314: istore #9
      //   316: aload_0
      //   317: getfield mLocaleList : Landroid/os/LocaleList;
      //   320: ifnull -> 330
      //   323: iload #6
      //   325: ldc 65536
      //   327: ior
      //   328: istore #9
      //   330: iload #9
      //   332: istore #8
      //   334: aload_0
      //   335: getfield mExtras : Landroid/os/Bundle;
      //   338: ifnull -> 348
      //   341: iload #9
      //   343: ldc 4194304
      //   345: ior
      //   346: istore #8
      //   348: iload #8
      //   350: istore #6
      //   352: aload_0
      //   353: getfield mChildren : [Landroid/app/assist/AssistStructure$ViewNode;
      //   356: ifnull -> 366
      //   359: iload #8
      //   361: ldc 1048576
      //   363: ior
      //   364: istore #6
      //   366: aload_0
      //   367: getfield mAutofillId : Landroid/view/autofill/AutofillId;
      //   370: astore #11
      //   372: iload #7
      //   374: istore #8
      //   376: aload #11
      //   378: ifnull -> 426
      //   381: iconst_0
      //   382: iconst_1
      //   383: ior
      //   384: istore #8
      //   386: iload #8
      //   388: istore #9
      //   390: aload #11
      //   392: invokevirtual isVirtualInt : ()Z
      //   395: ifeq -> 404
      //   398: iload #8
      //   400: iconst_2
      //   401: ior
      //   402: istore #9
      //   404: iload #9
      //   406: istore #8
      //   408: aload_0
      //   409: getfield mAutofillId : Landroid/view/autofill/AutofillId;
      //   412: invokevirtual hasSession : ()Z
      //   415: ifeq -> 426
      //   418: iload #9
      //   420: sipush #2048
      //   423: ior
      //   424: istore #8
      //   426: iload #8
      //   428: istore #9
      //   430: aload_0
      //   431: getfield mAutofillValue : Landroid/view/autofill/AutofillValue;
      //   434: ifnull -> 443
      //   437: iload #8
      //   439: iconst_4
      //   440: ior
      //   441: istore #9
      //   443: iload #9
      //   445: istore #10
      //   447: aload_0
      //   448: getfield mAutofillType : I
      //   451: ifeq -> 461
      //   454: iload #9
      //   456: bipush #8
      //   458: ior
      //   459: istore #10
      //   461: iload #10
      //   463: istore #8
      //   465: aload_0
      //   466: getfield mAutofillHints : [Ljava/lang/String;
      //   469: ifnull -> 479
      //   472: iload #10
      //   474: bipush #16
      //   476: ior
      //   477: istore #8
      //   479: iload #8
      //   481: istore #9
      //   483: aload_0
      //   484: getfield mAutofillOptions : [Ljava/lang/CharSequence;
      //   487: ifnull -> 497
      //   490: iload #8
      //   492: bipush #32
      //   494: ior
      //   495: istore #9
      //   497: iload #9
      //   499: istore #8
      //   501: aload_0
      //   502: getfield mHtmlInfo : Landroid/view/ViewStructure$HtmlInfo;
      //   505: instanceof android/os/Parcelable
      //   508: ifeq -> 518
      //   511: iload #9
      //   513: bipush #64
      //   515: ior
      //   516: istore #8
      //   518: iload #8
      //   520: istore #9
      //   522: aload_0
      //   523: getfield mMinEms : I
      //   526: iconst_m1
      //   527: if_icmple -> 538
      //   530: iload #8
      //   532: sipush #256
      //   535: ior
      //   536: istore #9
      //   538: iload #9
      //   540: istore #8
      //   542: aload_0
      //   543: getfield mMaxEms : I
      //   546: iconst_m1
      //   547: if_icmple -> 558
      //   550: iload #9
      //   552: sipush #512
      //   555: ior
      //   556: istore #8
      //   558: iload #8
      //   560: istore #9
      //   562: aload_0
      //   563: getfield mMaxLength : I
      //   566: iconst_m1
      //   567: if_icmple -> 578
      //   570: iload #8
      //   572: sipush #1024
      //   575: ior
      //   576: istore #9
      //   578: iload #9
      //   580: istore #8
      //   582: aload_0
      //   583: getfield mTextIdEntry : Ljava/lang/String;
      //   586: ifnull -> 597
      //   589: iload #9
      //   591: sipush #128
      //   594: ior
      //   595: istore #8
      //   597: iload #8
      //   599: istore #10
      //   601: aload_0
      //   602: getfield mHintIdEntry : Ljava/lang/String;
      //   605: ifnull -> 616
      //   608: iload #8
      //   610: sipush #4096
      //   613: ior
      //   614: istore #10
      //   616: aload_2
      //   617: aload_0
      //   618: getfield mClassName : Ljava/lang/String;
      //   621: invokevirtual writeString : (Ljava/lang/String;)V
      //   624: iload #6
      //   626: istore #9
      //   628: iload #9
      //   630: istore #8
      //   632: iload #10
      //   634: ifeq -> 660
      //   637: aload_0
      //   638: getfield mSanitized : Z
      //   641: ifne -> 652
      //   644: iload #9
      //   646: istore #8
      //   648: iload_3
      //   649: ifne -> 660
      //   652: iload #6
      //   654: sipush #-513
      //   657: iand
      //   658: istore #8
      //   660: aload_0
      //   661: getfield mAutofillOverlay : Landroid/app/assist/AssistStructure$AutofillOverlay;
      //   664: astore #11
      //   666: iload #8
      //   668: istore #9
      //   670: aload #11
      //   672: ifnull -> 700
      //   675: aload #11
      //   677: getfield focused : Z
      //   680: ifeq -> 693
      //   683: iload #8
      //   685: bipush #32
      //   687: ior
      //   688: istore #9
      //   690: goto -> 700
      //   693: iload #8
      //   695: bipush #-33
      //   697: iand
      //   698: istore #9
      //   700: aload_1
      //   701: iload #9
      //   703: invokevirtual writeInt : (I)V
      //   706: aload_1
      //   707: iload #10
      //   709: invokevirtual writeInt : (I)V
      //   712: ldc 2097152
      //   714: iload #6
      //   716: iand
      //   717: ifeq -> 767
      //   720: aload_1
      //   721: aload_0
      //   722: getfield mId : I
      //   725: invokevirtual writeInt : (I)V
      //   728: aload_0
      //   729: getfield mId : I
      //   732: iconst_m1
      //   733: if_icmpeq -> 767
      //   736: aload_2
      //   737: aload_0
      //   738: getfield mIdEntry : Ljava/lang/String;
      //   741: invokevirtual writeString : (Ljava/lang/String;)V
      //   744: aload_0
      //   745: getfield mIdEntry : Ljava/lang/String;
      //   748: ifnull -> 767
      //   751: aload_2
      //   752: aload_0
      //   753: getfield mIdType : Ljava/lang/String;
      //   756: invokevirtual writeString : (Ljava/lang/String;)V
      //   759: aload_2
      //   760: aload_0
      //   761: getfield mIdPackage : Ljava/lang/String;
      //   764: invokevirtual writeString : (Ljava/lang/String;)V
      //   767: iload #10
      //   769: ifeq -> 1085
      //   772: aload_1
      //   773: aload_0
      //   774: getfield mSanitized : Z
      //   777: invokevirtual writeInt : (I)V
      //   780: aload_1
      //   781: aload_0
      //   782: getfield mImportantForAutofill : I
      //   785: invokevirtual writeInt : (I)V
      //   788: aload_0
      //   789: getfield mSanitized : Z
      //   792: ifne -> 807
      //   795: iload_3
      //   796: ifne -> 802
      //   799: goto -> 807
      //   802: iconst_0
      //   803: istore_3
      //   804: goto -> 809
      //   807: iconst_1
      //   808: istore_3
      //   809: iload #10
      //   811: iconst_1
      //   812: iand
      //   813: ifeq -> 865
      //   816: aload_1
      //   817: aload_0
      //   818: getfield mAutofillId : Landroid/view/autofill/AutofillId;
      //   821: invokevirtual getViewId : ()I
      //   824: invokevirtual writeInt : (I)V
      //   827: iload #10
      //   829: iconst_2
      //   830: iand
      //   831: ifeq -> 845
      //   834: aload_1
      //   835: aload_0
      //   836: getfield mAutofillId : Landroid/view/autofill/AutofillId;
      //   839: invokevirtual getVirtualChildIntId : ()I
      //   842: invokevirtual writeInt : (I)V
      //   845: iload #10
      //   847: sipush #2048
      //   850: iand
      //   851: ifeq -> 865
      //   854: aload_1
      //   855: aload_0
      //   856: getfield mAutofillId : Landroid/view/autofill/AutofillId;
      //   859: invokevirtual getSessionId : ()I
      //   862: invokevirtual writeInt : (I)V
      //   865: iload #10
      //   867: bipush #8
      //   869: iand
      //   870: ifeq -> 881
      //   873: aload_1
      //   874: aload_0
      //   875: getfield mAutofillType : I
      //   878: invokevirtual writeInt : (I)V
      //   881: iload #10
      //   883: bipush #16
      //   885: iand
      //   886: ifeq -> 897
      //   889: aload_1
      //   890: aload_0
      //   891: getfield mAutofillHints : [Ljava/lang/String;
      //   894: invokevirtual writeStringArray : ([Ljava/lang/String;)V
      //   897: iload #10
      //   899: iconst_4
      //   900: iand
      //   901: ifeq -> 958
      //   904: iload_3
      //   905: ifeq -> 917
      //   908: aload_0
      //   909: getfield mAutofillValue : Landroid/view/autofill/AutofillValue;
      //   912: astore #11
      //   914: goto -> 951
      //   917: aload_0
      //   918: getfield mAutofillOverlay : Landroid/app/assist/AssistStructure$AutofillOverlay;
      //   921: astore #11
      //   923: aload #11
      //   925: ifnull -> 948
      //   928: aload #11
      //   930: getfield value : Landroid/view/autofill/AutofillValue;
      //   933: ifnull -> 948
      //   936: aload_0
      //   937: getfield mAutofillOverlay : Landroid/app/assist/AssistStructure$AutofillOverlay;
      //   940: getfield value : Landroid/view/autofill/AutofillValue;
      //   943: astore #11
      //   945: goto -> 951
      //   948: aconst_null
      //   949: astore #11
      //   951: aload_1
      //   952: aload #11
      //   954: iconst_0
      //   955: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
      //   958: iload #10
      //   960: bipush #32
      //   962: iand
      //   963: ifeq -> 974
      //   966: aload_1
      //   967: aload_0
      //   968: getfield mAutofillOptions : [Ljava/lang/CharSequence;
      //   971: invokevirtual writeCharSequenceArray : ([Ljava/lang/CharSequence;)V
      //   974: iload #10
      //   976: bipush #64
      //   978: iand
      //   979: ifeq -> 994
      //   982: aload_1
      //   983: aload_0
      //   984: getfield mHtmlInfo : Landroid/view/ViewStructure$HtmlInfo;
      //   987: checkcast android/os/Parcelable
      //   990: iconst_0
      //   991: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
      //   994: iload #10
      //   996: sipush #256
      //   999: iand
      //   1000: ifeq -> 1011
      //   1003: aload_1
      //   1004: aload_0
      //   1005: getfield mMinEms : I
      //   1008: invokevirtual writeInt : (I)V
      //   1011: iload #10
      //   1013: sipush #512
      //   1016: iand
      //   1017: ifeq -> 1028
      //   1020: aload_1
      //   1021: aload_0
      //   1022: getfield mMaxEms : I
      //   1025: invokevirtual writeInt : (I)V
      //   1028: iload #10
      //   1030: sipush #1024
      //   1033: iand
      //   1034: ifeq -> 1045
      //   1037: aload_1
      //   1038: aload_0
      //   1039: getfield mMaxLength : I
      //   1042: invokevirtual writeInt : (I)V
      //   1045: iload #10
      //   1047: sipush #128
      //   1050: iand
      //   1051: ifeq -> 1062
      //   1054: aload_2
      //   1055: aload_0
      //   1056: getfield mTextIdEntry : Ljava/lang/String;
      //   1059: invokevirtual writeString : (Ljava/lang/String;)V
      //   1062: iload_3
      //   1063: istore #5
      //   1065: iload #10
      //   1067: sipush #4096
      //   1070: iand
      //   1071: ifeq -> 1085
      //   1074: aload_2
      //   1075: aload_0
      //   1076: getfield mHintIdEntry : Ljava/lang/String;
      //   1079: invokevirtual writeString : (Ljava/lang/String;)V
      //   1082: iload_3
      //   1083: istore #5
      //   1085: iload #6
      //   1087: ldc 67108864
      //   1089: iand
      //   1090: ifeq -> 1128
      //   1093: aload_1
      //   1094: aload_0
      //   1095: getfield mX : I
      //   1098: invokevirtual writeInt : (I)V
      //   1101: aload_1
      //   1102: aload_0
      //   1103: getfield mY : I
      //   1106: invokevirtual writeInt : (I)V
      //   1109: aload_1
      //   1110: aload_0
      //   1111: getfield mWidth : I
      //   1114: invokevirtual writeInt : (I)V
      //   1117: aload_1
      //   1118: aload_0
      //   1119: getfield mHeight : I
      //   1122: invokevirtual writeInt : (I)V
      //   1125: goto -> 1160
      //   1128: aload_1
      //   1129: aload_0
      //   1130: getfield mY : I
      //   1133: bipush #16
      //   1135: ishl
      //   1136: aload_0
      //   1137: getfield mX : I
      //   1140: ior
      //   1141: invokevirtual writeInt : (I)V
      //   1144: aload_1
      //   1145: aload_0
      //   1146: getfield mHeight : I
      //   1149: bipush #16
      //   1151: ishl
      //   1152: aload_0
      //   1153: getfield mWidth : I
      //   1156: ior
      //   1157: invokevirtual writeInt : (I)V
      //   1160: iload #6
      //   1162: ldc 134217728
      //   1164: iand
      //   1165: ifeq -> 1184
      //   1168: aload_1
      //   1169: aload_0
      //   1170: getfield mScrollX : I
      //   1173: invokevirtual writeInt : (I)V
      //   1176: aload_1
      //   1177: aload_0
      //   1178: getfield mScrollY : I
      //   1181: invokevirtual writeInt : (I)V
      //   1184: iload #6
      //   1186: ldc 1073741824
      //   1188: iand
      //   1189: ifeq -> 1207
      //   1192: aload_0
      //   1193: getfield mMatrix : Landroid/graphics/Matrix;
      //   1196: aload #4
      //   1198: invokevirtual getValues : ([F)V
      //   1201: aload_1
      //   1202: aload #4
      //   1204: invokevirtual writeFloatArray : ([F)V
      //   1207: iload #6
      //   1209: ldc 268435456
      //   1211: iand
      //   1212: ifeq -> 1223
      //   1215: aload_1
      //   1216: aload_0
      //   1217: getfield mElevation : F
      //   1220: invokevirtual writeFloat : (F)V
      //   1223: iload #6
      //   1225: ldc 536870912
      //   1227: iand
      //   1228: ifeq -> 1239
      //   1231: aload_1
      //   1232: aload_0
      //   1233: getfield mAlpha : F
      //   1236: invokevirtual writeFloat : (F)V
      //   1239: iload #6
      //   1241: ldc 33554432
      //   1243: iand
      //   1244: ifeq -> 1256
      //   1247: aload_0
      //   1248: getfield mContentDescription : Ljava/lang/CharSequence;
      //   1251: aload_1
      //   1252: iconst_0
      //   1253: invokestatic writeToParcel : (Ljava/lang/CharSequence;Landroid/os/Parcel;I)V
      //   1256: iload #6
      //   1258: ldc 16777216
      //   1260: iand
      //   1261: ifeq -> 1292
      //   1264: aload_0
      //   1265: getfield mText : Landroid/app/assist/AssistStructure$ViewNodeText;
      //   1268: astore_2
      //   1269: iload #6
      //   1271: ldc 8388608
      //   1273: iand
      //   1274: ifne -> 1282
      //   1277: iconst_1
      //   1278: istore_3
      //   1279: goto -> 1284
      //   1282: iconst_0
      //   1283: istore_3
      //   1284: aload_2
      //   1285: aload_1
      //   1286: iload_3
      //   1287: iload #5
      //   1289: invokevirtual writeToParcel : (Landroid/os/Parcel;ZZ)V
      //   1292: iload #6
      //   1294: ldc 262144
      //   1296: iand
      //   1297: ifeq -> 1308
      //   1300: aload_1
      //   1301: aload_0
      //   1302: getfield mInputType : I
      //   1305: invokevirtual writeInt : (I)V
      //   1308: iload #6
      //   1310: ldc 131072
      //   1312: iand
      //   1313: ifeq -> 1324
      //   1316: aload_1
      //   1317: aload_0
      //   1318: getfield mWebScheme : Ljava/lang/String;
      //   1321: invokevirtual writeString : (Ljava/lang/String;)V
      //   1324: iload #6
      //   1326: ldc 524288
      //   1328: iand
      //   1329: ifeq -> 1340
      //   1332: aload_1
      //   1333: aload_0
      //   1334: getfield mWebDomain : Ljava/lang/String;
      //   1337: invokevirtual writeString : (Ljava/lang/String;)V
      //   1340: iload #6
      //   1342: ldc 65536
      //   1344: iand
      //   1345: ifeq -> 1357
      //   1348: aload_1
      //   1349: aload_0
      //   1350: getfield mLocaleList : Landroid/os/LocaleList;
      //   1353: iconst_0
      //   1354: invokevirtual writeParcelable : (Landroid/os/Parcelable;I)V
      //   1357: iload #6
      //   1359: ldc 4194304
      //   1361: iand
      //   1362: ifeq -> 1373
      //   1365: aload_1
      //   1366: aload_0
      //   1367: getfield mExtras : Landroid/os/Bundle;
      //   1370: invokevirtual writeBundle : (Landroid/os/Bundle;)V
      //   1373: iload #6
      //   1375: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #862	-> 0
      //   #864	-> 3
      //   #865	-> 13
      //   #867	-> 16
      //   #868	-> 28
      //   #870	-> 35
      //   #872	-> 109
      //   #874	-> 116
      //   #875	-> 134
      //   #877	-> 141
      //   #878	-> 152
      //   #880	-> 159
      //   #881	-> 172
      //   #883	-> 179
      //   #884	-> 192
      //   #886	-> 199
      //   #887	-> 210
      //   #889	-> 217
      //   #890	-> 232
      //   #891	-> 239
      //   #892	-> 251
      //   #895	-> 258
      //   #896	-> 269
      //   #898	-> 276
      //   #899	-> 287
      //   #901	-> 294
      //   #902	-> 305
      //   #904	-> 312
      //   #905	-> 323
      //   #907	-> 330
      //   #908	-> 341
      //   #910	-> 348
      //   #911	-> 359
      //   #913	-> 366
      //   #914	-> 381
      //   #915	-> 386
      //   #916	-> 398
      //   #918	-> 404
      //   #919	-> 418
      //   #922	-> 426
      //   #923	-> 437
      //   #925	-> 443
      //   #926	-> 454
      //   #928	-> 461
      //   #929	-> 472
      //   #931	-> 479
      //   #932	-> 490
      //   #934	-> 497
      //   #935	-> 511
      //   #937	-> 518
      //   #938	-> 530
      //   #940	-> 538
      //   #941	-> 550
      //   #943	-> 558
      //   #944	-> 570
      //   #946	-> 578
      //   #947	-> 589
      //   #949	-> 597
      //   #950	-> 608
      //   #953	-> 616
      //   #955	-> 624
      //   #956	-> 628
      //   #958	-> 652
      //   #960	-> 660
      //   #961	-> 675
      //   #962	-> 683
      //   #964	-> 693
      //   #968	-> 700
      //   #969	-> 706
      //   #970	-> 712
      //   #971	-> 720
      //   #972	-> 728
      //   #973	-> 736
      //   #974	-> 744
      //   #975	-> 751
      //   #976	-> 759
      //   #981	-> 767
      //   #982	-> 772
      //   #983	-> 780
      //   #984	-> 788
      //   #985	-> 809
      //   #986	-> 816
      //   #987	-> 827
      //   #988	-> 834
      //   #990	-> 845
      //   #991	-> 854
      //   #994	-> 865
      //   #995	-> 873
      //   #997	-> 881
      //   #998	-> 889
      //   #1000	-> 897
      //   #1002	-> 904
      //   #1003	-> 908
      //   #1004	-> 917
      //   #1005	-> 936
      //   #1007	-> 948
      //   #1009	-> 951
      //   #1011	-> 958
      //   #1012	-> 966
      //   #1014	-> 974
      //   #1015	-> 982
      //   #1017	-> 994
      //   #1018	-> 1003
      //   #1020	-> 1011
      //   #1021	-> 1020
      //   #1023	-> 1028
      //   #1024	-> 1037
      //   #1026	-> 1045
      //   #1027	-> 1054
      //   #1029	-> 1062
      //   #1030	-> 1074
      //   #1033	-> 1085
      //   #1034	-> 1093
      //   #1035	-> 1101
      //   #1036	-> 1109
      //   #1037	-> 1117
      //   #1039	-> 1128
      //   #1040	-> 1144
      //   #1042	-> 1160
      //   #1043	-> 1168
      //   #1044	-> 1176
      //   #1046	-> 1184
      //   #1047	-> 1192
      //   #1048	-> 1201
      //   #1050	-> 1207
      //   #1051	-> 1215
      //   #1053	-> 1223
      //   #1054	-> 1231
      //   #1056	-> 1239
      //   #1057	-> 1247
      //   #1059	-> 1256
      //   #1060	-> 1264
      //   #1062	-> 1292
      //   #1063	-> 1300
      //   #1065	-> 1308
      //   #1066	-> 1316
      //   #1068	-> 1324
      //   #1069	-> 1332
      //   #1071	-> 1340
      //   #1072	-> 1348
      //   #1074	-> 1357
      //   #1075	-> 1365
      //   #1077	-> 1373
    }
    
    public int getId() {
      return this.mId;
    }
    
    public String getIdPackage() {
      return this.mIdPackage;
    }
    
    public String getIdType() {
      return this.mIdType;
    }
    
    public String getIdEntry() {
      return this.mIdEntry;
    }
    
    public AutofillId getAutofillId() {
      return this.mAutofillId;
    }
    
    public int getAutofillType() {
      return this.mAutofillType;
    }
    
    public String[] getAutofillHints() {
      return this.mAutofillHints;
    }
    
    public AutofillValue getAutofillValue() {
      return this.mAutofillValue;
    }
    
    public void setAutofillOverlay(AssistStructure.AutofillOverlay param1AutofillOverlay) {
      this.mAutofillOverlay = param1AutofillOverlay;
    }
    
    public CharSequence[] getAutofillOptions() {
      return this.mAutofillOptions;
    }
    
    public int getInputType() {
      return this.mInputType;
    }
    
    public boolean isSanitized() {
      return this.mSanitized;
    }
    
    public void updateAutofillValue(AutofillValue param1AutofillValue) {
      this.mAutofillValue = param1AutofillValue;
      if (param1AutofillValue.isText()) {
        if (this.mText == null)
          this.mText = new AssistStructure.ViewNodeText(); 
        this.mText.mText = param1AutofillValue.getTextValue();
      } 
    }
    
    public int getLeft() {
      return this.mX;
    }
    
    public int getTop() {
      return this.mY;
    }
    
    public int getScrollX() {
      return this.mScrollX;
    }
    
    public int getScrollY() {
      return this.mScrollY;
    }
    
    public int getWidth() {
      return this.mWidth;
    }
    
    public int getHeight() {
      return this.mHeight;
    }
    
    public Matrix getTransformation() {
      return this.mMatrix;
    }
    
    public float getElevation() {
      return this.mElevation;
    }
    
    public float getAlpha() {
      return this.mAlpha;
    }
    
    public int getVisibility() {
      return this.mFlags & 0xC;
    }
    
    public boolean isAssistBlocked() {
      boolean bool;
      if ((this.mFlags & 0x80) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isEnabled() {
      int i = this.mFlags;
      boolean bool = true;
      if ((i & 0x1) != 0)
        bool = false; 
      return bool;
    }
    
    public boolean isClickable() {
      boolean bool;
      if ((this.mFlags & 0x400) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isFocusable() {
      boolean bool;
      if ((this.mFlags & 0x10) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isFocused() {
      boolean bool;
      if ((this.mFlags & 0x20) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isAccessibilityFocused() {
      boolean bool;
      if ((this.mFlags & 0x1000) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isCheckable() {
      boolean bool;
      if ((this.mFlags & 0x100) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isChecked() {
      boolean bool;
      if ((this.mFlags & 0x200) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isSelected() {
      boolean bool;
      if ((this.mFlags & 0x40) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isActivated() {
      boolean bool;
      if ((this.mFlags & 0x2000) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isOpaque() {
      boolean bool;
      if ((this.mFlags & 0x8000) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isLongClickable() {
      boolean bool;
      if ((this.mFlags & 0x800) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean isContextClickable() {
      boolean bool;
      if ((this.mFlags & 0x4000) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public String getClassName() {
      return this.mClassName;
    }
    
    public CharSequence getContentDescription() {
      return this.mContentDescription;
    }
    
    public String getWebDomain() {
      return this.mWebDomain;
    }
    
    public void setWebDomain(String param1String) {
      if (param1String == null)
        return; 
      Uri uri = Uri.parse(param1String);
      if (uri == null) {
        Log.w("AssistStructure", "Failed to parse web domain");
        return;
      } 
      String str = uri.getScheme();
      if (str == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("http://");
        stringBuilder.append(param1String);
        uri = Uri.parse(stringBuilder.toString());
      } 
      this.mWebDomain = uri.getHost();
    }
    
    public String getWebScheme() {
      return this.mWebScheme;
    }
    
    public ViewStructure.HtmlInfo getHtmlInfo() {
      return this.mHtmlInfo;
    }
    
    public LocaleList getLocaleList() {
      return this.mLocaleList;
    }
    
    public CharSequence getText() {
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        CharSequence charSequence = viewNodeText.mText;
      } else {
        viewNodeText = null;
      } 
      return (CharSequence)viewNodeText;
    }
    
    public int getTextSelectionStart() {
      byte b;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        b = viewNodeText.mTextSelectionStart;
      } else {
        b = -1;
      } 
      return b;
    }
    
    public int getTextSelectionEnd() {
      byte b;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        b = viewNodeText.mTextSelectionEnd;
      } else {
        b = -1;
      } 
      return b;
    }
    
    public int getTextColor() {
      boolean bool;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        bool = viewNodeText.mTextColor;
      } else {
        bool = true;
      } 
      return bool;
    }
    
    public int getTextBackgroundColor() {
      boolean bool;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        bool = viewNodeText.mTextBackgroundColor;
      } else {
        bool = true;
      } 
      return bool;
    }
    
    public float getTextSize() {
      float f;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        f = viewNodeText.mTextSize;
      } else {
        f = 0.0F;
      } 
      return f;
    }
    
    public int getTextStyle() {
      boolean bool;
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        bool = viewNodeText.mTextStyle;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int[] getTextLineCharOffsets() {
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        int[] arrayOfInt = viewNodeText.mLineCharOffsets;
      } else {
        viewNodeText = null;
      } 
      return (int[])viewNodeText;
    }
    
    public int[] getTextLineBaselines() {
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        int[] arrayOfInt = viewNodeText.mLineBaselines;
      } else {
        viewNodeText = null;
      } 
      return (int[])viewNodeText;
    }
    
    public String getTextIdEntry() {
      return this.mTextIdEntry;
    }
    
    public String getHint() {
      AssistStructure.ViewNodeText viewNodeText = this.mText;
      if (viewNodeText != null) {
        String str = viewNodeText.mHint;
      } else {
        viewNodeText = null;
      } 
      return (String)viewNodeText;
    }
    
    public String getHintIdEntry() {
      return this.mHintIdEntry;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    public int getChildCount() {
      boolean bool;
      ViewNode[] arrayOfViewNode = this.mChildren;
      if (arrayOfViewNode != null) {
        bool = arrayOfViewNode.length;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public ViewNode getChildAt(int param1Int) {
      return this.mChildren[param1Int];
    }
    
    public int getMinTextEms() {
      return this.mMinEms;
    }
    
    public int getMaxTextEms() {
      return this.mMaxEms;
    }
    
    public int getMaxTextLength() {
      return this.mMaxLength;
    }
    
    public int getImportantForAutofill() {
      return this.mImportantForAutofill;
    }
  }
  
  class AutofillOverlay {
    public boolean focused;
    
    public AutofillValue value;
  }
  
  static class ViewNodeBuilder extends ViewStructure {
    final AssistStructure mAssist;
    
    final boolean mAsync;
    
    final AssistStructure.ViewNode mNode;
    
    ViewNodeBuilder(AssistStructure param1AssistStructure, AssistStructure.ViewNode param1ViewNode, boolean param1Boolean) {
      this.mAssist = param1AssistStructure;
      this.mNode = param1ViewNode;
      this.mAsync = param1Boolean;
    }
    
    public void setId(int param1Int, String param1String1, String param1String2, String param1String3) {
      this.mNode.mId = param1Int;
      this.mNode.mIdPackage = param1String1;
      this.mNode.mIdType = param1String2;
      this.mNode.mIdEntry = param1String3;
    }
    
    public void setDimens(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      this.mNode.mX = param1Int1;
      this.mNode.mY = param1Int2;
      this.mNode.mScrollX = param1Int3;
      this.mNode.mScrollY = param1Int4;
      this.mNode.mWidth = param1Int5;
      this.mNode.mHeight = param1Int6;
    }
    
    public void setTransformation(Matrix param1Matrix) {
      if (param1Matrix == null) {
        this.mNode.mMatrix = null;
      } else {
        this.mNode.mMatrix = new Matrix(param1Matrix);
      } 
    }
    
    public void setElevation(float param1Float) {
      this.mNode.mElevation = param1Float;
    }
    
    public void setAlpha(float param1Float) {
      this.mNode.mAlpha = param1Float;
    }
    
    public void setVisibility(int param1Int) {
      AssistStructure.ViewNode viewNode = this.mNode;
      viewNode.mFlags = viewNode.mFlags & 0xFFFFFFF3 | param1Int & 0xC;
    }
    
    public void setAssistBlocked(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFF7F | bool;
    }
    
    public void setEnabled(boolean param1Boolean) {
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      viewNode.mFlags = i & 0xFFFFFFFE | param1Boolean ^ true;
    }
    
    public void setClickable(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFBFF | bool;
    }
    
    public void setLongClickable(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFF7FF | bool;
    }
    
    public void setContextClickable(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFBFFF | bool;
    }
    
    public void setFocusable(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFFEF | bool;
    }
    
    public void setFocused(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFFDF | bool;
    }
    
    public void setAccessibilityFocused(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFEFFF | bool;
    }
    
    public void setCheckable(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFEFF | bool;
    }
    
    public void setChecked(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFDFF | bool;
    }
    
    public void setSelected(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFFFBF | bool;
    }
    
    public void setActivated(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFFDFFF | bool;
    }
    
    public void setOpaque(boolean param1Boolean) {
      boolean bool;
      AssistStructure.ViewNode viewNode = this.mNode;
      int i = viewNode.mFlags;
      if (param1Boolean) {
        bool = true;
      } else {
        bool = false;
      } 
      viewNode.mFlags = i & 0xFFFF7FFF | bool;
    }
    
    public void setClassName(String param1String) {
      this.mNode.mClassName = param1String;
    }
    
    public void setContentDescription(CharSequence param1CharSequence) {
      this.mNode.mContentDescription = param1CharSequence;
    }
    
    private final AssistStructure.ViewNodeText getNodeText() {
      if (this.mNode.mText != null)
        return this.mNode.mText; 
      this.mNode.mText = new AssistStructure.ViewNodeText();
      return this.mNode.mText;
    }
    
    public void setText(CharSequence param1CharSequence) {
      AssistStructure.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mText = TextUtils.trimNoCopySpans(param1CharSequence);
      viewNodeText.mTextSelectionEnd = -1;
      viewNodeText.mTextSelectionStart = -1;
    }
    
    public void setText(CharSequence param1CharSequence, int param1Int1, int param1Int2) {
      AssistStructure.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mText = TextUtils.trimNoCopySpans(param1CharSequence);
      viewNodeText.mTextSelectionStart = param1Int1;
      viewNodeText.mTextSelectionEnd = param1Int2;
    }
    
    public void setTextStyle(float param1Float, int param1Int1, int param1Int2, int param1Int3) {
      AssistStructure.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mTextColor = param1Int1;
      viewNodeText.mTextBackgroundColor = param1Int2;
      viewNodeText.mTextSize = param1Float;
      viewNodeText.mTextStyle = param1Int3;
    }
    
    public void setTextLines(int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      AssistStructure.ViewNodeText viewNodeText = getNodeText();
      viewNodeText.mLineCharOffsets = param1ArrayOfint1;
      viewNodeText.mLineBaselines = param1ArrayOfint2;
    }
    
    public void setTextIdEntry(String param1String) {
      AssistStructure.ViewNode viewNode = this.mNode;
      Objects.requireNonNull(param1String);
      viewNode.mTextIdEntry = param1String;
    }
    
    public void setHint(CharSequence param1CharSequence) {
      AssistStructure.ViewNodeText viewNodeText = getNodeText();
      if (param1CharSequence != null) {
        param1CharSequence = param1CharSequence.toString();
      } else {
        param1CharSequence = null;
      } 
      viewNodeText.mHint = (String)param1CharSequence;
    }
    
    public void setHintIdEntry(String param1String) {
      AssistStructure.ViewNode viewNode = this.mNode;
      Objects.requireNonNull(param1String);
      viewNode.mHintIdEntry = param1String;
    }
    
    public CharSequence getText() {
      CharSequence charSequence;
      if (this.mNode.mText != null) {
        charSequence = this.mNode.mText.mText;
      } else {
        charSequence = null;
      } 
      return charSequence;
    }
    
    public int getTextSelectionStart() {
      byte b;
      if (this.mNode.mText != null) {
        b = this.mNode.mText.mTextSelectionStart;
      } else {
        b = -1;
      } 
      return b;
    }
    
    public int getTextSelectionEnd() {
      byte b;
      if (this.mNode.mText != null) {
        b = this.mNode.mText.mTextSelectionEnd;
      } else {
        b = -1;
      } 
      return b;
    }
    
    public CharSequence getHint() {
      CharSequence charSequence;
      if (this.mNode.mText != null) {
        charSequence = this.mNode.mText.mHint;
      } else {
        charSequence = null;
      } 
      return charSequence;
    }
    
    public Bundle getExtras() {
      if (this.mNode.mExtras != null)
        return this.mNode.mExtras; 
      this.mNode.mExtras = new Bundle();
      return this.mNode.mExtras;
    }
    
    public boolean hasExtras() {
      boolean bool;
      if (this.mNode.mExtras != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void setChildCount(int param1Int) {
      this.mNode.mChildren = new AssistStructure.ViewNode[param1Int];
    }
    
    public int addChildCount(int param1Int) {
      if (this.mNode.mChildren == null) {
        setChildCount(param1Int);
        return 0;
      } 
      int i = this.mNode.mChildren.length;
      AssistStructure.ViewNode[] arrayOfViewNode = new AssistStructure.ViewNode[i + param1Int];
      System.arraycopy(this.mNode.mChildren, 0, arrayOfViewNode, 0, i);
      this.mNode.mChildren = arrayOfViewNode;
      return i;
    }
    
    public int getChildCount() {
      boolean bool;
      if (this.mNode.mChildren != null) {
        bool = this.mNode.mChildren.length;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public ViewStructure newChild(int param1Int) {
      AssistStructure.ViewNode viewNode = new AssistStructure.ViewNode();
      this.mNode.mChildren[param1Int] = viewNode;
      return new ViewNodeBuilder(this.mAssist, viewNode, false);
    }
    
    public ViewStructure asyncNewChild(int param1Int) {
      synchronized (this.mAssist) {
        AssistStructure.ViewNode viewNode = new AssistStructure.ViewNode();
        this();
        this.mNode.mChildren[param1Int] = viewNode;
        ViewNodeBuilder viewNodeBuilder = new ViewNodeBuilder();
        this(this.mAssist, viewNode, true);
        this.mAssist.mPendingAsyncChildren.add(viewNodeBuilder);
        return viewNodeBuilder;
      } 
    }
    
    public void asyncCommit() {
      synchronized (this.mAssist) {
        if (this.mAsync) {
          if (this.mAssist.mPendingAsyncChildren.remove(this)) {
            this.mAssist.notifyAll();
            return;
          } 
          IllegalStateException illegalStateException1 = new IllegalStateException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Child ");
          stringBuilder1.append(this);
          stringBuilder1.append(" already committed");
          this(stringBuilder1.toString());
          throw illegalStateException1;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Child ");
        stringBuilder.append(this);
        stringBuilder.append(" was not created with ViewStructure.asyncNewChild");
        this(stringBuilder.toString());
        throw illegalStateException;
      } 
    }
    
    public Rect getTempRect() {
      return this.mAssist.mTmpRect;
    }
    
    public void setAutofillId(AutofillId param1AutofillId) {
      this.mNode.mAutofillId = param1AutofillId;
    }
    
    public void setAutofillId(AutofillId param1AutofillId, int param1Int) {
      this.mNode.mAutofillId = new AutofillId(param1AutofillId, param1Int);
    }
    
    public AutofillId getAutofillId() {
      return this.mNode.mAutofillId;
    }
    
    public void setAutofillType(int param1Int) {
      this.mNode.mAutofillType = param1Int;
    }
    
    public void setAutofillHints(String[] param1ArrayOfString) {
      this.mNode.mAutofillHints = param1ArrayOfString;
    }
    
    public void setAutofillValue(AutofillValue param1AutofillValue) {
      this.mNode.mAutofillValue = param1AutofillValue;
    }
    
    public void setAutofillOptions(CharSequence[] param1ArrayOfCharSequence) {
      this.mNode.mAutofillOptions = param1ArrayOfCharSequence;
    }
    
    public void setImportantForAutofill(int param1Int) {
      this.mNode.mImportantForAutofill = param1Int;
    }
    
    public void setInputType(int param1Int) {
      this.mNode.mInputType = param1Int;
    }
    
    public void setMinTextEms(int param1Int) {
      this.mNode.mMinEms = param1Int;
    }
    
    public void setMaxTextEms(int param1Int) {
      this.mNode.mMaxEms = param1Int;
    }
    
    public void setMaxTextLength(int param1Int) {
      this.mNode.mMaxLength = param1Int;
    }
    
    public void setDataIsSensitive(boolean param1Boolean) {
      this.mNode.mSanitized = param1Boolean ^ true;
    }
    
    public void setWebDomain(String param1String) {
      this.mNode.setWebDomain(param1String);
    }
    
    public void setLocaleList(LocaleList param1LocaleList) {
      this.mNode.mLocaleList = param1LocaleList;
    }
    
    public ViewStructure.HtmlInfo.Builder newHtmlInfoBuilder(String param1String) {
      return new AssistStructure.HtmlInfoNodeBuilder(param1String);
    }
    
    public void setHtmlInfo(ViewStructure.HtmlInfo param1HtmlInfo) {
      this.mNode.mHtmlInfo = param1HtmlInfo;
    }
  }
  
  private static final class HtmlInfoNode extends ViewStructure.HtmlInfo implements Parcelable {
    private HtmlInfoNode(AssistStructure.HtmlInfoNodeBuilder param1HtmlInfoNodeBuilder) {
      this.mTag = param1HtmlInfoNodeBuilder.mTag;
      if (param1HtmlInfoNodeBuilder.mNames == null) {
        this.mNames = null;
        this.mValues = null;
      } else {
        this.mNames = new String[param1HtmlInfoNodeBuilder.mNames.size()];
        this.mValues = new String[param1HtmlInfoNodeBuilder.mValues.size()];
        param1HtmlInfoNodeBuilder.mNames.toArray((Object[])this.mNames);
        param1HtmlInfoNodeBuilder.mValues.toArray((Object[])this.mValues);
      } 
    }
    
    public String getTag() {
      return this.mTag;
    }
    
    public List<Pair<String, String>> getAttributes() {
      if (this.mAttributes == null && this.mNames != null) {
        this.mAttributes = new ArrayList<>(this.mNames.length);
        byte b = 0;
        while (true) {
          String[] arrayOfString = this.mNames;
          if (b < arrayOfString.length) {
            Pair<String, String> pair = new Pair(arrayOfString[b], this.mValues[b]);
            this.mAttributes.add(b, pair);
            b++;
            continue;
          } 
          break;
        } 
      } 
      return this.mAttributes;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mTag);
      param1Parcel.writeStringArray(this.mNames);
      param1Parcel.writeStringArray(this.mValues);
    }
    
    public static final Parcelable.Creator<HtmlInfoNode> CREATOR = new Parcelable.Creator<HtmlInfoNode>() {
        public AssistStructure.HtmlInfoNode createFromParcel(Parcel param2Parcel) {
          String str = param2Parcel.readString();
          AssistStructure.HtmlInfoNodeBuilder htmlInfoNodeBuilder = new AssistStructure.HtmlInfoNodeBuilder(str);
          String[] arrayOfString1 = param2Parcel.readStringArray();
          String[] arrayOfString2 = param2Parcel.readStringArray();
          if (arrayOfString1 != null && arrayOfString2 != null)
            if (arrayOfString1.length != arrayOfString2.length) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("HtmlInfo attributes mismatch: names=");
              stringBuilder.append(arrayOfString1.length);
              stringBuilder.append(", values=");
              stringBuilder.append(arrayOfString2.length);
              Log.w("AssistStructure", stringBuilder.toString());
            } else {
              for (byte b = 0; b < arrayOfString1.length; b++)
                htmlInfoNodeBuilder.addAttribute(arrayOfString1[b], arrayOfString2[b]); 
            }  
          return htmlInfoNodeBuilder.build();
        }
        
        public AssistStructure.HtmlInfoNode[] newArray(int param2Int) {
          return new AssistStructure.HtmlInfoNode[param2Int];
        }
      };
    
    private ArrayList<Pair<String, String>> mAttributes;
    
    private final String[] mNames;
    
    private final String mTag;
    
    private final String[] mValues;
  }
  
  class null implements Parcelable.Creator<HtmlInfoNode> {
    public AssistStructure.HtmlInfoNode createFromParcel(Parcel param1Parcel) {
      String str = param1Parcel.readString();
      AssistStructure.HtmlInfoNodeBuilder htmlInfoNodeBuilder = new AssistStructure.HtmlInfoNodeBuilder(str);
      String[] arrayOfString1 = param1Parcel.readStringArray();
      String[] arrayOfString2 = param1Parcel.readStringArray();
      if (arrayOfString1 != null && arrayOfString2 != null)
        if (arrayOfString1.length != arrayOfString2.length) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("HtmlInfo attributes mismatch: names=");
          stringBuilder.append(arrayOfString1.length);
          stringBuilder.append(", values=");
          stringBuilder.append(arrayOfString2.length);
          Log.w("AssistStructure", stringBuilder.toString());
        } else {
          for (byte b = 0; b < arrayOfString1.length; b++)
            htmlInfoNodeBuilder.addAttribute(arrayOfString1[b], arrayOfString2[b]); 
        }  
      return htmlInfoNodeBuilder.build();
    }
    
    public AssistStructure.HtmlInfoNode[] newArray(int param1Int) {
      return new AssistStructure.HtmlInfoNode[param1Int];
    }
  }
  
  private static final class HtmlInfoNodeBuilder extends ViewStructure.HtmlInfo.Builder {
    private ArrayList<String> mNames;
    
    private final String mTag;
    
    private ArrayList<String> mValues;
    
    HtmlInfoNodeBuilder(String param1String) {
      this.mTag = param1String;
    }
    
    public ViewStructure.HtmlInfo.Builder addAttribute(String param1String1, String param1String2) {
      if (this.mNames == null) {
        this.mNames = new ArrayList<>();
        this.mValues = new ArrayList<>();
      } 
      this.mNames.add(param1String1);
      this.mValues.add(param1String2);
      return this;
    }
    
    public AssistStructure.HtmlInfoNode build() {
      return new AssistStructure.HtmlInfoNode(this);
    }
  }
  
  public void sanitizeForParceling(boolean paramBoolean) {
    this.mSanitizeOnWrite = paramBoolean;
  }
  
  public void dump(boolean paramBoolean) {
    if (this.mActivityComponent == null) {
      Log.i("AssistStructure", "dump(): calling ensureData() first");
      ensureData();
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Task id: ");
    stringBuilder2.append(this.mTaskId);
    Log.i("AssistStructure", stringBuilder2.toString());
    StringBuilder stringBuilder3 = new StringBuilder();
    stringBuilder3.append("Activity: ");
    ComponentName componentName = this.mActivityComponent;
    if (componentName != null) {
      String str1 = componentName.flattenToShortString();
    } else {
      componentName = null;
    } 
    stringBuilder3.append((String)componentName);
    String str = stringBuilder3.toString();
    Log.i("AssistStructure", str);
    StringBuilder stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Sanitize on write: ");
    stringBuilder1.append(this.mSanitizeOnWrite);
    Log.i("AssistStructure", stringBuilder1.toString());
    stringBuilder1 = new StringBuilder();
    stringBuilder1.append("Flags: ");
    stringBuilder1.append(this.mFlags);
    Log.i("AssistStructure", stringBuilder1.toString());
    int i = getWindowNodeCount();
    for (byte b = 0; b < i; b++) {
      WindowNode windowNode = getWindowNodeAt(b);
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append("Window #");
      stringBuilder3.append(b);
      stringBuilder3.append(" [");
      stringBuilder3.append(windowNode.getLeft());
      stringBuilder3.append(",");
      stringBuilder3.append(windowNode.getTop());
      stringBuilder3.append(" ");
      stringBuilder3.append(windowNode.getWidth());
      stringBuilder3.append("x");
      stringBuilder3.append(windowNode.getHeight());
      stringBuilder3.append("] ");
      stringBuilder3.append(windowNode.getTitle());
      String str1 = stringBuilder3.toString();
      Log.i("AssistStructure", str1);
      dump("  ", windowNode.getRootViewNode(), paramBoolean);
    } 
  }
  
  void dump(String paramString, ViewNode paramViewNode, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append("View [");
    stringBuilder.append(paramViewNode.getLeft());
    stringBuilder.append(",");
    stringBuilder.append(paramViewNode.getTop());
    stringBuilder.append(" ");
    stringBuilder.append(paramViewNode.getWidth());
    stringBuilder.append("x");
    stringBuilder.append(paramViewNode.getHeight());
    stringBuilder.append("] ");
    stringBuilder.append(paramViewNode.getClassName());
    String str1 = stringBuilder.toString();
    Log.i("AssistStructure", str1);
    int i = paramViewNode.getId();
    if (i != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  ID: #");
      stringBuilder1.append(Integer.toHexString(i));
      String str = paramViewNode.getIdEntry();
      if (str != null) {
        str1 = paramViewNode.getIdType();
        String str3 = paramViewNode.getIdPackage();
        stringBuilder1.append(" ");
        stringBuilder1.append(str3);
        stringBuilder1.append(":");
        stringBuilder1.append(str1);
        stringBuilder1.append("/");
        stringBuilder1.append(str);
      } 
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    i = paramViewNode.getScrollX();
    int j = paramViewNode.getScrollY();
    if (i != 0 || j != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Scroll: ");
      stringBuilder1.append(i);
      stringBuilder1.append(",");
      stringBuilder1.append(j);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    Matrix matrix = paramViewNode.getTransformation();
    if (matrix != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Transformation: ");
      stringBuilder1.append(matrix);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    float f1 = paramViewNode.getElevation();
    if (f1 != 0.0F) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Elevation: ");
      stringBuilder1.append(f1);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    float f2 = paramViewNode.getAlpha();
    if (f2 != 0.0F) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Alpha: ");
      stringBuilder1.append(f1);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    CharSequence charSequence = paramViewNode.getContentDescription();
    if (charSequence != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Content description: ");
      stringBuilder1.append(charSequence);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    charSequence = paramViewNode.getText();
    if (charSequence != null) {
      if (paramViewNode.isSanitized() || paramBoolean) {
        charSequence = charSequence.toString();
      } else {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("REDACTED[");
        stringBuilder2.append(charSequence.length());
        stringBuilder2.append(" chars]");
        charSequence = stringBuilder2.toString();
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Text (sel ");
      stringBuilder1.append(paramViewNode.getTextSelectionStart());
      stringBuilder1.append("-");
      stringBuilder1.append(paramViewNode.getTextSelectionEnd());
      stringBuilder1.append("): ");
      stringBuilder1.append((String)charSequence);
      charSequence = stringBuilder1.toString();
      Log.i("AssistStructure", (String)charSequence);
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Text size: ");
      charSequence.append(paramViewNode.getTextSize());
      charSequence.append(" , style: #");
      charSequence.append(paramViewNode.getTextStyle());
      charSequence = charSequence.toString();
      Log.i("AssistStructure", (String)charSequence);
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Text color fg: #");
      charSequence.append(Integer.toHexString(paramViewNode.getTextColor()));
      charSequence.append(", bg: #");
      charSequence.append(Integer.toHexString(paramViewNode.getTextBackgroundColor()));
      charSequence = charSequence.toString();
      Log.i("AssistStructure", (String)charSequence);
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Input type: ");
      charSequence.append(paramViewNode.getInputType());
      Log.i("AssistStructure", charSequence.toString());
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Resource id: ");
      charSequence.append(paramViewNode.getTextIdEntry());
      Log.i("AssistStructure", charSequence.toString());
    } 
    charSequence = paramViewNode.getWebDomain();
    if (charSequence != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Web domain: ");
      stringBuilder1.append((String)charSequence);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    ViewStructure.HtmlInfo htmlInfo = paramViewNode.getHtmlInfo();
    if (htmlInfo != null) {
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  HtmlInfo: tag=");
      charSequence.append(htmlInfo.getTag());
      charSequence.append(", attr=");
      charSequence.append(htmlInfo.getAttributes());
      charSequence = charSequence.toString();
      Log.i("AssistStructure", (String)charSequence);
    } 
    LocaleList localeList = paramViewNode.getLocaleList();
    if (localeList != null) {
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  LocaleList: ");
      charSequence.append(localeList);
      Log.i("AssistStructure", charSequence.toString());
    } 
    String str2 = paramViewNode.getHint();
    if (str2 != null) {
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Hint: ");
      charSequence.append(str2);
      Log.i("AssistStructure", charSequence.toString());
      charSequence = new StringBuilder();
      charSequence.append(paramString);
      charSequence.append("  Resource id: ");
      charSequence.append(paramViewNode.getHintIdEntry());
      Log.i("AssistStructure", charSequence.toString());
    } 
    Bundle bundle = paramViewNode.getExtras();
    if (bundle != null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Extras: ");
      stringBuilder1.append(bundle);
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    if (paramViewNode.isAssistBlocked()) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  BLOCKED");
      Log.i("AssistStructure", stringBuilder1.toString());
    } 
    AutofillId autofillId = paramViewNode.getAutofillId();
    if (autofillId == null) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append(" NO autofill ID");
      Log.i("AssistStructure", stringBuilder1.toString());
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Autofill info: id= ");
      stringBuilder1.append(autofillId);
      stringBuilder1.append(", type=");
      stringBuilder1.append(paramViewNode.getAutofillType());
      stringBuilder1.append(", options=");
      stringBuilder1.append(Arrays.toString((Object[])paramViewNode.getAutofillOptions()));
      stringBuilder1.append(", hints=");
      stringBuilder1.append(Arrays.toString((Object[])paramViewNode.getAutofillHints()));
      stringBuilder1.append(", value=");
      stringBuilder1.append(paramViewNode.getAutofillValue());
      stringBuilder1.append(", sanitized=");
      stringBuilder1.append(paramViewNode.isSanitized());
      stringBuilder1.append(", important=");
      stringBuilder1.append(paramViewNode.getImportantForAutofill());
      String str = stringBuilder1.toString();
      Log.i("AssistStructure", str);
    } 
    i = paramViewNode.getChildCount();
    if (i > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("  Children:");
      Log.i("AssistStructure", stringBuilder1.toString());
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramString);
      stringBuilder1.append("    ");
      String str = stringBuilder1.toString();
      for (j = 0; j < i; j++) {
        ViewNode viewNode = paramViewNode.getChildAt(j);
        dump(str, viewNode, paramBoolean);
      } 
    } 
  }
  
  public void setTaskId(int paramInt) {
    this.mTaskId = paramInt;
  }
  
  public int getTaskId() {
    return this.mTaskId;
  }
  
  public void setActivityComponent(ComponentName paramComponentName) {
    this.mActivityComponent = paramComponentName;
  }
  
  public ComponentName getActivityComponent() {
    return this.mActivityComponent;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public boolean isHomeActivity() {
    return this.mIsHomeActivity;
  }
  
  public int getWindowNodeCount() {
    ensureData();
    return this.mWindowNodes.size();
  }
  
  public WindowNode getWindowNodeAt(int paramInt) {
    ensureData();
    return this.mWindowNodes.get(paramInt);
  }
  
  public void ensureDataForAutofill() {
    if (this.mHaveData)
      return; 
    this.mHaveData = true;
    Binder.allowBlocking(this.mReceiveChannel);
    try {
      ParcelTransferReader parcelTransferReader = new ParcelTransferReader();
      this(this, this.mReceiveChannel);
      parcelTransferReader.go();
      return;
    } finally {
      Binder.defaultBlocking(this.mReceiveChannel);
    } 
  }
  
  public void ensureData() {
    if (this.mHaveData)
      return; 
    this.mHaveData = true;
    ParcelTransferReader parcelTransferReader = new ParcelTransferReader(this.mReceiveChannel);
    parcelTransferReader.go();
  }
  
  boolean waitForReady() {
    // Byte code:
    //   0: iconst_0
    //   1: istore_1
    //   2: aload_0
    //   3: monitorenter
    //   4: invokestatic uptimeMillis : ()J
    //   7: ldc2_w 5000
    //   10: ladd
    //   11: lstore_2
    //   12: aload_0
    //   13: getfield mPendingAsyncChildren : Ljava/util/ArrayList;
    //   16: invokevirtual size : ()I
    //   19: ifle -> 50
    //   22: invokestatic uptimeMillis : ()J
    //   25: lstore #4
    //   27: lload #4
    //   29: lload_2
    //   30: lcmp
    //   31: ifge -> 50
    //   34: aload_0
    //   35: lload_2
    //   36: lload #4
    //   38: lsub
    //   39: invokevirtual wait : (J)V
    //   42: goto -> 12
    //   45: astore #6
    //   47: goto -> 42
    //   50: aload_0
    //   51: getfield mPendingAsyncChildren : Ljava/util/ArrayList;
    //   54: invokevirtual size : ()I
    //   57: ifle -> 122
    //   60: new java/lang/StringBuilder
    //   63: astore #6
    //   65: aload #6
    //   67: invokespecial <init> : ()V
    //   70: aload #6
    //   72: ldc_w 'Skipping assist structure, waiting too long for async children (have '
    //   75: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload_0
    //   80: getfield mPendingAsyncChildren : Ljava/util/ArrayList;
    //   83: astore #7
    //   85: aload #6
    //   87: aload #7
    //   89: invokevirtual size : ()I
    //   92: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   95: pop
    //   96: aload #6
    //   98: ldc_w ' remaining'
    //   101: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   104: pop
    //   105: aload #6
    //   107: invokevirtual toString : ()Ljava/lang/String;
    //   110: astore #6
    //   112: ldc 'AssistStructure'
    //   114: aload #6
    //   116: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   119: pop
    //   120: iconst_1
    //   121: istore_1
    //   122: aload_0
    //   123: monitorexit
    //   124: iload_1
    //   125: iconst_1
    //   126: ixor
    //   127: ireturn
    //   128: astore #6
    //   130: aload_0
    //   131: monitorexit
    //   132: aload #6
    //   134: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2445	-> 0
    //   #2446	-> 2
    //   #2447	-> 4
    //   #2449	-> 12
    //   #2451	-> 34
    //   #2453	-> 42
    //   #2452	-> 45
    //   #2455	-> 50
    //   #2457	-> 60
    //   #2458	-> 85
    //   #2457	-> 112
    //   #2459	-> 120
    //   #2461	-> 122
    //   #2462	-> 124
    //   #2461	-> 128
    // Exception table:
    //   from	to	target	type
    //   4	12	128	finally
    //   12	27	128	finally
    //   34	42	45	java/lang/InterruptedException
    //   34	42	128	finally
    //   50	60	128	finally
    //   60	85	128	finally
    //   85	112	128	finally
    //   112	120	128	finally
    //   122	124	128	finally
    //   130	132	128	finally
  }
  
  public void clearSendChannel() {
    SendChannel sendChannel = this.mSendChannel;
    if (sendChannel != null)
      sendChannel.mAssistStructure = null; 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mTaskId);
    ComponentName.writeToParcel(this.mActivityComponent, paramParcel);
    paramParcel.writeInt(this.mIsHomeActivity);
    if (this.mHaveData) {
      if (this.mSendChannel == null)
        this.mSendChannel = new SendChannel(this); 
      paramParcel.writeStrongBinder((IBinder)this.mSendChannel);
    } else {
      paramParcel.writeStrongBinder(this.mReceiveChannel);
    } 
  }
  
  public static final Parcelable.Creator<AssistStructure> CREATOR = new Parcelable.Creator<AssistStructure>() {
      public AssistStructure createFromParcel(Parcel param1Parcel) {
        return new AssistStructure(param1Parcel);
      }
      
      public AssistStructure[] newArray(int param1Int) {
        return new AssistStructure[param1Int];
      }
    };
  
  private static final boolean DEBUG_PARCEL = false;
  
  private static final boolean DEBUG_PARCEL_CHILDREN = false;
  
  private static final boolean DEBUG_PARCEL_TREE = false;
  
  private static final String DESCRIPTOR = "android.app.AssistStructure";
  
  private static final String TAG = "AssistStructure";
  
  private static final int TRANSACTION_XFER = 2;
  
  private static final int VALIDATE_VIEW_TOKEN = 572662306;
  
  private static final int VALIDATE_WINDOW_TOKEN = 286331153;
  
  private long mAcquisitionEndTime;
  
  private long mAcquisitionStartTime;
  
  private ComponentName mActivityComponent;
  
  private int mAutofillFlags;
  
  private int mFlags;
  
  private boolean mHaveData;
  
  private boolean mIsHomeActivity;
  
  private IBinder mReceiveChannel;
  
  private boolean mSanitizeOnWrite;
  
  private SendChannel mSendChannel;
  
  private int mTaskId;
}
