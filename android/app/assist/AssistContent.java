package android.app.assist;

import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;

public class AssistContent implements Parcelable {
  public AssistContent() {
    this.mIsAppProvidedIntent = false;
    this.mIsAppProvidedWebUri = false;
    this.mExtras = new Bundle();
  }
  
  public void setDefaultIntent(Intent paramIntent) {
    this.mIntent = paramIntent;
    this.mIsAppProvidedIntent = false;
    this.mIsAppProvidedWebUri = false;
    this.mUri = null;
    if (paramIntent != null && "android.intent.action.VIEW".equals(paramIntent.getAction())) {
      Uri uri = paramIntent.getData();
      if (uri != null && (
        "http".equals(uri.getScheme()) || "https".equals(uri.getScheme())))
        this.mUri = uri; 
    } 
  }
  
  public void setIntent(Intent paramIntent) {
    this.mIsAppProvidedIntent = true;
    this.mIntent = paramIntent;
  }
  
  public Intent getIntent() {
    return this.mIntent;
  }
  
  public boolean isAppProvidedIntent() {
    return this.mIsAppProvidedIntent;
  }
  
  public void setClipData(ClipData paramClipData) {
    this.mClipData = paramClipData;
  }
  
  public ClipData getClipData() {
    return this.mClipData;
  }
  
  public void setStructuredData(String paramString) {
    this.mStructuredData = paramString;
  }
  
  public String getStructuredData() {
    return this.mStructuredData;
  }
  
  public void setWebUri(Uri paramUri) {
    this.mIsAppProvidedWebUri = true;
    this.mUri = paramUri;
  }
  
  public Uri getWebUri() {
    return this.mUri;
  }
  
  public boolean isAppProvidedWebUri() {
    return this.mIsAppProvidedWebUri;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  AssistContent(Parcel paramParcel) {
    boolean bool1 = false;
    this.mIsAppProvidedIntent = false;
    this.mIsAppProvidedWebUri = false;
    if (paramParcel.readInt() != 0)
      this.mIntent = (Intent)Intent.CREATOR.createFromParcel(paramParcel); 
    if (paramParcel.readInt() != 0)
      this.mClipData = (ClipData)ClipData.CREATOR.createFromParcel(paramParcel); 
    if (paramParcel.readInt() != 0)
      this.mUri = (Uri)Uri.CREATOR.createFromParcel(paramParcel); 
    if (paramParcel.readInt() != 0)
      this.mStructuredData = paramParcel.readString(); 
    if (paramParcel.readInt() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsAppProvidedIntent = bool2;
    this.mExtras = paramParcel.readBundle();
    boolean bool2 = bool1;
    if (paramParcel.readInt() == 1)
      bool2 = true; 
    this.mIsAppProvidedWebUri = bool2;
  }
  
  void writeToParcelInternal(Parcel paramParcel, int paramInt) {
    if (this.mIntent != null) {
      paramParcel.writeInt(1);
      this.mIntent.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mClipData != null) {
      paramParcel.writeInt(1);
      this.mClipData.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mUri != null) {
      paramParcel.writeInt(1);
      this.mUri.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mStructuredData != null) {
      paramParcel.writeInt(1);
      paramParcel.writeString(this.mStructuredData);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.mIsAppProvidedIntent);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeInt(this.mIsAppProvidedWebUri);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    writeToParcelInternal(paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<AssistContent> CREATOR = new Parcelable.Creator<AssistContent>() {
      public AssistContent createFromParcel(Parcel param1Parcel) {
        return new AssistContent(param1Parcel);
      }
      
      public AssistContent[] newArray(int param1Int) {
        return new AssistContent[param1Int];
      }
    };
  
  private ClipData mClipData;
  
  private final Bundle mExtras;
  
  private Intent mIntent;
  
  private boolean mIsAppProvidedIntent;
  
  private boolean mIsAppProvidedWebUri;
  
  private String mStructuredData;
  
  private Uri mUri;
}
