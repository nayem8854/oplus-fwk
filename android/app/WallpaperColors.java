package android.app;

import android.annotation.SystemApi;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Size;
import com.android.internal.graphics.ColorUtils;
import com.android.internal.graphics.palette.Palette;
import com.android.internal.graphics.palette.Quantizer;
import com.android.internal.graphics.palette.VariationalKMeansQuantizer;
import com.android.internal.util.ContrastColorUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class WallpaperColors implements Parcelable {
  private static final float BRIGHT_IMAGE_MEAN_LUMINANCE = 0.75F;
  
  public WallpaperColors(Parcel paramParcel) {
    this.mMainColors = new ArrayList<>();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      int j = paramParcel.readInt();
      Color color = Color.valueOf(j);
      this.mMainColors.add(color);
    } 
    this.mColorHints = paramParcel.readInt();
  }
  
  public static WallpaperColors fromDrawable(Drawable paramDrawable) {
    Rect rect;
    if (paramDrawable != null) {
      rect = paramDrawable.copyBounds();
      int i = paramDrawable.getIntrinsicWidth();
      int j = paramDrawable.getIntrinsicHeight();
      if (i > 0) {
        int k = j;
        if (j <= 0) {
          i = 112;
          k = 112;
          Size size2 = calculateOptimalSize(i, k);
          Bitmap bitmap2 = Bitmap.createBitmap(size2.getWidth(), size2.getHeight(), Bitmap.Config.ARGB_8888);
          Canvas canvas2 = new Canvas(bitmap2);
          paramDrawable.setBounds(0, 0, bitmap2.getWidth(), bitmap2.getHeight());
          paramDrawable.draw(canvas2);
          WallpaperColors wallpaperColors2 = fromBitmap(bitmap2);
          bitmap2.recycle();
          paramDrawable.setBounds(rect);
          return wallpaperColors2;
        } 
        Size size1 = calculateOptimalSize(i, k);
        Bitmap bitmap1 = Bitmap.createBitmap(size1.getWidth(), size1.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas1 = new Canvas(bitmap1);
        paramDrawable.setBounds(0, 0, bitmap1.getWidth(), bitmap1.getHeight());
        paramDrawable.draw(canvas1);
        WallpaperColors wallpaperColors1 = fromBitmap(bitmap1);
        bitmap1.recycle();
        paramDrawable.setBounds(rect);
        return wallpaperColors1;
      } 
    } else {
      throw new IllegalArgumentException("Drawable cannot be null");
    } 
    byte b1 = 112;
    byte b2 = 112;
    Size size = calculateOptimalSize(b1, b2);
    Bitmap bitmap = Bitmap.createBitmap(size.getWidth(), size.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas canvas = new Canvas(bitmap);
    paramDrawable.setBounds(0, 0, bitmap.getWidth(), bitmap.getHeight());
    paramDrawable.draw(canvas);
    WallpaperColors wallpaperColors = fromBitmap(bitmap);
    bitmap.recycle();
    paramDrawable.setBounds(rect);
    return wallpaperColors;
  }
  
  public static WallpaperColors fromBitmap(Bitmap paramBitmap) {
    if (paramBitmap != null) {
      Color color1;
      int i = paramBitmap.getWidth(), j = paramBitmap.getHeight();
      boolean bool = false;
      Bitmap bitmap = paramBitmap;
      if (i * j > 12544) {
        bool = true;
        Size size = calculateOptimalSize(paramBitmap.getWidth(), paramBitmap.getHeight());
        j = size.getWidth();
        i = size.getHeight();
        bitmap = Bitmap.createScaledBitmap(paramBitmap, j, i, true);
      } 
      Palette.Builder builder = Palette.from(bitmap);
      VariationalKMeansQuantizer variationalKMeansQuantizer = new VariationalKMeansQuantizer();
      builder = builder.setQuantizer((Quantizer)variationalKMeansQuantizer);
      builder = builder.maximumColorCount(5);
      builder = builder.clearFilters();
      builder = builder.resizeBitmapArea(12544);
      Palette palette = builder.generate();
      ArrayList<Palette.Swatch> arrayList = new ArrayList(palette.getSwatches());
      float f = (bitmap.getWidth() * bitmap.getHeight());
      arrayList.removeIf(new _$$Lambda$WallpaperColors$8R5kfKKLfHjpw_QXmD1mWOKwJxc(f * 0.05F));
      arrayList.sort((Comparator)_$$Lambda$WallpaperColors$MQFGJ9EZ9CDeGbIhMufJKqru3IE.INSTANCE);
      i = arrayList.size();
      Color color2 = null, color3 = null;
      variationalKMeansQuantizer = null;
      for (j = 0; j < i; j++) {
        Color color = Color.valueOf(((Palette.Swatch)arrayList.get(j)).getRgb());
        if (j != 0) {
          if (j != 1) {
            if (j != 2)
              break; 
            color1 = color;
          } else {
            color3 = color;
          } 
        } else {
          color2 = color;
        } 
      } 
      j = calculateDarkHints(bitmap);
      if (bool)
        bitmap.recycle(); 
      return new WallpaperColors(color2, color3, color1, j | 0x4);
    } 
    throw new IllegalArgumentException("Bitmap can't be null");
  }
  
  public WallpaperColors(Color paramColor1, Color paramColor2, Color paramColor3) {
    this(paramColor1, paramColor2, paramColor3, 0);
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramColor1.toArgb(), arrayOfFloat);
    float f = arrayOfFloat[2];
    if (f < 0.25F)
      this.mColorHints = 0x2 | this.mColorHints; 
  }
  
  @SystemApi
  public WallpaperColors(Color paramColor1, Color paramColor2, Color paramColor3, int paramInt) {
    if (paramColor1 != null) {
      ArrayList<Color> arrayList = new ArrayList(3);
      arrayList.add(paramColor1);
      if (paramColor2 != null)
        this.mMainColors.add(paramColor2); 
      if (paramColor3 != null)
        if (paramColor2 != null) {
          this.mMainColors.add(paramColor3);
        } else {
          throw new IllegalArgumentException("tertiaryColor can't be specified when secondaryColor is null");
        }  
      this.mColorHints = paramInt;
      return;
    } 
    throw new IllegalArgumentException("Primary color should never be null.");
  }
  
  public static final Parcelable.Creator<WallpaperColors> CREATOR = new Parcelable.Creator<WallpaperColors>() {
      public WallpaperColors createFromParcel(Parcel param1Parcel) {
        return new WallpaperColors(param1Parcel);
      }
      
      public WallpaperColors[] newArray(int param1Int) {
        return new WallpaperColors[param1Int];
      }
    };
  
  private static final float DARK_PIXEL_CONTRAST = 6.0F;
  
  private static final float DARK_THEME_MEAN_LUMINANCE = 0.25F;
  
  private static final boolean DEBUG_DARK_PIXELS = false;
  
  public static final int HINT_FROM_BITMAP = 4;
  
  @SystemApi
  public static final int HINT_SUPPORTS_DARK_TEXT = 1;
  
  @SystemApi
  public static final int HINT_SUPPORTS_DARK_THEME = 2;
  
  private static final int MAX_BITMAP_SIZE = 112;
  
  private static final float MAX_DARK_AREA = 0.025F;
  
  private static final int MAX_WALLPAPER_EXTRACTION_AREA = 12544;
  
  private static final float MIN_COLOR_OCCURRENCE = 0.05F;
  
  private int mColorHints;
  
  private final ArrayList<Color> mMainColors;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    List<Color> list = getMainColors();
    int i = list.size();
    paramParcel.writeInt(i);
    for (paramInt = 0; paramInt < i; paramInt++) {
      Color color = list.get(paramInt);
      paramParcel.writeInt(color.toArgb());
    } 
    paramParcel.writeInt(this.mColorHints);
  }
  
  public Color getPrimaryColor() {
    return this.mMainColors.get(0);
  }
  
  public Color getSecondaryColor() {
    Color color;
    if (this.mMainColors.size() < 2) {
      color = null;
    } else {
      color = this.mMainColors.get(1);
    } 
    return color;
  }
  
  public Color getTertiaryColor() {
    Color color;
    if (this.mMainColors.size() < 3) {
      color = null;
    } else {
      color = this.mMainColors.get(2);
    } 
    return color;
  }
  
  public List<Color> getMainColors() {
    return Collections.unmodifiableList(this.mMainColors);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (this.mMainColors.equals(((WallpaperColors)paramObject).mMainColors)) {
      bool2 = bool1;
      if (this.mColorHints == ((WallpaperColors)paramObject).mColorHints)
        bool2 = true; 
    } 
    return bool2;
  }
  
  public int hashCode() {
    return this.mMainColors.hashCode() * 31 + this.mColorHints;
  }
  
  @SystemApi
  public int getColorHints() {
    return this.mColorHints;
  }
  
  public void setColorHints(int paramInt) {
    this.mColorHints = paramInt;
  }
  
  private static int calculateDarkHints(Bitmap paramBitmap) {
    if (paramBitmap == null)
      return 0; 
    int[] arrayOfInt = new int[paramBitmap.getWidth() * paramBitmap.getHeight()];
    double d = 0.0D;
    int i = (int)(arrayOfInt.length * 0.025F);
    int j = 0;
    int k = paramBitmap.getWidth();
    int m = paramBitmap.getWidth(), n = paramBitmap.getHeight();
    paramBitmap.getPixels(arrayOfInt, 0, k, 0, 0, m, n);
    float[] arrayOfFloat = new float[3];
    for (n = 0; n < arrayOfInt.length; n++, j = k) {
      ColorUtils.colorToHSL(arrayOfInt[n], arrayOfFloat);
      float f = arrayOfFloat[2];
      int i1 = Color.alpha(arrayOfInt[n]);
      k = arrayOfInt[n];
      if (ContrastColorUtil.calculateContrast(k, -16777216) > 6.0D) {
        m = 1;
      } else {
        m = 0;
      } 
      k = j;
      if (m == 0) {
        k = j;
        if (i1 != 0)
          k = j + 1; 
      } 
      d += f;
    } 
    k = 0;
    d /= arrayOfInt.length;
    n = k;
    if (d > 0.75D) {
      n = k;
      if (j < i)
        n = false | true; 
    } 
    j = n;
    if (d < 0.25D)
      j = n | 0x2; 
    return j;
  }
  
  private static Size calculateOptimalSize(int paramInt1, int paramInt2) {
    int i = paramInt1 * paramInt2;
    double d = 1.0D;
    if (i > 12544)
      d = Math.sqrt(12544.0D / i); 
    int j = (int)(paramInt1 * d);
    i = (int)(paramInt2 * d);
    paramInt1 = j;
    if (j == 0)
      paramInt1 = 1; 
    paramInt2 = i;
    if (i == 0)
      paramInt2 = 1; 
    return new Size(paramInt1, paramInt2);
  }
  
  public String toString() {
    StringBuilder stringBuilder1 = new StringBuilder();
    for (byte b = 0; b < this.mMainColors.size(); b++) {
      stringBuilder1.append(Integer.toHexString(((Color)this.mMainColors.get(b)).toArgb()));
      stringBuilder1.append(" ");
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("[WallpaperColors: ");
    stringBuilder2.append(stringBuilder1.toString());
    stringBuilder2.append("h: ");
    stringBuilder2.append(this.mColorHints);
    stringBuilder2.append("]");
    return stringBuilder2.toString();
  }
}
