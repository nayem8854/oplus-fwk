package android.app;

import oplus.app.OplusCommonManager;

public abstract class OplusBaseActivityManager extends OplusCommonManager {
  public OplusBaseActivityManager() {
    super("activity");
  }
}
