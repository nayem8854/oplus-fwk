package android.app;

import android.accounts.AccountManager;
import android.annotation.SystemApi;
import android.app.admin.DevicePolicyManager;
import android.app.blob.BlobStoreManagerFrameworkInitializer;
import android.app.contentsuggestions.ContentSuggestionsManager;
import android.app.job.JobSchedulerFrameworkInitializer;
import android.app.prediction.AppPredictionManager;
import android.app.role.RoleControllerManager;
import android.app.role.RoleManager;
import android.app.slice.SliceManager;
import android.app.timedetector.TimeDetector;
import android.app.timezone.RulesManager;
import android.app.timezonedetector.TimeZoneDetector;
import android.app.trust.TrustManager;
import android.app.usage.NetworkStatsManager;
import android.app.usage.StorageStatsManager;
import android.app.usage.UsageStatsManager;
import android.appwidget.AppWidgetManager;
import android.bluetooth.BluetoothManager;
import android.companion.CompanionDeviceManager;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.RestrictionsManager;
import android.content.integrity.AppIntegrityManager;
import android.content.om.OverlayManager;
import android.content.pm.CrossProfileApps;
import android.content.pm.DataLoaderManager;
import android.content.pm.LauncherApps;
import android.content.pm.ShortcutManager;
import android.content.rollback.RollbackManager;
import android.debug.AdbManager;
import android.hardware.ConsumerIrManager;
import android.hardware.SensorManager;
import android.hardware.SensorPrivacyManager;
import android.hardware.SerialManager;
import android.hardware.alipay.AlipayManager;
import android.hardware.biometrics.BiometricManager;
import android.hardware.camera2.CameraManager;
import android.hardware.display.ColorDisplayManager;
import android.hardware.display.DisplayManager;
import android.hardware.face.FaceManager;
import android.hardware.fingerprint.FingerprintManager;
import android.hardware.hdmi.HdmiControlManager;
import android.hardware.input.InputManager;
import android.hardware.iris.IrisManager;
import android.hardware.lights.LightsManager;
import android.hardware.location.ContextHubManager;
import android.hardware.radio.RadioManager;
import android.hardware.usb.UsbManager;
import android.location.CountryDetector;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRouter;
import android.media.midi.MidiManager;
import android.media.projection.MediaProjectionManager;
import android.media.session.MediaSessionManager;
import android.media.soundtrigger.SoundTriggerManager;
import android.media.tv.TvInputManager;
import android.media.tv.tunerresourcemanager.TunerResourceManager;
import android.net.ConnectivityDiagnosticsManager;
import android.net.ConnectivityManager;
import android.net.EthernetManager;
import android.net.IpSecManager;
import android.net.NetworkPolicyManager;
import android.net.NetworkScoreManager;
import android.net.NetworkWatchlistManager;
import android.net.OplusNetworkingControlManager;
import android.net.TestNetworkManager;
import android.net.TetheringManager;
import android.net.VpnManager;
import android.net.lowpan.LowpanManager;
import android.net.nsd.NsdManager;
import android.net.wifi.WifiFrameworkInitializer;
import android.net.wifi.nl80211.WifiNl80211Manager;
import android.nfc.NfcManager;
import android.os.BatteryManager;
import android.os.BatteryStatsManager;
import android.os.BugreportManager;
import android.os.DropBoxManager;
import android.os.HardwarePropertiesManager;
import android.os.IBinder;
import android.os.IncidentManager;
import android.os.MotorManager;
import android.os.PowerManager;
import android.os.Process;
import android.os.RecoverySystem;
import android.os.ServiceManager;
import android.os.StatsFrameworkInitializer;
import android.os.SystemConfigManager;
import android.os.SystemUpdateManager;
import android.os.UserManager;
import android.os.Vibrator;
import android.os.health.SystemHealthManager;
import android.os.image.DynamicSystemManager;
import android.os.incremental.IncrementalManager;
import android.os.storage.StorageManager;
import android.permission.PermissionControllerManager;
import android.permission.PermissionManager;
import android.print.PrintManager;
import android.security.FileIntegrityManager;
import android.service.oemlock.OemLockManager;
import android.service.persistentdata.PersistentDataBlockManager;
import android.telecom.TelecomManager;
import android.telephony.MmsManager;
import android.telephony.TelephonyFrameworkInitializer;
import android.telephony.TelephonyRegistryManager;
import android.text.ClipboardManager;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Slog;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.CaptioningManager;
import android.view.autofill.AutofillManager;
import android.view.contentcapture.ContentCaptureManager;
import android.view.inputmethod.InputMethodManager;
import android.view.textclassifier.TextClassificationManager;
import android.view.textservice.TextServicesManager;
import com.android.internal.util.Preconditions;
import com.oplus.felica.FelicadeviceManager;
import com.oplus.filter.DynamicFilterManager;
import com.oplus.network.OplusDataLimitManager;
import com.oplus.network.OplusNetworkStackManager;
import com.oplus.os.LinearmotorVibrator;
import com.oplus.screenshot.OplusScreenshotManager;
import java.io.FileInputStream;
import java.util.Map;
import java.util.Objects;

@SystemApi
public final class SystemServiceRegistry {
  private static final String STORAGEHEALTHINFO_SERVICE = "storage_healthinfo";
  
  private static final Map<String, String> SYSTEM_SERVICE_CLASS_NAMES;
  
  private static final Map<String, ServiceFetcher<?>> SYSTEM_SERVICE_FETCHERS;
  
  private static final Map<Class<?>, String> SYSTEM_SERVICE_NAMES;
  
  private static final String TAG = "SystemServiceRegistry";
  
  public static boolean sEnableServiceNotFoundWtf = false;
  
  private static volatile boolean sInitializing;
  
  private static int sServiceCacheSize;
  
  static {
    SYSTEM_SERVICE_NAMES = (Map<Class<?>, String>)new ArrayMap();
    SYSTEM_SERVICE_FETCHERS = (Map<String, ServiceFetcher<?>>)new ArrayMap();
    SYSTEM_SERVICE_CLASS_NAMES = (Map<String, String>)new ArrayMap();
    registerService("accessibility", AccessibilityManager.class, (ServiceFetcher<AccessibilityManager>)new Object());
    registerService("captioning", CaptioningManager.class, (ServiceFetcher<CaptioningManager>)new Object());
    registerService("account", AccountManager.class, (ServiceFetcher<AccountManager>)new Object());
    registerService("activity", ActivityManager.class, (ServiceFetcher<ActivityManager>)new Object());
    registerService("activity_task", ActivityTaskManager.class, (ServiceFetcher<ActivityTaskManager>)new Object());
    registerService("uri_grants", UriGrantsManager.class, (ServiceFetcher<UriGrantsManager>)new Object());
    registerService("alarm", AlarmManager.class, (ServiceFetcher<AlarmManager>)new Object());
    registerService("audio", AudioManager.class, (ServiceFetcher<AudioManager>)new Object());
    registerService("media_router", MediaRouter.class, (ServiceFetcher<MediaRouter>)new Object());
    registerService("bluetooth", BluetoothManager.class, (ServiceFetcher<BluetoothManager>)new Object());
    registerService("hdmi_control", HdmiControlManager.class, (ServiceFetcher<HdmiControlManager>)new Object());
    registerService("textclassification", TextClassificationManager.class, (ServiceFetcher<TextClassificationManager>)new Object());
    registerService("clipboard", ClipboardManager.class, (ServiceFetcher<ClipboardManager>)new Object());
    SYSTEM_SERVICE_NAMES.put(ClipboardManager.class, "clipboard");
    registerService("connectivity", ConnectivityManager.class, (ServiceFetcher<ConnectivityManager>)new Object());
    registerService("netd", IBinder.class, (ServiceFetcher<IBinder>)new Object());
    registerService("tethering", TetheringManager.class, (ServiceFetcher<TetheringManager>)new Object());
    registerService("ipsec", IpSecManager.class, (ServiceFetcher<IpSecManager>)new Object());
    registerService("vpn_management", VpnManager.class, (ServiceFetcher<VpnManager>)new Object());
    registerService("connectivity_diagnostics", ConnectivityDiagnosticsManager.class, (ServiceFetcher<ConnectivityDiagnosticsManager>)new Object());
    registerService("test_network", TestNetworkManager.class, (ServiceFetcher<TestNetworkManager>)new Object());
    registerService("country_detector", CountryDetector.class, (ServiceFetcher<CountryDetector>)new Object());
    registerService("device_policy", DevicePolicyManager.class, (ServiceFetcher<DevicePolicyManager>)new Object());
    registerService("download", DownloadManager.class, (ServiceFetcher<DownloadManager>)new Object());
    registerService("batterymanager", BatteryManager.class, (ServiceFetcher<BatteryManager>)new Object());
    registerService("nfc", NfcManager.class, (ServiceFetcher<NfcManager>)new Object());
    registerService("dropbox", DropBoxManager.class, (ServiceFetcher<DropBoxManager>)new Object());
    registerService("input", InputManager.class, (ServiceFetcher<InputManager>)new Object());
    registerService("display", DisplayManager.class, (ServiceFetcher<DisplayManager>)new Object());
    registerService("color_display", ColorDisplayManager.class, (ServiceFetcher<ColorDisplayManager>)new Object());
    registerService("input_method", InputMethodManager.class, (ServiceFetcher<InputMethodManager>)new Object());
    registerService("textservices", TextServicesManager.class, (ServiceFetcher<TextServicesManager>)new Object());
    registerService("keyguard", KeyguardManager.class, (ServiceFetcher<KeyguardManager>)new Object());
    registerService("layout_inflater", LayoutInflater.class, (ServiceFetcher<LayoutInflater>)new Object());
    registerService("location", LocationManager.class, (ServiceFetcher<LocationManager>)new Object());
    registerService("netpolicy", NetworkPolicyManager.class, (ServiceFetcher<NetworkPolicyManager>)new Object());
    registerService("notification", NotificationManager.class, (ServiceFetcher<NotificationManager>)new Object());
    registerService("servicediscovery", NsdManager.class, (ServiceFetcher<NsdManager>)new Object());
    registerService("power", PowerManager.class, (ServiceFetcher<PowerManager>)new Object());
    registerService("recovery", RecoverySystem.class, (ServiceFetcher<RecoverySystem>)new Object());
    registerService("search", SearchManager.class, (ServiceFetcher<SearchManager>)new Object());
    registerService("sensor", SensorManager.class, (ServiceFetcher<SensorManager>)new Object());
    registerService("sensor_privacy", SensorPrivacyManager.class, (ServiceFetcher<SensorPrivacyManager>)new Object());
    registerService("statusbar", StatusBarManager.class, (ServiceFetcher<StatusBarManager>)new Object());
    registerService("storage", StorageManager.class, (ServiceFetcher<StorageManager>)new Object());
    registerService("storagestats", StorageStatsManager.class, (ServiceFetcher<StorageStatsManager>)new Object());
    registerService("system_update", SystemUpdateManager.class, (ServiceFetcher<SystemUpdateManager>)new Object());
    registerService("system_config", SystemConfigManager.class, (ServiceFetcher<SystemConfigManager>)new Object());
    registerService("telephony_registry", TelephonyRegistryManager.class, (ServiceFetcher<TelephonyRegistryManager>)new Object());
    registerService("telecom", TelecomManager.class, (ServiceFetcher<TelecomManager>)new Object());
    registerService("mms", MmsManager.class, (ServiceFetcher<MmsManager>)new Object());
    registerService("uimode", UiModeManager.class, (ServiceFetcher<UiModeManager>)new Object());
    registerService("usb", UsbManager.class, (ServiceFetcher<UsbManager>)new Object());
    registerService("adb", AdbManager.class, (ServiceFetcher<AdbManager>)new Object());
    registerService("serial", SerialManager.class, (ServiceFetcher<SerialManager>)new Object());
    registerService("vibrator", Vibrator.class, (ServiceFetcher<Vibrator>)new Object());
    registerService("wallpaper", WallpaperManager.class, (ServiceFetcher<WallpaperManager>)new Object());
    registerService("lowpan", LowpanManager.class, (ServiceFetcher<LowpanManager>)new Object());
    registerService("ethernet", EthernetManager.class, (ServiceFetcher<EthernetManager>)new Object());
    registerService("wifinl80211", WifiNl80211Manager.class, (ServiceFetcher<WifiNl80211Manager>)new Object());
    registerService("window", WindowManager.class, (ServiceFetcher<WindowManager>)new Object());
    registerService("user", UserManager.class, (ServiceFetcher<UserManager>)new Object());
    registerService("appops", AppOpsManager.class, (ServiceFetcher<AppOpsManager>)new Object());
    registerService("camera", CameraManager.class, (ServiceFetcher<CameraManager>)new Object());
    registerService("launcherapps", LauncherApps.class, (ServiceFetcher<LauncherApps>)new Object());
    registerService("restrictions", RestrictionsManager.class, (ServiceFetcher<RestrictionsManager>)new Object());
    registerService("print", PrintManager.class, (ServiceFetcher<PrintManager>)new Object());
    registerService("companiondevice", CompanionDeviceManager.class, (ServiceFetcher<CompanionDeviceManager>)new Object());
    registerService("consumer_ir", ConsumerIrManager.class, (ServiceFetcher<ConsumerIrManager>)new Object());
    registerService("media_session", MediaSessionManager.class, (ServiceFetcher<MediaSessionManager>)new Object());
    registerService("trust", TrustManager.class, (ServiceFetcher<TrustManager>)new Object());
    registerService("fingerprint", FingerprintManager.class, (ServiceFetcher<FingerprintManager>)new Object());
    registerService("motor", MotorManager.class, (ServiceFetcher<MotorManager>)new Object());
    registerService("face", FaceManager.class, (ServiceFetcher<FaceManager>)new Object());
    registerService("iris", IrisManager.class, (ServiceFetcher<IrisManager>)new Object());
    registerService("biometric", BiometricManager.class, (ServiceFetcher<BiometricManager>)new Object());
    registerService("alipay", AlipayManager.class, (ServiceFetcher<AlipayManager>)new Object());
    registerService("felicaser", FelicadeviceManager.class, (ServiceFetcher<FelicadeviceManager>)new Object());
    registerService("dynamic_filter", DynamicFilterManager.class, (ServiceFetcher<DynamicFilterManager>)new Object());
    registerService("tv_input", TvInputManager.class, (ServiceFetcher<TvInputManager>)new Object());
    registerService("tv_tuner_resource_mgr", TunerResourceManager.class, (ServiceFetcher<TunerResourceManager>)new Object());
    registerService("network_score", NetworkScoreManager.class, (ServiceFetcher<NetworkScoreManager>)new Object());
    registerService("usagestats", UsageStatsManager.class, (ServiceFetcher<UsageStatsManager>)new Object());
    registerService("netstats", NetworkStatsManager.class, (ServiceFetcher<NetworkStatsManager>)new Object());
    registerService("persistent_data_block", PersistentDataBlockManager.class, (ServiceFetcher<PersistentDataBlockManager>)new Object());
    registerService("oem_lock", OemLockManager.class, (ServiceFetcher<OemLockManager>)new Object());
    registerService("media_projection", MediaProjectionManager.class, (ServiceFetcher<MediaProjectionManager>)new Object());
    registerService("appwidget", AppWidgetManager.class, (ServiceFetcher<AppWidgetManager>)new Object());
    registerService("midi", MidiManager.class, (ServiceFetcher<MidiManager>)new Object());
    registerService("broadcastradio", RadioManager.class, (ServiceFetcher<RadioManager>)new Object());
    registerService("hardware_properties", HardwarePropertiesManager.class, (ServiceFetcher<HardwarePropertiesManager>)new Object());
    registerService("soundtrigger", SoundTriggerManager.class, (ServiceFetcher<SoundTriggerManager>)new Object());
    registerService("shortcut", ShortcutManager.class, (ServiceFetcher<ShortcutManager>)new Object());
    registerService("overlay", OverlayManager.class, (ServiceFetcher<OverlayManager>)new Object());
    registerService("network_watchlist", NetworkWatchlistManager.class, (ServiceFetcher<NetworkWatchlistManager>)new Object());
    registerService("systemhealth", SystemHealthManager.class, (ServiceFetcher<SystemHealthManager>)new Object());
    registerService("contexthub", ContextHubManager.class, (ServiceFetcher<ContextHubManager>)new Object());
    registerService("incident", IncidentManager.class, (ServiceFetcher<IncidentManager>)new Object());
    registerService("bugreport", BugreportManager.class, (ServiceFetcher<BugreportManager>)new Object());
    registerService("autofill", AutofillManager.class, (ServiceFetcher<AutofillManager>)new Object());
    registerService("content_capture", ContentCaptureManager.class, (ServiceFetcher<ContentCaptureManager>)new Object());
    registerService("app_prediction", AppPredictionManager.class, (ServiceFetcher<AppPredictionManager>)new Object());
    registerService("content_suggestions", ContentSuggestionsManager.class, (ServiceFetcher<ContentSuggestionsManager>)new Object());
    registerService("vrmanager", VrManager.class, (ServiceFetcher<VrManager>)new Object());
    registerService("timezone", RulesManager.class, (ServiceFetcher<RulesManager>)new Object());
    registerService("crossprofileapps", CrossProfileApps.class, (ServiceFetcher<CrossProfileApps>)new Object());
    registerService("slice", SliceManager.class, (ServiceFetcher<SliceManager>)new Object());
    registerService("color_screenshot", OplusScreenshotManager.class, (ServiceFetcher<OplusScreenshotManager>)new Object());
    registerService("time_detector", TimeDetector.class, (ServiceFetcher<TimeDetector>)new Object());
    registerService("time_zone_detector", TimeZoneDetector.class, (ServiceFetcher<TimeZoneDetector>)new Object());
    registerService("permission", PermissionManager.class, (ServiceFetcher<PermissionManager>)new Object());
    registerService("permission_controller", PermissionControllerManager.class, (ServiceFetcher<PermissionControllerManager>)new Object());
    registerService("role", RoleManager.class, (ServiceFetcher<RoleManager>)new Object());
    registerService("role_controller", RoleControllerManager.class, (ServiceFetcher<RoleControllerManager>)new Object());
    registerService("rollback", RollbackManager.class, (ServiceFetcher<RollbackManager>)new Object());
    registerService("dynamic_system", DynamicSystemManager.class, (ServiceFetcher<DynamicSystemManager>)new Object());
    registerService("batterystats", BatteryStatsManager.class, (ServiceFetcher<BatteryStatsManager>)new Object());
    registerService("dataloader_manager", DataLoaderManager.class, (ServiceFetcher<DataLoaderManager>)new Object());
    registerService("lights", LightsManager.class, (ServiceFetcher<LightsManager>)new Object());
    registerService("incremental", IncrementalManager.class, (ServiceFetcher<IncrementalManager>)new Object());
    registerService("file_integrity", FileIntegrityManager.class, (ServiceFetcher<FileIntegrityManager>)new Object());
    registerService("app_integrity", AppIntegrityManager.class, (ServiceFetcher<AppIntegrityManager>)new Object());
    registerService("dream", DreamManager.class, (ServiceFetcher<DreamManager>)new Object());
    registerService("networking_control", OplusNetworkingControlManager.class, (ServiceFetcher<OplusNetworkingControlManager>)new Object());
    registerService("oplusnetworkstack", OplusNetworkStackManager.class, (ServiceFetcher<OplusNetworkStackManager>)new Object());
    registerService("oplusdatalimit", OplusDataLimitManager.class, (ServiceFetcher<OplusDataLimitManager>)new Object());
    registerService("linearmotor", LinearmotorVibrator.class, (ServiceFetcher<LinearmotorVibrator>)new Object());
    sInitializing = true;
    try {
      JobSchedulerFrameworkInitializer.registerServiceWrappers();
      BlobStoreManagerFrameworkInitializer.initialize();
      TelephonyFrameworkInitializer.registerServiceWrappers();
      WifiFrameworkInitializer.registerServiceWrappers();
      StatsFrameworkInitializer.registerServiceWrappers();
      sInitializing = false;
      return;
    } finally {
      sInitializing = false;
    } 
  }
  
  private static void ensureInitializing(String paramString) {
    boolean bool = sInitializing;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Internal error: ");
    stringBuilder.append(paramString);
    stringBuilder.append(" can only be called during class initialization.");
    Preconditions.checkState(bool, stringBuilder.toString());
  }
  
  public static Object[] createServiceCache() {
    return new Object[sServiceCacheSize];
  }
  
  public static Object getSystemService(ContextImpl paramContextImpl, String paramString) {
    if (paramString == null)
      return null; 
    ServiceFetcher<StringBuilder> serviceFetcher = (ServiceFetcher)SYSTEM_SERVICE_FETCHERS.get(paramString);
    if (serviceFetcher == null) {
      if (sEnableServiceNotFoundWtf) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown manager requested: ");
        stringBuilder.append(paramString);
        Slog.wtf("SystemServiceRegistry", stringBuilder.toString());
      } 
      return null;
    } 
    StringBuilder stringBuilder = serviceFetcher.getService((ContextImpl)stringBuilder);
    if (sEnableServiceNotFoundWtf && stringBuilder == null) {
      byte b = -1;
      switch (paramString.hashCode()) {
        case 1085372378:
          if (paramString.equals("incremental"))
            b = 2; 
          break;
        case 974854528:
          if (paramString.equals("content_capture"))
            b = 0; 
          break;
        case -769002131:
          if (paramString.equals("app_prediction"))
            b = 1; 
          break;
        case -2016257936:
          if (paramString.equals("linearmotor"))
            b = 3; 
          break;
      } 
      if (b != 0 && b != 1 && b != 2 && b != 3) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Manager wrapper not available: ");
        stringBuilder.append(paramString);
        Slog.wtf("SystemServiceRegistry", stringBuilder.toString());
        return null;
      } 
      return null;
    } 
    return stringBuilder;
  }
  
  public static String getSystemServiceName(Class<?> paramClass) {
    if (paramClass == null)
      return null; 
    String str = SYSTEM_SERVICE_NAMES.get(paramClass);
    if (sEnableServiceNotFoundWtf && str == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown manager requested: ");
      stringBuilder.append(paramClass.getCanonicalName());
      Slog.wtf("SystemServiceRegistry", stringBuilder.toString());
    } 
    return str;
  }
  
  private static <T> void registerService(String paramString, Class<T> paramClass, ServiceFetcher<T> paramServiceFetcher) {
    SYSTEM_SERVICE_NAMES.put(paramClass, paramString);
    SYSTEM_SERVICE_FETCHERS.put(paramString, paramServiceFetcher);
    SYSTEM_SERVICE_CLASS_NAMES.put(paramString, paramClass.getSimpleName());
  }
  
  public static String getSystemServiceClassName(String paramString) {
    return SYSTEM_SERVICE_CLASS_NAMES.get(paramString);
  }
  
  @SystemApi
  public static <TServiceClass> void registerStaticService(String paramString, Class<TServiceClass> paramClass, StaticServiceProducerWithBinder<TServiceClass> paramStaticServiceProducerWithBinder) {
    ensureInitializing("registerStaticService");
    Preconditions.checkStringNotEmpty(paramString);
    Objects.requireNonNull(paramClass);
    Objects.requireNonNull(paramStaticServiceProducerWithBinder);
    registerService(paramString, paramClass, (ServiceFetcher<TServiceClass>)new Object(paramStaticServiceProducerWithBinder, paramString));
  }
  
  @SystemApi
  public static <TServiceClass> void registerStaticService(String paramString, Class<TServiceClass> paramClass, StaticServiceProducerWithoutBinder<TServiceClass> paramStaticServiceProducerWithoutBinder) {
    ensureInitializing("registerStaticService");
    Preconditions.checkStringNotEmpty(paramString);
    Objects.requireNonNull(paramClass);
    Objects.requireNonNull(paramStaticServiceProducerWithoutBinder);
    registerService(paramString, paramClass, (ServiceFetcher<TServiceClass>)new Object(paramStaticServiceProducerWithoutBinder));
  }
  
  @SystemApi
  public static <TServiceClass> void registerContextAwareService(String paramString, Class<TServiceClass> paramClass, ContextAwareServiceProducerWithBinder<TServiceClass> paramContextAwareServiceProducerWithBinder) {
    ensureInitializing("registerContextAwareService");
    Preconditions.checkStringNotEmpty(paramString);
    Objects.requireNonNull(paramClass);
    Objects.requireNonNull(paramContextAwareServiceProducerWithBinder);
    registerService(paramString, paramClass, (ServiceFetcher<TServiceClass>)new Object(paramContextAwareServiceProducerWithBinder, paramString));
  }
  
  @SystemApi
  public static <TServiceClass> void registerContextAwareService(String paramString, Class<TServiceClass> paramClass, ContextAwareServiceProducerWithoutBinder<TServiceClass> paramContextAwareServiceProducerWithoutBinder) {
    ensureInitializing("registerContextAwareService");
    Preconditions.checkStringNotEmpty(paramString);
    Objects.requireNonNull(paramClass);
    Objects.requireNonNull(paramContextAwareServiceProducerWithoutBinder);
    registerService(paramString, paramClass, (ServiceFetcher<TServiceClass>)new Object(paramContextAwareServiceProducerWithoutBinder));
  }
  
  class CachedServiceFetcher<T> implements ServiceFetcher<T> {
    private final int mCacheIndex = SystemServiceRegistry.access$008();
    
    public abstract T createService(ContextImpl param1ContextImpl) throws ServiceManager.ServiceNotFoundException;
    
    public final T getService(ContextImpl param1ContextImpl) {
      // Byte code:
      //   0: aload_1
      //   1: getfield mServiceCache : [Ljava/lang/Object;
      //   4: astore_2
      //   5: aload_1
      //   6: getfield mServiceInitializationStateArray : [I
      //   9: astore_3
      //   10: iconst_0
      //   11: istore #4
      //   13: iconst_0
      //   14: istore #5
      //   16: aload_2
      //   17: monitorenter
      //   18: aload_2
      //   19: aload_0
      //   20: getfield mCacheIndex : I
      //   23: aaload
      //   24: astore #6
      //   26: aload #6
      //   28: ifnonnull -> 279
      //   31: aload_3
      //   32: aload_0
      //   33: getfield mCacheIndex : I
      //   36: iaload
      //   37: iconst_3
      //   38: if_icmpne -> 44
      //   41: goto -> 279
      //   44: aload_3
      //   45: aload_0
      //   46: getfield mCacheIndex : I
      //   49: iaload
      //   50: iconst_2
      //   51: if_icmpne -> 61
      //   54: aload_3
      //   55: aload_0
      //   56: getfield mCacheIndex : I
      //   59: iconst_0
      //   60: iastore
      //   61: aload_3
      //   62: aload_0
      //   63: getfield mCacheIndex : I
      //   66: iaload
      //   67: ifne -> 80
      //   70: iconst_1
      //   71: istore #5
      //   73: aload_3
      //   74: aload_0
      //   75: getfield mCacheIndex : I
      //   78: iconst_1
      //   79: iastore
      //   80: aload_2
      //   81: monitorexit
      //   82: iload #5
      //   84: ifeq -> 222
      //   87: aconst_null
      //   88: astore #6
      //   90: aload_0
      //   91: aload_1
      //   92: invokevirtual createService : (Landroid/app/ContextImpl;)Ljava/lang/Object;
      //   95: astore_1
      //   96: aload_2
      //   97: monitorenter
      //   98: aload_2
      //   99: aload_0
      //   100: getfield mCacheIndex : I
      //   103: aload_1
      //   104: aastore
      //   105: aload_3
      //   106: aload_0
      //   107: getfield mCacheIndex : I
      //   110: iconst_2
      //   111: iastore
      //   112: aload_2
      //   113: invokevirtual notifyAll : ()V
      //   116: aload_2
      //   117: monitorexit
      //   118: goto -> 185
      //   121: astore_1
      //   122: aload_2
      //   123: monitorexit
      //   124: aload_1
      //   125: athrow
      //   126: astore_1
      //   127: goto -> 193
      //   130: astore_1
      //   131: aload_1
      //   132: invokestatic onServiceNotFound : (Landroid/os/ServiceManager$ServiceNotFoundException;)V
      //   135: invokestatic access$100 : ()Ljava/lang/String;
      //   138: ldc 'system_server'
      //   140: invokevirtual contains : (Ljava/lang/CharSequence;)Z
      //   143: istore #7
      //   145: iload #7
      //   147: ifeq -> 156
      //   150: iconst_0
      //   151: istore #5
      //   153: goto -> 159
      //   156: iconst_3
      //   157: istore #5
      //   159: aload_2
      //   160: monitorenter
      //   161: aload_2
      //   162: aload_0
      //   163: getfield mCacheIndex : I
      //   166: aconst_null
      //   167: aastore
      //   168: aload_3
      //   169: aload_0
      //   170: getfield mCacheIndex : I
      //   173: iload #5
      //   175: iastore
      //   176: aload_2
      //   177: invokevirtual notifyAll : ()V
      //   180: aload_2
      //   181: monitorexit
      //   182: aload #6
      //   184: astore_1
      //   185: goto -> 284
      //   188: astore_1
      //   189: aload_2
      //   190: monitorexit
      //   191: aload_1
      //   192: athrow
      //   193: aload_2
      //   194: monitorenter
      //   195: aload_2
      //   196: aload_0
      //   197: getfield mCacheIndex : I
      //   200: aconst_null
      //   201: aastore
      //   202: aload_3
      //   203: aload_0
      //   204: getfield mCacheIndex : I
      //   207: iconst_3
      //   208: iastore
      //   209: aload_2
      //   210: invokevirtual notifyAll : ()V
      //   213: aload_2
      //   214: monitorexit
      //   215: aload_1
      //   216: athrow
      //   217: astore_1
      //   218: aload_2
      //   219: monitorexit
      //   220: aload_1
      //   221: athrow
      //   222: aload_2
      //   223: monitorenter
      //   224: aload_3
      //   225: aload_0
      //   226: getfield mCacheIndex : I
      //   229: iaload
      //   230: istore #5
      //   232: iload #5
      //   234: iconst_2
      //   235: if_icmpge -> 269
      //   238: iload #4
      //   240: invokestatic interrupted : ()Z
      //   243: ior
      //   244: istore #4
      //   246: aload_2
      //   247: invokevirtual wait : ()V
      //   250: goto -> 224
      //   253: astore #6
      //   255: ldc 'SystemServiceRegistry'
      //   257: ldc 'getService() interrupted'
      //   259: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   262: pop
      //   263: iconst_1
      //   264: istore #4
      //   266: goto -> 250
      //   269: aload_2
      //   270: monitorexit
      //   271: goto -> 13
      //   274: astore_1
      //   275: aload_2
      //   276: monitorexit
      //   277: aload_1
      //   278: athrow
      //   279: aload #6
      //   281: astore_1
      //   282: aload_2
      //   283: monitorexit
      //   284: iload #4
      //   286: ifeq -> 295
      //   289: invokestatic currentThread : ()Ljava/lang/Thread;
      //   292: invokevirtual interrupt : ()V
      //   295: aload_1
      //   296: areturn
      //   297: astore_1
      //   298: aload_2
      //   299: monitorexit
      //   300: aload_1
      //   301: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1846	-> 0
      //   #1847	-> 5
      //   #1848	-> 10
      //   #1850	-> 13
      //   #1853	-> 13
      //   #1854	-> 16
      //   #1856	-> 18
      //   #1857	-> 26
      //   #1867	-> 44
      //   #1868	-> 54
      //   #1875	-> 61
      //   #1876	-> 70
      //   #1877	-> 73
      //   #1879	-> 80
      //   #1881	-> 82
      //   #1884	-> 87
      //   #1885	-> 90
      //   #1889	-> 90
      //   #1890	-> 96
      //   #1901	-> 96
      //   #1902	-> 98
      //   #1903	-> 105
      //   #1904	-> 112
      //   #1905	-> 116
      //   #1906	-> 118
      //   #1905	-> 121
      //   #1901	-> 126
      //   #1892	-> 130
      //   #1893	-> 131
      //   #1896	-> 135
      //   #1897	-> 150
      //   #1896	-> 156
      //   #1901	-> 159
      //   #1902	-> 161
      //   #1903	-> 168
      //   #1904	-> 176
      //   #1905	-> 180
      //   #1906	-> 182
      //   #1907	-> 185
      //   #1908	-> 185
      //   #1905	-> 188
      //   #1901	-> 193
      //   #1902	-> 195
      //   #1903	-> 202
      //   #1904	-> 209
      //   #1905	-> 213
      //   #1906	-> 215
      //   #1905	-> 217
      //   #1912	-> 222
      //   #1916	-> 224
      //   #1919	-> 238
      //   #1920	-> 246
      //   #1926	-> 250
      //   #1921	-> 253
      //   #1924	-> 255
      //   #1925	-> 263
      //   #1928	-> 269
      //   #1929	-> 271
      //   #1928	-> 274
      //   #1858	-> 279
      //   #1859	-> 282
      //   #1930	-> 284
      //   #1931	-> 289
      //   #1933	-> 295
      //   #1879	-> 297
      // Exception table:
      //   from	to	target	type
      //   18	26	297	finally
      //   31	41	297	finally
      //   44	54	297	finally
      //   54	61	297	finally
      //   61	70	297	finally
      //   73	80	297	finally
      //   80	82	297	finally
      //   90	96	130	android/os/ServiceManager$ServiceNotFoundException
      //   90	96	126	finally
      //   98	105	121	finally
      //   105	112	121	finally
      //   112	116	121	finally
      //   116	118	121	finally
      //   122	124	121	finally
      //   131	135	126	finally
      //   135	145	126	finally
      //   161	168	188	finally
      //   168	176	188	finally
      //   176	180	188	finally
      //   180	182	188	finally
      //   189	191	188	finally
      //   195	202	217	finally
      //   202	209	217	finally
      //   209	213	217	finally
      //   213	215	217	finally
      //   218	220	217	finally
      //   224	232	274	finally
      //   238	246	253	java/lang/InterruptedException
      //   238	246	274	finally
      //   246	250	253	java/lang/InterruptedException
      //   246	250	274	finally
      //   255	263	274	finally
      //   269	271	274	finally
      //   275	277	274	finally
      //   282	284	297	finally
      //   298	300	297	finally
    }
  }
  
  class StaticServiceFetcher<T> implements ServiceFetcher<T> {
    private T mCachedInstance;
    
    public abstract T createService() throws ServiceManager.ServiceNotFoundException;
    
    public final T getService(ContextImpl param1ContextImpl) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCachedInstance : Ljava/lang/Object;
      //   6: astore_1
      //   7: aload_1
      //   8: ifnonnull -> 27
      //   11: aload_0
      //   12: aload_0
      //   13: invokevirtual createService : ()Ljava/lang/Object;
      //   16: putfield mCachedInstance : Ljava/lang/Object;
      //   19: goto -> 27
      //   22: astore_1
      //   23: aload_1
      //   24: invokestatic onServiceNotFound : (Landroid/os/ServiceManager$ServiceNotFoundException;)V
      //   27: aload_0
      //   28: getfield mCachedInstance : Ljava/lang/Object;
      //   31: astore_1
      //   32: aload_0
      //   33: monitorexit
      //   34: aload_1
      //   35: areturn
      //   36: astore_1
      //   37: aload_0
      //   38: monitorexit
      //   39: aload_1
      //   40: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1948	-> 0
      //   #1949	-> 2
      //   #1951	-> 11
      //   #1954	-> 19
      //   #1952	-> 22
      //   #1953	-> 23
      //   #1956	-> 27
      //   #1957	-> 36
      // Exception table:
      //   from	to	target	type
      //   2	7	36	finally
      //   11	19	22	android/os/ServiceManager$ServiceNotFoundException
      //   11	19	36	finally
      //   23	27	36	finally
      //   27	34	36	finally
      //   37	39	36	finally
    }
  }
  
  class StaticApplicationContextServiceFetcher<T> implements ServiceFetcher<T> {
    private T mCachedInstance;
    
    public abstract T createService(Context param1Context) throws ServiceManager.ServiceNotFoundException;
    
    public final T getService(ContextImpl param1ContextImpl) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mCachedInstance : Ljava/lang/Object;
      //   6: ifnonnull -> 40
      //   9: aload_1
      //   10: invokevirtual getApplicationContext : ()Landroid/content/Context;
      //   13: astore_2
      //   14: aload_2
      //   15: ifnull -> 23
      //   18: aload_2
      //   19: astore_1
      //   20: goto -> 23
      //   23: aload_0
      //   24: aload_0
      //   25: aload_1
      //   26: invokevirtual createService : (Landroid/content/Context;)Ljava/lang/Object;
      //   29: putfield mCachedInstance : Ljava/lang/Object;
      //   32: goto -> 40
      //   35: astore_1
      //   36: aload_1
      //   37: invokestatic onServiceNotFound : (Landroid/os/ServiceManager$ServiceNotFoundException;)V
      //   40: aload_0
      //   41: getfield mCachedInstance : Ljava/lang/Object;
      //   44: astore_1
      //   45: aload_0
      //   46: monitorexit
      //   47: aload_1
      //   48: areturn
      //   49: astore_1
      //   50: aload_0
      //   51: monitorexit
      //   52: aload_1
      //   53: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1976	-> 0
      //   #1977	-> 2
      //   #1978	-> 9
      //   #1984	-> 14
      //   #1987	-> 32
      //   #1985	-> 35
      //   #1986	-> 36
      //   #1989	-> 40
      //   #1990	-> 49
      // Exception table:
      //   from	to	target	type
      //   2	9	49	finally
      //   9	14	49	finally
      //   23	32	35	android/os/ServiceManager$ServiceNotFoundException
      //   23	32	49	finally
      //   36	40	49	finally
      //   40	47	49	finally
      //   50	52	49	finally
    }
  }
  
  public static void onServiceNotFound(ServiceManager.ServiceNotFoundException paramServiceNotFoundException) {
    if (Process.myUid() < 10000) {
      Log.w("SystemServiceRegistry", paramServiceNotFoundException.getMessage());
    } else {
      Log.w("SystemServiceRegistry", paramServiceNotFoundException.getMessage());
    } 
  }
  
  private static String checkAppPackageName() {
    String str1, str2, str3;
    null = "";
    FileInputStream fileInputStream1 = null, fileInputStream2 = null;
    FileInputStream fileInputStream3 = fileInputStream2, fileInputStream4 = fileInputStream1;
    try {
      FileInputStream fileInputStream = new FileInputStream();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      StringBuilder stringBuilder = new StringBuilder();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this();
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      stringBuilder.append("/proc/");
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      stringBuilder.append(Process.myPid());
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      stringBuilder.append("/cmdline");
      fileInputStream3 = fileInputStream2;
      fileInputStream4 = fileInputStream1;
      this(stringBuilder.toString());
      fileInputStream1 = fileInputStream;
      fileInputStream3 = fileInputStream1;
      fileInputStream4 = fileInputStream1;
      byte[] arrayOfByte = new byte[50];
      fileInputStream3 = fileInputStream1;
      fileInputStream4 = fileInputStream1;
      int i = fileInputStream1.read(arrayOfByte);
      str3 = null;
      if (i > 0) {
        fileInputStream3 = fileInputStream1;
        FileInputStream fileInputStream5 = fileInputStream1;
        String str = new String();
        fileInputStream3 = fileInputStream1;
        fileInputStream5 = fileInputStream1;
        this(arrayOfByte, 0, i);
        str3 = str;
      } 
      str2 = str3;
    } catch (Exception exception) {
      str1 = null;
      if (str3 != null) {
        str2 = null;
        str3.close();
      } else {
        return str1;
      } 
    } finally {
      if (str2 != null)
        try {
          str2.close();
        } catch (Exception exception) {} 
    } 
    return str1;
  }
  
  @SystemApi
  public static interface ContextAwareServiceProducerWithBinder<TServiceClass> {
    TServiceClass createService(Context param1Context, IBinder param1IBinder);
  }
  
  @SystemApi
  public static interface ContextAwareServiceProducerWithoutBinder<TServiceClass> {
    TServiceClass createService(Context param1Context);
  }
  
  static interface ServiceFetcher<T> {
    T getService(ContextImpl param1ContextImpl);
  }
  
  @SystemApi
  public static interface StaticServiceProducerWithBinder<TServiceClass> {
    TServiceClass createService(IBinder param1IBinder);
  }
  
  @SystemApi
  public static interface StaticServiceProducerWithoutBinder<TServiceClass> {
    TServiceClass createService();
  }
}
