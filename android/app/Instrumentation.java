package android.app;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.hardware.input.InputManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.Looper;
import android.os.MessageQueue;
import android.os.Parcelable;
import android.os.PerformanceCollector;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.TestLooperManager;
import android.os.UserHandle;
import android.util.AndroidRuntimeException;
import android.util.Log;
import android.util.SeempLog;
import android.view.IWindowManager;
import android.view.InputEvent;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.ViewConfiguration;
import android.view.Window;
import android.view.WindowManagerGlobal;
import com.android.internal.content.ReferrerIntent;
import java.io.File;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public class Instrumentation {
  private final Object mSync = new Object();
  
  private ActivityThread mThread = null;
  
  private MessageQueue mMessageQueue = null;
  
  private boolean mAutomaticPerformanceSnapshots = false;
  
  private Bundle mPerfMetrics = new Bundle();
  
  private final Object mAnimationCompleteLock = new Object();
  
  public static final String REPORT_KEY_IDENTIFIER = "id";
  
  public static final String REPORT_KEY_STREAMRESULT = "stream";
  
  private static final String TAG = "Instrumentation";
  
  private List<ActivityMonitor> mActivityMonitors;
  
  private Context mAppContext;
  
  private ComponentName mComponent;
  
  private Context mInstrContext;
  
  private PerformanceCollector mPerformanceCollector;
  
  private Thread mRunner;
  
  private UiAutomation mUiAutomation;
  
  private IUiAutomationConnection mUiAutomationConnection;
  
  private List<ActivityWaiter> mWaitingActivities;
  
  private IInstrumentationWatcher mWatcher;
  
  private void checkInstrumenting(String paramString) {
    if (this.mInstrContext != null)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" cannot be called outside of instrumented processes");
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void onCreate(Bundle paramBundle) {}
  
  public void start() {
    if (this.mRunner == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Instr: ");
      stringBuilder.append(getClass().getName());
      InstrumentationThread instrumentationThread = new InstrumentationThread(stringBuilder.toString());
      instrumentationThread.start();
      return;
    } 
    throw new RuntimeException("Instrumentation already started");
  }
  
  public void onStart() {}
  
  public boolean onException(Object paramObject, Throwable paramThrowable) {
    return false;
  }
  
  public void sendStatus(int paramInt, Bundle paramBundle) {
    IInstrumentationWatcher iInstrumentationWatcher = this.mWatcher;
    if (iInstrumentationWatcher != null)
      try {
        iInstrumentationWatcher.instrumentationStatus(this.mComponent, paramInt, paramBundle);
      } catch (RemoteException remoteException) {
        this.mWatcher = null;
      }  
  }
  
  public void addResults(Bundle paramBundle) {
    IActivityManager iActivityManager = ActivityManager.getService();
    try {
      iActivityManager.addInstrumentationResults(this.mThread.getApplicationThread(), paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void finish(int paramInt, Bundle paramBundle) {
    if (this.mAutomaticPerformanceSnapshots)
      endPerformanceSnapshot(); 
    Bundle bundle = paramBundle;
    if (this.mPerfMetrics != null) {
      bundle = paramBundle;
      if (paramBundle == null)
        bundle = new Bundle(); 
      bundle.putAll(this.mPerfMetrics);
    } 
    UiAutomation uiAutomation = this.mUiAutomation;
    if (uiAutomation != null && !uiAutomation.isDestroyed()) {
      this.mUiAutomation.disconnect();
      this.mUiAutomation = null;
    } 
    this.mThread.finishInstrumentation(paramInt, bundle);
  }
  
  public void setAutomaticPerformanceSnapshots() {
    this.mAutomaticPerformanceSnapshots = true;
    this.mPerformanceCollector = new PerformanceCollector();
  }
  
  public void startPerformanceSnapshot() {
    if (!isProfiling())
      this.mPerformanceCollector.beginSnapshot(null); 
  }
  
  public void endPerformanceSnapshot() {
    if (!isProfiling())
      this.mPerfMetrics = this.mPerformanceCollector.endSnapshot(); 
  }
  
  public void onDestroy() {}
  
  public Context getContext() {
    return this.mInstrContext;
  }
  
  public ComponentName getComponentName() {
    return this.mComponent;
  }
  
  public Context getTargetContext() {
    return this.mAppContext;
  }
  
  public String getProcessName() {
    return this.mThread.getProcessName();
  }
  
  public boolean isProfiling() {
    return this.mThread.isProfiling();
  }
  
  public void startProfiling() {
    if (this.mThread.isProfiling()) {
      File file = new File(this.mThread.getProfileFilePath());
      file.getParentFile().mkdirs();
      Debug.startMethodTracing(file.toString(), 8388608);
    } 
  }
  
  public void stopProfiling() {
    if (this.mThread.isProfiling())
      Debug.stopMethodTracing(); 
  }
  
  public void setInTouchMode(boolean paramBoolean) {
    try {
      IBinder iBinder = ServiceManager.getService("window");
      IWindowManager iWindowManager = IWindowManager.Stub.asInterface(iBinder);
      iWindowManager.setInTouchMode(paramBoolean);
    } catch (RemoteException remoteException) {}
  }
  
  public void waitForIdle(Runnable paramRunnable) {
    this.mMessageQueue.addIdleHandler(new Idler(paramRunnable));
    this.mThread.getHandler().post(new EmptyRunnable());
  }
  
  public void waitForIdleSync() {
    validateNotAppThread();
    Idler idler = new Idler(null);
    this.mMessageQueue.addIdleHandler(idler);
    this.mThread.getHandler().post(new EmptyRunnable());
    idler.waitForIdle();
  }
  
  private void waitForEnterAnimationComplete(Activity paramActivity) {
    Object object = this.mAnimationCompleteLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    long l = 5000L;
    while (true) {
      if (l > 0L)
        try {
          if (!paramActivity.mEnterAnimationComplete) {
            long l1 = System.currentTimeMillis();
            this.mAnimationCompleteLock.wait(l);
            long l2 = System.currentTimeMillis();
            l -= l2 - l1;
            continue;
          } 
        } catch (InterruptedException interruptedException) {
        
        } finally {} 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return;
    } 
  }
  
  public void onEnterAnimationComplete() {
    synchronized (this.mAnimationCompleteLock) {
      this.mAnimationCompleteLock.notifyAll();
      return;
    } 
  }
  
  public void runOnMainSync(Runnable paramRunnable) {
    validateNotAppThread();
    paramRunnable = new SyncRunnable(paramRunnable);
    this.mThread.getHandler().post(paramRunnable);
    paramRunnable.waitForComplete();
  }
  
  public Activity startActivitySync(Intent paramIntent) {
    return startActivitySync(paramIntent, null);
  }
  
  public Activity startActivitySync(Intent paramIntent, Bundle paramBundle) {
    // Byte code:
    //   0: sipush #376
    //   3: aload_1
    //   4: invokevirtual toString : ()Ljava/lang/String;
    //   7: invokestatic record_str : (ILjava/lang/String;)I
    //   10: pop
    //   11: aload_0
    //   12: invokespecial validateNotAppThread : ()V
    //   15: aload_0
    //   16: getfield mSync : Ljava/lang/Object;
    //   19: astore_3
    //   20: aload_3
    //   21: monitorenter
    //   22: new android/content/Intent
    //   25: astore #4
    //   27: aload #4
    //   29: aload_1
    //   30: invokespecial <init> : (Landroid/content/Intent;)V
    //   33: aload_0
    //   34: invokevirtual getTargetContext : ()Landroid/content/Context;
    //   37: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   40: astore_1
    //   41: aload #4
    //   43: aload_1
    //   44: iconst_0
    //   45: invokevirtual resolveActivityInfo : (Landroid/content/pm/PackageManager;I)Landroid/content/pm/ActivityInfo;
    //   48: astore_1
    //   49: aload_1
    //   50: ifnull -> 306
    //   53: aload_0
    //   54: getfield mThread : Landroid/app/ActivityThread;
    //   57: invokevirtual getProcessName : ()Ljava/lang/String;
    //   60: astore #5
    //   62: aload_1
    //   63: getfield processName : Ljava/lang/String;
    //   66: aload #5
    //   68: invokevirtual equals : (Ljava/lang/Object;)Z
    //   71: ifeq -> 228
    //   74: new android/content/ComponentName
    //   77: astore #5
    //   79: aload #5
    //   81: aload_1
    //   82: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   85: getfield packageName : Ljava/lang/String;
    //   88: aload_1
    //   89: getfield name : Ljava/lang/String;
    //   92: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   95: aload #4
    //   97: aload #5
    //   99: invokevirtual setComponent : (Landroid/content/ComponentName;)Landroid/content/Intent;
    //   102: pop
    //   103: new android/app/Instrumentation$ActivityWaiter
    //   106: astore_1
    //   107: aload_1
    //   108: aload #4
    //   110: invokespecial <init> : (Landroid/content/Intent;)V
    //   113: aload_0
    //   114: getfield mWaitingActivities : Ljava/util/List;
    //   117: ifnonnull -> 136
    //   120: new java/util/ArrayList
    //   123: astore #5
    //   125: aload #5
    //   127: invokespecial <init> : ()V
    //   130: aload_0
    //   131: aload #5
    //   133: putfield mWaitingActivities : Ljava/util/List;
    //   136: aload_0
    //   137: getfield mWaitingActivities : Ljava/util/List;
    //   140: aload_1
    //   141: invokeinterface add : (Ljava/lang/Object;)Z
    //   146: pop
    //   147: aload_0
    //   148: invokevirtual getTargetContext : ()Landroid/content/Context;
    //   151: aload #4
    //   153: aload_2
    //   154: invokevirtual startActivity : (Landroid/content/Intent;Landroid/os/Bundle;)V
    //   157: aload_0
    //   158: getfield mSync : Ljava/lang/Object;
    //   161: invokevirtual wait : ()V
    //   164: goto -> 168
    //   167: astore_2
    //   168: aload_0
    //   169: getfield mWaitingActivities : Ljava/util/List;
    //   172: aload_1
    //   173: invokeinterface contains : (Ljava/lang/Object;)Z
    //   178: ifne -> 157
    //   181: aload_1
    //   182: getfield activity : Landroid/app/Activity;
    //   185: astore_1
    //   186: aload_3
    //   187: monitorexit
    //   188: aload_0
    //   189: aload_1
    //   190: invokespecial waitForEnterAnimationComplete : (Landroid/app/Activity;)V
    //   193: new android/view/SurfaceControl$Transaction
    //   196: dup
    //   197: invokespecial <init> : ()V
    //   200: astore_2
    //   201: aload_2
    //   202: iconst_1
    //   203: invokevirtual apply : (Z)V
    //   206: aload_2
    //   207: invokevirtual close : ()V
    //   210: aload_1
    //   211: areturn
    //   212: astore_1
    //   213: aload_2
    //   214: invokevirtual close : ()V
    //   217: goto -> 226
    //   220: astore_2
    //   221: aload_1
    //   222: aload_2
    //   223: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   226: aload_1
    //   227: athrow
    //   228: new java/lang/RuntimeException
    //   231: astore_2
    //   232: new java/lang/StringBuilder
    //   235: astore #6
    //   237: aload #6
    //   239: invokespecial <init> : ()V
    //   242: aload #6
    //   244: ldc_w 'Intent in process '
    //   247: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload #6
    //   253: aload #5
    //   255: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: aload #6
    //   261: ldc_w ' resolved to different process '
    //   264: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   267: pop
    //   268: aload #6
    //   270: aload_1
    //   271: getfield processName : Ljava/lang/String;
    //   274: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   277: pop
    //   278: aload #6
    //   280: ldc_w ': '
    //   283: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   286: pop
    //   287: aload #6
    //   289: aload #4
    //   291: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   294: pop
    //   295: aload_2
    //   296: aload #6
    //   298: invokevirtual toString : ()Ljava/lang/String;
    //   301: invokespecial <init> : (Ljava/lang/String;)V
    //   304: aload_2
    //   305: athrow
    //   306: new java/lang/RuntimeException
    //   309: astore_2
    //   310: new java/lang/StringBuilder
    //   313: astore_1
    //   314: aload_1
    //   315: invokespecial <init> : ()V
    //   318: aload_1
    //   319: ldc_w 'Unable to resolve activity for: '
    //   322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   325: pop
    //   326: aload_1
    //   327: aload #4
    //   329: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: aload_2
    //   334: aload_1
    //   335: invokevirtual toString : ()Ljava/lang/String;
    //   338: invokespecial <init> : (Ljava/lang/String;)V
    //   341: aload_2
    //   342: athrow
    //   343: astore_1
    //   344: aload_3
    //   345: monitorexit
    //   346: aload_1
    //   347: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #494	-> 0
    //   #495	-> 11
    //   #498	-> 15
    //   #499	-> 22
    //   #501	-> 33
    //   #502	-> 33
    //   #501	-> 41
    //   #503	-> 49
    //   #506	-> 53
    //   #507	-> 62
    //   #515	-> 74
    //   #517	-> 103
    //   #519	-> 113
    //   #520	-> 120
    //   #522	-> 136
    //   #524	-> 147
    //   #528	-> 157
    //   #530	-> 164
    //   #529	-> 167
    //   #531	-> 168
    //   #532	-> 181
    //   #533	-> 186
    //   #536	-> 188
    //   #540	-> 193
    //   #541	-> 201
    //   #542	-> 206
    //   #543	-> 210
    //   #540	-> 212
    //   #510	-> 228
    //   #504	-> 306
    //   #533	-> 343
    // Exception table:
    //   from	to	target	type
    //   22	33	343	finally
    //   33	41	343	finally
    //   41	49	343	finally
    //   53	62	343	finally
    //   62	74	343	finally
    //   74	103	343	finally
    //   103	113	343	finally
    //   113	120	343	finally
    //   120	136	343	finally
    //   136	147	343	finally
    //   147	157	343	finally
    //   157	164	167	java/lang/InterruptedException
    //   157	164	343	finally
    //   168	181	343	finally
    //   181	186	343	finally
    //   186	188	343	finally
    //   201	206	212	finally
    //   213	217	220	finally
    //   228	306	343	finally
    //   306	343	343	finally
    //   344	346	343	finally
  }
  
  public static class ActivityMonitor {
    private final boolean mBlock;
    
    private final String mClass;
    
    int mHits = 0;
    
    private final boolean mIgnoreMatchingSpecificIntents;
    
    Activity mLastActivity = null;
    
    private final Instrumentation.ActivityResult mResult;
    
    private final IntentFilter mWhich;
    
    public ActivityMonitor(IntentFilter param1IntentFilter, Instrumentation.ActivityResult param1ActivityResult, boolean param1Boolean) {
      this.mWhich = param1IntentFilter;
      this.mClass = null;
      this.mResult = param1ActivityResult;
      this.mBlock = param1Boolean;
      this.mIgnoreMatchingSpecificIntents = false;
    }
    
    public ActivityMonitor(String param1String, Instrumentation.ActivityResult param1ActivityResult, boolean param1Boolean) {
      this.mWhich = null;
      this.mClass = param1String;
      this.mResult = param1ActivityResult;
      this.mBlock = param1Boolean;
      this.mIgnoreMatchingSpecificIntents = false;
    }
    
    public ActivityMonitor() {
      this.mWhich = null;
      this.mClass = null;
      this.mResult = null;
      this.mBlock = false;
      this.mIgnoreMatchingSpecificIntents = true;
    }
    
    final boolean ignoreMatchingSpecificIntents() {
      return this.mIgnoreMatchingSpecificIntents;
    }
    
    public final IntentFilter getFilter() {
      return this.mWhich;
    }
    
    public final Instrumentation.ActivityResult getResult() {
      return this.mResult;
    }
    
    public final boolean isBlocking() {
      return this.mBlock;
    }
    
    public final int getHits() {
      return this.mHits;
    }
    
    public final Activity getLastActivity() {
      return this.mLastActivity;
    }
    
    public final Activity waitForActivity() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLastActivity : Landroid/app/Activity;
      //   6: astore_1
      //   7: aload_1
      //   8: ifnonnull -> 22
      //   11: aload_0
      //   12: invokevirtual wait : ()V
      //   15: goto -> 2
      //   18: astore_1
      //   19: goto -> 15
      //   22: aload_0
      //   23: getfield mLastActivity : Landroid/app/Activity;
      //   26: astore_1
      //   27: aload_0
      //   28: aconst_null
      //   29: putfield mLastActivity : Landroid/app/Activity;
      //   32: aload_0
      //   33: monitorexit
      //   34: aload_1
      //   35: areturn
      //   36: astore_1
      //   37: aload_0
      //   38: monitorexit
      //   39: aload_1
      //   40: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #688	-> 0
      //   #689	-> 2
      //   #691	-> 11
      //   #693	-> 15
      //   #692	-> 18
      //   #695	-> 22
      //   #696	-> 27
      //   #697	-> 32
      //   #698	-> 36
      // Exception table:
      //   from	to	target	type
      //   2	7	36	finally
      //   11	15	18	java/lang/InterruptedException
      //   11	15	36	finally
      //   22	27	36	finally
      //   27	32	36	finally
      //   32	34	36	finally
      //   37	39	36	finally
    }
    
    public final Activity waitForActivityWithTimeout(long param1Long) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mLastActivity : Landroid/app/Activity;
      //   6: astore_3
      //   7: aload_3
      //   8: ifnonnull -> 20
      //   11: aload_0
      //   12: lload_1
      //   13: invokevirtual wait : (J)V
      //   16: goto -> 20
      //   19: astore_3
      //   20: aload_0
      //   21: getfield mLastActivity : Landroid/app/Activity;
      //   24: ifnonnull -> 31
      //   27: aload_0
      //   28: monitorexit
      //   29: aconst_null
      //   30: areturn
      //   31: aload_0
      //   32: getfield mLastActivity : Landroid/app/Activity;
      //   35: astore_3
      //   36: aload_0
      //   37: aconst_null
      //   38: putfield mLastActivity : Landroid/app/Activity;
      //   41: aload_0
      //   42: monitorexit
      //   43: aload_3
      //   44: areturn
      //   45: astore_3
      //   46: aload_0
      //   47: monitorexit
      //   48: aload_3
      //   49: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #711	-> 0
      //   #712	-> 2
      //   #714	-> 11
      //   #716	-> 16
      //   #715	-> 19
      //   #718	-> 20
      //   #719	-> 27
      //   #721	-> 31
      //   #722	-> 36
      //   #723	-> 41
      //   #725	-> 45
      // Exception table:
      //   from	to	target	type
      //   2	7	45	finally
      //   11	16	19	java/lang/InterruptedException
      //   11	16	45	finally
      //   20	27	45	finally
      //   27	29	45	finally
      //   31	36	45	finally
      //   36	41	45	finally
      //   41	43	45	finally
      //   46	48	45	finally
    }
    
    public Instrumentation.ActivityResult onStartActivity(Intent param1Intent) {
      return null;
    }
    
    final boolean match(Context param1Context, Activity param1Activity, Intent param1Intent) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mIgnoreMatchingSpecificIntents : Z
      //   4: ifeq -> 9
      //   7: iconst_0
      //   8: ireturn
      //   9: aload_0
      //   10: monitorenter
      //   11: aload_0
      //   12: getfield mWhich : Landroid/content/IntentFilter;
      //   15: ifnull -> 44
      //   18: aload_0
      //   19: getfield mWhich : Landroid/content/IntentFilter;
      //   22: astore #4
      //   24: aload #4
      //   26: aload_1
      //   27: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
      //   30: aload_3
      //   31: iconst_1
      //   32: ldc 'Instrumentation'
      //   34: invokevirtual match : (Landroid/content/ContentResolver;Landroid/content/Intent;ZLjava/lang/String;)I
      //   37: ifge -> 44
      //   40: aload_0
      //   41: monitorexit
      //   42: iconst_0
      //   43: ireturn
      //   44: aload_0
      //   45: getfield mClass : Ljava/lang/String;
      //   48: ifnull -> 102
      //   51: aconst_null
      //   52: astore_1
      //   53: aload_2
      //   54: ifnull -> 68
      //   57: aload_2
      //   58: invokevirtual getClass : ()Ljava/lang/Class;
      //   61: invokevirtual getName : ()Ljava/lang/String;
      //   64: astore_1
      //   65: goto -> 83
      //   68: aload_3
      //   69: invokevirtual getComponent : ()Landroid/content/ComponentName;
      //   72: ifnull -> 83
      //   75: aload_3
      //   76: invokevirtual getComponent : ()Landroid/content/ComponentName;
      //   79: invokevirtual getClassName : ()Ljava/lang/String;
      //   82: astore_1
      //   83: aload_1
      //   84: ifnull -> 98
      //   87: aload_0
      //   88: getfield mClass : Ljava/lang/String;
      //   91: aload_1
      //   92: invokevirtual equals : (Ljava/lang/Object;)Z
      //   95: ifne -> 102
      //   98: aload_0
      //   99: monitorexit
      //   100: iconst_0
      //   101: ireturn
      //   102: aload_2
      //   103: ifnull -> 115
      //   106: aload_0
      //   107: aload_2
      //   108: putfield mLastActivity : Landroid/app/Activity;
      //   111: aload_0
      //   112: invokevirtual notifyAll : ()V
      //   115: aload_0
      //   116: monitorexit
      //   117: iconst_1
      //   118: ireturn
      //   119: astore_1
      //   120: aload_0
      //   121: monitorexit
      //   122: aload_1
      //   123: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #749	-> 0
      //   #750	-> 7
      //   #752	-> 9
      //   #753	-> 11
      //   #754	-> 24
      //   #756	-> 40
      //   #758	-> 44
      //   #759	-> 51
      //   #760	-> 53
      //   #761	-> 57
      //   #762	-> 68
      //   #763	-> 75
      //   #765	-> 83
      //   #766	-> 98
      //   #769	-> 102
      //   #770	-> 106
      //   #771	-> 111
      //   #773	-> 115
      //   #774	-> 119
      // Exception table:
      //   from	to	target	type
      //   11	24	119	finally
      //   24	40	119	finally
      //   40	42	119	finally
      //   44	51	119	finally
      //   57	65	119	finally
      //   68	75	119	finally
      //   75	83	119	finally
      //   87	98	119	finally
      //   98	100	119	finally
      //   106	111	119	finally
      //   111	115	119	finally
      //   115	117	119	finally
      //   120	122	119	finally
    }
  }
  
  public void addMonitor(ActivityMonitor paramActivityMonitor) {
    synchronized (this.mSync) {
      if (this.mActivityMonitors == null) {
        ArrayList<ActivityMonitor> arrayList = new ArrayList();
        this();
        this.mActivityMonitors = arrayList;
      } 
      this.mActivityMonitors.add(paramActivityMonitor);
      return;
    } 
  }
  
  public ActivityMonitor addMonitor(IntentFilter paramIntentFilter, ActivityResult paramActivityResult, boolean paramBoolean) {
    ActivityMonitor activityMonitor = new ActivityMonitor(paramIntentFilter, paramActivityResult, paramBoolean);
    addMonitor(activityMonitor);
    return activityMonitor;
  }
  
  public ActivityMonitor addMonitor(String paramString, ActivityResult paramActivityResult, boolean paramBoolean) {
    ActivityMonitor activityMonitor = new ActivityMonitor(paramString, paramActivityResult, paramBoolean);
    addMonitor(activityMonitor);
    return activityMonitor;
  }
  
  public boolean checkMonitorHit(ActivityMonitor paramActivityMonitor, int paramInt) {
    waitForIdleSync();
    synchronized (this.mSync) {
      if (paramActivityMonitor.getHits() < paramInt)
        return false; 
      this.mActivityMonitors.remove(paramActivityMonitor);
      return true;
    } 
  }
  
  public Activity waitForMonitor(ActivityMonitor paramActivityMonitor) {
    Activity activity = paramActivityMonitor.waitForActivity();
    synchronized (this.mSync) {
      this.mActivityMonitors.remove(paramActivityMonitor);
      return activity;
    } 
  }
  
  public Activity waitForMonitorWithTimeout(ActivityMonitor paramActivityMonitor, long paramLong) {
    Activity activity = paramActivityMonitor.waitForActivityWithTimeout(paramLong);
    synchronized (this.mSync) {
      this.mActivityMonitors.remove(paramActivityMonitor);
      return activity;
    } 
  }
  
  public void removeMonitor(ActivityMonitor paramActivityMonitor) {
    synchronized (this.mSync) {
      this.mActivityMonitors.remove(paramActivityMonitor);
      return;
    } 
  }
  
  public boolean invokeMenuActionSync(Activity paramActivity, int paramInt1, int paramInt2) {
    class MenuRunnable implements Runnable {
      private final Activity activity;
      
      private final int flags;
      
      private final int identifier;
      
      boolean returnValue;
      
      final Instrumentation this$0;
      
      public MenuRunnable(Activity param1Activity, int param1Int1, int param1Int2) {
        this.activity = param1Activity;
        this.identifier = param1Int1;
        this.flags = param1Int2;
      }
      
      public void run() {
        Window window = this.activity.getWindow();
        this.returnValue = window.performPanelIdentifierAction(0, this.identifier, this.flags);
      }
    };
    MenuRunnable menuRunnable = new MenuRunnable(paramActivity, paramInt1, paramInt2);
    runOnMainSync(menuRunnable);
    return menuRunnable.returnValue;
  }
  
  public boolean invokeContextMenuAction(Activity paramActivity, int paramInt1, int paramInt2) {
    validateNotAppThread();
    KeyEvent keyEvent = new KeyEvent(0, 23);
    sendKeySync(keyEvent);
    waitForIdleSync();
    try {
      Thread.sleep(ViewConfiguration.getLongPressTimeout());
      keyEvent = new KeyEvent(1, 23);
      sendKeySync(keyEvent);
      waitForIdleSync();
      ContextMenuRunnable contextMenuRunnable = new ContextMenuRunnable(paramActivity, paramInt1, paramInt2);
      runOnMainSync(contextMenuRunnable);
      return contextMenuRunnable.returnValue;
    } catch (InterruptedException interruptedException) {
      Log.e("Instrumentation", "Could not sleep for long press timeout", interruptedException);
      return false;
    } 
  }
  
  public void sendStringSync(String paramString) {
    if (paramString == null)
      return; 
    KeyCharacterMap keyCharacterMap = KeyCharacterMap.load(-1);
    KeyEvent[] arrayOfKeyEvent = keyCharacterMap.getEvents(paramString.toCharArray());
    if (arrayOfKeyEvent != null)
      for (byte b = 0; b < arrayOfKeyEvent.length; b++)
        sendKeySync(KeyEvent.changeTimeRepeat(arrayOfKeyEvent[b], SystemClock.uptimeMillis(), 0));  
  }
  
  public void sendKeySync(KeyEvent paramKeyEvent) {
    validateNotAppThread();
    long l1 = paramKeyEvent.getDownTime();
    long l2 = paramKeyEvent.getEventTime();
    int i = paramKeyEvent.getSource();
    int j = i;
    if (i == 0)
      j = 257; 
    long l3 = l2;
    if (l2 == 0L)
      l3 = SystemClock.uptimeMillis(); 
    l2 = l1;
    if (l1 == 0L)
      l2 = l3; 
    KeyEvent keyEvent = new KeyEvent(paramKeyEvent);
    keyEvent.setTime(l2, l3);
    keyEvent.setSource(j);
    keyEvent.setFlags(paramKeyEvent.getFlags() | 0x8);
    InputManager.getInstance().injectInputEvent((InputEvent)keyEvent, 2);
  }
  
  public void sendKeyDownUpSync(int paramInt) {
    sendKeySync(new KeyEvent(0, paramInt));
    sendKeySync(new KeyEvent(1, paramInt));
  }
  
  public void sendCharacterSync(int paramInt) {
    sendKeySync(new KeyEvent(0, paramInt));
    sendKeySync(new KeyEvent(1, paramInt));
  }
  
  public void sendPointerSync(MotionEvent paramMotionEvent) {
    validateNotAppThread();
    if ((paramMotionEvent.getSource() & 0x2) == 0)
      paramMotionEvent.setSource(4098); 
    try {
      WindowManagerGlobal.getWindowManagerService().injectInputAfterTransactionsApplied((InputEvent)paramMotionEvent, 2);
    } catch (RemoteException remoteException) {}
  }
  
  public void sendTrackballEventSync(MotionEvent paramMotionEvent) {
    validateNotAppThread();
    if ((paramMotionEvent.getSource() & 0x4) == 0)
      paramMotionEvent.setSource(65540); 
    InputManager.getInstance().injectInputEvent((InputEvent)paramMotionEvent, 2);
  }
  
  public Application newApplication(ClassLoader paramClassLoader, String paramString, Context paramContext) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    AppComponentFactory appComponentFactory = getFactory(paramContext.getPackageName());
    Application application = appComponentFactory.instantiateApplication(paramClassLoader, paramString);
    application.attach(paramContext);
    return application;
  }
  
  public static Application newApplication(Class<?> paramClass, Context paramContext) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    Application application = (Application)paramClass.newInstance();
    application.attach(paramContext);
    return application;
  }
  
  public void callApplicationOnCreate(Application paramApplication) {
    paramApplication.onCreate();
  }
  
  public Activity newActivity(Class<?> paramClass, Context paramContext, IBinder paramIBinder, Application paramApplication, Intent paramIntent, ActivityInfo paramActivityInfo, CharSequence paramCharSequence, Activity paramActivity, String paramString, Object paramObject) throws InstantiationException, IllegalAccessException {
    Activity activity = (Activity)paramClass.newInstance();
    if (paramApplication == null)
      paramApplication = new Application(); 
    activity.attach(paramContext, null, this, paramIBinder, 0, paramApplication, paramIntent, paramActivityInfo, paramCharSequence, paramActivity, paramString, (Activity.NonConfigurationInstances)paramObject, new Configuration(), null, null, null, null, null);
    return activity;
  }
  
  public Activity newActivity(ClassLoader paramClassLoader, String paramString, Intent paramIntent) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
    String str;
    if (paramIntent != null && paramIntent.getComponent() != null) {
      str = paramIntent.getComponent().getPackageName();
    } else {
      str = null;
    } 
    return getFactory(str).instantiateActivity(paramClassLoader, paramString, paramIntent);
  }
  
  private AppComponentFactory getFactory(String paramString) {
    if (paramString == null) {
      Log.e("Instrumentation", "No pkg specified, disabling AppComponentFactory");
      return AppComponentFactory.DEFAULT;
    } 
    ActivityThread activityThread = this.mThread;
    if (activityThread == null) {
      Log.e("Instrumentation", "Uninitialized ActivityThread, likely app-created Instrumentation, disabling AppComponentFactory", new Throwable());
      return AppComponentFactory.DEFAULT;
    } 
    LoadedApk loadedApk2 = activityThread.peekPackageInfo(paramString, true);
    LoadedApk loadedApk1 = loadedApk2;
    if (loadedApk2 == null)
      loadedApk1 = (this.mThread.getSystemContext()).mPackageInfo; 
    return loadedApk1.getAppFactory();
  }
  
  private void prePerformCreate(Activity paramActivity) {
    if (this.mWaitingActivities != null)
      synchronized (this.mSync) {
        int i = this.mWaitingActivities.size();
        for (byte b = 0; b < i; b++) {
          ActivityWaiter activityWaiter = this.mWaitingActivities.get(b);
          Intent intent = activityWaiter.intent;
          if (intent.filterEquals(paramActivity.getIntent())) {
            activityWaiter.activity = paramActivity;
            MessageQueue messageQueue = this.mMessageQueue;
            ActivityGoing activityGoing = new ActivityGoing();
            this(this, activityWaiter);
            messageQueue.addIdleHandler(activityGoing);
          } 
        } 
      }  
  }
  
  private void postPerformCreate(Activity paramActivity) {
    if (this.mActivityMonitors != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          activityMonitor.match((Context)paramActivity, paramActivity, paramActivity.getIntent());
        } 
      }  
  }
  
  public void callActivityOnCreate(Activity paramActivity, Bundle paramBundle) {
    prePerformCreate(paramActivity);
    paramActivity.performCreate(paramBundle);
    postPerformCreate(paramActivity);
  }
  
  public void callActivityOnCreate(Activity paramActivity, Bundle paramBundle, PersistableBundle paramPersistableBundle) {
    prePerformCreate(paramActivity);
    paramActivity.performCreate(paramBundle, paramPersistableBundle);
    postPerformCreate(paramActivity);
  }
  
  public void callActivityOnDestroy(Activity paramActivity) {
    paramActivity.performDestroy();
  }
  
  public void callActivityOnRestoreInstanceState(Activity paramActivity, Bundle paramBundle) {
    paramActivity.performRestoreInstanceState(paramBundle);
  }
  
  public void callActivityOnRestoreInstanceState(Activity paramActivity, Bundle paramBundle, PersistableBundle paramPersistableBundle) {
    paramActivity.performRestoreInstanceState(paramBundle, paramPersistableBundle);
  }
  
  public void callActivityOnPostCreate(Activity paramActivity, Bundle paramBundle) {
    paramActivity.onPostCreate(paramBundle);
  }
  
  public void callActivityOnPostCreate(Activity paramActivity, Bundle paramBundle, PersistableBundle paramPersistableBundle) {
    paramActivity.onPostCreate(paramBundle, paramPersistableBundle);
  }
  
  public void callActivityOnNewIntent(Activity paramActivity, Intent paramIntent) {
    paramActivity.performNewIntent(paramIntent);
  }
  
  public void callActivityOnNewIntent(Activity paramActivity, ReferrerIntent paramReferrerIntent) {
    String str = paramActivity.mReferrer;
    if (paramReferrerIntent != null)
      try {
        paramActivity.mReferrer = paramReferrerIntent.mReferrer;
      } finally {} 
    if (paramReferrerIntent != null) {
      Intent intent2 = new Intent();
      this((Intent)paramReferrerIntent);
      Intent intent1 = intent2;
    } else {
      paramReferrerIntent = null;
    } 
    callActivityOnNewIntent(paramActivity, (Intent)paramReferrerIntent);
    paramActivity.mReferrer = str;
  }
  
  public void callActivityOnStart(Activity paramActivity) {
    paramActivity.onStart();
  }
  
  public void callActivityOnRestart(Activity paramActivity) {
    paramActivity.onRestart();
  }
  
  public void callActivityOnResume(Activity paramActivity) {
    paramActivity.mResumed = true;
    paramActivity.onResume();
    if (this.mActivityMonitors != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          activityMonitor.match((Context)paramActivity, paramActivity, paramActivity.getIntent());
        } 
      }  
  }
  
  public void callActivityOnStop(Activity paramActivity) {
    paramActivity.onStop();
  }
  
  public void callActivityOnSaveInstanceState(Activity paramActivity, Bundle paramBundle) {
    paramActivity.performSaveInstanceState(paramBundle);
  }
  
  public void callActivityOnSaveInstanceState(Activity paramActivity, Bundle paramBundle, PersistableBundle paramPersistableBundle) {
    paramActivity.performSaveInstanceState(paramBundle, paramPersistableBundle);
  }
  
  public void callActivityOnPause(Activity paramActivity) {
    paramActivity.performPause();
  }
  
  public void callActivityOnUserLeaving(Activity paramActivity) {
    paramActivity.performUserLeaving();
  }
  
  public void callActivityOnPictureInPictureRequested(Activity paramActivity) {
    paramActivity.onPictureInPictureRequested();
  }
  
  @Deprecated
  public void startAllocCounting() {
    Runtime.getRuntime().gc();
    Runtime.getRuntime().runFinalization();
    Runtime.getRuntime().gc();
    Debug.resetAllCounts();
    Debug.startAllocCounting();
  }
  
  @Deprecated
  public void stopAllocCounting() {
    Runtime.getRuntime().gc();
    Runtime.getRuntime().runFinalization();
    Runtime.getRuntime().gc();
    Debug.stopAllocCounting();
  }
  
  private void addValue(String paramString, int paramInt, Bundle paramBundle) {
    ArrayList<Integer> arrayList;
    if (paramBundle.containsKey(paramString)) {
      arrayList = paramBundle.getIntegerArrayList(paramString);
      if (arrayList != null)
        arrayList.add(Integer.valueOf(paramInt)); 
    } else {
      ArrayList<Integer> arrayList1 = new ArrayList();
      arrayList1.add(Integer.valueOf(paramInt));
      paramBundle.putIntegerArrayList((String)arrayList, arrayList1);
    } 
  }
  
  public Bundle getAllocCounts() {
    Bundle bundle = new Bundle();
    bundle.putLong("global_alloc_count", Debug.getGlobalAllocCount());
    bundle.putLong("global_alloc_size", Debug.getGlobalAllocSize());
    bundle.putLong("global_freed_count", Debug.getGlobalFreedCount());
    bundle.putLong("global_freed_size", Debug.getGlobalFreedSize());
    bundle.putLong("gc_invocation_count", Debug.getGlobalGcInvocationCount());
    return bundle;
  }
  
  public Bundle getBinderCounts() {
    Bundle bundle = new Bundle();
    bundle.putLong("sent_transactions", Debug.getBinderSentTransactions());
    bundle.putLong("received_transactions", Debug.getBinderReceivedTransactions());
    return bundle;
  }
  
  public static final class ActivityResult {
    private final int mResultCode;
    
    private final Intent mResultData;
    
    public ActivityResult(int param1Int, Intent param1Intent) {
      this.mResultCode = param1Int;
      this.mResultData = param1Intent;
    }
    
    public int getResultCode() {
      return this.mResultCode;
    }
    
    public Intent getResultData() {
      return this.mResultData;
    }
  }
  
  public ActivityResult execStartActivity(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent paramIntent, int paramInt, Bundle paramBundle) {
    SeempLog.record_str(377, paramIntent.toString());
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder1;
    Context context = null;
    if (paramActivity != null) {
      Uri uri = paramActivity.onProvideReferrer();
    } else {
      paramIBinder1 = null;
    } 
    if (paramIBinder1 != null)
      paramIntent.putExtra("android.intent.extra.REFERRER", (Parcelable)paramIBinder1); 
    if (this.mActivityMonitors != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          paramIBinder1 = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramIntent); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return activityResult;
          } 
          if (activityMonitor.match(paramContext, null, paramIntent)) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking()) {
              ActivityResult activityResult1;
              paramContext = context;
              if (paramInt >= 0)
                activityResult1 = activityMonitor.getResult(); 
              return activityResult1;
            } 
            break;
          } 
        } 
      }  
    try {
      paramIntent.migrateExtraStreamToClipData(paramContext);
      paramIntent.prepareToLeaveProcess(paramContext);
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      String str2 = paramContext.getBasePackageName(), str1 = paramContext.getAttributionTag();
      String str3 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
      if (paramActivity != null) {
        try {
          String str = paramActivity.mEmbeddedID;
        } catch (RemoteException null) {}
      } else {
        paramContext = null;
      } 
      try {
        paramInt = iActivityTaskManager.startActivity(iApplicationThread, str2, str1, paramIntent, str3, paramIBinder2, (String)paramContext, paramInt, 0, null, paramBundle);
        checkStartActivityResult(paramInt, paramIntent);
        return null;
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    throw new RuntimeException("Failure from system", remoteException);
  }
  
  public void execStartActivities(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent[] paramArrayOfIntent, Bundle paramBundle) {
    int i = paramContext.getUserId();
    execStartActivitiesAsUser(paramContext, paramIBinder1, paramIBinder2, paramActivity, paramArrayOfIntent, paramBundle, i);
  }
  
  public int execStartActivitiesAsUser(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent[] paramArrayOfIntent, Bundle paramBundle, int paramInt) {
    SeempLog.record_str(378, paramArrayOfIntent.toString());
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder1;
    if (this.mActivityMonitors != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          paramIBinder1 = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramArrayOfIntent[0]); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return -96;
          } 
          if (activityMonitor.match(paramContext, null, paramArrayOfIntent[0])) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking())
              return -96; 
            break;
          } 
        } 
      }  
    try {
      String[] arrayOfString = new String[paramArrayOfIntent.length];
      for (byte b = 0; b < paramArrayOfIntent.length; b++) {
        paramArrayOfIntent[b].migrateExtraStreamToClipData(paramContext);
        paramArrayOfIntent[b].prepareToLeaveProcess(paramContext);
        arrayOfString[b] = paramArrayOfIntent[b].resolveTypeIfNeeded(paramContext.getContentResolver());
      } 
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      String str2 = paramContext.getBasePackageName(), str1 = paramContext.getAttributionTag();
      paramInt = iActivityTaskManager.startActivities(iApplicationThread, str2, str1, paramArrayOfIntent, arrayOfString, paramIBinder2, paramBundle, paramInt);
      checkStartActivityResult(paramInt, paramArrayOfIntent[0]);
      return paramInt;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Failure from system", remoteException);
    } 
  }
  
  public ActivityResult execStartActivity(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, String paramString, Intent paramIntent, int paramInt, Bundle paramBundle) {
    SeempLog.record_str(377, paramIntent.toString());
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder1;
    List<ActivityMonitor> list = this.mActivityMonitors;
    Context context = null;
    if (list != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          list = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramIntent); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return activityResult;
          } 
          if (activityMonitor.match(paramContext, null, paramIntent)) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking()) {
              ActivityResult activityResult1;
              paramContext = context;
              if (paramInt >= 0)
                activityResult1 = activityMonitor.getResult(); 
              return activityResult1;
            } 
            break;
          } 
        } 
      }  
    try {
      paramIntent.migrateExtraStreamToClipData(paramContext);
      paramIntent.prepareToLeaveProcess(paramContext);
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      String str3 = paramContext.getBasePackageName(), str2 = paramContext.getAttributionTag();
      String str1 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
      paramInt = iActivityTaskManager.startActivity(iApplicationThread, str3, str2, paramIntent, str1, paramIBinder2, paramString, paramInt, 0, null, paramBundle);
      checkStartActivityResult(paramInt, paramIntent);
      return null;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Failure from system", remoteException);
    } 
  }
  
  public ActivityResult execStartActivity(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, String paramString, Intent paramIntent, int paramInt, Bundle paramBundle, UserHandle paramUserHandle) {
    SeempLog.record_str(377, paramIntent.toString());
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder1;
    List<ActivityMonitor> list = this.mActivityMonitors;
    Context context = null;
    if (list != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          list = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramIntent); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return activityResult;
          } 
          if (activityMonitor.match(paramContext, null, paramIntent)) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking()) {
              ActivityResult activityResult1;
              paramContext = context;
              if (paramInt >= 0)
                activityResult1 = activityMonitor.getResult(); 
              return activityResult1;
            } 
            break;
          } 
        } 
      }  
    try {
      paramIntent.migrateExtraStreamToClipData(paramContext);
      paramIntent.prepareToLeaveProcess(paramContext);
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      String str2 = paramContext.getBasePackageName(), str3 = paramContext.getAttributionTag();
      String str1 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
      int i = paramUserHandle.getIdentifier();
      try {
        paramInt = iActivityTaskManager.startActivityAsUser(iApplicationThread, str2, str3, paramIntent, str1, paramIBinder2, paramString, paramInt, 0, null, paramBundle, i);
        checkStartActivityResult(paramInt, paramIntent);
        return null;
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    throw new RuntimeException("Failure from system", remoteException);
  }
  
  public ActivityResult execStartActivityAsCaller(Context paramContext, IBinder paramIBinder1, IBinder paramIBinder2, Activity paramActivity, Intent paramIntent, int paramInt1, Bundle paramBundle, IBinder paramIBinder3, boolean paramBoolean, int paramInt2) {
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder1;
    List<ActivityMonitor> list = this.mActivityMonitors;
    Context context = null;
    if (list != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          list = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramIntent); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return activityResult;
          } 
          if (activityMonitor.match(paramContext, null, paramIntent)) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking()) {
              ActivityResult activityResult1;
              paramContext = context;
              if (paramInt1 >= 0)
                activityResult1 = activityMonitor.getResult(); 
              return activityResult1;
            } 
            break;
          } 
        } 
      }  
    try {
      paramIntent.migrateExtraStreamToClipData(paramContext);
      paramIntent.prepareToLeaveProcess(paramContext);
      IActivityTaskManager iActivityTaskManager = ActivityTaskManager.getService();
      String str1 = paramContext.getBasePackageName();
      String str2 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
      if (paramActivity != null) {
        String str = paramActivity.mEmbeddedID;
      } else {
        paramContext = null;
      } 
      try {
        paramInt1 = iActivityTaskManager.startActivityAsCaller(iApplicationThread, str1, paramIntent, str2, paramIBinder2, (String)paramContext, paramInt1, 0, null, paramBundle, paramIBinder3, paramBoolean, paramInt2);
        try {
          checkStartActivityResult(paramInt1, paramIntent);
          return null;
        } catch (RemoteException null) {}
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    throw new RuntimeException("Failure from system", remoteException);
  }
  
  public void execStartActivityFromAppTask(Context paramContext, IBinder paramIBinder, IAppTask paramIAppTask, Intent paramIntent, Bundle paramBundle) {
    SeempLog.record_str(380, paramIntent.toString());
    IApplicationThread iApplicationThread = (IApplicationThread)paramIBinder;
    if (this.mActivityMonitors != null)
      synchronized (this.mSync) {
        int i = this.mActivityMonitors.size();
        for (byte b = 0; b < i; b++) {
          ActivityResult activityResult;
          ActivityMonitor activityMonitor = this.mActivityMonitors.get(b);
          paramIBinder = null;
          if (activityMonitor.ignoreMatchingSpecificIntents())
            activityResult = activityMonitor.onStartActivity(paramIntent); 
          if (activityResult != null) {
            activityMonitor.mHits++;
            return;
          } 
          if (activityMonitor.match(paramContext, null, paramIntent)) {
            activityMonitor.mHits++;
            if (activityMonitor.isBlocking())
              return; 
            break;
          } 
        } 
      }  
    try {
      paramIntent.migrateExtraStreamToClipData(paramContext);
      paramIntent.prepareToLeaveProcess(paramContext);
      paramIBinder = iApplicationThread.asBinder();
      String str3 = paramContext.getBasePackageName();
      String str2 = paramContext.getAttributionTag();
      String str1 = paramIntent.resolveTypeIfNeeded(paramContext.getContentResolver());
      int i = paramIAppTask.startActivity(paramIBinder, str3, str2, paramIntent, str1, paramBundle);
      checkStartActivityResult(i, paramIntent);
      return;
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Failure from system", remoteException);
    } 
  }
  
  final void init(ActivityThread paramActivityThread, Context paramContext1, Context paramContext2, ComponentName paramComponentName, IInstrumentationWatcher paramIInstrumentationWatcher, IUiAutomationConnection paramIUiAutomationConnection) {
    this.mThread = paramActivityThread;
    paramActivityThread.getLooper();
    this.mMessageQueue = Looper.myQueue();
    this.mInstrContext = paramContext1;
    this.mAppContext = paramContext2;
    this.mComponent = paramComponentName;
    this.mWatcher = paramIInstrumentationWatcher;
    this.mUiAutomationConnection = paramIUiAutomationConnection;
  }
  
  final void basicInit(ActivityThread paramActivityThread) {
    this.mThread = paramActivityThread;
  }
  
  public static void checkStartActivityResult(int paramInt, Object paramObject) {
    if (!ActivityManager.isStartResultFatalError(paramInt))
      return; 
    if (paramInt != -100) {
      if (paramInt != -99) {
        if (paramInt != -80) {
          switch (paramInt) {
            default:
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Unknown error code ");
              stringBuilder1.append(paramInt);
              stringBuilder1.append(" when starting ");
              stringBuilder1.append(paramObject);
              throw new AndroidRuntimeException(stringBuilder1.toString());
            case -89:
              throw new IllegalStateException("Session calling startAssistantActivity does not match active session");
            case -90:
              throw new IllegalStateException("Cannot start assistant activity on a hidden session");
            case -92:
            case -91:
              if (paramObject instanceof Intent && ((Intent)paramObject).getComponent() != null) {
                stringBuilder1 = new StringBuilder();
                stringBuilder1.append("Unable to find explicit activity class ");
                paramObject = paramObject;
                stringBuilder1.append(paramObject.getComponent().toShortString());
                stringBuilder1.append("; have you declared this activity in your AndroidManifest.xml?");
                throw new ActivityNotFoundException(stringBuilder1.toString());
              } 
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("No Activity found to handle ");
              stringBuilder1.append(paramObject);
              throw new ActivityNotFoundException(stringBuilder1.toString());
            case -93:
              throw new AndroidRuntimeException("FORWARD_RESULT_FLAG used while also requesting a result");
            case -94:
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Not allowed to start activity ");
              stringBuilder1.append(paramObject);
              throw new SecurityException(stringBuilder1.toString());
            case -95:
              throw new IllegalArgumentException("PendingIntent is not an activity");
            case -96:
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Activity could not be started for ");
              stringBuilder1.append(paramObject);
              throw new AndroidRuntimeException(stringBuilder1.toString());
            case -97:
              break;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Starting under voice control not allowed for: ");
          stringBuilder1.append(paramObject);
          throw new SecurityException(stringBuilder1.toString());
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("This activity start frequently, trigger frequency control: ");
        stringBuilder.append(paramObject);
        Log.w("Instrumentation", stringBuilder.toString());
        return;
      } 
      throw new IllegalStateException("Session calling startVoiceActivity does not match active session");
    } 
    throw new IllegalStateException("Cannot start voice activity on a hidden session");
  }
  
  private final void validateNotAppThread() {
    if (Looper.myLooper() != Looper.getMainLooper())
      return; 
    throw new RuntimeException("This method can not be called from the main application thread");
  }
  
  public UiAutomation getUiAutomation() {
    return getUiAutomation(0);
  }
  
  public UiAutomation getUiAutomation(int paramInt) {
    boolean bool;
    UiAutomation uiAutomation = this.mUiAutomation;
    if (uiAutomation == null || uiAutomation.isDestroyed()) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mUiAutomationConnection != null) {
      if (!bool && this.mUiAutomation.getFlags() == paramInt)
        return this.mUiAutomation; 
      if (bool) {
        this.mUiAutomation = new UiAutomation(getTargetContext().getMainLooper(), this.mUiAutomationConnection);
      } else {
        this.mUiAutomation.disconnect();
      } 
      this.mUiAutomation.connect(paramInt);
      return this.mUiAutomation;
    } 
    return null;
  }
  
  public TestLooperManager acquireLooperManager(Looper paramLooper) {
    checkInstrumenting("acquireLooperManager");
    return new TestLooperManager(paramLooper);
  }
  
  private final class InstrumentationThread extends Thread {
    final Instrumentation this$0;
    
    public InstrumentationThread(String param1String) {
      super(param1String);
    }
    
    public void run() {
      try {
        Process.setThreadPriority(-8);
      } catch (RuntimeException runtimeException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Exception setting priority of instrumentation thread ");
        stringBuilder.append(Process.myTid());
        String str = stringBuilder.toString();
        Log.w("Instrumentation", str, runtimeException);
      } 
      if (Instrumentation.this.mAutomaticPerformanceSnapshots)
        Instrumentation.this.startPerformanceSnapshot(); 
      Instrumentation.this.onStart();
    }
  }
  
  private static final class EmptyRunnable implements Runnable {
    private EmptyRunnable() {}
    
    public void run() {}
  }
  
  private static final class SyncRunnable implements Runnable {
    private boolean mComplete;
    
    private final Runnable mTarget;
    
    public SyncRunnable(Runnable param1Runnable) {
      this.mTarget = param1Runnable;
    }
    
    public void run() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mTarget : Ljava/lang/Runnable;
      //   4: invokeinterface run : ()V
      //   9: aload_0
      //   10: monitorenter
      //   11: aload_0
      //   12: iconst_1
      //   13: putfield mComplete : Z
      //   16: aload_0
      //   17: invokevirtual notifyAll : ()V
      //   20: aload_0
      //   21: monitorexit
      //   22: return
      //   23: astore_1
      //   24: aload_0
      //   25: monitorexit
      //   26: aload_1
      //   27: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2236	-> 0
      //   #2237	-> 9
      //   #2238	-> 11
      //   #2239	-> 16
      //   #2240	-> 20
      //   #2241	-> 22
      //   #2240	-> 23
      // Exception table:
      //   from	to	target	type
      //   11	16	23	finally
      //   16	20	23	finally
      //   20	22	23	finally
      //   24	26	23	finally
    }
    
    public void waitForComplete() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mComplete : Z
      //   6: istore_1
      //   7: iload_1
      //   8: ifne -> 22
      //   11: aload_0
      //   12: invokevirtual wait : ()V
      //   15: goto -> 2
      //   18: astore_2
      //   19: goto -> 15
      //   22: aload_0
      //   23: monitorexit
      //   24: return
      //   25: astore_2
      //   26: aload_0
      //   27: monitorexit
      //   28: aload_2
      //   29: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2244	-> 0
      //   #2245	-> 2
      //   #2247	-> 11
      //   #2249	-> 15
      //   #2248	-> 18
      //   #2251	-> 22
      //   #2252	-> 24
      //   #2251	-> 25
      // Exception table:
      //   from	to	target	type
      //   2	7	25	finally
      //   11	15	18	java/lang/InterruptedException
      //   11	15	25	finally
      //   22	24	25	finally
      //   26	28	25	finally
    }
  }
  
  private static final class ActivityWaiter {
    public Activity activity;
    
    public final Intent intent;
    
    public ActivityWaiter(Intent param1Intent) {
      this.intent = param1Intent;
    }
  }
  
  class ActivityGoing implements MessageQueue.IdleHandler {
    private final Instrumentation.ActivityWaiter mWaiter;
    
    final Instrumentation this$0;
    
    public ActivityGoing(Instrumentation.ActivityWaiter param1ActivityWaiter) {
      this.mWaiter = param1ActivityWaiter;
    }
    
    public final boolean queueIdle() {
      synchronized (Instrumentation.this.mSync) {
        Instrumentation.this.mWaitingActivities.remove(this.mWaiter);
        Instrumentation.this.mSync.notifyAll();
        return false;
      } 
    }
  }
  
  class Idler implements MessageQueue.IdleHandler {
    private final Runnable mCallback;
    
    private boolean mIdle;
    
    public Idler(Instrumentation this$0) {
      this.mCallback = (Runnable)this$0;
      this.mIdle = false;
    }
    
    public final boolean queueIdle() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mCallback : Ljava/lang/Runnable;
      //   4: astore_1
      //   5: aload_1
      //   6: ifnull -> 15
      //   9: aload_1
      //   10: invokeinterface run : ()V
      //   15: aload_0
      //   16: monitorenter
      //   17: aload_0
      //   18: iconst_1
      //   19: putfield mIdle : Z
      //   22: aload_0
      //   23: invokevirtual notifyAll : ()V
      //   26: aload_0
      //   27: monitorexit
      //   28: iconst_0
      //   29: ireturn
      //   30: astore_1
      //   31: aload_0
      //   32: monitorexit
      //   33: aload_1
      //   34: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2290	-> 0
      //   #2291	-> 9
      //   #2293	-> 15
      //   #2294	-> 17
      //   #2295	-> 22
      //   #2296	-> 26
      //   #2297	-> 28
      //   #2296	-> 30
      // Exception table:
      //   from	to	target	type
      //   17	22	30	finally
      //   22	26	30	finally
      //   26	28	30	finally
      //   31	33	30	finally
    }
    
    public void waitForIdle() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mIdle : Z
      //   6: istore_1
      //   7: iload_1
      //   8: ifne -> 22
      //   11: aload_0
      //   12: invokevirtual wait : ()V
      //   15: goto -> 2
      //   18: astore_2
      //   19: goto -> 15
      //   22: aload_0
      //   23: monitorexit
      //   24: return
      //   25: astore_2
      //   26: aload_0
      //   27: monitorexit
      //   28: aload_2
      //   29: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2301	-> 0
      //   #2302	-> 2
      //   #2304	-> 11
      //   #2306	-> 15
      //   #2305	-> 18
      //   #2308	-> 22
      //   #2309	-> 24
      //   #2308	-> 25
      // Exception table:
      //   from	to	target	type
      //   2	7	25	finally
      //   11	15	18	java/lang/InterruptedException
      //   11	15	25	finally
      //   22	24	25	finally
      //   26	28	25	finally
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface UiAutomationFlags {}
}
