package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IOplusKeyEventObserver;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.view.Surface;
import com.oplus.app.IOplusAppSwitchObserver;
import com.oplus.app.IOplusFreeformConfigChangedListener;
import com.oplus.app.IOplusSplitScreenObserver;
import com.oplus.app.IOplusZoomWindowConfigChangedListener;
import com.oplus.app.OplusAppInfo;
import com.oplus.app.OplusAppSwitchConfig;
import com.oplus.confinemode.IOplusConfineModeObserver;
import com.oplus.datasync.ISysStateChangeCallback;
import com.oplus.globaldrag.OplusGlobalDragAndDropRUSConfig;
import com.oplus.lockscreen.IOplusLockScreenCallback;
import com.oplus.miragewindow.IOplusMirageSessionCallback;
import com.oplus.miragewindow.IOplusMirageWindowObserver;
import com.oplus.miragewindow.IOplusMirageWindowSession;
import com.oplus.miragewindow.OplusMirageWindowInfo;
import com.oplus.zoomwindow.IOplusZoomAppObserver;
import com.oplus.zoomwindow.IOplusZoomWindowObserver;
import com.oplus.zoomwindow.OplusZoomControlViewInfo;
import com.oplus.zoomwindow.OplusZoomInputEventInfo;
import com.oplus.zoomwindow.OplusZoomWindowInfo;
import com.oplus.zoomwindow.OplusZoomWindowRUSConfig;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OplusActivityTaskManager extends OplusBaseActivityTaskManager implements IOplusActivityTaskManager {
  public static OplusActivityTaskManager getInstance() {
    return LazyHolder.INSTANCE;
  }
  
  class LazyHolder {
    private static final OplusActivityTaskManager INSTANCE = new OplusActivityTaskManager();
  }
  
  public void setSecureController(IActivityController paramIActivityController) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIActivityController != null) {
        IBinder iBinder = paramIActivityController.asBinder();
      } else {
        paramIActivityController = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIActivityController);
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public ComponentName getTopActivityComponentName() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      return ComponentName.readFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public ApplicationInfo getTopApplicationInfo() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10011, parcel1, parcel2, 0);
      parcel2.readException();
      return (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void swapDockedFullscreenStack() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10052, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getSplitScreenState(Intent paramIntent) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIntent != null) {
        parcel1.writeInt(1);
        paramIntent.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10074, parcel1, parcel2, 0);
      parcel2.readException();
      return Integer.valueOf(parcel2.readString()).intValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<OplusAppInfo> getAllTopAppInfos() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10053, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusAppInfo.CREATOR);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getFreeformConfigList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10054, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isFreeformEnabled() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10055, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean addFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusFreeformConfigChangedListener != null) {
        IBinder iBinder = paramIOplusFreeformConfigChangedListener.asBinder();
      } else {
        paramIOplusFreeformConfigChangedListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusFreeformConfigChangedListener);
      this.mRemote.transact(10056, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean removeFreeformConfigChangedListener(IOplusFreeformConfigChangedListener paramIOplusFreeformConfigChangedListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusFreeformConfigChangedListener != null) {
        IBinder iBinder = paramIOplusFreeformConfigChangedListener.asBinder();
      } else {
        paramIOplusFreeformConfigChangedListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusFreeformConfigChangedListener);
      this.mRemote.transact(10057, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerAppSwitchObserver(String paramString, IOplusAppSwitchObserver paramIOplusAppSwitchObserver, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramIOplusAppSwitchObserver != null) {
        IBinder iBinder = paramIOplusAppSwitchObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      paramOplusAppSwitchConfig.writeToParcel(parcel1, 0);
      this.mRemote.transact(10064, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterAppSwitchObserver(String paramString, OplusAppSwitchConfig paramOplusAppSwitchConfig) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      paramOplusAppSwitchConfig.writeToParcel(parcel1, 0);
      this.mRemote.transact(10065, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int startZoomWindow(Intent paramIntent, Bundle paramBundle, int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIntent != null) {
        parcel1.writeInt(1);
        paramIntent.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mRemote.transact(10068, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomWindowObserver != null) {
        IBinder iBinder = paramIOplusZoomWindowObserver.asBinder();
      } else {
        paramIOplusZoomWindowObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomWindowObserver);
      this.mRemote.transact(10069, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterZoomWindowObserver(IOplusZoomWindowObserver paramIOplusZoomWindowObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomWindowObserver != null) {
        IBinder iBinder = paramIOplusZoomWindowObserver.asBinder();
      } else {
        paramIOplusZoomWindowObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomWindowObserver);
      this.mRemote.transact(10070, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusZoomWindowInfo getCurrentZoomWindowState() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10071, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusZoomWindowInfo)OplusZoomWindowInfo.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void hideZoomWindow(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10073, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getZoomAppConfigList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new ArrayList();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10075, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<String> arrayList = new ArrayList();
      this();
      parcel2.readStringList(arrayList);
      return arrayList;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void onInputEvent(OplusZoomInputEventInfo paramOplusZoomInputEventInfo) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeParcelable((Parcelable)paramOplusZoomInputEventInfo, 0);
      this.mRemote.transact(10131, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void onControlViewChanged(OplusZoomControlViewInfo paramOplusZoomControlViewInfo) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeParcelable((Parcelable)paramOplusZoomControlViewInfo, 0);
      this.mRemote.transact(10132, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSupportZoomWindowMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10078, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSupportZoomMode(String paramString1, int paramInt, String paramString2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10135, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusZoomWindowRUSConfig getZoomWindowConfig() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new OplusZoomWindowRUSConfig();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10076, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusZoomWindowRUSConfig)parcel2.readParcelable(OplusZoomWindowRUSConfig.class.getClassLoader());
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setZoomWindowConfig(OplusZoomWindowRUSConfig paramOplusZoomWindowRUSConfig) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeParcelable((Parcelable)paramOplusZoomWindowRUSConfig, 0);
      this.mRemote.transact(10077, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean handleShowCompatibilityToast(String paramString1, int paramInt1, String paramString2, Bundle paramBundle, int paramInt2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt1);
      parcel1.writeString(paramString2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      parcel1.writeInt(paramInt2);
      this.mRemote.transact(10136, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean addZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomWindowConfigChangedListener != null) {
        IBinder iBinder = paramIOplusZoomWindowConfigChangedListener.asBinder();
      } else {
        paramIOplusZoomWindowConfigChangedListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomWindowConfigChangedListener);
      this.mRemote.transact(10079, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean removeZoomWindowConfigChangedListener(IOplusZoomWindowConfigChangedListener paramIOplusZoomWindowConfigChangedListener) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomWindowConfigChangedListener != null) {
        IBinder iBinder = paramIOplusZoomWindowConfigChangedListener.asBinder();
      } else {
        paramIOplusZoomWindowConfigChangedListener = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomWindowConfigChangedListener);
      this.mRemote.transact(10080, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void startLockDeviceMode(String paramString, String[] paramArrayOfString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      parcel1.writeStringArray(paramArrayOfString);
      this.mRemote.transact(10081, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void stopLockDeviceMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10082, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean writeEdgeTouchPreventParam(String paramString1, String paramString2, List<String> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeStringList(paramList);
      this.mRemote.transact(10083, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setDefaultEdgeTouchPreventParam(String paramString, List<String> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      parcel1.writeStringList(paramList);
      this.mRemote.transact(10084, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean resetDefaultEdgeTouchPreventParam(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10085, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSupportEdgeTouchPrevent() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10086, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setEdgeTouchCallRules(String paramString, Map<String, List<String>> paramMap) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      parcel1.writeMap(paramMap);
      this.mRemote.transact(10087, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int splitScreenForEdgePanel(Intent paramIntent, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      paramIntent.writeToParcel(parcel1, 0);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10088, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = Integer.valueOf(parcel2.readString()).intValue();
      return paramInt;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setConfineMode(int paramInt, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mRemote.transact(10089, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int getConfineMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10090, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readInt();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setPermitList(int paramInt1, int paramInt2, List<String> paramList, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeStringList(paramList);
      parcel1.writeString(String.valueOf(paramBoolean));
      this.mRemote.transact(10091, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setGimbalLaunchPkg(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10092, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setPackagesState(Map<String, Integer> paramMap) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeMap(paramMap);
      this.mRemote.transact(10093, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusLockScreenCallback != null) {
        IBinder iBinder = paramIOplusLockScreenCallback.asBinder();
      } else {
        paramIOplusLockScreenCallback = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusLockScreenCallback);
      this.mRemote.transact(10094, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterLockScreenCallback(IOplusLockScreenCallback paramIOplusLockScreenCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusLockScreenCallback != null) {
        IBinder iBinder = paramIOplusLockScreenCallback.asBinder();
      } else {
        paramIOplusLockScreenCallback = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusLockScreenCallback);
      this.mRemote.transact(10095, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void notifySplitScreenStateChanged(String paramString, Bundle paramBundle, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      parcel1.writeBundle(paramBundle);
      parcel1.writeBoolean(paramBoolean);
      this.mRemote.transact(10096, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean setSplitScreenObserver(IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusSplitScreenObserver != null) {
        IBinder iBinder = paramIOplusSplitScreenObserver.asBinder();
      } else {
        paramIOplusSplitScreenObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusSplitScreenObserver);
      this.mRemote.transact(10097, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isInSplitScreenMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10098, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean dismissSplitScreenMode(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10099, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      if (paramIOplusSplitScreenObserver != null) {
        IBinder iBinder = paramIOplusSplitScreenObserver.asBinder();
      } else {
        paramIOplusSplitScreenObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusSplitScreenObserver);
      this.mRemote.transact(10100, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterSplitScreenObserver(int paramInt, IOplusSplitScreenObserver paramIOplusSplitScreenObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      if (paramIOplusSplitScreenObserver != null) {
        IBinder iBinder = paramIOplusSplitScreenObserver.asBinder();
      } else {
        paramIOplusSplitScreenObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusSplitScreenObserver);
      this.mRemote.transact(10101, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public Bundle getSplitScreenStatus(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10102, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBundle();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean splitScreenForTopApp(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10128, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean splitScreenForRecentTasks(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10129, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerKeyEventObserver(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramIOplusKeyEventObserver != null) {
        IBinder iBinder = paramIOplusKeyEventObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10103, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterKeyEventObserver(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10104, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean updateAppData(String paramString, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      parcel1.writeBundle(paramBundle);
      this.mRemote.transact(10107, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramISysStateChangeCallback != null) {
        IBinder iBinder = paramISysStateChangeCallback.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10108, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterSysStateChangeObserver(String paramString, ISysStateChangeCallback paramISysStateChangeCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramISysStateChangeCallback != null) {
        IBinder iBinder = paramISysStateChangeCallback.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10109, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerKeyEventInterceptor(String paramString, IOplusKeyEventObserver paramIOplusKeyEventObserver, Map<Integer, Integer> paramMap) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      if (paramIOplusKeyEventObserver != null) {
        IBinder iBinder = paramIOplusKeyEventObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      parcel1.writeMap(paramMap);
      this.mRemote.transact(10105, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterKeyEventInterceptor(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10106, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusGlobalDragAndDropRUSConfig getGlobalDragAndDropConfig() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new OplusGlobalDragAndDropRUSConfig();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10110, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusGlobalDragAndDropRUSConfig)parcel2.readParcelable(OplusGlobalDragAndDropRUSConfig.class.getClassLoader());
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setGlobalDragAndDropConfig(OplusGlobalDragAndDropRUSConfig paramOplusGlobalDragAndDropRUSConfig) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeParcelable((Parcelable)paramOplusGlobalDragAndDropRUSConfig, 0);
      this.mRemote.transact(10111, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusMirageWindowObserver != null) {
        IBinder iBinder = paramIOplusMirageWindowObserver.asBinder();
      } else {
        paramIOplusMirageWindowObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusMirageWindowObserver);
      this.mRemote.transact(10112, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterMirageWindowObserver(IOplusMirageWindowObserver paramIOplusMirageWindowObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusMirageWindowObserver != null) {
        IBinder iBinder = paramIOplusMirageWindowObserver.asBinder();
      } else {
        paramIOplusMirageWindowObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusMirageWindowObserver);
      this.mRemote.transact(10113, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public int createMirageDisplay(Surface paramSurface) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeParcelable((Parcelable)paramSurface, 0);
      this.mRemote.transact(10114, parcel1, parcel2, 0);
      parcel2.readException();
      return Integer.valueOf(parcel2.readString()).intValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void startMirageWindowMode(ComponentName paramComponentName, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      ComponentName.writeToParcel(paramComponentName, parcel1);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10115, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isMirageWindowShow() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10116, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void stopMirageWindowMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10117, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void expandToFullScreen() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10118, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void setMirageWindowSilent(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10119, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isSupportMirageWindowMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10120, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public OplusMirageWindowInfo getMirageWindowInfo() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    new OplusMirageWindowInfo();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      this.mRemote.transact(10121, parcel1, parcel2, 0);
      parcel2.readException();
      return (OplusMirageWindowInfo)OplusMirageWindowInfo.CREATOR.createFromParcel(parcel2);
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean updateMirageWindowCastFlag(int paramInt, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10124, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readBoolean();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeStringList(paramList);
      parcel1.writeBoolean(paramBoolean);
      this.mRemote.transact(10125, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean = parcel2.readBoolean();
      return paramBoolean;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean updatePrivacyProtectionList(List<String> paramList, boolean paramBoolean1, boolean paramBoolean2, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeStringList(paramList);
      parcel1.writeBoolean(paramBoolean1);
      parcel1.writeBoolean(paramBoolean2);
      if (paramBundle != null) {
        parcel1.writeInt(1);
        parcel1.writeBundle(paramBundle);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10138, parcel1, parcel2, 0);
      parcel2.readException();
      paramBoolean1 = parcel2.readBoolean();
      return paramBoolean1;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public IOplusMirageWindowSession createMirageWindowSession(IOplusMirageSessionCallback paramIOplusMirageSessionCallback) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusMirageSessionCallback != null) {
        IBinder iBinder = paramIOplusMirageSessionCallback.asBinder();
      } else {
        paramIOplusMirageSessionCallback = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusMirageSessionCallback);
      this.mRemote.transact(10137, parcel1, parcel2, 0);
      parcel2.readException();
      return IOplusMirageWindowSession.Stub.asInterface(parcel2.readStrongBinder());
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusConfineModeObserver != null) {
        IBinder iBinder = paramIOplusConfineModeObserver.asBinder();
      } else {
        paramIOplusConfineModeObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusConfineModeObserver);
      this.mRemote.transact(10122, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterConfineModeObserver(IOplusConfineModeObserver paramIOplusConfineModeObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusConfineModeObserver != null) {
        IBinder iBinder = paramIOplusConfineModeObserver.asBinder();
      } else {
        paramIOplusConfineModeObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusConfineModeObserver);
      this.mRemote.transact(10123, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public String readNodeFile(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10126, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.readString();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean writeNodeFile(int paramInt, String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      this.mRemote.transact(10127, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public void clientTransactionComplete(IBinder paramIBinder, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      parcel1.writeStrongBinder(paramIBinder);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10130, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean registerZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomAppObserver != null) {
        IBinder iBinder = paramIOplusZoomAppObserver.asBinder();
      } else {
        paramIOplusZoomAppObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomAppObserver);
      this.mRemote.transact(10133, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean unregisterZoomAppObserver(IOplusZoomAppObserver paramIOplusZoomAppObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IActivityTaskManager");
      if (paramIOplusZoomAppObserver != null) {
        IBinder iBinder = paramIOplusZoomAppObserver.asBinder();
      } else {
        paramIOplusZoomAppObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIOplusZoomAppObserver);
      this.mRemote.transact(10134, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
