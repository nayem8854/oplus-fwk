package android.app;

import android.annotation.SystemApi;

@SystemApi
public abstract class VrStateCallback {
  public void onPersistentVrStateChanged(boolean paramBoolean) {}
  
  public void onVrStateChanged(boolean paramBoolean) {}
}
