package android.app;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Xml;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@Deprecated
public class AliasActivity extends Activity {
  public final String ALIAS_META_DATA = "android.app.alias";
  
  protected void onCreate(Bundle paramBundle) {
    XmlResourceParser xmlResourceParser1;
    RuntimeException runtimeException;
    super.onCreate(paramBundle);
    XmlResourceParser xmlResourceParser2 = null, xmlResourceParser3 = null, xmlResourceParser4 = null;
    Bundle bundle = null;
    paramBundle = bundle;
    XmlResourceParser xmlResourceParser5 = xmlResourceParser2, xmlResourceParser6 = xmlResourceParser3, xmlResourceParser7 = xmlResourceParser4;
    try {
      PackageManager packageManager = getPackageManager();
      paramBundle = bundle;
      xmlResourceParser5 = xmlResourceParser2;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser4;
      ComponentName componentName = getComponentName();
      paramBundle = bundle;
      xmlResourceParser5 = xmlResourceParser2;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser4;
      ActivityInfo activityInfo = packageManager.getActivityInfo(componentName, 128);
      paramBundle = bundle;
      xmlResourceParser5 = xmlResourceParser2;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser4;
      xmlResourceParser3 = activityInfo.loadXmlMetaData(getPackageManager(), "android.app.alias");
      if (xmlResourceParser3 != null) {
        XmlResourceParser xmlResourceParser = xmlResourceParser3;
        xmlResourceParser5 = xmlResourceParser3;
        xmlResourceParser6 = xmlResourceParser3;
        xmlResourceParser7 = xmlResourceParser3;
        Intent intent = parseAlias(xmlResourceParser3);
        if (intent != null) {
          xmlResourceParser = xmlResourceParser3;
          xmlResourceParser5 = xmlResourceParser3;
          xmlResourceParser6 = xmlResourceParser3;
          xmlResourceParser7 = xmlResourceParser3;
          startActivity(intent);
          xmlResourceParser = xmlResourceParser3;
          xmlResourceParser5 = xmlResourceParser3;
          xmlResourceParser6 = xmlResourceParser3;
          xmlResourceParser7 = xmlResourceParser3;
          finish();
          if (xmlResourceParser3 != null)
            xmlResourceParser3.close(); 
          return;
        } 
        xmlResourceParser = xmlResourceParser3;
        xmlResourceParser5 = xmlResourceParser3;
        xmlResourceParser6 = xmlResourceParser3;
        xmlResourceParser7 = xmlResourceParser3;
        RuntimeException runtimeException2 = new RuntimeException();
        xmlResourceParser = xmlResourceParser3;
        xmlResourceParser5 = xmlResourceParser3;
        xmlResourceParser6 = xmlResourceParser3;
        xmlResourceParser7 = xmlResourceParser3;
        this("No <intent> tag found in alias description");
        xmlResourceParser = xmlResourceParser3;
        xmlResourceParser5 = xmlResourceParser3;
        xmlResourceParser6 = xmlResourceParser3;
        xmlResourceParser7 = xmlResourceParser3;
        throw runtimeException2;
      } 
      xmlResourceParser1 = xmlResourceParser3;
      xmlResourceParser5 = xmlResourceParser3;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser3;
      RuntimeException runtimeException1 = new RuntimeException();
      xmlResourceParser1 = xmlResourceParser3;
      xmlResourceParser5 = xmlResourceParser3;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser3;
      this("Alias requires a meta-data field android.app.alias");
      xmlResourceParser1 = xmlResourceParser3;
      xmlResourceParser5 = xmlResourceParser3;
      xmlResourceParser6 = xmlResourceParser3;
      xmlResourceParser7 = xmlResourceParser3;
      throw runtimeException1;
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      xmlResourceParser1 = xmlResourceParser7;
      runtimeException = new RuntimeException();
      xmlResourceParser1 = xmlResourceParser7;
      this("Error parsing alias", (Throwable)nameNotFoundException);
      xmlResourceParser1 = xmlResourceParser7;
      throw runtimeException;
    } catch (XmlPullParserException xmlPullParserException) {
      RuntimeException runtimeException1 = runtimeException, runtimeException2 = new RuntimeException();
      runtimeException1 = runtimeException;
      this("Error parsing alias", (Throwable)xmlPullParserException);
      runtimeException1 = runtimeException;
      throw runtimeException2;
    } catch (IOException iOException) {
      xmlResourceParser1 = xmlResourceParser5;
      runtimeException = new RuntimeException();
      xmlResourceParser1 = xmlResourceParser5;
      this("Error parsing alias", iOException);
      xmlResourceParser1 = xmlResourceParser5;
      throw runtimeException;
    } finally {}
    if (xmlResourceParser1 != null)
      xmlResourceParser1.close(); 
    throw xmlResourceParser3;
  }
  
  private Intent parseAlias(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    Intent intent;
    AttributeSet attributeSet = Xml.asAttributeSet(paramXmlPullParser);
    String str1 = null;
    while (true) {
      int i = paramXmlPullParser.next();
      if (i != 1 && i != 2)
        continue; 
      break;
    } 
    String str2 = paramXmlPullParser.getName();
    if ("alias".equals(str2)) {
      Intent intent1;
      int i = paramXmlPullParser.getDepth();
      while (true) {
        int j = paramXmlPullParser.next();
        if (j != 1 && (j != 3 || 
          paramXmlPullParser.getDepth() > i)) {
          if (j == 3 || j == 4)
            continue; 
          str2 = paramXmlPullParser.getName();
          if ("intent".equals(str2)) {
            Intent intent2 = Intent.parseIntent(getResources(), paramXmlPullParser, attributeSet);
            str2 = str1;
            if (str1 == null)
              intent = intent2; 
            intent1 = intent;
            continue;
          } 
          XmlUtils.skipCurrentTag(paramXmlPullParser);
          continue;
        } 
        break;
      } 
      return intent1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Alias meta-data must start with <alias> tag; found");
    stringBuilder.append((String)intent);
    stringBuilder.append(" at ");
    stringBuilder.append(paramXmlPullParser.getPositionDescription());
    throw new RuntimeException(stringBuilder.toString());
  }
}
