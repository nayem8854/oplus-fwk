package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IRemoteCallback;
import android.os.Parcel;
import android.os.RemoteException;

public interface IEphemeralResolver extends IInterface {
  void getEphemeralIntentFilterList(IRemoteCallback paramIRemoteCallback, String paramString, int paramInt) throws RemoteException;
  
  void getEphemeralResolveInfoList(IRemoteCallback paramIRemoteCallback, int[] paramArrayOfint, int paramInt) throws RemoteException;
  
  class Default implements IEphemeralResolver {
    public void getEphemeralResolveInfoList(IRemoteCallback param1IRemoteCallback, int[] param1ArrayOfint, int param1Int) throws RemoteException {}
    
    public void getEphemeralIntentFilterList(IRemoteCallback param1IRemoteCallback, String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IEphemeralResolver {
    private static final String DESCRIPTOR = "android.app.IEphemeralResolver";
    
    static final int TRANSACTION_getEphemeralIntentFilterList = 2;
    
    static final int TRANSACTION_getEphemeralResolveInfoList = 1;
    
    public Stub() {
      attachInterface(this, "android.app.IEphemeralResolver");
    }
    
    public static IEphemeralResolver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IEphemeralResolver");
      if (iInterface != null && iInterface instanceof IEphemeralResolver)
        return (IEphemeralResolver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getEphemeralIntentFilterList";
      } 
      return "getEphemeralResolveInfoList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.app.IEphemeralResolver");
          return true;
        } 
        param1Parcel1.enforceInterface("android.app.IEphemeralResolver");
        IRemoteCallback iRemoteCallback1 = IRemoteCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
        String str = param1Parcel1.readString();
        param1Int1 = param1Parcel1.readInt();
        getEphemeralIntentFilterList(iRemoteCallback1, str, param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.app.IEphemeralResolver");
      IRemoteCallback iRemoteCallback = IRemoteCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
      int[] arrayOfInt = param1Parcel1.createIntArray();
      param1Int1 = param1Parcel1.readInt();
      getEphemeralResolveInfoList(iRemoteCallback, arrayOfInt, param1Int1);
      return true;
    }
    
    private static class Proxy implements IEphemeralResolver {
      public static IEphemeralResolver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IEphemeralResolver";
      }
      
      public void getEphemeralResolveInfoList(IRemoteCallback param2IRemoteCallback, int[] param2ArrayOfint, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IEphemeralResolver");
          if (param2IRemoteCallback != null) {
            iBinder = param2IRemoteCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeIntArray(param2ArrayOfint);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IEphemeralResolver.Stub.getDefaultImpl() != null) {
            IEphemeralResolver.Stub.getDefaultImpl().getEphemeralResolveInfoList(param2IRemoteCallback, param2ArrayOfint, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void getEphemeralIntentFilterList(IRemoteCallback param2IRemoteCallback, String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.app.IEphemeralResolver");
          if (param2IRemoteCallback != null) {
            iBinder = param2IRemoteCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IEphemeralResolver.Stub.getDefaultImpl() != null) {
            IEphemeralResolver.Stub.getDefaultImpl().getEphemeralIntentFilterList(param2IRemoteCallback, param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IEphemeralResolver param1IEphemeralResolver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IEphemeralResolver != null) {
          Proxy.sDefaultImpl = param1IEphemeralResolver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IEphemeralResolver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
