package android.app;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.util.Log;
import com.android.internal.util.FastPrintWriter;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicLong;

public abstract class PropertyInvalidatedCache<Query, Result> {
  private long mHits = 0L;
  
  private long mMisses = 0L;
  
  private static final HashMap<String, Long> sInvalidates = new HashMap<>();
  
  static {
    sEnabled = true;
    sCorkLock = new Object();
    sCorks = new HashMap<>();
    sCaches = new WeakHashMap<>();
  }
  
  private final Object mLock = new Object();
  
  private long mLastSeenNonce = 0L;
  
  private boolean mDisabled = false;
  
  private static final boolean DEBUG = false;
  
  private static final long NONCE_DISABLED = -1L;
  
  private static final long NONCE_UNSET = 0L;
  
  private static final String TAG = "PropertyInvalidatedCache";
  
  private static final boolean VERIFY = false;
  
  private static final WeakHashMap<PropertyInvalidatedCache, Void> sCaches;
  
  private static final Object sCorkLock;
  
  private static final HashMap<String, Integer> sCorks;
  
  private static boolean sEnabled;
  
  private final LinkedHashMap<Query, Result> mCache;
  
  private final int mMaxEntries;
  
  private volatile SystemProperties.Handle mPropertyHandle;
  
  private final String mPropertyName;
  
  public PropertyInvalidatedCache(final int maxEntries, String paramString) {
    this.mPropertyName = paramString;
    this.mMaxEntries = maxEntries;
    this.mCache = new LinkedHashMap<Query, Result>(2, 0.75F, true) {
        final PropertyInvalidatedCache this$0;
        
        final int val$maxEntries;
        
        protected boolean removeEldestEntry(Map.Entry param1Entry) {
          boolean bool;
          if (size() > maxEntries) {
            bool = true;
          } else {
            bool = false;
          } 
          return bool;
        }
      };
    synchronized (sCorkLock) {
      sCaches.put(this, null);
      sInvalidates.put(paramString, Long.valueOf(0L));
      return;
    } 
  }
  
  public final void clear() {
    synchronized (this.mLock) {
      this.mCache.clear();
      return;
    } 
  }
  
  protected boolean debugCompareQueryResults(Result paramResult1, Result paramResult2) {
    if (paramResult2 != null)
      return Objects.equals(paramResult1, paramResult2); 
    return true;
  }
  
  protected Result refresh(Result paramResult, Query paramQuery) {
    return paramResult;
  }
  
  private long getCurrentNonce() {
    SystemProperties.Handle handle1 = this.mPropertyHandle;
    SystemProperties.Handle handle2 = handle1;
    if (handle1 == null) {
      handle2 = SystemProperties.find(this.mPropertyName);
      if (handle2 == null)
        return 0L; 
      this.mPropertyHandle = handle2;
    } 
    return handle2.getLong(0L);
  }
  
  public final void disableLocal() {
    synchronized (this.mLock) {
      this.mDisabled = true;
      this.mCache.clear();
      return;
    } 
  }
  
  public final boolean isDisabledLocal() {
    return (this.mDisabled || !sEnabled);
  }
  
  public Result query(Query paramQuery) {
    long l;
    if (!isDisabledLocal()) {
      l = getCurrentNonce();
    } else {
      l = -1L;
    } 
    while (l != -1L && l != 0L) {
      synchronized (this.mLock) {
        if (l == this.mLastSeenNonce) {
          Result result1 = this.mCache.get(paramQuery);
          Result result2 = result1;
          if (result1 != null) {
            this.mHits++;
            Result result3 = result1;
          } 
        } else {
          this.mCache.clear();
          this.mLastSeenNonce = l;
          null = null;
        } 
        if (null != null) {
          Result result1 = refresh(null, paramQuery);
          if (result1 != null) {
            long l1 = getCurrentNonce();
            if (l != l1) {
              l = l1;
              continue;
            } 
            synchronized (this.mLock) {
              if (l == this.mLastSeenNonce)
                if (result1 == null) {
                  this.mCache.remove(paramQuery);
                } else {
                  this.mCache.put(paramQuery, result1);
                }  
              return maybeCheckConsistency(paramQuery, result1);
            } 
          } 
          return maybeCheckConsistency(paramQuery, null);
        } 
        Result result = recompute(paramQuery);
        synchronized (this.mLock) {
          if (this.mLastSeenNonce == l && result != null)
            this.mCache.put(paramQuery, result); 
          this.mMisses++;
          return maybeCheckConsistency(paramQuery, result);
        } 
      } 
    } 
    return recompute(paramQuery);
  }
  
  private static final class NoPreloadHolder {
    private static final AtomicLong sNextNonce = new AtomicLong((new Random()).nextLong());
    
    public static long next() {
      return sNextNonce.getAndIncrement();
    }
  }
  
  public final void disableSystemWide() {
    disableSystemWide(this.mPropertyName);
  }
  
  public static void disableSystemWide(String paramString) {
    if (!sEnabled)
      return; 
    SystemProperties.set(paramString, Long.toString(-1L));
  }
  
  public final void invalidateCache() {
    invalidateCache(this.mPropertyName);
  }
  
  public static void invalidateCache(String paramString) {
    if (!sEnabled)
      return; 
    synchronized (sCorkLock) {
      Integer integer = sCorks.get(paramString);
      if (integer != null && integer.intValue() > 0)
        return; 
      invalidateCacheLocked(paramString);
      return;
    } 
  }
  
  private static void invalidateCacheLocked(String paramString) {
    long l = SystemProperties.getLong(paramString, 0L);
    if (l == -1L)
      return; 
    while (true) {
      l = NoPreloadHolder.next();
      if (l != 0L && l != -1L) {
        String str = Long.toString(l);
        SystemProperties.set(paramString, str);
        l = ((Long)sInvalidates.getOrDefault(paramString, Long.valueOf(0L))).longValue();
        sInvalidates.put(paramString, Long.valueOf(1L + l));
        return;
      } 
    } 
  }
  
  public static void corkInvalidations(String paramString) {
    synchronized (sCorkLock) {
      int i = ((Integer)sCorks.getOrDefault(paramString, Integer.valueOf(0))).intValue();
      if (i == 0) {
        long l = SystemProperties.getLong(paramString, 0L);
        if (l != 0L && l != -1L)
          try {
            SystemProperties.set(paramString, Long.toString(0L));
          } catch (RuntimeException runtimeException) {
            StringBuilder stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("set property fail, name:");
            stringBuilder.append(paramString);
            Log.d("PropertyInvalidatedCache", stringBuilder.toString());
          }  
      } 
      sCorks.put(paramString, Integer.valueOf(i + 1));
      return;
    } 
  }
  
  public static void uncorkInvalidations(String paramString) {
    synchronized (sCorkLock) {
      int i = ((Integer)sCorks.getOrDefault(paramString, Integer.valueOf(0))).intValue();
      if (i >= 1) {
        if (i == 1) {
          sCorks.remove(paramString);
          invalidateCacheLocked(paramString);
        } else {
          sCorks.put(paramString, Integer.valueOf(i - 1));
        } 
        return;
      } 
      AssertionError assertionError = new AssertionError();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("cork underflow: ");
      stringBuilder.append(paramString);
      this(stringBuilder.toString());
      throw assertionError;
    } 
  }
  
  public static final class AutoCorker {
    public static final int DEFAULT_AUTO_CORK_DELAY_MS = 2000;
    
    private final int mAutoCorkDelayMs;
    
    private Handler mHandler;
    
    private final Object mLock = new Object();
    
    private final String mPropertyName;
    
    private long mUncorkDeadlineMs = -1L;
    
    public AutoCorker(String param1String) {
      this(param1String, 2000);
    }
    
    public AutoCorker(String param1String, int param1Int) {
      this.mPropertyName = param1String;
      this.mAutoCorkDelayMs = param1Int;
    }
    
    public void autoCork() {
      if (Looper.getMainLooper() == null) {
        PropertyInvalidatedCache.invalidateCache(this.mPropertyName);
        return;
      } 
      synchronized (this.mLock) {
        boolean bool;
        if (this.mUncorkDeadlineMs >= 0L) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mUncorkDeadlineMs = SystemClock.uptimeMillis() + this.mAutoCorkDelayMs;
        if (!bool) {
          getHandlerLocked().sendEmptyMessageAtTime(0, this.mUncorkDeadlineMs);
          PropertyInvalidatedCache.corkInvalidations(this.mPropertyName);
        } 
        return;
      } 
    }
    
    private void handleMessage(Message param1Message) {
      synchronized (this.mLock) {
        if (this.mUncorkDeadlineMs < 0L)
          return; 
        long l = SystemClock.uptimeMillis();
        if (this.mUncorkDeadlineMs > l) {
          this.mUncorkDeadlineMs = this.mAutoCorkDelayMs + l;
          getHandlerLocked().sendEmptyMessageAtTime(0, this.mUncorkDeadlineMs);
          return;
        } 
        this.mUncorkDeadlineMs = -1L;
        PropertyInvalidatedCache.uncorkInvalidations(this.mPropertyName);
        return;
      } 
    }
    
    private Handler getHandlerLocked() {
      if (this.mHandler == null)
        this.mHandler = (Handler)new Object(this, Looper.getMainLooper()); 
      return this.mHandler;
    }
  }
  
  protected Result maybeCheckConsistency(Query paramQuery, Result paramResult) {
    return paramResult;
  }
  
  public String cacheName() {
    return this.mPropertyName;
  }
  
  public String queryToString(Query paramQuery) {
    return Objects.toString(paramQuery);
  }
  
  public static void disableForTestMode() {
    Log.d("PropertyInvalidatedCache", "disabling all caches in the process");
    sEnabled = false;
  }
  
  public static ArrayList<PropertyInvalidatedCache> getActiveCaches() {
    synchronized (sCorkLock) {
      ArrayList<PropertyInvalidatedCache> arrayList = new ArrayList();
      this((Collection)sCaches.keySet());
      return arrayList;
    } 
  }
  
  public static ArrayList<Map.Entry<String, Integer>> getActiveCorks() {
    synchronized (sCorkLock) {
      ArrayList<Map.Entry<String, Integer>> arrayList = new ArrayList();
      this((Collection)sCorks.entrySet());
      return arrayList;
    } 
  }
  
  private void dumpContents(PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    synchronized (sCorkLock) {
      long l = ((Long)sInvalidates.getOrDefault(this.mPropertyName, Long.valueOf(0L))).longValue();
      synchronized (this.mLock) {
        paramPrintWriter.println(String.format("  Cache Property Name: %s", new Object[] { cacheName() }));
        long l1 = this.mHits;
        long l2 = this.mMisses;
        paramPrintWriter.println(String.format("    Hits: %d, Misses: %d, Invalidates: %d", new Object[] { Long.valueOf(l1), Long.valueOf(l2), Long.valueOf(l) }));
        paramPrintWriter.println(String.format("    Last Observed Nonce: %d", new Object[] { Long.valueOf(this.mLastSeenNonce) }));
        null = (Object<Query, Result>)this.mCache;
        int i = null.entrySet().size(), j = this.mMaxEntries;
        paramPrintWriter.println(String.format("    Current Size: %d, Max Size: %d", new Object[] { Integer.valueOf(i), Integer.valueOf(j) }));
        if (this.mDisabled) {
          null = (Object<Query, Result>)"false";
        } else {
          null = (Object<Query, Result>)"true";
        } 
        paramPrintWriter.println(String.format("    Enabled: %s", new Object[] { null }));
        null = (Object)this.mCache.entrySet();
        if (null.size() == 0) {
          paramPrintWriter.println("");
          return;
        } 
        paramPrintWriter.println("");
        paramPrintWriter.println("    Contents:");
        for (Map.Entry entry : null) {
          String str2 = Objects.toString(entry.getKey());
          String str1 = Objects.toString(entry.getValue());
          paramPrintWriter.println(String.format("      Key: %s\n      Value: %s\n", new Object[] { str2, str1 }));
        } 
        return;
      } 
    } 
  }
  
  public static void dumpCacheInfo(FileDescriptor paramFileDescriptor, String[] paramArrayOfString) {
    try {
      FileOutputStream fileOutputStream = new FileOutputStream();
      this(paramFileDescriptor);
      try {
        FastPrintWriter fastPrintWriter = new FastPrintWriter();
        this(fileOutputStream);
      } finally {
        try {
          fileOutputStream.close();
        } finally {
          paramArrayOfString = null;
        } 
      } 
    } catch (IOException iOException) {
      Log.e("PropertyInvalidatedCache", "Failed to dump PropertyInvalidatedCache instances");
    } 
  }
  
  protected abstract Result recompute(Query paramQuery);
}
