package android.app;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.WorkSource;

public interface IAlarmManager extends IInterface {
  long currentNetworkTimeMillis() throws RemoteException;
  
  AlarmManager.AlarmClockInfo getNextAlarmClock(int paramInt) throws RemoteException;
  
  long getNextWakeFromIdleTime() throws RemoteException;
  
  void remove(PendingIntent paramPendingIntent, IAlarmListener paramIAlarmListener) throws RemoteException;
  
  void set(String paramString1, int paramInt1, long paramLong1, long paramLong2, long paramLong3, int paramInt2, PendingIntent paramPendingIntent, IAlarmListener paramIAlarmListener, String paramString2, WorkSource paramWorkSource, AlarmManager.AlarmClockInfo paramAlarmClockInfo) throws RemoteException;
  
  void setHighTemp(boolean paramBoolean) throws RemoteException;
  
  boolean setTime(long paramLong) throws RemoteException;
  
  void setTimeZone(String paramString) throws RemoteException;
  
  class Default implements IAlarmManager {
    public void set(String param1String1, int param1Int1, long param1Long1, long param1Long2, long param1Long3, int param1Int2, PendingIntent param1PendingIntent, IAlarmListener param1IAlarmListener, String param1String2, WorkSource param1WorkSource, AlarmManager.AlarmClockInfo param1AlarmClockInfo) throws RemoteException {}
    
    public boolean setTime(long param1Long) throws RemoteException {
      return false;
    }
    
    public void setTimeZone(String param1String) throws RemoteException {}
    
    public void remove(PendingIntent param1PendingIntent, IAlarmListener param1IAlarmListener) throws RemoteException {}
    
    public long getNextWakeFromIdleTime() throws RemoteException {
      return 0L;
    }
    
    public AlarmManager.AlarmClockInfo getNextAlarmClock(int param1Int) throws RemoteException {
      return null;
    }
    
    public long currentNetworkTimeMillis() throws RemoteException {
      return 0L;
    }
    
    public void setHighTemp(boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAlarmManager {
    private static final String DESCRIPTOR = "android.app.IAlarmManager";
    
    static final int TRANSACTION_currentNetworkTimeMillis = 7;
    
    static final int TRANSACTION_getNextAlarmClock = 6;
    
    static final int TRANSACTION_getNextWakeFromIdleTime = 5;
    
    static final int TRANSACTION_remove = 4;
    
    static final int TRANSACTION_set = 1;
    
    static final int TRANSACTION_setHighTemp = 8;
    
    static final int TRANSACTION_setTime = 2;
    
    static final int TRANSACTION_setTimeZone = 3;
    
    public Stub() {
      attachInterface(this, "android.app.IAlarmManager");
    }
    
    public static IAlarmManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IAlarmManager");
      if (iInterface != null && iInterface instanceof IAlarmManager)
        return (IAlarmManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "setHighTemp";
        case 7:
          return "currentNetworkTimeMillis";
        case 6:
          return "getNextAlarmClock";
        case 5:
          return "getNextWakeFromIdleTime";
        case 4:
          return "remove";
        case 3:
          return "setTimeZone";
        case 2:
          return "setTime";
        case 1:
          break;
      } 
      return "set";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        AlarmManager.AlarmClockInfo alarmClockInfo;
        IAlarmListener iAlarmListener1;
        String str1;
        PendingIntent pendingIntent;
        WorkSource workSource;
        boolean bool1 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.app.IAlarmManager");
            if (param1Parcel1.readInt() != 0)
              bool1 = true; 
            setHighTemp(bool1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.app.IAlarmManager");
            l1 = currentNetworkTimeMillis();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.app.IAlarmManager");
            param1Int1 = param1Parcel1.readInt();
            alarmClockInfo = getNextAlarmClock(param1Int1);
            param1Parcel2.writeNoException();
            if (alarmClockInfo != null) {
              param1Parcel2.writeInt(1);
              alarmClockInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            alarmClockInfo.enforceInterface("android.app.IAlarmManager");
            l1 = getNextWakeFromIdleTime();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l1);
            return true;
          case 4:
            alarmClockInfo.enforceInterface("android.app.IAlarmManager");
            if (alarmClockInfo.readInt() != 0) {
              pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)alarmClockInfo);
            } else {
              pendingIntent = null;
            } 
            iAlarmListener1 = IAlarmListener.Stub.asInterface(alarmClockInfo.readStrongBinder());
            remove(pendingIntent, iAlarmListener1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iAlarmListener1.enforceInterface("android.app.IAlarmManager");
            str1 = iAlarmListener1.readString();
            setTimeZone(str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.app.IAlarmManager");
            l1 = str1.readLong();
            bool = setTime(l1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.app.IAlarmManager");
        String str2 = str1.readString();
        param1Int2 = str1.readInt();
        long l2 = str1.readLong();
        long l3 = str1.readLong();
        long l1 = str1.readLong();
        int i = str1.readInt();
        if (str1.readInt() != 0) {
          pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel((Parcel)str1);
        } else {
          pendingIntent = null;
        } 
        IAlarmListener iAlarmListener2 = IAlarmListener.Stub.asInterface(str1.readStrongBinder());
        String str3 = str1.readString();
        if (str1.readInt() != 0) {
          workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str1);
        } else {
          workSource = null;
        } 
        if (str1.readInt() != 0) {
          AlarmManager.AlarmClockInfo alarmClockInfo1 = (AlarmManager.AlarmClockInfo)AlarmManager.AlarmClockInfo.CREATOR.createFromParcel((Parcel)str1);
        } else {
          str1 = null;
        } 
        set(str2, param1Int2, l2, l3, l1, i, pendingIntent, iAlarmListener2, str3, workSource, (AlarmManager.AlarmClockInfo)str1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.app.IAlarmManager");
      return true;
    }
    
    private static class Proxy implements IAlarmManager {
      public static IAlarmManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IAlarmManager";
      }
      
      public void set(String param2String1, int param2Int1, long param2Long1, long param2Long2, long param2Long3, int param2Int2, PendingIntent param2PendingIntent, IAlarmListener param2IAlarmListener, String param2String2, WorkSource param2WorkSource, AlarmManager.AlarmClockInfo param2AlarmClockInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeLong(param2Long1);
          parcel1.writeLong(param2Long2);
          parcel1.writeLong(param2Long3);
          parcel1.writeInt(param2Int2);
          if (param2PendingIntent != null) {
            try {
              parcel1.writeInt(1);
              param2PendingIntent.writeToParcel(parcel1, 0);
            } finally {}
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IAlarmListener != null) {
            iBinder = param2IAlarmListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String2);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2AlarmClockInfo != null) {
            parcel1.writeInt(1);
            param2AlarmClockInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null) {
            IAlarmManager iAlarmManager = IAlarmManager.Stub.getDefaultImpl();
            try {
              iAlarmManager.set(param2String1, param2Int1, param2Long1, param2Long2, param2Long3, param2Int2, param2PendingIntent, param2IAlarmListener, param2String2, param2WorkSource, param2AlarmClockInfo);
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } else {
            parcel2.readException();
            parcel2.recycle();
            parcel1.recycle();
            return;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean setTime(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          parcel1.writeLong(param2Long);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IAlarmManager.Stub.getDefaultImpl() != null) {
            bool1 = IAlarmManager.Stub.getDefaultImpl().setTime(param2Long);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTimeZone(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null) {
            IAlarmManager.Stub.getDefaultImpl().setTimeZone(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void remove(PendingIntent param2PendingIntent, IAlarmListener param2IAlarmListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IAlarmListener != null) {
            iBinder = param2IAlarmListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null) {
            IAlarmManager.Stub.getDefaultImpl().remove(param2PendingIntent, param2IAlarmListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getNextWakeFromIdleTime() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null)
            return IAlarmManager.Stub.getDefaultImpl().getNextWakeFromIdleTime(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AlarmManager.AlarmClockInfo getNextAlarmClock(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          AlarmManager.AlarmClockInfo alarmClockInfo;
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null) {
            alarmClockInfo = IAlarmManager.Stub.getDefaultImpl().getNextAlarmClock(param2Int);
            return alarmClockInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            alarmClockInfo = (AlarmManager.AlarmClockInfo)AlarmManager.AlarmClockInfo.CREATOR.createFromParcel(parcel2);
          } else {
            alarmClockInfo = null;
          } 
          return alarmClockInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long currentNetworkTimeMillis() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IAlarmManager.Stub.getDefaultImpl() != null)
            return IAlarmManager.Stub.getDefaultImpl().currentNetworkTimeMillis(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHighTemp(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IAlarmManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool1 && IAlarmManager.Stub.getDefaultImpl() != null) {
            IAlarmManager.Stub.getDefaultImpl().setHighTemp(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAlarmManager param1IAlarmManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAlarmManager != null) {
          Proxy.sDefaultImpl = param1IAlarmManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAlarmManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
