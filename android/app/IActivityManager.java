package android.app;

import android.content.ComponentName;
import android.content.IIntentReceiver;
import android.content.IIntentSender;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.LocusId;
import android.content.pm.ApplicationInfo;
import android.content.pm.IPackageDataObserver;
import android.content.pm.ParceledListSlice;
import android.content.pm.UserInfo;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.IInterface;
import android.os.IProgressListener;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.WorkSource;
import android.text.TextUtils;
import android.view.IRecentsAnimationRunner;
import com.android.internal.os.IResultReceiver;
import java.util.ArrayList;
import java.util.List;

public interface IActivityManager extends IInterface {
  void addInstrumentationResults(IApplicationThread paramIApplicationThread, Bundle paramBundle) throws RemoteException;
  
  void addPackageDependency(String paramString) throws RemoteException;
  
  void appNotResponding(String paramString) throws RemoteException;
  
  void appNotRespondingViaProvider(IBinder paramIBinder) throws RemoteException;
  
  void attachApplication(IApplicationThread paramIApplicationThread, long paramLong) throws RemoteException;
  
  void backgroundWhitelistUid(int paramInt) throws RemoteException;
  
  void backupAgentCreated(String paramString, IBinder paramIBinder, int paramInt) throws RemoteException;
  
  boolean bindBackupAgent(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int bindIsolatedService(IApplicationThread paramIApplicationThread, IBinder paramIBinder, Intent paramIntent, String paramString1, IServiceConnection paramIServiceConnection, int paramInt1, String paramString2, String paramString3, int paramInt2) throws RemoteException;
  
  int bindService(IApplicationThread paramIApplicationThread, IBinder paramIBinder, Intent paramIntent, String paramString1, IServiceConnection paramIServiceConnection, int paramInt1, String paramString2, int paramInt2) throws RemoteException;
  
  void bootAnimationComplete() throws RemoteException;
  
  int broadcastIntent(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, int paramInt1, String paramString2, Bundle paramBundle1, String[] paramArrayOfString, int paramInt2, Bundle paramBundle2, boolean paramBoolean1, boolean paramBoolean2, int paramInt3) throws RemoteException;
  
  int broadcastIntentWithFeature(IApplicationThread paramIApplicationThread, String paramString1, Intent paramIntent, String paramString2, IIntentReceiver paramIIntentReceiver, int paramInt1, String paramString3, Bundle paramBundle1, String[] paramArrayOfString, int paramInt2, Bundle paramBundle2, boolean paramBoolean1, boolean paramBoolean2, int paramInt3) throws RemoteException;
  
  void cancelIntentSender(IIntentSender paramIIntentSender) throws RemoteException;
  
  void cancelRecentsAnimation(boolean paramBoolean) throws RemoteException;
  
  void cancelTaskWindowTransition(int paramInt) throws RemoteException;
  
  int checkPermission(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  int checkPermissionWithToken(String paramString, int paramInt1, int paramInt2, IBinder paramIBinder) throws RemoteException;
  
  int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IBinder paramIBinder) throws RemoteException;
  
  boolean clearApplicationUserData(String paramString, boolean paramBoolean, IPackageDataObserver paramIPackageDataObserver, int paramInt) throws RemoteException;
  
  void closeSystemDialogs(String paramString) throws RemoteException;
  
  void crashApplication(int paramInt1, int paramInt2, String paramString1, int paramInt3, String paramString2, boolean paramBoolean) throws RemoteException;
  
  void detectExceptionsForOIDT(int paramInt, String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  boolean dumpHeap(String paramString1, int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, String paramString2, ParcelFileDescriptor paramParcelFileDescriptor, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  void dumpHeapFinished(String paramString) throws RemoteException;
  
  void enterSafeMode() throws RemoteException;
  
  boolean finishActivity(IBinder paramIBinder, int paramInt1, Intent paramIntent, int paramInt2) throws RemoteException;
  
  void finishHeavyWeightApp() throws RemoteException;
  
  void finishInstrumentation(IApplicationThread paramIApplicationThread, int paramInt, Bundle paramBundle) throws RemoteException;
  
  void finishReceiver(IBinder paramIBinder, int paramInt1, String paramString, Bundle paramBundle, boolean paramBoolean, int paramInt2) throws RemoteException;
  
  void forceStopPackage(String paramString, int paramInt) throws RemoteException;
  
  List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException;
  
  List<String> getBugreportWhitelistedPackages() throws RemoteException;
  
  Configuration getConfiguration() throws RemoteException;
  
  ContentProviderHolder getContentProvider(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, int paramInt, boolean paramBoolean) throws RemoteException;
  
  ContentProviderHolder getContentProviderExternal(String paramString1, int paramInt, IBinder paramIBinder, String paramString2) throws RemoteException;
  
  UserInfo getCurrentUser() throws RemoteException;
  
  ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException;
  
  int getForegroundServiceType(ComponentName paramComponentName, IBinder paramIBinder) throws RemoteException;
  
  ParceledListSlice<ApplicationExitInfo> getHistoricalProcessExitReasons(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  Intent getIntentForIntentSender(IIntentSender paramIIntentSender) throws RemoteException;
  
  IIntentSender getIntentSender(int paramInt1, String paramString1, IBinder paramIBinder, String paramString2, int paramInt2, Intent[] paramArrayOfIntent, String[] paramArrayOfString, int paramInt3, Bundle paramBundle, int paramInt4) throws RemoteException;
  
  IIntentSender getIntentSenderWithFeature(int paramInt1, String paramString1, String paramString2, IBinder paramIBinder, String paramString3, int paramInt2, Intent[] paramArrayOfIntent, String[] paramArrayOfString, int paramInt3, Bundle paramBundle, int paramInt4) throws RemoteException;
  
  String getLaunchedFromPackage(IBinder paramIBinder) throws RemoteException;
  
  int getLaunchedFromUid(IBinder paramIBinder) throws RemoteException;
  
  ParcelFileDescriptor getLifeMonitor() throws RemoteException;
  
  int getLockTaskModeState() throws RemoteException;
  
  void getMemoryInfo(ActivityManager.MemoryInfo paramMemoryInfo) throws RemoteException;
  
  int getMemoryTrimLevel() throws RemoteException;
  
  void getMyMemoryState(ActivityManager.RunningAppProcessInfo paramRunningAppProcessInfo) throws RemoteException;
  
  String getPackageForIntentSender(IIntentSender paramIIntentSender) throws RemoteException;
  
  int getPackageProcessState(String paramString1, String paramString2) throws RemoteException;
  
  int getProcessLimit() throws RemoteException;
  
  Debug.MemoryInfo[] getProcessMemoryInfo(int[] paramArrayOfint) throws RemoteException;
  
  long[] getProcessPss(int[] paramArrayOfint) throws RemoteException;
  
  List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState() throws RemoteException;
  
  String getProviderMimeType(Uri paramUri, int paramInt) throws RemoteException;
  
  void getProviderMimeTypeAsync(Uri paramUri, int paramInt, RemoteCallback paramRemoteCallback) throws RemoteException;
  
  ParceledListSlice getRecentTasks(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses() throws RemoteException;
  
  List<ApplicationInfo> getRunningExternalApplications() throws RemoteException;
  
  PendingIntent getRunningServiceControlPanel(ComponentName paramComponentName) throws RemoteException;
  
  int[] getRunningUserIds() throws RemoteException;
  
  List<ActivityManager.RunningServiceInfo> getServices(int paramInt1, int paramInt2) throws RemoteException;
  
  String getTagForIntentSender(IIntentSender paramIIntentSender, String paramString) throws RemoteException;
  
  Rect getTaskBounds(int paramInt) throws RemoteException;
  
  int getTaskForActivity(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  ActivityManager.TaskSnapshot getTaskSnapshot(int paramInt, boolean paramBoolean) throws RemoteException;
  
  List<ActivityManager.RunningTaskInfo> getTasks(int paramInt) throws RemoteException;
  
  ComponentName getTopAppName() throws RemoteException;
  
  int getUidForIntentSender(IIntentSender paramIIntentSender) throws RemoteException;
  
  int getUidProcessState(int paramInt, String paramString) throws RemoteException;
  
  void grantUriPermission(IApplicationThread paramIApplicationThread, String paramString, Uri paramUri, int paramInt1, int paramInt2) throws RemoteException;
  
  void handleApplicationCrash(IBinder paramIBinder, ApplicationErrorReport.ParcelableCrashInfo paramParcelableCrashInfo) throws RemoteException;
  
  void handleApplicationStrictModeViolation(IBinder paramIBinder, int paramInt, StrictMode.ViolationInfo paramViolationInfo) throws RemoteException;
  
  boolean handleApplicationWtf(IBinder paramIBinder, String paramString, boolean paramBoolean, ApplicationErrorReport.ParcelableCrashInfo paramParcelableCrashInfo, int paramInt) throws RemoteException;
  
  int handleIncomingUser(int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean1, boolean paramBoolean2, String paramString1, String paramString2) throws RemoteException;
  
  void hang(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  boolean isAppFreezerSupported() throws RemoteException;
  
  boolean isAppStartModeDisabled(int paramInt, String paramString) throws RemoteException;
  
  boolean isBackgroundRestricted(String paramString) throws RemoteException;
  
  boolean isInLockTaskMode() throws RemoteException;
  
  boolean isIntentSenderABroadcast(IIntentSender paramIIntentSender) throws RemoteException;
  
  boolean isIntentSenderAForegroundService(IIntentSender paramIIntentSender) throws RemoteException;
  
  boolean isIntentSenderAnActivity(IIntentSender paramIIntentSender) throws RemoteException;
  
  boolean isIntentSenderTargetedToPackage(IIntentSender paramIIntentSender) throws RemoteException;
  
  boolean isTopActivityImmersive() throws RemoteException;
  
  boolean isTopOfTask(IBinder paramIBinder) throws RemoteException;
  
  boolean isUidActive(int paramInt, String paramString) throws RemoteException;
  
  boolean isUserAMonkey() throws RemoteException;
  
  boolean isUserRunning(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean isVrModePackageEnabled(ComponentName paramComponentName) throws RemoteException;
  
  void killAllBackgroundProcesses() throws RemoteException;
  
  void killApplication(String paramString1, int paramInt1, int paramInt2, String paramString2) throws RemoteException;
  
  void killApplicationProcess(String paramString, int paramInt) throws RemoteException;
  
  void killBackgroundProcesses(String paramString, int paramInt) throws RemoteException;
  
  void killPackageDependents(String paramString, int paramInt) throws RemoteException;
  
  boolean killPids(int[] paramArrayOfint, String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean killProcessesBelowForeground(String paramString) throws RemoteException;
  
  void killProcessesWhenImperceptible(int[] paramArrayOfint, String paramString) throws RemoteException;
  
  void killUid(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void killUidForPermissionChange(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  boolean launchBugReportHandlerApp() throws RemoteException;
  
  void makePackageIdle(String paramString, int paramInt) throws RemoteException;
  
  boolean moveActivityTaskToBack(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void moveTaskToFront(IApplicationThread paramIApplicationThread, String paramString, int paramInt1, int paramInt2, Bundle paramBundle) throws RemoteException;
  
  void moveTaskToStack(int paramInt1, int paramInt2, boolean paramBoolean) throws RemoteException;
  
  boolean moveTopActivityToPinnedStack(int paramInt, Rect paramRect) throws RemoteException;
  
  void noteAlarmFinish(IIntentSender paramIIntentSender, WorkSource paramWorkSource, int paramInt, String paramString) throws RemoteException;
  
  void noteAlarmStart(IIntentSender paramIIntentSender, WorkSource paramWorkSource, int paramInt, String paramString) throws RemoteException;
  
  void noteWakeupAlarm(IIntentSender paramIIntentSender, WorkSource paramWorkSource, int paramInt, String paramString1, String paramString2) throws RemoteException;
  
  void notifyCleartextNetwork(int paramInt, byte[] paramArrayOfbyte) throws RemoteException;
  
  void notifyLockedProfile(int paramInt) throws RemoteException;
  
  void onBackPressedOnTheiaMonitor(long paramLong) throws RemoteException;
  
  ParcelFileDescriptor openContentUri(String paramString) throws RemoteException;
  
  IBinder peekService(Intent paramIntent, String paramString1, String paramString2) throws RemoteException;
  
  void performIdleMaintenance() throws RemoteException;
  
  void positionTaskInStack(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean profileControl(String paramString, int paramInt1, boolean paramBoolean, ProfilerInfo paramProfilerInfo, int paramInt2) throws RemoteException;
  
  void publishContentProviders(IApplicationThread paramIApplicationThread, List<ContentProviderHolder> paramList) throws RemoteException;
  
  void publishService(IBinder paramIBinder1, Intent paramIntent, IBinder paramIBinder2) throws RemoteException;
  
  boolean refContentProvider(IBinder paramIBinder, int paramInt1, int paramInt2) throws RemoteException;
  
  void registerIntentSenderCancelListener(IIntentSender paramIIntentSender, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void registerProcessObserver(IProcessObserver paramIProcessObserver) throws RemoteException;
  
  Intent registerReceiver(IApplicationThread paramIApplicationThread, String paramString1, IIntentReceiver paramIIntentReceiver, IntentFilter paramIntentFilter, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  Intent registerReceiverWithFeature(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, IIntentReceiver paramIIntentReceiver, IntentFilter paramIntentFilter, String paramString3, int paramInt1, int paramInt2) throws RemoteException;
  
  void registerTaskStackListener(ITaskStackListener paramITaskStackListener) throws RemoteException;
  
  void registerUidObserver(IUidObserver paramIUidObserver, int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void registerUserSwitchObserver(IUserSwitchObserver paramIUserSwitchObserver, String paramString) throws RemoteException;
  
  void removeContentProvider(IBinder paramIBinder, boolean paramBoolean) throws RemoteException;
  
  void removeContentProviderExternal(String paramString, IBinder paramIBinder) throws RemoteException;
  
  void removeContentProviderExternalAsUser(String paramString, IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void removeStack(int paramInt) throws RemoteException;
  
  boolean removeTask(int paramInt) throws RemoteException;
  
  void reportBindApplicationFinished(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void requestBugReport(int paramInt) throws RemoteException;
  
  void requestBugReportWithDescription(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  void requestFullBugReport() throws RemoteException;
  
  void requestInteractiveBugReport() throws RemoteException;
  
  void requestInteractiveBugReportWithDescription(String paramString1, String paramString2) throws RemoteException;
  
  void requestRemoteBugReport() throws RemoteException;
  
  void requestSystemServerHeapDump() throws RemoteException;
  
  void requestTelephonyBugReport(String paramString1, String paramString2) throws RemoteException;
  
  void requestWifiBugReport(String paramString1, String paramString2) throws RemoteException;
  
  void resizeTask(int paramInt1, Rect paramRect, int paramInt2) throws RemoteException;
  
  void restart() throws RemoteException;
  
  int restartUserInBackground(int paramInt) throws RemoteException;
  
  void resumeAppSwitches() throws RemoteException;
  
  void revokeUriPermission(IApplicationThread paramIApplicationThread, String paramString, Uri paramUri, int paramInt1, int paramInt2) throws RemoteException;
  
  void scheduleApplicationInfoChanged(List<String> paramList, int paramInt) throws RemoteException;
  
  void sendIdleJobTrigger() throws RemoteException;
  
  int sendIntentSender(IIntentSender paramIIntentSender, IBinder paramIBinder, int paramInt, Intent paramIntent, String paramString1, IIntentReceiver paramIIntentReceiver, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void sendTheiaEvent(long paramLong, Intent paramIntent) throws RemoteException;
  
  void serviceDoneExecuting(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void setActivityController(IActivityController paramIActivityController, boolean paramBoolean) throws RemoteException;
  
  void setActivityLocusContext(ComponentName paramComponentName, LocusId paramLocusId, IBinder paramIBinder) throws RemoteException;
  
  void setAgentApp(String paramString1, String paramString2) throws RemoteException;
  
  void setAlwaysFinish(boolean paramBoolean) throws RemoteException;
  
  void setDebugApp(String paramString, boolean paramBoolean1, boolean paramBoolean2) throws RemoteException;
  
  void setDumpHeapDebugLimit(String paramString1, int paramInt, long paramLong, String paramString2) throws RemoteException;
  
  void setFocusedStack(int paramInt) throws RemoteException;
  
  void setHasTopUi(boolean paramBoolean) throws RemoteException;
  
  void setHwuiTaskThreads(int paramInt) throws RemoteException;
  
  void setOppoKinectActivityController(IOplusKinectActivityController paramIOplusKinectActivityController) throws RemoteException;
  
  void setPackageScreenCompatMode(String paramString, int paramInt) throws RemoteException;
  
  void setPersistentVrThread(int paramInt) throws RemoteException;
  
  void setProcessImportant(IBinder paramIBinder, int paramInt, boolean paramBoolean, String paramString) throws RemoteException;
  
  void setProcessLimit(int paramInt) throws RemoteException;
  
  boolean setProcessMemoryTrimLevel(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setProcessStateSummary(byte[] paramArrayOfbyte) throws RemoteException;
  
  void setRenderThread(int paramInt) throws RemoteException;
  
  void setRequestedOrientation(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void setServiceForeground(ComponentName paramComponentName, IBinder paramIBinder, int paramInt1, Notification paramNotification, int paramInt2, int paramInt3) throws RemoteException;
  
  void setTaskResizeable(int paramInt1, int paramInt2) throws RemoteException;
  
  void setUserIsMonkey(boolean paramBoolean) throws RemoteException;
  
  void showBootMessage(CharSequence paramCharSequence, boolean paramBoolean) throws RemoteException;
  
  void showWaitingForDebugger(IApplicationThread paramIApplicationThread, boolean paramBoolean) throws RemoteException;
  
  boolean shutdown(int paramInt) throws RemoteException;
  
  void signalPersistentProcesses(int paramInt) throws RemoteException;
  
  int startActivity(IApplicationThread paramIApplicationThread, String paramString1, Intent paramIntent, String paramString2, IBinder paramIBinder, String paramString3, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle) throws RemoteException;
  
  int startActivityAsUser(IApplicationThread paramIApplicationThread, String paramString1, Intent paramIntent, String paramString2, IBinder paramIBinder, String paramString3, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  int startActivityAsUserWithFeature(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  int startActivityFromRecents(int paramInt, Bundle paramBundle) throws RemoteException;
  
  int startActivityWithFeature(IApplicationThread paramIApplicationThread, String paramString1, String paramString2, Intent paramIntent, String paramString3, IBinder paramIBinder, String paramString4, int paramInt1, int paramInt2, ProfilerInfo paramProfilerInfo, Bundle paramBundle) throws RemoteException;
  
  boolean startBinderTracking() throws RemoteException;
  
  void startConfirmDeviceCredentialIntent(Intent paramIntent, Bundle paramBundle) throws RemoteException;
  
  void startDelegateShellPermissionIdentity(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  boolean startInstrumentation(ComponentName paramComponentName, String paramString1, int paramInt1, Bundle paramBundle, IInstrumentationWatcher paramIInstrumentationWatcher, IUiAutomationConnection paramIUiAutomationConnection, int paramInt2, String paramString2) throws RemoteException;
  
  void startRecentsActivity(Intent paramIntent, IAssistDataReceiver paramIAssistDataReceiver, IRecentsAnimationRunner paramIRecentsAnimationRunner) throws RemoteException;
  
  ComponentName startService(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString1, boolean paramBoolean, String paramString2, String paramString3, int paramInt) throws RemoteException;
  
  void startSystemLockTaskMode(int paramInt) throws RemoteException;
  
  boolean startUserInBackground(int paramInt) throws RemoteException;
  
  boolean startUserInBackgroundWithListener(int paramInt, IProgressListener paramIProgressListener) throws RemoteException;
  
  boolean startUserInForegroundWithListener(int paramInt, IProgressListener paramIProgressListener) throws RemoteException;
  
  void stopAppSwitches() throws RemoteException;
  
  boolean stopBinderTrackingAndDump(ParcelFileDescriptor paramParcelFileDescriptor) throws RemoteException;
  
  void stopDelegateShellPermissionIdentity() throws RemoteException;
  
  int stopService(IApplicationThread paramIApplicationThread, Intent paramIntent, String paramString, int paramInt) throws RemoteException;
  
  boolean stopServiceToken(ComponentName paramComponentName, IBinder paramIBinder, int paramInt) throws RemoteException;
  
  int stopUser(int paramInt, boolean paramBoolean, IStopUserCallback paramIStopUserCallback) throws RemoteException;
  
  int stopUserWithDelayedLocking(int paramInt, boolean paramBoolean, IStopUserCallback paramIStopUserCallback) throws RemoteException;
  
  void suppressResizeConfigChanges(boolean paramBoolean) throws RemoteException;
  
  boolean switchUser(int paramInt) throws RemoteException;
  
  void unbindBackupAgent(ApplicationInfo paramApplicationInfo) throws RemoteException;
  
  void unbindFinished(IBinder paramIBinder, Intent paramIntent, boolean paramBoolean) throws RemoteException;
  
  boolean unbindService(IServiceConnection paramIServiceConnection) throws RemoteException;
  
  void unbroadcastIntent(IApplicationThread paramIApplicationThread, Intent paramIntent, int paramInt) throws RemoteException;
  
  void unhandledBack() throws RemoteException;
  
  boolean unlockUser(int paramInt, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, IProgressListener paramIProgressListener) throws RemoteException;
  
  void unregisterIntentSenderCancelListener(IIntentSender paramIIntentSender, IResultReceiver paramIResultReceiver) throws RemoteException;
  
  void unregisterProcessObserver(IProcessObserver paramIProcessObserver) throws RemoteException;
  
  void unregisterReceiver(IIntentReceiver paramIIntentReceiver) throws RemoteException;
  
  void unregisterTaskStackListener(ITaskStackListener paramITaskStackListener) throws RemoteException;
  
  void unregisterUidObserver(IUidObserver paramIUidObserver) throws RemoteException;
  
  void unregisterUserSwitchObserver(IUserSwitchObserver paramIUserSwitchObserver) throws RemoteException;
  
  void unstableProviderDied(IBinder paramIBinder) throws RemoteException;
  
  boolean updateConfiguration(Configuration paramConfiguration) throws RemoteException;
  
  void updateDeviceOwner(String paramString) throws RemoteException;
  
  void updateLockTaskPackages(int paramInt, String[] paramArrayOfString) throws RemoteException;
  
  boolean updateMccMncConfiguration(String paramString1, String paramString2) throws RemoteException;
  
  void updatePersistentConfiguration(Configuration paramConfiguration) throws RemoteException;
  
  void updateServiceGroup(IServiceConnection paramIServiceConnection, int paramInt1, int paramInt2) throws RemoteException;
  
  void waitForNetworkStateUpdate(long paramLong) throws RemoteException;
  
  class Default implements IActivityManager {
    public ParcelFileDescriptor openContentUri(String param1String) throws RemoteException {
      return null;
    }
    
    public void registerUidObserver(IUidObserver param1IUidObserver, int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public void unregisterUidObserver(IUidObserver param1IUidObserver) throws RemoteException {}
    
    public boolean isUidActive(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public int getUidProcessState(int param1Int, String param1String) throws RemoteException {
      return 0;
    }
    
    public void setHwuiTaskThreads(int param1Int) throws RemoteException {}
    
    public void handleApplicationCrash(IBinder param1IBinder, ApplicationErrorReport.ParcelableCrashInfo param1ParcelableCrashInfo) throws RemoteException {}
    
    public int startActivity(IApplicationThread param1IApplicationThread, String param1String1, Intent param1Intent, String param1String2, IBinder param1IBinder, String param1String3, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public int startActivityWithFeature(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public void unhandledBack() throws RemoteException {}
    
    public boolean finishActivity(IBinder param1IBinder, int param1Int1, Intent param1Intent, int param1Int2) throws RemoteException {
      return false;
    }
    
    public Intent registerReceiver(IApplicationThread param1IApplicationThread, String param1String1, IIntentReceiver param1IIntentReceiver, IntentFilter param1IntentFilter, String param1String2, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public Intent registerReceiverWithFeature(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, IIntentReceiver param1IIntentReceiver, IntentFilter param1IntentFilter, String param1String3, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void unregisterReceiver(IIntentReceiver param1IIntentReceiver) throws RemoteException {}
    
    public int broadcastIntent(IApplicationThread param1IApplicationThread, Intent param1Intent, String param1String1, IIntentReceiver param1IIntentReceiver, int param1Int1, String param1String2, Bundle param1Bundle1, String[] param1ArrayOfString, int param1Int2, Bundle param1Bundle2, boolean param1Boolean1, boolean param1Boolean2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int broadcastIntentWithFeature(IApplicationThread param1IApplicationThread, String param1String1, Intent param1Intent, String param1String2, IIntentReceiver param1IIntentReceiver, int param1Int1, String param1String3, Bundle param1Bundle1, String[] param1ArrayOfString, int param1Int2, Bundle param1Bundle2, boolean param1Boolean1, boolean param1Boolean2, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public void unbroadcastIntent(IApplicationThread param1IApplicationThread, Intent param1Intent, int param1Int) throws RemoteException {}
    
    public void finishReceiver(IBinder param1IBinder, int param1Int1, String param1String, Bundle param1Bundle, boolean param1Boolean, int param1Int2) throws RemoteException {}
    
    public void attachApplication(IApplicationThread param1IApplicationThread, long param1Long) throws RemoteException {}
    
    public List<ActivityManager.RunningTaskInfo> getTasks(int param1Int) throws RemoteException {
      return null;
    }
    
    public void moveTaskToFront(IApplicationThread param1IApplicationThread, String param1String, int param1Int1, int param1Int2, Bundle param1Bundle) throws RemoteException {}
    
    public int getTaskForActivity(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {
      return 0;
    }
    
    public ContentProviderHolder getContentProvider(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void publishContentProviders(IApplicationThread param1IApplicationThread, List<ContentProviderHolder> param1List) throws RemoteException {}
    
    public boolean refContentProvider(IBinder param1IBinder, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public PendingIntent getRunningServiceControlPanel(ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public ComponentName startService(IApplicationThread param1IApplicationThread, Intent param1Intent, String param1String1, boolean param1Boolean, String param1String2, String param1String3, int param1Int) throws RemoteException {
      return null;
    }
    
    public int stopService(IApplicationThread param1IApplicationThread, Intent param1Intent, String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int bindService(IApplicationThread param1IApplicationThread, IBinder param1IBinder, Intent param1Intent, String param1String1, IServiceConnection param1IServiceConnection, int param1Int1, String param1String2, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int bindIsolatedService(IApplicationThread param1IApplicationThread, IBinder param1IBinder, Intent param1Intent, String param1String1, IServiceConnection param1IServiceConnection, int param1Int1, String param1String2, String param1String3, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public void updateServiceGroup(IServiceConnection param1IServiceConnection, int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean unbindService(IServiceConnection param1IServiceConnection) throws RemoteException {
      return false;
    }
    
    public void publishService(IBinder param1IBinder1, Intent param1Intent, IBinder param1IBinder2) throws RemoteException {}
    
    public void setDebugApp(String param1String, boolean param1Boolean1, boolean param1Boolean2) throws RemoteException {}
    
    public void setAgentApp(String param1String1, String param1String2) throws RemoteException {}
    
    public void setAlwaysFinish(boolean param1Boolean) throws RemoteException {}
    
    public boolean startInstrumentation(ComponentName param1ComponentName, String param1String1, int param1Int1, Bundle param1Bundle, IInstrumentationWatcher param1IInstrumentationWatcher, IUiAutomationConnection param1IUiAutomationConnection, int param1Int2, String param1String2) throws RemoteException {
      return false;
    }
    
    public void addInstrumentationResults(IApplicationThread param1IApplicationThread, Bundle param1Bundle) throws RemoteException {}
    
    public void finishInstrumentation(IApplicationThread param1IApplicationThread, int param1Int, Bundle param1Bundle) throws RemoteException {}
    
    public Configuration getConfiguration() throws RemoteException {
      return null;
    }
    
    public boolean updateConfiguration(Configuration param1Configuration) throws RemoteException {
      return false;
    }
    
    public boolean updateMccMncConfiguration(String param1String1, String param1String2) throws RemoteException {
      return false;
    }
    
    public boolean stopServiceToken(ComponentName param1ComponentName, IBinder param1IBinder, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setProcessLimit(int param1Int) throws RemoteException {}
    
    public int getProcessLimit() throws RemoteException {
      return 0;
    }
    
    public int checkPermission(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int checkUriPermission(Uri param1Uri, int param1Int1, int param1Int2, int param1Int3, int param1Int4, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public void grantUriPermission(IApplicationThread param1IApplicationThread, String param1String, Uri param1Uri, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void revokeUriPermission(IApplicationThread param1IApplicationThread, String param1String, Uri param1Uri, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setActivityController(IActivityController param1IActivityController, boolean param1Boolean) throws RemoteException {}
    
    public void showWaitingForDebugger(IApplicationThread param1IApplicationThread, boolean param1Boolean) throws RemoteException {}
    
    public void signalPersistentProcesses(int param1Int) throws RemoteException {}
    
    public ParceledListSlice getRecentTasks(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public void serviceDoneExecuting(IBinder param1IBinder, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IIntentSender getIntentSender(int param1Int1, String param1String1, IBinder param1IBinder, String param1String2, int param1Int2, Intent[] param1ArrayOfIntent, String[] param1ArrayOfString, int param1Int3, Bundle param1Bundle, int param1Int4) throws RemoteException {
      return null;
    }
    
    public IIntentSender getIntentSenderWithFeature(int param1Int1, String param1String1, String param1String2, IBinder param1IBinder, String param1String3, int param1Int2, Intent[] param1ArrayOfIntent, String[] param1ArrayOfString, int param1Int3, Bundle param1Bundle, int param1Int4) throws RemoteException {
      return null;
    }
    
    public void cancelIntentSender(IIntentSender param1IIntentSender) throws RemoteException {}
    
    public String getPackageForIntentSender(IIntentSender param1IIntentSender) throws RemoteException {
      return null;
    }
    
    public void registerIntentSenderCancelListener(IIntentSender param1IIntentSender, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void unregisterIntentSenderCancelListener(IIntentSender param1IIntentSender, IResultReceiver param1IResultReceiver) throws RemoteException {}
    
    public void enterSafeMode() throws RemoteException {}
    
    public void noteWakeupAlarm(IIntentSender param1IIntentSender, WorkSource param1WorkSource, int param1Int, String param1String1, String param1String2) throws RemoteException {}
    
    public void removeContentProvider(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public void setRequestedOrientation(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void unbindFinished(IBinder param1IBinder, Intent param1Intent, boolean param1Boolean) throws RemoteException {}
    
    public void setProcessImportant(IBinder param1IBinder, int param1Int, boolean param1Boolean, String param1String) throws RemoteException {}
    
    public void setServiceForeground(ComponentName param1ComponentName, IBinder param1IBinder, int param1Int1, Notification param1Notification, int param1Int2, int param1Int3) throws RemoteException {}
    
    public int getForegroundServiceType(ComponentName param1ComponentName, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public boolean moveActivityTaskToBack(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void getMemoryInfo(ActivityManager.MemoryInfo param1MemoryInfo) throws RemoteException {}
    
    public List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState() throws RemoteException {
      return null;
    }
    
    public boolean clearApplicationUserData(String param1String, boolean param1Boolean, IPackageDataObserver param1IPackageDataObserver, int param1Int) throws RemoteException {
      return false;
    }
    
    public void forceStopPackage(String param1String, int param1Int) throws RemoteException {}
    
    public boolean killPids(int[] param1ArrayOfint, String param1String, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public List<ActivityManager.RunningServiceInfo> getServices(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses() throws RemoteException {
      return null;
    }
    
    public IBinder peekService(Intent param1Intent, String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean profileControl(String param1String, int param1Int1, boolean param1Boolean, ProfilerInfo param1ProfilerInfo, int param1Int2) throws RemoteException {
      return false;
    }
    
    public boolean shutdown(int param1Int) throws RemoteException {
      return false;
    }
    
    public void stopAppSwitches() throws RemoteException {}
    
    public void resumeAppSwitches() throws RemoteException {}
    
    public boolean bindBackupAgent(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void backupAgentCreated(String param1String, IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void unbindBackupAgent(ApplicationInfo param1ApplicationInfo) throws RemoteException {}
    
    public int getUidForIntentSender(IIntentSender param1IIntentSender) throws RemoteException {
      return 0;
    }
    
    public int handleIncomingUser(int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean1, boolean param1Boolean2, String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void addPackageDependency(String param1String) throws RemoteException {}
    
    public void killApplication(String param1String1, int param1Int1, int param1Int2, String param1String2) throws RemoteException {}
    
    public void closeSystemDialogs(String param1String) throws RemoteException {}
    
    public Debug.MemoryInfo[] getProcessMemoryInfo(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void killApplicationProcess(String param1String, int param1Int) throws RemoteException {}
    
    public boolean handleApplicationWtf(IBinder param1IBinder, String param1String, boolean param1Boolean, ApplicationErrorReport.ParcelableCrashInfo param1ParcelableCrashInfo, int param1Int) throws RemoteException {
      return false;
    }
    
    public void killBackgroundProcesses(String param1String, int param1Int) throws RemoteException {}
    
    public boolean isUserAMonkey() throws RemoteException {
      return false;
    }
    
    public List<ApplicationInfo> getRunningExternalApplications() throws RemoteException {
      return null;
    }
    
    public void finishHeavyWeightApp() throws RemoteException {}
    
    public void handleApplicationStrictModeViolation(IBinder param1IBinder, int param1Int, StrictMode.ViolationInfo param1ViolationInfo) throws RemoteException {}
    
    public boolean isTopActivityImmersive() throws RemoteException {
      return false;
    }
    
    public void crashApplication(int param1Int1, int param1Int2, String param1String1, int param1Int3, String param1String2, boolean param1Boolean) throws RemoteException {}
    
    public String getProviderMimeType(Uri param1Uri, int param1Int) throws RemoteException {
      return null;
    }
    
    public void getProviderMimeTypeAsync(Uri param1Uri, int param1Int, RemoteCallback param1RemoteCallback) throws RemoteException {}
    
    public boolean dumpHeap(String param1String1, int param1Int, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, String param1String2, ParcelFileDescriptor param1ParcelFileDescriptor, RemoteCallback param1RemoteCallback) throws RemoteException {
      return false;
    }
    
    public boolean isUserRunning(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setPackageScreenCompatMode(String param1String, int param1Int) throws RemoteException {}
    
    public boolean switchUser(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean removeTask(int param1Int) throws RemoteException {
      return false;
    }
    
    public void registerProcessObserver(IProcessObserver param1IProcessObserver) throws RemoteException {}
    
    public void unregisterProcessObserver(IProcessObserver param1IProcessObserver) throws RemoteException {}
    
    public boolean isIntentSenderTargetedToPackage(IIntentSender param1IIntentSender) throws RemoteException {
      return false;
    }
    
    public void updatePersistentConfiguration(Configuration param1Configuration) throws RemoteException {}
    
    public long[] getProcessPss(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public void showBootMessage(CharSequence param1CharSequence, boolean param1Boolean) throws RemoteException {}
    
    public void killAllBackgroundProcesses() throws RemoteException {}
    
    public ContentProviderHolder getContentProviderExternal(String param1String1, int param1Int, IBinder param1IBinder, String param1String2) throws RemoteException {
      return null;
    }
    
    public void removeContentProviderExternal(String param1String, IBinder param1IBinder) throws RemoteException {}
    
    public void removeContentProviderExternalAsUser(String param1String, IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void getMyMemoryState(ActivityManager.RunningAppProcessInfo param1RunningAppProcessInfo) throws RemoteException {}
    
    public boolean killProcessesBelowForeground(String param1String) throws RemoteException {
      return false;
    }
    
    public UserInfo getCurrentUser() throws RemoteException {
      return null;
    }
    
    public int getLaunchedFromUid(IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public void unstableProviderDied(IBinder param1IBinder) throws RemoteException {}
    
    public boolean isIntentSenderAnActivity(IIntentSender param1IIntentSender) throws RemoteException {
      return false;
    }
    
    public boolean isIntentSenderAForegroundService(IIntentSender param1IIntentSender) throws RemoteException {
      return false;
    }
    
    public boolean isIntentSenderABroadcast(IIntentSender param1IIntentSender) throws RemoteException {
      return false;
    }
    
    public int startActivityAsUser(IApplicationThread param1IApplicationThread, String param1String1, Intent param1Intent, String param1String2, IBinder param1IBinder, String param1String3, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int startActivityAsUserWithFeature(IApplicationThread param1IApplicationThread, String param1String1, String param1String2, Intent param1Intent, String param1String3, IBinder param1IBinder, String param1String4, int param1Int1, int param1Int2, ProfilerInfo param1ProfilerInfo, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return 0;
    }
    
    public int stopUser(int param1Int, boolean param1Boolean, IStopUserCallback param1IStopUserCallback) throws RemoteException {
      return 0;
    }
    
    public int stopUserWithDelayedLocking(int param1Int, boolean param1Boolean, IStopUserCallback param1IStopUserCallback) throws RemoteException {
      return 0;
    }
    
    public void registerUserSwitchObserver(IUserSwitchObserver param1IUserSwitchObserver, String param1String) throws RemoteException {}
    
    public void unregisterUserSwitchObserver(IUserSwitchObserver param1IUserSwitchObserver) throws RemoteException {}
    
    public int[] getRunningUserIds() throws RemoteException {
      return null;
    }
    
    public void requestSystemServerHeapDump() throws RemoteException {}
    
    public void requestBugReport(int param1Int) throws RemoteException {}
    
    public void requestBugReportWithDescription(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void requestTelephonyBugReport(String param1String1, String param1String2) throws RemoteException {}
    
    public void requestWifiBugReport(String param1String1, String param1String2) throws RemoteException {}
    
    public void requestInteractiveBugReportWithDescription(String param1String1, String param1String2) throws RemoteException {}
    
    public void requestInteractiveBugReport() throws RemoteException {}
    
    public void requestFullBugReport() throws RemoteException {}
    
    public void requestRemoteBugReport() throws RemoteException {}
    
    public boolean launchBugReportHandlerApp() throws RemoteException {
      return false;
    }
    
    public List<String> getBugreportWhitelistedPackages() throws RemoteException {
      return null;
    }
    
    public Intent getIntentForIntentSender(IIntentSender param1IIntentSender) throws RemoteException {
      return null;
    }
    
    public String getLaunchedFromPackage(IBinder param1IBinder) throws RemoteException {
      return null;
    }
    
    public void killUid(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public void setUserIsMonkey(boolean param1Boolean) throws RemoteException {}
    
    public void hang(IBinder param1IBinder, boolean param1Boolean) throws RemoteException {}
    
    public List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException {
      return null;
    }
    
    public void moveTaskToStack(int param1Int1, int param1Int2, boolean param1Boolean) throws RemoteException {}
    
    public void setFocusedStack(int param1Int) throws RemoteException {}
    
    public ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException {
      return null;
    }
    
    public void restart() throws RemoteException {}
    
    public void performIdleMaintenance() throws RemoteException {}
    
    public void appNotRespondingViaProvider(IBinder param1IBinder) throws RemoteException {}
    
    public Rect getTaskBounds(int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setProcessMemoryTrimLevel(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public String getTagForIntentSender(IIntentSender param1IIntentSender, String param1String) throws RemoteException {
      return null;
    }
    
    public boolean startUserInBackground(int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isInLockTaskMode() throws RemoteException {
      return false;
    }
    
    public void startRecentsActivity(Intent param1Intent, IAssistDataReceiver param1IAssistDataReceiver, IRecentsAnimationRunner param1IRecentsAnimationRunner) throws RemoteException {}
    
    public void cancelRecentsAnimation(boolean param1Boolean) throws RemoteException {}
    
    public int startActivityFromRecents(int param1Int, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public void startSystemLockTaskMode(int param1Int) throws RemoteException {}
    
    public boolean isTopOfTask(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void bootAnimationComplete() throws RemoteException {}
    
    public int checkPermissionWithToken(String param1String, int param1Int1, int param1Int2, IBinder param1IBinder) throws RemoteException {
      return 0;
    }
    
    public void registerTaskStackListener(ITaskStackListener param1ITaskStackListener) throws RemoteException {}
    
    public void unregisterTaskStackListener(ITaskStackListener param1ITaskStackListener) throws RemoteException {}
    
    public void notifyCleartextNetwork(int param1Int, byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public void setTaskResizeable(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void resizeTask(int param1Int1, Rect param1Rect, int param1Int2) throws RemoteException {}
    
    public int getLockTaskModeState() throws RemoteException {
      return 0;
    }
    
    public void setDumpHeapDebugLimit(String param1String1, int param1Int, long param1Long, String param1String2) throws RemoteException {}
    
    public void dumpHeapFinished(String param1String) throws RemoteException {}
    
    public void updateLockTaskPackages(int param1Int, String[] param1ArrayOfString) throws RemoteException {}
    
    public void noteAlarmStart(IIntentSender param1IIntentSender, WorkSource param1WorkSource, int param1Int, String param1String) throws RemoteException {}
    
    public void noteAlarmFinish(IIntentSender param1IIntentSender, WorkSource param1WorkSource, int param1Int, String param1String) throws RemoteException {}
    
    public int getPackageProcessState(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public void updateDeviceOwner(String param1String) throws RemoteException {}
    
    public boolean startBinderTracking() throws RemoteException {
      return false;
    }
    
    public boolean stopBinderTrackingAndDump(ParcelFileDescriptor param1ParcelFileDescriptor) throws RemoteException {
      return false;
    }
    
    public void positionTaskInStack(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void suppressResizeConfigChanges(boolean param1Boolean) throws RemoteException {}
    
    public boolean moveTopActivityToPinnedStack(int param1Int, Rect param1Rect) throws RemoteException {
      return false;
    }
    
    public boolean isAppStartModeDisabled(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean unlockUser(int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, IProgressListener param1IProgressListener) throws RemoteException {
      return false;
    }
    
    public void killPackageDependents(String param1String, int param1Int) throws RemoteException {}
    
    public void removeStack(int param1Int) throws RemoteException {}
    
    public void makePackageIdle(String param1String, int param1Int) throws RemoteException {}
    
    public int getMemoryTrimLevel() throws RemoteException {
      return 0;
    }
    
    public boolean isVrModePackageEnabled(ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public void notifyLockedProfile(int param1Int) throws RemoteException {}
    
    public void startConfirmDeviceCredentialIntent(Intent param1Intent, Bundle param1Bundle) throws RemoteException {}
    
    public void sendIdleJobTrigger() throws RemoteException {}
    
    public int sendIntentSender(IIntentSender param1IIntentSender, IBinder param1IBinder, int param1Int, Intent param1Intent, String param1String1, IIntentReceiver param1IIntentReceiver, String param1String2, Bundle param1Bundle) throws RemoteException {
      return 0;
    }
    
    public boolean isBackgroundRestricted(String param1String) throws RemoteException {
      return false;
    }
    
    public void setRenderThread(int param1Int) throws RemoteException {}
    
    public void setHasTopUi(boolean param1Boolean) throws RemoteException {}
    
    public int restartUserInBackground(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void cancelTaskWindowTransition(int param1Int) throws RemoteException {}
    
    public ActivityManager.TaskSnapshot getTaskSnapshot(int param1Int, boolean param1Boolean) throws RemoteException {
      return null;
    }
    
    public void scheduleApplicationInfoChanged(List<String> param1List, int param1Int) throws RemoteException {}
    
    public void setPersistentVrThread(int param1Int) throws RemoteException {}
    
    public void waitForNetworkStateUpdate(long param1Long) throws RemoteException {}
    
    public void backgroundWhitelistUid(int param1Int) throws RemoteException {}
    
    public boolean startUserInBackgroundWithListener(int param1Int, IProgressListener param1IProgressListener) throws RemoteException {
      return false;
    }
    
    public void startDelegateShellPermissionIdentity(int param1Int, String[] param1ArrayOfString) throws RemoteException {}
    
    public void stopDelegateShellPermissionIdentity() throws RemoteException {}
    
    public ParcelFileDescriptor getLifeMonitor() throws RemoteException {
      return null;
    }
    
    public boolean startUserInForegroundWithListener(int param1Int, IProgressListener param1IProgressListener) throws RemoteException {
      return false;
    }
    
    public void appNotResponding(String param1String) throws RemoteException {}
    
    public ParceledListSlice<ApplicationExitInfo> getHistoricalProcessExitReasons(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return null;
    }
    
    public void killProcessesWhenImperceptible(int[] param1ArrayOfint, String param1String) throws RemoteException {}
    
    public void setActivityLocusContext(ComponentName param1ComponentName, LocusId param1LocusId, IBinder param1IBinder) throws RemoteException {}
    
    public void setProcessStateSummary(byte[] param1ArrayOfbyte) throws RemoteException {}
    
    public boolean isAppFreezerSupported() throws RemoteException {
      return false;
    }
    
    public ComponentName getTopAppName() throws RemoteException {
      return null;
    }
    
    public void detectExceptionsForOIDT(int param1Int, String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public void setOppoKinectActivityController(IOplusKinectActivityController param1IOplusKinectActivityController) throws RemoteException {}
    
    public void onBackPressedOnTheiaMonitor(long param1Long) throws RemoteException {}
    
    public void sendTheiaEvent(long param1Long, Intent param1Intent) throws RemoteException {}
    
    public void reportBindApplicationFinished(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void killUidForPermissionChange(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IActivityManager {
    private static final String DESCRIPTOR = "android.app.IActivityManager";
    
    static final int TRANSACTION_addInstrumentationResults = 38;
    
    static final int TRANSACTION_addPackageDependency = 87;
    
    static final int TRANSACTION_appNotResponding = 211;
    
    static final int TRANSACTION_appNotRespondingViaProvider = 154;
    
    static final int TRANSACTION_attachApplication = 19;
    
    static final int TRANSACTION_backgroundWhitelistUid = 205;
    
    static final int TRANSACTION_backupAgentCreated = 83;
    
    static final int TRANSACTION_bindBackupAgent = 82;
    
    static final int TRANSACTION_bindIsolatedService = 30;
    
    static final int TRANSACTION_bindService = 29;
    
    static final int TRANSACTION_bootAnimationComplete = 165;
    
    static final int TRANSACTION_broadcastIntent = 15;
    
    static final int TRANSACTION_broadcastIntentWithFeature = 16;
    
    static final int TRANSACTION_cancelIntentSender = 57;
    
    static final int TRANSACTION_cancelRecentsAnimation = 161;
    
    static final int TRANSACTION_cancelTaskWindowTransition = 200;
    
    static final int TRANSACTION_checkPermission = 46;
    
    static final int TRANSACTION_checkPermissionWithToken = 166;
    
    static final int TRANSACTION_checkUriPermission = 47;
    
    static final int TRANSACTION_clearApplicationUserData = 72;
    
    static final int TRANSACTION_closeSystemDialogs = 89;
    
    static final int TRANSACTION_crashApplication = 99;
    
    static final int TRANSACTION_detectExceptionsForOIDT = 218;
    
    static final int TRANSACTION_dumpHeap = 102;
    
    static final int TRANSACTION_dumpHeapFinished = 174;
    
    static final int TRANSACTION_enterSafeMode = 61;
    
    static final int TRANSACTION_finishActivity = 11;
    
    static final int TRANSACTION_finishHeavyWeightApp = 96;
    
    static final int TRANSACTION_finishInstrumentation = 39;
    
    static final int TRANSACTION_finishReceiver = 18;
    
    static final int TRANSACTION_forceStopPackage = 73;
    
    static final int TRANSACTION_getAllStackInfos = 148;
    
    static final int TRANSACTION_getBugreportWhitelistedPackages = 142;
    
    static final int TRANSACTION_getConfiguration = 40;
    
    static final int TRANSACTION_getContentProvider = 23;
    
    static final int TRANSACTION_getContentProviderExternal = 114;
    
    static final int TRANSACTION_getCurrentUser = 119;
    
    static final int TRANSACTION_getFocusedStackInfo = 151;
    
    static final int TRANSACTION_getForegroundServiceType = 68;
    
    static final int TRANSACTION_getHistoricalProcessExitReasons = 212;
    
    static final int TRANSACTION_getIntentForIntentSender = 143;
    
    static final int TRANSACTION_getIntentSender = 55;
    
    static final int TRANSACTION_getIntentSenderWithFeature = 56;
    
    static final int TRANSACTION_getLaunchedFromPackage = 144;
    
    static final int TRANSACTION_getLaunchedFromUid = 120;
    
    static final int TRANSACTION_getLifeMonitor = 209;
    
    static final int TRANSACTION_getLockTaskModeState = 172;
    
    static final int TRANSACTION_getMemoryInfo = 70;
    
    static final int TRANSACTION_getMemoryTrimLevel = 190;
    
    static final int TRANSACTION_getMyMemoryState = 117;
    
    static final int TRANSACTION_getPackageForIntentSender = 58;
    
    static final int TRANSACTION_getPackageProcessState = 178;
    
    static final int TRANSACTION_getProcessLimit = 45;
    
    static final int TRANSACTION_getProcessMemoryInfo = 90;
    
    static final int TRANSACTION_getProcessPss = 111;
    
    static final int TRANSACTION_getProcessesInErrorState = 71;
    
    static final int TRANSACTION_getProviderMimeType = 100;
    
    static final int TRANSACTION_getProviderMimeTypeAsync = 101;
    
    static final int TRANSACTION_getRecentTasks = 53;
    
    static final int TRANSACTION_getRunningAppProcesses = 76;
    
    static final int TRANSACTION_getRunningExternalApplications = 95;
    
    static final int TRANSACTION_getRunningServiceControlPanel = 26;
    
    static final int TRANSACTION_getRunningUserIds = 131;
    
    static final int TRANSACTION_getServices = 75;
    
    static final int TRANSACTION_getTagForIntentSender = 157;
    
    static final int TRANSACTION_getTaskBounds = 155;
    
    static final int TRANSACTION_getTaskForActivity = 22;
    
    static final int TRANSACTION_getTaskSnapshot = 201;
    
    static final int TRANSACTION_getTasks = 20;
    
    static final int TRANSACTION_getTopAppName = 217;
    
    static final int TRANSACTION_getUidForIntentSender = 85;
    
    static final int TRANSACTION_getUidProcessState = 5;
    
    static final int TRANSACTION_grantUriPermission = 48;
    
    static final int TRANSACTION_handleApplicationCrash = 7;
    
    static final int TRANSACTION_handleApplicationStrictModeViolation = 97;
    
    static final int TRANSACTION_handleApplicationWtf = 92;
    
    static final int TRANSACTION_handleIncomingUser = 86;
    
    static final int TRANSACTION_hang = 147;
    
    static final int TRANSACTION_isAppFreezerSupported = 216;
    
    static final int TRANSACTION_isAppStartModeDisabled = 185;
    
    static final int TRANSACTION_isBackgroundRestricted = 196;
    
    static final int TRANSACTION_isInLockTaskMode = 159;
    
    static final int TRANSACTION_isIntentSenderABroadcast = 124;
    
    static final int TRANSACTION_isIntentSenderAForegroundService = 123;
    
    static final int TRANSACTION_isIntentSenderAnActivity = 122;
    
    static final int TRANSACTION_isIntentSenderTargetedToPackage = 109;
    
    static final int TRANSACTION_isTopActivityImmersive = 98;
    
    static final int TRANSACTION_isTopOfTask = 164;
    
    static final int TRANSACTION_isUidActive = 4;
    
    static final int TRANSACTION_isUserAMonkey = 94;
    
    static final int TRANSACTION_isUserRunning = 103;
    
    static final int TRANSACTION_isVrModePackageEnabled = 191;
    
    static final int TRANSACTION_killAllBackgroundProcesses = 113;
    
    static final int TRANSACTION_killApplication = 88;
    
    static final int TRANSACTION_killApplicationProcess = 91;
    
    static final int TRANSACTION_killBackgroundProcesses = 93;
    
    static final int TRANSACTION_killPackageDependents = 187;
    
    static final int TRANSACTION_killPids = 74;
    
    static final int TRANSACTION_killProcessesBelowForeground = 118;
    
    static final int TRANSACTION_killProcessesWhenImperceptible = 213;
    
    static final int TRANSACTION_killUid = 145;
    
    static final int TRANSACTION_killUidForPermissionChange = 223;
    
    static final int TRANSACTION_launchBugReportHandlerApp = 141;
    
    static final int TRANSACTION_makePackageIdle = 189;
    
    static final int TRANSACTION_moveActivityTaskToBack = 69;
    
    static final int TRANSACTION_moveTaskToFront = 21;
    
    static final int TRANSACTION_moveTaskToStack = 149;
    
    static final int TRANSACTION_moveTopActivityToPinnedStack = 184;
    
    static final int TRANSACTION_noteAlarmFinish = 177;
    
    static final int TRANSACTION_noteAlarmStart = 176;
    
    static final int TRANSACTION_noteWakeupAlarm = 62;
    
    static final int TRANSACTION_notifyCleartextNetwork = 169;
    
    static final int TRANSACTION_notifyLockedProfile = 192;
    
    static final int TRANSACTION_onBackPressedOnTheiaMonitor = 220;
    
    static final int TRANSACTION_openContentUri = 1;
    
    static final int TRANSACTION_peekService = 77;
    
    static final int TRANSACTION_performIdleMaintenance = 153;
    
    static final int TRANSACTION_positionTaskInStack = 182;
    
    static final int TRANSACTION_profileControl = 78;
    
    static final int TRANSACTION_publishContentProviders = 24;
    
    static final int TRANSACTION_publishService = 33;
    
    static final int TRANSACTION_refContentProvider = 25;
    
    static final int TRANSACTION_registerIntentSenderCancelListener = 59;
    
    static final int TRANSACTION_registerProcessObserver = 107;
    
    static final int TRANSACTION_registerReceiver = 12;
    
    static final int TRANSACTION_registerReceiverWithFeature = 13;
    
    static final int TRANSACTION_registerTaskStackListener = 167;
    
    static final int TRANSACTION_registerUidObserver = 2;
    
    static final int TRANSACTION_registerUserSwitchObserver = 129;
    
    static final int TRANSACTION_removeContentProvider = 63;
    
    static final int TRANSACTION_removeContentProviderExternal = 115;
    
    static final int TRANSACTION_removeContentProviderExternalAsUser = 116;
    
    static final int TRANSACTION_removeStack = 188;
    
    static final int TRANSACTION_removeTask = 106;
    
    static final int TRANSACTION_reportBindApplicationFinished = 222;
    
    static final int TRANSACTION_requestBugReport = 133;
    
    static final int TRANSACTION_requestBugReportWithDescription = 134;
    
    static final int TRANSACTION_requestFullBugReport = 139;
    
    static final int TRANSACTION_requestInteractiveBugReport = 138;
    
    static final int TRANSACTION_requestInteractiveBugReportWithDescription = 137;
    
    static final int TRANSACTION_requestRemoteBugReport = 140;
    
    static final int TRANSACTION_requestSystemServerHeapDump = 132;
    
    static final int TRANSACTION_requestTelephonyBugReport = 135;
    
    static final int TRANSACTION_requestWifiBugReport = 136;
    
    static final int TRANSACTION_resizeTask = 171;
    
    static final int TRANSACTION_restart = 152;
    
    static final int TRANSACTION_restartUserInBackground = 199;
    
    static final int TRANSACTION_resumeAppSwitches = 81;
    
    static final int TRANSACTION_revokeUriPermission = 49;
    
    static final int TRANSACTION_scheduleApplicationInfoChanged = 202;
    
    static final int TRANSACTION_sendIdleJobTrigger = 194;
    
    static final int TRANSACTION_sendIntentSender = 195;
    
    static final int TRANSACTION_sendTheiaEvent = 221;
    
    static final int TRANSACTION_serviceDoneExecuting = 54;
    
    static final int TRANSACTION_setActivityController = 50;
    
    static final int TRANSACTION_setActivityLocusContext = 214;
    
    static final int TRANSACTION_setAgentApp = 35;
    
    static final int TRANSACTION_setAlwaysFinish = 36;
    
    static final int TRANSACTION_setDebugApp = 34;
    
    static final int TRANSACTION_setDumpHeapDebugLimit = 173;
    
    static final int TRANSACTION_setFocusedStack = 150;
    
    static final int TRANSACTION_setHasTopUi = 198;
    
    static final int TRANSACTION_setHwuiTaskThreads = 6;
    
    static final int TRANSACTION_setOppoKinectActivityController = 219;
    
    static final int TRANSACTION_setPackageScreenCompatMode = 104;
    
    static final int TRANSACTION_setPersistentVrThread = 203;
    
    static final int TRANSACTION_setProcessImportant = 66;
    
    static final int TRANSACTION_setProcessLimit = 44;
    
    static final int TRANSACTION_setProcessMemoryTrimLevel = 156;
    
    static final int TRANSACTION_setProcessStateSummary = 215;
    
    static final int TRANSACTION_setRenderThread = 197;
    
    static final int TRANSACTION_setRequestedOrientation = 64;
    
    static final int TRANSACTION_setServiceForeground = 67;
    
    static final int TRANSACTION_setTaskResizeable = 170;
    
    static final int TRANSACTION_setUserIsMonkey = 146;
    
    static final int TRANSACTION_showBootMessage = 112;
    
    static final int TRANSACTION_showWaitingForDebugger = 51;
    
    static final int TRANSACTION_shutdown = 79;
    
    static final int TRANSACTION_signalPersistentProcesses = 52;
    
    static final int TRANSACTION_startActivity = 8;
    
    static final int TRANSACTION_startActivityAsUser = 125;
    
    static final int TRANSACTION_startActivityAsUserWithFeature = 126;
    
    static final int TRANSACTION_startActivityFromRecents = 162;
    
    static final int TRANSACTION_startActivityWithFeature = 9;
    
    static final int TRANSACTION_startBinderTracking = 180;
    
    static final int TRANSACTION_startConfirmDeviceCredentialIntent = 193;
    
    static final int TRANSACTION_startDelegateShellPermissionIdentity = 207;
    
    static final int TRANSACTION_startInstrumentation = 37;
    
    static final int TRANSACTION_startRecentsActivity = 160;
    
    static final int TRANSACTION_startService = 27;
    
    static final int TRANSACTION_startSystemLockTaskMode = 163;
    
    static final int TRANSACTION_startUserInBackground = 158;
    
    static final int TRANSACTION_startUserInBackgroundWithListener = 206;
    
    static final int TRANSACTION_startUserInForegroundWithListener = 210;
    
    static final int TRANSACTION_stopAppSwitches = 80;
    
    static final int TRANSACTION_stopBinderTrackingAndDump = 181;
    
    static final int TRANSACTION_stopDelegateShellPermissionIdentity = 208;
    
    static final int TRANSACTION_stopService = 28;
    
    static final int TRANSACTION_stopServiceToken = 43;
    
    static final int TRANSACTION_stopUser = 127;
    
    static final int TRANSACTION_stopUserWithDelayedLocking = 128;
    
    static final int TRANSACTION_suppressResizeConfigChanges = 183;
    
    static final int TRANSACTION_switchUser = 105;
    
    static final int TRANSACTION_unbindBackupAgent = 84;
    
    static final int TRANSACTION_unbindFinished = 65;
    
    static final int TRANSACTION_unbindService = 32;
    
    static final int TRANSACTION_unbroadcastIntent = 17;
    
    static final int TRANSACTION_unhandledBack = 10;
    
    static final int TRANSACTION_unlockUser = 186;
    
    static final int TRANSACTION_unregisterIntentSenderCancelListener = 60;
    
    static final int TRANSACTION_unregisterProcessObserver = 108;
    
    static final int TRANSACTION_unregisterReceiver = 14;
    
    static final int TRANSACTION_unregisterTaskStackListener = 168;
    
    static final int TRANSACTION_unregisterUidObserver = 3;
    
    static final int TRANSACTION_unregisterUserSwitchObserver = 130;
    
    static final int TRANSACTION_unstableProviderDied = 121;
    
    static final int TRANSACTION_updateConfiguration = 41;
    
    static final int TRANSACTION_updateDeviceOwner = 179;
    
    static final int TRANSACTION_updateLockTaskPackages = 175;
    
    static final int TRANSACTION_updateMccMncConfiguration = 42;
    
    static final int TRANSACTION_updatePersistentConfiguration = 110;
    
    static final int TRANSACTION_updateServiceGroup = 31;
    
    static final int TRANSACTION_waitForNetworkStateUpdate = 204;
    
    public Stub() {
      attachInterface(this, "android.app.IActivityManager");
    }
    
    public static IActivityManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.app.IActivityManager");
      if (iInterface != null && iInterface instanceof IActivityManager)
        return (IActivityManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 223:
          return "killUidForPermissionChange";
        case 222:
          return "reportBindApplicationFinished";
        case 221:
          return "sendTheiaEvent";
        case 220:
          return "onBackPressedOnTheiaMonitor";
        case 219:
          return "setOppoKinectActivityController";
        case 218:
          return "detectExceptionsForOIDT";
        case 217:
          return "getTopAppName";
        case 216:
          return "isAppFreezerSupported";
        case 215:
          return "setProcessStateSummary";
        case 214:
          return "setActivityLocusContext";
        case 213:
          return "killProcessesWhenImperceptible";
        case 212:
          return "getHistoricalProcessExitReasons";
        case 211:
          return "appNotResponding";
        case 210:
          return "startUserInForegroundWithListener";
        case 209:
          return "getLifeMonitor";
        case 208:
          return "stopDelegateShellPermissionIdentity";
        case 207:
          return "startDelegateShellPermissionIdentity";
        case 206:
          return "startUserInBackgroundWithListener";
        case 205:
          return "backgroundWhitelistUid";
        case 204:
          return "waitForNetworkStateUpdate";
        case 203:
          return "setPersistentVrThread";
        case 202:
          return "scheduleApplicationInfoChanged";
        case 201:
          return "getTaskSnapshot";
        case 200:
          return "cancelTaskWindowTransition";
        case 199:
          return "restartUserInBackground";
        case 198:
          return "setHasTopUi";
        case 197:
          return "setRenderThread";
        case 196:
          return "isBackgroundRestricted";
        case 195:
          return "sendIntentSender";
        case 194:
          return "sendIdleJobTrigger";
        case 193:
          return "startConfirmDeviceCredentialIntent";
        case 192:
          return "notifyLockedProfile";
        case 191:
          return "isVrModePackageEnabled";
        case 190:
          return "getMemoryTrimLevel";
        case 189:
          return "makePackageIdle";
        case 188:
          return "removeStack";
        case 187:
          return "killPackageDependents";
        case 186:
          return "unlockUser";
        case 185:
          return "isAppStartModeDisabled";
        case 184:
          return "moveTopActivityToPinnedStack";
        case 183:
          return "suppressResizeConfigChanges";
        case 182:
          return "positionTaskInStack";
        case 181:
          return "stopBinderTrackingAndDump";
        case 180:
          return "startBinderTracking";
        case 179:
          return "updateDeviceOwner";
        case 178:
          return "getPackageProcessState";
        case 177:
          return "noteAlarmFinish";
        case 176:
          return "noteAlarmStart";
        case 175:
          return "updateLockTaskPackages";
        case 174:
          return "dumpHeapFinished";
        case 173:
          return "setDumpHeapDebugLimit";
        case 172:
          return "getLockTaskModeState";
        case 171:
          return "resizeTask";
        case 170:
          return "setTaskResizeable";
        case 169:
          return "notifyCleartextNetwork";
        case 168:
          return "unregisterTaskStackListener";
        case 167:
          return "registerTaskStackListener";
        case 166:
          return "checkPermissionWithToken";
        case 165:
          return "bootAnimationComplete";
        case 164:
          return "isTopOfTask";
        case 163:
          return "startSystemLockTaskMode";
        case 162:
          return "startActivityFromRecents";
        case 161:
          return "cancelRecentsAnimation";
        case 160:
          return "startRecentsActivity";
        case 159:
          return "isInLockTaskMode";
        case 158:
          return "startUserInBackground";
        case 157:
          return "getTagForIntentSender";
        case 156:
          return "setProcessMemoryTrimLevel";
        case 155:
          return "getTaskBounds";
        case 154:
          return "appNotRespondingViaProvider";
        case 153:
          return "performIdleMaintenance";
        case 152:
          return "restart";
        case 151:
          return "getFocusedStackInfo";
        case 150:
          return "setFocusedStack";
        case 149:
          return "moveTaskToStack";
        case 148:
          return "getAllStackInfos";
        case 147:
          return "hang";
        case 146:
          return "setUserIsMonkey";
        case 145:
          return "killUid";
        case 144:
          return "getLaunchedFromPackage";
        case 143:
          return "getIntentForIntentSender";
        case 142:
          return "getBugreportWhitelistedPackages";
        case 141:
          return "launchBugReportHandlerApp";
        case 140:
          return "requestRemoteBugReport";
        case 139:
          return "requestFullBugReport";
        case 138:
          return "requestInteractiveBugReport";
        case 137:
          return "requestInteractiveBugReportWithDescription";
        case 136:
          return "requestWifiBugReport";
        case 135:
          return "requestTelephonyBugReport";
        case 134:
          return "requestBugReportWithDescription";
        case 133:
          return "requestBugReport";
        case 132:
          return "requestSystemServerHeapDump";
        case 131:
          return "getRunningUserIds";
        case 130:
          return "unregisterUserSwitchObserver";
        case 129:
          return "registerUserSwitchObserver";
        case 128:
          return "stopUserWithDelayedLocking";
        case 127:
          return "stopUser";
        case 126:
          return "startActivityAsUserWithFeature";
        case 125:
          return "startActivityAsUser";
        case 124:
          return "isIntentSenderABroadcast";
        case 123:
          return "isIntentSenderAForegroundService";
        case 122:
          return "isIntentSenderAnActivity";
        case 121:
          return "unstableProviderDied";
        case 120:
          return "getLaunchedFromUid";
        case 119:
          return "getCurrentUser";
        case 118:
          return "killProcessesBelowForeground";
        case 117:
          return "getMyMemoryState";
        case 116:
          return "removeContentProviderExternalAsUser";
        case 115:
          return "removeContentProviderExternal";
        case 114:
          return "getContentProviderExternal";
        case 113:
          return "killAllBackgroundProcesses";
        case 112:
          return "showBootMessage";
        case 111:
          return "getProcessPss";
        case 110:
          return "updatePersistentConfiguration";
        case 109:
          return "isIntentSenderTargetedToPackage";
        case 108:
          return "unregisterProcessObserver";
        case 107:
          return "registerProcessObserver";
        case 106:
          return "removeTask";
        case 105:
          return "switchUser";
        case 104:
          return "setPackageScreenCompatMode";
        case 103:
          return "isUserRunning";
        case 102:
          return "dumpHeap";
        case 101:
          return "getProviderMimeTypeAsync";
        case 100:
          return "getProviderMimeType";
        case 99:
          return "crashApplication";
        case 98:
          return "isTopActivityImmersive";
        case 97:
          return "handleApplicationStrictModeViolation";
        case 96:
          return "finishHeavyWeightApp";
        case 95:
          return "getRunningExternalApplications";
        case 94:
          return "isUserAMonkey";
        case 93:
          return "killBackgroundProcesses";
        case 92:
          return "handleApplicationWtf";
        case 91:
          return "killApplicationProcess";
        case 90:
          return "getProcessMemoryInfo";
        case 89:
          return "closeSystemDialogs";
        case 88:
          return "killApplication";
        case 87:
          return "addPackageDependency";
        case 86:
          return "handleIncomingUser";
        case 85:
          return "getUidForIntentSender";
        case 84:
          return "unbindBackupAgent";
        case 83:
          return "backupAgentCreated";
        case 82:
          return "bindBackupAgent";
        case 81:
          return "resumeAppSwitches";
        case 80:
          return "stopAppSwitches";
        case 79:
          return "shutdown";
        case 78:
          return "profileControl";
        case 77:
          return "peekService";
        case 76:
          return "getRunningAppProcesses";
        case 75:
          return "getServices";
        case 74:
          return "killPids";
        case 73:
          return "forceStopPackage";
        case 72:
          return "clearApplicationUserData";
        case 71:
          return "getProcessesInErrorState";
        case 70:
          return "getMemoryInfo";
        case 69:
          return "moveActivityTaskToBack";
        case 68:
          return "getForegroundServiceType";
        case 67:
          return "setServiceForeground";
        case 66:
          return "setProcessImportant";
        case 65:
          return "unbindFinished";
        case 64:
          return "setRequestedOrientation";
        case 63:
          return "removeContentProvider";
        case 62:
          return "noteWakeupAlarm";
        case 61:
          return "enterSafeMode";
        case 60:
          return "unregisterIntentSenderCancelListener";
        case 59:
          return "registerIntentSenderCancelListener";
        case 58:
          return "getPackageForIntentSender";
        case 57:
          return "cancelIntentSender";
        case 56:
          return "getIntentSenderWithFeature";
        case 55:
          return "getIntentSender";
        case 54:
          return "serviceDoneExecuting";
        case 53:
          return "getRecentTasks";
        case 52:
          return "signalPersistentProcesses";
        case 51:
          return "showWaitingForDebugger";
        case 50:
          return "setActivityController";
        case 49:
          return "revokeUriPermission";
        case 48:
          return "grantUriPermission";
        case 47:
          return "checkUriPermission";
        case 46:
          return "checkPermission";
        case 45:
          return "getProcessLimit";
        case 44:
          return "setProcessLimit";
        case 43:
          return "stopServiceToken";
        case 42:
          return "updateMccMncConfiguration";
        case 41:
          return "updateConfiguration";
        case 40:
          return "getConfiguration";
        case 39:
          return "finishInstrumentation";
        case 38:
          return "addInstrumentationResults";
        case 37:
          return "startInstrumentation";
        case 36:
          return "setAlwaysFinish";
        case 35:
          return "setAgentApp";
        case 34:
          return "setDebugApp";
        case 33:
          return "publishService";
        case 32:
          return "unbindService";
        case 31:
          return "updateServiceGroup";
        case 30:
          return "bindIsolatedService";
        case 29:
          return "bindService";
        case 28:
          return "stopService";
        case 27:
          return "startService";
        case 26:
          return "getRunningServiceControlPanel";
        case 25:
          return "refContentProvider";
        case 24:
          return "publishContentProviders";
        case 23:
          return "getContentProvider";
        case 22:
          return "getTaskForActivity";
        case 21:
          return "moveTaskToFront";
        case 20:
          return "getTasks";
        case 19:
          return "attachApplication";
        case 18:
          return "finishReceiver";
        case 17:
          return "unbroadcastIntent";
        case 16:
          return "broadcastIntentWithFeature";
        case 15:
          return "broadcastIntent";
        case 14:
          return "unregisterReceiver";
        case 13:
          return "registerReceiverWithFeature";
        case 12:
          return "registerReceiver";
        case 11:
          return "finishActivity";
        case 10:
          return "unhandledBack";
        case 9:
          return "startActivityWithFeature";
        case 8:
          return "startActivity";
        case 7:
          return "handleApplicationCrash";
        case 6:
          return "setHwuiTaskThreads";
        case 5:
          return "getUidProcessState";
        case 4:
          return "isUidActive";
        case 3:
          return "unregisterUidObserver";
        case 2:
          return "registerUidObserver";
        case 1:
          break;
      } 
      return "openContentUri";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        boolean bool34;
        int i29;
        boolean bool33;
        int i28;
        boolean bool32;
        int i27;
        boolean bool31;
        int i26;
        boolean bool30;
        int i25;
        boolean bool29;
        int i24;
        boolean bool28;
        int i23;
        boolean bool27;
        int i22;
        boolean bool26;
        int i21;
        boolean bool25;
        int i20;
        boolean bool24;
        int i19;
        boolean bool23;
        int i18;
        boolean bool22;
        int i17;
        boolean bool21;
        int i16;
        boolean bool20;
        int i15;
        boolean bool19;
        int i14;
        boolean bool18;
        int i13;
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str24;
        IOplusKinectActivityController iOplusKinectActivityController;
        String str23;
        ComponentName componentName2;
        byte[] arrayOfByte2;
        IBinder iBinder12;
        String str22;
        ParceledListSlice<ApplicationExitInfo> parceledListSlice1;
        String str21;
        IProgressListener iProgressListener3;
        ParcelFileDescriptor parcelFileDescriptor2;
        String[] arrayOfString2;
        IProgressListener iProgressListener2;
        ActivityManager.TaskSnapshot taskSnapshot;
        String str20;
        IProgressListener iProgressListener1;
        String str19, arrayOfString1[], str18;
        byte[] arrayOfByte1;
        ITaskStackListener iTaskStackListener;
        IBinder iBinder11;
        IRecentsAnimationRunner iRecentsAnimationRunner;
        String str17;
        Rect rect;
        IBinder iBinder10;
        ActivityManager.StackInfo stackInfo;
        List<ActivityManager.StackInfo> list4;
        String str16;
        IBinder iBinder9;
        String str15;
        IIntentSender iIntentSender6;
        Intent intent2;
        List<String> list3;
        String str14;
        int[] arrayOfInt3;
        IUserSwitchObserver iUserSwitchObserver1;
        String str13;
        IStopUserCallback iStopUserCallback;
        IIntentSender iIntentSender5;
        IBinder iBinder8;
        UserInfo userInfo;
        String str12;
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo;
        IBinder iBinder7;
        String str11;
        ContentProviderHolder contentProviderHolder2;
        int[] arrayOfInt2;
        long[] arrayOfLong;
        IIntentSender iIntentSender4;
        IProcessObserver iProcessObserver;
        String str10;
        List<ApplicationInfo> list2;
        int[] arrayOfInt1;
        Debug.MemoryInfo[] arrayOfMemoryInfo;
        String str9;
        IIntentSender iIntentSender3;
        String str8;
        IBinder iBinder6;
        List<ActivityManager.RunningAppProcessInfo> list1;
        ActivityManager.MemoryInfo memoryInfo;
        IBinder iBinder5;
        String str7;
        IResultReceiver iResultReceiver;
        IIntentSender iIntentSender2;
        String str6;
        IIntentSender iIntentSender1;
        IBinder iBinder4;
        String str5;
        IBinder iBinder3;
        ParceledListSlice parceledListSlice;
        IBinder iBinder2;
        String str4;
        Configuration configuration;
        String str3;
        IBinder iBinder1;
        IServiceConnection iServiceConnection1;
        ComponentName componentName1;
        PendingIntent pendingIntent;
        ArrayList<ContentProviderHolder> arrayList;
        ContentProviderHolder contentProviderHolder1;
        List<ActivityManager.RunningTaskInfo> list;
        IIntentReceiver iIntentReceiver1;
        Intent intent1;
        String str2;
        IUidObserver iUidObserver1;
        String str25, str29;
        IBinder iBinder14;
        String str28;
        IApplicationThread iApplicationThread2;
        String str27;
        IApplicationThread iApplicationThread1;
        IBinder iBinder13;
        String str26;
        IIntentSender iIntentSender9;
        byte[] arrayOfByte3;
        IIntentSender iIntentSender8;
        IAssistDataReceiver iAssistDataReceiver;
        String str34;
        IBinder iBinder20;
        String str33;
        int[] arrayOfInt4;
        String str32;
        IBinder iBinder19;
        IIntentSender iIntentSender7;
        String arrayOfString3[], str31;
        IBinder iBinder18;
        IApplicationThread iApplicationThread5;
        String str30;
        IApplicationThread iApplicationThread4;
        IBinder iBinder17;
        IApplicationThread iApplicationThread3;
        IIntentReceiver iIntentReceiver2;
        IBinder iBinder16;
        long l;
        String str48;
        int[] arrayOfInt5;
        String str47;
        ArrayList<String> arrayList1;
        String str46;
        byte[] arrayOfByte4;
        String str45;
        IIntentSender iIntentSender11;
        String str44;
        IBinder iBinder28;
        String str43;
        IUserSwitchObserver iUserSwitchObserver2;
        IBinder iBinder27;
        String str42;
        IBinder iBinder26;
        String str41;
        IBinder iBinder25;
        String str40;
        IPackageDataObserver iPackageDataObserver;
        IBinder iBinder24;
        IIntentSender iIntentSender10;
        IApplicationThread iApplicationThread9;
        IActivityController iActivityController;
        String str39;
        IApplicationThread iApplicationThread8;
        String str38;
        IServiceConnection iServiceConnection2;
        IBinder iBinder23;
        IApplicationThread iApplicationThread7;
        IBinder iBinder22;
        String str37;
        IApplicationThread iApplicationThread6;
        String str36;
        IBinder iBinder21;
        IUidObserver iUidObserver2;
        int i30;
        String str52, arrayOfString4[];
        Intent[] arrayOfIntent1;
        IInstrumentationWatcher iInstrumentationWatcher;
        IServiceConnection iServiceConnection3;
        String str51;
        IApplicationThread iApplicationThread11;
        String str50;
        IIntentReceiver iIntentReceiver3;
        String str49;
        IApplicationThread iApplicationThread10;
        IBinder iBinder29;
        IIntentReceiver iIntentReceiver4;
        IApplicationThread iApplicationThread15;
        Intent[] arrayOfIntent2;
        String str57;
        IUiAutomationConnection iUiAutomationConnection;
        String str56;
        IServiceConnection iServiceConnection4;
        String str55;
        IApplicationThread iApplicationThread14;
        String str54;
        IApplicationThread iApplicationThread13;
        String str53;
        IApplicationThread iApplicationThread12;
        String str61;
        IBinder iBinder31;
        String str60;
        IIntentReceiver iIntentReceiver5;
        String str59;
        IBinder iBinder30;
        String str58, str63;
        IIntentReceiver iIntentReceiver6;
        String str62;
        IBinder iBinder32;
        String arrayOfString5[], str66;
        IBinder iBinder33;
        String str65, arrayOfString6[], str64;
        int i31;
        IBinder iBinder15 = null;
        String str35 = null;
        boolean bool35 = false, bool36 = false, bool37 = false, bool38 = false, bool39 = false, bool40 = false, bool41 = false, bool42 = false, bool43 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 223:
            param1Parcel1.enforceInterface("android.app.IActivityManager");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str24 = param1Parcel1.readString();
            killUidForPermissionChange(param1Int2, param1Int1, str24);
            param1Parcel2.writeNoException();
            return true;
          case 222:
            str24.enforceInterface("android.app.IActivityManager");
            str25 = str24.readString();
            param1Int1 = str24.readInt();
            param1Int2 = str24.readInt();
            reportBindApplicationFinished(str25, param1Int1, param1Int2);
            return true;
          case 221:
            str24.enforceInterface("android.app.IActivityManager");
            l = str24.readLong();
            if (str24.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str24);
            } else {
              str24 = null;
            } 
            sendTheiaEvent(l, (Intent)str24);
            return true;
          case 220:
            str24.enforceInterface("android.app.IActivityManager");
            l = str24.readLong();
            onBackPressedOnTheiaMonitor(l);
            return true;
          case 219:
            str24.enforceInterface("android.app.IActivityManager");
            iOplusKinectActivityController = IOplusKinectActivityController.Stub.asInterface(str24.readStrongBinder());
            setOppoKinectActivityController(iOplusKinectActivityController);
            str25.writeNoException();
            return true;
          case 218:
            iOplusKinectActivityController.enforceInterface("android.app.IActivityManager");
            param1Int1 = iOplusKinectActivityController.readInt();
            str48 = iOplusKinectActivityController.readString();
            str35 = iOplusKinectActivityController.readString();
            str23 = iOplusKinectActivityController.readString();
            detectExceptionsForOIDT(param1Int1, str48, str35, str23);
            str25.writeNoException();
            return true;
          case 217:
            str23.enforceInterface("android.app.IActivityManager");
            componentName2 = getTopAppName();
            str25.writeNoException();
            if (componentName2 != null) {
              str25.writeInt(1);
              componentName2.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 216:
            componentName2.enforceInterface("android.app.IActivityManager");
            bool34 = isAppFreezerSupported();
            str25.writeNoException();
            str25.writeInt(bool34);
            return true;
          case 215:
            componentName2.enforceInterface("android.app.IActivityManager");
            arrayOfByte2 = componentName2.createByteArray();
            setProcessStateSummary(arrayOfByte2);
            str25.writeNoException();
            return true;
          case 214:
            arrayOfByte2.enforceInterface("android.app.IActivityManager");
            if (arrayOfByte2.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              str48 = null;
            } 
            if (arrayOfByte2.readInt() != 0) {
              LocusId locusId = (LocusId)LocusId.CREATOR.createFromParcel((Parcel)arrayOfByte2);
            } else {
              str35 = null;
            } 
            iBinder12 = arrayOfByte2.readStrongBinder();
            setActivityLocusContext((ComponentName)str48, (LocusId)str35, iBinder12);
            str25.writeNoException();
            return true;
          case 213:
            iBinder12.enforceInterface("android.app.IActivityManager");
            arrayOfInt5 = iBinder12.createIntArray();
            str22 = iBinder12.readString();
            killProcessesWhenImperceptible(arrayOfInt5, str22);
            str25.writeNoException();
            return true;
          case 212:
            str22.enforceInterface("android.app.IActivityManager");
            str47 = str22.readString();
            i30 = str22.readInt();
            i29 = str22.readInt();
            param1Int2 = str22.readInt();
            parceledListSlice1 = getHistoricalProcessExitReasons(str47, i30, i29, param1Int2);
            str25.writeNoException();
            if (parceledListSlice1 != null) {
              str25.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 211:
            parceledListSlice1.enforceInterface("android.app.IActivityManager");
            str21 = parceledListSlice1.readString();
            appNotResponding(str21);
            str25.writeNoException();
            return true;
          case 210:
            str21.enforceInterface("android.app.IActivityManager");
            i29 = str21.readInt();
            iProgressListener3 = IProgressListener.Stub.asInterface(str21.readStrongBinder());
            bool33 = startUserInForegroundWithListener(i29, iProgressListener3);
            str25.writeNoException();
            str25.writeInt(bool33);
            return true;
          case 209:
            iProgressListener3.enforceInterface("android.app.IActivityManager");
            parcelFileDescriptor2 = getLifeMonitor();
            str25.writeNoException();
            if (parcelFileDescriptor2 != null) {
              str25.writeInt(1);
              parcelFileDescriptor2.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 208:
            parcelFileDescriptor2.enforceInterface("android.app.IActivityManager");
            stopDelegateShellPermissionIdentity();
            str25.writeNoException();
            return true;
          case 207:
            parcelFileDescriptor2.enforceInterface("android.app.IActivityManager");
            i28 = parcelFileDescriptor2.readInt();
            arrayOfString2 = parcelFileDescriptor2.createStringArray();
            startDelegateShellPermissionIdentity(i28, arrayOfString2);
            str25.writeNoException();
            return true;
          case 206:
            arrayOfString2.enforceInterface("android.app.IActivityManager");
            i28 = arrayOfString2.readInt();
            iProgressListener2 = IProgressListener.Stub.asInterface(arrayOfString2.readStrongBinder());
            bool32 = startUserInBackgroundWithListener(i28, iProgressListener2);
            str25.writeNoException();
            str25.writeInt(bool32);
            return true;
          case 205:
            iProgressListener2.enforceInterface("android.app.IActivityManager");
            i27 = iProgressListener2.readInt();
            backgroundWhitelistUid(i27);
            str25.writeNoException();
            return true;
          case 204:
            iProgressListener2.enforceInterface("android.app.IActivityManager");
            l = iProgressListener2.readLong();
            waitForNetworkStateUpdate(l);
            str25.writeNoException();
            return true;
          case 203:
            iProgressListener2.enforceInterface("android.app.IActivityManager");
            i27 = iProgressListener2.readInt();
            setPersistentVrThread(i27);
            str25.writeNoException();
            return true;
          case 202:
            iProgressListener2.enforceInterface("android.app.IActivityManager");
            arrayList1 = iProgressListener2.createStringArrayList();
            i27 = iProgressListener2.readInt();
            scheduleApplicationInfoChanged(arrayList1, i27);
            str25.writeNoException();
            return true;
          case 201:
            iProgressListener2.enforceInterface("android.app.IActivityManager");
            i27 = iProgressListener2.readInt();
            if (iProgressListener2.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            taskSnapshot = getTaskSnapshot(i27, bool43);
            str25.writeNoException();
            if (taskSnapshot != null) {
              str25.writeInt(1);
              taskSnapshot.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 200:
            taskSnapshot.enforceInterface("android.app.IActivityManager");
            i27 = taskSnapshot.readInt();
            cancelTaskWindowTransition(i27);
            str25.writeNoException();
            return true;
          case 199:
            taskSnapshot.enforceInterface("android.app.IActivityManager");
            i27 = taskSnapshot.readInt();
            i27 = restartUserInBackground(i27);
            str25.writeNoException();
            str25.writeInt(i27);
            return true;
          case 198:
            taskSnapshot.enforceInterface("android.app.IActivityManager");
            if (taskSnapshot.readInt() != 0)
              bool43 = true; 
            setHasTopUi(bool43);
            str25.writeNoException();
            return true;
          case 197:
            taskSnapshot.enforceInterface("android.app.IActivityManager");
            i27 = taskSnapshot.readInt();
            setRenderThread(i27);
            str25.writeNoException();
            return true;
          case 196:
            taskSnapshot.enforceInterface("android.app.IActivityManager");
            str20 = taskSnapshot.readString();
            bool31 = isBackgroundRestricted(str20);
            str25.writeNoException();
            str25.writeInt(bool31);
            return true;
          case 195:
            str20.enforceInterface("android.app.IActivityManager");
            iIntentSender9 = IIntentSender.Stub.asInterface(str20.readStrongBinder());
            iBinder15 = str20.readStrongBinder();
            i26 = str20.readInt();
            if (str20.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str20);
            } else {
              arrayList1 = null;
            } 
            str52 = str20.readString();
            iIntentReceiver4 = IIntentReceiver.Stub.asInterface(str20.readStrongBinder());
            str61 = str20.readString();
            if (str20.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str20);
            } else {
              str20 = null;
            } 
            i26 = sendIntentSender(iIntentSender9, iBinder15, i26, (Intent)arrayList1, str52, iIntentReceiver4, str61, (Bundle)str20);
            str25.writeNoException();
            str25.writeInt(i26);
            return true;
          case 194:
            str20.enforceInterface("android.app.IActivityManager");
            sendIdleJobTrigger();
            str25.writeNoException();
            return true;
          case 193:
            str20.enforceInterface("android.app.IActivityManager");
            if (str20.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str20);
            } else {
              arrayList1 = null;
            } 
            if (str20.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str20);
            } else {
              str20 = null;
            } 
            startConfirmDeviceCredentialIntent((Intent)arrayList1, (Bundle)str20);
            str25.writeNoException();
            return true;
          case 192:
            str20.enforceInterface("android.app.IActivityManager");
            i26 = str20.readInt();
            notifyLockedProfile(i26);
            str25.writeNoException();
            return true;
          case 191:
            str20.enforceInterface("android.app.IActivityManager");
            if (str20.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str20);
            } else {
              str20 = null;
            } 
            bool30 = isVrModePackageEnabled((ComponentName)str20);
            str25.writeNoException();
            str25.writeInt(bool30);
            return true;
          case 190:
            str20.enforceInterface("android.app.IActivityManager");
            i25 = getMemoryTrimLevel();
            str25.writeNoException();
            str25.writeInt(i25);
            return true;
          case 189:
            str20.enforceInterface("android.app.IActivityManager");
            str46 = str20.readString();
            i25 = str20.readInt();
            makePackageIdle(str46, i25);
            str25.writeNoException();
            return true;
          case 188:
            str20.enforceInterface("android.app.IActivityManager");
            i25 = str20.readInt();
            removeStack(i25);
            str25.writeNoException();
            return true;
          case 187:
            str20.enforceInterface("android.app.IActivityManager");
            str46 = str20.readString();
            i25 = str20.readInt();
            killPackageDependents(str46, i25);
            str25.writeNoException();
            return true;
          case 186:
            str20.enforceInterface("android.app.IActivityManager");
            i25 = str20.readInt();
            arrayOfByte4 = str20.createByteArray();
            arrayOfByte3 = str20.createByteArray();
            iProgressListener1 = IProgressListener.Stub.asInterface(str20.readStrongBinder());
            bool29 = unlockUser(i25, arrayOfByte4, arrayOfByte3, iProgressListener1);
            str25.writeNoException();
            str25.writeInt(bool29);
            return true;
          case 185:
            iProgressListener1.enforceInterface("android.app.IActivityManager");
            i24 = iProgressListener1.readInt();
            str19 = iProgressListener1.readString();
            bool28 = isAppStartModeDisabled(i24, str19);
            str25.writeNoException();
            str25.writeInt(bool28);
            return true;
          case 184:
            str19.enforceInterface("android.app.IActivityManager");
            i23 = str19.readInt();
            if (str19.readInt() != 0) {
              Rect rect1 = (Rect)Rect.CREATOR.createFromParcel((Parcel)str19);
            } else {
              str19 = null;
            } 
            bool27 = moveTopActivityToPinnedStack(i23, (Rect)str19);
            str25.writeNoException();
            str25.writeInt(bool27);
            return true;
          case 183:
            str19.enforceInterface("android.app.IActivityManager");
            bool43 = bool35;
            if (str19.readInt() != 0)
              bool43 = true; 
            suppressResizeConfigChanges(bool43);
            str25.writeNoException();
            return true;
          case 182:
            str19.enforceInterface("android.app.IActivityManager");
            param1Int2 = str19.readInt();
            i30 = str19.readInt();
            i22 = str19.readInt();
            positionTaskInStack(param1Int2, i30, i22);
            str25.writeNoException();
            return true;
          case 181:
            str19.enforceInterface("android.app.IActivityManager");
            if (str19.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)str19);
            } else {
              str19 = null;
            } 
            bool26 = stopBinderTrackingAndDump((ParcelFileDescriptor)str19);
            str25.writeNoException();
            str25.writeInt(bool26);
            return true;
          case 180:
            str19.enforceInterface("android.app.IActivityManager");
            bool26 = startBinderTracking();
            str25.writeNoException();
            str25.writeInt(bool26);
            return true;
          case 179:
            str19.enforceInterface("android.app.IActivityManager");
            str19 = str19.readString();
            updateDeviceOwner(str19);
            str25.writeNoException();
            return true;
          case 178:
            str19.enforceInterface("android.app.IActivityManager");
            str45 = str19.readString();
            str19 = str19.readString();
            i21 = getPackageProcessState(str45, str19);
            str25.writeNoException();
            str25.writeInt(i21);
            return true;
          case 177:
            str19.enforceInterface("android.app.IActivityManager");
            iIntentSender8 = IIntentSender.Stub.asInterface(str19.readStrongBinder());
            if (str19.readInt() != 0) {
              WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str19);
            } else {
              str45 = null;
            } 
            i21 = str19.readInt();
            str19 = str19.readString();
            noteAlarmFinish(iIntentSender8, (WorkSource)str45, i21, str19);
            str25.writeNoException();
            return true;
          case 176:
            str19.enforceInterface("android.app.IActivityManager");
            iIntentSender8 = IIntentSender.Stub.asInterface(str19.readStrongBinder());
            if (str19.readInt() != 0) {
              WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str19);
            } else {
              str45 = null;
            } 
            i21 = str19.readInt();
            str19 = str19.readString();
            noteAlarmStart(iIntentSender8, (WorkSource)str45, i21, str19);
            str25.writeNoException();
            return true;
          case 175:
            str19.enforceInterface("android.app.IActivityManager");
            i21 = str19.readInt();
            arrayOfString1 = str19.createStringArray();
            updateLockTaskPackages(i21, arrayOfString1);
            str25.writeNoException();
            return true;
          case 174:
            arrayOfString1.enforceInterface("android.app.IActivityManager");
            str18 = arrayOfString1.readString();
            dumpHeapFinished(str18);
            str25.writeNoException();
            return true;
          case 173:
            str18.enforceInterface("android.app.IActivityManager");
            str45 = str18.readString();
            i21 = str18.readInt();
            l = str18.readLong();
            str18 = str18.readString();
            setDumpHeapDebugLimit(str45, i21, l, str18);
            str25.writeNoException();
            return true;
          case 172:
            str18.enforceInterface("android.app.IActivityManager");
            i21 = getLockTaskModeState();
            str25.writeNoException();
            str25.writeInt(i21);
            return true;
          case 171:
            str18.enforceInterface("android.app.IActivityManager");
            i21 = str18.readInt();
            if (str18.readInt() != 0) {
              Rect rect1 = (Rect)Rect.CREATOR.createFromParcel((Parcel)str18);
            } else {
              str45 = null;
            } 
            param1Int2 = str18.readInt();
            resizeTask(i21, (Rect)str45, param1Int2);
            str25.writeNoException();
            return true;
          case 170:
            str18.enforceInterface("android.app.IActivityManager");
            param1Int2 = str18.readInt();
            i21 = str18.readInt();
            setTaskResizeable(param1Int2, i21);
            str25.writeNoException();
            return true;
          case 169:
            str18.enforceInterface("android.app.IActivityManager");
            i21 = str18.readInt();
            arrayOfByte1 = str18.createByteArray();
            notifyCleartextNetwork(i21, arrayOfByte1);
            str25.writeNoException();
            return true;
          case 168:
            arrayOfByte1.enforceInterface("android.app.IActivityManager");
            iTaskStackListener = ITaskStackListener.Stub.asInterface(arrayOfByte1.readStrongBinder());
            unregisterTaskStackListener(iTaskStackListener);
            str25.writeNoException();
            return true;
          case 167:
            iTaskStackListener.enforceInterface("android.app.IActivityManager");
            iTaskStackListener = ITaskStackListener.Stub.asInterface(iTaskStackListener.readStrongBinder());
            registerTaskStackListener(iTaskStackListener);
            str25.writeNoException();
            return true;
          case 166:
            iTaskStackListener.enforceInterface("android.app.IActivityManager");
            str45 = iTaskStackListener.readString();
            i21 = iTaskStackListener.readInt();
            param1Int2 = iTaskStackListener.readInt();
            iBinder11 = iTaskStackListener.readStrongBinder();
            i21 = checkPermissionWithToken(str45, i21, param1Int2, iBinder11);
            str25.writeNoException();
            str25.writeInt(i21);
            return true;
          case 165:
            iBinder11.enforceInterface("android.app.IActivityManager");
            bootAnimationComplete();
            str25.writeNoException();
            return true;
          case 164:
            iBinder11.enforceInterface("android.app.IActivityManager");
            iBinder11 = iBinder11.readStrongBinder();
            bool25 = isTopOfTask(iBinder11);
            str25.writeNoException();
            str25.writeInt(bool25);
            return true;
          case 163:
            iBinder11.enforceInterface("android.app.IActivityManager");
            i20 = iBinder11.readInt();
            startSystemLockTaskMode(i20);
            str25.writeNoException();
            return true;
          case 162:
            iBinder11.enforceInterface("android.app.IActivityManager");
            i20 = iBinder11.readInt();
            if (iBinder11.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder11);
            } else {
              iBinder11 = null;
            } 
            i20 = startActivityFromRecents(i20, (Bundle)iBinder11);
            str25.writeNoException();
            str25.writeInt(i20);
            return true;
          case 161:
            iBinder11.enforceInterface("android.app.IActivityManager");
            bool43 = bool36;
            if (iBinder11.readInt() != 0)
              bool43 = true; 
            cancelRecentsAnimation(bool43);
            str25.writeNoException();
            return true;
          case 160:
            iBinder11.enforceInterface("android.app.IActivityManager");
            if (iBinder11.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iBinder11);
            } else {
              str45 = null;
            } 
            iAssistDataReceiver = IAssistDataReceiver.Stub.asInterface(iBinder11.readStrongBinder());
            iRecentsAnimationRunner = IRecentsAnimationRunner.Stub.asInterface(iBinder11.readStrongBinder());
            startRecentsActivity((Intent)str45, iAssistDataReceiver, iRecentsAnimationRunner);
            str25.writeNoException();
            return true;
          case 159:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityManager");
            bool24 = isInLockTaskMode();
            str25.writeNoException();
            str25.writeInt(bool24);
            return true;
          case 158:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityManager");
            i19 = iRecentsAnimationRunner.readInt();
            bool23 = startUserInBackground(i19);
            str25.writeNoException();
            str25.writeInt(bool23);
            return true;
          case 157:
            iRecentsAnimationRunner.enforceInterface("android.app.IActivityManager");
            iIntentSender11 = IIntentSender.Stub.asInterface(iRecentsAnimationRunner.readStrongBinder());
            str17 = iRecentsAnimationRunner.readString();
            str17 = getTagForIntentSender(iIntentSender11, str17);
            str25.writeNoException();
            str25.writeString(str17);
            return true;
          case 156:
            str17.enforceInterface("android.app.IActivityManager");
            str44 = str17.readString();
            i18 = str17.readInt();
            param1Int2 = str17.readInt();
            bool22 = setProcessMemoryTrimLevel(str44, i18, param1Int2);
            str25.writeNoException();
            str25.writeInt(bool22);
            return true;
          case 155:
            str17.enforceInterface("android.app.IActivityManager");
            i17 = str17.readInt();
            rect = getTaskBounds(i17);
            str25.writeNoException();
            if (rect != null) {
              str25.writeInt(1);
              rect.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 154:
            rect.enforceInterface("android.app.IActivityManager");
            iBinder10 = rect.readStrongBinder();
            appNotRespondingViaProvider(iBinder10);
            str25.writeNoException();
            return true;
          case 153:
            iBinder10.enforceInterface("android.app.IActivityManager");
            performIdleMaintenance();
            str25.writeNoException();
            return true;
          case 152:
            iBinder10.enforceInterface("android.app.IActivityManager");
            restart();
            str25.writeNoException();
            return true;
          case 151:
            iBinder10.enforceInterface("android.app.IActivityManager");
            stackInfo = getFocusedStackInfo();
            str25.writeNoException();
            if (stackInfo != null) {
              str25.writeInt(1);
              stackInfo.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 150:
            stackInfo.enforceInterface("android.app.IActivityManager");
            i17 = stackInfo.readInt();
            setFocusedStack(i17);
            str25.writeNoException();
            return true;
          case 149:
            stackInfo.enforceInterface("android.app.IActivityManager");
            param1Int2 = stackInfo.readInt();
            i17 = stackInfo.readInt();
            bool43 = bool37;
            if (stackInfo.readInt() != 0)
              bool43 = true; 
            moveTaskToStack(param1Int2, i17, bool43);
            str25.writeNoException();
            return true;
          case 148:
            stackInfo.enforceInterface("android.app.IActivityManager");
            list4 = getAllStackInfos();
            str25.writeNoException();
            str25.writeTypedList(list4);
            return true;
          case 147:
            list4.enforceInterface("android.app.IActivityManager");
            iBinder28 = list4.readStrongBinder();
            bool43 = bool38;
            if (list4.readInt() != 0)
              bool43 = true; 
            hang(iBinder28, bool43);
            str25.writeNoException();
            return true;
          case 146:
            list4.enforceInterface("android.app.IActivityManager");
            bool43 = bool39;
            if (list4.readInt() != 0)
              bool43 = true; 
            setUserIsMonkey(bool43);
            str25.writeNoException();
            return true;
          case 145:
            list4.enforceInterface("android.app.IActivityManager");
            param1Int2 = list4.readInt();
            i17 = list4.readInt();
            str16 = list4.readString();
            killUid(param1Int2, i17, str16);
            str25.writeNoException();
            return true;
          case 144:
            str16.enforceInterface("android.app.IActivityManager");
            iBinder9 = str16.readStrongBinder();
            str15 = getLaunchedFromPackage(iBinder9);
            str25.writeNoException();
            str25.writeString(str15);
            return true;
          case 143:
            str15.enforceInterface("android.app.IActivityManager");
            iIntentSender6 = IIntentSender.Stub.asInterface(str15.readStrongBinder());
            intent2 = getIntentForIntentSender(iIntentSender6);
            str25.writeNoException();
            if (intent2 != null) {
              str25.writeInt(1);
              intent2.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 142:
            intent2.enforceInterface("android.app.IActivityManager");
            list3 = getBugreportWhitelistedPackages();
            str25.writeNoException();
            str25.writeStringList(list3);
            return true;
          case 141:
            list3.enforceInterface("android.app.IActivityManager");
            bool21 = launchBugReportHandlerApp();
            str25.writeNoException();
            str25.writeInt(bool21);
            return true;
          case 140:
            list3.enforceInterface("android.app.IActivityManager");
            requestRemoteBugReport();
            str25.writeNoException();
            return true;
          case 139:
            list3.enforceInterface("android.app.IActivityManager");
            requestFullBugReport();
            str25.writeNoException();
            return true;
          case 138:
            list3.enforceInterface("android.app.IActivityManager");
            requestInteractiveBugReport();
            str25.writeNoException();
            return true;
          case 137:
            list3.enforceInterface("android.app.IActivityManager");
            str43 = list3.readString();
            str14 = list3.readString();
            requestInteractiveBugReportWithDescription(str43, str14);
            str25.writeNoException();
            return true;
          case 136:
            str14.enforceInterface("android.app.IActivityManager");
            str43 = str14.readString();
            str14 = str14.readString();
            requestWifiBugReport(str43, str14);
            str25.writeNoException();
            return true;
          case 135:
            str14.enforceInterface("android.app.IActivityManager");
            str43 = str14.readString();
            str14 = str14.readString();
            requestTelephonyBugReport(str43, str14);
            str25.writeNoException();
            return true;
          case 134:
            str14.enforceInterface("android.app.IActivityManager");
            str43 = str14.readString();
            str34 = str14.readString();
            i16 = str14.readInt();
            requestBugReportWithDescription(str43, str34, i16);
            str25.writeNoException();
            return true;
          case 133:
            str14.enforceInterface("android.app.IActivityManager");
            i16 = str14.readInt();
            requestBugReport(i16);
            str25.writeNoException();
            return true;
          case 132:
            str14.enforceInterface("android.app.IActivityManager");
            requestSystemServerHeapDump();
            str25.writeNoException();
            return true;
          case 131:
            str14.enforceInterface("android.app.IActivityManager");
            arrayOfInt3 = getRunningUserIds();
            str25.writeNoException();
            str25.writeIntArray(arrayOfInt3);
            return true;
          case 130:
            arrayOfInt3.enforceInterface("android.app.IActivityManager");
            iUserSwitchObserver1 = IUserSwitchObserver.Stub.asInterface(arrayOfInt3.readStrongBinder());
            unregisterUserSwitchObserver(iUserSwitchObserver1);
            str25.writeNoException();
            return true;
          case 129:
            iUserSwitchObserver1.enforceInterface("android.app.IActivityManager");
            iUserSwitchObserver2 = IUserSwitchObserver.Stub.asInterface(iUserSwitchObserver1.readStrongBinder());
            str13 = iUserSwitchObserver1.readString();
            registerUserSwitchObserver(iUserSwitchObserver2, str13);
            str25.writeNoException();
            return true;
          case 128:
            str13.enforceInterface("android.app.IActivityManager");
            i16 = str13.readInt();
            bool43 = bool40;
            if (str13.readInt() != 0)
              bool43 = true; 
            iStopUserCallback = IStopUserCallback.Stub.asInterface(str13.readStrongBinder());
            i16 = stopUserWithDelayedLocking(i16, bool43, iStopUserCallback);
            str25.writeNoException();
            str25.writeInt(i16);
            return true;
          case 127:
            iStopUserCallback.enforceInterface("android.app.IActivityManager");
            i16 = iStopUserCallback.readInt();
            bool43 = bool41;
            if (iStopUserCallback.readInt() != 0)
              bool43 = true; 
            iStopUserCallback = IStopUserCallback.Stub.asInterface(iStopUserCallback.readStrongBinder());
            i16 = stopUser(i16, bool43, iStopUserCallback);
            str25.writeNoException();
            str25.writeInt(i16);
            return true;
          case 126:
            iStopUserCallback.enforceInterface("android.app.IActivityManager");
            iApplicationThread15 = IApplicationThread.Stub.asInterface(iStopUserCallback.readStrongBinder());
            str61 = iStopUserCallback.readString();
            str52 = iStopUserCallback.readString();
            if (iStopUserCallback.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              iUserSwitchObserver2 = null;
            } 
            str63 = iStopUserCallback.readString();
            iBinder32 = iStopUserCallback.readStrongBinder();
            str66 = iStopUserCallback.readString();
            param1Int2 = iStopUserCallback.readInt();
            i16 = iStopUserCallback.readInt();
            if (iStopUserCallback.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              str34 = null;
            } 
            if (iStopUserCallback.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              iBinder15 = null;
            } 
            i30 = iStopUserCallback.readInt();
            i16 = startActivityAsUserWithFeature(iApplicationThread15, str61, str52, (Intent)iUserSwitchObserver2, str63, iBinder32, str66, param1Int2, i16, (ProfilerInfo)str34, (Bundle)iBinder15, i30);
            str25.writeNoException();
            str25.writeInt(i16);
            return true;
          case 125:
            iStopUserCallback.enforceInterface("android.app.IActivityManager");
            iApplicationThread15 = IApplicationThread.Stub.asInterface(iStopUserCallback.readStrongBinder());
            str52 = iStopUserCallback.readString();
            if (iStopUserCallback.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              iUserSwitchObserver2 = null;
            } 
            str63 = iStopUserCallback.readString();
            iBinder33 = iStopUserCallback.readStrongBinder();
            str61 = iStopUserCallback.readString();
            param1Int2 = iStopUserCallback.readInt();
            i16 = iStopUserCallback.readInt();
            if (iStopUserCallback.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              str34 = null;
            } 
            if (iStopUserCallback.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iStopUserCallback);
            } else {
              iBinder15 = null;
            } 
            i30 = iStopUserCallback.readInt();
            i16 = startActivityAsUser(iApplicationThread15, str52, (Intent)iUserSwitchObserver2, str63, iBinder33, str61, param1Int2, i16, (ProfilerInfo)str34, (Bundle)iBinder15, i30);
            str25.writeNoException();
            str25.writeInt(i16);
            return true;
          case 124:
            iStopUserCallback.enforceInterface("android.app.IActivityManager");
            iIntentSender5 = IIntentSender.Stub.asInterface(iStopUserCallback.readStrongBinder());
            bool20 = isIntentSenderABroadcast(iIntentSender5);
            str25.writeNoException();
            str25.writeInt(bool20);
            return true;
          case 123:
            iIntentSender5.enforceInterface("android.app.IActivityManager");
            iIntentSender5 = IIntentSender.Stub.asInterface(iIntentSender5.readStrongBinder());
            bool20 = isIntentSenderAForegroundService(iIntentSender5);
            str25.writeNoException();
            str25.writeInt(bool20);
            return true;
          case 122:
            iIntentSender5.enforceInterface("android.app.IActivityManager");
            iIntentSender5 = IIntentSender.Stub.asInterface(iIntentSender5.readStrongBinder());
            bool20 = isIntentSenderAnActivity(iIntentSender5);
            str25.writeNoException();
            str25.writeInt(bool20);
            return true;
          case 121:
            iIntentSender5.enforceInterface("android.app.IActivityManager");
            iBinder8 = iIntentSender5.readStrongBinder();
            unstableProviderDied(iBinder8);
            str25.writeNoException();
            return true;
          case 120:
            iBinder8.enforceInterface("android.app.IActivityManager");
            iBinder8 = iBinder8.readStrongBinder();
            i15 = getLaunchedFromUid(iBinder8);
            str25.writeNoException();
            str25.writeInt(i15);
            return true;
          case 119:
            iBinder8.enforceInterface("android.app.IActivityManager");
            userInfo = getCurrentUser();
            str25.writeNoException();
            if (userInfo != null) {
              str25.writeInt(1);
              userInfo.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 118:
            userInfo.enforceInterface("android.app.IActivityManager");
            str12 = userInfo.readString();
            bool19 = killProcessesBelowForeground(str12);
            str25.writeNoException();
            str25.writeInt(bool19);
            return true;
          case 117:
            str12.enforceInterface("android.app.IActivityManager");
            runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
            getMyMemoryState(runningAppProcessInfo);
            str25.writeNoException();
            str25.writeInt(1);
            runningAppProcessInfo.writeToParcel((Parcel)str25, 1);
            return true;
          case 116:
            runningAppProcessInfo.enforceInterface("android.app.IActivityManager");
            str34 = runningAppProcessInfo.readString();
            iBinder27 = runningAppProcessInfo.readStrongBinder();
            i14 = runningAppProcessInfo.readInt();
            removeContentProviderExternalAsUser(str34, iBinder27, i14);
            str25.writeNoException();
            return true;
          case 115:
            runningAppProcessInfo.enforceInterface("android.app.IActivityManager");
            str42 = runningAppProcessInfo.readString();
            iBinder7 = runningAppProcessInfo.readStrongBinder();
            removeContentProviderExternal(str42, iBinder7);
            str25.writeNoException();
            return true;
          case 114:
            iBinder7.enforceInterface("android.app.IActivityManager");
            str34 = iBinder7.readString();
            i14 = iBinder7.readInt();
            iBinder26 = iBinder7.readStrongBinder();
            str11 = iBinder7.readString();
            contentProviderHolder2 = getContentProviderExternal(str34, i14, iBinder26, str11);
            str25.writeNoException();
            if (contentProviderHolder2 != null) {
              str25.writeInt(1);
              contentProviderHolder2.writeToParcel((Parcel)str25, 1);
            } else {
              str25.writeInt(0);
            } 
            return true;
          case 113:
            contentProviderHolder2.enforceInterface("android.app.IActivityManager");
            killAllBackgroundProcesses();
            str25.writeNoException();
            return true;
          case 112:
            contentProviderHolder2.enforceInterface("android.app.IActivityManager");
            if (contentProviderHolder2.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)contentProviderHolder2);
            } else {
              iBinder26 = null;
            } 
            if (contentProviderHolder2.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            showBootMessage((CharSequence)iBinder26, bool43);
            str25.writeNoException();
            return true;
          case 111:
            contentProviderHolder2.enforceInterface("android.app.IActivityManager");
            arrayOfInt2 = contentProviderHolder2.createIntArray();
            arrayOfLong = getProcessPss(arrayOfInt2);
            str25.writeNoException();
            str25.writeLongArray(arrayOfLong);
            return true;
          case 110:
            arrayOfLong.enforceInterface("android.app.IActivityManager");
            if (arrayOfLong.readInt() != 0) {
              Configuration configuration1 = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)arrayOfLong);
            } else {
              arrayOfLong = null;
            } 
            updatePersistentConfiguration((Configuration)arrayOfLong);
            str25.writeNoException();
            return true;
          case 109:
            arrayOfLong.enforceInterface("android.app.IActivityManager");
            iIntentSender4 = IIntentSender.Stub.asInterface(arrayOfLong.readStrongBinder());
            bool18 = isIntentSenderTargetedToPackage(iIntentSender4);
            str25.writeNoException();
            str25.writeInt(bool18);
            return true;
          case 108:
            iIntentSender4.enforceInterface("android.app.IActivityManager");
            iProcessObserver = IProcessObserver.Stub.asInterface(iIntentSender4.readStrongBinder());
            unregisterProcessObserver(iProcessObserver);
            str25.writeNoException();
            return true;
          case 107:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            iProcessObserver = IProcessObserver.Stub.asInterface(iProcessObserver.readStrongBinder());
            registerProcessObserver(iProcessObserver);
            str25.writeNoException();
            return true;
          case 106:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            i13 = iProcessObserver.readInt();
            bool17 = removeTask(i13);
            str25.writeNoException();
            str25.writeInt(bool17);
            return true;
          case 105:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            i12 = iProcessObserver.readInt();
            bool16 = switchUser(i12);
            str25.writeNoException();
            str25.writeInt(bool16);
            return true;
          case 104:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            str41 = iProcessObserver.readString();
            i11 = iProcessObserver.readInt();
            setPackageScreenCompatMode(str41, i11);
            str25.writeNoException();
            return true;
          case 103:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            param1Int2 = iProcessObserver.readInt();
            i11 = iProcessObserver.readInt();
            bool15 = isUserRunning(param1Int2, i11);
            str25.writeNoException();
            str25.writeInt(bool15);
            return true;
          case 102:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            str29 = iProcessObserver.readString();
            i10 = iProcessObserver.readInt();
            if (iProcessObserver.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (iProcessObserver.readInt() != 0) {
              bool42 = true;
            } else {
              bool42 = false;
            } 
            if (iProcessObserver.readInt() != 0) {
              bool36 = true;
            } else {
              bool36 = false;
            } 
            str34 = iProcessObserver.readString();
            if (iProcessObserver.readInt() != 0) {
              ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel((Parcel)iProcessObserver);
            } else {
              str41 = null;
            } 
            if (iProcessObserver.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)iProcessObserver);
            } else {
              iProcessObserver = null;
            } 
            bool14 = dumpHeap(str29, i10, bool43, bool42, bool36, str34, (ParcelFileDescriptor)str41, (RemoteCallback)iProcessObserver);
            str25.writeNoException();
            str25.writeInt(bool14);
            return true;
          case 101:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            if (iProcessObserver.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)iProcessObserver);
            } else {
              str25 = null;
            } 
            i9 = iProcessObserver.readInt();
            if (iProcessObserver.readInt() != 0) {
              RemoteCallback remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)iProcessObserver);
            } else {
              iProcessObserver = null;
            } 
            getProviderMimeTypeAsync((Uri)str25, i9, (RemoteCallback)iProcessObserver);
            return true;
          case 100:
            iProcessObserver.enforceInterface("android.app.IActivityManager");
            if (iProcessObserver.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)iProcessObserver);
            } else {
              str41 = null;
            } 
            i9 = iProcessObserver.readInt();
            str10 = getProviderMimeType((Uri)str41, i9);
            str25.writeNoException();
            str25.writeString(str10);
            return true;
          case 99:
            str10.enforceInterface("android.app.IActivityManager");
            i9 = str10.readInt();
            param1Int2 = str10.readInt();
            str41 = str10.readString();
            i30 = str10.readInt();
            str34 = str10.readString();
            if (str10.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            crashApplication(i9, param1Int2, str41, i30, str34, bool43);
            str25.writeNoException();
            return true;
          case 98:
            str10.enforceInterface("android.app.IActivityManager");
            bool13 = isTopActivityImmersive();
            str25.writeNoException();
            str25.writeInt(bool13);
            return true;
          case 97:
            str10.enforceInterface("android.app.IActivityManager");
            iBinder25 = str10.readStrongBinder();
            i8 = str10.readInt();
            if (str10.readInt() != 0) {
              StrictMode.ViolationInfo violationInfo = (StrictMode.ViolationInfo)StrictMode.ViolationInfo.CREATOR.createFromParcel((Parcel)str10);
            } else {
              str10 = null;
            } 
            handleApplicationStrictModeViolation(iBinder25, i8, (StrictMode.ViolationInfo)str10);
            str25.writeNoException();
            return true;
          case 96:
            str10.enforceInterface("android.app.IActivityManager");
            finishHeavyWeightApp();
            str25.writeNoException();
            return true;
          case 95:
            str10.enforceInterface("android.app.IActivityManager");
            list2 = getRunningExternalApplications();
            str25.writeNoException();
            str25.writeTypedList(list2);
            return true;
          case 94:
            list2.enforceInterface("android.app.IActivityManager");
            bool12 = isUserAMonkey();
            str25.writeNoException();
            str25.writeInt(bool12);
            return true;
          case 93:
            list2.enforceInterface("android.app.IActivityManager");
            str40 = list2.readString();
            i7 = list2.readInt();
            killBackgroundProcesses(str40, i7);
            str25.writeNoException();
            return true;
          case 92:
            list2.enforceInterface("android.app.IActivityManager");
            iBinder14 = list2.readStrongBinder();
            str34 = list2.readString();
            if (list2.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (list2.readInt() != 0) {
              ApplicationErrorReport.ParcelableCrashInfo parcelableCrashInfo = (ApplicationErrorReport.ParcelableCrashInfo)ApplicationErrorReport.ParcelableCrashInfo.CREATOR.createFromParcel((Parcel)list2);
            } else {
              str40 = null;
            } 
            i7 = list2.readInt();
            bool11 = handleApplicationWtf(iBinder14, str34, bool43, (ApplicationErrorReport.ParcelableCrashInfo)str40, i7);
            str25.writeNoException();
            str25.writeInt(bool11);
            return true;
          case 91:
            list2.enforceInterface("android.app.IActivityManager");
            str40 = list2.readString();
            i6 = list2.readInt();
            killApplicationProcess(str40, i6);
            str25.writeNoException();
            return true;
          case 90:
            list2.enforceInterface("android.app.IActivityManager");
            arrayOfInt1 = list2.createIntArray();
            arrayOfMemoryInfo = getProcessMemoryInfo(arrayOfInt1);
            str25.writeNoException();
            str25.writeTypedArray((Parcelable[])arrayOfMemoryInfo, 1);
            return true;
          case 89:
            arrayOfMemoryInfo.enforceInterface("android.app.IActivityManager");
            str9 = arrayOfMemoryInfo.readString();
            closeSystemDialogs(str9);
            str25.writeNoException();
            return true;
          case 88:
            str9.enforceInterface("android.app.IActivityManager");
            str40 = str9.readString();
            param1Int2 = str9.readInt();
            i6 = str9.readInt();
            str9 = str9.readString();
            killApplication(str40, param1Int2, i6, str9);
            str25.writeNoException();
            return true;
          case 87:
            str9.enforceInterface("android.app.IActivityManager");
            str9 = str9.readString();
            addPackageDependency(str9);
            str25.writeNoException();
            return true;
          case 86:
            str9.enforceInterface("android.app.IActivityManager");
            i30 = str9.readInt();
            i6 = str9.readInt();
            param1Int2 = str9.readInt();
            if (str9.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (str9.readInt() != 0) {
              bool42 = true;
            } else {
              bool42 = false;
            } 
            str40 = str9.readString();
            str9 = str9.readString();
            i6 = handleIncomingUser(i30, i6, param1Int2, bool43, bool42, str40, str9);
            str25.writeNoException();
            str25.writeInt(i6);
            return true;
          case 85:
            str9.enforceInterface("android.app.IActivityManager");
            iIntentSender3 = IIntentSender.Stub.asInterface(str9.readStrongBinder());
            i6 = getUidForIntentSender(iIntentSender3);
            str25.writeNoException();
            str25.writeInt(i6);
            return true;
          case 84:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            if (iIntentSender3.readInt() != 0) {
              ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel((Parcel)iIntentSender3);
            } else {
              iIntentSender3 = null;
            } 
            unbindBackupAgent((ApplicationInfo)iIntentSender3);
            str25.writeNoException();
            return true;
          case 83:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            str40 = iIntentSender3.readString();
            iBinder20 = iIntentSender3.readStrongBinder();
            i6 = iIntentSender3.readInt();
            backupAgentCreated(str40, iBinder20, i6);
            str25.writeNoException();
            return true;
          case 82:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            str40 = iIntentSender3.readString();
            param1Int2 = iIntentSender3.readInt();
            i6 = iIntentSender3.readInt();
            bool10 = bindBackupAgent(str40, param1Int2, i6);
            str25.writeNoException();
            str25.writeInt(bool10);
            return true;
          case 81:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            resumeAppSwitches();
            str25.writeNoException();
            return true;
          case 80:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            stopAppSwitches();
            str25.writeNoException();
            return true;
          case 79:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            i5 = iIntentSender3.readInt();
            bool9 = shutdown(i5);
            str25.writeNoException();
            str25.writeInt(bool9);
            return true;
          case 78:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            str33 = iIntentSender3.readString();
            param1Int2 = iIntentSender3.readInt();
            if (iIntentSender3.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (iIntentSender3.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)iIntentSender3);
            } else {
              str40 = null;
            } 
            i4 = iIntentSender3.readInt();
            bool8 = profileControl(str33, param1Int2, bool43, (ProfilerInfo)str40, i4);
            str25.writeNoException();
            str25.writeInt(bool8);
            return true;
          case 77:
            iIntentSender3.enforceInterface("android.app.IActivityManager");
            if (iIntentSender3.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iIntentSender3);
            } else {
              str40 = null;
            } 
            str33 = iIntentSender3.readString();
            str8 = iIntentSender3.readString();
            iBinder6 = peekService((Intent)str40, str33, str8);
            str25.writeNoException();
            str25.writeStrongBinder(iBinder6);
            return true;
          case 76:
            iBinder6.enforceInterface("android.app.IActivityManager");
            list1 = getRunningAppProcesses();
            str25.writeNoException();
            str25.writeTypedList(list1);
            return true;
          case 75:
            list1.enforceInterface("android.app.IActivityManager");
            param1Int2 = list1.readInt();
            i3 = list1.readInt();
            list1 = (List)getServices(param1Int2, i3);
            str25.writeNoException();
            str25.writeTypedList(list1);
            return true;
          case 74:
            list1.enforceInterface("android.app.IActivityManager");
            arrayOfInt4 = list1.createIntArray();
            str40 = list1.readString();
            if (list1.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            bool7 = killPids(arrayOfInt4, str40, bool43);
            str25.writeNoException();
            str25.writeInt(bool7);
            return true;
          case 73:
            list1.enforceInterface("android.app.IActivityManager");
            str40 = list1.readString();
            i2 = list1.readInt();
            forceStopPackage(str40, i2);
            str25.writeNoException();
            return true;
          case 72:
            list1.enforceInterface("android.app.IActivityManager");
            str32 = list1.readString();
            if (list1.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            iPackageDataObserver = IPackageDataObserver.Stub.asInterface(list1.readStrongBinder());
            i2 = list1.readInt();
            bool6 = clearApplicationUserData(str32, bool43, iPackageDataObserver, i2);
            str25.writeNoException();
            str25.writeInt(bool6);
            return true;
          case 71:
            list1.enforceInterface("android.app.IActivityManager");
            list1 = (List)getProcessesInErrorState();
            str25.writeNoException();
            str25.writeTypedList(list1);
            return true;
          case 70:
            list1.enforceInterface("android.app.IActivityManager");
            memoryInfo = new ActivityManager.MemoryInfo();
            getMemoryInfo(memoryInfo);
            str25.writeNoException();
            str25.writeInt(1);
            memoryInfo.writeToParcel((Parcel)str25, 1);
            return true;
          case 69:
            memoryInfo.enforceInterface("android.app.IActivityManager");
            iBinder24 = memoryInfo.readStrongBinder();
            if (memoryInfo.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            bool6 = moveActivityTaskToBack(iBinder24, bool43);
            str25.writeNoException();
            str25.writeInt(bool6);
            return true;
          case 68:
            memoryInfo.enforceInterface("android.app.IActivityManager");
            if (memoryInfo.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)memoryInfo);
            } else {
              iBinder24 = null;
            } 
            iBinder5 = memoryInfo.readStrongBinder();
            i1 = getForegroundServiceType((ComponentName)iBinder24, iBinder5);
            str25.writeNoException();
            str25.writeInt(i1);
            return true;
          case 67:
            iBinder5.enforceInterface("android.app.IActivityManager");
            if (iBinder5.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iBinder5);
            } else {
              iBinder24 = null;
            } 
            iBinder14 = iBinder5.readStrongBinder();
            param1Int2 = iBinder5.readInt();
            if (iBinder5.readInt() != 0) {
              Notification notification = (Notification)Notification.CREATOR.createFromParcel((Parcel)iBinder5);
            } else {
              str32 = null;
            } 
            i1 = iBinder5.readInt();
            i30 = iBinder5.readInt();
            setServiceForeground((ComponentName)iBinder24, iBinder14, param1Int2, (Notification)str32, i1, i30);
            str25.writeNoException();
            return true;
          case 66:
            iBinder5.enforceInterface("android.app.IActivityManager");
            iBinder24 = iBinder5.readStrongBinder();
            i1 = iBinder5.readInt();
            if (iBinder5.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            str7 = iBinder5.readString();
            setProcessImportant(iBinder24, i1, bool43, str7);
            str25.writeNoException();
            return true;
          case 65:
            str7.enforceInterface("android.app.IActivityManager");
            iBinder19 = str7.readStrongBinder();
            if (str7.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str7);
            } else {
              iBinder24 = null;
            } 
            if (str7.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            unbindFinished(iBinder19, (Intent)iBinder24, bool43);
            str25.writeNoException();
            return true;
          case 64:
            str7.enforceInterface("android.app.IActivityManager");
            iBinder24 = str7.readStrongBinder();
            i1 = str7.readInt();
            setRequestedOrientation(iBinder24, i1);
            str25.writeNoException();
            return true;
          case 63:
            str7.enforceInterface("android.app.IActivityManager");
            iBinder = str7.readStrongBinder();
            if (str7.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            removeContentProvider(iBinder, bool43);
            return true;
          case 62:
            str7.enforceInterface("android.app.IActivityManager");
            iIntentSender7 = IIntentSender.Stub.asInterface(str7.readStrongBinder());
            if (str7.readInt() != 0) {
              WorkSource workSource = (WorkSource)WorkSource.CREATOR.createFromParcel((Parcel)str7);
            } else {
              iBinder24 = null;
            } 
            i1 = str7.readInt();
            str28 = str7.readString();
            str7 = str7.readString();
            noteWakeupAlarm(iIntentSender7, (WorkSource)iBinder24, i1, str28, str7);
            iBinder.writeNoException();
            return true;
          case 61:
            str7.enforceInterface("android.app.IActivityManager");
            enterSafeMode();
            iBinder.writeNoException();
            return true;
          case 60:
            str7.enforceInterface("android.app.IActivityManager");
            iIntentSender10 = IIntentSender.Stub.asInterface(str7.readStrongBinder());
            iResultReceiver = IResultReceiver.Stub.asInterface(str7.readStrongBinder());
            unregisterIntentSenderCancelListener(iIntentSender10, iResultReceiver);
            iBinder.writeNoException();
            return true;
          case 59:
            iResultReceiver.enforceInterface("android.app.IActivityManager");
            iIntentSender10 = IIntentSender.Stub.asInterface(iResultReceiver.readStrongBinder());
            iResultReceiver = IResultReceiver.Stub.asInterface(iResultReceiver.readStrongBinder());
            registerIntentSenderCancelListener(iIntentSender10, iResultReceiver);
            iBinder.writeNoException();
            return true;
          case 58:
            iResultReceiver.enforceInterface("android.app.IActivityManager");
            iIntentSender2 = IIntentSender.Stub.asInterface(iResultReceiver.readStrongBinder());
            str6 = getPackageForIntentSender(iIntentSender2);
            iBinder.writeNoException();
            iBinder.writeString(str6);
            return true;
          case 57:
            str6.enforceInterface("android.app.IActivityManager");
            iIntentSender1 = IIntentSender.Stub.asInterface(str6.readStrongBinder());
            cancelIntentSender(iIntentSender1);
            iBinder.writeNoException();
            return true;
          case 56:
            iIntentSender1.enforceInterface("android.app.IActivityManager");
            i30 = iIntentSender1.readInt();
            str28 = iIntentSender1.readString();
            str63 = iIntentSender1.readString();
            iBinder31 = iIntentSender1.readStrongBinder();
            str65 = iIntentSender1.readString();
            i1 = iIntentSender1.readInt();
            arrayOfIntent2 = (Intent[])iIntentSender1.createTypedArray(Intent.CREATOR);
            arrayOfString4 = iIntentSender1.createStringArray();
            i31 = iIntentSender1.readInt();
            if (iIntentSender1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iIntentSender1);
            } else {
              iIntentSender10 = null;
            } 
            param1Int2 = iIntentSender1.readInt();
            iIntentSender10 = getIntentSenderWithFeature(i30, str28, str63, iBinder31, str65, i1, arrayOfIntent2, arrayOfString4, i31, (Bundle)iIntentSender10, param1Int2);
            iBinder.writeNoException();
            iIntentSender1 = iIntentSender7;
            if (iIntentSender10 != null)
              iBinder4 = iIntentSender10.asBinder(); 
            iBinder.writeStrongBinder(iBinder4);
            return true;
          case 55:
            iBinder4.enforceInterface("android.app.IActivityManager");
            i1 = iBinder4.readInt();
            str63 = iBinder4.readString();
            iBinder31 = iBinder4.readStrongBinder();
            str57 = iBinder4.readString();
            i31 = iBinder4.readInt();
            arrayOfIntent1 = (Intent[])iBinder4.createTypedArray(Intent.CREATOR);
            arrayOfString3 = iBinder4.createStringArray();
            param1Int2 = iBinder4.readInt();
            if (iBinder4.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iBinder4);
            } else {
              iIntentSender10 = null;
            } 
            i30 = iBinder4.readInt();
            iIntentSender10 = getIntentSender(i1, str63, iBinder31, str57, i31, arrayOfIntent1, arrayOfString3, param1Int2, (Bundle)iIntentSender10, i30);
            iBinder.writeNoException();
            str5 = str28;
            if (iIntentSender10 != null)
              iBinder3 = iIntentSender10.asBinder(); 
            iBinder.writeStrongBinder(iBinder3);
            return true;
          case 54:
            iBinder3.enforceInterface("android.app.IActivityManager");
            iBinder = iBinder3.readStrongBinder();
            i30 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            i1 = iBinder3.readInt();
            serviceDoneExecuting(iBinder, i30, param1Int2, i1);
            return true;
          case 53:
            iBinder3.enforceInterface("android.app.IActivityManager");
            i30 = iBinder3.readInt();
            param1Int2 = iBinder3.readInt();
            i1 = iBinder3.readInt();
            parceledListSlice = getRecentTasks(i30, param1Int2, i1);
            iBinder.writeNoException();
            if (parceledListSlice != null) {
              iBinder.writeInt(1);
              parceledListSlice.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 52:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            i1 = parceledListSlice.readInt();
            signalPersistentProcesses(i1);
            iBinder.writeNoException();
            return true;
          case 51:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            iApplicationThread9 = IApplicationThread.Stub.asInterface(parceledListSlice.readStrongBinder());
            if (parceledListSlice.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            showWaitingForDebugger(iApplicationThread9, bool43);
            iBinder.writeNoException();
            return true;
          case 50:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            iActivityController = IActivityController.Stub.asInterface(parceledListSlice.readStrongBinder());
            if (parceledListSlice.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            setActivityController(iActivityController, bool43);
            iBinder.writeNoException();
            return true;
          case 49:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            iApplicationThread2 = IApplicationThread.Stub.asInterface(parceledListSlice.readStrongBinder());
            str31 = parceledListSlice.readString();
            if (parceledListSlice.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              iActivityController = null;
            } 
            i1 = parceledListSlice.readInt();
            param1Int2 = parceledListSlice.readInt();
            revokeUriPermission(iApplicationThread2, str31, (Uri)iActivityController, i1, param1Int2);
            iBinder.writeNoException();
            return true;
          case 48:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            iApplicationThread2 = IApplicationThread.Stub.asInterface(parceledListSlice.readStrongBinder());
            str31 = parceledListSlice.readString();
            if (parceledListSlice.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              iActivityController = null;
            } 
            param1Int2 = parceledListSlice.readInt();
            i1 = parceledListSlice.readInt();
            grantUriPermission(iApplicationThread2, str31, (Uri)iActivityController, param1Int2, i1);
            iBinder.writeNoException();
            return true;
          case 47:
            parceledListSlice.enforceInterface("android.app.IActivityManager");
            if (parceledListSlice.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              iActivityController = null;
            } 
            i31 = parceledListSlice.readInt();
            param1Int2 = parceledListSlice.readInt();
            i1 = parceledListSlice.readInt();
            i30 = parceledListSlice.readInt();
            iBinder2 = parceledListSlice.readStrongBinder();
            i1 = checkUriPermission((Uri)iActivityController, i31, param1Int2, i1, i30, iBinder2);
            iBinder.writeNoException();
            iBinder.writeInt(i1);
            return true;
          case 46:
            iBinder2.enforceInterface("android.app.IActivityManager");
            str39 = iBinder2.readString();
            i1 = iBinder2.readInt();
            param1Int2 = iBinder2.readInt();
            i1 = checkPermission(str39, i1, param1Int2);
            iBinder.writeNoException();
            iBinder.writeInt(i1);
            return true;
          case 45:
            iBinder2.enforceInterface("android.app.IActivityManager");
            i1 = getProcessLimit();
            iBinder.writeNoException();
            iBinder.writeInt(i1);
            return true;
          case 44:
            iBinder2.enforceInterface("android.app.IActivityManager");
            i1 = iBinder2.readInt();
            setProcessLimit(i1);
            iBinder.writeNoException();
            return true;
          case 43:
            iBinder2.enforceInterface("android.app.IActivityManager");
            if (iBinder2.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iBinder2);
            } else {
              str39 = null;
            } 
            iBinder18 = iBinder2.readStrongBinder();
            i1 = iBinder2.readInt();
            bool5 = stopServiceToken((ComponentName)str39, iBinder18, i1);
            iBinder.writeNoException();
            iBinder.writeInt(bool5);
            return true;
          case 42:
            iBinder2.enforceInterface("android.app.IActivityManager");
            str39 = iBinder2.readString();
            str4 = iBinder2.readString();
            bool5 = updateMccMncConfiguration(str39, str4);
            iBinder.writeNoException();
            iBinder.writeInt(bool5);
            return true;
          case 41:
            str4.enforceInterface("android.app.IActivityManager");
            if (str4.readInt() != 0) {
              Configuration configuration1 = (Configuration)Configuration.CREATOR.createFromParcel((Parcel)str4);
            } else {
              str4 = null;
            } 
            bool5 = updateConfiguration((Configuration)str4);
            iBinder.writeNoException();
            iBinder.writeInt(bool5);
            return true;
          case 40:
            str4.enforceInterface("android.app.IActivityManager");
            configuration = getConfiguration();
            iBinder.writeNoException();
            if (configuration != null) {
              iBinder.writeInt(1);
              configuration.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 39:
            configuration.enforceInterface("android.app.IActivityManager");
            iApplicationThread8 = IApplicationThread.Stub.asInterface(configuration.readStrongBinder());
            n = configuration.readInt();
            if (configuration.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)configuration);
            } else {
              configuration = null;
            } 
            finishInstrumentation(iApplicationThread8, n, (Bundle)configuration);
            iBinder.writeNoException();
            return true;
          case 38:
            configuration.enforceInterface("android.app.IActivityManager");
            iApplicationThread8 = IApplicationThread.Stub.asInterface(configuration.readStrongBinder());
            if (configuration.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)configuration);
            } else {
              configuration = null;
            } 
            addInstrumentationResults(iApplicationThread8, (Bundle)configuration);
            iBinder.writeNoException();
            return true;
          case 37:
            configuration.enforceInterface("android.app.IActivityManager");
            if (configuration.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)configuration);
            } else {
              iApplicationThread8 = null;
            } 
            str27 = configuration.readString();
            param1Int2 = configuration.readInt();
            if (configuration.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)configuration);
            } else {
              iBinder18 = null;
            } 
            iInstrumentationWatcher = IInstrumentationWatcher.Stub.asInterface(configuration.readStrongBinder());
            iUiAutomationConnection = IUiAutomationConnection.Stub.asInterface(configuration.readStrongBinder());
            n = configuration.readInt();
            str3 = configuration.readString();
            bool4 = startInstrumentation((ComponentName)iApplicationThread8, str27, param1Int2, (Bundle)iBinder18, iInstrumentationWatcher, iUiAutomationConnection, n, str3);
            iBinder.writeNoException();
            iBinder.writeInt(bool4);
            return true;
          case 36:
            str3.enforceInterface("android.app.IActivityManager");
            if (str3.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            setAlwaysFinish(bool43);
            iBinder.writeNoException();
            return true;
          case 35:
            str3.enforceInterface("android.app.IActivityManager");
            str38 = str3.readString();
            str3 = str3.readString();
            setAgentApp(str38, str3);
            iBinder.writeNoException();
            return true;
          case 34:
            str3.enforceInterface("android.app.IActivityManager");
            str38 = str3.readString();
            if (str3.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (str3.readInt() != 0) {
              bool42 = true;
            } else {
              bool42 = false;
            } 
            setDebugApp(str38, bool43, bool42);
            iBinder.writeNoException();
            return true;
          case 33:
            str3.enforceInterface("android.app.IActivityManager");
            iBinder18 = str3.readStrongBinder();
            if (str3.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str38 = null;
            } 
            iBinder1 = str3.readStrongBinder();
            publishService(iBinder18, (Intent)str38, iBinder1);
            iBinder.writeNoException();
            return true;
          case 32:
            iBinder1.enforceInterface("android.app.IActivityManager");
            iServiceConnection1 = IServiceConnection.Stub.asInterface(iBinder1.readStrongBinder());
            bool4 = unbindService(iServiceConnection1);
            iBinder.writeNoException();
            iBinder.writeInt(bool4);
            return true;
          case 31:
            iServiceConnection1.enforceInterface("android.app.IActivityManager");
            iServiceConnection2 = IServiceConnection.Stub.asInterface(iServiceConnection1.readStrongBinder());
            param1Int2 = iServiceConnection1.readInt();
            m = iServiceConnection1.readInt();
            updateServiceGroup(iServiceConnection2, param1Int2, m);
            iBinder.writeNoException();
            return true;
          case 30:
            iServiceConnection1.enforceInterface("android.app.IActivityManager");
            iApplicationThread1 = IApplicationThread.Stub.asInterface(iServiceConnection1.readStrongBinder());
            iBinder18 = iServiceConnection1.readStrongBinder();
            if (iServiceConnection1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iServiceConnection1);
            } else {
              iServiceConnection2 = null;
            } 
            str56 = iServiceConnection1.readString();
            iServiceConnection3 = IServiceConnection.Stub.asInterface(iServiceConnection1.readStrongBinder());
            param1Int2 = iServiceConnection1.readInt();
            str63 = iServiceConnection1.readString();
            str60 = iServiceConnection1.readString();
            m = iServiceConnection1.readInt();
            m = bindIsolatedService(iApplicationThread1, iBinder18, (Intent)iServiceConnection2, str56, iServiceConnection3, param1Int2, str63, str60, m);
            iBinder.writeNoException();
            iBinder.writeInt(m);
            return true;
          case 29:
            iServiceConnection1.enforceInterface("android.app.IActivityManager");
            iApplicationThread5 = IApplicationThread.Stub.asInterface(iServiceConnection1.readStrongBinder());
            iBinder13 = iServiceConnection1.readStrongBinder();
            if (iServiceConnection1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iServiceConnection1);
            } else {
              iServiceConnection2 = null;
            } 
            str51 = iServiceConnection1.readString();
            iServiceConnection4 = IServiceConnection.Stub.asInterface(iServiceConnection1.readStrongBinder());
            param1Int2 = iServiceConnection1.readInt();
            str60 = iServiceConnection1.readString();
            m = iServiceConnection1.readInt();
            m = bindService(iApplicationThread5, iBinder13, (Intent)iServiceConnection2, str51, iServiceConnection4, param1Int2, str60, m);
            iBinder.writeNoException();
            iBinder.writeInt(m);
            return true;
          case 28:
            iServiceConnection1.enforceInterface("android.app.IActivityManager");
            iApplicationThread5 = IApplicationThread.Stub.asInterface(iServiceConnection1.readStrongBinder());
            if (iServiceConnection1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iServiceConnection1);
            } else {
              iServiceConnection2 = null;
            } 
            str26 = iServiceConnection1.readString();
            m = iServiceConnection1.readInt();
            m = stopService(iApplicationThread5, (Intent)iServiceConnection2, str26, m);
            iBinder.writeNoException();
            iBinder.writeInt(m);
            return true;
          case 27:
            iServiceConnection1.enforceInterface("android.app.IActivityManager");
            iApplicationThread5 = IApplicationThread.Stub.asInterface(iServiceConnection1.readStrongBinder());
            if (iServiceConnection1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)iServiceConnection1);
            } else {
              iServiceConnection2 = null;
            } 
            str51 = iServiceConnection1.readString();
            if (iServiceConnection1.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            str55 = iServiceConnection1.readString();
            str26 = iServiceConnection1.readString();
            m = iServiceConnection1.readInt();
            componentName1 = startService(iApplicationThread5, (Intent)iServiceConnection2, str51, bool43, str55, str26, m);
            iBinder.writeNoException();
            if (componentName1 != null) {
              iBinder.writeInt(1);
              componentName1.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 26:
            componentName1.enforceInterface("android.app.IActivityManager");
            if (componentName1.readInt() != 0) {
              componentName1 = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)componentName1);
            } else {
              componentName1 = null;
            } 
            pendingIntent = getRunningServiceControlPanel(componentName1);
            iBinder.writeNoException();
            if (pendingIntent != null) {
              iBinder.writeInt(1);
              pendingIntent.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 25:
            pendingIntent.enforceInterface("android.app.IActivityManager");
            iBinder23 = pendingIntent.readStrongBinder();
            param1Int2 = pendingIntent.readInt();
            m = pendingIntent.readInt();
            bool3 = refContentProvider(iBinder23, param1Int2, m);
            iBinder.writeNoException();
            iBinder.writeInt(bool3);
            return true;
          case 24:
            pendingIntent.enforceInterface("android.app.IActivityManager");
            iApplicationThread7 = IApplicationThread.Stub.asInterface(pendingIntent.readStrongBinder());
            arrayList = pendingIntent.createTypedArrayList(ContentProviderHolder.CREATOR);
            publishContentProviders(iApplicationThread7, arrayList);
            iBinder.writeNoException();
            return true;
          case 23:
            arrayList.enforceInterface("android.app.IActivityManager");
            iApplicationThread7 = IApplicationThread.Stub.asInterface(arrayList.readStrongBinder());
            str30 = arrayList.readString();
            str26 = arrayList.readString();
            k = arrayList.readInt();
            if (arrayList.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            contentProviderHolder1 = getContentProvider(iApplicationThread7, str30, str26, k, bool43);
            iBinder.writeNoException();
            if (contentProviderHolder1 != null) {
              iBinder.writeInt(1);
              contentProviderHolder1.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 22:
            contentProviderHolder1.enforceInterface("android.app.IActivityManager");
            iBinder22 = contentProviderHolder1.readStrongBinder();
            if (contentProviderHolder1.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            k = getTaskForActivity(iBinder22, bool43);
            iBinder.writeNoException();
            iBinder.writeInt(k);
            return true;
          case 21:
            contentProviderHolder1.enforceInterface("android.app.IActivityManager");
            iApplicationThread4 = IApplicationThread.Stub.asInterface(contentProviderHolder1.readStrongBinder());
            str37 = contentProviderHolder1.readString();
            param1Int2 = contentProviderHolder1.readInt();
            k = contentProviderHolder1.readInt();
            if (contentProviderHolder1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)contentProviderHolder1);
            } else {
              contentProviderHolder1 = null;
            } 
            moveTaskToFront(iApplicationThread4, str37, param1Int2, k, (Bundle)contentProviderHolder1);
            iBinder.writeNoException();
            return true;
          case 20:
            contentProviderHolder1.enforceInterface("android.app.IActivityManager");
            k = contentProviderHolder1.readInt();
            list = getTasks(k);
            iBinder.writeNoException();
            iBinder.writeTypedList(list);
            return true;
          case 19:
            list.enforceInterface("android.app.IActivityManager");
            iApplicationThread6 = IApplicationThread.Stub.asInterface(list.readStrongBinder());
            l = list.readLong();
            attachApplication(iApplicationThread6, l);
            iBinder.writeNoException();
            return true;
          case 18:
            list.enforceInterface("android.app.IActivityManager");
            iBinder17 = list.readStrongBinder();
            k = list.readInt();
            str36 = list.readString();
            if (list.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)list);
            } else {
              iBinder = null;
            } 
            if (list.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            param1Int2 = list.readInt();
            finishReceiver(iBinder17, k, str36, (Bundle)iBinder, bool43, param1Int2);
            return true;
          case 17:
            list.enforceInterface("android.app.IActivityManager");
            iApplicationThread3 = IApplicationThread.Stub.asInterface(list.readStrongBinder());
            if (list.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)list);
            } else {
              str36 = null;
            } 
            k = list.readInt();
            unbroadcastIntent(iApplicationThread3, (Intent)str36, k);
            iBinder.writeNoException();
            return true;
          case 16:
            list.enforceInterface("android.app.IActivityManager");
            iApplicationThread11 = IApplicationThread.Stub.asInterface(list.readStrongBinder());
            str55 = list.readString();
            if (list.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)list);
            } else {
              str36 = null;
            } 
            str65 = list.readString();
            iIntentReceiver5 = IIntentReceiver.Stub.asInterface(list.readStrongBinder());
            param1Int2 = list.readInt();
            str63 = list.readString();
            if (list.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)list);
            } else {
              iApplicationThread3 = null;
            } 
            arrayOfString5 = list.createStringArray();
            k = list.readInt();
            if (list.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)list);
            } else {
              str26 = null;
            } 
            if (list.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (list.readInt() != 0)
              bool42 = true; 
            i30 = list.readInt();
            k = broadcastIntentWithFeature(iApplicationThread11, str55, (Intent)str36, str65, iIntentReceiver5, param1Int2, str63, (Bundle)iApplicationThread3, arrayOfString5, k, (Bundle)str26, bool43, bool42, i30);
            iBinder.writeNoException();
            iBinder.writeInt(k);
            return true;
          case 15:
            list.enforceInterface("android.app.IActivityManager");
            iApplicationThread14 = IApplicationThread.Stub.asInterface(list.readStrongBinder());
            if (list.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)list);
            } else {
              str36 = null;
            } 
            str59 = list.readString();
            iIntentReceiver6 = IIntentReceiver.Stub.asInterface(list.readStrongBinder());
            k = list.readInt();
            str50 = list.readString();
            if (list.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)list);
            } else {
              iApplicationThread3 = null;
            } 
            arrayOfString6 = list.createStringArray();
            param1Int2 = list.readInt();
            if (list.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)list);
            } else {
              str26 = null;
            } 
            if (list.readInt() != 0) {
              bool43 = true;
            } else {
              bool43 = false;
            } 
            if (list.readInt() != 0) {
              bool42 = true;
            } else {
              bool42 = false;
            } 
            i30 = list.readInt();
            k = broadcastIntent(iApplicationThread14, (Intent)str36, str59, iIntentReceiver6, k, str50, (Bundle)iApplicationThread3, arrayOfString6, param1Int2, (Bundle)str26, bool43, bool42, i30);
            iBinder.writeNoException();
            iBinder.writeInt(k);
            return true;
          case 14:
            list.enforceInterface("android.app.IActivityManager");
            iIntentReceiver1 = IIntentReceiver.Stub.asInterface(list.readStrongBinder());
            unregisterReceiver(iIntentReceiver1);
            iBinder.writeNoException();
            return true;
          case 13:
            iIntentReceiver1.enforceInterface("android.app.IActivityManager");
            iApplicationThread3 = IApplicationThread.Stub.asInterface(iIntentReceiver1.readStrongBinder());
            str26 = iIntentReceiver1.readString();
            str54 = iIntentReceiver1.readString();
            iIntentReceiver3 = IIntentReceiver.Stub.asInterface(iIntentReceiver1.readStrongBinder());
            if (iIntentReceiver1.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)iIntentReceiver1);
            } else {
              str36 = null;
            } 
            str59 = iIntentReceiver1.readString();
            param1Int2 = iIntentReceiver1.readInt();
            k = iIntentReceiver1.readInt();
            intent1 = registerReceiverWithFeature(iApplicationThread3, str26, str54, iIntentReceiver3, (IntentFilter)str36, str59, param1Int2, k);
            iBinder.writeNoException();
            if (intent1 != null) {
              iBinder.writeInt(1);
              intent1.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 12:
            intent1.enforceInterface("android.app.IActivityManager");
            iApplicationThread13 = IApplicationThread.Stub.asInterface(intent1.readStrongBinder());
            str26 = intent1.readString();
            iIntentReceiver2 = IIntentReceiver.Stub.asInterface(intent1.readStrongBinder());
            if (intent1.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              str36 = null;
            } 
            str49 = intent1.readString();
            k = intent1.readInt();
            param1Int2 = intent1.readInt();
            intent1 = registerReceiver(iApplicationThread13, str26, iIntentReceiver2, (IntentFilter)str36, str49, k, param1Int2);
            iBinder.writeNoException();
            if (intent1 != null) {
              iBinder.writeInt(1);
              intent1.writeToParcel((Parcel)iBinder, 1);
            } else {
              iBinder.writeInt(0);
            } 
            return true;
          case 11:
            intent1.enforceInterface("android.app.IActivityManager");
            iBinder16 = intent1.readStrongBinder();
            param1Int2 = intent1.readInt();
            if (intent1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              str36 = null;
            } 
            k = intent1.readInt();
            bool2 = finishActivity(iBinder16, param1Int2, (Intent)str36, k);
            iBinder.writeNoException();
            iBinder.writeInt(bool2);
            return true;
          case 10:
            intent1.enforceInterface("android.app.IActivityManager");
            unhandledBack();
            iBinder.writeNoException();
            return true;
          case 9:
            intent1.enforceInterface("android.app.IActivityManager");
            iApplicationThread10 = IApplicationThread.Stub.asInterface(intent1.readStrongBinder());
            str26 = intent1.readString();
            str53 = intent1.readString();
            if (intent1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              str36 = null;
            } 
            str64 = intent1.readString();
            iBinder30 = intent1.readStrongBinder();
            str62 = intent1.readString();
            j = intent1.readInt();
            param1Int2 = intent1.readInt();
            if (intent1.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              iBinder16 = null;
            } 
            if (intent1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              intent1 = null;
            } 
            j = startActivityWithFeature(iApplicationThread10, str26, str53, (Intent)str36, str64, iBinder30, str62, j, param1Int2, (ProfilerInfo)iBinder16, (Bundle)intent1);
            iBinder.writeNoException();
            iBinder.writeInt(j);
            return true;
          case 8:
            intent1.enforceInterface("android.app.IActivityManager");
            iApplicationThread12 = IApplicationThread.Stub.asInterface(intent1.readStrongBinder());
            str26 = intent1.readString();
            if (intent1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              str36 = null;
            } 
            str62 = intent1.readString();
            iBinder29 = intent1.readStrongBinder();
            str58 = intent1.readString();
            j = intent1.readInt();
            param1Int2 = intent1.readInt();
            if (intent1.readInt() != 0) {
              ProfilerInfo profilerInfo = (ProfilerInfo)ProfilerInfo.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              iBinder16 = null;
            } 
            if (intent1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              intent1 = null;
            } 
            j = startActivity(iApplicationThread12, str26, (Intent)str36, str62, iBinder29, str58, j, param1Int2, (ProfilerInfo)iBinder16, (Bundle)intent1);
            iBinder.writeNoException();
            iBinder.writeInt(j);
            return true;
          case 7:
            intent1.enforceInterface("android.app.IActivityManager");
            iBinder21 = intent1.readStrongBinder();
            if (intent1.readInt() != 0) {
              ApplicationErrorReport.ParcelableCrashInfo parcelableCrashInfo = (ApplicationErrorReport.ParcelableCrashInfo)ApplicationErrorReport.ParcelableCrashInfo.CREATOR.createFromParcel((Parcel)intent1);
            } else {
              intent1 = null;
            } 
            handleApplicationCrash(iBinder21, (ApplicationErrorReport.ParcelableCrashInfo)intent1);
            iBinder.writeNoException();
            return true;
          case 6:
            intent1.enforceInterface("android.app.IActivityManager");
            j = intent1.readInt();
            setHwuiTaskThreads(j);
            return true;
          case 5:
            intent1.enforceInterface("android.app.IActivityManager");
            j = intent1.readInt();
            str2 = intent1.readString();
            j = getUidProcessState(j, str2);
            iBinder.writeNoException();
            iBinder.writeInt(j);
            return true;
          case 4:
            str2.enforceInterface("android.app.IActivityManager");
            j = str2.readInt();
            str2 = str2.readString();
            bool1 = isUidActive(j, str2);
            iBinder.writeNoException();
            iBinder.writeInt(bool1);
            return true;
          case 3:
            str2.enforceInterface("android.app.IActivityManager");
            iUidObserver1 = IUidObserver.Stub.asInterface(str2.readStrongBinder());
            unregisterUidObserver(iUidObserver1);
            iBinder.writeNoException();
            return true;
          case 2:
            iUidObserver1.enforceInterface("android.app.IActivityManager");
            iUidObserver2 = IUidObserver.Stub.asInterface(iUidObserver1.readStrongBinder());
            param1Int2 = iUidObserver1.readInt();
            i = iUidObserver1.readInt();
            str1 = iUidObserver1.readString();
            registerUidObserver(iUidObserver2, param1Int2, i, str1);
            iBinder.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.app.IActivityManager");
        String str1 = str1.readString();
        ParcelFileDescriptor parcelFileDescriptor1 = openContentUri(str1);
        iBinder.writeNoException();
        if (parcelFileDescriptor1 != null) {
          iBinder.writeInt(1);
          parcelFileDescriptor1.writeToParcel((Parcel)iBinder, 1);
        } else {
          iBinder.writeInt(0);
        } 
        return true;
      } 
      iBinder.writeString("android.app.IActivityManager");
      return true;
    }
    
    private static class Proxy implements IActivityManager {
      public static IActivityManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.app.IActivityManager";
      }
      
      public ParcelFileDescriptor openContentUri(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().openContentUri(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParcelFileDescriptor)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerUidObserver(IUidObserver param2IUidObserver, int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IUidObserver != null) {
            iBinder = param2IUidObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().registerUidObserver(param2IUidObserver, param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterUidObserver(IUidObserver param2IUidObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IUidObserver != null) {
            iBinder = param2IUidObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterUidObserver(param2IUidObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUidActive(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isUidActive(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUidProcessState(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().getUidProcessState(param2Int, param2String);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHwuiTaskThreads(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setHwuiTaskThreads(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void handleApplicationCrash(IBinder param2IBinder, ApplicationErrorReport.ParcelableCrashInfo param2ParcelableCrashInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2ParcelableCrashInfo != null) {
            parcel1.writeInt(1);
            param2ParcelableCrashInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().handleApplicationCrash(param2IBinder, param2ParcelableCrashInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivity(IApplicationThread param2IApplicationThread, String param2String1, Intent param2Intent, String param2String2, IBinder param2IBinder, String param2String3, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.startActivity(param2IApplicationThread, param2String1, param2Intent, param2String2, param2IBinder, param2String3, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startActivityWithFeature(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.startActivityWithFeature(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public void unhandledBack() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unhandledBack();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean finishActivity(IBinder param2IBinder, int param2Int1, Intent param2Intent, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().finishActivity(param2IBinder, param2Int1, param2Intent, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent registerReceiver(IApplicationThread param2IApplicationThread, String param2String1, IIntentReceiver param2IIntentReceiver, IntentFilter param2IntentFilter, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            iBinder2 = param2IApplicationThread.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeString(param2String1);
            iBinder2 = iBinder1;
            if (param2IIntentReceiver != null)
              iBinder2 = param2IIntentReceiver.asBinder(); 
            parcel1.writeStrongBinder(iBinder2);
            if (param2IntentFilter != null) {
              parcel1.writeInt(1);
              param2IntentFilter.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeInt(param2Int1);
                try {
                  parcel1.writeInt(param2Int2);
                  boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
                  if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                    Intent intent = IActivityManager.Stub.getDefaultImpl().registerReceiver(param2IApplicationThread, param2String1, param2IIntentReceiver, param2IntentFilter, param2String2, param2Int1, param2Int2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return intent;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    Intent intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2IApplicationThread = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (Intent)param2IApplicationThread;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public Intent registerReceiverWithFeature(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, IIntentReceiver param2IIntentReceiver, IntentFilter param2IntentFilter, String param2String3, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            iBinder2 = param2IApplicationThread.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeString(param2String2);
              iBinder2 = iBinder1;
              if (param2IIntentReceiver != null)
                iBinder2 = param2IIntentReceiver.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              if (param2IntentFilter != null) {
                parcel1.writeInt(1);
                param2IntentFilter.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeString(param2String3);
                parcel1.writeInt(param2Int1);
                parcel1.writeInt(param2Int2);
                boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
                if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                  Intent intent = IActivityManager.Stub.getDefaultImpl().registerReceiverWithFeature(param2IApplicationThread, param2String1, param2String2, param2IIntentReceiver, param2IntentFilter, param2String3, param2Int1, param2Int2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return intent;
                } 
                parcel2.readException();
                if (parcel2.readInt() != 0) {
                  Intent intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
                } else {
                  param2IApplicationThread = null;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return (Intent)param2IApplicationThread;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public void unregisterReceiver(IIntentReceiver param2IIntentReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentReceiver != null) {
            iBinder = param2IIntentReceiver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterReceiver(param2IIntentReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int broadcastIntent(IApplicationThread param2IApplicationThread, Intent param2Intent, String param2String1, IIntentReceiver param2IIntentReceiver, int param2Int1, String param2String2, Bundle param2Bundle1, String[] param2ArrayOfString, int param2Int2, Bundle param2Bundle2, boolean param2Boolean1, boolean param2Boolean2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            try {
              iBinder2 = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          IBinder iBinder2 = iBinder1;
          if (param2IIntentReceiver != null)
            iBinder2 = param2IIntentReceiver.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String2);
          if (param2Bundle1 != null) {
            parcel1.writeInt(1);
            param2Bundle1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int2);
          if (param2Bundle2 != null) {
            parcel1.writeInt(1);
            param2Bundle2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.broadcastIntent(param2IApplicationThread, param2Intent, param2String1, param2IIntentReceiver, param2Int1, param2String2, param2Bundle1, param2ArrayOfString, param2Int2, param2Bundle2, param2Boolean1, param2Boolean2, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int broadcastIntentWithFeature(IApplicationThread param2IApplicationThread, String param2String1, Intent param2Intent, String param2String2, IIntentReceiver param2IIntentReceiver, int param2Int1, String param2String3, Bundle param2Bundle1, String[] param2ArrayOfString, int param2Int2, Bundle param2Bundle2, boolean param2Boolean1, boolean param2Boolean2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            try {
              iBinder2 = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeString(param2String1);
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          IBinder iBinder2 = iBinder1;
          if (param2IIntentReceiver != null)
            iBinder2 = param2IIntentReceiver.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String3);
          if (param2Bundle1 != null) {
            parcel1.writeInt(1);
            param2Bundle1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int2);
          if (param2Bundle2 != null) {
            parcel1.writeInt(1);
            param2Bundle2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.broadcastIntentWithFeature(param2IApplicationThread, param2String1, param2Intent, param2String2, param2IIntentReceiver, param2Int1, param2String3, param2Bundle1, param2ArrayOfString, param2Int2, param2Bundle2, param2Boolean1, param2Boolean2, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public void unbroadcastIntent(IApplicationThread param2IApplicationThread, Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unbroadcastIntent(param2IApplicationThread, param2Intent, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishReceiver(IBinder param2IBinder, int param2Int1, String param2String, Bundle param2Bundle, boolean param2Boolean, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel.writeStrongBinder(param2IBinder);
            try {
              parcel.writeInt(param2Int1);
              try {
                parcel.writeString(param2String);
                boolean bool = false;
                if (param2Bundle != null) {
                  parcel.writeInt(1);
                  param2Bundle.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                if (param2Boolean)
                  bool = true; 
                parcel.writeInt(bool);
                try {
                  parcel.writeInt(param2Int2);
                  try {
                    boolean bool1 = this.mRemote.transact(18, parcel, null, 1);
                    if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
                      IActivityManager.Stub.getDefaultImpl().finishReceiver(param2IBinder, param2Int1, param2String, param2Bundle, param2Boolean, param2Int2);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void attachApplication(IApplicationThread param2IApplicationThread, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().attachApplication(param2IApplicationThread, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningTaskInfo> getTasks(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getTasks(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningTaskInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveTaskToFront(IApplicationThread param2IApplicationThread, String param2String, int param2Int1, int param2Int2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().moveTaskToFront(param2IApplicationThread, param2String, param2Int1, param2Int2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTaskForActivity(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            i = IActivityManager.Stub.getDefaultImpl().getTaskForActivity(param2IBinder, param2Boolean);
            return i;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ContentProviderHolder getContentProvider(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getContentProvider(param2IApplicationThread, param2String1, param2String2, param2Int, param2Boolean); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ContentProviderHolder contentProviderHolder = (ContentProviderHolder)ContentProviderHolder.CREATOR.createFromParcel(parcel2);
          } else {
            param2IApplicationThread = null;
          } 
          return (ContentProviderHolder)param2IApplicationThread;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void publishContentProviders(IApplicationThread param2IApplicationThread, List<ContentProviderHolder> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().publishContentProviders(param2IApplicationThread, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean refContentProvider(IBinder param2IBinder, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().refContentProvider(param2IBinder, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PendingIntent getRunningServiceControlPanel(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getRunningServiceControlPanel(param2ComponentName); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PendingIntent pendingIntent = (PendingIntent)PendingIntent.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (PendingIntent)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName startService(IApplicationThread param2IApplicationThread, Intent param2Intent, String param2String1, boolean param2Boolean, String param2String2, String param2String3, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeString(param2String1);
            if (!param2Boolean)
              bool = false; 
            parcel1.writeInt(bool);
            try {
              parcel1.writeString(param2String2);
              try {
                parcel1.writeString(param2String3);
                try {
                  parcel1.writeInt(param2Int);
                  boolean bool1 = this.mRemote.transact(27, parcel1, parcel2, 0);
                  if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
                    ComponentName componentName = IActivityManager.Stub.getDefaultImpl().startService(param2IApplicationThread, param2Intent, param2String1, param2Boolean, param2String2, param2String3, param2Int);
                    parcel2.recycle();
                    parcel1.recycle();
                    return componentName;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2IApplicationThread = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (ComponentName)param2IApplicationThread;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int stopService(IApplicationThread param2IApplicationThread, Intent param2Intent, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().stopService(param2IApplicationThread, param2Intent, param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int bindService(IApplicationThread param2IApplicationThread, IBinder param2IBinder, Intent param2Intent, String param2String1, IServiceConnection param2IServiceConnection, int param2Int1, String param2String2, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            iBinder2 = param2IApplicationThread.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeStrongBinder(param2IBinder);
            if (param2Intent != null) {
              parcel1.writeInt(1);
              param2Intent.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String1);
              iBinder2 = iBinder1;
              if (param2IServiceConnection != null)
                iBinder2 = param2IServiceConnection.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              try {
                parcel1.writeInt(param2Int1);
                parcel1.writeString(param2String2);
                parcel1.writeInt(param2Int2);
                boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
                if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                  param2Int1 = IActivityManager.Stub.getDefaultImpl().bindService(param2IApplicationThread, param2IBinder, param2Intent, param2String1, param2IServiceConnection, param2Int1, param2String2, param2Int2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Int1;
                } 
                parcel2.readException();
                param2Int1 = parcel2.readInt();
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int bindIsolatedService(IApplicationThread param2IApplicationThread, IBinder param2IBinder, Intent param2Intent, String param2String1, IServiceConnection param2IServiceConnection, int param2Int1, String param2String2, String param2String3, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IApplicationThread != null) {
            iBinder2 = param2IApplicationThread.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeStrongBinder(param2IBinder);
            if (param2Intent != null) {
              parcel1.writeInt(1);
              param2Intent.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String1);
              iBinder2 = iBinder1;
              if (param2IServiceConnection != null)
                iBinder2 = param2IServiceConnection.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              parcel1.writeInt(param2Int1);
              parcel1.writeString(param2String2);
              parcel1.writeString(param2String3);
              parcel1.writeInt(param2Int2);
              boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
              if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                param2Int1 = IActivityManager.Stub.getDefaultImpl().bindIsolatedService(param2IApplicationThread, param2IBinder, param2Intent, param2String1, param2IServiceConnection, param2Int1, param2String2, param2String3, param2Int2);
                parcel2.recycle();
                parcel1.recycle();
                return param2Int1;
              } 
              parcel2.readException();
              param2Int1 = parcel2.readInt();
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public void updateServiceGroup(IServiceConnection param2IServiceConnection, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IServiceConnection != null) {
            iBinder = param2IServiceConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().updateServiceGroup(param2IServiceConnection, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unbindService(IServiceConnection param2IServiceConnection) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IServiceConnection != null) {
            iBinder = param2IServiceConnection.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(32, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().unbindService(param2IServiceConnection);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void publishService(IBinder param2IBinder1, Intent param2Intent, IBinder param2IBinder2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder1);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder2);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().publishService(param2IBinder1, param2Intent, param2IBinder2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDebugApp(String param2String, boolean param2Boolean1, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setDebugApp(param2String, param2Boolean1, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAgentApp(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setAgentApp(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setAlwaysFinish(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setAlwaysFinish(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startInstrumentation(ComponentName param2ComponentName, String param2String1, int param2Int1, Bundle param2Bundle, IInstrumentationWatcher param2IInstrumentationWatcher, IUiAutomationConnection param2IUiAutomationConnection, int param2Int2, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeString(param2String1);
            try {
              parcel1.writeInt(param2Int1);
              if (param2Bundle != null) {
                parcel1.writeInt(1);
                param2Bundle.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              IBinder iBinder1 = null;
              if (param2IInstrumentationWatcher != null) {
                iBinder2 = param2IInstrumentationWatcher.asBinder();
              } else {
                iBinder2 = null;
              } 
              parcel1.writeStrongBinder(iBinder2);
              IBinder iBinder2 = iBinder1;
              if (param2IUiAutomationConnection != null)
                iBinder2 = param2IUiAutomationConnection.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              parcel1.writeInt(param2Int2);
              parcel1.writeString(param2String2);
              boolean bool1 = this.mRemote.transact(37, parcel1, parcel2, 0);
              if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
                bool = IActivityManager.Stub.getDefaultImpl().startInstrumentation(param2ComponentName, param2String1, param2Int1, param2Bundle, param2IInstrumentationWatcher, param2IUiAutomationConnection, param2Int2, param2String2);
                parcel2.recycle();
                parcel1.recycle();
                return bool;
              } 
              parcel2.readException();
              param2Int1 = parcel2.readInt();
              if (param2Int1 == 0)
                bool = false; 
              parcel2.recycle();
              parcel1.recycle();
              return bool;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ComponentName;
      }
      
      public void addInstrumentationResults(IApplicationThread param2IApplicationThread, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().addInstrumentationResults(param2IApplicationThread, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishInstrumentation(IApplicationThread param2IApplicationThread, int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().finishInstrumentation(param2IApplicationThread, param2Int, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Configuration getConfiguration() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Configuration configuration;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            configuration = IActivityManager.Stub.getDefaultImpl().getConfiguration();
            return configuration;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            configuration = (Configuration)Configuration.CREATOR.createFromParcel(parcel2);
          } else {
            configuration = null;
          } 
          return configuration;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateConfiguration(Configuration param2Configuration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool1 = true;
          if (param2Configuration != null) {
            parcel1.writeInt(1);
            param2Configuration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().updateConfiguration(param2Configuration);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateMccMncConfiguration(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().updateMccMncConfiguration(param2String1, param2String2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopServiceToken(ComponentName param2ComponentName, IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().stopServiceToken(param2ComponentName, param2IBinder, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setProcessLimit(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setProcessLimit(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getProcessLimit() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getProcessLimit(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkPermission(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IActivityManager.Stub.getDefaultImpl().checkPermission(param2String, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkUriPermission(Uri param2Uri, int param2Int1, int param2Int2, int param2Int3, int param2Int4, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeInt(param2Int3);
                try {
                  parcel1.writeInt(param2Int4);
                  try {
                    parcel1.writeStrongBinder(param2IBinder);
                    boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
                    if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                      param2Int1 = IActivityManager.Stub.getDefaultImpl().checkUriPermission(param2Uri, param2Int1, param2Int2, param2Int3, param2Int4, param2IBinder);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } 
                    parcel2.readException();
                    param2Int1 = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Uri;
      }
      
      public void grantUriPermission(IApplicationThread param2IApplicationThread, String param2String, Uri param2Uri, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().grantUriPermission(param2IApplicationThread, param2String, param2Uri, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void revokeUriPermission(IApplicationThread param2IApplicationThread, String param2String, Uri param2Uri, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().revokeUriPermission(param2IApplicationThread, param2String, param2Uri, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActivityController(IActivityController param2IActivityController, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IActivityController != null) {
            iBinder = param2IActivityController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setActivityController(param2IActivityController, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showWaitingForDebugger(IApplicationThread param2IApplicationThread, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            iBinder = param2IApplicationThread.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().showWaitingForDebugger(param2IApplicationThread, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void signalPersistentProcesses(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().signalPersistentProcesses(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getRecentTasks(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IActivityManager.Stub.getDefaultImpl().getRecentTasks(param2Int1, param2Int2, param2Int3);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void serviceDoneExecuting(IBinder param2IBinder, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(54, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().serviceDoneExecuting(param2IBinder, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public IIntentSender getIntentSender(int param2Int1, String param2String1, IBinder param2IBinder, String param2String2, int param2Int2, Intent[] param2ArrayOfIntent, String[] param2ArrayOfString, int param2Int3, Bundle param2Bundle, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeInt(param2Int1);
            parcel1.writeString(param2String1);
            parcel1.writeStrongBinder(param2IBinder);
            parcel1.writeString(param2String2);
            parcel1.writeInt(param2Int2);
            parcel1.writeTypedArray((Parcelable[])param2ArrayOfIntent, 0);
            parcel1.writeStringArray(param2ArrayOfString);
            parcel1.writeInt(param2Int3);
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            parcel1.writeInt(param2Int4);
            boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
            if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
              IIntentSender iIntentSender1 = IActivityManager.Stub.getDefaultImpl().getIntentSender(param2Int1, param2String1, param2IBinder, param2String2, param2Int2, param2ArrayOfIntent, param2ArrayOfString, param2Int3, param2Bundle, param2Int4);
              parcel2.recycle();
              parcel1.recycle();
              return iIntentSender1;
            } 
            parcel2.readException();
            IIntentSender iIntentSender = IIntentSender.Stub.asInterface(parcel2.readStrongBinder());
            parcel2.recycle();
            parcel1.recycle();
            return iIntentSender;
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public IIntentSender getIntentSenderWithFeature(int param2Int1, String param2String1, String param2String2, IBinder param2IBinder, String param2String3, int param2Int2, Intent[] param2ArrayOfIntent, String[] param2ArrayOfString, int param2Int3, Bundle param2Bundle, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int2);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfIntent, 0);
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int3);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getIntentSenderWithFeature(param2Int1, param2String1, param2String2, param2IBinder, param2String3, param2Int2, param2ArrayOfIntent, param2ArrayOfString, param2Int3, param2Bundle, param2Int4); 
          parcel2.readException();
          return IIntentSender.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelIntentSender(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().cancelIntentSender(param2IIntentSender);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPackageForIntentSender(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getPackageForIntentSender(param2IIntentSender); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerIntentSenderCancelListener(IIntentSender param2IIntentSender, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IIntentSender != null) {
            iBinder2 = param2IIntentSender.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IResultReceiver != null)
            iBinder2 = param2IResultReceiver.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().registerIntentSenderCancelListener(param2IIntentSender, param2IResultReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterIntentSenderCancelListener(IIntentSender param2IIntentSender, IResultReceiver param2IResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IIntentSender != null) {
            iBinder2 = param2IIntentSender.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IResultReceiver != null)
            iBinder2 = param2IResultReceiver.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterIntentSenderCancelListener(param2IIntentSender, param2IResultReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterSafeMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().enterSafeMode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteWakeupAlarm(IIntentSender param2IIntentSender, WorkSource param2WorkSource, int param2Int, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().noteWakeupAlarm(param2IIntentSender, param2WorkSource, param2Int, param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeContentProvider(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(63, parcel, null, 1);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().removeContentProvider(param2IBinder, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setRequestedOrientation(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(64, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setRequestedOrientation(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unbindFinished(IBinder param2IBinder, Intent param2Intent, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unbindFinished(param2IBinder, param2Intent, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setProcessImportant(IBinder param2IBinder, int param2Int, boolean param2Boolean, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeString(param2String);
          boolean bool1 = this.mRemote.transact(66, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setProcessImportant(param2IBinder, param2Int, param2Boolean, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setServiceForeground(ComponentName param2ComponentName, IBinder param2IBinder, int param2Int1, Notification param2Notification, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeInt(param2Int1);
              if (param2Notification != null) {
                parcel1.writeInt(1);
                param2Notification.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
                  if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                    IActivityManager.Stub.getDefaultImpl().setServiceForeground(param2ComponentName, param2IBinder, param2Int1, param2Notification, param2Int2, param2Int3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ComponentName;
      }
      
      public int getForegroundServiceType(ComponentName param2ComponentName, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getForegroundServiceType(param2ComponentName, param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean moveActivityTaskToBack(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityManager.Stub.getDefaultImpl().moveActivityTaskToBack(param2IBinder, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getMemoryInfo(ActivityManager.MemoryInfo param2MemoryInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().getMemoryInfo(param2MemoryInfo);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2MemoryInfo.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.ProcessErrorStateInfo> getProcessesInErrorState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getProcessesInErrorState(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.ProcessErrorStateInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean clearApplicationUserData(String param2String, boolean param2Boolean, IPackageDataObserver param2IPackageDataObserver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2IPackageDataObserver != null) {
            iBinder = param2IPackageDataObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityManager.Stub.getDefaultImpl().clearApplicationUserData(param2String, param2Boolean, param2IPackageDataObserver, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceStopPackage(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().forceStopPackage(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean killPids(int[] param2ArrayOfint, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IActivityManager.Stub.getDefaultImpl().killPids(param2ArrayOfint, param2String, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningServiceInfo> getServices(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getServices(param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningServiceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.RunningAppProcessInfo> getRunningAppProcesses() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getRunningAppProcesses(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.RunningAppProcessInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IBinder peekService(Intent param2Intent, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().peekService(param2Intent, param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean profileControl(String param2String, int param2Int1, boolean param2Boolean, ProfilerInfo param2ProfilerInfo, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeString(param2String);
            try {
              boolean bool2;
              parcel1.writeInt(param2Int1);
              boolean bool1 = true;
              if (param2Boolean) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              if (param2ProfilerInfo != null) {
                parcel1.writeInt(1);
                param2ProfilerInfo.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                try {
                  boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
                  if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                    param2Boolean = IActivityManager.Stub.getDefaultImpl().profileControl(param2String, param2Int1, param2Boolean, param2ProfilerInfo, param2Int2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean;
                  } 
                  parcel2.readException();
                  param2Int1 = parcel2.readInt();
                  if (param2Int1 != 0) {
                    param2Boolean = bool1;
                  } else {
                    param2Boolean = false;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String;
      }
      
      public boolean shutdown(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(79, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().shutdown(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopAppSwitches() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().stopAppSwitches();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resumeAppSwitches() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().resumeAppSwitches();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean bindBackupAgent(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(82, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().bindBackupAgent(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void backupAgentCreated(String param2String, IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().backupAgentCreated(param2String, param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unbindBackupAgent(ApplicationInfo param2ApplicationInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ApplicationInfo != null) {
            parcel1.writeInt(1);
            param2ApplicationInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unbindBackupAgent(param2ApplicationInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUidForIntentSender(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getUidForIntentSender(param2IIntentSender); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int handleIncomingUser(int param2Int1, int param2Int2, int param2Int3, boolean param2Boolean1, boolean param2Boolean2, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                boolean bool2;
                parcel1.writeInt(param2Int3);
                boolean bool1 = true;
                if (param2Boolean1) {
                  bool2 = true;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                if (param2Boolean2) {
                  bool2 = bool1;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                try {
                  parcel1.writeString(param2String1);
                  try {
                    parcel1.writeString(param2String2);
                    boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
                    if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                      param2Int1 = IActivityManager.Stub.getDefaultImpl().handleIncomingUser(param2Int1, param2Int2, param2Int3, param2Boolean1, param2Boolean2, param2String1, param2String2);
                      parcel2.recycle();
                      parcel1.recycle();
                      return param2Int1;
                    } 
                    parcel2.readException();
                    param2Int1 = parcel2.readInt();
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Int1;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void addPackageDependency(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().addPackageDependency(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killApplication(String param2String1, int param2Int1, int param2Int2, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killApplication(param2String1, param2Int1, param2Int2, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeSystemDialogs(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().closeSystemDialogs(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Debug.MemoryInfo[] getProcessMemoryInfo(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getProcessMemoryInfo(param2ArrayOfint); 
          parcel2.readException();
          return (Debug.MemoryInfo[])parcel2.createTypedArray(Debug.MemoryInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killApplicationProcess(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killApplicationProcess(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean handleApplicationWtf(IBinder param2IBinder, String param2String, boolean param2Boolean, ApplicationErrorReport.ParcelableCrashInfo param2ParcelableCrashInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              boolean bool2;
              parcel1.writeString(param2String);
              boolean bool1 = true;
              if (param2Boolean) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              if (param2ParcelableCrashInfo != null) {
                parcel1.writeInt(1);
                param2ParcelableCrashInfo.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int);
                try {
                  boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
                  if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                    param2Boolean = IActivityManager.Stub.getDefaultImpl().handleApplicationWtf(param2IBinder, param2String, param2Boolean, param2ParcelableCrashInfo, param2Int);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean;
                  } 
                  parcel2.readException();
                  param2Int = parcel2.readInt();
                  if (param2Int != 0) {
                    param2Boolean = bool1;
                  } else {
                    param2Boolean = false;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IBinder;
      }
      
      public void killBackgroundProcesses(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killBackgroundProcesses(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUserAMonkey() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(94, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isUserAMonkey();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ApplicationInfo> getRunningExternalApplications() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getRunningExternalApplications(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ApplicationInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishHeavyWeightApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(96, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().finishHeavyWeightApp();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void handleApplicationStrictModeViolation(IBinder param2IBinder, int param2Int, StrictMode.ViolationInfo param2ViolationInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          if (param2ViolationInfo != null) {
            parcel1.writeInt(1);
            param2ViolationInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().handleApplicationStrictModeViolation(param2IBinder, param2Int, param2ViolationInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTopActivityImmersive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(98, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isTopActivityImmersive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void crashApplication(int param2Int1, int param2Int2, String param2String1, int param2Int3, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeString(param2String1);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    boolean bool;
                    parcel1.writeString(param2String2);
                    if (param2Boolean) {
                      bool = true;
                    } else {
                      bool = false;
                    } 
                    parcel1.writeInt(bool);
                    try {
                      boolean bool1 = this.mRemote.transact(99, parcel1, parcel2, 0);
                      if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
                        IActivityManager.Stub.getDefaultImpl().crashApplication(param2Int1, param2Int2, param2String1, param2Int3, param2String2, param2Boolean);
                        parcel2.recycle();
                        parcel1.recycle();
                        return;
                      } 
                      parcel2.readException();
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } finally {}
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public String getProviderMimeType(Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getProviderMimeType(param2Uri, param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getProviderMimeTypeAsync(Uri param2Uri, int param2Int, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2RemoteCallback != null) {
            parcel.writeInt(1);
            param2RemoteCallback.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(101, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().getProviderMimeTypeAsync(param2Uri, param2Int, param2RemoteCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean dumpHeap(String param2String1, int param2Int, boolean param2Boolean1, boolean param2Boolean2, boolean param2Boolean3, String param2String2, ParcelFileDescriptor param2ParcelFileDescriptor, RemoteCallback param2RemoteCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          try {
            parcel1.writeString(param2String1);
            try {
              boolean bool2;
              parcel1.writeInt(param2Int);
              boolean bool1 = true;
              if (param2Boolean1) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              if (param2Boolean2) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              if (param2Boolean3) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              parcel1.writeString(param2String2);
              if (param2ParcelFileDescriptor != null) {
                parcel1.writeInt(1);
                param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              if (param2RemoteCallback != null) {
                parcel1.writeInt(1);
                param2RemoteCallback.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(102, parcel1, parcel2, 0);
              if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                param2Boolean1 = IActivityManager.Stub.getDefaultImpl().dumpHeap(param2String1, param2Int, param2Boolean1, param2Boolean2, param2Boolean3, param2String2, param2ParcelFileDescriptor, param2RemoteCallback);
                parcel2.recycle();
                parcel1.recycle();
                return param2Boolean1;
              } 
              parcel2.readException();
              param2Int = parcel2.readInt();
              if (param2Int != 0) {
                param2Boolean1 = bool1;
              } else {
                param2Boolean1 = false;
              } 
              parcel2.recycle();
              parcel1.recycle();
              return param2Boolean1;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean isUserRunning(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(103, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isUserRunning(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPackageScreenCompatMode(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(104, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setPackageScreenCompatMode(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean switchUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(105, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().switchUser(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean removeTask(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(106, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().removeTask(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerProcessObserver(IProcessObserver param2IProcessObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IProcessObserver != null) {
            iBinder = param2IProcessObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().registerProcessObserver(param2IProcessObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterProcessObserver(IProcessObserver param2IProcessObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IProcessObserver != null) {
            iBinder = param2IProcessObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterProcessObserver(param2IProcessObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIntentSenderTargetedToPackage(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(109, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isIntentSenderTargetedToPackage(param2IIntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updatePersistentConfiguration(Configuration param2Configuration) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Configuration != null) {
            parcel1.writeInt(1);
            param2Configuration.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().updatePersistentConfiguration(param2Configuration);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long[] getProcessPss(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getProcessPss(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createLongArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void showBootMessage(CharSequence param2CharSequence, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = true;
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(112, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().showBootMessage(param2CharSequence, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killAllBackgroundProcesses() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killAllBackgroundProcesses();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ContentProviderHolder getContentProviderExternal(String param2String1, int param2Int, IBinder param2IBinder, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getContentProviderExternal(param2String1, param2Int, param2IBinder, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ContentProviderHolder contentProviderHolder = (ContentProviderHolder)ContentProviderHolder.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ContentProviderHolder)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeContentProviderExternal(String param2String, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().removeContentProviderExternal(param2String, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeContentProviderExternalAsUser(String param2String, IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(116, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().removeContentProviderExternalAsUser(param2String, param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getMyMemoryState(ActivityManager.RunningAppProcessInfo param2RunningAppProcessInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().getMyMemoryState(param2RunningAppProcessInfo);
            return;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0)
            param2RunningAppProcessInfo.readFromParcel(parcel2); 
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean killProcessesBelowForeground(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(118, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().killProcessesBelowForeground(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public UserInfo getCurrentUser() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          UserInfo userInfo;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            userInfo = IActivityManager.Stub.getDefaultImpl().getCurrentUser();
            return userInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            userInfo = (UserInfo)UserInfo.CREATOR.createFromParcel(parcel2);
          } else {
            userInfo = null;
          } 
          return userInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLaunchedFromUid(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getLaunchedFromUid(param2IBinder); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unstableProviderDied(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unstableProviderDied(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIntentSenderAnActivity(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(122, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isIntentSenderAnActivity(param2IIntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIntentSenderAForegroundService(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(123, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isIntentSenderAForegroundService(param2IIntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isIntentSenderABroadcast(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(124, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isIntentSenderABroadcast(param2IIntentSender);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivityAsUser(IApplicationThread param2IApplicationThread, String param2String1, Intent param2Intent, String param2String2, IBinder param2IBinder, String param2String3, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.startActivityAsUser(param2IApplicationThread, param2String1, param2Intent, param2String2, param2IBinder, param2String3, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int startActivityAsUserWithFeature(IApplicationThread param2IApplicationThread, String param2String1, String param2String2, Intent param2Intent, String param2String3, IBinder param2IBinder, String param2String4, int param2Int1, int param2Int2, ProfilerInfo param2ProfilerInfo, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IApplicationThread != null) {
            try {
              iBinder = param2IApplicationThread.asBinder();
            } finally {}
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String3);
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String4);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2ProfilerInfo != null) {
            parcel1.writeInt(1);
            param2ProfilerInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager iActivityManager = IActivityManager.Stub.getDefaultImpl();
            try {
              param2Int1 = iActivityManager.startActivityAsUserWithFeature(param2IApplicationThread, param2String1, param2String2, param2Intent, param2String3, param2IBinder, param2String4, param2Int1, param2Int2, param2ProfilerInfo, param2Bundle, param2Int3);
              parcel2.recycle();
              parcel1.recycle();
              return param2Int1;
            } finally {}
          } else {
            parcel2.readException();
            param2Int1 = parcel2.readInt();
            parcel2.recycle();
            parcel1.recycle();
            return param2Int1;
          } 
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IApplicationThread;
      }
      
      public int stopUser(int param2Int, boolean param2Boolean, IStopUserCallback param2IStopUserCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2IStopUserCallback != null) {
            iBinder = param2IStopUserCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(127, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().stopUser(param2Int, param2Boolean, param2IStopUserCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int stopUserWithDelayedLocking(int param2Int, boolean param2Boolean, IStopUserCallback param2IStopUserCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          if (param2IStopUserCallback != null) {
            iBinder = param2IStopUserCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(128, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().stopUserWithDelayedLocking(param2Int, param2Boolean, param2IStopUserCallback);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerUserSwitchObserver(IUserSwitchObserver param2IUserSwitchObserver, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IUserSwitchObserver != null) {
            iBinder = param2IUserSwitchObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(129, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().registerUserSwitchObserver(param2IUserSwitchObserver, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterUserSwitchObserver(IUserSwitchObserver param2IUserSwitchObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IUserSwitchObserver != null) {
            iBinder = param2IUserSwitchObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(130, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterUserSwitchObserver(param2IUserSwitchObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getRunningUserIds() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getRunningUserIds(); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestSystemServerHeapDump() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(132, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestSystemServerHeapDump();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestBugReport(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestBugReport(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestBugReportWithDescription(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(134, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestBugReportWithDescription(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestTelephonyBugReport(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(135, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestTelephonyBugReport(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestWifiBugReport(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(136, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestWifiBugReport(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestInteractiveBugReportWithDescription(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(137, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestInteractiveBugReportWithDescription(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestInteractiveBugReport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(138, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestInteractiveBugReport();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestFullBugReport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(139, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestFullBugReport();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestRemoteBugReport() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(140, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().requestRemoteBugReport();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean launchBugReportHandlerApp() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(141, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().launchBugReportHandlerApp();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getBugreportWhitelistedPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(142, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getBugreportWhitelistedPackages(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent getIntentForIntentSender(IIntentSender param2IIntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(143, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getIntentForIntentSender(param2IIntentSender); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Intent intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            param2IIntentSender = null;
          } 
          return (Intent)param2IIntentSender;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getLaunchedFromPackage(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(144, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getLaunchedFromPackage(param2IBinder); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killUid(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(145, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killUid(param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUserIsMonkey(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(146, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setUserIsMonkey(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void hang(IBinder param2IBinder, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(147, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().hang(param2IBinder, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ActivityManager.StackInfo> getAllStackInfos() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(148, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getAllStackInfos(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ActivityManager.StackInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void moveTaskToStack(int param2Int1, int param2Int2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(149, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().moveTaskToStack(param2Int1, param2Int2, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFocusedStack(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(150, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setFocusedStack(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.StackInfo getFocusedStackInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ActivityManager.StackInfo stackInfo;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(151, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            stackInfo = IActivityManager.Stub.getDefaultImpl().getFocusedStackInfo();
            return stackInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            stackInfo = (ActivityManager.StackInfo)ActivityManager.StackInfo.CREATOR.createFromParcel(parcel2);
          } else {
            stackInfo = null;
          } 
          return stackInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restart() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(152, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().restart();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void performIdleMaintenance() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(153, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().performIdleMaintenance();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void appNotRespondingViaProvider(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(154, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().appNotRespondingViaProvider(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Rect getTaskBounds(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Rect rect;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(155, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            rect = IActivityManager.Stub.getDefaultImpl().getTaskBounds(param2Int);
            return rect;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            rect = (Rect)Rect.CREATOR.createFromParcel(parcel2);
          } else {
            rect = null;
          } 
          return rect;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setProcessMemoryTrimLevel(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(156, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().setProcessMemoryTrimLevel(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTagForIntentSender(IIntentSender param2IIntentSender, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(157, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getTagForIntentSender(param2IIntentSender, param2String); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startUserInBackground(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(158, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().startUserInBackground(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInLockTaskMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(159, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isInLockTaskMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startRecentsActivity(Intent param2Intent, IAssistDataReceiver param2IAssistDataReceiver, IRecentsAnimationRunner param2IRecentsAnimationRunner) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          IBinder iBinder1 = null;
          if (param2IAssistDataReceiver != null) {
            iBinder2 = param2IAssistDataReceiver.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          IBinder iBinder2 = iBinder1;
          if (param2IRecentsAnimationRunner != null)
            iBinder2 = param2IRecentsAnimationRunner.asBinder(); 
          parcel1.writeStrongBinder(iBinder2);
          boolean bool = this.mRemote.transact(160, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().startRecentsActivity(param2Intent, param2IAssistDataReceiver, param2IRecentsAnimationRunner);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelRecentsAnimation(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(161, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().cancelRecentsAnimation(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int startActivityFromRecents(int param2Int, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(162, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().startActivityFromRecents(param2Int, param2Bundle);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startSystemLockTaskMode(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(163, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().startSystemLockTaskMode(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTopOfTask(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(164, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isTopOfTask(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void bootAnimationComplete() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(165, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().bootAnimationComplete();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkPermissionWithToken(String param2String, int param2Int1, int param2Int2, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(166, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IActivityManager.Stub.getDefaultImpl().checkPermissionWithToken(param2String, param2Int1, param2Int2, param2IBinder);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerTaskStackListener(ITaskStackListener param2ITaskStackListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ITaskStackListener != null) {
            iBinder = param2ITaskStackListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(167, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().registerTaskStackListener(param2ITaskStackListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterTaskStackListener(ITaskStackListener param2ITaskStackListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ITaskStackListener != null) {
            iBinder = param2ITaskStackListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(168, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().unregisterTaskStackListener(param2ITaskStackListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyCleartextNetwork(int param2Int, byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(169, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().notifyCleartextNetwork(param2Int, param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setTaskResizeable(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(170, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setTaskResizeable(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resizeTask(int param2Int1, Rect param2Rect, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(171, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().resizeTask(param2Int1, param2Rect, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getLockTaskModeState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(172, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getLockTaskModeState(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDumpHeapDebugLimit(String param2String1, int param2Int, long param2Long, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(173, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setDumpHeapDebugLimit(param2String1, param2Int, param2Long, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dumpHeapFinished(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(174, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().dumpHeapFinished(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateLockTaskPackages(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(175, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().updateLockTaskPackages(param2Int, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteAlarmStart(IIntentSender param2IIntentSender, WorkSource param2WorkSource, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(176, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().noteAlarmStart(param2IIntentSender, param2WorkSource, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void noteAlarmFinish(IIntentSender param2IIntentSender, WorkSource param2WorkSource, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IIntentSender != null) {
            iBinder = param2IIntentSender.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2WorkSource != null) {
            parcel1.writeInt(1);
            param2WorkSource.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(177, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().noteAlarmFinish(param2IIntentSender, param2WorkSource, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPackageProcessState(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(178, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getPackageProcessState(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateDeviceOwner(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(179, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().updateDeviceOwner(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startBinderTracking() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(180, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().startBinderTracking();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean stopBinderTrackingAndDump(ParcelFileDescriptor param2ParcelFileDescriptor) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool1 = true;
          if (param2ParcelFileDescriptor != null) {
            parcel1.writeInt(1);
            param2ParcelFileDescriptor.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(181, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().stopBinderTrackingAndDump(param2ParcelFileDescriptor);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void positionTaskInStack(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(182, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().positionTaskInStack(param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void suppressResizeConfigChanges(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(183, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().suppressResizeConfigChanges(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean moveTopActivityToPinnedStack(int param2Int, Rect param2Rect) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(184, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().moveTopActivityToPinnedStack(param2Int, param2Rect);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAppStartModeDisabled(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(185, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isAppStartModeDisabled(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean unlockUser(int param2Int, byte[] param2ArrayOfbyte1, byte[] param2ArrayOfbyte2, IProgressListener param2IProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeByteArray(param2ArrayOfbyte1);
          parcel1.writeByteArray(param2ArrayOfbyte2);
          if (param2IProgressListener != null) {
            iBinder = param2IProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(186, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().unlockUser(param2Int, param2ArrayOfbyte1, param2ArrayOfbyte2, param2IProgressListener);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killPackageDependents(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(187, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killPackageDependents(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeStack(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(188, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().removeStack(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void makePackageIdle(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(189, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().makePackageIdle(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMemoryTrimLevel() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(190, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getMemoryTrimLevel(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isVrModePackageEnabled(ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(191, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isVrModePackageEnabled(param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyLockedProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(192, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().notifyLockedProfile(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startConfirmDeviceCredentialIntent(Intent param2Intent, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(193, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().startConfirmDeviceCredentialIntent(param2Intent, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendIdleJobTrigger() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(194, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().sendIdleJobTrigger();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int sendIntentSender(IIntentSender param2IIntentSender, IBinder param2IBinder, int param2Int, Intent param2Intent, String param2String1, IIntentReceiver param2IIntentReceiver, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder2;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder1 = null;
          if (param2IIntentSender != null) {
            iBinder2 = param2IIntentSender.asBinder();
          } else {
            iBinder2 = null;
          } 
          parcel1.writeStrongBinder(iBinder2);
          try {
            parcel1.writeStrongBinder(param2IBinder);
            try {
              parcel1.writeInt(param2Int);
              if (param2Intent != null) {
                parcel1.writeInt(1);
                param2Intent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              parcel1.writeString(param2String1);
              iBinder2 = iBinder1;
              if (param2IIntentReceiver != null)
                iBinder2 = param2IIntentReceiver.asBinder(); 
              parcel1.writeStrongBinder(iBinder2);
              parcel1.writeString(param2String2);
              if (param2Bundle != null) {
                parcel1.writeInt(1);
                param2Bundle.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              boolean bool = this.mRemote.transact(195, parcel1, parcel2, 0);
              if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
                param2Int = IActivityManager.Stub.getDefaultImpl().sendIntentSender(param2IIntentSender, param2IBinder, param2Int, param2Intent, param2String1, param2IIntentReceiver, param2String2, param2Bundle);
                parcel2.recycle();
                parcel1.recycle();
                return param2Int;
              } 
              parcel2.readException();
              param2Int = parcel2.readInt();
              parcel2.recycle();
              parcel1.recycle();
              return param2Int;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IIntentSender;
      }
      
      public boolean isBackgroundRestricted(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(196, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isBackgroundRestricted(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRenderThread(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(197, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setRenderThread(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHasTopUi(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(198, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setHasTopUi(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int restartUserInBackground(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(199, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            param2Int = IActivityManager.Stub.getDefaultImpl().restartUserInBackground(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelTaskWindowTransition(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(200, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().cancelTaskWindowTransition(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityManager.TaskSnapshot getTaskSnapshot(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          ActivityManager.TaskSnapshot taskSnapshot;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(201, parcel1, parcel2, 0);
          if (!bool1 && IActivityManager.Stub.getDefaultImpl() != null) {
            taskSnapshot = IActivityManager.Stub.getDefaultImpl().getTaskSnapshot(param2Int, param2Boolean);
            return taskSnapshot;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            taskSnapshot = (ActivityManager.TaskSnapshot)ActivityManager.TaskSnapshot.CREATOR.createFromParcel(parcel2);
          } else {
            taskSnapshot = null;
          } 
          return taskSnapshot;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void scheduleApplicationInfoChanged(List<String> param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeStringList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(202, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().scheduleApplicationInfoChanged(param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPersistentVrThread(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(203, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setPersistentVrThread(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void waitForNetworkStateUpdate(long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(204, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().waitForNetworkStateUpdate(param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void backgroundWhitelistUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(205, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().backgroundWhitelistUid(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startUserInBackgroundWithListener(int param2Int, IProgressListener param2IProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2IProgressListener != null) {
            iBinder = param2IProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(206, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().startUserInBackgroundWithListener(param2Int, param2IProgressListener);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startDelegateShellPermissionIdentity(int param2Int, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(207, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().startDelegateShellPermissionIdentity(param2Int, param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopDelegateShellPermissionIdentity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(208, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().stopDelegateShellPermissionIdentity();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor getLifeMonitor() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParcelFileDescriptor parcelFileDescriptor;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(209, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            parcelFileDescriptor = IActivityManager.Stub.getDefaultImpl().getLifeMonitor();
            return parcelFileDescriptor;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parcelFileDescriptor = (ParcelFileDescriptor)ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            parcelFileDescriptor = null;
          } 
          return parcelFileDescriptor;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean startUserInForegroundWithListener(int param2Int, IProgressListener param2IProgressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          if (param2IProgressListener != null) {
            iBinder = param2IProgressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(210, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().startUserInForegroundWithListener(param2Int, param2IProgressListener);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void appNotResponding(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(211, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().appNotResponding(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice<ApplicationExitInfo> getHistoricalProcessExitReasons(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(212, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null)
            return IActivityManager.Stub.getDefaultImpl().getHistoricalProcessExitReasons(param2String, param2Int1, param2Int2, param2Int3); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice<ApplicationExitInfo>)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void killProcessesWhenImperceptible(int[] param2ArrayOfint, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeIntArray(param2ArrayOfint);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(213, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killProcessesWhenImperceptible(param2ArrayOfint, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActivityLocusContext(ComponentName param2ComponentName, LocusId param2LocusId, IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2LocusId != null) {
            parcel1.writeInt(1);
            param2LocusId.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(214, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setActivityLocusContext(param2ComponentName, param2LocusId, param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setProcessStateSummary(byte[] param2ArrayOfbyte) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          boolean bool = this.mRemote.transact(215, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setProcessStateSummary(param2ArrayOfbyte);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAppFreezerSupported() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(216, parcel1, parcel2, 0);
          if (!bool2 && IActivityManager.Stub.getDefaultImpl() != null) {
            bool1 = IActivityManager.Stub.getDefaultImpl().isAppFreezerSupported();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getTopAppName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          boolean bool = this.mRemote.transact(217, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            componentName = IActivityManager.Stub.getDefaultImpl().getTopAppName();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void detectExceptionsForOIDT(int param2Int, String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(218, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().detectExceptionsForOIDT(param2Int, param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOppoKinectActivityController(IOplusKinectActivityController param2IOplusKinectActivityController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          if (param2IOplusKinectActivityController != null) {
            iBinder = param2IOplusKinectActivityController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(219, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().setOppoKinectActivityController(param2IOplusKinectActivityController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onBackPressedOnTheiaMonitor(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(220, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().onBackPressedOnTheiaMonitor(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendTheiaEvent(long param2Long, Intent param2Intent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeLong(param2Long);
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(221, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().sendTheiaEvent(param2Long, param2Intent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void reportBindApplicationFinished(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.app.IActivityManager");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(222, parcel, null, 1);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().reportBindApplicationFinished(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void killUidForPermissionChange(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.app.IActivityManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(223, parcel1, parcel2, 0);
          if (!bool && IActivityManager.Stub.getDefaultImpl() != null) {
            IActivityManager.Stub.getDefaultImpl().killUidForPermissionChange(param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IActivityManager param1IActivityManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IActivityManager != null) {
          Proxy.sDefaultImpl = param1IActivityManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IActivityManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
