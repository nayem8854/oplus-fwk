package android.app;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.PropertyValuesHolder;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.os.Looper;
import android.os.Parcelable;
import android.util.ArraySet;
import android.util.AttributeSet;
import android.util.DebugUtils;
import android.util.Log;
import android.util.LogWriter;
import android.util.Pair;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import com.android.internal.R;
import com.android.internal.util.FastPrintWriter;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

final class FragmentManagerImpl extends FragmentManager implements LayoutInflater.Factory2 {
  static boolean DEBUG = false;
  
  class AnimateOnHWLayerIfNeededListener implements Animator.AnimatorListener {
    private boolean mShouldRunOnHWLayer = false;
    
    private View mView;
    
    public AnimateOnHWLayerIfNeededListener(FragmentManagerImpl this$0) {
      if (this$0 == null)
        return; 
      this.mView = (View)this$0;
    }
    
    public void onAnimationStart(Animator param1Animator) {
      boolean bool = FragmentManagerImpl.shouldRunOnHWLayer(this.mView, param1Animator);
      if (bool)
        this.mView.setLayerType(2, null); 
    }
    
    public void onAnimationEnd(Animator param1Animator) {
      if (this.mShouldRunOnHWLayer)
        this.mView.setLayerType(0, null); 
      this.mView = null;
      param1Animator.removeListener(this);
    }
    
    public void onAnimationCancel(Animator param1Animator) {}
    
    public void onAnimationRepeat(Animator param1Animator) {}
  }
  
  int mNextFragmentIndex = 0;
  
  final ArrayList<Fragment> mAdded = new ArrayList<>();
  
  final CopyOnWriteArrayList<Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean>> mLifecycleCallbacks = new CopyOnWriteArrayList<>();
  
  int mCurState = 0;
  
  Bundle mStateBundle = null;
  
  SparseArray<Parcelable> mStateArray = null;
  
  Runnable mExecCommit = (Runnable)new Object(this);
  
  static final String TAG = "FragmentManager";
  
  static final String TARGET_REQUEST_CODE_STATE_TAG = "android:target_req_state";
  
  static final String TARGET_STATE_TAG = "android:target_state";
  
  static final String USER_VISIBLE_HINT_TAG = "android:user_visible_hint";
  
  static final String VIEW_STATE_TAG = "android:view_state";
  
  SparseArray<Fragment> mActive;
  
  boolean mAllowOldReentrantBehavior;
  
  ArrayList<Integer> mAvailBackStackIndices;
  
  ArrayList<BackStackRecord> mBackStack;
  
  ArrayList<FragmentManager.OnBackStackChangedListener> mBackStackChangeListeners;
  
  ArrayList<BackStackRecord> mBackStackIndices;
  
  FragmentContainer mContainer;
  
  ArrayList<Fragment> mCreatedMenus;
  
  boolean mDestroyed;
  
  boolean mExecutingActions;
  
  boolean mHavePendingDeferredStart;
  
  FragmentHostCallback<?> mHost;
  
  boolean mNeedMenuInvalidate;
  
  String mNoTransactionsBecause;
  
  Fragment mParent;
  
  ArrayList<OpGenerator> mPendingActions;
  
  ArrayList<StartEnterTransitionListener> mPostponedTransactions;
  
  Fragment mPrimaryNav;
  
  FragmentManagerNonConfig mSavedNonConfig;
  
  boolean mStateSaved;
  
  ArrayList<Fragment> mTmpAddedFragments;
  
  ArrayList<Boolean> mTmpIsPop;
  
  ArrayList<BackStackRecord> mTmpRecords;
  
  private void throwException(RuntimeException paramRuntimeException) {
    Log.e("FragmentManager", paramRuntimeException.getMessage());
    LogWriter logWriter = new LogWriter(6, "FragmentManager");
    FastPrintWriter fastPrintWriter = new FastPrintWriter((Writer)logWriter, false, 1024);
    if (this.mHost != null) {
      Log.e("FragmentManager", "Activity state:");
      try {
        this.mHost.onDump("  ", null, (PrintWriter)fastPrintWriter, new String[0]);
      } catch (Exception exception) {
        fastPrintWriter.flush();
        Log.e("FragmentManager", "Failed dumping state", exception);
      } 
    } else {
      Log.e("FragmentManager", "Fragment manager state:");
      try {
        dump("  ", null, (PrintWriter)fastPrintWriter, new String[0]);
      } catch (Exception exception) {
        fastPrintWriter.flush();
        Log.e("FragmentManager", "Failed dumping state", exception);
      } 
    } 
    fastPrintWriter.flush();
    throw paramRuntimeException;
  }
  
  static boolean modifiesAlpha(Animator paramAnimator) {
    PropertyValuesHolder[] arrayOfPropertyValuesHolder;
    if (paramAnimator == null)
      return false; 
    if (paramAnimator instanceof android.animation.ValueAnimator) {
      paramAnimator = paramAnimator;
      arrayOfPropertyValuesHolder = paramAnimator.getValues();
      for (byte b = 0; b < arrayOfPropertyValuesHolder.length; b++) {
        if ("alpha".equals(arrayOfPropertyValuesHolder[b].getPropertyName()))
          return true; 
      } 
    } else if (arrayOfPropertyValuesHolder instanceof AnimatorSet) {
      ArrayList<Animator> arrayList = ((AnimatorSet)arrayOfPropertyValuesHolder).getChildAnimations();
      for (byte b = 0; b < arrayList.size(); b++) {
        if (modifiesAlpha(arrayList.get(b)))
          return true; 
      } 
    } 
    return false;
  }
  
  static boolean shouldRunOnHWLayer(View paramView, Animator paramAnimator) {
    boolean bool = false;
    if (paramView == null || paramAnimator == null)
      return false; 
    if (paramView.getLayerType() == 0 && 
      paramView.hasOverlappingRendering() && 
      modifiesAlpha(paramAnimator))
      bool = true; 
    return bool;
  }
  
  private void setHWLayerAnimListenerIfAlpha(View paramView, Animator paramAnimator) {
    if (paramView == null || paramAnimator == null)
      return; 
    if (shouldRunOnHWLayer(paramView, paramAnimator))
      paramAnimator.addListener(new AnimateOnHWLayerIfNeededListener(paramView)); 
  }
  
  public FragmentTransaction beginTransaction() {
    return new BackStackRecord(this);
  }
  
  public boolean executePendingTransactions() {
    boolean bool = execPendingActions();
    forcePostponedTransactions();
    return bool;
  }
  
  public void popBackStack() {
    enqueueAction(new PopBackStackState(null, -1, 0), false);
  }
  
  public boolean popBackStackImmediate() {
    checkStateLoss();
    return popBackStackImmediate(null, -1, 0);
  }
  
  public void popBackStack(String paramString, int paramInt) {
    enqueueAction(new PopBackStackState(paramString, -1, paramInt), false);
  }
  
  public boolean popBackStackImmediate(String paramString, int paramInt) {
    checkStateLoss();
    return popBackStackImmediate(paramString, -1, paramInt);
  }
  
  public void popBackStack(int paramInt1, int paramInt2) {
    if (paramInt1 >= 0) {
      enqueueAction(new PopBackStackState(null, paramInt1, paramInt2), false);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad id: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public boolean popBackStackImmediate(int paramInt1, int paramInt2) {
    checkStateLoss();
    if (paramInt1 >= 0)
      return popBackStackImmediate(null, paramInt1, paramInt2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Bad id: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private boolean popBackStackImmediate(String paramString, int paramInt1, int paramInt2) {
    execPendingActions();
    ensureExecReady(true);
    Fragment fragment = this.mPrimaryNav;
    if (fragment != null && paramInt1 < 0 && paramString == null) {
      FragmentManagerImpl fragmentManagerImpl = fragment.mChildFragmentManager;
      if (fragmentManagerImpl != null && fragmentManagerImpl.popBackStackImmediate())
        return true; 
    } 
    boolean bool = popBackStackState(this.mTmpRecords, this.mTmpIsPop, paramString, paramInt1, paramInt2);
    if (bool) {
      this.mExecutingActions = true;
      try {
        removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
      } finally {
        cleanupExec();
      } 
    } 
    doPendingDeferredStart();
    burpActive();
    return bool;
  }
  
  public int getBackStackEntryCount() {
    boolean bool;
    ArrayList<BackStackRecord> arrayList = this.mBackStack;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public FragmentManager.BackStackEntry getBackStackEntryAt(int paramInt) {
    return this.mBackStack.get(paramInt);
  }
  
  public void addOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener paramOnBackStackChangedListener) {
    if (this.mBackStackChangeListeners == null)
      this.mBackStackChangeListeners = new ArrayList<>(); 
    this.mBackStackChangeListeners.add(paramOnBackStackChangedListener);
  }
  
  public void removeOnBackStackChangedListener(FragmentManager.OnBackStackChangedListener paramOnBackStackChangedListener) {
    ArrayList<FragmentManager.OnBackStackChangedListener> arrayList = this.mBackStackChangeListeners;
    if (arrayList != null)
      arrayList.remove(paramOnBackStackChangedListener); 
  }
  
  public void putFragment(Bundle paramBundle, String paramString, Fragment paramFragment) {
    if (paramFragment.mIndex < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fragment ");
      stringBuilder.append(paramFragment);
      stringBuilder.append(" is not currently in the FragmentManager");
      throwException(new IllegalStateException(stringBuilder.toString()));
    } 
    paramBundle.putInt(paramString, paramFragment.mIndex);
  }
  
  public Fragment getFragment(Bundle paramBundle, String paramString) {
    int i = paramBundle.getInt(paramString, -1);
    if (i == -1)
      return null; 
    Fragment fragment = (Fragment)this.mActive.get(i);
    if (fragment == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fragment no longer exists for key ");
      stringBuilder.append(paramString);
      stringBuilder.append(": index ");
      stringBuilder.append(i);
      throwException(new IllegalStateException(stringBuilder.toString()));
    } 
    return fragment;
  }
  
  public List<Fragment> getFragments() {
    if (this.mAdded.isEmpty())
      return Collections.EMPTY_LIST; 
    synchronized (this.mAdded) {
      return (List)this.mAdded.clone();
    } 
  }
  
  public Fragment.SavedState saveFragmentInstanceState(Fragment paramFragment) {
    if (paramFragment.mIndex < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Fragment ");
      stringBuilder.append(paramFragment);
      stringBuilder.append(" is not currently in the FragmentManager");
      throwException(new IllegalStateException(stringBuilder.toString()));
    } 
    int i = paramFragment.mState;
    Fragment fragment = null;
    if (i > 0) {
      Fragment.SavedState savedState;
      Bundle bundle = saveFragmentBasicState(paramFragment);
      paramFragment = fragment;
      if (bundle != null)
        savedState = new Fragment.SavedState(bundle); 
      return savedState;
    } 
    return null;
  }
  
  public boolean isDestroyed() {
    return this.mDestroyed;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("FragmentManager{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" in ");
    Fragment fragment = this.mParent;
    if (fragment != null) {
      DebugUtils.buildShortClassTag(fragment, stringBuilder);
    } else {
      DebugUtils.buildShortClassTag(this.mHost, stringBuilder);
    } 
    stringBuilder.append("}}");
    return stringBuilder.toString();
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    // Byte code:
    //   0: new java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore #5
    //   9: aload #5
    //   11: aload_1
    //   12: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   15: pop
    //   16: aload #5
    //   18: ldc_w '    '
    //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: pop
    //   25: aload #5
    //   27: invokevirtual toString : ()Ljava/lang/String;
    //   30: astore #5
    //   32: aload_0
    //   33: getfield mActive : Landroid/util/SparseArray;
    //   36: astore #6
    //   38: aload #6
    //   40: ifnull -> 162
    //   43: aload #6
    //   45: invokevirtual size : ()I
    //   48: istore #7
    //   50: iload #7
    //   52: ifle -> 162
    //   55: aload_3
    //   56: aload_1
    //   57: invokevirtual print : (Ljava/lang/String;)V
    //   60: aload_3
    //   61: ldc_w 'Active Fragments in '
    //   64: invokevirtual print : (Ljava/lang/String;)V
    //   67: aload_3
    //   68: aload_0
    //   69: invokestatic identityHashCode : (Ljava/lang/Object;)I
    //   72: invokestatic toHexString : (I)Ljava/lang/String;
    //   75: invokevirtual print : (Ljava/lang/String;)V
    //   78: aload_3
    //   79: ldc_w ':'
    //   82: invokevirtual println : (Ljava/lang/String;)V
    //   85: iconst_0
    //   86: istore #8
    //   88: iload #8
    //   90: iload #7
    //   92: if_icmpge -> 162
    //   95: aload_0
    //   96: getfield mActive : Landroid/util/SparseArray;
    //   99: iload #8
    //   101: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   104: checkcast android/app/Fragment
    //   107: astore #6
    //   109: aload_3
    //   110: aload_1
    //   111: invokevirtual print : (Ljava/lang/String;)V
    //   114: aload_3
    //   115: ldc_w '  #'
    //   118: invokevirtual print : (Ljava/lang/String;)V
    //   121: aload_3
    //   122: iload #8
    //   124: invokevirtual print : (I)V
    //   127: aload_3
    //   128: ldc_w ': '
    //   131: invokevirtual print : (Ljava/lang/String;)V
    //   134: aload_3
    //   135: aload #6
    //   137: invokevirtual println : (Ljava/lang/Object;)V
    //   140: aload #6
    //   142: ifnull -> 156
    //   145: aload #6
    //   147: aload #5
    //   149: aload_2
    //   150: aload_3
    //   151: aload #4
    //   153: invokevirtual dump : (Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    //   156: iinc #8, 1
    //   159: goto -> 88
    //   162: aload_0
    //   163: getfield mAdded : Ljava/util/ArrayList;
    //   166: invokevirtual size : ()I
    //   169: istore #7
    //   171: iload #7
    //   173: ifle -> 252
    //   176: aload_3
    //   177: aload_1
    //   178: invokevirtual print : (Ljava/lang/String;)V
    //   181: aload_3
    //   182: ldc_w 'Added Fragments:'
    //   185: invokevirtual println : (Ljava/lang/String;)V
    //   188: iconst_0
    //   189: istore #8
    //   191: iload #8
    //   193: iload #7
    //   195: if_icmpge -> 252
    //   198: aload_0
    //   199: getfield mAdded : Ljava/util/ArrayList;
    //   202: iload #8
    //   204: invokevirtual get : (I)Ljava/lang/Object;
    //   207: checkcast android/app/Fragment
    //   210: astore #6
    //   212: aload_3
    //   213: aload_1
    //   214: invokevirtual print : (Ljava/lang/String;)V
    //   217: aload_3
    //   218: ldc_w '  #'
    //   221: invokevirtual print : (Ljava/lang/String;)V
    //   224: aload_3
    //   225: iload #8
    //   227: invokevirtual print : (I)V
    //   230: aload_3
    //   231: ldc_w ': '
    //   234: invokevirtual print : (Ljava/lang/String;)V
    //   237: aload_3
    //   238: aload #6
    //   240: invokevirtual toString : ()Ljava/lang/String;
    //   243: invokevirtual println : (Ljava/lang/String;)V
    //   246: iinc #8, 1
    //   249: goto -> 191
    //   252: aload_0
    //   253: getfield mCreatedMenus : Ljava/util/ArrayList;
    //   256: astore #6
    //   258: aload #6
    //   260: ifnull -> 351
    //   263: aload #6
    //   265: invokevirtual size : ()I
    //   268: istore #7
    //   270: iload #7
    //   272: ifle -> 351
    //   275: aload_3
    //   276: aload_1
    //   277: invokevirtual print : (Ljava/lang/String;)V
    //   280: aload_3
    //   281: ldc_w 'Fragments Created Menus:'
    //   284: invokevirtual println : (Ljava/lang/String;)V
    //   287: iconst_0
    //   288: istore #8
    //   290: iload #8
    //   292: iload #7
    //   294: if_icmpge -> 351
    //   297: aload_0
    //   298: getfield mCreatedMenus : Ljava/util/ArrayList;
    //   301: iload #8
    //   303: invokevirtual get : (I)Ljava/lang/Object;
    //   306: checkcast android/app/Fragment
    //   309: astore #6
    //   311: aload_3
    //   312: aload_1
    //   313: invokevirtual print : (Ljava/lang/String;)V
    //   316: aload_3
    //   317: ldc_w '  #'
    //   320: invokevirtual print : (Ljava/lang/String;)V
    //   323: aload_3
    //   324: iload #8
    //   326: invokevirtual print : (I)V
    //   329: aload_3
    //   330: ldc_w ': '
    //   333: invokevirtual print : (Ljava/lang/String;)V
    //   336: aload_3
    //   337: aload #6
    //   339: invokevirtual toString : ()Ljava/lang/String;
    //   342: invokevirtual println : (Ljava/lang/String;)V
    //   345: iinc #8, 1
    //   348: goto -> 290
    //   351: aload_0
    //   352: getfield mBackStack : Ljava/util/ArrayList;
    //   355: astore #6
    //   357: aload #6
    //   359: ifnull -> 461
    //   362: aload #6
    //   364: invokevirtual size : ()I
    //   367: istore #7
    //   369: iload #7
    //   371: ifle -> 461
    //   374: aload_3
    //   375: aload_1
    //   376: invokevirtual print : (Ljava/lang/String;)V
    //   379: aload_3
    //   380: ldc_w 'Back Stack:'
    //   383: invokevirtual println : (Ljava/lang/String;)V
    //   386: iconst_0
    //   387: istore #8
    //   389: iload #8
    //   391: iload #7
    //   393: if_icmpge -> 461
    //   396: aload_0
    //   397: getfield mBackStack : Ljava/util/ArrayList;
    //   400: iload #8
    //   402: invokevirtual get : (I)Ljava/lang/Object;
    //   405: checkcast android/app/BackStackRecord
    //   408: astore #6
    //   410: aload_3
    //   411: aload_1
    //   412: invokevirtual print : (Ljava/lang/String;)V
    //   415: aload_3
    //   416: ldc_w '  #'
    //   419: invokevirtual print : (Ljava/lang/String;)V
    //   422: aload_3
    //   423: iload #8
    //   425: invokevirtual print : (I)V
    //   428: aload_3
    //   429: ldc_w ': '
    //   432: invokevirtual print : (Ljava/lang/String;)V
    //   435: aload_3
    //   436: aload #6
    //   438: invokevirtual toString : ()Ljava/lang/String;
    //   441: invokevirtual println : (Ljava/lang/String;)V
    //   444: aload #6
    //   446: aload #5
    //   448: aload_2
    //   449: aload_3
    //   450: aload #4
    //   452: invokevirtual dump : (Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    //   455: iinc #8, 1
    //   458: goto -> 389
    //   461: aload_0
    //   462: monitorenter
    //   463: aload_0
    //   464: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   467: ifnull -> 555
    //   470: aload_0
    //   471: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   474: invokevirtual size : ()I
    //   477: istore #7
    //   479: iload #7
    //   481: ifle -> 555
    //   484: aload_3
    //   485: aload_1
    //   486: invokevirtual print : (Ljava/lang/String;)V
    //   489: aload_3
    //   490: ldc_w 'Back Stack Indices:'
    //   493: invokevirtual println : (Ljava/lang/String;)V
    //   496: iconst_0
    //   497: istore #8
    //   499: iload #8
    //   501: iload #7
    //   503: if_icmpge -> 555
    //   506: aload_0
    //   507: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   510: iload #8
    //   512: invokevirtual get : (I)Ljava/lang/Object;
    //   515: checkcast android/app/BackStackRecord
    //   518: astore_2
    //   519: aload_3
    //   520: aload_1
    //   521: invokevirtual print : (Ljava/lang/String;)V
    //   524: aload_3
    //   525: ldc_w '  #'
    //   528: invokevirtual print : (Ljava/lang/String;)V
    //   531: aload_3
    //   532: iload #8
    //   534: invokevirtual print : (I)V
    //   537: aload_3
    //   538: ldc_w ': '
    //   541: invokevirtual print : (Ljava/lang/String;)V
    //   544: aload_3
    //   545: aload_2
    //   546: invokevirtual println : (Ljava/lang/Object;)V
    //   549: iinc #8, 1
    //   552: goto -> 499
    //   555: aload_0
    //   556: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   559: ifnull -> 598
    //   562: aload_0
    //   563: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   566: invokevirtual size : ()I
    //   569: ifle -> 598
    //   572: aload_3
    //   573: aload_1
    //   574: invokevirtual print : (Ljava/lang/String;)V
    //   577: aload_3
    //   578: ldc_w 'mAvailBackStackIndices: '
    //   581: invokevirtual print : (Ljava/lang/String;)V
    //   584: aload_3
    //   585: aload_0
    //   586: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   589: invokevirtual toArray : ()[Ljava/lang/Object;
    //   592: invokestatic toString : ([Ljava/lang/Object;)Ljava/lang/String;
    //   595: invokevirtual println : (Ljava/lang/String;)V
    //   598: aload_0
    //   599: monitorexit
    //   600: aload_0
    //   601: getfield mPendingActions : Ljava/util/ArrayList;
    //   604: astore_2
    //   605: aload_2
    //   606: ifnull -> 691
    //   609: aload_2
    //   610: invokevirtual size : ()I
    //   613: istore #7
    //   615: iload #7
    //   617: ifle -> 691
    //   620: aload_3
    //   621: aload_1
    //   622: invokevirtual print : (Ljava/lang/String;)V
    //   625: aload_3
    //   626: ldc_w 'Pending Actions:'
    //   629: invokevirtual println : (Ljava/lang/String;)V
    //   632: iconst_0
    //   633: istore #8
    //   635: iload #8
    //   637: iload #7
    //   639: if_icmpge -> 691
    //   642: aload_0
    //   643: getfield mPendingActions : Ljava/util/ArrayList;
    //   646: iload #8
    //   648: invokevirtual get : (I)Ljava/lang/Object;
    //   651: checkcast android/app/FragmentManagerImpl$OpGenerator
    //   654: astore_2
    //   655: aload_3
    //   656: aload_1
    //   657: invokevirtual print : (Ljava/lang/String;)V
    //   660: aload_3
    //   661: ldc_w '  #'
    //   664: invokevirtual print : (Ljava/lang/String;)V
    //   667: aload_3
    //   668: iload #8
    //   670: invokevirtual print : (I)V
    //   673: aload_3
    //   674: ldc_w ': '
    //   677: invokevirtual print : (Ljava/lang/String;)V
    //   680: aload_3
    //   681: aload_2
    //   682: invokevirtual println : (Ljava/lang/Object;)V
    //   685: iinc #8, 1
    //   688: goto -> 635
    //   691: aload_3
    //   692: aload_1
    //   693: invokevirtual print : (Ljava/lang/String;)V
    //   696: aload_3
    //   697: ldc_w 'FragmentManager misc state:'
    //   700: invokevirtual println : (Ljava/lang/String;)V
    //   703: aload_3
    //   704: aload_1
    //   705: invokevirtual print : (Ljava/lang/String;)V
    //   708: aload_3
    //   709: ldc_w '  mHost='
    //   712: invokevirtual print : (Ljava/lang/String;)V
    //   715: aload_3
    //   716: aload_0
    //   717: getfield mHost : Landroid/app/FragmentHostCallback;
    //   720: invokevirtual println : (Ljava/lang/Object;)V
    //   723: aload_3
    //   724: aload_1
    //   725: invokevirtual print : (Ljava/lang/String;)V
    //   728: aload_3
    //   729: ldc_w '  mContainer='
    //   732: invokevirtual print : (Ljava/lang/String;)V
    //   735: aload_3
    //   736: aload_0
    //   737: getfield mContainer : Landroid/app/FragmentContainer;
    //   740: invokevirtual println : (Ljava/lang/Object;)V
    //   743: aload_0
    //   744: getfield mParent : Landroid/app/Fragment;
    //   747: ifnull -> 770
    //   750: aload_3
    //   751: aload_1
    //   752: invokevirtual print : (Ljava/lang/String;)V
    //   755: aload_3
    //   756: ldc_w '  mParent='
    //   759: invokevirtual print : (Ljava/lang/String;)V
    //   762: aload_3
    //   763: aload_0
    //   764: getfield mParent : Landroid/app/Fragment;
    //   767: invokevirtual println : (Ljava/lang/Object;)V
    //   770: aload_3
    //   771: aload_1
    //   772: invokevirtual print : (Ljava/lang/String;)V
    //   775: aload_3
    //   776: ldc_w '  mCurState='
    //   779: invokevirtual print : (Ljava/lang/String;)V
    //   782: aload_3
    //   783: aload_0
    //   784: getfield mCurState : I
    //   787: invokevirtual print : (I)V
    //   790: aload_3
    //   791: ldc_w ' mStateSaved='
    //   794: invokevirtual print : (Ljava/lang/String;)V
    //   797: aload_3
    //   798: aload_0
    //   799: getfield mStateSaved : Z
    //   802: invokevirtual print : (Z)V
    //   805: aload_3
    //   806: ldc_w ' mDestroyed='
    //   809: invokevirtual print : (Ljava/lang/String;)V
    //   812: aload_3
    //   813: aload_0
    //   814: getfield mDestroyed : Z
    //   817: invokevirtual println : (Z)V
    //   820: aload_0
    //   821: getfield mNeedMenuInvalidate : Z
    //   824: ifeq -> 847
    //   827: aload_3
    //   828: aload_1
    //   829: invokevirtual print : (Ljava/lang/String;)V
    //   832: aload_3
    //   833: ldc_w '  mNeedMenuInvalidate='
    //   836: invokevirtual print : (Ljava/lang/String;)V
    //   839: aload_3
    //   840: aload_0
    //   841: getfield mNeedMenuInvalidate : Z
    //   844: invokevirtual println : (Z)V
    //   847: aload_0
    //   848: getfield mNoTransactionsBecause : Ljava/lang/String;
    //   851: ifnull -> 874
    //   854: aload_3
    //   855: aload_1
    //   856: invokevirtual print : (Ljava/lang/String;)V
    //   859: aload_3
    //   860: ldc_w '  mNoTransactionsBecause='
    //   863: invokevirtual print : (Ljava/lang/String;)V
    //   866: aload_3
    //   867: aload_0
    //   868: getfield mNoTransactionsBecause : Ljava/lang/String;
    //   871: invokevirtual println : (Ljava/lang/String;)V
    //   874: return
    //   875: astore_1
    //   876: aload_0
    //   877: monitorexit
    //   878: aload_1
    //   879: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #995	-> 0
    //   #998	-> 32
    //   #999	-> 43
    //   #1000	-> 50
    //   #1001	-> 55
    //   #1002	-> 67
    //   #1003	-> 78
    //   #1004	-> 85
    //   #1005	-> 95
    //   #1006	-> 109
    //   #1007	-> 127
    //   #1008	-> 140
    //   #1009	-> 145
    //   #1004	-> 156
    //   #1015	-> 162
    //   #1016	-> 171
    //   #1017	-> 176
    //   #1018	-> 181
    //   #1019	-> 188
    //   #1020	-> 198
    //   #1021	-> 212
    //   #1022	-> 217
    //   #1023	-> 224
    //   #1024	-> 230
    //   #1025	-> 237
    //   #1019	-> 246
    //   #1029	-> 252
    //   #1030	-> 263
    //   #1031	-> 270
    //   #1032	-> 275
    //   #1033	-> 287
    //   #1034	-> 297
    //   #1035	-> 311
    //   #1036	-> 329
    //   #1033	-> 345
    //   #1041	-> 351
    //   #1042	-> 362
    //   #1043	-> 369
    //   #1044	-> 374
    //   #1045	-> 386
    //   #1046	-> 396
    //   #1047	-> 410
    //   #1048	-> 428
    //   #1049	-> 444
    //   #1045	-> 455
    //   #1054	-> 461
    //   #1055	-> 463
    //   #1056	-> 470
    //   #1057	-> 479
    //   #1058	-> 484
    //   #1059	-> 496
    //   #1060	-> 506
    //   #1061	-> 519
    //   #1062	-> 537
    //   #1059	-> 549
    //   #1067	-> 555
    //   #1068	-> 572
    //   #1069	-> 584
    //   #1071	-> 598
    //   #1073	-> 600
    //   #1074	-> 609
    //   #1075	-> 615
    //   #1076	-> 620
    //   #1077	-> 632
    //   #1078	-> 642
    //   #1079	-> 655
    //   #1080	-> 673
    //   #1077	-> 685
    //   #1085	-> 691
    //   #1086	-> 703
    //   #1087	-> 723
    //   #1088	-> 743
    //   #1089	-> 750
    //   #1091	-> 770
    //   #1092	-> 790
    //   #1093	-> 805
    //   #1094	-> 820
    //   #1095	-> 827
    //   #1096	-> 839
    //   #1098	-> 847
    //   #1099	-> 854
    //   #1100	-> 866
    //   #1102	-> 874
    //   #1071	-> 875
    // Exception table:
    //   from	to	target	type
    //   463	470	875	finally
    //   470	479	875	finally
    //   484	496	875	finally
    //   506	519	875	finally
    //   519	537	875	finally
    //   537	549	875	finally
    //   555	572	875	finally
    //   572	584	875	finally
    //   584	598	875	finally
    //   598	600	875	finally
    //   876	878	875	finally
  }
  
  Animator loadAnimator(Fragment paramFragment, int paramInt1, boolean paramBoolean, int paramInt2) {
    Animator animator = paramFragment.onCreateAnimator(paramInt1, paramBoolean, paramFragment.getNextAnim());
    if (animator != null)
      return animator; 
    if (paramFragment.getNextAnim() != 0) {
      Context context = this.mHost.getContext();
      int j = paramFragment.getNextAnim();
      Animator animator1 = AnimatorInflater.loadAnimator(context, j);
      if (animator1 != null)
        return animator1; 
    } 
    if (paramInt1 == 0)
      return null; 
    int i = transitToStyleIndex(paramInt1, paramBoolean);
    if (i < 0)
      return null; 
    paramInt1 = paramInt2;
    if (paramInt2 == 0) {
      paramInt1 = paramInt2;
      if (this.mHost.onHasWindowAnimations())
        paramInt1 = this.mHost.onGetWindowAnimations(); 
    } 
    if (paramInt1 == 0)
      return null; 
    TypedArray typedArray = this.mHost.getContext().obtainStyledAttributes(paramInt1, R.styleable.FragmentAnimation);
    paramInt1 = typedArray.getResourceId(i, 0);
    typedArray.recycle();
    if (paramInt1 == 0)
      return null; 
    return AnimatorInflater.loadAnimator(this.mHost.getContext(), paramInt1);
  }
  
  public void performPendingDeferredStart(Fragment paramFragment) {
    if (paramFragment.mDeferStart) {
      if (this.mExecutingActions) {
        this.mHavePendingDeferredStart = true;
        return;
      } 
      paramFragment.mDeferStart = false;
      moveToState(paramFragment, this.mCurState, 0, 0, false);
    } 
  }
  
  boolean isStateAtLeast(int paramInt) {
    boolean bool;
    if (this.mCurState >= paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  void moveToState(Fragment paramFragment, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: getfield mAdded : Z
    //   4: istore #6
    //   6: iconst_1
    //   7: istore #7
    //   9: iload #6
    //   11: ifeq -> 27
    //   14: aload_1
    //   15: getfield mDetached : Z
    //   18: ifeq -> 24
    //   21: goto -> 27
    //   24: goto -> 41
    //   27: iload_2
    //   28: istore #8
    //   30: iload #8
    //   32: istore_2
    //   33: iload #8
    //   35: iconst_1
    //   36: if_icmple -> 41
    //   39: iconst_1
    //   40: istore_2
    //   41: iload_2
    //   42: istore #8
    //   44: aload_1
    //   45: getfield mRemoving : Z
    //   48: ifeq -> 88
    //   51: iload_2
    //   52: istore #8
    //   54: iload_2
    //   55: aload_1
    //   56: getfield mState : I
    //   59: if_icmple -> 88
    //   62: aload_1
    //   63: getfield mState : I
    //   66: ifne -> 82
    //   69: aload_1
    //   70: invokevirtual isInBackStack : ()Z
    //   73: ifeq -> 82
    //   76: iconst_1
    //   77: istore #8
    //   79: goto -> 88
    //   82: aload_1
    //   83: getfield mState : I
    //   86: istore #8
    //   88: iload #8
    //   90: istore_2
    //   91: aload_1
    //   92: getfield mDeferStart : Z
    //   95: ifeq -> 120
    //   98: iload #8
    //   100: istore_2
    //   101: aload_1
    //   102: getfield mState : I
    //   105: iconst_4
    //   106: if_icmpge -> 120
    //   109: iload #8
    //   111: istore_2
    //   112: iload #8
    //   114: iconst_3
    //   115: if_icmple -> 120
    //   118: iconst_3
    //   119: istore_2
    //   120: aload_1
    //   121: getfield mState : I
    //   124: iload_2
    //   125: if_icmpgt -> 1307
    //   128: aload_1
    //   129: getfield mFromLayout : Z
    //   132: ifeq -> 143
    //   135: aload_1
    //   136: getfield mInLayout : Z
    //   139: ifne -> 143
    //   142: return
    //   143: aload_1
    //   144: invokevirtual getAnimatingAway : ()Landroid/animation/Animator;
    //   147: ifnull -> 167
    //   150: aload_1
    //   151: aconst_null
    //   152: invokevirtual setAnimatingAway : (Landroid/animation/Animator;)V
    //   155: aload_0
    //   156: aload_1
    //   157: aload_1
    //   158: invokevirtual getStateAfterAnimating : ()I
    //   161: iconst_0
    //   162: iconst_0
    //   163: iconst_1
    //   164: invokevirtual moveToState : (Landroid/app/Fragment;IIIZ)V
    //   167: aload_1
    //   168: getfield mState : I
    //   171: istore #8
    //   173: iload #8
    //   175: ifeq -> 212
    //   178: iload #8
    //   180: iconst_1
    //   181: if_icmpeq -> 711
    //   184: iload_2
    //   185: istore_3
    //   186: iload #8
    //   188: iconst_2
    //   189: if_icmpeq -> 1151
    //   192: iload_2
    //   193: istore #4
    //   195: iload #8
    //   197: iconst_3
    //   198: if_icmpeq -> 1167
    //   201: iload_2
    //   202: istore_3
    //   203: iload #8
    //   205: iconst_4
    //   206: if_icmpeq -> 1231
    //   209: goto -> 1302
    //   212: iload_2
    //   213: ifle -> 711
    //   216: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   219: ifeq -> 258
    //   222: new java/lang/StringBuilder
    //   225: dup
    //   226: invokespecial <init> : ()V
    //   229: astore #9
    //   231: aload #9
    //   233: ldc_w 'moveto CREATED: '
    //   236: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   239: pop
    //   240: aload #9
    //   242: aload_1
    //   243: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   246: pop
    //   247: ldc 'FragmentManager'
    //   249: aload #9
    //   251: invokevirtual toString : ()Ljava/lang/String;
    //   254: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   257: pop
    //   258: iload_2
    //   259: istore_3
    //   260: aload_1
    //   261: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   264: ifnull -> 352
    //   267: aload_1
    //   268: aload_1
    //   269: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   272: ldc 'android:view_state'
    //   274: invokevirtual getSparseParcelableArray : (Ljava/lang/String;)Landroid/util/SparseArray;
    //   277: putfield mSavedViewState : Landroid/util/SparseArray;
    //   280: aload_1
    //   281: aload_0
    //   282: aload_1
    //   283: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   286: ldc 'android:target_state'
    //   288: invokevirtual getFragment : (Landroid/os/Bundle;Ljava/lang/String;)Landroid/app/Fragment;
    //   291: putfield mTarget : Landroid/app/Fragment;
    //   294: aload_1
    //   295: getfield mTarget : Landroid/app/Fragment;
    //   298: ifnull -> 315
    //   301: aload_1
    //   302: aload_1
    //   303: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   306: ldc 'android:target_req_state'
    //   308: iconst_0
    //   309: invokevirtual getInt : (Ljava/lang/String;I)I
    //   312: putfield mTargetRequestCode : I
    //   315: aload_1
    //   316: aload_1
    //   317: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   320: ldc 'android:user_visible_hint'
    //   322: iconst_1
    //   323: invokevirtual getBoolean : (Ljava/lang/String;Z)Z
    //   326: putfield mUserVisibleHint : Z
    //   329: iload_2
    //   330: istore_3
    //   331: aload_1
    //   332: getfield mUserVisibleHint : Z
    //   335: ifne -> 352
    //   338: aload_1
    //   339: iconst_1
    //   340: putfield mDeferStart : Z
    //   343: iload_2
    //   344: istore_3
    //   345: iload_2
    //   346: iconst_3
    //   347: if_icmple -> 352
    //   350: iconst_3
    //   351: istore_3
    //   352: aload_1
    //   353: aload_0
    //   354: getfield mHost : Landroid/app/FragmentHostCallback;
    //   357: putfield mHost : Landroid/app/FragmentHostCallback;
    //   360: aload_1
    //   361: aload_0
    //   362: getfield mParent : Landroid/app/Fragment;
    //   365: putfield mParentFragment : Landroid/app/Fragment;
    //   368: aload_0
    //   369: getfield mParent : Landroid/app/Fragment;
    //   372: astore #9
    //   374: aload #9
    //   376: ifnull -> 389
    //   379: aload #9
    //   381: getfield mChildFragmentManager : Landroid/app/FragmentManagerImpl;
    //   384: astore #9
    //   386: goto -> 398
    //   389: aload_0
    //   390: getfield mHost : Landroid/app/FragmentHostCallback;
    //   393: invokevirtual getFragmentManagerImpl : ()Landroid/app/FragmentManagerImpl;
    //   396: astore #9
    //   398: aload_1
    //   399: aload #9
    //   401: putfield mFragmentManager : Landroid/app/FragmentManagerImpl;
    //   404: aload_1
    //   405: getfield mTarget : Landroid/app/Fragment;
    //   408: ifnull -> 527
    //   411: aload_0
    //   412: getfield mActive : Landroid/util/SparseArray;
    //   415: aload_1
    //   416: getfield mTarget : Landroid/app/Fragment;
    //   419: getfield mIndex : I
    //   422: invokevirtual get : (I)Ljava/lang/Object;
    //   425: aload_1
    //   426: getfield mTarget : Landroid/app/Fragment;
    //   429: if_acmpne -> 461
    //   432: aload_1
    //   433: getfield mTarget : Landroid/app/Fragment;
    //   436: getfield mState : I
    //   439: iconst_1
    //   440: if_icmpge -> 458
    //   443: aload_0
    //   444: aload_1
    //   445: getfield mTarget : Landroid/app/Fragment;
    //   448: iconst_1
    //   449: iconst_0
    //   450: iconst_0
    //   451: iconst_1
    //   452: invokevirtual moveToState : (Landroid/app/Fragment;IIIZ)V
    //   455: goto -> 527
    //   458: goto -> 527
    //   461: new java/lang/StringBuilder
    //   464: dup
    //   465: invokespecial <init> : ()V
    //   468: astore #9
    //   470: aload #9
    //   472: ldc_w 'Fragment '
    //   475: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   478: pop
    //   479: aload #9
    //   481: aload_1
    //   482: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   485: pop
    //   486: aload #9
    //   488: ldc_w ' declared target fragment '
    //   491: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   494: pop
    //   495: aload #9
    //   497: aload_1
    //   498: getfield mTarget : Landroid/app/Fragment;
    //   501: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   504: pop
    //   505: aload #9
    //   507: ldc_w ' that does not belong to this FragmentManager!'
    //   510: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   513: pop
    //   514: new java/lang/IllegalStateException
    //   517: dup
    //   518: aload #9
    //   520: invokevirtual toString : ()Ljava/lang/String;
    //   523: invokespecial <init> : (Ljava/lang/String;)V
    //   526: athrow
    //   527: aload_0
    //   528: aload_1
    //   529: aload_0
    //   530: getfield mHost : Landroid/app/FragmentHostCallback;
    //   533: invokevirtual getContext : ()Landroid/content/Context;
    //   536: iconst_0
    //   537: invokevirtual dispatchOnFragmentPreAttached : (Landroid/app/Fragment;Landroid/content/Context;Z)V
    //   540: aload_1
    //   541: iconst_0
    //   542: putfield mCalled : Z
    //   545: aload_1
    //   546: aload_0
    //   547: getfield mHost : Landroid/app/FragmentHostCallback;
    //   550: invokevirtual getContext : ()Landroid/content/Context;
    //   553: invokevirtual onAttach : (Landroid/content/Context;)V
    //   556: aload_1
    //   557: getfield mCalled : Z
    //   560: ifeq -> 664
    //   563: aload_1
    //   564: getfield mParentFragment : Landroid/app/Fragment;
    //   567: ifnonnull -> 581
    //   570: aload_0
    //   571: getfield mHost : Landroid/app/FragmentHostCallback;
    //   574: aload_1
    //   575: invokevirtual onAttachFragment : (Landroid/app/Fragment;)V
    //   578: goto -> 589
    //   581: aload_1
    //   582: getfield mParentFragment : Landroid/app/Fragment;
    //   585: aload_1
    //   586: invokevirtual onAttachFragment : (Landroid/app/Fragment;)V
    //   589: aload_0
    //   590: aload_1
    //   591: aload_0
    //   592: getfield mHost : Landroid/app/FragmentHostCallback;
    //   595: invokevirtual getContext : ()Landroid/content/Context;
    //   598: iconst_0
    //   599: invokevirtual dispatchOnFragmentAttached : (Landroid/app/Fragment;Landroid/content/Context;Z)V
    //   602: aload_1
    //   603: getfield mIsCreated : Z
    //   606: ifne -> 640
    //   609: aload_0
    //   610: aload_1
    //   611: aload_1
    //   612: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   615: iconst_0
    //   616: invokevirtual dispatchOnFragmentPreCreated : (Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    //   619: aload_1
    //   620: aload_1
    //   621: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   624: invokevirtual performCreate : (Landroid/os/Bundle;)V
    //   627: aload_0
    //   628: aload_1
    //   629: aload_1
    //   630: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   633: iconst_0
    //   634: invokevirtual dispatchOnFragmentCreated : (Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    //   637: goto -> 654
    //   640: aload_1
    //   641: aload_1
    //   642: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   645: iconst_1
    //   646: invokevirtual restoreChildFragmentState : (Landroid/os/Bundle;Z)V
    //   649: aload_1
    //   650: iconst_1
    //   651: putfield mState : I
    //   654: aload_1
    //   655: iconst_0
    //   656: putfield mRetaining : Z
    //   659: iload_3
    //   660: istore_2
    //   661: goto -> 711
    //   664: new java/lang/StringBuilder
    //   667: dup
    //   668: invokespecial <init> : ()V
    //   671: astore #9
    //   673: aload #9
    //   675: ldc_w 'Fragment '
    //   678: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   681: pop
    //   682: aload #9
    //   684: aload_1
    //   685: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   688: pop
    //   689: aload #9
    //   691: ldc_w ' did not call through to super.onAttach()'
    //   694: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   697: pop
    //   698: new android/util/SuperNotCalledException
    //   701: dup
    //   702: aload #9
    //   704: invokevirtual toString : ()Ljava/lang/String;
    //   707: invokespecial <init> : (Ljava/lang/String;)V
    //   710: athrow
    //   711: aload_0
    //   712: aload_1
    //   713: invokevirtual ensureInflatedFragmentView : (Landroid/app/Fragment;)V
    //   716: iload_2
    //   717: iconst_1
    //   718: if_icmple -> 1149
    //   721: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   724: ifeq -> 763
    //   727: new java/lang/StringBuilder
    //   730: dup
    //   731: invokespecial <init> : ()V
    //   734: astore #9
    //   736: aload #9
    //   738: ldc_w 'moveto ACTIVITY_CREATED: '
    //   741: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   744: pop
    //   745: aload #9
    //   747: aload_1
    //   748: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   751: pop
    //   752: ldc 'FragmentManager'
    //   754: aload #9
    //   756: invokevirtual toString : ()Ljava/lang/String;
    //   759: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   762: pop
    //   763: aload_1
    //   764: getfield mFromLayout : Z
    //   767: ifne -> 1111
    //   770: aconst_null
    //   771: astore #9
    //   773: aload_1
    //   774: getfield mContainerId : I
    //   777: ifeq -> 979
    //   780: aload_1
    //   781: getfield mContainerId : I
    //   784: iconst_m1
    //   785: if_icmpne -> 838
    //   788: new java/lang/StringBuilder
    //   791: dup
    //   792: invokespecial <init> : ()V
    //   795: astore #9
    //   797: aload #9
    //   799: ldc_w 'Cannot create fragment '
    //   802: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   805: pop
    //   806: aload #9
    //   808: aload_1
    //   809: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   812: pop
    //   813: aload #9
    //   815: ldc_w ' for a container view with no id'
    //   818: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   821: pop
    //   822: aload_0
    //   823: new java/lang/IllegalArgumentException
    //   826: dup
    //   827: aload #9
    //   829: invokevirtual toString : ()Ljava/lang/String;
    //   832: invokespecial <init> : (Ljava/lang/String;)V
    //   835: invokespecial throwException : (Ljava/lang/RuntimeException;)V
    //   838: aload_0
    //   839: getfield mContainer : Landroid/app/FragmentContainer;
    //   842: aload_1
    //   843: getfield mContainerId : I
    //   846: invokevirtual onFindViewById : (I)Landroid/view/View;
    //   849: checkcast android/view/ViewGroup
    //   852: astore #10
    //   854: aload #10
    //   856: ifnonnull -> 975
    //   859: aload_1
    //   860: getfield mRestored : Z
    //   863: ifne -> 975
    //   866: aload_1
    //   867: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   870: aload_1
    //   871: getfield mContainerId : I
    //   874: invokevirtual getResourceName : (I)Ljava/lang/String;
    //   877: astore #9
    //   879: goto -> 889
    //   882: astore #9
    //   884: ldc_w 'unknown'
    //   887: astore #9
    //   889: new java/lang/StringBuilder
    //   892: dup
    //   893: invokespecial <init> : ()V
    //   896: astore #11
    //   898: aload #11
    //   900: ldc_w 'No view found for id 0x'
    //   903: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   906: pop
    //   907: aload_1
    //   908: getfield mContainerId : I
    //   911: istore_3
    //   912: aload #11
    //   914: iload_3
    //   915: invokestatic toHexString : (I)Ljava/lang/String;
    //   918: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   921: pop
    //   922: aload #11
    //   924: ldc_w ' ('
    //   927: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   930: pop
    //   931: aload #11
    //   933: aload #9
    //   935: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   938: pop
    //   939: aload #11
    //   941: ldc_w ') for fragment '
    //   944: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   947: pop
    //   948: aload #11
    //   950: aload_1
    //   951: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   954: pop
    //   955: new java/lang/IllegalArgumentException
    //   958: dup
    //   959: aload #11
    //   961: invokevirtual toString : ()Ljava/lang/String;
    //   964: invokespecial <init> : (Ljava/lang/String;)V
    //   967: astore #9
    //   969: aload_0
    //   970: aload #9
    //   972: invokespecial throwException : (Ljava/lang/RuntimeException;)V
    //   975: aload #10
    //   977: astore #9
    //   979: aload_1
    //   980: aload #9
    //   982: putfield mContainer : Landroid/view/ViewGroup;
    //   985: aload_1
    //   986: aload_1
    //   987: aload_1
    //   988: aload_1
    //   989: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   992: invokevirtual performGetLayoutInflater : (Landroid/os/Bundle;)Landroid/view/LayoutInflater;
    //   995: aload #9
    //   997: aload_1
    //   998: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1001: invokevirtual performCreateView : (Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    //   1004: putfield mView : Landroid/view/View;
    //   1007: aload_1
    //   1008: getfield mView : Landroid/view/View;
    //   1011: ifnull -> 1111
    //   1014: aload_1
    //   1015: getfield mView : Landroid/view/View;
    //   1018: iconst_0
    //   1019: invokevirtual setSaveFromParentEnabled : (Z)V
    //   1022: aload #9
    //   1024: ifnull -> 1036
    //   1027: aload #9
    //   1029: aload_1
    //   1030: getfield mView : Landroid/view/View;
    //   1033: invokevirtual addView : (Landroid/view/View;)V
    //   1036: aload_1
    //   1037: getfield mHidden : Z
    //   1040: ifeq -> 1052
    //   1043: aload_1
    //   1044: getfield mView : Landroid/view/View;
    //   1047: bipush #8
    //   1049: invokevirtual setVisibility : (I)V
    //   1052: aload_1
    //   1053: aload_1
    //   1054: getfield mView : Landroid/view/View;
    //   1057: aload_1
    //   1058: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1061: invokevirtual onViewCreated : (Landroid/view/View;Landroid/os/Bundle;)V
    //   1064: aload_0
    //   1065: aload_1
    //   1066: aload_1
    //   1067: getfield mView : Landroid/view/View;
    //   1070: aload_1
    //   1071: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1074: iconst_0
    //   1075: invokevirtual dispatchOnFragmentViewCreated : (Landroid/app/Fragment;Landroid/view/View;Landroid/os/Bundle;Z)V
    //   1078: aload_1
    //   1079: getfield mView : Landroid/view/View;
    //   1082: invokevirtual getVisibility : ()I
    //   1085: ifne -> 1102
    //   1088: aload_1
    //   1089: getfield mContainer : Landroid/view/ViewGroup;
    //   1092: ifnull -> 1102
    //   1095: iload #7
    //   1097: istore #5
    //   1099: goto -> 1105
    //   1102: iconst_0
    //   1103: istore #5
    //   1105: aload_1
    //   1106: iload #5
    //   1108: putfield mIsNewlyAdded : Z
    //   1111: aload_1
    //   1112: aload_1
    //   1113: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1116: invokevirtual performActivityCreated : (Landroid/os/Bundle;)V
    //   1119: aload_0
    //   1120: aload_1
    //   1121: aload_1
    //   1122: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1125: iconst_0
    //   1126: invokevirtual dispatchOnFragmentActivityCreated : (Landroid/app/Fragment;Landroid/os/Bundle;Z)V
    //   1129: aload_1
    //   1130: getfield mView : Landroid/view/View;
    //   1133: ifnull -> 1144
    //   1136: aload_1
    //   1137: aload_1
    //   1138: getfield mSavedFragmentState : Landroid/os/Bundle;
    //   1141: invokevirtual restoreViewState : (Landroid/os/Bundle;)V
    //   1144: aload_1
    //   1145: aconst_null
    //   1146: putfield mSavedFragmentState : Landroid/os/Bundle;
    //   1149: iload_2
    //   1150: istore_3
    //   1151: iload_3
    //   1152: istore #4
    //   1154: iload_3
    //   1155: iconst_2
    //   1156: if_icmple -> 1167
    //   1159: aload_1
    //   1160: iconst_3
    //   1161: putfield mState : I
    //   1164: iload_3
    //   1165: istore #4
    //   1167: iload #4
    //   1169: istore_3
    //   1170: iload #4
    //   1172: iconst_3
    //   1173: if_icmple -> 1231
    //   1176: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1179: ifeq -> 1218
    //   1182: new java/lang/StringBuilder
    //   1185: dup
    //   1186: invokespecial <init> : ()V
    //   1189: astore #9
    //   1191: aload #9
    //   1193: ldc_w 'moveto STARTED: '
    //   1196: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1199: pop
    //   1200: aload #9
    //   1202: aload_1
    //   1203: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1206: pop
    //   1207: ldc 'FragmentManager'
    //   1209: aload #9
    //   1211: invokevirtual toString : ()Ljava/lang/String;
    //   1214: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1217: pop
    //   1218: aload_1
    //   1219: invokevirtual performStart : ()V
    //   1222: aload_0
    //   1223: aload_1
    //   1224: iconst_0
    //   1225: invokevirtual dispatchOnFragmentStarted : (Landroid/app/Fragment;Z)V
    //   1228: iload #4
    //   1230: istore_3
    //   1231: iload_3
    //   1232: istore_2
    //   1233: iload_3
    //   1234: iconst_4
    //   1235: if_icmple -> 1302
    //   1238: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1241: ifeq -> 1280
    //   1244: new java/lang/StringBuilder
    //   1247: dup
    //   1248: invokespecial <init> : ()V
    //   1251: astore #9
    //   1253: aload #9
    //   1255: ldc_w 'moveto RESUMED: '
    //   1258: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1261: pop
    //   1262: aload #9
    //   1264: aload_1
    //   1265: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1268: pop
    //   1269: ldc 'FragmentManager'
    //   1271: aload #9
    //   1273: invokevirtual toString : ()Ljava/lang/String;
    //   1276: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1279: pop
    //   1280: aload_1
    //   1281: invokevirtual performResume : ()V
    //   1284: aload_0
    //   1285: aload_1
    //   1286: iconst_0
    //   1287: invokevirtual dispatchOnFragmentResumed : (Landroid/app/Fragment;Z)V
    //   1290: aload_1
    //   1291: aconst_null
    //   1292: putfield mSavedFragmentState : Landroid/os/Bundle;
    //   1295: aload_1
    //   1296: aconst_null
    //   1297: putfield mSavedViewState : Landroid/util/SparseArray;
    //   1300: iload_3
    //   1301: istore_2
    //   1302: iload_2
    //   1303: istore_3
    //   1304: goto -> 1963
    //   1307: aload_1
    //   1308: getfield mState : I
    //   1311: iload_2
    //   1312: if_icmple -> 1961
    //   1315: aload_1
    //   1316: getfield mState : I
    //   1319: istore #8
    //   1321: iload #8
    //   1323: iconst_1
    //   1324: if_icmpeq -> 1786
    //   1327: iload #8
    //   1329: iconst_2
    //   1330: if_icmpeq -> 1470
    //   1333: iload #8
    //   1335: iconst_3
    //   1336: if_icmpeq -> 1470
    //   1339: iload #8
    //   1341: iconst_4
    //   1342: if_icmpeq -> 1413
    //   1345: iload #8
    //   1347: iconst_5
    //   1348: if_icmpeq -> 1356
    //   1351: iload_2
    //   1352: istore_3
    //   1353: goto -> 1963
    //   1356: iload_2
    //   1357: iconst_5
    //   1358: if_icmpge -> 1413
    //   1361: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1364: ifeq -> 1403
    //   1367: new java/lang/StringBuilder
    //   1370: dup
    //   1371: invokespecial <init> : ()V
    //   1374: astore #9
    //   1376: aload #9
    //   1378: ldc_w 'movefrom RESUMED: '
    //   1381: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1384: pop
    //   1385: aload #9
    //   1387: aload_1
    //   1388: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1391: pop
    //   1392: ldc 'FragmentManager'
    //   1394: aload #9
    //   1396: invokevirtual toString : ()Ljava/lang/String;
    //   1399: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1402: pop
    //   1403: aload_1
    //   1404: invokevirtual performPause : ()V
    //   1407: aload_0
    //   1408: aload_1
    //   1409: iconst_0
    //   1410: invokevirtual dispatchOnFragmentPaused : (Landroid/app/Fragment;Z)V
    //   1413: iload_2
    //   1414: iconst_4
    //   1415: if_icmpge -> 1470
    //   1418: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1421: ifeq -> 1460
    //   1424: new java/lang/StringBuilder
    //   1427: dup
    //   1428: invokespecial <init> : ()V
    //   1431: astore #9
    //   1433: aload #9
    //   1435: ldc_w 'movefrom STARTED: '
    //   1438: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1441: pop
    //   1442: aload #9
    //   1444: aload_1
    //   1445: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1448: pop
    //   1449: ldc 'FragmentManager'
    //   1451: aload #9
    //   1453: invokevirtual toString : ()Ljava/lang/String;
    //   1456: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1459: pop
    //   1460: aload_1
    //   1461: invokevirtual performStop : ()V
    //   1464: aload_0
    //   1465: aload_1
    //   1466: iconst_0
    //   1467: invokevirtual dispatchOnFragmentStopped : (Landroid/app/Fragment;Z)V
    //   1470: iload_2
    //   1471: iconst_2
    //   1472: if_icmpge -> 1783
    //   1475: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1478: ifeq -> 1517
    //   1481: new java/lang/StringBuilder
    //   1484: dup
    //   1485: invokespecial <init> : ()V
    //   1488: astore #9
    //   1490: aload #9
    //   1492: ldc_w 'movefrom ACTIVITY_CREATED: '
    //   1495: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1498: pop
    //   1499: aload #9
    //   1501: aload_1
    //   1502: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1505: pop
    //   1506: ldc 'FragmentManager'
    //   1508: aload #9
    //   1510: invokevirtual toString : ()Ljava/lang/String;
    //   1513: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1516: pop
    //   1517: aload_1
    //   1518: getfield mView : Landroid/view/View;
    //   1521: ifnull -> 1547
    //   1524: aload_0
    //   1525: getfield mHost : Landroid/app/FragmentHostCallback;
    //   1528: aload_1
    //   1529: invokevirtual onShouldSaveFragmentState : (Landroid/app/Fragment;)Z
    //   1532: ifeq -> 1547
    //   1535: aload_1
    //   1536: getfield mSavedViewState : Landroid/util/SparseArray;
    //   1539: ifnonnull -> 1547
    //   1542: aload_0
    //   1543: aload_1
    //   1544: invokevirtual saveFragmentViewState : (Landroid/app/Fragment;)V
    //   1547: aload_1
    //   1548: invokevirtual performDestroyView : ()V
    //   1551: aload_0
    //   1552: aload_1
    //   1553: iconst_0
    //   1554: invokevirtual dispatchOnFragmentViewDestroyed : (Landroid/app/Fragment;Z)V
    //   1557: aload_1
    //   1558: getfield mView : Landroid/view/View;
    //   1561: ifnull -> 1765
    //   1564: aload_1
    //   1565: getfield mContainer : Landroid/view/ViewGroup;
    //   1568: ifnull -> 1765
    //   1571: aload_0
    //   1572: invokevirtual getTargetSdk : ()I
    //   1575: bipush #26
    //   1577: if_icmplt -> 1598
    //   1580: aload_1
    //   1581: getfield mView : Landroid/view/View;
    //   1584: invokevirtual clearAnimation : ()V
    //   1587: aload_1
    //   1588: getfield mContainer : Landroid/view/ViewGroup;
    //   1591: aload_1
    //   1592: getfield mView : Landroid/view/View;
    //   1595: invokevirtual endViewTransition : (Landroid/view/View;)V
    //   1598: aload_0
    //   1599: getfield mCurState : I
    //   1602: ifle -> 1662
    //   1605: aload_0
    //   1606: getfield mDestroyed : Z
    //   1609: ifne -> 1662
    //   1612: aload_1
    //   1613: getfield mView : Landroid/view/View;
    //   1616: astore #9
    //   1618: aload #9
    //   1620: invokevirtual getVisibility : ()I
    //   1623: ifne -> 1659
    //   1626: aload_1
    //   1627: getfield mView : Landroid/view/View;
    //   1630: astore #9
    //   1632: aload #9
    //   1634: invokevirtual getTransitionAlpha : ()F
    //   1637: fconst_0
    //   1638: fcmpl
    //   1639: ifle -> 1656
    //   1642: aload_0
    //   1643: aload_1
    //   1644: iload_3
    //   1645: iconst_0
    //   1646: iload #4
    //   1648: invokevirtual loadAnimator : (Landroid/app/Fragment;IZI)Landroid/animation/Animator;
    //   1651: astore #9
    //   1653: goto -> 1665
    //   1656: goto -> 1662
    //   1659: goto -> 1662
    //   1662: aconst_null
    //   1663: astore #9
    //   1665: aload_1
    //   1666: getfield mView : Landroid/view/View;
    //   1669: fconst_1
    //   1670: invokevirtual setTransitionAlpha : (F)V
    //   1673: aload #9
    //   1675: ifnull -> 1751
    //   1678: aload_1
    //   1679: getfield mContainer : Landroid/view/ViewGroup;
    //   1682: astore #11
    //   1684: aload_1
    //   1685: getfield mView : Landroid/view/View;
    //   1688: astore #10
    //   1690: aload #11
    //   1692: aload #10
    //   1694: invokevirtual startViewTransition : (Landroid/view/View;)V
    //   1697: aload_1
    //   1698: aload #9
    //   1700: invokevirtual setAnimatingAway : (Landroid/animation/Animator;)V
    //   1703: aload_1
    //   1704: iload_2
    //   1705: invokevirtual setStateAfterAnimating : (I)V
    //   1708: aload #9
    //   1710: new android/app/FragmentManagerImpl$2
    //   1713: dup
    //   1714: aload_0
    //   1715: aload #11
    //   1717: aload #10
    //   1719: aload_1
    //   1720: aload_1
    //   1721: invokespecial <init> : (Landroid/app/FragmentManagerImpl;Landroid/view/ViewGroup;Landroid/view/View;Landroid/app/Fragment;Landroid/app/Fragment;)V
    //   1724: invokevirtual addListener : (Landroid/animation/Animator$AnimatorListener;)V
    //   1727: aload #9
    //   1729: aload_1
    //   1730: getfield mView : Landroid/view/View;
    //   1733: invokevirtual setTarget : (Ljava/lang/Object;)V
    //   1736: aload_0
    //   1737: aload_1
    //   1738: getfield mView : Landroid/view/View;
    //   1741: aload #9
    //   1743: invokespecial setHWLayerAnimListenerIfAlpha : (Landroid/view/View;Landroid/animation/Animator;)V
    //   1746: aload #9
    //   1748: invokevirtual start : ()V
    //   1751: aload_1
    //   1752: getfield mContainer : Landroid/view/ViewGroup;
    //   1755: aload_1
    //   1756: getfield mView : Landroid/view/View;
    //   1759: invokevirtual removeView : (Landroid/view/View;)V
    //   1762: goto -> 1765
    //   1765: aload_1
    //   1766: aconst_null
    //   1767: putfield mContainer : Landroid/view/ViewGroup;
    //   1770: aload_1
    //   1771: aconst_null
    //   1772: putfield mView : Landroid/view/View;
    //   1775: aload_1
    //   1776: iconst_0
    //   1777: putfield mInLayout : Z
    //   1780: goto -> 1786
    //   1783: goto -> 1786
    //   1786: iload_2
    //   1787: istore_3
    //   1788: iload_2
    //   1789: iconst_1
    //   1790: if_icmpge -> 1963
    //   1793: aload_0
    //   1794: getfield mDestroyed : Z
    //   1797: ifeq -> 1823
    //   1800: aload_1
    //   1801: invokevirtual getAnimatingAway : ()Landroid/animation/Animator;
    //   1804: ifnull -> 1823
    //   1807: aload_1
    //   1808: invokevirtual getAnimatingAway : ()Landroid/animation/Animator;
    //   1811: astore #9
    //   1813: aload_1
    //   1814: aconst_null
    //   1815: invokevirtual setAnimatingAway : (Landroid/animation/Animator;)V
    //   1818: aload #9
    //   1820: invokevirtual cancel : ()V
    //   1823: aload_1
    //   1824: invokevirtual getAnimatingAway : ()Landroid/animation/Animator;
    //   1827: ifnull -> 1840
    //   1830: aload_1
    //   1831: iload_2
    //   1832: invokevirtual setStateAfterAnimating : (I)V
    //   1835: iconst_1
    //   1836: istore_3
    //   1837: goto -> 1963
    //   1840: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   1843: ifeq -> 1882
    //   1846: new java/lang/StringBuilder
    //   1849: dup
    //   1850: invokespecial <init> : ()V
    //   1853: astore #9
    //   1855: aload #9
    //   1857: ldc_w 'movefrom CREATED: '
    //   1860: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1863: pop
    //   1864: aload #9
    //   1866: aload_1
    //   1867: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1870: pop
    //   1871: ldc 'FragmentManager'
    //   1873: aload #9
    //   1875: invokevirtual toString : ()Ljava/lang/String;
    //   1878: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   1881: pop
    //   1882: aload_1
    //   1883: getfield mRetaining : Z
    //   1886: ifne -> 1902
    //   1889: aload_1
    //   1890: invokevirtual performDestroy : ()V
    //   1893: aload_0
    //   1894: aload_1
    //   1895: iconst_0
    //   1896: invokevirtual dispatchOnFragmentDestroyed : (Landroid/app/Fragment;Z)V
    //   1899: goto -> 1907
    //   1902: aload_1
    //   1903: iconst_0
    //   1904: putfield mState : I
    //   1907: aload_1
    //   1908: invokevirtual performDetach : ()V
    //   1911: aload_0
    //   1912: aload_1
    //   1913: iconst_0
    //   1914: invokevirtual dispatchOnFragmentDetached : (Landroid/app/Fragment;Z)V
    //   1917: iload_2
    //   1918: istore_3
    //   1919: iload #5
    //   1921: ifne -> 1963
    //   1924: aload_1
    //   1925: getfield mRetaining : Z
    //   1928: ifne -> 1941
    //   1931: aload_0
    //   1932: aload_1
    //   1933: invokevirtual makeInactive : (Landroid/app/Fragment;)V
    //   1936: iload_2
    //   1937: istore_3
    //   1938: goto -> 1963
    //   1941: aload_1
    //   1942: aconst_null
    //   1943: putfield mHost : Landroid/app/FragmentHostCallback;
    //   1946: aload_1
    //   1947: aconst_null
    //   1948: putfield mParentFragment : Landroid/app/Fragment;
    //   1951: aload_1
    //   1952: aconst_null
    //   1953: putfield mFragmentManager : Landroid/app/FragmentManagerImpl;
    //   1956: iload_2
    //   1957: istore_3
    //   1958: goto -> 1963
    //   1961: iload_2
    //   1962: istore_3
    //   1963: aload_1
    //   1964: getfield mState : I
    //   1967: iload_3
    //   1968: if_icmpeq -> 2047
    //   1971: new java/lang/StringBuilder
    //   1974: dup
    //   1975: invokespecial <init> : ()V
    //   1978: astore #9
    //   1980: aload #9
    //   1982: ldc_w 'moveToState: Fragment state for '
    //   1985: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1988: pop
    //   1989: aload #9
    //   1991: aload_1
    //   1992: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1995: pop
    //   1996: aload #9
    //   1998: ldc_w ' not updated inline; expected state '
    //   2001: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2004: pop
    //   2005: aload #9
    //   2007: iload_3
    //   2008: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2011: pop
    //   2012: aload #9
    //   2014: ldc_w ' found '
    //   2017: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2020: pop
    //   2021: aload #9
    //   2023: aload_1
    //   2024: getfield mState : I
    //   2027: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   2030: pop
    //   2031: ldc 'FragmentManager'
    //   2033: aload #9
    //   2035: invokevirtual toString : ()Ljava/lang/String;
    //   2038: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   2041: pop
    //   2042: aload_1
    //   2043: iload_3
    //   2044: putfield mState : I
    //   2047: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1167	-> 0
    //   #1172	-> 0
    //   #1173	-> 39
    //   #1175	-> 41
    //   #1176	-> 62
    //   #1178	-> 76
    //   #1181	-> 82
    //   #1186	-> 88
    //   #1187	-> 118
    //   #1189	-> 120
    //   #1193	-> 128
    //   #1194	-> 142
    //   #1196	-> 143
    //   #1201	-> 150
    //   #1202	-> 155
    //   #1204	-> 167
    //   #1206	-> 212
    //   #1207	-> 216
    //   #1208	-> 258
    //   #1209	-> 267
    //   #1211	-> 280
    //   #1213	-> 294
    //   #1214	-> 301
    //   #1217	-> 315
    //   #1219	-> 329
    //   #1220	-> 338
    //   #1221	-> 343
    //   #1222	-> 350
    //   #1227	-> 352
    //   #1228	-> 360
    //   #1229	-> 368
    //   #1230	-> 379
    //   #1234	-> 404
    //   #1235	-> 411
    //   #1240	-> 432
    //   #1241	-> 443
    //   #1240	-> 458
    //   #1236	-> 461
    //   #1234	-> 527
    //   #1245	-> 527
    //   #1246	-> 540
    //   #1247	-> 545
    //   #1248	-> 556
    //   #1252	-> 563
    //   #1253	-> 570
    //   #1255	-> 581
    //   #1257	-> 589
    //   #1259	-> 602
    //   #1260	-> 609
    //   #1261	-> 619
    //   #1262	-> 627
    //   #1264	-> 640
    //   #1265	-> 649
    //   #1267	-> 654
    //   #1249	-> 664
    //   #1274	-> 711
    //   #1276	-> 716
    //   #1277	-> 721
    //   #1278	-> 763
    //   #1279	-> 770
    //   #1280	-> 773
    //   #1281	-> 780
    //   #1282	-> 788
    //   #1287	-> 838
    //   #1288	-> 854
    //   #1291	-> 866
    //   #1294	-> 879
    //   #1292	-> 882
    //   #1293	-> 884
    //   #1295	-> 889
    //   #1297	-> 912
    //   #1295	-> 969
    //   #1302	-> 975
    //   #1303	-> 985
    //   #1305	-> 1007
    //   #1306	-> 1014
    //   #1307	-> 1022
    //   #1308	-> 1027
    //   #1310	-> 1036
    //   #1311	-> 1043
    //   #1313	-> 1052
    //   #1314	-> 1064
    //   #1318	-> 1078
    //   #1323	-> 1111
    //   #1324	-> 1119
    //   #1325	-> 1129
    //   #1326	-> 1136
    //   #1328	-> 1144
    //   #1332	-> 1149
    //   #1333	-> 1159
    //   #1337	-> 1167
    //   #1338	-> 1176
    //   #1339	-> 1218
    //   #1340	-> 1222
    //   #1344	-> 1231
    //   #1345	-> 1238
    //   #1346	-> 1280
    //   #1347	-> 1284
    //   #1349	-> 1290
    //   #1350	-> 1295
    //   #1353	-> 1307
    //   #1354	-> 1315
    //   #1356	-> 1356
    //   #1357	-> 1361
    //   #1358	-> 1403
    //   #1359	-> 1407
    //   #1363	-> 1413
    //   #1364	-> 1418
    //   #1365	-> 1460
    //   #1366	-> 1464
    //   #1371	-> 1470
    //   #1372	-> 1475
    //   #1373	-> 1517
    //   #1376	-> 1524
    //   #1377	-> 1542
    //   #1380	-> 1547
    //   #1381	-> 1551
    //   #1382	-> 1557
    //   #1383	-> 1571
    //   #1385	-> 1580
    //   #1386	-> 1587
    //   #1388	-> 1598
    //   #1389	-> 1598
    //   #1390	-> 1618
    //   #1391	-> 1632
    //   #1392	-> 1642
    //   #1391	-> 1656
    //   #1390	-> 1659
    //   #1389	-> 1662
    //   #1395	-> 1662
    //   #1396	-> 1673
    //   #1397	-> 1678
    //   #1398	-> 1684
    //   #1399	-> 1690
    //   #1400	-> 1690
    //   #1401	-> 1697
    //   #1402	-> 1703
    //   #1403	-> 1708
    //   #1419	-> 1727
    //   #1420	-> 1736
    //   #1421	-> 1746
    //   #1424	-> 1751
    //   #1382	-> 1765
    //   #1426	-> 1765
    //   #1427	-> 1770
    //   #1428	-> 1775
    //   #1371	-> 1783
    //   #1354	-> 1786
    //   #1432	-> 1786
    //   #1433	-> 1793
    //   #1434	-> 1800
    //   #1441	-> 1807
    //   #1442	-> 1813
    //   #1443	-> 1818
    //   #1446	-> 1823
    //   #1451	-> 1830
    //   #1452	-> 1835
    //   #1454	-> 1840
    //   #1455	-> 1882
    //   #1456	-> 1889
    //   #1457	-> 1893
    //   #1459	-> 1902
    //   #1462	-> 1907
    //   #1463	-> 1911
    //   #1464	-> 1917
    //   #1465	-> 1924
    //   #1466	-> 1931
    //   #1468	-> 1941
    //   #1469	-> 1946
    //   #1470	-> 1951
    //   #1353	-> 1961
    //   #1478	-> 1963
    //   #1479	-> 1971
    //   #1481	-> 2042
    //   #1483	-> 2047
    // Exception table:
    //   from	to	target	type
    //   866	879	882	android/content/res/Resources$NotFoundException
  }
  
  void moveToState(Fragment paramFragment) {
    moveToState(paramFragment, this.mCurState, 0, 0, false);
  }
  
  void ensureInflatedFragmentView(Fragment paramFragment) {
    if (paramFragment.mFromLayout && !paramFragment.mPerformedCreateView) {
      paramFragment.mView = paramFragment.performCreateView(paramFragment.performGetLayoutInflater(paramFragment.mSavedFragmentState), null, paramFragment.mSavedFragmentState);
      if (paramFragment.mView != null) {
        paramFragment.mView.setSaveFromParentEnabled(false);
        if (paramFragment.mHidden)
          paramFragment.mView.setVisibility(8); 
        paramFragment.onViewCreated(paramFragment.mView, paramFragment.mSavedFragmentState);
        dispatchOnFragmentViewCreated(paramFragment, paramFragment.mView, paramFragment.mSavedFragmentState, false);
      } 
    } 
  }
  
  void completeShowHideFragment(Fragment paramFragment) {
    if (paramFragment.mView != null) {
      int i = paramFragment.getNextTransition();
      boolean bool = paramFragment.mHidden;
      int j = paramFragment.getNextTransitionStyle();
      Animator animator = loadAnimator(paramFragment, i, bool ^ true, j);
      if (animator != null) {
        animator.setTarget(paramFragment.mView);
        if (paramFragment.mHidden) {
          if (paramFragment.isHideReplaced()) {
            paramFragment.setHideReplaced(false);
          } else {
            final ViewGroup container = paramFragment.mContainer;
            final View animatingView = paramFragment.mView;
            if (viewGroup != null)
              viewGroup.startViewTransition(view); 
            animator.addListener(new AnimatorListenerAdapter() {
                  final FragmentManagerImpl this$0;
                  
                  final View val$animatingView;
                  
                  final ViewGroup val$container;
                  
                  public void onAnimationEnd(Animator param1Animator) {
                    ViewGroup viewGroup = container;
                    if (viewGroup != null)
                      viewGroup.endViewTransition(animatingView); 
                    param1Animator.removeListener(this);
                    animatingView.setVisibility(8);
                  }
                });
          } 
        } else {
          paramFragment.mView.setVisibility(0);
        } 
        setHWLayerAnimListenerIfAlpha(paramFragment.mView, animator);
        animator.start();
      } else {
        if (paramFragment.mHidden && !paramFragment.isHideReplaced()) {
          j = 8;
        } else {
          j = 0;
        } 
        paramFragment.mView.setVisibility(j);
        if (paramFragment.isHideReplaced())
          paramFragment.setHideReplaced(false); 
      } 
    } 
    if (paramFragment.mAdded && paramFragment.mHasMenu && paramFragment.mMenuVisible)
      this.mNeedMenuInvalidate = true; 
    paramFragment.mHiddenChanged = false;
    paramFragment.onHiddenChanged(paramFragment.mHidden);
  }
  
  void moveFragmentToExpectedState(Fragment paramFragment) {
    if (paramFragment == null)
      return; 
    int i = this.mCurState;
    int j = i;
    if (paramFragment.mRemoving)
      if (paramFragment.isInBackStack()) {
        j = Math.min(i, 1);
      } else {
        j = Math.min(i, 0);
      }  
    moveToState(paramFragment, j, paramFragment.getNextTransition(), paramFragment.getNextTransitionStyle(), false);
    if (paramFragment.mView != null) {
      Fragment fragment = findFragmentUnder(paramFragment);
      if (fragment != null) {
        View view = fragment.mView;
        ViewGroup viewGroup = paramFragment.mContainer;
        j = viewGroup.indexOfChild(view);
        i = viewGroup.indexOfChild(paramFragment.mView);
        if (i < j) {
          viewGroup.removeViewAt(i);
          viewGroup.addView(paramFragment.mView, j);
        } 
      } 
      if (paramFragment.mIsNewlyAdded && paramFragment.mContainer != null) {
        paramFragment.mView.setTransitionAlpha(1.0F);
        paramFragment.mIsNewlyAdded = false;
        Animator animator = loadAnimator(paramFragment, paramFragment.getNextTransition(), true, paramFragment.getNextTransitionStyle());
        if (animator != null) {
          animator.setTarget(paramFragment.mView);
          setHWLayerAnimListenerIfAlpha(paramFragment.mView, animator);
          animator.start();
        } 
      } 
    } 
    if (paramFragment.mHiddenChanged)
      completeShowHideFragment(paramFragment); 
  }
  
  void moveToState(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mHost : Landroid/app/FragmentHostCallback;
    //   4: ifnonnull -> 25
    //   7: iload_1
    //   8: ifne -> 14
    //   11: goto -> 25
    //   14: new java/lang/IllegalStateException
    //   17: dup
    //   18: ldc_w 'No activity'
    //   21: invokespecial <init> : (Ljava/lang/String;)V
    //   24: athrow
    //   25: iload_2
    //   26: ifne -> 38
    //   29: aload_0
    //   30: getfield mCurState : I
    //   33: iload_1
    //   34: if_icmpne -> 38
    //   37: return
    //   38: aload_0
    //   39: iload_1
    //   40: putfield mCurState : I
    //   43: aload_0
    //   44: getfield mActive : Landroid/util/SparseArray;
    //   47: ifnull -> 276
    //   50: iconst_0
    //   51: istore_1
    //   52: aload_0
    //   53: getfield mAdded : Ljava/util/ArrayList;
    //   56: invokevirtual size : ()I
    //   59: istore_3
    //   60: iconst_0
    //   61: istore #4
    //   63: iload #4
    //   65: iload_3
    //   66: if_icmpge -> 121
    //   69: aload_0
    //   70: getfield mAdded : Ljava/util/ArrayList;
    //   73: iload #4
    //   75: invokevirtual get : (I)Ljava/lang/Object;
    //   78: checkcast android/app/Fragment
    //   81: astore #5
    //   83: aload_0
    //   84: aload #5
    //   86: invokevirtual moveFragmentToExpectedState : (Landroid/app/Fragment;)V
    //   89: iload_1
    //   90: istore #6
    //   92: aload #5
    //   94: getfield mLoaderManager : Landroid/app/LoaderManagerImpl;
    //   97: ifnull -> 112
    //   100: iload_1
    //   101: aload #5
    //   103: getfield mLoaderManager : Landroid/app/LoaderManagerImpl;
    //   106: invokevirtual hasRunningLoaders : ()Z
    //   109: ior
    //   110: istore #6
    //   112: iinc #4, 1
    //   115: iload #6
    //   117: istore_1
    //   118: goto -> 63
    //   121: aload_0
    //   122: getfield mActive : Landroid/util/SparseArray;
    //   125: invokevirtual size : ()I
    //   128: istore_3
    //   129: iconst_0
    //   130: istore #4
    //   132: iload_1
    //   133: istore #6
    //   135: iload #4
    //   137: iload_3
    //   138: if_icmpge -> 231
    //   141: aload_0
    //   142: getfield mActive : Landroid/util/SparseArray;
    //   145: iload #4
    //   147: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   150: checkcast android/app/Fragment
    //   153: astore #5
    //   155: iload #6
    //   157: istore_1
    //   158: aload #5
    //   160: ifnull -> 222
    //   163: aload #5
    //   165: getfield mRemoving : Z
    //   168: ifne -> 182
    //   171: iload #6
    //   173: istore_1
    //   174: aload #5
    //   176: getfield mDetached : Z
    //   179: ifeq -> 222
    //   182: iload #6
    //   184: istore_1
    //   185: aload #5
    //   187: getfield mIsNewlyAdded : Z
    //   190: ifne -> 222
    //   193: aload_0
    //   194: aload #5
    //   196: invokevirtual moveFragmentToExpectedState : (Landroid/app/Fragment;)V
    //   199: iload #6
    //   201: istore_1
    //   202: aload #5
    //   204: getfield mLoaderManager : Landroid/app/LoaderManagerImpl;
    //   207: ifnull -> 222
    //   210: iload #6
    //   212: aload #5
    //   214: getfield mLoaderManager : Landroid/app/LoaderManagerImpl;
    //   217: invokevirtual hasRunningLoaders : ()Z
    //   220: ior
    //   221: istore_1
    //   222: iinc #4, 1
    //   225: iload_1
    //   226: istore #6
    //   228: goto -> 135
    //   231: iload #6
    //   233: ifne -> 240
    //   236: aload_0
    //   237: invokevirtual startPendingDeferredFragments : ()V
    //   240: aload_0
    //   241: getfield mNeedMenuInvalidate : Z
    //   244: ifeq -> 276
    //   247: aload_0
    //   248: getfield mHost : Landroid/app/FragmentHostCallback;
    //   251: astore #5
    //   253: aload #5
    //   255: ifnull -> 276
    //   258: aload_0
    //   259: getfield mCurState : I
    //   262: iconst_5
    //   263: if_icmpne -> 276
    //   266: aload #5
    //   268: invokevirtual onInvalidateOptionsMenu : ()V
    //   271: aload_0
    //   272: iconst_0
    //   273: putfield mNeedMenuInvalidate : Z
    //   276: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1625	-> 0
    //   #1626	-> 14
    //   #1629	-> 25
    //   #1630	-> 37
    //   #1633	-> 38
    //   #1635	-> 43
    //   #1636	-> 50
    //   #1639	-> 52
    //   #1640	-> 60
    //   #1641	-> 69
    //   #1642	-> 83
    //   #1643	-> 89
    //   #1644	-> 100
    //   #1640	-> 112
    //   #1650	-> 121
    //   #1651	-> 129
    //   #1652	-> 141
    //   #1653	-> 155
    //   #1654	-> 193
    //   #1655	-> 199
    //   #1656	-> 210
    //   #1651	-> 222
    //   #1661	-> 231
    //   #1662	-> 236
    //   #1665	-> 240
    //   #1666	-> 266
    //   #1667	-> 271
    //   #1670	-> 276
  }
  
  void startPendingDeferredFragments() {
    if (this.mActive == null)
      return; 
    for (byte b = 0; b < this.mActive.size(); b++) {
      Fragment fragment = (Fragment)this.mActive.valueAt(b);
      if (fragment != null)
        performPendingDeferredStart(fragment); 
    } 
  }
  
  void makeActive(Fragment paramFragment) {
    if (paramFragment.mIndex >= 0)
      return; 
    int i = this.mNextFragmentIndex;
    this.mNextFragmentIndex = i + 1;
    paramFragment.setIndex(i, this.mParent);
    if (this.mActive == null)
      this.mActive = new SparseArray(); 
    this.mActive.put(paramFragment.mIndex, paramFragment);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Allocated fragment index ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
  }
  
  void makeInactive(Fragment paramFragment) {
    if (paramFragment.mIndex < 0)
      return; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Freeing fragment index ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    this.mActive.put(paramFragment.mIndex, null);
    this.mHost.inactivateFragment(paramFragment.mWho);
    paramFragment.initState();
  }
  
  public void addFragment(Fragment paramFragment, boolean paramBoolean) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("add: ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    makeActive(paramFragment);
    if (!paramFragment.mDetached)
      if (!this.mAdded.contains(paramFragment)) {
        synchronized (this.mAdded) {
          this.mAdded.add(paramFragment);
          paramFragment.mAdded = true;
          paramFragment.mRemoving = false;
          if (paramFragment.mView == null)
            paramFragment.mHiddenChanged = false; 
          if (paramFragment.mHasMenu && paramFragment.mMenuVisible)
            this.mNeedMenuInvalidate = true; 
          if (paramBoolean)
            moveToState(paramFragment); 
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Fragment already added: ");
        stringBuilder.append(paramFragment);
        throw new IllegalStateException(stringBuilder.toString());
      }  
  }
  
  public void removeFragment(Fragment paramFragment) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("remove: ");
      stringBuilder.append(paramFragment);
      stringBuilder.append(" nesting=");
      stringBuilder.append(paramFragment.mBackStackNesting);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    boolean bool = paramFragment.isInBackStack();
    if (!paramFragment.mDetached || (bool ^ true) != 0)
      synchronized (this.mAdded) {
        this.mAdded.remove(paramFragment);
        if (paramFragment.mHasMenu && paramFragment.mMenuVisible)
          this.mNeedMenuInvalidate = true; 
        paramFragment.mAdded = false;
        paramFragment.mRemoving = true;
        return;
      }  
  }
  
  public void hideFragment(Fragment paramFragment) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("hide: ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    if (!paramFragment.mHidden) {
      paramFragment.mHidden = true;
      paramFragment.mHiddenChanged = true ^ paramFragment.mHiddenChanged;
    } 
  }
  
  public void showFragment(Fragment paramFragment) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("show: ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    if (paramFragment.mHidden) {
      paramFragment.mHidden = false;
      paramFragment.mHiddenChanged ^= 0x1;
    } 
  }
  
  public void detachFragment(Fragment paramFragment) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("detach: ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    if (!paramFragment.mDetached) {
      paramFragment.mDetached = true;
      if (paramFragment.mAdded) {
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("remove from detach: ");
          stringBuilder.append(paramFragment);
          Log.v("FragmentManager", stringBuilder.toString());
        } 
        synchronized (this.mAdded) {
          this.mAdded.remove(paramFragment);
          if (paramFragment.mHasMenu && paramFragment.mMenuVisible)
            this.mNeedMenuInvalidate = true; 
          paramFragment.mAdded = false;
        } 
      } 
    } 
  }
  
  public void attachFragment(Fragment paramFragment) {
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("attach: ");
      stringBuilder.append(paramFragment);
      Log.v("FragmentManager", stringBuilder.toString());
    } 
    if (paramFragment.mDetached) {
      paramFragment.mDetached = false;
      if (!paramFragment.mAdded)
        if (!this.mAdded.contains(paramFragment)) {
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("add from attach: ");
            stringBuilder.append(paramFragment);
            Log.v("FragmentManager", stringBuilder.toString());
          } 
          synchronized (this.mAdded) {
            this.mAdded.add(paramFragment);
            paramFragment.mAdded = true;
            if (paramFragment.mHasMenu && paramFragment.mMenuVisible)
              this.mNeedMenuInvalidate = true; 
          } 
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Fragment already added: ");
          stringBuilder.append(paramFragment);
          throw new IllegalStateException(stringBuilder.toString());
        }  
    } 
  }
  
  public Fragment findFragmentById(int paramInt) {
    int i;
    for (i = this.mAdded.size() - 1; i >= 0; i--) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment != null && fragment.mFragmentId == paramInt)
        return fragment; 
    } 
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray != null)
      for (i = sparseArray.size() - 1; i >= 0; i--) {
        Fragment fragment = (Fragment)this.mActive.valueAt(i);
        if (fragment != null && fragment.mFragmentId == paramInt)
          return fragment; 
      }  
    return null;
  }
  
  public Fragment findFragmentByTag(String paramString) {
    if (paramString != null)
      for (int i = this.mAdded.size() - 1; i >= 0; i--) {
        Fragment fragment = this.mAdded.get(i);
        if (fragment != null && paramString.equals(fragment.mTag))
          return fragment; 
      }  
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray != null && paramString != null)
      for (int i = sparseArray.size() - 1; i >= 0; i--) {
        Fragment fragment = (Fragment)this.mActive.valueAt(i);
        if (fragment != null && paramString.equals(fragment.mTag))
          return fragment; 
      }  
    return null;
  }
  
  public Fragment findFragmentByWho(String paramString) {
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray != null && paramString != null)
      for (int i = sparseArray.size() - 1; i >= 0; i--) {
        Fragment fragment = (Fragment)this.mActive.valueAt(i);
        if (fragment != null) {
          fragment = fragment.findFragmentByWho(paramString);
          if (fragment != null)
            return fragment; 
        } 
      }  
    return null;
  }
  
  private void checkStateLoss() {
    if (!this.mStateSaved) {
      if (this.mNoTransactionsBecause == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can not perform this action inside of ");
      stringBuilder.append(this.mNoTransactionsBecause);
      throw new IllegalStateException(stringBuilder.toString());
    } 
    throw new IllegalStateException("Can not perform this action after onSaveInstanceState");
  }
  
  public boolean isStateSaved() {
    return this.mStateSaved;
  }
  
  public void enqueueAction(OpGenerator paramOpGenerator, boolean paramBoolean) {
    // Byte code:
    //   0: iload_2
    //   1: ifne -> 8
    //   4: aload_0
    //   5: invokespecial checkStateLoss : ()V
    //   8: aload_0
    //   9: monitorenter
    //   10: aload_0
    //   11: getfield mDestroyed : Z
    //   14: ifne -> 63
    //   17: aload_0
    //   18: getfield mHost : Landroid/app/FragmentHostCallback;
    //   21: ifnonnull -> 27
    //   24: goto -> 63
    //   27: aload_0
    //   28: getfield mPendingActions : Ljava/util/ArrayList;
    //   31: ifnonnull -> 47
    //   34: new java/util/ArrayList
    //   37: astore_3
    //   38: aload_3
    //   39: invokespecial <init> : ()V
    //   42: aload_0
    //   43: aload_3
    //   44: putfield mPendingActions : Ljava/util/ArrayList;
    //   47: aload_0
    //   48: getfield mPendingActions : Ljava/util/ArrayList;
    //   51: aload_1
    //   52: invokevirtual add : (Ljava/lang/Object;)Z
    //   55: pop
    //   56: aload_0
    //   57: invokespecial scheduleCommit : ()V
    //   60: aload_0
    //   61: monitorexit
    //   62: return
    //   63: iload_2
    //   64: ifeq -> 70
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: new java/lang/IllegalStateException
    //   73: astore_1
    //   74: aload_1
    //   75: ldc_w 'Activity has been destroyed'
    //   78: invokespecial <init> : (Ljava/lang/String;)V
    //   81: aload_1
    //   82: athrow
    //   83: astore_1
    //   84: aload_0
    //   85: monitorexit
    //   86: aload_1
    //   87: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1904	-> 0
    //   #1905	-> 4
    //   #1907	-> 8
    //   #1908	-> 10
    //   #1915	-> 27
    //   #1916	-> 34
    //   #1918	-> 47
    //   #1919	-> 56
    //   #1920	-> 60
    //   #1921	-> 62
    //   #1909	-> 63
    //   #1911	-> 67
    //   #1913	-> 70
    //   #1920	-> 83
    // Exception table:
    //   from	to	target	type
    //   10	24	83	finally
    //   27	34	83	finally
    //   34	47	83	finally
    //   47	56	83	finally
    //   56	60	83	finally
    //   60	62	83	finally
    //   67	69	83	finally
    //   70	83	83	finally
    //   84	86	83	finally
  }
  
  private void scheduleCommit() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPostponedTransactions : Ljava/util/ArrayList;
    //   6: astore_1
    //   7: iconst_0
    //   8: istore_2
    //   9: aload_1
    //   10: ifnull -> 30
    //   13: aload_0
    //   14: getfield mPostponedTransactions : Ljava/util/ArrayList;
    //   17: astore_1
    //   18: aload_1
    //   19: invokevirtual isEmpty : ()Z
    //   22: ifne -> 30
    //   25: iconst_1
    //   26: istore_3
    //   27: goto -> 32
    //   30: iconst_0
    //   31: istore_3
    //   32: iload_2
    //   33: istore #4
    //   35: aload_0
    //   36: getfield mPendingActions : Ljava/util/ArrayList;
    //   39: ifnull -> 59
    //   42: iload_2
    //   43: istore #4
    //   45: aload_0
    //   46: getfield mPendingActions : Ljava/util/ArrayList;
    //   49: invokevirtual size : ()I
    //   52: iconst_1
    //   53: if_icmpne -> 59
    //   56: iconst_1
    //   57: istore #4
    //   59: iload_3
    //   60: ifne -> 68
    //   63: iload #4
    //   65: ifeq -> 97
    //   68: aload_0
    //   69: getfield mHost : Landroid/app/FragmentHostCallback;
    //   72: invokevirtual getHandler : ()Landroid/os/Handler;
    //   75: aload_0
    //   76: getfield mExecCommit : Ljava/lang/Runnable;
    //   79: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)V
    //   82: aload_0
    //   83: getfield mHost : Landroid/app/FragmentHostCallback;
    //   86: invokevirtual getHandler : ()Landroid/os/Handler;
    //   89: aload_0
    //   90: getfield mExecCommit : Ljava/lang/Runnable;
    //   93: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   96: pop
    //   97: aload_0
    //   98: monitorexit
    //   99: return
    //   100: astore_1
    //   101: aload_0
    //   102: monitorexit
    //   103: aload_1
    //   104: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1930	-> 0
    //   #1931	-> 2
    //   #1932	-> 18
    //   #1933	-> 32
    //   #1934	-> 59
    //   #1935	-> 68
    //   #1936	-> 82
    //   #1938	-> 97
    //   #1939	-> 99
    //   #1938	-> 100
    // Exception table:
    //   from	to	target	type
    //   2	7	100	finally
    //   13	18	100	finally
    //   18	25	100	finally
    //   35	42	100	finally
    //   45	56	100	finally
    //   68	82	100	finally
    //   82	97	100	finally
    //   97	99	100	finally
    //   101	103	100	finally
  }
  
  public int allocBackStackIndex(BackStackRecord paramBackStackRecord) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   6: ifnull -> 111
    //   9: aload_0
    //   10: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   13: invokevirtual size : ()I
    //   16: ifgt -> 22
    //   19: goto -> 111
    //   22: aload_0
    //   23: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   26: aload_0
    //   27: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   30: invokevirtual size : ()I
    //   33: iconst_1
    //   34: isub
    //   35: invokevirtual remove : (I)Ljava/lang/Object;
    //   38: checkcast java/lang/Integer
    //   41: invokevirtual intValue : ()I
    //   44: istore_2
    //   45: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   48: ifeq -> 97
    //   51: new java/lang/StringBuilder
    //   54: astore_3
    //   55: aload_3
    //   56: invokespecial <init> : ()V
    //   59: aload_3
    //   60: ldc_w 'Adding back stack index '
    //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   66: pop
    //   67: aload_3
    //   68: iload_2
    //   69: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   72: pop
    //   73: aload_3
    //   74: ldc_w ' with '
    //   77: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   80: pop
    //   81: aload_3
    //   82: aload_1
    //   83: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: ldc 'FragmentManager'
    //   89: aload_3
    //   90: invokevirtual toString : ()Ljava/lang/String;
    //   93: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   96: pop
    //   97: aload_0
    //   98: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   101: iload_2
    //   102: aload_1
    //   103: invokevirtual set : (ILjava/lang/Object;)Ljava/lang/Object;
    //   106: pop
    //   107: aload_0
    //   108: monitorexit
    //   109: iload_2
    //   110: ireturn
    //   111: aload_0
    //   112: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   115: ifnonnull -> 131
    //   118: new java/util/ArrayList
    //   121: astore_3
    //   122: aload_3
    //   123: invokespecial <init> : ()V
    //   126: aload_0
    //   127: aload_3
    //   128: putfield mBackStackIndices : Ljava/util/ArrayList;
    //   131: aload_0
    //   132: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   135: invokevirtual size : ()I
    //   138: istore_2
    //   139: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   142: ifeq -> 191
    //   145: new java/lang/StringBuilder
    //   148: astore_3
    //   149: aload_3
    //   150: invokespecial <init> : ()V
    //   153: aload_3
    //   154: ldc_w 'Setting back stack index '
    //   157: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload_3
    //   162: iload_2
    //   163: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   166: pop
    //   167: aload_3
    //   168: ldc_w ' to '
    //   171: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload_3
    //   176: aload_1
    //   177: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: ldc 'FragmentManager'
    //   183: aload_3
    //   184: invokevirtual toString : ()Ljava/lang/String;
    //   187: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   190: pop
    //   191: aload_0
    //   192: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   195: aload_1
    //   196: invokevirtual add : (Ljava/lang/Object;)Z
    //   199: pop
    //   200: aload_0
    //   201: monitorexit
    //   202: iload_2
    //   203: ireturn
    //   204: astore_1
    //   205: aload_0
    //   206: monitorexit
    //   207: aload_1
    //   208: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1942	-> 0
    //   #1943	-> 2
    //   #1953	-> 22
    //   #1954	-> 45
    //   #1955	-> 97
    //   #1956	-> 107
    //   #1944	-> 111
    //   #1945	-> 118
    //   #1947	-> 131
    //   #1948	-> 139
    //   #1949	-> 191
    //   #1950	-> 200
    //   #1958	-> 204
    // Exception table:
    //   from	to	target	type
    //   2	19	204	finally
    //   22	45	204	finally
    //   45	97	204	finally
    //   97	107	204	finally
    //   107	109	204	finally
    //   111	118	204	finally
    //   118	131	204	finally
    //   131	139	204	finally
    //   139	191	204	finally
    //   191	200	204	finally
    //   200	202	204	finally
    //   205	207	204	finally
  }
  
  public void setBackStackIndex(int paramInt, BackStackRecord paramBackStackRecord) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   6: ifnonnull -> 22
    //   9: new java/util/ArrayList
    //   12: astore_3
    //   13: aload_3
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_3
    //   19: putfield mBackStackIndices : Ljava/util/ArrayList;
    //   22: aload_0
    //   23: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   26: invokevirtual size : ()I
    //   29: istore #4
    //   31: iload #4
    //   33: istore #5
    //   35: iload_1
    //   36: iload #4
    //   38: if_icmpge -> 106
    //   41: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   44: ifeq -> 93
    //   47: new java/lang/StringBuilder
    //   50: astore_3
    //   51: aload_3
    //   52: invokespecial <init> : ()V
    //   55: aload_3
    //   56: ldc_w 'Setting back stack index '
    //   59: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   62: pop
    //   63: aload_3
    //   64: iload_1
    //   65: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_3
    //   70: ldc_w ' to '
    //   73: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   76: pop
    //   77: aload_3
    //   78: aload_2
    //   79: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: ldc 'FragmentManager'
    //   85: aload_3
    //   86: invokevirtual toString : ()Ljava/lang/String;
    //   89: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   92: pop
    //   93: aload_0
    //   94: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   97: iload_1
    //   98: aload_2
    //   99: invokevirtual set : (ILjava/lang/Object;)Ljava/lang/Object;
    //   102: pop
    //   103: goto -> 260
    //   106: iload #5
    //   108: iload_1
    //   109: if_icmpge -> 199
    //   112: aload_0
    //   113: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   116: aconst_null
    //   117: invokevirtual add : (Ljava/lang/Object;)Z
    //   120: pop
    //   121: aload_0
    //   122: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   125: ifnonnull -> 141
    //   128: new java/util/ArrayList
    //   131: astore_3
    //   132: aload_3
    //   133: invokespecial <init> : ()V
    //   136: aload_0
    //   137: aload_3
    //   138: putfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   141: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   144: ifeq -> 180
    //   147: new java/lang/StringBuilder
    //   150: astore_3
    //   151: aload_3
    //   152: invokespecial <init> : ()V
    //   155: aload_3
    //   156: ldc_w 'Adding available back stack index '
    //   159: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload_3
    //   164: iload #5
    //   166: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: ldc 'FragmentManager'
    //   172: aload_3
    //   173: invokevirtual toString : ()Ljava/lang/String;
    //   176: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   179: pop
    //   180: aload_0
    //   181: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   184: iload #5
    //   186: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   189: invokevirtual add : (Ljava/lang/Object;)Z
    //   192: pop
    //   193: iinc #5, 1
    //   196: goto -> 106
    //   199: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   202: ifeq -> 251
    //   205: new java/lang/StringBuilder
    //   208: astore_3
    //   209: aload_3
    //   210: invokespecial <init> : ()V
    //   213: aload_3
    //   214: ldc_w 'Adding back stack index '
    //   217: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   220: pop
    //   221: aload_3
    //   222: iload_1
    //   223: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload_3
    //   228: ldc_w ' with '
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload_3
    //   236: aload_2
    //   237: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   240: pop
    //   241: ldc 'FragmentManager'
    //   243: aload_3
    //   244: invokevirtual toString : ()Ljava/lang/String;
    //   247: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   250: pop
    //   251: aload_0
    //   252: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   255: aload_2
    //   256: invokevirtual add : (Ljava/lang/Object;)Z
    //   259: pop
    //   260: aload_0
    //   261: monitorexit
    //   262: return
    //   263: astore_2
    //   264: aload_0
    //   265: monitorexit
    //   266: aload_2
    //   267: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1962	-> 0
    //   #1963	-> 2
    //   #1964	-> 9
    //   #1966	-> 22
    //   #1967	-> 31
    //   #1968	-> 41
    //   #1969	-> 93
    //   #1971	-> 106
    //   #1972	-> 112
    //   #1973	-> 121
    //   #1974	-> 128
    //   #1976	-> 141
    //   #1977	-> 180
    //   #1978	-> 193
    //   #1980	-> 199
    //   #1981	-> 251
    //   #1983	-> 260
    //   #1984	-> 262
    //   #1983	-> 263
    // Exception table:
    //   from	to	target	type
    //   2	9	263	finally
    //   9	22	263	finally
    //   22	31	263	finally
    //   41	93	263	finally
    //   93	103	263	finally
    //   112	121	263	finally
    //   121	128	263	finally
    //   128	141	263	finally
    //   141	180	263	finally
    //   180	193	263	finally
    //   199	251	263	finally
    //   251	260	263	finally
    //   260	262	263	finally
    //   264	266	263	finally
  }
  
  public void freeBackStackIndex(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mBackStackIndices : Ljava/util/ArrayList;
    //   6: iload_1
    //   7: aconst_null
    //   8: invokevirtual set : (ILjava/lang/Object;)Ljava/lang/Object;
    //   11: pop
    //   12: aload_0
    //   13: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   16: ifnonnull -> 32
    //   19: new java/util/ArrayList
    //   22: astore_2
    //   23: aload_2
    //   24: invokespecial <init> : ()V
    //   27: aload_0
    //   28: aload_2
    //   29: putfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   32: getstatic android/app/FragmentManagerImpl.DEBUG : Z
    //   35: ifeq -> 70
    //   38: new java/lang/StringBuilder
    //   41: astore_2
    //   42: aload_2
    //   43: invokespecial <init> : ()V
    //   46: aload_2
    //   47: ldc_w 'Freeing back stack index '
    //   50: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   53: pop
    //   54: aload_2
    //   55: iload_1
    //   56: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   59: pop
    //   60: ldc 'FragmentManager'
    //   62: aload_2
    //   63: invokevirtual toString : ()Ljava/lang/String;
    //   66: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   69: pop
    //   70: aload_0
    //   71: getfield mAvailBackStackIndices : Ljava/util/ArrayList;
    //   74: iload_1
    //   75: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   78: invokevirtual add : (Ljava/lang/Object;)Z
    //   81: pop
    //   82: aload_0
    //   83: monitorexit
    //   84: return
    //   85: astore_2
    //   86: aload_0
    //   87: monitorexit
    //   88: aload_2
    //   89: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1987	-> 0
    //   #1988	-> 2
    //   #1989	-> 12
    //   #1990	-> 19
    //   #1992	-> 32
    //   #1993	-> 70
    //   #1994	-> 82
    //   #1995	-> 84
    //   #1994	-> 85
    // Exception table:
    //   from	to	target	type
    //   2	12	85	finally
    //   12	19	85	finally
    //   19	32	85	finally
    //   32	70	85	finally
    //   70	82	85	finally
    //   82	84	85	finally
    //   86	88	85	finally
  }
  
  private void ensureExecReady(boolean paramBoolean) {
    if (!this.mExecutingActions) {
      if (Looper.myLooper() == this.mHost.getHandler().getLooper()) {
        if (!paramBoolean)
          checkStateLoss(); 
        if (this.mTmpRecords == null) {
          this.mTmpRecords = new ArrayList<>();
          this.mTmpIsPop = new ArrayList<>();
        } 
        this.mExecutingActions = true;
        try {
          executePostponedTransaction(null, null);
          return;
        } finally {
          this.mExecutingActions = false;
        } 
      } 
      throw new IllegalStateException("Must be called from main thread of fragment host");
    } 
    throw new IllegalStateException("FragmentManager is already executing transactions");
  }
  
  public void execSingleAction(OpGenerator paramOpGenerator, boolean paramBoolean) {
    if (paramBoolean && (this.mHost == null || this.mDestroyed))
      return; 
    ensureExecReady(paramBoolean);
    if (paramOpGenerator.generateOps(this.mTmpRecords, this.mTmpIsPop)) {
      this.mExecutingActions = true;
      try {
        removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
      } finally {
        cleanupExec();
      } 
    } 
    doPendingDeferredStart();
    burpActive();
  }
  
  private void cleanupExec() {
    this.mExecutingActions = false;
    this.mTmpIsPop.clear();
    this.mTmpRecords.clear();
  }
  
  public boolean execPendingActions() {
    ensureExecReady(true);
    boolean bool = false;
    while (generateOpsForPendingActions(this.mTmpRecords, this.mTmpIsPop)) {
      this.mExecutingActions = true;
      try {
        removeRedundantOperationsAndExecute(this.mTmpRecords, this.mTmpIsPop);
        cleanupExec();
      } finally {
        cleanupExec();
      } 
    } 
    doPendingDeferredStart();
    burpActive();
    return bool;
  }
  
  private void executePostponedTransaction(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPostponedTransactions : Ljava/util/ArrayList;
    //   4: astore_3
    //   5: aload_3
    //   6: ifnonnull -> 15
    //   9: iconst_0
    //   10: istore #4
    //   12: goto -> 21
    //   15: aload_3
    //   16: invokevirtual size : ()I
    //   19: istore #4
    //   21: iconst_0
    //   22: istore #5
    //   24: iload #5
    //   26: iload #4
    //   28: if_icmpge -> 232
    //   31: aload_0
    //   32: getfield mPostponedTransactions : Ljava/util/ArrayList;
    //   35: iload #5
    //   37: invokevirtual get : (I)Ljava/lang/Object;
    //   40: checkcast android/app/FragmentManagerImpl$StartEnterTransitionListener
    //   43: astore_3
    //   44: aload_1
    //   45: ifnull -> 101
    //   48: aload_3
    //   49: invokestatic access$000 : (Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Z
    //   52: ifne -> 101
    //   55: aload_1
    //   56: aload_3
    //   57: invokestatic access$100 : (Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;
    //   60: invokevirtual indexOf : (Ljava/lang/Object;)I
    //   63: istore #6
    //   65: iload #6
    //   67: iconst_m1
    //   68: if_icmpeq -> 101
    //   71: aload_2
    //   72: iload #6
    //   74: invokevirtual get : (I)Ljava/lang/Object;
    //   77: checkcast java/lang/Boolean
    //   80: invokevirtual booleanValue : ()Z
    //   83: ifeq -> 101
    //   86: aload_3
    //   87: invokevirtual cancelTransaction : ()V
    //   90: iload #4
    //   92: istore #6
    //   94: iload #5
    //   96: istore #7
    //   98: goto -> 219
    //   101: aload_3
    //   102: invokevirtual isReady : ()Z
    //   105: ifne -> 144
    //   108: iload #4
    //   110: istore #6
    //   112: iload #5
    //   114: istore #7
    //   116: aload_1
    //   117: ifnull -> 219
    //   120: iload #4
    //   122: istore #6
    //   124: iload #5
    //   126: istore #7
    //   128: aload_3
    //   129: invokestatic access$100 : (Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;
    //   132: aload_1
    //   133: iconst_0
    //   134: aload_1
    //   135: invokevirtual size : ()I
    //   138: invokevirtual interactsWith : (Ljava/util/ArrayList;II)Z
    //   141: ifeq -> 219
    //   144: aload_0
    //   145: getfield mPostponedTransactions : Ljava/util/ArrayList;
    //   148: iload #5
    //   150: invokevirtual remove : (I)Ljava/lang/Object;
    //   153: pop
    //   154: iload #5
    //   156: iconst_1
    //   157: isub
    //   158: istore #7
    //   160: iload #4
    //   162: iconst_1
    //   163: isub
    //   164: istore #6
    //   166: aload_1
    //   167: ifnull -> 215
    //   170: aload_3
    //   171: invokestatic access$000 : (Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Z
    //   174: ifne -> 215
    //   177: aload_1
    //   178: aload_3
    //   179: invokestatic access$100 : (Landroid/app/FragmentManagerImpl$StartEnterTransitionListener;)Landroid/app/BackStackRecord;
    //   182: invokevirtual indexOf : (Ljava/lang/Object;)I
    //   185: istore #5
    //   187: iload #5
    //   189: iconst_m1
    //   190: if_icmpeq -> 215
    //   193: aload_2
    //   194: iload #5
    //   196: invokevirtual get : (I)Ljava/lang/Object;
    //   199: checkcast java/lang/Boolean
    //   202: invokevirtual booleanValue : ()Z
    //   205: ifeq -> 215
    //   208: aload_3
    //   209: invokevirtual cancelTransaction : ()V
    //   212: goto -> 219
    //   215: aload_3
    //   216: invokevirtual completeTransaction : ()V
    //   219: iload #7
    //   221: iconst_1
    //   222: iadd
    //   223: istore #5
    //   225: iload #6
    //   227: istore #4
    //   229: goto -> 24
    //   232: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2086	-> 0
    //   #2087	-> 21
    //   #2088	-> 31
    //   #2089	-> 44
    //   #2090	-> 55
    //   #2091	-> 65
    //   #2092	-> 86
    //   #2093	-> 90
    //   #2096	-> 101
    //   #2097	-> 120
    //   #2098	-> 144
    //   #2099	-> 154
    //   #2100	-> 160
    //   #2102	-> 166
    //   #2103	-> 177
    //   #2104	-> 193
    //   #2106	-> 208
    //   #2108	-> 215
    //   #2087	-> 219
    //   #2112	-> 232
  }
  
  private void removeRedundantOperationsAndExecute(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1) {
    if (paramArrayList == null || paramArrayList.isEmpty())
      return; 
    if (paramArrayList1 != null && paramArrayList.size() == paramArrayList1.size()) {
      executePostponedTransaction(paramArrayList, paramArrayList1);
      int i = paramArrayList.size();
      int j = 0;
      for (int k = 0; k < i; k = n + 1, j = m) {
        boolean bool = ((BackStackRecord)paramArrayList.get(k)).mReorderingAllowed;
        int m = j, n = k;
        if (!bool) {
          if (j != k)
            executeOpsTogether(paramArrayList, paramArrayList1, j, k); 
          j = k + 1;
          m = j;
          if (((Boolean)paramArrayList1.get(k)).booleanValue())
            while (true) {
              m = j;
              if (j < i) {
                m = j;
                if (((Boolean)paramArrayList1.get(j)).booleanValue()) {
                  m = j;
                  if (!((BackStackRecord)paramArrayList.get(j)).mReorderingAllowed) {
                    j++;
                    continue;
                  } 
                } 
              } 
              break;
            }  
          executeOpsTogether(paramArrayList, paramArrayList1, k, m);
          k = m;
          n = m - 1;
          m = k;
        } 
      } 
      if (j != i)
        executeOpsTogether(paramArrayList, paramArrayList1, j, i); 
      return;
    } 
    throw new IllegalStateException("Internal error with the back stack records");
  }
  
  private void executeOpsTogether(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2) {
    boolean bool = ((BackStackRecord)paramArrayList.get(paramInt1)).mReorderingAllowed;
    ArrayList<Fragment> arrayList = this.mTmpAddedFragments;
    if (arrayList == null) {
      this.mTmpAddedFragments = new ArrayList<>();
    } else {
      arrayList.clear();
    } 
    this.mTmpAddedFragments.addAll(this.mAdded);
    Fragment fragment = getPrimaryNavigationFragment();
    int i = paramInt1;
    boolean bool1 = false;
    while (true) {
      boolean bool2 = true;
      if (i < paramInt2) {
        BackStackRecord backStackRecord = paramArrayList.get(i);
        boolean bool3 = ((Boolean)paramArrayList1.get(i)).booleanValue();
        if (!bool3) {
          fragment = backStackRecord.expandOps(this.mTmpAddedFragments, fragment);
        } else {
          backStackRecord.trackAddedFragmentsInPop(this.mTmpAddedFragments);
        } 
        boolean bool4 = bool2;
        if (!bool1)
          if (backStackRecord.mAddToBackStack) {
            bool4 = bool2;
          } else {
            bool4 = false;
          }  
        i++;
        bool1 = bool4;
        continue;
      } 
      break;
    } 
    this.mTmpAddedFragments.clear();
    if (!bool)
      FragmentTransition.startTransitions(this, paramArrayList, paramArrayList1, paramInt1, paramInt2, false); 
    executeOps(paramArrayList, paramArrayList1, paramInt1, paramInt2);
    int j = paramInt2;
    if (bool) {
      ArraySet<Fragment> arraySet = new ArraySet();
      addAddedFragments(arraySet);
      j = postponePostponableTransactions(paramArrayList, paramArrayList1, paramInt1, paramInt2, arraySet);
      makeRemovedFragmentsInvisible(arraySet);
    } 
    if (j != paramInt1 && bool) {
      FragmentTransition.startTransitions(this, paramArrayList, paramArrayList1, paramInt1, j, true);
      moveToState(this.mCurState, true);
    } 
    for (; paramInt1 < paramInt2; paramInt1++) {
      BackStackRecord backStackRecord = paramArrayList.get(paramInt1);
      boolean bool2 = ((Boolean)paramArrayList1.get(paramInt1)).booleanValue();
      if (bool2 && backStackRecord.mIndex >= 0) {
        freeBackStackIndex(backStackRecord.mIndex);
        backStackRecord.mIndex = -1;
      } 
      backStackRecord.runOnCommitRunnables();
    } 
    if (bool1)
      reportBackStackChanged(); 
  }
  
  private void makeRemovedFragmentsInvisible(ArraySet<Fragment> paramArraySet) {
    int i = paramArraySet.size();
    for (byte b = 0; b < i; b++) {
      Fragment fragment = (Fragment)paramArraySet.valueAt(b);
      if (!fragment.mAdded) {
        View view = fragment.getView();
        view.setTransitionAlpha(0.0F);
      } 
    } 
  }
  
  private int postponePostponableTransactions(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2, ArraySet<Fragment> paramArraySet) {
    int i = paramInt2;
    for (int j = paramInt2 - 1; j >= paramInt1; j--, i = k) {
      boolean bool1;
      BackStackRecord backStackRecord = paramArrayList.get(j);
      boolean bool = ((Boolean)paramArrayList1.get(j)).booleanValue();
      if (backStackRecord.isPostponed() && 
        !backStackRecord.interactsWith(paramArrayList, j + 1, paramInt2)) {
        bool1 = true;
      } else {
        bool1 = false;
      } 
      int k = i;
      if (bool1) {
        if (this.mPostponedTransactions == null)
          this.mPostponedTransactions = new ArrayList<>(); 
        StartEnterTransitionListener startEnterTransitionListener = new StartEnterTransitionListener(backStackRecord, bool);
        this.mPostponedTransactions.add(startEnterTransitionListener);
        backStackRecord.setOnStartPostponedListener(startEnterTransitionListener);
        if (bool) {
          backStackRecord.executeOps();
        } else {
          backStackRecord.executePopOps(false);
        } 
        k = i - 1;
        if (j != k) {
          paramArrayList.remove(j);
          paramArrayList.add(k, backStackRecord);
        } 
        addAddedFragments(paramArraySet);
      } 
    } 
    return i;
  }
  
  private void completeExecute(BackStackRecord paramBackStackRecord, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    if (paramBoolean1) {
      paramBackStackRecord.executePopOps(paramBoolean3);
    } else {
      paramBackStackRecord.executeOps();
    } 
    ArrayList<BackStackRecord> arrayList = new ArrayList(1);
    ArrayList<Boolean> arrayList1 = new ArrayList(1);
    arrayList.add(paramBackStackRecord);
    arrayList1.add(Boolean.valueOf(paramBoolean1));
    if (paramBoolean2)
      FragmentTransition.startTransitions(this, arrayList, arrayList1, 0, 1, true); 
    if (paramBoolean3)
      moveToState(this.mCurState, true); 
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray != null) {
      int i = sparseArray.size();
      for (byte b = 0; b < i; b++) {
        Fragment fragment = (Fragment)this.mActive.valueAt(b);
        if (fragment != null && fragment.mView != null && fragment.mIsNewlyAdded) {
          int j = fragment.mContainerId;
          if (paramBackStackRecord.interactsWith(j))
            fragment.mIsNewlyAdded = false; 
        } 
      } 
    } 
  }
  
  private Fragment findFragmentUnder(Fragment paramFragment) {
    ViewGroup viewGroup = paramFragment.mContainer;
    View view = paramFragment.mView;
    if (viewGroup == null || view == null)
      return null; 
    int i = this.mAdded.indexOf(paramFragment);
    for (; --i >= 0; i--) {
      paramFragment = this.mAdded.get(i);
      if (paramFragment.mContainer == viewGroup && paramFragment.mView != null)
        return paramFragment; 
    } 
    return null;
  }
  
  private static void executeOps(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, int paramInt1, int paramInt2) {
    for (; paramInt1 < paramInt2; paramInt1++) {
      BackStackRecord backStackRecord = paramArrayList.get(paramInt1);
      boolean bool = ((Boolean)paramArrayList1.get(paramInt1)).booleanValue();
      boolean bool1 = true;
      if (bool) {
        backStackRecord.bumpBackStackNesting(-1);
        if (paramInt1 != paramInt2 - 1)
          bool1 = false; 
        backStackRecord.executePopOps(bool1);
      } else {
        backStackRecord.bumpBackStackNesting(1);
        backStackRecord.executeOps();
      } 
    } 
  }
  
  private void addAddedFragments(ArraySet<Fragment> paramArraySet) {
    int i = this.mCurState;
    if (i < 1)
      return; 
    int j = Math.min(i, 4);
    int k = this.mAdded.size();
    for (i = 0; i < k; i++) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment.mState < j) {
        moveToState(fragment, j, fragment.getNextAnim(), fragment.getNextTransition(), false);
        if (fragment.mView != null && !fragment.mHidden && fragment.mIsNewlyAdded)
          paramArraySet.add(fragment); 
      } 
    } 
  }
  
  private void forcePostponedTransactions() {
    if (this.mPostponedTransactions != null)
      while (!this.mPostponedTransactions.isEmpty())
        ((StartEnterTransitionListener)this.mPostponedTransactions.remove(0)).completeTransaction();  
  }
  
  private void endAnimatingAwayFragments() {
    int i;
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray == null) {
      i = 0;
    } else {
      i = sparseArray.size();
    } 
    for (byte b = 0; b < i; b++) {
      Fragment fragment = (Fragment)this.mActive.valueAt(b);
      if (fragment != null && fragment.getAnimatingAway() != null)
        fragment.getAnimatingAway().end(); 
    } 
  }
  
  private boolean generateOpsForPendingActions(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_3
    //   2: aload_0
    //   3: monitorenter
    //   4: aload_0
    //   5: getfield mPendingActions : Ljava/util/ArrayList;
    //   8: ifnull -> 96
    //   11: aload_0
    //   12: getfield mPendingActions : Ljava/util/ArrayList;
    //   15: invokevirtual size : ()I
    //   18: ifne -> 24
    //   21: goto -> 96
    //   24: aload_0
    //   25: getfield mPendingActions : Ljava/util/ArrayList;
    //   28: invokevirtual size : ()I
    //   31: istore #4
    //   33: iconst_0
    //   34: istore #5
    //   36: iload #5
    //   38: iload #4
    //   40: if_icmpge -> 71
    //   43: iload_3
    //   44: aload_0
    //   45: getfield mPendingActions : Ljava/util/ArrayList;
    //   48: iload #5
    //   50: invokevirtual get : (I)Ljava/lang/Object;
    //   53: checkcast android/app/FragmentManagerImpl$OpGenerator
    //   56: aload_1
    //   57: aload_2
    //   58: invokeinterface generateOps : (Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
    //   63: ior
    //   64: istore_3
    //   65: iinc #5, 1
    //   68: goto -> 36
    //   71: aload_0
    //   72: getfield mPendingActions : Ljava/util/ArrayList;
    //   75: invokevirtual clear : ()V
    //   78: aload_0
    //   79: getfield mHost : Landroid/app/FragmentHostCallback;
    //   82: invokevirtual getHandler : ()Landroid/os/Handler;
    //   85: aload_0
    //   86: getfield mExecCommit : Ljava/lang/Runnable;
    //   89: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)V
    //   92: aload_0
    //   93: monitorexit
    //   94: iload_3
    //   95: ireturn
    //   96: aload_0
    //   97: monitorexit
    //   98: iconst_0
    //   99: ireturn
    //   100: astore_1
    //   101: aload_0
    //   102: monitorexit
    //   103: aload_1
    //   104: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2477	-> 0
    //   #2478	-> 2
    //   #2479	-> 4
    //   #2483	-> 24
    //   #2484	-> 33
    //   #2485	-> 43
    //   #2484	-> 65
    //   #2487	-> 71
    //   #2488	-> 78
    //   #2489	-> 92
    //   #2490	-> 94
    //   #2480	-> 96
    //   #2489	-> 100
    // Exception table:
    //   from	to	target	type
    //   4	21	100	finally
    //   24	33	100	finally
    //   43	65	100	finally
    //   71	78	100	finally
    //   78	92	100	finally
    //   92	94	100	finally
    //   96	98	100	finally
    //   101	103	100	finally
  }
  
  void doPendingDeferredStart() {
    if (this.mHavePendingDeferredStart) {
      boolean bool = false;
      for (byte b = 0; b < this.mActive.size(); b++, bool = bool1) {
        Fragment fragment = (Fragment)this.mActive.valueAt(b);
        boolean bool1 = bool;
        if (fragment != null) {
          bool1 = bool;
          if (fragment.mLoaderManager != null)
            bool1 = bool | fragment.mLoaderManager.hasRunningLoaders(); 
        } 
      } 
      if (!bool) {
        this.mHavePendingDeferredStart = false;
        startPendingDeferredFragments();
      } 
    } 
  }
  
  void reportBackStackChanged() {
    if (this.mBackStackChangeListeners != null)
      for (byte b = 0; b < this.mBackStackChangeListeners.size(); b++)
        ((FragmentManager.OnBackStackChangedListener)this.mBackStackChangeListeners.get(b)).onBackStackChanged();  
  }
  
  void addBackStackState(BackStackRecord paramBackStackRecord) {
    if (this.mBackStack == null)
      this.mBackStack = new ArrayList<>(); 
    this.mBackStack.add(paramBackStackRecord);
  }
  
  boolean popBackStackState(ArrayList<BackStackRecord> paramArrayList, ArrayList<Boolean> paramArrayList1, String paramString, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBackStack : Ljava/util/ArrayList;
    //   4: astore #6
    //   6: aload #6
    //   8: ifnonnull -> 13
    //   11: iconst_0
    //   12: ireturn
    //   13: aload_3
    //   14: ifnonnull -> 74
    //   17: iload #4
    //   19: ifge -> 74
    //   22: iload #5
    //   24: iconst_1
    //   25: iand
    //   26: ifne -> 74
    //   29: aload #6
    //   31: invokevirtual size : ()I
    //   34: iconst_1
    //   35: isub
    //   36: istore #4
    //   38: iload #4
    //   40: ifge -> 45
    //   43: iconst_0
    //   44: ireturn
    //   45: aload_1
    //   46: aload_0
    //   47: getfield mBackStack : Ljava/util/ArrayList;
    //   50: iload #4
    //   52: invokevirtual remove : (I)Ljava/lang/Object;
    //   55: checkcast android/app/BackStackRecord
    //   58: invokevirtual add : (Ljava/lang/Object;)Z
    //   61: pop
    //   62: aload_2
    //   63: iconst_1
    //   64: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   67: invokevirtual add : (Ljava/lang/Object;)Z
    //   70: pop
    //   71: goto -> 317
    //   74: iconst_m1
    //   75: istore #7
    //   77: aload_3
    //   78: ifnonnull -> 86
    //   81: iload #4
    //   83: iflt -> 251
    //   86: aload_0
    //   87: getfield mBackStack : Ljava/util/ArrayList;
    //   90: invokevirtual size : ()I
    //   93: iconst_1
    //   94: isub
    //   95: istore #8
    //   97: iload #8
    //   99: iflt -> 159
    //   102: aload_0
    //   103: getfield mBackStack : Ljava/util/ArrayList;
    //   106: iload #8
    //   108: invokevirtual get : (I)Ljava/lang/Object;
    //   111: checkcast android/app/BackStackRecord
    //   114: astore #6
    //   116: aload_3
    //   117: ifnull -> 135
    //   120: aload_3
    //   121: aload #6
    //   123: invokevirtual getName : ()Ljava/lang/String;
    //   126: invokevirtual equals : (Ljava/lang/Object;)Z
    //   129: ifeq -> 135
    //   132: goto -> 159
    //   135: iload #4
    //   137: iflt -> 153
    //   140: iload #4
    //   142: aload #6
    //   144: getfield mIndex : I
    //   147: if_icmpne -> 153
    //   150: goto -> 159
    //   153: iinc #8, -1
    //   156: goto -> 97
    //   159: iload #8
    //   161: ifge -> 166
    //   164: iconst_0
    //   165: ireturn
    //   166: iload #8
    //   168: istore #7
    //   170: iload #5
    //   172: iconst_1
    //   173: iand
    //   174: ifeq -> 251
    //   177: iload #8
    //   179: iconst_1
    //   180: isub
    //   181: istore #5
    //   183: iload #5
    //   185: istore #7
    //   187: iload #5
    //   189: iflt -> 251
    //   192: aload_0
    //   193: getfield mBackStack : Ljava/util/ArrayList;
    //   196: iload #5
    //   198: invokevirtual get : (I)Ljava/lang/Object;
    //   201: checkcast android/app/BackStackRecord
    //   204: astore #6
    //   206: aload_3
    //   207: ifnull -> 222
    //   210: aload_3
    //   211: aload #6
    //   213: invokevirtual getName : ()Ljava/lang/String;
    //   216: invokevirtual equals : (Ljava/lang/Object;)Z
    //   219: ifne -> 245
    //   222: iload #5
    //   224: istore #7
    //   226: iload #4
    //   228: iflt -> 251
    //   231: iload #5
    //   233: istore #7
    //   235: iload #4
    //   237: aload #6
    //   239: getfield mIndex : I
    //   242: if_icmpne -> 251
    //   245: iinc #5, -1
    //   248: goto -> 183
    //   251: iload #7
    //   253: aload_0
    //   254: getfield mBackStack : Ljava/util/ArrayList;
    //   257: invokevirtual size : ()I
    //   260: iconst_1
    //   261: isub
    //   262: if_icmpne -> 267
    //   265: iconst_0
    //   266: ireturn
    //   267: aload_0
    //   268: getfield mBackStack : Ljava/util/ArrayList;
    //   271: invokevirtual size : ()I
    //   274: iconst_1
    //   275: isub
    //   276: istore #4
    //   278: iload #4
    //   280: iload #7
    //   282: if_icmple -> 317
    //   285: aload_1
    //   286: aload_0
    //   287: getfield mBackStack : Ljava/util/ArrayList;
    //   290: iload #4
    //   292: invokevirtual remove : (I)Ljava/lang/Object;
    //   295: checkcast android/app/BackStackRecord
    //   298: invokevirtual add : (Ljava/lang/Object;)Z
    //   301: pop
    //   302: aload_2
    //   303: iconst_1
    //   304: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   307: invokevirtual add : (Ljava/lang/Object;)Z
    //   310: pop
    //   311: iinc #4, -1
    //   314: goto -> 278
    //   317: iconst_1
    //   318: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2526	-> 0
    //   #2527	-> 11
    //   #2529	-> 13
    //   #2530	-> 29
    //   #2531	-> 38
    //   #2532	-> 43
    //   #2534	-> 45
    //   #2535	-> 62
    //   #2536	-> 71
    //   #2537	-> 74
    //   #2538	-> 77
    //   #2541	-> 86
    //   #2542	-> 97
    //   #2543	-> 102
    //   #2544	-> 116
    //   #2545	-> 132
    //   #2547	-> 135
    //   #2548	-> 150
    //   #2550	-> 153
    //   #2551	-> 156
    //   #2552	-> 159
    //   #2553	-> 164
    //   #2555	-> 166
    //   #2556	-> 177
    //   #2558	-> 183
    //   #2559	-> 192
    //   #2560	-> 206
    //   #2562	-> 245
    //   #2563	-> 248
    //   #2569	-> 251
    //   #2570	-> 265
    //   #2572	-> 267
    //   #2573	-> 285
    //   #2574	-> 302
    //   #2572	-> 311
    //   #2577	-> 317
  }
  
  FragmentManagerNonConfig retainNonConfig() {
    setRetaining(this.mSavedNonConfig);
    return this.mSavedNonConfig;
  }
  
  private static void setRetaining(FragmentManagerNonConfig paramFragmentManagerNonConfig) {
    if (paramFragmentManagerNonConfig == null)
      return; 
    List<Fragment> list1 = paramFragmentManagerNonConfig.getFragments();
    if (list1 != null)
      for (Fragment fragment : list1)
        fragment.mRetaining = true;  
    List<FragmentManagerNonConfig> list = paramFragmentManagerNonConfig.getChildNonConfigs();
    if (list != null)
      for (FragmentManagerNonConfig fragmentManagerNonConfig : list)
        setRetaining(fragmentManagerNonConfig);  
  }
  
  void saveNonConfig() {
    ArrayList<Fragment> arrayList1 = null, arrayList2 = null;
    ArrayList<Fragment> arrayList3 = null, arrayList4 = null;
    if (this.mActive != null) {
      byte b = 0;
      while (true) {
        arrayList1 = arrayList2;
        arrayList3 = arrayList4;
        if (b < this.mActive.size()) {
          Fragment fragment = (Fragment)this.mActive.valueAt(b);
          ArrayList<Fragment> arrayList = arrayList2;
          arrayList3 = arrayList4;
          if (fragment != null) {
            FragmentManagerNonConfig fragmentManagerNonConfig;
            arrayList1 = arrayList2;
            if (fragment.mRetainInstance) {
              byte b1;
              arrayList3 = arrayList2;
              if (arrayList2 == null)
                arrayList3 = new ArrayList(); 
              arrayList3.add(fragment);
              if (fragment.mTarget != null) {
                b1 = fragment.mTarget.mIndex;
              } else {
                b1 = -1;
              } 
              fragment.mTargetIndex = b1;
              arrayList1 = arrayList3;
              if (DEBUG) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("retainNonConfig: keeping retained ");
                stringBuilder.append(fragment);
                Log.v("FragmentManager", stringBuilder.toString());
                arrayList1 = arrayList3;
              } 
            } 
            if (fragment.mChildFragmentManager != null) {
              fragment.mChildFragmentManager.saveNonConfig();
              fragmentManagerNonConfig = fragment.mChildFragmentManager.mSavedNonConfig;
            } else {
              fragmentManagerNonConfig = ((Fragment)fragmentManagerNonConfig).mChildNonConfig;
            } 
            arrayList2 = arrayList4;
            if (arrayList4 == null) {
              arrayList2 = arrayList4;
              if (fragmentManagerNonConfig != null) {
                arrayList4 = new ArrayList<>(this.mActive.size());
                byte b1 = 0;
                while (true) {
                  arrayList2 = arrayList4;
                  if (b1 < b) {
                    arrayList4.add(null);
                    b1++;
                    continue;
                  } 
                  break;
                } 
              } 
            } 
            arrayList = arrayList1;
            arrayList3 = arrayList2;
            if (arrayList2 != null) {
              arrayList2.add(fragmentManagerNonConfig);
              arrayList3 = arrayList2;
              arrayList = arrayList1;
            } 
          } 
          b++;
          arrayList2 = arrayList;
          arrayList4 = arrayList3;
          continue;
        } 
        break;
      } 
    } 
    if (arrayList1 == null && arrayList3 == null) {
      this.mSavedNonConfig = null;
    } else {
      this.mSavedNonConfig = new FragmentManagerNonConfig(arrayList1, (List)arrayList3);
    } 
  }
  
  void saveFragmentViewState(Fragment paramFragment) {
    if (paramFragment.mView == null)
      return; 
    SparseArray<Parcelable> sparseArray = this.mStateArray;
    if (sparseArray == null) {
      this.mStateArray = new SparseArray();
    } else {
      sparseArray.clear();
    } 
    paramFragment.mView.saveHierarchyState(this.mStateArray);
    if (this.mStateArray.size() > 0) {
      paramFragment.mSavedViewState = this.mStateArray;
      this.mStateArray = null;
    } 
  }
  
  Bundle saveFragmentBasicState(Fragment paramFragment) {
    Bundle bundle1 = null;
    if (this.mStateBundle == null)
      this.mStateBundle = new Bundle(); 
    paramFragment.performSaveInstanceState(this.mStateBundle);
    dispatchOnFragmentSaveInstanceState(paramFragment, this.mStateBundle, false);
    if (!this.mStateBundle.isEmpty()) {
      bundle1 = this.mStateBundle;
      this.mStateBundle = null;
    } 
    if (paramFragment.mView != null)
      saveFragmentViewState(paramFragment); 
    Bundle bundle2 = bundle1;
    if (paramFragment.mSavedViewState != null) {
      bundle2 = bundle1;
      if (bundle1 == null)
        bundle2 = new Bundle(); 
      bundle2.putSparseParcelableArray("android:view_state", paramFragment.mSavedViewState);
    } 
    bundle1 = bundle2;
    if (!paramFragment.mUserVisibleHint) {
      bundle1 = bundle2;
      if (bundle2 == null)
        bundle1 = new Bundle(); 
      bundle1.putBoolean("android:user_visible_hint", paramFragment.mUserVisibleHint);
    } 
    return bundle1;
  }
  
  Parcelable saveAllState() {
    String str1, str2;
    forcePostponedTransactions();
    endAnimatingAwayFragments();
    execPendingActions();
    this.mStateSaved = true;
    this.mSavedNonConfig = null;
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray == null || sparseArray.size() <= 0)
      return null; 
    int i = this.mActive.size();
    FragmentState[] arrayOfFragmentState = new FragmentState[i];
    int j = 0;
    byte b;
    for (b = 0; b < i; b++) {
      Fragment fragment1 = (Fragment)this.mActive.valueAt(b);
      if (fragment1 != null) {
        if (fragment1.mIndex < 0) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Failure saving state: active ");
          stringBuilder.append(fragment1);
          stringBuilder.append(" has cleared index: ");
          stringBuilder.append(fragment1.mIndex);
          throwException(new IllegalStateException(stringBuilder.toString()));
        } 
        byte b1 = 1;
        FragmentState fragmentState = new FragmentState(fragment1);
        arrayOfFragmentState[b] = fragmentState;
        if (fragment1.mState > 0 && fragmentState.mSavedFragmentState == null) {
          fragmentState.mSavedFragmentState = saveFragmentBasicState(fragment1);
          if (fragment1.mTarget != null) {
            if (fragment1.mTarget.mIndex < 0) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("Failure saving state: ");
              stringBuilder.append(fragment1);
              stringBuilder.append(" has target not in fragment manager: ");
              stringBuilder.append(fragment1.mTarget);
              throwException(new IllegalStateException(stringBuilder.toString()));
            } 
            if (fragmentState.mSavedFragmentState == null)
              fragmentState.mSavedFragmentState = new Bundle(); 
            putFragment(fragmentState.mSavedFragmentState, "android:target_state", fragment1.mTarget);
            if (fragment1.mTargetRequestCode != 0)
              fragmentState.mSavedFragmentState.putInt("android:target_req_state", fragment1.mTargetRequestCode); 
          } 
        } else {
          fragmentState.mSavedFragmentState = fragment1.mSavedFragmentState;
        } 
        j = b1;
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Saved state of ");
          stringBuilder.append(fragment1);
          stringBuilder.append(": ");
          stringBuilder.append(fragmentState.mSavedFragmentState);
          Log.v("FragmentManager", stringBuilder.toString());
          j = b1;
        } 
      } 
    } 
    if (!j) {
      if (DEBUG)
        Log.v("FragmentManager", "saveAllState: no fragments!"); 
      return null;
    } 
    sparseArray = null;
    BackStackState[] arrayOfBackStackState2 = null;
    j = this.mAdded.size();
    if (j > 0) {
      int[] arrayOfInt = new int[j];
      b = 0;
      while (true) {
        int[] arrayOfInt1 = arrayOfInt;
        if (b < j) {
          arrayOfInt[b] = ((Fragment)this.mAdded.get(b)).mIndex;
          if (arrayOfInt[b] < 0) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Failure saving state: active ");
            ArrayList<Fragment> arrayList1 = this.mAdded;
            stringBuilder.append(arrayList1.get(b));
            stringBuilder.append(" has cleared index: ");
            stringBuilder.append(arrayOfInt[b]);
            IllegalStateException illegalStateException = new IllegalStateException(stringBuilder.toString());
            throwException(illegalStateException);
          } 
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("saveAllState: adding fragment #");
            stringBuilder.append(b);
            stringBuilder.append(": ");
            ArrayList<Fragment> arrayList1 = this.mAdded;
            stringBuilder.append(arrayList1.get(b));
            str1 = stringBuilder.toString();
            Log.v("FragmentManager", str1);
          } 
          b++;
          continue;
        } 
        break;
      } 
    } 
    ArrayList<BackStackRecord> arrayList = this.mBackStack;
    BackStackState[] arrayOfBackStackState1 = arrayOfBackStackState2;
    if (arrayList != null) {
      j = arrayList.size();
      arrayOfBackStackState1 = arrayOfBackStackState2;
      if (j > 0) {
        arrayOfBackStackState2 = new BackStackState[j];
        b = 0;
        while (true) {
          arrayOfBackStackState1 = arrayOfBackStackState2;
          if (b < j) {
            arrayOfBackStackState2[b] = new BackStackState(this, this.mBackStack.get(b));
            if (DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("saveAllState: adding back stack #");
              stringBuilder.append(b);
              stringBuilder.append(": ");
              ArrayList<BackStackRecord> arrayList1 = this.mBackStack;
              stringBuilder.append(arrayList1.get(b));
              str2 = stringBuilder.toString();
              Log.v("FragmentManager", str2);
            } 
            b++;
            continue;
          } 
          break;
        } 
      } 
    } 
    FragmentManagerState fragmentManagerState = new FragmentManagerState();
    fragmentManagerState.mActive = arrayOfFragmentState;
    fragmentManagerState.mAdded = (int[])str1;
    fragmentManagerState.mBackStack = (BackStackState[])str2;
    fragmentManagerState.mNextFragmentIndex = this.mNextFragmentIndex;
    Fragment fragment = this.mPrimaryNav;
    if (fragment != null)
      fragmentManagerState.mPrimaryNavActiveIndex = fragment.mIndex; 
    saveNonConfig();
    return fragmentManagerState;
  }
  
  void restoreAllState(Parcelable paramParcelable, FragmentManagerNonConfig paramFragmentManagerNonConfig) {
    Fragment fragment;
    if (paramParcelable == null)
      return; 
    FragmentManagerState fragmentManagerState = (FragmentManagerState)paramParcelable;
    if (fragmentManagerState.mActive == null)
      return; 
    paramParcelable = null;
    if (paramFragmentManagerNonConfig != null) {
      byte b1;
      List<Fragment> list = paramFragmentManagerNonConfig.getFragments();
      List<FragmentManagerNonConfig> list1 = paramFragmentManagerNonConfig.getChildNonConfigs();
      if (list != null) {
        b1 = list.size();
      } else {
        b1 = 0;
      } 
      byte b2 = 0;
      while (true) {
        List<FragmentManagerNonConfig> list2 = list1;
        if (b2 < b1) {
          fragment = list.get(b2);
          if (DEBUG) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("restoreAllState: re-attaching retained ");
            stringBuilder.append(fragment);
            Log.v("FragmentManager", stringBuilder.toString());
          } 
          byte b = 0;
          while (b < fragmentManagerState.mActive.length && (fragmentManagerState.mActive[b]).mIndex != fragment.mIndex)
            b++; 
          if (b == fragmentManagerState.mActive.length) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Could not find active fragment with index ");
            stringBuilder.append(fragment.mIndex);
            throwException(new IllegalStateException(stringBuilder.toString()));
          } 
          FragmentState fragmentState = fragmentManagerState.mActive[b];
          fragmentState.mInstance = fragment;
          fragment.mSavedViewState = null;
          fragment.mBackStackNesting = 0;
          fragment.mInLayout = false;
          fragment.mAdded = false;
          fragment.mTarget = null;
          if (fragmentState.mSavedFragmentState != null) {
            fragmentState.mSavedFragmentState.setClassLoader(this.mHost.getContext().getClassLoader());
            fragment.mSavedViewState = fragmentState.mSavedFragmentState.getSparseParcelableArray("android:view_state");
            fragment.mSavedFragmentState = fragmentState.mSavedFragmentState;
          } 
          b2++;
          continue;
        } 
        break;
      } 
    } 
    this.mActive = new SparseArray(fragmentManagerState.mActive.length);
    int i;
    for (i = 0; i < fragmentManagerState.mActive.length; i++) {
      FragmentState fragmentState = fragmentManagerState.mActive[i];
      if (fragmentState != null) {
        FragmentManagerNonConfig fragmentManagerNonConfig1 = null;
        FragmentManagerNonConfig fragmentManagerNonConfig2 = fragmentManagerNonConfig1;
        if (fragment != null) {
          fragmentManagerNonConfig2 = fragmentManagerNonConfig1;
          if (i < fragment.size())
            fragmentManagerNonConfig2 = fragment.get(i); 
        } 
        Fragment fragment1 = fragmentState.instantiate(this.mHost, this.mContainer, this.mParent, fragmentManagerNonConfig2);
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("restoreAllState: active #");
          stringBuilder.append(i);
          stringBuilder.append(": ");
          stringBuilder.append(fragment1);
          Log.v("FragmentManager", stringBuilder.toString());
        } 
        this.mActive.put(fragment1.mIndex, fragment1);
        fragmentState.mInstance = null;
      } 
    } 
    if (paramFragmentManagerNonConfig != null) {
      List<Fragment> list = paramFragmentManagerNonConfig.getFragments();
      if (list != null) {
        i = list.size();
      } else {
        i = 0;
      } 
      for (byte b = 0; b < i; b++) {
        Fragment fragment1 = list.get(b);
        if (fragment1.mTargetIndex >= 0) {
          fragment1.mTarget = (Fragment)this.mActive.get(fragment1.mTargetIndex);
          if (fragment1.mTarget == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Re-attaching retained fragment ");
            stringBuilder.append(fragment1);
            stringBuilder.append(" target no longer exists: ");
            stringBuilder.append(fragment1.mTargetIndex);
            Log.w("FragmentManager", stringBuilder.toString());
            fragment1.mTarget = null;
          } 
        } 
      } 
    } 
    this.mAdded.clear();
    if (fragmentManagerState.mAdded != null)
      for (i = 0; i < fragmentManagerState.mAdded.length; ) {
        fragment = (Fragment)this.mActive.get(fragmentManagerState.mAdded[i]);
        if (fragment == null) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("No instantiated fragment for index #");
          stringBuilder.append(fragmentManagerState.mAdded[i]);
          throwException(new IllegalStateException(stringBuilder.toString()));
        } 
        fragment.mAdded = true;
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("restoreAllState: added #");
          stringBuilder.append(i);
          stringBuilder.append(": ");
          stringBuilder.append(fragment);
          Log.v("FragmentManager", stringBuilder.toString());
        } 
        if (!this.mAdded.contains(fragment)) {
          synchronized (this.mAdded) {
            this.mAdded.add(fragment);
            i++;
          } 
          continue;
        } 
        throw new IllegalStateException("Already added!");
      }  
    if (fragmentManagerState.mBackStack != null) {
      this.mBackStack = new ArrayList<>(fragmentManagerState.mBackStack.length);
      for (i = 0; i < fragmentManagerState.mBackStack.length; i++) {
        BackStackRecord backStackRecord = fragmentManagerState.mBackStack[i].instantiate(this);
        if (DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("restoreAllState: back stack #");
          stringBuilder.append(i);
          stringBuilder.append(" (index ");
          stringBuilder.append(backStackRecord.mIndex);
          stringBuilder.append("): ");
          stringBuilder.append(backStackRecord);
          Log.v("FragmentManager", stringBuilder.toString());
          LogWriter logWriter = new LogWriter(2, "FragmentManager");
          FastPrintWriter fastPrintWriter = new FastPrintWriter((Writer)logWriter, false, 1024);
          backStackRecord.dump("  ", (PrintWriter)fastPrintWriter, false);
          fastPrintWriter.flush();
        } 
        this.mBackStack.add(backStackRecord);
        if (backStackRecord.mIndex >= 0)
          setBackStackIndex(backStackRecord.mIndex, backStackRecord); 
      } 
    } else {
      this.mBackStack = null;
    } 
    if (fragmentManagerState.mPrimaryNavActiveIndex >= 0)
      this.mPrimaryNav = (Fragment)this.mActive.get(fragmentManagerState.mPrimaryNavActiveIndex); 
    this.mNextFragmentIndex = fragmentManagerState.mNextFragmentIndex;
  }
  
  private void burpActive() {
    SparseArray<Fragment> sparseArray = this.mActive;
    if (sparseArray != null)
      for (int i = sparseArray.size() - 1; i >= 0; i--) {
        if (this.mActive.valueAt(i) == null) {
          sparseArray = this.mActive;
          sparseArray.delete(sparseArray.keyAt(i));
        } 
      }  
  }
  
  public void attachController(FragmentHostCallback<?> paramFragmentHostCallback, FragmentContainer paramFragmentContainer, Fragment paramFragment) {
    if (this.mHost == null) {
      boolean bool;
      this.mHost = paramFragmentHostCallback;
      this.mContainer = paramFragmentContainer;
      this.mParent = paramFragment;
      if (getTargetSdk() <= 25) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mAllowOldReentrantBehavior = bool;
      return;
    } 
    throw new IllegalStateException("Already attached");
  }
  
  int getTargetSdk() {
    FragmentHostCallback<?> fragmentHostCallback = this.mHost;
    if (fragmentHostCallback != null) {
      Context context = fragmentHostCallback.getContext();
      if (context != null) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo != null)
          return applicationInfo.targetSdkVersion; 
      } 
    } 
    return 0;
  }
  
  public void noteStateNotSaved() {
    this.mSavedNonConfig = null;
    this.mStateSaved = false;
    int i = this.mAdded.size();
    for (byte b = 0; b < i; b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null)
        fragment.noteStateNotSaved(); 
    } 
  }
  
  public void dispatchCreate() {
    this.mStateSaved = false;
    dispatchMoveToState(1);
  }
  
  public void dispatchActivityCreated() {
    this.mStateSaved = false;
    dispatchMoveToState(2);
  }
  
  public void dispatchStart() {
    this.mStateSaved = false;
    dispatchMoveToState(4);
  }
  
  public void dispatchResume() {
    this.mStateSaved = false;
    dispatchMoveToState(5);
  }
  
  public void dispatchPause() {
    dispatchMoveToState(4);
  }
  
  public void dispatchStop() {
    dispatchMoveToState(3);
  }
  
  public void dispatchDestroyView() {
    dispatchMoveToState(1);
  }
  
  public void dispatchDestroy() {
    this.mDestroyed = true;
    execPendingActions();
    dispatchMoveToState(0);
    this.mHost = null;
    this.mContainer = null;
    this.mParent = null;
  }
  
  private void dispatchMoveToState(int paramInt) {
    if (this.mAllowOldReentrantBehavior) {
      moveToState(paramInt, false);
    } else {
      try {
        this.mExecutingActions = true;
        moveToState(paramInt, false);
        this.mExecutingActions = false;
        return;
      } finally {
        this.mExecutingActions = false;
      } 
    } 
    execPendingActions();
  }
  
  @Deprecated
  public void dispatchMultiWindowModeChanged(boolean paramBoolean) {
    for (int i = this.mAdded.size() - 1; i >= 0; i--) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment != null)
        fragment.performMultiWindowModeChanged(paramBoolean); 
    } 
  }
  
  public void dispatchMultiWindowModeChanged(boolean paramBoolean, Configuration paramConfiguration) {
    for (int i = this.mAdded.size() - 1; i >= 0; i--) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment != null)
        fragment.performMultiWindowModeChanged(paramBoolean, paramConfiguration); 
    } 
  }
  
  @Deprecated
  public void dispatchPictureInPictureModeChanged(boolean paramBoolean) {
    for (int i = this.mAdded.size() - 1; i >= 0; i--) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment != null)
        fragment.performPictureInPictureModeChanged(paramBoolean); 
    } 
  }
  
  public void dispatchPictureInPictureModeChanged(boolean paramBoolean, Configuration paramConfiguration) {
    for (int i = this.mAdded.size() - 1; i >= 0; i--) {
      Fragment fragment = this.mAdded.get(i);
      if (fragment != null)
        fragment.performPictureInPictureModeChanged(paramBoolean, paramConfiguration); 
    } 
  }
  
  public void dispatchConfigurationChanged(Configuration paramConfiguration) {
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null)
        fragment.performConfigurationChanged(paramConfiguration); 
    } 
  }
  
  public void dispatchLowMemory() {
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null)
        fragment.performLowMemory(); 
    } 
  }
  
  public void dispatchTrimMemory(int paramInt) {
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null)
        fragment.performTrimMemory(paramInt); 
    } 
  }
  
  public boolean dispatchCreateOptionsMenu(Menu paramMenu, MenuInflater paramMenuInflater) {
    if (this.mCurState < 1)
      return false; 
    boolean bool = false;
    ArrayList<Fragment> arrayList = null;
    byte b;
    for (b = 0; b < this.mAdded.size(); b++, bool = bool1, arrayList = arrayList1) {
      Fragment fragment = this.mAdded.get(b);
      boolean bool1 = bool;
      ArrayList<Fragment> arrayList1 = arrayList;
      if (fragment != null) {
        bool1 = bool;
        arrayList1 = arrayList;
        if (fragment.performCreateOptionsMenu(paramMenu, paramMenuInflater)) {
          bool1 = true;
          arrayList1 = arrayList;
          if (arrayList == null)
            arrayList1 = new ArrayList(); 
          arrayList1.add(fragment);
        } 
      } 
    } 
    if (this.mCreatedMenus != null)
      for (b = 0; b < this.mCreatedMenus.size(); b++) {
        Fragment fragment = this.mCreatedMenus.get(b);
        if (arrayList == null || !arrayList.contains(fragment))
          fragment.onDestroyOptionsMenu(); 
      }  
    this.mCreatedMenus = arrayList;
    return bool;
  }
  
  public boolean dispatchPrepareOptionsMenu(Menu paramMenu) {
    if (this.mCurState < 1)
      return false; 
    boolean bool = false;
    for (byte b = 0; b < this.mAdded.size(); b++, bool = bool1) {
      Fragment fragment = this.mAdded.get(b);
      boolean bool1 = bool;
      if (fragment != null) {
        bool1 = bool;
        if (fragment.performPrepareOptionsMenu(paramMenu))
          bool1 = true; 
      } 
    } 
    return bool;
  }
  
  public boolean dispatchOptionsItemSelected(MenuItem paramMenuItem) {
    if (this.mCurState < 1)
      return false; 
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null && 
        fragment.performOptionsItemSelected(paramMenuItem))
        return true; 
    } 
    return false;
  }
  
  public boolean dispatchContextItemSelected(MenuItem paramMenuItem) {
    if (this.mCurState < 1)
      return false; 
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null && 
        fragment.performContextItemSelected(paramMenuItem))
        return true; 
    } 
    return false;
  }
  
  public void dispatchOptionsMenuClosed(Menu paramMenu) {
    if (this.mCurState < 1)
      return; 
    for (byte b = 0; b < this.mAdded.size(); b++) {
      Fragment fragment = this.mAdded.get(b);
      if (fragment != null)
        fragment.performOptionsMenuClosed(paramMenu); 
    } 
  }
  
  public void setPrimaryNavigationFragment(Fragment paramFragment) {
    if (paramFragment == null || (this.mActive.get(paramFragment.mIndex) == paramFragment && (paramFragment.mHost == null || 
      paramFragment.getFragmentManager() == this))) {
      this.mPrimaryNav = paramFragment;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Fragment ");
    stringBuilder.append(paramFragment);
    stringBuilder.append(" is not an active fragment of FragmentManager ");
    stringBuilder.append(this);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Fragment getPrimaryNavigationFragment() {
    return this.mPrimaryNav;
  }
  
  public void registerFragmentLifecycleCallbacks(FragmentManager.FragmentLifecycleCallbacks paramFragmentLifecycleCallbacks, boolean paramBoolean) {
    this.mLifecycleCallbacks.add(new Pair(paramFragmentLifecycleCallbacks, Boolean.valueOf(paramBoolean)));
  }
  
  public void unregisterFragmentLifecycleCallbacks(FragmentManager.FragmentLifecycleCallbacks paramFragmentLifecycleCallbacks) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLifecycleCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: getfield mLifecycleCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
    //   13: invokevirtual size : ()I
    //   16: istore #4
    //   18: iload_3
    //   19: iload #4
    //   21: if_icmpge -> 60
    //   24: aload_0
    //   25: getfield mLifecycleCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
    //   28: iload_3
    //   29: invokevirtual get : (I)Ljava/lang/Object;
    //   32: checkcast android/util/Pair
    //   35: getfield first : Ljava/lang/Object;
    //   38: aload_1
    //   39: if_acmpne -> 54
    //   42: aload_0
    //   43: getfield mLifecycleCallbacks : Ljava/util/concurrent/CopyOnWriteArrayList;
    //   46: iload_3
    //   47: invokevirtual remove : (I)Ljava/lang/Object;
    //   50: pop
    //   51: goto -> 60
    //   54: iinc #3, 1
    //   57: goto -> 18
    //   60: aload_2
    //   61: monitorexit
    //   62: return
    //   63: astore_1
    //   64: aload_2
    //   65: monitorexit
    //   66: aload_1
    //   67: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3244	-> 0
    //   #3245	-> 7
    //   #3246	-> 24
    //   #3247	-> 42
    //   #3248	-> 51
    //   #3245	-> 54
    //   #3251	-> 60
    //   #3252	-> 62
    //   #3251	-> 63
    // Exception table:
    //   from	to	target	type
    //   9	18	63	finally
    //   24	42	63	finally
    //   42	51	63	finally
    //   60	62	63	finally
    //   64	66	63	finally
  }
  
  void dispatchOnFragmentPreAttached(Fragment paramFragment, Context paramContext, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentPreAttached(paramFragment, paramContext, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentPreAttached(this, paramFragment, paramContext); 
    } 
  }
  
  void dispatchOnFragmentAttached(Fragment paramFragment, Context paramContext, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentAttached(paramFragment, paramContext, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentAttached(this, paramFragment, paramContext); 
    } 
  }
  
  void dispatchOnFragmentPreCreated(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentPreCreated(paramFragment, paramBundle, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentPreCreated(this, paramFragment, paramBundle); 
    } 
  }
  
  void dispatchOnFragmentCreated(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentCreated(paramFragment, paramBundle, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentCreated(this, paramFragment, paramBundle); 
    } 
  }
  
  void dispatchOnFragmentActivityCreated(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentActivityCreated(paramFragment, paramBundle, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentActivityCreated(this, paramFragment, paramBundle); 
    } 
  }
  
  void dispatchOnFragmentViewCreated(Fragment paramFragment, View paramView, Bundle paramBundle, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentViewCreated(paramFragment, paramView, paramBundle, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentViewCreated(this, paramFragment, paramView, paramBundle); 
    } 
  }
  
  void dispatchOnFragmentStarted(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentStarted(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentStarted(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentResumed(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentResumed(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentResumed(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentPaused(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentPaused(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentPaused(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentStopped(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentStopped(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentStopped(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentSaveInstanceState(Fragment paramFragment, Bundle paramBundle, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentSaveInstanceState(paramFragment, paramBundle, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentSaveInstanceState(this, paramFragment, paramBundle); 
    } 
  }
  
  void dispatchOnFragmentViewDestroyed(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentViewDestroyed(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentViewDestroyed(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentDestroyed(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentDestroyed(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentDestroyed(this, paramFragment); 
    } 
  }
  
  void dispatchOnFragmentDetached(Fragment paramFragment, boolean paramBoolean) {
    Fragment fragment = this.mParent;
    if (fragment != null) {
      FragmentManager fragmentManager = fragment.getFragmentManager();
      if (fragmentManager instanceof FragmentManagerImpl) {
        fragmentManager = fragmentManager;
        fragmentManager.dispatchOnFragmentDetached(paramFragment, true);
      } 
    } 
    for (Pair<FragmentManager.FragmentLifecycleCallbacks, Boolean> pair : this.mLifecycleCallbacks) {
      if (!paramBoolean || ((Boolean)pair.second).booleanValue())
        ((FragmentManager.FragmentLifecycleCallbacks)pair.first).onFragmentDetached(this, paramFragment); 
    } 
  }
  
  public void invalidateOptionsMenu() {
    FragmentHostCallback<?> fragmentHostCallback = this.mHost;
    if (fragmentHostCallback != null && this.mCurState == 5) {
      fragmentHostCallback.onInvalidateOptionsMenu();
    } else {
      this.mNeedMenuInvalidate = true;
    } 
  }
  
  public static int reverseTransit(int paramInt) {
    boolean bool = false;
    if (paramInt != 4097) {
      if (paramInt != 4099) {
        if (paramInt != 8194) {
          paramInt = bool;
        } else {
          paramInt = 4097;
        } 
      } else {
        paramInt = 4099;
      } 
    } else {
      paramInt = 8194;
    } 
    return paramInt;
  }
  
  public static int transitToStyleIndex(int paramInt, boolean paramBoolean) {
    byte b = -1;
    if (paramInt != 4097) {
      if (paramInt != 4099) {
        if (paramInt != 8194) {
          paramInt = b;
        } else if (paramBoolean) {
          paramInt = 2;
        } else {
          paramInt = 3;
        } 
      } else if (paramBoolean) {
        paramInt = 4;
      } else {
        paramInt = 5;
      } 
    } else if (paramBoolean) {
      paramInt = 0;
    } else {
      paramInt = 1;
    } 
    return paramInt;
  }
  
  public View onCreateView(View paramView, String paramString, Context paramContext, AttributeSet paramAttributeSet) {
    if (!"fragment".equals(paramString))
      return null; 
    String str1 = paramAttributeSet.getAttributeValue(null, "class");
    int[] arrayOfInt = R.styleable.Fragment;
    TypedArray typedArray = paramContext.obtainStyledAttributes(paramAttributeSet, arrayOfInt);
    int i = 0;
    if (str1 == null)
      str1 = typedArray.getString(0); 
    int j = typedArray.getResourceId(1, -1);
    String str2 = typedArray.getString(2);
    typedArray.recycle();
    if (paramView != null)
      i = paramView.getId(); 
    if (i != -1 || j != -1 || str2 != null) {
      Fragment fragment2;
      if (j != -1) {
        Fragment fragment = findFragmentById(j);
      } else {
        paramView = null;
      } 
      View view = paramView;
      if (paramView == null) {
        view = paramView;
        if (str2 != null)
          fragment2 = findFragmentByTag(str2); 
      } 
      Fragment fragment1 = fragment2;
      if (fragment2 == null) {
        fragment1 = fragment2;
        if (i != -1)
          fragment1 = findFragmentById(i); 
      } 
      if (DEBUG) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("onCreateView: id=0x");
        stringBuilder2.append(Integer.toHexString(j));
        stringBuilder2.append(" fname=");
        stringBuilder2.append(str1);
        stringBuilder2.append(" existing=");
        stringBuilder2.append(fragment1);
        String str = stringBuilder2.toString();
        Log.v("FragmentManager", str);
      } 
      if (fragment1 == null) {
        int k;
        fragment1 = this.mContainer.instantiate(paramContext, str1, null);
        fragment1.mFromLayout = true;
        if (j != 0) {
          k = j;
        } else {
          k = i;
        } 
        fragment1.mFragmentId = k;
        fragment1.mContainerId = i;
        fragment1.mTag = str2;
        fragment1.mInLayout = true;
        fragment1.mFragmentManager = this;
        fragment1.mHost = this.mHost;
        fragment1.onInflate(this.mHost.getContext(), paramAttributeSet, fragment1.mSavedFragmentState);
        addFragment(fragment1, true);
      } else if (!fragment1.mInLayout) {
        fragment1.mInLayout = true;
        fragment1.mHost = this.mHost;
        if (!fragment1.mRetaining)
          fragment1.onInflate(this.mHost.getContext(), paramAttributeSet, fragment1.mSavedFragmentState); 
      } else {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(paramAttributeSet.getPositionDescription());
        stringBuilder1.append(": Duplicate id 0x");
        stringBuilder1.append(Integer.toHexString(j));
        stringBuilder1.append(", tag ");
        stringBuilder1.append(str2);
        stringBuilder1.append(", or parent id 0x");
        stringBuilder1.append(Integer.toHexString(i));
        stringBuilder1.append(" with another fragment for ");
        stringBuilder1.append(str1);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      if (this.mCurState < 1 && ((Fragment)stringBuilder1).mFromLayout) {
        moveToState((Fragment)stringBuilder1, 1, 0, 0, false);
      } else {
        moveToState((Fragment)stringBuilder1);
      } 
      if (((Fragment)stringBuilder1).mView != null) {
        if (j != 0)
          ((Fragment)stringBuilder1).mView.setId(j); 
        if (((Fragment)stringBuilder1).mView.getTag() == null)
          ((Fragment)stringBuilder1).mView.setTag(str2); 
        return ((Fragment)stringBuilder1).mView;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Fragment ");
      stringBuilder1.append(str1);
      stringBuilder1.append(" did not create a view.");
      throw new IllegalStateException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramAttributeSet.getPositionDescription());
    stringBuilder.append(": Must specify unique android:id, android:tag, or have a parent with an id for ");
    stringBuilder.append(str1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public View onCreateView(String paramString, Context paramContext, AttributeSet paramAttributeSet) {
    return null;
  }
  
  LayoutInflater.Factory2 getLayoutInflaterFactory() {
    return this;
  }
  
  class PopBackStackState implements OpGenerator {
    final int mFlags;
    
    final int mId;
    
    final String mName;
    
    final FragmentManagerImpl this$0;
    
    public PopBackStackState(String param1String, int param1Int1, int param1Int2) {
      this.mName = param1String;
      this.mId = param1Int1;
      this.mFlags = param1Int2;
    }
    
    public boolean generateOps(ArrayList<BackStackRecord> param1ArrayList, ArrayList<Boolean> param1ArrayList1) {
      if (FragmentManagerImpl.this.mPrimaryNav != null && this.mId < 0 && this.mName == null) {
        FragmentManagerImpl fragmentManagerImpl = FragmentManagerImpl.this.mPrimaryNav.mChildFragmentManager;
        if (fragmentManagerImpl != null && fragmentManagerImpl.popBackStackImmediate())
          return false; 
      } 
      return FragmentManagerImpl.this.popBackStackState(param1ArrayList, param1ArrayList1, this.mName, this.mId, this.mFlags);
    }
  }
  
  class StartEnterTransitionListener implements Fragment.OnStartEnterTransitionListener {
    private final boolean mIsBack;
    
    private int mNumPostponed;
    
    private final BackStackRecord mRecord;
    
    public StartEnterTransitionListener(FragmentManagerImpl this$0, boolean param1Boolean) {
      this.mIsBack = param1Boolean;
      this.mRecord = (BackStackRecord)this$0;
    }
    
    public void onStartEnterTransition() {
      int i = this.mNumPostponed - 1;
      if (i != 0)
        return; 
      this.mRecord.mManager.scheduleCommit();
    }
    
    public void startListening() {
      this.mNumPostponed++;
    }
    
    public boolean isReady() {
      boolean bool;
      if (this.mNumPostponed == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public void completeTransaction() {
      int i = this.mNumPostponed;
      boolean bool = false;
      if (i > 0) {
        i = 1;
      } else {
        i = 0;
      } 
      FragmentManagerImpl fragmentManagerImpl1 = this.mRecord.mManager;
      int j = fragmentManagerImpl1.mAdded.size();
      for (byte b = 0; b < j; b++) {
        Fragment fragment = fragmentManagerImpl1.mAdded.get(b);
        fragment.setOnStartEnterTransitionListener(null);
        if (i != 0 && fragment.isPostponed())
          fragment.startPostponedEnterTransition(); 
      } 
      FragmentManagerImpl fragmentManagerImpl2 = this.mRecord.mManager;
      BackStackRecord backStackRecord = this.mRecord;
      boolean bool1 = this.mIsBack;
      if (i == 0)
        bool = true; 
      fragmentManagerImpl2.completeExecute(backStackRecord, bool1, bool, true);
    }
    
    public void cancelTransaction() {
      this.mRecord.mManager.completeExecute(this.mRecord, this.mIsBack, false, false);
    }
  }
  
  class OpGenerator {
    public abstract boolean generateOps(ArrayList<BackStackRecord> param1ArrayList, ArrayList<Boolean> param1ArrayList1);
  }
}
