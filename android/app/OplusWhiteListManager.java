package android.app;

import android.content.Context;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;

public class OplusWhiteListManager {
  private static final boolean DEBUG = false;
  
  private static final long MAX_PROTECT_SELF_TIMEOUT = 10800000L;
  
  private static final long MIN_PROTECT_SELF_TIMEOUT = 60000L;
  
  private static final String TAG = "OplusWhiteListManager";
  
  public static final int TYPE_DEEPSLEEP = 6;
  
  public static final int TYPE_FILTER_CLEAR = 1;
  
  public static final int TYPE_FILTER_PERMISSION = 2;
  
  public static final int TYPE_FLOATWINDOW = 5;
  
  public static final int TYPE_FLOATWINDOW_DEFAULT_GRANT_BUILDIN = 4;
  
  public static final int TYPE_NO_CLEAR_NOTIFICATION = 10;
  
  public static final int TYPE_SCREENOFF_AUDIO_IN = 9;
  
  public static final int TYPE_SCREENOFF_AUDIO_OUT = 7;
  
  public static final int TYPE_SCREENOFF_POSSIBLE_AUDIO_OUT = 8;
  
  public static final int TYPE_SPECIFIG_PKG_PROTECT = 3;
  
  private Context mContext;
  
  public OplusWhiteListManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public ArrayList<String> getGlobalWhiteList() {
    return getGlobalWhiteList(1);
  }
  
  public ArrayList<String> getGlobalWhiteList(int paramInt) {
    ArrayList<String> arrayList1 = new ArrayList();
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      ArrayList<String> arrayList = oplusActivityManager.getGlobalPkgWhiteList(paramInt);
    } catch (RemoteException remoteException) {}
    ArrayList<String> arrayList2 = arrayList1;
    if (arrayList1 == null)
      arrayList2 = new ArrayList<>(); 
    return arrayList2;
  }
  
  public ArrayList<String> getStageProtectListFromPkg(String paramString, int paramInt) {
    ArrayList<String> arrayList1, arrayList2 = new ArrayList();
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      arrayList1 = oplusActivityManager.getStageProtectListFromPkg(paramString, paramInt);
    } catch (RemoteException remoteException) {
      arrayList1 = arrayList2;
    } 
    arrayList2 = arrayList1;
    if (arrayList1 == null)
      arrayList2 = new ArrayList(); 
    return arrayList2;
  }
  
  public ArrayList<String> getGlobalProcessWhiteList() {
    ArrayList<String> arrayList1 = new ArrayList();
    this.mContext.getPackageManager();
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      ArrayList<String> arrayList = oplusActivityManager.getGlobalProcessWhiteList();
    } catch (RemoteException remoteException) {}
    ArrayList<String> arrayList2 = arrayList1;
    if (arrayList1 == null)
      arrayList2 = new ArrayList<>(); 
    return arrayList2;
  }
  
  public void addStageProtectInfo(String paramString, long paramLong) {
    if (paramString == null || paramString.isEmpty()) {
      Log.e("OplusWhiteListManager", "can't add empty info to protect infos");
      return;
    } 
    if (paramLong < 60000L) {
      Log.e("OplusWhiteListManager", "timeout should be longer than 1 minute in milliseconds, return.");
      return;
    } 
    String str = this.mContext.getPackageName();
    if (!paramString.equals(str)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" try to add ");
      stringBuilder.append(paramString);
      stringBuilder.append(" to protect info, are you sure?");
      Log.w("OplusWhiteListManager", stringBuilder.toString());
    } 
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      oplusActivityManager.addStageProtectInfo(paramString, str, paramLong);
    } catch (RemoteException remoteException) {}
  }
  
  public void removeStageProtectInfo(String paramString) {
    if (paramString == null || paramString.isEmpty()) {
      Log.e("OplusWhiteListManager", "can't add empty info to protect infos");
      return;
    } 
    String str = this.mContext.getPackageName();
    OplusActivityManager oplusActivityManager = new OplusActivityManager();
    try {
      oplusActivityManager.removeStageProtectInfo(paramString, str);
    } catch (RemoteException remoteException) {}
  }
}
