package android.app;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;

public class OplusResolverUxIconDrawableImpl implements IOplusResolverUxIconDrawableManager {
  private String mIconName;
  
  public OplusResolverUxIconDrawableImpl(String paramString) {
    this.mIconName = paramString;
  }
  
  public Drawable getDrawable(PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo) {
    Drawable drawable1 = null;
    try {
      Resources resources = paramPackageManager.getResourcesForApplication(paramApplicationInfo);
      Drawable drawable = OplusUXIconLoader.getLoader().findAppDrawable(paramString, this.mIconName, resources, false, true, false);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      nameNotFoundException.printStackTrace();
    } 
    Drawable drawable2 = drawable1;
    if (drawable1 == null)
      drawable2 = IOplusResolverUxIconDrawableManager.DEFAULT.getDrawable(paramPackageManager, paramString, paramInt, paramApplicationInfo); 
    (OplusUXIconLoader.getLoader()).mOplusUxIconDrawableManager = IOplusResolverUxIconDrawableManager.DEFAULT;
    return drawable2;
  }
}
