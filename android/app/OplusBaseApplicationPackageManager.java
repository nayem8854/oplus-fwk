package android.app;

import android.common.OplusFeatureCache;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.IOplusPackageManager;
import android.content.pm.IPackageDeleteObserver;
import android.content.pm.IPackageManager;
import android.content.pm.OplusPackageManager;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.res.IOplusThemeManager;
import android.content.res.OplusBaseResources;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import android.permission.IPermissionManager;
import android.util.Log;
import com.oplus.content.OplusRuleInfo;
import com.oplus.multiapp.OplusMultiAppManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public abstract class OplusBaseApplicationPackageManager extends PackageManager {
  private static final String TAG = "OplusBaseApplicationPackageManager";
  
  private static HashMap<String, Bitmap> mActivityIconsCache;
  
  private static HashMap<String, Bitmap> mAppIconsCache = new HashMap<>();
  
  private static boolean mIconCacheDirty = false;
  
  private static final int sDefaultFlags = 1024;
  
  private final ContextImpl mContext;
  
  private final IOplusPackageManager mOppoPm;
  
  private final PackageDeleteObserver mPackageDeleleteObserver;
  
  static {
    mActivityIconsCache = new HashMap<>();
    mIconCacheDirty = false;
  }
  
  protected OplusBaseApplicationPackageManager(ContextImpl paramContextImpl, IPackageManager paramIPackageManager, IPermissionManager paramIPermissionManager) {
    this.mPackageDeleleteObserver = new PackageDeleteObserver();
    this.mContext = paramContextImpl;
    this.mOppoPm = new OplusPackageManager(paramContextImpl);
  }
  
  protected void checkAndLogMultiApp(boolean paramBoolean, Context paramContext, Object paramObject, String paramString) {
    if (paramBoolean && paramContext != null && OplusMultiAppManager.getInstance().isMultiAppUserId(paramContext.getUserId())) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("multi app -> ");
      stringBuilder.append(paramString);
      stringBuilder.append(" is null! ");
      stringBuilder.append(paramObject);
      stringBuilder.append(" ,pkg:");
      stringBuilder.append(paramContext.getPackageName());
      String str = stringBuilder.toString();
      Log.i("OplusBaseApplicationPackageManager", str);
    } 
  }
  
  protected Drawable getMultiAppUserBadgedIcon(UserHandle paramUserHandle) {
    Drawable drawable1 = null;
    Drawable drawable2 = drawable1;
    if (paramUserHandle != null) {
      drawable2 = drawable1;
      if (999 == paramUserHandle.getIdentifier())
        drawable2 = this.mContext.getResources().getDrawable(201850903); 
    } 
    return drawable2;
  }
  
  protected int getMultiAppUserBadgeId(UserHandle paramUserHandle, boolean paramBoolean) {
    if (paramUserHandle != null && OplusMultiAppManager.getInstance().isMultiAppUserId(paramUserHandle.getIdentifier())) {
      if (paramBoolean)
        return 201850911; 
      return 201850912;
    } 
    return -1;
  }
  
  public Bitmap getAppIconBitmap(String paramString) {
    try {
      return this.mOppoPm.getAppIconBitmap(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Map<String, Bitmap> getAppIconsCache(boolean paramBoolean) {
    try {
      return this.mOppoPm.getAppIconsCache(paramBoolean);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Drawable getApplicationIconCacheAll(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadIcon(this);
  }
  
  public Drawable getApplicationIconCache(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadIcon(this);
  }
  
  public Drawable getApplicationIconCache(String paramString) throws PackageManager.NameNotFoundException {
    return getApplicationIcon(getApplicationInfo(paramString, 1024));
  }
  
  public Drawable getApplicationIconCacheOrignal(ApplicationInfo paramApplicationInfo) {
    return paramApplicationInfo.loadIcon(this);
  }
  
  public Drawable getApplicationIconCacheOrignal(String paramString) throws PackageManager.NameNotFoundException {
    return getApplicationIcon(getApplicationInfo(paramString, 1024));
  }
  
  public Drawable getActivityIconCache(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    return getActivityInfo(paramComponentName, 1024).loadIcon(this);
  }
  
  public Map<String, Bitmap> getActivityIconsCache(IPackageDeleteObserver paramIPackageDeleteObserver) {
    try {
      return this.mOppoPm.getActivityIconsCache(paramIPackageDeleteObserver);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  class PackageDeleteObserver extends IPackageDeleteObserver.Stub {
    final OplusBaseApplicationPackageManager this$0;
    
    private PackageDeleteObserver() {}
    
    public void packageDeleted(String param1String, int param1Int) {
      if (param1String == null)
        return; 
      try {
        if (OplusBaseApplicationPackageManager.mAppIconsCache.get(param1String) != null)
          OplusBaseApplicationPackageManager.mAppIconsCache.remove(param1String); 
        Iterator<Map.Entry> iterator = OplusBaseApplicationPackageManager.mActivityIconsCache.entrySet().iterator();
        ArrayList<String> arrayList = new ArrayList();
        this();
        while (iterator.hasNext()) {
          Map.Entry entry = iterator.next();
          String str = (String)entry.getKey();
          if (param1String.equals(str.split("/")[0]))
            arrayList.add(str); 
        } 
        for (String param1String : arrayList)
          OplusBaseApplicationPackageManager.mActivityIconsCache.remove(param1String); 
        OplusBaseApplicationPackageManager.access$302(true);
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    }
  }
  
  public Drawable getApplicationIcon(ApplicationInfo paramApplicationInfo) {
    try {
      Bitmap bitmap = getAppIconBitmap(paramApplicationInfo.packageName);
      if (bitmap != null)
        return new BitmapDrawable(this.mContext.getResources(), bitmap); 
    } catch (Exception exception) {
      exception.printStackTrace();
    } 
    return null;
  }
  
  public int oplusFreezePackage(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    try {
      return this.mOppoPm.oplusFreezePackage(paramString, paramInt1, paramInt2, paramInt3, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public int oplusUnFreezePackage(String paramString, int paramInt1, int paramInt2, int paramInt3) {
    try {
      return this.mOppoPm.oplusUnFreezePackage(paramString, paramInt1, paramInt2, paramInt3, this.mContext.getOpPackageName());
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public int getOplusFreezePackageState(String paramString, int paramInt) {
    try {
      return this.mOppoPm.getOplusFreezePackageState(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public boolean inOplusFreezePackageList(String paramString, int paramInt) {
    try {
      return this.mOppoPm.inOplusFreezePackageList(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public List<String> getOplusFreezedPackageList(int paramInt) {
    try {
      return this.mOppoPm.getOplusFreezedPackageList(paramInt);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public int getOplusPackageFreezeFlag(String paramString, int paramInt) {
    try {
      return this.mOppoPm.getOplusPackageFreezeFlag(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  static void oplusConfigurationChanged() {
    if (!mAppIconsCache.isEmpty())
      mAppIconsCache.clear(); 
    if (!mActivityIconsCache.isEmpty())
      mActivityIconsCache.clear(); 
  }
  
  public boolean isSecurePayApp(String paramString) {
    try {
      return this.mOppoPm.isSecurePayApp(paramString);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public boolean setInterceptRuleInfos(List<OplusRuleInfo> paramList) {
    try {
      return this.mOppoPm.setInterceptRuleInfos(paramList);
    } catch (RemoteException remoteException) {
      Log.e("OplusBaseApplicationPackageManager", "setInterceptRuleInfos failed");
      return false;
    } 
  }
  
  public List<OplusRuleInfo> getInterceptRuleInfos() {
    try {
      return this.mOppoPm.getInterceptRuleInfos();
    } catch (RemoteException remoteException) {
      Log.e("OplusBaseApplicationPackageManager", "getInterceptRuleInfos failed");
      return null;
    } 
  }
  
  public boolean isFullFunctionMode() {
    try {
      return this.mOppoPm.isClosedSuperFirewall();
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public boolean isClosedSuperFirewall() {
    try {
      return this.mOppoPm.isClosedSuperFirewall();
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public boolean loadRegionFeature(String paramString) {
    try {
      return this.mOppoPm.loadRegionFeature(paramString);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public FeatureInfo[] getOplusSystemAvailableFeatures() {
    try {
      return this.mOppoPm.getOplusSystemAvailableFeatures();
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public boolean isSystemDataApp(String paramString) {
    try {
      return this.mOppoPm.isSystemDataApp(paramString);
    } catch (RemoteException remoteException) {
      throw new RuntimeException("Package manager has died", remoteException);
    } 
  }
  
  public Drawable loadItemIcon(PackageItemInfo paramPackageItemInfo, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    // Byte code:
    //   0: aload_1
    //   1: getfield showUserIcon : I
    //   4: sipush #-10000
    //   7: if_icmpeq -> 51
    //   10: aload_0
    //   11: invokevirtual getUserManager : ()Landroid/os/UserManager;
    //   14: aload_1
    //   15: getfield showUserIcon : I
    //   18: invokevirtual getUserIcon : (I)Landroid/graphics/Bitmap;
    //   21: astore_2
    //   22: aload_2
    //   23: ifnonnull -> 42
    //   26: aload_0
    //   27: getfield mContext : Landroid/app/ContextImpl;
    //   30: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   33: aload_1
    //   34: getfield showUserIcon : I
    //   37: iconst_0
    //   38: invokestatic getDefaultUserIcon : (Landroid/content/res/Resources;IZ)Landroid/graphics/drawable/Drawable;
    //   41: areturn
    //   42: new android/graphics/drawable/BitmapDrawable
    //   45: dup
    //   46: aload_2
    //   47: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   50: areturn
    //   51: aconst_null
    //   52: astore #4
    //   54: aload_1
    //   55: getfield packageName : Ljava/lang/String;
    //   58: ifnull -> 214
    //   61: iload_3
    //   62: ifeq -> 199
    //   65: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   68: astore #4
    //   70: aload #4
    //   72: iconst_0
    //   73: anewarray java/lang/Object
    //   76: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   79: checkcast android/content/res/IOplusThemeManager
    //   82: aload_0
    //   83: aload_0
    //   84: getfield mContext : Landroid/app/ContextImpl;
    //   87: invokevirtual getPackageName : ()Ljava/lang/String;
    //   90: invokeinterface supportUxOnline : (Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    //   95: ifeq -> 199
    //   98: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   101: iconst_0
    //   102: anewarray java/lang/Object
    //   105: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   108: checkcast android/content/res/IOplusThemeManager
    //   111: aload_0
    //   112: aload_2
    //   113: aload_1
    //   114: getfield packageName : Ljava/lang/String;
    //   117: invokeinterface supportUxIcon : (Landroid/content/pm/PackageManager;Landroid/content/pm/ApplicationInfo;Ljava/lang/String;)Z
    //   122: ifeq -> 177
    //   125: aconst_null
    //   126: astore #4
    //   128: aload_1
    //   129: instanceof android/content/pm/ActivityInfo
    //   132: ifeq -> 141
    //   135: aload_1
    //   136: getfield name : Ljava/lang/String;
    //   139: astore #4
    //   141: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   144: iconst_0
    //   145: anewarray java/lang/Object
    //   148: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   151: checkcast android/content/res/IOplusThemeManager
    //   154: aload_0
    //   155: aload_1
    //   156: getfield packageName : Ljava/lang/String;
    //   159: aload #4
    //   161: aload_1
    //   162: getfield icon : I
    //   165: aload_2
    //   166: iconst_0
    //   167: invokeinterface getDrawableFromUxIcon : (Landroid/content/pm/PackageManager;Ljava/lang/String;Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Z)Landroid/graphics/drawable/Drawable;
    //   172: astore #4
    //   174: goto -> 214
    //   177: aload_0
    //   178: aload_1
    //   179: getfield packageName : Ljava/lang/String;
    //   182: aload_1
    //   183: getfield icon : I
    //   186: aload_2
    //   187: aload_1
    //   188: getfield name : Ljava/lang/String;
    //   191: invokestatic getDrawable : (Landroid/app/OplusBaseApplicationPackageManager;Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    //   194: astore #4
    //   196: goto -> 214
    //   199: aload_0
    //   200: aload_1
    //   201: getfield packageName : Ljava/lang/String;
    //   204: aload_1
    //   205: getfield icon : I
    //   208: aload_2
    //   209: invokevirtual getDrawable : (Ljava/lang/String;ILandroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;
    //   212: astore #4
    //   214: aload #4
    //   216: astore_2
    //   217: aload #4
    //   219: ifnonnull -> 228
    //   222: aload_1
    //   223: aload_0
    //   224: invokevirtual loadDefaultIcon : (Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;
    //   227: astore_2
    //   228: aload_0
    //   229: aload_2
    //   230: new android/os/UserHandle
    //   233: dup
    //   234: aload_0
    //   235: getfield mContext : Landroid/app/ContextImpl;
    //   238: invokevirtual getUserId : ()I
    //   241: invokespecial <init> : (I)V
    //   244: invokevirtual getUserBadgedIcon : (Landroid/graphics/drawable/Drawable;Landroid/os/UserHandle;)Landroid/graphics/drawable/Drawable;
    //   247: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #470	-> 0
    //   #471	-> 10
    //   #472	-> 22
    //   #473	-> 26
    //   #475	-> 42
    //   #477	-> 51
    //   #482	-> 54
    //   #484	-> 61
    //   #485	-> 70
    //   #486	-> 98
    //   #492	-> 125
    //   #493	-> 128
    //   #494	-> 135
    //   #496	-> 141
    //   #499	-> 174
    //   #500	-> 177
    //   #503	-> 199
    //   #506	-> 214
    //   #507	-> 222
    //   #509	-> 228
  }
  
  public Drawable getUxIconDrawable(Drawable paramDrawable, boolean paramBoolean) {
    return ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).getDrawableForApp(paramDrawable, paramBoolean);
  }
  
  public Drawable getUxIconDrawable(String paramString, Drawable paramDrawable, boolean paramBoolean) {
    try {
      Resources resources = getResourcesForApplication(paramString);
      OplusBaseResources oplusBaseResources = getOplusBaseResourcesForThemeHelper(getApplicationInfo(paramString, 1024));
      return ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).getDrawableForApp(resources, oplusBaseResources, paramDrawable, paramBoolean);
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getResourcesForApplication error: ");
      stringBuilder.append(nameNotFoundException.getMessage());
      Log.e("OplusBaseApplicationPackageManager", stringBuilder.toString());
      return paramDrawable;
    } 
  }
  
  public void checkEMMApkRuntimePermission(ComponentName paramComponentName) throws SecurityException {
    String str = paramComponentName.getPackageName();
    if (str != null)
      return; 
    throw new SecurityException("Package name is null");
  }
  
  public List<ApplicationInfo> getIconPackListInner() {
    try {
      return this.mOppoPm.getIconPackList();
    } catch (Exception exception) {
      Log.e("OplusBaseApplicationPackageManager", exception.getMessage(), exception);
      return null;
    } 
  }
  
  public abstract Drawable getCachedIconForThemeHelper(String paramString, int paramInt);
  
  public abstract OplusBaseResources getOplusBaseResourcesForThemeHelper(ApplicationInfo paramApplicationInfo) throws PackageManager.NameNotFoundException;
  
  abstract UserManager getUserManager();
  
  public abstract void putCachedIconForThemeHelper(String paramString, int paramInt, Drawable paramDrawable);
}
