package android.app.compat;

import android.annotation.SystemApi;
import android.compat.Compatibility;
import android.os.UserHandle;

@SystemApi
public final class CompatChanges {
  private static final ChangeIdStateCache QUERY_CACHE = new ChangeIdStateCache();
  
  public static boolean isChangeEnabled(long paramLong) {
    return Compatibility.isChangeEnabled(paramLong);
  }
  
  public static boolean isChangeEnabled(long paramLong, String paramString, UserHandle paramUserHandle) {
    ChangeIdStateCache changeIdStateCache = QUERY_CACHE;
    int i = paramUserHandle.getIdentifier();
    return changeIdStateCache.query(ChangeIdStateQuery.byPackageName(paramLong, paramString, i)).booleanValue();
  }
  
  public static boolean isChangeEnabled(long paramLong, int paramInt) {
    return QUERY_CACHE.query(ChangeIdStateQuery.byUid(paramLong, paramInt)).booleanValue();
  }
}
