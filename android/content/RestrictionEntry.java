package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.Objects;

public class RestrictionEntry implements Parcelable {
  public RestrictionEntry(int paramInt, String paramString) {
    this.mType = paramInt;
    this.mKey = paramString;
  }
  
  public RestrictionEntry(String paramString1, String paramString2) {
    this.mKey = paramString1;
    this.mType = 2;
    this.mCurrentValue = paramString2;
  }
  
  public RestrictionEntry(String paramString, boolean paramBoolean) {
    this.mKey = paramString;
    this.mType = 1;
    setSelectedState(paramBoolean);
  }
  
  public RestrictionEntry(String paramString, String[] paramArrayOfString) {
    this.mKey = paramString;
    this.mType = 4;
    this.mCurrentValues = paramArrayOfString;
  }
  
  public RestrictionEntry(String paramString, int paramInt) {
    this.mKey = paramString;
    this.mType = 5;
    setIntValue(paramInt);
  }
  
  private RestrictionEntry(String paramString, RestrictionEntry[] paramArrayOfRestrictionEntry, boolean paramBoolean) {
    this.mKey = paramString;
    if (paramBoolean) {
      this.mType = 8;
      if (paramArrayOfRestrictionEntry != null) {
        int i;
        byte b;
        for (i = paramArrayOfRestrictionEntry.length, b = 0; b < i; ) {
          RestrictionEntry restrictionEntry = paramArrayOfRestrictionEntry[b];
          if (restrictionEntry.getType() == 7) {
            b++;
            continue;
          } 
          throw new IllegalArgumentException("bundle_array restriction can only have nested restriction entries of type bundle");
        } 
      } 
    } else {
      this.mType = 7;
    } 
    setRestrictions(paramArrayOfRestrictionEntry);
  }
  
  public static RestrictionEntry createBundleEntry(String paramString, RestrictionEntry[] paramArrayOfRestrictionEntry) {
    return new RestrictionEntry(paramString, paramArrayOfRestrictionEntry, false);
  }
  
  public static RestrictionEntry createBundleArrayEntry(String paramString, RestrictionEntry[] paramArrayOfRestrictionEntry) {
    return new RestrictionEntry(paramString, paramArrayOfRestrictionEntry, true);
  }
  
  public void setType(int paramInt) {
    this.mType = paramInt;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public String getSelectedString() {
    return this.mCurrentValue;
  }
  
  public String[] getAllSelectedStrings() {
    return this.mCurrentValues;
  }
  
  public boolean getSelectedState() {
    return Boolean.parseBoolean(this.mCurrentValue);
  }
  
  public int getIntValue() {
    return Integer.parseInt(this.mCurrentValue);
  }
  
  public void setIntValue(int paramInt) {
    this.mCurrentValue = Integer.toString(paramInt);
  }
  
  public void setSelectedString(String paramString) {
    this.mCurrentValue = paramString;
  }
  
  public void setSelectedState(boolean paramBoolean) {
    this.mCurrentValue = Boolean.toString(paramBoolean);
  }
  
  public void setAllSelectedStrings(String[] paramArrayOfString) {
    this.mCurrentValues = paramArrayOfString;
  }
  
  public void setChoiceValues(String[] paramArrayOfString) {
    this.mChoiceValues = paramArrayOfString;
  }
  
  public void setChoiceValues(Context paramContext, int paramInt) {
    this.mChoiceValues = paramContext.getResources().getStringArray(paramInt);
  }
  
  public RestrictionEntry[] getRestrictions() {
    return this.mRestrictions;
  }
  
  public void setRestrictions(RestrictionEntry[] paramArrayOfRestrictionEntry) {
    this.mRestrictions = paramArrayOfRestrictionEntry;
  }
  
  public String[] getChoiceValues() {
    return this.mChoiceValues;
  }
  
  public void setChoiceEntries(String[] paramArrayOfString) {
    this.mChoiceEntries = paramArrayOfString;
  }
  
  public void setChoiceEntries(Context paramContext, int paramInt) {
    this.mChoiceEntries = paramContext.getResources().getStringArray(paramInt);
  }
  
  public String[] getChoiceEntries() {
    return this.mChoiceEntries;
  }
  
  public String getDescription() {
    return this.mDescription;
  }
  
  public void setDescription(String paramString) {
    this.mDescription = paramString;
  }
  
  public String getKey() {
    return this.mKey;
  }
  
  public String getTitle() {
    return this.mTitle;
  }
  
  public void setTitle(String paramString) {
    this.mTitle = paramString;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof RestrictionEntry))
      return false; 
    paramObject = paramObject;
    if (this.mType != ((RestrictionEntry)paramObject).mType || !this.mKey.equals(((RestrictionEntry)paramObject).mKey))
      return false; 
    if (this.mCurrentValues == null && ((RestrictionEntry)paramObject).mCurrentValues == null && this.mRestrictions == null && ((RestrictionEntry)paramObject).mRestrictions == null) {
      String str1 = this.mCurrentValue, str2 = ((RestrictionEntry)paramObject).mCurrentValue;
      if (Objects.equals(str1, str2))
        return true; 
    } 
    if (this.mCurrentValue == null && ((RestrictionEntry)paramObject).mCurrentValue == null && this.mRestrictions == null && ((RestrictionEntry)paramObject).mRestrictions == null) {
      String[] arrayOfString2 = this.mCurrentValues, arrayOfString1 = ((RestrictionEntry)paramObject).mCurrentValues;
      if (Arrays.equals((Object[])arrayOfString2, (Object[])arrayOfString1))
        return true; 
    } 
    String str = this.mCurrentValue;
    if (str == null) {
      String str1 = ((RestrictionEntry)paramObject).mCurrentValue;
      if (str1 == null && str == null && str1 == null) {
        RestrictionEntry[] arrayOfRestrictionEntry = this.mRestrictions;
        paramObject = ((RestrictionEntry)paramObject).mRestrictions;
        if (Arrays.equals((Object[])arrayOfRestrictionEntry, (Object[])paramObject))
          return true; 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int j, i = 17 * 31 + this.mKey.hashCode();
    String str = this.mCurrentValue;
    if (str != null) {
      j = i * 31 + str.hashCode();
    } else {
      String[] arrayOfString = this.mCurrentValues;
      if (arrayOfString != null) {
        int k = arrayOfString.length;
        byte b = 0;
        while (true) {
          j = i;
          if (b < k) {
            str = arrayOfString[b];
            j = i;
            if (str != null)
              j = i * 31 + str.hashCode(); 
            b++;
            i = j;
            continue;
          } 
          break;
        } 
      } else {
        RestrictionEntry[] arrayOfRestrictionEntry = this.mRestrictions;
        j = i;
        if (arrayOfRestrictionEntry != null)
          j = i * 31 + Arrays.hashCode((Object[])arrayOfRestrictionEntry); 
      } 
    } 
    return j;
  }
  
  public RestrictionEntry(Parcel paramParcel) {
    this.mType = paramParcel.readInt();
    this.mKey = paramParcel.readString();
    this.mTitle = paramParcel.readString();
    this.mDescription = paramParcel.readString();
    this.mChoiceEntries = paramParcel.readStringArray();
    this.mChoiceValues = paramParcel.readStringArray();
    this.mCurrentValue = paramParcel.readString();
    this.mCurrentValues = paramParcel.readStringArray();
    Parcelable[] arrayOfParcelable = paramParcel.readParcelableArray(null);
    if (arrayOfParcelable != null) {
      this.mRestrictions = new RestrictionEntry[arrayOfParcelable.length];
      for (byte b = 0; b < arrayOfParcelable.length; b++)
        this.mRestrictions[b] = (RestrictionEntry)arrayOfParcelable[b]; 
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeString(this.mKey);
    paramParcel.writeString(this.mTitle);
    paramParcel.writeString(this.mDescription);
    paramParcel.writeStringArray(this.mChoiceEntries);
    paramParcel.writeStringArray(this.mChoiceValues);
    paramParcel.writeString(this.mCurrentValue);
    paramParcel.writeStringArray(this.mCurrentValues);
    paramParcel.writeParcelableArray((Parcelable[])this.mRestrictions, 0);
  }
  
  public static final Parcelable.Creator<RestrictionEntry> CREATOR = new Parcelable.Creator<RestrictionEntry>() {
      public RestrictionEntry createFromParcel(Parcel param1Parcel) {
        return new RestrictionEntry(param1Parcel);
      }
      
      public RestrictionEntry[] newArray(int param1Int) {
        return new RestrictionEntry[param1Int];
      }
    };
  
  public static final int TYPE_BOOLEAN = 1;
  
  public static final int TYPE_BUNDLE = 7;
  
  public static final int TYPE_BUNDLE_ARRAY = 8;
  
  public static final int TYPE_CHOICE = 2;
  
  public static final int TYPE_CHOICE_LEVEL = 3;
  
  public static final int TYPE_INTEGER = 5;
  
  public static final int TYPE_MULTI_SELECT = 4;
  
  public static final int TYPE_NULL = 0;
  
  public static final int TYPE_STRING = 6;
  
  private String[] mChoiceEntries;
  
  private String[] mChoiceValues;
  
  private String mCurrentValue;
  
  private String[] mCurrentValues;
  
  private String mDescription;
  
  private String mKey;
  
  private RestrictionEntry[] mRestrictions;
  
  private String mTitle;
  
  private int mType;
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RestrictionEntry{mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append(", mKey='");
    stringBuilder.append(this.mKey);
    stringBuilder.append('\'');
    stringBuilder.append(", mTitle='");
    stringBuilder.append(this.mTitle);
    stringBuilder.append('\'');
    stringBuilder.append(", mDescription='");
    stringBuilder.append(this.mDescription);
    stringBuilder.append('\'');
    stringBuilder.append(", mChoiceEntries=");
    String[] arrayOfString = this.mChoiceEntries;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(", mChoiceValues=");
    arrayOfString = this.mChoiceValues;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(", mCurrentValue='");
    stringBuilder.append(this.mCurrentValue);
    stringBuilder.append('\'');
    stringBuilder.append(", mCurrentValues=");
    arrayOfString = this.mCurrentValues;
    stringBuilder.append(Arrays.toString((Object[])arrayOfString));
    stringBuilder.append(", mRestrictions=");
    RestrictionEntry[] arrayOfRestrictionEntry = this.mRestrictions;
    stringBuilder.append(Arrays.toString((Object[])arrayOfRestrictionEntry));
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
}
