package android.content;

import android.accounts.Account;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ISyncAdapter extends IInterface {
  void cancelSync(ISyncContext paramISyncContext) throws RemoteException;
  
  void onUnsyncableAccount(ISyncAdapterUnsyncableAccountCallback paramISyncAdapterUnsyncableAccountCallback) throws RemoteException;
  
  void startSync(ISyncContext paramISyncContext, String paramString, Account paramAccount, Bundle paramBundle) throws RemoteException;
  
  class Default implements ISyncAdapter {
    public void onUnsyncableAccount(ISyncAdapterUnsyncableAccountCallback param1ISyncAdapterUnsyncableAccountCallback) throws RemoteException {}
    
    public void startSync(ISyncContext param1ISyncContext, String param1String, Account param1Account, Bundle param1Bundle) throws RemoteException {}
    
    public void cancelSync(ISyncContext param1ISyncContext) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISyncAdapter {
    private static final String DESCRIPTOR = "android.content.ISyncAdapter";
    
    static final int TRANSACTION_cancelSync = 3;
    
    static final int TRANSACTION_onUnsyncableAccount = 1;
    
    static final int TRANSACTION_startSync = 2;
    
    public Stub() {
      attachInterface(this, "android.content.ISyncAdapter");
    }
    
    public static ISyncAdapter asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.ISyncAdapter");
      if (iInterface != null && iInterface instanceof ISyncAdapter)
        return (ISyncAdapter)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "cancelSync";
        } 
        return "startSync";
      } 
      return "onUnsyncableAccount";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ISyncContext iSyncContext;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.content.ISyncAdapter");
            return true;
          } 
          param1Parcel1.enforceInterface("android.content.ISyncAdapter");
          iSyncContext = ISyncContext.Stub.asInterface(param1Parcel1.readStrongBinder());
          cancelSync(iSyncContext);
          return true;
        } 
        iSyncContext.enforceInterface("android.content.ISyncAdapter");
        ISyncContext iSyncContext1 = ISyncContext.Stub.asInterface(iSyncContext.readStrongBinder());
        String str = iSyncContext.readString();
        if (iSyncContext.readInt() != 0) {
          Account account = (Account)Account.CREATOR.createFromParcel((Parcel)iSyncContext);
        } else {
          param1Parcel2 = null;
        } 
        if (iSyncContext.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)iSyncContext);
        } else {
          iSyncContext = null;
        } 
        startSync(iSyncContext1, str, (Account)param1Parcel2, (Bundle)iSyncContext);
        return true;
      } 
      iSyncContext.enforceInterface("android.content.ISyncAdapter");
      ISyncAdapterUnsyncableAccountCallback iSyncAdapterUnsyncableAccountCallback = ISyncAdapterUnsyncableAccountCallback.Stub.asInterface(iSyncContext.readStrongBinder());
      onUnsyncableAccount(iSyncAdapterUnsyncableAccountCallback);
      return true;
    }
    
    private static class Proxy implements ISyncAdapter {
      public static ISyncAdapter sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.ISyncAdapter";
      }
      
      public void onUnsyncableAccount(ISyncAdapterUnsyncableAccountCallback param2ISyncAdapterUnsyncableAccountCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.content.ISyncAdapter");
          if (param2ISyncAdapterUnsyncableAccountCallback != null) {
            iBinder = param2ISyncAdapterUnsyncableAccountCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && ISyncAdapter.Stub.getDefaultImpl() != null) {
            ISyncAdapter.Stub.getDefaultImpl().onUnsyncableAccount(param2ISyncAdapterUnsyncableAccountCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startSync(ISyncContext param2ISyncContext, String param2String, Account param2Account, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.content.ISyncAdapter");
          if (param2ISyncContext != null) {
            iBinder = param2ISyncContext.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String);
          if (param2Account != null) {
            parcel.writeInt(1);
            param2Account.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && ISyncAdapter.Stub.getDefaultImpl() != null) {
            ISyncAdapter.Stub.getDefaultImpl().startSync(param2ISyncContext, param2String, param2Account, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void cancelSync(ISyncContext param2ISyncContext) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.content.ISyncAdapter");
          if (param2ISyncContext != null) {
            iBinder = param2ISyncContext.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && ISyncAdapter.Stub.getDefaultImpl() != null) {
            ISyncAdapter.Stub.getDefaultImpl().cancelSync(param2ISyncContext);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISyncAdapter param1ISyncAdapter) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISyncAdapter != null) {
          Proxy.sDefaultImpl = param1ISyncAdapter;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISyncAdapter getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
