package android.content;

import android.accounts.Account;
import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.IActivityManager;
import android.app.IUriGrantsManager;
import android.app.UriGrantsManager;
import android.content.pm.PackageManager;
import android.content.pm.ParceledListSlice;
import android.content.res.AssetFileDescriptor;
import android.content.res.Resources;
import android.database.ContentObserver;
import android.database.CrossProcessCursorWrapper;
import android.database.Cursor;
import android.database.IContentObserver;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.ParcelFileDescriptor;
import android.os.RemoteCallback;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemClock;
import android.os.UserHandle;
import android.os.storage.StorageManager;
import android.system.Int32Ref;
import android.text.TextUtils;
import android.util.Log;
import android.util.SeempLog;
import android.util.Size;
import android.util.SparseArray;
import com.android.internal.util.MimeIconUtils;
import com.oplus.multiapp.OplusMultiAppManager;
import dalvik.system.CloseGuard;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class ContentResolver implements ContentInterface {
  public static final Intent ACTION_SYNC_CONN_STATUS_CHANGED;
  
  public static final String ANY_CURSOR_ITEM_TYPE = "vnd.android.cursor.item/*";
  
  public static final int CONTENT_PROVIDER_PUBLISH_TIMEOUT_MILLIS = 10000;
  
  public static final int CONTENT_PROVIDER_READY_TIMEOUT_MILLIS = 20000;
  
  private static final int CONTENT_PROVIDER_TIMEOUT_MILLIS = 3000;
  
  public static final String CONTENT_SERVICE_NAME = "content";
  
  public static final String CURSOR_DIR_BASE_TYPE = "vnd.android.cursor.dir";
  
  public static final String CURSOR_ITEM_BASE_TYPE = "vnd.android.cursor.item";
  
  public static final boolean DEPRECATE_DATA_COLUMNS = StorageManager.hasIsolatedStorage();
  
  public static final String DEPRECATE_DATA_PREFIX = "/mnt/content/";
  
  private static final boolean ENABLE_CONTENT_SAMPLE = false;
  
  public static final String EXTRA_HONORED_ARGS = "android.content.extra.HONORED_ARGS";
  
  public static final String EXTRA_REFRESH_SUPPORTED = "android.content.extra.REFRESH_SUPPORTED";
  
  public static final String EXTRA_SIZE = "android.content.extra.SIZE";
  
  public static final String EXTRA_TOTAL_COUNT = "android.content.extra.TOTAL_COUNT";
  
  @Deprecated
  public static final String MIME_TYPE_DEFAULT = "application/octet-stream";
  
  public static final int NOTIFY_DELETE = 16;
  
  public static final int NOTIFY_INSERT = 4;
  
  public static final int NOTIFY_NO_DELAY = 32768;
  
  public static final int NOTIFY_SKIP_NOTIFY_FOR_DESCENDANTS = 2;
  
  public static final int NOTIFY_SYNC_TO_NETWORK = 1;
  
  public static final int NOTIFY_UPDATE = 8;
  
  public static final String QUERY_ARG_GROUP_COLUMNS = "android:query-arg-group-columns";
  
  public static final String QUERY_ARG_LIMIT = "android:query-arg-limit";
  
  public static final String QUERY_ARG_OFFSET = "android:query-arg-offset";
  
  public static final String QUERY_ARG_SORT_COLLATION = "android:query-arg-sort-collation";
  
  public static final String QUERY_ARG_SORT_COLUMNS = "android:query-arg-sort-columns";
  
  public static final String QUERY_ARG_SORT_DIRECTION = "android:query-arg-sort-direction";
  
  public static final String QUERY_ARG_SORT_LOCALE = "android:query-arg-sort-locale";
  
  public static final String QUERY_ARG_SQL_GROUP_BY = "android:query-arg-sql-group-by";
  
  public static final String QUERY_ARG_SQL_HAVING = "android:query-arg-sql-having";
  
  public static final String QUERY_ARG_SQL_LIMIT = "android:query-arg-sql-limit";
  
  public static final String QUERY_ARG_SQL_SELECTION = "android:query-arg-sql-selection";
  
  public static final String QUERY_ARG_SQL_SELECTION_ARGS = "android:query-arg-sql-selection-args";
  
  public static final String QUERY_ARG_SQL_SORT_ORDER = "android:query-arg-sql-sort-order";
  
  public static final int QUERY_SORT_DIRECTION_ASCENDING = 0;
  
  public static final int QUERY_SORT_DIRECTION_DESCENDING = 1;
  
  public static final String REMOTE_CALLBACK_ERROR = "error";
  
  public static final String REMOTE_CALLBACK_RESULT = "result";
  
  private static final int REMOTE_CONTENT_PROVIDER_TIMEOUT_MILLIS = 23000;
  
  public static final String SCHEME_ANDROID_RESOURCE = "android.resource";
  
  public static final String SCHEME_CONTENT = "content";
  
  public static final String SCHEME_FILE = "file";
  
  private static final int SLOW_THRESHOLD_MILLIS = 500;
  
  public static final int SYNC_ERROR_AUTHENTICATION = 2;
  
  public static final int SYNC_ERROR_CONFLICT = 5;
  
  public static final int SYNC_ERROR_INTERNAL = 8;
  
  public static final int SYNC_ERROR_IO = 3;
  
  private static final String[] SYNC_ERROR_NAMES;
  
  public static final int SYNC_ERROR_PARSE = 4;
  
  public static final int SYNC_ERROR_SYNC_ALREADY_IN_PROGRESS = 1;
  
  public static final int SYNC_ERROR_TOO_MANY_DELETIONS = 6;
  
  public static final int SYNC_ERROR_TOO_MANY_RETRIES = 7;
  
  public static final int SYNC_EXEMPTION_NONE = 0;
  
  public static final int SYNC_EXEMPTION_PROMOTE_BUCKET = 1;
  
  public static final int SYNC_EXEMPTION_PROMOTE_BUCKET_WITH_TEMP = 2;
  
  @Deprecated
  public static final String SYNC_EXTRAS_ACCOUNT = "account";
  
  public static final String SYNC_EXTRAS_DISALLOW_METERED = "allow_metered";
  
  public static final String SYNC_EXTRAS_DISCARD_LOCAL_DELETIONS = "discard_deletions";
  
  public static final String SYNC_EXTRAS_DO_NOT_RETRY = "do_not_retry";
  
  public static final String SYNC_EXTRAS_EXPECTED_DOWNLOAD = "expected_download";
  
  public static final String SYNC_EXTRAS_EXPECTED_UPLOAD = "expected_upload";
  
  public static final String SYNC_EXTRAS_EXPEDITED = "expedited";
  
  @Deprecated
  public static final String SYNC_EXTRAS_FORCE = "force";
  
  public static final String SYNC_EXTRAS_IGNORE_BACKOFF = "ignore_backoff";
  
  public static final String SYNC_EXTRAS_IGNORE_SETTINGS = "ignore_settings";
  
  public static final String SYNC_EXTRAS_INITIALIZE = "initialize";
  
  public static final String SYNC_EXTRAS_MANUAL = "force";
  
  public static final String SYNC_EXTRAS_OVERRIDE_TOO_MANY_DELETIONS = "deletions_override";
  
  public static final String SYNC_EXTRAS_PRIORITY = "sync_priority";
  
  public static final String SYNC_EXTRAS_REQUIRE_CHARGING = "require_charging";
  
  public static final String SYNC_EXTRAS_UPLOAD = "upload";
  
  public static final int SYNC_OBSERVER_TYPE_ACTIVE = 4;
  
  public static final int SYNC_OBSERVER_TYPE_ALL = 2147483647;
  
  public static final int SYNC_OBSERVER_TYPE_PENDING = 2;
  
  public static final int SYNC_OBSERVER_TYPE_SETTINGS = 1;
  
  public static final int SYNC_OBSERVER_TYPE_STATUS = 8;
  
  public static final String SYNC_VIRTUAL_EXTRAS_EXEMPTION_FLAG = "v_exemption";
  
  private static final String TAG = "ContentResolver";
  
  private static volatile IContentService sContentService;
  
  final String mAttributionTag;
  
  private final Context mContext;
  
  final String mPackageName;
  
  static {
    ACTION_SYNC_CONN_STATUS_CHANGED = new Intent("com.android.sync.SYNC_CONN_STATUS_CHANGED");
    SYNC_ERROR_NAMES = new String[] { "already-in-progress", "authentication-error", "io-error", "parse-error", "conflict", "too-many-deletions", "too-many-retries", "internal-error" };
  }
  
  public static String syncErrorToString(int paramInt) {
    if (paramInt >= 1) {
      String[] arrayOfString = SYNC_ERROR_NAMES;
      if (paramInt <= arrayOfString.length)
        return arrayOfString[paramInt - 1]; 
    } 
    return String.valueOf(paramInt);
  }
  
  public static int syncErrorStringToInt(String paramString) {
    int i;
    int j;
    for (i = 0, j = SYNC_ERROR_NAMES.length; i < j; i++) {
      if (SYNC_ERROR_NAMES[i].equals(paramString))
        return i + 1; 
    } 
    if (paramString != null)
      try {
        return Integer.parseInt(paramString);
      } catch (NumberFormatException numberFormatException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("error parsing sync error: ");
        stringBuilder.append(paramString);
        Log.d("ContentResolver", stringBuilder.toString());
      }  
    return 0;
  }
  
  private final Random mRandom = new Random();
  
  final int mTargetSdkVersion;
  
  final ContentInterface mWrapped;
  
  public ContentResolver(Context paramContext) {
    this(paramContext, null);
  }
  
  public ContentResolver(Context paramContext, ContentInterface paramContentInterface) {
    if (paramContext == null)
      paramContext = ActivityThread.currentApplication(); 
    this.mContext = paramContext;
    this.mPackageName = paramContext.getOpPackageName();
    this.mAttributionTag = this.mContext.getAttributionTag();
    this.mTargetSdkVersion = (this.mContext.getApplicationInfo()).targetSdkVersion;
    this.mWrapped = paramContentInterface;
  }
  
  public static ContentResolver wrap(ContentInterface paramContentInterface) {
    Objects.requireNonNull(paramContentInterface);
    return (ContentResolver)new Object(null, paramContentInterface);
  }
  
  public static ContentResolver wrap(ContentProvider paramContentProvider) {
    return wrap(paramContentProvider);
  }
  
  public static ContentResolver wrap(ContentProviderClient paramContentProviderClient) {
    return wrap(paramContentProviderClient);
  }
  
  protected IContentProvider acquireExistingProvider(Context paramContext, String paramString) {
    return acquireProvider(paramContext, paramString);
  }
  
  public void appNotRespondingViaProvider(IContentProvider paramIContentProvider) {
    throw new UnsupportedOperationException("appNotRespondingViaProvider");
  }
  
  public final String getType(Uri paramUri) {
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.getType(paramUri); 
      IContentProvider iContentProvider = acquireExistingProvider(paramUri);
      if (iContentProvider != null)
        try {
          String str;
          StringResultListener stringResultListener = new StringResultListener();
          this();
          RemoteCallback remoteCallback = new RemoteCallback();
          this(stringResultListener);
          iContentProvider.getTypeAsync(paramUri, remoteCallback);
          stringResultListener.waitForResult(3000L);
          if (stringResultListener.exception == null) {
            str = stringResultListener.result;
            return str;
          } 
          throw str.exception;
        } catch (RemoteException null) {
          return null;
        } catch (Exception exception) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Failed to get type for: ");
          stringBuilder.append(null);
          stringBuilder.append(" (");
          stringBuilder.append(exception.getMessage());
          stringBuilder.append(")");
          Log.w("ContentResolver", stringBuilder.toString());
          return null;
        } finally {
          releaseProvider(iContentProvider);
        }  
      if (!"content".equals(paramUri.getScheme()))
        return null; 
      try {
        StringResultListener stringResultListener = new StringResultListener();
        this();
        IActivityManager iActivityManager = ActivityManager.getService();
        Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
        int i = resolveUserId(paramUri);
        RemoteCallback remoteCallback = new RemoteCallback();
        this(stringResultListener);
        iActivityManager.getProviderMimeTypeAsync(uri, i, remoteCallback);
        stringResultListener.waitForResult(23000L);
        if (stringResultListener.exception == null)
          return stringResultListener.result; 
        throw stringResultListener.exception;
      } catch (RemoteException remoteException) {
        return null;
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to get type for: ");
        stringBuilder.append(remoteException);
        stringBuilder.append(" (");
        stringBuilder.append(exception.getMessage());
        stringBuilder.append(")");
        Log.w("ContentResolver", stringBuilder.toString());
        return null;
      } 
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  private static abstract class ResultListener<T> implements RemoteCallback.OnResultListener {
    public boolean done;
    
    public RuntimeException exception;
    
    public T result;
    
    private ResultListener() {}
    
    protected abstract T getResultFromBundle(Bundle param1Bundle);
    
    public void onResult(Bundle param1Bundle) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_1
      //   3: ldc 'error'
      //   5: invokevirtual getParcelable : (Ljava/lang/String;)Landroid/os/Parcelable;
      //   8: checkcast android/os/ParcelableException
      //   11: astore_2
      //   12: aload_2
      //   13: ifnull -> 56
      //   16: aload_2
      //   17: invokevirtual getCause : ()Ljava/lang/Throwable;
      //   20: astore_2
      //   21: aload_2
      //   22: instanceof java/lang/RuntimeException
      //   25: ifeq -> 39
      //   28: aload_0
      //   29: aload_2
      //   30: checkcast java/lang/RuntimeException
      //   33: putfield exception : Ljava/lang/RuntimeException;
      //   36: goto -> 53
      //   39: new java/lang/RuntimeException
      //   42: astore_1
      //   43: aload_1
      //   44: aload_2
      //   45: invokespecial <init> : (Ljava/lang/Throwable;)V
      //   48: aload_0
      //   49: aload_1
      //   50: putfield exception : Ljava/lang/RuntimeException;
      //   53: goto -> 65
      //   56: aload_0
      //   57: aload_0
      //   58: aload_1
      //   59: invokevirtual getResultFromBundle : (Landroid/os/Bundle;)Ljava/lang/Object;
      //   62: putfield result : Ljava/lang/Object;
      //   65: aload_0
      //   66: iconst_1
      //   67: putfield done : Z
      //   70: aload_0
      //   71: invokevirtual notifyAll : ()V
      //   74: aload_0
      //   75: monitorexit
      //   76: return
      //   77: astore_1
      //   78: aload_0
      //   79: monitorexit
      //   80: aload_1
      //   81: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #955	-> 0
      //   #956	-> 2
      //   #957	-> 12
      //   #958	-> 16
      //   #959	-> 21
      //   #960	-> 28
      //   #962	-> 39
      //   #964	-> 53
      //   #965	-> 56
      //   #967	-> 65
      //   #968	-> 70
      //   #969	-> 74
      //   #970	-> 76
      //   #969	-> 77
      // Exception table:
      //   from	to	target	type
      //   2	12	77	finally
      //   16	21	77	finally
      //   21	28	77	finally
      //   28	36	77	finally
      //   39	53	77	finally
      //   56	65	77	finally
      //   65	70	77	finally
      //   70	74	77	finally
      //   74	76	77	finally
      //   78	80	77	finally
    }
    
    public void waitForResult(long param1Long) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield done : Z
      //   6: istore_3
      //   7: iload_3
      //   8: ifne -> 21
      //   11: aload_0
      //   12: lload_1
      //   13: invokevirtual wait : (J)V
      //   16: goto -> 21
      //   19: astore #4
      //   21: aload_0
      //   22: monitorexit
      //   23: return
      //   24: astore #4
      //   26: aload_0
      //   27: monitorexit
      //   28: aload #4
      //   30: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #975	-> 0
      //   #976	-> 2
      //   #978	-> 11
      //   #981	-> 16
      //   #979	-> 19
      //   #983	-> 21
      //   #984	-> 23
      //   #983	-> 24
      // Exception table:
      //   from	to	target	type
      //   2	7	24	finally
      //   11	16	19	java/lang/InterruptedException
      //   11	16	24	finally
      //   21	23	24	finally
      //   26	28	24	finally
    }
  }
  
  class StringResultListener extends ResultListener<String> {
    private StringResultListener() {}
    
    protected String getResultFromBundle(Bundle param1Bundle) {
      return param1Bundle.getString("result");
    }
  }
  
  class UriResultListener extends ResultListener<Uri> {
    private UriResultListener() {}
    
    protected Uri getResultFromBundle(Bundle param1Bundle) {
      return (Uri)param1Bundle.getParcelable("result");
    }
  }
  
  public String[] getStreamTypes(Uri paramUri, String paramString) {
    Objects.requireNonNull(paramUri, "url");
    Objects.requireNonNull(paramString, "mimeTypeFilter");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.getStreamTypes(paramUri, paramString); 
      IContentProvider iContentProvider = acquireProvider(paramUri);
      if (iContentProvider == null)
        return null; 
      try {
        return iContentProvider.getStreamTypes(paramUri, paramString);
      } catch (RemoteException remoteException) {
        return null;
      } finally {
        releaseProvider(iContentProvider);
      } 
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    SeempLog.record_uri(13, paramUri);
    return query(paramUri, paramArrayOfString1, paramString1, paramArrayOfString2, paramString2, null);
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2, CancellationSignal paramCancellationSignal) {
    Bundle bundle = createSqlQueryBundle(paramString1, paramArrayOfString2, paramString2);
    return query(paramUri, paramArrayOfString1, bundle, paramCancellationSignal);
  }
  
  public final Cursor query(Uri paramUri, String[] paramArrayOfString, Bundle paramBundle, CancellationSignal paramCancellationSignal) {
    SeempLog.record_uri(13, paramUri);
    Objects.requireNonNull(paramUri, "uri");
    try {
      ContentInterface contentInterface = this.mWrapped;
      if (contentInterface != null) {
        try {
          contentInterface = this.mWrapped;
          return contentInterface.query(paramUri, paramArrayOfString, paramBundle, paramCancellationSignal);
        } catch (RemoteException remoteException) {}
      } else {
        Cursor cursor1;
        IContentProvider iContentProvider1 = acquireUnstableProvider((Uri)remoteException);
        if (iContentProvider1 == null)
          return null; 
        contentInterface = null;
        IContentProvider iContentProvider2 = null;
        RemoteException remoteException1 = null;
        Cursor cursor2 = null;
        String[] arrayOfString = null;
        Bundle bundle = null;
        Cursor cursor3 = cursor2;
        IContentProvider iContentProvider3 = iContentProvider2;
        try {
          DeadObjectException deadObjectException;
          ICancellationSignal iCancellationSignal;
          Cursor cursor;
          long l = SystemClock.uptimeMillis();
          if (paramCancellationSignal != null) {
            try {
              paramCancellationSignal.throwIfCanceled();
              iCancellationSignal = iContentProvider1.createCancellationSignal();
              paramCancellationSignal.setRemote(iCancellationSignal);
              try {
                cursor3 = iContentProvider1.query(this.mPackageName, this.mAttributionTag, (Uri)remoteException, paramArrayOfString, paramBundle, iCancellationSignal);
                contentInterface = null;
              } catch (DeadObjectException deadObjectException1) {
                cursor3 = cursor2;
                iContentProvider3 = iContentProvider2;
              } 
            } catch (RemoteException remoteException2) {
            
            } finally {
              remoteException = null;
              deadObjectException = deadObjectException1;
            } 
          } else {
            iCancellationSignal = null;
            try {
              cursor3 = iContentProvider1.query(this.mPackageName, this.mAttributionTag, (Uri)remoteException, (String[])deadObjectException, paramBundle, iCancellationSignal);
              contentInterface = null;
            } catch (DeadObjectException deadObjectException1) {
              cursor3 = cursor2;
              iContentProvider3 = iContentProvider2;
            } 
          } 
          unstableProviderDied(iContentProvider1);
          cursor3 = cursor2;
        } catch (RemoteException remoteException2) {
        
        } finally {
          paramBundle = bundle;
          cursor1 = cursor3;
          if (paramBundle != null)
            paramBundle.close(); 
          if (paramCancellationSignal != null)
            paramCancellationSignal.setRemote(null); 
          if (iContentProvider1 != null)
            releaseUnstableProvider(iContentProvider1); 
          if (cursor1 != null)
            releaseProvider((IContentProvider)cursor1); 
        } 
        if (cursor1 != null)
          cursor1.close(); 
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public final Uri canonicalizeOrElse(Uri paramUri) {
    Uri uri = canonicalize(paramUri);
    if (uri != null)
      paramUri = uri; 
    return paramUri;
  }
  
  public final Uri canonicalize(Uri paramUri) {
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.canonicalize(paramUri); 
      IContentProvider iContentProvider = acquireProvider(paramUri);
      if (iContentProvider == null)
        return null; 
      try {
        UriResultListener uriResultListener = new UriResultListener();
        this();
        String str1 = this.mPackageName, str2 = this.mAttributionTag;
        RemoteCallback remoteCallback = new RemoteCallback();
        this(uriResultListener);
        iContentProvider.canonicalizeAsync(str1, str2, paramUri, remoteCallback);
        uriResultListener.waitForResult(3000L);
        if (uriResultListener.exception == null) {
          paramUri = uriResultListener.result;
          return paramUri;
        } 
        throw uriResultListener.exception;
      } catch (RemoteException remoteException) {
        return null;
      } finally {
        releaseProvider(iContentProvider);
      } 
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final Uri uncanonicalize(Uri paramUri) {
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.uncanonicalize(paramUri); 
      IContentProvider iContentProvider = acquireProvider(paramUri);
      if (iContentProvider == null)
        return null; 
      try {
        paramUri = iContentProvider.uncanonicalize(this.mPackageName, this.mAttributionTag, paramUri);
        return paramUri;
      } catch (RemoteException remoteException) {
        return null;
      } finally {
        releaseProvider(iContentProvider);
      } 
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final boolean refresh(Uri paramUri, Bundle paramBundle, CancellationSignal paramCancellationSignal) {
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.refresh(paramUri, paramBundle, paramCancellationSignal); 
      IContentProvider iContentProvider = acquireProvider(paramUri);
      if (iContentProvider == null)
        return false; 
      if (paramCancellationSignal != null)
        try {
          paramCancellationSignal.throwIfCanceled();
          ICancellationSignal iCancellationSignal2 = iContentProvider.createCancellationSignal();
          paramCancellationSignal.setRemote(iCancellationSignal2);
          ICancellationSignal iCancellationSignal1 = iCancellationSignal2;
          return iContentProvider.refresh(this.mPackageName, this.mAttributionTag, paramUri, paramBundle, iCancellationSignal1);
        } catch (RemoteException remoteException) {
          return false;
        } finally {
          releaseProvider(iContentProvider);
        }  
      paramCancellationSignal = null;
      boolean bool = iContentProvider.refresh(this.mPackageName, this.mAttributionTag, paramUri, paramBundle, (ICancellationSignal)paramCancellationSignal);
      releaseProvider(iContentProvider);
      return bool;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  @SystemApi
  public int checkUriPermission(Uri paramUri, int paramInt1, int paramInt2) {
    Objects.requireNonNull(paramUri, "uri");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.checkUriPermission(paramUri, paramInt1, paramInt2); 
      try {
        ContentProviderClient contentProviderClient = acquireUnstableContentProviderClient(paramUri);
        try {
          paramInt1 = contentProviderClient.checkUriPermission(paramUri, paramInt1, paramInt2);
          return paramInt1;
        } finally {
          if (contentProviderClient != null)
            try {
              contentProviderClient.close();
            } finally {
              contentProviderClient = null;
            }  
        } 
      } catch (RemoteException remoteException) {
        return -1;
      } 
    } catch (RemoteException remoteException) {
      return -1;
    } 
  }
  
  public final InputStream openInputStream(Uri paramUri) throws FileNotFoundException {
    Objects.requireNonNull(paramUri, "uri");
    String str = paramUri.getScheme();
    if ("android.resource".equals(str)) {
      OpenResourceIdResult openResourceIdResult = getResourceId(paramUri);
      try {
        return openResourceIdResult.r.openRawResource(openResourceIdResult.id);
      } catch (android.content.res.Resources.NotFoundException notFoundException) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Resource does not exist: ");
        stringBuilder2.append(paramUri);
        throw new FileNotFoundException(stringBuilder2.toString());
      } 
    } 
    if ("file".equals(stringBuilder2))
      return new FileInputStream(paramUri.getPath()); 
    StringBuilder stringBuilder2 = null;
    AssetFileDescriptor assetFileDescriptor = openAssetFileDescriptor(paramUri, "r", null);
    StringBuilder stringBuilder1 = stringBuilder2;
    if (assetFileDescriptor != null)
      try {
        FileInputStream fileInputStream = assetFileDescriptor.createInputStream();
      } catch (IOException iOException) {
        throw new FileNotFoundException("Unable to create stream");
      }  
    return (InputStream)iOException;
  }
  
  public final OutputStream openOutputStream(Uri paramUri) throws FileNotFoundException {
    return openOutputStream(paramUri, "w");
  }
  
  public final OutputStream openOutputStream(Uri paramUri, String paramString) throws FileNotFoundException {
    Uri uri = null;
    AssetFileDescriptor assetFileDescriptor = openAssetFileDescriptor(paramUri, paramString, null);
    paramUri = uri;
    if (assetFileDescriptor != null)
      try {
        FileOutputStream fileOutputStream = assetFileDescriptor.createOutputStream();
      } catch (IOException iOException) {
        throw new FileNotFoundException("Unable to create stream");
      }  
    return (OutputStream)iOException;
  }
  
  public final ParcelFileDescriptor openFile(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    try {
      if (this.mWrapped != null)
        return this.mWrapped.openFile(paramUri, paramString, paramCancellationSignal); 
      return openFileDescriptor(paramUri, paramString, paramCancellationSignal);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final ParcelFileDescriptor openFileDescriptor(Uri paramUri, String paramString) throws FileNotFoundException {
    return openFileDescriptor(paramUri, paramString, null);
  }
  
  public final ParcelFileDescriptor openFileDescriptor(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    try {
      if (this.mWrapped != null)
        return this.mWrapped.openFile(paramUri, paramString, paramCancellationSignal); 
      AssetFileDescriptor assetFileDescriptor = openAssetFileDescriptor(paramUri, paramString, paramCancellationSignal);
      if (assetFileDescriptor == null)
        return null; 
      if (assetFileDescriptor.getDeclaredLength() < 0L)
        return assetFileDescriptor.getParcelFileDescriptor(); 
      try {
        assetFileDescriptor.close();
      } catch (IOException iOException) {}
      throw new FileNotFoundException("Not a whole file");
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final AssetFileDescriptor openAssetFile(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    try {
      if (this.mWrapped != null)
        return this.mWrapped.openAssetFile(paramUri, paramString, paramCancellationSignal); 
      return openAssetFileDescriptor(paramUri, paramString, paramCancellationSignal);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final AssetFileDescriptor openAssetFileDescriptor(Uri paramUri, String paramString) throws FileNotFoundException {
    return openAssetFileDescriptor(paramUri, paramString, null);
  }
  
  public final AssetFileDescriptor openAssetFileDescriptor(Uri paramUri, String paramString, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    Objects.requireNonNull(paramUri, "uri");
    Objects.requireNonNull(paramString, "mode");
    try {
      ParcelFileDescriptor parcelFileDescriptor;
      if (this.mWrapped != null)
        return this.mWrapped.openAssetFile(paramUri, paramString, paramCancellationSignal); 
      String str = paramUri.getScheme();
      if ("android.resource".equals(str)) {
        if ("r".equals(paramString)) {
          OpenResourceIdResult openResourceIdResult = getResourceId(paramUri);
          try {
            return openResourceIdResult.r.openRawResourceFd(openResourceIdResult.id);
          } catch (android.content.res.Resources.NotFoundException notFoundException) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Resource does not exist: ");
            stringBuilder1.append(paramUri);
            throw new FileNotFoundException(stringBuilder1.toString());
          } 
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Can't write resources: ");
        stringBuilder.append(paramUri);
        throw new FileNotFoundException(stringBuilder.toString());
      } 
      if ("file".equals(str)) {
        File file = new File(paramUri.getPath());
        int i = ParcelFileDescriptor.parseMode((String)stringBuilder);
        parcelFileDescriptor = ParcelFileDescriptor.open(file, i);
        return new AssetFileDescriptor(parcelFileDescriptor, 0L, -1L);
      } 
      if ("r".equals(stringBuilder))
        return openTypedAssetFileDescriptor((Uri)parcelFileDescriptor, "*/*", null, paramCancellationSignal); 
      IContentProvider iContentProvider = acquireUnstableProvider((Uri)parcelFileDescriptor);
      if (iContentProvider != null) {
        AssetFileDescriptor assetFileDescriptor;
        IContentProvider iContentProvider1 = null, iContentProvider2 = null;
        Object object = null;
        String str1 = null;
        if (paramCancellationSignal != null) {
          IContentProvider iContentProvider9 = iContentProvider;
          str = str1;
          IContentProvider iContentProvider10 = iContentProvider, iContentProvider11 = iContentProvider1, iContentProvider12 = iContentProvider, iContentProvider13 = iContentProvider2;
          try {
            paramCancellationSignal.throwIfCanceled();
            iContentProvider9 = iContentProvider;
            str = str1;
            iContentProvider10 = iContentProvider;
            iContentProvider11 = iContentProvider1;
            iContentProvider12 = iContentProvider;
            iContentProvider13 = iContentProvider2;
            assetFileDescriptor = (AssetFileDescriptor)iContentProvider.createCancellationSignal();
            iContentProvider9 = iContentProvider;
            str = str1;
            iContentProvider10 = iContentProvider;
            iContentProvider11 = iContentProvider1;
            iContentProvider12 = iContentProvider;
            iContentProvider13 = iContentProvider2;
            paramCancellationSignal.setRemote((ICancellationSignal)assetFileDescriptor);
            iContentProvider9 = iContentProvider;
            str = str1;
            iContentProvider10 = iContentProvider;
            iContentProvider11 = iContentProvider1;
            iContentProvider12 = iContentProvider;
            iContentProvider13 = iContentProvider2;
          } catch (RemoteException remoteException) {
          
          } catch (FileNotFoundException fileNotFoundException) {
          
          } finally {}
        } else {
          assetFileDescriptor = null;
          IContentProvider iContentProvider9 = iContentProvider;
          str = str1;
          IContentProvider iContentProvider10 = iContentProvider, iContentProvider11 = iContentProvider1, iContentProvider12 = iContentProvider, iContentProvider13 = iContentProvider2;
        } 
        unstableProviderDied(iContentProvider);
        IContentProvider iContentProvider4 = iContentProvider;
        str = str1;
        IContentProvider iContentProvider5 = iContentProvider, iContentProvider6 = iContentProvider1, iContentProvider7 = iContentProvider, iContentProvider8 = iContentProvider2, iContentProvider3 = acquireProvider((Uri)parcelFileDescriptor);
        if (iContentProvider3 != null) {
          iContentProvider4 = iContentProvider;
          IContentProvider iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          assetFileDescriptor = iContentProvider3.openAssetFile(this.mPackageName, this.mAttributionTag, (Uri)parcelFileDescriptor, (String)remoteException, (ICancellationSignal)assetFileDescriptor);
          if (assetFileDescriptor == null) {
            if (paramCancellationSignal != null)
              paramCancellationSignal.setRemote(null); 
            if (iContentProvider3 != null)
              releaseProvider(iContentProvider3); 
            if (iContentProvider != null)
              releaseUnstableProvider(iContentProvider); 
            return null;
          } 
          iContentProvider8 = iContentProvider3;
        } else {
          iContentProvider4 = iContentProvider;
          IContentProvider iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          FileNotFoundException fileNotFoundException = new FileNotFoundException();
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          StringBuilder stringBuilder1 = new StringBuilder();
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          this();
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          stringBuilder1.append("No content provider: ");
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          stringBuilder1.append(parcelFileDescriptor);
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          this(stringBuilder1.toString());
          iContentProvider4 = iContentProvider;
          iContentProvider9 = iContentProvider3;
          iContentProvider5 = iContentProvider;
          iContentProvider6 = iContentProvider3;
          iContentProvider7 = iContentProvider;
          iContentProvider8 = iContentProvider3;
          throw fileNotFoundException;
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No content provider: ");
      stringBuilder.append(parcelFileDescriptor);
      throw new FileNotFoundException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final AssetFileDescriptor openTypedAssetFile(Uri paramUri, String paramString, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    try {
      if (this.mWrapped != null)
        return this.mWrapped.openTypedAssetFile(paramUri, paramString, paramBundle, paramCancellationSignal); 
      return openTypedAssetFileDescriptor(paramUri, paramString, paramBundle, paramCancellationSignal);
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final AssetFileDescriptor openTypedAssetFileDescriptor(Uri paramUri, String paramString, Bundle paramBundle) throws FileNotFoundException {
    return openTypedAssetFileDescriptor(paramUri, paramString, paramBundle, null);
  }
  
  public final AssetFileDescriptor openTypedAssetFileDescriptor(Uri paramUri, String paramString, Bundle paramBundle, CancellationSignal paramCancellationSignal) throws FileNotFoundException {
    // Byte code:
    //   0: aload_1
    //   1: ldc_w 'uri'
    //   4: invokestatic requireNonNull : (Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   7: pop
    //   8: aload_2
    //   9: ldc_w 'mimeType'
    //   12: invokestatic requireNonNull : (Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    //   15: pop
    //   16: aload_0
    //   17: getfield mWrapped : Landroid/content/ContentInterface;
    //   20: ifnull -> 48
    //   23: aload_0
    //   24: getfield mWrapped : Landroid/content/ContentInterface;
    //   27: astore #5
    //   29: aload #5
    //   31: aload_1
    //   32: aload_2
    //   33: aload_3
    //   34: aload #4
    //   36: invokeinterface openTypedAssetFile : (Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/CancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    //   41: astore_1
    //   42: aload_1
    //   43: areturn
    //   44: astore_1
    //   45: goto -> 1043
    //   48: aload_0
    //   49: aload_1
    //   50: invokevirtual acquireUnstableProvider : (Landroid/net/Uri;)Landroid/content/IContentProvider;
    //   53: astore #6
    //   55: aload #6
    //   57: ifnull -> 1008
    //   60: aconst_null
    //   61: astore #7
    //   63: aconst_null
    //   64: astore #8
    //   66: aconst_null
    //   67: astore #9
    //   69: aconst_null
    //   70: astore #10
    //   72: aconst_null
    //   73: astore #11
    //   75: aconst_null
    //   76: astore #12
    //   78: aload #4
    //   80: ifnull -> 214
    //   83: aload #12
    //   85: astore #5
    //   87: aload #6
    //   89: astore #13
    //   91: aload #7
    //   93: astore #14
    //   95: aload #6
    //   97: astore #15
    //   99: aload #8
    //   101: astore #16
    //   103: aload #6
    //   105: astore #17
    //   107: aload #4
    //   109: invokevirtual throwIfCanceled : ()V
    //   112: aload #12
    //   114: astore #5
    //   116: aload #6
    //   118: astore #13
    //   120: aload #7
    //   122: astore #14
    //   124: aload #6
    //   126: astore #15
    //   128: aload #8
    //   130: astore #16
    //   132: aload #6
    //   134: astore #17
    //   136: aload #6
    //   138: invokeinterface createCancellationSignal : ()Landroid/os/ICancellationSignal;
    //   143: astore #18
    //   145: aload #12
    //   147: astore #5
    //   149: aload #6
    //   151: astore #13
    //   153: aload #7
    //   155: astore #14
    //   157: aload #6
    //   159: astore #15
    //   161: aload #8
    //   163: astore #16
    //   165: aload #6
    //   167: astore #17
    //   169: aload #4
    //   171: aload #18
    //   173: invokevirtual setRemote : (Landroid/os/ICancellationSignal;)V
    //   176: aload #18
    //   178: astore #15
    //   180: goto -> 217
    //   183: astore_1
    //   184: aload #13
    //   186: astore #6
    //   188: goto -> 971
    //   191: astore_1
    //   192: aload #14
    //   194: astore #5
    //   196: aload #15
    //   198: astore #6
    //   200: goto -> 871
    //   203: astore_2
    //   204: aload #16
    //   206: astore_2
    //   207: aload #17
    //   209: astore #6
    //   211: goto -> 881
    //   214: aconst_null
    //   215: astore #15
    //   217: aload_0
    //   218: getfield mPackageName : Ljava/lang/String;
    //   221: astore #5
    //   223: aload_0
    //   224: getfield mAttributionTag : Ljava/lang/String;
    //   227: astore #13
    //   229: aload #6
    //   231: aload #5
    //   233: aload #13
    //   235: aload_1
    //   236: aload_2
    //   237: aload_3
    //   238: aload #15
    //   240: invokeinterface openTypedAssetFile : (Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ICancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    //   245: astore #11
    //   247: aload #11
    //   249: ifnonnull -> 287
    //   252: aload #4
    //   254: ifnull -> 263
    //   257: aload #4
    //   259: aconst_null
    //   260: invokevirtual setRemote : (Landroid/os/ICancellationSignal;)V
    //   263: iconst_0
    //   264: ifeq -> 273
    //   267: aload_0
    //   268: aconst_null
    //   269: invokevirtual releaseProvider : (Landroid/content/IContentProvider;)Z
    //   272: pop
    //   273: aload #6
    //   275: ifnull -> 285
    //   278: aload_0
    //   279: aload #6
    //   281: invokevirtual releaseUnstableProvider : (Landroid/content/IContentProvider;)Z
    //   284: pop
    //   285: aconst_null
    //   286: areturn
    //   287: aconst_null
    //   288: astore_2
    //   289: goto -> 468
    //   292: astore_1
    //   293: aconst_null
    //   294: astore #5
    //   296: goto -> 971
    //   299: astore_1
    //   300: aconst_null
    //   301: astore #5
    //   303: goto -> 871
    //   306: astore_2
    //   307: aconst_null
    //   308: astore_2
    //   309: goto -> 881
    //   312: astore #5
    //   314: goto -> 342
    //   317: astore_1
    //   318: aload #11
    //   320: astore #5
    //   322: goto -> 971
    //   325: astore_1
    //   326: aload #9
    //   328: astore #5
    //   330: goto -> 871
    //   333: astore_2
    //   334: aload #10
    //   336: astore_2
    //   337: goto -> 881
    //   340: astore #5
    //   342: aconst_null
    //   343: astore #17
    //   345: aconst_null
    //   346: astore #5
    //   348: aconst_null
    //   349: astore #11
    //   351: aload #11
    //   353: astore #13
    //   355: aload #17
    //   357: astore #14
    //   359: aload #5
    //   361: astore #16
    //   363: aload_0
    //   364: aload #6
    //   366: invokevirtual unstableProviderDied : (Landroid/content/IContentProvider;)V
    //   369: aload #11
    //   371: astore #13
    //   373: aload #17
    //   375: astore #14
    //   377: aload #5
    //   379: astore #16
    //   381: aload_0
    //   382: aload_1
    //   383: invokevirtual acquireProvider : (Landroid/net/Uri;)Landroid/content/IContentProvider;
    //   386: astore #5
    //   388: aload #5
    //   390: ifnull -> 738
    //   393: aload_0
    //   394: getfield mPackageName : Ljava/lang/String;
    //   397: astore #14
    //   399: aload_0
    //   400: getfield mAttributionTag : Ljava/lang/String;
    //   403: astore #13
    //   405: aload #5
    //   407: aload #14
    //   409: aload #13
    //   411: aload_1
    //   412: aload_2
    //   413: aload_3
    //   414: aload #15
    //   416: invokeinterface openTypedAssetFile : (Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/ICancellationSignal;)Landroid/content/res/AssetFileDescriptor;
    //   421: astore #11
    //   423: aload #11
    //   425: ifnonnull -> 465
    //   428: aload #4
    //   430: ifnull -> 439
    //   433: aload #4
    //   435: aconst_null
    //   436: invokevirtual setRemote : (Landroid/os/ICancellationSignal;)V
    //   439: aload #5
    //   441: ifnull -> 451
    //   444: aload_0
    //   445: aload #5
    //   447: invokevirtual releaseProvider : (Landroid/content/IContentProvider;)Z
    //   450: pop
    //   451: aload #6
    //   453: ifnull -> 463
    //   456: aload_0
    //   457: aload #6
    //   459: invokevirtual releaseUnstableProvider : (Landroid/content/IContentProvider;)Z
    //   462: pop
    //   463: aconst_null
    //   464: areturn
    //   465: aload #5
    //   467: astore_2
    //   468: aload_2
    //   469: astore_3
    //   470: aload_2
    //   471: ifnonnull -> 501
    //   474: aload_2
    //   475: astore #5
    //   477: aload #6
    //   479: astore #13
    //   481: aload_2
    //   482: astore #14
    //   484: aload #6
    //   486: astore #15
    //   488: aload_2
    //   489: astore #16
    //   491: aload #6
    //   493: astore #17
    //   495: aload_0
    //   496: aload_1
    //   497: invokevirtual acquireProvider : (Landroid/net/Uri;)Landroid/content/IContentProvider;
    //   500: astore_3
    //   501: aload_3
    //   502: astore #5
    //   504: aload #6
    //   506: astore #13
    //   508: aload_3
    //   509: astore #14
    //   511: aload #6
    //   513: astore #15
    //   515: aload_3
    //   516: astore #16
    //   518: aload #6
    //   520: astore #17
    //   522: aload_0
    //   523: aload #6
    //   525: invokevirtual releaseUnstableProvider : (Landroid/content/IContentProvider;)Z
    //   528: pop
    //   529: aconst_null
    //   530: astore #6
    //   532: aconst_null
    //   533: astore #9
    //   535: aconst_null
    //   536: astore_2
    //   537: aload_3
    //   538: astore #5
    //   540: aload_2
    //   541: astore #13
    //   543: aload_3
    //   544: astore #14
    //   546: aload #6
    //   548: astore #15
    //   550: aload_3
    //   551: astore #16
    //   553: aload #9
    //   555: astore #17
    //   557: new android/content/ContentResolver$ParcelFileDescriptorInner
    //   560: astore #8
    //   562: aload_3
    //   563: astore #5
    //   565: aload_2
    //   566: astore #13
    //   568: aload_3
    //   569: astore #14
    //   571: aload #6
    //   573: astore #15
    //   575: aload_3
    //   576: astore #16
    //   578: aload #9
    //   580: astore #17
    //   582: aload #8
    //   584: aload_0
    //   585: aload #11
    //   587: invokevirtual getParcelFileDescriptor : ()Landroid/os/ParcelFileDescriptor;
    //   590: aload_3
    //   591: invokespecial <init> : (Landroid/content/ContentResolver;Landroid/os/ParcelFileDescriptor;Landroid/content/IContentProvider;)V
    //   594: aconst_null
    //   595: astore #12
    //   597: aconst_null
    //   598: astore #10
    //   600: aconst_null
    //   601: astore_3
    //   602: aload_3
    //   603: astore #5
    //   605: aload_2
    //   606: astore #13
    //   608: aload #12
    //   610: astore #14
    //   612: aload #6
    //   614: astore #15
    //   616: aload #10
    //   618: astore #16
    //   620: aload #9
    //   622: astore #17
    //   624: aload #11
    //   626: invokevirtual getStartOffset : ()J
    //   629: lstore #19
    //   631: aload_3
    //   632: astore #5
    //   634: aload_2
    //   635: astore #13
    //   637: aload #12
    //   639: astore #14
    //   641: aload #6
    //   643: astore #15
    //   645: aload #10
    //   647: astore #16
    //   649: aload #9
    //   651: astore #17
    //   653: new android/content/res/AssetFileDescriptor
    //   656: dup
    //   657: aload #8
    //   659: lload #19
    //   661: aload #11
    //   663: invokevirtual getDeclaredLength : ()J
    //   666: aload #11
    //   668: invokevirtual getExtras : ()Landroid/os/Bundle;
    //   671: invokespecial <init> : (Landroid/os/ParcelFileDescriptor;JJLandroid/os/Bundle;)V
    //   674: astore_2
    //   675: aload #4
    //   677: ifnull -> 686
    //   680: aload #4
    //   682: aconst_null
    //   683: invokevirtual setRemote : (Landroid/os/ICancellationSignal;)V
    //   686: iconst_0
    //   687: ifeq -> 696
    //   690: aload_0
    //   691: aconst_null
    //   692: invokevirtual releaseProvider : (Landroid/content/IContentProvider;)Z
    //   695: pop
    //   696: iconst_0
    //   697: ifeq -> 706
    //   700: aload_0
    //   701: aconst_null
    //   702: invokevirtual releaseUnstableProvider : (Landroid/content/IContentProvider;)Z
    //   705: pop
    //   706: aload_2
    //   707: areturn
    //   708: astore_1
    //   709: goto -> 971
    //   712: astore_1
    //   713: goto -> 871
    //   716: astore_2
    //   717: aload #5
    //   719: astore_2
    //   720: goto -> 881
    //   723: astore_1
    //   724: goto -> 971
    //   727: astore_1
    //   728: goto -> 871
    //   731: astore_2
    //   732: aload #5
    //   734: astore_2
    //   735: goto -> 881
    //   738: aload #5
    //   740: astore #13
    //   742: aload #5
    //   744: astore #14
    //   746: aload #5
    //   748: astore #16
    //   750: new java/io/FileNotFoundException
    //   753: astore_3
    //   754: aload #5
    //   756: astore #13
    //   758: aload #5
    //   760: astore #14
    //   762: aload #5
    //   764: astore #16
    //   766: new java/lang/StringBuilder
    //   769: astore_2
    //   770: aload #5
    //   772: astore #13
    //   774: aload #5
    //   776: astore #14
    //   778: aload #5
    //   780: astore #16
    //   782: aload_2
    //   783: invokespecial <init> : ()V
    //   786: aload #5
    //   788: astore #13
    //   790: aload #5
    //   792: astore #14
    //   794: aload #5
    //   796: astore #16
    //   798: aload_2
    //   799: ldc_w 'No content provider: '
    //   802: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   805: pop
    //   806: aload #5
    //   808: astore #13
    //   810: aload #5
    //   812: astore #14
    //   814: aload #5
    //   816: astore #16
    //   818: aload_2
    //   819: aload_1
    //   820: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   823: pop
    //   824: aload #5
    //   826: astore #13
    //   828: aload #5
    //   830: astore #14
    //   832: aload #5
    //   834: astore #16
    //   836: aload_3
    //   837: aload_2
    //   838: invokevirtual toString : ()Ljava/lang/String;
    //   841: invokespecial <init> : (Ljava/lang/String;)V
    //   844: aload #5
    //   846: astore #13
    //   848: aload #5
    //   850: astore #14
    //   852: aload #5
    //   854: astore #16
    //   856: aload_3
    //   857: athrow
    //   858: astore_1
    //   859: aload #13
    //   861: astore #5
    //   863: goto -> 971
    //   866: astore_1
    //   867: aload #14
    //   869: astore #5
    //   871: aload #6
    //   873: astore #13
    //   875: aload_1
    //   876: athrow
    //   877: astore_2
    //   878: aload #16
    //   880: astore_2
    //   881: aload_2
    //   882: astore #5
    //   884: aload #6
    //   886: astore #13
    //   888: new java/io/FileNotFoundException
    //   891: astore_3
    //   892: aload_2
    //   893: astore #5
    //   895: aload #6
    //   897: astore #13
    //   899: new java/lang/StringBuilder
    //   902: astore #14
    //   904: aload_2
    //   905: astore #5
    //   907: aload #6
    //   909: astore #13
    //   911: aload #14
    //   913: invokespecial <init> : ()V
    //   916: aload_2
    //   917: astore #5
    //   919: aload #6
    //   921: astore #13
    //   923: aload #14
    //   925: ldc_w 'Failed opening content provider: '
    //   928: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   931: pop
    //   932: aload_2
    //   933: astore #5
    //   935: aload #6
    //   937: astore #13
    //   939: aload #14
    //   941: aload_1
    //   942: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   945: pop
    //   946: aload_2
    //   947: astore #5
    //   949: aload #6
    //   951: astore #13
    //   953: aload_3
    //   954: aload #14
    //   956: invokevirtual toString : ()Ljava/lang/String;
    //   959: invokespecial <init> : (Ljava/lang/String;)V
    //   962: aload_2
    //   963: astore #5
    //   965: aload #6
    //   967: astore #13
    //   969: aload_3
    //   970: athrow
    //   971: aload #4
    //   973: ifnull -> 982
    //   976: aload #4
    //   978: aconst_null
    //   979: invokevirtual setRemote : (Landroid/os/ICancellationSignal;)V
    //   982: aload #5
    //   984: ifnull -> 994
    //   987: aload_0
    //   988: aload #5
    //   990: invokevirtual releaseProvider : (Landroid/content/IContentProvider;)Z
    //   993: pop
    //   994: aload #6
    //   996: ifnull -> 1006
    //   999: aload_0
    //   1000: aload #6
    //   1002: invokevirtual releaseUnstableProvider : (Landroid/content/IContentProvider;)Z
    //   1005: pop
    //   1006: aload_1
    //   1007: athrow
    //   1008: new java/lang/StringBuilder
    //   1011: dup
    //   1012: invokespecial <init> : ()V
    //   1015: astore_2
    //   1016: aload_2
    //   1017: ldc_w 'No content provider: '
    //   1020: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1023: pop
    //   1024: aload_2
    //   1025: aload_1
    //   1026: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1029: pop
    //   1030: new java/io/FileNotFoundException
    //   1033: dup
    //   1034: aload_2
    //   1035: invokevirtual toString : ()Ljava/lang/String;
    //   1038: invokespecial <init> : (Ljava/lang/String;)V
    //   1041: athrow
    //   1042: astore_1
    //   1043: aconst_null
    //   1044: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1969	-> 0
    //   #1970	-> 8
    //   #1973	-> 16
    //   #1974	-> 44
    //   #1976	-> 48
    //   #1978	-> 48
    //   #1979	-> 55
    //   #1982	-> 60
    //   #1983	-> 78
    //   #1986	-> 78
    //   #1987	-> 78
    //   #1988	-> 83
    //   #1989	-> 112
    //   #1990	-> 145
    //   #2041	-> 183
    //   #2038	-> 191
    //   #2034	-> 203
    //   #1987	-> 214
    //   #1994	-> 217
    //   #1997	-> 247
    //   #1999	-> 252
    //   #2041	-> 252
    //   #2042	-> 257
    //   #2044	-> 263
    //   #2045	-> 267
    //   #2047	-> 273
    //   #2048	-> 278
    //   #1999	-> 285
    //   #2017	-> 287
    //   #2041	-> 292
    //   #2038	-> 299
    //   #2034	-> 306
    //   #2001	-> 312
    //   #2041	-> 317
    //   #2038	-> 325
    //   #2034	-> 333
    //   #2001	-> 340
    //   #2005	-> 342
    //   #2006	-> 369
    //   #2007	-> 388
    //   #2010	-> 393
    //   #2013	-> 423
    //   #2015	-> 428
    //   #2041	-> 428
    //   #2042	-> 433
    //   #2044	-> 439
    //   #2045	-> 444
    //   #2047	-> 451
    //   #2048	-> 456
    //   #2015	-> 463
    //   #2013	-> 465
    //   #2019	-> 468
    //   #2020	-> 474
    //   #2022	-> 501
    //   #2023	-> 529
    //   #2024	-> 537
    //   #2025	-> 562
    //   #2029	-> 594
    //   #2031	-> 602
    //   #2032	-> 631
    //   #2041	-> 675
    //   #2042	-> 680
    //   #2044	-> 686
    //   #2045	-> 690
    //   #2047	-> 696
    //   #2048	-> 700
    //   #2031	-> 706
    //   #2041	-> 708
    //   #2038	-> 712
    //   #2034	-> 716
    //   #2041	-> 723
    //   #2038	-> 727
    //   #2034	-> 731
    //   #2008	-> 738
    //   #2041	-> 858
    //   #2038	-> 866
    //   #2039	-> 871
    //   #2034	-> 877
    //   #2036	-> 881
    //   #2041	-> 971
    //   #2042	-> 976
    //   #2044	-> 982
    //   #2045	-> 987
    //   #2047	-> 994
    //   #2048	-> 999
    //   #2050	-> 1006
    //   #1980	-> 1008
    //   #1974	-> 1042
    //   #1975	-> 1043
    // Exception table:
    //   from	to	target	type
    //   16	29	1042	android/os/RemoteException
    //   29	42	44	android/os/RemoteException
    //   107	112	203	android/os/RemoteException
    //   107	112	191	java/io/FileNotFoundException
    //   107	112	183	finally
    //   136	145	203	android/os/RemoteException
    //   136	145	191	java/io/FileNotFoundException
    //   136	145	183	finally
    //   169	176	203	android/os/RemoteException
    //   169	176	191	java/io/FileNotFoundException
    //   169	176	183	finally
    //   217	229	340	android/os/DeadObjectException
    //   217	229	333	android/os/RemoteException
    //   217	229	325	java/io/FileNotFoundException
    //   217	229	317	finally
    //   229	247	312	android/os/DeadObjectException
    //   229	247	306	android/os/RemoteException
    //   229	247	299	java/io/FileNotFoundException
    //   229	247	292	finally
    //   363	369	877	android/os/RemoteException
    //   363	369	866	java/io/FileNotFoundException
    //   363	369	858	finally
    //   381	388	877	android/os/RemoteException
    //   381	388	866	java/io/FileNotFoundException
    //   381	388	858	finally
    //   393	405	731	android/os/RemoteException
    //   393	405	727	java/io/FileNotFoundException
    //   393	405	723	finally
    //   405	423	716	android/os/RemoteException
    //   405	423	712	java/io/FileNotFoundException
    //   405	423	708	finally
    //   495	501	203	android/os/RemoteException
    //   495	501	191	java/io/FileNotFoundException
    //   495	501	183	finally
    //   522	529	203	android/os/RemoteException
    //   522	529	191	java/io/FileNotFoundException
    //   522	529	183	finally
    //   557	562	203	android/os/RemoteException
    //   557	562	191	java/io/FileNotFoundException
    //   557	562	183	finally
    //   582	594	203	android/os/RemoteException
    //   582	594	191	java/io/FileNotFoundException
    //   582	594	183	finally
    //   624	631	203	android/os/RemoteException
    //   624	631	191	java/io/FileNotFoundException
    //   624	631	183	finally
    //   653	675	203	android/os/RemoteException
    //   653	675	191	java/io/FileNotFoundException
    //   653	675	183	finally
    //   750	754	877	android/os/RemoteException
    //   750	754	866	java/io/FileNotFoundException
    //   750	754	858	finally
    //   766	770	877	android/os/RemoteException
    //   766	770	866	java/io/FileNotFoundException
    //   766	770	858	finally
    //   782	786	877	android/os/RemoteException
    //   782	786	866	java/io/FileNotFoundException
    //   782	786	858	finally
    //   798	806	877	android/os/RemoteException
    //   798	806	866	java/io/FileNotFoundException
    //   798	806	858	finally
    //   818	824	877	android/os/RemoteException
    //   818	824	866	java/io/FileNotFoundException
    //   818	824	858	finally
    //   836	844	877	android/os/RemoteException
    //   836	844	866	java/io/FileNotFoundException
    //   836	844	858	finally
    //   856	858	877	android/os/RemoteException
    //   856	858	866	java/io/FileNotFoundException
    //   856	858	858	finally
    //   875	877	183	finally
    //   888	892	183	finally
    //   899	904	183	finally
    //   911	916	183	finally
    //   923	932	183	finally
    //   939	946	183	finally
    //   953	962	183	finally
    //   969	971	183	finally
  }
  
  class OpenResourceIdResult {
    public int id;
    
    public Resources r;
    
    final ContentResolver this$0;
  }
  
  public OpenResourceIdResult getResourceId(Uri paramUri) throws FileNotFoundException {
    OpenResourceIdResult openResourceIdResult;
    String str = paramUri.getAuthority();
    if (!TextUtils.isEmpty(str))
      try {
        Resources resources = this.mContext.getPackageManager().getResourcesForApplication(str);
        List<String> list = paramUri.getPathSegments();
        if (list != null) {
          int i = list.size();
          if (i == 1) {
            try {
              i = Integer.parseInt(list.get(0));
            } catch (NumberFormatException numberFormatException) {
              stringBuilder2 = new StringBuilder();
              stringBuilder2.append("Single path segment is not a resource ID: ");
              stringBuilder2.append(paramUri);
              throw new FileNotFoundException(stringBuilder2.toString());
            } 
          } else if (i == 2) {
            i = stringBuilder2.getIdentifier(list.get(1), list.get(0), str);
          } else {
            stringBuilder2 = new StringBuilder();
            stringBuilder2.append("More than two path segments: ");
            stringBuilder2.append(paramUri);
            throw new FileNotFoundException(stringBuilder2.toString());
          } 
          if (i != 0) {
            openResourceIdResult = new OpenResourceIdResult();
            openResourceIdResult.r = (Resources)stringBuilder2;
            openResourceIdResult.id = i;
            return openResourceIdResult;
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("No resource found for: ");
          stringBuilder2.append(openResourceIdResult);
          throw new FileNotFoundException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("No path: ");
        stringBuilder1.append(openResourceIdResult);
        throw new FileNotFoundException(stringBuilder1.toString());
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("No package found for authority: ");
        stringBuilder1.append(openResourceIdResult);
        throw new FileNotFoundException(stringBuilder1.toString());
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("No authority: ");
    stringBuilder.append(openResourceIdResult);
    throw new FileNotFoundException(stringBuilder.toString());
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues) {
    return insert(paramUri, paramContentValues, null);
  }
  
  public final Uri insert(Uri paramUri, ContentValues paramContentValues, Bundle paramBundle) {
    SeempLog.record_uri(37, paramUri);
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null) {
        ContentInterface contentInterface = this.mWrapped;
        try {
          return contentInterface.insert(paramUri, paramContentValues, paramBundle);
        } catch (RemoteException remoteException) {}
      } else {
        Uri uri = OplusMultiAppManager.changeCrossUserVolume((Uri)remoteException, paramContentValues);
        IContentProvider iContentProvider = acquireProvider(uri);
        if (iContentProvider != null)
          try {
            long l1 = SystemClock.uptimeMillis();
            Uri uri1 = iContentProvider.insert(this.mPackageName, this.mAttributionTag, uri, paramContentValues, paramBundle);
            long l2 = SystemClock.uptimeMillis();
            maybeLogUpdateToEventLog(l2 - l1, uri, "insert", null);
            return uri1;
          } catch (RemoteException remoteException1) {
            return null;
          } finally {
            releaseProvider(iContentProvider);
          }  
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown URL ");
        stringBuilder.append(remoteException);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } catch (RemoteException remoteException) {}
    return null;
  }
  
  public ContentProviderResult[] applyBatch(String paramString, ArrayList<ContentProviderOperation> paramArrayList) throws RemoteException, OperationApplicationException {
    Objects.requireNonNull(paramString, "authority");
    Objects.requireNonNull(paramArrayList, "operations");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.applyBatch(paramString, paramArrayList); 
      ContentProviderClient contentProviderClient = acquireContentProviderClient(paramString);
      if (contentProviderClient != null)
        try {
          return contentProviderClient.applyBatch(paramArrayList);
        } finally {
          contentProviderClient.release();
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown authority ");
      stringBuilder.append(paramString);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final int bulkInsert(Uri paramUri, ContentValues[] paramArrayOfContentValues) {
    Objects.requireNonNull(paramUri, "url");
    Objects.requireNonNull(paramArrayOfContentValues, "values");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.bulkInsert(paramUri, paramArrayOfContentValues); 
      IContentProvider iContentProvider = acquireProvider(paramUri);
      if (iContentProvider != null)
        try {
          long l1 = SystemClock.uptimeMillis();
          int i = iContentProvider.bulkInsert(this.mPackageName, this.mAttributionTag, paramUri, paramArrayOfContentValues);
          long l2 = SystemClock.uptimeMillis();
          maybeLogUpdateToEventLog(l2 - l1, paramUri, "bulkinsert", null);
          return i;
        } catch (RemoteException remoteException) {
          return 0;
        } finally {
          releaseProvider(iContentProvider);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown URL ");
      stringBuilder.append(paramUri);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public final int delete(Uri paramUri, String paramString, String[] paramArrayOfString) {
    return delete(paramUri, createSqlQueryBundle(paramString, paramArrayOfString));
  }
  
  public final int delete(Uri paramUri, Bundle paramBundle) {
    Objects.requireNonNull(paramUri, "url");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.delete(paramUri, paramBundle); 
      Uri uri = OplusMultiAppManager.changeCrossUserVolume(paramUri, null);
      IContentProvider iContentProvider = acquireProvider(uri);
      if (iContentProvider != null)
        try {
          boolean bool;
          long l1 = SystemClock.uptimeMillis();
          if (!DcimProtectHelper.interceptByDcimProtect(uri, paramBundle)) {
            bool = iContentProvider.delete(this.mPackageName, this.mAttributionTag, uri, paramBundle);
          } else {
            bool = false;
          } 
          long l2 = SystemClock.uptimeMillis();
          maybeLogUpdateToEventLog(l2 - l1, uri, "delete", null);
          return bool;
        } catch (RemoteException remoteException) {
          return -1;
        } finally {
          releaseProvider(iContentProvider);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown URL ");
      stringBuilder.append(uri);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, String paramString, String[] paramArrayOfString) {
    return update(paramUri, paramContentValues, createSqlQueryBundle(paramString, paramArrayOfString));
  }
  
  public final int update(Uri paramUri, ContentValues paramContentValues, Bundle paramBundle) {
    Objects.requireNonNull(paramUri, "uri");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.update(paramUri, paramContentValues, paramBundle); 
      Uri uri = OplusMultiAppManager.changeCrossUserVolume(paramUri, paramContentValues);
      IContentProvider iContentProvider = acquireProvider(uri);
      if (iContentProvider != null)
        try {
          long l1 = SystemClock.uptimeMillis();
          int i = iContentProvider.update(this.mPackageName, this.mAttributionTag, uri, paramContentValues, paramBundle);
          long l2 = SystemClock.uptimeMillis();
          maybeLogUpdateToEventLog(l2 - l1, uri, "update", null);
          return i;
        } catch (RemoteException remoteException) {
          return -1;
        } finally {
          releaseProvider(iContentProvider);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown URI ");
      stringBuilder.append(uri);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return 0;
    } 
  }
  
  public final Bundle call(Uri paramUri, String paramString1, String paramString2, Bundle paramBundle) {
    return call(paramUri.getAuthority(), paramString1, paramString2, paramBundle);
  }
  
  public final Bundle call(String paramString1, String paramString2, String paramString3, Bundle paramBundle) {
    Objects.requireNonNull(paramString1, "authority");
    Objects.requireNonNull(paramString2, "method");
    try {
      if (this.mWrapped != null)
        return this.mWrapped.call(paramString1, paramString2, paramString3, paramBundle); 
      IContentProvider iContentProvider = acquireProvider(paramString1);
      if (iContentProvider != null)
        try {
          Bundle bundle = iContentProvider.call(this.mPackageName, this.mAttributionTag, paramString1, paramString2, paramString3, paramBundle);
          Bundle.setDefusable(bundle, true);
          return bundle;
        } catch (RemoteException remoteException) {
          return null;
        } finally {
          releaseProvider(iContentProvider);
        }  
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown authority ");
      stringBuilder.append(paramString1);
      throw new IllegalArgumentException(stringBuilder.toString());
    } catch (RemoteException remoteException) {
      return null;
    } 
  }
  
  public final IContentProvider acquireProvider(Uri paramUri) {
    if (!"content".equals(paramUri.getScheme()))
      return null; 
    String str = paramUri.getAuthority();
    if (str != null)
      return acquireProvider(this.mContext, str); 
    return null;
  }
  
  public final IContentProvider acquireExistingProvider(Uri paramUri) {
    if (!"content".equals(paramUri.getScheme()))
      return null; 
    String str = paramUri.getAuthority();
    if (str != null)
      return acquireExistingProvider(this.mContext, str); 
    return null;
  }
  
  public final IContentProvider acquireProvider(String paramString) {
    if (paramString == null)
      return null; 
    return acquireProvider(this.mContext, paramString);
  }
  
  public final IContentProvider acquireUnstableProvider(Uri paramUri) {
    if (!"content".equals(paramUri.getScheme()))
      return null; 
    String str = paramUri.getAuthority();
    if (str != null)
      return acquireUnstableProvider(this.mContext, paramUri.getAuthority()); 
    return null;
  }
  
  public final IContentProvider acquireUnstableProvider(String paramString) {
    if (paramString == null)
      return null; 
    return acquireUnstableProvider(this.mContext, paramString);
  }
  
  public final ContentProviderClient acquireContentProviderClient(Uri paramUri) {
    Objects.requireNonNull(paramUri, "uri");
    IContentProvider iContentProvider = acquireProvider(paramUri);
    if (iContentProvider != null)
      return new ContentProviderClient(this, iContentProvider, paramUri.getAuthority(), true); 
    return null;
  }
  
  public final ContentProviderClient acquireContentProviderClient(String paramString) {
    Objects.requireNonNull(paramString, "name");
    IContentProvider iContentProvider = acquireProvider(paramString);
    if (iContentProvider != null)
      return new ContentProviderClient(this, iContentProvider, paramString, true); 
    return null;
  }
  
  public final ContentProviderClient acquireUnstableContentProviderClient(Uri paramUri) {
    Objects.requireNonNull(paramUri, "uri");
    IContentProvider iContentProvider = acquireUnstableProvider(paramUri);
    if (iContentProvider != null)
      return new ContentProviderClient(this, iContentProvider, paramUri.getAuthority(), false); 
    return null;
  }
  
  public final ContentProviderClient acquireUnstableContentProviderClient(String paramString) {
    Objects.requireNonNull(paramString, "name");
    IContentProvider iContentProvider = acquireUnstableProvider(paramString);
    if (iContentProvider != null)
      return new ContentProviderClient(this, iContentProvider, paramString, false); 
    return null;
  }
  
  public final void registerContentObserver(Uri paramUri, boolean paramBoolean, ContentObserver paramContentObserver) {
    Objects.requireNonNull(paramUri, "uri");
    Objects.requireNonNull(paramContentObserver, "observer");
    Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
    Context context = this.mContext;
    int i = ContentProvider.getUserIdFromUri(paramUri, context.getUserId());
    registerContentObserver(uri, paramBoolean, paramContentObserver, i);
  }
  
  public final void registerContentObserver(Uri paramUri, boolean paramBoolean, ContentObserver paramContentObserver, int paramInt) {
    try {
      IContentService iContentService = getContentService();
      IContentObserver iContentObserver = paramContentObserver.getContentObserver();
      int i = this.mTargetSdkVersion;
      iContentService.registerContentObserver(paramUri, paramBoolean, iContentObserver, paramInt, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public final void unregisterContentObserver(ContentObserver paramContentObserver) {
    Objects.requireNonNull(paramContentObserver, "observer");
    try {
      IContentObserver iContentObserver = paramContentObserver.releaseContentObserver();
      if (iContentObserver != null)
        getContentService().unregisterContentObserver(iContentObserver); 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void notifyChange(Uri paramUri, ContentObserver paramContentObserver) {
    notifyChange(paramUri, paramContentObserver, true);
  }
  
  @Deprecated
  public void notifyChange(Uri paramUri, ContentObserver paramContentObserver, boolean paramBoolean) {
    notifyChange(paramUri, paramContentObserver, paramBoolean);
  }
  
  public void notifyChange(Uri paramUri, ContentObserver paramContentObserver, int paramInt) {
    Objects.requireNonNull(paramUri, "uri");
    Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
    Context context = this.mContext;
    int i = ContentProvider.getUserIdFromUri(paramUri, context.getUserId());
    notifyChange(uri, paramContentObserver, paramInt, i);
  }
  
  @Deprecated
  public void notifyChange(Iterable<Uri> paramIterable, ContentObserver paramContentObserver, int paramInt) {
    ArrayList<Uri> arrayList = new ArrayList();
    Objects.requireNonNull(arrayList);
    paramIterable.forEach(new _$$Lambda$TxJXFacN6KB_OTXom31IyFcSl48(arrayList));
    notifyChange(arrayList, paramContentObserver, paramInt);
  }
  
  public void notifyChange(Collection<Uri> paramCollection, ContentObserver paramContentObserver, int paramInt) {
    Objects.requireNonNull(paramCollection, "uris");
    SparseArray sparseArray = new SparseArray();
    for (Uri uri : paramCollection) {
      int i = ContentProvider.getUserIdFromUri(uri, this.mContext.getUserId());
      ArrayList<Uri> arrayList = (ArrayList)sparseArray.get(i);
      paramCollection = arrayList;
      if (arrayList == null) {
        paramCollection = new ArrayList<>();
        sparseArray.put(i, paramCollection);
      } 
      paramCollection.add(ContentProvider.getUriWithoutUserId(uri));
    } 
    for (byte b = 0; b < sparseArray.size(); b++) {
      int i = sparseArray.keyAt(b);
      paramCollection = (ArrayList)sparseArray.valueAt(b);
      notifyChange(paramCollection.<Uri>toArray(new Uri[paramCollection.size()]), paramContentObserver, paramInt, i);
    } 
  }
  
  @Deprecated
  public void notifyChange(Uri paramUri, ContentObserver paramContentObserver, boolean paramBoolean, int paramInt) {
    notifyChange(paramUri, paramContentObserver, paramBoolean, paramInt);
  }
  
  public void notifyChange(Uri paramUri, ContentObserver paramContentObserver, int paramInt1, int paramInt2) {
    notifyChange(new Uri[] { paramUri }, paramContentObserver, paramInt1, paramInt2);
  }
  
  public void notifyChange(Uri[] paramArrayOfUri, ContentObserver paramContentObserver, int paramInt1, int paramInt2) {
    try {
      IContentObserver iContentObserver;
      boolean bool;
      IContentService iContentService = getContentService();
      if (paramContentObserver == null) {
        iContentObserver = null;
      } else {
        iContentObserver = paramContentObserver.getContentObserver();
      } 
      if (paramContentObserver != null && 
        paramContentObserver.deliverSelfNotifications()) {
        bool = true;
      } else {
        bool = false;
      } 
      int i = this.mTargetSdkVersion;
      Context context = this.mContext;
      String str = context.getPackageName();
      iContentService.notifyChange(paramArrayOfUri, iContentObserver, bool, paramInt1, paramInt2, i, str);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void takePersistableUriPermission(Uri paramUri, int paramInt) {
    Objects.requireNonNull(paramUri, "uri");
    try {
      IUriGrantsManager iUriGrantsManager = UriGrantsManager.getService();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iUriGrantsManager.takePersistableUriPermission(uri, paramInt, null, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void takePersistableUriPermission(String paramString, Uri paramUri, int paramInt) {
    Objects.requireNonNull(paramString, "toPackage");
    Objects.requireNonNull(paramUri, "uri");
    try {
      IUriGrantsManager iUriGrantsManager = UriGrantsManager.getService();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iUriGrantsManager.takePersistableUriPermission(uri, paramInt, paramString, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releasePersistableUriPermission(Uri paramUri, int paramInt) {
    Objects.requireNonNull(paramUri, "uri");
    try {
      IUriGrantsManager iUriGrantsManager = UriGrantsManager.getService();
      Uri uri = ContentProvider.getUriWithoutUserId(paramUri);
      int i = resolveUserId(paramUri);
      iUriGrantsManager.releasePersistableUriPermission(uri, paramInt, null, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UriPermission> getPersistedUriPermissions() {
    try {
      ParceledListSlice parceledListSlice = UriGrantsManager.getService().getUriPermissions(this.mPackageName, true, true);
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UriPermission> getOutgoingPersistedUriPermissions() {
    try {
      ParceledListSlice parceledListSlice = UriGrantsManager.getService().getUriPermissions(this.mPackageName, false, true);
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UriPermission> getOutgoingUriPermissions() {
    try {
      ParceledListSlice parceledListSlice = UriGrantsManager.getService().getUriPermissions(this.mPackageName, false, false);
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Deprecated
  public void startSync(Uri paramUri, Bundle paramBundle) {
    Account account1 = null, account2 = null;
    if (paramBundle != null) {
      String str = paramBundle.getString("account");
      account1 = account2;
      if (!TextUtils.isEmpty(str))
        account1 = new Account(str, "com.google"); 
      paramBundle.remove("account");
    } 
    if (paramUri != null) {
      String str = paramUri.getAuthority();
    } else {
      paramUri = null;
    } 
    requestSync(account1, (String)paramUri, paramBundle);
  }
  
  public static void requestSync(Account paramAccount, String paramString, Bundle paramBundle) {
    requestSyncAsUser(paramAccount, paramString, UserHandle.myUserId(), paramBundle);
  }
  
  public static void requestSyncAsUser(Account paramAccount, String paramString, int paramInt, Bundle paramBundle) {
    if (paramBundle != null) {
      SyncRequest.Builder builder2 = new SyncRequest.Builder();
      SyncRequest.Builder builder1 = builder2.setSyncAdapter(paramAccount, paramString);
      builder1 = builder1.setExtras(paramBundle);
      builder1 = builder1.syncOnce();
      SyncRequest syncRequest = builder1.build();
      try {
        getContentService().syncAsUser(syncRequest, paramInt, ActivityThread.currentPackageName());
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
    throw new IllegalArgumentException("Must specify extras.");
  }
  
  public static void requestSync(SyncRequest paramSyncRequest) {
    try {
      getContentService().sync(paramSyncRequest, ActivityThread.currentPackageName());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void validateSyncExtrasBundle(Bundle paramBundle) {
    try {
      for (Iterator<String> iterator = paramBundle.keySet().iterator(); iterator.hasNext(); ) {
        String str = iterator.next();
        Object object = paramBundle.get(str);
        if (object == null || 
          object instanceof Long || 
          object instanceof Integer || 
          object instanceof Boolean || 
          object instanceof Float || 
          object instanceof Double || 
          object instanceof String || 
          object instanceof Account)
          continue; 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("unexpected value type: ");
        stringBuilder.append(object.getClass().getName());
        this(stringBuilder.toString());
        throw illegalArgumentException;
      } 
      return;
    } catch (IllegalArgumentException illegalArgumentException) {
      throw illegalArgumentException;
    } catch (RuntimeException runtimeException) {
      throw new IllegalArgumentException("error unparceling Bundle", runtimeException);
    } 
  }
  
  @Deprecated
  public void cancelSync(Uri paramUri) {
    if (paramUri != null) {
      String str = paramUri.getAuthority();
    } else {
      paramUri = null;
    } 
    cancelSync(null, (String)paramUri);
  }
  
  public static void cancelSync(Account paramAccount, String paramString) {
    try {
      getContentService().cancelSync(paramAccount, paramString, null);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void cancelSyncAsUser(Account paramAccount, String paramString, int paramInt) {
    try {
      getContentService().cancelSyncAsUser(paramAccount, paramString, null, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static SyncAdapterType[] getSyncAdapterTypes() {
    try {
      return getContentService().getSyncAdapterTypes();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static SyncAdapterType[] getSyncAdapterTypesAsUser(int paramInt) {
    try {
      return getContentService().getSyncAdapterTypesAsUser(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static String[] getSyncAdapterPackagesForAuthorityAsUser(String paramString, int paramInt) {
    try {
      return getContentService().getSyncAdapterPackagesForAuthorityAsUser(paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean getSyncAutomatically(Account paramAccount, String paramString) {
    try {
      return getContentService().getSyncAutomatically(paramAccount, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean getSyncAutomaticallyAsUser(Account paramAccount, String paramString, int paramInt) {
    try {
      return getContentService().getSyncAutomaticallyAsUser(paramAccount, paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean) {
    setSyncAutomaticallyAsUser(paramAccount, paramString, paramBoolean, UserHandle.myUserId());
  }
  
  public static void setSyncAutomaticallyAsUser(Account paramAccount, String paramString, boolean paramBoolean, int paramInt) {
    try {
      getContentService().setSyncAutomaticallyAsUser(paramAccount, paramString, paramBoolean, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong) {
    validateSyncExtrasBundle(paramBundle);
    if (!invalidPeriodicExtras(paramBundle))
      try {
        getContentService().addPeriodicSync(paramAccount, paramString, paramBundle, paramLong);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("illegal extras were set");
  }
  
  public static boolean invalidPeriodicExtras(Bundle paramBundle) {
    if (paramBundle.getBoolean("force", false) || 
      paramBundle.getBoolean("do_not_retry", false) || 
      paramBundle.getBoolean("ignore_backoff", false) || 
      paramBundle.getBoolean("ignore_settings", false) || 
      paramBundle.getBoolean("initialize", false) || 
      paramBundle.getBoolean("force", false) || 
      paramBundle.getBoolean("expedited", false))
      return true; 
    return false;
  }
  
  public static void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle) {
    validateSyncExtrasBundle(paramBundle);
    try {
      getContentService().removePeriodicSync(paramAccount, paramString, paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void cancelSync(SyncRequest paramSyncRequest) {
    if (paramSyncRequest != null)
      try {
        getContentService().cancelRequest(paramSyncRequest);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("request cannot be null");
  }
  
  public static List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString) {
    try {
      return getContentService().getPeriodicSyncs(paramAccount, paramString, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static int getIsSyncable(Account paramAccount, String paramString) {
    try {
      return getContentService().getIsSyncable(paramAccount, paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static int getIsSyncableAsUser(Account paramAccount, String paramString, int paramInt) {
    try {
      return getContentService().getIsSyncableAsUser(paramAccount, paramString, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setIsSyncable(Account paramAccount, String paramString, int paramInt) {
    try {
      getContentService().setIsSyncable(paramAccount, paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setIsSyncableAsUser(Account paramAccount, String paramString, int paramInt1, int paramInt2) {
    try {
      getContentService().setIsSyncableAsUser(paramAccount, paramString, paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean getMasterSyncAutomatically() {
    try {
      return getContentService().getMasterSyncAutomatically();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean getMasterSyncAutomaticallyAsUser(int paramInt) {
    try {
      return getContentService().getMasterSyncAutomaticallyAsUser(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static void setMasterSyncAutomatically(boolean paramBoolean) {
    setMasterSyncAutomaticallyAsUser(paramBoolean, UserHandle.myUserId());
  }
  
  public static void setMasterSyncAutomaticallyAsUser(boolean paramBoolean, int paramInt) {
    try {
      getContentService().setMasterSyncAutomaticallyAsUser(paramBoolean, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isSyncActive(Account paramAccount, String paramString) {
    if (paramAccount != null) {
      if (paramString != null)
        try {
          return getContentService().isSyncActive(paramAccount, paramString, null);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      throw new IllegalArgumentException("authority must not be null");
    } 
    throw new IllegalArgumentException("account must not be null");
  }
  
  @Deprecated
  public static SyncInfo getCurrentSync() {
    try {
      List<SyncInfo> list = getContentService().getCurrentSyncs();
      if (list.isEmpty())
        return null; 
      return list.get(0);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static List<SyncInfo> getCurrentSyncs() {
    try {
      return getContentService().getCurrentSyncs();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static List<SyncInfo> getCurrentSyncsAsUser(int paramInt) {
    try {
      return getContentService().getCurrentSyncsAsUser(paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static SyncStatusInfo getSyncStatus(Account paramAccount, String paramString) {
    try {
      return getContentService().getSyncStatus(paramAccount, paramString, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static SyncStatusInfo getSyncStatusAsUser(Account paramAccount, String paramString, int paramInt) {
    try {
      return getContentService().getSyncStatusAsUser(paramAccount, paramString, null, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean isSyncPending(Account paramAccount, String paramString) {
    return isSyncPendingAsUser(paramAccount, paramString, UserHandle.myUserId());
  }
  
  public static boolean isSyncPendingAsUser(Account paramAccount, String paramString, int paramInt) {
    try {
      return getContentService().isSyncPendingAsUser(paramAccount, paramString, null, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static Object addStatusChangeListener(int paramInt, SyncStatusObserver paramSyncStatusObserver) {
    if (paramSyncStatusObserver != null)
      try {
        Object object = new Object();
        super(paramSyncStatusObserver);
        getContentService().addStatusChangeListener(paramInt, (ISyncStatusObserver)object);
        return object;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("you passed in a null callback");
  }
  
  public static void removeStatusChangeListener(Object paramObject) {
    if (paramObject != null)
      try {
        getContentService().removeStatusChangeListener((ISyncStatusObserver.Stub)paramObject);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    throw new IllegalArgumentException("you passed in a null handle");
  }
  
  @SystemApi
  public void putCache(Uri paramUri, Bundle paramBundle) {
    try {
      IContentService iContentService = getContentService();
      String str = this.mContext.getPackageName();
      Context context = this.mContext;
      int i = context.getUserId();
      iContentService.putCache(str, paramUri, paramBundle, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public Bundle getCache(Uri paramUri) {
    try {
      IContentService iContentService = getContentService();
      String str = this.mContext.getPackageName();
      Context context = this.mContext;
      int i = context.getUserId();
      Bundle bundle = iContentService.getCache(str, paramUri, i);
      if (bundle != null)
        bundle.setClassLoader(this.mContext.getClassLoader()); 
      return bundle;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getTargetSdkVersion() {
    return this.mTargetSdkVersion;
  }
  
  private int samplePercentForDuration(long paramLong) {
    if (paramLong >= 500L)
      return 100; 
    return (int)(100L * paramLong / 500L) + 1;
  }
  
  private void maybeLogQueryToEventLog(long paramLong, Uri paramUri, String[] paramArrayOfString, Bundle paramBundle) {}
  
  private void maybeLogUpdateToEventLog(long paramLong, Uri paramUri, String paramString1, String paramString2) {}
  
  class CursorWrapperInner extends CrossProcessCursorWrapper {
    private final CloseGuard mCloseGuard;
    
    private final IContentProvider mContentProvider;
    
    private final AtomicBoolean mProviderReleased = new AtomicBoolean();
    
    final ContentResolver this$0;
    
    CursorWrapperInner(Cursor param1Cursor, IContentProvider param1IContentProvider) {
      super(param1Cursor);
      CloseGuard closeGuard = CloseGuard.get();
      this.mContentProvider = param1IContentProvider;
      closeGuard.open("close");
    }
    
    public void close() {
      this.mCloseGuard.close();
      super.close();
      if (this.mProviderReleased.compareAndSet(false, true))
        ContentResolver.this.releaseProvider(this.mContentProvider); 
    }
    
    protected void finalize() throws Throwable {
      try {
        if (this.mCloseGuard != null)
          this.mCloseGuard.warnIfOpen(); 
        close();
        return;
      } finally {
        super.finalize();
      } 
    }
  }
  
  class ParcelFileDescriptorInner extends ParcelFileDescriptor {
    private final IContentProvider mContentProvider;
    
    private final AtomicBoolean mProviderReleased = new AtomicBoolean();
    
    final ContentResolver this$0;
    
    ParcelFileDescriptorInner(ParcelFileDescriptor param1ParcelFileDescriptor, IContentProvider param1IContentProvider) {
      super(param1ParcelFileDescriptor);
      this.mContentProvider = param1IContentProvider;
    }
    
    public void releaseResources() {
      if (this.mProviderReleased.compareAndSet(false, true))
        ContentResolver.this.releaseProvider(this.mContentProvider); 
    }
  }
  
  public static IContentService getContentService() {
    if (sContentService != null)
      return sContentService; 
    IBinder iBinder = ServiceManager.getService("content");
    sContentService = IContentService.Stub.asInterface(iBinder);
    return sContentService;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public String getAttributionTag() {
    return this.mAttributionTag;
  }
  
  public int resolveUserId(Uri paramUri) {
    return ContentProvider.getUserIdFromUri(paramUri, this.mContext.getUserId());
  }
  
  public int getUserId() {
    return this.mContext.getUserId();
  }
  
  @Deprecated
  public Drawable getTypeDrawable(String paramString) {
    return getTypeInfo(paramString).getIcon().loadDrawable(this.mContext);
  }
  
  public final MimeTypeInfo getTypeInfo(String paramString) {
    Objects.requireNonNull(paramString);
    return MimeIconUtils.getTypeInfo(paramString);
  }
  
  class MimeTypeInfo {
    private final CharSequence mContentDescription;
    
    private final Icon mIcon;
    
    private final CharSequence mLabel;
    
    public MimeTypeInfo(ContentResolver this$0, CharSequence param1CharSequence1, CharSequence param1CharSequence2) {
      Objects.requireNonNull(this$0);
      this.mIcon = (Icon)this$0;
      Objects.requireNonNull(param1CharSequence1);
      this.mLabel = param1CharSequence1;
      Objects.requireNonNull(param1CharSequence2);
      this.mContentDescription = param1CharSequence2;
    }
    
    public Icon getIcon() {
      return this.mIcon;
    }
    
    public CharSequence getLabel() {
      return this.mLabel;
    }
    
    public CharSequence getContentDescription() {
      return this.mContentDescription;
    }
  }
  
  public static Bundle createSqlQueryBundle(String paramString, String[] paramArrayOfString) {
    return createSqlQueryBundle(paramString, paramArrayOfString, null);
  }
  
  public static Bundle createSqlQueryBundle(String paramString1, String[] paramArrayOfString, String paramString2) {
    if (paramString1 == null && paramArrayOfString == null && paramString2 == null)
      return null; 
    Bundle bundle = new Bundle();
    if (paramString1 != null)
      bundle.putString("android:query-arg-sql-selection", paramString1); 
    if (paramArrayOfString != null)
      bundle.putStringArray("android:query-arg-sql-selection-args", paramArrayOfString); 
    if (paramString2 != null)
      bundle.putString("android:query-arg-sql-sort-order", paramString2); 
    return bundle;
  }
  
  public static Bundle includeSqlSelectionArgs(Bundle paramBundle, String paramString, String[] paramArrayOfString) {
    if (paramString != null)
      paramBundle.putString("android:query-arg-sql-selection", paramString); 
    if (paramArrayOfString != null)
      paramBundle.putStringArray("android:query-arg-sql-selection-args", paramArrayOfString); 
    return paramBundle;
  }
  
  public static String createSqlSortClause(Bundle paramBundle) {
    // Byte code:
    //   0: aload_0
    //   1: ldc 'android:query-arg-sort-columns'
    //   3: invokevirtual getStringArray : (Ljava/lang/String;)[Ljava/lang/String;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnull -> 168
    //   11: aload_1
    //   12: arraylength
    //   13: ifeq -> 168
    //   16: ldc_w ', '
    //   19: aload_1
    //   20: invokestatic join : (Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;
    //   23: astore_2
    //   24: aload_0
    //   25: ldc 'android:query-arg-sort-collation'
    //   27: iconst_3
    //   28: invokevirtual getInt : (Ljava/lang/String;I)I
    //   31: istore_3
    //   32: iload_3
    //   33: ifeq -> 43
    //   36: aload_2
    //   37: astore_1
    //   38: iload_3
    //   39: iconst_1
    //   40: if_icmpne -> 70
    //   43: new java/lang/StringBuilder
    //   46: dup
    //   47: invokespecial <init> : ()V
    //   50: astore_1
    //   51: aload_1
    //   52: aload_2
    //   53: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   56: pop
    //   57: aload_1
    //   58: ldc_w ' COLLATE NOCASE'
    //   61: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   64: pop
    //   65: aload_1
    //   66: invokevirtual toString : ()Ljava/lang/String;
    //   69: astore_1
    //   70: aload_0
    //   71: ldc 'android:query-arg-sort-direction'
    //   73: ldc_w -2147483648
    //   76: invokevirtual getInt : (Ljava/lang/String;I)I
    //   79: istore_3
    //   80: aload_1
    //   81: astore_0
    //   82: iload_3
    //   83: ldc_w -2147483648
    //   86: if_icmpeq -> 166
    //   89: iload_3
    //   90: ifeq -> 139
    //   93: iload_3
    //   94: iconst_1
    //   95: if_icmpne -> 128
    //   98: new java/lang/StringBuilder
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: astore_0
    //   106: aload_0
    //   107: aload_1
    //   108: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload_0
    //   113: ldc_w ' DESC'
    //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: aload_0
    //   121: invokevirtual toString : ()Ljava/lang/String;
    //   124: astore_0
    //   125: goto -> 166
    //   128: new java/lang/IllegalArgumentException
    //   131: dup
    //   132: ldc_w 'Unsupported sort direction value. See ContentResolver documentation for details.'
    //   135: invokespecial <init> : (Ljava/lang/String;)V
    //   138: athrow
    //   139: new java/lang/StringBuilder
    //   142: dup
    //   143: invokespecial <init> : ()V
    //   146: astore_0
    //   147: aload_0
    //   148: aload_1
    //   149: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload_0
    //   154: ldc_w ' ASC'
    //   157: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload_0
    //   162: invokevirtual toString : ()Ljava/lang/String;
    //   165: astore_0
    //   166: aload_0
    //   167: areturn
    //   168: new java/lang/IllegalArgumentException
    //   171: dup
    //   172: ldc_w 'Can't create sort clause without columns.'
    //   175: invokespecial <init> : (Ljava/lang/String;)V
    //   178: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4004	-> 0
    //   #4005	-> 7
    //   #4009	-> 16
    //   #4013	-> 24
    //   #4015	-> 32
    //   #4016	-> 43
    //   #4019	-> 70
    //   #4020	-> 80
    //   #4021	-> 89
    //   #4026	-> 98
    //   #4027	-> 125
    //   #4029	-> 128
    //   #4023	-> 139
    //   #4033	-> 166
    //   #4006	-> 168
  }
  
  public Bitmap loadThumbnail(Uri paramUri, Size paramSize, CancellationSignal paramCancellationSignal) throws IOException {
    return loadThumbnail(this, paramUri, paramSize, paramCancellationSignal, 1);
  }
  
  public static Bitmap loadThumbnail(ContentInterface paramContentInterface, Uri paramUri, Size paramSize, CancellationSignal paramCancellationSignal, int paramInt) throws IOException {
    Objects.requireNonNull(paramContentInterface);
    Objects.requireNonNull(paramUri);
    Objects.requireNonNull(paramSize);
    Bundle bundle = new Bundle();
    bundle.putParcelable("android.content.extra.SIZE", Point.convert(paramSize));
    Int32Ref int32Ref = new Int32Ref(0);
    Bitmap bitmap2 = ImageDecoder.decodeBitmap(ImageDecoder.createSource(new _$$Lambda$ContentResolver$7ILY1SWNxC2xhk_fQUG6tAXW9Ik(paramContentInterface, paramUri, bundle, paramCancellationSignal, int32Ref)), new _$$Lambda$ContentResolver$RVw7W0M7r0cGmbYi8rAG5GKxq4M(paramInt, paramCancellationSignal, paramSize));
    Bitmap bitmap1 = bitmap2;
    if (int32Ref.value != 0) {
      int i = bitmap2.getWidth();
      paramInt = bitmap2.getHeight();
      Matrix matrix = new Matrix();
      matrix.setRotate(int32Ref.value, (i / 2), (paramInt / 2));
      bitmap1 = Bitmap.createBitmap(bitmap2, 0, 0, i, paramInt, matrix, false);
    } 
    return bitmap1;
  }
  
  public static void onDbCorruption(String paramString1, String paramString2, Throwable paramThrowable) {
    try {
      getContentService().onDbCorruption(paramString1, paramString2, Log.getStackTraceString(paramThrowable));
    } catch (RemoteException remoteException) {
      remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public static Uri decodeFromFile(File paramFile) {
    return translateDeprecatedDataPath(paramFile.getAbsolutePath());
  }
  
  @SystemApi
  public static File encodeToFile(Uri paramUri) {
    return new File(translateDeprecatedDataPath(paramUri));
  }
  
  public static Uri translateDeprecatedDataPath(String paramString) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("//");
    stringBuilder.append(paramString.substring("/mnt/content/".length()));
    paramString = stringBuilder.toString();
    Uri.Builder builder = (new Uri.Builder()).scheme("content");
    paramString = builder.encodedOpaquePart(paramString).build().toString();
    return Uri.parse(paramString);
  }
  
  public static String translateDeprecatedDataPath(Uri paramUri) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("/mnt/content/");
    stringBuilder.append(paramUri.getEncodedSchemeSpecificPart().substring(2));
    return stringBuilder.toString();
  }
  
  protected abstract IContentProvider acquireProvider(Context paramContext, String paramString);
  
  protected abstract IContentProvider acquireUnstableProvider(Context paramContext, String paramString);
  
  public abstract boolean releaseProvider(IContentProvider paramIContentProvider);
  
  public abstract boolean releaseUnstableProvider(IContentProvider paramIContentProvider);
  
  public abstract void unstableProviderDied(IContentProvider paramIContentProvider);
  
  @Retention(RetentionPolicy.SOURCE)
  class NotifyFlags implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class QueryCollator implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SortDirection implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SyncExemption implements Annotation {}
}
