package android.content;

import android.os.IBinder;
import android.os.RemoteException;
import android.os.SystemClock;

public class SyncContext {
  private static final long HEARTBEAT_SEND_INTERVAL_IN_MS = 1000L;
  
  private long mLastHeartbeatSendTime;
  
  private ISyncContext mSyncContext;
  
  public SyncContext(ISyncContext paramISyncContext) {
    this.mSyncContext = paramISyncContext;
    this.mLastHeartbeatSendTime = 0L;
  }
  
  public void setStatusText(String paramString) {
    updateHeartbeat();
  }
  
  private void updateHeartbeat() {
    long l = SystemClock.elapsedRealtime();
    if (l < this.mLastHeartbeatSendTime + 1000L)
      return; 
    try {
      this.mLastHeartbeatSendTime = l;
      if (this.mSyncContext != null)
        this.mSyncContext.sendHeartbeat(); 
    } catch (RemoteException remoteException) {}
  }
  
  public void onFinished(SyncResult paramSyncResult) {
    try {
      if (this.mSyncContext != null)
        this.mSyncContext.onFinished(paramSyncResult); 
    } catch (RemoteException remoteException) {}
  }
  
  public IBinder getSyncContextBinder() {
    IBinder iBinder;
    ISyncContext iSyncContext = this.mSyncContext;
    if (iSyncContext == null) {
      iSyncContext = null;
    } else {
      iBinder = iSyncContext.asBinder();
    } 
    return iBinder;
  }
}
