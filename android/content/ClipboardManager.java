package android.content;

import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.ClipboardManager;
import java.util.ArrayList;
import java.util.Objects;

public class ClipboardManager extends ClipboardManager {
  private final Context mContext;
  
  private final Handler mHandler;
  
  private final ArrayList<OnPrimaryClipChangedListener> mPrimaryClipChangedListeners = new ArrayList<>();
  
  private final IOnPrimaryClipChangedListener.Stub mPrimaryClipChangedServiceListener = (IOnPrimaryClipChangedListener.Stub)new Object(this);
  
  private final IClipboard mService;
  
  public ClipboardManager(Context paramContext, Handler paramHandler) throws ServiceManager.ServiceNotFoundException {
    this.mContext = paramContext;
    this.mHandler = paramHandler;
    IBinder iBinder = ServiceManager.getServiceOrThrow("clipboard");
    this.mService = IClipboard.Stub.asInterface(iBinder);
  }
  
  public void setPrimaryClip(ClipData paramClipData) {
    try {
      Objects.requireNonNull(paramClipData);
      paramClipData.prepareToLeaveProcess(true);
      this.mService.setPrimaryClip(paramClipData, this.mContext.getOpPackageName(), this.mContext.getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearPrimaryClip() {
    try {
      this.mService.clearPrimaryClip(this.mContext.getOpPackageName(), this.mContext.getUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ClipData getPrimaryClip() {
    try {
      return this.mService.getPrimaryClip(this.mContext.getOpPackageName(), this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public ClipDescription getPrimaryClipDescription() {
    try {
      IClipboard iClipboard = this.mService;
      String str = this.mContext.getOpPackageName();
      Context context = this.mContext;
      int i = context.getUserId();
      return iClipboard.getPrimaryClipDescription(str, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean hasPrimaryClip() {
    try {
      return this.mService.hasPrimaryClip(this.mContext.getOpPackageName(), this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void addPrimaryClipChangedListener(OnPrimaryClipChangedListener paramOnPrimaryClipChangedListener) {
    synchronized (this.mPrimaryClipChangedListeners) {
      boolean bool = this.mPrimaryClipChangedListeners.isEmpty();
      if (bool)
        try {
          IClipboard iClipboard = this.mService;
          IOnPrimaryClipChangedListener.Stub stub = this.mPrimaryClipChangedServiceListener;
          Context context = this.mContext;
          String str = context.getOpPackageName();
          context = this.mContext;
          int i = context.getUserId();
          iClipboard.addPrimaryClipChangedListener(stub, str, i);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      this.mPrimaryClipChangedListeners.add(remoteException);
      return;
    } 
  }
  
  public void removePrimaryClipChangedListener(OnPrimaryClipChangedListener paramOnPrimaryClipChangedListener) {
    synchronized (this.mPrimaryClipChangedListeners) {
      this.mPrimaryClipChangedListeners.remove(paramOnPrimaryClipChangedListener);
      boolean bool = this.mPrimaryClipChangedListeners.isEmpty();
      if (bool)
        try {
          IClipboard iClipboard = this.mService;
          IOnPrimaryClipChangedListener.Stub stub = this.mPrimaryClipChangedServiceListener;
          Context context1 = this.mContext;
          String str = context1.getOpPackageName();
          Context context2 = this.mContext;
          int i = context2.getUserId();
          iClipboard.removePrimaryClipChangedListener(stub, str, i);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      return;
    } 
  }
  
  @Deprecated
  public CharSequence getText() {
    ClipData clipData = getPrimaryClip();
    if (clipData != null && clipData.getItemCount() > 0)
      return clipData.getItemAt(0).coerceToText(this.mContext); 
    return null;
  }
  
  @Deprecated
  public void setText(CharSequence paramCharSequence) {
    setPrimaryClip(ClipData.newPlainText(null, paramCharSequence));
  }
  
  @Deprecated
  public boolean hasText() {
    try {
      return this.mService.hasClipboardText(this.mContext.getOpPackageName(), this.mContext.getUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  void reportPrimaryClipChanged() {
    synchronized (this.mPrimaryClipChangedListeners) {
      int i = this.mPrimaryClipChangedListeners.size();
      if (i <= 0)
        return; 
      Object[] arrayOfObject = this.mPrimaryClipChangedListeners.toArray();
      for (i = 0; i < arrayOfObject.length; i++)
        ((OnPrimaryClipChangedListener)arrayOfObject[i]).onPrimaryClipChanged(); 
      return;
    } 
  }
  
  class OnPrimaryClipChangedListener {
    public abstract void onPrimaryClipChanged();
  }
}
