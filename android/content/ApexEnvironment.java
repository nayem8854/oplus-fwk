package android.content;

import android.annotation.SystemApi;
import android.os.Environment;
import android.os.UserHandle;
import java.io.File;
import java.util.Objects;

@SystemApi
public class ApexEnvironment {
  private static final String APEX_DATA = "apexdata";
  
  private final String mApexModuleName;
  
  public static ApexEnvironment getApexEnvironment(String paramString) {
    Objects.requireNonNull(paramString, "apexModuleName cannot be null");
    return new ApexEnvironment(paramString);
  }
  
  private ApexEnvironment(String paramString) {
    this.mApexModuleName = paramString;
  }
  
  public File getDeviceProtectedDataDir() {
    File file = Environment.getDataMiscDirectory();
    String str = this.mApexModuleName;
    return Environment.buildPath(file, new String[] { "apexdata", str });
  }
  
  public File getDeviceProtectedDataDirForUser(UserHandle paramUserHandle) {
    File file = Environment.getDataMiscDeDirectory(paramUserHandle.getIdentifier());
    String str = this.mApexModuleName;
    return Environment.buildPath(file, new String[] { "apexdata", str });
  }
  
  public File getCredentialProtectedDataDirForUser(UserHandle paramUserHandle) {
    File file = Environment.getDataMiscCeDirectory(paramUserHandle.getIdentifier());
    String str = this.mApexModuleName;
    return Environment.buildPath(file, new String[] { "apexdata", str });
  }
}
