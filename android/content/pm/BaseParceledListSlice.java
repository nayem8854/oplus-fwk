package android.content.pm;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

abstract class BaseParceledListSlice<T> implements Parcelable {
  private static boolean DEBUG;
  
  private static final int MAX_IPC_SIZE;
  
  private static String TAG = "ParceledListSlice";
  
  static {
    DEBUG = false;
    MAX_IPC_SIZE = IBinder.getSuggestedMaxIpcSizeBytes();
  }
  
  private int mInlineCountLimit = Integer.MAX_VALUE;
  
  private final List<T> mList;
  
  public BaseParceledListSlice(List<T> paramList) {
    this.mList = paramList;
  }
  
  BaseParceledListSlice(Parcel paramParcel, ClassLoader paramClassLoader) {
    int i = paramParcel.readInt();
    this.mList = new ArrayList<>(i);
    if (DEBUG) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Retrieving ");
      stringBuilder.append(i);
      stringBuilder.append(" items");
      Log.d(str, stringBuilder.toString());
    } 
    if (i <= 0)
      return; 
    Parcelable.Creator<?> creator = readParcelableCreator(paramParcel, paramClassLoader);
    Class<?> clazz = null;
    byte b = 0;
    while (b < i && 
      paramParcel.readInt() != 0) {
      T t = readCreator(creator, paramParcel, paramClassLoader);
      if (clazz == null) {
        clazz = t.getClass();
      } else {
        verifySameType(clazz, t.getClass());
      } 
      this.mList.add(t);
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Read inline #");
        stringBuilder.append(b);
        stringBuilder.append(": ");
        List<T> list = this.mList;
        stringBuilder.append(list.get(list.size() - 1));
        Log.d(str, stringBuilder.toString());
      } 
      b++;
    } 
    if (b >= i)
      return; 
    IBinder iBinder = paramParcel.readStrongBinder();
    while (b < i) {
      if (DEBUG) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Reading more @");
        stringBuilder.append(b);
        stringBuilder.append(" of ");
        stringBuilder.append(i);
        stringBuilder.append(": retriever=");
        stringBuilder.append(iBinder);
        Log.d(str, stringBuilder.toString());
      } 
      Parcel parcel1 = Parcel.obtain();
      Parcel parcel2 = Parcel.obtain();
      parcel1.writeInt(b);
      try {
        iBinder.transact(1, parcel1, parcel2, 0);
        while (b < i && parcel2.readInt() != 0) {
          T t = readCreator(creator, parcel2, paramClassLoader);
          verifySameType(clazz, t.getClass());
          this.mList.add(t);
          if (DEBUG) {
            String str = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Read extra #");
            stringBuilder.append(b);
            stringBuilder.append(": ");
            List<T> list = this.mList;
            stringBuilder.append(list.get(list.size() - 1));
            Log.d(str, stringBuilder.toString());
          } 
          b++;
        } 
        parcel2.recycle();
        parcel1.recycle();
      } catch (RemoteException remoteException) {
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failure retrieving array; only received ");
        stringBuilder.append(b);
        stringBuilder.append(" of ");
        stringBuilder.append(i);
        Log.w(str, stringBuilder.toString(), (Throwable)remoteException);
        return;
      } 
    } 
  }
  
  private T readCreator(Parcelable.Creator<?> paramCreator, Parcel paramParcel, ClassLoader paramClassLoader) {
    Parcelable.ClassLoaderCreator classLoaderCreator;
    if (paramCreator instanceof Parcelable.ClassLoaderCreator) {
      classLoaderCreator = (Parcelable.ClassLoaderCreator)paramCreator;
      return (T)classLoaderCreator.createFromParcel(paramParcel, paramClassLoader);
    } 
    return (T)classLoaderCreator.createFromParcel(paramParcel);
  }
  
  private static void verifySameType(Class<?> paramClass1, Class<?> paramClass2) {
    if (!paramClass2.equals(paramClass1)) {
      String str1, str2;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Can't unparcel type ");
      Class clazz = null;
      if (paramClass2 == null) {
        paramClass2 = null;
      } else {
        str2 = paramClass2.getName();
      } 
      stringBuilder.append(str2);
      stringBuilder.append(" in list of type ");
      if (paramClass1 == null) {
        paramClass1 = clazz;
      } else {
        str1 = paramClass1.getName();
      } 
      stringBuilder.append(str1);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
  }
  
  public List<T> getList() {
    return this.mList;
  }
  
  public void setInlineCountLimit(int paramInt) {
    this.mInlineCountLimit = paramInt;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    int i = this.mList.size();
    paramParcel.writeInt(i);
    if (DEBUG) {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Writing ");
      stringBuilder.append(i);
      stringBuilder.append(" items");
      Log.d(str, stringBuilder.toString());
    } 
    if (i > 0) {
      Class<?> clazz = this.mList.get(0).getClass();
      writeParcelableCreator(this.mList.get(0), paramParcel);
      byte b = 0;
      while (b < i && b < this.mInlineCountLimit && paramParcel.dataSize() < MAX_IPC_SIZE) {
        paramParcel.writeInt(1);
        T t = this.mList.get(b);
        verifySameType(clazz, t.getClass());
        writeElement(t, paramParcel, paramInt);
        if (DEBUG) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Wrote inline #");
          stringBuilder.append(b);
          stringBuilder.append(": ");
          stringBuilder.append(this.mList.get(b));
          Log.d(str, stringBuilder.toString());
        } 
        b++;
      } 
      if (b < i) {
        paramParcel.writeInt(0);
        Object object = new Object(this, i, clazz, paramInt);
        if (DEBUG) {
          String str = TAG;
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Breaking @");
          stringBuilder.append(b);
          stringBuilder.append(" of ");
          stringBuilder.append(i);
          stringBuilder.append(": retriever=");
          stringBuilder.append(object);
          Log.d(str, stringBuilder.toString());
        } 
        paramParcel.writeStrongBinder((IBinder)object);
      } 
    } 
  }
  
  protected abstract Parcelable.Creator<?> readParcelableCreator(Parcel paramParcel, ClassLoader paramClassLoader);
  
  protected abstract void writeElement(T paramT, Parcel paramParcel, int paramInt);
  
  protected abstract void writeParcelableCreator(T paramT, Parcel paramParcel);
}
