package android.content.pm;

import android.common.OplusFeatureCache;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.IOplusThemeManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;

public class LauncherActivityInfo {
  private static final String TAG = "LauncherActivityInfo";
  
  private ActivityInfo mActivityInfo;
  
  private ComponentName mComponentName;
  
  private final PackageManager mPm;
  
  private UserHandle mUser;
  
  LauncherActivityInfo(Context paramContext, ActivityInfo paramActivityInfo, UserHandle paramUserHandle) {
    this(paramContext);
    this.mActivityInfo = paramActivityInfo;
    this.mComponentName = new ComponentName(paramActivityInfo.packageName, paramActivityInfo.name);
    this.mUser = paramUserHandle;
  }
  
  LauncherActivityInfo(Context paramContext) {
    this.mPm = paramContext.getPackageManager();
  }
  
  public ComponentName getComponentName() {
    return this.mComponentName;
  }
  
  public UserHandle getUser() {
    return this.mUser;
  }
  
  public CharSequence getLabel() {
    return this.mActivityInfo.loadLabel(this.mPm);
  }
  
  public Drawable getIcon(int paramInt) {
    Drawable drawable;
    int i = this.mActivityInfo.getIconResource();
    ApplicationInfo applicationInfo1 = null;
    ApplicationInfo applicationInfo2 = applicationInfo1;
    if (!((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).isOplusIcons()) {
      applicationInfo2 = applicationInfo1;
      if (paramInt != 0) {
        applicationInfo2 = applicationInfo1;
        if (i != 0)
          try {
            PackageManager packageManager = this.mPm;
            applicationInfo2 = this.mActivityInfo.applicationInfo;
            Resources resources = packageManager.getResourcesForApplication(applicationInfo2);
            Drawable drawable1 = resources.getDrawableForDensity(i, paramInt);
          } catch (NameNotFoundException|android.content.res.Resources.NotFoundException nameNotFoundException) {
            applicationInfo2 = applicationInfo1;
          }  
      } 
    } 
    applicationInfo1 = applicationInfo2;
    if (applicationInfo2 == null)
      drawable = this.mActivityInfo.loadIcon(this.mPm); 
    return drawable;
  }
  
  public int getApplicationFlags() {
    return this.mActivityInfo.applicationInfo.flags;
  }
  
  public ApplicationInfo getApplicationInfo() {
    return this.mActivityInfo.applicationInfo;
  }
  
  public long getFirstInstallTime() {
    try {
      return (this.mPm.getPackageInfo(this.mActivityInfo.packageName, 8192)).firstInstallTime;
    } catch (NameNotFoundException nameNotFoundException) {
      return 0L;
    } 
  }
  
  public String getName() {
    return this.mActivityInfo.name;
  }
  
  public Drawable getBadgedIcon(int paramInt) {
    return ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).getBadgedIcon(this, paramInt, this.mPm, this.mUser, this.mActivityInfo);
  }
}
