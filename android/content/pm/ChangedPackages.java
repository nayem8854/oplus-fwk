package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public final class ChangedPackages implements Parcelable {
  public ChangedPackages(int paramInt, List<String> paramList) {
    this.mSequenceNumber = paramInt;
    this.mPackageNames = paramList;
  }
  
  protected ChangedPackages(Parcel paramParcel) {
    this.mSequenceNumber = paramParcel.readInt();
    this.mPackageNames = paramParcel.createStringArrayList();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mSequenceNumber);
    paramParcel.writeStringList(this.mPackageNames);
  }
  
  public int getSequenceNumber() {
    return this.mSequenceNumber;
  }
  
  public List<String> getPackageNames() {
    return this.mPackageNames;
  }
  
  public static final Parcelable.Creator<ChangedPackages> CREATOR = (Parcelable.Creator<ChangedPackages>)new Object();
  
  private final List<String> mPackageNames;
  
  private final int mSequenceNumber;
}
