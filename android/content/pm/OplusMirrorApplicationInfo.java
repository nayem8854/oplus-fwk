package android.content.pm;

import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefInt;
import com.oplus.reflect.RefObject;

public class OplusMirrorApplicationInfo {
  public static Class<?> TYPE = RefClass.load(OplusMirrorApplicationInfo.class, ApplicationInfo.class);
  
  public static RefObject<OplusApplicationInfoEx> mOplusApplicationInfoEx;
  
  public static RefInt oplusPrivateFlags;
}
