package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class VersionedPackage extends OplusBaseVersionedPackage implements Parcelable {
  public VersionedPackage(String paramString, int paramInt) {
    this.mPackageName = paramString;
    this.mVersionCode = paramInt;
  }
  
  public VersionedPackage(String paramString, long paramLong) {
    this.mPackageName = paramString;
    this.mVersionCode = paramLong;
  }
  
  private VersionedPackage(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString8();
    this.mVersionCode = paramParcel.readLong();
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  @Deprecated
  public int getVersionCode() {
    return (int)(this.mVersionCode & 0x7FFFFFFFL);
  }
  
  public long getLongVersionCode() {
    return this.mVersionCode;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("VersionedPackage[");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append("/");
    stringBuilder.append(this.mVersionCode);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject instanceof VersionedPackage) {
      String str1 = ((VersionedPackage)paramObject).mPackageName, str2 = this.mPackageName;
      if (str1.equals(str2) && ((VersionedPackage)paramObject).mVersionCode == this.mVersionCode)
        return true; 
    } 
    return false;
  }
  
  public int hashCode() {
    return this.mPackageName.hashCode() * 31 + Long.hashCode(this.mVersionCode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString8(this.mPackageName);
    paramParcel.writeLong(this.mVersionCode);
  }
  
  public static final Parcelable.Creator<VersionedPackage> CREATOR = new Parcelable.Creator<VersionedPackage>() {
      public VersionedPackage createFromParcel(Parcel param1Parcel) {
        return new VersionedPackage(param1Parcel);
      }
      
      public VersionedPackage[] newArray(int param1Int) {
        return new VersionedPackage[param1Int];
      }
    };
  
  private final String mPackageName;
  
  private final long mVersionCode;
  
  @Retention(RetentionPolicy.SOURCE)
  class VersionCode implements Annotation {}
}
