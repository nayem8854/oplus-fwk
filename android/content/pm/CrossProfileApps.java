package android.content.pm;

import android.annotation.SystemApi;
import android.app.Activity;
import android.app.IApplicationThread;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserHandle;
import android.os.UserManager;
import com.android.internal.util.UserIcons;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CrossProfileApps {
  public static final String ACTION_CAN_INTERACT_ACROSS_PROFILES_CHANGED = "android.content.pm.action.CAN_INTERACT_ACROSS_PROFILES_CHANGED";
  
  private final Context mContext;
  
  private final Resources mResources;
  
  private final ICrossProfileApps mService;
  
  private final UserManager mUserManager;
  
  public CrossProfileApps(Context paramContext, ICrossProfileApps paramICrossProfileApps) {
    this.mContext = paramContext;
    this.mService = paramICrossProfileApps;
    this.mUserManager = paramContext.<UserManager>getSystemService(UserManager.class);
    this.mResources = paramContext.getResources();
  }
  
  public void startMainActivity(ComponentName paramComponentName, UserHandle paramUserHandle) {
    try {
      ICrossProfileApps iCrossProfileApps = this.mService;
      Context context1 = this.mContext;
      IApplicationThread iApplicationThread = context1.getIApplicationThread();
      Context context2 = this.mContext;
      String str1 = context2.getPackageName();
      Context context3 = this.mContext;
      String str2 = context3.getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iCrossProfileApps.startActivityAsUser(iApplicationThread, str1, str2, paramComponentName, i, true);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void startActivity(Intent paramIntent, UserHandle paramUserHandle, Activity paramActivity) {
    startActivity(paramIntent, paramUserHandle, paramActivity, null);
  }
  
  public void startActivity(Intent paramIntent, UserHandle paramUserHandle, Activity paramActivity, Bundle paramBundle) {
    try {
      ICrossProfileApps iCrossProfileApps = this.mService;
      Context context1 = this.mContext;
      IApplicationThread iApplicationThread = context1.getIApplicationThread();
      Context context2 = this.mContext;
      String str1 = context2.getPackageName();
      Context context3 = this.mContext;
      String str2 = context3.getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      if (paramActivity != null) {
        IBinder iBinder = paramActivity.getActivityToken();
      } else {
        paramUserHandle = null;
      } 
      iCrossProfileApps.startActivityAsUserByIntent(iApplicationThread, str1, str2, paramIntent, i, (IBinder)paramUserHandle, paramBundle);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void startActivity(ComponentName paramComponentName, UserHandle paramUserHandle) {
    try {
      ICrossProfileApps iCrossProfileApps = this.mService;
      IApplicationThread iApplicationThread = this.mContext.getIApplicationThread();
      Context context = this.mContext;
      String str1 = context.getPackageName(), str2 = this.mContext.getAttributionTag();
      int i = paramUserHandle.getIdentifier();
      iCrossProfileApps.startActivityAsUser(iApplicationThread, str1, str2, paramComponentName, i, false);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<UserHandle> getTargetUserProfiles() {
    try {
      return this.mService.getTargetUserProfiles(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public CharSequence getProfileSwitchingLabel(UserHandle paramUserHandle) {
    int i;
    verifyCanAccessUser(paramUserHandle);
    if (this.mUserManager.isManagedProfile(paramUserHandle.getIdentifier())) {
      i = 17040522;
    } else {
      i = 17041456;
    } 
    return this.mResources.getString(i);
  }
  
  public Drawable getProfileSwitchingIconDrawable(UserHandle paramUserHandle) {
    verifyCanAccessUser(paramUserHandle);
    UserManager userManager = this.mUserManager;
    boolean bool = userManager.isManagedProfile(paramUserHandle.getIdentifier());
    if (bool)
      return this.mResources.getDrawable(17302382, null); 
    return UserIcons.getDefaultUserIcon(this.mResources, 0, true);
  }
  
  public boolean canRequestInteractAcrossProfiles() {
    try {
      return this.mService.canRequestInteractAcrossProfiles(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canInteractAcrossProfiles() {
    try {
      return this.mService.canInteractAcrossProfiles(this.mContext.getPackageName());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Intent createRequestInteractAcrossProfilesIntent() {
    if (canRequestInteractAcrossProfiles()) {
      Intent intent = new Intent();
      intent.setAction("android.settings.MANAGE_CROSS_PROFILE_ACCESS");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("package:");
      stringBuilder.append(this.mContext.getPackageName());
      Uri uri = Uri.parse(stringBuilder.toString());
      intent.setData(uri);
      return intent;
    } 
    throw new SecurityException("The calling package can not request to interact across profiles.");
  }
  
  public void setInteractAcrossProfilesAppOp(String paramString, int paramInt) {
    try {
      this.mService.setInteractAcrossProfilesAppOp(paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canConfigureInteractAcrossProfiles(String paramString) {
    try {
      return this.mService.canConfigureInteractAcrossProfiles(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean canUserAttemptToConfigureInteractAcrossProfiles(String paramString) {
    try {
      return this.mService.canUserAttemptToConfigureInteractAcrossProfiles(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void resetInteractAcrossProfilesAppOps(Collection<String> paramCollection, Set<String> paramSet) {
    if (paramCollection.isEmpty())
      return; 
    Stream<String> stream = paramCollection.stream();
    _$$Lambda$CrossProfileApps$q8A4D5J_ieuBm118fz0XxGYzHwQ _$$Lambda$CrossProfileApps$q8A4D5J_ieuBm118fz0XxGYzHwQ = new _$$Lambda$CrossProfileApps$q8A4D5J_ieuBm118fz0XxGYzHwQ(paramSet);
    stream = stream.filter(_$$Lambda$CrossProfileApps$q8A4D5J_ieuBm118fz0XxGYzHwQ);
    List<String> list = stream.collect((Collector)Collectors.toList());
    if (list.isEmpty())
      return; 
    try {
      this.mService.resetInteractAcrossProfilesAppOps(list);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void clearInteractAcrossProfilesAppOps() {
    try {
      this.mService.clearInteractAcrossProfilesAppOps();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void verifyCanAccessUser(UserHandle paramUserHandle) {
    if (getTargetUserProfiles().contains(paramUserHandle))
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not allowed to access ");
    stringBuilder.append(paramUserHandle);
    throw new SecurityException(stringBuilder.toString());
  }
}
