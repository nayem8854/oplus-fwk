package android.content.pm;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import com.oplus.app.OplusAppDynamicFeatureData;
import com.oplus.content.OplusRemovableAppInfo;
import com.oplus.content.OplusRuleInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class OplusPackageManager extends OplusBasePackageManager implements IOplusPackageManager {
  public static final int INSTALL_FROM_OPLUS_ADB_INSTALLER = 268435456;
  
  public static final int INSTALL_SPEED_BACKGROUND = -2147483648;
  
  public static final int INSTALL_SPEED_CPU_HIGH = 1073741824;
  
  public static final int INSTALL_SPEED_CPU_MID = 536870912;
  
  public static final int MATCH_OPLUS_FREEZE_APP = 1073741824;
  
  public static final int OPLUS_DONT_KILL_APP = 268435456;
  
  public static final int OPLUS_FREEZE_FLAG_AUTO = 2;
  
  public static final int OPLUS_FREEZE_FLAG_MANUAL = 1;
  
  public static final int OPLUS_UNFREEZE_FLAG_NORMAL = 1;
  
  public static final int OPLUS_UNFREEZE_FLAG_TEMP = 2;
  
  public static final int RE_INSTALL_DUPLICATE_PERMISSION = 1;
  
  public static final int STATE_OPLUS_FREEZE_FREEZED = 2;
  
  public static final int STATE_OPLUS_FREEZE_NORMAL = 0;
  
  public static final int STATE_OPLUS_FREEZE_TEMP_UNFREEZED = 1;
  
  private static final String TAG = "OplusPackageManager";
  
  private static HashMap<String, Bitmap> mActivityIconsCache;
  
  private static HashMap<String, Bitmap> mAppIconsCache = new HashMap<>();
  
  private static OplusPackageManager mColorPackageManager;
  
  private static boolean mIconCacheDirty = false;
  
  private static final int sDefaultFlags = 1024;
  
  private final Context mContext;
  
  static {
    mActivityIconsCache = new HashMap<>();
    mIconCacheDirty = false;
  }
  
  public OplusPackageManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public OplusPackageManager() {
    this.mContext = null;
  }
  
  public static OplusPackageManager getOplusPackageManager(Context paramContext) {
    OplusPackageManager oplusPackageManager2 = mColorPackageManager;
    if (oplusPackageManager2 != null)
      return oplusPackageManager2; 
    OplusPackageManager oplusPackageManager1 = new OplusPackageManager(paramContext);
    return oplusPackageManager1;
  }
  
  public boolean isClosedSuperFirewall() {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    boolean bool = false;
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      boolean bool1 = Boolean.valueOf(parcel2.readString()).booleanValue();
    } catch (RemoteException remoteException) {
      Log.e("OplusPackageManager", "isClosedSuperFirewall failed");
    } finally {
      Exception exception;
    } 
    parcel1.recycle();
    parcel2.recycle();
    return bool;
  }
  
  public Bitmap getAppIconBitmap(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10004, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0) {
        Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(parcel2);
      } else {
        paramString = null;
      } 
      return (Bitmap)paramString;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public Map getAppIconsCache(boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      boolean bool;
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      if (paramBoolean) {
        bool = true;
      } else {
        bool = false;
      } 
      parcel1.writeInt(bool);
      this.mRemote.transact(10005, parcel1, parcel2, 0);
      parcel2.readException();
      ClassLoader classLoader = getClass().getClassLoader();
      return parcel2.readHashMap(classLoader);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public Map getActivityIconsCache(IPackageDeleteObserver paramIPackageDeleteObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      if (paramIPackageDeleteObserver != null) {
        IBinder iBinder = paramIPackageDeleteObserver.asBinder();
      } else {
        paramIPackageDeleteObserver = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramIPackageDeleteObserver);
      this.mRemote.transact(10006, parcel1, parcel2, 0);
      parcel2.readException();
      ClassLoader classLoader = getClass().getClassLoader();
      return parcel2.readHashMap(classLoader);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean prohibitChildInstallation(int paramInt, boolean paramBoolean) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeInt(paramInt);
      parcel1.writeBoolean(paramBoolean);
      IBinder iBinder = this.mRemote;
      paramBoolean = false;
      iBinder.transact(10007, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        paramBoolean = true; 
      return paramBoolean;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int oplusFreezePackage(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10008, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt1 = parcel2.readInt();
      return paramInt1;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int oplusUnFreezePackage(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10009, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt1 = parcel2.readInt();
      return paramInt1;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getOplusFreezePackageState(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10010, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean inOplusFreezePackageList(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10011, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<String> getOplusFreezedPackageList(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10012, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public int getOplusPackageFreezeFlag(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10013, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      return paramInt;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean loadRegionFeature(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10014, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public FeatureInfo[] getOplusSystemAvailableFeatures() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10015, parcel1, parcel2, 0);
      parcel2.readException();
      return (FeatureInfo[])parcel2.createTypedArray(FeatureInfo.CREATOR);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isSecurePayApp(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10016, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isSystemDataApp(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10017, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean inPmsWhiteList(int paramInt, String paramString, List<String> paramList) {
    boolean bool2;
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    boolean bool1 = false;
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      parcel1.writeStringList(paramList);
      IBinder iBinder = this.mRemote;
      bool2 = false;
      iBinder.transact(10020, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool2 = true; 
    } catch (RemoteException remoteException) {
      Log.e("OplusPackageManager", "inPmsWhiteList failed");
      bool2 = bool1;
    } finally {}
    parcel2.recycle();
    parcel1.recycle();
    return bool2;
  }
  
  public boolean setInterceptRuleInfos(List<OplusRuleInfo> paramList) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeTypedList(paramList);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10018, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<OplusRuleInfo> getInterceptRuleInfos() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10019, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusRuleInfo.CREATOR);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isFullFunctionMode() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public Drawable getApplicationIconCacheAll(ApplicationInfo paramApplicationInfo) {
    Context context = this.mContext;
    if (context == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    return paramApplicationInfo.loadIcon(context.getPackageManager());
  }
  
  public Drawable getApplicationIconCache(ApplicationInfo paramApplicationInfo) {
    Context context = this.mContext;
    if (context == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    return paramApplicationInfo.loadIcon(context.getPackageManager());
  }
  
  public Drawable getApplicationIconCache(String paramString) throws PackageManager.NameNotFoundException {
    Context context1 = this.mContext;
    if (context1 == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    PackageManager packageManager = context1.getPackageManager();
    Context context2 = this.mContext;
    ApplicationInfo applicationInfo = context2.getPackageManager().getApplicationInfoAsUser(paramString, 1024, ActivityManager.getCurrentUser());
    return packageManager.getApplicationIcon(applicationInfo);
  }
  
  public Drawable getApplicationIconCacheOrignal(ApplicationInfo paramApplicationInfo) {
    Context context = this.mContext;
    if (context == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    return paramApplicationInfo.loadIcon(context.getPackageManager());
  }
  
  public Drawable getApplicationIconCacheOrignal(String paramString) throws PackageManager.NameNotFoundException {
    Context context1 = this.mContext;
    if (context1 == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    PackageManager packageManager = context1.getPackageManager();
    Context context2 = this.mContext;
    ApplicationInfo applicationInfo = context2.getPackageManager().getApplicationInfoAsUser(paramString, 1024, ActivityManager.getCurrentUser());
    return packageManager.getApplicationIcon(applicationInfo);
  }
  
  private final PackageDeleteObserver mPackageDeleleteObserver = new PackageDeleteObserver();
  
  private class PackageDeleteObserver extends IPackageDeleteObserver.Stub {
    final OplusPackageManager this$0;
    
    private PackageDeleteObserver() {}
    
    public void packageDeleted(String param1String, int param1Int) {
      if (param1String == null)
        return; 
      try {
        if (OplusPackageManager.mAppIconsCache.get(param1String) != null)
          OplusPackageManager.mAppIconsCache.remove(param1String); 
        Iterator<Map.Entry> iterator = OplusPackageManager.mActivityIconsCache.entrySet().iterator();
        ArrayList<String> arrayList = new ArrayList();
        this();
        while (iterator.hasNext()) {
          Map.Entry entry = iterator.next();
          String str = (String)entry.getKey();
          if (param1String.equals(str.split("/")[0]))
            arrayList.add(str); 
        } 
        for (String str : arrayList)
          OplusPackageManager.mActivityIconsCache.remove(str); 
        OplusPackageManager.access$302(true);
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    }
  }
  
  public Drawable getActivityIconCache(ComponentName paramComponentName) throws PackageManager.NameNotFoundException {
    Context context = this.mContext;
    if (context == null) {
      Log.e("OplusPackageManager", "App must create OplusPackageManager with context parameter when using this method");
      return null;
    } 
    return context.getPackageManager().getActivityInfo(paramComponentName, 1024).loadIcon(this.mContext.getPackageManager());
  }
  
  public List<String> getRemovableAppList() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10021, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<OplusRemovableAppInfo> getRemovedAppInfos() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10022, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusRemovableAppInfo.CREATOR);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<OplusRemovableAppInfo> getRemovableAppInfos() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10023, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createTypedArrayList(OplusRemovableAppInfo.CREATOR);
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public OplusRemovableAppInfo getRemovableAppInfo(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10024, parcel1, parcel2, 0);
      parcel2.readException();
      if (parcel2.readInt() != 0) {
        OplusRemovableAppInfo oplusRemovableAppInfo = (OplusRemovableAppInfo)OplusRemovableAppInfo.CREATOR.createFromParcel(parcel2);
      } else {
        paramString = null;
      } 
      return (OplusRemovableAppInfo)paramString;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean restoreRemovableApp(String paramString, IntentSender paramIntentSender, Bundle paramBundle) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      boolean bool = true;
      if (paramIntentSender != null) {
        parcel1.writeInt(1);
        paramIntentSender.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      if (paramBundle != null) {
        parcel1.writeInt(1);
        paramBundle.writeToParcel(parcel1, 0);
      } else {
        parcel1.writeInt(0);
      } 
      this.mRemote.transact(10025, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i == 0)
        bool = false; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void checkEMMApkRuntimePermission(ComponentName paramComponentName) throws SecurityException {
    String str = paramComponentName.getPackageName();
    if (str != null) {
      Parcel parcel2 = Parcel.obtain();
      Parcel parcel1 = Parcel.obtain();
      try {
        parcel2.writeInterfaceToken("android.app.IPackageManager");
        parcel2.writeString(str);
        this.mRemote.transact(10026, parcel2, parcel1, 0);
        parcel1.readException();
        str = parcel1.readString();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("check EMM apk runtime permission:");
        stringBuilder.append(str);
        Log.d("OplusPackageManager", stringBuilder.toString());
        boolean bool = "".equals(str);
        if (bool) {
          parcel1.recycle();
          parcel2.recycle();
          return;
        } 
        SecurityException securityException = new SecurityException();
        this(str);
        throw securityException;
      } catch (RemoteException remoteException) {
        SecurityException securityException = new SecurityException();
        this(remoteException.getMessage());
        throw securityException;
      } finally {}
      parcel1.recycle();
      parcel2.recycle();
      throw str;
    } 
    throw new SecurityException("Package name is null");
  }
  
  public boolean isSupportSessionWrite() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10027, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getCptListByType(int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10028, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void sendCptUpload(String paramString1, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      this.mRemote.transact(10029, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean inCptWhiteList(int paramInt, String paramString) {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    boolean bool = false;
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool1 = false;
      iBinder.transact(10030, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      bool = bool1;
      if (paramInt != 0)
        bool = true; 
    } catch (RemoteException remoteException) {
      Log.e("OplusPackageManager", "inCptWhiteList failed");
    } finally {}
    parcel2.recycle();
    parcel1.recycle();
    return bool;
  }
  
  public boolean inOplusStandardWhiteList(String paramString1, int paramInt, String paramString2) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeInt(paramInt);
      parcel1.writeString(paramString2);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10033, parcel1, parcel2, 0);
      parcel2.readException();
      paramInt = parcel2.readInt();
      if (paramInt != 0)
        bool = true; 
      return bool;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void sendMapCommonDcsUpload(String paramString1, String paramString2, Map paramMap) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString1);
      parcel1.writeString(paramString2);
      parcel1.writeMap(paramMap);
      this.mRemote.transact(10031, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<ApplicationInfo> getIconPackList() {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    ArrayList<ApplicationInfo> arrayList = null;
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10032, parcel1, parcel2, 0);
      parcel2.readException();
      ArrayList<ApplicationInfo> arrayList1 = parcel2.createTypedArrayList(ApplicationInfo.CREATOR);
    } catch (Exception exception) {
      Log.e("OplusPackageManager", exception.getMessage(), exception);
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    return arrayList;
  }
  
  public void dynamicDetectApp(OplusAppDynamicFeatureData paramOplusAppDynamicFeatureData) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      paramOplusAppDynamicFeatureData.writeToParcel(parcel1, 0);
      this.mRemote.transact(10034, parcel1, parcel2, 1);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public boolean isDetectApp(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      IBinder iBinder = this.mRemote;
      boolean bool = false;
      iBinder.transact(10035, parcel1, parcel2, 0);
      parcel2.readException();
      int i = parcel2.readInt();
      if (i != 0)
        bool = true; 
      return bool;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getDetectAppList() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10036, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public boolean isCrossVersionUpdate() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10037, parcel1, parcel2, 0);
      parcel2.readException();
      return Boolean.valueOf(parcel2.readString()).booleanValue();
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
  
  public List<String> getNotInstalledSystemApps() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10038, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<String> getValidAppList() throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      this.mRemote.transact(10039, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public List<String> getAppListFromPartition(String paramString) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      this.mRemote.transact(10040, parcel1, parcel2, 0);
      parcel2.readException();
      return parcel2.createStringArrayList();
    } finally {
      parcel2.recycle();
      parcel1.recycle();
    } 
  }
  
  public void deletePackageDelegated(String paramString, int paramInt1, int paramInt2, int paramInt3, int paramInt4, IPackageDeleteObserver paramIPackageDeleteObserver) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.app.IPackageManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt1);
      parcel1.writeInt(paramInt2);
      parcel1.writeInt(paramInt3);
      parcel1.writeInt(paramInt4);
      if (paramIPackageDeleteObserver != null) {
        IBinder iBinder = paramIPackageDeleteObserver.asBinder();
      } else {
        paramString = null;
      } 
      parcel1.writeStrongBinder((IBinder)paramString);
      this.mRemote.transact(10041, parcel1, parcel2, 0);
      parcel2.readException();
      parcel1.recycle();
      parcel2.recycle();
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } finally {}
    parcel1.recycle();
    parcel2.recycle();
    throw paramString;
  }
}
