package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPackageInstallerCallback extends IInterface {
  void onSessionActiveChanged(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onSessionBadgingChanged(int paramInt) throws RemoteException;
  
  void onSessionCreated(int paramInt) throws RemoteException;
  
  void onSessionFinished(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void onSessionProgressChanged(int paramInt, float paramFloat) throws RemoteException;
  
  class Default implements IPackageInstallerCallback {
    public void onSessionCreated(int param1Int) throws RemoteException {}
    
    public void onSessionBadgingChanged(int param1Int) throws RemoteException {}
    
    public void onSessionActiveChanged(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void onSessionProgressChanged(int param1Int, float param1Float) throws RemoteException {}
    
    public void onSessionFinished(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPackageInstallerCallback {
    private static final String DESCRIPTOR = "android.content.pm.IPackageInstallerCallback";
    
    static final int TRANSACTION_onSessionActiveChanged = 3;
    
    static final int TRANSACTION_onSessionBadgingChanged = 2;
    
    static final int TRANSACTION_onSessionCreated = 1;
    
    static final int TRANSACTION_onSessionFinished = 5;
    
    static final int TRANSACTION_onSessionProgressChanged = 4;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IPackageInstallerCallback");
    }
    
    public static IPackageInstallerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IPackageInstallerCallback");
      if (iInterface != null && iInterface instanceof IPackageInstallerCallback)
        return (IPackageInstallerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onSessionFinished";
            } 
            return "onSessionProgressChanged";
          } 
          return "onSessionActiveChanged";
        } 
        return "onSessionBadgingChanged";
      } 
      return "onSessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          boolean bool1 = false, bool2 = false;
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.content.pm.IPackageInstallerCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.content.pm.IPackageInstallerCallback");
              param1Int1 = param1Parcel1.readInt();
              if (param1Parcel1.readInt() != 0)
                bool2 = true; 
              onSessionFinished(param1Int1, bool2);
              return true;
            } 
            param1Parcel1.enforceInterface("android.content.pm.IPackageInstallerCallback");
            param1Int1 = param1Parcel1.readInt();
            float f = param1Parcel1.readFloat();
            onSessionProgressChanged(param1Int1, f);
            return true;
          } 
          param1Parcel1.enforceInterface("android.content.pm.IPackageInstallerCallback");
          param1Int1 = param1Parcel1.readInt();
          bool2 = bool1;
          if (param1Parcel1.readInt() != 0)
            bool2 = true; 
          onSessionActiveChanged(param1Int1, bool2);
          return true;
        } 
        param1Parcel1.enforceInterface("android.content.pm.IPackageInstallerCallback");
        param1Int1 = param1Parcel1.readInt();
        onSessionBadgingChanged(param1Int1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.content.pm.IPackageInstallerCallback");
      param1Int1 = param1Parcel1.readInt();
      onSessionCreated(param1Int1);
      return true;
    }
    
    private static class Proxy implements IPackageInstallerCallback {
      public static IPackageInstallerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IPackageInstallerCallback";
      }
      
      public void onSessionCreated(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageInstallerCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPackageInstallerCallback.Stub.getDefaultImpl() != null) {
            IPackageInstallerCallback.Stub.getDefaultImpl().onSessionCreated(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionBadgingChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageInstallerCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IPackageInstallerCallback.Stub.getDefaultImpl() != null) {
            IPackageInstallerCallback.Stub.getDefaultImpl().onSessionBadgingChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionActiveChanged(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.content.pm.IPackageInstallerCallback");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(3, parcel, null, 1);
          if (!bool1 && IPackageInstallerCallback.Stub.getDefaultImpl() != null) {
            IPackageInstallerCallback.Stub.getDefaultImpl().onSessionActiveChanged(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionProgressChanged(int param2Int, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageInstallerCallback");
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(4, parcel, null, 1);
          if (!bool && IPackageInstallerCallback.Stub.getDefaultImpl() != null) {
            IPackageInstallerCallback.Stub.getDefaultImpl().onSessionProgressChanged(param2Int, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionFinished(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.content.pm.IPackageInstallerCallback");
          parcel.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IPackageInstallerCallback.Stub.getDefaultImpl() != null) {
            IPackageInstallerCallback.Stub.getDefaultImpl().onSessionFinished(param2Int, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPackageInstallerCallback param1IPackageInstallerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPackageInstallerCallback != null) {
          Proxy.sDefaultImpl = param1IPackageInstallerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPackageInstallerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
