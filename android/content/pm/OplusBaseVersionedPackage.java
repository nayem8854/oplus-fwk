package android.content.pm;

public class OplusBaseVersionedPackage {
  private int mFlag = 0;
  
  private int mCallUid = -1;
  
  private int mCallPid = -1;
  
  public static final int DELETE_PACKAGE_MULTI_SYSTEM_FLAG = 1;
  
  public void setCallInfo(int paramInt1, int paramInt2) {
    this.mCallUid = paramInt1;
    this.mCallPid = paramInt2;
  }
  
  public void setDeleteFlag(int paramInt) {
    this.mFlag = paramInt;
  }
  
  public int getDeleteFlag() {
    return this.mFlag;
  }
  
  public int getCallUid() {
    return this.mCallUid;
  }
  
  public int getCallPid() {
    return this.mCallPid;
  }
}
