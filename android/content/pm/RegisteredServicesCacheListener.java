package android.content.pm;

public interface RegisteredServicesCacheListener<V> {
  void onServiceChanged(V paramV, int paramInt, boolean paramBoolean);
}
