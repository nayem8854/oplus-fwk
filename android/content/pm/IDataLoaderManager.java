package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IDataLoaderManager extends IInterface {
  boolean bindToDataLoader(int paramInt, DataLoaderParamsParcel paramDataLoaderParamsParcel, IDataLoaderStatusListener paramIDataLoaderStatusListener) throws RemoteException;
  
  IDataLoader getDataLoader(int paramInt) throws RemoteException;
  
  void unbindFromDataLoader(int paramInt) throws RemoteException;
  
  class Default implements IDataLoaderManager {
    public boolean bindToDataLoader(int param1Int, DataLoaderParamsParcel param1DataLoaderParamsParcel, IDataLoaderStatusListener param1IDataLoaderStatusListener) throws RemoteException {
      return false;
    }
    
    public IDataLoader getDataLoader(int param1Int) throws RemoteException {
      return null;
    }
    
    public void unbindFromDataLoader(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IDataLoaderManager {
    private static final String DESCRIPTOR = "android.content.pm.IDataLoaderManager";
    
    static final int TRANSACTION_bindToDataLoader = 1;
    
    static final int TRANSACTION_getDataLoader = 2;
    
    static final int TRANSACTION_unbindFromDataLoader = 3;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IDataLoaderManager");
    }
    
    public static IDataLoaderManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IDataLoaderManager");
      if (iInterface != null && iInterface instanceof IDataLoaderManager)
        return (IDataLoaderManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "unbindFromDataLoader";
        } 
        return "getDataLoader";
      } 
      return "bindToDataLoader";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IDataLoader iDataLoader;
      DataLoaderParamsParcel dataLoaderParamsParcel;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.content.pm.IDataLoaderManager");
            return true;
          } 
          param1Parcel1.enforceInterface("android.content.pm.IDataLoaderManager");
          param1Int1 = param1Parcel1.readInt();
          unbindFromDataLoader(param1Int1);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("android.content.pm.IDataLoaderManager");
        param1Int1 = param1Parcel1.readInt();
        iDataLoader = getDataLoader(param1Int1);
        param1Parcel2.writeNoException();
        if (iDataLoader != null) {
          IBinder iBinder = iDataLoader.asBinder();
        } else {
          iDataLoader = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iDataLoader);
        return true;
      } 
      iDataLoader.enforceInterface("android.content.pm.IDataLoaderManager");
      param1Int1 = iDataLoader.readInt();
      if (iDataLoader.readInt() != 0) {
        dataLoaderParamsParcel = (DataLoaderParamsParcel)DataLoaderParamsParcel.CREATOR.createFromParcel((Parcel)iDataLoader);
      } else {
        dataLoaderParamsParcel = null;
      } 
      IDataLoaderStatusListener iDataLoaderStatusListener = IDataLoaderStatusListener.Stub.asInterface(iDataLoader.readStrongBinder());
      boolean bool = bindToDataLoader(param1Int1, dataLoaderParamsParcel, iDataLoaderStatusListener);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IDataLoaderManager {
      public static IDataLoaderManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IDataLoaderManager";
      }
      
      public boolean bindToDataLoader(int param2Int, DataLoaderParamsParcel param2DataLoaderParamsParcel, IDataLoaderStatusListener param2IDataLoaderStatusListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IDataLoaderManager");
          parcel1.writeInt(param2Int);
          boolean bool1 = true;
          if (param2DataLoaderParamsParcel != null) {
            parcel1.writeInt(1);
            param2DataLoaderParamsParcel.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IDataLoaderStatusListener != null) {
            iBinder = param2IDataLoaderStatusListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IDataLoaderManager.Stub.getDefaultImpl() != null) {
            bool1 = IDataLoaderManager.Stub.getDefaultImpl().bindToDataLoader(param2Int, param2DataLoaderParamsParcel, param2IDataLoaderStatusListener);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IDataLoader getDataLoader(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IDataLoaderManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IDataLoaderManager.Stub.getDefaultImpl() != null)
            return IDataLoaderManager.Stub.getDefaultImpl().getDataLoader(param2Int); 
          parcel2.readException();
          return IDataLoader.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unbindFromDataLoader(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IDataLoaderManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IDataLoaderManager.Stub.getDefaultImpl() != null) {
            IDataLoaderManager.Stub.getDefaultImpl().unbindFromDataLoader(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IDataLoaderManager param1IDataLoaderManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IDataLoaderManager != null) {
          Proxy.sDefaultImpl = param1IDataLoaderManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IDataLoaderManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
