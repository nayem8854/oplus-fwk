package android.content.pm;

import android.os.Parcel;
import android.util.Printer;

public abstract class OplusBaseActivityInfo extends ComponentInfo {
  public int resizeModeOriginal = 2;
  
  public int oplusFlags;
  
  public boolean hasResizeModeInit = false;
  
  public static final int OPLUS_FLAG_GP_INTERCEPT_MARK = 1;
  
  public OplusBaseActivityInfo(OplusBaseActivityInfo paramOplusBaseActivityInfo) {
    super(paramOplusBaseActivityInfo);
    this.oplusFlags = paramOplusBaseActivityInfo.oplusFlags;
  }
  
  protected void dumpFront(Printer paramPrinter, String paramString) {
    super.dumpFront(paramPrinter, paramString);
    if (this.oplusFlags != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append("oplusFlags=0x");
      stringBuilder.append(Integer.toHexString(this.oplusFlags));
      paramPrinter.println(stringBuilder.toString());
    } 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.oplusFlags);
  }
  
  protected OplusBaseActivityInfo(Parcel paramParcel) {
    super(paramParcel);
    this.oplusFlags = paramParcel.readInt();
  }
  
  public boolean hasGPInterceptMark() {
    int i = this.oplusFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public OplusBaseActivityInfo() {}
}
