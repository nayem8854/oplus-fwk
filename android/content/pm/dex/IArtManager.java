package android.content.pm.dex;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IArtManager extends IInterface {
  boolean isRuntimeProfilingEnabled(int paramInt, String paramString) throws RemoteException;
  
  void snapshotRuntimeProfile(int paramInt, String paramString1, String paramString2, ISnapshotRuntimeProfileCallback paramISnapshotRuntimeProfileCallback, String paramString3) throws RemoteException;
  
  class Default implements IArtManager {
    public void snapshotRuntimeProfile(int param1Int, String param1String1, String param1String2, ISnapshotRuntimeProfileCallback param1ISnapshotRuntimeProfileCallback, String param1String3) throws RemoteException {}
    
    public boolean isRuntimeProfilingEnabled(int param1Int, String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IArtManager {
    private static final String DESCRIPTOR = "android.content.pm.dex.IArtManager";
    
    static final int TRANSACTION_isRuntimeProfilingEnabled = 2;
    
    static final int TRANSACTION_snapshotRuntimeProfile = 1;
    
    public Stub() {
      attachInterface(this, "android.content.pm.dex.IArtManager");
    }
    
    public static IArtManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.dex.IArtManager");
      if (iInterface != null && iInterface instanceof IArtManager)
        return (IArtManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "isRuntimeProfilingEnabled";
      } 
      return "snapshotRuntimeProfile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.content.pm.dex.IArtManager");
          return true;
        } 
        param1Parcel1.enforceInterface("android.content.pm.dex.IArtManager");
        param1Int1 = param1Parcel1.readInt();
        str1 = param1Parcel1.readString();
        boolean bool = isRuntimeProfilingEnabled(param1Int1, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool);
        return true;
      } 
      str1.enforceInterface("android.content.pm.dex.IArtManager");
      param1Int1 = str1.readInt();
      String str2 = str1.readString();
      String str3 = str1.readString();
      ISnapshotRuntimeProfileCallback iSnapshotRuntimeProfileCallback = ISnapshotRuntimeProfileCallback.Stub.asInterface(str1.readStrongBinder());
      String str1 = str1.readString();
      snapshotRuntimeProfile(param1Int1, str2, str3, iSnapshotRuntimeProfileCallback, str1);
      param1Parcel2.writeNoException();
      return true;
    }
    
    private static class Proxy implements IArtManager {
      public static IArtManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.dex.IArtManager";
      }
      
      public void snapshotRuntimeProfile(int param2Int, String param2String1, String param2String2, ISnapshotRuntimeProfileCallback param2ISnapshotRuntimeProfileCallback, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.dex.IArtManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2ISnapshotRuntimeProfileCallback != null) {
            iBinder = param2ISnapshotRuntimeProfileCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IArtManager.Stub.getDefaultImpl() != null) {
            IArtManager.Stub.getDefaultImpl().snapshotRuntimeProfile(param2Int, param2String1, param2String2, param2ISnapshotRuntimeProfileCallback, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRuntimeProfilingEnabled(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.dex.IArtManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IArtManager.Stub.getDefaultImpl() != null) {
            bool1 = IArtManager.Stub.getDefaultImpl().isRuntimeProfilingEnabled(param2Int, param2String);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IArtManager param1IArtManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IArtManager != null) {
          Proxy.sDefaultImpl = param1IArtManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IArtManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
