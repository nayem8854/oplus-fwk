package android.content.pm.dex;

import android.content.pm.IPackageManager;
import android.content.pm.PackageInfo;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class OplusArtManager {
  private static final String TAG = "OplusArtManager";
  
  public static boolean runSnapshotApplicationProfile(String paramString1, String paramString2, String paramString3) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("runSnapdshotApplicationProfile, callingPackage = ");
    stringBuilder.append(paramString2);
    stringBuilder.append("packageName = ");
    stringBuilder.append(paramString1);
    Log.d("OplusArtManager", stringBuilder.toString());
    boolean bool = "android".equals(paramString1);
    RemoteException remoteException2 = null;
    stringBuilder = null;
    IPackageManager iPackageManager = IPackageManager.Stub.asInterface(ServiceManager.getService("package"));
    if (!bool)
      try {
        StringBuilder stringBuilder1;
        PackageInfo packageInfo = iPackageManager.getPackageInfo(paramString1, 0, 0);
        if (packageInfo == null) {
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Snapshot Profile: Package not found ");
          stringBuilder1.append(paramString1);
          Log.d("OplusArtManager", stringBuilder1.toString());
          return false;
        } 
        String str = ((PackageInfo)stringBuilder1).applicationInfo.getBaseCodePath();
      } catch (RemoteException remoteException1) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("RemoteException : ");
        stringBuilder1.append(remoteException1.getMessage());
        Log.e("OplusArtManager", stringBuilder1.toString());
        remoteException1 = remoteException2;
      }  
    SnapshotRuntimeProfileCallback snapshotRuntimeProfileCallback = new SnapshotRuntimeProfileCallback();
    try {
      if (!iPackageManager.getArtManager().isRuntimeProfilingEnabled(bool, paramString2)) {
        Log.e("OplusArtManager", "Error: Runtime profiling is not enabled");
        return false;
      } 
      iPackageManager.getArtManager().snapshotRuntimeProfile(bool, paramString1, (String)remoteException1, snapshotRuntimeProfileCallback, paramString2);
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Snapshot Profile Exception : ");
      stringBuilder1.append(exception.getMessage());
      Log.e("OplusArtManager", stringBuilder1.toString());
    } 
    if (!snapshotRuntimeProfileCallback.waitTillDone()) {
      Log.e("OplusArtManager", "Error: Snapshot profile callback not called");
      return false;
    } 
    try {
      ParcelFileDescriptor.AutoCloseInputStream autoCloseInputStream = new ParcelFileDescriptor.AutoCloseInputStream();
      this(snapshotRuntimeProfileCallback.mProfileReadFd);
      try {
        FileOutputStream fileOutputStream = new FileOutputStream();
        this(paramString3);
      } finally {
        try {
          autoCloseInputStream.close();
        } finally {
          autoCloseInputStream = null;
        } 
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Error when reading the profile fd: ");
      stringBuilder1.append(exception.getMessage());
      Log.e("OplusArtManager", stringBuilder1.toString());
      return false;
    } 
  }
  
  class SnapshotRuntimeProfileCallback extends ISnapshotRuntimeProfileCallback.Stub {
    private boolean mSuccess = false;
    
    private int mErrCode = -1;
    
    private ParcelFileDescriptor mProfileReadFd = null;
    
    private CountDownLatch mDoneSignal = new CountDownLatch(1);
    
    public void onSuccess(ParcelFileDescriptor param1ParcelFileDescriptor) {
      Log.i("OplusArtManager", "SnapshotRuntimeProfileCallback onSuccess");
      this.mSuccess = true;
      try {
        this.mProfileReadFd = param1ParcelFileDescriptor.dup();
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("SnapshotRuntimeProfileCallback onSuccess IOException : ");
        stringBuilder.append(iOException.getMessage());
        Log.e("OplusArtManager", stringBuilder.toString());
      } 
      this.mDoneSignal.countDown();
    }
    
    public void onError(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("SnapshotRuntimeProfileCallback onError, errorCode = ");
      stringBuilder.append(param1Int);
      Log.i("OplusArtManager", stringBuilder.toString());
      this.mSuccess = false;
      this.mErrCode = param1Int;
      this.mDoneSignal.countDown();
    }
    
    boolean waitTillDone() {
      boolean bool = false;
      try {
        boolean bool1 = this.mDoneSignal.await(10000000L, TimeUnit.MILLISECONDS);
      } catch (Exception exception) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("waitTillDone Exception = ");
        stringBuilder.append(exception.getMessage());
        Log.e("OplusArtManager", stringBuilder.toString());
      } 
      if (bool && this.mSuccess) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private SnapshotRuntimeProfileCallback() {}
  }
}
