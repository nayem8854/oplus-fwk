package android.content.pm.dex;

public class PackageOptimizationInfo {
  private final int mCompilationFilter;
  
  private final int mCompilationReason;
  
  public PackageOptimizationInfo(int paramInt1, int paramInt2) {
    this.mCompilationReason = paramInt2;
    this.mCompilationFilter = paramInt1;
  }
  
  public int getCompilationReason() {
    return this.mCompilationReason;
  }
  
  public int getCompilationFilter() {
    return this.mCompilationFilter;
  }
  
  public static PackageOptimizationInfo createWithNoInfo() {
    return new PackageOptimizationInfo(-1, -1);
  }
}
