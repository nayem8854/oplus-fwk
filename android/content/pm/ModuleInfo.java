package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

public final class ModuleInfo implements Parcelable {
  public ModuleInfo() {}
  
  public ModuleInfo(ModuleInfo paramModuleInfo) {
    this.mName = paramModuleInfo.mName;
    this.mPackageName = paramModuleInfo.mPackageName;
    this.mHidden = paramModuleInfo.mHidden;
    this.mApexModuleName = paramModuleInfo.mApexModuleName;
  }
  
  public ModuleInfo setName(CharSequence paramCharSequence) {
    this.mName = paramCharSequence;
    return this;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public ModuleInfo setPackageName(String paramString) {
    this.mPackageName = paramString;
    return this;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public ModuleInfo setHidden(boolean paramBoolean) {
    this.mHidden = paramBoolean;
    return this;
  }
  
  public boolean isHidden() {
    return this.mHidden;
  }
  
  public ModuleInfo setApexModuleName(String paramString) {
    this.mApexModuleName = paramString;
    return this;
  }
  
  public String getApexModuleName() {
    return this.mApexModuleName;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("ModuleInfo{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" ");
    stringBuilder.append(this.mName);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mName);
    int j = Objects.hashCode(this.mPackageName);
    int k = Objects.hashCode(this.mApexModuleName);
    int m = Boolean.hashCode(this.mHidden);
    return (((0 * 31 + i) * 31 + j) * 31 + k) * 31 + m;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof ModuleInfo;
    boolean bool1 = false;
    if (!bool)
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mName, ((ModuleInfo)paramObject).mName)) {
      String str1 = this.mPackageName, str2 = ((ModuleInfo)paramObject).mPackageName;
      if (Objects.equals(str1, str2)) {
        str1 = this.mApexModuleName;
        str2 = ((ModuleInfo)paramObject).mApexModuleName;
        if (Objects.equals(str1, str2) && this.mHidden == ((ModuleInfo)paramObject).mHidden)
          bool1 = true; 
      } 
    } 
    return bool1;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mName);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeBoolean(this.mHidden);
    paramParcel.writeString(this.mApexModuleName);
  }
  
  private ModuleInfo(Parcel paramParcel) {
    this.mName = paramParcel.readCharSequence();
    this.mPackageName = paramParcel.readString();
    this.mHidden = paramParcel.readBoolean();
    this.mApexModuleName = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<ModuleInfo> CREATOR = new Parcelable.Creator<ModuleInfo>() {
      public ModuleInfo createFromParcel(Parcel param1Parcel) {
        return new ModuleInfo(param1Parcel);
      }
      
      public ModuleInfo[] newArray(int param1Int) {
        return new ModuleInfo[param1Int];
      }
    };
  
  private String mApexModuleName;
  
  private boolean mHidden;
  
  private CharSequence mName;
  
  private String mPackageName;
}
