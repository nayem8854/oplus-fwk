package android.content.pm;

import android.app.IOplusCommonInjector;
import android.common.OplusFeatureCache;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;

public abstract class OplusBasePackageParser {
  private static final String PACKAGE_OPPO = "oplus";
  
  void hookDispCompat(PackageParser.Package paramPackage, float paramFloat) {
    paramPackage.applicationInfo.maxAspectRatio = paramFloat;
  }
  
  void hookActivityAliasTheme(PackageParser.Activity paramActivity1, Resources paramResources, XmlResourceParser paramXmlResourceParser, PackageParser.Activity paramActivity2) {
    ((IOplusCommonInjector)OplusFeatureCache.<IOplusCommonInjector>getOrCreate(IOplusCommonInjector.DEFAULT, new Object[0])).hookActivityAliasTheme(paramActivity1, paramResources, paramXmlResourceParser, paramActivity2);
  }
  
  static String hookNameError(String paramString1, String paramString2) {
    if ("oplus".equals(paramString1))
      return null; 
    return paramString2;
  }
}
