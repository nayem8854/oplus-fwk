package android.content.pm;

import android.content.ComponentName;
import android.content.pm.parsing.component.ParsedMainComponent;
import android.os.BaseBundle;
import android.os.PersistableBundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.Pair;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class PackageUserState extends OplusBasePackageUserState {
  private static final boolean DEBUG = false;
  
  private static final String LOG_TAG = "PackageUserState";
  
  public int appLinkGeneration;
  
  private String[] cachedOverlayPaths;
  
  public int categoryHint = -1;
  
  public long ceDataInode;
  
  private ArrayMap<ComponentName, Pair<String, Integer>> componentLabelIconOverrideMap;
  
  public ArraySet<String> disabledComponents;
  
  public int distractionFlags;
  
  public int domainVerificationStatus;
  
  public int enabled;
  
  public ArraySet<String> enabledComponents;
  
  public String harmfulAppWarning;
  
  public boolean hidden;
  
  public int installReason;
  
  public boolean installed;
  
  public boolean instantApp;
  
  public String lastDisableAppCaller;
  
  public boolean notLaunched;
  
  private String[] overlayPaths;
  
  private ArrayMap<String, String[]> sharedLibraryOverlayPaths;
  
  public boolean stopped;
  
  public ArrayMap<String, SuspendParams> suspendParams;
  
  public boolean suspended;
  
  public int uninstallReason;
  
  public boolean virtualPreload;
  
  public PackageUserState() {
    this.installed = true;
    this.hidden = false;
    this.suspended = false;
    this.enabled = 0;
    this.domainVerificationStatus = 0;
    this.installReason = 0;
    this.uninstallReason = 0;
  }
  
  public PackageUserState(PackageUserState paramPackageUserState) {
    this.ceDataInode = paramPackageUserState.ceDataInode;
    this.installed = paramPackageUserState.installed;
    this.stopped = paramPackageUserState.stopped;
    this.notLaunched = paramPackageUserState.notLaunched;
    this.hidden = paramPackageUserState.hidden;
    this.distractionFlags = paramPackageUserState.distractionFlags;
    this.suspended = paramPackageUserState.suspended;
    this.suspendParams = new ArrayMap(paramPackageUserState.suspendParams);
    this.instantApp = paramPackageUserState.instantApp;
    this.virtualPreload = paramPackageUserState.virtualPreload;
    this.enabled = paramPackageUserState.enabled;
    this.lastDisableAppCaller = paramPackageUserState.lastDisableAppCaller;
    this.domainVerificationStatus = paramPackageUserState.domainVerificationStatus;
    this.appLinkGeneration = paramPackageUserState.appLinkGeneration;
    this.categoryHint = paramPackageUserState.categoryHint;
    this.installReason = paramPackageUserState.installReason;
    this.uninstallReason = paramPackageUserState.uninstallReason;
    this.disabledComponents = ArrayUtils.cloneOrNull(paramPackageUserState.disabledComponents);
    this.enabledComponents = ArrayUtils.cloneOrNull(paramPackageUserState.enabledComponents);
    String[] arrayOfString = paramPackageUserState.overlayPaths;
    if (arrayOfString == null) {
      arrayOfString = null;
    } else {
      arrayOfString = Arrays.<String>copyOf(arrayOfString, arrayOfString.length);
    } 
    this.overlayPaths = arrayOfString;
    if (paramPackageUserState.sharedLibraryOverlayPaths != null)
      this.sharedLibraryOverlayPaths = new ArrayMap(paramPackageUserState.sharedLibraryOverlayPaths); 
    this.harmfulAppWarning = paramPackageUserState.harmfulAppWarning;
    if (paramPackageUserState.componentLabelIconOverrideMap != null)
      this.componentLabelIconOverrideMap = new ArrayMap(paramPackageUserState.componentLabelIconOverrideMap); 
  }
  
  public String[] getOverlayPaths() {
    return this.overlayPaths;
  }
  
  public void setOverlayPaths(String[] paramArrayOfString) {
    this.overlayPaths = paramArrayOfString;
    this.cachedOverlayPaths = null;
  }
  
  public Map<String, String[]> getSharedLibraryOverlayPaths() {
    return (Map<String, String[]>)this.sharedLibraryOverlayPaths;
  }
  
  public void setSharedLibraryOverlayPaths(String paramString, String[] paramArrayOfString) {
    if (this.sharedLibraryOverlayPaths == null)
      this.sharedLibraryOverlayPaths = new ArrayMap(); 
    this.sharedLibraryOverlayPaths.put(paramString, paramArrayOfString);
    this.cachedOverlayPaths = null;
  }
  
  public boolean overrideLabelAndIcon(ComponentName paramComponentName, String paramString, Integer paramInteger) {
    boolean bool;
    String str1 = null;
    Integer integer1 = null;
    ArrayMap<ComponentName, Pair<String, Integer>> arrayMap = this.componentLabelIconOverrideMap;
    String str2 = str1;
    Integer integer2 = integer1;
    if (arrayMap != null) {
      Pair pair = (Pair)arrayMap.get(paramComponentName);
      str2 = str1;
      integer2 = integer1;
      if (pair != null) {
        str2 = (String)pair.first;
        integer2 = (Integer)pair.second;
      } 
    } 
    if (!TextUtils.equals(str2, paramString) || 
      !Objects.equals(integer2, paramInteger)) {
      bool = true;
    } else {
      bool = false;
    } 
    if (bool)
      if (paramString == null && paramInteger == null) {
        this.componentLabelIconOverrideMap.remove(paramComponentName);
        if (this.componentLabelIconOverrideMap.isEmpty())
          this.componentLabelIconOverrideMap = null; 
      } else {
        if (this.componentLabelIconOverrideMap == null)
          this.componentLabelIconOverrideMap = new ArrayMap(1); 
        this.componentLabelIconOverrideMap.put(paramComponentName, Pair.create(paramString, paramInteger));
      }  
    return bool;
  }
  
  public void resetOverrideComponentLabelIcon() {
    this.componentLabelIconOverrideMap = null;
  }
  
  public Pair<String, Integer> getOverrideLabelIconForComponent(ComponentName paramComponentName) {
    if (ArrayUtils.isEmpty((Map)this.componentLabelIconOverrideMap))
      return null; 
    return (Pair<String, Integer>)this.componentLabelIconOverrideMap.get(paramComponentName);
  }
  
  public boolean isAvailable(int paramInt) {
    boolean bool2, bool1 = true;
    if ((0x400000 & paramInt) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt & 0x2000) != 0) {
      paramInt = 1;
    } else {
      paramInt = 0;
    } 
    boolean bool3 = bool1;
    if (!bool2) {
      if (this.installed) {
        bool3 = bool1;
        if (this.hidden) {
          if (paramInt != 0)
            return bool1; 
        } else {
          return bool3;
        } 
      } 
      bool3 = false;
    } 
    return bool3;
  }
  
  public boolean isMatch(ComponentInfo paramComponentInfo, int paramInt) {
    return isMatch(paramComponentInfo.applicationInfo.isSystemApp(), paramComponentInfo.applicationInfo.enabled, paramComponentInfo.enabled, paramComponentInfo.directBootAware, paramComponentInfo.name, paramInt);
  }
  
  public boolean isMatch(boolean paramBoolean1, boolean paramBoolean2, ParsedMainComponent paramParsedMainComponent, int paramInt) {
    boolean bool1 = paramParsedMainComponent.isEnabled();
    boolean bool2 = paramParsedMainComponent.isDirectBootAware();
    String str = paramParsedMainComponent.getName();
    return isMatch(paramBoolean1, paramBoolean2, bool1, bool2, str, paramInt);
  }
  
  public boolean isMatch(boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, String paramString, int paramInt) {
    boolean bool2, bool3, bool1 = true;
    if ((0x402000 & paramInt) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (!isAvailable(paramInt) && (!paramBoolean1 || !bool2))
      return reportIfDebug(false, paramInt); 
    if (!isEnabled(paramBoolean2, paramBoolean3, paramString, paramInt))
      return reportIfDebug(false, paramInt); 
    if ((0x100000 & paramInt) != 0 && 
      !paramBoolean1)
      return reportIfDebug(false, paramInt); 
    if ((0x40000 & paramInt) != 0 && !paramBoolean4) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((0x80000 & paramInt) != 0 && paramBoolean4) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    paramBoolean1 = bool1;
    if (!bool2)
      if (bool3) {
        paramBoolean1 = bool1;
      } else {
        paramBoolean1 = false;
      }  
    return reportIfDebug(paramBoolean1, paramInt);
  }
  
  public boolean reportIfDebug(boolean paramBoolean, int paramInt) {
    return paramBoolean;
  }
  
  public boolean isEnabled(ComponentInfo paramComponentInfo, int paramInt) {
    return isEnabled(paramComponentInfo.applicationInfo.enabled, paramComponentInfo.enabled, paramComponentInfo.name, paramInt);
  }
  
  public boolean isEnabled(boolean paramBoolean, ParsedMainComponent paramParsedMainComponent, int paramInt) {
    return isEnabled(paramBoolean, paramParsedMainComponent.isEnabled(), paramParsedMainComponent.getName(), paramInt);
  }
  
  public boolean isEnabled(boolean paramBoolean1, boolean paramBoolean2, String paramString, int paramInt) {
    if ((paramInt & 0x200) != 0)
      return true; 
    int i = this.enabled;
    if (i != 0)
      if (i != 2 && i != 3) {
        if (i != 4)
          if (ArrayUtils.contains((Collection)this.enabledComponents, paramString))
            return true;  
        if ((0x8000 & paramInt) == 0)
          return false; 
      } else {
        boolean bool = false;
        i = bool;
        if (this.enabled == 2) {
          i = bool;
          if (this.oplusFreezeState == 2) {
            i = bool;
            if ((0x40000000 & paramInt) != 0)
              i = 1; 
          } 
        } 
        if (i == 0)
          return false; 
        if (ArrayUtils.contains((Collection)this.enabledComponents, paramString))
          return true; 
      }  
    if (!paramBoolean1)
      return false; 
    if (ArrayUtils.contains((Collection)this.enabledComponents, paramString))
      return true; 
  }
  
  public String[] getAllOverlayPaths() {
    if (this.overlayPaths == null && this.sharedLibraryOverlayPaths == null)
      return null; 
    String[] arrayOfString2 = this.cachedOverlayPaths;
    if (arrayOfString2 != null)
      return arrayOfString2; 
    LinkedHashSet<String> linkedHashSet = new LinkedHashSet();
    String[] arrayOfString3 = this.overlayPaths;
    if (arrayOfString3 != null) {
      int i = arrayOfString3.length;
      for (byte b = 0; b < i; b++)
        linkedHashSet.add(this.overlayPaths[b]); 
    } 
    ArrayMap<String, String[]> arrayMap = this.sharedLibraryOverlayPaths;
    if (arrayMap != null)
      for (String[] arrayOfString : arrayMap.values()) {
        if (arrayOfString != null) {
          int i = arrayOfString.length;
          for (byte b = 0; b < i; b++)
            linkedHashSet.add(arrayOfString[b]); 
        } 
      }  
    String[] arrayOfString1 = (String[])linkedHashSet.toArray((Object[])new String[0]);
    return arrayOfString1;
  }
  
  public final boolean equals(Object paramObject) {
    if (!(paramObject instanceof PackageUserState))
      return false; 
    paramObject = paramObject;
    if (this.ceDataInode != ((PackageUserState)paramObject).ceDataInode)
      return false; 
    if (this.installed != ((PackageUserState)paramObject).installed)
      return false; 
    if (this.stopped != ((PackageUserState)paramObject).stopped)
      return false; 
    if (this.notLaunched != ((PackageUserState)paramObject).notLaunched)
      return false; 
    if (this.hidden != ((PackageUserState)paramObject).hidden)
      return false; 
    if (this.distractionFlags != ((PackageUserState)paramObject).distractionFlags)
      return false; 
    boolean bool = this.suspended;
    if (bool != ((PackageUserState)paramObject).suspended)
      return false; 
    if (bool && 
      !Objects.equals(this.suspendParams, ((PackageUserState)paramObject).suspendParams))
      return false; 
    if (this.instantApp != ((PackageUserState)paramObject).instantApp)
      return false; 
    if (this.virtualPreload != ((PackageUserState)paramObject).virtualPreload)
      return false; 
    if (this.enabled != ((PackageUserState)paramObject).enabled)
      return false; 
    if (this.lastDisableAppCaller != null || ((PackageUserState)paramObject).lastDisableAppCaller == null) {
      String str = this.lastDisableAppCaller;
      if (str != null) {
        String str1 = ((PackageUserState)paramObject).lastDisableAppCaller;
        if (!str.equals(str1))
          return false; 
      } 
    } else {
      return false;
    } 
    if (this.domainVerificationStatus != ((PackageUserState)paramObject).domainVerificationStatus)
      return false; 
    if (this.appLinkGeneration != ((PackageUserState)paramObject).appLinkGeneration)
      return false; 
    if (this.categoryHint != ((PackageUserState)paramObject).categoryHint)
      return false; 
    if (this.installReason != ((PackageUserState)paramObject).installReason)
      return false; 
    if (this.uninstallReason != ((PackageUserState)paramObject).uninstallReason)
      return false; 
    if ((this.disabledComponents == null && ((PackageUserState)paramObject).disabledComponents != null) || (this.disabledComponents != null && ((PackageUserState)paramObject).disabledComponents == null))
      return false; 
    ArraySet<String> arraySet = this.disabledComponents;
    if (arraySet != null) {
      if (arraySet.size() != ((PackageUserState)paramObject).disabledComponents.size())
        return false; 
      for (int i = this.disabledComponents.size() - 1; i >= 0; i--) {
        if (!((PackageUserState)paramObject).disabledComponents.contains(this.disabledComponents.valueAt(i)))
          return false; 
      } 
    } 
    if ((this.enabledComponents == null && ((PackageUserState)paramObject).enabledComponents != null) || (this.enabledComponents != null && ((PackageUserState)paramObject).enabledComponents == null))
      return false; 
    arraySet = this.enabledComponents;
    if (arraySet != null) {
      if (arraySet.size() != ((PackageUserState)paramObject).enabledComponents.size())
        return false; 
      for (int i = this.enabledComponents.size() - 1; i >= 0; i--) {
        if (!((PackageUserState)paramObject).enabledComponents.contains(this.enabledComponents.valueAt(i)))
          return false; 
      } 
    } 
    if (this.harmfulAppWarning != null || ((PackageUserState)paramObject).harmfulAppWarning == null) {
      String str = this.harmfulAppWarning;
      if (str != null) {
        paramObject = ((PackageUserState)paramObject).harmfulAppWarning;
        if (!str.equals(paramObject))
          return false; 
      } 
    } else {
      return false;
    } 
    return true;
  }
  
  public int hashCode() {
    int i = Long.hashCode(this.ceDataInode);
    int j = Boolean.hashCode(this.installed);
    int k = Boolean.hashCode(this.stopped);
    int m = Boolean.hashCode(this.notLaunched);
    int n = Boolean.hashCode(this.hidden);
    int i1 = this.distractionFlags;
    int i2 = Boolean.hashCode(this.suspended);
    int i3 = Objects.hashCode(this.suspendParams);
    int i4 = Boolean.hashCode(this.instantApp);
    int i5 = Boolean.hashCode(this.virtualPreload);
    int i6 = this.enabled;
    int i7 = Objects.hashCode(this.lastDisableAppCaller);
    int i8 = this.domainVerificationStatus;
    int i9 = this.appLinkGeneration;
    int i10 = this.categoryHint;
    int i11 = this.installReason;
    int i12 = this.uninstallReason;
    int i13 = Objects.hashCode(this.disabledComponents);
    int i14 = Objects.hashCode(this.enabledComponents);
    int i15 = Objects.hashCode(this.harmfulAppWarning);
    return ((((((((((((((((((i * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31 + i11) * 31 + i12) * 31 + i13) * 31 + i14) * 31 + i15;
  }
  
  class SuspendParams {
    private static final String TAG_APP_EXTRAS = "app-extras";
    
    private static final String TAG_DIALOG_INFO = "dialog-info";
    
    private static final String TAG_LAUNCHER_EXTRAS = "launcher-extras";
    
    public PersistableBundle appExtras;
    
    public SuspendDialogInfo dialogInfo;
    
    public PersistableBundle launcherExtras;
    
    public static SuspendParams getInstanceOrNull(SuspendDialogInfo param1SuspendDialogInfo, PersistableBundle param1PersistableBundle1, PersistableBundle param1PersistableBundle2) {
      if (param1SuspendDialogInfo == null && param1PersistableBundle1 == null && param1PersistableBundle2 == null)
        return null; 
      SuspendParams suspendParams = new SuspendParams();
      suspendParams.dialogInfo = param1SuspendDialogInfo;
      suspendParams.appExtras = param1PersistableBundle1;
      suspendParams.launcherExtras = param1PersistableBundle2;
      return suspendParams;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof SuspendParams))
        return false; 
      param1Object = param1Object;
      if (!Objects.equals(this.dialogInfo, ((SuspendParams)param1Object).dialogInfo))
        return false; 
      if (!BaseBundle.kindofEquals((BaseBundle)this.appExtras, (BaseBundle)((SuspendParams)param1Object).appExtras))
        return false; 
      if (!BaseBundle.kindofEquals((BaseBundle)this.launcherExtras, (BaseBundle)((SuspendParams)param1Object).launcherExtras))
        return false; 
      return true;
    }
    
    public int hashCode() {
      byte b;
      int i = Objects.hashCode(this.dialogInfo);
      PersistableBundle persistableBundle = this.appExtras;
      int j = 0;
      if (persistableBundle != null) {
        b = persistableBundle.size();
      } else {
        b = 0;
      } 
      persistableBundle = this.launcherExtras;
      if (persistableBundle != null)
        j = persistableBundle.size(); 
      return (i * 31 + b) * 31 + j;
    }
    
    public void saveToXml(XmlSerializer param1XmlSerializer) throws IOException {
      if (this.dialogInfo != null) {
        param1XmlSerializer.startTag(null, "dialog-info");
        this.dialogInfo.saveToXml(param1XmlSerializer);
        param1XmlSerializer.endTag(null, "dialog-info");
      } 
      if (this.appExtras != null) {
        param1XmlSerializer.startTag(null, "app-extras");
        try {
          this.appExtras.saveToXml(param1XmlSerializer);
        } catch (XmlPullParserException xmlPullParserException) {
          Slog.e("PackageUserState", "Exception while trying to write appExtras. Will be lost on reboot", (Throwable)xmlPullParserException);
        } 
        param1XmlSerializer.endTag(null, "app-extras");
      } 
      if (this.launcherExtras != null) {
        param1XmlSerializer.startTag(null, "launcher-extras");
        try {
          this.launcherExtras.saveToXml(param1XmlSerializer);
        } catch (XmlPullParserException xmlPullParserException) {
          Slog.e("PackageUserState", "Exception while trying to write launcherExtras. Will be lost on reboot", (Throwable)xmlPullParserException);
        } 
        param1XmlSerializer.endTag(null, "launcher-extras");
      } 
    }
    
    public static SuspendParams restoreFromXml(XmlPullParser param1XmlPullParser) throws IOException {
      // Byte code:
      //   0: aconst_null
      //   1: astore_1
      //   2: aconst_null
      //   3: astore_2
      //   4: aconst_null
      //   5: astore_3
      //   6: aload_0
      //   7: invokeinterface getDepth : ()I
      //   12: istore #4
      //   14: aload_0
      //   15: invokeinterface next : ()I
      //   20: istore #5
      //   22: iload #5
      //   24: iconst_1
      //   25: if_icmpeq -> 252
      //   28: iload #5
      //   30: iconst_3
      //   31: if_icmpne -> 45
      //   34: aload_0
      //   35: invokeinterface getDepth : ()I
      //   40: iload #4
      //   42: if_icmple -> 252
      //   45: iload #5
      //   47: iconst_3
      //   48: if_icmpeq -> 14
      //   51: iload #5
      //   53: iconst_4
      //   54: if_icmpne -> 60
      //   57: goto -> 14
      //   60: aload_0
      //   61: invokeinterface getName : ()Ljava/lang/String;
      //   66: astore #6
      //   68: iconst_m1
      //   69: istore #5
      //   71: aload #6
      //   73: invokevirtual hashCode : ()I
      //   76: istore #7
      //   78: iload #7
      //   80: ldc -538220657
      //   82: if_icmpeq -> 134
      //   85: iload #7
      //   87: ldc -22768109
      //   89: if_icmpeq -> 118
      //   92: iload #7
      //   94: ldc 1627485488
      //   96: if_icmpeq -> 102
      //   99: goto -> 147
      //   102: aload #6
      //   104: ldc 'launcher-extras'
      //   106: invokevirtual equals : (Ljava/lang/Object;)Z
      //   109: ifeq -> 99
      //   112: iconst_2
      //   113: istore #5
      //   115: goto -> 147
      //   118: aload #6
      //   120: ldc 'dialog-info'
      //   122: invokevirtual equals : (Ljava/lang/Object;)Z
      //   125: ifeq -> 99
      //   128: iconst_0
      //   129: istore #5
      //   131: goto -> 147
      //   134: aload #6
      //   136: ldc 'app-extras'
      //   138: invokevirtual equals : (Ljava/lang/Object;)Z
      //   141: ifeq -> 99
      //   144: iconst_1
      //   145: istore #5
      //   147: iload #5
      //   149: ifeq -> 240
      //   152: iload #5
      //   154: iconst_1
      //   155: if_icmpeq -> 228
      //   158: iload #5
      //   160: iconst_2
      //   161: if_icmpeq -> 216
      //   164: new java/lang/StringBuilder
      //   167: astore #6
      //   169: aload #6
      //   171: invokespecial <init> : ()V
      //   174: aload #6
      //   176: ldc 'Unknown tag '
      //   178: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   181: pop
      //   182: aload #6
      //   184: aload_0
      //   185: invokeinterface getName : ()Ljava/lang/String;
      //   190: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   193: pop
      //   194: aload #6
      //   196: ldc ' in SuspendParams. Ignoring'
      //   198: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   201: pop
      //   202: ldc 'PackageUserState'
      //   204: aload #6
      //   206: invokevirtual toString : ()Ljava/lang/String;
      //   209: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   212: pop
      //   213: goto -> 249
      //   216: aload_0
      //   217: invokestatic restoreFromXml : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/os/PersistableBundle;
      //   220: astore #6
      //   222: aload #6
      //   224: astore_3
      //   225: goto -> 249
      //   228: aload_0
      //   229: invokestatic restoreFromXml : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/os/PersistableBundle;
      //   232: astore #6
      //   234: aload #6
      //   236: astore_2
      //   237: goto -> 249
      //   240: aload_0
      //   241: invokestatic restoreFromXml : (Lorg/xmlpull/v1/XmlPullParser;)Landroid/content/pm/SuspendDialogInfo;
      //   244: astore #6
      //   246: aload #6
      //   248: astore_1
      //   249: goto -> 14
      //   252: goto -> 265
      //   255: astore_0
      //   256: ldc 'PackageUserState'
      //   258: ldc 'Exception while trying to parse SuspendParams, some fields may default'
      //   260: aload_0
      //   261: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   264: pop
      //   265: aload_1
      //   266: aload_2
      //   267: aload_3
      //   268: invokestatic getInstanceOrNull : (Landroid/content/pm/SuspendDialogInfo;Landroid/os/PersistableBundle;Landroid/os/PersistableBundle;)Landroid/content/pm/PackageUserState$SuspendParams;
      //   271: areturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #617	-> 0
      //   #618	-> 2
      //   #619	-> 4
      //   #621	-> 6
      //   #624	-> 14
      //   #626	-> 34
      //   #627	-> 45
      //   #629	-> 57
      //   #631	-> 60
      //   #642	-> 164
      //   #639	-> 216
      //   #640	-> 222
      //   #636	-> 228
      //   #637	-> 234
      //   #633	-> 240
      //   #634	-> 249
      //   #644	-> 249
      //   #650	-> 252
      //   #647	-> 255
      //   #648	-> 256
      //   #651	-> 265
      // Exception table:
      //   from	to	target	type
      //   14	22	255	org/xmlpull/v1/XmlPullParserException
      //   34	45	255	org/xmlpull/v1/XmlPullParserException
      //   60	68	255	org/xmlpull/v1/XmlPullParserException
      //   71	78	255	org/xmlpull/v1/XmlPullParserException
      //   102	112	255	org/xmlpull/v1/XmlPullParserException
      //   118	128	255	org/xmlpull/v1/XmlPullParserException
      //   134	144	255	org/xmlpull/v1/XmlPullParserException
      //   164	213	255	org/xmlpull/v1/XmlPullParserException
      //   216	222	255	org/xmlpull/v1/XmlPullParserException
      //   228	234	255	org/xmlpull/v1/XmlPullParserException
      //   240	246	255	org/xmlpull/v1/XmlPullParserException
    }
  }
}
