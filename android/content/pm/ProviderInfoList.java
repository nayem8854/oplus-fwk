package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;
import java.util.List;

public final class ProviderInfoList implements Parcelable {
  private ProviderInfoList(Parcel paramParcel) {
    ArrayList<ProviderInfo> arrayList = new ArrayList();
    paramParcel.readTypedList(arrayList, ProviderInfo.CREATOR);
    this.mList = arrayList;
  }
  
  private ProviderInfoList(List<ProviderInfo> paramList) {
    this.mList = paramList;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    boolean bool = paramParcel.allowSquashing();
    paramParcel.writeTypedList(this.mList, paramInt);
    paramParcel.restoreAllowSquashing(bool);
  }
  
  public static final Parcelable.Creator<ProviderInfoList> CREATOR = new Parcelable.Creator<ProviderInfoList>() {
      public ProviderInfoList createFromParcel(Parcel param1Parcel) {
        return new ProviderInfoList(param1Parcel);
      }
      
      public ProviderInfoList[] newArray(int param1Int) {
        return new ProviderInfoList[param1Int];
      }
    };
  
  private final List<ProviderInfo> mList;
  
  public List<ProviderInfo> getList() {
    return this.mList;
  }
  
  public static ProviderInfoList fromList(List<ProviderInfo> paramList) {
    return new ProviderInfoList(paramList);
  }
}
