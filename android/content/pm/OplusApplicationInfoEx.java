package android.content.pm;

import android.os.Parcel;
import android.util.Log;
import android.util.proto.ProtoOutputStream;

public class OplusApplicationInfoEx {
  private float mAppscale = 1.0F;
  
  private float mNewappscale = 1.0F;
  
  private float mAappInvscale = 1.0F;
  
  private int mOverrideDensity = 0;
  
  private int mCompatDensity = 0;
  
  public void setAppScale(float paramFloat) {
    this.mAppscale = paramFloat;
  }
  
  public float getAppScale() {
    return this.mAppscale;
  }
  
  public void setNewAppScale(float paramFloat) {
    this.mNewappscale = paramFloat;
  }
  
  public float getNewAppScale() {
    return this.mNewappscale;
  }
  
  public void setAppInvScale(float paramFloat) {
    this.mAappInvscale = paramFloat;
  }
  
  public float getAppInvScale() {
    return this.mAappInvscale;
  }
  
  public void addPrivateFlags(int paramInt) {
    setPrivateFlags(paramInt, paramInt);
  }
  
  public void clearPrivateFlags(int paramInt) {
    setPrivateFlags(0, paramInt);
  }
  
  public void setPrivateFlags(int paramInt1, int paramInt2) {
    this.oplusPrivateFlags = this.oplusPrivateFlags & (paramInt2 ^ 0xFFFFFFFF) | paramInt1 & paramInt2;
  }
  
  public boolean hasPrivateFlags(int paramInt) {
    boolean bool;
    if ((this.oplusPrivateFlags & paramInt) == paramInt) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  private OverrideDensityChangedListener mChangingListener = null;
  
  public static final int OPLUS_PRIVATE_FLAG_DEX2OAT_ROLLBACK = 1;
  
  public static final int OPLUS_PRIVATE_FLAG_IGNORE_OPENNDK = 4;
  
  public static final int OPLUS_PRIVATE_FLAG_IGNORE_TOAST = 2;
  
  public static final int OPPO_PRIVATE_FLAG_CHECK_DISPLAY_COMPAT = 16;
  
  public static final int OPPO_PRIVATE_FLAG_ENABLE_DISPLAY_COMPAT = 8;
  
  public int oplusFreezeState;
  
  public int oplusPrivateFlags;
  
  public String[] specialNativeLibraryDirs;
  
  public void setOverrideDensityChangedListener(OverrideDensityChangedListener paramOverrideDensityChangedListener) {
    this.mChangingListener = paramOverrideDensityChangedListener;
  }
  
  public int getOverrideDensity() {
    return this.mOverrideDensity;
  }
  
  public void setOverrideDensity(int paramInt) {
    this.mOverrideDensity = paramInt;
    OverrideDensityChangedListener overrideDensityChangedListener = this.mChangingListener;
    if (overrideDensityChangedListener != null)
      overrideDensityChangedListener.onOverrideDensityChanged(paramInt); 
  }
  
  public int getCompatDensity() {
    return this.mCompatDensity;
  }
  
  public void setCompatDensity(int paramInt) {
    this.mCompatDensity = paramInt;
  }
  
  public OplusApplicationInfoEx(OplusApplicationInfoEx paramOplusApplicationInfoEx) {
    this.specialNativeLibraryDirs = paramOplusApplicationInfoEx.specialNativeLibraryDirs;
    this.mAppscale = paramOplusApplicationInfoEx.mAppscale;
    this.mNewappscale = paramOplusApplicationInfoEx.mNewappscale;
    this.oplusFreezeState = paramOplusApplicationInfoEx.oplusFreezeState;
    this.mOverrideDensity = paramOplusApplicationInfoEx.mOverrideDensity;
    this.mAappInvscale = paramOplusApplicationInfoEx.mAappInvscale;
    this.mCompatDensity = paramOplusApplicationInfoEx.mCompatDensity;
    this.oplusPrivateFlags = paramOplusApplicationInfoEx.oplusPrivateFlags;
  }
  
  protected OplusApplicationInfoEx(Parcel paramParcel) {
    this.specialNativeLibraryDirs = paramParcel.readStringArray();
    this.mAppscale = paramParcel.readFloat();
    this.mNewappscale = paramParcel.readFloat();
    this.oplusFreezeState = paramParcel.readInt();
    this.mOverrideDensity = paramParcel.readInt();
    this.mAappInvscale = paramParcel.readFloat();
    this.mCompatDensity = paramParcel.readInt();
    this.oplusPrivateFlags = paramParcel.readInt();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringArray(this.specialNativeLibraryDirs);
    paramParcel.writeFloat(this.mAppscale);
    paramParcel.writeFloat(this.mNewappscale);
    paramParcel.writeInt(this.oplusFreezeState);
    paramParcel.writeInt(this.mOverrideDensity);
    paramParcel.writeFloat(this.mAappInvscale);
    paramParcel.writeInt(this.mCompatDensity);
    paramParcel.writeInt(this.oplusPrivateFlags);
  }
  
  public void writeToProto(ProtoOutputStream paramProtoOutputStream, long paramLong, int paramInt) {}
  
  public static OplusApplicationInfoEx getOplusAppInfoExFromAppInfoRef(ApplicationInfo paramApplicationInfo) {
    if (paramApplicationInfo == null)
      return null; 
    if (OplusMirrorApplicationInfo.mOplusApplicationInfoEx != null)
      try {
        return (OplusApplicationInfoEx)OplusMirrorApplicationInfo.mOplusApplicationInfoEx.get(paramApplicationInfo);
      } catch (Exception exception) {
        Log.e("OplusApplicationInfoEx", "getOplusAppInfoExFromAppInfoRef failed!", exception);
        return null;
      }  
    return null;
  }
  
  public OplusApplicationInfoEx() {}
  
  public static interface OverrideDensityChangedListener {
    void onOverrideDensityChanged(int param1Int);
  }
}
