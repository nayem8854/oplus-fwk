package android.content.pm;

import android.net.Uri;
import android.os.IBinder;
import android.os.ISecurityPermissionService;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.Log;
import java.util.Arrays;
import java.util.List;

public class OplusPermissionManager {
  private static final boolean DEBUG = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  public static final Uri PERMISSIONS_PROVIDER_URI = Uri.parse("content://com.coloros.provider.PermissionProvider/pp_permission");
  
  private ISecurityPermissionService mSecurityPermissionService = null;
  
  private static OplusPermissionManager mInstance = null;
  
  public static final int ACCEPT = 0;
  
  public static final String AUTHORITY = "com.coloros.provider.PermissionProvider";
  
  public static final int FIRST_MASK = 1;
  
  public static final int INVALID_RES = 3;
  
  public static final List<String> OPLUS_DEFINED_PERMISSIONS;
  
  public static final List<String> OPLUS_DETECT_FREQUENT_CHECK_PERMISSIONS;
  
  public static final List<String> OPLUS_PRIVACY_PROTECT_PERMISSIONS;
  
  public static final String PERMISSION_ACCESS_MEDIA_PROVIDER = "android.permission.ACCESS_MEDIA_PROVIDER";
  
  public static final String PERMISSION_CALL_FORWARDING = "oppo.permission.call.FORWARDING";
  
  public static final String PERMISSION_DELETE_CALENDAR = "android.permission.WRITE_CALENDAR_DELETE";
  
  public static final String PERMISSION_DELETE_CALL = "android.permission.WRITE_CALL_LOG_DELETE";
  
  public static final String PERMISSION_DELETE_CONTACTS = "android.permission.WRITE_CONTACTS_DELETE";
  
  public static final String PERMISSION_DELETE_MMS = "android.permission.WRITE_MMS_DELETE";
  
  public static final String PERMISSION_DELETE_SMS = "android.permission.WRITE_SMS_DELETE";
  
  public static final String PERMISSION_WR_EXTERNAL_STORAGE = "android.permission.WR_EXTERNAL_STORAGE";
  
  public static final int PROMPT = 2;
  
  public static final String READ_MMS_PERMISSION = "android.permission.READ_MMS";
  
  public static final int REJECT = 1;
  
  public static final String SEND_MMS_PERMISSION = "android.permission.SEND_MMS";
  
  public static final String SERVICE_NAME = "security_permission";
  
  private static final String TAG = "OplusPermissionManager";
  
  public static final String WRITE_MMS_PERMISSION = "android.permission.WRITE_MMS";
  
  public static final List<String> WRITE_PERMISSIONS;
  
  public static List<String> sAlwaysInterceptingPermissions;
  
  public static List<String> sInterceptingPermissions;
  
  public static int[] sPermissionsDefaultChoices;
  
  static {
    WRITE_PERMISSIONS = Arrays.asList(new String[] { "android.permission.WRITE_CALL_LOG", "android.permission.WRITE_CONTACTS", "android.permission.WRITE_SMS", "android.permission.WRITE_MMS", "android.permission.WRITE_CALENDAR" });
    OPLUS_DEFINED_PERMISSIONS = Arrays.asList(new String[] { "android.permission.READ_MMS", "android.permission.WRITE_MMS", "android.permission.SEND_MMS", "android.permission.WR_EXTERNAL_STORAGE", "android.permission.ACCESS_MEDIA_PROVIDER" });
    OPLUS_PRIVACY_PROTECT_PERMISSIONS = Arrays.asList(new String[] { "android.permission.READ_SMS", "android.permission.WRITE_SMS", "android.permission.READ_CALL_LOG", "android.permission.WRITE_CALL_LOG", "android.permission.READ_CONTACTS", "android.permission.WRITE_CONTACTS", "android.permission.READ_CALENDAR", "android.permission.WRITE_CALENDAR" });
    OPLUS_DETECT_FREQUENT_CHECK_PERMISSIONS = Arrays.asList(new String[] { "android.permission.READ_PHONE_STATE", "android.permission.ACCESS_FINE_LOCATION", "android.permission.ACCESS_COARSE_LOCATION", "android.permission.READ_CONTACTS#com.callershow.colorcaller" });
    sInterceptingPermissions = Arrays.asList(new String[] { 
          "android.permission.CALL_PHONE", "android.permission.READ_CALL_LOG", "android.permission.READ_CONTACTS", "android.permission.READ_SMS", "android.permission.SEND_SMS", "android.permission.SEND_MMS", "android.permission.CHANGE_NETWORK_STATE", "android.permission.CHANGE_WIFI_STATE", "android.permission.BLUETOOTH_ADMIN", "android.permission.ACCESS_FINE_LOCATION", 
          "android.permission.CAMERA", "android.permission.RECORD_AUDIO", "android.permission.NFC", "android.permission.WRITE_CALL_LOG", "android.permission.WRITE_CONTACTS", "android.permission.WRITE_SMS", "android.permission.WRITE_MMS", "android.permission.READ_MMS", "com.android.browser.permission.READ_HISTORY_BOOKMARKS", "android.permission.READ_CALENDAR", 
          "android.permission.WRITE_CALENDAR", "android.permission.WRITE_CALL_LOG_DELETE", "android.permission.WRITE_CONTACTS_DELETE", "android.permission.WRITE_SMS_DELETE", "android.permission.WRITE_MMS_DELETE", "android.permission.WRITE_CALENDAR_DELETE", "android.permission.GET_ACCOUNTS", "android.permission.READ_PHONE_STATE", "com.android.voicemail.permission.ADD_VOICEMAIL", "android.permission.USE_SIP", 
          "android.permission.PROCESS_OUTGOING_CALLS", "android.permission.RECEIVE_SMS", "android.permission.RECEIVE_MMS", "android.permission.RECEIVE_WAP_PUSH", "android.permission.BODY_SENSORS", "android.permission.WR_EXTERNAL_STORAGE", "android.permission.ACCESS_MEDIA_PROVIDER", "android.permission.BIND_VPN_SERVICE", "oppo.permission.call.FORWARDING", "android.permission.ACTIVITY_RECOGNITION" });
    sPermissionsDefaultChoices = new int[] { 
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 
        2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 
        2, 2, 2, 2, 2, 2, 2, 2, 2, 2 };
    sAlwaysInterceptingPermissions = Arrays.asList(new String[] { "android.permission.SEND_SMS" });
  }
  
  private OplusPermissionManager() {
    IBinder iBinder = ServiceManager.getService("security_permission");
    this.mSecurityPermissionService = ISecurityPermissionService.Stub.asInterface(iBinder);
  }
  
  public static long getPermissionMask(String paramString) {
    long l = 0L;
    int i = sInterceptingPermissions.indexOf(paramString);
    if (i > -1)
      l = 1L << i; 
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPermissionMask, permission=");
      stringBuilder.append(paramString);
      stringBuilder.append(", mask=");
      stringBuilder.append(l);
      stringBuilder.append(", index=");
      stringBuilder.append(i);
      Log.d("OplusPermissionManager", stringBuilder.toString());
    } 
    return l;
  }
  
  public static OplusPermissionManager getInstance() {
    if (mInstance == null)
      mInstance = new OplusPermissionManager(); 
    return mInstance;
  }
  
  public int queryPermissionAsUser(String paramString1, String paramString2, int paramInt) {
    ISecurityPermissionService iSecurityPermissionService = this.mSecurityPermissionService;
    if (iSecurityPermissionService != null) {
      try {
        return iSecurityPermissionService.queryPermissionAsUser(paramString1, paramString2, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusPermissionManager", "queryPermission failed!", (Throwable)remoteException);
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } else {
      Log.w("OplusPermissionManager", "queryPermission:oppo_permission not publish!");
    } 
    return -1;
  }
  
  public PackagePermission queryPackagePermissionsAsUser(String paramString, int paramInt) {
    ISecurityPermissionService iSecurityPermissionService = this.mSecurityPermissionService;
    if (iSecurityPermissionService != null) {
      try {
        return iSecurityPermissionService.queryPackagePermissionsAsUser(paramString, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("OplusPermissionManager", "queryPackagePermissionsAsUser failed!", (Throwable)remoteException);
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } else {
      Log.w("OplusPermissionManager", "queryPermission:oppo_permission not publish!");
    } 
    return null;
  }
  
  public void updateCachedPermission(String paramString, int paramInt, boolean paramBoolean) {
    ISecurityPermissionService iSecurityPermissionService = this.mSecurityPermissionService;
    if (iSecurityPermissionService != null) {
      try {
        iSecurityPermissionService.updateCachedPermission(paramString, paramInt, paramBoolean);
      } catch (RemoteException remoteException) {
        Log.e("OplusPermissionManager", "updateCachedPermission failed!", (Throwable)remoteException);
      } catch (Exception exception) {
        exception.printStackTrace();
      } 
    } else {
      Log.w("OplusPermissionManager", "updateCachedPermission:oppo_permission not publish!");
    } 
  }
}
