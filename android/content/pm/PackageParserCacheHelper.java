package android.content.pm;

import android.os.Parcel;
import java.util.ArrayList;
import java.util.HashMap;

public class PackageParserCacheHelper {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "PackageParserCacheHelper";
  
  class ReadHelper extends Parcel.ReadWriteHelper {
    private final Parcel mParcel;
    
    private final ArrayList<String> mStrings = new ArrayList<>();
    
    public ReadHelper(PackageParserCacheHelper this$0) {
      this.mParcel = (Parcel)this$0;
    }
    
    public void startAndInstall() {
      this.mStrings.clear();
      int i = this.mParcel.readInt();
      int j = this.mParcel.dataPosition();
      this.mParcel.setDataPosition(i);
      this.mParcel.readStringList(this.mStrings);
      this.mParcel.setDataPosition(j);
      this.mParcel.setReadWriteHelper(this);
    }
    
    public String readString(Parcel param1Parcel) {
      return this.mStrings.get(param1Parcel.readInt());
    }
    
    public String readString8(Parcel param1Parcel) {
      return readString(param1Parcel);
    }
    
    public String readString16(Parcel param1Parcel) {
      return readString(param1Parcel);
    }
  }
  
  class WriteHelper extends Parcel.ReadWriteHelper {
    private final ArrayList<String> mStrings = new ArrayList<>();
    
    private final int mStartPos;
    
    private final Parcel mParcel;
    
    private final HashMap<String, Integer> mIndexes = new HashMap<>();
    
    public WriteHelper(PackageParserCacheHelper this$0) {
      this.mParcel = (Parcel)this$0;
      this.mStartPos = this$0.dataPosition();
      this.mParcel.writeInt(0);
      this.mParcel.setReadWriteHelper(this);
    }
    
    public void writeString(Parcel param1Parcel, String param1String) {
      Integer integer = this.mIndexes.get(param1String);
      if (integer != null) {
        param1Parcel.writeInt(integer.intValue());
      } else {
        int i = this.mStrings.size();
        this.mIndexes.put(param1String, Integer.valueOf(i));
        this.mStrings.add(param1String);
        param1Parcel.writeInt(i);
      } 
    }
    
    public void writeString8(Parcel param1Parcel, String param1String) {
      writeString(param1Parcel, param1String);
    }
    
    public void writeString16(Parcel param1Parcel, String param1String) {
      writeString(param1Parcel, param1String);
    }
    
    public void finishAndUninstall() {
      this.mParcel.setReadWriteHelper(null);
      int i = this.mParcel.dataPosition();
      this.mParcel.writeStringList(this.mStrings);
      this.mParcel.setDataPosition(this.mStartPos);
      this.mParcel.writeInt(i);
      Parcel parcel = this.mParcel;
      parcel.setDataPosition(parcel.dataSize());
    }
  }
}
