package android.content.pm;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArraySet;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

@SystemApi
public final class IntentFilterVerificationInfo implements Parcelable {
  private static final String ATTR_DOMAIN_NAME = "name";
  
  private static final String ATTR_PACKAGE_NAME = "packageName";
  
  private static final String ATTR_STATUS = "status";
  
  public static final Parcelable.Creator<IntentFilterVerificationInfo> CREATOR;
  
  private static final String TAG = IntentFilterVerificationInfo.class.getName();
  
  private static final String TAG_DOMAIN = "domain";
  
  private ArraySet<String> mDomains = new ArraySet();
  
  private int mMainStatus;
  
  private String mPackageName;
  
  public IntentFilterVerificationInfo() {
    this.mPackageName = null;
    this.mMainStatus = 0;
  }
  
  public IntentFilterVerificationInfo(String paramString, ArraySet<String> paramArraySet) {
    this.mPackageName = paramString;
    this.mDomains = paramArraySet;
    this.mMainStatus = 0;
  }
  
  public IntentFilterVerificationInfo(XmlPullParser paramXmlPullParser) throws IOException, XmlPullParserException {
    readFromXml(paramXmlPullParser);
  }
  
  public IntentFilterVerificationInfo(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public int getStatus() {
    return this.mMainStatus;
  }
  
  public void setStatus(int paramInt) {
    if (paramInt >= 0 && paramInt <= 3) {
      this.mMainStatus = paramInt;
    } else {
      String str = TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Trying to set a non supported status: ");
      stringBuilder.append(paramInt);
      Log.w(str, stringBuilder.toString());
    } 
  }
  
  public Set<String> getDomains() {
    return (Set<String>)this.mDomains;
  }
  
  public void setDomains(ArraySet<String> paramArraySet) {
    this.mDomains = paramArraySet;
  }
  
  public String getDomainsString() {
    StringBuilder stringBuilder = new StringBuilder();
    for (String str : this.mDomains) {
      if (stringBuilder.length() > 0)
        stringBuilder.append(" "); 
      stringBuilder.append(str);
    } 
    return stringBuilder.toString();
  }
  
  String getStringFromXml(XmlPullParser paramXmlPullParser, String paramString1, String paramString2) {
    StringBuilder stringBuilder;
    String str = paramXmlPullParser.getAttributeValue(null, paramString1);
    if (str == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Missing element under ");
      stringBuilder.append(TAG);
      stringBuilder.append(": ");
      stringBuilder.append(paramString1);
      stringBuilder.append(" at ");
      stringBuilder.append(paramXmlPullParser.getPositionDescription());
      String str1 = stringBuilder.toString();
      Log.w(TAG, str1);
      return paramString2;
    } 
    return (String)stringBuilder;
  }
  
  int getIntFromXml(XmlPullParser paramXmlPullParser, String paramString, int paramInt) {
    StringBuilder stringBuilder;
    String str = paramXmlPullParser.getAttributeValue(null, paramString);
    if (TextUtils.isEmpty(str)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Missing element under ");
      stringBuilder.append(TAG);
      stringBuilder.append(": ");
      stringBuilder.append(paramString);
      stringBuilder.append(" at ");
      stringBuilder.append(paramXmlPullParser.getPositionDescription());
      String str1 = stringBuilder.toString();
      Log.w(TAG, str1);
      return paramInt;
    } 
    return Integer.parseInt((String)stringBuilder);
  }
  
  public void readFromXml(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: ldc 'packageName'
    //   4: aconst_null
    //   5: invokevirtual getStringFromXml : (Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   8: astore_2
    //   9: aload_0
    //   10: aload_2
    //   11: putfield mPackageName : Ljava/lang/String;
    //   14: aload_2
    //   15: ifnonnull -> 27
    //   18: getstatic android/content/pm/IntentFilterVerificationInfo.TAG : Ljava/lang/String;
    //   21: ldc 'Package name cannot be null!'
    //   23: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   26: pop
    //   27: aload_0
    //   28: aload_1
    //   29: ldc 'status'
    //   31: iconst_m1
    //   32: invokevirtual getIntFromXml : (Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;I)I
    //   35: istore_3
    //   36: iload_3
    //   37: iconst_m1
    //   38: if_icmpne -> 79
    //   41: getstatic android/content/pm/IntentFilterVerificationInfo.TAG : Ljava/lang/String;
    //   44: astore_2
    //   45: new java/lang/StringBuilder
    //   48: dup
    //   49: invokespecial <init> : ()V
    //   52: astore #4
    //   54: aload #4
    //   56: ldc 'Unknown status value: '
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload #4
    //   64: iload_3
    //   65: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   68: pop
    //   69: aload_2
    //   70: aload #4
    //   72: invokevirtual toString : ()Ljava/lang/String;
    //   75: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   78: pop
    //   79: aload_0
    //   80: iload_3
    //   81: putfield mMainStatus : I
    //   84: aload_1
    //   85: invokeinterface getDepth : ()I
    //   90: istore_3
    //   91: aload_1
    //   92: invokeinterface next : ()I
    //   97: istore #5
    //   99: iload #5
    //   101: iconst_1
    //   102: if_icmpeq -> 228
    //   105: iload #5
    //   107: iconst_3
    //   108: if_icmpne -> 121
    //   111: aload_1
    //   112: invokeinterface getDepth : ()I
    //   117: iload_3
    //   118: if_icmple -> 228
    //   121: iload #5
    //   123: iconst_3
    //   124: if_icmpeq -> 91
    //   127: iload #5
    //   129: iconst_4
    //   130: if_icmpne -> 136
    //   133: goto -> 91
    //   136: aload_1
    //   137: invokeinterface getName : ()Ljava/lang/String;
    //   142: astore #6
    //   144: aload #6
    //   146: ldc 'domain'
    //   148: invokevirtual equals : (Ljava/lang/Object;)Z
    //   151: ifeq -> 182
    //   154: aload_0
    //   155: aload_1
    //   156: ldc 'name'
    //   158: aconst_null
    //   159: invokevirtual getStringFromXml : (Lorg/xmlpull/v1/XmlPullParser;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   162: astore_2
    //   163: aload_2
    //   164: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   167: ifne -> 179
    //   170: aload_0
    //   171: getfield mDomains : Landroid/util/ArraySet;
    //   174: aload_2
    //   175: invokevirtual add : (Ljava/lang/Object;)Z
    //   178: pop
    //   179: goto -> 221
    //   182: getstatic android/content/pm/IntentFilterVerificationInfo.TAG : Ljava/lang/String;
    //   185: astore_2
    //   186: new java/lang/StringBuilder
    //   189: dup
    //   190: invokespecial <init> : ()V
    //   193: astore #4
    //   195: aload #4
    //   197: ldc 'Unknown tag parsing IntentFilter: '
    //   199: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   202: pop
    //   203: aload #4
    //   205: aload #6
    //   207: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload_2
    //   212: aload #4
    //   214: invokevirtual toString : ()Ljava/lang/String;
    //   217: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   220: pop
    //   221: aload_1
    //   222: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   225: goto -> 91
    //   228: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #151	-> 0
    //   #152	-> 14
    //   #153	-> 18
    //   #155	-> 27
    //   #156	-> 36
    //   #157	-> 41
    //   #159	-> 79
    //   #161	-> 84
    //   #163	-> 91
    //   #165	-> 111
    //   #166	-> 121
    //   #168	-> 133
    //   #171	-> 136
    //   #172	-> 144
    //   #173	-> 154
    //   #174	-> 163
    //   #175	-> 170
    //   #177	-> 179
    //   #178	-> 182
    //   #180	-> 221
    //   #181	-> 225
    //   #182	-> 228
  }
  
  public void writeToXml(XmlSerializer paramXmlSerializer) throws IOException {
    paramXmlSerializer.attribute(null, "packageName", this.mPackageName);
    paramXmlSerializer.attribute(null, "status", String.valueOf(this.mMainStatus));
    for (String str : this.mDomains) {
      paramXmlSerializer.startTag(null, "domain");
      paramXmlSerializer.attribute(null, "name", str);
      paramXmlSerializer.endTag(null, "domain");
    } 
  }
  
  public String getStatusString() {
    return getStatusStringFromValue(this.mMainStatus << 32L);
  }
  
  public static String getStatusStringFromValue(long paramLong) {
    StringBuilder stringBuilder = new StringBuilder();
    int i = (int)(paramLong >> 32L);
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            stringBuilder.append("undefined");
          } else {
            stringBuilder.append("always-ask");
          } 
        } else {
          stringBuilder.append("never");
        } 
      } else {
        stringBuilder.append("always : ");
        stringBuilder.append(Long.toHexString(0xFFFFFFFFFFFFFFFFL & paramLong));
      } 
    } else {
      stringBuilder.append("ask");
    } 
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  private void readFromParcel(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mMainStatus = paramParcel.readInt();
    ArrayList arrayList = new ArrayList();
    paramParcel.readStringList(arrayList);
    this.mDomains.addAll(arrayList);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeInt(this.mMainStatus);
    paramParcel.writeStringList(new ArrayList<>((Collection<? extends String>)this.mDomains));
  }
  
  static {
    CREATOR = new Parcelable.Creator<IntentFilterVerificationInfo>() {
        public IntentFilterVerificationInfo createFromParcel(Parcel param1Parcel) {
          return new IntentFilterVerificationInfo(param1Parcel);
        }
        
        public IntentFilterVerificationInfo[] newArray(int param1Int) {
          return new IntentFilterVerificationInfo[param1Int];
        }
      };
  }
}
