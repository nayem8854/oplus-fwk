package android.content.pm.permission;

import android.annotation.IntRange;
import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SplitPermissionInfoParcelable implements Parcelable {
  private void onConstructed() {
    Preconditions.checkCollectionElementsNotNull(this.mNewPermissions, "newPermissions");
  }
  
  public SplitPermissionInfoParcelable(String paramString, List<String> paramList, int paramInt) {
    this.mSplitPermission = paramString;
    AnnotationValidations.validate(NonNull.class, null, paramString);
    this.mNewPermissions = paramList;
    AnnotationValidations.validate(NonNull.class, null, paramList);
    this.mTargetSdk = paramInt;
    AnnotationValidations.validate(IntRange.class, null, paramInt, "from", 0L);
    onConstructed();
  }
  
  public String getSplitPermission() {
    return this.mSplitPermission;
  }
  
  public List<String> getNewPermissions() {
    return this.mNewPermissions;
  }
  
  public int getTargetSdk() {
    return this.mTargetSdk;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str1 = this.mSplitPermission, str2 = ((SplitPermissionInfoParcelable)paramObject).mSplitPermission;
    if (Objects.equals(str1, str2)) {
      List<String> list1 = this.mNewPermissions, list2 = ((SplitPermissionInfoParcelable)paramObject).mNewPermissions;
      if (Objects.equals(list1, list2) && this.mTargetSdk == ((SplitPermissionInfoParcelable)paramObject).mTargetSdk)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.mSplitPermission);
    int j = Objects.hashCode(this.mNewPermissions);
    int k = this.mTargetSdk;
    return ((1 * 31 + i) * 31 + j) * 31 + k;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mSplitPermission);
    paramParcel.writeStringList(this.mNewPermissions);
    paramParcel.writeInt(this.mTargetSdk);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public static final Parcelable.Creator<SplitPermissionInfoParcelable> CREATOR = new Parcelable.Creator<SplitPermissionInfoParcelable>() {
      public SplitPermissionInfoParcelable[] newArray(int param1Int) {
        return new SplitPermissionInfoParcelable[param1Int];
      }
      
      public SplitPermissionInfoParcelable createFromParcel(Parcel param1Parcel) {
        String str = param1Parcel.readString();
        ArrayList<String> arrayList = new ArrayList();
        param1Parcel.readStringList(arrayList);
        int i = param1Parcel.readInt();
        return new SplitPermissionInfoParcelable(str, arrayList, i);
      }
    };
  
  private final List<String> mNewPermissions;
  
  private final String mSplitPermission;
  
  private final int mTargetSdk;
  
  @Deprecated
  private void __metadata() {}
}
