package android.content.pm.permission;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
@Deprecated
public final class RuntimePermissionPresentationInfo implements Parcelable {
  public RuntimePermissionPresentationInfo(CharSequence paramCharSequence, boolean paramBoolean1, boolean paramBoolean2) {
    this.mLabel = paramCharSequence;
    int i = 0;
    if (paramBoolean1)
      i = false | true; 
    int j = i;
    if (paramBoolean2)
      j = i | 0x2; 
    this.mFlags = j;
  }
  
  private RuntimePermissionPresentationInfo(Parcel paramParcel) {
    this.mLabel = paramParcel.readCharSequence();
    this.mFlags = paramParcel.readInt();
  }
  
  public boolean isGranted() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public boolean isStandard() {
    boolean bool;
    if ((this.mFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public CharSequence getLabel() {
    return this.mLabel;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeCharSequence(this.mLabel);
    paramParcel.writeInt(this.mFlags);
  }
  
  public static final Parcelable.Creator<RuntimePermissionPresentationInfo> CREATOR = new Parcelable.Creator<RuntimePermissionPresentationInfo>() {
      public RuntimePermissionPresentationInfo createFromParcel(Parcel param1Parcel) {
        return new RuntimePermissionPresentationInfo(param1Parcel);
      }
      
      public RuntimePermissionPresentationInfo[] newArray(int param1Int) {
        return new RuntimePermissionPresentationInfo[param1Int];
      }
    };
  
  private static final int FLAG_GRANTED = 1;
  
  private static final int FLAG_STANDARD = 2;
  
  private final int mFlags;
  
  private final CharSequence mLabel;
}
