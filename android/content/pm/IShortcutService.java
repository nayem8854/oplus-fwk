package android.content.pm;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.List;

public interface IShortcutService extends IInterface {
  boolean addDynamicShortcuts(String paramString, ParceledListSlice paramParceledListSlice, int paramInt) throws RemoteException;
  
  void applyRestore(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  Intent createShortcutResultIntent(String paramString, ShortcutInfo paramShortcutInfo, int paramInt) throws RemoteException;
  
  void disableShortcuts(String paramString, List paramList, CharSequence paramCharSequence, int paramInt1, int paramInt2) throws RemoteException;
  
  void enableShortcuts(String paramString, List paramList, int paramInt) throws RemoteException;
  
  byte[] getBackupPayload(int paramInt) throws RemoteException;
  
  int getIconMaxDimensions(String paramString, int paramInt) throws RemoteException;
  
  int getMaxShortcutCountPerActivity(String paramString, int paramInt) throws RemoteException;
  
  long getRateLimitResetTime(String paramString, int paramInt) throws RemoteException;
  
  int getRemainingCallCount(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getShareTargets(String paramString, IntentFilter paramIntentFilter, int paramInt) throws RemoteException;
  
  ParceledListSlice getShortcuts(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean hasShareTargets(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean isRequestPinItemSupported(int paramInt1, int paramInt2) throws RemoteException;
  
  void onApplicationActive(String paramString, int paramInt) throws RemoteException;
  
  void pushDynamicShortcut(String paramString, ShortcutInfo paramShortcutInfo, int paramInt) throws RemoteException;
  
  void removeAllDynamicShortcuts(String paramString, int paramInt) throws RemoteException;
  
  void removeDynamicShortcuts(String paramString, List paramList, int paramInt) throws RemoteException;
  
  void removeLongLivedShortcuts(String paramString, List paramList, int paramInt) throws RemoteException;
  
  void reportShortcutUsed(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean requestPinShortcut(String paramString, ShortcutInfo paramShortcutInfo, IntentSender paramIntentSender, int paramInt) throws RemoteException;
  
  void resetThrottling() throws RemoteException;
  
  boolean setDynamicShortcuts(String paramString, ParceledListSlice paramParceledListSlice, int paramInt) throws RemoteException;
  
  boolean updateShortcuts(String paramString, ParceledListSlice paramParceledListSlice, int paramInt) throws RemoteException;
  
  class Default implements IShortcutService {
    public boolean setDynamicShortcuts(String param1String, ParceledListSlice param1ParceledListSlice, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean addDynamicShortcuts(String param1String, ParceledListSlice param1ParceledListSlice, int param1Int) throws RemoteException {
      return false;
    }
    
    public void removeDynamicShortcuts(String param1String, List param1List, int param1Int) throws RemoteException {}
    
    public void removeAllDynamicShortcuts(String param1String, int param1Int) throws RemoteException {}
    
    public boolean updateShortcuts(String param1String, ParceledListSlice param1ParceledListSlice, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean requestPinShortcut(String param1String, ShortcutInfo param1ShortcutInfo, IntentSender param1IntentSender, int param1Int) throws RemoteException {
      return false;
    }
    
    public Intent createShortcutResultIntent(String param1String, ShortcutInfo param1ShortcutInfo, int param1Int) throws RemoteException {
      return null;
    }
    
    public void disableShortcuts(String param1String, List param1List, CharSequence param1CharSequence, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void enableShortcuts(String param1String, List param1List, int param1Int) throws RemoteException {}
    
    public int getMaxShortcutCountPerActivity(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getRemainingCallCount(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public long getRateLimitResetTime(String param1String, int param1Int) throws RemoteException {
      return 0L;
    }
    
    public int getIconMaxDimensions(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void reportShortcutUsed(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public void resetThrottling() throws RemoteException {}
    
    public void onApplicationActive(String param1String, int param1Int) throws RemoteException {}
    
    public byte[] getBackupPayload(int param1Int) throws RemoteException {
      return null;
    }
    
    public void applyRestore(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public boolean isRequestPinItemSupported(int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public ParceledListSlice getShareTargets(String param1String, IntentFilter param1IntentFilter, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean hasShareTargets(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return false;
    }
    
    public void removeLongLivedShortcuts(String param1String, List param1List, int param1Int) throws RemoteException {}
    
    public ParceledListSlice getShortcuts(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void pushDynamicShortcut(String param1String, ShortcutInfo param1ShortcutInfo, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IShortcutService {
    private static final String DESCRIPTOR = "android.content.pm.IShortcutService";
    
    static final int TRANSACTION_addDynamicShortcuts = 2;
    
    static final int TRANSACTION_applyRestore = 18;
    
    static final int TRANSACTION_createShortcutResultIntent = 7;
    
    static final int TRANSACTION_disableShortcuts = 8;
    
    static final int TRANSACTION_enableShortcuts = 9;
    
    static final int TRANSACTION_getBackupPayload = 17;
    
    static final int TRANSACTION_getIconMaxDimensions = 13;
    
    static final int TRANSACTION_getMaxShortcutCountPerActivity = 10;
    
    static final int TRANSACTION_getRateLimitResetTime = 12;
    
    static final int TRANSACTION_getRemainingCallCount = 11;
    
    static final int TRANSACTION_getShareTargets = 20;
    
    static final int TRANSACTION_getShortcuts = 23;
    
    static final int TRANSACTION_hasShareTargets = 21;
    
    static final int TRANSACTION_isRequestPinItemSupported = 19;
    
    static final int TRANSACTION_onApplicationActive = 16;
    
    static final int TRANSACTION_pushDynamicShortcut = 24;
    
    static final int TRANSACTION_removeAllDynamicShortcuts = 4;
    
    static final int TRANSACTION_removeDynamicShortcuts = 3;
    
    static final int TRANSACTION_removeLongLivedShortcuts = 22;
    
    static final int TRANSACTION_reportShortcutUsed = 14;
    
    static final int TRANSACTION_requestPinShortcut = 6;
    
    static final int TRANSACTION_resetThrottling = 15;
    
    static final int TRANSACTION_setDynamicShortcuts = 1;
    
    static final int TRANSACTION_updateShortcuts = 5;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IShortcutService");
    }
    
    public static IShortcutService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IShortcutService");
      if (iInterface != null && iInterface instanceof IShortcutService)
        return (IShortcutService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "pushDynamicShortcut";
        case 23:
          return "getShortcuts";
        case 22:
          return "removeLongLivedShortcuts";
        case 21:
          return "hasShareTargets";
        case 20:
          return "getShareTargets";
        case 19:
          return "isRequestPinItemSupported";
        case 18:
          return "applyRestore";
        case 17:
          return "getBackupPayload";
        case 16:
          return "onApplicationActive";
        case 15:
          return "resetThrottling";
        case 14:
          return "reportShortcutUsed";
        case 13:
          return "getIconMaxDimensions";
        case 12:
          return "getRateLimitResetTime";
        case 11:
          return "getRemainingCallCount";
        case 10:
          return "getMaxShortcutCountPerActivity";
        case 9:
          return "enableShortcuts";
        case 8:
          return "disableShortcuts";
        case 7:
          return "createShortcutResultIntent";
        case 6:
          return "requestPinShortcut";
        case 5:
          return "updateShortcuts";
        case 4:
          return "removeAllDynamicShortcuts";
        case 3:
          return "removeDynamicShortcuts";
        case 2:
          return "addDynamicShortcuts";
        case 1:
          break;
      } 
      return "setDynamicShortcuts";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool6;
        int n;
        boolean bool5;
        int m;
        boolean bool4;
        int k;
        boolean bool3;
        int j;
        boolean bool2;
        ParceledListSlice parceledListSlice;
        byte[] arrayOfByte1;
        Intent intent;
        String str4;
        ClassLoader classLoader3;
        ArrayList arrayList3;
        String str3;
        ClassLoader classLoader2;
        ArrayList arrayList2;
        String str2;
        ClassLoader classLoader1;
        ArrayList arrayList1;
        ShortcutInfo shortcutInfo;
        String str7;
        byte[] arrayOfByte2;
        String str6;
        ClassLoader classLoader4;
        String str5;
        long l;
        ArrayList arrayList4;
        String str8;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.content.pm.IShortcutService");
            str4 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              shortcutInfo = (ShortcutInfo)ShortcutInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              shortcutInfo = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            pushDynamicShortcut(str4, shortcutInfo, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            param1Parcel1.enforceInterface("android.content.pm.IShortcutService");
            str7 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            parceledListSlice = getShortcuts(str7, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            if (parceledListSlice != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 22:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            str7 = parceledListSlice.readString();
            classLoader3 = getClass().getClassLoader();
            arrayList3 = parceledListSlice.readArrayList(classLoader3);
            param1Int1 = parceledListSlice.readInt();
            removeLongLivedShortcuts(str7, arrayList3, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            str3 = parceledListSlice.readString();
            str7 = parceledListSlice.readString();
            param1Int1 = parceledListSlice.readInt();
            bool6 = hasShareTargets(str3, str7, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            return true;
          case 20:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            str3 = parceledListSlice.readString();
            if (parceledListSlice.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              str7 = null;
            } 
            n = parceledListSlice.readInt();
            parceledListSlice = getShareTargets(str3, (IntentFilter)str7, n);
            param1Parcel2.writeNoException();
            if (parceledListSlice != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 19:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            param1Int2 = parceledListSlice.readInt();
            n = parceledListSlice.readInt();
            bool5 = isRequestPinItemSupported(param1Int2, n);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            return true;
          case 18:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            arrayOfByte2 = parceledListSlice.createByteArray();
            m = parceledListSlice.readInt();
            applyRestore(arrayOfByte2, m);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            parceledListSlice.enforceInterface("android.content.pm.IShortcutService");
            m = parceledListSlice.readInt();
            arrayOfByte1 = getBackupPayload(m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte1);
            return true;
          case 16:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            onApplicationActive(str6, m);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            resetThrottling();
            param1Parcel2.writeNoException();
            return true;
          case 14:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            str3 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            reportShortcutUsed(str6, str3, m);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            m = getIconMaxDimensions(str6, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 12:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            l = getRateLimitResetTime(str6, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 11:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            m = getRemainingCallCount(str6, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 10:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            m = arrayOfByte1.readInt();
            m = getMaxShortcutCountPerActivity(str6, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(m);
            return true;
          case 9:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str6 = arrayOfByte1.readString();
            classLoader2 = getClass().getClassLoader();
            arrayList2 = arrayOfByte1.readArrayList(classLoader2);
            m = arrayOfByte1.readInt();
            enableShortcuts(str6, arrayList2, m);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str2 = arrayOfByte1.readString();
            classLoader4 = getClass().getClassLoader();
            arrayList4 = arrayOfByte1.readArrayList(classLoader4);
            if (arrayOfByte1.readInt() != 0) {
              CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              classLoader4 = null;
            } 
            m = arrayOfByte1.readInt();
            param1Int2 = arrayOfByte1.readInt();
            disableShortcuts(str2, arrayList4, (CharSequence)classLoader4, m, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfByte1.enforceInterface("android.content.pm.IShortcutService");
            str2 = arrayOfByte1.readString();
            if (arrayOfByte1.readInt() != 0) {
              ShortcutInfo shortcutInfo1 = (ShortcutInfo)ShortcutInfo.CREATOR.createFromParcel((Parcel)arrayOfByte1);
            } else {
              classLoader4 = null;
            } 
            m = arrayOfByte1.readInt();
            intent = createShortcutResultIntent(str2, (ShortcutInfo)classLoader4, m);
            param1Parcel2.writeNoException();
            if (intent != null) {
              param1Parcel2.writeInt(1);
              intent.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            intent.enforceInterface("android.content.pm.IShortcutService");
            str8 = intent.readString();
            if (intent.readInt() != 0) {
              ShortcutInfo shortcutInfo1 = (ShortcutInfo)ShortcutInfo.CREATOR.createFromParcel((Parcel)intent);
            } else {
              classLoader4 = null;
            } 
            if (intent.readInt() != 0) {
              IntentSender intentSender = (IntentSender)IntentSender.CREATOR.createFromParcel((Parcel)intent);
            } else {
              str2 = null;
            } 
            m = intent.readInt();
            bool4 = requestPinShortcut(str8, (ShortcutInfo)classLoader4, (IntentSender)str2, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 5:
            intent.enforceInterface("android.content.pm.IShortcutService");
            str2 = intent.readString();
            if (intent.readInt() != 0) {
              ParceledListSlice parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)intent);
            } else {
              classLoader4 = null;
            } 
            k = intent.readInt();
            bool3 = updateShortcuts(str2, (ParceledListSlice)classLoader4, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 4:
            intent.enforceInterface("android.content.pm.IShortcutService");
            str5 = intent.readString();
            j = intent.readInt();
            removeAllDynamicShortcuts(str5, j);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            intent.enforceInterface("android.content.pm.IShortcutService");
            str5 = intent.readString();
            classLoader1 = getClass().getClassLoader();
            arrayList1 = intent.readArrayList(classLoader1);
            j = intent.readInt();
            removeDynamicShortcuts(str5, arrayList1, j);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            intent.enforceInterface("android.content.pm.IShortcutService");
            str1 = intent.readString();
            if (intent.readInt() != 0) {
              ParceledListSlice parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)intent);
            } else {
              str5 = null;
            } 
            j = intent.readInt();
            bool2 = addDynamicShortcuts(str1, (ParceledListSlice)str5, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 1:
            break;
        } 
        intent.enforceInterface("android.content.pm.IShortcutService");
        String str1 = intent.readString();
        if (intent.readInt() != 0) {
          ParceledListSlice parceledListSlice1 = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel((Parcel)intent);
        } else {
          str5 = null;
        } 
        int i = intent.readInt();
        boolean bool1 = setDynamicShortcuts(str1, (ParceledListSlice)str5, i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      param1Parcel2.writeString("android.content.pm.IShortcutService");
      return true;
    }
    
    private static class Proxy implements IShortcutService {
      public static IShortcutService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IShortcutService";
      }
      
      public boolean setDynamicShortcuts(String param2String, ParceledListSlice param2ParceledListSlice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().setDynamicShortcuts(param2String, param2ParceledListSlice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addDynamicShortcuts(String param2String, ParceledListSlice param2ParceledListSlice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().addDynamicShortcuts(param2String, param2ParceledListSlice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeDynamicShortcuts(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().removeDynamicShortcuts(param2String, param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeAllDynamicShortcuts(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().removeAllDynamicShortcuts(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateShortcuts(String param2String, ParceledListSlice param2ParceledListSlice, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().updateShortcuts(param2String, param2ParceledListSlice, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestPinShortcut(String param2String, ShortcutInfo param2ShortcutInfo, IntentSender param2IntentSender, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2ShortcutInfo != null) {
            parcel1.writeInt(1);
            param2ShortcutInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IntentSender != null) {
            parcel1.writeInt(1);
            param2IntentSender.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().requestPinShortcut(param2String, param2ShortcutInfo, param2IntentSender, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Intent createShortcutResultIntent(String param2String, ShortcutInfo param2ShortcutInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          if (param2ShortcutInfo != null) {
            parcel1.writeInt(1);
            param2ShortcutInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null)
            return IShortcutService.Stub.getDefaultImpl().createShortcutResultIntent(param2String, param2ShortcutInfo, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Intent intent = (Intent)Intent.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Intent)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disableShortcuts(String param2String, List param2List, CharSequence param2CharSequence, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().disableShortcuts(param2String, param2List, param2CharSequence, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enableShortcuts(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().enableShortcuts(param2String, param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMaxShortcutCountPerActivity(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            param2Int = IShortcutService.Stub.getDefaultImpl().getMaxShortcutCountPerActivity(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRemainingCallCount(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            param2Int = IShortcutService.Stub.getDefaultImpl().getRemainingCallCount(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getRateLimitResetTime(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null)
            return IShortcutService.Stub.getDefaultImpl().getRateLimitResetTime(param2String, param2Int); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getIconMaxDimensions(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            param2Int = IShortcutService.Stub.getDefaultImpl().getIconMaxDimensions(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reportShortcutUsed(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().reportShortcutUsed(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetThrottling() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().resetThrottling();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onApplicationActive(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().onApplicationActive(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getBackupPayload(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null)
            return IShortcutService.Stub.getDefaultImpl().getBackupPayload(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void applyRestore(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().applyRestore(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRequestPinItemSupported(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(19, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().isRequestPinItemSupported(param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getShareTargets(String param2String, IntentFilter param2IntentFilter, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          if (param2IntentFilter != null) {
            parcel1.writeInt(1);
            param2IntentFilter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null)
            return IShortcutService.Stub.getDefaultImpl().getShareTargets(param2String, param2IntentFilter, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasShareTargets(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(21, parcel1, parcel2, 0);
          if (!bool2 && IShortcutService.Stub.getDefaultImpl() != null) {
            bool1 = IShortcutService.Stub.getDefaultImpl().hasShareTargets(param2String1, param2String2, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeLongLivedShortcuts(String param2String, List param2List, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeList(param2List);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().removeLongLivedShortcuts(param2String, param2List, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getShortcuts(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null)
            return IShortcutService.Stub.getDefaultImpl().getShortcuts(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pushDynamicShortcut(String param2String, ShortcutInfo param2ShortcutInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IShortcutService");
          parcel1.writeString(param2String);
          if (param2ShortcutInfo != null) {
            parcel1.writeInt(1);
            param2ShortcutInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IShortcutService.Stub.getDefaultImpl() != null) {
            IShortcutService.Stub.getDefaultImpl().pushDynamicShortcut(param2String, param2ShortcutInfo, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IShortcutService param1IShortcutService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IShortcutService != null) {
          Proxy.sDefaultImpl = param1IShortcutService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IShortcutService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
