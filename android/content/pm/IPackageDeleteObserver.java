package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPackageDeleteObserver extends IInterface {
  void packageDeleted(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IPackageDeleteObserver {
    public void packageDeleted(String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPackageDeleteObserver {
    private static final String DESCRIPTOR = "android.content.pm.IPackageDeleteObserver";
    
    static final int TRANSACTION_packageDeleted = 1;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IPackageDeleteObserver");
    }
    
    public static IPackageDeleteObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IPackageDeleteObserver");
      if (iInterface != null && iInterface instanceof IPackageDeleteObserver)
        return (IPackageDeleteObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "packageDeleted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.content.pm.IPackageDeleteObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.content.pm.IPackageDeleteObserver");
      String str = param1Parcel1.readString();
      param1Int1 = param1Parcel1.readInt();
      packageDeleted(str, param1Int1);
      return true;
    }
    
    private static class Proxy implements IPackageDeleteObserver {
      public static IPackageDeleteObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IPackageDeleteObserver";
      }
      
      public void packageDeleted(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageDeleteObserver");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IPackageDeleteObserver.Stub.getDefaultImpl() != null) {
            IPackageDeleteObserver.Stub.getDefaultImpl().packageDeleted(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPackageDeleteObserver param1IPackageDeleteObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPackageDeleteObserver != null) {
          Proxy.sDefaultImpl = param1IPackageDeleteObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPackageDeleteObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
