package android.content.pm;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public class ShortcutManager {
  public static final int FLAG_MATCH_CACHED = 8;
  
  public static final int FLAG_MATCH_DYNAMIC = 2;
  
  public static final int FLAG_MATCH_MANIFEST = 1;
  
  public static final int FLAG_MATCH_PINNED = 4;
  
  private static final String TAG = "ShortcutManager";
  
  private final Context mContext;
  
  private final IShortcutService mService;
  
  public ShortcutManager(Context paramContext, IShortcutService paramIShortcutService) {
    this.mContext = paramContext;
    this.mService = paramIShortcutService;
  }
  
  public ShortcutManager(Context paramContext) {
    this(paramContext, IShortcutService.Stub.asInterface(iBinder));
  }
  
  public boolean setDynamicShortcuts(List<ShortcutInfo> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      ParceledListSlice<Parcelable> parceledListSlice = new ParceledListSlice<>();
      this((List)paramList);
      int i = injectMyUserId();
      return iShortcutService.setDynamicShortcuts(str, parceledListSlice, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ShortcutInfo> getDynamicShortcuts() {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      ParceledListSlice parceledListSlice = iShortcutService.getShortcuts(str, 2, i);
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ShortcutInfo> getManifestShortcuts() {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      ParceledListSlice parceledListSlice = iShortcutService.getShortcuts(str, 1, i);
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ShortcutInfo> getShortcuts(int paramInt) {
    try {
      ParceledListSlice parceledListSlice = this.mService.getShortcuts(this.mContext.getPackageName(), paramInt, injectMyUserId());
      return 
        parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean addDynamicShortcuts(List<ShortcutInfo> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      ParceledListSlice<Parcelable> parceledListSlice = new ParceledListSlice<>();
      this((List)paramList);
      int i = injectMyUserId();
      return iShortcutService.addDynamicShortcuts(str, parceledListSlice, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeDynamicShortcuts(List<String> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.removeDynamicShortcuts(str, paramList, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeAllDynamicShortcuts() {
    try {
      this.mService.removeAllDynamicShortcuts(this.mContext.getPackageName(), injectMyUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeLongLivedShortcuts(List<String> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.removeLongLivedShortcuts(str, paramList, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<ShortcutInfo> getPinnedShortcuts() {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      ParceledListSlice parceledListSlice = iShortcutService.getShortcuts(str, 4, i);
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean updateShortcuts(List<ShortcutInfo> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      ParceledListSlice<Parcelable> parceledListSlice = new ParceledListSlice<>();
      this((List)paramList);
      int i = injectMyUserId();
      return iShortcutService.updateShortcuts(str, parceledListSlice, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableShortcuts(List<String> paramList) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.disableShortcuts(str, paramList, null, 0, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableShortcuts(List<String> paramList, int paramInt) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.disableShortcuts(str, paramList, null, paramInt, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void disableShortcuts(List<String> paramList, String paramString) {
    disableShortcuts(paramList, paramString);
  }
  
  public void disableShortcuts(List<String> paramList, CharSequence paramCharSequence) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.disableShortcuts(str, paramList, paramCharSequence, 0, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void enableShortcuts(List<String> paramList) {
    try {
      this.mService.enableShortcuts(this.mContext.getPackageName(), paramList, injectMyUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getMaxShortcutCountForActivity() {
    return getMaxShortcutCountPerActivity();
  }
  
  public int getMaxShortcutCountPerActivity() {
    try {
      IShortcutService iShortcutService = this.mService;
      Context context = this.mContext;
      String str = context.getPackageName();
      null = injectMyUserId();
      return iShortcutService.getMaxShortcutCountPerActivity(str, null);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getRemainingCallCount() {
    try {
      return this.mService.getRemainingCallCount(this.mContext.getPackageName(), injectMyUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public long getRateLimitResetTime() {
    try {
      return this.mService.getRateLimitResetTime(this.mContext.getPackageName(), injectMyUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isRateLimitingActive() {
    try {
      boolean bool;
      int i = this.mService.getRemainingCallCount(this.mContext.getPackageName(), injectMyUserId());
      if (i == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getIconMaxWidth() {
    try {
      return this.mService.getIconMaxDimensions(this.mContext.getPackageName(), injectMyUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getIconMaxHeight() {
    try {
      return this.mService.getIconMaxDimensions(this.mContext.getPackageName(), injectMyUserId());
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void reportShortcutUsed(String paramString) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      iShortcutService.reportShortcutUsed(str, paramString, i);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isRequestPinShortcutSupported() {
    try {
      return this.mService.isRequestPinItemSupported(injectMyUserId(), 1);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestPinShortcut(ShortcutInfo paramShortcutInfo, IntentSender paramIntentSender) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      return iShortcutService.requestPinShortcut(str, paramShortcutInfo, paramIntentSender, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Intent createShortcutResultIntent(ShortcutInfo paramShortcutInfo) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      return iShortcutService.createShortcutResultIntent(str, paramShortcutInfo, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void onApplicationActive(String paramString, int paramInt) {
    try {
      this.mService.onApplicationActive(paramString, paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  protected int injectMyUserId() {
    return this.mContext.getUserId();
  }
  
  @SystemApi
  public List<ShareShortcutInfo> getShareTargets(IntentFilter paramIntentFilter) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      ParceledListSlice parceledListSlice = iShortcutService.getShareTargets(str, paramIntentFilter, i);
      return parceledListSlice.getList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  class ShareShortcutInfo implements Parcelable {
    public ShareShortcutInfo(ShortcutManager this$0, ComponentName param1ComponentName) {
      if (this$0 != null) {
        if (param1ComponentName != null) {
          this.mShortcutInfo = (ShortcutInfo)this$0;
          this.mTargetComponent = param1ComponentName;
          return;
        } 
        throw new NullPointerException("target component is null");
      } 
      throw new NullPointerException("shortcut info is null");
    }
    
    private ShareShortcutInfo(ShortcutManager this$0) {
      this.mShortcutInfo = (ShortcutInfo)this$0.readParcelable(ShortcutInfo.class.getClassLoader());
      this.mTargetComponent = (ComponentName)this$0.readParcelable(ComponentName.class.getClassLoader());
    }
    
    public ShortcutInfo getShortcutInfo() {
      return this.mShortcutInfo;
    }
    
    public ComponentName getTargetComponent() {
      return this.mTargetComponent;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeParcelable(this.mShortcutInfo, param1Int);
      param1Parcel.writeParcelable(this.mTargetComponent, param1Int);
    }
    
    public static final Parcelable.Creator<ShareShortcutInfo> CREATOR = new Parcelable.Creator<ShareShortcutInfo>() {
        public ShortcutManager.ShareShortcutInfo createFromParcel(Parcel param2Parcel) {
          return new ShortcutManager.ShareShortcutInfo();
        }
        
        public ShortcutManager.ShareShortcutInfo[] newArray(int param2Int) {
          return new ShortcutManager.ShareShortcutInfo[param2Int];
        }
      };
    
    private final ShortcutInfo mShortcutInfo;
    
    private final ComponentName mTargetComponent;
  }
  
  @SystemApi
  public boolean hasShareTargets(String paramString) {
    try {
      IShortcutService iShortcutService = this.mService;
      String str = this.mContext.getPackageName();
      int i = injectMyUserId();
      return iShortcutService.hasShareTargets(str, paramString, i);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void pushDynamicShortcut(ShortcutInfo paramShortcutInfo) {
    try {
      this.mService.pushDynamicShortcut(this.mContext.getPackageName(), paramShortcutInfo, injectMyUserId());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ShortcutMatchFlags {}
}
