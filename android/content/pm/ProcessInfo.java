package android.content.pm;

import android.annotation.NonNull;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.ArraySet;
import com.android.internal.util.AnnotationValidations;
import com.android.internal.util.Parcelling;

public class ProcessInfo implements Parcelable {
  @Deprecated
  public ProcessInfo(ProcessInfo paramProcessInfo) {
    this.name = paramProcessInfo.name;
    this.deniedPermissions = paramProcessInfo.deniedPermissions;
    this.gwpAsanMode = paramProcessInfo.gwpAsanMode;
  }
  
  public ProcessInfo(String paramString, ArraySet<String> paramArraySet, int paramInt) {
    this.name = paramString;
    AnnotationValidations.validate(NonNull.class, null, paramString);
    this.deniedPermissions = paramArraySet;
    this.gwpAsanMode = paramInt;
    AnnotationValidations.validate(ApplicationInfo.GwpAsanMode.class, null, paramInt);
  }
  
  static {
    Parcelling<ArraySet<String>> parcelling = Parcelling.Cache.get(Parcelling.BuiltIn.ForInternedStringArraySet.class);
    if (parcelling == null)
      sParcellingForDeniedPermissions = Parcelling.Cache.put((Parcelling)new Parcelling.BuiltIn.ForInternedStringArraySet()); 
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    byte b = 0;
    if (this.deniedPermissions != null)
      b = (byte)(0x0 | 0x2); 
    paramParcel.writeByte(b);
    paramParcel.writeString(this.name);
    sParcellingForDeniedPermissions.parcel(this.deniedPermissions, paramParcel, paramInt);
    paramParcel.writeInt(this.gwpAsanMode);
  }
  
  public int describeContents() {
    return 0;
  }
  
  protected ProcessInfo(Parcel paramParcel) {
    paramParcel.readByte();
    String str = paramParcel.readString();
    ArraySet<String> arraySet = (ArraySet)sParcellingForDeniedPermissions.unparcel(paramParcel);
    int i = paramParcel.readInt();
    this.name = str;
    AnnotationValidations.validate(NonNull.class, null, str);
    this.deniedPermissions = arraySet;
    this.gwpAsanMode = i;
    AnnotationValidations.validate(ApplicationInfo.GwpAsanMode.class, null, i);
  }
  
  public static final Parcelable.Creator<ProcessInfo> CREATOR = new Parcelable.Creator<ProcessInfo>() {
      public ProcessInfo[] newArray(int param1Int) {
        return new ProcessInfo[param1Int];
      }
      
      public ProcessInfo createFromParcel(Parcel param1Parcel) {
        return new ProcessInfo(param1Parcel);
      }
    };
  
  static Parcelling<ArraySet<String>> sParcellingForDeniedPermissions;
  
  public ArraySet<String> deniedPermissions;
  
  public int gwpAsanMode;
  
  public String name;
  
  @Deprecated
  private void __metadata() {}
}
