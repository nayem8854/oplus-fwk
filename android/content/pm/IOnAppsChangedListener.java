package android.content.pm;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.UserHandle;

public interface IOnAppsChangedListener extends IInterface {
  void onPackageAdded(UserHandle paramUserHandle, String paramString) throws RemoteException;
  
  void onPackageChanged(UserHandle paramUserHandle, String paramString) throws RemoteException;
  
  void onPackageRemoved(UserHandle paramUserHandle, String paramString) throws RemoteException;
  
  void onPackagesAvailable(UserHandle paramUserHandle, String[] paramArrayOfString, boolean paramBoolean) throws RemoteException;
  
  void onPackagesSuspended(UserHandle paramUserHandle, String[] paramArrayOfString, Bundle paramBundle) throws RemoteException;
  
  void onPackagesUnavailable(UserHandle paramUserHandle, String[] paramArrayOfString, boolean paramBoolean) throws RemoteException;
  
  void onPackagesUnsuspended(UserHandle paramUserHandle, String[] paramArrayOfString) throws RemoteException;
  
  void onShortcutChanged(UserHandle paramUserHandle, String paramString, ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  class Default implements IOnAppsChangedListener {
    public void onPackageRemoved(UserHandle param1UserHandle, String param1String) throws RemoteException {}
    
    public void onPackageAdded(UserHandle param1UserHandle, String param1String) throws RemoteException {}
    
    public void onPackageChanged(UserHandle param1UserHandle, String param1String) throws RemoteException {}
    
    public void onPackagesAvailable(UserHandle param1UserHandle, String[] param1ArrayOfString, boolean param1Boolean) throws RemoteException {}
    
    public void onPackagesUnavailable(UserHandle param1UserHandle, String[] param1ArrayOfString, boolean param1Boolean) throws RemoteException {}
    
    public void onPackagesSuspended(UserHandle param1UserHandle, String[] param1ArrayOfString, Bundle param1Bundle) throws RemoteException {}
    
    public void onPackagesUnsuspended(UserHandle param1UserHandle, String[] param1ArrayOfString) throws RemoteException {}
    
    public void onShortcutChanged(UserHandle param1UserHandle, String param1String, ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnAppsChangedListener {
    private static final String DESCRIPTOR = "android.content.pm.IOnAppsChangedListener";
    
    static final int TRANSACTION_onPackageAdded = 2;
    
    static final int TRANSACTION_onPackageChanged = 3;
    
    static final int TRANSACTION_onPackageRemoved = 1;
    
    static final int TRANSACTION_onPackagesAvailable = 4;
    
    static final int TRANSACTION_onPackagesSuspended = 6;
    
    static final int TRANSACTION_onPackagesUnavailable = 5;
    
    static final int TRANSACTION_onPackagesUnsuspended = 7;
    
    static final int TRANSACTION_onShortcutChanged = 8;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IOnAppsChangedListener");
    }
    
    public static IOnAppsChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IOnAppsChangedListener");
      if (iInterface != null && iInterface instanceof IOnAppsChangedListener)
        return (IOnAppsChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onShortcutChanged";
        case 7:
          return "onPackagesUnsuspended";
        case 6:
          return "onPackagesSuspended";
        case 5:
          return "onPackagesUnavailable";
        case 4:
          return "onPackagesAvailable";
        case 3:
          return "onPackageChanged";
        case 2:
          return "onPackageAdded";
        case 1:
          break;
      } 
      return "onPackageRemoved";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String arrayOfString1[], str2, arrayOfString2[];
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (param1Parcel1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onShortcutChanged((UserHandle)param1Parcel2, str2, (ParceledListSlice)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (param1Parcel1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            arrayOfString1 = param1Parcel1.createStringArray();
            onPackagesUnsuspended((UserHandle)param1Parcel2, arrayOfString1);
            return true;
          case 6:
            arrayOfString1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (arrayOfString1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              param1Parcel2 = null;
            } 
            arrayOfString2 = arrayOfString1.createStringArray();
            if (arrayOfString1.readInt() != 0) {
              Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              arrayOfString1 = null;
            } 
            onPackagesSuspended((UserHandle)param1Parcel2, arrayOfString2, (Bundle)arrayOfString1);
            return true;
          case 5:
            arrayOfString1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (arrayOfString1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              param1Parcel2 = null;
            } 
            arrayOfString2 = arrayOfString1.createStringArray();
            bool1 = bool2;
            if (arrayOfString1.readInt() != 0)
              bool1 = true; 
            onPackagesUnavailable((UserHandle)param1Parcel2, arrayOfString2, bool1);
            return true;
          case 4:
            arrayOfString1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (arrayOfString1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              param1Parcel2 = null;
            } 
            arrayOfString2 = arrayOfString1.createStringArray();
            if (arrayOfString1.readInt() != 0)
              bool1 = true; 
            onPackagesAvailable((UserHandle)param1Parcel2, arrayOfString2, bool1);
            return true;
          case 3:
            arrayOfString1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (arrayOfString1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)arrayOfString1);
            } else {
              param1Parcel2 = null;
            } 
            str1 = arrayOfString1.readString();
            onPackageChanged((UserHandle)param1Parcel2, str1);
            return true;
          case 2:
            str1.enforceInterface("android.content.pm.IOnAppsChangedListener");
            if (str1.readInt() != 0) {
              UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              param1Parcel2 = null;
            } 
            str1 = str1.readString();
            onPackageAdded((UserHandle)param1Parcel2, str1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.content.pm.IOnAppsChangedListener");
        if (str1.readInt() != 0) {
          UserHandle userHandle = (UserHandle)UserHandle.CREATOR.createFromParcel((Parcel)str1);
        } else {
          param1Parcel2 = null;
        } 
        String str1 = str1.readString();
        onPackageRemoved((UserHandle)param1Parcel2, str1);
        return true;
      } 
      param1Parcel2.writeString("android.content.pm.IOnAppsChangedListener");
      return true;
    }
    
    private static class Proxy implements IOnAppsChangedListener {
      public static IOnAppsChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IOnAppsChangedListener";
      }
      
      public void onPackageRemoved(UserHandle param2UserHandle, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackageRemoved(param2UserHandle, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackageAdded(UserHandle param2UserHandle, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackageAdded(param2UserHandle, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackageChanged(UserHandle param2UserHandle, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackageChanged(param2UserHandle, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackagesAvailable(UserHandle param2UserHandle, String[] param2ArrayOfString, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          boolean bool = false;
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel, null, 1);
          if (!bool1 && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackagesAvailable(param2UserHandle, param2ArrayOfString, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackagesUnavailable(UserHandle param2UserHandle, String[] param2ArrayOfString, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          boolean bool = false;
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel, null, 1);
          if (!bool1 && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackagesUnavailable(param2UserHandle, param2ArrayOfString, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackagesSuspended(UserHandle param2UserHandle, String[] param2ArrayOfString, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackagesSuspended(param2UserHandle, param2ArrayOfString, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPackagesUnsuspended(UserHandle param2UserHandle, String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(7, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onPackagesUnsuspended(param2UserHandle, param2ArrayOfString);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onShortcutChanged(UserHandle param2UserHandle, String param2String, ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IOnAppsChangedListener");
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeString(param2String);
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, null, 1);
          if (!bool && IOnAppsChangedListener.Stub.getDefaultImpl() != null) {
            IOnAppsChangedListener.Stub.getDefaultImpl().onShortcutChanged(param2UserHandle, param2String, param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnAppsChangedListener param1IOnAppsChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnAppsChangedListener != null) {
          Proxy.sDefaultImpl = param1IOnAppsChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnAppsChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
