package android.content.pm;

import android.annotation.SystemApi;
import android.graphics.drawable.Drawable;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class InstantAppInfo implements Parcelable {
  public InstantAppInfo(ApplicationInfo paramApplicationInfo, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    this.mApplicationInfo = paramApplicationInfo;
    this.mPackageName = null;
    this.mLabelText = null;
    this.mRequestedPermissions = paramArrayOfString1;
    this.mGrantedPermissions = paramArrayOfString2;
  }
  
  public InstantAppInfo(String paramString, CharSequence paramCharSequence, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    this.mApplicationInfo = null;
    this.mPackageName = paramString;
    this.mLabelText = paramCharSequence;
    this.mRequestedPermissions = paramArrayOfString1;
    this.mGrantedPermissions = paramArrayOfString2;
  }
  
  private InstantAppInfo(Parcel paramParcel) {
    this.mPackageName = paramParcel.readString();
    this.mLabelText = paramParcel.readCharSequence();
    this.mRequestedPermissions = paramParcel.readStringArray();
    this.mGrantedPermissions = paramParcel.createStringArray();
    this.mApplicationInfo = (ApplicationInfo)paramParcel.readParcelable(null);
  }
  
  public ApplicationInfo getApplicationInfo() {
    return this.mApplicationInfo;
  }
  
  public String getPackageName() {
    ApplicationInfo applicationInfo = this.mApplicationInfo;
    if (applicationInfo != null)
      return applicationInfo.packageName; 
    return this.mPackageName;
  }
  
  public CharSequence loadLabel(PackageManager paramPackageManager) {
    ApplicationInfo applicationInfo = this.mApplicationInfo;
    if (applicationInfo != null)
      return applicationInfo.loadLabel(paramPackageManager); 
    return this.mLabelText;
  }
  
  public Drawable loadIcon(PackageManager paramPackageManager) {
    ApplicationInfo applicationInfo = this.mApplicationInfo;
    if (applicationInfo != null)
      return applicationInfo.loadIcon(paramPackageManager); 
    return paramPackageManager.getInstantAppIcon(this.mPackageName);
  }
  
  public String[] getRequestedPermissions() {
    return this.mRequestedPermissions;
  }
  
  public String[] getGrantedPermissions() {
    return this.mGrantedPermissions;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeCharSequence(this.mLabelText);
    paramParcel.writeStringArray(this.mRequestedPermissions);
    paramParcel.writeStringArray(this.mGrantedPermissions);
    paramParcel.writeParcelable(this.mApplicationInfo, paramInt);
  }
  
  public static final Parcelable.Creator<InstantAppInfo> CREATOR = new Parcelable.Creator<InstantAppInfo>() {
      public InstantAppInfo createFromParcel(Parcel param1Parcel) {
        return new InstantAppInfo(param1Parcel);
      }
      
      public InstantAppInfo[] newArray(int param1Int) {
        return new InstantAppInfo[0];
      }
    };
  
  private final ApplicationInfo mApplicationInfo;
  
  private final String[] mGrantedPermissions;
  
  private final CharSequence mLabelText;
  
  private final String mPackageName;
  
  private final String[] mRequestedPermissions;
}
