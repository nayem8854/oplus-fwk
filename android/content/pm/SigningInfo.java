package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;

public final class SigningInfo implements Parcelable {
  public SigningInfo() {
    this.mSigningDetails = PackageParser.SigningDetails.UNKNOWN;
  }
  
  public SigningInfo(PackageParser.SigningDetails paramSigningDetails) {
    this.mSigningDetails = new PackageParser.SigningDetails(paramSigningDetails);
  }
  
  public SigningInfo(SigningInfo paramSigningInfo) {
    this.mSigningDetails = new PackageParser.SigningDetails(paramSigningInfo.mSigningDetails);
  }
  
  private SigningInfo(Parcel paramParcel) {
    this.mSigningDetails = (PackageParser.SigningDetails)PackageParser.SigningDetails.CREATOR.createFromParcel(paramParcel);
  }
  
  public boolean hasMultipleSigners() {
    Signature[] arrayOfSignature = this.mSigningDetails.signatures;
    boolean bool = true;
    if (arrayOfSignature == null || this.mSigningDetails.signatures.length <= 1)
      bool = false; 
    return bool;
  }
  
  public boolean hasPastSigningCertificates() {
    boolean bool;
    if (this.mSigningDetails.signatures != null && this.mSigningDetails.pastSigningCertificates != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Signature[] getSigningCertificateHistory() {
    if (hasMultipleSigners())
      return null; 
    if (!hasPastSigningCertificates())
      return this.mSigningDetails.signatures; 
    return this.mSigningDetails.pastSigningCertificates;
  }
  
  public Signature[] getApkContentsSigners() {
    return this.mSigningDetails.signatures;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mSigningDetails.writeToParcel(paramParcel, paramInt);
  }
  
  public static final Parcelable.Creator<SigningInfo> CREATOR = new Parcelable.Creator<SigningInfo>() {
      public SigningInfo createFromParcel(Parcel param1Parcel) {
        return new SigningInfo(param1Parcel);
      }
      
      public SigningInfo[] newArray(int param1Int) {
        return new SigningInfo[param1Int];
      }
    };
  
  private final PackageParser.SigningDetails mSigningDetails;
}
