package android.content.pm;

import android.annotation.SystemApi;

@SystemApi
public final class InstallationFile {
  private final InstallationFileParcel mParcel;
  
  public InstallationFile(int paramInt, String paramString, long paramLong, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) {
    InstallationFileParcel installationFileParcel = new InstallationFileParcel();
    installationFileParcel.location = paramInt;
    this.mParcel.name = paramString;
    this.mParcel.size = paramLong;
    this.mParcel.metadata = paramArrayOfbyte1;
    this.mParcel.signature = paramArrayOfbyte2;
  }
  
  public int getLocation() {
    return this.mParcel.location;
  }
  
  public String getName() {
    return this.mParcel.name;
  }
  
  public long getLengthBytes() {
    return this.mParcel.size;
  }
  
  public byte[] getMetadata() {
    return this.mParcel.metadata;
  }
  
  public byte[] getSignature() {
    return this.mParcel.signature;
  }
  
  public InstallationFileParcel getData() {
    return this.mParcel;
  }
}
