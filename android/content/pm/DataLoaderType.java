package android.content.pm;

public @interface DataLoaderType {
  public static final int INCREMENTAL = 2;
  
  public static final int NONE = 0;
  
  public static final int STREAMING = 1;
}
