package android.content.pm;

public @interface InstallationFileLocation {
  public static final int DATA_APP = 0;
  
  public static final int MEDIA_DATA = 2;
  
  public static final int MEDIA_OBB = 1;
  
  public static final int UNKNOWN = -1;
}
