package android.content.pm;

import android.apex.ApexInfo;
import android.app.ActivityTaskManager;
import android.app.ActivityThread;
import android.app.ResourcesManager;
import android.content.ComponentName;
import android.content.IntentFilter;
import android.content.pm.permission.SplitPermissionInfoParcelable;
import android.content.pm.split.DefaultSplitAssetLoader;
import android.content.pm.split.SplitAssetDependencyLoader;
import android.content.pm.split.SplitDependencyLoader;
import android.content.res.ApkAssets;
import android.content.res.AssetManager;
import android.content.res.CompatibilityInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.os.storage.StorageManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.PackageUtils;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.TypedValue;
import android.util.apk.ApkSignatureVerifier;
import com.android.internal.R;
import com.android.internal.os.ClassLoaderFactory;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Constructor;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import libcore.io.IoUtils;
import libcore.util.EmptyArray;
import libcore.util.HexEncoding;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class PackageParser extends OplusBasePackageParser {
  public static final String ANDROID_MANIFEST_FILENAME = "AndroidManifest.xml";
  
  public static final String ANDROID_RESOURCES = "http://schemas.android.com/apk/res/android";
  
  public static final String APEX_FILE_EXTENSION = ".apex";
  
  public static final String APK_FILE_EXTENSION = ".apk";
  
  public static final Set<String> CHILD_PACKAGE_TAGS;
  
  public static final boolean DEBUG_BACKUP = false;
  
  public static final boolean DEBUG_JAR = false;
  
  public static final boolean DEBUG_PARSER = false;
  
  private static final int DEFAULT_MIN_SDK_VERSION = 1;
  
  public static final float DEFAULT_PRE_O_MAX_ASPECT_RATIO = 1.86F;
  
  public static final float DEFAULT_PRE_Q_MIN_ASPECT_RATIO = 1.333F;
  
  public static final float DEFAULT_PRE_Q_MIN_ASPECT_RATIO_WATCH = 1.0F;
  
  private static final int DEFAULT_TARGET_SDK_VERSION = 0;
  
  static {
    boolean bool;
  }
  
  public static final boolean LOG_PARSE_TIMINGS = Build.IS_DEBUGGABLE;
  
  public static final int LOG_PARSE_TIMINGS_THRESHOLD_MS = 100;
  
  public static final boolean LOG_UNSAFE_BROADCASTS = false;
  
  public static final String METADATA_ACTIVITY_WINDOW_LAYOUT_AFFINITY = "android.activity_window_layout_affinity";
  
  public static final String METADATA_MAX_ASPECT_RATIO = "android.max_aspect";
  
  public static final String METADATA_SUPPORTS_SIZE_CHANGES = "android.supports_size_changes";
  
  public static final String MNT_EXPAND = "/mnt/expand/";
  
  public static final boolean MULTI_PACKAGE_APK_ENABLED;
  
  public static final NewPermissionInfo[] NEW_PERMISSIONS;
  
  public static final int PARSE_CHATTY = -2147483648;
  
  public static final int PARSE_COLLECT_CERTIFICATES = 32;
  
  public static final int PARSE_DEFAULT_INSTALL_LOCATION = -1;
  
  public static final int PARSE_DEFAULT_TARGET_SANDBOX = 1;
  
  public static final int PARSE_ENFORCE_CODE = 64;
  
  public static final int PARSE_EXTERNAL_STORAGE = 8;
  
  public static final int PARSE_IGNORE_PROCESSES = 2;
  
  public static final int PARSE_IS_SYSTEM_DIR = 16;
  
  public static final int PARSE_MUST_BE_APK = 1;
  
  private static final String PROPERTY_CHILD_PACKAGES_ENABLED = "persist.sys.child_packages_enabled";
  
  private static final int RECREATE_ON_CONFIG_CHANGES_MASK = 3;
  
  public static final boolean RIGID_PARSER = false;
  
  public static final Set<String> SAFE_BROADCASTS;
  
  public static final String[] SDK_CODENAMES;
  
  public static final int SDK_VERSION;
  
  private static final String TAG = "PackageParser";
  
  public static final String TAG_ADOPT_PERMISSIONS = "adopt-permissions";
  
  public static final String TAG_APPLICATION = "application";
  
  public static final String TAG_ATTRIBUTION = "attribution";
  
  public static final String TAG_COMPATIBLE_SCREENS = "compatible-screens";
  
  public static final String TAG_EAT_COMMENT = "eat-comment";
  
  public static final String TAG_FEATURE_GROUP = "feature-group";
  
  public static final String TAG_INSTRUMENTATION = "instrumentation";
  
  public static final String TAG_KEY_SETS = "key-sets";
  
  public static final String TAG_MANIFEST = "manifest";
  
  public static final String TAG_ORIGINAL_PACKAGE = "original-package";
  
  public static final String TAG_OVERLAY = "overlay";
  
  public static final String TAG_PACKAGE = "package";
  
  public static final String TAG_PACKAGE_VERIFIER = "package-verifier";
  
  public static final String TAG_PERMISSION = "permission";
  
  public static final String TAG_PERMISSION_GROUP = "permission-group";
  
  public static final String TAG_PERMISSION_TREE = "permission-tree";
  
  public static final String TAG_PROFILEABLE = "profileable";
  
  public static final String TAG_PROTECTED_BROADCAST = "protected-broadcast";
  
  public static final String TAG_QUERIES = "queries";
  
  public static final String TAG_RESTRICT_UPDATE = "restrict-update";
  
  public static final String TAG_SUPPORTS_INPUT = "supports-input";
  
  public static final String TAG_SUPPORT_SCREENS = "supports-screens";
  
  public static final String TAG_USES_CONFIGURATION = "uses-configuration";
  
  public static final String TAG_USES_FEATURE = "uses-feature";
  
  public static final String TAG_USES_GL_TEXTURE = "uses-gl-texture";
  
  public static final String TAG_USES_PERMISSION = "uses-permission";
  
  public static final String TAG_USES_PERMISSION_SDK_23 = "uses-permission-sdk-23";
  
  public static final String TAG_USES_PERMISSION_SDK_M = "uses-permission-sdk-m";
  
  public static final String TAG_USES_SDK = "uses-sdk";
  
  public static final String TAG_USES_SPLIT = "uses-split";
  
  public static boolean sCompatibilityModeEnabled;
  
  public static final Comparator<String> sSplitNameComparator;
  
  public static boolean sUseRoundIcon;
  
  @Deprecated
  public String mArchiveSourcePath;
  
  private File mCacheDir;
  
  public Callback mCallback;
  
  private DisplayMetrics mMetrics;
  
  private boolean mOnlyCoreApps;
  
  static {
    if (Build.IS_DEBUGGABLE && 
      SystemProperties.getBoolean("persist.sys.child_packages_enabled", false)) {
      bool = true;
    } else {
      bool = false;
    } 
    MULTI_PACKAGE_APK_ENABLED = bool;
    ArraySet<String> arraySet = new ArraySet();
    arraySet.add("application");
    CHILD_PACKAGE_TAGS.add("compatible-screens");
    CHILD_PACKAGE_TAGS.add("eat-comment");
    CHILD_PACKAGE_TAGS.add("feature-group");
    CHILD_PACKAGE_TAGS.add("instrumentation");
    CHILD_PACKAGE_TAGS.add("supports-screens");
    CHILD_PACKAGE_TAGS.add("supports-input");
    CHILD_PACKAGE_TAGS.add("uses-configuration");
    CHILD_PACKAGE_TAGS.add("uses-feature");
    CHILD_PACKAGE_TAGS.add("uses-gl-texture");
    CHILD_PACKAGE_TAGS.add("uses-permission");
    CHILD_PACKAGE_TAGS.add("uses-permission-sdk-23");
    CHILD_PACKAGE_TAGS.add("uses-permission-sdk-m");
    CHILD_PACKAGE_TAGS.add("uses-sdk");
    SAFE_BROADCASTS = (Set<String>)(arraySet = new ArraySet());
    arraySet.add("android.intent.action.BOOT_COMPLETED");
    NEW_PERMISSIONS = new NewPermissionInfo[] { new NewPermissionInfo("android.permission.WRITE_EXTERNAL_STORAGE", 4, 0), new NewPermissionInfo("android.permission.READ_PHONE_STATE", 4, 0) };
    SDK_VERSION = Build.VERSION.SDK_INT;
    SDK_CODENAMES = Build.VERSION.ACTIVE_CODENAMES;
    sCompatibilityModeEnabled = true;
    sUseRoundIcon = false;
    sSplitNameComparator = new SplitNameComparator();
  }
  
  class NewPermissionInfo {
    public final int fileVersion;
    
    public final String name;
    
    public final int sdkVersion;
    
    public NewPermissionInfo(PackageParser this$0, int param1Int1, int param1Int2) {
      this.name = (String)this$0;
      this.sdkVersion = param1Int1;
      this.fileVersion = param1Int2;
    }
  }
  
  public int mParseError = 1;
  
  private ParsePackageItemArgs mParseInstrumentationArgs;
  
  public String[] mSeparateProcesses;
  
  class ParsePackageItemArgs {
    final int bannerRes;
    
    final int iconRes;
    
    final int labelRes;
    
    final int logoRes;
    
    final int nameRes;
    
    final String[] outError;
    
    final PackageParser.Package owner;
    
    final int roundIconRes;
    
    TypedArray sa;
    
    String tag;
    
    ParsePackageItemArgs(PackageParser this$0, String[] param1ArrayOfString, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6) {
      this.owner = (PackageParser.Package)this$0;
      this.outError = param1ArrayOfString;
      this.nameRes = param1Int1;
      this.labelRes = param1Int2;
      this.iconRes = param1Int3;
      this.logoRes = param1Int5;
      this.bannerRes = param1Int6;
      this.roundIconRes = param1Int4;
    }
  }
  
  class ParseComponentArgs extends ParsePackageItemArgs {
    final int descriptionRes;
    
    final int enabledRes;
    
    int flags;
    
    final int processRes;
    
    final String[] sepProcesses;
    
    public ParseComponentArgs(PackageParser this$0, String[] param1ArrayOfString1, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, String[] param1ArrayOfString2, int param1Int7, int param1Int8, int param1Int9) {
      super((PackageParser.Package)this$0, param1ArrayOfString1, param1Int1, param1Int2, param1Int3, param1Int4, param1Int5, param1Int6);
      this.sepProcesses = param1ArrayOfString2;
      this.processRes = param1Int7;
      this.descriptionRes = param1Int8;
      this.enabledRes = param1Int9;
    }
  }
  
  class PackageLite {
    public final String baseCodePath;
    
    public final int baseRevisionCode;
    
    public final String codePath;
    
    public final String[] configForSplit;
    
    public final boolean coreApp;
    
    public final boolean debuggable;
    
    public final boolean extractNativeLibs;
    
    public final int installLocation;
    
    public final boolean[] isFeatureSplits;
    
    public final boolean isolatedSplits;
    
    public final boolean multiArch;
    
    public final String packageName;
    
    public final String[] splitCodePaths;
    
    public final String[] splitNames;
    
    public final int[] splitRevisionCodes;
    
    public final boolean use32bitAbi;
    
    public final String[] usesSplitNames;
    
    public final VerifierInfo[] verifiers;
    
    public final int versionCode;
    
    public final int versionCodeMajor;
    
    public PackageLite(PackageParser this$0, PackageParser.ApkLite param1ApkLite, String[] param1ArrayOfString1, boolean[] param1ArrayOfboolean, String[] param1ArrayOfString2, String[] param1ArrayOfString3, String[] param1ArrayOfString4, int[] param1ArrayOfint) {
      this.packageName = param1ApkLite.packageName;
      this.versionCode = param1ApkLite.versionCode;
      this.versionCodeMajor = param1ApkLite.versionCodeMajor;
      this.installLocation = param1ApkLite.installLocation;
      this.verifiers = param1ApkLite.verifiers;
      this.splitNames = param1ArrayOfString1;
      this.isFeatureSplits = param1ArrayOfboolean;
      this.usesSplitNames = param1ArrayOfString2;
      this.configForSplit = param1ArrayOfString3;
      this.codePath = (String)this$0;
      this.baseCodePath = param1ApkLite.codePath;
      this.splitCodePaths = param1ArrayOfString4;
      this.baseRevisionCode = param1ApkLite.revisionCode;
      this.splitRevisionCodes = param1ArrayOfint;
      this.coreApp = param1ApkLite.coreApp;
      this.debuggable = param1ApkLite.debuggable;
      this.multiArch = param1ApkLite.multiArch;
      this.use32bitAbi = param1ApkLite.use32bitAbi;
      this.extractNativeLibs = param1ApkLite.extractNativeLibs;
      this.isolatedSplits = param1ApkLite.isolatedSplits;
    }
    
    public List<String> getAllCodePaths() {
      ArrayList<String> arrayList = new ArrayList();
      arrayList.add(this.baseCodePath);
      if (!ArrayUtils.isEmpty((Object[])this.splitCodePaths))
        Collections.addAll(arrayList, this.splitCodePaths); 
      return arrayList;
    }
  }
  
  class ApkLite {
    public final String codePath;
    
    public final String configForSplit;
    
    public final boolean coreApp;
    
    public final boolean debuggable;
    
    public final boolean extractNativeLibs;
    
    public final int installLocation;
    
    public boolean isFeatureSplit;
    
    public final boolean isSplitRequired;
    
    public final boolean isolatedSplits;
    
    public final int minSdkVersion;
    
    public final boolean multiArch;
    
    public final boolean overlayIsStatic;
    
    public final int overlayPriority;
    
    public final String packageName;
    
    public final boolean profilableByShell;
    
    public final int revisionCode;
    
    public final PackageParser.SigningDetails signingDetails;
    
    public final String splitName;
    
    public final String targetPackageName;
    
    public final int targetSdkVersion;
    
    public final boolean use32bitAbi;
    
    public final boolean useEmbeddedDex;
    
    public final String usesSplitName;
    
    public final VerifierInfo[] verifiers;
    
    public final int versionCode;
    
    public final int versionCodeMajor;
    
    public ApkLite(PackageParser this$0, String param1String1, String param1String2, boolean param1Boolean1, String param1String3, String param1String4, boolean param1Boolean2, int param1Int1, int param1Int2, int param1Int3, int param1Int4, List<VerifierInfo> param1List, PackageParser.SigningDetails param1SigningDetails, boolean param1Boolean3, boolean param1Boolean4, boolean param1Boolean5, boolean param1Boolean6, boolean param1Boolean7, boolean param1Boolean8, boolean param1Boolean9, boolean param1Boolean10, String param1String5, boolean param1Boolean11, int param1Int5, int param1Int6, int param1Int7) {
      this.codePath = (String)this$0;
      this.packageName = param1String1;
      this.splitName = param1String2;
      this.isFeatureSplit = param1Boolean1;
      this.configForSplit = param1String3;
      this.usesSplitName = param1String4;
      this.versionCode = param1Int1;
      this.versionCodeMajor = param1Int2;
      this.revisionCode = param1Int3;
      this.installLocation = param1Int4;
      this.signingDetails = param1SigningDetails;
      this.verifiers = param1List.<VerifierInfo>toArray(new VerifierInfo[param1List.size()]);
      this.coreApp = param1Boolean3;
      this.debuggable = param1Boolean4;
      this.profilableByShell = param1Boolean5;
      this.multiArch = param1Boolean6;
      this.use32bitAbi = param1Boolean7;
      this.useEmbeddedDex = param1Boolean8;
      this.extractNativeLibs = param1Boolean9;
      this.isolatedSplits = param1Boolean10;
      this.isSplitRequired = param1Boolean2;
      this.targetPackageName = param1String5;
      this.overlayIsStatic = param1Boolean11;
      this.overlayPriority = param1Int5;
      this.minSdkVersion = param1Int6;
      this.targetSdkVersion = param1Int7;
    }
    
    public long getLongVersionCode() {
      return PackageInfo.composeLongVersionCode(this.versionCodeMajor, this.versionCode);
    }
  }
  
  class CachedComponentArgs {
    PackageParser.ParseComponentArgs mActivityAliasArgs;
    
    PackageParser.ParseComponentArgs mActivityArgs;
    
    PackageParser.ParseComponentArgs mProviderArgs;
    
    PackageParser.ParseComponentArgs mServiceArgs;
    
    private CachedComponentArgs() {}
  }
  
  public PackageParser() {
    DisplayMetrics displayMetrics = new DisplayMetrics();
    displayMetrics.setToDefaults();
  }
  
  public void setSeparateProcesses(String[] paramArrayOfString) {
    this.mSeparateProcesses = paramArrayOfString;
  }
  
  public void setOnlyCoreApps(boolean paramBoolean) {
    this.mOnlyCoreApps = paramBoolean;
  }
  
  public void setDisplayMetrics(DisplayMetrics paramDisplayMetrics) {
    this.mMetrics = paramDisplayMetrics;
  }
  
  public void setCacheDir(File paramFile) {
    this.mCacheDir = paramFile;
  }
  
  class CallbackImpl implements Callback {
    private final PackageManager mPm;
    
    public CallbackImpl(PackageParser this$0) {
      this.mPm = (PackageManager)this$0;
    }
    
    public boolean hasFeature(String param1String) {
      return this.mPm.hasSystemFeature(param1String);
    }
  }
  
  public void setCallback(Callback paramCallback) {
    this.mCallback = paramCallback;
  }
  
  public static final boolean isApkFile(File paramFile) {
    return isApkPath(paramFile.getName());
  }
  
  public static boolean isApkPath(String paramString) {
    return paramString.endsWith(".apk");
  }
  
  private static boolean checkUseInstalledOrHidden(int paramInt, PackageUserState paramPackageUserState, ApplicationInfo paramApplicationInfo) {
    boolean bool = false;
    if ((paramInt & 0x20000000) == 0 && !paramPackageUserState.installed && paramApplicationInfo != null && paramApplicationInfo.hiddenUntilInstalled)
      return false; 
    if (paramPackageUserState.isAvailable(paramInt) || (paramApplicationInfo != null && paramApplicationInfo.isSystemApp() && ((0x402000 & paramInt) != 0 || (0x20000000 & paramInt) != 0)))
      bool = true; 
    return bool;
  }
  
  public static boolean isAvailable(PackageUserState paramPackageUserState) {
    return checkUseInstalledOrHidden(0, paramPackageUserState, null);
  }
  
  public static PackageInfo generatePackageInfo(Package paramPackage, int[] paramArrayOfint, int paramInt, long paramLong1, long paramLong2, Set<String> paramSet, PackageUserState paramPackageUserState) {
    int i = UserHandle.getCallingUserId();
    return generatePackageInfo(paramPackage, paramArrayOfint, paramInt, paramLong1, paramLong2, paramSet, paramPackageUserState, i);
  }
  
  public static PackageInfo generatePackageInfo(Package paramPackage, int[] paramArrayOfint, int paramInt1, long paramLong1, long paramLong2, Set<String> paramSet, PackageUserState paramPackageUserState, int paramInt2) {
    return generatePackageInfo(paramPackage, null, paramArrayOfint, paramInt1, paramLong1, paramLong2, paramSet, paramPackageUserState, paramInt2);
  }
  
  public static PackageInfo generatePackageInfo(Package paramPackage, ApexInfo paramApexInfo, int paramInt) {
    int[] arrayOfInt = EmptyArray.INT;
    Set<?> set = Collections.emptySet();
    PackageUserState packageUserState = new PackageUserState();
    int i = UserHandle.getCallingUserId();
    return generatePackageInfo(paramPackage, paramApexInfo, arrayOfInt, paramInt, 0L, 0L, (Set)set, packageUserState, i);
  }
  
  private static PackageInfo generatePackageInfo(Package paramPackage, ApexInfo paramApexInfo, int[] paramArrayOfint, int paramInt1, long paramLong1, long paramLong2, Set<String> paramSet, PackageUserState paramPackageUserState, int paramInt2) {
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramPackage.applicationInfo) || !paramPackage.isMatch(paramInt1))
      return null; 
    PackageInfo packageInfo = new PackageInfo();
    packageInfo.packageName = paramPackage.packageName;
    packageInfo.splitNames = paramPackage.splitNames;
    packageInfo.versionCode = paramPackage.mVersionCode;
    packageInfo.versionCodeMajor = paramPackage.mVersionCodeMajor;
    packageInfo.baseRevisionCode = paramPackage.baseRevisionCode;
    packageInfo.splitRevisionCodes = paramPackage.splitRevisionCodes;
    packageInfo.versionName = paramPackage.mVersionName;
    packageInfo.sharedUserId = paramPackage.mSharedUserId;
    packageInfo.sharedUserLabel = paramPackage.mSharedUserLabel;
    packageInfo.applicationInfo = generateApplicationInfo(paramPackage, paramInt1, paramPackageUserState, paramInt2);
    packageInfo.installLocation = paramPackage.installLocation;
    packageInfo.isStub = paramPackage.isStub;
    packageInfo.coreApp = paramPackage.coreApp;
    if ((packageInfo.applicationInfo.flags & 0x1) != 0 || (packageInfo.applicationInfo.flags & 0x80) != 0)
      packageInfo.requiredForAllUsers = paramPackage.mRequiredForAllUsers; 
    packageInfo.restrictedAccountType = paramPackage.mRestrictedAccountType;
    packageInfo.requiredAccountType = paramPackage.mRequiredAccountType;
    packageInfo.overlayTarget = paramPackage.mOverlayTarget;
    packageInfo.targetOverlayableName = paramPackage.mOverlayTargetName;
    packageInfo.overlayCategory = paramPackage.mOverlayCategory;
    packageInfo.overlayPriority = paramPackage.mOverlayPriority;
    packageInfo.mOverlayIsStatic = paramPackage.mOverlayIsStatic;
    packageInfo.compileSdkVersion = paramPackage.mCompileSdkVersion;
    packageInfo.compileSdkVersionCodename = paramPackage.mCompileSdkVersionCodename;
    packageInfo.firstInstallTime = paramLong1;
    packageInfo.lastUpdateTime = paramLong2;
    if ((paramInt1 & 0x100) != 0)
      packageInfo.gids = paramArrayOfint; 
    if ((paramInt1 & 0x4000) != 0) {
      int i;
      if (paramPackage.configPreferences != null) {
        i = paramPackage.configPreferences.size();
      } else {
        i = 0;
      } 
      if (i) {
        packageInfo.configPreferences = new ConfigurationInfo[i];
        paramPackage.configPreferences.toArray(packageInfo.configPreferences);
      } 
      if (paramPackage.reqFeatures != null) {
        i = paramPackage.reqFeatures.size();
      } else {
        i = 0;
      } 
      if (i > 0) {
        packageInfo.reqFeatures = new FeatureInfo[i];
        paramPackage.reqFeatures.toArray(packageInfo.reqFeatures);
      } 
      if (paramPackage.featureGroups != null) {
        i = paramPackage.featureGroups.size();
      } else {
        i = 0;
      } 
      if (i > 0) {
        packageInfo.featureGroups = new FeatureGroupInfo[i];
        paramPackage.featureGroups.toArray(packageInfo.featureGroups);
      } 
    } 
    if ((paramInt1 & 0x1) != 0) {
      int i = paramPackage.activities.size();
      if (i > 0) {
        int j = 0;
        ActivityInfo[] arrayOfActivityInfo = new ActivityInfo[i];
        for (byte b = 0; b < i; b++, j = k) {
          Activity activity = paramPackage.activities.get(b);
          int k = j;
          if (paramPackageUserState.isMatch(activity.info, paramInt1))
            if (PackageManager.APP_DETAILS_ACTIVITY_CLASS_NAME.equals(activity.className)) {
              k = j;
            } else {
              arrayOfActivityInfo[j] = generateActivityInfo(activity, paramInt1, paramPackageUserState, paramInt2);
              k = j + 1;
            }  
        } 
        packageInfo.activities = (ActivityInfo[])ArrayUtils.trimToSize((Object[])arrayOfActivityInfo, j);
      } 
    } 
    if ((paramInt1 & 0x2) != 0) {
      int i = paramPackage.receivers.size();
      if (i > 0) {
        int j = 0;
        ActivityInfo[] arrayOfActivityInfo = new ActivityInfo[i];
        for (byte b = 0; b < i; b++, j = k) {
          Activity activity = paramPackage.receivers.get(b);
          int k = j;
          if (paramPackageUserState.isMatch(activity.info, paramInt1)) {
            arrayOfActivityInfo[j] = generateActivityInfo(activity, paramInt1, paramPackageUserState, paramInt2);
            k = j + 1;
          } 
        } 
        packageInfo.receivers = (ActivityInfo[])ArrayUtils.trimToSize((Object[])arrayOfActivityInfo, j);
      } 
    } 
    if ((paramInt1 & 0x4) != 0) {
      int i = paramPackage.services.size();
      if (i > 0) {
        int j = 0;
        ServiceInfo[] arrayOfServiceInfo = new ServiceInfo[i];
        for (byte b = 0; b < i; b++, j = k) {
          Service service = paramPackage.services.get(b);
          int k = j;
          if (paramPackageUserState.isMatch(service.info, paramInt1)) {
            arrayOfServiceInfo[j] = generateServiceInfo(service, paramInt1, paramPackageUserState, paramInt2);
            k = j + 1;
          } 
        } 
        packageInfo.services = (ServiceInfo[])ArrayUtils.trimToSize((Object[])arrayOfServiceInfo, j);
      } 
    } 
    if ((paramInt1 & 0x8) != 0) {
      int i = paramPackage.providers.size();
      if (i > 0) {
        int j = 0;
        ProviderInfo[] arrayOfProviderInfo = new ProviderInfo[i];
        for (byte b = 0; b < i; b++, j = k) {
          Provider provider = paramPackage.providers.get(b);
          int k = j;
          if (paramPackageUserState.isMatch(provider.info, paramInt1)) {
            arrayOfProviderInfo[j] = generateProviderInfo(provider, paramInt1, paramPackageUserState, paramInt2);
            k = j + 1;
          } 
        } 
        packageInfo.providers = (ProviderInfo[])ArrayUtils.trimToSize((Object[])arrayOfProviderInfo, j);
      } 
    } 
    if ((paramInt1 & 0x10) != 0) {
      int i = paramPackage.instrumentation.size();
      if (i > 0) {
        packageInfo.instrumentation = new InstrumentationInfo[i];
        for (paramInt2 = 0; paramInt2 < i; paramInt2++) {
          InstrumentationInfo[] arrayOfInstrumentationInfo = packageInfo.instrumentation;
          ArrayList<Instrumentation> arrayList = paramPackage.instrumentation;
          Instrumentation instrumentation = arrayList.get(paramInt2);
          arrayOfInstrumentationInfo[paramInt2] = generateInstrumentationInfo(instrumentation, paramInt1);
        } 
      } 
    } 
    if ((paramInt1 & 0x1000) != 0) {
      int i = paramPackage.permissions.size();
      if (i > 0) {
        packageInfo.permissions = new PermissionInfo[i];
        for (paramInt2 = 0; paramInt2 < i; paramInt2++)
          packageInfo.permissions[paramInt2] = generatePermissionInfo(paramPackage.permissions.get(paramInt2), paramInt1); 
      } 
      i = paramPackage.requestedPermissions.size();
      if (i > 0) {
        packageInfo.requestedPermissions = new String[i];
        packageInfo.requestedPermissionsFlags = new int[i];
        for (paramInt2 = 0; paramInt2 < i; paramInt2++) {
          String str = paramPackage.requestedPermissions.get(paramInt2);
          packageInfo.requestedPermissions[paramInt2] = str;
          int[] arrayOfInt = packageInfo.requestedPermissionsFlags;
          arrayOfInt[paramInt2] = arrayOfInt[paramInt2] | 0x1;
          if (paramSet != null && paramSet.contains(str)) {
            int[] arrayOfInt1 = packageInfo.requestedPermissionsFlags;
            arrayOfInt1[paramInt2] = arrayOfInt1[paramInt2] | 0x2;
          } 
        } 
      } 
    } 
    if (paramApexInfo != null) {
      File file = new File(paramApexInfo.modulePath);
      packageInfo.applicationInfo.sourceDir = file.getPath();
      packageInfo.applicationInfo.publicSourceDir = file.getPath();
      if (paramApexInfo.isFactory) {
        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        applicationInfo.flags |= 0x1;
      } else {
        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        applicationInfo.flags &= 0xFFFFFFFE;
      } 
      if (paramApexInfo.isActive) {
        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        applicationInfo.flags |= 0x800000;
      } else {
        ApplicationInfo applicationInfo = packageInfo.applicationInfo;
        applicationInfo.flags &= 0xFF7FFFFF;
      } 
      packageInfo.isApex = true;
    } 
    if ((paramInt1 & 0x40) != 0)
      if (paramPackage.mSigningDetails.hasPastSigningCertificates()) {
        packageInfo.signatures = new Signature[1];
        packageInfo.signatures[0] = paramPackage.mSigningDetails.pastSigningCertificates[0];
      } else if (paramPackage.mSigningDetails.hasSignatures()) {
        paramInt2 = paramPackage.mSigningDetails.signatures.length;
        packageInfo.signatures = new Signature[paramInt2];
        System.arraycopy(paramPackage.mSigningDetails.signatures, 0, packageInfo.signatures, 0, paramInt2);
      }  
    if ((0x8000000 & paramInt1) != 0)
      if (paramPackage.mSigningDetails != SigningDetails.UNKNOWN) {
        packageInfo.signingInfo = new SigningInfo(paramPackage.mSigningDetails);
      } else {
        packageInfo.signingInfo = null;
      }  
    return packageInfo;
  }
  
  class SplitNameComparator implements Comparator<String> {
    private SplitNameComparator() {}
    
    public int compare(String param1String1, String param1String2) {
      if (param1String1 == null)
        return -1; 
      if (param1String2 == null)
        return 1; 
      return param1String1.compareTo(param1String2);
    }
  }
  
  public static PackageLite parsePackageLite(File paramFile, int paramInt) throws PackageParserException {
    if (paramFile.isDirectory())
      return parseClusterPackageLite(paramFile, paramInt); 
    return parseMonolithicPackageLite(paramFile, paramInt);
  }
  
  private static PackageLite parseMonolithicPackageLite(File paramFile, int paramInt) throws PackageParserException {
    Trace.traceBegin(262144L, "parseApkLite");
    ApkLite apkLite = parseApkLite(paramFile, paramInt);
    String str = paramFile.getAbsolutePath();
    Trace.traceEnd(262144L);
    return new PackageLite(str, apkLite, null, null, null, null, null, null);
  }
  
  static PackageLite parseClusterPackageLite(File paramFile, int paramInt) throws PackageParserException {
    File[] arrayOfFile = paramFile.listFiles();
    if (!ArrayUtils.isEmpty((Object[])arrayOfFile)) {
      StringBuilder stringBuilder1;
      String str;
      int i = arrayOfFile.length;
      byte b = 0;
      if (i == 1 && arrayOfFile[0].isDirectory())
        return parseClusterPackageLite(arrayOfFile[0], paramInt); 
      ApkLite apkLite1 = null;
      i = 0;
      Trace.traceBegin(262144L, "parseApkLite");
      ArrayMap arrayMap = new ArrayMap();
      for (int j = arrayOfFile.length; b < j; ) {
        String str2;
        File file = arrayOfFile[b];
        ApkLite apkLite = apkLite1;
        int k = i;
        if (isApkFile(file)) {
          String str3;
          apkLite = parseApkLite(file, paramInt);
          if (apkLite1 == null) {
            str3 = apkLite.packageName;
            i = apkLite.versionCode;
          } else if (str3.equals(apkLite.packageName)) {
            if (i != apkLite.versionCode) {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Inconsistent version ");
              stringBuilder1.append(apkLite.versionCode);
              stringBuilder1.append(" in ");
              stringBuilder1.append(file);
              stringBuilder1.append("; expected ");
              stringBuilder1.append(i);
              throw new PackageParserException(-101, stringBuilder1.toString());
            } 
          } else {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Inconsistent package ");
            stringBuilder1.append(apkLite.packageName);
            stringBuilder1.append(" in ");
            stringBuilder1.append(file);
            stringBuilder1.append("; expected ");
            stringBuilder1.append(str3);
            throw new PackageParserException(-101, stringBuilder1.toString());
          } 
          if (arrayMap.put(apkLite.splitName, apkLite) == null) {
            str2 = str3;
            k = i;
          } else {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Split name ");
            stringBuilder1.append(((ApkLite)str2).splitName);
            stringBuilder1.append(" defined more than once; most recent was ");
            stringBuilder1.append(file);
            throw new PackageParserException(-101, stringBuilder1.toString());
          } 
        } 
        b++;
        String str1 = str2;
        i = k;
      } 
      Trace.traceEnd(262144L);
      ApkLite apkLite2 = (ApkLite)arrayMap.remove(null);
      if (apkLite2 != null) {
        String[] arrayOfString1, arrayOfString2, arrayOfString3;
        int[] arrayOfInt;
        String[] arrayOfString4;
        i = arrayMap.size();
        arrayOfFile = null;
        boolean[] arrayOfBoolean = null;
        apkLite1 = null;
        if (i > 0) {
          arrayOfString1 = new String[i];
          arrayOfBoolean = new boolean[i];
          arrayOfString2 = new String[i];
          arrayOfString3 = new String[i];
          arrayOfString4 = new String[i];
          arrayOfInt = new int[i];
          arrayOfString1 = (String[])arrayMap.keySet().toArray((Object[])arrayOfString1);
          Arrays.sort(arrayOfString1, sSplitNameComparator);
          for (paramInt = 0; paramInt < i; paramInt++) {
            ApkLite apkLite = (ApkLite)arrayMap.get(arrayOfString1[paramInt]);
            arrayOfString2[paramInt] = apkLite.usesSplitName;
            arrayOfBoolean[paramInt] = apkLite.isFeatureSplit;
            arrayOfString3[paramInt] = apkLite.configForSplit;
            arrayOfString4[paramInt] = apkLite.codePath;
            arrayOfInt[paramInt] = apkLite.revisionCode;
          } 
        } else {
          arrayOfString3 = null;
          arrayOfString4 = null;
          arrayOfInt = null;
        } 
        str = stringBuilder1.getAbsolutePath();
        return new PackageLite(str, apkLite2, arrayOfString1, arrayOfBoolean, arrayOfString2, arrayOfString3, arrayOfString4, arrayOfInt);
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Missing base APK in ");
      stringBuilder2.append(str);
      throw new PackageParserException(-101, stringBuilder2.toString());
    } 
    throw new PackageParserException(-100, "No packages found in split");
  }
  
  public Package parsePackage(File paramFile, int paramInt, boolean paramBoolean) throws PackageParserException {
    if (paramFile.isDirectory())
      return parseClusterPackage(paramFile, paramInt); 
    return parseMonolithicPackage(paramFile, paramInt);
  }
  
  public Package parsePackage(File paramFile, int paramInt) throws PackageParserException {
    return parsePackage(paramFile, paramInt, false);
  }
  
  private Package parseClusterPackage(File paramFile, int paramInt) throws PackageParserException {
    DefaultSplitAssetLoader defaultSplitAssetLoader;
    PackageLite packageLite = parseClusterPackageLite(paramFile, 0);
    if (!this.mOnlyCoreApps || packageLite.coreApp) {
      null = null;
      if (packageLite.isolatedSplits && !ArrayUtils.isEmpty((Object[])packageLite.splitNames)) {
        try {
          null = SplitAssetDependencyLoader.createDependenciesFromPackage(packageLite);
          SplitAssetDependencyLoader splitAssetDependencyLoader = new SplitAssetDependencyLoader(packageLite, null, paramInt);
        } catch (android.content.pm.split.SplitDependencyLoader.IllegalDependencyException illegalDependencyException) {
          throw new PackageParserException(-101, illegalDependencyException.getMessage());
        } 
      } else {
        defaultSplitAssetLoader = new DefaultSplitAssetLoader(packageLite, paramInt);
      } 
      try {
        AssetManager assetManager = defaultSplitAssetLoader.getBaseAssetManager();
        File file = new File();
        this(packageLite.baseCodePath);
        Package package_ = parseBaseApk(file, assetManager, paramInt);
        if (package_ != null) {
          if (!ArrayUtils.isEmpty((Object[])packageLite.splitNames)) {
            int i = packageLite.splitNames.length;
            package_.splitNames = packageLite.splitNames;
            package_.splitCodePaths = packageLite.splitCodePaths;
            package_.splitRevisionCodes = packageLite.splitRevisionCodes;
            package_.splitFlags = new int[i];
            package_.splitPrivateFlags = new int[i];
            package_.applicationInfo.splitNames = package_.splitNames;
            package_.applicationInfo.splitDependencies = null;
            package_.applicationInfo.splitClassLoaderNames = new String[i];
            for (byte b = 0; b < i; b++) {
              AssetManager assetManager1 = defaultSplitAssetLoader.getSplitAssetManager(b);
              parseSplitApk(package_, b, assetManager1, paramInt);
            } 
          } 
          package_.setCodePath(packageLite.codePath);
          package_.setUse32bitAbi(packageLite.use32bitAbi);
          return package_;
        } 
        PackageParserException packageParserException = new PackageParserException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to parse base APK: ");
        stringBuilder1.append(file);
        this(-100, stringBuilder1.toString());
        throw packageParserException;
      } finally {
        IoUtils.closeQuietly(defaultSplitAssetLoader);
      } 
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a coreApp: ");
    stringBuilder.append(defaultSplitAssetLoader);
    throw new PackageParserException(-108, stringBuilder.toString());
  }
  
  public Package parseMonolithicPackage(File paramFile, int paramInt) throws PackageParserException {
    PackageLite packageLite = parseMonolithicPackageLite(paramFile, paramInt);
    if (!this.mOnlyCoreApps || 
      packageLite.coreApp) {
      DefaultSplitAssetLoader defaultSplitAssetLoader = new DefaultSplitAssetLoader(packageLite, paramInt);
      try {
        Package package_ = parseBaseApk(paramFile, defaultSplitAssetLoader.getBaseAssetManager(), paramInt);
        package_.setCodePath(paramFile.getCanonicalPath());
        package_.setUse32bitAbi(packageLite.use32bitAbi);
        IoUtils.closeQuietly(defaultSplitAssetLoader);
        return package_;
      } catch (IOException iOException) {
        PackageParserException packageParserException = new PackageParserException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to get path: ");
        stringBuilder1.append(paramFile);
        this(-102, stringBuilder1.toString(), iOException);
        throw packageParserException;
      } finally {}
      IoUtils.closeQuietly(defaultSplitAssetLoader);
      throw paramFile;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Not a coreApp: ");
    stringBuilder.append(paramFile);
    throw new PackageParserException(-108, stringBuilder.toString());
  }
  
  private Package parseBaseApk(File paramFile, AssetManager paramAssetManager, int paramInt) throws PackageParserException {
    String str2, str1 = paramFile.getAbsolutePath();
    if (str1.startsWith("/mnt/expand/")) {
      int i = str1.indexOf('/', "/mnt/expand/".length());
      str2 = str1.substring("/mnt/expand/".length(), i);
    } else {
      str2 = null;
    } 
    this.mParseError = 1;
    this.mArchiveSourcePath = paramFile.getAbsolutePath();
    AssetManager assetManager = null;
    Resources resources = null;
    File file = null;
    paramFile = file;
    try {
      Exception exception;
      int i = paramAssetManager.findCookieForPath(str1);
      if (i != 0) {
        paramFile = file;
        XmlResourceParser xmlResourceParser = paramAssetManager.openXmlResourceParser(i, "AndroidManifest.xml");
        try {
          resources = new Resources();
          this(paramAssetManager, this.mMetrics, null);
          String[] arrayOfString = new String[1];
          Package package_ = parseBaseApk(str1, resources, xmlResourceParser, paramInt, arrayOfString);
          if (package_ != null) {
            package_.setVolumeUuid(str2);
            package_.setApplicationVolumeUuid(str2);
            package_.setBaseCodePath(str1);
            package_.setSigningDetails(SigningDetails.UNKNOWN);
            return package_;
          } 
          PackageParserException packageParserException = new PackageParserException();
          paramInt = this.mParseError;
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append(str1);
          stringBuilder.append(" (at ");
          stringBuilder.append(xmlResourceParser.getPositionDescription());
          stringBuilder.append("): ");
          stringBuilder.append(arrayOfString[0]);
          this(paramInt, stringBuilder.toString());
          throw packageParserException;
        } catch (PackageParserException packageParserException) {
        
        } catch (Exception exception1) {
          XmlResourceParser xmlResourceParser1 = xmlResourceParser;
        } finally {
          Exception exception1;
          paramAssetManager = null;
        } 
      } else {
        Exception exception1 = exception;
        PackageParserException packageParserException = new PackageParserException();
        exception1 = exception;
        StringBuilder stringBuilder = new StringBuilder();
        exception1 = exception;
        this();
        exception1 = exception;
        stringBuilder.append("Failed adding asset path: ");
        exception1 = exception;
        stringBuilder.append(str1);
        exception1 = exception;
        this(-101, stringBuilder.toString());
        exception1 = exception;
        throw packageParserException;
      } 
    } catch (PackageParserException packageParserException) {
      Resources resources1 = resources;
    } catch (Exception exception) {
      paramAssetManager = assetManager;
      AssetManager assetManager1 = paramAssetManager;
      PackageParserException packageParserException = new PackageParserException();
      assetManager1 = paramAssetManager;
      StringBuilder stringBuilder = new StringBuilder();
      assetManager1 = paramAssetManager;
      this();
      assetManager1 = paramAssetManager;
      stringBuilder.append("Failed to read manifest from ");
      assetManager1 = paramAssetManager;
      stringBuilder.append(str1);
      assetManager1 = paramAssetManager;
      this(-102, stringBuilder.toString(), exception);
      assetManager1 = paramAssetManager;
      throw packageParserException;
    } finally {}
    throw paramAssetManager;
  }
  
  private void parseSplitApk(Package paramPackage, int paramInt1, AssetManager paramAssetManager, int paramInt2) throws PackageParserException {
    Package package_1;
    StringBuilder stringBuilder;
    String str = paramPackage.splitCodePaths[paramInt1];
    this.mParseError = 1;
    this.mArchiveSourcePath = str;
    Package package_2 = null;
    Resources resources = null;
    Package package_3 = null;
    try {
      int i = paramAssetManager.findCookieForPath(str);
      if (i != 0) {
        XmlResourceParser xmlResourceParser = paramAssetManager.openXmlResourceParser(i, "AndroidManifest.xml");
      } else {
        PackageParserException packageParserException = new PackageParserException();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed adding asset path: ");
        stringBuilder1.append(str);
        this(-101, stringBuilder1.toString());
        throw packageParserException;
      } 
    } catch (PackageParserException null) {
    
    } catch (Exception exception) {
      paramPackage = package_3;
      Package package_ = paramPackage;
    } finally {
      StringBuilder stringBuilder1;
      paramPackage = null;
    } 
    throw exception;
  }
  
  private Package parseSplitApk(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt1, int paramInt2, String[] paramArrayOfString) throws XmlPullParserException, IOException, PackageParserException {
    parsePackageSplitNames(paramXmlResourceParser, paramXmlResourceParser);
    this.mParseInstrumentationArgs = null;
    boolean bool = false;
    int i = paramXmlResourceParser.getDepth();
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        if (j == 3 || j == 4)
          continue; 
        String str2 = paramXmlResourceParser.getName();
        if (str2.equals("application")) {
          if (bool) {
            Slog.w("PackageParser", "<manifest> has more than one <application>");
            XmlUtils.skipCurrentTag(paramXmlResourceParser);
            continue;
          } 
          bool = true;
          if (!parseSplitApplication(paramPackage, paramResources, paramXmlResourceParser, paramInt1, paramInt2, paramArrayOfString))
            return null; 
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown element under <manifest>: ");
        stringBuilder.append(paramXmlResourceParser.getName());
        stringBuilder.append(" at ");
        stringBuilder.append(this.mArchiveSourcePath);
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str1 = stringBuilder.toString();
        Slog.w("PackageParser", str1);
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        continue;
      } 
      break;
    } 
    if (!bool) {
      paramArrayOfString[0] = "<manifest> does not contain an <application>";
      this.mParseError = -109;
    } 
    return paramPackage;
  }
  
  public static ArraySet<PublicKey> toSigningKeys(Signature[] paramArrayOfSignature) throws CertificateException {
    ArraySet<PublicKey> arraySet = new ArraySet(paramArrayOfSignature.length);
    for (byte b = 0; b < paramArrayOfSignature.length; b++)
      arraySet.add(paramArrayOfSignature[b].getPublicKey()); 
    return arraySet;
  }
  
  public static void collectCertificates(Package paramPackage, boolean paramBoolean) throws PackageParserException {
    byte b1;
    collectCertificatesInternal(paramPackage, paramBoolean);
    if (paramPackage.childPackages != null) {
      b1 = paramPackage.childPackages.size();
    } else {
      b1 = 0;
    } 
    for (byte b2 = 0; b2 < b1; b2++) {
      Package package_ = paramPackage.childPackages.get(b2);
      package_.mSigningDetails = paramPackage.mSigningDetails;
    } 
  }
  
  private static void collectCertificatesInternal(Package paramPackage, boolean paramBoolean) throws PackageParserException {
    paramPackage.mSigningDetails = SigningDetails.UNKNOWN;
    Trace.traceBegin(262144L, "collectCertificates");
    try {
      File file = new File();
      this(paramPackage.baseCodePath);
      collectCertificates(paramPackage, file, paramBoolean);
      if (!ArrayUtils.isEmpty((Object[])paramPackage.splitCodePaths))
        for (byte b = 0; b < paramPackage.splitCodePaths.length; b++) {
          file = new File();
          this(paramPackage.splitCodePaths[b]);
          collectCertificates(paramPackage, file, paramBoolean);
        }  
      return;
    } finally {
      Trace.traceEnd(262144L);
    } 
  }
  
  private static void collectCertificates(Package paramPackage, File paramFile, boolean paramBoolean) throws PackageParserException {
    SigningDetails signingDetails;
    String str = paramFile.getAbsolutePath();
    int i = ApkSignatureVerifier.getMinimumSignatureSchemeVersionForTargetSdk(paramPackage.applicationInfo.targetSdkVersion);
    if (paramPackage.applicationInfo.isStaticSharedLibrary())
      i = 2; 
    if (paramBoolean) {
      signingDetails = ApkSignatureVerifier.unsafeGetCertsWithoutVerification(str, 1);
    } else {
      signingDetails = ApkSignatureVerifier.verify(str, i);
    } 
    if (paramPackage.mSigningDetails == SigningDetails.UNKNOWN) {
      paramPackage.mSigningDetails = signingDetails;
    } else if (!Signature.areExactMatch(paramPackage.mSigningDetails.signatures, signingDetails.signatures)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(" has mismatched certificates");
      throw new PackageParserException(-104, stringBuilder.toString());
    } 
  }
  
  private static AssetManager newConfiguredAssetManager() {
    AssetManager assetManager = new AssetManager();
    assetManager.setConfiguration(0, 0, null, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, Build.VERSION.RESOURCES_SDK_INT);
    return assetManager;
  }
  
  public static ApkLite parseApkLite(File paramFile, int paramInt) throws PackageParserException {
    return parseApkLiteInner(paramFile, null, null, paramInt);
  }
  
  public static ApkLite parseApkLite(FileDescriptor paramFileDescriptor, String paramString, int paramInt) throws PackageParserException {
    return parseApkLiteInner(null, paramFileDescriptor, paramString, paramInt);
  }
  
  private static ApkLite parseApkLiteInner(File paramFile, FileDescriptor paramFileDescriptor, String paramString, int paramInt) throws PackageParserException {
    if (paramFileDescriptor != null) {
      str = paramString;
    } else {
      str = paramFile.getAbsolutePath();
    } 
    Object object1 = null, object2 = null;
    ApkAssets apkAssets1 = null, apkAssets2 = null;
    boolean bool = false;
    if (paramFileDescriptor != null) {
      Object object = object2;
      ApkAssets apkAssets = apkAssets2;
      object4 = object1;
      apkAssets4 = apkAssets1;
      try {
        ApkAssets apkAssets5 = ApkAssets.loadFromFd(paramFileDescriptor, paramString, 0, null);
        object = object2;
        apkAssets = apkAssets5;
        object4 = object1;
        apkAssets4 = apkAssets5;
      } catch (IOException iOException) {
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        PackageParserException packageParserException1 = new PackageParserException();
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        StringBuilder stringBuilder1 = new StringBuilder();
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        this();
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        stringBuilder1.append("Failed to parse ");
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        stringBuilder1.append(str);
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        this(-100, stringBuilder1.toString());
        object = object2;
        apkAssets = apkAssets2;
        object4 = object1;
        apkAssets4 = apkAssets1;
        throw packageParserException1;
      } catch (XmlPullParserException|RuntimeException xmlPullParserException) {
      
      } finally {}
    } else {
      Object object = object2;
      ApkAssets apkAssets6 = apkAssets2;
      object4 = object1;
      apkAssets4 = apkAssets1;
      ApkAssets apkAssets5 = ApkAssets.loadFromPath(str);
      object = object2;
      apkAssets6 = apkAssets5;
      object4 = object1;
      apkAssets4 = apkAssets5;
    } 
    Object object3 = object4;
    ApkAssets apkAssets3 = apkAssets4;
    StringBuilder stringBuilder = new StringBuilder();
    object3 = object4;
    apkAssets3 = apkAssets4;
    this();
    String str;
    Object object4;
    ApkAssets apkAssets4;
    object3 = object4;
    apkAssets3 = apkAssets4;
    stringBuilder.append("Failed to parse ");
    object3 = object4;
    apkAssets3 = apkAssets4;
    stringBuilder.append(str);
    object3 = object4;
    apkAssets3 = apkAssets4;
    Slog.w("PackageParser", stringBuilder.toString(), (Throwable)paramFile);
    object3 = object4;
    apkAssets3 = apkAssets4;
    PackageParserException packageParserException = new PackageParserException();
    object3 = object4;
    apkAssets3 = apkAssets4;
    stringBuilder = new StringBuilder();
    object3 = object4;
    apkAssets3 = apkAssets4;
    this();
    object3 = object4;
    apkAssets3 = apkAssets4;
    stringBuilder.append("Failed to parse ");
    object3 = object4;
    apkAssets3 = apkAssets4;
    stringBuilder.append(str);
    object3 = object4;
    apkAssets3 = apkAssets4;
    this(-102, stringBuilder.toString(), (Throwable)paramFile);
    object3 = object4;
    apkAssets3 = apkAssets4;
    throw packageParserException;
  }
  
  public static String validateName(String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual length : ()I
    //   4: istore_3
    //   5: iconst_0
    //   6: istore #4
    //   8: iconst_1
    //   9: istore #5
    //   11: iconst_0
    //   12: istore #6
    //   14: iload #6
    //   16: iload_3
    //   17: if_icmpge -> 174
    //   20: aload_0
    //   21: iload #6
    //   23: invokevirtual charAt : (I)C
    //   26: istore #7
    //   28: iload #7
    //   30: bipush #97
    //   32: if_icmplt -> 42
    //   35: iload #7
    //   37: bipush #122
    //   39: if_icmple -> 56
    //   42: iload #7
    //   44: bipush #65
    //   46: if_icmplt -> 66
    //   49: iload #7
    //   51: bipush #90
    //   53: if_icmpgt -> 66
    //   56: iconst_0
    //   57: istore #8
    //   59: iload #4
    //   61: istore #9
    //   63: goto -> 124
    //   66: iload #5
    //   68: ifne -> 111
    //   71: iload #7
    //   73: bipush #48
    //   75: if_icmplt -> 93
    //   78: iload #4
    //   80: istore #9
    //   82: iload #5
    //   84: istore #8
    //   86: iload #7
    //   88: bipush #57
    //   90: if_icmple -> 124
    //   93: iload #7
    //   95: bipush #95
    //   97: if_icmpne -> 111
    //   100: iload #4
    //   102: istore #9
    //   104: iload #5
    //   106: istore #8
    //   108: goto -> 124
    //   111: iload #7
    //   113: bipush #46
    //   115: if_icmpne -> 138
    //   118: iconst_1
    //   119: istore #9
    //   121: iconst_1
    //   122: istore #8
    //   124: iinc #6, 1
    //   127: iload #9
    //   129: istore #4
    //   131: iload #8
    //   133: istore #5
    //   135: goto -> 14
    //   138: new java/lang/StringBuilder
    //   141: dup
    //   142: invokespecial <init> : ()V
    //   145: astore_0
    //   146: aload_0
    //   147: ldc_w 'bad character ''
    //   150: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   153: pop
    //   154: aload_0
    //   155: iload #7
    //   157: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload_0
    //   162: ldc_w '''
    //   165: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   168: pop
    //   169: aload_0
    //   170: invokevirtual toString : ()Ljava/lang/String;
    //   173: areturn
    //   174: iload_2
    //   175: ifeq -> 189
    //   178: aload_0
    //   179: invokestatic isValidExtFilename : (Ljava/lang/String;)Z
    //   182: ifne -> 189
    //   185: ldc_w 'Invalid filename'
    //   188: areturn
    //   189: iload #4
    //   191: ifne -> 208
    //   194: iload_1
    //   195: ifne -> 201
    //   198: goto -> 208
    //   201: ldc_w 'must have at least one '.' separator'
    //   204: astore_0
    //   205: goto -> 210
    //   208: aconst_null
    //   209: astore_0
    //   210: aload_0
    //   211: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1518	-> 0
    //   #1519	-> 5
    //   #1520	-> 8
    //   #1521	-> 11
    //   #1522	-> 20
    //   #1523	-> 28
    //   #1524	-> 56
    //   #1525	-> 59
    //   #1527	-> 66
    //   #1528	-> 71
    //   #1529	-> 100
    //   #1532	-> 111
    //   #1533	-> 118
    //   #1534	-> 121
    //   #1535	-> 124
    //   #1521	-> 124
    //   #1537	-> 138
    //   #1539	-> 174
    //   #1540	-> 185
    //   #1542	-> 189
    //   #1543	-> 201
    //   #1542	-> 210
  }
  
  @Deprecated
  public static Pair<String, String> parsePackageSplitNames(XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet) throws IOException, XmlPullParserException, PackageParserException {
    int i;
    while (true) {
      i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
    if (i == 2) {
      if (paramXmlPullParser.getName().equals("manifest")) {
        StringBuilder stringBuilder;
        String str3 = paramAttributeSet.getAttributeValue(null, "package");
        if (!"android".equals(str3)) {
          String str = OplusBasePackageParser.hookNameError(str3, validateName(str3, true, true));
          if (str != null) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Invalid manifest package: ");
            stringBuilder.append(str);
            throw new PackageParserException(-106, stringBuilder.toString());
          } 
        } 
        String str2 = stringBuilder.getAttributeValue(null, "split");
        String str1 = str2;
        if (str2 != null)
          if (str2.length() == 0) {
            str1 = null;
          } else {
            str1 = validateName(str2, false, false);
            if (str1 == null) {
              str1 = str2;
            } else {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Invalid manifest split: ");
              stringBuilder1.append(str1);
              throw new PackageParserException(-106, stringBuilder1.toString());
            } 
          }  
        str2 = str3.intern();
        if (str1 != null)
          str1 = str1.intern(); 
        return Pair.create(str2, str1);
      } 
      throw new PackageParserException(-108, "No <manifest> tag");
    } 
    throw new PackageParserException(-108, "No start tag found");
  }
  
  private static ApkLite parseApkLite(String paramString, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, SigningDetails paramSigningDetails) throws IOException, XmlPullParserException, PackageParserException {
    Pair<String, String> pair = parsePackageSplitNames(paramXmlPullParser, paramAttributeSet);
    byte b = -1;
    boolean bool1 = false;
    boolean bool2 = false;
    int i = 1;
    boolean bool3 = false;
    boolean bool4 = false;
    boolean bool5 = false;
    boolean bool6 = false;
    boolean bool7 = false;
    boolean bool8 = false;
    String str1 = null;
    byte b1;
    int j;
    for (b1 = 0, j = 0; j < paramAttributeSet.getAttributeCount(); j++, b = b2, bool1 = bool16, bool2 = bool17, bool3 = bool18, bool4 = bool19, bool6 = bool20, bool7 = bool21, str1 = str7) {
      byte b2;
      boolean bool16, bool17, bool18, bool19, bool20, bool21;
      String str7, str6 = paramAttributeSet.getAttributeName(j);
      if (str6.equals("installLocation")) {
        b2 = paramAttributeSet.getAttributeIntValue(j, -1);
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("versionCode")) {
        bool16 = paramAttributeSet.getAttributeIntValue(j, 0);
        b2 = b;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("versionCodeMajor")) {
        bool17 = paramAttributeSet.getAttributeIntValue(j, 0);
        b2 = b;
        bool16 = bool1;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("revisionCode")) {
        bool18 = paramAttributeSet.getAttributeIntValue(j, 0);
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("coreApp")) {
        bool19 = paramAttributeSet.getAttributeBooleanValue(j, false);
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("isolatedSplits")) {
        bool20 = paramAttributeSet.getAttributeBooleanValue(j, false);
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool21 = bool7;
        str7 = str1;
      } else if (str6.equals("configForSplit")) {
        str7 = paramAttributeSet.getAttributeValue(j);
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
      } else if (str6.equals("isFeatureSplit")) {
        bool21 = paramAttributeSet.getAttributeBooleanValue(j, false);
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        str7 = str1;
      } else {
        b2 = b;
        bool16 = bool1;
        bool17 = bool2;
        bool18 = bool3;
        bool19 = bool4;
        bool20 = bool6;
        bool21 = bool7;
        str7 = str1;
        if (str6.equals("isSplitRequired")) {
          bool8 = paramAttributeSet.getAttributeBooleanValue(j, false);
          str7 = str1;
          bool21 = bool7;
          bool20 = bool6;
          bool19 = bool4;
          bool18 = bool3;
          bool17 = bool2;
          bool16 = bool1;
          b2 = b;
        } 
      } 
    } 
    j = paramXmlPullParser.getDepth() + 1;
    ArrayList<VerifierInfo> arrayList = new ArrayList();
    String str3 = null, str4 = null, str5 = null;
    boolean bool12 = false;
    int k = 0;
    boolean bool13 = false, bool14 = false, bool9 = false;
    String str2 = null;
    boolean bool15 = true, bool11 = false, bool10 = bool5;
    int m = b1;
    bool5 = bool14;
    while (true) {
      int n = paramXmlPullParser.next();
      if (n != 1 && (n != 3 || 
        paramXmlPullParser.getDepth() >= j)) {
        if (n == 3 || n == 4)
          continue; 
        if (paramXmlPullParser.getDepth() != j)
          continue; 
        String str = paramXmlPullParser.getName();
        if ("package-verifier".equals(str)) {
          VerifierInfo verifierInfo = parseVerifier(paramAttributeSet);
          if (verifierInfo != null)
            arrayList.add(verifierInfo); 
          continue;
        } 
        if ("application".equals(paramXmlPullParser.getName())) {
          for (n = 0, bool14 = bool9; n < paramAttributeSet.getAttributeCount(); n++, bool14 = bool9) {
            str = paramAttributeSet.getAttributeName(n);
            bool9 = bool14;
            if ("debuggable".equals(str)) {
              boolean bool = paramAttributeSet.getAttributeBooleanValue(n, false);
              bool10 = bool;
              bool9 = bool14;
              if (bool) {
                bool9 = true;
                bool10 = bool;
              } 
            } 
            if ("multiArch".equals(str))
              bool11 = paramAttributeSet.getAttributeBooleanValue(n, false); 
            if ("use32bitAbi".equals(str))
              bool13 = paramAttributeSet.getAttributeBooleanValue(n, false); 
            if ("extractNativeLibs".equals(str))
              bool15 = paramAttributeSet.getAttributeBooleanValue(n, true); 
            if ("useEmbeddedDex".equals(str))
              bool12 = paramAttributeSet.getAttributeBooleanValue(n, false); 
          } 
          bool9 = bool14;
          continue;
        } 
        if ("overlay".equals(paramXmlPullParser.getName())) {
          for (n = 0, str = str3; n < paramAttributeSet.getAttributeCount(); n++, bool5 = bool14, str = str3, str4 = str7, str5 = str8) {
            String str7, str8, str6 = paramAttributeSet.getAttributeName(n);
            if ("requiredSystemPropertyName".equals(str6)) {
              str7 = paramAttributeSet.getAttributeValue(n);
              bool14 = bool5;
              str3 = str;
              str8 = str5;
            } else if ("requiredSystemPropertyValue".equals(str6)) {
              str8 = paramAttributeSet.getAttributeValue(n);
              bool14 = bool5;
              str3 = str;
              str7 = str4;
            } else if ("targetPackage".equals(str6)) {
              str3 = paramAttributeSet.getAttributeValue(n);
              bool14 = bool5;
              str7 = str4;
              str8 = str5;
            } else if ("isStatic".equals(str6)) {
              bool14 = paramAttributeSet.getAttributeBooleanValue(n, false);
              str3 = str;
              str7 = str4;
              str8 = str5;
            } else {
              bool14 = bool5;
              str3 = str;
              str7 = str4;
              str8 = str5;
              if ("priority".equals(str6)) {
                k = paramAttributeSet.getAttributeIntValue(n, 0);
                str8 = str5;
                str7 = str4;
                str3 = str;
                bool14 = bool5;
              } 
            } 
          } 
          str3 = str;
          continue;
        } 
        if ("uses-split".equals(paramXmlPullParser.getName())) {
          if (str2 != null) {
            Slog.w("PackageParser", "Only one <uses-split> permitted. Ignoring others.");
            continue;
          } 
          str2 = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
          if (str2 != null)
            continue; 
          throw new PackageParserException(-108, "<uses-split> tag requires 'android:name' attribute");
        } 
        if ("uses-sdk".equals(paramXmlPullParser.getName())) {
          for (byte b2 = 0; m < paramAttributeSet.getAttributeCount(); m++) {
            str = paramAttributeSet.getAttributeName(m);
            if ("targetSdkVersion".equals(str))
              n = paramAttributeSet.getAttributeIntValue(m, 0); 
            if ("minSdkVersion".equals(str))
              i = paramAttributeSet.getAttributeIntValue(m, 1); 
          } 
          m = n;
          continue;
        } 
        if ("profileable".equals(paramXmlPullParser.getName()))
          for (n = 0; n < paramAttributeSet.getAttributeCount(); n++, bool9 = bool14) {
            str = paramAttributeSet.getAttributeName(n);
            bool14 = bool9;
            if ("shell".equals(str))
              bool14 = paramAttributeSet.getAttributeBooleanValue(n, bool9); 
          }  
        continue;
      } 
      break;
    } 
    if (!checkRequiredSystemProperties(str4, str5)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Skipping target and overlay pair ");
      stringBuilder.append(str3);
      stringBuilder.append(" and ");
      stringBuilder.append(paramString);
      stringBuilder.append(": overlay ignored due to required system property: ");
      stringBuilder.append(str4);
      stringBuilder.append(" with value: ");
      stringBuilder.append(str5);
      Slog.i("PackageParser", stringBuilder.toString());
      str3 = null;
      bool5 = false;
      k = 0;
    } 
    return new ApkLite(paramString, (String)pair.first, (String)pair.second, bool7, str1, str2, bool8, bool1, bool2, bool3, b, arrayList, paramSigningDetails, bool4, bool10, bool9, bool11, bool13, bool12, bool15, bool6, str3, bool5, k, i, m);
  }
  
  private boolean parseBaseApkChild(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    String str1;
    StringBuilder stringBuilder;
    String str2 = paramXmlResourceParser.getAttributeValue(null, "package");
    if (validateName(str2, true, false) != null) {
      this.mParseError = -106;
      return false;
    } 
    if (str2.equals(paramPackage.packageName)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Child package name cannot be equal to parent package name: ");
      stringBuilder.append(paramPackage.packageName);
      str1 = stringBuilder.toString();
      Slog.w("PackageParser", str1);
      paramArrayOfString[0] = str1;
      this.mParseError = -108;
      return false;
    } 
    if (str1.hasChildPackage(str2)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Duplicate child package:");
      stringBuilder1.append(str2);
      str1 = stringBuilder1.toString();
      Slog.w("PackageParser", str1);
      paramArrayOfString[0] = str1;
      this.mParseError = -108;
      return false;
    } 
    Package package_2 = new Package(str2);
    package_2.mVersionCode = ((Package)str1).mVersionCode;
    package_2.baseRevisionCode = ((Package)str1).baseRevisionCode;
    package_2.mVersionName = ((Package)str1).mVersionName;
    package_2.applicationInfo.targetSdkVersion = ((Package)str1).applicationInfo.targetSdkVersion;
    package_2.applicationInfo.minSdkVersion = ((Package)str1).applicationInfo.minSdkVersion;
    Package package_1 = parseBaseApkCommon(package_2, CHILD_PACKAGE_TAGS, (Resources)stringBuilder, paramXmlResourceParser, paramInt, paramArrayOfString);
    if (package_1 == null)
      return false; 
    if (((Package)str1).childPackages == null)
      ((Package)str1).childPackages = new ArrayList<>(); 
    ((Package)str1).childPackages.add(package_1);
    package_1.parentPackage = (Package)str1;
    return true;
  }
  
  private Package parseBaseApk(String paramString, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    try {
      StringBuilder stringBuilder;
      Pair<String, String> pair = parsePackageSplitNames(paramXmlResourceParser, paramXmlResourceParser);
      paramString = (String)pair.first;
      String str = (String)pair.second;
      if (!TextUtils.isEmpty(str)) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Expected base APK, but found split ");
        stringBuilder.append(str);
        paramArrayOfString[0] = stringBuilder.toString();
        this.mParseError = -106;
        return null;
      } 
      Package package_ = new Package((String)stringBuilder);
      TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifest);
      package_.mVersionCode = typedArray.getInteger(1, 0);
      package_.mVersionCodeMajor = typedArray.getInteger(11, 0);
      package_.applicationInfo.setVersionCode(package_.getLongVersionCode());
      package_.baseRevisionCode = typedArray.getInteger(5, 0);
      package_.mVersionName = typedArray.getNonConfigurationString(2, 0);
      if (package_.mVersionName != null)
        package_.mVersionName = package_.mVersionName.intern(); 
      package_.coreApp = paramXmlResourceParser.getAttributeBooleanValue(null, "coreApp", false);
      boolean bool = typedArray.getBoolean(6, false);
      if (bool) {
        ApplicationInfo applicationInfo = package_.applicationInfo;
        applicationInfo.privateFlags |= 0x8000;
      } 
      package_.mCompileSdkVersion = typedArray.getInteger(9, 0);
      package_.applicationInfo.compileSdkVersion = package_.mCompileSdkVersion;
      package_.mCompileSdkVersionCodename = typedArray.getNonConfigurationString(10, 0);
      if (package_.mCompileSdkVersionCodename != null)
        package_.mCompileSdkVersionCodename = package_.mCompileSdkVersionCodename.intern(); 
      package_.applicationInfo.compileSdkVersionCodename = package_.mCompileSdkVersionCodename;
      typedArray.recycle();
      return parseBaseApkCommon(package_, null, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString);
    } catch (PackageParserException packageParserException) {
      this.mParseError = -106;
      return null;
    } 
  }
  
  private Package parseBaseApkCommon(Package paramPackage, Set<String> paramSet, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder1;
    this.mParseInstrumentationArgs = null;
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifest);
    String str = typedArray.getNonConfigurationString(0, 0);
    if (str != null && str.length() > 0) {
      String str1 = OplusBasePackageParser.hookNameError(paramPackage.packageName, validateName(str, true, false));
      if (str1 != null && !"android".equals(paramPackage.packageName)) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("<manifest> specifies bad sharedUserId name \"");
        stringBuilder1.append(str);
        stringBuilder1.append("\": ");
        stringBuilder1.append(str1);
        paramArrayOfString[0] = stringBuilder1.toString();
        this.mParseError = -107;
        return null;
      } 
      ((Package)stringBuilder1).mSharedUserId = str.intern();
      ((Package)stringBuilder1).mSharedUserLabel = typedArray.getResourceId(3, 0);
    } 
    ((Package)stringBuilder1).installLocation = typedArray.getInteger(4, -1);
    ((Package)stringBuilder1).applicationInfo.installLocation = ((Package)stringBuilder1).installLocation;
    int i = typedArray.getInteger(7, 1);
    ((Package)stringBuilder1).applicationInfo.targetSandboxVersion = i;
    if ((paramInt & 0x8) != 0) {
      ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
      applicationInfo.flags |= 0x40000;
    } 
    int j = 1;
    int k = paramXmlResourceParser.getDepth(), m = 1, n = 1, i1 = 1, i2 = 0, i3 = 1, i4 = 1;
    while (true) {
      int i6, i5 = paramXmlResourceParser.next();
      if (i5 != 1 && (i5 != 3 || 
        paramXmlResourceParser.getDepth() > k)) {
        if (i5 == 3 || i5 == 4)
          continue; 
        String str1 = paramXmlResourceParser.getName();
        if (paramSet != null && !paramSet.contains(str1)) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Skipping unsupported element under <manifest>: ");
          stringBuilder.append(str1);
          stringBuilder.append(" at ");
          stringBuilder.append(this.mArchiveSourcePath);
          stringBuilder.append(" ");
          stringBuilder.append(paramXmlResourceParser.getPositionDescription());
          String str2 = stringBuilder.toString();
          Slog.w("PackageParser", str2);
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          continue;
        } 
        if (str1.equals("application")) {
          if (i2) {
            Slog.w("PackageParser", "<manifest> has more than one <application>");
            XmlUtils.skipCurrentTag(paramXmlResourceParser);
            continue;
          } 
          if (!parseBaseApplication((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString))
            return null; 
          i2 = i4;
          i6 = i1;
          i5 = 1;
          i1 = i3;
        } else {
          i5 = i4;
          if (str1.equals("overlay")) {
            TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestResourceOverlay);
            ((Package)stringBuilder1).mOverlayTarget = typedArray1.getString(1);
            ((Package)stringBuilder1).mOverlayTargetName = typedArray1.getString(3);
            ((Package)stringBuilder1).mOverlayCategory = typedArray1.getString(2);
            ((Package)stringBuilder1).mOverlayPriority = typedArray1.getInt(0, 0);
            ((Package)stringBuilder1).mOverlayIsStatic = typedArray1.getBoolean(4, false);
            String str2 = typedArray1.getString(5);
            str1 = typedArray1.getString(6);
            typedArray1.recycle();
            if (((Package)stringBuilder1).mOverlayTarget == null) {
              paramArrayOfString[0] = "<overlay> does not specify a target package";
              this.mParseError = -108;
              return null;
            } 
            if (((Package)stringBuilder1).mOverlayPriority < 0 || ((Package)stringBuilder1).mOverlayPriority > 9999) {
              paramArrayOfString[0] = "<overlay> priority must be between 0 and 9999";
              this.mParseError = -108;
              return null;
            } 
            if (!checkRequiredSystemProperties(str2, str1)) {
              stringBuilder2 = new StringBuilder();
              stringBuilder2.append("Skipping target and overlay pair ");
              stringBuilder2.append(((Package)stringBuilder1).mOverlayTarget);
              stringBuilder2.append(" and ");
              stringBuilder2.append(((Package)stringBuilder1).baseCodePath);
              stringBuilder2.append(": overlay ignored due to required system property: ");
              stringBuilder2.append(str2);
              stringBuilder2.append(" with value: ");
              stringBuilder2.append(str1);
              Slog.i("PackageParser", stringBuilder2.toString());
              this.mParseError = -125;
              return null;
            } 
            ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
            applicationInfo.privateFlags |= 0x10000000;
            XmlUtils.skipCurrentTag(paramXmlResourceParser);
            i4 = i3;
            i3 = i2;
            i6 = i1;
            i1 = i4;
            i2 = i5;
            i5 = i3;
          } else {
            if (str1.equals("key-sets")) {
              if (!parseKeySets((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramArrayOfString))
                return null; 
            } else if (str1.equals("permission-group")) {
              if (!parsePermissionGroup((Package)stringBuilder1, paramInt, paramResources, paramXmlResourceParser, paramArrayOfString))
                return null; 
            } else {
              int i7 = i3;
              if (str1.equals("permission")) {
                if (!parsePermission((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramArrayOfString))
                  return null; 
              } else if (str1.equals("permission-tree")) {
                if (!parsePermissionTree((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramArrayOfString))
                  return null; 
              } else if (str1.equals("uses-permission")) {
                if (!parseUsesPermission((Package)stringBuilder1, paramResources, paramXmlResourceParser))
                  return null; 
              } else if (str1.equals("uses-permission-sdk-m") || 
                str1.equals("uses-permission-sdk-23")) {
                if (!parseUsesPermission((Package)stringBuilder1, paramResources, paramXmlResourceParser))
                  return null; 
              } else {
                ConfigurationInfo configurationInfo;
                if (str1.equals("uses-configuration")) {
                  configurationInfo = new ConfigurationInfo();
                  TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesConfiguration);
                  configurationInfo.reqTouchScreen = typedArray1.getInt(0, 0);
                  configurationInfo.reqKeyboardType = typedArray1.getInt(1, 0);
                  if (typedArray1.getBoolean(2, false))
                    configurationInfo.reqInputFeatures |= 0x1; 
                  configurationInfo.reqNavigation = typedArray1.getInt(3, 0);
                  if (typedArray1.getBoolean(4, false))
                    configurationInfo.reqInputFeatures |= 0x2; 
                  typedArray1.recycle();
                  ((Package)stringBuilder1).configPreferences = ArrayUtils.add(((Package)stringBuilder1).configPreferences, configurationInfo);
                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                  i4 = i7;
                  i3 = i2;
                  i7 = i1;
                  i1 = i4;
                  i2 = i5;
                  i5 = i3;
                } else {
                  FeatureInfo featureInfo;
                  String str2 = "uses-feature";
                  if (configurationInfo.equals("uses-feature")) {
                    featureInfo = parseUsesFeature(paramResources, paramXmlResourceParser);
                    ((Package)stringBuilder1).reqFeatures = ArrayUtils.add(((Package)stringBuilder1).reqFeatures, featureInfo);
                    if (featureInfo.name == null) {
                      configurationInfo = new ConfigurationInfo();
                      configurationInfo.reqGlEsVersion = featureInfo.reqGlEsVersion;
                      ((Package)stringBuilder1).configPreferences = ArrayUtils.add(((Package)stringBuilder1).configPreferences, configurationInfo);
                    } 
                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                  } else {
                    ArrayList arrayList;
                    if (configurationInfo.equals("feature-group")) {
                      FeatureGroupInfo featureGroupInfo = new FeatureGroupInfo();
                      configurationInfo = null;
                      i3 = paramXmlResourceParser.getDepth();
                      while (true) {
                        i4 = paramXmlResourceParser.next();
                        if (i4 != 1 && (i4 != 3 || 
                          paramXmlResourceParser.getDepth() > i3)) {
                          FeatureInfo featureInfo1;
                          if (i4 == 3 || i4 == 4)
                            continue; 
                          String str3 = paramXmlResourceParser.getName();
                          if (str3.equals(featureInfo)) {
                            featureInfo1 = parseUsesFeature(paramResources, paramXmlResourceParser);
                            featureInfo1.flags |= 0x1;
                            arrayList = ArrayUtils.add((ArrayList)configurationInfo, featureInfo1);
                          } else {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("Unknown element under <feature-group>: ");
                            stringBuilder.append((String)featureInfo1);
                            stringBuilder.append(" at ");
                            stringBuilder.append(this.mArchiveSourcePath);
                            stringBuilder.append(" ");
                            stringBuilder.append(paramXmlResourceParser.getPositionDescription());
                            String str4 = stringBuilder.toString();
                            Slog.w("PackageParser", str4);
                          } 
                          XmlUtils.skipCurrentTag(paramXmlResourceParser);
                          continue;
                        } 
                        break;
                      } 
                      if (arrayList != null) {
                        featureGroupInfo.features = new FeatureInfo[arrayList.size()];
                        featureGroupInfo.features = (FeatureInfo[])arrayList.toArray((Object[])featureGroupInfo.features);
                      } 
                      ((Package)stringBuilder1).featureGroups = ArrayUtils.add(((Package)stringBuilder1).featureGroups, featureGroupInfo);
                      i4 = i2;
                      i3 = i1;
                      i1 = i7;
                      i2 = i5;
                      i5 = i4;
                      i7 = i3;
                    } else {
                      FeatureInfo featureInfo1;
                      if (arrayList.equals("uses-sdk")) {
                        if (SDK_VERSION > 0) {
                          FeatureInfo featureInfo3;
                          TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesSdk);
                          i4 = 1;
                          FeatureInfo featureInfo2 = null;
                          boolean bool = false;
                          arrayList = null;
                          TypedValue typedValue2 = typedArray1.peekValue(0);
                          i3 = i4;
                          featureInfo = featureInfo2;
                          if (typedValue2 != null)
                            if (typedValue2.type == 3 && typedValue2.string != null) {
                              String str3 = typedValue2.string.toString();
                              i3 = i4;
                            } else {
                              i3 = typedValue2.data;
                              featureInfo = featureInfo2;
                            }  
                          TypedValue typedValue1 = typedArray1.peekValue(1);
                          if (typedValue1 != null) {
                            String str3;
                            if (typedValue1.type == 3 && typedValue1.string != null) {
                              str3 = typedValue1.string.toString();
                              featureInfo3 = featureInfo;
                              i4 = bool;
                              String str4 = str3;
                              if (featureInfo == null) {
                                String str5 = str3;
                                i4 = bool;
                                str4 = str3;
                              } 
                            } else {
                              i4 = ((TypedValue)str3).data;
                              featureInfo3 = featureInfo;
                            } 
                          } else {
                            i4 = i3;
                            featureInfo1 = featureInfo;
                            featureInfo3 = featureInfo;
                          } 
                          typedArray1.recycle();
                          i3 = computeMinSdkVersion(i3, (String)featureInfo3, SDK_VERSION, SDK_CODENAMES, paramArrayOfString);
                          if (i3 < 0) {
                            this.mParseError = -12;
                            return null;
                          } 
                          i4 = computeTargetSdkVersion(i4, (String)featureInfo1, SDK_CODENAMES, paramArrayOfString);
                          if (i4 < 0) {
                            this.mParseError = -12;
                            return null;
                          } 
                          ((Package)stringBuilder1).applicationInfo.minSdkVersion = i3;
                          ((Package)stringBuilder1).applicationInfo.targetSdkVersion = i4;
                        } 
                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                        i3 = i2;
                        i4 = i1;
                        i1 = i7;
                        i2 = i5;
                        i5 = i3;
                        i7 = i4;
                      } else if (featureInfo1.equals("supports-screens")) {
                        TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestSupportsScreens);
                        ((Package)stringBuilder1).applicationInfo.requiresSmallestWidthDp = typedArray1.getInteger(6, 0);
                        ((Package)stringBuilder1).applicationInfo.compatibleWidthLimitDp = typedArray1.getInteger(7, 0);
                        ((Package)stringBuilder1).applicationInfo.largestWidthLimitDp = typedArray1.getInteger(8, 0);
                        j = typedArray1.getInteger(1, j);
                        i3 = typedArray1.getInteger(2, i5);
                        i4 = typedArray1.getInteger(3, i7);
                        m = typedArray1.getInteger(5, m);
                        n = typedArray1.getInteger(4, n);
                        i7 = typedArray1.getInteger(0, i1);
                        typedArray1.recycle();
                        XmlUtils.skipCurrentTag(paramXmlResourceParser);
                        i5 = i2;
                        i1 = i4;
                        i2 = i3;
                      } else {
                        String str3;
                        if (featureInfo1.equals("protected-broadcast")) {
                          TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProtectedBroadcast);
                          str3 = typedArray1.getNonResourceString(0);
                          typedArray1.recycle();
                          if (str3 != null) {
                            if (((Package)stringBuilder1).protectedBroadcasts == null)
                              ((Package)stringBuilder1).protectedBroadcasts = new ArrayList<>(); 
                            if (!((Package)stringBuilder1).protectedBroadcasts.contains(str3))
                              ((Package)stringBuilder1).protectedBroadcasts.add(str3.intern()); 
                          } 
                          XmlUtils.skipCurrentTag(paramXmlResourceParser);
                          i4 = i7;
                          i7 = i5;
                          i5 = i2;
                          i3 = i1;
                          i1 = i4;
                          i2 = i7;
                          i7 = i3;
                        } else {
                          if (str3.equals("instrumentation")) {
                            if (parseInstrumentation((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramArrayOfString) == null)
                              return null; 
                          } else {
                            TypedArray typedArray1;
                            if (str3.equals("original-package")) {
                              typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
                              String str4 = typedArray1.getNonConfigurationString(0, 0);
                              if (!((Package)stringBuilder1).packageName.equals(str4)) {
                                if (((Package)stringBuilder1).mOriginalPackages == null) {
                                  ((Package)stringBuilder1).mOriginalPackages = new ArrayList<>();
                                  ((Package)stringBuilder1).mRealPackage = ((Package)stringBuilder1).packageName;
                                } 
                                ((Package)stringBuilder1).mOriginalPackages.add(str4);
                              } 
                              typedArray1.recycle();
                              XmlUtils.skipCurrentTag(paramXmlResourceParser);
                              i4 = i5;
                              i5 = i2;
                              i3 = i1;
                              i1 = i7;
                              i2 = i4;
                              i7 = i3;
                            } else {
                              String str4;
                              if (typedArray1.equals("adopt-permissions")) {
                                TypedArray typedArray2 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
                                str4 = typedArray2.getNonConfigurationString(0, 0);
                                typedArray2.recycle();
                                if (str4 != null) {
                                  if (((Package)stringBuilder1).mAdoptPermissions == null)
                                    ((Package)stringBuilder1).mAdoptPermissions = new ArrayList<>(); 
                                  ((Package)stringBuilder1).mAdoptPermissions.add(str4);
                                } 
                                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                i3 = i7;
                                i4 = i2;
                                i7 = i1;
                                i1 = i3;
                                i2 = i5;
                                i5 = i4;
                              } else {
                                if (str4.equals("uses-gl-texture")) {
                                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                  continue;
                                } 
                                if (str4.equals("compatible-screens")) {
                                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                  continue;
                                } 
                                if (str4.equals("supports-input")) {
                                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                  continue;
                                } 
                                if (str4.equals("eat-comment")) {
                                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                  continue;
                                } 
                                if (str4.equals("package")) {
                                  if (!MULTI_PACKAGE_APK_ENABLED) {
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    continue;
                                  } 
                                  if (!parseBaseApkChild((Package)stringBuilder1, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString))
                                    return null; 
                                } else {
                                  if (str4.equals("restrict-update")) {
                                    if ((paramInt & 0x10) != 0) {
                                      TypedArray typedArray2 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestRestrictUpdate);
                                      String str5 = typedArray2.getNonConfigurationString(0, 0);
                                      typedArray2.recycle();
                                      ((Package)stringBuilder1).restrictUpdateHash = null;
                                      if (str5 != null) {
                                        i4 = str5.length();
                                        byte[] arrayOfByte = new byte[i4 / 2];
                                        for (i3 = 0; i3 < i4; i3 += 2) {
                                          int i8 = i3 / 2, i9 = Character.digit(str5.charAt(i3), 16);
                                          arrayOfByte[i8] = (byte)((i9 << 4) + Character.digit(str5.charAt(i3 + 1), 16));
                                        } 
                                        ((Package)stringBuilder1).restrictUpdateHash = arrayOfByte;
                                      } 
                                    } 
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    i3 = i2;
                                    i4 = i1;
                                    i1 = i7;
                                    i2 = i5;
                                    i5 = i3;
                                    i7 = i4;
                                  } else {
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.append("Unknown element under <manifest>: ");
                                    stringBuilder.append(paramXmlResourceParser.getName());
                                    stringBuilder.append(" at ");
                                    stringBuilder.append(this.mArchiveSourcePath);
                                    stringBuilder.append(" ");
                                    stringBuilder.append(paramXmlResourceParser.getPositionDescription());
                                    String str5 = stringBuilder.toString();
                                    Slog.w("PackageParser", str5);
                                    XmlUtils.skipCurrentTag(paramXmlResourceParser);
                                    continue;
                                  } 
                                  i3 = i1;
                                  i4 = i2;
                                  i2 = i5;
                                  i1 = i7;
                                } 
                                i7 = i1;
                                i1 = i3;
                                i5 = i2;
                                i2 = i4;
                              } 
                            } 
                            i3 = i1;
                            i4 = i2;
                            i2 = i5;
                            i1 = i7;
                          } 
                          i7 = i1;
                          i1 = i3;
                          i5 = i2;
                          i2 = i4;
                        } 
                      } 
                    } 
                    i3 = i1;
                    i4 = i2;
                    i2 = i5;
                    i1 = i7;
                  } 
                  i7 = i1;
                  i1 = i3;
                  i5 = i2;
                  i2 = i4;
                } 
                i3 = i1;
                i4 = i2;
                i2 = i5;
                i1 = i7;
              } 
            } 
            i6 = i1;
            i1 = i3;
            i5 = i2;
            i2 = i4;
          } 
        } 
      } else {
        break;
      } 
      i3 = i1;
      i4 = i2;
      i2 = i5;
      i1 = i6;
    } 
    if (i2 == 0 && ((Package)stringBuilder1).instrumentation.size() == 0) {
      paramArrayOfString[0] = "<manifest> does not contain an <application> or <instrumentation>";
      this.mParseError = -109;
    } 
    i2 = NEW_PERMISSIONS.length;
    StringBuilder stringBuilder2;
    for (paramSet = null, paramInt = 0; paramInt < i2; paramInt++, stringBuilder2 = stringBuilder) {
      StringBuilder stringBuilder;
      NewPermissionInfo newPermissionInfo = NEW_PERMISSIONS[paramInt];
      if (((Package)stringBuilder1).applicationInfo.targetSdkVersion >= newPermissionInfo.sdkVersion)
        break; 
      Set<String> set = paramSet;
      if (!((Package)stringBuilder1).requestedPermissions.contains(newPermissionInfo.name)) {
        StringBuilder stringBuilder3;
        if (paramSet == null) {
          stringBuilder3 = new StringBuilder(128);
          stringBuilder3.append(((Package)stringBuilder1).packageName);
          stringBuilder3.append(": compat added ");
        } else {
          stringBuilder3.append(' ');
        } 
        stringBuilder3.append(newPermissionInfo.name);
        ((Package)stringBuilder1).requestedPermissions.add(newPermissionInfo.name);
        ((Package)stringBuilder1).implicitPermissions.add(newPermissionInfo.name);
        stringBuilder = stringBuilder3;
      } 
    } 
    if (stringBuilder2 != null)
      Slog.i("PackageParser", stringBuilder2.toString()); 
    try {
      List<SplitPermissionInfoParcelable> list = ActivityThread.getPermissionManager().getSplitPermissions();
      int i5 = list.size();
      for (paramInt = 0; paramInt < i5; paramInt++) {
        SplitPermissionInfoParcelable splitPermissionInfoParcelable = list.get(paramInt);
        if (((Package)stringBuilder1).applicationInfo.targetSdkVersion < splitPermissionInfoParcelable.getTargetSdk()) {
          ArrayList<String> arrayList = ((Package)stringBuilder1).requestedPermissions;
          if (arrayList.contains(splitPermissionInfoParcelable.getSplitPermission())) {
            List<String> list1 = splitPermissionInfoParcelable.getNewPermissions();
            for (i2 = 0; i2 < list1.size(); i2++) {
              String str1 = list1.get(i2);
              if (!((Package)stringBuilder1).requestedPermissions.contains(str1)) {
                ((Package)stringBuilder1).requestedPermissions.add(str1);
                ((Package)stringBuilder1).implicitPermissions.add(str1);
              } 
            } 
          } 
        } 
      } 
      if (j < 0 || (j > 0 && ((Package)stringBuilder1).applicationInfo.targetSdkVersion >= 4)) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x200;
      } 
      if (i4 != 0) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x400;
      } 
      if (i3 < 0 || (i3 > 0 && ((Package)stringBuilder1).applicationInfo.targetSdkVersion >= 4)) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x800;
      } 
      if (m < 0 || (m > 0 && ((Package)stringBuilder1).applicationInfo.targetSdkVersion >= 9)) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x80000;
      } 
      if (n < 0 || (n > 0 && ((Package)stringBuilder1).applicationInfo.targetSdkVersion >= 4)) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x1000;
      } 
      if (i1 < 0 || (i1 > 0 && ((Package)stringBuilder1).applicationInfo.targetSdkVersion >= 4)) {
        ApplicationInfo applicationInfo = ((Package)stringBuilder1).applicationInfo;
        applicationInfo.flags |= 0x2000;
      } 
      if (((Package)stringBuilder1).applicationInfo.usesCompatibilityMode())
        adjustPackageToBeUnresizeableAndUnpipable((Package)stringBuilder1); 
      return (Package)stringBuilder1;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static boolean checkRequiredSystemProperties(String paramString1, String paramString2) {
    StringBuilder stringBuilder;
    if (TextUtils.isEmpty(paramString1) || TextUtils.isEmpty(paramString2)) {
      if (!TextUtils.isEmpty(paramString1) || !TextUtils.isEmpty(paramString2)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Disabling overlay - incomplete property :'");
        stringBuilder.append(paramString1);
        stringBuilder.append("=");
        stringBuilder.append(paramString2);
        stringBuilder.append("' - require both requiredSystemPropertyName AND requiredSystemPropertyValue to be specified.");
        Slog.w("PackageParser", stringBuilder.toString());
        return false;
      } 
      return true;
    } 
    String[] arrayOfString1 = paramString1.split(",");
    String[] arrayOfString2 = paramString2.split(",");
    if (arrayOfString1.length != arrayOfString2.length) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Disabling overlay - property :'");
      stringBuilder.append(paramString1);
      stringBuilder.append("=");
      stringBuilder.append(paramString2);
      stringBuilder.append("' - require both requiredSystemPropertyName AND requiredSystemPropertyValue lists to have the same size.");
      Slog.w("PackageParser", stringBuilder.toString());
      return false;
    } 
    for (byte b = 0; b < arrayOfString1.length; b++) {
      paramString1 = SystemProperties.get(arrayOfString1[b]);
      if (!TextUtils.equals(paramString1, stringBuilder[b]))
        return false; 
    } 
    return true;
  }
  
  private void adjustPackageToBeUnresizeableAndUnpipable(Package paramPackage) {
    for (Activity activity : paramPackage.activities) {
      activity.info.resizeMode = 0;
      ActivityInfo activityInfo = activity.info;
      activityInfo.flags &= 0xFFBFFFFF;
    } 
  }
  
  private static boolean matchTargetCode(String[] paramArrayOfString, String paramString) {
    int i = paramString.indexOf('.');
    if (i != -1)
      paramString = paramString.substring(0, i); 
    return ArrayUtils.contains((Object[])paramArrayOfString, paramString);
  }
  
  public static int computeTargetSdkVersion(int paramInt, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    if (paramString == null)
      return paramInt; 
    if (matchTargetCode(paramArrayOfString1, paramString))
      return 10000; 
    if (paramArrayOfString1.length > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Requires development platform ");
      stringBuilder.append(paramString);
      stringBuilder.append(" (current platform is any of ");
      stringBuilder.append(Arrays.toString((Object[])paramArrayOfString1));
      stringBuilder.append(")");
      paramArrayOfString2[0] = stringBuilder.toString();
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Requires development platform ");
      stringBuilder.append(paramString);
      stringBuilder.append(" but this is a release platform.");
      paramArrayOfString2[0] = stringBuilder.toString();
    } 
    return -1;
  }
  
  public static int computeMinSdkVersion(int paramInt1, String paramString, int paramInt2, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    StringBuilder stringBuilder;
    if (paramString == null) {
      if (paramInt1 <= paramInt2)
        return paramInt1; 
      stringBuilder = new StringBuilder();
      stringBuilder.append("Requires newer sdk version #");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" (current version is #");
      stringBuilder.append(paramInt2);
      stringBuilder.append(")");
      paramArrayOfString2[0] = stringBuilder.toString();
      return -1;
    } 
    if (matchTargetCode(paramArrayOfString1, (String)stringBuilder))
      return 10000; 
    if (paramArrayOfString1.length > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Requires development platform ");
      stringBuilder1.append((String)stringBuilder);
      stringBuilder1.append(" (current platform is any of ");
      stringBuilder1.append(Arrays.toString((Object[])paramArrayOfString1));
      stringBuilder1.append(")");
      paramArrayOfString2[0] = stringBuilder1.toString();
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Requires development platform ");
      stringBuilder1.append((String)stringBuilder);
      stringBuilder1.append(" but this is a release platform.");
      paramArrayOfString2[0] = stringBuilder1.toString();
    } 
    return -1;
  }
  
  private FeatureInfo parseUsesFeature(Resources paramResources, AttributeSet paramAttributeSet) {
    FeatureInfo featureInfo = new FeatureInfo();
    TypedArray typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestUsesFeature);
    featureInfo.name = typedArray.getNonResourceString(0);
    featureInfo.version = typedArray.getInt(3, 0);
    if (featureInfo.name == null)
      featureInfo.reqGlEsVersion = typedArray.getInt(1, 0); 
    if (typedArray.getBoolean(2, true))
      featureInfo.flags |= 0x1; 
    typedArray.recycle();
    return featureInfo;
  }
  
  private boolean parseUsesStaticLibrary(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder;
    String[] arrayOfString1;
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesStaticLibrary);
    String str2 = typedArray.getNonResourceString(0);
    int i = typedArray.getInt(1, -1);
    String str3 = typedArray.getNonResourceString(2);
    typedArray.recycle();
    if (str2 == null || i < 0 || str3 == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Bad uses-static-library declaration name: ");
      stringBuilder.append(str2);
      stringBuilder.append(" version: ");
      stringBuilder.append(i);
      stringBuilder.append(" certDigest");
      stringBuilder.append(str3);
      paramArrayOfString[0] = stringBuilder.toString();
      this.mParseError = -108;
      XmlUtils.skipCurrentTag(paramXmlResourceParser);
      return false;
    } 
    if (((Package)stringBuilder).usesStaticLibraries != null && ((Package)stringBuilder).usesStaticLibraries.contains(str2)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Depending on multiple versions of static library ");
      stringBuilder.append(str2);
      paramArrayOfString[0] = stringBuilder.toString();
      this.mParseError = -108;
      XmlUtils.skipCurrentTag(paramXmlResourceParser);
      return false;
    } 
    String str1 = str2.intern();
    str3 = str3.replace(":", "").toLowerCase();
    String[] arrayOfString3 = EmptyArray.STRING;
    if (((Package)stringBuilder).applicationInfo.targetSdkVersion >= 27) {
      arrayOfString2 = parseAdditionalCertificates(paramResources, paramXmlResourceParser, paramArrayOfString);
      arrayOfString1 = arrayOfString2;
      if (arrayOfString2 == null)
        return false; 
    } else {
      XmlUtils.skipCurrentTag((XmlPullParser)arrayOfString2);
      arrayOfString1 = arrayOfString3;
    } 
    String[] arrayOfString2 = new String[arrayOfString1.length + 1];
    arrayOfString2[0] = str3;
    System.arraycopy(arrayOfString1, 0, arrayOfString2, 1, arrayOfString1.length);
    ((Package)stringBuilder).usesStaticLibraries = ArrayUtils.add(((Package)stringBuilder).usesStaticLibraries, str1);
    ((Package)stringBuilder).usesStaticLibrariesVersions = ArrayUtils.appendLong(((Package)stringBuilder).usesStaticLibrariesVersions, i, true);
    ((Package)stringBuilder).usesStaticLibrariesCertDigests = (String[][])ArrayUtils.appendElement(String[].class, (Object[])((Package)stringBuilder).usesStaticLibrariesCertDigests, arrayOfString2, true);
    return true;
  }
  
  private String[] parseAdditionalCertificates(Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    String[] arrayOfString = EmptyArray.STRING;
    int i = paramXmlResourceParser.getDepth();
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        if (j == 3 || j == 4)
          continue; 
        String str = paramXmlResourceParser.getName();
        if (str.equals("additional-certificate")) {
          TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestAdditionalCertificate);
          str = typedArray.getNonResourceString(0);
          typedArray.recycle();
          if (TextUtils.isEmpty(str)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Bad additional-certificate declaration with empty certDigest:");
            stringBuilder.append(str);
            paramArrayOfString[0] = stringBuilder.toString();
            this.mParseError = -108;
            XmlUtils.skipCurrentTag(paramXmlResourceParser);
            typedArray.recycle();
            return null;
          } 
          str = str.replace(":", "").toLowerCase();
          arrayOfString = (String[])ArrayUtils.appendElement(String.class, (Object[])arrayOfString, str);
          continue;
        } 
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        continue;
      } 
      break;
    } 
    return arrayOfString;
  }
  
  private boolean parseUsesPermission(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesPermission);
    String str1 = typedArray.getNonResourceString(0);
    byte b = 0;
    TypedValue typedValue = typedArray.peekValue(1);
    int i = b;
    if (typedValue != null) {
      i = b;
      if (typedValue.type >= 16) {
        i = b;
        if (typedValue.type <= 31)
          i = typedValue.data; 
      } 
    } 
    String str3 = typedArray.getNonConfigurationString(2, 0);
    String str2 = typedArray.getNonConfigurationString(3, 0);
    typedArray.recycle();
    XmlUtils.skipCurrentTag(paramXmlResourceParser);
    if (str1 == null)
      return true; 
    if (i != 0 && i < Build.VERSION.RESOURCES_SDK_INT)
      return true; 
    if (str3 != null) {
      Callback callback = this.mCallback;
      if (callback != null && !callback.hasFeature(str3))
        return true; 
    } 
    if (str2 != null) {
      Callback callback = this.mCallback;
      if (callback != null && 
        callback.hasFeature(str2))
        return true; 
    } 
    i = paramPackage.requestedPermissions.indexOf(str1);
    if (i == -1) {
      paramPackage.requestedPermissions.add(str1.intern());
    } else {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring duplicate uses-permissions/uses-permissions-sdk-m: ");
      stringBuilder.append(str1);
      stringBuilder.append(" in package: ");
      stringBuilder.append(paramPackage.packageName);
      stringBuilder.append(" at: ");
      stringBuilder.append(paramXmlResourceParser.getPositionDescription());
      String str = stringBuilder.toString();
      Slog.w("PackageParser", str);
    } 
    return true;
  }
  
  public static String buildClassName(String paramString, CharSequence paramCharSequence, String[] paramArrayOfString) {
    if (paramCharSequence == null || paramCharSequence.length() <= 0) {
      paramCharSequence = new StringBuilder();
      paramCharSequence.append("Empty class name in package ");
      paramCharSequence.append(paramString);
      paramArrayOfString[0] = paramCharSequence.toString();
      return null;
    } 
    paramCharSequence = paramCharSequence.toString();
    char c = paramCharSequence.charAt(0);
    if (c == '.') {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append((String)paramCharSequence);
      return stringBuilder.toString();
    } 
    if (paramCharSequence.indexOf('.') < 0) {
      StringBuilder stringBuilder = new StringBuilder(paramString);
      stringBuilder.append('.');
      stringBuilder.append((String)paramCharSequence);
      return stringBuilder.toString();
    } 
    return (String)paramCharSequence;
  }
  
  private static String buildCompoundName(String paramString1, CharSequence paramCharSequence, String paramString2, String[] paramArrayOfString) {
    StringBuilder stringBuilder;
    paramCharSequence = paramCharSequence.toString();
    char c = paramCharSequence.charAt(0);
    if (paramString1 != null && c == ':') {
      if (paramCharSequence.length() < 2) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Bad ");
        stringBuilder1.append(paramString2);
        stringBuilder1.append(" name ");
        stringBuilder1.append((String)paramCharSequence);
        stringBuilder1.append(" in package ");
        stringBuilder1.append(paramString1);
        stringBuilder1.append(": must be at least two characters");
        paramArrayOfString[0] = stringBuilder1.toString();
        return null;
      } 
      String str1 = paramCharSequence.substring(1);
      String str2 = validateName(str1, false, false);
      if (str2 != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Invalid ");
        stringBuilder1.append(paramString2);
        stringBuilder1.append(" name ");
        stringBuilder1.append((String)paramCharSequence);
        stringBuilder1.append(" in package ");
        stringBuilder1.append(paramString1);
        stringBuilder1.append(": ");
        stringBuilder1.append(str2);
        paramArrayOfString[0] = stringBuilder1.toString();
        return null;
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append((String)paramCharSequence);
      return stringBuilder.toString();
    } 
    String str = validateName((String)paramCharSequence, true, false);
    if (str != null && !"system".equals(paramCharSequence)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid ");
      stringBuilder1.append((String)stringBuilder);
      stringBuilder1.append(" name ");
      stringBuilder1.append((String)paramCharSequence);
      stringBuilder1.append(" in package ");
      stringBuilder1.append(paramString1);
      stringBuilder1.append(": ");
      stringBuilder1.append(str);
      paramArrayOfString[0] = stringBuilder1.toString();
      return null;
    } 
    return (String)paramCharSequence;
  }
  
  public static String buildProcessName(String paramString1, String paramString2, CharSequence paramCharSequence, int paramInt, String[] paramArrayOfString1, String[] paramArrayOfString2) {
    if ((paramInt & 0x2) != 0 && !"system".equals(paramCharSequence)) {
      if (paramString2 != null)
        paramString1 = paramString2; 
      return paramString1;
    } 
    if (paramArrayOfString1 != null)
      for (paramInt = paramArrayOfString1.length - 1; paramInt >= 0; paramInt--) {
        String str = paramArrayOfString1[paramInt];
        if (str.equals(paramString1) || str.equals(paramString2) || str.equals(paramCharSequence))
          return paramString1; 
      }  
    if (paramCharSequence == null || paramCharSequence.length() <= 0)
      return paramString2; 
    return TextUtils.safeIntern(buildCompoundName(paramString1, paramCharSequence, "process", paramArrayOfString2));
  }
  
  public static String buildTaskAffinityName(String paramString1, String paramString2, CharSequence paramCharSequence, String[] paramArrayOfString) {
    if (paramCharSequence == null)
      return paramString2; 
    if (paramCharSequence.length() <= 0)
      return null; 
    return buildCompoundName(paramString1, paramCharSequence, "taskAffinity", paramArrayOfString);
  }
  
  private boolean parseKeySets(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder1;
    int i = paramXmlResourceParser.getDepth();
    int j = -1;
    String str = null;
    ArrayMap arrayMap1 = new ArrayMap();
    ArraySet<String> arraySet = new ArraySet();
    ArrayMap arrayMap2 = new ArrayMap();
    ArraySet arraySet1 = new ArraySet();
    while (true) {
      int k = paramXmlResourceParser.next();
      if (k != 1 && (k != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        TypedArray typedArray;
        if (k == 3) {
          if (paramXmlResourceParser.getDepth() == j) {
            str = null;
            j = -1;
          } 
          continue;
        } 
        String str2 = paramXmlResourceParser.getName();
        if (str2.equals("key-set")) {
          if (str != null) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Improperly nested 'key-set' tag at ");
            stringBuilder1.append(paramXmlResourceParser.getPositionDescription());
            paramArrayOfString[0] = stringBuilder1.toString();
            this.mParseError = -108;
            return false;
          } 
          typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestKeySet);
          str = typedArray.getNonResourceString(0);
          arrayMap2.put(str, new ArraySet());
          j = paramXmlResourceParser.getDepth();
          typedArray.recycle();
          continue;
        } 
        if (typedArray.equals("public-key")) {
          if (str == null) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Improperly nested 'key-set' tag at ");
            stringBuilder1.append(paramXmlResourceParser.getPositionDescription());
            paramArrayOfString[0] = stringBuilder1.toString();
            this.mParseError = -108;
            return false;
          } 
          typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPublicKey);
          String str3 = typedArray.getNonResourceString(0);
          String str4 = typedArray.getNonResourceString(1);
          if (str4 == null && arrayMap1.get(str3) == null) {
            stringBuilder1 = new StringBuilder();
            stringBuilder1.append("'public-key' ");
            stringBuilder1.append(str3);
            stringBuilder1.append(" must define a public-key value on first use at ");
            stringBuilder1.append(paramXmlResourceParser.getPositionDescription());
            paramArrayOfString[0] = stringBuilder1.toString();
            this.mParseError = -108;
            typedArray.recycle();
            return false;
          } 
          if (str4 != null) {
            PublicKey publicKey = parsePublicKey(str4);
            if (publicKey == null) {
              StringBuilder stringBuilder3 = new StringBuilder();
              stringBuilder3.append("No recognized valid key in 'public-key' tag at ");
              stringBuilder3.append(paramXmlResourceParser.getPositionDescription());
              stringBuilder3.append(" key-set ");
              stringBuilder3.append(str);
              stringBuilder3.append(" will not be added to the package's defined key-sets.");
              str3 = stringBuilder3.toString();
              Slog.w("PackageParser", str3);
              typedArray.recycle();
              arraySet1.add(str);
              XmlUtils.skipCurrentTag(paramXmlResourceParser);
              continue;
            } 
            if (arrayMap1.get(str3) == null || (
              (PublicKey)arrayMap1.get(str3)).equals(publicKey)) {
              arrayMap1.put(str3, publicKey);
            } else {
              stringBuilder1 = new StringBuilder();
              stringBuilder1.append("Value of 'public-key' ");
              stringBuilder1.append(str3);
              stringBuilder1.append(" conflicts with previously defined value at ");
              stringBuilder1.append(paramXmlResourceParser.getPositionDescription());
              paramArrayOfString[0] = stringBuilder1.toString();
              this.mParseError = -108;
              typedArray.recycle();
              return false;
            } 
          } 
          ((ArraySet)arrayMap2.get(str)).add(str3);
          typedArray.recycle();
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          continue;
        } 
        if (typedArray.equals("upgrade-key-set")) {
          typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUpgradeKeySet);
          String str3 = typedArray.getNonResourceString(0);
          arraySet.add(str3);
          typedArray.recycle();
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown element under <key-sets>: ");
        stringBuilder.append(paramXmlResourceParser.getName());
        stringBuilder.append(" at ");
        stringBuilder.append(this.mArchiveSourcePath);
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str1 = stringBuilder.toString();
        Slog.w("PackageParser", str1);
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        continue;
      } 
      break;
    } 
    Set set = arrayMap1.keySet();
    if (set.removeAll(arrayMap2.keySet())) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package");
      stringBuilder.append(((Package)stringBuilder1).packageName);
      stringBuilder.append(" AndroidManifext.xml 'key-set' and 'public-key' names must be distinct.");
      paramArrayOfString[0] = stringBuilder.toString();
      this.mParseError = -108;
      return false;
    } 
    ((Package)stringBuilder1).mKeySetMapping = new ArrayMap();
    for (Map.Entry entry : arrayMap2.entrySet()) {
      StringBuilder stringBuilder;
      String str1 = (String)entry.getKey();
      if (((ArraySet)entry.getValue()).size() == 0) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Package");
        stringBuilder.append(((Package)stringBuilder1).packageName);
        stringBuilder.append(" AndroidManifext.xml 'key-set' ");
        stringBuilder.append(str1);
        stringBuilder.append(" has no valid associated 'public-key'. Not including in package's defined key-sets.");
        Slog.w("PackageParser", stringBuilder.toString());
        continue;
      } 
      if (arraySet1.contains(str1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Package");
        stringBuilder.append(((Package)stringBuilder1).packageName);
        stringBuilder.append(" AndroidManifext.xml 'key-set' ");
        stringBuilder.append(str1);
        stringBuilder.append(" contained improper 'public-key' tags. Not including in package's defined key-sets.");
        Slog.w("PackageParser", stringBuilder.toString());
        continue;
      } 
      ((Package)stringBuilder1).mKeySetMapping.put(str1, new ArraySet());
      for (String str2 : stringBuilder.getValue())
        ((ArraySet)((Package)stringBuilder1).mKeySetMapping.get(str1)).add(arrayMap1.get(str2)); 
    } 
    if (((Package)stringBuilder1).mKeySetMapping.keySet().containsAll((Collection<?>)arraySet)) {
      ((Package)stringBuilder1).mUpgradeKeySets = arraySet;
      return true;
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Package");
    stringBuilder2.append(((Package)stringBuilder1).packageName);
    stringBuilder2.append(" AndroidManifext.xml does not define all 'upgrade-key-set's .");
    paramArrayOfString[0] = stringBuilder2.toString();
    this.mParseError = -108;
    return false;
  }
  
  private boolean parsePermissionGroup(Package paramPackage, int paramInt, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermissionGroup);
    int i = typedArray.getResourceId(12, 0);
    paramInt = typedArray.getResourceId(9, 0);
    int j = typedArray.getResourceId(10, 0);
    PermissionGroup permissionGroup = new PermissionGroup(i, paramInt, j);
    if (!parsePackageItemInfo(paramPackage, permissionGroup.info, paramArrayOfString, "<permission-group>", typedArray, true, 2, 0, 1, 8, 5, 7)) {
      typedArray.recycle();
      this.mParseError = -108;
      return false;
    } 
    permissionGroup.info.descriptionRes = typedArray.getResourceId(4, 0);
    permissionGroup.info.requestRes = typedArray.getResourceId(11, 0);
    permissionGroup.info.flags = typedArray.getInt(6, 0);
    permissionGroup.info.priority = typedArray.getInt(3, 0);
    typedArray.recycle();
    if (!parseAllMetaData(paramResources, paramXmlResourceParser, "<permission-group>", permissionGroup, paramArrayOfString)) {
      this.mParseError = -108;
      return false;
    } 
    paramPackage.permissionGroups.add(permissionGroup);
    return true;
  }
  
  private boolean parsePermission(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermission);
    if (typedArray.hasValue(10)) {
      StringBuilder stringBuilder1, stringBuilder2;
      if ("android".equals(paramPackage.packageName)) {
        String str = typedArray.getNonResourceString(10);
      } else {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(paramPackage.packageName);
        stringBuilder2.append(" defines a background permission. Only the 'android' package can do that.");
        Slog.w("PackageParser", stringBuilder2.toString());
        stringBuilder2 = null;
      } 
      Permission permission = new Permission((String)stringBuilder2);
      if (!parsePackageItemInfo(paramPackage, permission.info, paramArrayOfString, "<permission>", typedArray, true, 2, 0, 1, 9, 6, 8)) {
        typedArray.recycle();
        this.mParseError = -108;
        return false;
      } 
      permission.info.group = typedArray.getNonResourceString(4);
      if (permission.info.group != null)
        permission.info.group = permission.info.group.intern(); 
      permission.info.descriptionRes = typedArray.getResourceId(5, 0);
      permission.info.requestRes = typedArray.getResourceId(11, 0);
      permission.info.protectionLevel = typedArray.getInt(3, 0);
      permission.info.flags = typedArray.getInt(7, 0);
      if (!permission.info.isRuntime() || !"android".equals(permission.info.packageName)) {
        PermissionInfo permissionInfo = permission.info;
        permissionInfo.flags &= 0xFFFFFFFB;
        permissionInfo = permission.info;
        permissionInfo.flags &= 0xFFFFFFF7;
      } else if ((permission.info.flags & 0x4) != 0 && (permission.info.flags & 0x8) != 0) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Permission cannot be both soft and hard restricted: ");
        stringBuilder1.append(permission.info.name);
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      typedArray.recycle();
      if (permission.info.protectionLevel == -1) {
        paramArrayOfString[0] = "<permission> does not specify protectionLevel";
        this.mParseError = -108;
        return false;
      } 
      permission.info.protectionLevel = PermissionInfo.fixProtectionLevel(permission.info.protectionLevel);
      if (permission.info.getProtectionFlags() != 0 && (
        permission.info.protectionLevel & 0x1000) == 0 && (permission.info.protectionLevel & 0x2000) == 0 && (permission.info.protectionLevel & 0xF) != 2) {
        paramArrayOfString[0] = "<permission>  protectionLevel specifies a non-instant flag but is not based on signature type";
        this.mParseError = -108;
        return false;
      } 
      if (!parseAllMetaData(paramResources, paramXmlResourceParser, "<permission>", permission, paramArrayOfString)) {
        this.mParseError = -108;
        return false;
      } 
      ((Package)stringBuilder1).permissions.add(permission);
      return true;
    } 
    Object object = null;
  }
  
  private boolean parsePermissionTree(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder;
    Permission permission = new Permission((String)null);
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermissionTree);
    if (!parsePackageItemInfo(paramPackage, permission.info, paramArrayOfString, "<permission-tree>", typedArray, true, 2, 0, 1, 5, 3, 4)) {
      typedArray.recycle();
      this.mParseError = -108;
      return false;
    } 
    typedArray.recycle();
    int i = permission.info.name.indexOf('.');
    if (i > 0)
      i = permission.info.name.indexOf('.', i + 1); 
    if (i < 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("<permission-tree> name has less than three segments: ");
      stringBuilder.append(permission.info.name);
      paramArrayOfString[0] = stringBuilder.toString();
      this.mParseError = -108;
      return false;
    } 
    permission.info.descriptionRes = 0;
    permission.info.requestRes = 0;
    permission.info.protectionLevel = 0;
    permission.tree = true;
    if (!parseAllMetaData(paramResources, paramXmlResourceParser, "<permission-tree>", permission, paramArrayOfString)) {
      this.mParseError = -108;
      return false;
    } 
    ((Package)stringBuilder).permissions.add(permission);
    return true;
  }
  
  private Instrumentation parseInstrumentation(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestInstrumentation);
    if (this.mParseInstrumentationArgs == null) {
      ParsePackageItemArgs parsePackageItemArgs = new ParsePackageItemArgs(paramPackage, paramArrayOfString, 2, 0, 1, 8, 6, 7);
      parsePackageItemArgs.tag = "<instrumentation>";
    } 
    this.mParseInstrumentationArgs.sa = typedArray;
    Instrumentation instrumentation = new Instrumentation(new InstrumentationInfo());
    if (paramArrayOfString[0] != null) {
      typedArray.recycle();
      this.mParseError = -108;
      return null;
    } 
    String str = typedArray.getNonResourceString(3);
    InstrumentationInfo instrumentationInfo = instrumentation.info;
    if (str != null) {
      str = str.intern();
    } else {
      str = null;
    } 
    instrumentationInfo.targetPackage = str;
    str = typedArray.getNonResourceString(9);
    instrumentationInfo = instrumentation.info;
    if (str != null) {
      str = str.intern();
    } else {
      str = null;
    } 
    instrumentationInfo.targetProcesses = str;
    instrumentation.info.handleProfiling = typedArray.getBoolean(4, false);
    instrumentation.info.functionalTest = typedArray.getBoolean(5, false);
    typedArray.recycle();
    if (instrumentation.info.targetPackage == null) {
      paramArrayOfString[0] = "<instrumentation> does not specify targetPackage";
      this.mParseError = -108;
      return null;
    } 
    if (!parseAllMetaData(paramResources, paramXmlResourceParser, "<instrumentation>", instrumentation, paramArrayOfString)) {
      this.mParseError = -108;
      return null;
    } 
    paramPackage.instrumentation.add(instrumentation);
    return instrumentation;
  }
  
  private boolean parseBaseApplication(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    StringBuilder stringBuilder;
    XmlResourceParser xmlResourceParser2;
    String str3;
    PackageParser packageParser = this;
    Package package_ = paramPackage;
    XmlResourceParser xmlResourceParser1 = paramXmlResourceParser;
    ApplicationInfo applicationInfo = package_.applicationInfo;
    String str1 = package_.applicationInfo.packageName;
    TypedArray typedArray = paramResources.obtainAttributes(xmlResourceParser1, R.styleable.AndroidManifestApplication);
    applicationInfo.iconRes = typedArray.getResourceId(2, 0);
    applicationInfo.roundIconRes = typedArray.getResourceId(42, 0);
    String[] arrayOfString = paramArrayOfString;
    if (!parsePackageItemInfo(paramPackage, applicationInfo, paramArrayOfString, "<application>", typedArray, false, 3, 1, 2, 42, 22, 30)) {
      typedArray.recycle();
      packageParser.mParseError = -108;
      return false;
    } 
    if (applicationInfo.name != null)
      applicationInfo.className = applicationInfo.name; 
    String str2 = typedArray.getNonConfigurationString(4, 1024);
    if (str2 != null)
      applicationInfo.manageSpaceActivityName = buildClassName(str1, str2, arrayOfString); 
    boolean bool = typedArray.getBoolean(17, true);
    if (bool) {
      applicationInfo.flags |= 0x8000;
      str2 = typedArray.getNonConfigurationString(16, 1024);
      if (str2 != null) {
        applicationInfo.backupAgentName = buildClassName(str1, str2, arrayOfString);
        if (typedArray.getBoolean(18, true))
          applicationInfo.flags |= 0x10000; 
        if (typedArray.getBoolean(21, false))
          applicationInfo.flags |= 0x20000; 
        if (typedArray.getBoolean(32, false))
          applicationInfo.flags |= 0x4000000; 
        if (typedArray.getBoolean(40, false))
          applicationInfo.privateFlags |= 0x2000; 
      } 
      TypedValue typedValue = typedArray.peekValue(35);
      if (typedValue != null) {
        int n = typedValue.resourceId;
        if (n == 0) {
          if (typedValue.data == 0) {
            n = -1;
          } else {
            n = 0;
          } 
          applicationInfo.fullBackupContent = n;
        } 
      } 
    } 
    applicationInfo.theme = typedArray.getResourceId(0, 0);
    applicationInfo.descriptionRes = typedArray.getResourceId(13, 0);
    if (typedArray.getBoolean(8, false)) {
      str2 = typedArray.getNonResourceString(45);
      if (str2 == null || packageParser.mCallback.hasFeature(str2))
        applicationInfo.flags |= 0x8; 
    } 
    if (typedArray.getBoolean(27, false))
      package_.mRequiredForAllUsers = true; 
    str2 = typedArray.getString(28);
    if (str2 != null && str2.length() > 0)
      package_.mRestrictedAccountType = str2; 
    str2 = typedArray.getString(29);
    if (str2 != null && str2.length() > 0)
      package_.mRequiredAccountType = str2; 
    if (typedArray.getBoolean(10, false)) {
      applicationInfo.flags |= 0x2;
      applicationInfo.privateFlags |= 0x800000;
    } 
    if (typedArray.getBoolean(20, false))
      applicationInfo.flags |= 0x4000; 
    if (package_.applicationInfo.targetSdkVersion >= 14) {
      bool = true;
    } else {
      bool = false;
    } 
    package_.baseHardwareAccelerated = typedArray.getBoolean(23, bool);
    if (package_.baseHardwareAccelerated)
      applicationInfo.flags |= 0x20000000; 
    if (typedArray.getBoolean(7, true))
      applicationInfo.flags |= 0x4; 
    if (typedArray.getBoolean(14, false))
      applicationInfo.flags |= 0x20; 
    if (typedArray.getBoolean(5, true))
      applicationInfo.flags |= 0x40; 
    if (package_.parentPackage == null && 
      typedArray.getBoolean(15, false))
      applicationInfo.flags |= 0x100; 
    if (typedArray.getBoolean(24, false))
      applicationInfo.flags |= 0x100000; 
    if (package_.applicationInfo.targetSdkVersion < 28) {
      bool = true;
    } else {
      bool = false;
    } 
    if (typedArray.getBoolean(36, bool))
      applicationInfo.flags |= 0x8000000; 
    if (typedArray.getBoolean(26, false))
      applicationInfo.flags |= 0x400000; 
    if (typedArray.getBoolean(33, false))
      applicationInfo.flags |= Integer.MIN_VALUE; 
    if (typedArray.getBoolean(34, true))
      applicationInfo.flags |= 0x10000000; 
    if (typedArray.getBoolean(53, false))
      applicationInfo.privateFlags |= 0x2000000; 
    if (typedArray.getBoolean(38, false))
      applicationInfo.privateFlags |= 0x20; 
    if (typedArray.getBoolean(39, false))
      applicationInfo.privateFlags |= 0x40; 
    if (typedArray.hasValueOrEmpty(37)) {
      if (typedArray.getBoolean(37, true)) {
        applicationInfo.privateFlags |= 0x400;
      } else {
        applicationInfo.privateFlags |= 0x800;
      } 
    } else if (package_.applicationInfo.targetSdkVersion >= 24) {
      applicationInfo.privateFlags |= 0x1000;
    } 
    if (typedArray.getBoolean(54, true))
      applicationInfo.privateFlags |= 0x4000000; 
    if (package_.applicationInfo.targetSdkVersion >= 29) {
      bool = true;
    } else {
      bool = false;
    } 
    if (typedArray.getBoolean(55, bool))
      applicationInfo.privateFlags |= 0x8000000; 
    if (package_.applicationInfo.targetSdkVersion < 29) {
      bool = true;
    } else {
      bool = false;
    } 
    if (typedArray.getBoolean(56, bool))
      applicationInfo.privateFlags |= 0x20000000; 
    if (typedArray.getBoolean(59, true))
      applicationInfo.privateFlags |= Integer.MIN_VALUE; 
    applicationInfo.maxAspectRatio = typedArray.getFloat(44, 0.0F);
    applicationInfo.minAspectRatio = typedArray.getFloat(51, 0.0F);
    applicationInfo.networkSecurityConfigRes = typedArray.getResourceId(41, 0);
    applicationInfo.category = typedArray.getInt(43, -1);
    str2 = typedArray.getNonConfigurationString(6, 0);
    if (str2 != null && str2.length() > 0) {
      str2 = str2.intern();
    } else {
      str2 = null;
    } 
    applicationInfo.permission = str2;
    if (package_.applicationInfo.targetSdkVersion >= 8) {
      str3 = typedArray.getNonConfigurationString(12, 1024);
    } else {
      str3 = typedArray.getNonResourceString(12);
    } 
    applicationInfo.taskAffinity = buildTaskAffinityName(applicationInfo.packageName, applicationInfo.packageName, str3, arrayOfString);
    String str4 = typedArray.getNonResourceString(48);
    if (str4 != null)
      applicationInfo.appComponentFactory = buildClassName(applicationInfo.packageName, str4, arrayOfString); 
    if (typedArray.getBoolean(49, false))
      applicationInfo.privateFlags |= 0x400000; 
    if (typedArray.getBoolean(50, false))
      applicationInfo.privateFlags |= 0x1000000; 
    if (arrayOfString[0] == null) {
      if (package_.applicationInfo.targetSdkVersion >= 8) {
        str2 = typedArray.getNonConfigurationString(11, 1024);
      } else {
        str2 = typedArray.getNonResourceString(11);
      } 
      applicationInfo.processName = buildProcessName(applicationInfo.packageName, null, str2, paramInt, packageParser.mSeparateProcesses, paramArrayOfString);
      applicationInfo.enabled = typedArray.getBoolean(9, true);
      if (typedArray.getBoolean(31, false))
        applicationInfo.flags |= 0x2000000; 
      if (typedArray.getBoolean(47, false)) {
        applicationInfo.privateFlags |= 0x2;
        if (applicationInfo.processName != null && !applicationInfo.processName.equals(applicationInfo.packageName))
          arrayOfString[0] = "cantSaveState applications can not use custom processes"; 
      } 
    } 
    str2 = str1;
    applicationInfo.uiOptions = typedArray.getInt(25, 0);
    applicationInfo.classLoaderName = typedArray.getString(46);
    if (applicationInfo.classLoaderName != null) {
      str1 = applicationInfo.classLoaderName;
      if (!ClassLoaderFactory.isValidClassLoaderName(str1)) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Invalid class loader name: ");
        stringBuilder1.append(applicationInfo.classLoaderName);
        arrayOfString[0] = stringBuilder1.toString();
      } 
    } 
    applicationInfo.zygotePreloadName = typedArray.getString(52);
    typedArray.recycle();
    if (arrayOfString[0] != null) {
      packageParser.mParseError = -108;
      return false;
    } 
    int i = paramXmlResourceParser.getDepth();
    CachedComponentArgs cachedComponentArgs = new CachedComponentArgs();
    int j = 0, k = 0, m = 0;
    str1 = str4;
    while (true) {
      PackageParser packageParser1;
      XmlResourceParser xmlResourceParser;
      String[] arrayOfString3;
      int n = paramXmlResourceParser.next();
      if (n != 1 && (n != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        String[] arrayOfString4;
        XmlResourceParser xmlResourceParser3;
        if (n != 3) {
          if (n == 4) {
            XmlResourceParser xmlResourceParser4 = xmlResourceParser1;
            arrayOfString4 = arrayOfString;
            xmlResourceParser3 = xmlResourceParser4;
          } else {
            PackageParser packageParser3;
            Package package_1;
            String str = paramXmlResourceParser.getName();
            if (str.equals("activity")) {
              Activity activity = parseActivity(paramPackage, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString, cachedComponentArgs, false, package_.baseHardwareAccelerated);
              if (activity == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              if (activity.order != 0) {
                n = 1;
              } else {
                n = 0;
              } 
              package_.activities.add(activity);
              j |= n;
              String[] arrayOfString6 = arrayOfString4;
              xmlResourceParser = xmlResourceParser3;
              arrayOfString3 = arrayOfString6;
              package_1 = package_;
            } else if (package_1.equals("receiver")) {
              Activity activity = parseActivity(paramPackage, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString, cachedComponentArgs, true, false);
              if (activity == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              if (activity.order != 0) {
                n = 1;
              } else {
                n = 0;
              } 
              package_1 = paramPackage;
              package_1.receivers.add(activity);
              XmlResourceParser xmlResourceParser4 = paramXmlResourceParser;
              arrayOfString4 = paramArrayOfString;
              k |= n;
            } else if (package_1.equals("service")) {
              Service service = parseService(paramPackage, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString, cachedComponentArgs);
              if (service == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              if (service.order != 0) {
                n = 1;
              } else {
                n = 0;
              } 
              package_.services.add(service);
              XmlResourceParser xmlResourceParser4 = paramXmlResourceParser;
              arrayOfString4 = paramArrayOfString;
              m |= n;
              package_1 = package_;
            } else if (package_1.equals("provider")) {
              Provider provider = parseProvider(paramPackage, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString, cachedComponentArgs);
              if (provider == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              package_.providers.add(provider);
              XmlResourceParser xmlResourceParser4 = paramXmlResourceParser;
              arrayOfString4 = paramArrayOfString;
              package_1 = package_;
            } else if (package_1.equals("activity-alias")) {
              Activity activity = parseActivityAlias(paramPackage, paramResources, paramXmlResourceParser, paramInt, paramArrayOfString, cachedComponentArgs);
              if (activity == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              if (activity.order != 0) {
                n = 1;
              } else {
                n = 0;
              } 
              package_.activities.add(activity);
              XmlResourceParser xmlResourceParser4 = paramXmlResourceParser;
              arrayOfString4 = paramArrayOfString;
              j |= n;
              package_1 = package_;
            } else if (paramXmlResourceParser.getName().equals("meta-data")) {
              Bundle bundle = package_.mAppMetaData;
              xmlResourceParser3 = paramXmlResourceParser;
              arrayOfString4 = paramArrayOfString;
              package_.mAppMetaData = bundle = packageParser.parseMetaData(paramResources, xmlResourceParser3, bundle, arrayOfString4);
              if (bundle == null) {
                packageParser.mParseError = -108;
                return false;
              } 
              package_1 = package_;
            } else {
              XmlResourceParser xmlResourceParser4 = paramXmlResourceParser;
              String[] arrayOfString6 = paramArrayOfString;
              if (package_1.equals("static-library")) {
                TypedArray typedArray1 = paramResources.obtainAttributes(xmlResourceParser4, R.styleable.AndroidManifestStaticLibrary);
                String str5 = typedArray1.getNonResourceString(0);
                int i1 = typedArray1.getInt(1, -1);
                n = typedArray1.getInt(2, 0);
                typedArray1.recycle();
                if (str5 == null || i1 < 0) {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Bad static-library declaration name: ");
                  stringBuilder.append(str5);
                  stringBuilder.append(" version: ");
                  stringBuilder.append(i1);
                  arrayOfString6[0] = stringBuilder.toString();
                  packageParser.mParseError = -108;
                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                  return false;
                } 
                if (package_.mSharedUserId != null) {
                  arrayOfString6[0] = "sharedUserId not allowed in static shared library";
                  packageParser.mParseError = -107;
                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                  return false;
                } 
                if (package_.staticSharedLibName != null) {
                  stringBuilder = new StringBuilder();
                  stringBuilder.append("Multiple static-shared libs for package ");
                  stringBuilder.append(str2);
                  arrayOfString6[0] = stringBuilder.toString();
                  packageParser.mParseError = -108;
                  XmlUtils.skipCurrentTag(paramXmlResourceParser);
                  return false;
                } 
                package_.staticSharedLibName = str5.intern();
                if (i1 >= 0) {
                  package_.staticSharedLibVersion = PackageInfo.composeLongVersionCode(n, i1);
                } else {
                  package_.staticSharedLibVersion = i1;
                } 
                applicationInfo.privateFlags |= 0x4000;
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                XmlResourceParser xmlResourceParser5 = xmlResourceParser4;
                String[] arrayOfString7 = arrayOfString6;
                package_1 = package_;
              } else if (package_1.equals("library")) {
                TypedArray typedArray1 = paramResources.obtainAttributes(xmlResourceParser4, R.styleable.AndroidManifestLibrary);
                String str5 = typedArray1.getNonResourceString(0);
                typedArray1.recycle();
                if (str5 != null) {
                  str5 = str5.intern();
                  if (!ArrayUtils.contains(package_.libraryNames, str5))
                    package_.libraryNames = ArrayUtils.add(package_.libraryNames, str5); 
                } 
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                XmlResourceParser xmlResourceParser5 = xmlResourceParser4;
                String[] arrayOfString7 = arrayOfString6;
                package_1 = package_;
              } else if (package_1.equals("uses-static-library")) {
                xmlResourceParser3 = xmlResourceParser4;
                arrayOfString4 = arrayOfString6;
                package_1 = package_;
                if (!packageParser.parseUsesStaticLibrary(package_, paramResources, xmlResourceParser4, arrayOfString6))
                  return false; 
              } else if (package_1.equals("uses-library")) {
                TypedArray typedArray1 = paramResources.obtainAttributes(xmlResourceParser4, R.styleable.AndroidManifestUsesLibrary);
                String str5 = typedArray1.getNonResourceString(0);
                bool = typedArray1.getBoolean(1, true);
                typedArray1.recycle();
                if (str5 != null) {
                  String str6 = str5.intern();
                  if (bool) {
                    package_.usesLibraries = ArrayUtils.add(package_.usesLibraries, str6);
                  } else {
                    package_.usesOptionalLibraries = ArrayUtils.add(package_.usesOptionalLibraries, str6);
                  } 
                } 
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                XmlResourceParser xmlResourceParser5 = xmlResourceParser4;
                String[] arrayOfString7 = arrayOfString6;
                package_1 = package_;
              } else if (package_1.equals("uses-package")) {
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                xmlResourceParser3 = xmlResourceParser4;
                arrayOfString4 = arrayOfString6;
                package_1 = package_;
              } else if (package_1.equals("profileable")) {
                TypedArray typedArray1 = paramResources.obtainAttributes(xmlResourceParser4, R.styleable.AndroidManifestProfileable);
                if (typedArray1.getBoolean(0, false))
                  applicationInfo.privateFlags |= 0x800000; 
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                package_1 = package_;
                arrayOfString4 = arrayOfString6;
                XmlResourceParser xmlResourceParser5 = xmlResourceParser4;
              } else {
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("Unknown element under <application>: ");
                stringBuilder1.append((String)package_1);
                stringBuilder1.append(" at ");
                stringBuilder1.append(packageParser.mArchiveSourcePath);
                stringBuilder1.append(" ");
                stringBuilder1.append(paramXmlResourceParser.getPositionDescription());
                String str5 = stringBuilder1.toString();
                Slog.w("PackageParser", str5);
                XmlUtils.skipCurrentTag(paramXmlResourceParser);
                XmlResourceParser xmlResourceParser6 = xmlResourceParser4;
                arrayOfString4 = arrayOfString6;
                PackageParser packageParser5 = packageParser;
                XmlResourceParser xmlResourceParser5 = xmlResourceParser6;
                arrayOfString3 = arrayOfString4;
                xmlResourceParser = xmlResourceParser5;
                packageParser3 = packageParser5;
              } 
            } 
            PackageParser packageParser4 = packageParser3;
            String[] arrayOfString5 = arrayOfString3;
            xmlResourceParser3 = xmlResourceParser;
            arrayOfString4 = arrayOfString5;
            package_ = package_1;
            packageParser1 = packageParser4;
            continue;
          } 
        } else {
          String[] arrayOfString5 = arrayOfString4;
          xmlResourceParser = xmlResourceParser3;
          arrayOfString3 = arrayOfString5;
        } 
      } else {
        break;
      } 
      PackageParser packageParser2 = packageParser1;
      String[] arrayOfString1 = arrayOfString3;
      xmlResourceParser2 = xmlResourceParser;
      String[] arrayOfString2 = arrayOfString1;
      packageParser = packageParser2;
    } 
    if (TextUtils.isEmpty(package_.staticSharedLibName)) {
      Activity activity = packageParser.generateAppDetailsHiddenActivity(package_, paramInt, (String[])xmlResourceParser2, package_.baseHardwareAccelerated);
      package_.activities.add(activity);
    } 
    if (j != 0)
      Collections.sort(package_.activities, (Comparator<? super Activity>)_$$Lambda$PackageParser$0aobsT7Zf7WVZCqMZ5z2clAuQf4.INSTANCE); 
    if (k != 0)
      Collections.sort(package_.receivers, (Comparator<? super Activity>)_$$Lambda$PackageParser$0DZRgzfgaIMpCOhJqjw6PUiU5vw.INSTANCE); 
    if (m != 0)
      Collections.sort(package_.services, (Comparator<? super Service>)_$$Lambda$PackageParser$M_9fHqS_eEp1oYkuKJhRHOGUxf8.INSTANCE); 
    setMaxAspectRatio((Package)stringBuilder);
    setMinAspectRatio((Package)stringBuilder);
    setSupportsSizeChanges((Package)stringBuilder);
    if (hasDomainURLs((Package)stringBuilder)) {
      ApplicationInfo applicationInfo1 = package_.applicationInfo;
      applicationInfo1.privateFlags |= 0x10;
    } else {
      ApplicationInfo applicationInfo1 = package_.applicationInfo;
      applicationInfo1.privateFlags &= 0xFFFFFFEF;
    } 
    return true;
  }
  
  private static boolean hasDomainURLs(Package paramPackage) {
    if (paramPackage == null || paramPackage.activities == null)
      return false; 
    ArrayList<Activity> arrayList = paramPackage.activities;
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      Activity activity = arrayList.get(b);
      ArrayList<ActivityIntentInfo> arrayList1 = activity.intents;
      if (arrayList1 != null) {
        int j = arrayList1.size();
        for (byte b1 = 0; b1 < j; b1++) {
          ActivityIntentInfo activityIntentInfo = arrayList1.get(b1);
          if (activityIntentInfo.hasAction("android.intent.action.VIEW") && 
            activityIntentInfo.hasAction("android.intent.action.VIEW") && (
            activityIntentInfo.hasDataScheme("http") || 
            activityIntentInfo.hasDataScheme("https")))
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  private boolean parseSplitApplication(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt1, int paramInt2, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_0
    //   1: astore #7
    //   3: aload_1
    //   4: astore #8
    //   6: aload_2
    //   7: astore #9
    //   9: aload_3
    //   10: astore #10
    //   12: aload #6
    //   14: astore #11
    //   16: aload #9
    //   18: aload #10
    //   20: getstatic com/android/internal/R$styleable.AndroidManifestApplication : [I
    //   23: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   26: astore #12
    //   28: aload #12
    //   30: bipush #7
    //   32: iconst_1
    //   33: invokevirtual getBoolean : (IZ)Z
    //   36: ifeq -> 58
    //   39: aload #8
    //   41: getfield splitFlags : [I
    //   44: astore #13
    //   46: aload #13
    //   48: iload #5
    //   50: aload #13
    //   52: iload #5
    //   54: iaload
    //   55: iconst_4
    //   56: ior
    //   57: iastore
    //   58: aload #12
    //   60: bipush #46
    //   62: invokevirtual getString : (I)Ljava/lang/String;
    //   65: astore #12
    //   67: bipush #-108
    //   69: istore #14
    //   71: iconst_0
    //   72: istore #15
    //   74: aload #12
    //   76: ifnull -> 130
    //   79: aload #12
    //   81: invokestatic isValidClassLoaderName : (Ljava/lang/String;)Z
    //   84: ifeq -> 90
    //   87: goto -> 130
    //   90: new java/lang/StringBuilder
    //   93: dup
    //   94: invokespecial <init> : ()V
    //   97: astore_1
    //   98: aload_1
    //   99: ldc_w 'Invalid class loader name: '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_1
    //   107: aload #12
    //   109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   112: pop
    //   113: aload #11
    //   115: iconst_0
    //   116: aload_1
    //   117: invokevirtual toString : ()Ljava/lang/String;
    //   120: aastore
    //   121: aload #7
    //   123: bipush #-108
    //   125: putfield mParseError : I
    //   128: iconst_0
    //   129: ireturn
    //   130: aload #8
    //   132: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   135: getfield splitClassLoaderNames : [Ljava/lang/String;
    //   138: iload #5
    //   140: aload #12
    //   142: aastore
    //   143: aload_3
    //   144: invokeinterface getDepth : ()I
    //   149: istore #16
    //   151: aload_3
    //   152: invokeinterface next : ()I
    //   157: istore #17
    //   159: iload #17
    //   161: iconst_1
    //   162: if_icmpeq -> 1027
    //   165: iload #17
    //   167: iconst_3
    //   168: if_icmpne -> 188
    //   171: aload_3
    //   172: invokeinterface getDepth : ()I
    //   177: iload #16
    //   179: if_icmple -> 185
    //   182: goto -> 188
    //   185: goto -> 1027
    //   188: iload #17
    //   190: iconst_3
    //   191: if_icmpeq -> 1000
    //   194: iload #17
    //   196: iconst_4
    //   197: if_icmpne -> 215
    //   200: aload #11
    //   202: astore #13
    //   204: aload #9
    //   206: astore #11
    //   208: aload #13
    //   210: astore #9
    //   212: goto -> 1012
    //   215: new android/content/pm/PackageParser$CachedComponentArgs
    //   218: dup
    //   219: aconst_null
    //   220: invokespecial <init> : (Landroid/content/pm/PackageParser$1;)V
    //   223: astore #13
    //   225: aload_3
    //   226: invokeinterface getName : ()Ljava/lang/String;
    //   231: astore #9
    //   233: aload #9
    //   235: ldc_w 'activity'
    //   238: invokevirtual equals : (Ljava/lang/Object;)Z
    //   241: ifeq -> 303
    //   244: aload_0
    //   245: aload_1
    //   246: aload_2
    //   247: aload_3
    //   248: iload #4
    //   250: aload #6
    //   252: aload #13
    //   254: iconst_0
    //   255: aload #8
    //   257: getfield baseHardwareAccelerated : Z
    //   260: invokespecial parseActivity : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;Landroid/content/pm/PackageParser$CachedComponentArgs;ZZ)Landroid/content/pm/PackageParser$Activity;
    //   263: astore #9
    //   265: aload #9
    //   267: ifnonnull -> 279
    //   270: aload #7
    //   272: iload #14
    //   274: putfield mParseError : I
    //   277: iconst_0
    //   278: ireturn
    //   279: aload #8
    //   281: getfield activities : Ljava/util/ArrayList;
    //   284: aload #9
    //   286: invokevirtual add : (Ljava/lang/Object;)Z
    //   289: pop
    //   290: aload #9
    //   292: getfield info : Landroid/content/pm/ActivityInfo;
    //   295: astore #9
    //   297: iconst_0
    //   298: istore #15
    //   300: goto -> 875
    //   303: aload #9
    //   305: ldc_w 'receiver'
    //   308: invokevirtual equals : (Ljava/lang/Object;)Z
    //   311: ifeq -> 377
    //   314: aload_0
    //   315: aload_1
    //   316: aload_2
    //   317: aload_3
    //   318: iload #4
    //   320: aload #6
    //   322: aload #13
    //   324: iconst_1
    //   325: iconst_0
    //   326: invokespecial parseActivity : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;Landroid/content/pm/PackageParser$CachedComponentArgs;ZZ)Landroid/content/pm/PackageParser$Activity;
    //   329: astore #11
    //   331: aload #11
    //   333: ifnonnull -> 345
    //   336: aload #7
    //   338: bipush #-108
    //   340: putfield mParseError : I
    //   343: iconst_0
    //   344: ireturn
    //   345: bipush #-108
    //   347: istore #14
    //   349: iconst_0
    //   350: istore #15
    //   352: aload #8
    //   354: getfield receivers : Ljava/util/ArrayList;
    //   357: aload #11
    //   359: invokevirtual add : (Ljava/lang/Object;)Z
    //   362: pop
    //   363: aload #11
    //   365: getfield info : Landroid/content/pm/ActivityInfo;
    //   368: astore #9
    //   370: aload #6
    //   372: astore #11
    //   374: goto -> 875
    //   377: aload_2
    //   378: astore #11
    //   380: iload #15
    //   382: istore #17
    //   384: iload #14
    //   386: istore #18
    //   388: aload #9
    //   390: ldc_w 'service'
    //   393: invokevirtual equals : (Ljava/lang/Object;)Z
    //   396: ifeq -> 462
    //   399: aload_0
    //   400: aload_1
    //   401: aload_2
    //   402: aload_3
    //   403: iload #4
    //   405: aload #6
    //   407: aload #13
    //   409: invokespecial parseService : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;Landroid/content/pm/PackageParser$CachedComponentArgs;)Landroid/content/pm/PackageParser$Service;
    //   412: astore #11
    //   414: aload #11
    //   416: ifnonnull -> 429
    //   419: aload #7
    //   421: iload #18
    //   423: putfield mParseError : I
    //   426: iload #17
    //   428: ireturn
    //   429: aload #8
    //   431: getfield services : Ljava/util/ArrayList;
    //   434: aload #11
    //   436: invokevirtual add : (Ljava/lang/Object;)Z
    //   439: pop
    //   440: aload #11
    //   442: getfield info : Landroid/content/pm/ServiceInfo;
    //   445: astore #9
    //   447: aload #6
    //   449: astore #11
    //   451: iload #18
    //   453: istore #14
    //   455: iload #17
    //   457: istore #15
    //   459: goto -> 875
    //   462: aload #9
    //   464: ldc_w 'provider'
    //   467: invokevirtual equals : (Ljava/lang/Object;)Z
    //   470: ifeq -> 536
    //   473: aload_0
    //   474: aload_1
    //   475: aload_2
    //   476: aload_3
    //   477: iload #4
    //   479: aload #6
    //   481: aload #13
    //   483: invokespecial parseProvider : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;Landroid/content/pm/PackageParser$CachedComponentArgs;)Landroid/content/pm/PackageParser$Provider;
    //   486: astore #11
    //   488: aload #11
    //   490: ifnonnull -> 503
    //   493: aload #7
    //   495: iload #18
    //   497: putfield mParseError : I
    //   500: iload #17
    //   502: ireturn
    //   503: aload #8
    //   505: getfield providers : Ljava/util/ArrayList;
    //   508: aload #11
    //   510: invokevirtual add : (Ljava/lang/Object;)Z
    //   513: pop
    //   514: aload #11
    //   516: getfield info : Landroid/content/pm/ProviderInfo;
    //   519: astore #9
    //   521: aload #6
    //   523: astore #11
    //   525: iload #18
    //   527: istore #14
    //   529: iload #17
    //   531: istore #15
    //   533: goto -> 875
    //   536: aload #9
    //   538: ldc_w 'activity-alias'
    //   541: invokevirtual equals : (Ljava/lang/Object;)Z
    //   544: ifeq -> 610
    //   547: aload_0
    //   548: aload_1
    //   549: aload_2
    //   550: aload_3
    //   551: iload #4
    //   553: aload #6
    //   555: aload #13
    //   557: invokespecial parseActivityAlias : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I[Ljava/lang/String;Landroid/content/pm/PackageParser$CachedComponentArgs;)Landroid/content/pm/PackageParser$Activity;
    //   560: astore #11
    //   562: aload #11
    //   564: ifnonnull -> 577
    //   567: aload #7
    //   569: iload #18
    //   571: putfield mParseError : I
    //   574: iload #17
    //   576: ireturn
    //   577: aload #8
    //   579: getfield activities : Ljava/util/ArrayList;
    //   582: aload #11
    //   584: invokevirtual add : (Ljava/lang/Object;)Z
    //   587: pop
    //   588: aload #11
    //   590: getfield info : Landroid/content/pm/ActivityInfo;
    //   593: astore #9
    //   595: aload #6
    //   597: astore #11
    //   599: iload #18
    //   601: istore #14
    //   603: iload #17
    //   605: istore #15
    //   607: goto -> 875
    //   610: aload_3
    //   611: invokeinterface getName : ()Ljava/lang/String;
    //   616: ldc_w 'meta-data'
    //   619: invokevirtual equals : (Ljava/lang/Object;)Z
    //   622: ifeq -> 665
    //   625: aload #7
    //   627: aload #11
    //   629: aload #10
    //   631: aload #8
    //   633: getfield mAppMetaData : Landroid/os/Bundle;
    //   636: aload #6
    //   638: invokespecial parseMetaData : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    //   641: astore #11
    //   643: aload #8
    //   645: aload #11
    //   647: putfield mAppMetaData : Landroid/os/Bundle;
    //   650: aload #11
    //   652: ifnonnull -> 860
    //   655: aload #7
    //   657: iload #18
    //   659: putfield mParseError : I
    //   662: iload #17
    //   664: ireturn
    //   665: aload #6
    //   667: astore #13
    //   669: aload #9
    //   671: ldc_w 'uses-static-library'
    //   674: invokevirtual equals : (Ljava/lang/Object;)Z
    //   677: ifeq -> 699
    //   680: aload #7
    //   682: aload #8
    //   684: aload #11
    //   686: aload #10
    //   688: aload #13
    //   690: invokespecial parseUsesStaticLibrary : (Landroid/content/pm/PackageParser$Package;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;[Ljava/lang/String;)Z
    //   693: ifne -> 860
    //   696: iload #17
    //   698: ireturn
    //   699: aload #9
    //   701: ldc_w 'uses-library'
    //   704: invokevirtual equals : (Ljava/lang/Object;)Z
    //   707: ifeq -> 845
    //   710: aload #11
    //   712: aload #10
    //   714: getstatic com/android/internal/R$styleable.AndroidManifestUsesLibrary : [I
    //   717: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   720: astore #9
    //   722: aload #9
    //   724: iload #17
    //   726: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   729: astore #11
    //   731: aload #9
    //   733: iconst_1
    //   734: iconst_1
    //   735: invokevirtual getBoolean : (IZ)Z
    //   738: istore #19
    //   740: aload #9
    //   742: invokevirtual recycle : ()V
    //   745: aload #11
    //   747: ifnull -> 823
    //   750: aload #11
    //   752: invokevirtual intern : ()Ljava/lang/String;
    //   755: astore #11
    //   757: iload #19
    //   759: ifeq -> 795
    //   762: aload #8
    //   764: aload #8
    //   766: getfield usesLibraries : Ljava/util/ArrayList;
    //   769: aload #11
    //   771: invokestatic add : (Ljava/util/ArrayList;Ljava/lang/Object;)Ljava/util/ArrayList;
    //   774: putfield usesLibraries : Ljava/util/ArrayList;
    //   777: aload #8
    //   779: aload #8
    //   781: getfield usesOptionalLibraries : Ljava/util/ArrayList;
    //   784: aload #11
    //   786: invokestatic remove : (Ljava/util/ArrayList;Ljava/lang/Object;)Ljava/util/ArrayList;
    //   789: putfield usesOptionalLibraries : Ljava/util/ArrayList;
    //   792: goto -> 823
    //   795: aload #8
    //   797: getfield usesLibraries : Ljava/util/ArrayList;
    //   800: aload #11
    //   802: invokestatic contains : (Ljava/util/Collection;Ljava/lang/Object;)Z
    //   805: ifne -> 823
    //   808: aload #8
    //   810: aload #8
    //   812: getfield usesOptionalLibraries : Ljava/util/ArrayList;
    //   815: aload #11
    //   817: invokestatic add : (Ljava/util/ArrayList;Ljava/lang/Object;)Ljava/util/ArrayList;
    //   820: putfield usesOptionalLibraries : Ljava/util/ArrayList;
    //   823: aload_3
    //   824: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   827: aconst_null
    //   828: astore #9
    //   830: aload #13
    //   832: astore #11
    //   834: iload #18
    //   836: istore #14
    //   838: iload #17
    //   840: istore #15
    //   842: goto -> 875
    //   845: aload #9
    //   847: ldc_w 'uses-package'
    //   850: invokevirtual equals : (Ljava/lang/Object;)Z
    //   853: ifeq -> 907
    //   856: aload_3
    //   857: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   860: aload #6
    //   862: astore #11
    //   864: aconst_null
    //   865: astore #9
    //   867: iload #17
    //   869: istore #15
    //   871: iload #18
    //   873: istore #14
    //   875: aload #9
    //   877: ifnull -> 901
    //   880: aload #9
    //   882: getfield splitName : Ljava/lang/String;
    //   885: ifnonnull -> 901
    //   888: aload #9
    //   890: aload #8
    //   892: getfield splitNames : [Ljava/lang/String;
    //   895: iload #5
    //   897: aaload
    //   898: putfield splitName : Ljava/lang/String;
    //   901: aload_2
    //   902: astore #9
    //   904: goto -> 151
    //   907: new java/lang/StringBuilder
    //   910: dup
    //   911: invokespecial <init> : ()V
    //   914: astore #20
    //   916: aload #20
    //   918: ldc_w 'Unknown element under <application>: '
    //   921: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   924: pop
    //   925: aload #20
    //   927: aload #9
    //   929: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   932: pop
    //   933: aload #20
    //   935: ldc_w ' at '
    //   938: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   941: pop
    //   942: aload #20
    //   944: aload #7
    //   946: getfield mArchiveSourcePath : Ljava/lang/String;
    //   949: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   952: pop
    //   953: aload #20
    //   955: ldc_w ' '
    //   958: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   961: pop
    //   962: aload #20
    //   964: aload_3
    //   965: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   970: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   973: pop
    //   974: aload #20
    //   976: invokevirtual toString : ()Ljava/lang/String;
    //   979: astore #9
    //   981: ldc 'PackageParser'
    //   983: aload #9
    //   985: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   988: pop
    //   989: aload_3
    //   990: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   993: aload #13
    //   995: astore #9
    //   997: goto -> 1012
    //   1000: aload #9
    //   1002: astore #13
    //   1004: aload #11
    //   1006: astore #9
    //   1008: aload #13
    //   1010: astore #11
    //   1012: aload #11
    //   1014: astore #13
    //   1016: aload #9
    //   1018: astore #11
    //   1020: aload #13
    //   1022: astore #9
    //   1024: goto -> 151
    //   1027: iconst_1
    //   1028: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4006	-> 0
    //   #4009	-> 28
    //   #4011	-> 39
    //   #4014	-> 58
    //   #4016	-> 67
    //   #4019	-> 90
    //   #4020	-> 121
    //   #4021	-> 128
    //   #4017	-> 130
    //   #4024	-> 143
    //   #4026	-> 151
    //   #4027	-> 171
    //   #4028	-> 188
    //   #4029	-> 200
    //   #4032	-> 215
    //   #4036	-> 215
    //   #4037	-> 225
    //   #4038	-> 233
    //   #4039	-> 244
    //   #4041	-> 265
    //   #4042	-> 270
    //   #4043	-> 277
    //   #4046	-> 279
    //   #4047	-> 290
    //   #4049	-> 297
    //   #4050	-> 314
    //   #4052	-> 331
    //   #4053	-> 336
    //   #4054	-> 343
    //   #4057	-> 345
    //   #4058	-> 363
    //   #4060	-> 370
    //   #4061	-> 399
    //   #4062	-> 414
    //   #4063	-> 419
    //   #4064	-> 426
    //   #4067	-> 429
    //   #4068	-> 440
    //   #4070	-> 447
    //   #4071	-> 473
    //   #4072	-> 488
    //   #4073	-> 493
    //   #4074	-> 500
    //   #4077	-> 503
    //   #4078	-> 514
    //   #4080	-> 521
    //   #4081	-> 547
    //   #4082	-> 562
    //   #4083	-> 567
    //   #4084	-> 574
    //   #4087	-> 577
    //   #4088	-> 588
    //   #4090	-> 595
    //   #4094	-> 625
    //   #4096	-> 655
    //   #4097	-> 662
    //   #4100	-> 665
    //   #4101	-> 680
    //   #4102	-> 696
    //   #4105	-> 699
    //   #4106	-> 710
    //   #4111	-> 722
    //   #4113	-> 731
    //   #4117	-> 740
    //   #4119	-> 745
    //   #4120	-> 750
    //   #4121	-> 757
    //   #4123	-> 762
    //   #4124	-> 777
    //   #4128	-> 795
    //   #4129	-> 808
    //   #4135	-> 823
    //   #4137	-> 827
    //   #4140	-> 856
    //   #4156	-> 860
    //   #4161	-> 888
    //   #4163	-> 901
    //   #4144	-> 907
    //   #4146	-> 962
    //   #4144	-> 981
    //   #4147	-> 989
    //   #4148	-> 993
    //   #4028	-> 1000
    //   #4026	-> 1012
    //   #4165	-> 1027
  }
  
  private static boolean parsePackageItemInfo(Package paramPackage, PackageItemInfo paramPackageItemInfo, String[] paramArrayOfString, String paramString, TypedArray paramTypedArray, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    StringBuilder stringBuilder;
    if (paramTypedArray == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" does not contain any attributes");
      paramArrayOfString[0] = stringBuilder.toString();
      return false;
    } 
    String str = paramTypedArray.getNonConfigurationString(paramInt1, 0);
    if (str == null) {
      if (paramBoolean) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append(" does not specify android:name");
        paramArrayOfString[0] = stringBuilder.toString();
        return false;
      } 
    } else {
      String str1 = ((Package)stringBuilder).applicationInfo.packageName;
      str1 = buildClassName(str1, str, paramArrayOfString);
      if (PackageManager.APP_DETAILS_ACTIVITY_CLASS_NAME.equals(str1)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append(" invalid android:name");
        paramArrayOfString[0] = stringBuilder.toString();
        return false;
      } 
      paramPackageItemInfo.name = str1;
      if (str1 == null)
        return false; 
    } 
    if (sUseRoundIcon) {
      paramInt1 = paramTypedArray.getResourceId(paramInt4, 0);
    } else {
      paramInt1 = 0;
    } 
    if (paramInt1 != 0) {
      paramPackageItemInfo.icon = paramInt1;
      paramPackageItemInfo.nonLocalizedLabel = null;
    } else {
      paramInt1 = paramTypedArray.getResourceId(paramInt3, 0);
      if (paramInt1 != 0) {
        paramPackageItemInfo.icon = paramInt1;
        paramPackageItemInfo.nonLocalizedLabel = null;
      } 
    } 
    paramInt1 = paramTypedArray.getResourceId(paramInt5, 0);
    if (paramInt1 != 0)
      paramPackageItemInfo.logo = paramInt1; 
    paramInt1 = paramTypedArray.getResourceId(paramInt6, 0);
    if (paramInt1 != 0)
      paramPackageItemInfo.banner = paramInt1; 
    TypedValue typedValue = paramTypedArray.peekValue(paramInt2);
    if (typedValue != null) {
      paramPackageItemInfo.labelRes = paramInt1 = typedValue.resourceId;
      if (paramInt1 == 0)
        paramPackageItemInfo.nonLocalizedLabel = typedValue.coerceToString(); 
    } 
    paramPackageItemInfo.packageName = ((Package)stringBuilder).packageName;
    return true;
  }
  
  private Activity generateAppDetailsHiddenActivity(Package paramPackage, int paramInt, String[] paramArrayOfString, boolean paramBoolean) {
    Activity activity = new Activity(PackageManager.APP_DETAILS_ACTIVITY_CLASS_NAME, new ActivityInfo());
    activity.owner = paramPackage;
    activity.setPackageName(paramPackage.packageName);
    activity.info.theme = 16973909;
    activity.info.exported = true;
    activity.info.name = PackageManager.APP_DETAILS_ACTIVITY_CLASS_NAME;
    activity.info.processName = paramPackage.applicationInfo.processName;
    activity.info.uiOptions = activity.info.applicationInfo.uiOptions;
    activity.info.taskAffinity = buildTaskAffinityName(paramPackage.packageName, paramPackage.packageName, ":app_details", paramArrayOfString);
    activity.info.enabled = true;
    activity.info.launchMode = 0;
    activity.info.documentLaunchMode = 0;
    activity.info.maxRecents = ActivityTaskManager.getDefaultAppRecentsLimitStatic();
    activity.info.configChanges = getActivityConfigChanges(0, 0);
    activity.info.softInputMode = 0;
    activity.info.persistableMode = 1;
    activity.info.screenOrientation = -1;
    activity.info.resizeMode = 4;
    activity.info.lockTaskLaunchMode = 0;
    activity.info.directBootAware = false;
    activity.info.rotationAnimation = -1;
    activity.info.colorMode = 0;
    if (paramBoolean) {
      ActivityInfo activityInfo = activity.info;
      activityInfo.flags |= 0x200;
    } 
    return activity;
  }
  
  private Activity parseActivity(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString, CachedComponentArgs paramCachedComponentArgs, boolean paramBoolean1, boolean paramBoolean2) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: astore #9
    //   3: aload_3
    //   4: astore #10
    //   6: aload #5
    //   8: astore #11
    //   10: aload_2
    //   11: aload #10
    //   13: getstatic com/android/internal/R$styleable.AndroidManifestActivity : [I
    //   16: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   19: astore #12
    //   21: aload #6
    //   23: getfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   26: ifnonnull -> 62
    //   29: aload #6
    //   31: new android/content/pm/PackageParser$ParseComponentArgs
    //   34: dup
    //   35: aload_1
    //   36: aload #5
    //   38: iconst_3
    //   39: iconst_1
    //   40: iconst_2
    //   41: bipush #44
    //   43: bipush #23
    //   45: bipush #30
    //   47: aload_0
    //   48: getfield mSeparateProcesses : [Ljava/lang/String;
    //   51: bipush #7
    //   53: bipush #17
    //   55: iconst_5
    //   56: invokespecial <init> : (Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIIIII[Ljava/lang/String;III)V
    //   59: putfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   62: aload #6
    //   64: getfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   67: astore #13
    //   69: iload #7
    //   71: ifeq -> 81
    //   74: ldc_w '<receiver>'
    //   77: astore_1
    //   78: goto -> 85
    //   81: ldc_w '<activity>'
    //   84: astore_1
    //   85: aload #13
    //   87: aload_1
    //   88: putfield tag : Ljava/lang/String;
    //   91: aload #6
    //   93: getfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   96: aload #12
    //   98: putfield sa : Landroid/content/res/TypedArray;
    //   101: aload #6
    //   103: getfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   106: iload #4
    //   108: putfield flags : I
    //   111: new android/content/pm/PackageParser$Activity
    //   114: dup
    //   115: aload #6
    //   117: getfield mActivityArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   120: new android/content/pm/ActivityInfo
    //   123: dup
    //   124: invokespecial <init> : ()V
    //   127: invokespecial <init> : (Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ActivityInfo;)V
    //   130: astore #14
    //   132: aload #11
    //   134: iconst_0
    //   135: aaload
    //   136: ifnull -> 146
    //   139: aload #12
    //   141: invokevirtual recycle : ()V
    //   144: aconst_null
    //   145: areturn
    //   146: aload #12
    //   148: bipush #6
    //   150: invokevirtual hasValue : (I)Z
    //   153: istore #15
    //   155: iload #15
    //   157: ifeq -> 176
    //   160: aload #14
    //   162: getfield info : Landroid/content/pm/ActivityInfo;
    //   165: aload #12
    //   167: bipush #6
    //   169: iconst_0
    //   170: invokevirtual getBoolean : (IZ)Z
    //   173: putfield exported : Z
    //   176: aload #14
    //   178: getfield info : Landroid/content/pm/ActivityInfo;
    //   181: aload #12
    //   183: iconst_0
    //   184: iconst_0
    //   185: invokevirtual getResourceId : (II)I
    //   188: putfield theme : I
    //   191: aload #14
    //   193: getfield info : Landroid/content/pm/ActivityInfo;
    //   196: aload #12
    //   198: bipush #26
    //   200: aload #14
    //   202: getfield info : Landroid/content/pm/ActivityInfo;
    //   205: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   208: getfield uiOptions : I
    //   211: invokevirtual getInt : (II)I
    //   214: putfield uiOptions : I
    //   217: aload #12
    //   219: bipush #27
    //   221: sipush #1024
    //   224: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   227: astore_1
    //   228: aload_1
    //   229: ifnull -> 332
    //   232: aload #14
    //   234: getfield info : Landroid/content/pm/ActivityInfo;
    //   237: getfield packageName : Ljava/lang/String;
    //   240: aload_1
    //   241: aload #11
    //   243: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    //   246: astore #6
    //   248: aload #11
    //   250: iconst_0
    //   251: aaload
    //   252: ifnonnull -> 268
    //   255: aload #14
    //   257: getfield info : Landroid/content/pm/ActivityInfo;
    //   260: aload #6
    //   262: putfield parentActivityName : Ljava/lang/String;
    //   265: goto -> 332
    //   268: new java/lang/StringBuilder
    //   271: dup
    //   272: invokespecial <init> : ()V
    //   275: astore #6
    //   277: aload #6
    //   279: ldc_w 'Activity '
    //   282: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   285: pop
    //   286: aload #6
    //   288: aload #14
    //   290: getfield info : Landroid/content/pm/ActivityInfo;
    //   293: getfield name : Ljava/lang/String;
    //   296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload #6
    //   302: ldc_w ' specified invalid parentActivityName '
    //   305: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: pop
    //   309: aload #6
    //   311: aload_1
    //   312: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   315: pop
    //   316: ldc 'PackageParser'
    //   318: aload #6
    //   320: invokevirtual toString : ()Ljava/lang/String;
    //   323: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   326: pop
    //   327: aload #11
    //   329: iconst_0
    //   330: aconst_null
    //   331: aastore
    //   332: aload #12
    //   334: iconst_4
    //   335: iconst_0
    //   336: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   339: astore_1
    //   340: aload_1
    //   341: ifnonnull -> 363
    //   344: aload #14
    //   346: getfield info : Landroid/content/pm/ActivityInfo;
    //   349: aload #9
    //   351: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   354: getfield permission : Ljava/lang/String;
    //   357: putfield permission : Ljava/lang/String;
    //   360: goto -> 396
    //   363: aload #14
    //   365: getfield info : Landroid/content/pm/ActivityInfo;
    //   368: astore #6
    //   370: aload_1
    //   371: invokevirtual length : ()I
    //   374: ifle -> 388
    //   377: aload_1
    //   378: invokevirtual toString : ()Ljava/lang/String;
    //   381: invokevirtual intern : ()Ljava/lang/String;
    //   384: astore_1
    //   385: goto -> 390
    //   388: aconst_null
    //   389: astore_1
    //   390: aload #6
    //   392: aload_1
    //   393: putfield permission : Ljava/lang/String;
    //   396: aload #12
    //   398: bipush #8
    //   400: sipush #1024
    //   403: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   406: astore #13
    //   408: aload #14
    //   410: getfield info : Landroid/content/pm/ActivityInfo;
    //   413: aload #9
    //   415: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   418: getfield packageName : Ljava/lang/String;
    //   421: aload #9
    //   423: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   426: getfield taskAffinity : Ljava/lang/String;
    //   429: aload #13
    //   431: aload #11
    //   433: invokestatic buildTaskAffinityName : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    //   436: putfield taskAffinity : Ljava/lang/String;
    //   439: aload #14
    //   441: getfield info : Landroid/content/pm/ActivityInfo;
    //   444: astore_1
    //   445: aload_1
    //   446: aload #12
    //   448: bipush #48
    //   450: iconst_0
    //   451: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   454: putfield splitName : Ljava/lang/String;
    //   457: aload #14
    //   459: getfield info : Landroid/content/pm/ActivityInfo;
    //   462: iconst_0
    //   463: putfield flags : I
    //   466: aload #12
    //   468: bipush #9
    //   470: iconst_0
    //   471: invokevirtual getBoolean : (IZ)Z
    //   474: ifeq -> 493
    //   477: aload #14
    //   479: getfield info : Landroid/content/pm/ActivityInfo;
    //   482: astore_1
    //   483: aload_1
    //   484: aload_1
    //   485: getfield flags : I
    //   488: iconst_1
    //   489: ior
    //   490: putfield flags : I
    //   493: aload #12
    //   495: bipush #10
    //   497: iconst_0
    //   498: invokevirtual getBoolean : (IZ)Z
    //   501: ifeq -> 520
    //   504: aload #14
    //   506: getfield info : Landroid/content/pm/ActivityInfo;
    //   509: astore_1
    //   510: aload_1
    //   511: aload_1
    //   512: getfield flags : I
    //   515: iconst_2
    //   516: ior
    //   517: putfield flags : I
    //   520: aload #12
    //   522: bipush #11
    //   524: iconst_0
    //   525: invokevirtual getBoolean : (IZ)Z
    //   528: ifeq -> 547
    //   531: aload #14
    //   533: getfield info : Landroid/content/pm/ActivityInfo;
    //   536: astore_1
    //   537: aload_1
    //   538: aload_1
    //   539: getfield flags : I
    //   542: iconst_4
    //   543: ior
    //   544: putfield flags : I
    //   547: aload #12
    //   549: bipush #21
    //   551: iconst_0
    //   552: invokevirtual getBoolean : (IZ)Z
    //   555: ifeq -> 576
    //   558: aload #14
    //   560: getfield info : Landroid/content/pm/ActivityInfo;
    //   563: astore_1
    //   564: aload_1
    //   565: aload_1
    //   566: getfield flags : I
    //   569: sipush #128
    //   572: ior
    //   573: putfield flags : I
    //   576: aload #12
    //   578: bipush #18
    //   580: iconst_0
    //   581: invokevirtual getBoolean : (IZ)Z
    //   584: ifeq -> 604
    //   587: aload #14
    //   589: getfield info : Landroid/content/pm/ActivityInfo;
    //   592: astore_1
    //   593: aload_1
    //   594: aload_1
    //   595: getfield flags : I
    //   598: bipush #8
    //   600: ior
    //   601: putfield flags : I
    //   604: aload #12
    //   606: bipush #12
    //   608: iconst_0
    //   609: invokevirtual getBoolean : (IZ)Z
    //   612: ifeq -> 632
    //   615: aload #14
    //   617: getfield info : Landroid/content/pm/ActivityInfo;
    //   620: astore_1
    //   621: aload_1
    //   622: aload_1
    //   623: getfield flags : I
    //   626: bipush #16
    //   628: ior
    //   629: putfield flags : I
    //   632: aload #12
    //   634: bipush #13
    //   636: iconst_0
    //   637: invokevirtual getBoolean : (IZ)Z
    //   640: ifeq -> 660
    //   643: aload #14
    //   645: getfield info : Landroid/content/pm/ActivityInfo;
    //   648: astore_1
    //   649: aload_1
    //   650: aload_1
    //   651: getfield flags : I
    //   654: bipush #32
    //   656: ior
    //   657: putfield flags : I
    //   660: aload #9
    //   662: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   665: getfield flags : I
    //   668: bipush #32
    //   670: iand
    //   671: ifeq -> 680
    //   674: iconst_1
    //   675: istore #16
    //   677: goto -> 683
    //   680: iconst_0
    //   681: istore #16
    //   683: aload #12
    //   685: bipush #19
    //   687: iload #16
    //   689: invokevirtual getBoolean : (IZ)Z
    //   692: ifeq -> 712
    //   695: aload #14
    //   697: getfield info : Landroid/content/pm/ActivityInfo;
    //   700: astore_1
    //   701: aload_1
    //   702: aload_1
    //   703: getfield flags : I
    //   706: bipush #64
    //   708: ior
    //   709: putfield flags : I
    //   712: aload #12
    //   714: bipush #22
    //   716: iconst_0
    //   717: invokevirtual getBoolean : (IZ)Z
    //   720: ifeq -> 741
    //   723: aload #14
    //   725: getfield info : Landroid/content/pm/ActivityInfo;
    //   728: astore_1
    //   729: aload_1
    //   730: aload_1
    //   731: getfield flags : I
    //   734: sipush #256
    //   737: ior
    //   738: putfield flags : I
    //   741: aload #12
    //   743: bipush #29
    //   745: iconst_0
    //   746: invokevirtual getBoolean : (IZ)Z
    //   749: ifne -> 763
    //   752: aload #12
    //   754: bipush #39
    //   756: iconst_0
    //   757: invokevirtual getBoolean : (IZ)Z
    //   760: ifeq -> 781
    //   763: aload #14
    //   765: getfield info : Landroid/content/pm/ActivityInfo;
    //   768: astore_1
    //   769: aload_1
    //   770: aload_1
    //   771: getfield flags : I
    //   774: sipush #1024
    //   777: ior
    //   778: putfield flags : I
    //   781: aload #12
    //   783: bipush #24
    //   785: iconst_0
    //   786: invokevirtual getBoolean : (IZ)Z
    //   789: ifeq -> 810
    //   792: aload #14
    //   794: getfield info : Landroid/content/pm/ActivityInfo;
    //   797: astore_1
    //   798: aload_1
    //   799: aload_1
    //   800: getfield flags : I
    //   803: sipush #2048
    //   806: ior
    //   807: putfield flags : I
    //   810: aload #12
    //   812: bipush #58
    //   814: iconst_0
    //   815: invokevirtual getBoolean : (IZ)Z
    //   818: ifeq -> 839
    //   821: aload #14
    //   823: getfield info : Landroid/content/pm/ActivityInfo;
    //   826: astore_1
    //   827: aload_1
    //   828: aload_1
    //   829: getfield flags : I
    //   832: ldc_w 536870912
    //   835: ior
    //   836: putfield flags : I
    //   839: iload #7
    //   841: ifne -> 1471
    //   844: aload #12
    //   846: bipush #25
    //   848: iload #8
    //   850: invokevirtual getBoolean : (IZ)Z
    //   853: ifeq -> 874
    //   856: aload #14
    //   858: getfield info : Landroid/content/pm/ActivityInfo;
    //   861: astore_1
    //   862: aload_1
    //   863: aload_1
    //   864: getfield flags : I
    //   867: sipush #512
    //   870: ior
    //   871: putfield flags : I
    //   874: aload #14
    //   876: getfield info : Landroid/content/pm/ActivityInfo;
    //   879: aload #12
    //   881: bipush #14
    //   883: iconst_0
    //   884: invokevirtual getInt : (II)I
    //   887: putfield launchMode : I
    //   890: aload #14
    //   892: getfield info : Landroid/content/pm/ActivityInfo;
    //   895: aload #12
    //   897: bipush #33
    //   899: iconst_0
    //   900: invokevirtual getInt : (II)I
    //   903: putfield documentLaunchMode : I
    //   906: aload #14
    //   908: getfield info : Landroid/content/pm/ActivityInfo;
    //   911: astore_1
    //   912: invokestatic getDefaultAppRecentsLimitStatic : ()I
    //   915: istore #4
    //   917: aload_1
    //   918: aload #12
    //   920: bipush #34
    //   922: iload #4
    //   924: invokevirtual getInt : (II)I
    //   927: putfield maxRecents : I
    //   930: aload #14
    //   932: getfield info : Landroid/content/pm/ActivityInfo;
    //   935: astore_1
    //   936: aload #12
    //   938: bipush #16
    //   940: iconst_0
    //   941: invokevirtual getInt : (II)I
    //   944: istore #4
    //   946: aload #12
    //   948: bipush #47
    //   950: iconst_0
    //   951: invokevirtual getInt : (II)I
    //   954: istore #17
    //   956: aload_1
    //   957: iload #4
    //   959: iload #17
    //   961: invokestatic getActivityConfigChanges : (II)I
    //   964: putfield configChanges : I
    //   967: aload #14
    //   969: getfield info : Landroid/content/pm/ActivityInfo;
    //   972: aload #12
    //   974: bipush #20
    //   976: iconst_0
    //   977: invokevirtual getInt : (II)I
    //   980: putfield softInputMode : I
    //   983: aload #14
    //   985: getfield info : Landroid/content/pm/ActivityInfo;
    //   988: aload #12
    //   990: bipush #32
    //   992: iconst_0
    //   993: invokevirtual getInteger : (II)I
    //   996: putfield persistableMode : I
    //   999: aload #12
    //   1001: bipush #31
    //   1003: iconst_0
    //   1004: invokevirtual getBoolean : (IZ)Z
    //   1007: ifeq -> 1027
    //   1010: aload #14
    //   1012: getfield info : Landroid/content/pm/ActivityInfo;
    //   1015: astore_1
    //   1016: aload_1
    //   1017: aload_1
    //   1018: getfield flags : I
    //   1021: ldc -2147483648
    //   1023: ior
    //   1024: putfield flags : I
    //   1027: aload #12
    //   1029: bipush #35
    //   1031: iconst_0
    //   1032: invokevirtual getBoolean : (IZ)Z
    //   1035: ifeq -> 1056
    //   1038: aload #14
    //   1040: getfield info : Landroid/content/pm/ActivityInfo;
    //   1043: astore_1
    //   1044: aload_1
    //   1045: aload_1
    //   1046: getfield flags : I
    //   1049: sipush #8192
    //   1052: ior
    //   1053: putfield flags : I
    //   1056: aload #12
    //   1058: bipush #36
    //   1060: iconst_0
    //   1061: invokevirtual getBoolean : (IZ)Z
    //   1064: ifeq -> 1085
    //   1067: aload #14
    //   1069: getfield info : Landroid/content/pm/ActivityInfo;
    //   1072: astore_1
    //   1073: aload_1
    //   1074: aload_1
    //   1075: getfield flags : I
    //   1078: sipush #4096
    //   1081: ior
    //   1082: putfield flags : I
    //   1085: aload #12
    //   1087: bipush #37
    //   1089: iconst_0
    //   1090: invokevirtual getBoolean : (IZ)Z
    //   1093: ifeq -> 1114
    //   1096: aload #14
    //   1098: getfield info : Landroid/content/pm/ActivityInfo;
    //   1101: astore_1
    //   1102: aload_1
    //   1103: aload_1
    //   1104: getfield flags : I
    //   1107: sipush #16384
    //   1110: ior
    //   1111: putfield flags : I
    //   1114: aload #14
    //   1116: getfield info : Landroid/content/pm/ActivityInfo;
    //   1119: aload #12
    //   1121: bipush #15
    //   1123: iconst_m1
    //   1124: invokevirtual getInt : (II)I
    //   1127: putfield screenOrientation : I
    //   1130: aload_0
    //   1131: aload #14
    //   1133: getfield info : Landroid/content/pm/ActivityInfo;
    //   1136: aload #12
    //   1138: aload #9
    //   1140: invokespecial setActivityResizeMode : (Landroid/content/pm/ActivityInfo;Landroid/content/res/TypedArray;Landroid/content/pm/PackageParser$Package;)V
    //   1143: aload #12
    //   1145: bipush #41
    //   1147: iconst_0
    //   1148: invokevirtual getBoolean : (IZ)Z
    //   1151: ifeq -> 1172
    //   1154: aload #14
    //   1156: getfield info : Landroid/content/pm/ActivityInfo;
    //   1159: astore_1
    //   1160: aload_1
    //   1161: aload_1
    //   1162: getfield flags : I
    //   1165: ldc_w 4194304
    //   1168: ior
    //   1169: putfield flags : I
    //   1172: aload #12
    //   1174: bipush #57
    //   1176: iconst_0
    //   1177: invokevirtual getBoolean : (IZ)Z
    //   1180: ifeq -> 1201
    //   1183: aload #14
    //   1185: getfield info : Landroid/content/pm/ActivityInfo;
    //   1188: astore_1
    //   1189: aload_1
    //   1190: aload_1
    //   1191: getfield flags : I
    //   1194: ldc_w 262144
    //   1197: ior
    //   1198: putfield flags : I
    //   1201: aload #12
    //   1203: bipush #50
    //   1205: invokevirtual hasValue : (I)Z
    //   1208: ifeq -> 1235
    //   1211: aload #12
    //   1213: bipush #50
    //   1215: invokevirtual getType : (I)I
    //   1218: iconst_4
    //   1219: if_icmpne -> 1235
    //   1222: aload #14
    //   1224: aload #12
    //   1226: bipush #50
    //   1228: fconst_0
    //   1229: invokevirtual getFloat : (IF)F
    //   1232: invokestatic access$200 : (Landroid/content/pm/PackageParser$Activity;F)V
    //   1235: aload #12
    //   1237: bipush #53
    //   1239: invokevirtual hasValue : (I)Z
    //   1242: ifeq -> 1269
    //   1245: aload #12
    //   1247: bipush #53
    //   1249: invokevirtual getType : (I)I
    //   1252: iconst_4
    //   1253: if_icmpne -> 1269
    //   1256: aload #14
    //   1258: aload #12
    //   1260: bipush #53
    //   1262: fconst_0
    //   1263: invokevirtual getFloat : (IF)F
    //   1266: invokestatic access$300 : (Landroid/content/pm/PackageParser$Activity;F)V
    //   1269: aload #14
    //   1271: getfield info : Landroid/content/pm/ActivityInfo;
    //   1274: astore_1
    //   1275: aload_1
    //   1276: aload #12
    //   1278: bipush #38
    //   1280: iconst_0
    //   1281: invokevirtual getInt : (II)I
    //   1284: putfield lockTaskLaunchMode : I
    //   1287: aload #14
    //   1289: getfield info : Landroid/content/pm/ActivityInfo;
    //   1292: aload #12
    //   1294: bipush #42
    //   1296: iconst_0
    //   1297: invokevirtual getBoolean : (IZ)Z
    //   1300: putfield directBootAware : Z
    //   1303: aload #14
    //   1305: getfield info : Landroid/content/pm/ActivityInfo;
    //   1308: astore_1
    //   1309: aload_1
    //   1310: aload #12
    //   1312: bipush #43
    //   1314: invokevirtual getString : (I)Ljava/lang/String;
    //   1317: putfield requestedVrComponent : Ljava/lang/String;
    //   1320: aload #14
    //   1322: getfield info : Landroid/content/pm/ActivityInfo;
    //   1325: astore_1
    //   1326: aload_1
    //   1327: aload #12
    //   1329: bipush #46
    //   1331: iconst_m1
    //   1332: invokevirtual getInt : (II)I
    //   1335: putfield rotationAnimation : I
    //   1338: aload #14
    //   1340: getfield info : Landroid/content/pm/ActivityInfo;
    //   1343: aload #12
    //   1345: bipush #49
    //   1347: iconst_0
    //   1348: invokevirtual getInt : (II)I
    //   1351: putfield colorMode : I
    //   1354: aload #12
    //   1356: bipush #56
    //   1358: iconst_0
    //   1359: invokevirtual getBoolean : (IZ)Z
    //   1362: ifeq -> 1383
    //   1365: aload #14
    //   1367: getfield info : Landroid/content/pm/ActivityInfo;
    //   1370: astore_1
    //   1371: aload_1
    //   1372: aload_1
    //   1373: getfield flags : I
    //   1376: ldc_w 33554432
    //   1379: ior
    //   1380: putfield flags : I
    //   1383: aload #12
    //   1385: bipush #51
    //   1387: iconst_0
    //   1388: invokevirtual getBoolean : (IZ)Z
    //   1391: ifeq -> 1412
    //   1394: aload #14
    //   1396: getfield info : Landroid/content/pm/ActivityInfo;
    //   1399: astore_1
    //   1400: aload_1
    //   1401: aload_1
    //   1402: getfield flags : I
    //   1405: ldc_w 8388608
    //   1408: ior
    //   1409: putfield flags : I
    //   1412: aload #12
    //   1414: bipush #52
    //   1416: iconst_0
    //   1417: invokevirtual getBoolean : (IZ)Z
    //   1420: ifeq -> 1441
    //   1423: aload #14
    //   1425: getfield info : Landroid/content/pm/ActivityInfo;
    //   1428: astore_1
    //   1429: aload_1
    //   1430: aload_1
    //   1431: getfield flags : I
    //   1434: ldc_w 16777216
    //   1437: ior
    //   1438: putfield flags : I
    //   1441: aload #12
    //   1443: bipush #54
    //   1445: iconst_0
    //   1446: invokevirtual getBoolean : (IZ)Z
    //   1449: ifeq -> 1534
    //   1452: aload #14
    //   1454: getfield info : Landroid/content/pm/ActivityInfo;
    //   1457: astore_1
    //   1458: aload_1
    //   1459: aload_1
    //   1460: getfield privateFlags : I
    //   1463: iconst_1
    //   1464: ior
    //   1465: putfield privateFlags : I
    //   1468: goto -> 1534
    //   1471: aload #14
    //   1473: getfield info : Landroid/content/pm/ActivityInfo;
    //   1476: iconst_0
    //   1477: putfield launchMode : I
    //   1480: aload #14
    //   1482: getfield info : Landroid/content/pm/ActivityInfo;
    //   1485: iconst_0
    //   1486: putfield configChanges : I
    //   1489: aload #12
    //   1491: bipush #28
    //   1493: iconst_0
    //   1494: invokevirtual getBoolean : (IZ)Z
    //   1497: ifeq -> 1518
    //   1500: aload #14
    //   1502: getfield info : Landroid/content/pm/ActivityInfo;
    //   1505: astore_1
    //   1506: aload_1
    //   1507: aload_1
    //   1508: getfield flags : I
    //   1511: ldc_w 1073741824
    //   1514: ior
    //   1515: putfield flags : I
    //   1518: aload #14
    //   1520: getfield info : Landroid/content/pm/ActivityInfo;
    //   1523: aload #12
    //   1525: bipush #42
    //   1527: iconst_0
    //   1528: invokevirtual getBoolean : (IZ)Z
    //   1531: putfield directBootAware : Z
    //   1534: aload #14
    //   1536: getfield info : Landroid/content/pm/ActivityInfo;
    //   1539: getfield directBootAware : Z
    //   1542: ifeq -> 1563
    //   1545: aload #9
    //   1547: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   1550: astore_1
    //   1551: aload_1
    //   1552: aload_1
    //   1553: getfield privateFlags : I
    //   1556: sipush #256
    //   1559: ior
    //   1560: putfield privateFlags : I
    //   1563: aload #12
    //   1565: bipush #45
    //   1567: iconst_0
    //   1568: invokevirtual getBoolean : (IZ)Z
    //   1571: istore #8
    //   1573: iload #8
    //   1575: ifeq -> 1602
    //   1578: aload #14
    //   1580: getfield info : Landroid/content/pm/ActivityInfo;
    //   1583: astore_1
    //   1584: aload_1
    //   1585: aload_1
    //   1586: getfield flags : I
    //   1589: ldc_w 1048576
    //   1592: ior
    //   1593: putfield flags : I
    //   1596: aload #9
    //   1598: iconst_1
    //   1599: putfield visibleToInstantApps : Z
    //   1602: aload #12
    //   1604: invokevirtual recycle : ()V
    //   1607: iload #7
    //   1609: ifeq -> 1654
    //   1612: aload #9
    //   1614: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   1617: getfield privateFlags : I
    //   1620: iconst_2
    //   1621: iand
    //   1622: ifeq -> 1654
    //   1625: aload #14
    //   1627: getfield info : Landroid/content/pm/ActivityInfo;
    //   1630: getfield processName : Ljava/lang/String;
    //   1633: aload #9
    //   1635: getfield packageName : Ljava/lang/String;
    //   1638: if_acmpne -> 1651
    //   1641: aload #11
    //   1643: iconst_0
    //   1644: ldc_w 'Heavy-weight applications can not have receivers in main process'
    //   1647: aastore
    //   1648: goto -> 1654
    //   1651: goto -> 1654
    //   1654: aload #11
    //   1656: iconst_0
    //   1657: aaload
    //   1658: ifnull -> 1663
    //   1661: aconst_null
    //   1662: areturn
    //   1663: aload_3
    //   1664: invokeinterface getDepth : ()I
    //   1669: istore #4
    //   1671: aload #10
    //   1673: astore #6
    //   1675: aload #12
    //   1677: astore_1
    //   1678: aload_3
    //   1679: invokeinterface next : ()I
    //   1684: istore #17
    //   1686: iload #17
    //   1688: iconst_1
    //   1689: if_icmpeq -> 2567
    //   1692: iload #17
    //   1694: iconst_3
    //   1695: if_icmpne -> 1715
    //   1698: aload_3
    //   1699: invokeinterface getDepth : ()I
    //   1704: iload #4
    //   1706: if_icmple -> 1712
    //   1709: goto -> 1715
    //   1712: goto -> 2567
    //   1715: iload #17
    //   1717: iconst_3
    //   1718: if_icmpeq -> 2564
    //   1721: iload #17
    //   1723: iconst_4
    //   1724: if_icmpne -> 1730
    //   1727: goto -> 1678
    //   1730: aload_3
    //   1731: invokeinterface getName : ()Ljava/lang/String;
    //   1736: ldc_w 'intent-filter'
    //   1739: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1742: ifeq -> 1982
    //   1745: new android/content/pm/PackageParser$ActivityIntentInfo
    //   1748: dup
    //   1749: aload #14
    //   1751: invokespecial <init> : (Landroid/content/pm/PackageParser$Activity;)V
    //   1754: astore #6
    //   1756: aload_0
    //   1757: aload_2
    //   1758: aload_3
    //   1759: iconst_1
    //   1760: iconst_1
    //   1761: aload #6
    //   1763: aload #5
    //   1765: invokespecial parseIntent : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZLandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;)Z
    //   1768: ifne -> 1773
    //   1771: aconst_null
    //   1772: areturn
    //   1773: aload #6
    //   1775: invokevirtual countActions : ()I
    //   1778: ifne -> 1848
    //   1781: new java/lang/StringBuilder
    //   1784: dup
    //   1785: invokespecial <init> : ()V
    //   1788: astore #10
    //   1790: aload #10
    //   1792: ldc_w 'No actions in intent filter at '
    //   1795: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1798: pop
    //   1799: aload #10
    //   1801: aload_0
    //   1802: getfield mArchiveSourcePath : Ljava/lang/String;
    //   1805: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1808: pop
    //   1809: aload #10
    //   1811: ldc_w ' '
    //   1814: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1817: pop
    //   1818: aload #10
    //   1820: aload_3
    //   1821: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   1826: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1829: pop
    //   1830: aload #10
    //   1832: invokevirtual toString : ()Ljava/lang/String;
    //   1835: astore #10
    //   1837: ldc 'PackageParser'
    //   1839: aload #10
    //   1841: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1844: pop
    //   1845: goto -> 1877
    //   1848: aload #14
    //   1850: aload #6
    //   1852: invokevirtual getOrder : ()I
    //   1855: aload #14
    //   1857: getfield order : I
    //   1860: invokestatic max : (II)I
    //   1863: putfield order : I
    //   1866: aload #14
    //   1868: getfield intents : Ljava/util/ArrayList;
    //   1871: aload #6
    //   1873: invokevirtual add : (Ljava/lang/Object;)Z
    //   1876: pop
    //   1877: iload #8
    //   1879: ifeq -> 1888
    //   1882: iconst_1
    //   1883: istore #17
    //   1885: goto -> 1911
    //   1888: iload #7
    //   1890: ifne -> 1908
    //   1893: aload_0
    //   1894: aload #6
    //   1896: invokespecial isImplicitlyExposedIntent : (Landroid/content/pm/PackageParser$IntentInfo;)Z
    //   1899: ifeq -> 1908
    //   1902: iconst_2
    //   1903: istore #17
    //   1905: goto -> 1911
    //   1908: iconst_0
    //   1909: istore #17
    //   1911: aload #6
    //   1913: iload #17
    //   1915: invokevirtual setVisibilityToInstantApp : (I)V
    //   1918: aload #6
    //   1920: invokevirtual isVisibleToInstantApp : ()Z
    //   1923: ifeq -> 1947
    //   1926: aload #14
    //   1928: getfield info : Landroid/content/pm/ActivityInfo;
    //   1931: astore #10
    //   1933: aload #10
    //   1935: aload #10
    //   1937: getfield flags : I
    //   1940: ldc_w 1048576
    //   1943: ior
    //   1944: putfield flags : I
    //   1947: aload #6
    //   1949: invokevirtual isImplicitlyVisibleToInstantApp : ()Z
    //   1952: ifeq -> 1976
    //   1955: aload #14
    //   1957: getfield info : Landroid/content/pm/ActivityInfo;
    //   1960: astore #6
    //   1962: aload #6
    //   1964: aload #6
    //   1966: getfield flags : I
    //   1969: ldc_w 2097152
    //   1972: ior
    //   1973: putfield flags : I
    //   1976: aload_3
    //   1977: astore #6
    //   1979: goto -> 1678
    //   1982: iload #7
    //   1984: ifne -> 2244
    //   1987: aload_3
    //   1988: invokeinterface getName : ()Ljava/lang/String;
    //   1993: ldc_w 'preferred'
    //   1996: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1999: ifeq -> 2244
    //   2002: new android/content/pm/PackageParser$ActivityIntentInfo
    //   2005: dup
    //   2006: aload #14
    //   2008: invokespecial <init> : (Landroid/content/pm/PackageParser$Activity;)V
    //   2011: astore #6
    //   2013: aload_0
    //   2014: aload_2
    //   2015: aload_3
    //   2016: iconst_0
    //   2017: iconst_0
    //   2018: aload #6
    //   2020: aload #5
    //   2022: invokespecial parseIntent : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZLandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;)Z
    //   2025: ifne -> 2030
    //   2028: aconst_null
    //   2029: areturn
    //   2030: aload #6
    //   2032: invokevirtual countActions : ()I
    //   2035: ifne -> 2105
    //   2038: new java/lang/StringBuilder
    //   2041: dup
    //   2042: invokespecial <init> : ()V
    //   2045: astore #10
    //   2047: aload #10
    //   2049: ldc_w 'No actions in preferred at '
    //   2052: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2055: pop
    //   2056: aload #10
    //   2058: aload_0
    //   2059: getfield mArchiveSourcePath : Ljava/lang/String;
    //   2062: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2065: pop
    //   2066: aload #10
    //   2068: ldc_w ' '
    //   2071: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2074: pop
    //   2075: aload #10
    //   2077: aload_3
    //   2078: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   2083: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2086: pop
    //   2087: aload #10
    //   2089: invokevirtual toString : ()Ljava/lang/String;
    //   2092: astore #10
    //   2094: ldc 'PackageParser'
    //   2096: aload #10
    //   2098: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   2101: pop
    //   2102: goto -> 2136
    //   2105: aload #9
    //   2107: getfield preferredActivityFilters : Ljava/util/ArrayList;
    //   2110: ifnonnull -> 2125
    //   2113: aload #9
    //   2115: new java/util/ArrayList
    //   2118: dup
    //   2119: invokespecial <init> : ()V
    //   2122: putfield preferredActivityFilters : Ljava/util/ArrayList;
    //   2125: aload #9
    //   2127: getfield preferredActivityFilters : Ljava/util/ArrayList;
    //   2130: aload #6
    //   2132: invokevirtual add : (Ljava/lang/Object;)Z
    //   2135: pop
    //   2136: iload #8
    //   2138: ifeq -> 2147
    //   2141: iconst_1
    //   2142: istore #17
    //   2144: goto -> 2170
    //   2147: iload #7
    //   2149: ifne -> 2167
    //   2152: aload_0
    //   2153: aload #6
    //   2155: invokespecial isImplicitlyExposedIntent : (Landroid/content/pm/PackageParser$IntentInfo;)Z
    //   2158: ifeq -> 2167
    //   2161: iconst_2
    //   2162: istore #17
    //   2164: goto -> 2170
    //   2167: iconst_0
    //   2168: istore #17
    //   2170: aload #6
    //   2172: iload #17
    //   2174: invokevirtual setVisibilityToInstantApp : (I)V
    //   2177: aload #6
    //   2179: invokevirtual isVisibleToInstantApp : ()Z
    //   2182: ifeq -> 2209
    //   2185: aload #14
    //   2187: getfield info : Landroid/content/pm/ActivityInfo;
    //   2190: astore #10
    //   2192: aload #10
    //   2194: aload #10
    //   2196: getfield flags : I
    //   2199: ldc_w 1048576
    //   2202: ior
    //   2203: putfield flags : I
    //   2206: goto -> 2209
    //   2209: aload #6
    //   2211: invokevirtual isImplicitlyVisibleToInstantApp : ()Z
    //   2214: ifeq -> 2238
    //   2217: aload #14
    //   2219: getfield info : Landroid/content/pm/ActivityInfo;
    //   2222: astore #6
    //   2224: aload #6
    //   2226: aload #6
    //   2228: getfield flags : I
    //   2231: ldc_w 2097152
    //   2234: ior
    //   2235: putfield flags : I
    //   2238: aload_3
    //   2239: astore #6
    //   2241: goto -> 1678
    //   2244: aload_3
    //   2245: invokeinterface getName : ()Ljava/lang/String;
    //   2250: ldc_w 'meta-data'
    //   2253: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2256: ifeq -> 2294
    //   2259: aload_0
    //   2260: aload_2
    //   2261: aload_3
    //   2262: aload #14
    //   2264: getfield metaData : Landroid/os/Bundle;
    //   2267: aload #11
    //   2269: invokespecial parseMetaData : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    //   2272: astore #6
    //   2274: aload #14
    //   2276: aload #6
    //   2278: putfield metaData : Landroid/os/Bundle;
    //   2281: aload #6
    //   2283: ifnonnull -> 2288
    //   2286: aconst_null
    //   2287: areturn
    //   2288: aload_3
    //   2289: astore #6
    //   2291: goto -> 1678
    //   2294: iload #7
    //   2296: ifne -> 2328
    //   2299: aload_3
    //   2300: invokeinterface getName : ()Ljava/lang/String;
    //   2305: ldc_w 'layout'
    //   2308: invokevirtual equals : (Ljava/lang/Object;)Z
    //   2311: ifeq -> 2328
    //   2314: aload_0
    //   2315: aload_2
    //   2316: aload_3
    //   2317: aload #14
    //   2319: invokespecial parseLayout : (Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/pm/PackageParser$Activity;)V
    //   2322: aload_3
    //   2323: astore #6
    //   2325: goto -> 1678
    //   2328: new java/lang/StringBuilder
    //   2331: dup
    //   2332: invokespecial <init> : ()V
    //   2335: astore #6
    //   2337: aload #6
    //   2339: ldc_w 'Problem in package '
    //   2342: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2345: pop
    //   2346: aload #6
    //   2348: aload_0
    //   2349: getfield mArchiveSourcePath : Ljava/lang/String;
    //   2352: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2355: pop
    //   2356: aload #6
    //   2358: ldc_w ':'
    //   2361: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2364: pop
    //   2365: ldc 'PackageParser'
    //   2367: aload #6
    //   2369: invokevirtual toString : ()Ljava/lang/String;
    //   2372: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   2375: pop
    //   2376: iload #7
    //   2378: ifeq -> 2469
    //   2381: new java/lang/StringBuilder
    //   2384: dup
    //   2385: invokespecial <init> : ()V
    //   2388: astore #6
    //   2390: aload #6
    //   2392: ldc_w 'Unknown element under <receiver>: '
    //   2395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2398: pop
    //   2399: aload #6
    //   2401: aload_3
    //   2402: invokeinterface getName : ()Ljava/lang/String;
    //   2407: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2410: pop
    //   2411: aload #6
    //   2413: ldc_w ' at '
    //   2416: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2419: pop
    //   2420: aload #6
    //   2422: aload_0
    //   2423: getfield mArchiveSourcePath : Ljava/lang/String;
    //   2426: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2429: pop
    //   2430: aload #6
    //   2432: ldc_w ' '
    //   2435: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2438: pop
    //   2439: aload #6
    //   2441: aload_3
    //   2442: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   2447: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2450: pop
    //   2451: aload #6
    //   2453: invokevirtual toString : ()Ljava/lang/String;
    //   2456: astore #6
    //   2458: ldc 'PackageParser'
    //   2460: aload #6
    //   2462: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   2465: pop
    //   2466: goto -> 2554
    //   2469: new java/lang/StringBuilder
    //   2472: dup
    //   2473: invokespecial <init> : ()V
    //   2476: astore #6
    //   2478: aload #6
    //   2480: ldc_w 'Unknown element under <activity>: '
    //   2483: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2486: pop
    //   2487: aload #6
    //   2489: aload_3
    //   2490: invokeinterface getName : ()Ljava/lang/String;
    //   2495: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2498: pop
    //   2499: aload #6
    //   2501: ldc_w ' at '
    //   2504: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2507: pop
    //   2508: aload #6
    //   2510: aload_0
    //   2511: getfield mArchiveSourcePath : Ljava/lang/String;
    //   2514: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2517: pop
    //   2518: aload #6
    //   2520: ldc_w ' '
    //   2523: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2526: pop
    //   2527: aload #6
    //   2529: aload_3
    //   2530: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   2535: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   2538: pop
    //   2539: aload #6
    //   2541: invokevirtual toString : ()Ljava/lang/String;
    //   2544: astore #6
    //   2546: ldc 'PackageParser'
    //   2548: aload #6
    //   2550: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   2553: pop
    //   2554: aload_3
    //   2555: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   2558: aload_3
    //   2559: astore #6
    //   2561: goto -> 1678
    //   2564: goto -> 1678
    //   2567: aload_0
    //   2568: aload #14
    //   2570: invokespecial resolveWindowLayout : (Landroid/content/pm/PackageParser$Activity;)V
    //   2573: iload #15
    //   2575: ifne -> 2610
    //   2578: aload #14
    //   2580: getfield info : Landroid/content/pm/ActivityInfo;
    //   2583: astore_1
    //   2584: aload #14
    //   2586: getfield intents : Ljava/util/ArrayList;
    //   2589: invokevirtual size : ()I
    //   2592: ifle -> 2601
    //   2595: iconst_1
    //   2596: istore #7
    //   2598: goto -> 2604
    //   2601: iconst_0
    //   2602: istore #7
    //   2604: aload_1
    //   2605: iload #7
    //   2607: putfield exported : Z
    //   2610: aload #14
    //   2612: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4273	-> 0
    //   #4275	-> 21
    //   #4276	-> 29
    //   #4289	-> 62
    //   #4290	-> 91
    //   #4291	-> 101
    //   #4293	-> 111
    //   #4294	-> 132
    //   #4295	-> 139
    //   #4296	-> 144
    //   #4299	-> 146
    //   #4300	-> 155
    //   #4301	-> 160
    //   #4304	-> 176
    //   #4306	-> 191
    //   #4309	-> 217
    //   #4312	-> 228
    //   #4313	-> 232
    //   #4314	-> 248
    //   #4315	-> 255
    //   #4317	-> 268
    //   #4319	-> 327
    //   #4324	-> 332
    //   #4325	-> 340
    //   #4326	-> 344
    //   #4328	-> 363
    //   #4331	-> 396
    //   #4334	-> 408
    //   #4337	-> 439
    //   #4338	-> 445
    //   #4340	-> 457
    //   #4341	-> 466
    //   #4343	-> 477
    //   #4346	-> 493
    //   #4347	-> 504
    //   #4350	-> 520
    //   #4351	-> 531
    //   #4354	-> 547
    //   #4355	-> 558
    //   #4358	-> 576
    //   #4359	-> 587
    //   #4362	-> 604
    //   #4363	-> 615
    //   #4366	-> 632
    //   #4367	-> 643
    //   #4370	-> 660
    //   #4372	-> 695
    //   #4375	-> 712
    //   #4376	-> 723
    //   #4379	-> 741
    //   #4380	-> 752
    //   #4381	-> 763
    //   #4384	-> 781
    //   #4385	-> 792
    //   #4388	-> 810
    //   #4389	-> 821
    //   #4392	-> 839
    //   #4393	-> 844
    //   #4395	-> 856
    //   #4398	-> 874
    //   #4400	-> 890
    //   #4403	-> 906
    //   #4405	-> 912
    //   #4403	-> 917
    //   #4406	-> 930
    //   #4407	-> 936
    //   #4408	-> 946
    //   #4406	-> 956
    //   #4409	-> 967
    //   #4412	-> 983
    //   #4416	-> 999
    //   #4417	-> 1010
    //   #4420	-> 1027
    //   #4421	-> 1038
    //   #4424	-> 1056
    //   #4425	-> 1067
    //   #4428	-> 1085
    //   #4429	-> 1096
    //   #4432	-> 1114
    //   #4436	-> 1130
    //   #4438	-> 1143
    //   #4440	-> 1154
    //   #4443	-> 1172
    //   #4444	-> 1183
    //   #4447	-> 1201
    //   #4448	-> 1211
    //   #4450	-> 1222
    //   #4454	-> 1235
    //   #4455	-> 1245
    //   #4457	-> 1256
    //   #4461	-> 1269
    //   #4462	-> 1275
    //   #4464	-> 1287
    //   #4468	-> 1303
    //   #4469	-> 1309
    //   #4471	-> 1320
    //   #4472	-> 1326
    //   #4474	-> 1338
    //   #4477	-> 1354
    //   #4479	-> 1365
    //   #4482	-> 1383
    //   #4483	-> 1394
    //   #4486	-> 1412
    //   #4487	-> 1423
    //   #4490	-> 1441
    //   #4491	-> 1452
    //   #4494	-> 1471
    //   #4495	-> 1480
    //   #4497	-> 1489
    //   #4498	-> 1500
    //   #4501	-> 1518
    //   #4506	-> 1534
    //   #4507	-> 1545
    //   #4512	-> 1563
    //   #4513	-> 1563
    //   #4514	-> 1573
    //   #4515	-> 1578
    //   #4516	-> 1596
    //   #4519	-> 1602
    //   #4521	-> 1607
    //   #4525	-> 1625
    //   #4526	-> 1641
    //   #4525	-> 1651
    //   #4521	-> 1654
    //   #4530	-> 1654
    //   #4531	-> 1661
    //   #4534	-> 1663
    //   #4536	-> 1678
    //   #4538	-> 1698
    //   #4539	-> 1715
    //   #4540	-> 1727
    //   #4543	-> 1730
    //   #4544	-> 1745
    //   #4545	-> 1756
    //   #4547	-> 1771
    //   #4549	-> 1773
    //   #4550	-> 1781
    //   #4552	-> 1818
    //   #4550	-> 1837
    //   #4554	-> 1848
    //   #4555	-> 1866
    //   #4558	-> 1877
    //   #4559	-> 1882
    //   #4560	-> 1888
    //   #4561	-> 1902
    //   #4562	-> 1908
    //   #4563	-> 1911
    //   #4564	-> 1918
    //   #4565	-> 1926
    //   #4567	-> 1947
    //   #4568	-> 1955
    //   #4582	-> 1976
    //   #4583	-> 2002
    //   #4584	-> 2013
    //   #4586	-> 2028
    //   #4588	-> 2030
    //   #4589	-> 2038
    //   #4591	-> 2075
    //   #4589	-> 2094
    //   #4593	-> 2105
    //   #4594	-> 2113
    //   #4596	-> 2125
    //   #4599	-> 2136
    //   #4600	-> 2141
    //   #4601	-> 2147
    //   #4602	-> 2161
    //   #4603	-> 2167
    //   #4604	-> 2170
    //   #4605	-> 2177
    //   #4606	-> 2185
    //   #4605	-> 2209
    //   #4608	-> 2209
    //   #4609	-> 2217
    //   #4611	-> 2238
    //   #4582	-> 2244
    //   #4611	-> 2244
    //   #4612	-> 2259
    //   #4614	-> 2286
    //   #4612	-> 2288
    //   #4616	-> 2294
    //   #4617	-> 2314
    //   #4620	-> 2328
    //   #4621	-> 2376
    //   #4622	-> 2381
    //   #4624	-> 2439
    //   #4622	-> 2458
    //   #4626	-> 2469
    //   #4628	-> 2527
    //   #4626	-> 2546
    //   #4630	-> 2554
    //   #4631	-> 2558
    //   #4539	-> 2564
    //   #4536	-> 2567
    //   #4643	-> 2567
    //   #4645	-> 2573
    //   #4646	-> 2578
    //   #4649	-> 2610
  }
  
  private void setActivityResizeMode(ActivityInfo paramActivityInfo, TypedArray paramTypedArray, Package paramPackage) {
    int i = paramPackage.applicationInfo.privateFlags;
    boolean bool = true;
    if ((i & 0xC00) != 0) {
      i = 1;
    } else {
      i = 0;
    } 
    if (paramTypedArray.hasValue(40) || i != 0) {
      if ((paramPackage.applicationInfo.privateFlags & 0x400) == 0)
        bool = false; 
      if (paramTypedArray.getBoolean(40, bool)) {
        paramActivityInfo.resizeMode = 2;
      } else {
        paramActivityInfo.resizeMode = 0;
      } 
      return;
    } 
    if ((paramPackage.applicationInfo.privateFlags & 0x1000) != 0) {
      paramActivityInfo.resizeMode = 1;
      return;
    } 
    if (paramActivityInfo.isFixedOrientationPortrait()) {
      paramActivityInfo.resizeMode = 6;
    } else if (paramActivityInfo.isFixedOrientationLandscape()) {
      paramActivityInfo.resizeMode = 5;
    } else if (paramActivityInfo.isFixedOrientation()) {
      paramActivityInfo.resizeMode = 7;
    } else {
      paramActivityInfo.resizeMode = 4;
    } 
  }
  
  private void setMaxAspectRatio(Package paramPackage) {
    float f1, f2;
    if (paramPackage.applicationInfo.targetSdkVersion < 26) {
      f1 = 1.86F;
    } else {
      f1 = 0.0F;
    } 
    if (paramPackage.applicationInfo.maxAspectRatio != 0.0F) {
      f2 = paramPackage.applicationInfo.maxAspectRatio;
    } else {
      f2 = f1;
      if (paramPackage.mAppMetaData != null) {
        Bundle bundle = paramPackage.mAppMetaData;
        f2 = f1;
        if (bundle.containsKey("android.max_aspect"))
          f2 = paramPackage.mAppMetaData.getFloat("android.max_aspect", f1); 
      } 
    } 
    hookDispCompat(paramPackage, f2);
    for (Activity activity : paramPackage.activities) {
      if (activity.hasMaxAspectRatio())
        continue; 
      if (activity.metaData != null) {
        f1 = activity.metaData.getFloat("android.max_aspect", f2);
      } else {
        f1 = f2;
      } 
      activity.setMaxAspectRatio(f1);
    } 
  }
  
  private void setMinAspectRatio(Package paramPackage) {
    float f1 = paramPackage.applicationInfo.minAspectRatio, f2 = 0.0F;
    if (f1 != 0.0F) {
      f2 = paramPackage.applicationInfo.minAspectRatio;
    } else if (paramPackage.applicationInfo.targetSdkVersion < 29) {
      Callback callback = this.mCallback;
      if (callback != null && callback.hasFeature("android.hardware.type.watch")) {
        f2 = 1.0F;
      } else {
        f2 = 1.333F;
      } 
    } 
    for (Activity activity : paramPackage.activities) {
      if (activity.hasMinAspectRatio())
        continue; 
      activity.setMinAspectRatio(f2);
    } 
  }
  
  private void setSupportsSizeChanges(Package paramPackage) {
    // Byte code:
    //   0: aload_1
    //   1: getfield mAppMetaData : Landroid/os/Bundle;
    //   4: ifnull -> 27
    //   7: aload_1
    //   8: getfield mAppMetaData : Landroid/os/Bundle;
    //   11: astore_2
    //   12: aload_2
    //   13: ldc 'android.supports_size_changes'
    //   15: iconst_0
    //   16: invokevirtual getBoolean : (Ljava/lang/String;Z)Z
    //   19: ifeq -> 27
    //   22: iconst_1
    //   23: istore_3
    //   24: goto -> 29
    //   27: iconst_0
    //   28: istore_3
    //   29: aload_1
    //   30: getfield activities : Ljava/util/ArrayList;
    //   33: invokevirtual iterator : ()Ljava/util/Iterator;
    //   36: astore_1
    //   37: aload_1
    //   38: invokeinterface hasNext : ()Z
    //   43: ifeq -> 95
    //   46: aload_1
    //   47: invokeinterface next : ()Ljava/lang/Object;
    //   52: checkcast android/content/pm/PackageParser$Activity
    //   55: astore_2
    //   56: iload_3
    //   57: ifne -> 84
    //   60: aload_2
    //   61: getfield metaData : Landroid/os/Bundle;
    //   64: ifnull -> 92
    //   67: aload_2
    //   68: getfield metaData : Landroid/os/Bundle;
    //   71: astore #4
    //   73: aload #4
    //   75: ldc 'android.supports_size_changes'
    //   77: iconst_0
    //   78: invokevirtual getBoolean : (Ljava/lang/String;Z)Z
    //   81: ifeq -> 92
    //   84: aload_2
    //   85: getfield info : Landroid/content/pm/ActivityInfo;
    //   88: iconst_1
    //   89: putfield supportsSizeChanges : Z
    //   92: goto -> 37
    //   95: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4761	-> 0
    //   #4762	-> 12
    //   #4764	-> 29
    //   #4765	-> 56
    //   #4766	-> 73
    //   #4767	-> 84
    //   #4769	-> 92
    //   #4770	-> 95
  }
  
  public static int getActivityConfigChanges(int paramInt1, int paramInt2) {
    return (paramInt2 ^ 0xFFFFFFFF) & 0x3 | paramInt1;
  }
  
  private void parseLayout(Resources paramResources, AttributeSet paramAttributeSet, Activity paramActivity) {
    float f3;
    TypedArray typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestLayout);
    int i = -1;
    float f1 = -1.0F;
    int j = -1;
    float f2 = -1.0F;
    int k = typedArray.getType(3);
    if (k == 6) {
      f3 = typedArray.getFraction(3, 1, 1, -1.0F);
    } else {
      f3 = f1;
      if (k == 5) {
        i = typedArray.getDimensionPixelSize(3, -1);
        f3 = f1;
      } 
    } 
    k = typedArray.getType(4);
    if (k == 6) {
      f1 = typedArray.getFraction(4, 1, 1, -1.0F);
    } else {
      f1 = f2;
      if (k == 5) {
        j = typedArray.getDimensionPixelSize(4, -1);
        f1 = f2;
      } 
    } 
    int m = typedArray.getInt(0, 17);
    k = typedArray.getDimensionPixelSize(1, -1);
    int n = typedArray.getDimensionPixelSize(2, -1);
    typedArray.recycle();
    paramActivity.info.windowLayout = new ActivityInfo.WindowLayout(i, f3, j, f1, m, k, n);
  }
  
  private void resolveWindowLayout(Activity paramActivity) {
    if (paramActivity.metaData != null) {
      Bundle bundle = paramActivity.metaData;
      if (bundle.containsKey("android.activity_window_layout_affinity")) {
        ActivityInfo activityInfo = paramActivity.info;
        if (activityInfo.windowLayout != null && activityInfo.windowLayout.windowLayoutAffinity != null)
          return; 
        String str = paramActivity.metaData.getString("android.activity_window_layout_affinity");
        if (activityInfo.windowLayout == null)
          activityInfo.windowLayout = new ActivityInfo.WindowLayout(-1, -1.0F, -1, -1.0F, 0, -1, -1); 
        activityInfo.windowLayout.windowLayoutAffinity = str;
        return;
      } 
    } 
  }
  
  private Activity parseActivityAlias(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString, CachedComponentArgs paramCachedComponentArgs) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_2
    //   1: astore #7
    //   3: aload #5
    //   5: astore #8
    //   7: aload #7
    //   9: aload_3
    //   10: getstatic com/android/internal/R$styleable.AndroidManifestActivityAlias : [I
    //   13: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   16: astore #9
    //   18: aload #9
    //   20: bipush #7
    //   22: sipush #1024
    //   25: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   28: astore #10
    //   30: aload #10
    //   32: ifnonnull -> 49
    //   35: aload #8
    //   37: iconst_0
    //   38: ldc_w '<activity-alias> does not specify android:targetActivity'
    //   41: aastore
    //   42: aload #9
    //   44: invokevirtual recycle : ()V
    //   47: aconst_null
    //   48: areturn
    //   49: aload_1
    //   50: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   53: getfield packageName : Ljava/lang/String;
    //   56: aload #10
    //   58: aload #8
    //   60: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    //   63: astore #11
    //   65: aload #11
    //   67: ifnonnull -> 77
    //   70: aload #9
    //   72: invokevirtual recycle : ()V
    //   75: aconst_null
    //   76: areturn
    //   77: aload #6
    //   79: getfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   82: ifnonnull -> 128
    //   85: aload #6
    //   87: new android/content/pm/PackageParser$ParseComponentArgs
    //   90: dup
    //   91: aload_1
    //   92: aload #5
    //   94: iconst_2
    //   95: iconst_0
    //   96: iconst_1
    //   97: bipush #11
    //   99: bipush #8
    //   101: bipush #10
    //   103: aload_0
    //   104: getfield mSeparateProcesses : [Ljava/lang/String;
    //   107: iconst_0
    //   108: bipush #6
    //   110: iconst_4
    //   111: invokespecial <init> : (Landroid/content/pm/PackageParser$Package;[Ljava/lang/String;IIIIII[Ljava/lang/String;III)V
    //   114: putfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   117: aload #6
    //   119: getfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   122: ldc_w '<activity-alias>'
    //   125: putfield tag : Ljava/lang/String;
    //   128: aload #6
    //   130: getfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   133: aload #9
    //   135: putfield sa : Landroid/content/res/TypedArray;
    //   138: aload #6
    //   140: getfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   143: iload #4
    //   145: putfield flags : I
    //   148: aconst_null
    //   149: astore #12
    //   151: aload_1
    //   152: getfield activities : Ljava/util/ArrayList;
    //   155: invokevirtual size : ()I
    //   158: istore #13
    //   160: iconst_0
    //   161: istore #4
    //   163: aload #12
    //   165: astore #10
    //   167: iload #4
    //   169: iload #13
    //   171: if_icmpge -> 213
    //   174: aload_1
    //   175: getfield activities : Ljava/util/ArrayList;
    //   178: iload #4
    //   180: invokevirtual get : (I)Ljava/lang/Object;
    //   183: checkcast android/content/pm/PackageParser$Activity
    //   186: astore #10
    //   188: aload #11
    //   190: aload #10
    //   192: getfield info : Landroid/content/pm/ActivityInfo;
    //   195: getfield name : Ljava/lang/String;
    //   198: invokevirtual equals : (Ljava/lang/Object;)Z
    //   201: ifeq -> 207
    //   204: goto -> 213
    //   207: iinc #4, 1
    //   210: goto -> 163
    //   213: aload #10
    //   215: ifnonnull -> 264
    //   218: new java/lang/StringBuilder
    //   221: dup
    //   222: invokespecial <init> : ()V
    //   225: astore_1
    //   226: aload_1
    //   227: ldc_w '<activity-alias> target activity '
    //   230: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload_1
    //   235: aload #11
    //   237: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   240: pop
    //   241: aload_1
    //   242: ldc_w ' not found in manifest'
    //   245: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   248: pop
    //   249: aload #8
    //   251: iconst_0
    //   252: aload_1
    //   253: invokevirtual toString : ()Ljava/lang/String;
    //   256: aastore
    //   257: aload #9
    //   259: invokevirtual recycle : ()V
    //   262: aconst_null
    //   263: areturn
    //   264: new android/content/pm/ActivityInfo
    //   267: dup
    //   268: invokespecial <init> : ()V
    //   271: astore_1
    //   272: aload_1
    //   273: aload #11
    //   275: putfield targetActivity : Ljava/lang/String;
    //   278: aload_1
    //   279: aload #10
    //   281: getfield info : Landroid/content/pm/ActivityInfo;
    //   284: getfield configChanges : I
    //   287: putfield configChanges : I
    //   290: aload_1
    //   291: aload #10
    //   293: getfield info : Landroid/content/pm/ActivityInfo;
    //   296: getfield flags : I
    //   299: putfield flags : I
    //   302: aload_1
    //   303: aload #10
    //   305: getfield info : Landroid/content/pm/ActivityInfo;
    //   308: getfield privateFlags : I
    //   311: putfield privateFlags : I
    //   314: aload_1
    //   315: aload #10
    //   317: getfield info : Landroid/content/pm/ActivityInfo;
    //   320: getfield icon : I
    //   323: putfield icon : I
    //   326: aload_1
    //   327: aload #10
    //   329: getfield info : Landroid/content/pm/ActivityInfo;
    //   332: getfield logo : I
    //   335: putfield logo : I
    //   338: aload_1
    //   339: aload #10
    //   341: getfield info : Landroid/content/pm/ActivityInfo;
    //   344: getfield banner : I
    //   347: putfield banner : I
    //   350: aload_1
    //   351: aload #10
    //   353: getfield info : Landroid/content/pm/ActivityInfo;
    //   356: getfield labelRes : I
    //   359: putfield labelRes : I
    //   362: aload_1
    //   363: aload #10
    //   365: getfield info : Landroid/content/pm/ActivityInfo;
    //   368: getfield nonLocalizedLabel : Ljava/lang/CharSequence;
    //   371: putfield nonLocalizedLabel : Ljava/lang/CharSequence;
    //   374: aload_1
    //   375: aload #10
    //   377: getfield info : Landroid/content/pm/ActivityInfo;
    //   380: getfield launchMode : I
    //   383: putfield launchMode : I
    //   386: aload_1
    //   387: aload #10
    //   389: getfield info : Landroid/content/pm/ActivityInfo;
    //   392: getfield lockTaskLaunchMode : I
    //   395: putfield lockTaskLaunchMode : I
    //   398: aload_1
    //   399: aload #10
    //   401: getfield info : Landroid/content/pm/ActivityInfo;
    //   404: getfield processName : Ljava/lang/String;
    //   407: putfield processName : Ljava/lang/String;
    //   410: aload_1
    //   411: getfield descriptionRes : I
    //   414: ifne -> 429
    //   417: aload_1
    //   418: aload #10
    //   420: getfield info : Landroid/content/pm/ActivityInfo;
    //   423: getfield descriptionRes : I
    //   426: putfield descriptionRes : I
    //   429: aload_1
    //   430: aload #10
    //   432: getfield info : Landroid/content/pm/ActivityInfo;
    //   435: getfield screenOrientation : I
    //   438: putfield screenOrientation : I
    //   441: aload_1
    //   442: aload #10
    //   444: getfield info : Landroid/content/pm/ActivityInfo;
    //   447: getfield taskAffinity : Ljava/lang/String;
    //   450: putfield taskAffinity : Ljava/lang/String;
    //   453: aload_1
    //   454: aload #10
    //   456: getfield info : Landroid/content/pm/ActivityInfo;
    //   459: getfield theme : I
    //   462: putfield theme : I
    //   465: aload_1
    //   466: aload #10
    //   468: getfield info : Landroid/content/pm/ActivityInfo;
    //   471: getfield softInputMode : I
    //   474: putfield softInputMode : I
    //   477: aload_1
    //   478: aload #10
    //   480: getfield info : Landroid/content/pm/ActivityInfo;
    //   483: getfield uiOptions : I
    //   486: putfield uiOptions : I
    //   489: aload_1
    //   490: aload #10
    //   492: getfield info : Landroid/content/pm/ActivityInfo;
    //   495: getfield parentActivityName : Ljava/lang/String;
    //   498: putfield parentActivityName : Ljava/lang/String;
    //   501: aload_1
    //   502: aload #10
    //   504: getfield info : Landroid/content/pm/ActivityInfo;
    //   507: getfield maxRecents : I
    //   510: putfield maxRecents : I
    //   513: aload_1
    //   514: aload #10
    //   516: getfield info : Landroid/content/pm/ActivityInfo;
    //   519: getfield windowLayout : Landroid/content/pm/ActivityInfo$WindowLayout;
    //   522: putfield windowLayout : Landroid/content/pm/ActivityInfo$WindowLayout;
    //   525: aload_1
    //   526: aload #10
    //   528: getfield info : Landroid/content/pm/ActivityInfo;
    //   531: getfield resizeMode : I
    //   534: putfield resizeMode : I
    //   537: aload_1
    //   538: aload #10
    //   540: getfield info : Landroid/content/pm/ActivityInfo;
    //   543: getfield maxAspectRatio : F
    //   546: putfield maxAspectRatio : F
    //   549: aload_1
    //   550: aload #10
    //   552: getfield info : Landroid/content/pm/ActivityInfo;
    //   555: getfield minAspectRatio : F
    //   558: putfield minAspectRatio : F
    //   561: aload_1
    //   562: aload #10
    //   564: getfield info : Landroid/content/pm/ActivityInfo;
    //   567: getfield supportsSizeChanges : Z
    //   570: putfield supportsSizeChanges : Z
    //   573: aload_1
    //   574: aload #10
    //   576: getfield info : Landroid/content/pm/ActivityInfo;
    //   579: getfield requestedVrComponent : Ljava/lang/String;
    //   582: putfield requestedVrComponent : Ljava/lang/String;
    //   585: aload_1
    //   586: aload #10
    //   588: getfield info : Landroid/content/pm/ActivityInfo;
    //   591: getfield directBootAware : Z
    //   594: putfield directBootAware : Z
    //   597: new android/content/pm/PackageParser$Activity
    //   600: dup
    //   601: aload #6
    //   603: getfield mActivityAliasArgs : Landroid/content/pm/PackageParser$ParseComponentArgs;
    //   606: aload_1
    //   607: invokespecial <init> : (Landroid/content/pm/PackageParser$ParseComponentArgs;Landroid/content/pm/ActivityInfo;)V
    //   610: astore #11
    //   612: aload #8
    //   614: iconst_0
    //   615: aaload
    //   616: ifnull -> 626
    //   619: aload #9
    //   621: invokevirtual recycle : ()V
    //   624: aconst_null
    //   625: areturn
    //   626: aload #9
    //   628: iconst_5
    //   629: invokevirtual hasValue : (I)Z
    //   632: istore #14
    //   634: iload #14
    //   636: ifeq -> 657
    //   639: aload #11
    //   641: getfield info : Landroid/content/pm/ActivityInfo;
    //   644: aload #9
    //   646: iconst_5
    //   647: iconst_0
    //   648: invokevirtual getBoolean : (IZ)Z
    //   651: putfield exported : Z
    //   654: goto -> 657
    //   657: aload #9
    //   659: iconst_3
    //   660: iconst_0
    //   661: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   664: astore_1
    //   665: aload_1
    //   666: ifnull -> 702
    //   669: aload #11
    //   671: getfield info : Landroid/content/pm/ActivityInfo;
    //   674: astore #6
    //   676: aload_1
    //   677: invokevirtual length : ()I
    //   680: ifle -> 694
    //   683: aload_1
    //   684: invokevirtual toString : ()Ljava/lang/String;
    //   687: invokevirtual intern : ()Ljava/lang/String;
    //   690: astore_1
    //   691: goto -> 696
    //   694: aconst_null
    //   695: astore_1
    //   696: aload #6
    //   698: aload_1
    //   699: putfield permission : Ljava/lang/String;
    //   702: aload #9
    //   704: bipush #9
    //   706: sipush #1024
    //   709: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   712: astore_1
    //   713: ldc 'PackageParser'
    //   715: astore #12
    //   717: aload_1
    //   718: ifnull -> 821
    //   721: aload #11
    //   723: getfield info : Landroid/content/pm/ActivityInfo;
    //   726: getfield packageName : Ljava/lang/String;
    //   729: aload_1
    //   730: aload #8
    //   732: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;[Ljava/lang/String;)Ljava/lang/String;
    //   735: astore #6
    //   737: aload #8
    //   739: iconst_0
    //   740: aaload
    //   741: ifnonnull -> 757
    //   744: aload #11
    //   746: getfield info : Landroid/content/pm/ActivityInfo;
    //   749: aload #6
    //   751: putfield parentActivityName : Ljava/lang/String;
    //   754: goto -> 821
    //   757: new java/lang/StringBuilder
    //   760: dup
    //   761: invokespecial <init> : ()V
    //   764: astore #6
    //   766: aload #6
    //   768: ldc_w 'Activity alias '
    //   771: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   774: pop
    //   775: aload #6
    //   777: aload #11
    //   779: getfield info : Landroid/content/pm/ActivityInfo;
    //   782: getfield name : Ljava/lang/String;
    //   785: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   788: pop
    //   789: aload #6
    //   791: ldc_w ' specified invalid parentActivityName '
    //   794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   797: pop
    //   798: aload #6
    //   800: aload_1
    //   801: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   804: pop
    //   805: ldc 'PackageParser'
    //   807: aload #6
    //   809: invokevirtual toString : ()Ljava/lang/String;
    //   812: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   815: pop
    //   816: aload #8
    //   818: iconst_0
    //   819: aconst_null
    //   820: aastore
    //   821: aload #11
    //   823: getfield info : Landroid/content/pm/ActivityInfo;
    //   826: getfield flags : I
    //   829: ldc_w 1048576
    //   832: iand
    //   833: ifeq -> 842
    //   836: iconst_1
    //   837: istore #15
    //   839: goto -> 845
    //   842: iconst_0
    //   843: istore #15
    //   845: aload #9
    //   847: invokevirtual recycle : ()V
    //   850: aload_0
    //   851: aload #11
    //   853: aload #7
    //   855: aload_3
    //   856: aload #10
    //   858: invokevirtual hookActivityAliasTheme : (Landroid/content/pm/PackageParser$Activity;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/PackageParser$Activity;)V
    //   861: aload #8
    //   863: iconst_0
    //   864: aaload
    //   865: ifnull -> 870
    //   868: aconst_null
    //   869: areturn
    //   870: aload_3
    //   871: invokeinterface getDepth : ()I
    //   876: istore #4
    //   878: aload #10
    //   880: astore #6
    //   882: aload #8
    //   884: astore #10
    //   886: aload #9
    //   888: astore_1
    //   889: aload #12
    //   891: astore #8
    //   893: aload_3
    //   894: invokeinterface next : ()I
    //   899: istore #13
    //   901: iload #13
    //   903: iconst_1
    //   904: if_icmpeq -> 1343
    //   907: iload #13
    //   909: iconst_3
    //   910: if_icmpne -> 930
    //   913: aload_3
    //   914: invokeinterface getDepth : ()I
    //   919: iload #4
    //   921: if_icmple -> 927
    //   924: goto -> 930
    //   927: goto -> 1343
    //   930: iload #13
    //   932: iconst_3
    //   933: if_icmpeq -> 1340
    //   936: iload #13
    //   938: iconst_4
    //   939: if_icmpne -> 945
    //   942: goto -> 893
    //   945: aload_3
    //   946: invokeinterface getName : ()Ljava/lang/String;
    //   951: ldc_w 'intent-filter'
    //   954: invokevirtual equals : (Ljava/lang/Object;)Z
    //   957: ifeq -> 1195
    //   960: new android/content/pm/PackageParser$ActivityIntentInfo
    //   963: dup
    //   964: aload #11
    //   966: invokespecial <init> : (Landroid/content/pm/PackageParser$Activity;)V
    //   969: astore #7
    //   971: aload_0
    //   972: aload_2
    //   973: aload_3
    //   974: iconst_1
    //   975: iconst_1
    //   976: aload #7
    //   978: aload #5
    //   980: invokespecial parseIntent : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZLandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;)Z
    //   983: ifne -> 988
    //   986: aconst_null
    //   987: areturn
    //   988: aload #7
    //   990: invokevirtual countActions : ()I
    //   993: ifne -> 1063
    //   996: new java/lang/StringBuilder
    //   999: dup
    //   1000: invokespecial <init> : ()V
    //   1003: astore #9
    //   1005: aload #9
    //   1007: ldc_w 'No actions in intent filter at '
    //   1010: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1013: pop
    //   1014: aload #9
    //   1016: aload_0
    //   1017: getfield mArchiveSourcePath : Ljava/lang/String;
    //   1020: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1023: pop
    //   1024: aload #9
    //   1026: ldc_w ' '
    //   1029: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1032: pop
    //   1033: aload #9
    //   1035: aload_3
    //   1036: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   1041: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1044: pop
    //   1045: aload #9
    //   1047: invokevirtual toString : ()Ljava/lang/String;
    //   1050: astore #9
    //   1052: aload #8
    //   1054: aload #9
    //   1056: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1059: pop
    //   1060: goto -> 1092
    //   1063: aload #11
    //   1065: aload #7
    //   1067: invokevirtual getOrder : ()I
    //   1070: aload #11
    //   1072: getfield order : I
    //   1075: invokestatic max : (II)I
    //   1078: putfield order : I
    //   1081: aload #11
    //   1083: getfield intents : Ljava/util/ArrayList;
    //   1086: aload #7
    //   1088: invokevirtual add : (Ljava/lang/Object;)Z
    //   1091: pop
    //   1092: iload #15
    //   1094: ifeq -> 1103
    //   1097: iconst_1
    //   1098: istore #13
    //   1100: goto -> 1121
    //   1103: aload_0
    //   1104: aload #7
    //   1106: invokespecial isImplicitlyExposedIntent : (Landroid/content/pm/PackageParser$IntentInfo;)Z
    //   1109: ifeq -> 1118
    //   1112: iconst_2
    //   1113: istore #13
    //   1115: goto -> 1121
    //   1118: iconst_0
    //   1119: istore #13
    //   1121: aload #7
    //   1123: iload #13
    //   1125: invokevirtual setVisibilityToInstantApp : (I)V
    //   1128: aload #7
    //   1130: invokevirtual isVisibleToInstantApp : ()Z
    //   1133: ifeq -> 1160
    //   1136: aload #11
    //   1138: getfield info : Landroid/content/pm/ActivityInfo;
    //   1141: astore #9
    //   1143: aload #9
    //   1145: aload #9
    //   1147: getfield flags : I
    //   1150: ldc_w 1048576
    //   1153: ior
    //   1154: putfield flags : I
    //   1157: goto -> 1160
    //   1160: aload #7
    //   1162: invokevirtual isImplicitlyVisibleToInstantApp : ()Z
    //   1165: ifeq -> 1189
    //   1168: aload #11
    //   1170: getfield info : Landroid/content/pm/ActivityInfo;
    //   1173: astore #7
    //   1175: aload #7
    //   1177: aload #7
    //   1179: getfield flags : I
    //   1182: ldc_w 2097152
    //   1185: ior
    //   1186: putfield flags : I
    //   1189: aload_2
    //   1190: astore #7
    //   1192: goto -> 893
    //   1195: aload_3
    //   1196: invokeinterface getName : ()Ljava/lang/String;
    //   1201: ldc_w 'meta-data'
    //   1204: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1207: ifeq -> 1245
    //   1210: aload_0
    //   1211: aload_2
    //   1212: aload_3
    //   1213: aload #11
    //   1215: getfield metaData : Landroid/os/Bundle;
    //   1218: aload #10
    //   1220: invokespecial parseMetaData : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    //   1223: astore #7
    //   1225: aload #11
    //   1227: aload #7
    //   1229: putfield metaData : Landroid/os/Bundle;
    //   1232: aload #7
    //   1234: ifnonnull -> 1239
    //   1237: aconst_null
    //   1238: areturn
    //   1239: aload_2
    //   1240: astore #7
    //   1242: goto -> 893
    //   1245: new java/lang/StringBuilder
    //   1248: dup
    //   1249: invokespecial <init> : ()V
    //   1252: astore #7
    //   1254: aload #7
    //   1256: ldc_w 'Unknown element under <activity-alias>: '
    //   1259: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1262: pop
    //   1263: aload #7
    //   1265: aload_3
    //   1266: invokeinterface getName : ()Ljava/lang/String;
    //   1271: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1274: pop
    //   1275: aload #7
    //   1277: ldc_w ' at '
    //   1280: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1283: pop
    //   1284: aload #7
    //   1286: aload_0
    //   1287: getfield mArchiveSourcePath : Ljava/lang/String;
    //   1290: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1293: pop
    //   1294: aload #7
    //   1296: ldc_w ' '
    //   1299: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1302: pop
    //   1303: aload #7
    //   1305: aload_3
    //   1306: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   1311: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1314: pop
    //   1315: aload #7
    //   1317: invokevirtual toString : ()Ljava/lang/String;
    //   1320: astore #7
    //   1322: aload #8
    //   1324: aload #7
    //   1326: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1329: pop
    //   1330: aload_3
    //   1331: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   1334: aload_2
    //   1335: astore #7
    //   1337: goto -> 893
    //   1340: goto -> 893
    //   1343: iload #14
    //   1345: ifne -> 1380
    //   1348: aload #11
    //   1350: getfield info : Landroid/content/pm/ActivityInfo;
    //   1353: astore_1
    //   1354: aload #11
    //   1356: getfield intents : Ljava/util/ArrayList;
    //   1359: invokevirtual size : ()I
    //   1362: ifle -> 1371
    //   1365: iconst_1
    //   1366: istore #14
    //   1368: goto -> 1374
    //   1371: iconst_0
    //   1372: istore #14
    //   1374: aload_1
    //   1375: iload #14
    //   1377: putfield exported : Z
    //   1380: aload #11
    //   1382: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #4859	-> 0
    //   #4862	-> 18
    //   #4865	-> 30
    //   #4866	-> 35
    //   #4867	-> 42
    //   #4868	-> 47
    //   #4871	-> 49
    //   #4873	-> 65
    //   #4874	-> 70
    //   #4875	-> 75
    //   #4878	-> 77
    //   #4879	-> 85
    //   #4890	-> 117
    //   #4893	-> 128
    //   #4894	-> 138
    //   #4896	-> 148
    //   #4898	-> 151
    //   #4899	-> 160
    //   #4900	-> 174
    //   #4901	-> 188
    //   #4902	-> 204
    //   #4903	-> 204
    //   #4899	-> 207
    //   #4907	-> 213
    //   #4908	-> 218
    //   #4910	-> 257
    //   #4911	-> 262
    //   #4914	-> 264
    //   #4915	-> 272
    //   #4916	-> 278
    //   #4917	-> 290
    //   #4918	-> 302
    //   #4919	-> 314
    //   #4920	-> 326
    //   #4921	-> 338
    //   #4922	-> 350
    //   #4923	-> 362
    //   #4924	-> 374
    //   #4925	-> 386
    //   #4926	-> 398
    //   #4927	-> 410
    //   #4928	-> 417
    //   #4930	-> 429
    //   #4931	-> 441
    //   #4932	-> 453
    //   #4933	-> 465
    //   #4934	-> 477
    //   #4935	-> 489
    //   #4936	-> 501
    //   #4937	-> 513
    //   #4938	-> 525
    //   #4939	-> 537
    //   #4940	-> 549
    //   #4941	-> 561
    //   #4942	-> 573
    //   #4944	-> 585
    //   #4946	-> 597
    //   #4947	-> 612
    //   #4948	-> 619
    //   #4949	-> 624
    //   #4952	-> 626
    //   #4954	-> 634
    //   #4955	-> 639
    //   #4954	-> 657
    //   #4960	-> 657
    //   #4962	-> 665
    //   #4963	-> 669
    //   #4966	-> 702
    //   #4969	-> 713
    //   #4970	-> 721
    //   #4971	-> 737
    //   #4972	-> 744
    //   #4974	-> 757
    //   #4976	-> 816
    //   #4981	-> 821
    //   #4984	-> 845
    //   #4988	-> 850
    //   #4990	-> 861
    //   #4991	-> 868
    //   #4994	-> 870
    //   #4996	-> 893
    //   #4998	-> 913
    //   #4999	-> 930
    //   #5000	-> 942
    //   #5003	-> 945
    //   #5004	-> 960
    //   #5005	-> 971
    //   #5007	-> 986
    //   #5009	-> 988
    //   #5010	-> 996
    //   #5012	-> 1033
    //   #5010	-> 1052
    //   #5014	-> 1063
    //   #5015	-> 1081
    //   #5018	-> 1092
    //   #5019	-> 1097
    //   #5020	-> 1103
    //   #5021	-> 1112
    //   #5022	-> 1118
    //   #5023	-> 1121
    //   #5024	-> 1128
    //   #5025	-> 1136
    //   #5024	-> 1160
    //   #5027	-> 1160
    //   #5028	-> 1168
    //   #5030	-> 1189
    //   #5031	-> 1210
    //   #5033	-> 1237
    //   #5031	-> 1239
    //   #5037	-> 1245
    //   #5039	-> 1303
    //   #5037	-> 1322
    //   #5040	-> 1330
    //   #5041	-> 1334
    //   #4999	-> 1340
    //   #4996	-> 1343
    //   #5049	-> 1343
    //   #5050	-> 1348
    //   #5053	-> 1380
  }
  
  private Provider parseProvider(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString, CachedComponentArgs paramCachedComponentArgs) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProvider);
    if (paramCachedComponentArgs.mProviderArgs == null) {
      paramCachedComponentArgs.mProviderArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 2, 0, 1, 19, 15, 17, this.mSeparateProcesses, 8, 14, 6);
      paramCachedComponentArgs.mProviderArgs.tag = "<provider>";
    } 
    paramCachedComponentArgs.mProviderArgs.sa = typedArray;
    paramCachedComponentArgs.mProviderArgs.flags = paramInt;
    Provider provider = new Provider(new ProviderInfo());
    if (paramArrayOfString[0] != null) {
      typedArray.recycle();
      return null;
    } 
    if (paramPackage.applicationInfo.targetSdkVersion < 17) {
      bool = true;
    } else {
      bool = false;
    } 
    provider.info.exported = typedArray.getBoolean(7, bool);
    String str2 = typedArray.getNonConfigurationString(10, 0);
    provider.info.isSyncable = typedArray.getBoolean(11, false);
    String str1 = typedArray.getNonConfigurationString(3, 0);
    String str3 = typedArray.getNonConfigurationString(4, 0);
    String str4 = str3;
    if (str3 == null)
      str4 = str1; 
    if (str4 == null) {
      provider.info.readPermission = paramPackage.applicationInfo.permission;
    } else {
      ProviderInfo providerInfo1 = provider.info;
      if (str4.length() > 0) {
        str4 = str4.toString().intern();
      } else {
        str4 = null;
      } 
      providerInfo1.readPermission = str4;
    } 
    str4 = typedArray.getNonConfigurationString(5, 0);
    if (str4 != null)
      str1 = str4; 
    if (str1 == null) {
      provider.info.writePermission = paramPackage.applicationInfo.permission;
    } else {
      ProviderInfo providerInfo1 = provider.info;
      if (str1.length() > 0) {
        str1 = str1.toString().intern();
      } else {
        str1 = null;
      } 
      providerInfo1.writePermission = str1;
    } 
    provider.info.grantUriPermissions = typedArray.getBoolean(13, false);
    provider.info.forceUriPermissions = typedArray.getBoolean(22, false);
    provider.info.multiprocess = typedArray.getBoolean(9, false);
    provider.info.initOrder = typedArray.getInt(12, 0);
    ProviderInfo providerInfo = provider.info;
    providerInfo.splitName = typedArray.getNonConfigurationString(21, 0);
    provider.info.flags = 0;
    if (typedArray.getBoolean(16, false)) {
      providerInfo = provider.info;
      providerInfo.flags |= 0x40000000;
    } 
    provider.info.directBootAware = typedArray.getBoolean(18, false);
    if (provider.info.directBootAware) {
      ApplicationInfo applicationInfo = paramPackage.applicationInfo;
      applicationInfo.privateFlags |= 0x100;
    } 
    boolean bool = typedArray.getBoolean(20, false);
    if (bool) {
      providerInfo = provider.info;
      providerInfo.flags |= 0x100000;
      paramPackage.visibleToInstantApps = true;
    } 
    typedArray.recycle();
    if ((paramPackage.applicationInfo.privateFlags & 0x2) != 0)
      if (provider.info.processName == paramPackage.packageName) {
        paramArrayOfString[0] = "Heavy-weight applications can not have providers in main process";
        return null;
      }  
    if (str2 == null) {
      paramArrayOfString[0] = "<provider> does not include authorities attribute";
      return null;
    } 
    if (str2.length() <= 0) {
      paramArrayOfString[0] = "<provider> has empty authorities attribute";
      return null;
    } 
    provider.info.authority = str2.intern();
    if (!parseProviderTags(paramResources, paramXmlResourceParser, bool, provider, paramArrayOfString))
      return null; 
    return provider;
  }
  
  private boolean parseProviderTags(Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, Provider paramProvider, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_2
    //   1: invokeinterface getDepth : ()I
    //   6: istore #6
    //   8: aload_2
    //   9: invokeinterface next : ()I
    //   14: istore #7
    //   16: iload #7
    //   18: iconst_1
    //   19: if_icmpeq -> 1166
    //   22: iload #7
    //   24: iconst_3
    //   25: if_icmpne -> 39
    //   28: aload_2
    //   29: invokeinterface getDepth : ()I
    //   34: iload #6
    //   36: if_icmple -> 1166
    //   39: iload #7
    //   41: iconst_3
    //   42: if_icmpeq -> 8
    //   45: iload #7
    //   47: iconst_4
    //   48: if_icmpne -> 54
    //   51: goto -> 8
    //   54: aload_2
    //   55: invokeinterface getName : ()Ljava/lang/String;
    //   60: ldc_w 'intent-filter'
    //   63: invokevirtual equals : (Ljava/lang/Object;)Z
    //   66: ifeq -> 160
    //   69: new android/content/pm/PackageParser$ProviderIntentInfo
    //   72: dup
    //   73: aload #4
    //   75: invokespecial <init> : (Landroid/content/pm/PackageParser$Provider;)V
    //   78: astore #8
    //   80: aload_0
    //   81: aload_1
    //   82: aload_2
    //   83: iconst_1
    //   84: iconst_0
    //   85: aload #8
    //   87: aload #5
    //   89: invokespecial parseIntent : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZLandroid/content/pm/PackageParser$IntentInfo;[Ljava/lang/String;)Z
    //   92: ifne -> 97
    //   95: iconst_0
    //   96: ireturn
    //   97: iload_3
    //   98: ifeq -> 128
    //   101: aload #8
    //   103: iconst_1
    //   104: invokevirtual setVisibilityToInstantApp : (I)V
    //   107: aload #4
    //   109: getfield info : Landroid/content/pm/ProviderInfo;
    //   112: astore #9
    //   114: aload #9
    //   116: aload #9
    //   118: getfield flags : I
    //   121: ldc_w 1048576
    //   124: ior
    //   125: putfield flags : I
    //   128: aload #4
    //   130: aload #8
    //   132: invokevirtual getOrder : ()I
    //   135: aload #4
    //   137: getfield order : I
    //   140: invokestatic max : (II)I
    //   143: putfield order : I
    //   146: aload #4
    //   148: getfield intents : Ljava/util/ArrayList;
    //   151: aload #8
    //   153: invokevirtual add : (Ljava/lang/Object;)Z
    //   156: pop
    //   157: goto -> 8
    //   160: aload_2
    //   161: invokeinterface getName : ()Ljava/lang/String;
    //   166: ldc_w 'meta-data'
    //   169: invokevirtual equals : (Ljava/lang/Object;)Z
    //   172: ifeq -> 204
    //   175: aload_0
    //   176: aload_1
    //   177: aload_2
    //   178: aload #4
    //   180: getfield metaData : Landroid/os/Bundle;
    //   183: aload #5
    //   185: invokespecial parseMetaData : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    //   188: astore #9
    //   190: aload #4
    //   192: aload #9
    //   194: putfield metaData : Landroid/os/Bundle;
    //   197: aload #9
    //   199: ifnonnull -> 8
    //   202: iconst_0
    //   203: ireturn
    //   204: aload_2
    //   205: invokeinterface getName : ()Ljava/lang/String;
    //   210: ldc_w 'grant-uri-permission'
    //   213: invokevirtual equals : (Ljava/lang/Object;)Z
    //   216: ifeq -> 520
    //   219: aload_1
    //   220: aload_2
    //   221: getstatic com/android/internal/R$styleable.AndroidManifestGrantUriPermission : [I
    //   224: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   227: astore #8
    //   229: aconst_null
    //   230: astore #9
    //   232: aload #8
    //   234: iconst_0
    //   235: iconst_0
    //   236: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   239: astore #10
    //   241: aload #10
    //   243: ifnull -> 258
    //   246: new android/os/PatternMatcher
    //   249: dup
    //   250: aload #10
    //   252: iconst_0
    //   253: invokespecial <init> : (Ljava/lang/String;I)V
    //   256: astore #9
    //   258: aload #8
    //   260: iconst_1
    //   261: iconst_0
    //   262: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   265: astore #10
    //   267: aload #10
    //   269: ifnull -> 284
    //   272: new android/os/PatternMatcher
    //   275: dup
    //   276: aload #10
    //   278: iconst_1
    //   279: invokespecial <init> : (Ljava/lang/String;I)V
    //   282: astore #9
    //   284: aload #8
    //   286: iconst_2
    //   287: iconst_0
    //   288: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   291: astore #10
    //   293: aload #10
    //   295: ifnull -> 310
    //   298: new android/os/PatternMatcher
    //   301: dup
    //   302: aload #10
    //   304: iconst_2
    //   305: invokespecial <init> : (Ljava/lang/String;I)V
    //   308: astore #9
    //   310: aload #8
    //   312: invokevirtual recycle : ()V
    //   315: aload #9
    //   317: ifnull -> 428
    //   320: aload #4
    //   322: getfield info : Landroid/content/pm/ProviderInfo;
    //   325: getfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   328: ifnonnull -> 358
    //   331: aload #4
    //   333: getfield info : Landroid/content/pm/ProviderInfo;
    //   336: iconst_1
    //   337: anewarray android/os/PatternMatcher
    //   340: putfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   343: aload #4
    //   345: getfield info : Landroid/content/pm/ProviderInfo;
    //   348: getfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   351: iconst_0
    //   352: aload #9
    //   354: aastore
    //   355: goto -> 412
    //   358: aload #4
    //   360: getfield info : Landroid/content/pm/ProviderInfo;
    //   363: getfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   366: arraylength
    //   367: istore #7
    //   369: iload #7
    //   371: iconst_1
    //   372: iadd
    //   373: anewarray android/os/PatternMatcher
    //   376: astore #8
    //   378: aload #4
    //   380: getfield info : Landroid/content/pm/ProviderInfo;
    //   383: getfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   386: iconst_0
    //   387: aload #8
    //   389: iconst_0
    //   390: iload #7
    //   392: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   395: aload #8
    //   397: iload #7
    //   399: aload #9
    //   401: aastore
    //   402: aload #4
    //   404: getfield info : Landroid/content/pm/ProviderInfo;
    //   407: aload #8
    //   409: putfield uriPermissionPatterns : [Landroid/os/PatternMatcher;
    //   412: aload #4
    //   414: getfield info : Landroid/content/pm/ProviderInfo;
    //   417: iconst_1
    //   418: putfield grantUriPermissions : Z
    //   421: aload_2
    //   422: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   425: goto -> 8
    //   428: new java/lang/StringBuilder
    //   431: dup
    //   432: invokespecial <init> : ()V
    //   435: astore #9
    //   437: aload #9
    //   439: ldc_w 'Unknown element under <path-permission>: '
    //   442: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   445: pop
    //   446: aload #9
    //   448: aload_2
    //   449: invokeinterface getName : ()Ljava/lang/String;
    //   454: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   457: pop
    //   458: aload #9
    //   460: ldc_w ' at '
    //   463: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   466: pop
    //   467: aload #9
    //   469: aload_0
    //   470: getfield mArchiveSourcePath : Ljava/lang/String;
    //   473: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   476: pop
    //   477: aload #9
    //   479: ldc_w ' '
    //   482: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   485: pop
    //   486: aload #9
    //   488: aload_2
    //   489: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   494: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   497: pop
    //   498: aload #9
    //   500: invokevirtual toString : ()Ljava/lang/String;
    //   503: astore #9
    //   505: ldc 'PackageParser'
    //   507: aload #9
    //   509: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   512: pop
    //   513: aload_2
    //   514: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   517: goto -> 8
    //   520: aload_2
    //   521: invokeinterface getName : ()Ljava/lang/String;
    //   526: ldc_w 'path-permission'
    //   529: invokevirtual equals : (Ljava/lang/Object;)Z
    //   532: ifeq -> 1074
    //   535: aload_1
    //   536: aload_2
    //   537: getstatic com/android/internal/R$styleable.AndroidManifestPathPermission : [I
    //   540: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   543: astore #11
    //   545: aload #11
    //   547: iconst_0
    //   548: iconst_0
    //   549: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   552: astore #10
    //   554: aload #11
    //   556: iconst_1
    //   557: iconst_0
    //   558: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   561: astore #8
    //   563: aload #8
    //   565: ifnonnull -> 575
    //   568: aload #10
    //   570: astore #8
    //   572: goto -> 575
    //   575: aload #11
    //   577: iconst_2
    //   578: iconst_0
    //   579: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   582: astore #12
    //   584: aload #12
    //   586: astore #9
    //   588: aload #12
    //   590: ifnonnull -> 597
    //   593: aload #10
    //   595: astore #9
    //   597: iconst_0
    //   598: istore #7
    //   600: aload #8
    //   602: ifnull -> 618
    //   605: aload #8
    //   607: invokevirtual intern : ()Ljava/lang/String;
    //   610: astore #8
    //   612: iconst_1
    //   613: istore #7
    //   615: goto -> 618
    //   618: aload #9
    //   620: ifnull -> 636
    //   623: aload #9
    //   625: invokevirtual intern : ()Ljava/lang/String;
    //   628: astore #10
    //   630: iconst_1
    //   631: istore #7
    //   633: goto -> 640
    //   636: aload #9
    //   638: astore #10
    //   640: iload #7
    //   642: ifne -> 737
    //   645: new java/lang/StringBuilder
    //   648: dup
    //   649: invokespecial <init> : ()V
    //   652: astore #9
    //   654: aload #9
    //   656: ldc_w 'No readPermission or writePermssion for <path-permission>: '
    //   659: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   662: pop
    //   663: aload #9
    //   665: aload_2
    //   666: invokeinterface getName : ()Ljava/lang/String;
    //   671: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   674: pop
    //   675: aload #9
    //   677: ldc_w ' at '
    //   680: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   683: pop
    //   684: aload #9
    //   686: aload_0
    //   687: getfield mArchiveSourcePath : Ljava/lang/String;
    //   690: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   693: pop
    //   694: aload #9
    //   696: ldc_w ' '
    //   699: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   702: pop
    //   703: aload #9
    //   705: aload_2
    //   706: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   711: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   714: pop
    //   715: aload #9
    //   717: invokevirtual toString : ()Ljava/lang/String;
    //   720: astore #9
    //   722: ldc 'PackageParser'
    //   724: aload #9
    //   726: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   729: pop
    //   730: aload_2
    //   731: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   734: goto -> 8
    //   737: aload #11
    //   739: iconst_3
    //   740: iconst_0
    //   741: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   744: astore #9
    //   746: aload #9
    //   748: ifnull -> 770
    //   751: new android/content/pm/PathPermission
    //   754: dup
    //   755: aload #9
    //   757: iconst_0
    //   758: aload #8
    //   760: aload #10
    //   762: invokespecial <init> : (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    //   765: astore #9
    //   767: goto -> 773
    //   770: aconst_null
    //   771: astore #9
    //   773: aload #11
    //   775: iconst_4
    //   776: iconst_0
    //   777: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   780: astore #12
    //   782: aload #12
    //   784: ifnull -> 806
    //   787: new android/content/pm/PathPermission
    //   790: dup
    //   791: aload #12
    //   793: iconst_1
    //   794: aload #8
    //   796: aload #10
    //   798: invokespecial <init> : (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    //   801: astore #9
    //   803: goto -> 806
    //   806: aload #11
    //   808: iconst_5
    //   809: iconst_0
    //   810: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   813: astore #12
    //   815: aload #12
    //   817: ifnull -> 839
    //   820: new android/content/pm/PathPermission
    //   823: dup
    //   824: aload #12
    //   826: iconst_2
    //   827: aload #8
    //   829: aload #10
    //   831: invokespecial <init> : (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    //   834: astore #9
    //   836: goto -> 839
    //   839: aload #11
    //   841: bipush #6
    //   843: iconst_0
    //   844: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   847: astore #12
    //   849: aload #12
    //   851: ifnull -> 873
    //   854: new android/content/pm/PathPermission
    //   857: dup
    //   858: aload #12
    //   860: iconst_3
    //   861: aload #8
    //   863: aload #10
    //   865: invokespecial <init> : (Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    //   868: astore #9
    //   870: goto -> 873
    //   873: aload #11
    //   875: invokevirtual recycle : ()V
    //   878: aload #9
    //   880: ifnull -> 982
    //   883: aload #4
    //   885: getfield info : Landroid/content/pm/ProviderInfo;
    //   888: getfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   891: ifnonnull -> 921
    //   894: aload #4
    //   896: getfield info : Landroid/content/pm/ProviderInfo;
    //   899: iconst_1
    //   900: anewarray android/content/pm/PathPermission
    //   903: putfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   906: aload #4
    //   908: getfield info : Landroid/content/pm/ProviderInfo;
    //   911: getfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   914: iconst_0
    //   915: aload #9
    //   917: aastore
    //   918: goto -> 975
    //   921: aload #4
    //   923: getfield info : Landroid/content/pm/ProviderInfo;
    //   926: getfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   929: arraylength
    //   930: istore #7
    //   932: iload #7
    //   934: iconst_1
    //   935: iadd
    //   936: anewarray android/content/pm/PathPermission
    //   939: astore #8
    //   941: aload #4
    //   943: getfield info : Landroid/content/pm/ProviderInfo;
    //   946: getfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   949: iconst_0
    //   950: aload #8
    //   952: iconst_0
    //   953: iload #7
    //   955: invokestatic arraycopy : (Ljava/lang/Object;ILjava/lang/Object;II)V
    //   958: aload #8
    //   960: iload #7
    //   962: aload #9
    //   964: aastore
    //   965: aload #4
    //   967: getfield info : Landroid/content/pm/ProviderInfo;
    //   970: aload #8
    //   972: putfield pathPermissions : [Landroid/content/pm/PathPermission;
    //   975: aload_2
    //   976: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   979: goto -> 8
    //   982: new java/lang/StringBuilder
    //   985: dup
    //   986: invokespecial <init> : ()V
    //   989: astore #9
    //   991: aload #9
    //   993: ldc_w 'No path, pathPrefix, or pathPattern for <path-permission>: '
    //   996: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   999: pop
    //   1000: aload #9
    //   1002: aload_2
    //   1003: invokeinterface getName : ()Ljava/lang/String;
    //   1008: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1011: pop
    //   1012: aload #9
    //   1014: ldc_w ' at '
    //   1017: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1020: pop
    //   1021: aload #9
    //   1023: aload_0
    //   1024: getfield mArchiveSourcePath : Ljava/lang/String;
    //   1027: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1030: pop
    //   1031: aload #9
    //   1033: ldc_w ' '
    //   1036: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1039: pop
    //   1040: aload #9
    //   1042: aload_2
    //   1043: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   1048: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1051: pop
    //   1052: aload #9
    //   1054: invokevirtual toString : ()Ljava/lang/String;
    //   1057: astore #9
    //   1059: ldc 'PackageParser'
    //   1061: aload #9
    //   1063: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1066: pop
    //   1067: aload_2
    //   1068: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   1071: goto -> 8
    //   1074: new java/lang/StringBuilder
    //   1077: dup
    //   1078: invokespecial <init> : ()V
    //   1081: astore #9
    //   1083: aload #9
    //   1085: ldc_w 'Unknown element under <provider>: '
    //   1088: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1091: pop
    //   1092: aload #9
    //   1094: aload_2
    //   1095: invokeinterface getName : ()Ljava/lang/String;
    //   1100: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1103: pop
    //   1104: aload #9
    //   1106: ldc_w ' at '
    //   1109: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1112: pop
    //   1113: aload #9
    //   1115: aload_0
    //   1116: getfield mArchiveSourcePath : Ljava/lang/String;
    //   1119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1122: pop
    //   1123: aload #9
    //   1125: ldc_w ' '
    //   1128: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1131: pop
    //   1132: aload #9
    //   1134: aload_2
    //   1135: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   1140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1143: pop
    //   1144: aload #9
    //   1146: invokevirtual toString : ()Ljava/lang/String;
    //   1149: astore #9
    //   1151: ldc 'PackageParser'
    //   1153: aload #9
    //   1155: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1158: pop
    //   1159: aload_2
    //   1160: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   1163: goto -> 8
    //   1166: iconst_1
    //   1167: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5207	-> 0
    //   #5209	-> 8
    //   #5211	-> 28
    //   #5212	-> 39
    //   #5213	-> 51
    //   #5216	-> 54
    //   #5217	-> 69
    //   #5218	-> 80
    //   #5220	-> 95
    //   #5222	-> 97
    //   #5223	-> 101
    //   #5224	-> 107
    //   #5226	-> 128
    //   #5227	-> 146
    //   #5229	-> 157
    //   #5230	-> 175
    //   #5232	-> 202
    //   #5235	-> 204
    //   #5236	-> 219
    //   #5239	-> 229
    //   #5241	-> 232
    //   #5243	-> 241
    //   #5244	-> 246
    //   #5247	-> 258
    //   #5249	-> 267
    //   #5250	-> 272
    //   #5253	-> 284
    //   #5255	-> 293
    //   #5256	-> 298
    //   #5259	-> 310
    //   #5261	-> 315
    //   #5262	-> 320
    //   #5263	-> 331
    //   #5264	-> 343
    //   #5266	-> 358
    //   #5267	-> 369
    //   #5268	-> 378
    //   #5269	-> 395
    //   #5270	-> 402
    //   #5272	-> 412
    //   #5285	-> 421
    //   #5287	-> 425
    //   #5275	-> 428
    //   #5276	-> 446
    //   #5277	-> 486
    //   #5275	-> 505
    //   #5278	-> 513
    //   #5279	-> 517
    //   #5287	-> 520
    //   #5288	-> 535
    //   #5291	-> 545
    //   #5293	-> 545
    //   #5295	-> 554
    //   #5297	-> 563
    //   #5298	-> 568
    //   #5297	-> 575
    //   #5300	-> 575
    //   #5302	-> 584
    //   #5303	-> 593
    //   #5306	-> 597
    //   #5307	-> 600
    //   #5308	-> 605
    //   #5309	-> 612
    //   #5307	-> 618
    //   #5311	-> 618
    //   #5312	-> 623
    //   #5313	-> 630
    //   #5311	-> 636
    //   #5316	-> 640
    //   #5318	-> 645
    //   #5319	-> 663
    //   #5320	-> 703
    //   #5318	-> 722
    //   #5321	-> 730
    //   #5322	-> 734
    //   #5329	-> 737
    //   #5331	-> 746
    //   #5332	-> 751
    //   #5331	-> 770
    //   #5336	-> 773
    //   #5338	-> 782
    //   #5339	-> 787
    //   #5338	-> 806
    //   #5343	-> 806
    //   #5345	-> 815
    //   #5346	-> 820
    //   #5345	-> 839
    //   #5350	-> 839
    //   #5352	-> 849
    //   #5353	-> 854
    //   #5352	-> 873
    //   #5357	-> 873
    //   #5359	-> 878
    //   #5360	-> 883
    //   #5361	-> 894
    //   #5362	-> 906
    //   #5364	-> 921
    //   #5365	-> 932
    //   #5366	-> 941
    //   #5367	-> 958
    //   #5368	-> 965
    //   #5369	-> 975
    //   #5381	-> 975
    //   #5383	-> 979
    //   #5372	-> 982
    //   #5373	-> 1000
    //   #5374	-> 1040
    //   #5372	-> 1059
    //   #5375	-> 1067
    //   #5376	-> 1071
    //   #5385	-> 1074
    //   #5386	-> 1092
    //   #5387	-> 1132
    //   #5385	-> 1151
    //   #5388	-> 1159
    //   #5389	-> 1163
    //   #5396	-> 1166
  }
  
  private Service parseService(Package paramPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, String[] paramArrayOfString, CachedComponentArgs paramCachedComponentArgs) throws XmlPullParserException, IOException {
    XmlResourceParser xmlResourceParser2 = paramXmlResourceParser;
    String[] arrayOfString = paramArrayOfString;
    TypedArray typedArray2 = paramResources.obtainAttributes(xmlResourceParser2, R.styleable.AndroidManifestService);
    if (paramCachedComponentArgs.mServiceArgs == null) {
      paramCachedComponentArgs.mServiceArgs = new ParseComponentArgs(paramPackage, paramArrayOfString, 2, 0, 1, 15, 8, 12, this.mSeparateProcesses, 6, 7, 4);
      paramCachedComponentArgs.mServiceArgs.tag = "<service>";
    } 
    paramCachedComponentArgs.mServiceArgs.sa = typedArray2;
    paramCachedComponentArgs.mServiceArgs.flags = paramInt;
    Service service = new Service(new ServiceInfo());
    if (arrayOfString[0] != null) {
      typedArray2.recycle();
      return null;
    } 
    boolean bool1 = typedArray2.hasValue(5);
    if (bool1)
      service.info.exported = typedArray2.getBoolean(5, false); 
    String str = typedArray2.getNonConfigurationString(3, 0);
    if (str == null) {
      service.info.permission = paramPackage.applicationInfo.permission;
    } else {
      ServiceInfo serviceInfo1 = service.info;
      if (str.length() > 0) {
        str = str.toString().intern();
      } else {
        str = null;
      } 
      serviceInfo1.permission = str;
    } 
    ServiceInfo serviceInfo = service.info;
    serviceInfo.splitName = typedArray2.getNonConfigurationString(17, 0);
    service.info.mForegroundServiceType = typedArray2.getInt(19, 0);
    service.info.flags = 0;
    boolean bool2 = typedArray2.getBoolean(9, false);
    if (bool2) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x1;
    } 
    if (typedArray2.getBoolean(10, false)) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x2;
    } 
    if (typedArray2.getBoolean(14, false)) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x4;
    } 
    if (typedArray2.getBoolean(18, false)) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x8;
    } 
    if (typedArray2.getBoolean(11, false)) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x40000000;
    } 
    service.info.directBootAware = typedArray2.getBoolean(13, false);
    if (service.info.directBootAware) {
      ApplicationInfo applicationInfo = paramPackage.applicationInfo;
      applicationInfo.privateFlags |= 0x100;
    } 
    bool2 = typedArray2.getBoolean(16, false);
    if (bool2) {
      serviceInfo = service.info;
      serviceInfo.flags |= 0x100000;
      paramPackage.visibleToInstantApps = true;
    } 
    typedArray2.recycle();
    if ((paramPackage.applicationInfo.privateFlags & 0x2) != 0)
      if (service.info.processName == paramPackage.packageName) {
        arrayOfString[0] = "Heavy-weight applications can not have services in main process";
        return null;
      }  
    int i = paramXmlResourceParser.getDepth();
    XmlResourceParser xmlResourceParser1 = xmlResourceParser2;
    TypedArray typedArray1 = typedArray2;
    while (true) {
      paramInt = paramXmlResourceParser.next();
      if (paramInt != 1) {
        if (paramInt == 3)
          if (paramXmlResourceParser.getDepth() <= i) {
            bool2 = true;
            break;
          }  
        if (paramInt == 3 || paramInt == 4)
          continue; 
        if (paramXmlResourceParser.getName().equals("intent-filter")) {
          ServiceIntentInfo serviceIntentInfo = new ServiceIntentInfo(service);
          if (!parseIntent(paramResources, paramXmlResourceParser, true, false, serviceIntentInfo, paramArrayOfString))
            return null; 
          if (bool2) {
            serviceIntentInfo.setVisibilityToInstantApp(1);
            ServiceInfo serviceInfo1 = service.info;
            serviceInfo1.flags |= 0x100000;
          } 
          service.order = Math.max(serviceIntentInfo.getOrder(), service.order);
          service.intents.add(serviceIntentInfo);
          continue;
        } 
        if (paramXmlResourceParser.getName().equals("meta-data")) {
          Bundle bundle = parseMetaData(paramResources, xmlResourceParser1, service.metaData, arrayOfString);
          if (bundle == null)
            return null; 
          continue;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown element under <service>: ");
        stringBuilder.append(paramXmlResourceParser.getName());
        stringBuilder.append(" at ");
        stringBuilder.append(this.mArchiveSourcePath);
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str1 = stringBuilder.toString();
        Slog.w("PackageParser", str1);
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        continue;
      } 
      bool2 = true;
      break;
    } 
    if (!bool1) {
      ServiceInfo serviceInfo1 = service.info;
      if (service.intents.size() <= 0)
        bool2 = false; 
      serviceInfo1.exported = bool2;
    } 
    return service;
  }
  
  private boolean isImplicitlyExposedIntent(IntentInfo paramIntentInfo) {
    return (paramIntentInfo.hasCategory("android.intent.category.BROWSABLE") || 
      paramIntentInfo.hasAction("android.intent.action.SEND") || 
      paramIntentInfo.hasAction("android.intent.action.SENDTO") || 
      paramIntentInfo.hasAction("android.intent.action.SEND_MULTIPLE"));
  }
  
  private boolean parseAllMetaData(Resources paramResources, XmlResourceParser paramXmlResourceParser, String paramString, Component<?> paramComponent, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_2
    //   1: invokeinterface getDepth : ()I
    //   6: istore #6
    //   8: aload_2
    //   9: invokeinterface next : ()I
    //   14: istore #7
    //   16: iload #7
    //   18: iconst_1
    //   19: if_icmpeq -> 206
    //   22: iload #7
    //   24: iconst_3
    //   25: if_icmpne -> 39
    //   28: aload_2
    //   29: invokeinterface getDepth : ()I
    //   34: iload #6
    //   36: if_icmple -> 206
    //   39: iload #7
    //   41: iconst_3
    //   42: if_icmpeq -> 8
    //   45: iload #7
    //   47: iconst_4
    //   48: if_icmpne -> 54
    //   51: goto -> 8
    //   54: aload_2
    //   55: invokeinterface getName : ()Ljava/lang/String;
    //   60: ldc_w 'meta-data'
    //   63: invokevirtual equals : (Ljava/lang/Object;)Z
    //   66: ifeq -> 98
    //   69: aload_0
    //   70: aload_1
    //   71: aload_2
    //   72: aload #4
    //   74: getfield metaData : Landroid/os/Bundle;
    //   77: aload #5
    //   79: invokespecial parseMetaData : (Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/os/Bundle;[Ljava/lang/String;)Landroid/os/Bundle;
    //   82: astore #8
    //   84: aload #4
    //   86: aload #8
    //   88: putfield metaData : Landroid/os/Bundle;
    //   91: aload #8
    //   93: ifnonnull -> 8
    //   96: iconst_0
    //   97: ireturn
    //   98: new java/lang/StringBuilder
    //   101: dup
    //   102: invokespecial <init> : ()V
    //   105: astore #8
    //   107: aload #8
    //   109: ldc_w 'Unknown element under '
    //   112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: aload #8
    //   118: aload_3
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload #8
    //   125: ldc_w ': '
    //   128: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: aload #8
    //   134: aload_2
    //   135: invokeinterface getName : ()Ljava/lang/String;
    //   140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: aload #8
    //   146: ldc_w ' at '
    //   149: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload #8
    //   155: aload_0
    //   156: getfield mArchiveSourcePath : Ljava/lang/String;
    //   159: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload #8
    //   165: ldc_w ' '
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload #8
    //   174: aload_2
    //   175: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   180: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload #8
    //   186: invokevirtual toString : ()Ljava/lang/String;
    //   189: astore #8
    //   191: ldc 'PackageParser'
    //   193: aload #8
    //   195: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   198: pop
    //   199: aload_2
    //   200: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   203: goto -> 8
    //   206: iconst_1
    //   207: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #5562	-> 0
    //   #5564	-> 8
    //   #5566	-> 28
    //   #5567	-> 39
    //   #5568	-> 51
    //   #5571	-> 54
    //   #5572	-> 69
    //   #5574	-> 96
    //   #5578	-> 98
    //   #5579	-> 132
    //   #5580	-> 172
    //   #5578	-> 191
    //   #5581	-> 199
    //   #5582	-> 203
    //   #5589	-> 206
  }
  
  private Bundle parseMetaData(Resources paramResources, XmlResourceParser paramXmlResourceParser, Bundle paramBundle, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestMetaData);
    Bundle bundle = paramBundle;
    if (paramBundle == null)
      bundle = new Bundle(); 
    boolean bool = false;
    String str = typedArray.getNonConfigurationString(0, 0);
    paramBundle = null;
    if (str == null) {
      paramArrayOfString[0] = "<meta-data> requires an android:name attribute";
      typedArray.recycle();
      return null;
    } 
    str = str.intern();
    TypedValue typedValue = typedArray.peekValue(2);
    if (typedValue != null && typedValue.resourceId != 0) {
      bundle.putInt(str, typedValue.resourceId);
    } else {
      CharSequence charSequence;
      typedValue = typedArray.peekValue(1);
      if (typedValue != null) {
        if (typedValue.type == 3) {
          String str1;
          charSequence = typedValue.coerceToString();
          if (charSequence != null)
            str1 = charSequence.toString(); 
          bundle.putString(str, str1);
        } else if (typedValue.type == 18) {
          if (typedValue.data != 0)
            bool = true; 
          bundle.putBoolean(str, bool);
        } else if (typedValue.type >= 16 && typedValue.type <= 31) {
          bundle.putInt(str, typedValue.data);
        } else if (typedValue.type == 4) {
          bundle.putFloat(str, typedValue.getFloat());
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("<meta-data> only supports string, integer, float, color, boolean, and resource reference types: ");
          stringBuilder.append(paramXmlResourceParser.getName());
          stringBuilder.append(" at ");
          stringBuilder.append(this.mArchiveSourcePath);
          stringBuilder.append(" ");
          stringBuilder.append(paramXmlResourceParser.getPositionDescription());
          String str1 = stringBuilder.toString();
          Slog.w("PackageParser", str1);
        } 
      } else {
        charSequence[0] = "<meta-data> requires an android:value or android:resource attribute";
        bundle = null;
      } 
    } 
    typedArray.recycle();
    XmlUtils.skipCurrentTag(paramXmlResourceParser);
    return bundle;
  }
  
  private static VerifierInfo parseVerifier(AttributeSet paramAttributeSet) {
    StringBuilder stringBuilder;
    String str1 = null;
    String str2 = null;
    int i = paramAttributeSet.getAttributeCount();
    for (byte b = 0; b < i; b++) {
      int j = paramAttributeSet.getAttributeNameResource(b);
      if (j != 16842755) {
        if (j == 16843686)
          str2 = paramAttributeSet.getAttributeValue(b); 
      } else {
        str1 = paramAttributeSet.getAttributeValue(b);
      } 
    } 
    if (str1 == null || str1.length() == 0) {
      Slog.i("PackageParser", "verifier package name was null; skipping");
      return null;
    } 
    PublicKey publicKey = parsePublicKey(str2);
    if (publicKey == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to parse verifier public key for ");
      stringBuilder.append(str1);
      Slog.i("PackageParser", stringBuilder.toString());
      return null;
    } 
    return new VerifierInfo(str1, (PublicKey)stringBuilder);
  }
  
  public static final PublicKey parsePublicKey(String paramString) {
    if (paramString == null) {
      Slog.w("PackageParser", "Could not parse null public key");
      return null;
    } 
    try {
      byte[] arrayOfByte = Base64.decode(paramString, 0);
      X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(arrayOfByte);
      try {
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        return keyFactory.generatePublic(x509EncodedKeySpec);
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        Slog.wtf("PackageParser", "Could not parse public key: RSA KeyFactory not included in build");
      } catch (InvalidKeySpecException invalidKeySpecException) {}
      try {
        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        return keyFactory.generatePublic(x509EncodedKeySpec);
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        Slog.wtf("PackageParser", "Could not parse public key: EC KeyFactory not included in build");
      } catch (InvalidKeySpecException invalidKeySpecException) {}
      try {
        KeyFactory keyFactory = KeyFactory.getInstance("DSA");
        return keyFactory.generatePublic(x509EncodedKeySpec);
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        Slog.wtf("PackageParser", "Could not parse public key: DSA KeyFactory not included in build");
      } catch (InvalidKeySpecException invalidKeySpecException) {}
      return null;
    } catch (IllegalArgumentException illegalArgumentException) {
      Slog.w("PackageParser", "Could not parse verifier public key; invalid Base64");
      return null;
    } 
  }
  
  private boolean parseIntent(Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean1, boolean paramBoolean2, IntentInfo paramIntentInfo, String[] paramArrayOfString) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestIntentFilter);
    int i = typedArray.getInt(2, 0);
    paramIntentInfo.setPriority(i);
    int j = 3;
    i = typedArray.getInt(3, 0);
    paramIntentInfo.setOrder(i);
    TypedValue typedValue = typedArray.peekValue(0);
    if (typedValue != null) {
      paramIntentInfo.labelRes = i = typedValue.resourceId;
      if (i == 0)
        paramIntentInfo.nonLocalizedLabel = typedValue.coerceToString(); 
    } 
    if (sUseRoundIcon) {
      i = typedArray.getResourceId(7, 0);
    } else {
      i = 0;
    } 
    if (i != 0) {
      paramIntentInfo.icon = i;
    } else {
      paramIntentInfo.icon = typedArray.getResourceId(1, 0);
    } 
    paramIntentInfo.logo = typedArray.getResourceId(4, 0);
    paramIntentInfo.banner = typedArray.getResourceId(5, 0);
    if (paramBoolean2)
      paramIntentInfo.setAutoVerify(typedArray.getBoolean(6, false)); 
    typedArray.recycle();
    int k = paramXmlResourceParser.getDepth();
    i = j;
    while (true) {
      j = paramXmlResourceParser.next();
      if (j != 1 && (j != i || 
        paramXmlResourceParser.getDepth() > k)) {
        if (j == i || j == 4)
          continue; 
        String str = paramXmlResourceParser.getName();
        if (str.equals("action")) {
          str = paramXmlResourceParser.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
          if (str == null || str == "") {
            paramArrayOfString[0] = "No value supplied for <android:name>";
            return false;
          } 
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          paramIntentInfo.addAction(str);
        } else if (str.equals("category")) {
          str = paramXmlResourceParser.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
          if (str == null || str == "") {
            paramArrayOfString[0] = "No value supplied for <android:name>";
            return false;
          } 
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          paramIntentInfo.addCategory(str);
        } else if (str.equals("data")) {
          TypedArray typedArray1 = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestData);
          String str1 = typedArray1.getNonConfigurationString(0, 0);
          if (str1 != null)
            try {
              paramIntentInfo.addDataType(str1);
            } catch (android.content.IntentFilter.MalformedMimeTypeException malformedMimeTypeException) {
              paramArrayOfString[0] = malformedMimeTypeException.toString();
              typedArray1.recycle();
              return false;
            }  
          str1 = typedArray1.getNonConfigurationString(1, 0);
          if (str1 != null)
            paramIntentInfo.addDataScheme(str1); 
          str1 = typedArray1.getNonConfigurationString(7, 0);
          if (str1 != null)
            paramIntentInfo.addDataSchemeSpecificPart(str1, 0); 
          str1 = typedArray1.getNonConfigurationString(8, 0);
          if (str1 != null)
            paramIntentInfo.addDataSchemeSpecificPart(str1, 1); 
          str1 = typedArray1.getNonConfigurationString(9, 0);
          if (str1 != null) {
            if (!paramBoolean1) {
              paramArrayOfString[0] = "sspPattern not allowed here; ssp must be literal";
              return false;
            } 
            paramIntentInfo.addDataSchemeSpecificPart(str1, 2);
          } 
          String str2 = typedArray1.getNonConfigurationString(2, 0);
          str1 = typedArray1.getNonConfigurationString(3, 0);
          if (str2 != null)
            paramIntentInfo.addDataAuthority(str2, str1); 
          str1 = typedArray1.getNonConfigurationString(4, 0);
          if (str1 != null)
            paramIntentInfo.addDataPath(str1, 0); 
          str1 = typedArray1.getNonConfigurationString(5, 0);
          if (str1 != null)
            paramIntentInfo.addDataPath(str1, 1); 
          str1 = typedArray1.getNonConfigurationString(6, 0);
          if (str1 != null) {
            if (!paramBoolean1) {
              paramArrayOfString[0] = "pathPattern not allowed here; path must be literal";
              return false;
            } 
            paramIntentInfo.addDataPath(str1, 2);
          } 
          str1 = typedArray1.getNonConfigurationString(11, 0);
          if (str1 != null) {
            if (!paramBoolean1) {
              paramArrayOfString[0] = "pathAdvancedPattern not allowed here; path must be literal";
              return false;
            } 
            paramIntentInfo.addDataPath(str1, 3);
          } 
          typedArray1.recycle();
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown element under <intent-filter>: ");
          stringBuilder.append(paramXmlResourceParser.getName());
          stringBuilder.append(" at ");
          stringBuilder.append(this.mArchiveSourcePath);
          stringBuilder.append(" ");
          stringBuilder.append(paramXmlResourceParser.getPositionDescription());
          String str1 = stringBuilder.toString();
          Slog.w("PackageParser", str1);
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
        } 
        i = 3;
        continue;
      } 
      break;
    } 
    paramIntentInfo.hasDefault = paramIntentInfo.hasCategory("android.intent.category.DEFAULT");
    return true;
  }
  
  class SigningDetails implements Parcelable {
    public static final Parcelable.Creator<SigningDetails> CREATOR = new Parcelable.Creator<SigningDetails>() {
        public PackageParser.SigningDetails createFromParcel(Parcel param2Parcel) {
          if (param2Parcel.readBoolean())
            return PackageParser.SigningDetails.UNKNOWN; 
          return new PackageParser.SigningDetails();
        }
        
        public PackageParser.SigningDetails[] newArray(int param2Int) {
          return new PackageParser.SigningDetails[param2Int];
        }
      };
    
    private static final int PAST_CERT_EXISTS = 0;
    
    public static final SigningDetails UNKNOWN = new SigningDetails(0, null, null);
    
    public final Signature[] pastSigningCertificates;
    
    public final ArraySet<PublicKey> publicKeys;
    
    public final int signatureSchemeVersion;
    
    public final Signature[] signatures;
    
    public SigningDetails(int param1Int, ArraySet<PublicKey> param1ArraySet, Signature[] param1ArrayOfSignature1) {
      this.signatures = (Signature[])this$0;
      this.signatureSchemeVersion = param1Int;
      this.publicKeys = param1ArraySet;
      this.pastSigningCertificates = param1ArrayOfSignature1;
    }
    
    public SigningDetails(int param1Int, Signature[] param1ArrayOfSignature1) throws CertificateException {
      this((Signature[])this$0, param1Int, PackageParser.toSigningKeys((Signature[])this$0), param1ArrayOfSignature1);
    }
    
    public SigningDetails(int param1Int) throws CertificateException {
      this((Signature[])this$0, param1Int, null);
    }
    
    public SigningDetails(PackageParser this$0) {
      if (this$0 != null) {
        Signature[] arrayOfSignature2 = ((SigningDetails)this$0).signatures;
        if (arrayOfSignature2 != null) {
          this.signatures = (Signature[])arrayOfSignature2.clone();
        } else {
          this.signatures = null;
        } 
        this.signatureSchemeVersion = ((SigningDetails)this$0).signatureSchemeVersion;
        this.publicKeys = new ArraySet(((SigningDetails)this$0).publicKeys);
        Signature[] arrayOfSignature1 = ((SigningDetails)this$0).pastSigningCertificates;
        if (arrayOfSignature1 != null) {
          this.pastSigningCertificates = (Signature[])arrayOfSignature1.clone();
        } else {
          this.pastSigningCertificates = null;
        } 
      } else {
        this.signatures = null;
        this.signatureSchemeVersion = 0;
        this.publicKeys = null;
        this.pastSigningCertificates = null;
      } 
    }
    
    public SigningDetails mergeLineageWith(SigningDetails param1SigningDetails) {
      if (!hasPastSigningCertificates()) {
        if (!param1SigningDetails.hasPastSigningCertificates() || !param1SigningDetails.hasAncestorOrSelf(this))
          param1SigningDetails = this; 
        return param1SigningDetails;
      } 
      if (!param1SigningDetails.hasPastSigningCertificates())
        return this; 
      SigningDetails signingDetails = getDescendantOrSelf(param1SigningDetails);
      if (signingDetails == null)
        return this; 
      if (signingDetails == this) {
        param1SigningDetails = mergeLineageWithAncestorOrSelf(param1SigningDetails);
      } else {
        param1SigningDetails = param1SigningDetails.mergeLineageWithAncestorOrSelf(this);
      } 
      return param1SigningDetails;
    }
    
    private SigningDetails mergeLineageWithAncestorOrSelf(SigningDetails param1SigningDetails) {
      int n, i = this.pastSigningCertificates.length - 1;
      int j = param1SigningDetails.pastSigningCertificates.length - 1;
      if (i < 0 || j < 0)
        return this; 
      ArrayList<Signature> arrayList = new ArrayList();
      int k = 0;
      while (i >= 0 && !this.pastSigningCertificates[i].equals(param1SigningDetails.pastSigningCertificates[j])) {
        arrayList.add(new Signature(this.pastSigningCertificates[i]));
        i--;
      } 
      int m = i;
      if (i < 0)
        return this; 
      while (true) {
        Signature[] arrayOfSignature1 = this.pastSigningCertificates;
        n = m - 1;
        Signature signature1 = arrayOfSignature1[m];
        Signature[] arrayOfSignature2 = param1SigningDetails.pastSigningCertificates;
        m = j - 1;
        Signature signature3 = arrayOfSignature2[j];
        Signature signature2 = new Signature(signature1);
        j = signature1.getFlags() & signature3.getFlags();
        i = k;
        if (signature1.getFlags() != j) {
          i = 1;
          signature2.setFlags(j);
        } 
        arrayList.add(signature2);
        if (n < 0 || m < 0 || !this.pastSigningCertificates[n].equals(param1SigningDetails.pastSigningCertificates[m]))
          break; 
        j = m;
        m = n;
        k = i;
      } 
      j = m;
      if (n >= 0) {
        j = m;
        if (m >= 0)
          return this; 
      } 
      while (true) {
        m = n;
        if (j >= 0) {
          arrayList.add(new Signature(param1SigningDetails.pastSigningCertificates[j]));
          j--;
          continue;
        } 
        break;
      } 
      while (m >= 0) {
        arrayList.add(new Signature(this.pastSigningCertificates[m]));
        m--;
      } 
      if (arrayList.size() == this.pastSigningCertificates.length && i == 0)
        return this; 
      Collections.reverse(arrayList);
      try {
        Signature signature = new Signature();
        this(this.signatures[0]);
        i = this.signatureSchemeVersion;
        Signature[] arrayOfSignature = arrayList.<Signature>toArray(new Signature[0]);
        return new SigningDetails(i, arrayOfSignature);
      } catch (CertificateException certificateException) {
        Slog.e("PackageParser", "Caught an exception creating the merged lineage: ", certificateException);
        return this;
      } 
    }
    
    public boolean hasCommonAncestor(SigningDetails param1SigningDetails) {
      boolean bool;
      if (!hasPastSigningCertificates())
        return param1SigningDetails.hasAncestorOrSelf(this); 
      if (!param1SigningDetails.hasPastSigningCertificates())
        return hasAncestorOrSelf(param1SigningDetails); 
      if (getDescendantOrSelf(param1SigningDetails) != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private SigningDetails getDescendantOrSelf(SigningDetails param1SigningDetails) {
      SigningDetails signingDetails;
      int m;
      if (hasAncestorOrSelf(param1SigningDetails)) {
        signingDetails = this;
      } else if (param1SigningDetails.hasAncestor(this)) {
        signingDetails = param1SigningDetails;
        param1SigningDetails = this;
      } else {
        return null;
      } 
      int i = signingDetails.pastSigningCertificates.length - 1;
      int j = param1SigningDetails.pastSigningCertificates.length - 1;
      while (i >= 0) {
        Signature signature1 = signingDetails.pastSigningCertificates[i], signature2 = param1SigningDetails.pastSigningCertificates[j];
        if (!signature1.equals(signature2))
          i--; 
      } 
      int k = i;
      if (i < 0)
        return null; 
      while (true) {
        m = k - 1;
        i = j - 1;
        if (m >= 0 && i >= 0) {
          Signature signature1 = signingDetails.pastSigningCertificates[m], signature2 = param1SigningDetails.pastSigningCertificates[i];
          k = m;
          j = i;
          if (!signature1.equals(signature2))
            break; 
          continue;
        } 
        break;
      } 
      if (m >= 0 && i >= 0)
        return null; 
      return signingDetails;
    }
    
    public boolean hasSignatures() {
      boolean bool;
      Signature[] arrayOfSignature = this.signatures;
      if (arrayOfSignature != null && arrayOfSignature.length > 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean hasPastSigningCertificates() {
      boolean bool;
      Signature[] arrayOfSignature = this.pastSigningCertificates;
      if (arrayOfSignature != null && arrayOfSignature.length > 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean hasAncestorOrSelf(SigningDetails param1SigningDetails) {
      SigningDetails signingDetails = UNKNOWN;
      if (this == signingDetails || param1SigningDetails == signingDetails)
        return false; 
      Signature[] arrayOfSignature = param1SigningDetails.signatures;
      if (arrayOfSignature.length > 1)
        return signaturesMatchExactly(param1SigningDetails); 
      return hasCertificate(arrayOfSignature[0]);
    }
    
    public boolean hasAncestor(SigningDetails param1SigningDetails) {
      SigningDetails signingDetails = UNKNOWN;
      if (this == signingDetails || param1SigningDetails == signingDetails)
        return false; 
      if (hasPastSigningCertificates() && param1SigningDetails.signatures.length == 1) {
        byte b = 0;
        while (true) {
          Signature[] arrayOfSignature = this.pastSigningCertificates;
          if (b < arrayOfSignature.length - 1) {
            if (arrayOfSignature[b].equals(param1SigningDetails.signatures[0]))
              return true; 
            b++;
            continue;
          } 
          break;
        } 
      } 
      return false;
    }
    
    public boolean checkCapability(SigningDetails param1SigningDetails, int param1Int) {
      SigningDetails signingDetails = UNKNOWN;
      if (this == signingDetails || param1SigningDetails == signingDetails)
        return false; 
      Signature[] arrayOfSignature = param1SigningDetails.signatures;
      if (arrayOfSignature.length > 1)
        return signaturesMatchExactly(param1SigningDetails); 
      return hasCertificate(arrayOfSignature[0], param1Int);
    }
    
    public boolean checkCapabilityRecover(SigningDetails param1SigningDetails, int param1Int) throws CertificateException {
      SigningDetails signingDetails = UNKNOWN;
      if (param1SigningDetails == signingDetails || this == signingDetails)
        return false; 
      if (hasPastSigningCertificates() && param1SigningDetails.signatures.length == 1) {
        byte b = 0;
        while (true) {
          Signature[] arrayOfSignature = this.pastSigningCertificates;
          if (b < arrayOfSignature.length) {
            if (Signature.areEffectiveMatch(param1SigningDetails.signatures[0], arrayOfSignature[b])) {
              Signature signature = this.pastSigningCertificates[b];
              if (signature.getFlags() == param1Int)
                return true; 
            } 
            b++;
            continue;
          } 
          break;
        } 
        return false;
      } 
      return Signature.areEffectiveMatch(param1SigningDetails.signatures, this.signatures);
    }
    
    public boolean hasCertificate(Signature param1Signature) {
      return hasCertificateInternal(param1Signature, 0);
    }
    
    public boolean hasCertificate(Signature param1Signature, int param1Int) {
      return hasCertificateInternal(param1Signature, param1Int);
    }
    
    public boolean hasCertificate(byte[] param1ArrayOfbyte) {
      Signature signature = new Signature(param1ArrayOfbyte);
      return hasCertificate(signature);
    }
    
    private boolean hasCertificateInternal(Signature param1Signature, int param1Int) {
      SigningDetails signingDetails = UNKNOWN;
      boolean bool1 = false;
      if (this == signingDetails)
        return false; 
      if (hasPastSigningCertificates()) {
        byte b = 0;
        while (true) {
          Signature[] arrayOfSignature1 = this.pastSigningCertificates;
          if (b < arrayOfSignature1.length - 1) {
            if (arrayOfSignature1[b].equals(param1Signature))
              if (param1Int != 0) {
                Signature signature = this.pastSigningCertificates[b];
                if ((signature.getFlags() & param1Int) == param1Int)
                  return true; 
              } else {
                return true;
              }  
            b++;
            continue;
          } 
          break;
        } 
      } 
      Signature[] arrayOfSignature = this.signatures;
      boolean bool2 = bool1;
      if (arrayOfSignature.length == 1) {
        bool2 = bool1;
        if (arrayOfSignature[0].equals(param1Signature))
          bool2 = true; 
      } 
      return bool2;
    }
    
    public boolean checkCapability(String param1String, int param1Int) {
      byte[] arrayOfByte;
      if (this == UNKNOWN)
        return false; 
      if (param1String == null) {
        arrayOfByte = null;
      } else {
        arrayOfByte = HexEncoding.decode(param1String, false);
      } 
      if (hasSha256Certificate(arrayOfByte, param1Int))
        return true; 
      Signature[] arrayOfSignature = this.signatures;
      String[] arrayOfString = PackageUtils.computeSignaturesSha256Digests(arrayOfSignature);
      String str = PackageUtils.computeSignaturesSha256Digest(arrayOfString);
      return str.equals(param1String);
    }
    
    public boolean hasSha256Certificate(byte[] param1ArrayOfbyte) {
      return hasSha256CertificateInternal(param1ArrayOfbyte, 0);
    }
    
    public boolean hasSha256Certificate(byte[] param1ArrayOfbyte, int param1Int) {
      return hasSha256CertificateInternal(param1ArrayOfbyte, param1Int);
    }
    
    private boolean hasSha256CertificateInternal(byte[] param1ArrayOfbyte, int param1Int) {
      if (this == UNKNOWN)
        return false; 
      if (hasPastSigningCertificates()) {
        byte b = 0;
        while (true) {
          Signature[] arrayOfSignature1 = this.pastSigningCertificates;
          if (b < arrayOfSignature1.length - 1) {
            Signature signature = arrayOfSignature1[b];
            byte[] arrayOfByte = signature.toByteArray();
            arrayOfByte = PackageUtils.computeSha256DigestBytes(arrayOfByte);
            if (Arrays.equals(param1ArrayOfbyte, arrayOfByte))
              if (param1Int != 0) {
                Signature signature1 = this.pastSigningCertificates[b];
                if ((signature1.getFlags() & param1Int) == param1Int)
                  return true; 
              } else {
                return true;
              }  
            b++;
            continue;
          } 
          break;
        } 
      } 
      Signature[] arrayOfSignature = this.signatures;
      if (arrayOfSignature.length == 1) {
        Signature signature = arrayOfSignature[0];
        byte[] arrayOfByte = PackageUtils.computeSha256DigestBytes(signature.toByteArray());
        return Arrays.equals(param1ArrayOfbyte, arrayOfByte);
      } 
      return false;
    }
    
    public boolean signaturesMatchExactly(SigningDetails param1SigningDetails) {
      return Signature.areExactMatch(this.signatures, param1SigningDetails.signatures);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      boolean bool;
      if (UNKNOWN == this) {
        bool = true;
      } else {
        bool = false;
      } 
      param1Parcel.writeBoolean(bool);
      if (bool)
        return; 
      param1Parcel.writeTypedArray((Parcelable[])this.signatures, param1Int);
      param1Parcel.writeInt(this.signatureSchemeVersion);
      param1Parcel.writeArraySet(this.publicKeys);
      param1Parcel.writeTypedArray((Parcelable[])this.pastSigningCertificates, param1Int);
    }
    
    protected SigningDetails(PackageParser this$0) {
      ClassLoader classLoader = Object.class.getClassLoader();
      this.signatures = (Signature[])this$0.createTypedArray(Signature.CREATOR);
      this.signatureSchemeVersion = this$0.readInt();
      this.publicKeys = this$0.readArraySet(classLoader);
      this.pastSigningCertificates = (Signature[])this$0.createTypedArray(Signature.CREATOR);
    }
    
    static {
    
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof SigningDetails))
        return false; 
      param1Object = param1Object;
      if (this.signatureSchemeVersion != ((SigningDetails)param1Object).signatureSchemeVersion)
        return false; 
      if (!Signature.areExactMatch(this.signatures, ((SigningDetails)param1Object).signatures))
        return false; 
      ArraySet<PublicKey> arraySet = this.publicKeys;
      if (arraySet != null) {
        if (!arraySet.equals(((SigningDetails)param1Object).publicKeys))
          return false; 
      } else if (((SigningDetails)param1Object).publicKeys != null) {
        return false;
      } 
      if (!Arrays.equals((Object[])this.pastSigningCertificates, (Object[])((SigningDetails)param1Object).pastSigningCertificates))
        return false; 
      byte b = 0;
      while (true) {
        Signature[] arrayOfSignature = this.pastSigningCertificates;
        if (b < arrayOfSignature.length) {
          int i = arrayOfSignature[b].getFlags();
          Signature signature = ((SigningDetails)param1Object).pastSigningCertificates[b];
          if (i != signature.getFlags())
            return false; 
          b++;
          continue;
        } 
        break;
      } 
      return true;
    }
    
    public int hashCode() {
      byte b;
      int i = Arrays.hashCode((Object[])this.signatures);
      int j = this.signatureSchemeVersion;
      ArraySet<PublicKey> arraySet = this.publicKeys;
      if (arraySet != null) {
        b = arraySet.hashCode();
      } else {
        b = 0;
      } 
      int k = Arrays.hashCode((Object[])this.pastSigningCertificates);
      return ((i * 31 + j) * 31 + b) * 31 + k;
    }
    
    class Builder {
      private Signature[] mPastSigningCertificates;
      
      private int mSignatureSchemeVersion = 0;
      
      private Signature[] mSignatures;
      
      public Builder setSignatures(Signature[] param2ArrayOfSignature) {
        this.mSignatures = param2ArrayOfSignature;
        return this;
      }
      
      public Builder setSignatureSchemeVersion(int param2Int) {
        this.mSignatureSchemeVersion = param2Int;
        return this;
      }
      
      public Builder setPastSigningCertificates(Signature[] param2ArrayOfSignature) {
        this.mPastSigningCertificates = param2ArrayOfSignature;
        return this;
      }
      
      private void checkInvariants() {
        if (this.mSignatures != null)
          return; 
        throw new IllegalStateException("SigningDetails requires the current signing certificates.");
      }
      
      public PackageParser.SigningDetails build() throws CertificateException {
        checkInvariants();
        return new PackageParser.SigningDetails(this.mSignatureSchemeVersion, this.mPastSigningCertificates);
      }
    }
    
    class CertCapabilities implements Annotation {
      public static final int AUTH = 16;
      
      public static final int INSTALLED_DATA = 1;
      
      public static final int PERMISSION = 4;
      
      public static final int ROLLBACK = 8;
      
      public static final int SHARED_USER_ID = 2;
    }
    
    class SignatureSchemeVersion implements Annotation {
      public static final int JAR = 1;
      
      public static final int SIGNING_BLOCK_V2 = 2;
      
      public static final int SIGNING_BLOCK_V3 = 3;
      
      public static final int SIGNING_BLOCK_V4 = 4;
      
      public static final int UNKNOWN = 0;
    }
  }
  
  class Package implements Parcelable {
    public String volumeUuid;
    
    public boolean visibleToInstantApps;
    
    public long[] usesStaticLibrariesVersions;
    
    public String[][] usesStaticLibrariesCertDigests;
    
    public ArrayList<String> usesStaticLibraries;
    
    public ArrayList<String> usesOptionalLibraries;
    
    public ArrayList<SharedLibraryInfo> usesLibraryInfos;
    
    public String[] usesLibraryFiles;
    
    public ArrayList<String> usesLibraries;
    
    public boolean use32bitAbi;
    
    public long staticSharedLibVersion;
    
    public String staticSharedLibName;
    
    public int[] splitRevisionCodes;
    
    public int[] splitPrivateFlags;
    
    public String[] splitNames;
    
    public int[] splitFlags;
    
    public String[] splitCodePaths;
    
    public final ArrayList<PackageParser.Service> services;
    
    public byte[] restrictUpdateHash;
    
    public final ArrayList<String> requestedPermissions;
    
    public ArrayList<FeatureInfo> reqFeatures;
    
    public final ArrayList<PackageParser.Activity> receivers;
    
    public final ArrayList<PackageParser.Provider> providers;
    
    public ArrayList<String> protectedBroadcasts;
    
    public ArrayList<PackageParser.ActivityIntentInfo> preferredActivityFilters;
    
    public final ArrayList<PackageParser.Permission> permissions;
    
    public final ArrayList<PackageParser.PermissionGroup> permissionGroups;
    
    public Package parentPackage;
    
    public String packageName;
    
    public String manifestPackageName;
    
    public String mVersionName;
    
    public int mVersionCodeMajor;
    
    public int mVersionCode;
    
    public ArraySet<String> mUpgradeKeySets;
    
    public PackageParser.SigningDetails mSigningDetails;
    
    public int mSharedUserLabel;
    
    public String mSharedUserId;
    
    public String mRestrictedAccountType;
    
    public boolean mRequiredForAllUsers;
    
    public String mRequiredAccountType;
    
    public String mRealPackage;
    
    public int mPreferredOrder;
    
    public String mOverlayTargetName;
    
    public String mOverlayTarget;
    
    public int mOverlayPriority;
    
    public boolean mOverlayIsStatic;
    
    public String mOverlayCategory;
    
    public ArrayList<String> mOriginalPackages;
    
    public long[] mLastPackageUsageTimeInMills;
    
    public ArrayMap<String, ArraySet<PublicKey>> mKeySetMapping;
    
    public Object mExtras;
    
    public String mCompileSdkVersionCodename;
    
    public int mCompileSdkVersion;
    
    public Bundle mAppMetaData;
    
    public ArrayList<String> mAdoptPermissions;
    
    public ArrayList<String> libraryNames;
    
    public boolean isStub;
    
    public final ArrayList<PackageParser.Instrumentation> instrumentation;
    
    public int installLocation;
    
    public final ArrayList<String> implicitPermissions;
    
    public ArrayList<FeatureGroupInfo> featureGroups;
    
    public String cpuAbiOverride;
    
    public boolean coreApp;
    
    public ArrayList<ConfigurationInfo> configPreferences;
    
    public String codePath;
    
    public ArrayList<Package> childPackages;
    
    public int baseRevisionCode;
    
    public boolean baseHardwareAccelerated;
    
    public String baseCodePath;
    
    public ApplicationInfo applicationInfo = new ApplicationInfo();
    
    public final ArrayList<PackageParser.Activity> activities;
    
    public Package(PackageParser this$0) {
      boolean bool1 = false;
      this.permissions = new ArrayList<>(0);
      this.permissionGroups = new ArrayList<>(0);
      this.activities = new ArrayList<>(0);
      this.receivers = new ArrayList<>(0);
      this.providers = new ArrayList<>(0);
      this.services = new ArrayList<>(0);
      this.instrumentation = new ArrayList<>(0);
      this.requestedPermissions = new ArrayList<>();
      this.implicitPermissions = new ArrayList<>();
      this.staticSharedLibName = null;
      this.staticSharedLibVersion = 0L;
      this.libraryNames = null;
      this.usesLibraries = null;
      this.usesStaticLibraries = null;
      this.usesStaticLibrariesVersions = null;
      this.usesStaticLibrariesCertDigests = null;
      this.usesOptionalLibraries = null;
      this.usesLibraryFiles = null;
      this.usesLibraryInfos = null;
      this.preferredActivityFilters = null;
      this.mOriginalPackages = null;
      this.mRealPackage = null;
      this.mAdoptPermissions = null;
      this.mAppMetaData = null;
      this.mSigningDetails = PackageParser.SigningDetails.UNKNOWN;
      this.mPreferredOrder = 0;
      this.mLastPackageUsageTimeInMills = new long[8];
      this.configPreferences = null;
      this.reqFeatures = null;
      this.featureGroups = null;
      ClassLoader classLoader = Object.class.getClassLoader();
      this.packageName = this$0.readString().intern();
      this.manifestPackageName = this$0.readString();
      this.splitNames = this$0.readStringArray();
      this.volumeUuid = this$0.readString();
      this.codePath = this$0.readString();
      this.baseCodePath = this$0.readString();
      this.splitCodePaths = this$0.readStringArray();
      this.baseRevisionCode = this$0.readInt();
      this.splitRevisionCodes = this$0.createIntArray();
      this.splitFlags = this$0.createIntArray();
      this.splitPrivateFlags = this$0.createIntArray();
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.baseHardwareAccelerated = bool2;
      ApplicationInfo applicationInfo = (ApplicationInfo)this$0.readParcelable(classLoader);
      if (applicationInfo.permission != null) {
        applicationInfo = this.applicationInfo;
        applicationInfo.permission = applicationInfo.permission.intern();
      } 
      this$0.readParcelableList(this.permissions, classLoader);
      fixupOwner((List)this.permissions);
      this$0.readParcelableList(this.permissionGroups, classLoader);
      fixupOwner((List)this.permissionGroups);
      this$0.readParcelableList(this.activities, classLoader);
      fixupOwner((List)this.activities);
      this$0.readParcelableList(this.receivers, classLoader);
      fixupOwner((List)this.receivers);
      this$0.readParcelableList(this.providers, classLoader);
      fixupOwner((List)this.providers);
      this$0.readParcelableList(this.services, classLoader);
      fixupOwner((List)this.services);
      this$0.readParcelableList(this.instrumentation, classLoader);
      fixupOwner((List)this.instrumentation);
      this$0.readStringList(this.requestedPermissions);
      internStringArrayList(this.requestedPermissions);
      this$0.readStringList(this.implicitPermissions);
      internStringArrayList(this.implicitPermissions);
      ArrayList<String> arrayList2 = this$0.createStringArrayList();
      internStringArrayList(arrayList2);
      this.parentPackage = (Package)this$0.readParcelable(classLoader);
      this.childPackages = (ArrayList)(arrayList2 = new ArrayList<>());
      this$0.readParcelableList(arrayList2, classLoader);
      if (this.childPackages.size() == 0)
        this.childPackages = null; 
      String str2 = this$0.readString();
      if (str2 != null)
        this.staticSharedLibName = str2.intern(); 
      this.staticSharedLibVersion = this$0.readLong();
      ArrayList<String> arrayList1 = this$0.createStringArrayList();
      internStringArrayList(arrayList1);
      this.usesLibraries = arrayList1 = this$0.createStringArrayList();
      internStringArrayList(arrayList1);
      this.usesOptionalLibraries = arrayList1 = this$0.createStringArrayList();
      internStringArrayList(arrayList1);
      this.usesLibraryFiles = this$0.readStringArray();
      this.usesLibraryInfos = this$0.createTypedArrayList(SharedLibraryInfo.CREATOR);
      int i = this$0.readInt();
      if (i > 0) {
        this.usesStaticLibraries = arrayList1 = new ArrayList<>(i);
        this$0.readStringList(arrayList1);
        internStringArrayList(this.usesStaticLibraries);
        long[] arrayOfLong = new long[i];
        this$0.readLongArray(arrayOfLong);
        this.usesStaticLibrariesCertDigests = new String[i][];
        for (byte b = 0; b < i; b++)
          this.usesStaticLibrariesCertDigests[b] = this$0.createStringArray(); 
      } 
      this.preferredActivityFilters = (ArrayList)(arrayList1 = new ArrayList<>());
      this$0.readParcelableList(arrayList1, classLoader);
      if (this.preferredActivityFilters.size() == 0)
        this.preferredActivityFilters = null; 
      this.mOriginalPackages = this$0.createStringArrayList();
      this.mRealPackage = this$0.readString();
      this.mAdoptPermissions = this$0.createStringArrayList();
      this.mAppMetaData = this$0.readBundle();
      this.mVersionCode = this$0.readInt();
      this.mVersionCodeMajor = this$0.readInt();
      String str1 = this$0.readString();
      if (str1 != null)
        this.mVersionName = str1.intern(); 
      this.mSharedUserId = str1 = this$0.readString();
      if (str1 != null)
        this.mSharedUserId = str1.intern(); 
      this.mSharedUserLabel = this$0.readInt();
      this.mSigningDetails = (PackageParser.SigningDetails)this$0.readParcelable(classLoader);
      this.mPreferredOrder = this$0.readInt();
      ArrayList<ConfigurationInfo> arrayList = new ArrayList();
      this$0.readParcelableList(arrayList, classLoader);
      if (this.configPreferences.size() == 0)
        this.configPreferences = null; 
      this.reqFeatures = (ArrayList)(arrayList = new ArrayList<>());
      this$0.readParcelableList(arrayList, classLoader);
      if (this.reqFeatures.size() == 0)
        this.reqFeatures = null; 
      this.featureGroups = (ArrayList)(arrayList = new ArrayList<>());
      this$0.readParcelableList(arrayList, classLoader);
      if (this.featureGroups.size() == 0)
        this.featureGroups = null; 
      this.installLocation = this$0.readInt();
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.coreApp = bool2;
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mRequiredForAllUsers = bool2;
      this.mRestrictedAccountType = this$0.readString();
      this.mRequiredAccountType = this$0.readString();
      this.mOverlayTarget = this$0.readString();
      this.mOverlayTargetName = this$0.readString();
      this.mOverlayCategory = this$0.readString();
      this.mOverlayPriority = this$0.readInt();
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.mOverlayIsStatic = bool2;
      this.mCompileSdkVersion = this$0.readInt();
      this.mCompileSdkVersionCodename = this$0.readString();
      this.mUpgradeKeySets = this$0.readArraySet(classLoader);
      this.mKeySetMapping = readKeySetMapping((Parcel)this$0);
      this.cpuAbiOverride = this$0.readString();
      if (this$0.readInt() == 1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      this.use32bitAbi = bool2;
      this.restrictUpdateHash = this$0.createByteArray();
      boolean bool2 = bool1;
      if (this$0.readInt() == 1)
        bool2 = true; 
      this.visibleToInstantApps = bool2;
    }
    
    public Package(PackageParser this$0) {
      this.permissions = new ArrayList<>(0);
      this.permissionGroups = new ArrayList<>(0);
      this.activities = new ArrayList<>(0);
      this.receivers = new ArrayList<>(0);
      this.providers = new ArrayList<>(0);
      this.services = new ArrayList<>(0);
      this.instrumentation = new ArrayList<>(0);
      this.requestedPermissions = new ArrayList<>();
      this.implicitPermissions = new ArrayList<>();
      this.staticSharedLibName = null;
      this.staticSharedLibVersion = 0L;
      this.libraryNames = null;
      this.usesLibraries = null;
      this.usesStaticLibraries = null;
      this.usesStaticLibrariesVersions = null;
      this.usesStaticLibrariesCertDigests = null;
      this.usesOptionalLibraries = null;
      this.usesLibraryFiles = null;
      this.usesLibraryInfos = null;
      this.preferredActivityFilters = null;
      this.mOriginalPackages = null;
      this.mRealPackage = null;
      this.mAdoptPermissions = null;
      this.mAppMetaData = null;
      this.mSigningDetails = PackageParser.SigningDetails.UNKNOWN;
      this.mPreferredOrder = 0;
      this.mLastPackageUsageTimeInMills = new long[8];
      this.configPreferences = null;
      this.reqFeatures = null;
      this.featureGroups = null;
      this.packageName = (String)this$0;
      this.manifestPackageName = (String)this$0;
      this.applicationInfo.packageName = (String)this$0;
      this.applicationInfo.uid = -1;
    }
    
    public long getLongVersionCode() {
      return PackageInfo.composeLongVersionCode(this.mVersionCodeMajor, this.mVersionCode);
    }
    
    public void setApplicationVolumeUuid(String param1String) {
      UUID uUID = StorageManager.convert(param1String);
      this.applicationInfo.volumeUuid = param1String;
      this.applicationInfo.storageUuid = uUID;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++) {
          ((Package)this.childPackages.get(b)).applicationInfo.volumeUuid = param1String;
          ((Package)this.childPackages.get(b)).applicationInfo.storageUuid = uUID;
        } 
      } 
    }
    
    public void setApplicationInfoCodePath(String param1String) {
      this.applicationInfo.setCodePath(param1String);
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).applicationInfo.setCodePath(param1String); 
      } 
    }
    
    @Deprecated
    public void setApplicationInfoResourcePath(String param1String) {
      this.applicationInfo.setResourcePath(param1String);
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).applicationInfo.setResourcePath(param1String); 
      } 
    }
    
    @Deprecated
    public void setApplicationInfoBaseResourcePath(String param1String) {
      this.applicationInfo.setBaseResourcePath(param1String);
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).applicationInfo.setBaseResourcePath(param1String); 
      } 
    }
    
    public void setApplicationInfoBaseCodePath(String param1String) {
      this.applicationInfo.setBaseCodePath(param1String);
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).applicationInfo.setBaseCodePath(param1String); 
      } 
    }
    
    public List<String> getChildPackageNames() {
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList == null)
        return null; 
      int i = arrayList.size();
      ArrayList<String> arrayList1 = new ArrayList(i);
      for (byte b = 0; b < i; b++) {
        String str = ((Package)this.childPackages.get(b)).packageName;
        arrayList1.add(str);
      } 
      return arrayList1;
    }
    
    public boolean hasChildPackage(String param1String) {
      byte b1;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        b1 = arrayList.size();
      } else {
        b1 = 0;
      } 
      for (byte b2 = 0; b2 < b1; b2++) {
        if (((Package)this.childPackages.get(b2)).packageName.equals(param1String))
          return true; 
      } 
      return false;
    }
    
    public void setApplicationInfoSplitCodePaths(String[] param1ArrayOfString) {
      this.applicationInfo.setSplitCodePaths(param1ArrayOfString);
    }
    
    @Deprecated
    public void setApplicationInfoSplitResourcePaths(String[] param1ArrayOfString) {
      this.applicationInfo.setSplitResourcePaths(param1ArrayOfString);
    }
    
    public void setSplitCodePaths(String[] param1ArrayOfString) {
      this.splitCodePaths = param1ArrayOfString;
    }
    
    public void setCodePath(String param1String) {
      this.codePath = param1String;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).codePath = param1String; 
      } 
    }
    
    public void setBaseCodePath(String param1String) {
      this.baseCodePath = param1String;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).baseCodePath = param1String; 
      } 
    }
    
    public void setSigningDetails(PackageParser.SigningDetails param1SigningDetails) {
      this.mSigningDetails = param1SigningDetails;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).mSigningDetails = param1SigningDetails; 
      } 
    }
    
    public void setVolumeUuid(String param1String) {
      this.volumeUuid = param1String;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).volumeUuid = param1String; 
      } 
    }
    
    public void setApplicationInfoFlags(int param1Int1, int param1Int2) {
      ApplicationInfo applicationInfo = this.applicationInfo;
      applicationInfo.flags = applicationInfo.flags & (param1Int1 ^ 0xFFFFFFFF) | param1Int1 & param1Int2;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).applicationInfo.flags = this.applicationInfo.flags & (param1Int1 ^ 0xFFFFFFFF) | param1Int1 & param1Int2; 
      } 
    }
    
    public void setUse32bitAbi(boolean param1Boolean) {
      this.use32bitAbi = param1Boolean;
      ArrayList<Package> arrayList = this.childPackages;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++)
          ((Package)this.childPackages.get(b)).use32bitAbi = param1Boolean; 
      } 
    }
    
    public boolean isLibrary() {
      return (this.staticSharedLibName != null || !ArrayUtils.isEmpty(this.libraryNames));
    }
    
    public List<String> getAllCodePaths() {
      ArrayList<String> arrayList = new ArrayList();
      arrayList.add(this.baseCodePath);
      if (!ArrayUtils.isEmpty((Object[])this.splitCodePaths))
        Collections.addAll(arrayList, this.splitCodePaths); 
      return arrayList;
    }
    
    public List<String> getAllCodePathsExcludingResourceOnly() {
      ArrayList<String> arrayList = new ArrayList();
      if ((this.applicationInfo.flags & 0x4) != 0)
        arrayList.add(this.baseCodePath); 
      if (!ArrayUtils.isEmpty((Object[])this.splitCodePaths)) {
        byte b = 0;
        while (true) {
          String[] arrayOfString = this.splitCodePaths;
          if (b < arrayOfString.length) {
            if ((this.splitFlags[b] & 0x4) != 0)
              arrayList.add(arrayOfString[b]); 
            b++;
            continue;
          } 
          break;
        } 
      } 
      return arrayList;
    }
    
    public void setPackageName(String param1String) {
      this.packageName = param1String;
      this.applicationInfo.packageName = param1String;
      int i;
      for (i = this.permissions.size() - 1; i >= 0; i--)
        ((PackageParser.Permission)this.permissions.get(i)).setPackageName(param1String); 
      for (i = this.permissionGroups.size() - 1; i >= 0; i--)
        ((PackageParser.PermissionGroup)this.permissionGroups.get(i)).setPackageName(param1String); 
      for (i = this.activities.size() - 1; i >= 0; i--)
        ((PackageParser.Activity)this.activities.get(i)).setPackageName(param1String); 
      for (i = this.receivers.size() - 1; i >= 0; i--)
        ((PackageParser.Activity)this.receivers.get(i)).setPackageName(param1String); 
      for (i = this.providers.size() - 1; i >= 0; i--)
        ((PackageParser.Provider)this.providers.get(i)).setPackageName(param1String); 
      for (i = this.services.size() - 1; i >= 0; i--)
        ((PackageParser.Service)this.services.get(i)).setPackageName(param1String); 
      for (i = this.instrumentation.size() - 1; i >= 0; i--)
        ((PackageParser.Instrumentation)this.instrumentation.get(i)).setPackageName(param1String); 
    }
    
    public boolean hasComponentClassName(String param1String) {
      int i;
      for (i = this.activities.size() - 1; i >= 0; i--) {
        if (param1String.equals(((PackageParser.Activity)this.activities.get(i)).className))
          return true; 
      } 
      for (i = this.receivers.size() - 1; i >= 0; i--) {
        if (param1String.equals(((PackageParser.Activity)this.receivers.get(i)).className))
          return true; 
      } 
      for (i = this.providers.size() - 1; i >= 0; i--) {
        if (param1String.equals(((PackageParser.Provider)this.providers.get(i)).className))
          return true; 
      } 
      for (i = this.services.size() - 1; i >= 0; i--) {
        if (param1String.equals(((PackageParser.Service)this.services.get(i)).className))
          return true; 
      } 
      for (i = this.instrumentation.size() - 1; i >= 0; i--) {
        if (param1String.equals(((PackageParser.Instrumentation)this.instrumentation.get(i)).className))
          return true; 
      } 
      return false;
    }
    
    public boolean isExternal() {
      return this.applicationInfo.isExternal();
    }
    
    public boolean isForwardLocked() {
      return false;
    }
    
    public boolean isOem() {
      return this.applicationInfo.isOem();
    }
    
    public boolean isVendor() {
      return this.applicationInfo.isVendor();
    }
    
    public boolean isProduct() {
      return this.applicationInfo.isProduct();
    }
    
    public boolean isSystemExt() {
      return this.applicationInfo.isSystemExt();
    }
    
    public boolean isOdm() {
      return this.applicationInfo.isOdm();
    }
    
    public boolean isPrivileged() {
      return this.applicationInfo.isPrivilegedApp();
    }
    
    public boolean isSystem() {
      return this.applicationInfo.isSystemApp();
    }
    
    public boolean isUpdatedSystemApp() {
      return this.applicationInfo.isUpdatedSystemApp();
    }
    
    public boolean canHaveOatDir() {
      return true;
    }
    
    public boolean isMatch(int param1Int) {
      if ((0x100000 & param1Int) != 0)
        return isSystem(); 
      return true;
    }
    
    public long getLatestPackageUseTimeInMills() {
      long l = 0L;
      for (long l1 : this.mLastPackageUsageTimeInMills)
        l = Math.max(l, l1); 
      return l;
    }
    
    public long getLatestForegroundPackageUseTimeInMills() {
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = 0;
      arrayOfInt[1] = 2;
      long l = 0L;
      int i;
      byte b;
      for (i = arrayOfInt.length, b = 0; b < i; ) {
        int j = arrayOfInt[b];
        l = Math.max(l, this.mLastPackageUsageTimeInMills[j]);
        b++;
      } 
      return l;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Package{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" ");
      stringBuilder.append(this.packageName);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    private static void internStringArrayList(List<String> param1List) {
      if (param1List != null) {
        int i = param1List.size();
        for (byte b = 0; b < i; b++)
          param1List.set(b, ((String)param1List.get(b)).intern()); 
      } 
    }
    
    public void fixupOwner(List<? extends PackageParser.Component<?>> param1List) {
      if (param1List != null)
        for (PackageParser.Component<?> component : param1List) {
          component.owner = this;
          if (component instanceof PackageParser.Activity) {
            ((PackageParser.Activity)component).info.applicationInfo = this.applicationInfo;
            continue;
          } 
          if (component instanceof PackageParser.Service) {
            ((PackageParser.Service)component).info.applicationInfo = this.applicationInfo;
            continue;
          } 
          if (component instanceof PackageParser.Provider)
            ((PackageParser.Provider)component).info.applicationInfo = this.applicationInfo; 
        }  
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.packageName);
      param1Parcel.writeString(this.manifestPackageName);
      param1Parcel.writeStringArray(this.splitNames);
      param1Parcel.writeString(this.volumeUuid);
      param1Parcel.writeString(this.codePath);
      param1Parcel.writeString(this.baseCodePath);
      param1Parcel.writeStringArray(this.splitCodePaths);
      param1Parcel.writeInt(this.baseRevisionCode);
      param1Parcel.writeIntArray(this.splitRevisionCodes);
      param1Parcel.writeIntArray(this.splitFlags);
      param1Parcel.writeIntArray(this.splitPrivateFlags);
      param1Parcel.writeInt(this.baseHardwareAccelerated);
      param1Parcel.writeParcelable(this.applicationInfo, param1Int);
      param1Parcel.writeParcelableList(this.permissions, param1Int);
      param1Parcel.writeParcelableList(this.permissionGroups, param1Int);
      param1Parcel.writeParcelableList(this.activities, param1Int);
      param1Parcel.writeParcelableList(this.receivers, param1Int);
      param1Parcel.writeParcelableList(this.providers, param1Int);
      param1Parcel.writeParcelableList(this.services, param1Int);
      param1Parcel.writeParcelableList(this.instrumentation, param1Int);
      param1Parcel.writeStringList(this.requestedPermissions);
      param1Parcel.writeStringList(this.implicitPermissions);
      param1Parcel.writeStringList(this.protectedBroadcasts);
      param1Parcel.writeParcelable(this.parentPackage, param1Int);
      param1Parcel.writeParcelableList(this.childPackages, param1Int);
      param1Parcel.writeString(this.staticSharedLibName);
      param1Parcel.writeLong(this.staticSharedLibVersion);
      param1Parcel.writeStringList(this.libraryNames);
      param1Parcel.writeStringList(this.usesLibraries);
      param1Parcel.writeStringList(this.usesOptionalLibraries);
      param1Parcel.writeStringArray(this.usesLibraryFiles);
      param1Parcel.writeTypedList(this.usesLibraryInfos);
      if (ArrayUtils.isEmpty(this.usesStaticLibraries)) {
        param1Parcel.writeInt(-1);
      } else {
        param1Parcel.writeInt(this.usesStaticLibraries.size());
        param1Parcel.writeStringList(this.usesStaticLibraries);
        param1Parcel.writeLongArray(this.usesStaticLibrariesVersions);
        for (String[] arrayOfString : this.usesStaticLibrariesCertDigests)
          param1Parcel.writeStringArray(arrayOfString); 
      } 
      param1Parcel.writeParcelableList(this.preferredActivityFilters, param1Int);
      param1Parcel.writeStringList(this.mOriginalPackages);
      param1Parcel.writeString(this.mRealPackage);
      param1Parcel.writeStringList(this.mAdoptPermissions);
      param1Parcel.writeBundle(this.mAppMetaData);
      param1Parcel.writeInt(this.mVersionCode);
      param1Parcel.writeInt(this.mVersionCodeMajor);
      param1Parcel.writeString(this.mVersionName);
      param1Parcel.writeString(this.mSharedUserId);
      param1Parcel.writeInt(this.mSharedUserLabel);
      param1Parcel.writeParcelable(this.mSigningDetails, param1Int);
      param1Parcel.writeInt(this.mPreferredOrder);
      param1Parcel.writeParcelableList(this.configPreferences, param1Int);
      param1Parcel.writeParcelableList(this.reqFeatures, param1Int);
      param1Parcel.writeParcelableList(this.featureGroups, param1Int);
      param1Parcel.writeInt(this.installLocation);
      param1Parcel.writeInt(this.coreApp);
      param1Parcel.writeInt(this.mRequiredForAllUsers);
      param1Parcel.writeString(this.mRestrictedAccountType);
      param1Parcel.writeString(this.mRequiredAccountType);
      param1Parcel.writeString(this.mOverlayTarget);
      param1Parcel.writeString(this.mOverlayTargetName);
      param1Parcel.writeString(this.mOverlayCategory);
      param1Parcel.writeInt(this.mOverlayPriority);
      param1Parcel.writeInt(this.mOverlayIsStatic);
      param1Parcel.writeInt(this.mCompileSdkVersion);
      param1Parcel.writeString(this.mCompileSdkVersionCodename);
      param1Parcel.writeArraySet(this.mUpgradeKeySets);
      writeKeySetMapping(param1Parcel, this.mKeySetMapping);
      param1Parcel.writeString(this.cpuAbiOverride);
      param1Parcel.writeInt(this.use32bitAbi);
      param1Parcel.writeByteArray(this.restrictUpdateHash);
      param1Parcel.writeInt(this.visibleToInstantApps);
    }
    
    private static void writeKeySetMapping(Parcel param1Parcel, ArrayMap<String, ArraySet<PublicKey>> param1ArrayMap) {
      if (param1ArrayMap == null) {
        param1Parcel.writeInt(-1);
        return;
      } 
      int i = param1ArrayMap.size();
      param1Parcel.writeInt(i);
      for (byte b = 0; b < i; b++) {
        param1Parcel.writeString((String)param1ArrayMap.keyAt(b));
        ArraySet arraySet = (ArraySet)param1ArrayMap.valueAt(b);
        if (arraySet == null) {
          param1Parcel.writeInt(-1);
        } else {
          int j = arraySet.size();
          param1Parcel.writeInt(j);
          for (byte b1 = 0; b1 < j; b1++)
            param1Parcel.writeSerializable((Serializable)arraySet.valueAt(b1)); 
        } 
      } 
    }
    
    private static ArrayMap<String, ArraySet<PublicKey>> readKeySetMapping(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i == -1)
        return null; 
      ArrayMap<String, ArraySet<PublicKey>> arrayMap = new ArrayMap();
      for (byte b = 0; b < i; b++) {
        String str = param1Parcel.readString();
        int j = param1Parcel.readInt();
        if (j == -1) {
          arrayMap.put(str, null);
        } else {
          ArraySet arraySet = new ArraySet(j);
          for (byte b1 = 0; b1 < j; b1++) {
            PublicKey publicKey = (PublicKey)param1Parcel.readSerializable();
            arraySet.add(publicKey);
          } 
          arrayMap.put(str, arraySet);
        } 
      } 
      return arrayMap;
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Package>() {
        public PackageParser.Package createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Package(param2Parcel);
        }
        
        public PackageParser.Package[] newArray(int param2Int) {
          return new PackageParser.Package[param2Int];
        }
      };
  }
  
  class Component<II extends IntentInfo> {
    public final String className;
    
    ComponentName componentName;
    
    String componentShortName;
    
    public final ArrayList<II> intents;
    
    public Bundle metaData;
    
    public int order;
    
    public PackageParser.Package owner;
    
    public Component(PackageParser this$0, ArrayList<II> param1ArrayList, String param1String) {
      this.owner = (PackageParser.Package)this$0;
      this.intents = param1ArrayList;
      this.className = param1String;
    }
    
    public Component(PackageParser this$0) {
      this.owner = (PackageParser.Package)this$0;
      this.intents = null;
      this.className = null;
    }
    
    public Component(PackageItemInfo param1PackageItemInfo) {
      this.owner = ((PackageParser.ParsePackageItemArgs)this$0).owner;
      this.intents = new ArrayList<>(0);
      if (PackageParser.parsePackageItemInfo(((PackageParser.ParsePackageItemArgs)this$0).owner, param1PackageItemInfo, ((PackageParser.ParsePackageItemArgs)this$0).outError, ((PackageParser.ParsePackageItemArgs)this$0).tag, ((PackageParser.ParsePackageItemArgs)this$0).sa, true, ((PackageParser.ParsePackageItemArgs)this$0).nameRes, ((PackageParser.ParsePackageItemArgs)this$0).labelRes, ((PackageParser.ParsePackageItemArgs)this$0).iconRes, ((PackageParser.ParsePackageItemArgs)this$0).roundIconRes, ((PackageParser.ParsePackageItemArgs)this$0).logoRes, ((PackageParser.ParsePackageItemArgs)this$0).bannerRes)) {
        this.className = param1PackageItemInfo.name;
      } else {
        this.className = null;
      } 
    }
    
    public Component(ComponentInfo param1ComponentInfo) {
      this((PackageParser.ParsePackageItemArgs)this$0, param1ComponentInfo);
      if (((PackageParser.ParseComponentArgs)this$0).outError[0] != null)
        return; 
      if (((PackageParser.ParseComponentArgs)this$0).processRes != 0) {
        String str;
        if (this.owner.applicationInfo.targetSdkVersion >= 8) {
          str = ((PackageParser.ParseComponentArgs)this$0).sa.getNonConfigurationString(((PackageParser.ParseComponentArgs)this$0).processRes, 1024);
        } else {
          str = ((PackageParser.ParseComponentArgs)this$0).sa.getNonResourceString(((PackageParser.ParseComponentArgs)this$0).processRes);
        } 
        param1ComponentInfo.processName = PackageParser.buildProcessName(this.owner.applicationInfo.packageName, this.owner.applicationInfo.processName, str, ((PackageParser.ParseComponentArgs)this$0).flags, ((PackageParser.ParseComponentArgs)this$0).sepProcesses, ((PackageParser.ParseComponentArgs)this$0).outError);
      } 
      if (((PackageParser.ParseComponentArgs)this$0).descriptionRes != 0)
        param1ComponentInfo.descriptionRes = ((PackageParser.ParseComponentArgs)this$0).sa.getResourceId(((PackageParser.ParseComponentArgs)this$0).descriptionRes, 0); 
      param1ComponentInfo.enabled = ((PackageParser.ParseComponentArgs)this$0).sa.getBoolean(((PackageParser.ParseComponentArgs)this$0).enabledRes, true);
    }
    
    public Component(PackageParser this$0) {
      this.owner = ((Component)this$0).owner;
      this.intents = ((Component)this$0).intents;
      this.className = ((Component)this$0).className;
      this.componentName = ((Component)this$0).componentName;
      this.componentShortName = ((Component)this$0).componentShortName;
    }
    
    public ComponentName getComponentName() {
      ComponentName componentName = this.componentName;
      if (componentName != null)
        return componentName; 
      if (this.className != null)
        this.componentName = new ComponentName(this.owner.applicationInfo.packageName, this.className); 
      return this.componentName;
    }
    
    protected Component() {
      this.className = this$0.readString();
      this.metaData = this$0.readBundle();
      this.intents = createIntentsList((Parcel)this$0);
      this.owner = null;
    }
    
    protected void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.className);
      param1Parcel.writeBundle(this.metaData);
      writeIntentsList(this.intents, param1Parcel, param1Int);
    }
    
    private static void writeIntentsList(ArrayList<? extends PackageParser.IntentInfo> param1ArrayList, Parcel param1Parcel, int param1Int) {
      if (param1ArrayList == null) {
        param1Parcel.writeInt(-1);
        return;
      } 
      int i = param1ArrayList.size();
      param1Parcel.writeInt(i);
      if (i > 0) {
        PackageParser.IntentInfo intentInfo = param1ArrayList.get(0);
        param1Parcel.writeString(intentInfo.getClass().getName());
        for (byte b = 0; b < i; b++)
          ((PackageParser.IntentInfo)param1ArrayList.get(b)).writeIntentInfoToParcel(param1Parcel, param1Int); 
      } 
    }
    
    private static <T extends PackageParser.IntentInfo> ArrayList<T> createIntentsList(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      if (i == -1)
        return null; 
      if (i == 0)
        return new ArrayList<>(0); 
      String str = param1Parcel.readString();
      try {
        Class<?> clazz = Class.forName(str);
        Constructor<?> constructor = clazz.getConstructor(new Class[] { Parcel.class });
        ArrayList<PackageParser.IntentInfo> arrayList = new ArrayList();
        this(i);
        for (byte b = 0; b < i; b++) {
          arrayList.add((PackageParser.IntentInfo)constructor.newInstance(new Object[] { param1Parcel }));
        } 
        return (ArrayList)arrayList;
      } catch (ReflectiveOperationException reflectiveOperationException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to construct intent list for: ");
        stringBuilder.append(str);
        throw new AssertionError(stringBuilder.toString());
      } 
    }
    
    public void appendComponentShortName(StringBuilder param1StringBuilder) {
      ComponentName.appendShortString(param1StringBuilder, this.owner.applicationInfo.packageName, this.className);
    }
    
    public void printComponentShortName(PrintWriter param1PrintWriter) {
      ComponentName.printShortString(param1PrintWriter, this.owner.applicationInfo.packageName, this.className);
    }
    
    public void setPackageName(String param1String) {
      this.componentName = null;
      this.componentShortName = null;
    }
  }
  
  class Permission extends Component<IntentInfo> implements Parcelable {
    public Permission(PackageParser this$0, String param1String) {
      this.info = new PermissionInfo(param1String);
    }
    
    public Permission(PackageParser this$0, PermissionInfo param1PermissionInfo) {
      this.info = param1PermissionInfo;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Permission{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" ");
      stringBuilder.append(this.info.name);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int);
      param1Parcel.writeInt(this.tree);
      param1Parcel.writeParcelable(this.group, param1Int);
    }
    
    public boolean isAppOp() {
      return this.info.isAppOp();
    }
    
    private Permission(PackageParser this$0) {
      ClassLoader classLoader = Object.class.getClassLoader();
      PermissionInfo permissionInfo = (PermissionInfo)this$0.readParcelable(classLoader);
      if (permissionInfo.group != null) {
        permissionInfo = this.info;
        permissionInfo.group = permissionInfo.group.intern();
      } 
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.tree = bool;
      this.group = (PackageParser.PermissionGroup)this$0.readParcelable(classLoader);
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Permission>() {
        public PackageParser.Permission createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Permission();
        }
        
        public PackageParser.Permission[] newArray(int param2Int) {
          return new PackageParser.Permission[param2Int];
        }
      };
    
    public PackageParser.PermissionGroup group;
    
    public final PermissionInfo info;
    
    public boolean tree;
  }
  
  class PermissionGroup extends Component<IntentInfo> implements Parcelable {
    public PermissionGroup(PackageParser this$0, int param1Int1, int param1Int2, int param1Int3) {
      this.info = new PermissionGroupInfo(param1Int1, param1Int2, param1Int3);
    }
    
    public PermissionGroup(PackageParser this$0, PermissionGroupInfo param1PermissionGroupInfo) {
      this.info = param1PermissionGroupInfo;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PermissionGroup{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(" ");
      stringBuilder.append(this.info.name);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int);
    }
    
    private PermissionGroup(PackageParser this$0) {
      this.info = (PermissionGroupInfo)this$0.readParcelable(Object.class.getClassLoader());
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<PermissionGroup>() {
        public PackageParser.PermissionGroup createFromParcel(Parcel param2Parcel) {
          return new PackageParser.PermissionGroup();
        }
        
        public PackageParser.PermissionGroup[] newArray(int param2Int) {
          return new PackageParser.PermissionGroup[param2Int];
        }
      };
    
    public final PermissionGroupInfo info;
  }
  
  private static boolean copyNeeded(int paramInt1, Package paramPackage, PackageUserState paramPackageUserState, Bundle paramBundle, int paramInt2) {
    boolean bool;
    if (paramInt2 != 0)
      return true; 
    if (paramPackageUserState.enabled != 0) {
      if (paramPackageUserState.enabled == 1) {
        bool = true;
      } else {
        bool = false;
      } 
      if (paramPackage.applicationInfo.enabled != bool)
        return true; 
    } 
    if ((paramPackage.applicationInfo.flags & 0x40000000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramPackageUserState.suspended != bool)
      return true; 
    if (!paramPackageUserState.installed || paramPackageUserState.hidden)
      return true; 
    if (paramPackageUserState.stopped)
      return true; 
    if (paramPackageUserState.instantApp != paramPackage.applicationInfo.isInstantApp())
      return true; 
    if ((paramInt1 & 0x80) != 0 && (paramBundle != null || paramPackage.mAppMetaData != null))
      return true; 
    if ((paramInt1 & 0x400) != 0 && paramPackage.usesLibraryFiles != null)
      return true; 
    if ((paramInt1 & 0x400) != 0 && paramPackage.usesLibraryInfos != null)
      return true; 
    if (paramPackage.staticSharedLibName != null)
      return true; 
    return false;
  }
  
  public static ApplicationInfo generateApplicationInfo(Package paramPackage, int paramInt, PackageUserState paramPackageUserState) {
    return generateApplicationInfo(paramPackage, paramInt, paramPackageUserState, UserHandle.getCallingUserId());
  }
  
  private static void updateApplicationInfo(ApplicationInfo paramApplicationInfo, int paramInt, PackageUserState paramPackageUserState) {
    if (!sCompatibilityModeEnabled)
      paramApplicationInfo.disableCompatibilityMode(); 
    if (paramPackageUserState.installed) {
      paramApplicationInfo.flags |= 0x800000;
    } else {
      paramApplicationInfo.flags &= 0xFF7FFFFF;
    } 
    if (paramPackageUserState.suspended) {
      paramApplicationInfo.flags |= 0x40000000;
    } else {
      paramApplicationInfo.flags &= 0xBFFFFFFF;
    } 
    if (paramPackageUserState.instantApp) {
      paramApplicationInfo.privateFlags |= 0x80;
    } else {
      paramApplicationInfo.privateFlags &= 0xFFFFFF7F;
    } 
    if (paramPackageUserState.virtualPreload) {
      paramApplicationInfo.privateFlags |= 0x10000;
    } else {
      paramApplicationInfo.privateFlags &= 0xFFFEFFFF;
    } 
    boolean bool = paramPackageUserState.hidden;
    boolean bool1 = true;
    if (bool) {
      paramApplicationInfo.privateFlags |= 0x1;
    } else {
      paramApplicationInfo.privateFlags &= 0xFFFFFFFE;
    } 
    if (paramPackageUserState.enabled == 1) {
      paramApplicationInfo.enabled = true;
    } else if (paramPackageUserState.enabled == 4) {
      if ((0x8000 & paramInt) == 0)
        bool1 = false; 
      paramApplicationInfo.enabled = bool1;
    } else if (paramPackageUserState.enabled == 2 || paramPackageUserState.enabled == 3) {
      paramApplicationInfo.enabled = false;
    } 
    paramApplicationInfo.enabledSetting = paramPackageUserState.enabled;
    if (paramApplicationInfo.category == -1)
      paramApplicationInfo.category = paramPackageUserState.categoryHint; 
    if (paramApplicationInfo.category == -1)
      paramApplicationInfo.category = FallbackCategoryProvider.getFallbackCategory(paramApplicationInfo.packageName); 
    paramApplicationInfo.seInfoUser = SELinuxUtil.assignSeinfoUser(paramPackageUserState);
    paramApplicationInfo.resourceDirs = paramPackageUserState.getAllOverlayPaths();
    if (sUseRoundIcon && paramApplicationInfo.roundIconRes != 0) {
      paramInt = paramApplicationInfo.roundIconRes;
    } else {
      paramInt = paramApplicationInfo.iconRes;
    } 
    paramApplicationInfo.icon = paramInt;
    if (paramApplicationInfo.mOplusApplicationInfoEx != null)
      paramApplicationInfo.mOplusApplicationInfoEx.oplusFreezeState = paramPackageUserState.oplusFreezeState; 
  }
  
  public static ApplicationInfo generateApplicationInfo(Package paramPackage, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramPackage == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramPackage.applicationInfo) || !paramPackage.isMatch(paramInt1))
      return null; 
    if (!copyNeeded(paramInt1, paramPackage, paramPackageUserState, (Bundle)null, paramInt2) && ((0x8000 & paramInt1) == 0 || paramPackageUserState.enabled != 4)) {
      updateApplicationInfo(paramPackage.applicationInfo, paramInt1, paramPackageUserState);
      return paramPackage.applicationInfo;
    } 
    ApplicationInfo applicationInfo = new ApplicationInfo(paramPackage.applicationInfo);
    applicationInfo.initForUser(paramInt2);
    if ((paramInt1 & 0x80) != 0)
      applicationInfo.metaData = paramPackage.mAppMetaData; 
    if ((paramInt1 & 0x400) != 0) {
      applicationInfo.sharedLibraryFiles = paramPackage.usesLibraryFiles;
      applicationInfo.sharedLibraryInfos = paramPackage.usesLibraryInfos;
    } 
    if (paramPackageUserState.stopped) {
      applicationInfo.flags |= 0x200000;
    } else {
      applicationInfo.flags &= 0xFFDFFFFF;
    } 
    updateApplicationInfo(applicationInfo, paramInt1, paramPackageUserState);
    return applicationInfo;
  }
  
  public static ApplicationInfo generateApplicationInfo(ApplicationInfo paramApplicationInfo, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramApplicationInfo == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramApplicationInfo))
      return null; 
    paramApplicationInfo = new ApplicationInfo(paramApplicationInfo);
    paramApplicationInfo.initForUser(paramInt2);
    if (paramPackageUserState.stopped) {
      paramApplicationInfo.flags |= 0x200000;
    } else {
      paramApplicationInfo.flags &= 0xFFDFFFFF;
    } 
    updateApplicationInfo(paramApplicationInfo, paramInt1, paramPackageUserState);
    return paramApplicationInfo;
  }
  
  public static final PermissionInfo generatePermissionInfo(Permission paramPermission, int paramInt) {
    if (paramPermission == null)
      return null; 
    if ((paramInt & 0x80) == 0)
      return paramPermission.info; 
    PermissionInfo permissionInfo = new PermissionInfo(paramPermission.info);
    permissionInfo.metaData = paramPermission.metaData;
    return permissionInfo;
  }
  
  public static final PermissionGroupInfo generatePermissionGroupInfo(PermissionGroup paramPermissionGroup, int paramInt) {
    if (paramPermissionGroup == null)
      return null; 
    if ((paramInt & 0x80) == 0)
      return paramPermissionGroup.info; 
    PermissionGroupInfo permissionGroupInfo = new PermissionGroupInfo(paramPermissionGroup.info);
    permissionGroupInfo.metaData = paramPermissionGroup.metaData;
    return permissionGroupInfo;
  }
  
  class Activity extends Component<ActivityIntentInfo> implements Parcelable {
    private boolean hasMaxAspectRatio() {
      return this.mHasMaxAspectRatio;
    }
    
    private boolean hasMinAspectRatio() {
      return this.mHasMinAspectRatio;
    }
    
    Activity(PackageParser this$0, String param1String, ActivityInfo param1ActivityInfo) {
      super(new ArrayList<>(0), param1String);
      this.info = param1ActivityInfo;
      param1ActivityInfo.applicationInfo = ((PackageParser.Package)this$0).applicationInfo;
    }
    
    public Activity(PackageParser this$0, ActivityInfo param1ActivityInfo) {
      super(param1ActivityInfo);
      this.info = param1ActivityInfo;
      param1ActivityInfo.applicationInfo = ((PackageParser.ParseComponentArgs)this$0).owner.applicationInfo;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    private void setMaxAspectRatio(float param1Float) {
      if (this.info.resizeMode == 2 || this.info.resizeMode == 1)
        return; 
      if (param1Float < 1.0F && param1Float != 0.0F)
        return; 
      this.info.maxAspectRatio = param1Float;
      this.mHasMaxAspectRatio = true;
    }
    
    private void setMinAspectRatio(float param1Float) {
      if (this.info.resizeMode == 2 || this.info.resizeMode == 1)
        return; 
      if (param1Float < 1.0F && param1Float != 0.0F)
        return; 
      this.info.minAspectRatio = param1Float;
      this.mHasMinAspectRatio = true;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("Activity{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int | 0x2);
      param1Parcel.writeBoolean(this.mHasMaxAspectRatio);
      param1Parcel.writeBoolean(this.mHasMinAspectRatio);
    }
    
    private Activity(PackageParser this$0) {
      this.info = (ActivityInfo)this$0.readParcelable(Object.class.getClassLoader());
      this.mHasMaxAspectRatio = this$0.readBoolean();
      this.mHasMinAspectRatio = this$0.readBoolean();
      for (PackageParser.ActivityIntentInfo activityIntentInfo : this.intents) {
        activityIntentInfo.activity = this;
        this.order = Math.max(activityIntentInfo.getOrder(), this.order);
      } 
      if (this.info.permission != null) {
        ActivityInfo activityInfo = this.info;
        activityInfo.permission = activityInfo.permission.intern();
      } 
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Activity>() {
        public PackageParser.Activity createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Activity();
        }
        
        public PackageParser.Activity[] newArray(int param2Int) {
          return new PackageParser.Activity[param2Int];
        }
      };
    
    public final ActivityInfo info;
    
    private boolean mHasMaxAspectRatio;
    
    private boolean mHasMinAspectRatio;
  }
  
  public static final ActivityInfo generateActivityInfo(Activity paramActivity, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramActivity == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramActivity.owner.applicationInfo))
      return null; 
    if (!copyNeeded(paramInt1, paramActivity.owner, paramPackageUserState, paramActivity.metaData, paramInt2)) {
      updateApplicationInfo(paramActivity.info.applicationInfo, paramInt1, paramPackageUserState);
      return paramActivity.info;
    } 
    ActivityInfo activityInfo = new ActivityInfo(paramActivity.info);
    activityInfo.metaData = paramActivity.metaData;
    activityInfo.applicationInfo = generateApplicationInfo(paramActivity.owner, paramInt1, paramPackageUserState, paramInt2);
    return activityInfo;
  }
  
  public static final ActivityInfo generateActivityInfo(ActivityInfo paramActivityInfo, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramActivityInfo == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramActivityInfo.applicationInfo))
      return null; 
    paramActivityInfo = new ActivityInfo(paramActivityInfo);
    paramActivityInfo.applicationInfo = generateApplicationInfo(paramActivityInfo.applicationInfo, paramInt1, paramPackageUserState, paramInt2);
    return paramActivityInfo;
  }
  
  class Service extends Component<ServiceIntentInfo> implements Parcelable {
    public Service(PackageParser this$0, ServiceInfo param1ServiceInfo) {
      super(param1ServiceInfo);
      this.info = param1ServiceInfo;
      param1ServiceInfo.applicationInfo = ((PackageParser.ParseComponentArgs)this$0).owner.applicationInfo;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("Service{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int | 0x2);
    }
    
    private Service(PackageParser this$0) {
      this.info = (ServiceInfo)this$0.readParcelable(Object.class.getClassLoader());
      for (PackageParser.ServiceIntentInfo serviceIntentInfo : this.intents) {
        serviceIntentInfo.service = this;
        this.order = Math.max(serviceIntentInfo.getOrder(), this.order);
      } 
      if (this.info.permission != null) {
        ServiceInfo serviceInfo = this.info;
        serviceInfo.permission = serviceInfo.permission.intern();
      } 
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Service>() {
        public PackageParser.Service createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Service();
        }
        
        public PackageParser.Service[] newArray(int param2Int) {
          return new PackageParser.Service[param2Int];
        }
      };
    
    public final ServiceInfo info;
  }
  
  public static final ServiceInfo generateServiceInfo(Service paramService, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramService == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramService.owner.applicationInfo))
      return null; 
    if (!copyNeeded(paramInt1, paramService.owner, paramPackageUserState, paramService.metaData, paramInt2)) {
      updateApplicationInfo(paramService.info.applicationInfo, paramInt1, paramPackageUserState);
      return paramService.info;
    } 
    ServiceInfo serviceInfo = new ServiceInfo(paramService.info);
    serviceInfo.metaData = paramService.metaData;
    serviceInfo.applicationInfo = generateApplicationInfo(paramService.owner, paramInt1, paramPackageUserState, paramInt2);
    return serviceInfo;
  }
  
  class Provider extends Component<ProviderIntentInfo> implements Parcelable {
    public Provider(PackageParser this$0, ProviderInfo param1ProviderInfo) {
      super(param1ProviderInfo);
      this.info = param1ProviderInfo;
      param1ProviderInfo.applicationInfo = ((PackageParser.ParseComponentArgs)this$0).owner.applicationInfo;
      this.syncable = false;
    }
    
    public Provider(PackageParser this$0) {
      this.info = ((Provider)this$0).info;
      this.syncable = ((Provider)this$0).syncable;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("Provider{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int | 0x2);
      param1Parcel.writeInt(this.syncable);
    }
    
    private Provider(PackageParser this$0) {
      this.info = (ProviderInfo)this$0.readParcelable(Object.class.getClassLoader());
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.syncable = bool;
      for (PackageParser.ProviderIntentInfo providerIntentInfo : this.intents)
        providerIntentInfo.provider = this; 
      if (this.info.readPermission != null) {
        ProviderInfo providerInfo = this.info;
        providerInfo.readPermission = providerInfo.readPermission.intern();
      } 
      if (this.info.writePermission != null) {
        ProviderInfo providerInfo = this.info;
        providerInfo.writePermission = providerInfo.writePermission.intern();
      } 
      if (this.info.authority != null) {
        ProviderInfo providerInfo = this.info;
        providerInfo.authority = providerInfo.authority.intern();
      } 
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Provider>() {
        public PackageParser.Provider createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Provider();
        }
        
        public PackageParser.Provider[] newArray(int param2Int) {
          return new PackageParser.Provider[param2Int];
        }
      };
    
    public final ProviderInfo info;
    
    public boolean syncable;
  }
  
  public static final ProviderInfo generateProviderInfo(Provider paramProvider, int paramInt1, PackageUserState paramPackageUserState, int paramInt2) {
    if (paramProvider == null)
      return null; 
    if (!checkUseInstalledOrHidden(paramInt1, paramPackageUserState, paramProvider.owner.applicationInfo))
      return null; 
    if (!copyNeeded(paramInt1, paramProvider.owner, paramPackageUserState, paramProvider.metaData, paramInt2) && ((paramInt1 & 0x800) != 0 || paramProvider.info.uriPermissionPatterns == null)) {
      updateApplicationInfo(paramProvider.info.applicationInfo, paramInt1, paramPackageUserState);
      return paramProvider.info;
    } 
    ProviderInfo providerInfo = new ProviderInfo(paramProvider.info);
    providerInfo.metaData = paramProvider.metaData;
    if ((paramInt1 & 0x800) == 0)
      providerInfo.uriPermissionPatterns = null; 
    providerInfo.applicationInfo = generateApplicationInfo(paramProvider.owner, paramInt1, paramPackageUserState, paramInt2);
    return providerInfo;
  }
  
  class Instrumentation extends Component<IntentInfo> implements Parcelable {
    public Instrumentation(PackageParser this$0, InstrumentationInfo param1InstrumentationInfo) {
      super(param1InstrumentationInfo);
      this.info = param1InstrumentationInfo;
    }
    
    public void setPackageName(String param1String) {
      super.setPackageName(param1String);
      this.info.packageName = param1String;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("Instrumentation{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      super.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeParcelable(this.info, param1Int);
    }
    
    private Instrumentation(PackageParser this$0) {
      InstrumentationInfo instrumentationInfo = (InstrumentationInfo)this$0.readParcelable(Object.class.getClassLoader());
      if (instrumentationInfo.targetPackage != null) {
        instrumentationInfo = this.info;
        instrumentationInfo.targetPackage = instrumentationInfo.targetPackage.intern();
      } 
      if (this.info.targetProcesses != null) {
        instrumentationInfo = this.info;
        instrumentationInfo.targetProcesses = instrumentationInfo.targetProcesses.intern();
      } 
    }
    
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<Instrumentation>() {
        public PackageParser.Instrumentation createFromParcel(Parcel param2Parcel) {
          return new PackageParser.Instrumentation();
        }
        
        public PackageParser.Instrumentation[] newArray(int param2Int) {
          return new PackageParser.Instrumentation[param2Int];
        }
      };
    
    public final InstrumentationInfo info;
  }
  
  public static final InstrumentationInfo generateInstrumentationInfo(Instrumentation paramInstrumentation, int paramInt) {
    if (paramInstrumentation == null)
      return null; 
    if ((paramInt & 0x80) == 0)
      return paramInstrumentation.info; 
    InstrumentationInfo instrumentationInfo = new InstrumentationInfo(paramInstrumentation.info);
    instrumentationInfo.metaData = paramInstrumentation.metaData;
    return instrumentationInfo;
  }
  
  class IntentInfo extends IntentFilter {
    public int banner;
    
    public boolean hasDefault;
    
    public int icon;
    
    public int labelRes;
    
    public int logo;
    
    public CharSequence nonLocalizedLabel;
    
    public int preferred;
    
    protected IntentInfo() {}
    
    protected IntentInfo(PackageParser this$0) {
      super((Parcel)this$0);
      int i = this$0.readInt();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      this.hasDefault = bool;
      this.labelRes = this$0.readInt();
      this.nonLocalizedLabel = this$0.readCharSequence();
      this.icon = this$0.readInt();
      this.logo = this$0.readInt();
      this.banner = this$0.readInt();
      this.preferred = this$0.readInt();
    }
    
    public void writeIntentInfoToParcel(Parcel param1Parcel, int param1Int) {
      writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeInt(this.hasDefault);
      param1Parcel.writeInt(this.labelRes);
      param1Parcel.writeCharSequence(this.nonLocalizedLabel);
      param1Parcel.writeInt(this.icon);
      param1Parcel.writeInt(this.logo);
      param1Parcel.writeInt(this.banner);
      param1Parcel.writeInt(this.preferred);
    }
  }
  
  class ActivityIntentInfo extends IntentInfo {
    public PackageParser.Activity activity;
    
    public ActivityIntentInfo(PackageParser this$0) {
      this.activity = (PackageParser.Activity)this$0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("ActivityIntentInfo{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      this.activity.appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public ActivityIntentInfo(PackageParser this$0) {
      super((Parcel)this$0);
    }
  }
  
  class ServiceIntentInfo extends IntentInfo {
    public PackageParser.Service service;
    
    public ServiceIntentInfo(PackageParser this$0) {
      this.service = (PackageParser.Service)this$0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("ServiceIntentInfo{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      this.service.appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public ServiceIntentInfo(PackageParser this$0) {
      super((Parcel)this$0);
    }
  }
  
  class ProviderIntentInfo extends IntentInfo {
    public PackageParser.Provider provider;
    
    public ProviderIntentInfo(PackageParser this$0) {
      this.provider = (PackageParser.Provider)this$0;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("ProviderIntentInfo{");
      stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
      stringBuilder.append(' ');
      this.provider.appendComponentShortName(stringBuilder);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public ProviderIntentInfo(PackageParser this$0) {
      super((Parcel)this$0);
    }
  }
  
  public static void setCompatibilityModeEnabled(boolean paramBoolean) {
    sCompatibilityModeEnabled = paramBoolean;
  }
  
  public static void readConfigUseRoundIcon(Resources paramResources) {
    if (paramResources != null) {
      sUseRoundIcon = paramResources.getBoolean(17891574);
      return;
    } 
    try {
      IPackageManager iPackageManager = ActivityThread.getPackageManager();
      int i = UserHandle.myUserId();
      ApplicationInfo applicationInfo = iPackageManager.getApplicationInfo("android", 0, i);
      Resources resources2 = Resources.getSystem();
      ResourcesManager resourcesManager = ResourcesManager.getInstance();
      String[] arrayOfString1 = applicationInfo.resourceDirs, arrayOfString2 = applicationInfo.sharedLibraryFiles;
      CompatibilityInfo compatibilityInfo = resources2.getCompatibilityInfo();
      ClassLoader classLoader = resources2.getClassLoader();
      Resources resources1 = resourcesManager.getResources(null, null, null, arrayOfString1, arrayOfString2, 0, null, compatibilityInfo, classLoader, null);
      sUseRoundIcon = resources1.getBoolean(17891574);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class PackageParserException extends Exception {
    public final int error;
    
    public PackageParserException(PackageParser this$0, String param1String) {
      super(param1String);
      this.error = this$0;
    }
    
    public PackageParserException(PackageParser this$0, String param1String, Throwable param1Throwable) {
      super(param1String, param1Throwable);
      this.error = this$0;
    }
  }
  
  class Callback {
    public abstract boolean hasFeature(String param1String);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ParseFlags implements Annotation {}
}
