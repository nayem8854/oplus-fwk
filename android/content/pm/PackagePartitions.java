package android.content.pm;

import android.os.Environment;
import android.os.FileUtils;
import android.os.OplusSystemProperties;
import java.io.File;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.function.Function;

public class PackagePartitions {
  public static final ArrayList<SystemPartition> CUSTOM_PARTITIONS;
  
  public static final int PARTITION_ODM = 2;
  
  public static final int PARTITION_OEM = 3;
  
  public static final int PARTITION_PRODUCT = 4;
  
  public static final int PARTITION_SYSTEM = 0;
  
  public static final int PARTITION_SYSTEM_EXT = 5;
  
  public static final int PARTITION_VENDOR = 1;
  
  private static final ArrayList<SystemPartition> SYSTEM_PARTITIONS;
  
  static {
    SystemPartition systemPartition1 = new SystemPartition(Environment.getRootDirectory(), 0, true, false);
    SystemPartition systemPartition2 = new SystemPartition(Environment.getVendorDirectory(), 1, true, true);
    SystemPartition systemPartition3 = new SystemPartition(Environment.getOdmDirectory(), 2, true, true);
    SystemPartition systemPartition4 = new SystemPartition(Environment.getOemDirectory(), 3, false, true);
    SystemPartition systemPartition5 = new SystemPartition(Environment.getProductDirectory(), 4, true, true);
    SystemPartition systemPartition6 = new SystemPartition(Environment.getSystemExtDirectory(), 5, true, true);
    SYSTEM_PARTITIONS = new ArrayList<>(Arrays.asList(new SystemPartition[] { systemPartition1, systemPartition2, systemPartition3, systemPartition4, systemPartition5, systemPartition6 }));
    systemPartition5 = new SystemPartition(Environment.getMyHeytapDirectory(), 4, true, true);
    systemPartition3 = new SystemPartition(Environment.getMyStockDirectory(), 4, true, true);
    systemPartition2 = new SystemPartition(Environment.getMyProductDirectory(), 4, true, true);
    SystemPartition systemPartition7 = new SystemPartition(Environment.getMyCountryDirectory(), 4, true, true);
    SystemPartition systemPartition8 = new SystemPartition(Environment.getMyOperatorDirectory(), 4, true, true);
    systemPartition4 = new SystemPartition(Environment.getMyCompanyDirectory(), 4, true, true);
    systemPartition1 = new SystemPartition(Environment.getMyEngineeringDirectory(), 4, true, true);
    systemPartition6 = new SystemPartition(Environment.getMyPreloadDirectory(), 4, true, true);
    CUSTOM_PARTITIONS = new ArrayList<>(Arrays.asList(new SystemPartition[] { systemPartition5, systemPartition3, systemPartition2, systemPartition7, systemPartition8, systemPartition4, systemPartition1, systemPartition6 }));
  }
  
  public static <T> ArrayList<T> getOrderedPartitions(Function<SystemPartition, T> paramFunction) {
    ArrayList<T> arrayList = new ArrayList();
    byte b;
    int i;
    for (b = 0, i = SYSTEM_PARTITIONS.size(); b < i; b++) {
      T t = paramFunction.apply(SYSTEM_PARTITIONS.get(b));
      if (t != null)
        arrayList.add(t); 
    } 
    for (b = 0, i = CUSTOM_PARTITIONS.size(); b < i; b++) {
      T t = paramFunction.apply(CUSTOM_PARTITIONS.get(b));
      if (t != null)
        arrayList.add(t); 
    } 
    SystemPartition systemPartition = getOperatorGroupExtensionPartition();
    if (systemPartition != null) {
      paramFunction = (Function<SystemPartition, T>)paramFunction.apply(systemPartition);
      if (paramFunction != null)
        arrayList.add((T)paramFunction); 
    } 
    return arrayList;
  }
  
  public static SystemPartition getOperatorGroupExtensionPartition() {
    SystemPartition systemPartition;
    StringBuilder stringBuilder1 = null;
    String str1 = OplusSystemProperties.get("persist.sys.oplus.operator.opta", "");
    String str2 = OplusSystemProperties.get("persist.sys.oplus.operator.optb", "");
    StringBuilder stringBuilder2 = stringBuilder1;
    if (str1.length() != 0) {
      stringBuilder2 = stringBuilder1;
      if (str2.length() != 0) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append(Environment.getMyOperatorDirectory().getAbsolutePath());
        stringBuilder2.append("/apps_extension/");
        stringBuilder2.append(str1);
        stringBuilder2.append("_");
        stringBuilder2.append(str2);
        File file = new File(stringBuilder2.toString());
        stringBuilder2 = stringBuilder1;
        if (file.exists()) {
          stringBuilder2 = stringBuilder1;
          if (file.isDirectory()) {
            stringBuilder2 = stringBuilder1;
            if (file.canRead())
              systemPartition = new SystemPartition(file, 4, true, true); 
          } 
        } 
      } 
    } 
    return systemPartition;
  }
  
  private static File canonicalize(File paramFile) {
    try {
      return paramFile.getCanonicalFile();
    } catch (IOException iOException) {
      return paramFile;
    } 
  }
  
  public static class SystemPartition {
    private final PackagePartitions.DeferredCanonicalFile mAppFolder;
    
    private final PackagePartitions.DeferredCanonicalFile mFolder;
    
    private final PackagePartitions.DeferredCanonicalFile mOverlayFolder;
    
    private final PackagePartitions.DeferredCanonicalFile mPrivAppFolder;
    
    public final int type;
    
    private SystemPartition(File param1File, int param1Int, boolean param1Boolean1, boolean param1Boolean2) {
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile;
      this.type = param1Int;
      File file = null;
      this.mFolder = new PackagePartitions.DeferredCanonicalFile(param1File);
      this.mAppFolder = new PackagePartitions.DeferredCanonicalFile(param1File, "app");
      if (param1Boolean1) {
        deferredCanonicalFile = new PackagePartitions.DeferredCanonicalFile(param1File, "priv-app");
      } else {
        deferredCanonicalFile = null;
      } 
      this.mPrivAppFolder = deferredCanonicalFile;
      if (param1Boolean2) {
        PackagePartitions.DeferredCanonicalFile deferredCanonicalFile1 = new PackagePartitions.DeferredCanonicalFile(param1File, "overlay");
      } else {
        param1File = file;
      } 
      this.mOverlayFolder = (PackagePartitions.DeferredCanonicalFile)param1File;
    }
    
    public SystemPartition(SystemPartition param1SystemPartition) {
      this.type = param1SystemPartition.type;
      this.mFolder = new PackagePartitions.DeferredCanonicalFile(param1SystemPartition.mFolder.getFile());
      this.mAppFolder = param1SystemPartition.mAppFolder;
      this.mPrivAppFolder = param1SystemPartition.mPrivAppFolder;
      this.mOverlayFolder = param1SystemPartition.mOverlayFolder;
    }
    
    public SystemPartition(File param1File, SystemPartition param1SystemPartition) {
      this(param1File, i, bool2, bool1);
    }
    
    public File getFolder() {
      return this.mFolder.getFile();
    }
    
    public File getAppFolder() {
      File file;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mAppFolder;
      if (deferredCanonicalFile == null) {
        deferredCanonicalFile = null;
      } else {
        file = deferredCanonicalFile.getFile();
      } 
      return file;
    }
    
    public File getPrivAppFolder() {
      File file;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mPrivAppFolder;
      if (deferredCanonicalFile == null) {
        deferredCanonicalFile = null;
      } else {
        file = deferredCanonicalFile.getFile();
      } 
      return file;
    }
    
    public File getOverlayFolder() {
      File file;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mOverlayFolder;
      if (deferredCanonicalFile == null) {
        deferredCanonicalFile = null;
      } else {
        file = deferredCanonicalFile.getFile();
      } 
      return file;
    }
    
    public boolean containsPath(String param1String) {
      return containsFile(new File(param1String));
    }
    
    public boolean containsFile(File param1File) {
      return FileUtils.contains(this.mFolder.getFile(), PackagePartitions.canonicalize(param1File));
    }
    
    public boolean containsPrivApp(File param1File) {
      boolean bool;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mPrivAppFolder;
      if (deferredCanonicalFile != null && 
        FileUtils.contains(deferredCanonicalFile.getFile(), PackagePartitions.canonicalize(param1File))) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean containsApp(File param1File) {
      boolean bool;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mAppFolder;
      if (deferredCanonicalFile != null && 
        FileUtils.contains(deferredCanonicalFile.getFile(), PackagePartitions.canonicalize(param1File))) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public boolean containsOverlay(File param1File) {
      boolean bool;
      PackagePartitions.DeferredCanonicalFile deferredCanonicalFile = this.mOverlayFolder;
      if (deferredCanonicalFile != null && 
        FileUtils.contains(deferredCanonicalFile.getFile(), PackagePartitions.canonicalize(param1File))) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PartitionType {}
  
  private static class DeferredCanonicalFile {
    private File mFile;
    
    private boolean mIsCanonical = false;
    
    private DeferredCanonicalFile(File param1File) {
      this.mFile = param1File;
    }
    
    private DeferredCanonicalFile(File param1File, String param1String) {
      this.mFile = new File(param1File, param1String);
    }
    
    private File getFile() {
      if (!this.mIsCanonical) {
        this.mFile = PackagePartitions.canonicalize(this.mFile);
        this.mIsCanonical = true;
      } 
      return this.mFile;
    }
  }
}
