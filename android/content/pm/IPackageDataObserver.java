package android.content.pm;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPackageDataObserver extends IInterface {
  void onRemoveCompleted(String paramString, boolean paramBoolean) throws RemoteException;
  
  class Default implements IPackageDataObserver {
    public void onRemoveCompleted(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPackageDataObserver {
    private static final String DESCRIPTOR = "android.content.pm.IPackageDataObserver";
    
    static final int TRANSACTION_onRemoveCompleted = 1;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IPackageDataObserver");
    }
    
    public static IPackageDataObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IPackageDataObserver");
      if (iInterface != null && iInterface instanceof IPackageDataObserver)
        return (IPackageDataObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onRemoveCompleted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      boolean bool;
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.content.pm.IPackageDataObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.content.pm.IPackageDataObserver");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      onRemoveCompleted(str, bool);
      return true;
    }
    
    private static class Proxy implements IPackageDataObserver {
      public static IPackageDataObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IPackageDataObserver";
      }
      
      public void onRemoveCompleted(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.content.pm.IPackageDataObserver");
          parcel.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, null, 1);
          if (!bool1 && IPackageDataObserver.Stub.getDefaultImpl() != null) {
            IPackageDataObserver.Stub.getDefaultImpl().onRemoveCompleted(param2String, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPackageDataObserver param1IPackageDataObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPackageDataObserver != null) {
          Proxy.sDefaultImpl = param1IPackageDataObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPackageDataObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
