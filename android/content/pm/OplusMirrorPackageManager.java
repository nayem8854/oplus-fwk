package android.content.pm;

import android.content.ComponentName;
import android.graphics.drawable.Drawable;
import com.oplus.reflect.MethodParams;
import com.oplus.reflect.RefClass;
import com.oplus.reflect.RefMethod;

public class OplusMirrorPackageManager {
  public static Class<?> TYPE = RefClass.load(OplusMirrorPackageManager.class, PackageManager.class);
  
  @MethodParams({ComponentName.class})
  public static RefMethod<Void> clearCachedIconForActivity;
  
  @MethodParams({Drawable.class, boolean.class})
  public static RefMethod<Drawable> getUxIconDrawable;
}
