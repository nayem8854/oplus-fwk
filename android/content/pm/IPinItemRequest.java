package android.content.pm;

import android.appwidget.AppWidgetProviderInfo;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPinItemRequest extends IInterface {
  boolean accept(Bundle paramBundle) throws RemoteException;
  
  AppWidgetProviderInfo getAppWidgetProviderInfo() throws RemoteException;
  
  Bundle getExtras() throws RemoteException;
  
  ShortcutInfo getShortcutInfo() throws RemoteException;
  
  boolean isValid() throws RemoteException;
  
  class Default implements IPinItemRequest {
    public boolean isValid() throws RemoteException {
      return false;
    }
    
    public boolean accept(Bundle param1Bundle) throws RemoteException {
      return false;
    }
    
    public ShortcutInfo getShortcutInfo() throws RemoteException {
      return null;
    }
    
    public AppWidgetProviderInfo getAppWidgetProviderInfo() throws RemoteException {
      return null;
    }
    
    public Bundle getExtras() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPinItemRequest {
    private static final String DESCRIPTOR = "android.content.pm.IPinItemRequest";
    
    static final int TRANSACTION_accept = 2;
    
    static final int TRANSACTION_getAppWidgetProviderInfo = 4;
    
    static final int TRANSACTION_getExtras = 5;
    
    static final int TRANSACTION_getShortcutInfo = 3;
    
    static final int TRANSACTION_isValid = 1;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IPinItemRequest");
    }
    
    public static IPinItemRequest asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IPinItemRequest");
      if (iInterface != null && iInterface instanceof IPinItemRequest)
        return (IPinItemRequest)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "getExtras";
            } 
            return "getAppWidgetProviderInfo";
          } 
          return "getShortcutInfo";
        } 
        return "accept";
      } 
      return "isValid";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ShortcutInfo shortcutInfo;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          AppWidgetProviderInfo appWidgetProviderInfo;
          if (param1Int1 != 3) {
            Bundle bundle;
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.content.pm.IPinItemRequest");
                return true;
              } 
              param1Parcel1.enforceInterface("android.content.pm.IPinItemRequest");
              bundle = getExtras();
              param1Parcel2.writeNoException();
              if (bundle != null) {
                param1Parcel2.writeInt(1);
                bundle.writeToParcel(param1Parcel2, 1);
              } else {
                param1Parcel2.writeInt(0);
              } 
              return true;
            } 
            bundle.enforceInterface("android.content.pm.IPinItemRequest");
            appWidgetProviderInfo = getAppWidgetProviderInfo();
            param1Parcel2.writeNoException();
            if (appWidgetProviderInfo != null) {
              param1Parcel2.writeInt(1);
              appWidgetProviderInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          } 
          appWidgetProviderInfo.enforceInterface("android.content.pm.IPinItemRequest");
          shortcutInfo = getShortcutInfo();
          param1Parcel2.writeNoException();
          if (shortcutInfo != null) {
            param1Parcel2.writeInt(1);
            shortcutInfo.writeToParcel(param1Parcel2, 1);
          } else {
            param1Parcel2.writeInt(0);
          } 
          return true;
        } 
        shortcutInfo.enforceInterface("android.content.pm.IPinItemRequest");
        if (shortcutInfo.readInt() != 0) {
          Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)shortcutInfo);
        } else {
          shortcutInfo = null;
        } 
        boolean bool1 = accept((Bundle)shortcutInfo);
        param1Parcel2.writeNoException();
        param1Parcel2.writeInt(bool1);
        return true;
      } 
      shortcutInfo.enforceInterface("android.content.pm.IPinItemRequest");
      boolean bool = isValid();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IPinItemRequest {
      public static IPinItemRequest sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IPinItemRequest";
      }
      
      public boolean isValid() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPinItemRequest");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IPinItemRequest.Stub.getDefaultImpl() != null) {
            bool1 = IPinItemRequest.Stub.getDefaultImpl().isValid();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean accept(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPinItemRequest");
          boolean bool1 = true;
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IPinItemRequest.Stub.getDefaultImpl() != null) {
            bool1 = IPinItemRequest.Stub.getDefaultImpl().accept(param2Bundle);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ShortcutInfo getShortcutInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ShortcutInfo shortcutInfo;
          parcel1.writeInterfaceToken("android.content.pm.IPinItemRequest");
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPinItemRequest.Stub.getDefaultImpl() != null) {
            shortcutInfo = IPinItemRequest.Stub.getDefaultImpl().getShortcutInfo();
            return shortcutInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            shortcutInfo = (ShortcutInfo)ShortcutInfo.CREATOR.createFromParcel(parcel2);
          } else {
            shortcutInfo = null;
          } 
          return shortcutInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public AppWidgetProviderInfo getAppWidgetProviderInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          AppWidgetProviderInfo appWidgetProviderInfo;
          parcel1.writeInterfaceToken("android.content.pm.IPinItemRequest");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPinItemRequest.Stub.getDefaultImpl() != null) {
            appWidgetProviderInfo = IPinItemRequest.Stub.getDefaultImpl().getAppWidgetProviderInfo();
            return appWidgetProviderInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            appWidgetProviderInfo = (AppWidgetProviderInfo)AppWidgetProviderInfo.CREATOR.createFromParcel(parcel2);
          } else {
            appWidgetProviderInfo = null;
          } 
          return appWidgetProviderInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getExtras() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.content.pm.IPinItemRequest");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPinItemRequest.Stub.getDefaultImpl() != null) {
            bundle = IPinItemRequest.Stub.getDefaultImpl().getExtras();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPinItemRequest param1IPinItemRequest) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPinItemRequest != null) {
          Proxy.sDefaultImpl = param1IPinItemRequest;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPinItemRequest getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
