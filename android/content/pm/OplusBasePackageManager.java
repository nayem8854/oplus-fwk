package android.content.pm;

import oplus.app.OplusCommonManager;

public abstract class OplusBasePackageManager extends OplusCommonManager {
  public OplusBasePackageManager() {
    super("package");
  }
}
