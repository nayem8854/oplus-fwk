package android.content.pm;

import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.dex.IArtManager;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public interface IPackageManager extends IInterface {
  boolean activitySupportsIntent(ComponentName paramComponentName, Intent paramIntent, String paramString) throws RemoteException;
  
  void addCrossProfileIntentFilter(IntentFilter paramIntentFilter, String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  boolean addPermission(PermissionInfo paramPermissionInfo) throws RemoteException;
  
  boolean addPermissionAsync(PermissionInfo paramPermissionInfo) throws RemoteException;
  
  void addPersistentPreferredActivity(IntentFilter paramIntentFilter, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void addPreferredActivity(IntentFilter paramIntentFilter, int paramInt1, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName, int paramInt2) throws RemoteException;
  
  boolean canForwardTo(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean canRequestPackageInstalls(String paramString, int paramInt) throws RemoteException;
  
  String[] canonicalToCurrentPackageNames(String[] paramArrayOfString) throws RemoteException;
  
  void checkPackageStartable(String paramString, int paramInt) throws RemoteException;
  
  int checkPermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  int checkSignatures(String paramString1, String paramString2) throws RemoteException;
  
  int checkUidPermission(String paramString, int paramInt) throws RemoteException;
  
  int checkUidSignatures(int paramInt1, int paramInt2) throws RemoteException;
  
  void clearApplicationProfileData(String paramString) throws RemoteException;
  
  void clearApplicationUserData(String paramString, IPackageDataObserver paramIPackageDataObserver, int paramInt) throws RemoteException;
  
  void clearCrossProfileIntentFilters(int paramInt, String paramString) throws RemoteException;
  
  void clearPackagePersistentPreferredActivities(String paramString, int paramInt) throws RemoteException;
  
  void clearPackagePreferredActivities(String paramString) throws RemoteException;
  
  boolean compileLayouts(String paramString) throws RemoteException;
  
  String[] currentToCanonicalPackageNames(String[] paramArrayOfString) throws RemoteException;
  
  void deleteApplicationCacheFiles(String paramString, IPackageDataObserver paramIPackageDataObserver) throws RemoteException;
  
  void deleteApplicationCacheFilesAsUser(String paramString, int paramInt, IPackageDataObserver paramIPackageDataObserver) throws RemoteException;
  
  void deleteExistingPackageAsUser(VersionedPackage paramVersionedPackage, IPackageDeleteObserver2 paramIPackageDeleteObserver2, int paramInt) throws RemoteException;
  
  void deletePackageAsUser(String paramString, int paramInt1, IPackageDeleteObserver paramIPackageDeleteObserver, int paramInt2, int paramInt3) throws RemoteException;
  
  void deletePackageVersioned(VersionedPackage paramVersionedPackage, IPackageDeleteObserver2 paramIPackageDeleteObserver2, int paramInt1, int paramInt2) throws RemoteException;
  
  void deletePreloadsFileCache() throws RemoteException;
  
  void dumpProfiles(String paramString) throws RemoteException;
  
  void enterSafeMode() throws RemoteException;
  
  void extendVerificationTimeout(int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  ResolveInfo findPersistentPreferredActivity(Intent paramIntent, int paramInt) throws RemoteException;
  
  void finishPackageInstall(int paramInt, boolean paramBoolean) throws RemoteException;
  
  void flushPackageRestrictionsAsUser(int paramInt) throws RemoteException;
  
  void forceDexOpt(String paramString) throws RemoteException;
  
  void freeStorage(String paramString, long paramLong, int paramInt, IntentSender paramIntentSender) throws RemoteException;
  
  void freeStorageAndNotify(String paramString, long paramLong, int paramInt, IPackageDataObserver paramIPackageDataObserver) throws RemoteException;
  
  ActivityInfo getActivityInfo(ComponentName paramComponentName, int paramInt1, int paramInt2) throws RemoteException;
  
  ParceledListSlice getAllIntentFilters(String paramString) throws RemoteException;
  
  List<String> getAllPackages() throws RemoteException;
  
  String[] getAppOpPermissionPackages(String paramString) throws RemoteException;
  
  String getAppPredictionServicePackageName() throws RemoteException;
  
  int getApplicationEnabledSetting(String paramString, int paramInt) throws RemoteException;
  
  boolean getApplicationHiddenSettingAsUser(String paramString, int paramInt) throws RemoteException;
  
  ApplicationInfo getApplicationInfo(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  IArtManager getArtManager() throws RemoteException;
  
  String getAttentionServicePackageName() throws RemoteException;
  
  boolean getBlockUninstallForUser(String paramString, int paramInt) throws RemoteException;
  
  ChangedPackages getChangedPackages(int paramInt1, int paramInt2) throws RemoteException;
  
  int getComponentEnabledSetting(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  String getContentCaptureServicePackageName() throws RemoteException;
  
  ParceledListSlice getDeclaredSharedLibraries(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  byte[] getDefaultAppsBackup(int paramInt) throws RemoteException;
  
  String getDefaultTextClassifierPackageName() throws RemoteException;
  
  int getFlagsForUid(int paramInt) throws RemoteException;
  
  CharSequence getHarmfulAppWarning(String paramString, int paramInt) throws RemoteException;
  
  ComponentName getHomeActivities(List<ResolveInfo> paramList) throws RemoteException;
  
  String getIncidentReportApproverPackageName() throws RemoteException;
  
  int getInstallLocation() throws RemoteException;
  
  int getInstallReason(String paramString, int paramInt) throws RemoteException;
  
  InstallSourceInfo getInstallSourceInfo(String paramString) throws RemoteException;
  
  ParceledListSlice getInstalledApplications(int paramInt1, int paramInt2) throws RemoteException;
  
  List<ModuleInfo> getInstalledModules(int paramInt) throws RemoteException;
  
  ParceledListSlice getInstalledPackages(int paramInt1, int paramInt2) throws RemoteException;
  
  String getInstallerPackageName(String paramString) throws RemoteException;
  
  String getInstantAppAndroidId(String paramString, int paramInt) throws RemoteException;
  
  byte[] getInstantAppCookie(String paramString, int paramInt) throws RemoteException;
  
  Bitmap getInstantAppIcon(String paramString, int paramInt) throws RemoteException;
  
  ComponentName getInstantAppInstallerComponent() throws RemoteException;
  
  ComponentName getInstantAppResolverComponent() throws RemoteException;
  
  ComponentName getInstantAppResolverSettingsComponent() throws RemoteException;
  
  ParceledListSlice getInstantApps(int paramInt) throws RemoteException;
  
  InstrumentationInfo getInstrumentationInfo(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  byte[] getIntentFilterVerificationBackup(int paramInt) throws RemoteException;
  
  ParceledListSlice getIntentFilterVerifications(String paramString) throws RemoteException;
  
  int getIntentVerificationStatus(String paramString, int paramInt) throws RemoteException;
  
  KeySet getKeySetByAlias(String paramString1, String paramString2) throws RemoteException;
  
  ResolveInfo getLastChosenActivity(Intent paramIntent, String paramString, int paramInt) throws RemoteException;
  
  List<String> getMimeGroup(String paramString1, String paramString2) throws RemoteException;
  
  ModuleInfo getModuleInfo(String paramString, int paramInt) throws RemoteException;
  
  int getMoveStatus(int paramInt) throws RemoteException;
  
  String getNameForUid(int paramInt) throws RemoteException;
  
  String[] getNamesForUids(int[] paramArrayOfint) throws RemoteException;
  
  int[] getPackageGids(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  PackageInfo getPackageInfo(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  PackageInfo getPackageInfoVersioned(VersionedPackage paramVersionedPackage, int paramInt1, int paramInt2) throws RemoteException;
  
  IPackageInstaller getPackageInstaller() throws RemoteException;
  
  void getPackageSizeInfo(String paramString, int paramInt, IPackageStatsObserver paramIPackageStatsObserver) throws RemoteException;
  
  int getPackageUid(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  String[] getPackagesForUid(int paramInt) throws RemoteException;
  
  ParceledListSlice getPackagesHoldingPermissions(String[] paramArrayOfString, int paramInt1, int paramInt2) throws RemoteException;
  
  String getPermissionControllerPackageName() throws RemoteException;
  
  PermissionGroupInfo getPermissionGroupInfo(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getPersistentApplications(int paramInt) throws RemoteException;
  
  int getPreferredActivities(List<IntentFilter> paramList, List<ComponentName> paramList1, String paramString) throws RemoteException;
  
  byte[] getPreferredActivityBackup(int paramInt) throws RemoteException;
  
  int getPrivateFlagsForUid(int paramInt) throws RemoteException;
  
  ProviderInfo getProviderInfo(ComponentName paramComponentName, int paramInt1, int paramInt2) throws RemoteException;
  
  ActivityInfo getReceiverInfo(ComponentName paramComponentName, int paramInt1, int paramInt2) throws RemoteException;
  
  int getRuntimePermissionsVersion(int paramInt) throws RemoteException;
  
  ServiceInfo getServiceInfo(ComponentName paramComponentName, int paramInt1, int paramInt2) throws RemoteException;
  
  String getServicesSystemSharedLibraryPackageName() throws RemoteException;
  
  String getSetupWizardPackageName() throws RemoteException;
  
  ParceledListSlice getSharedLibraries(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  String getSharedSystemSharedLibraryPackageName() throws RemoteException;
  
  KeySet getSigningKeySet(String paramString) throws RemoteException;
  
  Bundle getSuspendedPackageAppExtras(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice getSystemAvailableFeatures() throws RemoteException;
  
  String getSystemCaptionsServicePackageName() throws RemoteException;
  
  String[] getSystemSharedLibraryNames() throws RemoteException;
  
  String getSystemTextClassifierPackageName() throws RemoteException;
  
  int getUidForSharedUser(String paramString) throws RemoteException;
  
  String[] getUnsuspendablePackagesForUser(String[] paramArrayOfString, int paramInt) throws RemoteException;
  
  VerifierDeviceIdentity getVerifierDeviceIdentity() throws RemoteException;
  
  String getWellbeingPackageName() throws RemoteException;
  
  void grantRuntimePermission(String paramString1, String paramString2, int paramInt) throws RemoteException;
  
  boolean hasSigningCertificate(String paramString, byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  boolean hasSystemFeature(String paramString, int paramInt) throws RemoteException;
  
  boolean hasSystemUidErrors() throws RemoteException;
  
  boolean hasUidSigningCertificate(int paramInt1, byte[] paramArrayOfbyte, int paramInt2) throws RemoteException;
  
  int installExistingPackageAsUser(String paramString, int paramInt1, int paramInt2, int paramInt3, List<String> paramList) throws RemoteException;
  
  boolean isAutoRevokeWhitelisted(String paramString) throws RemoteException;
  
  boolean isDeviceUpgrading() throws RemoteException;
  
  boolean isFirstBoot() throws RemoteException;
  
  boolean isInstantApp(String paramString, int paramInt) throws RemoteException;
  
  boolean isOnlyCoreApps() throws RemoteException;
  
  boolean isPackageAvailable(String paramString, int paramInt) throws RemoteException;
  
  boolean isPackageDeviceAdminOnAnyUser(String paramString) throws RemoteException;
  
  boolean isPackageSignedByKeySet(String paramString, KeySet paramKeySet) throws RemoteException;
  
  boolean isPackageSignedByKeySetExactly(String paramString, KeySet paramKeySet) throws RemoteException;
  
  boolean isPackageStateProtected(String paramString, int paramInt) throws RemoteException;
  
  boolean isPackageSuspendedForUser(String paramString, int paramInt) throws RemoteException;
  
  boolean isProtectedBroadcast(String paramString) throws RemoteException;
  
  boolean isSafeMode() throws RemoteException;
  
  boolean isStorageLow() throws RemoteException;
  
  boolean isUidPrivileged(int paramInt) throws RemoteException;
  
  void logAppProcessStartIfNeeded(String paramString1, int paramInt1, String paramString2, String paramString3, int paramInt2) throws RemoteException;
  
  int movePackage(String paramString1, String paramString2) throws RemoteException;
  
  int movePrimaryStorage(String paramString) throws RemoteException;
  
  void notifyDexLoad(String paramString1, Map<String, String> paramMap, String paramString2) throws RemoteException;
  
  void notifyPackageUse(String paramString, int paramInt) throws RemoteException;
  
  void notifyPackagesReplacedReceived(String[] paramArrayOfString) throws RemoteException;
  
  void overrideLabelAndIcon(ComponentName paramComponentName, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  boolean performDexOptMode(String paramString1, boolean paramBoolean1, String paramString2, boolean paramBoolean2, boolean paramBoolean3, String paramString3) throws RemoteException;
  
  boolean performDexOptSecondary(String paramString1, String paramString2, boolean paramBoolean) throws RemoteException;
  
  void performFstrimIfNeeded() throws RemoteException;
  
  ParceledListSlice queryContentProviders(String paramString1, int paramInt1, int paramInt2, String paramString2) throws RemoteException;
  
  ParceledListSlice queryInstrumentation(String paramString, int paramInt) throws RemoteException;
  
  ParceledListSlice queryIntentActivities(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ParceledListSlice queryIntentActivityOptions(ComponentName paramComponentName, Intent[] paramArrayOfIntent, String[] paramArrayOfString, Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ParceledListSlice queryIntentContentProviders(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ParceledListSlice queryIntentReceivers(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ParceledListSlice queryIntentServices(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void querySyncProviders(List<String> paramList, List<ProviderInfo> paramList1) throws RemoteException;
  
  void reconcileSecondaryDexFiles(String paramString) throws RemoteException;
  
  void registerDexModule(String paramString1, String paramString2, boolean paramBoolean, IDexModuleRegisterCallback paramIDexModuleRegisterCallback) throws RemoteException;
  
  void registerMoveCallback(IPackageMoveObserver paramIPackageMoveObserver) throws RemoteException;
  
  void removePermission(String paramString) throws RemoteException;
  
  void replacePreferredActivity(IntentFilter paramIntentFilter, int paramInt1, ComponentName[] paramArrayOfComponentName, ComponentName paramComponentName, int paramInt2) throws RemoteException;
  
  void resetApplicationPreferences(int paramInt) throws RemoteException;
  
  ProviderInfo resolveContentProvider(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ResolveInfo resolveIntent(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  ResolveInfo resolveService(Intent paramIntent, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void restoreDefaultApps(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  void restoreIntentFilterVerification(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  void restoreLabelAndIcon(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void restorePreferredActivities(byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  boolean runBackgroundDexoptJob(List<String> paramList) throws RemoteException;
  
  void sendDeviceCustomizationReadyBroadcast() throws RemoteException;
  
  void setApplicationCategoryHint(String paramString1, int paramInt, String paramString2) throws RemoteException;
  
  void setApplicationEnabledSetting(String paramString1, int paramInt1, int paramInt2, int paramInt3, String paramString2) throws RemoteException;
  
  boolean setApplicationHiddenSettingAsUser(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  boolean setBlockUninstallForUser(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setComponentEnabledSetting(ComponentName paramComponentName, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  String[] setDistractingPackageRestrictionsAsUser(String[] paramArrayOfString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setHarmfulAppWarning(String paramString, CharSequence paramCharSequence, int paramInt) throws RemoteException;
  
  void setHomeActivity(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean setInstallLocation(int paramInt) throws RemoteException;
  
  void setInstallerPackageName(String paramString1, String paramString2) throws RemoteException;
  
  boolean setInstantAppCookie(String paramString, byte[] paramArrayOfbyte, int paramInt) throws RemoteException;
  
  void setLastChosenActivity(Intent paramIntent, String paramString, int paramInt1, IntentFilter paramIntentFilter, int paramInt2, ComponentName paramComponentName) throws RemoteException;
  
  void setMimeGroup(String paramString1, String paramString2, List<String> paramList) throws RemoteException;
  
  void setPackageStoppedState(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  String[] setPackagesSuspendedAsUser(String[] paramArrayOfString, boolean paramBoolean, PersistableBundle paramPersistableBundle1, PersistableBundle paramPersistableBundle2, SuspendDialogInfo paramSuspendDialogInfo, String paramString, int paramInt) throws RemoteException;
  
  boolean setRequiredForSystemUser(String paramString, boolean paramBoolean) throws RemoteException;
  
  void setRuntimePermissionsVersion(int paramInt1, int paramInt2) throws RemoteException;
  
  void setSystemAppHiddenUntilInstalled(String paramString, boolean paramBoolean) throws RemoteException;
  
  boolean setSystemAppInstallState(String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setUpdateAvailable(String paramString, boolean paramBoolean) throws RemoteException;
  
  void systemReady() throws RemoteException;
  
  void unregisterMoveCallback(IPackageMoveObserver paramIPackageMoveObserver) throws RemoteException;
  
  boolean updateIntentVerificationStatus(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void updatePackagesIfNeeded() throws RemoteException;
  
  void verifyIntentFilter(int paramInt1, int paramInt2, List<String> paramList) throws RemoteException;
  
  void verifyPendingInstall(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IPackageManager {
    public void checkPackageStartable(String param1String, int param1Int) throws RemoteException {}
    
    public boolean isPackageAvailable(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public PackageInfo getPackageInfo(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public PackageInfo getPackageInfoVersioned(VersionedPackage param1VersionedPackage, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public int getPackageUid(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public int[] getPackageGids(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public String[] currentToCanonicalPackageNames(String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public String[] canonicalToCurrentPackageNames(String[] param1ArrayOfString) throws RemoteException {
      return null;
    }
    
    public ApplicationInfo getApplicationInfo(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ActivityInfo getActivityInfo(ComponentName param1ComponentName, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean activitySupportsIntent(ComponentName param1ComponentName, Intent param1Intent, String param1String) throws RemoteException {
      return false;
    }
    
    public ActivityInfo getReceiverInfo(ComponentName param1ComponentName, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ServiceInfo getServiceInfo(ComponentName param1ComponentName, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ProviderInfo getProviderInfo(ComponentName param1ComponentName, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean isProtectedBroadcast(String param1String) throws RemoteException {
      return false;
    }
    
    public int checkSignatures(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int checkUidSignatures(int param1Int1, int param1Int2) throws RemoteException {
      return 0;
    }
    
    public List<String> getAllPackages() throws RemoteException {
      return null;
    }
    
    public String[] getPackagesForUid(int param1Int) throws RemoteException {
      return null;
    }
    
    public String getNameForUid(int param1Int) throws RemoteException {
      return null;
    }
    
    public String[] getNamesForUids(int[] param1ArrayOfint) throws RemoteException {
      return null;
    }
    
    public int getUidForSharedUser(String param1String) throws RemoteException {
      return 0;
    }
    
    public int getFlagsForUid(int param1Int) throws RemoteException {
      return 0;
    }
    
    public int getPrivateFlagsForUid(int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean isUidPrivileged(int param1Int) throws RemoteException {
      return false;
    }
    
    public ResolveInfo resolveIntent(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ResolveInfo findPersistentPreferredActivity(Intent param1Intent, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean canForwardTo(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public ParceledListSlice queryIntentActivities(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryIntentActivityOptions(ComponentName param1ComponentName, Intent[] param1ArrayOfIntent, String[] param1ArrayOfString, Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryIntentReceivers(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ResolveInfo resolveService(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryIntentServices(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryIntentContentProviders(Intent param1Intent, String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getInstalledPackages(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getPackagesHoldingPermissions(String[] param1ArrayOfString, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getInstalledApplications(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getPersistentApplications(int param1Int) throws RemoteException {
      return null;
    }
    
    public ProviderInfo resolveContentProvider(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public void querySyncProviders(List<String> param1List, List<ProviderInfo> param1List1) throws RemoteException {}
    
    public ParceledListSlice queryContentProviders(String param1String1, int param1Int1, int param1Int2, String param1String2) throws RemoteException {
      return null;
    }
    
    public InstrumentationInfo getInstrumentationInfo(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice queryInstrumentation(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void finishPackageInstall(int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setInstallerPackageName(String param1String1, String param1String2) throws RemoteException {}
    
    public void setApplicationCategoryHint(String param1String1, int param1Int, String param1String2) throws RemoteException {}
    
    public void deletePackageAsUser(String param1String, int param1Int1, IPackageDeleteObserver param1IPackageDeleteObserver, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void deletePackageVersioned(VersionedPackage param1VersionedPackage, IPackageDeleteObserver2 param1IPackageDeleteObserver2, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void deleteExistingPackageAsUser(VersionedPackage param1VersionedPackage, IPackageDeleteObserver2 param1IPackageDeleteObserver2, int param1Int) throws RemoteException {}
    
    public String getInstallerPackageName(String param1String) throws RemoteException {
      return null;
    }
    
    public InstallSourceInfo getInstallSourceInfo(String param1String) throws RemoteException {
      return null;
    }
    
    public void resetApplicationPreferences(int param1Int) throws RemoteException {}
    
    public ResolveInfo getLastChosenActivity(Intent param1Intent, String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void setLastChosenActivity(Intent param1Intent, String param1String, int param1Int1, IntentFilter param1IntentFilter, int param1Int2, ComponentName param1ComponentName) throws RemoteException {}
    
    public void addPreferredActivity(IntentFilter param1IntentFilter, int param1Int1, ComponentName[] param1ArrayOfComponentName, ComponentName param1ComponentName, int param1Int2) throws RemoteException {}
    
    public void replacePreferredActivity(IntentFilter param1IntentFilter, int param1Int1, ComponentName[] param1ArrayOfComponentName, ComponentName param1ComponentName, int param1Int2) throws RemoteException {}
    
    public void clearPackagePreferredActivities(String param1String) throws RemoteException {}
    
    public int getPreferredActivities(List<IntentFilter> param1List, List<ComponentName> param1List1, String param1String) throws RemoteException {
      return 0;
    }
    
    public void addPersistentPreferredActivity(IntentFilter param1IntentFilter, ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void clearPackagePersistentPreferredActivities(String param1String, int param1Int) throws RemoteException {}
    
    public void addCrossProfileIntentFilter(IntentFilter param1IntentFilter, String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void clearCrossProfileIntentFilters(int param1Int, String param1String) throws RemoteException {}
    
    public String[] setDistractingPackageRestrictionsAsUser(String[] param1ArrayOfString, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public String[] setPackagesSuspendedAsUser(String[] param1ArrayOfString, boolean param1Boolean, PersistableBundle param1PersistableBundle1, PersistableBundle param1PersistableBundle2, SuspendDialogInfo param1SuspendDialogInfo, String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public String[] getUnsuspendablePackagesForUser(String[] param1ArrayOfString, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isPackageSuspendedForUser(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public Bundle getSuspendedPackageAppExtras(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public byte[] getPreferredActivityBackup(int param1Int) throws RemoteException {
      return null;
    }
    
    public void restorePreferredActivities(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public byte[] getDefaultAppsBackup(int param1Int) throws RemoteException {
      return null;
    }
    
    public void restoreDefaultApps(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public byte[] getIntentFilterVerificationBackup(int param1Int) throws RemoteException {
      return null;
    }
    
    public void restoreIntentFilterVerification(byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {}
    
    public ComponentName getHomeActivities(List<ResolveInfo> param1List) throws RemoteException {
      return null;
    }
    
    public void setHomeActivity(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void overrideLabelAndIcon(ComponentName param1ComponentName, String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void restoreLabelAndIcon(ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void setComponentEnabledSetting(ComponentName param1ComponentName, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public int getComponentEnabledSetting(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setApplicationEnabledSetting(String param1String1, int param1Int1, int param1Int2, int param1Int3, String param1String2) throws RemoteException {}
    
    public int getApplicationEnabledSetting(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void logAppProcessStartIfNeeded(String param1String1, int param1Int1, String param1String2, String param1String3, int param1Int2) throws RemoteException {}
    
    public void flushPackageRestrictionsAsUser(int param1Int) throws RemoteException {}
    
    public void setPackageStoppedState(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void freeStorageAndNotify(String param1String, long param1Long, int param1Int, IPackageDataObserver param1IPackageDataObserver) throws RemoteException {}
    
    public void freeStorage(String param1String, long param1Long, int param1Int, IntentSender param1IntentSender) throws RemoteException {}
    
    public void deleteApplicationCacheFiles(String param1String, IPackageDataObserver param1IPackageDataObserver) throws RemoteException {}
    
    public void deleteApplicationCacheFilesAsUser(String param1String, int param1Int, IPackageDataObserver param1IPackageDataObserver) throws RemoteException {}
    
    public void clearApplicationUserData(String param1String, IPackageDataObserver param1IPackageDataObserver, int param1Int) throws RemoteException {}
    
    public void clearApplicationProfileData(String param1String) throws RemoteException {}
    
    public void getPackageSizeInfo(String param1String, int param1Int, IPackageStatsObserver param1IPackageStatsObserver) throws RemoteException {}
    
    public String[] getSystemSharedLibraryNames() throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getSystemAvailableFeatures() throws RemoteException {
      return null;
    }
    
    public boolean hasSystemFeature(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void enterSafeMode() throws RemoteException {}
    
    public boolean isSafeMode() throws RemoteException {
      return false;
    }
    
    public void systemReady() throws RemoteException {}
    
    public boolean hasSystemUidErrors() throws RemoteException {
      return false;
    }
    
    public void performFstrimIfNeeded() throws RemoteException {}
    
    public void updatePackagesIfNeeded() throws RemoteException {}
    
    public void notifyPackageUse(String param1String, int param1Int) throws RemoteException {}
    
    public void notifyDexLoad(String param1String1, Map<String, String> param1Map, String param1String2) throws RemoteException {}
    
    public void registerDexModule(String param1String1, String param1String2, boolean param1Boolean, IDexModuleRegisterCallback param1IDexModuleRegisterCallback) throws RemoteException {}
    
    public boolean performDexOptMode(String param1String1, boolean param1Boolean1, String param1String2, boolean param1Boolean2, boolean param1Boolean3, String param1String3) throws RemoteException {
      return false;
    }
    
    public boolean performDexOptSecondary(String param1String1, String param1String2, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public boolean compileLayouts(String param1String) throws RemoteException {
      return false;
    }
    
    public void dumpProfiles(String param1String) throws RemoteException {}
    
    public void forceDexOpt(String param1String) throws RemoteException {}
    
    public boolean runBackgroundDexoptJob(List<String> param1List) throws RemoteException {
      return false;
    }
    
    public void reconcileSecondaryDexFiles(String param1String) throws RemoteException {}
    
    public int getMoveStatus(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void registerMoveCallback(IPackageMoveObserver param1IPackageMoveObserver) throws RemoteException {}
    
    public void unregisterMoveCallback(IPackageMoveObserver param1IPackageMoveObserver) throws RemoteException {}
    
    public int movePackage(String param1String1, String param1String2) throws RemoteException {
      return 0;
    }
    
    public int movePrimaryStorage(String param1String) throws RemoteException {
      return 0;
    }
    
    public boolean setInstallLocation(int param1Int) throws RemoteException {
      return false;
    }
    
    public int getInstallLocation() throws RemoteException {
      return 0;
    }
    
    public int installExistingPackageAsUser(String param1String, int param1Int1, int param1Int2, int param1Int3, List<String> param1List) throws RemoteException {
      return 0;
    }
    
    public void verifyPendingInstall(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void extendVerificationTimeout(int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void verifyIntentFilter(int param1Int1, int param1Int2, List<String> param1List) throws RemoteException {}
    
    public int getIntentVerificationStatus(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public boolean updateIntentVerificationStatus(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public ParceledListSlice getIntentFilterVerifications(String param1String) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getAllIntentFilters(String param1String) throws RemoteException {
      return null;
    }
    
    public VerifierDeviceIdentity getVerifierDeviceIdentity() throws RemoteException {
      return null;
    }
    
    public boolean isFirstBoot() throws RemoteException {
      return false;
    }
    
    public boolean isOnlyCoreApps() throws RemoteException {
      return false;
    }
    
    public boolean isDeviceUpgrading() throws RemoteException {
      return false;
    }
    
    public boolean isStorageLow() throws RemoteException {
      return false;
    }
    
    public boolean setApplicationHiddenSettingAsUser(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean getApplicationHiddenSettingAsUser(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setSystemAppHiddenUntilInstalled(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public boolean setSystemAppInstallState(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public IPackageInstaller getPackageInstaller() throws RemoteException {
      return null;
    }
    
    public boolean setBlockUninstallForUser(String param1String, boolean param1Boolean, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean getBlockUninstallForUser(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public KeySet getKeySetByAlias(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public KeySet getSigningKeySet(String param1String) throws RemoteException {
      return null;
    }
    
    public boolean isPackageSignedByKeySet(String param1String, KeySet param1KeySet) throws RemoteException {
      return false;
    }
    
    public boolean isPackageSignedByKeySetExactly(String param1String, KeySet param1KeySet) throws RemoteException {
      return false;
    }
    
    public String getPermissionControllerPackageName() throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getInstantApps(int param1Int) throws RemoteException {
      return null;
    }
    
    public byte[] getInstantAppCookie(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean setInstantAppCookie(String param1String, byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {
      return false;
    }
    
    public Bitmap getInstantAppIcon(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isInstantApp(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean setRequiredForSystemUser(String param1String, boolean param1Boolean) throws RemoteException {
      return false;
    }
    
    public void setUpdateAvailable(String param1String, boolean param1Boolean) throws RemoteException {}
    
    public String getServicesSystemSharedLibraryPackageName() throws RemoteException {
      return null;
    }
    
    public String getSharedSystemSharedLibraryPackageName() throws RemoteException {
      return null;
    }
    
    public ChangedPackages getChangedPackages(int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean isPackageDeviceAdminOnAnyUser(String param1String) throws RemoteException {
      return false;
    }
    
    public int getInstallReason(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public ParceledListSlice getSharedLibraries(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getDeclaredSharedLibraries(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return null;
    }
    
    public boolean canRequestPackageInstalls(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void deletePreloadsFileCache() throws RemoteException {}
    
    public ComponentName getInstantAppResolverComponent() throws RemoteException {
      return null;
    }
    
    public ComponentName getInstantAppResolverSettingsComponent() throws RemoteException {
      return null;
    }
    
    public ComponentName getInstantAppInstallerComponent() throws RemoteException {
      return null;
    }
    
    public String getInstantAppAndroidId(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public IArtManager getArtManager() throws RemoteException {
      return null;
    }
    
    public void setHarmfulAppWarning(String param1String, CharSequence param1CharSequence, int param1Int) throws RemoteException {}
    
    public CharSequence getHarmfulAppWarning(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean hasSigningCertificate(String param1String, byte[] param1ArrayOfbyte, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean hasUidSigningCertificate(int param1Int1, byte[] param1ArrayOfbyte, int param1Int2) throws RemoteException {
      return false;
    }
    
    public String getDefaultTextClassifierPackageName() throws RemoteException {
      return null;
    }
    
    public String getSystemTextClassifierPackageName() throws RemoteException {
      return null;
    }
    
    public String getAttentionServicePackageName() throws RemoteException {
      return null;
    }
    
    public String getWellbeingPackageName() throws RemoteException {
      return null;
    }
    
    public String getAppPredictionServicePackageName() throws RemoteException {
      return null;
    }
    
    public String getSystemCaptionsServicePackageName() throws RemoteException {
      return null;
    }
    
    public String getSetupWizardPackageName() throws RemoteException {
      return null;
    }
    
    public String getIncidentReportApproverPackageName() throws RemoteException {
      return null;
    }
    
    public String getContentCaptureServicePackageName() throws RemoteException {
      return null;
    }
    
    public boolean isPackageStateProtected(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void sendDeviceCustomizationReadyBroadcast() throws RemoteException {}
    
    public List<ModuleInfo> getInstalledModules(int param1Int) throws RemoteException {
      return null;
    }
    
    public ModuleInfo getModuleInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public int getRuntimePermissionsVersion(int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setRuntimePermissionsVersion(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void notifyPackagesReplacedReceived(String[] param1ArrayOfString) throws RemoteException {}
    
    public String[] getAppOpPermissionPackages(String param1String) throws RemoteException {
      return null;
    }
    
    public PermissionGroupInfo getPermissionGroupInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean addPermission(PermissionInfo param1PermissionInfo) throws RemoteException {
      return false;
    }
    
    public boolean addPermissionAsync(PermissionInfo param1PermissionInfo) throws RemoteException {
      return false;
    }
    
    public void removePermission(String param1String) throws RemoteException {}
    
    public int checkPermission(String param1String1, String param1String2, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void grantRuntimePermission(String param1String1, String param1String2, int param1Int) throws RemoteException {}
    
    public int checkUidPermission(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setMimeGroup(String param1String1, String param1String2, List<String> param1List) throws RemoteException {}
    
    public List<String> getMimeGroup(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public boolean isAutoRevokeWhitelisted(String param1String) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPackageManager {
    private static final String DESCRIPTOR = "android.content.pm.IPackageManager";
    
    static final int TRANSACTION_activitySupportsIntent = 11;
    
    static final int TRANSACTION_addCrossProfileIntentFilter = 61;
    
    static final int TRANSACTION_addPermission = 186;
    
    static final int TRANSACTION_addPermissionAsync = 187;
    
    static final int TRANSACTION_addPersistentPreferredActivity = 59;
    
    static final int TRANSACTION_addPreferredActivity = 55;
    
    static final int TRANSACTION_canForwardTo = 28;
    
    static final int TRANSACTION_canRequestPackageInstalls = 157;
    
    static final int TRANSACTION_canonicalToCurrentPackageNames = 8;
    
    static final int TRANSACTION_checkPackageStartable = 1;
    
    static final int TRANSACTION_checkPermission = 189;
    
    static final int TRANSACTION_checkSignatures = 16;
    
    static final int TRANSACTION_checkUidPermission = 191;
    
    static final int TRANSACTION_checkUidSignatures = 17;
    
    static final int TRANSACTION_clearApplicationProfileData = 90;
    
    static final int TRANSACTION_clearApplicationUserData = 89;
    
    static final int TRANSACTION_clearCrossProfileIntentFilters = 62;
    
    static final int TRANSACTION_clearPackagePersistentPreferredActivities = 60;
    
    static final int TRANSACTION_clearPackagePreferredActivities = 57;
    
    static final int TRANSACTION_compileLayouts = 106;
    
    static final int TRANSACTION_currentToCanonicalPackageNames = 7;
    
    static final int TRANSACTION_deleteApplicationCacheFiles = 87;
    
    static final int TRANSACTION_deleteApplicationCacheFilesAsUser = 88;
    
    static final int TRANSACTION_deleteExistingPackageAsUser = 49;
    
    static final int TRANSACTION_deletePackageAsUser = 47;
    
    static final int TRANSACTION_deletePackageVersioned = 48;
    
    static final int TRANSACTION_deletePreloadsFileCache = 158;
    
    static final int TRANSACTION_dumpProfiles = 107;
    
    static final int TRANSACTION_enterSafeMode = 95;
    
    static final int TRANSACTION_extendVerificationTimeout = 120;
    
    static final int TRANSACTION_findPersistentPreferredActivity = 27;
    
    static final int TRANSACTION_finishPackageInstall = 44;
    
    static final int TRANSACTION_flushPackageRestrictionsAsUser = 83;
    
    static final int TRANSACTION_forceDexOpt = 108;
    
    static final int TRANSACTION_freeStorage = 86;
    
    static final int TRANSACTION_freeStorageAndNotify = 85;
    
    static final int TRANSACTION_getActivityInfo = 10;
    
    static final int TRANSACTION_getAllIntentFilters = 125;
    
    static final int TRANSACTION_getAllPackages = 18;
    
    static final int TRANSACTION_getAppOpPermissionPackages = 184;
    
    static final int TRANSACTION_getAppPredictionServicePackageName = 172;
    
    static final int TRANSACTION_getApplicationEnabledSetting = 81;
    
    static final int TRANSACTION_getApplicationHiddenSettingAsUser = 132;
    
    static final int TRANSACTION_getApplicationInfo = 9;
    
    static final int TRANSACTION_getArtManager = 163;
    
    static final int TRANSACTION_getAttentionServicePackageName = 170;
    
    static final int TRANSACTION_getBlockUninstallForUser = 137;
    
    static final int TRANSACTION_getChangedPackages = 152;
    
    static final int TRANSACTION_getComponentEnabledSetting = 79;
    
    static final int TRANSACTION_getContentCaptureServicePackageName = 176;
    
    static final int TRANSACTION_getDeclaredSharedLibraries = 156;
    
    static final int TRANSACTION_getDefaultAppsBackup = 70;
    
    static final int TRANSACTION_getDefaultTextClassifierPackageName = 168;
    
    static final int TRANSACTION_getFlagsForUid = 23;
    
    static final int TRANSACTION_getHarmfulAppWarning = 165;
    
    static final int TRANSACTION_getHomeActivities = 74;
    
    static final int TRANSACTION_getIncidentReportApproverPackageName = 175;
    
    static final int TRANSACTION_getInstallLocation = 117;
    
    static final int TRANSACTION_getInstallReason = 154;
    
    static final int TRANSACTION_getInstallSourceInfo = 51;
    
    static final int TRANSACTION_getInstalledApplications = 37;
    
    static final int TRANSACTION_getInstalledModules = 179;
    
    static final int TRANSACTION_getInstalledPackages = 35;
    
    static final int TRANSACTION_getInstallerPackageName = 50;
    
    static final int TRANSACTION_getInstantAppAndroidId = 162;
    
    static final int TRANSACTION_getInstantAppCookie = 144;
    
    static final int TRANSACTION_getInstantAppIcon = 146;
    
    static final int TRANSACTION_getInstantAppInstallerComponent = 161;
    
    static final int TRANSACTION_getInstantAppResolverComponent = 159;
    
    static final int TRANSACTION_getInstantAppResolverSettingsComponent = 160;
    
    static final int TRANSACTION_getInstantApps = 143;
    
    static final int TRANSACTION_getInstrumentationInfo = 42;
    
    static final int TRANSACTION_getIntentFilterVerificationBackup = 72;
    
    static final int TRANSACTION_getIntentFilterVerifications = 124;
    
    static final int TRANSACTION_getIntentVerificationStatus = 122;
    
    static final int TRANSACTION_getKeySetByAlias = 138;
    
    static final int TRANSACTION_getLastChosenActivity = 53;
    
    static final int TRANSACTION_getMimeGroup = 193;
    
    static final int TRANSACTION_getModuleInfo = 180;
    
    static final int TRANSACTION_getMoveStatus = 111;
    
    static final int TRANSACTION_getNameForUid = 20;
    
    static final int TRANSACTION_getNamesForUids = 21;
    
    static final int TRANSACTION_getPackageGids = 6;
    
    static final int TRANSACTION_getPackageInfo = 3;
    
    static final int TRANSACTION_getPackageInfoVersioned = 4;
    
    static final int TRANSACTION_getPackageInstaller = 135;
    
    static final int TRANSACTION_getPackageSizeInfo = 91;
    
    static final int TRANSACTION_getPackageUid = 5;
    
    static final int TRANSACTION_getPackagesForUid = 19;
    
    static final int TRANSACTION_getPackagesHoldingPermissions = 36;
    
    static final int TRANSACTION_getPermissionControllerPackageName = 142;
    
    static final int TRANSACTION_getPermissionGroupInfo = 185;
    
    static final int TRANSACTION_getPersistentApplications = 38;
    
    static final int TRANSACTION_getPreferredActivities = 58;
    
    static final int TRANSACTION_getPreferredActivityBackup = 68;
    
    static final int TRANSACTION_getPrivateFlagsForUid = 24;
    
    static final int TRANSACTION_getProviderInfo = 14;
    
    static final int TRANSACTION_getReceiverInfo = 12;
    
    static final int TRANSACTION_getRuntimePermissionsVersion = 181;
    
    static final int TRANSACTION_getServiceInfo = 13;
    
    static final int TRANSACTION_getServicesSystemSharedLibraryPackageName = 150;
    
    static final int TRANSACTION_getSetupWizardPackageName = 174;
    
    static final int TRANSACTION_getSharedLibraries = 155;
    
    static final int TRANSACTION_getSharedSystemSharedLibraryPackageName = 151;
    
    static final int TRANSACTION_getSigningKeySet = 139;
    
    static final int TRANSACTION_getSuspendedPackageAppExtras = 67;
    
    static final int TRANSACTION_getSystemAvailableFeatures = 93;
    
    static final int TRANSACTION_getSystemCaptionsServicePackageName = 173;
    
    static final int TRANSACTION_getSystemSharedLibraryNames = 92;
    
    static final int TRANSACTION_getSystemTextClassifierPackageName = 169;
    
    static final int TRANSACTION_getUidForSharedUser = 22;
    
    static final int TRANSACTION_getUnsuspendablePackagesForUser = 65;
    
    static final int TRANSACTION_getVerifierDeviceIdentity = 126;
    
    static final int TRANSACTION_getWellbeingPackageName = 171;
    
    static final int TRANSACTION_grantRuntimePermission = 190;
    
    static final int TRANSACTION_hasSigningCertificate = 166;
    
    static final int TRANSACTION_hasSystemFeature = 94;
    
    static final int TRANSACTION_hasSystemUidErrors = 98;
    
    static final int TRANSACTION_hasUidSigningCertificate = 167;
    
    static final int TRANSACTION_installExistingPackageAsUser = 118;
    
    static final int TRANSACTION_isAutoRevokeWhitelisted = 194;
    
    static final int TRANSACTION_isDeviceUpgrading = 129;
    
    static final int TRANSACTION_isFirstBoot = 127;
    
    static final int TRANSACTION_isInstantApp = 147;
    
    static final int TRANSACTION_isOnlyCoreApps = 128;
    
    static final int TRANSACTION_isPackageAvailable = 2;
    
    static final int TRANSACTION_isPackageDeviceAdminOnAnyUser = 153;
    
    static final int TRANSACTION_isPackageSignedByKeySet = 140;
    
    static final int TRANSACTION_isPackageSignedByKeySetExactly = 141;
    
    static final int TRANSACTION_isPackageStateProtected = 177;
    
    static final int TRANSACTION_isPackageSuspendedForUser = 66;
    
    static final int TRANSACTION_isProtectedBroadcast = 15;
    
    static final int TRANSACTION_isSafeMode = 96;
    
    static final int TRANSACTION_isStorageLow = 130;
    
    static final int TRANSACTION_isUidPrivileged = 25;
    
    static final int TRANSACTION_logAppProcessStartIfNeeded = 82;
    
    static final int TRANSACTION_movePackage = 114;
    
    static final int TRANSACTION_movePrimaryStorage = 115;
    
    static final int TRANSACTION_notifyDexLoad = 102;
    
    static final int TRANSACTION_notifyPackageUse = 101;
    
    static final int TRANSACTION_notifyPackagesReplacedReceived = 183;
    
    static final int TRANSACTION_overrideLabelAndIcon = 76;
    
    static final int TRANSACTION_performDexOptMode = 104;
    
    static final int TRANSACTION_performDexOptSecondary = 105;
    
    static final int TRANSACTION_performFstrimIfNeeded = 99;
    
    static final int TRANSACTION_queryContentProviders = 41;
    
    static final int TRANSACTION_queryInstrumentation = 43;
    
    static final int TRANSACTION_queryIntentActivities = 29;
    
    static final int TRANSACTION_queryIntentActivityOptions = 30;
    
    static final int TRANSACTION_queryIntentContentProviders = 34;
    
    static final int TRANSACTION_queryIntentReceivers = 31;
    
    static final int TRANSACTION_queryIntentServices = 33;
    
    static final int TRANSACTION_querySyncProviders = 40;
    
    static final int TRANSACTION_reconcileSecondaryDexFiles = 110;
    
    static final int TRANSACTION_registerDexModule = 103;
    
    static final int TRANSACTION_registerMoveCallback = 112;
    
    static final int TRANSACTION_removePermission = 188;
    
    static final int TRANSACTION_replacePreferredActivity = 56;
    
    static final int TRANSACTION_resetApplicationPreferences = 52;
    
    static final int TRANSACTION_resolveContentProvider = 39;
    
    static final int TRANSACTION_resolveIntent = 26;
    
    static final int TRANSACTION_resolveService = 32;
    
    static final int TRANSACTION_restoreDefaultApps = 71;
    
    static final int TRANSACTION_restoreIntentFilterVerification = 73;
    
    static final int TRANSACTION_restoreLabelAndIcon = 77;
    
    static final int TRANSACTION_restorePreferredActivities = 69;
    
    static final int TRANSACTION_runBackgroundDexoptJob = 109;
    
    static final int TRANSACTION_sendDeviceCustomizationReadyBroadcast = 178;
    
    static final int TRANSACTION_setApplicationCategoryHint = 46;
    
    static final int TRANSACTION_setApplicationEnabledSetting = 80;
    
    static final int TRANSACTION_setApplicationHiddenSettingAsUser = 131;
    
    static final int TRANSACTION_setBlockUninstallForUser = 136;
    
    static final int TRANSACTION_setComponentEnabledSetting = 78;
    
    static final int TRANSACTION_setDistractingPackageRestrictionsAsUser = 63;
    
    static final int TRANSACTION_setHarmfulAppWarning = 164;
    
    static final int TRANSACTION_setHomeActivity = 75;
    
    static final int TRANSACTION_setInstallLocation = 116;
    
    static final int TRANSACTION_setInstallerPackageName = 45;
    
    static final int TRANSACTION_setInstantAppCookie = 145;
    
    static final int TRANSACTION_setLastChosenActivity = 54;
    
    static final int TRANSACTION_setMimeGroup = 192;
    
    static final int TRANSACTION_setPackageStoppedState = 84;
    
    static final int TRANSACTION_setPackagesSuspendedAsUser = 64;
    
    static final int TRANSACTION_setRequiredForSystemUser = 148;
    
    static final int TRANSACTION_setRuntimePermissionsVersion = 182;
    
    static final int TRANSACTION_setSystemAppHiddenUntilInstalled = 133;
    
    static final int TRANSACTION_setSystemAppInstallState = 134;
    
    static final int TRANSACTION_setUpdateAvailable = 149;
    
    static final int TRANSACTION_systemReady = 97;
    
    static final int TRANSACTION_unregisterMoveCallback = 113;
    
    static final int TRANSACTION_updateIntentVerificationStatus = 123;
    
    static final int TRANSACTION_updatePackagesIfNeeded = 100;
    
    static final int TRANSACTION_verifyIntentFilter = 121;
    
    static final int TRANSACTION_verifyPendingInstall = 119;
    
    public Stub() {
      attachInterface(this, "android.content.pm.IPackageManager");
    }
    
    public static IPackageManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.pm.IPackageManager");
      if (iInterface != null && iInterface instanceof IPackageManager)
        return (IPackageManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 194:
          return "isAutoRevokeWhitelisted";
        case 193:
          return "getMimeGroup";
        case 192:
          return "setMimeGroup";
        case 191:
          return "checkUidPermission";
        case 190:
          return "grantRuntimePermission";
        case 189:
          return "checkPermission";
        case 188:
          return "removePermission";
        case 187:
          return "addPermissionAsync";
        case 186:
          return "addPermission";
        case 185:
          return "getPermissionGroupInfo";
        case 184:
          return "getAppOpPermissionPackages";
        case 183:
          return "notifyPackagesReplacedReceived";
        case 182:
          return "setRuntimePermissionsVersion";
        case 181:
          return "getRuntimePermissionsVersion";
        case 180:
          return "getModuleInfo";
        case 179:
          return "getInstalledModules";
        case 178:
          return "sendDeviceCustomizationReadyBroadcast";
        case 177:
          return "isPackageStateProtected";
        case 176:
          return "getContentCaptureServicePackageName";
        case 175:
          return "getIncidentReportApproverPackageName";
        case 174:
          return "getSetupWizardPackageName";
        case 173:
          return "getSystemCaptionsServicePackageName";
        case 172:
          return "getAppPredictionServicePackageName";
        case 171:
          return "getWellbeingPackageName";
        case 170:
          return "getAttentionServicePackageName";
        case 169:
          return "getSystemTextClassifierPackageName";
        case 168:
          return "getDefaultTextClassifierPackageName";
        case 167:
          return "hasUidSigningCertificate";
        case 166:
          return "hasSigningCertificate";
        case 165:
          return "getHarmfulAppWarning";
        case 164:
          return "setHarmfulAppWarning";
        case 163:
          return "getArtManager";
        case 162:
          return "getInstantAppAndroidId";
        case 161:
          return "getInstantAppInstallerComponent";
        case 160:
          return "getInstantAppResolverSettingsComponent";
        case 159:
          return "getInstantAppResolverComponent";
        case 158:
          return "deletePreloadsFileCache";
        case 157:
          return "canRequestPackageInstalls";
        case 156:
          return "getDeclaredSharedLibraries";
        case 155:
          return "getSharedLibraries";
        case 154:
          return "getInstallReason";
        case 153:
          return "isPackageDeviceAdminOnAnyUser";
        case 152:
          return "getChangedPackages";
        case 151:
          return "getSharedSystemSharedLibraryPackageName";
        case 150:
          return "getServicesSystemSharedLibraryPackageName";
        case 149:
          return "setUpdateAvailable";
        case 148:
          return "setRequiredForSystemUser";
        case 147:
          return "isInstantApp";
        case 146:
          return "getInstantAppIcon";
        case 145:
          return "setInstantAppCookie";
        case 144:
          return "getInstantAppCookie";
        case 143:
          return "getInstantApps";
        case 142:
          return "getPermissionControllerPackageName";
        case 141:
          return "isPackageSignedByKeySetExactly";
        case 140:
          return "isPackageSignedByKeySet";
        case 139:
          return "getSigningKeySet";
        case 138:
          return "getKeySetByAlias";
        case 137:
          return "getBlockUninstallForUser";
        case 136:
          return "setBlockUninstallForUser";
        case 135:
          return "getPackageInstaller";
        case 134:
          return "setSystemAppInstallState";
        case 133:
          return "setSystemAppHiddenUntilInstalled";
        case 132:
          return "getApplicationHiddenSettingAsUser";
        case 131:
          return "setApplicationHiddenSettingAsUser";
        case 130:
          return "isStorageLow";
        case 129:
          return "isDeviceUpgrading";
        case 128:
          return "isOnlyCoreApps";
        case 127:
          return "isFirstBoot";
        case 126:
          return "getVerifierDeviceIdentity";
        case 125:
          return "getAllIntentFilters";
        case 124:
          return "getIntentFilterVerifications";
        case 123:
          return "updateIntentVerificationStatus";
        case 122:
          return "getIntentVerificationStatus";
        case 121:
          return "verifyIntentFilter";
        case 120:
          return "extendVerificationTimeout";
        case 119:
          return "verifyPendingInstall";
        case 118:
          return "installExistingPackageAsUser";
        case 117:
          return "getInstallLocation";
        case 116:
          return "setInstallLocation";
        case 115:
          return "movePrimaryStorage";
        case 114:
          return "movePackage";
        case 113:
          return "unregisterMoveCallback";
        case 112:
          return "registerMoveCallback";
        case 111:
          return "getMoveStatus";
        case 110:
          return "reconcileSecondaryDexFiles";
        case 109:
          return "runBackgroundDexoptJob";
        case 108:
          return "forceDexOpt";
        case 107:
          return "dumpProfiles";
        case 106:
          return "compileLayouts";
        case 105:
          return "performDexOptSecondary";
        case 104:
          return "performDexOptMode";
        case 103:
          return "registerDexModule";
        case 102:
          return "notifyDexLoad";
        case 101:
          return "notifyPackageUse";
        case 100:
          return "updatePackagesIfNeeded";
        case 99:
          return "performFstrimIfNeeded";
        case 98:
          return "hasSystemUidErrors";
        case 97:
          return "systemReady";
        case 96:
          return "isSafeMode";
        case 95:
          return "enterSafeMode";
        case 94:
          return "hasSystemFeature";
        case 93:
          return "getSystemAvailableFeatures";
        case 92:
          return "getSystemSharedLibraryNames";
        case 91:
          return "getPackageSizeInfo";
        case 90:
          return "clearApplicationProfileData";
        case 89:
          return "clearApplicationUserData";
        case 88:
          return "deleteApplicationCacheFilesAsUser";
        case 87:
          return "deleteApplicationCacheFiles";
        case 86:
          return "freeStorage";
        case 85:
          return "freeStorageAndNotify";
        case 84:
          return "setPackageStoppedState";
        case 83:
          return "flushPackageRestrictionsAsUser";
        case 82:
          return "logAppProcessStartIfNeeded";
        case 81:
          return "getApplicationEnabledSetting";
        case 80:
          return "setApplicationEnabledSetting";
        case 79:
          return "getComponentEnabledSetting";
        case 78:
          return "setComponentEnabledSetting";
        case 77:
          return "restoreLabelAndIcon";
        case 76:
          return "overrideLabelAndIcon";
        case 75:
          return "setHomeActivity";
        case 74:
          return "getHomeActivities";
        case 73:
          return "restoreIntentFilterVerification";
        case 72:
          return "getIntentFilterVerificationBackup";
        case 71:
          return "restoreDefaultApps";
        case 70:
          return "getDefaultAppsBackup";
        case 69:
          return "restorePreferredActivities";
        case 68:
          return "getPreferredActivityBackup";
        case 67:
          return "getSuspendedPackageAppExtras";
        case 66:
          return "isPackageSuspendedForUser";
        case 65:
          return "getUnsuspendablePackagesForUser";
        case 64:
          return "setPackagesSuspendedAsUser";
        case 63:
          return "setDistractingPackageRestrictionsAsUser";
        case 62:
          return "clearCrossProfileIntentFilters";
        case 61:
          return "addCrossProfileIntentFilter";
        case 60:
          return "clearPackagePersistentPreferredActivities";
        case 59:
          return "addPersistentPreferredActivity";
        case 58:
          return "getPreferredActivities";
        case 57:
          return "clearPackagePreferredActivities";
        case 56:
          return "replacePreferredActivity";
        case 55:
          return "addPreferredActivity";
        case 54:
          return "setLastChosenActivity";
        case 53:
          return "getLastChosenActivity";
        case 52:
          return "resetApplicationPreferences";
        case 51:
          return "getInstallSourceInfo";
        case 50:
          return "getInstallerPackageName";
        case 49:
          return "deleteExistingPackageAsUser";
        case 48:
          return "deletePackageVersioned";
        case 47:
          return "deletePackageAsUser";
        case 46:
          return "setApplicationCategoryHint";
        case 45:
          return "setInstallerPackageName";
        case 44:
          return "finishPackageInstall";
        case 43:
          return "queryInstrumentation";
        case 42:
          return "getInstrumentationInfo";
        case 41:
          return "queryContentProviders";
        case 40:
          return "querySyncProviders";
        case 39:
          return "resolveContentProvider";
        case 38:
          return "getPersistentApplications";
        case 37:
          return "getInstalledApplications";
        case 36:
          return "getPackagesHoldingPermissions";
        case 35:
          return "getInstalledPackages";
        case 34:
          return "queryIntentContentProviders";
        case 33:
          return "queryIntentServices";
        case 32:
          return "resolveService";
        case 31:
          return "queryIntentReceivers";
        case 30:
          return "queryIntentActivityOptions";
        case 29:
          return "queryIntentActivities";
        case 28:
          return "canForwardTo";
        case 27:
          return "findPersistentPreferredActivity";
        case 26:
          return "resolveIntent";
        case 25:
          return "isUidPrivileged";
        case 24:
          return "getPrivateFlagsForUid";
        case 23:
          return "getFlagsForUid";
        case 22:
          return "getUidForSharedUser";
        case 21:
          return "getNamesForUids";
        case 20:
          return "getNameForUid";
        case 19:
          return "getPackagesForUid";
        case 18:
          return "getAllPackages";
        case 17:
          return "checkUidSignatures";
        case 16:
          return "checkSignatures";
        case 15:
          return "isProtectedBroadcast";
        case 14:
          return "getProviderInfo";
        case 13:
          return "getServiceInfo";
        case 12:
          return "getReceiverInfo";
        case 11:
          return "activitySupportsIntent";
        case 10:
          return "getActivityInfo";
        case 9:
          return "getApplicationInfo";
        case 8:
          return "canonicalToCurrentPackageNames";
        case 7:
          return "currentToCanonicalPackageNames";
        case 6:
          return "getPackageGids";
        case 5:
          return "getPackageUid";
        case 4:
          return "getPackageInfoVersioned";
        case 3:
          return "getPackageInfo";
        case 2:
          return "isPackageAvailable";
        case 1:
          break;
      } 
      return "checkPackageStartable";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool27;
        int i22;
        boolean bool26;
        int i21;
        boolean bool25;
        int i20;
        boolean bool24;
        int i19;
        boolean bool23;
        int i18;
        boolean bool22;
        int i17;
        boolean bool21;
        int i16;
        boolean bool20;
        int i15;
        boolean bool19;
        int i14;
        boolean bool18;
        int i13;
        boolean bool17;
        int i12;
        boolean bool16;
        int i11;
        boolean bool15;
        int i10;
        boolean bool14;
        int i9;
        boolean bool13;
        int i8;
        boolean bool12;
        int i7;
        boolean bool11;
        int i6;
        boolean bool10;
        int i5;
        boolean bool9;
        int i4;
        boolean bool8;
        int i3;
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        String str25;
        List<String> list2;
        String str24;
        PermissionGroupInfo permissionGroupInfo;
        String str23, arrayOfString6[];
        ModuleInfo moduleInfo;
        List<ModuleInfo> list;
        String str22;
        CharSequence charSequence;
        IBinder iBinder2;
        String str21;
        ComponentName componentName1;
        ParceledListSlice parceledListSlice9;
        String str20;
        ChangedPackages changedPackages;
        String str19;
        Bitmap bitmap;
        byte[] arrayOfByte2;
        ParceledListSlice parceledListSlice8;
        String str18;
        KeySet keySet2;
        String str17;
        KeySet keySet1;
        IBinder iBinder1;
        VerifierDeviceIdentity verifierDeviceIdentity;
        String str16;
        ParceledListSlice parceledListSlice7;
        String str15;
        ParceledListSlice parceledListSlice6;
        ArrayList<String> arrayList3;
        String str14;
        IPackageMoveObserver iPackageMoveObserver;
        String str13;
        ArrayList<String> arrayList2;
        String str12;
        IDexModuleRegisterCallback iDexModuleRegisterCallback;
        String str11;
        ParceledListSlice parceledListSlice5;
        String[] arrayOfString5;
        IPackageStatsObserver iPackageStatsObserver;
        String str10;
        IPackageDataObserver iPackageDataObserver1;
        String str9;
        ArrayList<ResolveInfo> arrayList1;
        byte[] arrayOfByte1;
        Bundle bundle;
        String arrayOfString4[], str8;
        ResolveInfo resolveInfo3;
        String str7;
        InstallSourceInfo installSourceInfo;
        String str6;
        ParceledListSlice parceledListSlice4;
        InstrumentationInfo instrumentationInfo;
        String str5;
        ParceledListSlice parceledListSlice3;
        ArrayList<ProviderInfo> arrayList;
        ProviderInfo providerInfo2;
        ParceledListSlice parceledListSlice2;
        ResolveInfo resolveInfo2;
        ParceledListSlice parceledListSlice1;
        ResolveInfo resolveInfo1;
        String str4;
        int[] arrayOfInt2;
        String arrayOfString3[], str3, arrayOfString2[];
        List<String> list1;
        String str2;
        ProviderInfo providerInfo1;
        ServiceInfo serviceInfo;
        ActivityInfo activityInfo2;
        String str1;
        ActivityInfo activityInfo1;
        ApplicationInfo applicationInfo;
        String[] arrayOfString1;
        int[] arrayOfInt1;
        PackageInfo packageInfo;
        String str26;
        HashMap<Object, Object> hashMap;
        String str28;
        ComponentName[] arrayOfComponentName;
        String str27;
        Intent[] arrayOfIntent;
        byte[] arrayOfByte5;
        String str38;
        IArtManager iArtManager;
        String str37;
        byte[] arrayOfByte4;
        String str36;
        IPackageInstaller iPackageInstaller;
        String str35;
        IPackageDataObserver iPackageDataObserver2;
        String str34;
        ComponentName componentName2;
        byte[] arrayOfByte3;
        String str33, arrayOfString8[], str32;
        ArrayList<ComponentName> arrayList5;
        IPackageDeleteObserver iPackageDeleteObserver;
        String str31;
        ArrayList<String> arrayList4;
        String str30, arrayOfString7[];
        ArrayList<IntentFilter> arrayList6;
        String str41;
        IPackageDeleteObserver2 iPackageDeleteObserver2;
        String str40;
        long l;
        int i23;
        String arrayOfString9[], str43;
        KeySet keySet3 = null;
        String str39 = null, str42 = null;
        boolean bool28 = false, bool29 = false, bool30 = false, bool31 = false, bool32 = false, bool33 = false, bool34 = false, bool35 = false, bool36 = false, bool37 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 194:
            param1Parcel1.enforceInterface("android.content.pm.IPackageManager");
            str25 = param1Parcel1.readString();
            bool27 = isAutoRevokeWhitelisted(str25);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool27);
            return true;
          case 193:
            str25.enforceInterface("android.content.pm.IPackageManager");
            str39 = str25.readString();
            str25 = str25.readString();
            list2 = getMimeGroup(str39, str25);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list2);
            return true;
          case 192:
            list2.enforceInterface("android.content.pm.IPackageManager");
            str42 = list2.readString();
            str39 = list2.readString();
            list2 = list2.createStringArrayList();
            setMimeGroup(str42, str39, list2);
            param1Parcel2.writeNoException();
            return true;
          case 191:
            list2.enforceInterface("android.content.pm.IPackageManager");
            str39 = list2.readString();
            i22 = list2.readInt();
            i22 = checkUidPermission(str39, i22);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i22);
            return true;
          case 190:
            list2.enforceInterface("android.content.pm.IPackageManager");
            str39 = list2.readString();
            str42 = list2.readString();
            i22 = list2.readInt();
            grantRuntimePermission(str39, str42, i22);
            param1Parcel2.writeNoException();
            return true;
          case 189:
            list2.enforceInterface("android.content.pm.IPackageManager");
            str42 = list2.readString();
            str39 = list2.readString();
            i22 = list2.readInt();
            i22 = checkPermission(str42, str39, i22);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i22);
            return true;
          case 188:
            list2.enforceInterface("android.content.pm.IPackageManager");
            str24 = list2.readString();
            removePermission(str24);
            param1Parcel2.writeNoException();
            return true;
          case 187:
            str24.enforceInterface("android.content.pm.IPackageManager");
            if (str24.readInt() != 0) {
              PermissionInfo permissionInfo = (PermissionInfo)PermissionInfo.CREATOR.createFromParcel((Parcel)str24);
            } else {
              str24 = null;
            } 
            bool26 = addPermissionAsync((PermissionInfo)str24);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool26);
            return true;
          case 186:
            str24.enforceInterface("android.content.pm.IPackageManager");
            if (str24.readInt() != 0) {
              PermissionInfo permissionInfo = (PermissionInfo)PermissionInfo.CREATOR.createFromParcel((Parcel)str24);
            } else {
              str24 = null;
            } 
            bool26 = addPermission((PermissionInfo)str24);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool26);
            return true;
          case 185:
            str24.enforceInterface("android.content.pm.IPackageManager");
            str39 = str24.readString();
            i21 = str24.readInt();
            permissionGroupInfo = getPermissionGroupInfo(str39, i21);
            param1Parcel2.writeNoException();
            if (permissionGroupInfo != null) {
              param1Parcel2.writeInt(1);
              permissionGroupInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 184:
            permissionGroupInfo.enforceInterface("android.content.pm.IPackageManager");
            str23 = permissionGroupInfo.readString();
            arrayOfString6 = getAppOpPermissionPackages(str23);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString6);
            return true;
          case 183:
            arrayOfString6.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString6 = arrayOfString6.createStringArray();
            notifyPackagesReplacedReceived(arrayOfString6);
            param1Parcel2.writeNoException();
            return true;
          case 182:
            arrayOfString6.enforceInterface("android.content.pm.IPackageManager");
            i21 = arrayOfString6.readInt();
            param1Int2 = arrayOfString6.readInt();
            setRuntimePermissionsVersion(i21, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 181:
            arrayOfString6.enforceInterface("android.content.pm.IPackageManager");
            i21 = arrayOfString6.readInt();
            i21 = getRuntimePermissionsVersion(i21);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i21);
            return true;
          case 180:
            arrayOfString6.enforceInterface("android.content.pm.IPackageManager");
            str39 = arrayOfString6.readString();
            i21 = arrayOfString6.readInt();
            moduleInfo = getModuleInfo(str39, i21);
            param1Parcel2.writeNoException();
            if (moduleInfo != null) {
              param1Parcel2.writeInt(1);
              moduleInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 179:
            moduleInfo.enforceInterface("android.content.pm.IPackageManager");
            i21 = moduleInfo.readInt();
            list = getInstalledModules(i21);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 178:
            list.enforceInterface("android.content.pm.IPackageManager");
            sendDeviceCustomizationReadyBroadcast();
            param1Parcel2.writeNoException();
            return true;
          case 177:
            list.enforceInterface("android.content.pm.IPackageManager");
            str39 = list.readString();
            i21 = list.readInt();
            bool25 = isPackageStateProtected(str39, i21);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool25);
            return true;
          case 176:
            list.enforceInterface("android.content.pm.IPackageManager");
            str22 = getContentCaptureServicePackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 175:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getIncidentReportApproverPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 174:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getSetupWizardPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 173:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getSystemCaptionsServicePackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 172:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getAppPredictionServicePackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 171:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getWellbeingPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 170:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getAttentionServicePackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 169:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getSystemTextClassifierPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 168:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str22 = getDefaultTextClassifierPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str22);
            return true;
          case 167:
            str22.enforceInterface("android.content.pm.IPackageManager");
            i20 = str22.readInt();
            arrayOfByte5 = str22.createByteArray();
            param1Int2 = str22.readInt();
            bool24 = hasUidSigningCertificate(i20, arrayOfByte5, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool24);
            return true;
          case 166:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str42 = str22.readString();
            arrayOfByte5 = str22.createByteArray();
            i19 = str22.readInt();
            bool23 = hasSigningCertificate(str42, arrayOfByte5, i19);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool23);
            return true;
          case 165:
            str22.enforceInterface("android.content.pm.IPackageManager");
            str38 = str22.readString();
            i18 = str22.readInt();
            charSequence = getHarmfulAppWarning(str38, i18);
            param1Parcel2.writeNoException();
            if (charSequence != null) {
              param1Parcel2.writeInt(1);
              TextUtils.writeToParcel(charSequence, param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 164:
            charSequence.enforceInterface("android.content.pm.IPackageManager");
            str42 = charSequence.readString();
            if (charSequence.readInt() != 0) {
              CharSequence charSequence1 = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)charSequence);
            } else {
              str38 = null;
            } 
            i18 = charSequence.readInt();
            setHarmfulAppWarning(str42, str38, i18);
            param1Parcel2.writeNoException();
            return true;
          case 163:
            charSequence.enforceInterface("android.content.pm.IPackageManager");
            iArtManager = getArtManager();
            param1Parcel2.writeNoException();
            charSequence = str42;
            if (iArtManager != null)
              iBinder2 = iArtManager.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder2);
            return true;
          case 162:
            iBinder2.enforceInterface("android.content.pm.IPackageManager");
            str37 = iBinder2.readString();
            i18 = iBinder2.readInt();
            str21 = getInstantAppAndroidId(str37, i18);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str21);
            return true;
          case 161:
            str21.enforceInterface("android.content.pm.IPackageManager");
            componentName1 = getInstantAppInstallerComponent();
            param1Parcel2.writeNoException();
            if (componentName1 != null) {
              param1Parcel2.writeInt(1);
              componentName1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 160:
            componentName1.enforceInterface("android.content.pm.IPackageManager");
            componentName1 = getInstantAppResolverSettingsComponent();
            param1Parcel2.writeNoException();
            if (componentName1 != null) {
              param1Parcel2.writeInt(1);
              componentName1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 159:
            componentName1.enforceInterface("android.content.pm.IPackageManager");
            componentName1 = getInstantAppResolverComponent();
            param1Parcel2.writeNoException();
            if (componentName1 != null) {
              param1Parcel2.writeInt(1);
              componentName1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 158:
            componentName1.enforceInterface("android.content.pm.IPackageManager");
            deletePreloadsFileCache();
            param1Parcel2.writeNoException();
            return true;
          case 157:
            componentName1.enforceInterface("android.content.pm.IPackageManager");
            str37 = componentName1.readString();
            i18 = componentName1.readInt();
            bool22 = canRequestPackageInstalls(str37, i18);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool22);
            return true;
          case 156:
            componentName1.enforceInterface("android.content.pm.IPackageManager");
            str37 = componentName1.readString();
            i17 = componentName1.readInt();
            param1Int2 = componentName1.readInt();
            parceledListSlice9 = getDeclaredSharedLibraries(str37, i17, param1Int2);
            param1Parcel2.writeNoException();
            if (parceledListSlice9 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice9.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 155:
            parceledListSlice9.enforceInterface("android.content.pm.IPackageManager");
            str37 = parceledListSlice9.readString();
            i17 = parceledListSlice9.readInt();
            param1Int2 = parceledListSlice9.readInt();
            parceledListSlice9 = getSharedLibraries(str37, i17, param1Int2);
            param1Parcel2.writeNoException();
            if (parceledListSlice9 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice9.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 154:
            parceledListSlice9.enforceInterface("android.content.pm.IPackageManager");
            str37 = parceledListSlice9.readString();
            i17 = parceledListSlice9.readInt();
            i17 = getInstallReason(str37, i17);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i17);
            return true;
          case 153:
            parceledListSlice9.enforceInterface("android.content.pm.IPackageManager");
            str20 = parceledListSlice9.readString();
            bool21 = isPackageDeviceAdminOnAnyUser(str20);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool21);
            return true;
          case 152:
            str20.enforceInterface("android.content.pm.IPackageManager");
            i16 = str20.readInt();
            param1Int2 = str20.readInt();
            changedPackages = getChangedPackages(i16, param1Int2);
            param1Parcel2.writeNoException();
            if (changedPackages != null) {
              param1Parcel2.writeInt(1);
              changedPackages.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 151:
            changedPackages.enforceInterface("android.content.pm.IPackageManager");
            str19 = getSharedSystemSharedLibraryPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str19);
            return true;
          case 150:
            str19.enforceInterface("android.content.pm.IPackageManager");
            str19 = getServicesSystemSharedLibraryPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str19);
            return true;
          case 149:
            str19.enforceInterface("android.content.pm.IPackageManager");
            str37 = str19.readString();
            bool29 = bool37;
            if (str19.readInt() != 0)
              bool29 = true; 
            setUpdateAvailable(str37, bool29);
            param1Parcel2.writeNoException();
            return true;
          case 148:
            str19.enforceInterface("android.content.pm.IPackageManager");
            str37 = str19.readString();
            bool29 = bool28;
            if (str19.readInt() != 0)
              bool29 = true; 
            bool20 = setRequiredForSystemUser(str37, bool29);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool20);
            return true;
          case 147:
            str19.enforceInterface("android.content.pm.IPackageManager");
            str37 = str19.readString();
            i15 = str19.readInt();
            bool19 = isInstantApp(str37, i15);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool19);
            return true;
          case 146:
            str19.enforceInterface("android.content.pm.IPackageManager");
            str37 = str19.readString();
            i14 = str19.readInt();
            bitmap = getInstantAppIcon(str37, i14);
            param1Parcel2.writeNoException();
            if (bitmap != null) {
              param1Parcel2.writeInt(1);
              bitmap.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 145:
            bitmap.enforceInterface("android.content.pm.IPackageManager");
            str42 = bitmap.readString();
            arrayOfByte4 = bitmap.createByteArray();
            i14 = bitmap.readInt();
            bool18 = setInstantAppCookie(str42, arrayOfByte4, i14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool18);
            return true;
          case 144:
            bitmap.enforceInterface("android.content.pm.IPackageManager");
            str36 = bitmap.readString();
            i13 = bitmap.readInt();
            arrayOfByte2 = getInstantAppCookie(str36, i13);
            param1Parcel2.writeNoException();
            param1Parcel2.writeByteArray(arrayOfByte2);
            return true;
          case 143:
            arrayOfByte2.enforceInterface("android.content.pm.IPackageManager");
            i13 = arrayOfByte2.readInt();
            parceledListSlice8 = getInstantApps(i13);
            param1Parcel2.writeNoException();
            if (parceledListSlice8 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice8.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 142:
            parceledListSlice8.enforceInterface("android.content.pm.IPackageManager");
            str18 = getPermissionControllerPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str18);
            return true;
          case 141:
            str18.enforceInterface("android.content.pm.IPackageManager");
            str36 = str18.readString();
            if (str18.readInt() != 0) {
              KeySet keySet = (KeySet)KeySet.CREATOR.createFromParcel((Parcel)str18);
            } else {
              str18 = null;
            } 
            bool17 = isPackageSignedByKeySetExactly(str36, (KeySet)str18);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 140:
            str18.enforceInterface("android.content.pm.IPackageManager");
            str36 = str18.readString();
            if (str18.readInt() != 0) {
              KeySet keySet = (KeySet)KeySet.CREATOR.createFromParcel((Parcel)str18);
            } else {
              str18 = null;
            } 
            bool17 = isPackageSignedByKeySet(str36, (KeySet)str18);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool17);
            return true;
          case 139:
            str18.enforceInterface("android.content.pm.IPackageManager");
            str18 = str18.readString();
            keySet2 = getSigningKeySet(str18);
            param1Parcel2.writeNoException();
            if (keySet2 != null) {
              param1Parcel2.writeInt(1);
              keySet2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 138:
            keySet2.enforceInterface("android.content.pm.IPackageManager");
            str36 = keySet2.readString();
            str17 = keySet2.readString();
            keySet1 = getKeySetByAlias(str36, str17);
            param1Parcel2.writeNoException();
            if (keySet1 != null) {
              param1Parcel2.writeInt(1);
              keySet1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 137:
            keySet1.enforceInterface("android.content.pm.IPackageManager");
            str36 = keySet1.readString();
            i12 = keySet1.readInt();
            bool16 = getBlockUninstallForUser(str36, i12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool16);
            return true;
          case 136:
            keySet1.enforceInterface("android.content.pm.IPackageManager");
            str36 = keySet1.readString();
            if (keySet1.readInt() != 0)
              bool29 = true; 
            i11 = keySet1.readInt();
            bool15 = setBlockUninstallForUser(str36, bool29, i11);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool15);
            return true;
          case 135:
            keySet1.enforceInterface("android.content.pm.IPackageManager");
            iPackageInstaller = getPackageInstaller();
            param1Parcel2.writeNoException();
            keySet1 = keySet3;
            if (iPackageInstaller != null)
              iBinder1 = iPackageInstaller.asBinder(); 
            param1Parcel2.writeStrongBinder(iBinder1);
            return true;
          case 134:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            str35 = iBinder1.readString();
            bool29 = bool30;
            if (iBinder1.readInt() != 0)
              bool29 = true; 
            i10 = iBinder1.readInt();
            bool14 = setSystemAppInstallState(str35, bool29, i10);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool14);
            return true;
          case 133:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            str35 = iBinder1.readString();
            bool29 = bool31;
            if (iBinder1.readInt() != 0)
              bool29 = true; 
            setSystemAppHiddenUntilInstalled(str35, bool29);
            param1Parcel2.writeNoException();
            return true;
          case 132:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            str35 = iBinder1.readString();
            i9 = iBinder1.readInt();
            bool13 = getApplicationHiddenSettingAsUser(str35, i9);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool13);
            return true;
          case 131:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            str35 = iBinder1.readString();
            bool29 = bool32;
            if (iBinder1.readInt() != 0)
              bool29 = true; 
            i8 = iBinder1.readInt();
            bool12 = setApplicationHiddenSettingAsUser(str35, bool29, i8);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 130:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            bool12 = isStorageLow();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 129:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            bool12 = isDeviceUpgrading();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 128:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            bool12 = isOnlyCoreApps();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 127:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            bool12 = isFirstBoot();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool12);
            return true;
          case 126:
            iBinder1.enforceInterface("android.content.pm.IPackageManager");
            verifierDeviceIdentity = getVerifierDeviceIdentity();
            param1Parcel2.writeNoException();
            if (verifierDeviceIdentity != null) {
              param1Parcel2.writeInt(1);
              verifierDeviceIdentity.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 125:
            verifierDeviceIdentity.enforceInterface("android.content.pm.IPackageManager");
            str16 = verifierDeviceIdentity.readString();
            parceledListSlice7 = getAllIntentFilters(str16);
            param1Parcel2.writeNoException();
            if (parceledListSlice7 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice7.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 124:
            parceledListSlice7.enforceInterface("android.content.pm.IPackageManager");
            str15 = parceledListSlice7.readString();
            parceledListSlice6 = getIntentFilterVerifications(str15);
            param1Parcel2.writeNoException();
            if (parceledListSlice6 != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice6.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 123:
            parceledListSlice6.enforceInterface("android.content.pm.IPackageManager");
            str35 = parceledListSlice6.readString();
            i7 = parceledListSlice6.readInt();
            param1Int2 = parceledListSlice6.readInt();
            bool11 = updateIntentVerificationStatus(str35, i7, param1Int2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool11);
            return true;
          case 122:
            parceledListSlice6.enforceInterface("android.content.pm.IPackageManager");
            str35 = parceledListSlice6.readString();
            i6 = parceledListSlice6.readInt();
            i6 = getIntentVerificationStatus(str35, i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 121:
            parceledListSlice6.enforceInterface("android.content.pm.IPackageManager");
            param1Int2 = parceledListSlice6.readInt();
            i6 = parceledListSlice6.readInt();
            arrayList3 = parceledListSlice6.createStringArrayList();
            verifyIntentFilter(param1Int2, i6, arrayList3);
            param1Parcel2.writeNoException();
            return true;
          case 120:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            i6 = arrayList3.readInt();
            param1Int2 = arrayList3.readInt();
            l = arrayList3.readLong();
            extendVerificationTimeout(i6, param1Int2, l);
            param1Parcel2.writeNoException();
            return true;
          case 119:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            i6 = arrayList3.readInt();
            param1Int2 = arrayList3.readInt();
            verifyPendingInstall(i6, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 118:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            str35 = arrayList3.readString();
            i6 = arrayList3.readInt();
            i23 = arrayList3.readInt();
            param1Int2 = arrayList3.readInt();
            arrayList3 = arrayList3.createStringArrayList();
            i6 = installExistingPackageAsUser(str35, i6, i23, param1Int2, arrayList3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 117:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            i6 = getInstallLocation();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i6);
            return true;
          case 116:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            i6 = arrayList3.readInt();
            bool10 = setInstallLocation(i6);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool10);
            return true;
          case 115:
            arrayList3.enforceInterface("android.content.pm.IPackageManager");
            str14 = arrayList3.readString();
            i5 = movePrimaryStorage(str14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 114:
            str14.enforceInterface("android.content.pm.IPackageManager");
            str35 = str14.readString();
            str14 = str14.readString();
            i5 = movePackage(str35, str14);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 113:
            str14.enforceInterface("android.content.pm.IPackageManager");
            iPackageMoveObserver = IPackageMoveObserver.Stub.asInterface(str14.readStrongBinder());
            unregisterMoveCallback(iPackageMoveObserver);
            param1Parcel2.writeNoException();
            return true;
          case 112:
            iPackageMoveObserver.enforceInterface("android.content.pm.IPackageManager");
            iPackageMoveObserver = IPackageMoveObserver.Stub.asInterface(iPackageMoveObserver.readStrongBinder());
            registerMoveCallback(iPackageMoveObserver);
            param1Parcel2.writeNoException();
            return true;
          case 111:
            iPackageMoveObserver.enforceInterface("android.content.pm.IPackageManager");
            i5 = iPackageMoveObserver.readInt();
            i5 = getMoveStatus(i5);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i5);
            return true;
          case 110:
            iPackageMoveObserver.enforceInterface("android.content.pm.IPackageManager");
            str13 = iPackageMoveObserver.readString();
            reconcileSecondaryDexFiles(str13);
            param1Parcel2.writeNoException();
            return true;
          case 109:
            str13.enforceInterface("android.content.pm.IPackageManager");
            arrayList2 = str13.createStringArrayList();
            bool9 = runBackgroundDexoptJob(arrayList2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 108:
            arrayList2.enforceInterface("android.content.pm.IPackageManager");
            str12 = arrayList2.readString();
            forceDexOpt(str12);
            param1Parcel2.writeNoException();
            return true;
          case 107:
            str12.enforceInterface("android.content.pm.IPackageManager");
            str12 = str12.readString();
            dumpProfiles(str12);
            param1Parcel2.writeNoException();
            return true;
          case 106:
            str12.enforceInterface("android.content.pm.IPackageManager");
            str12 = str12.readString();
            bool9 = compileLayouts(str12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 105:
            str12.enforceInterface("android.content.pm.IPackageManager");
            str42 = str12.readString();
            str35 = str12.readString();
            bool29 = bool33;
            if (str12.readInt() != 0)
              bool29 = true; 
            bool9 = performDexOptSecondary(str42, str35, bool29);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 104:
            str12.enforceInterface("android.content.pm.IPackageManager");
            str42 = str12.readString();
            if (str12.readInt() != 0) {
              bool29 = true;
            } else {
              bool29 = false;
            } 
            str35 = str12.readString();
            if (str12.readInt() != 0) {
              bool37 = true;
            } else {
              bool37 = false;
            } 
            if (str12.readInt() != 0) {
              bool35 = true;
            } else {
              bool35 = false;
            } 
            str12 = str12.readString();
            bool9 = performDexOptMode(str42, bool29, str35, bool37, bool35, str12);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool9);
            return true;
          case 103:
            str12.enforceInterface("android.content.pm.IPackageManager");
            str35 = str12.readString();
            str26 = str12.readString();
            bool29 = bool34;
            if (str12.readInt() != 0)
              bool29 = true; 
            iDexModuleRegisterCallback = IDexModuleRegisterCallback.Stub.asInterface(str12.readStrongBinder());
            registerDexModule(str35, str26, bool29, iDexModuleRegisterCallback);
            return true;
          case 102:
            iDexModuleRegisterCallback.enforceInterface("android.content.pm.IPackageManager");
            str42 = iDexModuleRegisterCallback.readString();
            i4 = iDexModuleRegisterCallback.readInt();
            if (i4 < 0) {
              str26 = str35;
            } else {
              hashMap = new HashMap<>();
            } 
            IntStream.range(0, i4).forEach(new _$$Lambda$IPackageManager$Stub$ZVp6oEh_Gn_bn8lM7TgSgpaGriw((Parcel)iDexModuleRegisterCallback, hashMap));
            str11 = iDexModuleRegisterCallback.readString();
            notifyDexLoad(str42, (Map)hashMap, str11);
            return true;
          case 101:
            str11.enforceInterface("android.content.pm.IPackageManager");
            str = str11.readString();
            i4 = str11.readInt();
            notifyPackageUse(str, i4);
            return true;
          case 100:
            str11.enforceInterface("android.content.pm.IPackageManager");
            updatePackagesIfNeeded();
            str.writeNoException();
            return true;
          case 99:
            str11.enforceInterface("android.content.pm.IPackageManager");
            performFstrimIfNeeded();
            str.writeNoException();
            return true;
          case 98:
            str11.enforceInterface("android.content.pm.IPackageManager");
            bool8 = hasSystemUidErrors();
            str.writeNoException();
            str.writeInt(bool8);
            return true;
          case 97:
            str11.enforceInterface("android.content.pm.IPackageManager");
            systemReady();
            str.writeNoException();
            return true;
          case 96:
            str11.enforceInterface("android.content.pm.IPackageManager");
            bool8 = isSafeMode();
            str.writeNoException();
            str.writeInt(bool8);
            return true;
          case 95:
            str11.enforceInterface("android.content.pm.IPackageManager");
            enterSafeMode();
            str.writeNoException();
            return true;
          case 94:
            str11.enforceInterface("android.content.pm.IPackageManager");
            str35 = str11.readString();
            i3 = str11.readInt();
            bool7 = hasSystemFeature(str35, i3);
            str.writeNoException();
            str.writeInt(bool7);
            return true;
          case 93:
            str11.enforceInterface("android.content.pm.IPackageManager");
            parceledListSlice5 = getSystemAvailableFeatures();
            str.writeNoException();
            if (parceledListSlice5 != null) {
              str.writeInt(1);
              parceledListSlice5.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 92:
            parceledListSlice5.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString5 = getSystemSharedLibraryNames();
            str.writeNoException();
            str.writeStringArray(arrayOfString5);
            return true;
          case 91:
            arrayOfString5.enforceInterface("android.content.pm.IPackageManager");
            str35 = arrayOfString5.readString();
            i2 = arrayOfString5.readInt();
            iPackageStatsObserver = IPackageStatsObserver.Stub.asInterface(arrayOfString5.readStrongBinder());
            getPackageSizeInfo(str35, i2, iPackageStatsObserver);
            str.writeNoException();
            return true;
          case 90:
            iPackageStatsObserver.enforceInterface("android.content.pm.IPackageManager");
            str10 = iPackageStatsObserver.readString();
            clearApplicationProfileData(str10);
            str.writeNoException();
            return true;
          case 89:
            str10.enforceInterface("android.content.pm.IPackageManager");
            str42 = str10.readString();
            iPackageDataObserver2 = IPackageDataObserver.Stub.asInterface(str10.readStrongBinder());
            i2 = str10.readInt();
            clearApplicationUserData(str42, iPackageDataObserver2, i2);
            str.writeNoException();
            return true;
          case 88:
            str10.enforceInterface("android.content.pm.IPackageManager");
            str34 = str10.readString();
            i2 = str10.readInt();
            iPackageDataObserver1 = IPackageDataObserver.Stub.asInterface(str10.readStrongBinder());
            deleteApplicationCacheFilesAsUser(str34, i2, iPackageDataObserver1);
            str.writeNoException();
            return true;
          case 87:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            iPackageDataObserver1 = IPackageDataObserver.Stub.asInterface(iPackageDataObserver1.readStrongBinder());
            deleteApplicationCacheFiles(str34, iPackageDataObserver1);
            str.writeNoException();
            return true;
          case 86:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            l = iPackageDataObserver1.readLong();
            i2 = iPackageDataObserver1.readInt();
            if (iPackageDataObserver1.readInt() != 0) {
              IntentSender intentSender = (IntentSender)IntentSender.CREATOR.createFromParcel((Parcel)iPackageDataObserver1);
            } else {
              iPackageDataObserver1 = null;
            } 
            freeStorage(str34, l, i2, (IntentSender)iPackageDataObserver1);
            str.writeNoException();
            return true;
          case 85:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            l = iPackageDataObserver1.readLong();
            i2 = iPackageDataObserver1.readInt();
            iPackageDataObserver1 = IPackageDataObserver.Stub.asInterface(iPackageDataObserver1.readStrongBinder());
            freeStorageAndNotify(str34, l, i2, iPackageDataObserver1);
            str.writeNoException();
            return true;
          case 84:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            bool29 = bool35;
            if (iPackageDataObserver1.readInt() != 0)
              bool29 = true; 
            i2 = iPackageDataObserver1.readInt();
            setPackageStoppedState(str34, bool29, i2);
            str.writeNoException();
            return true;
          case 83:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            i2 = iPackageDataObserver1.readInt();
            flushPackageRestrictionsAsUser(i2);
            str.writeNoException();
            return true;
          case 82:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str28 = iPackageDataObserver1.readString();
            param1Int2 = iPackageDataObserver1.readInt();
            str42 = iPackageDataObserver1.readString();
            str34 = iPackageDataObserver1.readString();
            i2 = iPackageDataObserver1.readInt();
            logAppProcessStartIfNeeded(str28, param1Int2, str42, str34, i2);
            str.writeNoException();
            return true;
          case 81:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            i2 = iPackageDataObserver1.readInt();
            i2 = getApplicationEnabledSetting(str34, i2);
            str.writeNoException();
            str.writeInt(i2);
            return true;
          case 80:
            iPackageDataObserver1.enforceInterface("android.content.pm.IPackageManager");
            str34 = iPackageDataObserver1.readString();
            i23 = iPackageDataObserver1.readInt();
            i2 = iPackageDataObserver1.readInt();
            param1Int2 = iPackageDataObserver1.readInt();
            str9 = iPackageDataObserver1.readString();
            setApplicationEnabledSetting(str34, i23, i2, param1Int2, str9);
            str.writeNoException();
            return true;
          case 79:
            str9.enforceInterface("android.content.pm.IPackageManager");
            if (str9.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str34 = null;
            } 
            i2 = str9.readInt();
            i2 = getComponentEnabledSetting((ComponentName)str34, i2);
            str.writeNoException();
            str.writeInt(i2);
            return true;
          case 78:
            str9.enforceInterface("android.content.pm.IPackageManager");
            if (str9.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str34 = null;
            } 
            i23 = str9.readInt();
            param1Int2 = str9.readInt();
            i2 = str9.readInt();
            setComponentEnabledSetting((ComponentName)str34, i23, param1Int2, i2);
            str.writeNoException();
            return true;
          case 77:
            str9.enforceInterface("android.content.pm.IPackageManager");
            if (str9.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str34 = null;
            } 
            i2 = str9.readInt();
            restoreLabelAndIcon((ComponentName)str34, i2);
            str.writeNoException();
            return true;
          case 76:
            str9.enforceInterface("android.content.pm.IPackageManager");
            if (str9.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str34 = null;
            } 
            str42 = str9.readString();
            param1Int2 = str9.readInt();
            i2 = str9.readInt();
            overrideLabelAndIcon((ComponentName)str34, str42, param1Int2, i2);
            str.writeNoException();
            return true;
          case 75:
            str9.enforceInterface("android.content.pm.IPackageManager");
            if (str9.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str9);
            } else {
              str34 = null;
            } 
            i2 = str9.readInt();
            setHomeActivity((ComponentName)str34, i2);
            str.writeNoException();
            return true;
          case 74:
            str9.enforceInterface("android.content.pm.IPackageManager");
            arrayList1 = new ArrayList();
            componentName2 = getHomeActivities(arrayList1);
            str.writeNoException();
            if (componentName2 != null) {
              str.writeInt(1);
              componentName2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            str.writeTypedList(arrayList1);
            return true;
          case 73:
            arrayList1.enforceInterface("android.content.pm.IPackageManager");
            arrayOfByte3 = arrayList1.createByteArray();
            i2 = arrayList1.readInt();
            restoreIntentFilterVerification(arrayOfByte3, i2);
            str.writeNoException();
            return true;
          case 72:
            arrayList1.enforceInterface("android.content.pm.IPackageManager");
            i2 = arrayList1.readInt();
            arrayOfByte1 = getIntentFilterVerificationBackup(i2);
            str.writeNoException();
            str.writeByteArray(arrayOfByte1);
            return true;
          case 71:
            arrayOfByte1.enforceInterface("android.content.pm.IPackageManager");
            arrayOfByte3 = arrayOfByte1.createByteArray();
            i2 = arrayOfByte1.readInt();
            restoreDefaultApps(arrayOfByte3, i2);
            str.writeNoException();
            return true;
          case 70:
            arrayOfByte1.enforceInterface("android.content.pm.IPackageManager");
            i2 = arrayOfByte1.readInt();
            arrayOfByte1 = getDefaultAppsBackup(i2);
            str.writeNoException();
            str.writeByteArray(arrayOfByte1);
            return true;
          case 69:
            arrayOfByte1.enforceInterface("android.content.pm.IPackageManager");
            arrayOfByte3 = arrayOfByte1.createByteArray();
            i2 = arrayOfByte1.readInt();
            restorePreferredActivities(arrayOfByte3, i2);
            str.writeNoException();
            return true;
          case 68:
            arrayOfByte1.enforceInterface("android.content.pm.IPackageManager");
            i2 = arrayOfByte1.readInt();
            arrayOfByte1 = getPreferredActivityBackup(i2);
            str.writeNoException();
            str.writeByteArray(arrayOfByte1);
            return true;
          case 67:
            arrayOfByte1.enforceInterface("android.content.pm.IPackageManager");
            str33 = arrayOfByte1.readString();
            i2 = arrayOfByte1.readInt();
            bundle = getSuspendedPackageAppExtras(str33, i2);
            str.writeNoException();
            if (bundle != null) {
              str.writeInt(1);
              bundle.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 66:
            bundle.enforceInterface("android.content.pm.IPackageManager");
            str33 = bundle.readString();
            i2 = bundle.readInt();
            bool6 = isPackageSuspendedForUser(str33, i2);
            str.writeNoException();
            str.writeInt(bool6);
            return true;
          case 65:
            bundle.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString8 = bundle.createStringArray();
            i1 = bundle.readInt();
            arrayOfString4 = getUnsuspendablePackagesForUser(arrayOfString8, i1);
            str.writeNoException();
            str.writeStringArray(arrayOfString4);
            return true;
          case 64:
            arrayOfString4.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString9 = arrayOfString4.createStringArray();
            if (arrayOfString4.readInt() != 0) {
              bool29 = true;
            } else {
              bool29 = false;
            } 
            if (arrayOfString4.readInt() != 0) {
              PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel((Parcel)arrayOfString4);
            } else {
              arrayOfString8 = null;
            } 
            if (arrayOfString4.readInt() != 0) {
              PersistableBundle persistableBundle = (PersistableBundle)PersistableBundle.CREATOR.createFromParcel((Parcel)arrayOfString4);
            } else {
              str42 = null;
            } 
            if (arrayOfString4.readInt() != 0) {
              SuspendDialogInfo suspendDialogInfo = (SuspendDialogInfo)SuspendDialogInfo.CREATOR.createFromParcel((Parcel)arrayOfString4);
            } else {
              str28 = null;
            } 
            str43 = arrayOfString4.readString();
            i1 = arrayOfString4.readInt();
            arrayOfString4 = setPackagesSuspendedAsUser(arrayOfString9, bool29, (PersistableBundle)arrayOfString8, (PersistableBundle)str42, (SuspendDialogInfo)str28, str43, i1);
            str.writeNoException();
            str.writeStringArray(arrayOfString4);
            return true;
          case 63:
            arrayOfString4.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString8 = arrayOfString4.createStringArray();
            param1Int2 = arrayOfString4.readInt();
            i1 = arrayOfString4.readInt();
            arrayOfString4 = setDistractingPackageRestrictionsAsUser(arrayOfString8, param1Int2, i1);
            str.writeNoException();
            str.writeStringArray(arrayOfString4);
            return true;
          case 62:
            arrayOfString4.enforceInterface("android.content.pm.IPackageManager");
            i1 = arrayOfString4.readInt();
            str8 = arrayOfString4.readString();
            clearCrossProfileIntentFilters(i1, str8);
            str.writeNoException();
            return true;
          case 61:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayOfString8 = null;
            } 
            str42 = str8.readString();
            i1 = str8.readInt();
            param1Int2 = str8.readInt();
            i23 = str8.readInt();
            addCrossProfileIntentFilter((IntentFilter)arrayOfString8, str42, i1, param1Int2, i23);
            str.writeNoException();
            return true;
          case 60:
            str8.enforceInterface("android.content.pm.IPackageManager");
            str32 = str8.readString();
            i1 = str8.readInt();
            clearPackagePersistentPreferredActivities(str32, i1);
            str.writeNoException();
            return true;
          case 59:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str32 = null;
            } 
            if (str8.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str42 = null;
            } 
            i1 = str8.readInt();
            addPersistentPreferredActivity((IntentFilter)str32, (ComponentName)str42, i1);
            str.writeNoException();
            return true;
          case 58:
            str8.enforceInterface("android.content.pm.IPackageManager");
            arrayList6 = new ArrayList();
            arrayList5 = new ArrayList();
            str8 = str8.readString();
            i1 = getPreferredActivities(arrayList6, arrayList5, str8);
            str.writeNoException();
            str.writeInt(i1);
            str.writeTypedList(arrayList6);
            str.writeTypedList(arrayList5);
            return true;
          case 57:
            str8.enforceInterface("android.content.pm.IPackageManager");
            str8 = str8.readString();
            clearPackagePreferredActivities(str8);
            str.writeNoException();
            return true;
          case 56:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList5 = null;
            } 
            param1Int2 = str8.readInt();
            arrayOfComponentName = (ComponentName[])str8.createTypedArray(ComponentName.CREATOR);
            if (str8.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList6 = null;
            } 
            i1 = str8.readInt();
            replacePreferredActivity((IntentFilter)arrayList5, param1Int2, arrayOfComponentName, (ComponentName)arrayList6, i1);
            str.writeNoException();
            return true;
          case 55:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList5 = null;
            } 
            param1Int2 = str8.readInt();
            arrayOfComponentName = (ComponentName[])str8.createTypedArray(ComponentName.CREATOR);
            if (str8.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList6 = null;
            } 
            i1 = str8.readInt();
            addPreferredActivity((IntentFilter)arrayList5, param1Int2, arrayOfComponentName, (ComponentName)arrayList6, i1);
            str.writeNoException();
            return true;
          case 54:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList5 = null;
            } 
            str27 = str8.readString();
            param1Int2 = str8.readInt();
            if (str8.readInt() != 0) {
              IntentFilter intentFilter = (IntentFilter)IntentFilter.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList6 = null;
            } 
            i1 = str8.readInt();
            if (str8.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str8);
            } else {
              str8 = null;
            } 
            setLastChosenActivity((Intent)arrayList5, str27, param1Int2, (IntentFilter)arrayList6, i1, (ComponentName)str8);
            str.writeNoException();
            return true;
          case 53:
            str8.enforceInterface("android.content.pm.IPackageManager");
            if (str8.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)str8);
            } else {
              arrayList5 = null;
            } 
            str41 = str8.readString();
            i1 = str8.readInt();
            resolveInfo3 = getLastChosenActivity((Intent)arrayList5, str41, i1);
            str.writeNoException();
            if (resolveInfo3 != null) {
              str.writeInt(1);
              resolveInfo3.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 52:
            resolveInfo3.enforceInterface("android.content.pm.IPackageManager");
            i1 = resolveInfo3.readInt();
            resetApplicationPreferences(i1);
            str.writeNoException();
            return true;
          case 51:
            resolveInfo3.enforceInterface("android.content.pm.IPackageManager");
            str7 = resolveInfo3.readString();
            installSourceInfo = getInstallSourceInfo(str7);
            str.writeNoException();
            if (installSourceInfo != null) {
              str.writeInt(1);
              installSourceInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 50:
            installSourceInfo.enforceInterface("android.content.pm.IPackageManager");
            str6 = installSourceInfo.readString();
            str6 = getInstallerPackageName(str6);
            str.writeNoException();
            str.writeString(str6);
            return true;
          case 49:
            str6.enforceInterface("android.content.pm.IPackageManager");
            if (str6.readInt() != 0) {
              VersionedPackage versionedPackage = (VersionedPackage)VersionedPackage.CREATOR.createFromParcel((Parcel)str6);
            } else {
              arrayList5 = null;
            } 
            iPackageDeleteObserver2 = IPackageDeleteObserver2.Stub.asInterface(str6.readStrongBinder());
            i1 = str6.readInt();
            deleteExistingPackageAsUser((VersionedPackage)arrayList5, iPackageDeleteObserver2, i1);
            str.writeNoException();
            return true;
          case 48:
            str6.enforceInterface("android.content.pm.IPackageManager");
            if (str6.readInt() != 0) {
              VersionedPackage versionedPackage = (VersionedPackage)VersionedPackage.CREATOR.createFromParcel((Parcel)str6);
            } else {
              arrayList5 = null;
            } 
            iPackageDeleteObserver2 = IPackageDeleteObserver2.Stub.asInterface(str6.readStrongBinder());
            i1 = str6.readInt();
            param1Int2 = str6.readInt();
            deletePackageVersioned((VersionedPackage)arrayList5, iPackageDeleteObserver2, i1, param1Int2);
            str.writeNoException();
            return true;
          case 47:
            str6.enforceInterface("android.content.pm.IPackageManager");
            str40 = str6.readString();
            i1 = str6.readInt();
            iPackageDeleteObserver = IPackageDeleteObserver.Stub.asInterface(str6.readStrongBinder());
            i23 = str6.readInt();
            param1Int2 = str6.readInt();
            deletePackageAsUser(str40, i1, iPackageDeleteObserver, i23, param1Int2);
            str.writeNoException();
            return true;
          case 46:
            str6.enforceInterface("android.content.pm.IPackageManager");
            str31 = str6.readString();
            i1 = str6.readInt();
            str6 = str6.readString();
            setApplicationCategoryHint(str31, i1, str6);
            str.writeNoException();
            return true;
          case 45:
            str6.enforceInterface("android.content.pm.IPackageManager");
            str31 = str6.readString();
            str6 = str6.readString();
            setInstallerPackageName(str31, str6);
            str.writeNoException();
            return true;
          case 44:
            str6.enforceInterface("android.content.pm.IPackageManager");
            i1 = str6.readInt();
            bool29 = bool36;
            if (str6.readInt() != 0)
              bool29 = true; 
            finishPackageInstall(i1, bool29);
            str.writeNoException();
            return true;
          case 43:
            str6.enforceInterface("android.content.pm.IPackageManager");
            str31 = str6.readString();
            i1 = str6.readInt();
            parceledListSlice4 = queryInstrumentation(str31, i1);
            str.writeNoException();
            if (parceledListSlice4 != null) {
              str.writeInt(1);
              parceledListSlice4.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 42:
            parceledListSlice4.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice4.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)parceledListSlice4);
            } else {
              str31 = null;
            } 
            i1 = parceledListSlice4.readInt();
            instrumentationInfo = getInstrumentationInfo((ComponentName)str31, i1);
            str.writeNoException();
            if (instrumentationInfo != null) {
              str.writeInt(1);
              instrumentationInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 41:
            instrumentationInfo.enforceInterface("android.content.pm.IPackageManager");
            str31 = instrumentationInfo.readString();
            i1 = instrumentationInfo.readInt();
            param1Int2 = instrumentationInfo.readInt();
            str5 = instrumentationInfo.readString();
            parceledListSlice3 = queryContentProviders(str31, i1, param1Int2, str5);
            str.writeNoException();
            if (parceledListSlice3 != null) {
              str.writeInt(1);
              parceledListSlice3.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 40:
            parceledListSlice3.enforceInterface("android.content.pm.IPackageManager");
            arrayList4 = parceledListSlice3.createStringArrayList();
            arrayList = parceledListSlice3.createTypedArrayList(ProviderInfo.CREATOR);
            querySyncProviders(arrayList4, arrayList);
            str.writeNoException();
            str.writeStringList(arrayList4);
            str.writeTypedList(arrayList);
            return true;
          case 39:
            arrayList.enforceInterface("android.content.pm.IPackageManager");
            str30 = arrayList.readString();
            i1 = arrayList.readInt();
            param1Int2 = arrayList.readInt();
            providerInfo2 = resolveContentProvider(str30, i1, param1Int2);
            str.writeNoException();
            if (providerInfo2 != null) {
              str.writeInt(1);
              providerInfo2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 38:
            providerInfo2.enforceInterface("android.content.pm.IPackageManager");
            i1 = providerInfo2.readInt();
            parceledListSlice2 = getPersistentApplications(i1);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 37:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            param1Int2 = parceledListSlice2.readInt();
            i1 = parceledListSlice2.readInt();
            parceledListSlice2 = getInstalledApplications(param1Int2, i1);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 36:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString7 = parceledListSlice2.createStringArray();
            param1Int2 = parceledListSlice2.readInt();
            i1 = parceledListSlice2.readInt();
            parceledListSlice2 = getPackagesHoldingPermissions(arrayOfString7, param1Int2, i1);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 35:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            i1 = parceledListSlice2.readInt();
            param1Int2 = parceledListSlice2.readInt();
            parceledListSlice2 = getInstalledPackages(i1, param1Int2);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 34:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice2);
            } else {
              arrayOfString7 = null;
            } 
            str40 = parceledListSlice2.readString();
            i1 = parceledListSlice2.readInt();
            param1Int2 = parceledListSlice2.readInt();
            parceledListSlice2 = queryIntentContentProviders((Intent)arrayOfString7, str40, i1, param1Int2);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 33:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice2);
            } else {
              arrayOfString7 = null;
            } 
            str40 = parceledListSlice2.readString();
            param1Int2 = parceledListSlice2.readInt();
            i1 = parceledListSlice2.readInt();
            parceledListSlice2 = queryIntentServices((Intent)arrayOfString7, str40, param1Int2, i1);
            str.writeNoException();
            if (parceledListSlice2 != null) {
              str.writeInt(1);
              parceledListSlice2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 32:
            parceledListSlice2.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice2);
            } else {
              arrayOfString7 = null;
            } 
            str40 = parceledListSlice2.readString();
            i1 = parceledListSlice2.readInt();
            param1Int2 = parceledListSlice2.readInt();
            resolveInfo2 = resolveService((Intent)arrayOfString7, str40, i1, param1Int2);
            str.writeNoException();
            if (resolveInfo2 != null) {
              str.writeInt(1);
              resolveInfo2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 31:
            resolveInfo2.enforceInterface("android.content.pm.IPackageManager");
            if (resolveInfo2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)resolveInfo2);
            } else {
              arrayOfString7 = null;
            } 
            str40 = resolveInfo2.readString();
            i1 = resolveInfo2.readInt();
            param1Int2 = resolveInfo2.readInt();
            parceledListSlice1 = queryIntentReceivers((Intent)arrayOfString7, str40, i1, param1Int2);
            str.writeNoException();
            if (parceledListSlice1 != null) {
              str.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 30:
            parceledListSlice1.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              arrayOfString7 = null;
            } 
            arrayOfIntent = (Intent[])parceledListSlice1.createTypedArray(Intent.CREATOR);
            arrayOfString9 = parceledListSlice1.createStringArray();
            if (parceledListSlice1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              str40 = null;
            } 
            str43 = parceledListSlice1.readString();
            i1 = parceledListSlice1.readInt();
            param1Int2 = parceledListSlice1.readInt();
            parceledListSlice1 = queryIntentActivityOptions((ComponentName)arrayOfString7, arrayOfIntent, arrayOfString9, (Intent)str40, str43, i1, param1Int2);
            str.writeNoException();
            if (parceledListSlice1 != null) {
              str.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 29:
            parceledListSlice1.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              arrayOfString7 = null;
            } 
            str40 = parceledListSlice1.readString();
            param1Int2 = parceledListSlice1.readInt();
            i1 = parceledListSlice1.readInt();
            parceledListSlice1 = queryIntentActivities((Intent)arrayOfString7, str40, param1Int2, i1);
            str.writeNoException();
            if (parceledListSlice1 != null) {
              str.writeInt(1);
              parceledListSlice1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 28:
            parceledListSlice1.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              arrayOfString7 = null;
            } 
            str40 = parceledListSlice1.readString();
            param1Int2 = parceledListSlice1.readInt();
            i1 = parceledListSlice1.readInt();
            bool5 = canForwardTo((Intent)arrayOfString7, str40, param1Int2, i1);
            str.writeNoException();
            str.writeInt(bool5);
            return true;
          case 27:
            parceledListSlice1.enforceInterface("android.content.pm.IPackageManager");
            if (parceledListSlice1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)parceledListSlice1);
            } else {
              arrayOfString7 = null;
            } 
            n = parceledListSlice1.readInt();
            resolveInfo1 = findPersistentPreferredActivity((Intent)arrayOfString7, n);
            str.writeNoException();
            if (resolveInfo1 != null) {
              str.writeInt(1);
              resolveInfo1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 26:
            resolveInfo1.enforceInterface("android.content.pm.IPackageManager");
            if (resolveInfo1.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)resolveInfo1);
            } else {
              arrayOfString7 = null;
            } 
            str40 = resolveInfo1.readString();
            param1Int2 = resolveInfo1.readInt();
            n = resolveInfo1.readInt();
            resolveInfo1 = resolveIntent((Intent)arrayOfString7, str40, param1Int2, n);
            str.writeNoException();
            if (resolveInfo1 != null) {
              str.writeInt(1);
              resolveInfo1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 25:
            resolveInfo1.enforceInterface("android.content.pm.IPackageManager");
            n = resolveInfo1.readInt();
            bool4 = isUidPrivileged(n);
            str.writeNoException();
            str.writeInt(bool4);
            return true;
          case 24:
            resolveInfo1.enforceInterface("android.content.pm.IPackageManager");
            m = resolveInfo1.readInt();
            m = getPrivateFlagsForUid(m);
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 23:
            resolveInfo1.enforceInterface("android.content.pm.IPackageManager");
            m = resolveInfo1.readInt();
            m = getFlagsForUid(m);
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 22:
            resolveInfo1.enforceInterface("android.content.pm.IPackageManager");
            str4 = resolveInfo1.readString();
            m = getUidForSharedUser(str4);
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 21:
            str4.enforceInterface("android.content.pm.IPackageManager");
            arrayOfInt2 = str4.createIntArray();
            arrayOfString3 = getNamesForUids(arrayOfInt2);
            str.writeNoException();
            str.writeStringArray(arrayOfString3);
            return true;
          case 20:
            arrayOfString3.enforceInterface("android.content.pm.IPackageManager");
            m = arrayOfString3.readInt();
            str3 = getNameForUid(m);
            str.writeNoException();
            str.writeString(str3);
            return true;
          case 19:
            str3.enforceInterface("android.content.pm.IPackageManager");
            m = str3.readInt();
            arrayOfString2 = getPackagesForUid(m);
            str.writeNoException();
            str.writeStringArray(arrayOfString2);
            return true;
          case 18:
            arrayOfString2.enforceInterface("android.content.pm.IPackageManager");
            list1 = getAllPackages();
            str.writeNoException();
            str.writeStringList(list1);
            return true;
          case 17:
            list1.enforceInterface("android.content.pm.IPackageManager");
            param1Int2 = list1.readInt();
            m = list1.readInt();
            m = checkUidSignatures(param1Int2, m);
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 16:
            list1.enforceInterface("android.content.pm.IPackageManager");
            str29 = list1.readString();
            str2 = list1.readString();
            m = checkSignatures(str29, str2);
            str.writeNoException();
            str.writeInt(m);
            return true;
          case 15:
            str2.enforceInterface("android.content.pm.IPackageManager");
            str2 = str2.readString();
            bool3 = isProtectedBroadcast(str2);
            str.writeNoException();
            str.writeInt(bool3);
            return true;
          case 14:
            str2.enforceInterface("android.content.pm.IPackageManager");
            if (str2.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str29 = null;
            } 
            param1Int2 = str2.readInt();
            k = str2.readInt();
            providerInfo1 = getProviderInfo((ComponentName)str29, param1Int2, k);
            str.writeNoException();
            if (providerInfo1 != null) {
              str.writeInt(1);
              providerInfo1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 13:
            providerInfo1.enforceInterface("android.content.pm.IPackageManager");
            if (providerInfo1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)providerInfo1);
            } else {
              str29 = null;
            } 
            k = providerInfo1.readInt();
            param1Int2 = providerInfo1.readInt();
            serviceInfo = getServiceInfo((ComponentName)str29, k, param1Int2);
            str.writeNoException();
            if (serviceInfo != null) {
              str.writeInt(1);
              serviceInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 12:
            serviceInfo.enforceInterface("android.content.pm.IPackageManager");
            if (serviceInfo.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)serviceInfo);
            } else {
              str29 = null;
            } 
            param1Int2 = serviceInfo.readInt();
            k = serviceInfo.readInt();
            activityInfo2 = getReceiverInfo((ComponentName)str29, param1Int2, k);
            str.writeNoException();
            if (activityInfo2 != null) {
              str.writeInt(1);
              activityInfo2.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 11:
            activityInfo2.enforceInterface("android.content.pm.IPackageManager");
            if (activityInfo2.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)activityInfo2);
            } else {
              str29 = null;
            } 
            if (activityInfo2.readInt() != 0) {
              Intent intent = (Intent)Intent.CREATOR.createFromParcel((Parcel)activityInfo2);
            } else {
              str40 = null;
            } 
            str1 = activityInfo2.readString();
            bool2 = activitySupportsIntent((ComponentName)str29, (Intent)str40, str1);
            str.writeNoException();
            str.writeInt(bool2);
            return true;
          case 10:
            str1.enforceInterface("android.content.pm.IPackageManager");
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str29 = null;
            } 
            j = str1.readInt();
            param1Int2 = str1.readInt();
            activityInfo1 = getActivityInfo((ComponentName)str29, j, param1Int2);
            str.writeNoException();
            if (activityInfo1 != null) {
              str.writeInt(1);
              activityInfo1.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 9:
            activityInfo1.enforceInterface("android.content.pm.IPackageManager");
            str29 = activityInfo1.readString();
            param1Int2 = activityInfo1.readInt();
            j = activityInfo1.readInt();
            applicationInfo = getApplicationInfo(str29, param1Int2, j);
            str.writeNoException();
            if (applicationInfo != null) {
              str.writeInt(1);
              applicationInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 8:
            applicationInfo.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString1 = applicationInfo.createStringArray();
            arrayOfString1 = canonicalToCurrentPackageNames(arrayOfString1);
            str.writeNoException();
            str.writeStringArray(arrayOfString1);
            return true;
          case 7:
            arrayOfString1.enforceInterface("android.content.pm.IPackageManager");
            arrayOfString1 = arrayOfString1.createStringArray();
            arrayOfString1 = currentToCanonicalPackageNames(arrayOfString1);
            str.writeNoException();
            str.writeStringArray(arrayOfString1);
            return true;
          case 6:
            arrayOfString1.enforceInterface("android.content.pm.IPackageManager");
            str29 = arrayOfString1.readString();
            j = arrayOfString1.readInt();
            param1Int2 = arrayOfString1.readInt();
            arrayOfInt1 = getPackageGids(str29, j, param1Int2);
            str.writeNoException();
            str.writeIntArray(arrayOfInt1);
            return true;
          case 5:
            arrayOfInt1.enforceInterface("android.content.pm.IPackageManager");
            str29 = arrayOfInt1.readString();
            j = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            j = getPackageUid(str29, j, param1Int2);
            str.writeNoException();
            str.writeInt(j);
            return true;
          case 4:
            arrayOfInt1.enforceInterface("android.content.pm.IPackageManager");
            if (arrayOfInt1.readInt() != 0) {
              VersionedPackage versionedPackage = (VersionedPackage)VersionedPackage.CREATOR.createFromParcel((Parcel)arrayOfInt1);
            } else {
              str29 = null;
            } 
            j = arrayOfInt1.readInt();
            param1Int2 = arrayOfInt1.readInt();
            packageInfo = getPackageInfoVersioned((VersionedPackage)str29, j, param1Int2);
            str.writeNoException();
            if (packageInfo != null) {
              str.writeInt(1);
              packageInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 3:
            packageInfo.enforceInterface("android.content.pm.IPackageManager");
            str29 = packageInfo.readString();
            param1Int2 = packageInfo.readInt();
            j = packageInfo.readInt();
            packageInfo = getPackageInfo(str29, param1Int2, j);
            str.writeNoException();
            if (packageInfo != null) {
              str.writeInt(1);
              packageInfo.writeToParcel((Parcel)str, 1);
            } else {
              str.writeInt(0);
            } 
            return true;
          case 2:
            packageInfo.enforceInterface("android.content.pm.IPackageManager");
            str29 = packageInfo.readString();
            j = packageInfo.readInt();
            bool1 = isPackageAvailable(str29, j);
            str.writeNoException();
            str.writeInt(bool1);
            return true;
          case 1:
            break;
        } 
        packageInfo.enforceInterface("android.content.pm.IPackageManager");
        String str29 = packageInfo.readString();
        int i = packageInfo.readInt();
        checkPackageStartable(str29, i);
        str.writeNoException();
        return true;
      } 
      str.writeString("android.content.pm.IPackageManager");
      return true;
    }
    
    private static class Proxy implements IPackageManager {
      public static IPackageManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.pm.IPackageManager";
      }
      
      public void checkPackageStartable(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().checkPackageStartable(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageAvailable(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(2, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageAvailable(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PackageInfo getPackageInfo(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackageInfo(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PackageInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PackageInfo getPackageInfoVersioned(VersionedPackage param2VersionedPackage, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2VersionedPackage != null) {
            parcel1.writeInt(1);
            param2VersionedPackage.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackageInfoVersioned(param2VersionedPackage, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PackageInfo packageInfo = (PackageInfo)PackageInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2VersionedPackage = null;
          } 
          return (PackageInfo)param2VersionedPackage;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPackageUid(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IPackageManager.Stub.getDefaultImpl().getPackageUid(param2String, param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int[] getPackageGids(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackageGids(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          return parcel2.createIntArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] currentToCanonicalPackageNames(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2ArrayOfString = IPackageManager.Stub.getDefaultImpl().currentToCanonicalPackageNames(param2ArrayOfString);
            return param2ArrayOfString;
          } 
          parcel2.readException();
          param2ArrayOfString = parcel2.createStringArray();
          return param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] canonicalToCurrentPackageNames(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2ArrayOfString = IPackageManager.Stub.getDefaultImpl().canonicalToCurrentPackageNames(param2ArrayOfString);
            return param2ArrayOfString;
          } 
          parcel2.readException();
          param2ArrayOfString = parcel2.createStringArray();
          return param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ApplicationInfo getApplicationInfo(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getApplicationInfo(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ApplicationInfo applicationInfo = (ApplicationInfo)ApplicationInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ApplicationInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityInfo getActivityInfo(ComponentName param2ComponentName, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getActivityInfo(param2ComponentName, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ActivityInfo activityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (ActivityInfo)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean activitySupportsIntent(ComponentName param2ComponentName, Intent param2Intent, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool1 = true;
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().activitySupportsIntent(param2ComponentName, param2Intent, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ActivityInfo getReceiverInfo(ComponentName param2ComponentName, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getReceiverInfo(param2ComponentName, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ActivityInfo activityInfo = (ActivityInfo)ActivityInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (ActivityInfo)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ServiceInfo getServiceInfo(ComponentName param2ComponentName, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getServiceInfo(param2ComponentName, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ServiceInfo serviceInfo = (ServiceInfo)ServiceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (ServiceInfo)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ProviderInfo getProviderInfo(ComponentName param2ComponentName, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getProviderInfo(param2ComponentName, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ProviderInfo providerInfo = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (ProviderInfo)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isProtectedBroadcast(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(15, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isProtectedBroadcast(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkSignatures(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().checkSignatures(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkUidSignatures(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IPackageManager.Stub.getDefaultImpl().checkUidSignatures(param2Int1, param2Int2);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getAllPackages() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getAllPackages(); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getPackagesForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackagesForUid(param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getNameForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getNameForUid(param2Int); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getNamesForUids(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getNamesForUids(param2ArrayOfint); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getUidForSharedUser(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getUidForSharedUser(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getFlagsForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getFlagsForUid(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPrivateFlagsForUid(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getPrivateFlagsForUid(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isUidPrivileged(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(25, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isUidPrivileged(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ResolveInfo resolveIntent(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().resolveIntent(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ResolveInfo resolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ResolveInfo)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ResolveInfo findPersistentPreferredActivity(Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().findPersistentPreferredActivity(param2Intent, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ResolveInfo resolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ResolveInfo)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canForwardTo(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool1 = true;
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool2 = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().canForwardTo(param2Intent, param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryIntentActivities(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryIntentActivities(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ParceledListSlice)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryIntentActivityOptions(ComponentName param2ComponentName, Intent[] param2ArrayOfIntent, String[] param2ArrayOfString, Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeTypedArray((Parcelable[])param2ArrayOfIntent, 0);
            try {
              parcel1.writeStringArray(param2ArrayOfString);
              if (param2Intent != null) {
                parcel1.writeInt(1);
                param2Intent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeString(param2String);
                parcel1.writeInt(param2Int1);
                parcel1.writeInt(param2Int2);
                boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
                if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
                  ParceledListSlice parceledListSlice = IPackageManager.Stub.getDefaultImpl().queryIntentActivityOptions(param2ComponentName, param2ArrayOfIntent, param2ArrayOfString, param2Intent, param2String, param2Int1, param2Int2);
                  parcel2.recycle();
                  parcel1.recycle();
                  return parceledListSlice;
                } 
                parcel2.readException();
                if (parcel2.readInt() != 0) {
                  ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
                } else {
                  param2ComponentName = null;
                } 
                parcel2.recycle();
                parcel1.recycle();
                return (ParceledListSlice)param2ComponentName;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ComponentName;
      }
      
      public ParceledListSlice queryIntentReceivers(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryIntentReceivers(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ParceledListSlice)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ResolveInfo resolveService(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().resolveService(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ResolveInfo resolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ResolveInfo)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryIntentServices(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryIntentServices(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ParceledListSlice)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryIntentContentProviders(Intent param2Intent, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryIntentContentProviders(param2Intent, param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ParceledListSlice)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getInstalledPackages(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPackageManager.Stub.getDefaultImpl().getInstalledPackages(param2Int1, param2Int2);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getPackagesHoldingPermissions(String[] param2ArrayOfString, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackagesHoldingPermissions(param2ArrayOfString, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2ArrayOfString = null;
          } 
          return (ParceledListSlice)param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getInstalledApplications(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPackageManager.Stub.getDefaultImpl().getInstalledApplications(param2Int1, param2Int2);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getPersistentApplications(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPackageManager.Stub.getDefaultImpl().getPersistentApplications(param2Int);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ProviderInfo resolveContentProvider(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().resolveContentProvider(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ProviderInfo providerInfo = (ProviderInfo)ProviderInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ProviderInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void querySyncProviders(List<String> param2List, List<ProviderInfo> param2List1) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringList(param2List);
          parcel1.writeTypedList(param2List1);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().querySyncProviders(param2List, param2List1);
            return;
          } 
          parcel2.readException();
          parcel2.readStringList(param2List);
          parcel2.readTypedList(param2List1, ProviderInfo.CREATOR);
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryContentProviders(String param2String1, int param2Int1, int param2Int2, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryContentProviders(param2String1, param2Int1, param2Int2, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (ParceledListSlice)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InstrumentationInfo getInstrumentationInfo(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(42, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstrumentationInfo(param2ComponentName, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            InstrumentationInfo instrumentationInfo = (InstrumentationInfo)InstrumentationInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2ComponentName = null;
          } 
          return (InstrumentationInfo)param2ComponentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice queryInstrumentation(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().queryInstrumentation(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void finishPackageInstall(int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().finishPackageInstall(param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setInstallerPackageName(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setInstallerPackageName(param2String1, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setApplicationCategoryHint(String param2String1, int param2Int, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setApplicationCategoryHint(param2String1, param2Int, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deletePackageAsUser(String param2String, int param2Int1, IPackageDeleteObserver param2IPackageDeleteObserver, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          if (param2IPackageDeleteObserver != null) {
            iBinder = param2IPackageDeleteObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deletePackageAsUser(param2String, param2Int1, param2IPackageDeleteObserver, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deletePackageVersioned(VersionedPackage param2VersionedPackage, IPackageDeleteObserver2 param2IPackageDeleteObserver2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2VersionedPackage != null) {
            parcel1.writeInt(1);
            param2VersionedPackage.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IPackageDeleteObserver2 != null) {
            iBinder = param2IPackageDeleteObserver2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deletePackageVersioned(param2VersionedPackage, param2IPackageDeleteObserver2, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteExistingPackageAsUser(VersionedPackage param2VersionedPackage, IPackageDeleteObserver2 param2IPackageDeleteObserver2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2VersionedPackage != null) {
            parcel1.writeInt(1);
            param2VersionedPackage.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IPackageDeleteObserver2 != null) {
            iBinder = param2IPackageDeleteObserver2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(49, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deleteExistingPackageAsUser(param2VersionedPackage, param2IPackageDeleteObserver2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getInstallerPackageName(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(50, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2String = IPackageManager.Stub.getDefaultImpl().getInstallerPackageName(param2String);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public InstallSourceInfo getInstallSourceInfo(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(51, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstallSourceInfo(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            InstallSourceInfo installSourceInfo = (InstallSourceInfo)InstallSourceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (InstallSourceInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetApplicationPreferences(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(52, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().resetApplicationPreferences(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ResolveInfo getLastChosenActivity(Intent param2Intent, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(53, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getLastChosenActivity(param2Intent, param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ResolveInfo resolveInfo = (ResolveInfo)ResolveInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Intent = null;
          } 
          return (ResolveInfo)param2Intent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLastChosenActivity(Intent param2Intent, String param2String, int param2Int1, IntentFilter param2IntentFilter, int param2Int2, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          try {
            parcel1.writeString(param2String);
            try {
              parcel1.writeInt(param2Int1);
              if (param2IntentFilter != null) {
                parcel1.writeInt(1);
                param2IntentFilter.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int2);
                if (param2ComponentName != null) {
                  parcel1.writeInt(1);
                  param2ComponentName.writeToParcel(parcel1, 0);
                } else {
                  parcel1.writeInt(0);
                } 
                boolean bool = this.mRemote.transact(54, parcel1, parcel2, 0);
                if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
                  IPackageManager.Stub.getDefaultImpl().setLastChosenActivity(param2Intent, param2String, param2Int1, param2IntentFilter, param2Int2, param2ComponentName);
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } 
                parcel2.readException();
                parcel2.recycle();
                parcel1.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2Intent;
      }
      
      public void addPreferredActivity(IntentFilter param2IntentFilter, int param2Int1, ComponentName[] param2ArrayOfComponentName, ComponentName param2ComponentName, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IntentFilter != null) {
            parcel1.writeInt(1);
            param2IntentFilter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfComponentName, 0);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(55, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().addPreferredActivity(param2IntentFilter, param2Int1, param2ArrayOfComponentName, param2ComponentName, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void replacePreferredActivity(IntentFilter param2IntentFilter, int param2Int1, ComponentName[] param2ArrayOfComponentName, ComponentName param2ComponentName, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IntentFilter != null) {
            parcel1.writeInt(1);
            param2IntentFilter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeTypedArray((Parcelable[])param2ArrayOfComponentName, 0);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(56, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().replacePreferredActivity(param2IntentFilter, param2Int1, param2ArrayOfComponentName, param2ComponentName, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearPackagePreferredActivities(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(57, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().clearPackagePreferredActivities(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getPreferredActivities(List<IntentFilter> param2List, List<ComponentName> param2List1, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(58, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPreferredActivities(param2List, param2List1, param2String); 
          parcel2.readException();
          int i = parcel2.readInt();
          parcel2.readTypedList(param2List, IntentFilter.CREATOR);
          parcel2.readTypedList(param2List1, ComponentName.CREATOR);
          return i;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPersistentPreferredActivity(IntentFilter param2IntentFilter, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IntentFilter != null) {
            parcel1.writeInt(1);
            param2IntentFilter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(59, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().addPersistentPreferredActivity(param2IntentFilter, param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearPackagePersistentPreferredActivities(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(60, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().clearPackagePersistentPreferredActivities(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addCrossProfileIntentFilter(IntentFilter param2IntentFilter, String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IntentFilter != null) {
            parcel1.writeInt(1);
            param2IntentFilter.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(61, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().addCrossProfileIntentFilter(param2IntentFilter, param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearCrossProfileIntentFilters(int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(62, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().clearCrossProfileIntentFilters(param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] setDistractingPackageRestrictionsAsUser(String[] param2ArrayOfString, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(63, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2ArrayOfString = IPackageManager.Stub.getDefaultImpl().setDistractingPackageRestrictionsAsUser(param2ArrayOfString, param2Int1, param2Int2);
            return param2ArrayOfString;
          } 
          parcel2.readException();
          param2ArrayOfString = parcel2.createStringArray();
          return param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] setPackagesSuspendedAsUser(String[] param2ArrayOfString, boolean param2Boolean, PersistableBundle param2PersistableBundle1, PersistableBundle param2PersistableBundle2, SuspendDialogInfo param2SuspendDialogInfo, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          try {
            boolean bool;
            parcel1.writeStringArray(param2ArrayOfString);
            if (param2Boolean) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel1.writeInt(bool);
            if (param2PersistableBundle1 != null) {
              parcel1.writeInt(1);
              param2PersistableBundle1.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2PersistableBundle2 != null) {
              parcel1.writeInt(1);
              param2PersistableBundle2.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2SuspendDialogInfo != null) {
              parcel1.writeInt(1);
              param2SuspendDialogInfo.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeString(param2String);
              parcel1.writeInt(param2Int);
              boolean bool1 = this.mRemote.transact(64, parcel1, parcel2, 0);
              if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
                param2ArrayOfString = IPackageManager.Stub.getDefaultImpl().setPackagesSuspendedAsUser(param2ArrayOfString, param2Boolean, param2PersistableBundle1, param2PersistableBundle2, param2SuspendDialogInfo, param2String, param2Int);
                parcel2.recycle();
                parcel1.recycle();
                return param2ArrayOfString;
              } 
              parcel2.readException();
              param2ArrayOfString = parcel2.createStringArray();
              parcel2.recycle();
              parcel1.recycle();
              return param2ArrayOfString;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfString;
      }
      
      public String[] getUnsuspendablePackagesForUser(String[] param2ArrayOfString, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(65, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2ArrayOfString = IPackageManager.Stub.getDefaultImpl().getUnsuspendablePackagesForUser(param2ArrayOfString, param2Int);
            return param2ArrayOfString;
          } 
          parcel2.readException();
          param2ArrayOfString = parcel2.createStringArray();
          return param2ArrayOfString;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageSuspendedForUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(66, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageSuspendedForUser(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getSuspendedPackageAppExtras(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(67, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSuspendedPackageAppExtras(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getPreferredActivityBackup(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(68, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPreferredActivityBackup(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restorePreferredActivities(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(69, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().restorePreferredActivities(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getDefaultAppsBackup(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(70, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getDefaultAppsBackup(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreDefaultApps(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(71, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().restoreDefaultApps(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getIntentFilterVerificationBackup(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(72, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getIntentFilterVerificationBackup(param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreIntentFilterVerification(byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(73, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().restoreIntentFilterVerification(param2ArrayOfbyte, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getHomeActivities(List<ResolveInfo> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName1, componentName2;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(74, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            componentName1 = IPackageManager.Stub.getDefaultImpl().getHomeActivities(param2List);
            return componentName1;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName2 = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName2 = null;
          } 
          parcel2.readTypedList((List)componentName1, ResolveInfo.CREATOR);
          return componentName2;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHomeActivity(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(75, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setHomeActivity(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overrideLabelAndIcon(ComponentName param2ComponentName, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(76, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().overrideLabelAndIcon(param2ComponentName, param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void restoreLabelAndIcon(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(77, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().restoreLabelAndIcon(param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setComponentEnabledSetting(ComponentName param2ComponentName, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(78, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setComponentEnabledSetting(param2ComponentName, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getComponentEnabledSetting(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(79, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getComponentEnabledSetting(param2ComponentName, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setApplicationEnabledSetting(String param2String1, int param2Int1, int param2Int2, int param2Int3, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(80, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setApplicationEnabledSetting(param2String1, param2Int1, param2Int2, param2Int3, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getApplicationEnabledSetting(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(81, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getApplicationEnabledSetting(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void logAppProcessStartIfNeeded(String param2String1, int param2Int1, String param2String2, String param2String3, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(82, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().logAppProcessStartIfNeeded(param2String1, param2Int1, param2String2, param2String3, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void flushPackageRestrictionsAsUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(83, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().flushPackageRestrictionsAsUser(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPackageStoppedState(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(84, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setPackageStoppedState(param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void freeStorageAndNotify(String param2String, long param2Long, int param2Int, IPackageDataObserver param2IPackageDataObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          if (param2IPackageDataObserver != null) {
            iBinder = param2IPackageDataObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(85, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().freeStorageAndNotify(param2String, param2Long, param2Int, param2IPackageDataObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void freeStorage(String param2String, long param2Long, int param2Int, IntentSender param2IntentSender) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          if (param2IntentSender != null) {
            parcel1.writeInt(1);
            param2IntentSender.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(86, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().freeStorage(param2String, param2Long, param2Int, param2IntentSender);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteApplicationCacheFiles(String param2String, IPackageDataObserver param2IPackageDataObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2IPackageDataObserver != null) {
            iBinder = param2IPackageDataObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(87, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deleteApplicationCacheFiles(param2String, param2IPackageDataObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deleteApplicationCacheFilesAsUser(String param2String, int param2Int, IPackageDataObserver param2IPackageDataObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2IPackageDataObserver != null) {
            iBinder = param2IPackageDataObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(88, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deleteApplicationCacheFilesAsUser(param2String, param2Int, param2IPackageDataObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearApplicationUserData(String param2String, IPackageDataObserver param2IPackageDataObserver, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2IPackageDataObserver != null) {
            iBinder = param2IPackageDataObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(89, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().clearApplicationUserData(param2String, param2IPackageDataObserver, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void clearApplicationProfileData(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(90, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().clearApplicationProfileData(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void getPackageSizeInfo(String param2String, int param2Int, IPackageStatsObserver param2IPackageStatsObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          if (param2IPackageStatsObserver != null) {
            iBinder = param2IPackageStatsObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(91, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().getPackageSizeInfo(param2String, param2Int, param2IPackageStatsObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getSystemSharedLibraryNames() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(92, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSystemSharedLibraryNames(); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getSystemAvailableFeatures() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(93, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPackageManager.Stub.getDefaultImpl().getSystemAvailableFeatures();
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSystemFeature(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(94, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().hasSystemFeature(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void enterSafeMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(95, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().enterSafeMode();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSafeMode() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(96, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isSafeMode();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void systemReady() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(97, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().systemReady();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSystemUidErrors() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(98, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().hasSystemUidErrors();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void performFstrimIfNeeded() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(99, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().performFstrimIfNeeded();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updatePackagesIfNeeded() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(100, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().updatePackagesIfNeeded();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyPackageUse(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(101, parcel, null, 1);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().notifyPackageUse(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyDexLoad(String param2String1, Map<String, String> param2Map, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel.writeString(param2String1);
          if (param2Map == null) {
            parcel.writeInt(-1);
          } else {
            parcel.writeInt(param2Map.size());
            _$$Lambda$IPackageManager$Stub$Proxy$X2I1qlX4SiKMZSjDTNzS_nTibbo _$$Lambda$IPackageManager$Stub$Proxy$X2I1qlX4SiKMZSjDTNzS_nTibbo = new _$$Lambda$IPackageManager$Stub$Proxy$X2I1qlX4SiKMZSjDTNzS_nTibbo();
            this(parcel);
            param2Map.forEach(_$$Lambda$IPackageManager$Stub$Proxy$X2I1qlX4SiKMZSjDTNzS_nTibbo);
          } 
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(102, parcel, null, 1);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().notifyDexLoad(param2String1, param2Map, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void registerDexModule(String param2String1, String param2String2, boolean param2Boolean, IDexModuleRegisterCallback param2IDexModuleRegisterCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          IBinder iBinder;
          parcel.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2IDexModuleRegisterCallback != null) {
            iBinder = param2IDexModuleRegisterCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool1 = this.mRemote.transact(103, parcel, null, 1);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().registerDexModule(param2String1, param2String2, param2Boolean, param2IDexModuleRegisterCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean performDexOptMode(String param2String1, boolean param2Boolean1, String param2String2, boolean param2Boolean2, boolean param2Boolean3, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          try {
            int i;
            parcel1.writeString(param2String1);
            boolean bool = true;
            if (param2Boolean1) {
              i = 1;
            } else {
              i = 0;
            } 
            parcel1.writeInt(i);
            try {
              parcel1.writeString(param2String2);
              if (param2Boolean2) {
                i = 1;
              } else {
                i = 0;
              } 
              parcel1.writeInt(i);
              if (param2Boolean3) {
                i = 1;
              } else {
                i = 0;
              } 
              parcel1.writeInt(i);
              try {
                parcel1.writeString(param2String3);
                try {
                  boolean bool1 = this.mRemote.transact(104, parcel1, parcel2, 0);
                  if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
                    param2Boolean1 = IPackageManager.Stub.getDefaultImpl().performDexOptMode(param2String1, param2Boolean1, param2String2, param2Boolean2, param2Boolean3, param2String3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return param2Boolean1;
                  } 
                  parcel2.readException();
                  i = parcel2.readInt();
                  if (i != 0) {
                    param2Boolean1 = bool;
                  } else {
                    param2Boolean1 = false;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return param2Boolean1;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public boolean performDexOptSecondary(String param2String1, String param2String2, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(105, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPackageManager.Stub.getDefaultImpl().performDexOptSecondary(param2String1, param2String2, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean compileLayouts(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(106, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().compileLayouts(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dumpProfiles(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(107, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().dumpProfiles(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void forceDexOpt(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(108, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().forceDexOpt(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean runBackgroundDexoptJob(List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringList(param2List);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(109, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().runBackgroundDexoptJob(param2List);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void reconcileSecondaryDexFiles(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(110, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().reconcileSecondaryDexFiles(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getMoveStatus(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(111, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getMoveStatus(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerMoveCallback(IPackageMoveObserver param2IPackageMoveObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IPackageMoveObserver != null) {
            iBinder = param2IPackageMoveObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(112, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().registerMoveCallback(param2IPackageMoveObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterMoveCallback(IPackageMoveObserver param2IPackageMoveObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          if (param2IPackageMoveObserver != null) {
            iBinder = param2IPackageMoveObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(113, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().unregisterMoveCallback(param2IPackageMoveObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int movePackage(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(114, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().movePackage(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int movePrimaryStorage(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(115, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().movePrimaryStorage(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setInstallLocation(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(116, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().setInstallLocation(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInstallLocation() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(117, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstallLocation(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int installExistingPackageAsUser(String param2String, int param2Int1, int param2Int2, int param2Int3, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(118, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int1 = IPackageManager.Stub.getDefaultImpl().installExistingPackageAsUser(param2String, param2Int1, param2Int2, param2Int3, param2List);
            return param2Int1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          return param2Int1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void verifyPendingInstall(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(119, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().verifyPendingInstall(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void extendVerificationTimeout(int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(120, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().extendVerificationTimeout(param2Int1, param2Int2, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void verifyIntentFilter(int param2Int1, int param2Int2, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(121, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().verifyIntentFilter(param2Int1, param2Int2, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getIntentVerificationStatus(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(122, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getIntentVerificationStatus(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateIntentVerificationStatus(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(123, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().updateIntentVerificationStatus(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getIntentFilterVerifications(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(124, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getIntentFilterVerifications(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getAllIntentFilters(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(125, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getAllIntentFilters(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public VerifierDeviceIdentity getVerifierDeviceIdentity() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          VerifierDeviceIdentity verifierDeviceIdentity;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(126, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            verifierDeviceIdentity = IPackageManager.Stub.getDefaultImpl().getVerifierDeviceIdentity();
            return verifierDeviceIdentity;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            verifierDeviceIdentity = (VerifierDeviceIdentity)VerifierDeviceIdentity.CREATOR.createFromParcel(parcel2);
          } else {
            verifierDeviceIdentity = null;
          } 
          return verifierDeviceIdentity;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isFirstBoot() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(127, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isFirstBoot();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isOnlyCoreApps() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(128, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isOnlyCoreApps();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isDeviceUpgrading() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(129, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isDeviceUpgrading();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isStorageLow() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(130, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isStorageLow();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setApplicationHiddenSettingAsUser(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(131, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPackageManager.Stub.getDefaultImpl().setApplicationHiddenSettingAsUser(param2String, param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getApplicationHiddenSettingAsUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(132, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().getApplicationHiddenSettingAsUser(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSystemAppHiddenUntilInstalled(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(133, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setSystemAppHiddenUntilInstalled(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setSystemAppInstallState(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(134, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPackageManager.Stub.getDefaultImpl().setSystemAppInstallState(param2String, param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IPackageInstaller getPackageInstaller() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(135, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPackageInstaller(); 
          parcel2.readException();
          return IPackageInstaller.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setBlockUninstallForUser(String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(136, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPackageManager.Stub.getDefaultImpl().setBlockUninstallForUser(param2String, param2Boolean, param2Int);
            return param2Boolean;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0) {
            param2Boolean = bool1;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getBlockUninstallForUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(137, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().getBlockUninstallForUser(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeySet getKeySetByAlias(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(138, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getKeySetByAlias(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            KeySet keySet = (KeySet)KeySet.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (KeySet)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public KeySet getSigningKeySet(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(139, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSigningKeySet(param2String); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            KeySet keySet = (KeySet)KeySet.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (KeySet)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageSignedByKeySet(String param2String, KeySet param2KeySet) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2KeySet != null) {
            parcel1.writeInt(1);
            param2KeySet.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(140, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageSignedByKeySet(param2String, param2KeySet);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageSignedByKeySetExactly(String param2String, KeySet param2KeySet) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2KeySet != null) {
            parcel1.writeInt(1);
            param2KeySet.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(141, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageSignedByKeySetExactly(param2String, param2KeySet);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPermissionControllerPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(142, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPermissionControllerPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getInstantApps(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(143, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = IPackageManager.Stub.getDefaultImpl().getInstantApps(param2Int);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public byte[] getInstantAppCookie(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(144, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstantAppCookie(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createByteArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setInstantAppCookie(String param2String, byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(145, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().setInstantAppCookie(param2String, param2ArrayOfbyte, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bitmap getInstantAppIcon(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(146, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstantAppIcon(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bitmap bitmap = (Bitmap)Bitmap.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bitmap)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isInstantApp(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(147, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isInstantApp(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean setRequiredForSystemUser(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = true;
          if (param2Boolean) {
            i = 1;
          } else {
            i = 0;
          } 
          parcel1.writeInt(i);
          boolean bool1 = this.mRemote.transact(148, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Boolean = IPackageManager.Stub.getDefaultImpl().setRequiredForSystemUser(param2String, param2Boolean);
            return param2Boolean;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0) {
            param2Boolean = bool;
          } else {
            param2Boolean = false;
          } 
          return param2Boolean;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setUpdateAvailable(String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(149, parcel1, parcel2, 0);
          if (!bool1 && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setUpdateAvailable(param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getServicesSystemSharedLibraryPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(150, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getServicesSystemSharedLibraryPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSharedSystemSharedLibraryPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(151, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSharedSystemSharedLibraryPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ChangedPackages getChangedPackages(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ChangedPackages changedPackages;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(152, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            changedPackages = IPackageManager.Stub.getDefaultImpl().getChangedPackages(param2Int1, param2Int2);
            return changedPackages;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            changedPackages = (ChangedPackages)ChangedPackages.CREATOR.createFromParcel(parcel2);
          } else {
            changedPackages = null;
          } 
          return changedPackages;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageDeviceAdminOnAnyUser(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(153, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageDeviceAdminOnAnyUser(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getInstallReason(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(154, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getInstallReason(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getSharedLibraries(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(155, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSharedLibraries(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getDeclaredSharedLibraries(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(156, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getDeclaredSharedLibraries(param2String, param2Int1, param2Int2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParceledListSlice parceledListSlice = (ParceledListSlice)ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ParceledListSlice)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean canRequestPackageInstalls(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(157, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().canRequestPackageInstalls(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deletePreloadsFileCache() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(158, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().deletePreloadsFileCache();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getInstantAppResolverComponent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(159, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            componentName = IPackageManager.Stub.getDefaultImpl().getInstantAppResolverComponent();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getInstantAppResolverSettingsComponent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(160, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            componentName = IPackageManager.Stub.getDefaultImpl().getInstantAppResolverSettingsComponent();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ComponentName getInstantAppInstallerComponent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ComponentName componentName;
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(161, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            componentName = IPackageManager.Stub.getDefaultImpl().getInstantAppInstallerComponent();
            return componentName;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            componentName = (ComponentName)ComponentName.CREATOR.createFromParcel(parcel2);
          } else {
            componentName = null;
          } 
          return componentName;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getInstantAppAndroidId(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(162, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2String = IPackageManager.Stub.getDefaultImpl().getInstantAppAndroidId(param2String, param2Int);
            return param2String;
          } 
          parcel2.readException();
          param2String = parcel2.readString();
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public IArtManager getArtManager() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(163, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getArtManager(); 
          parcel2.readException();
          return IArtManager.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setHarmfulAppWarning(String param2String, CharSequence param2CharSequence, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(164, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setHarmfulAppWarning(param2String, param2CharSequence, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CharSequence getHarmfulAppWarning(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(165, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getHarmfulAppWarning(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasSigningCertificate(String param2String, byte[] param2ArrayOfbyte, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(166, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().hasSigningCertificate(param2String, param2ArrayOfbyte, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean hasUidSigningCertificate(int param2Int1, byte[] param2ArrayOfbyte, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeByteArray(param2ArrayOfbyte);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(167, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().hasUidSigningCertificate(param2Int1, param2ArrayOfbyte, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getDefaultTextClassifierPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(168, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getDefaultTextClassifierPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSystemTextClassifierPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(169, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSystemTextClassifierPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAttentionServicePackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(170, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getAttentionServicePackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getWellbeingPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(171, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getWellbeingPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getAppPredictionServicePackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(172, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getAppPredictionServicePackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSystemCaptionsServicePackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(173, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSystemCaptionsServicePackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getSetupWizardPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(174, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getSetupWizardPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getIncidentReportApproverPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(175, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getIncidentReportApproverPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getContentCaptureServicePackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(176, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getContentCaptureServicePackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPackageStateProtected(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(177, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isPackageStateProtected(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendDeviceCustomizationReadyBroadcast() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool = this.mRemote.transact(178, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().sendDeviceCustomizationReadyBroadcast();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<ModuleInfo> getInstalledModules(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(179, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getInstalledModules(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(ModuleInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ModuleInfo getModuleInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(180, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getModuleInfo(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ModuleInfo moduleInfo = (ModuleInfo)ModuleInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (ModuleInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRuntimePermissionsVersion(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(181, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().getRuntimePermissionsVersion(param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRuntimePermissionsVersion(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(182, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setRuntimePermissionsVersion(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyPackagesReplacedReceived(String[] param2ArrayOfString) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeStringArray(param2ArrayOfString);
          boolean bool = this.mRemote.transact(183, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().notifyPackagesReplacedReceived(param2ArrayOfString);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getAppOpPermissionPackages(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(184, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getAppOpPermissionPackages(param2String); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PermissionGroupInfo getPermissionGroupInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(185, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getPermissionGroupInfo(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            PermissionGroupInfo permissionGroupInfo = (PermissionGroupInfo)PermissionGroupInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (PermissionGroupInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addPermission(PermissionInfo param2PermissionInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool1 = true;
          if (param2PermissionInfo != null) {
            parcel1.writeInt(1);
            param2PermissionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(186, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().addPermission(param2PermissionInfo);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean addPermissionAsync(PermissionInfo param2PermissionInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          boolean bool1 = true;
          if (param2PermissionInfo != null) {
            parcel1.writeInt(1);
            param2PermissionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(187, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().addPermissionAsync(param2PermissionInfo);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePermission(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(188, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().removePermission(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkPermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(189, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().checkPermission(param2String1, param2String2, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void grantRuntimePermission(String param2String1, String param2String2, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(190, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().grantRuntimePermission(param2String1, param2String2, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int checkUidPermission(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(191, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            param2Int = IPackageManager.Stub.getDefaultImpl().checkUidPermission(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMimeGroup(String param2String1, String param2String2, List<String> param2List) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeStringList(param2List);
          boolean bool = this.mRemote.transact(192, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null) {
            IPackageManager.Stub.getDefaultImpl().setMimeGroup(param2String1, param2String2, param2List);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getMimeGroup(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(193, parcel1, parcel2, 0);
          if (!bool && IPackageManager.Stub.getDefaultImpl() != null)
            return IPackageManager.Stub.getDefaultImpl().getMimeGroup(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isAutoRevokeWhitelisted(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.pm.IPackageManager");
          parcel1.writeString(param2String);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(194, parcel1, parcel2, 0);
          if (!bool2 && IPackageManager.Stub.getDefaultImpl() != null) {
            bool1 = IPackageManager.Stub.getDefaultImpl().isAutoRevokeWhitelisted(param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPackageManager param1IPackageManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPackageManager != null) {
          Proxy.sDefaultImpl = param1IPackageManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPackageManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
