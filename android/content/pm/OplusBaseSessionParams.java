package android.content.pm;

import android.os.Parcel;

public class OplusBaseSessionParams {
  public int extraInstallFlags;
  
  public String extraSessionInfo;
  
  public void initFromParcel(Parcel paramParcel) {
    this.extraInstallFlags = paramParcel.readInt();
    this.extraSessionInfo = paramParcel.readString();
  }
  
  public void setExtraSessionInfo(String paramString) {
    this.extraSessionInfo = paramString;
  }
  
  public void setExtraInstallFlags(int paramInt) {
    this.extraInstallFlags |= paramInt;
  }
  
  public void baseWriteToParcel(Parcel paramParcel) {
    paramParcel.writeInt(this.extraInstallFlags);
    paramParcel.writeString(this.extraSessionInfo);
  }
}
