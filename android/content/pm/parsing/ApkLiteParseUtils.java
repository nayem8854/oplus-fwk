package android.content.pm.parsing;

import android.content.pm.PackageParser;
import android.content.pm.VerifierInfo;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.os.Trace;
import android.util.ArrayMap;
import android.util.AttributeSet;
import android.util.Pair;
import android.util.Slog;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Arrays;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ApkLiteParseUtils {
  private static final int DEFAULT_MIN_SDK_VERSION = 1;
  
  private static final int DEFAULT_TARGET_SDK_VERSION = 0;
  
  private static final int PARSE_DEFAULT_INSTALL_LOCATION = -1;
  
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<PackageParser.PackageLite> parsePackageLite(ParseInput paramParseInput, File paramFile, int paramInt) {
    if (paramFile.isDirectory())
      return parseClusterPackageLite(paramParseInput, paramFile, paramInt); 
    return parseMonolithicPackageLite(paramParseInput, paramFile, paramInt);
  }
  
  public static ParseResult<PackageParser.PackageLite> parseMonolithicPackageLite(ParseInput paramParseInput, File paramFile, int paramInt) {
    Trace.traceBegin(262144L, "parseApkLite");
    try {
      ParseResult<PackageParser.ApkLite> parseResult1 = parseApkLite(paramParseInput, paramFile, paramInt);
      if (parseResult1.isError()) {
        parseResult = paramParseInput.error(parseResult1);
        return (ParseResult)parseResult;
      } 
      PackageParser.ApkLite apkLite = parseResult1.getResult();
      String str = paramFile.getAbsolutePath();
      PackageParser.PackageLite packageLite = new PackageParser.PackageLite();
      this(str, apkLite, null, null, null, null, null, null);
      ParseResult<?> parseResult = parseResult.success(packageLite);
      return (ParseResult)parseResult;
    } finally {
      Trace.traceEnd(262144L);
    } 
  }
  
  public static ParseResult<PackageParser.PackageLite> parseClusterPackageLite(ParseInput paramParseInput, File paramFile, int paramInt) {
    File[] arrayOfFile = paramFile.listFiles();
    if (ArrayUtils.isEmpty((Object[])arrayOfFile))
      return paramParseInput.error(-100, "No packages found in split"); 
    int i = arrayOfFile.length;
    byte b = 0;
    if (i == 1 && arrayOfFile[0].isDirectory())
      return parseClusterPackageLite(paramParseInput, arrayOfFile[0], paramInt); 
    ParseResult<PackageParser.ApkLite> parseResult = null;
    i = 0;
    ArrayMap arrayMap = new ArrayMap();
    Trace.traceBegin(262144L, "parseApkLite");
    try {
      int j = arrayOfFile.length;
      while (true) {
        StringBuilder stringBuilder;
        if (b < j) {
          File file = arrayOfFile[b];
          ParseResult<PackageParser.ApkLite> parseResult1 = parseResult;
          int k = i;
          try {
            String str2;
            if (PackageParser.isApkFile(file)) {
              ParseResult<?> parseResult2;
              String str;
              parseResult1 = parseApkLite(paramParseInput, file, paramInt);
              if (parseResult1.isError()) {
                parseResult2 = paramParseInput.error(parseResult1);
                Trace.traceEnd(262144L);
                return (ParseResult)parseResult2;
              } 
              PackageParser.ApkLite apkLite = parseResult1.getResult();
              if (parseResult == null) {
                str = apkLite.packageName;
                k = apkLite.versionCode;
              } else {
                boolean bool = str.equals(apkLite.packageName);
                if (!bool) {
                  stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Inconsistent package ");
                  stringBuilder.append(apkLite.packageName);
                  stringBuilder.append(" in ");
                  stringBuilder.append(file);
                  stringBuilder.append("; expected ");
                  stringBuilder.append(str);
                  parseResult2 = parseResult2.error(-101, stringBuilder.toString());
                  Trace.traceEnd(262144L);
                  return (ParseResult)parseResult2;
                } 
                k = i;
                if (i != apkLite.versionCode) {
                  stringBuilder = new StringBuilder();
                  this();
                  stringBuilder.append("Inconsistent version ");
                  stringBuilder.append(apkLite.versionCode);
                  stringBuilder.append(" in ");
                  stringBuilder.append(file);
                  stringBuilder.append("; expected ");
                  stringBuilder.append(i);
                  parseResult2 = parseResult2.error(-101, stringBuilder.toString());
                  Trace.traceEnd(262144L);
                  return (ParseResult)parseResult2;
                } 
              } 
              str2 = str;
              if (arrayMap.put(apkLite.splitName, apkLite) != null) {
                stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Split name ");
                stringBuilder.append(apkLite.splitName);
                stringBuilder.append(" defined more than once; most recent was ");
                stringBuilder.append(file);
                parseResult2 = parseResult2.error(-101, stringBuilder.toString());
                Trace.traceEnd(262144L);
                return (ParseResult)parseResult2;
              } 
            } 
            b++;
            String str1 = str2;
            i = k;
            continue;
          } finally {}
        } else {
          String[] arrayOfString1;
          int[] arrayOfInt;
          String[] arrayOfString3;
          Trace.traceEnd(262144L);
          PackageParser.ApkLite apkLite = (PackageParser.ApkLite)arrayMap.remove(null);
          if (apkLite == null) {
            StringBuilder stringBuilder1 = new StringBuilder();
            stringBuilder1.append("Missing base APK in ");
            stringBuilder1.append(stringBuilder);
            return paramParseInput.error(-101, stringBuilder1.toString());
          } 
          i = arrayMap.size();
          String[] arrayOfString2 = null;
          boolean[] arrayOfBoolean = null;
          if (i > 0) {
            String[] arrayOfString5 = new String[i];
            arrayOfBoolean = new boolean[i];
            arrayOfString2 = new String[i];
            arrayOfString3 = new String[i];
            String[] arrayOfString6 = new String[i];
            arrayOfInt = new int[i];
            String[] arrayOfString7 = (String[])arrayMap.keySet().toArray((Object[])arrayOfString5);
            Arrays.sort(arrayOfString7, PackageParser.sSplitNameComparator);
            File[] arrayOfFile1;
            for (paramInt = 0, arrayOfFile1 = arrayOfFile; paramInt < i; paramInt++) {
              PackageParser.ApkLite apkLite1 = (PackageParser.ApkLite)arrayMap.get(arrayOfString7[paramInt]);
              arrayOfString2[paramInt] = apkLite1.usesSplitName;
              arrayOfBoolean[paramInt] = apkLite1.isFeatureSplit;
              arrayOfString3[paramInt] = apkLite1.configForSplit;
              arrayOfString6[paramInt] = apkLite1.codePath;
              arrayOfInt[paramInt] = apkLite1.revisionCode;
            } 
            String[] arrayOfString4 = arrayOfString6;
            arrayOfString1 = arrayOfString7;
          } else {
            String[] arrayOfString = null;
            arrayOfString3 = null;
            parseResult = null;
            arrayOfInt = null;
            arrayOfString1 = arrayOfString2;
            arrayOfString2 = arrayOfString;
          } 
          String str = stringBuilder.getAbsolutePath();
          return paramParseInput.success(new PackageParser.PackageLite(str, apkLite, arrayOfString1, arrayOfBoolean, arrayOfString2, arrayOfString3, (String[])parseResult, arrayOfInt));
        } 
        Trace.traceEnd(262144L);
        throw paramParseInput;
      } 
    } finally {}
    Trace.traceEnd(262144L);
    throw paramParseInput;
  }
  
  public static ParseResult<PackageParser.ApkLite> parseApkLite(ParseInput paramParseInput, File paramFile, int paramInt) {
    return parseApkLiteInner(paramParseInput, paramFile, null, null, paramInt);
  }
  
  public static ParseResult<PackageParser.ApkLite> parseApkLite(ParseInput paramParseInput, FileDescriptor paramFileDescriptor, String paramString, int paramInt) {
    return parseApkLiteInner(paramParseInput, null, paramFileDescriptor, paramString, paramInt);
  }
  
  private static ParseResult<PackageParser.ApkLite> parseApkLiteInner(ParseInput paramParseInput, File paramFile, FileDescriptor paramFileDescriptor, String paramString, int paramInt) {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 10
    //   4: aload_3
    //   5: astore #5
    //   7: goto -> 16
    //   10: aload_1
    //   11: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   14: astore #5
    //   16: aconst_null
    //   17: astore #6
    //   19: aconst_null
    //   20: astore #7
    //   22: aconst_null
    //   23: astore #8
    //   25: aconst_null
    //   26: astore #9
    //   28: aconst_null
    //   29: astore #10
    //   31: iconst_0
    //   32: istore #11
    //   34: aload_2
    //   35: ifnull -> 57
    //   38: aload #8
    //   40: astore #12
    //   42: aload #10
    //   44: astore #13
    //   46: aload_2
    //   47: aload_3
    //   48: iconst_0
    //   49: aconst_null
    //   50: invokestatic loadFromFd : (Ljava/io/FileDescriptor;Ljava/lang/String;ILandroid/content/res/loader/AssetsProvider;)Landroid/content/res/ApkAssets;
    //   53: astore_2
    //   54: goto -> 71
    //   57: aload #8
    //   59: astore #12
    //   61: aload #10
    //   63: astore #13
    //   65: aload #5
    //   67: invokestatic loadFromPath : (Ljava/lang/String;)Landroid/content/res/ApkAssets;
    //   70: astore_2
    //   71: aload_2
    //   72: ldc 'AndroidManifest.xml'
    //   74: invokevirtual openXml : (Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    //   77: astore_3
    //   78: iload #4
    //   80: bipush #32
    //   82: iand
    //   83: ifeq -> 197
    //   86: iload #4
    //   88: bipush #16
    //   90: iand
    //   91: ifeq -> 97
    //   94: iconst_1
    //   95: istore #11
    //   97: ldc2_w 262144
    //   100: ldc_w 'collectCertificates'
    //   103: invokestatic traceBegin : (JLjava/lang/String;)V
    //   106: aload_1
    //   107: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   110: astore_1
    //   111: getstatic android/content/pm/PackageParser$SigningDetails.UNKNOWN : Landroid/content/pm/PackageParser$SigningDetails;
    //   114: astore #13
    //   116: aload_0
    //   117: aload_1
    //   118: iload #11
    //   120: iconst_0
    //   121: aload #13
    //   123: iconst_0
    //   124: invokestatic getSigningDetails : (Landroid/content/pm/parsing/result/ParseInput;Ljava/lang/String;ZZLandroid/content/pm/PackageParser$SigningDetails;I)Landroid/content/pm/parsing/result/ParseResult;
    //   127: astore_1
    //   128: aload_1
    //   129: invokeinterface isError : ()Z
    //   134: ifeq -> 169
    //   137: aload_0
    //   138: aload_1
    //   139: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   144: astore_1
    //   145: ldc2_w 262144
    //   148: invokestatic traceEnd : (J)V
    //   151: aload_3
    //   152: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   155: aload_2
    //   156: ifnull -> 167
    //   159: aload_2
    //   160: invokevirtual close : ()V
    //   163: goto -> 167
    //   166: astore_0
    //   167: aload_1
    //   168: areturn
    //   169: aload_1
    //   170: invokeinterface getResult : ()Ljava/lang/Object;
    //   175: checkcast android/content/pm/PackageParser$SigningDetails
    //   178: astore_1
    //   179: ldc2_w 262144
    //   182: invokestatic traceEnd : (J)V
    //   185: goto -> 201
    //   188: astore_1
    //   189: ldc2_w 262144
    //   192: invokestatic traceEnd : (J)V
    //   195: aload_1
    //   196: athrow
    //   197: getstatic android/content/pm/PackageParser$SigningDetails.UNKNOWN : Landroid/content/pm/PackageParser$SigningDetails;
    //   200: astore_1
    //   201: aload_0
    //   202: aload #5
    //   204: aload_3
    //   205: aload_3
    //   206: aload_1
    //   207: invokestatic parseApkLite : (Landroid/content/pm/parsing/result/ParseInput;Ljava/lang/String;Lorg/xmlpull/v1/XmlPullParser;Landroid/util/AttributeSet;Landroid/content/pm/PackageParser$SigningDetails;)Landroid/content/pm/parsing/result/ParseResult;
    //   210: astore_1
    //   211: aload_3
    //   212: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   215: aload_2
    //   216: ifnull -> 227
    //   219: aload_2
    //   220: invokevirtual close : ()V
    //   223: goto -> 227
    //   226: astore_0
    //   227: aload_1
    //   228: areturn
    //   229: astore_0
    //   230: aload_3
    //   231: astore #12
    //   233: aload_2
    //   234: astore #13
    //   236: goto -> 541
    //   239: astore #13
    //   241: aload_2
    //   242: astore_1
    //   243: aload #13
    //   245: astore_2
    //   246: goto -> 382
    //   249: astore_0
    //   250: aload #7
    //   252: astore #12
    //   254: aload_2
    //   255: astore #13
    //   257: goto -> 541
    //   260: astore_3
    //   261: aload_2
    //   262: astore_1
    //   263: aload_3
    //   264: astore_2
    //   265: aload #6
    //   267: astore_3
    //   268: goto -> 382
    //   271: astore_0
    //   272: goto -> 541
    //   275: astore_2
    //   276: aload #6
    //   278: astore_3
    //   279: aload #9
    //   281: astore_1
    //   282: goto -> 382
    //   285: astore_1
    //   286: aload #8
    //   288: astore #12
    //   290: aload #10
    //   292: astore #13
    //   294: new java/lang/StringBuilder
    //   297: astore_2
    //   298: aload #8
    //   300: astore #12
    //   302: aload #10
    //   304: astore #13
    //   306: aload_2
    //   307: invokespecial <init> : ()V
    //   310: aload #8
    //   312: astore #12
    //   314: aload #10
    //   316: astore #13
    //   318: aload_2
    //   319: ldc_w 'Failed to parse '
    //   322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   325: pop
    //   326: aload #8
    //   328: astore #12
    //   330: aload #10
    //   332: astore #13
    //   334: aload_2
    //   335: aload #5
    //   337: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: aload #8
    //   343: astore #12
    //   345: aload #10
    //   347: astore #13
    //   349: aload_0
    //   350: bipush #-100
    //   352: aload_2
    //   353: invokevirtual toString : ()Ljava/lang/String;
    //   356: aload_1
    //   357: invokeinterface error : (ILjava/lang/String;Ljava/lang/Exception;)Landroid/content/pm/parsing/result/ParseResult;
    //   362: astore_1
    //   363: aconst_null
    //   364: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   367: iconst_0
    //   368: ifeq -> 380
    //   371: new java/lang/NullPointerException
    //   374: dup
    //   375: invokespecial <init> : ()V
    //   378: athrow
    //   379: astore_0
    //   380: aload_1
    //   381: areturn
    //   382: aload_3
    //   383: astore #12
    //   385: aload_1
    //   386: astore #13
    //   388: new java/lang/StringBuilder
    //   391: astore #6
    //   393: aload_3
    //   394: astore #12
    //   396: aload_1
    //   397: astore #13
    //   399: aload #6
    //   401: invokespecial <init> : ()V
    //   404: aload_3
    //   405: astore #12
    //   407: aload_1
    //   408: astore #13
    //   410: aload #6
    //   412: ldc_w 'Failed to parse '
    //   415: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload_3
    //   420: astore #12
    //   422: aload_1
    //   423: astore #13
    //   425: aload #6
    //   427: aload #5
    //   429: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   432: pop
    //   433: aload_3
    //   434: astore #12
    //   436: aload_1
    //   437: astore #13
    //   439: ldc 'PackageParsing'
    //   441: aload #6
    //   443: invokevirtual toString : ()Ljava/lang/String;
    //   446: aload_2
    //   447: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   450: pop
    //   451: aload_3
    //   452: astore #12
    //   454: aload_1
    //   455: astore #13
    //   457: new java/lang/StringBuilder
    //   460: astore #6
    //   462: aload_3
    //   463: astore #12
    //   465: aload_1
    //   466: astore #13
    //   468: aload #6
    //   470: invokespecial <init> : ()V
    //   473: aload_3
    //   474: astore #12
    //   476: aload_1
    //   477: astore #13
    //   479: aload #6
    //   481: ldc_w 'Failed to parse '
    //   484: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   487: pop
    //   488: aload_3
    //   489: astore #12
    //   491: aload_1
    //   492: astore #13
    //   494: aload #6
    //   496: aload #5
    //   498: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   501: pop
    //   502: aload_3
    //   503: astore #12
    //   505: aload_1
    //   506: astore #13
    //   508: aload_0
    //   509: bipush #-102
    //   511: aload #6
    //   513: invokevirtual toString : ()Ljava/lang/String;
    //   516: aload_2
    //   517: invokeinterface error : (ILjava/lang/String;Ljava/lang/Exception;)Landroid/content/pm/parsing/result/ParseResult;
    //   522: astore_0
    //   523: aload_3
    //   524: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   527: aload_1
    //   528: ifnull -> 539
    //   531: aload_1
    //   532: invokevirtual close : ()V
    //   535: goto -> 539
    //   538: astore_1
    //   539: aload_0
    //   540: areturn
    //   541: aload #12
    //   543: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   546: aload #13
    //   548: ifnull -> 560
    //   551: aload #13
    //   553: invokevirtual close : ()V
    //   556: goto -> 560
    //   559: astore_1
    //   560: aload_0
    //   561: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #231	-> 0
    //   #233	-> 16
    //   #234	-> 25
    //   #237	-> 31
    //   #238	-> 38
    //   #239	-> 57
    //   #243	-> 71
    //   #245	-> 71
    //   #248	-> 78
    //   #249	-> 86
    //   #250	-> 97
    //   #252	-> 106
    //   #254	-> 106
    //   #253	-> 116
    //   #257	-> 128
    //   #258	-> 137
    //   #262	-> 145
    //   #275	-> 151
    //   #276	-> 155
    //   #278	-> 159
    //   #280	-> 163
    //   #279	-> 166
    //   #258	-> 167
    //   #260	-> 169
    //   #262	-> 179
    //   #263	-> 185
    //   #264	-> 185
    //   #262	-> 188
    //   #263	-> 195
    //   #265	-> 197
    //   #268	-> 201
    //   #269	-> 201
    //   #275	-> 211
    //   #276	-> 215
    //   #278	-> 219
    //   #280	-> 223
    //   #279	-> 226
    //   #269	-> 227
    //   #275	-> 229
    //   #270	-> 239
    //   #275	-> 249
    //   #270	-> 260
    //   #275	-> 271
    //   #270	-> 275
    //   #240	-> 285
    //   #241	-> 286
    //   #275	-> 363
    //   #276	-> 367
    //   #278	-> 371
    //   #280	-> 379
    //   #279	-> 379
    //   #241	-> 380
    //   #270	-> 382
    //   #271	-> 382
    //   #272	-> 451
    //   #275	-> 523
    //   #276	-> 527
    //   #278	-> 531
    //   #280	-> 535
    //   #279	-> 538
    //   #272	-> 539
    //   #275	-> 541
    //   #276	-> 546
    //   #278	-> 551
    //   #280	-> 556
    //   #279	-> 559
    //   #283	-> 560
    // Exception table:
    //   from	to	target	type
    //   46	54	285	java/io/IOException
    //   46	54	275	org/xmlpull/v1/XmlPullParserException
    //   46	54	275	java/lang/RuntimeException
    //   46	54	271	finally
    //   65	71	285	java/io/IOException
    //   65	71	275	org/xmlpull/v1/XmlPullParserException
    //   65	71	275	java/lang/RuntimeException
    //   65	71	271	finally
    //   71	78	260	org/xmlpull/v1/XmlPullParserException
    //   71	78	260	java/io/IOException
    //   71	78	260	java/lang/RuntimeException
    //   71	78	249	finally
    //   97	106	239	org/xmlpull/v1/XmlPullParserException
    //   97	106	239	java/io/IOException
    //   97	106	239	java/lang/RuntimeException
    //   97	106	229	finally
    //   106	116	188	finally
    //   116	128	188	finally
    //   128	137	188	finally
    //   137	145	188	finally
    //   145	151	239	org/xmlpull/v1/XmlPullParserException
    //   145	151	239	java/io/IOException
    //   145	151	239	java/lang/RuntimeException
    //   145	151	229	finally
    //   159	163	166	finally
    //   169	179	188	finally
    //   179	185	239	org/xmlpull/v1/XmlPullParserException
    //   179	185	239	java/io/IOException
    //   179	185	239	java/lang/RuntimeException
    //   179	185	229	finally
    //   189	195	239	org/xmlpull/v1/XmlPullParserException
    //   189	195	239	java/io/IOException
    //   189	195	239	java/lang/RuntimeException
    //   189	195	229	finally
    //   195	197	239	org/xmlpull/v1/XmlPullParserException
    //   195	197	239	java/io/IOException
    //   195	197	239	java/lang/RuntimeException
    //   195	197	229	finally
    //   197	201	239	org/xmlpull/v1/XmlPullParserException
    //   197	201	239	java/io/IOException
    //   197	201	239	java/lang/RuntimeException
    //   197	201	229	finally
    //   201	211	239	org/xmlpull/v1/XmlPullParserException
    //   201	211	239	java/io/IOException
    //   201	211	239	java/lang/RuntimeException
    //   201	211	229	finally
    //   219	223	226	finally
    //   294	298	275	org/xmlpull/v1/XmlPullParserException
    //   294	298	275	java/io/IOException
    //   294	298	275	java/lang/RuntimeException
    //   294	298	271	finally
    //   306	310	275	org/xmlpull/v1/XmlPullParserException
    //   306	310	275	java/io/IOException
    //   306	310	275	java/lang/RuntimeException
    //   306	310	271	finally
    //   318	326	275	org/xmlpull/v1/XmlPullParserException
    //   318	326	275	java/io/IOException
    //   318	326	275	java/lang/RuntimeException
    //   318	326	271	finally
    //   334	341	275	org/xmlpull/v1/XmlPullParserException
    //   334	341	275	java/io/IOException
    //   334	341	275	java/lang/RuntimeException
    //   334	341	271	finally
    //   349	363	275	org/xmlpull/v1/XmlPullParserException
    //   349	363	275	java/io/IOException
    //   349	363	275	java/lang/RuntimeException
    //   349	363	271	finally
    //   371	379	379	finally
    //   388	393	271	finally
    //   399	404	271	finally
    //   410	419	271	finally
    //   425	433	271	finally
    //   439	451	271	finally
    //   457	462	271	finally
    //   468	473	271	finally
    //   479	488	271	finally
    //   494	502	271	finally
    //   508	523	271	finally
    //   531	535	538	finally
    //   551	556	559	finally
  }
  
  private static ParseResult<PackageParser.ApkLite> parseApkLite(ParseInput paramParseInput, String paramString, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet, PackageParser.SigningDetails paramSigningDetails) throws IOException, XmlPullParserException {
    StringBuilder stringBuilder;
    ParseResult<Pair<String, String>> parseResult = parsePackageSplitNames(paramParseInput, paramXmlPullParser, paramAttributeSet);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    Pair pair = parseResult.getResult();
    int i = -1;
    int j = 0;
    int k = 0;
    int m = 0;
    int n = 0;
    boolean bool1 = false;
    boolean bool2 = false;
    boolean bool3 = false;
    boolean bool4 = false;
    String str2 = null;
    boolean bool5 = false;
    int i1 = 0;
    int i2;
    for (i2 = 0; i2 < paramAttributeSet.getAttributeCount(); i2++) {
      byte b;
      String str = paramAttributeSet.getAttributeName(i2);
      switch (str.hashCode()) {
        default:
          b = -1;
          break;
        case 1566947635:
          if (str.equals("configForSplit")) {
            b = 6;
            break;
          } 
        case 954743298:
          if (str.equals("coreApp")) {
            b = 4;
            break;
          } 
        case 688591589:
          if (str.equals("versionCode")) {
            b = 1;
            break;
          } 
        case 568084431:
          if (str.equals("isSplitRequired")) {
            b = 8;
            break;
          } 
        case 436669454:
          if (str.equals("isFeatureSplit")) {
            b = 7;
            break;
          } 
        case 138459604:
          if (str.equals("versionCodeMajor")) {
            b = 2;
            break;
          } 
        case 89284208:
          if (str.equals("installLocation")) {
            b = 0;
            break;
          } 
        case -403639534:
          if (str.equals("isolatedSplits")) {
            b = 5;
            break;
          } 
        case -1250986904:
          if (str.equals("revisionCode")) {
            b = 3;
            break;
          } 
      } 
      switch (b) {
        case 8:
          bool4 = paramAttributeSet.getAttributeBooleanValue(i2, false);
          break;
        case 7:
          bool3 = paramAttributeSet.getAttributeBooleanValue(i2, false);
          break;
        case 6:
          str2 = paramAttributeSet.getAttributeValue(i2);
          break;
        case 5:
          bool2 = paramAttributeSet.getAttributeBooleanValue(i2, false);
          break;
        case 4:
          bool1 = paramAttributeSet.getAttributeBooleanValue(i2, false);
          break;
        case 3:
          n = paramAttributeSet.getAttributeIntValue(i2, 0);
          break;
        case 2:
          k = paramAttributeSet.getAttributeIntValue(i2, 0);
          break;
        case 1:
          j = paramAttributeSet.getAttributeIntValue(i2, 0);
          break;
        case 0:
          i = paramAttributeSet.getAttributeIntValue(i2, -1);
          break;
      } 
    } 
    i2 = paramXmlPullParser.getDepth() + 1;
    ArrayList<VerifierInfo> arrayList = new ArrayList();
    String str3 = null, str4 = null, str5 = null, str6 = null;
    boolean bool6 = false, bool7 = true, bool8 = false;
    boolean bool9 = false, bool10 = false, bool11 = false;
    int i4 = 1, i3 = m;
    m = i4;
    while (true) {
      i4 = paramXmlPullParser.next();
      if (i4 != 1 && (i4 != 3 || 
        paramXmlPullParser.getDepth() >= i2)) {
        if (i4 == 3 || i4 == 4)
          continue; 
        if (paramXmlPullParser.getDepth() != i2)
          continue; 
        String str = paramXmlPullParser.getName();
        if ("package-verifier".equals(str)) {
          VerifierInfo verifierInfo = parseVerifier(paramAttributeSet);
          if (verifierInfo != null)
            arrayList.add(verifierInfo); 
          continue;
        } 
        if ("application".equals(paramXmlPullParser.getName())) {
          boolean bool;
          for (byte b = 0; b < paramAttributeSet.getAttributeCount(); b++, bool9 = bool11, bool8 = bool12, bool7 = bool13, bool6 = bool14) {
            boolean bool12, bool13, bool14;
            str = paramAttributeSet.getAttributeName(b);
            switch (str.hashCode()) {
              default:
                i4 = -1;
                break;
              case -57729599:
                if (str.equals("use32bitAbi")) {
                  i4 = 2;
                  break;
                } 
              case -597534042:
                if (str.equals("extractNativeLibs")) {
                  i4 = 3;
                  break;
                } 
              case -1207511761:
                if (str.equals("multiArch")) {
                  i4 = 1;
                  break;
                } 
              case -1517066970:
                if (str.equals("useEmbeddedDex")) {
                  i4 = 4;
                  break;
                } 
              case -1833406514:
                if (str.equals("debuggable")) {
                  i4 = 0;
                  break;
                } 
            } 
            if (i4 != 0) {
              if (i4 != 1) {
                if (i4 != 2) {
                  if (i4 != 3) {
                    if (i4 != 4) {
                      bool11 = bool9;
                      bool12 = bool8;
                      bool13 = bool7;
                      bool14 = bool6;
                    } else {
                      bool14 = paramAttributeSet.getAttributeBooleanValue(b, false);
                      bool11 = bool9;
                      bool12 = bool8;
                      bool13 = bool7;
                    } 
                  } else {
                    bool13 = paramAttributeSet.getAttributeBooleanValue(b, true);
                    bool11 = bool9;
                    bool12 = bool8;
                    bool14 = bool6;
                  } 
                } else {
                  bool12 = paramAttributeSet.getAttributeBooleanValue(b, false);
                  bool11 = bool9;
                  bool13 = bool7;
                  bool14 = bool6;
                } 
              } else {
                bool11 = paramAttributeSet.getAttributeBooleanValue(b, false);
                bool12 = bool8;
                bool13 = bool7;
                bool14 = bool6;
              } 
            } else {
              boolean bool15 = paramAttributeSet.getAttributeBooleanValue(b, false);
              bool = bool15;
              bool11 = bool9;
              bool12 = bool8;
              bool13 = bool7;
              bool14 = bool6;
              if (bool15) {
                bool10 = true;
                bool14 = bool6;
                bool13 = bool7;
                bool12 = bool8;
                bool11 = bool9;
                bool = bool15;
              } 
            } 
          } 
          bool11 = bool;
          continue;
        } 
        if ("overlay".equals(paramXmlPullParser.getName())) {
          boolean bool;
          for (i4 = 0, bool = bool5; i4 < paramAttributeSet.getAttributeCount(); i4++, str3 = str, bool = bool5, str4 = str8, str5 = str9) {
            String str8, str9, str7 = paramAttributeSet.getAttributeName(i4);
            if ("requiredSystemPropertyName".equals(str7)) {
              str8 = paramAttributeSet.getAttributeValue(i4);
              str = str3;
              bool5 = bool;
              str9 = str5;
            } else if ("requiredSystemPropertyValue".equals(str7)) {
              str9 = paramAttributeSet.getAttributeValue(i4);
              str = str3;
              bool5 = bool;
              str8 = str4;
            } else if ("targetPackage".equals(str7)) {
              str = paramAttributeSet.getAttributeValue(i4);
              bool5 = bool;
              str8 = str4;
              str9 = str5;
            } else if ("isStatic".equals(str7)) {
              bool5 = paramAttributeSet.getAttributeBooleanValue(i4, false);
              str = str3;
              str8 = str4;
              str9 = str5;
            } else {
              str = str3;
              bool5 = bool;
              str8 = str4;
              str9 = str5;
              if ("priority".equals(str7)) {
                i1 = paramAttributeSet.getAttributeIntValue(i4, 0);
                str9 = str5;
                str8 = str4;
                bool5 = bool;
                str = str3;
              } 
            } 
          } 
          bool5 = bool;
          continue;
        } 
        if ("uses-split".equals(paramXmlPullParser.getName())) {
          if (str6 != null) {
            Slog.w("PackageParsing", "Only one <uses-split> permitted. Ignoring others.");
            continue;
          } 
          str6 = paramAttributeSet.getAttributeValue("http://schemas.android.com/apk/res/android", "name");
          if (str6 == null)
            return paramParseInput.error(-108, "<uses-split> tag requires 'android:name' attribute"); 
          continue;
        } 
        if ("uses-sdk".equals(paramXmlPullParser.getName())) {
          for (i4 = 0; i4 < paramAttributeSet.getAttributeCount(); i4++) {
            str = paramAttributeSet.getAttributeName(i4);
            if ("targetSdkVersion".equals(str))
              i3 = paramAttributeSet.getAttributeIntValue(i4, 0); 
            if ("minSdkVersion".equals(str))
              m = paramAttributeSet.getAttributeIntValue(i4, 1); 
          } 
          continue;
        } 
        if ("profileable".equals(paramXmlPullParser.getName())) {
          boolean bool;
          for (i4 = 0, bool = bool10; i4 < paramAttributeSet.getAttributeCount(); i4++, bool = bool10) {
            str = paramAttributeSet.getAttributeName(i4);
            bool10 = bool;
            if ("shell".equals(str))
              bool10 = paramAttributeSet.getAttributeBooleanValue(i4, bool); 
          } 
          bool10 = bool;
        } 
        continue;
      } 
      break;
    } 
    String str1 = str3;
    if (!PackageParser.checkRequiredSystemProperties(str4, str5)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Skipping target and overlay pair ");
      stringBuilder.append(str3);
      stringBuilder.append(" and ");
      stringBuilder.append(paramString);
      stringBuilder.append(": overlay ignored due to required system property: ");
      stringBuilder.append(str4);
      stringBuilder.append(" with value: ");
      stringBuilder.append(str5);
      Slog.i("PackageParsing", stringBuilder.toString());
      stringBuilder = null;
      bool5 = false;
      i1 = 0;
    } 
    return paramParseInput.success(new PackageParser.ApkLite(paramString, (String)pair.first, (String)pair.second, bool3, str2, str6, bool4, j, k, n, i, arrayList, paramSigningDetails, bool1, bool11, bool10, bool9, bool8, bool6, bool7, bool2, (String)stringBuilder, bool5, i1, m, i3));
  }
  
  public static ParseResult<Pair<String, String>> parsePackageSplitNames(ParseInput paramParseInput, XmlPullParser paramXmlPullParser, AttributeSet paramAttributeSet) throws IOException, XmlPullParserException {
    StringBuilder stringBuilder1;
    String str1;
    StringBuilder stringBuilder2;
    int i;
    while (true) {
      i = paramXmlPullParser.next();
      if (i != 2 && i != 1)
        continue; 
      break;
    } 
    if (i != 2)
      return paramParseInput.error(-108, "No start tag found"); 
    if (!paramXmlPullParser.getName().equals("manifest"))
      return paramParseInput.error(-108, "No <manifest> tag"); 
    String str4 = paramAttributeSet.getAttributeValue(null, "package");
    if (!"android".equals(str4)) {
      if ("oplus".equals(str4)) {
        paramXmlPullParser = null;
      } else {
        str1 = PackageParser.validateName(str4, true, true);
      } 
      if (str1 != null) {
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Invalid manifest package: ");
        stringBuilder2.append(str1);
        return paramParseInput.error(-106, stringBuilder2.toString());
      } 
    } 
    String str3 = stringBuilder2.getAttributeValue(null, "split");
    String str2 = str3;
    if (str3 != null)
      if (str3.length() == 0) {
        str2 = null;
      } else {
        String str = PackageParser.validateName(str3, false, false);
        str2 = str3;
        if (str != null) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Invalid manifest split: ");
          stringBuilder1.append(str);
          return paramParseInput.error(-106, stringBuilder1.toString());
        } 
      }  
    str3 = str4.intern();
    if (stringBuilder1 != null)
      str1 = stringBuilder1.intern(); 
    return paramParseInput.success(Pair.create(str3, str1));
  }
  
  public static VerifierInfo parseVerifier(AttributeSet paramAttributeSet) {
    StringBuilder stringBuilder;
    String str1 = null;
    String str2 = null;
    int i = paramAttributeSet.getAttributeCount();
    for (byte b = 0; b < i; b++) {
      int j = paramAttributeSet.getAttributeNameResource(b);
      if (j != 16842755) {
        if (j == 16843686)
          str2 = paramAttributeSet.getAttributeValue(b); 
      } else {
        str1 = paramAttributeSet.getAttributeValue(b);
      } 
    } 
    if (str1 == null || str1.length() == 0) {
      Slog.i("PackageParsing", "verifier package name was null; skipping");
      return null;
    } 
    PublicKey publicKey = PackageParser.parsePublicKey(str2);
    if (publicKey == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to parse verifier public key for ");
      stringBuilder.append(str1);
      Slog.i("PackageParsing", stringBuilder.toString());
      return null;
    } 
    return new VerifierInfo(str1, (PublicKey)stringBuilder);
  }
}
