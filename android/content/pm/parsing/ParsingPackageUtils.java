package android.content.pm.parsing;

import android.app.ActivityThread;
import android.content.pm.ConfigurationInfo;
import android.content.pm.FeatureInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageParser;
import android.content.pm.Signature;
import android.content.pm.parsing.component.ComponentParseUtils;
import android.content.pm.parsing.component.ParsedActivity;
import android.content.pm.parsing.component.ParsedAttribution;
import android.content.pm.parsing.component.ParsedAttributionUtils;
import android.content.pm.parsing.component.ParsedInstrumentation;
import android.content.pm.parsing.component.ParsedInstrumentationUtils;
import android.content.pm.parsing.component.ParsedIntentInfo;
import android.content.pm.parsing.component.ParsedPermission;
import android.content.pm.parsing.component.ParsedPermissionGroup;
import android.content.pm.parsing.component.ParsedPermissionUtils;
import android.content.pm.parsing.component.ParsedProcess;
import android.content.pm.parsing.component.ParsedProcessUtils;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.pm.parsing.result.ParseTypeImpl;
import android.content.pm.permission.SplitPermissionInfoParcelable;
import android.content.pm.split.DefaultSplitAssetLoader;
import android.content.res.ApkAssets;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.Trace;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.ArraySet;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.util.Slog;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import android.util.apk.ApkSignatureVerifier;
import com.android.internal.R;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.XmlUtils;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import libcore.io.IoUtils;
import libcore.util.EmptyArray;
import org.xmlpull.v1.XmlPullParserException;

public class ParsingPackageUtils {
  public static final String TAG = "PackageParsing";
  
  private Callback mCallback;
  
  private DisplayMetrics mDisplayMetrics;
  
  private boolean mOnlyCoreApps;
  
  private String[] mSeparateProcesses;
  
  public static ParseResult<ParsingPackage> parseDefaultOneTime(File paramFile, int paramInt, boolean paramBoolean) {
    ParseInput parseInput = ParseTypeImpl.forDefaultParsing().reset();
    return parseDefault(parseInput, paramFile, paramInt, paramBoolean);
  }
  
  public static ParseResult<ParsingPackage> parseDefault(ParseInput paramParseInput, File paramFile, int paramInt, boolean paramBoolean) {
    ParsingPackageUtils parsingPackageUtils = new ParsingPackageUtils(false, null, null, (Callback)new Object());
    try {
      ParseResult<ParsingPackage> parseResult = parsingPackageUtils.parsePackage(paramParseInput, paramFile, paramInt);
      boolean bool = parseResult.isError();
      if (bool)
        return parseResult; 
      try {
        ParsingPackage parsingPackage = parseResult.getResult();
        if (paramBoolean) {
          PackageParser.SigningDetails signingDetails = getSigningDetails(parsingPackage, false);
          parsingPackage.setSigningDetails(signingDetails);
        } 
        return paramParseInput.success(parsingPackage);
      } catch (android.content.pm.PackageParser.PackageParserException packageParserException) {
        return paramParseInput.error(-102, "Error collecting package certificates", packageParserException);
      } 
    } catch (android.content.pm.PackageParser.PackageParserException packageParserException) {
      return paramParseInput.error(-102, "Error parsing package", packageParserException);
    } 
  }
  
  public ParsingPackageUtils(boolean paramBoolean, String[] paramArrayOfString, DisplayMetrics paramDisplayMetrics, Callback paramCallback) {
    this.mOnlyCoreApps = paramBoolean;
    this.mSeparateProcesses = paramArrayOfString;
    this.mDisplayMetrics = paramDisplayMetrics;
    this.mCallback = paramCallback;
  }
  
  public ParseResult<ParsingPackage> parsePackage(ParseInput paramParseInput, File paramFile, int paramInt) throws PackageParser.PackageParserException {
    if (paramFile.isDirectory())
      return parseClusterPackage(paramParseInput, paramFile, paramInt); 
    return parseMonolithicPackage(paramParseInput, paramFile, paramInt);
  }
  
  private ParseResult<ParsingPackage> parseClusterPackage(ParseInput paramParseInput, File paramFile, int paramInt) {
    // Byte code:
    //   0: aload_1
    //   1: aload_2
    //   2: iconst_0
    //   3: invokestatic parseClusterPackageLite : (Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;I)Landroid/content/pm/parsing/result/ParseResult;
    //   6: astore #4
    //   8: aload #4
    //   10: invokeinterface isError : ()Z
    //   15: ifeq -> 27
    //   18: aload_1
    //   19: aload #4
    //   21: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   26: areturn
    //   27: aload #4
    //   29: invokeinterface getResult : ()Ljava/lang/Object;
    //   34: checkcast android/content/pm/PackageParser$PackageLite
    //   37: astore #4
    //   39: aload_0
    //   40: getfield mOnlyCoreApps : Z
    //   43: ifeq -> 93
    //   46: aload #4
    //   48: getfield coreApp : Z
    //   51: ifne -> 93
    //   54: new java/lang/StringBuilder
    //   57: dup
    //   58: invokespecial <init> : ()V
    //   61: astore #4
    //   63: aload #4
    //   65: ldc_w 'Not a coreApp: '
    //   68: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: aload #4
    //   74: aload_2
    //   75: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   78: pop
    //   79: aload_1
    //   80: bipush #-123
    //   82: aload #4
    //   84: invokevirtual toString : ()Ljava/lang/String;
    //   87: invokeinterface error : (ILjava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   92: areturn
    //   93: aload #4
    //   95: getfield isolatedSplits : Z
    //   98: ifeq -> 149
    //   101: aload #4
    //   103: getfield splitNames : [Ljava/lang/String;
    //   106: invokestatic isEmpty : ([Ljava/lang/Object;)Z
    //   109: ifne -> 149
    //   112: aload #4
    //   114: invokestatic createDependenciesFromPackage : (Landroid/content/pm/PackageParser$PackageLite;)Landroid/util/SparseArray;
    //   117: astore #5
    //   119: new android/content/pm/split/SplitAssetDependencyLoader
    //   122: dup
    //   123: aload #4
    //   125: aload #5
    //   127: iload_3
    //   128: invokespecial <init> : (Landroid/content/pm/PackageParser$PackageLite;Landroid/util/SparseArray;I)V
    //   131: astore_2
    //   132: goto -> 163
    //   135: astore_2
    //   136: aload_1
    //   137: bipush #-101
    //   139: aload_2
    //   140: invokevirtual getMessage : ()Ljava/lang/String;
    //   143: invokeinterface error : (ILjava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   148: areturn
    //   149: new android/content/pm/split/DefaultSplitAssetLoader
    //   152: dup
    //   153: aload #4
    //   155: iload_3
    //   156: invokespecial <init> : (Landroid/content/pm/PackageParser$PackageLite;I)V
    //   159: astore_2
    //   160: aconst_null
    //   161: astore #5
    //   163: aload #5
    //   165: astore #6
    //   167: aload_2
    //   168: astore #6
    //   170: aload #4
    //   172: astore #7
    //   174: aload #5
    //   176: astore #7
    //   178: aload_2
    //   179: astore #8
    //   181: aload #4
    //   183: astore #7
    //   185: aload_2
    //   186: invokeinterface getBaseAssetManager : ()Landroid/content/res/AssetManager;
    //   191: astore #9
    //   193: aload #5
    //   195: astore #6
    //   197: aload_2
    //   198: astore #6
    //   200: aload #4
    //   202: astore #7
    //   204: aload #5
    //   206: astore #7
    //   208: aload_2
    //   209: astore #8
    //   211: aload #4
    //   213: astore #7
    //   215: new java/io/File
    //   218: astore #10
    //   220: aload #5
    //   222: astore #6
    //   224: aload_2
    //   225: astore #6
    //   227: aload #4
    //   229: astore #7
    //   231: aload #5
    //   233: astore #7
    //   235: aload_2
    //   236: astore #8
    //   238: aload #4
    //   240: astore #7
    //   242: aload #10
    //   244: aload #4
    //   246: getfield baseCodePath : Ljava/lang/String;
    //   249: invokespecial <init> : (Ljava/lang/String;)V
    //   252: aload #5
    //   254: astore #6
    //   256: aload_2
    //   257: astore #6
    //   259: aload #4
    //   261: astore #7
    //   263: aload #5
    //   265: astore #7
    //   267: aload_2
    //   268: astore #8
    //   270: aload #4
    //   272: astore #7
    //   274: aload_0
    //   275: aload_1
    //   276: aload #10
    //   278: aload #4
    //   280: getfield codePath : Ljava/lang/String;
    //   283: aload #9
    //   285: iload_3
    //   286: invokespecial parseBaseApk : (Landroid/content/pm/parsing/result/ParseInput;Ljava/io/File;Ljava/lang/String;Landroid/content/res/AssetManager;I)Landroid/content/pm/parsing/result/ParseResult;
    //   289: astore #10
    //   291: aload #5
    //   293: astore #6
    //   295: aload_2
    //   296: astore #6
    //   298: aload #4
    //   300: astore #7
    //   302: aload #5
    //   304: astore #7
    //   306: aload_2
    //   307: astore #8
    //   309: aload #4
    //   311: astore #7
    //   313: aload #10
    //   315: invokeinterface isError : ()Z
    //   320: istore #11
    //   322: iload #11
    //   324: ifeq -> 360
    //   327: aload_1
    //   328: aload #10
    //   330: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   335: astore #5
    //   337: aload_2
    //   338: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   341: aload #5
    //   343: areturn
    //   344: astore_1
    //   345: goto -> 747
    //   348: astore #5
    //   350: aload #4
    //   352: astore #8
    //   354: aload_2
    //   355: astore #4
    //   357: goto -> 678
    //   360: aload #5
    //   362: astore #6
    //   364: aload_2
    //   365: astore #6
    //   367: aload #4
    //   369: astore #7
    //   371: aload #5
    //   373: astore #7
    //   375: aload_2
    //   376: astore #8
    //   378: aload #4
    //   380: astore #7
    //   382: aload #10
    //   384: invokeinterface getResult : ()Ljava/lang/Object;
    //   389: checkcast android/content/pm/parsing/ParsingPackage
    //   392: astore #10
    //   394: aload #5
    //   396: astore #6
    //   398: aload_2
    //   399: astore #6
    //   401: aload #4
    //   403: astore #7
    //   405: aload #5
    //   407: astore #7
    //   409: aload_2
    //   410: astore #8
    //   412: aload #4
    //   414: astore #7
    //   416: aload #4
    //   418: getfield splitNames : [Ljava/lang/String;
    //   421: invokestatic isEmpty : ([Ljava/lang/Object;)Z
    //   424: ifne -> 592
    //   427: aload #5
    //   429: astore #6
    //   431: aload_2
    //   432: astore #6
    //   434: aload #4
    //   436: astore #7
    //   438: aload #5
    //   440: astore #7
    //   442: aload_2
    //   443: astore #8
    //   445: aload #4
    //   447: astore #7
    //   449: aload #10
    //   451: aload #4
    //   453: getfield splitNames : [Ljava/lang/String;
    //   456: aload #4
    //   458: getfield splitCodePaths : [Ljava/lang/String;
    //   461: aload #4
    //   463: getfield splitRevisionCodes : [I
    //   466: aload #5
    //   468: invokeinterface asSplit : ([Ljava/lang/String;[Ljava/lang/String;[ILandroid/util/SparseArray;)Landroid/content/pm/parsing/ParsingPackage;
    //   473: pop
    //   474: aload #5
    //   476: astore #6
    //   478: aload_2
    //   479: astore #6
    //   481: aload #4
    //   483: astore #7
    //   485: aload #5
    //   487: astore #7
    //   489: aload_2
    //   490: astore #8
    //   492: aload #4
    //   494: astore #7
    //   496: aload #4
    //   498: getfield splitNames : [Ljava/lang/String;
    //   501: arraylength
    //   502: istore #12
    //   504: iconst_0
    //   505: istore #13
    //   507: iload #13
    //   509: iload #12
    //   511: if_icmpge -> 582
    //   514: aload #5
    //   516: astore #6
    //   518: aload_2
    //   519: astore #6
    //   521: aload #4
    //   523: astore #7
    //   525: aload #5
    //   527: astore #7
    //   529: aload_2
    //   530: astore #8
    //   532: aload #4
    //   534: astore #7
    //   536: aload_2
    //   537: iload #13
    //   539: invokeinterface getSplitAssetManager : (I)Landroid/content/res/AssetManager;
    //   544: astore #9
    //   546: aload_2
    //   547: astore #6
    //   549: aload #4
    //   551: astore #8
    //   553: aload #6
    //   555: astore #7
    //   557: aload #6
    //   559: astore_2
    //   560: aload_0
    //   561: aload_1
    //   562: aload #10
    //   564: iload #13
    //   566: aload #9
    //   568: iload_3
    //   569: invokespecial parseSplitApk : (Landroid/content/pm/parsing/result/ParseInput;Landroid/content/pm/parsing/ParsingPackage;ILandroid/content/res/AssetManager;I)Landroid/content/pm/parsing/result/ParseResult;
    //   572: pop
    //   573: iinc #13, 1
    //   576: aload #6
    //   578: astore_2
    //   579: goto -> 507
    //   582: aload #4
    //   584: astore #5
    //   586: aload_2
    //   587: astore #4
    //   589: goto -> 599
    //   592: aload #4
    //   594: astore #5
    //   596: aload_2
    //   597: astore #4
    //   599: aload #5
    //   601: astore #8
    //   603: aload #4
    //   605: astore #7
    //   607: aload #4
    //   609: astore_2
    //   610: aload #10
    //   612: aload #5
    //   614: getfield use32bitAbi : Z
    //   617: invokeinterface setUse32BitAbi : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   622: pop
    //   623: aload #5
    //   625: astore #8
    //   627: aload #4
    //   629: astore #7
    //   631: aload #4
    //   633: astore_2
    //   634: aload_1
    //   635: aload #10
    //   637: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   642: astore #5
    //   644: aload #4
    //   646: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   649: aload #5
    //   651: areturn
    //   652: astore #5
    //   654: aload #7
    //   656: astore #4
    //   658: goto -> 678
    //   661: astore_1
    //   662: aload #6
    //   664: astore_2
    //   665: goto -> 747
    //   668: astore #5
    //   670: aload #8
    //   672: astore #4
    //   674: aload #7
    //   676: astore #8
    //   678: aload #4
    //   680: astore_2
    //   681: new java/lang/StringBuilder
    //   684: astore #6
    //   686: aload #4
    //   688: astore_2
    //   689: aload #6
    //   691: invokespecial <init> : ()V
    //   694: aload #4
    //   696: astore_2
    //   697: aload #6
    //   699: ldc_w 'Failed to load assets: '
    //   702: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: pop
    //   706: aload #4
    //   708: astore_2
    //   709: aload #6
    //   711: aload #8
    //   713: getfield baseCodePath : Ljava/lang/String;
    //   716: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   719: pop
    //   720: aload #4
    //   722: astore_2
    //   723: aload_1
    //   724: bipush #-102
    //   726: aload #6
    //   728: invokevirtual toString : ()Ljava/lang/String;
    //   731: aload #5
    //   733: invokeinterface error : (ILjava/lang/String;Ljava/lang/Exception;)Landroid/content/pm/parsing/result/ParseResult;
    //   738: astore_1
    //   739: aload #4
    //   741: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   744: aload_1
    //   745: areturn
    //   746: astore_1
    //   747: aload_2
    //   748: invokestatic closeQuietly : (Ljava/lang/AutoCloseable;)V
    //   751: aload_1
    //   752: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #250	-> 0
    //   #251	-> 0
    //   #252	-> 8
    //   #253	-> 18
    //   #256	-> 27
    //   #257	-> 39
    //   #258	-> 54
    //   #263	-> 93
    //   #265	-> 93
    //   #267	-> 112
    //   #268	-> 119
    //   #271	-> 132
    //   #269	-> 135
    //   #270	-> 136
    //   #273	-> 149
    //   #277	-> 163
    //   #278	-> 193
    //   #279	-> 252
    //   #281	-> 291
    //   #282	-> 327
    //   #307	-> 337
    //   #282	-> 341
    //   #307	-> 344
    //   #303	-> 348
    //   #285	-> 360
    //   #286	-> 394
    //   #287	-> 427
    //   #293	-> 474
    //   #295	-> 504
    //   #296	-> 514
    //   #297	-> 546
    //   #295	-> 573
    //   #286	-> 592
    //   #301	-> 599
    //   #302	-> 623
    //   #307	-> 644
    //   #302	-> 649
    //   #303	-> 652
    //   #307	-> 661
    //   #303	-> 668
    //   #304	-> 678
    //   #307	-> 739
    //   #304	-> 744
    //   #307	-> 746
    //   #308	-> 751
    // Exception table:
    //   from	to	target	type
    //   112	119	135	android/content/pm/split/SplitDependencyLoader$IllegalDependencyException
    //   119	132	135	android/content/pm/split/SplitDependencyLoader$IllegalDependencyException
    //   185	193	668	android/content/pm/PackageParser$PackageParserException
    //   185	193	661	finally
    //   215	220	668	android/content/pm/PackageParser$PackageParserException
    //   215	220	661	finally
    //   242	252	668	android/content/pm/PackageParser$PackageParserException
    //   242	252	661	finally
    //   274	291	668	android/content/pm/PackageParser$PackageParserException
    //   274	291	661	finally
    //   313	322	668	android/content/pm/PackageParser$PackageParserException
    //   313	322	661	finally
    //   327	337	348	android/content/pm/PackageParser$PackageParserException
    //   327	337	344	finally
    //   382	394	668	android/content/pm/PackageParser$PackageParserException
    //   382	394	661	finally
    //   416	427	668	android/content/pm/PackageParser$PackageParserException
    //   416	427	661	finally
    //   449	474	668	android/content/pm/PackageParser$PackageParserException
    //   449	474	661	finally
    //   496	504	668	android/content/pm/PackageParser$PackageParserException
    //   496	504	661	finally
    //   536	546	668	android/content/pm/PackageParser$PackageParserException
    //   536	546	661	finally
    //   560	573	652	android/content/pm/PackageParser$PackageParserException
    //   560	573	746	finally
    //   610	623	652	android/content/pm/PackageParser$PackageParserException
    //   610	623	746	finally
    //   634	644	652	android/content/pm/PackageParser$PackageParserException
    //   634	644	746	finally
    //   681	686	746	finally
    //   689	694	746	finally
    //   697	706	746	finally
    //   709	720	746	finally
    //   723	739	746	finally
  }
  
  private ParseResult<ParsingPackage> parseMonolithicPackage(ParseInput paramParseInput, File paramFile, int paramInt) throws PackageParser.PackageParserException {
    ParseResult<PackageParser.PackageLite> parseResult = ApkLiteParseUtils.parseMonolithicPackageLite(paramParseInput, paramFile, paramInt);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    PackageParser.PackageLite packageLite = parseResult.getResult();
    if (this.mOnlyCoreApps && !packageLite.coreApp) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Not a coreApp: ");
      stringBuilder.append(paramFile);
      return paramParseInput.error(-123, stringBuilder.toString());
    } 
    DefaultSplitAssetLoader defaultSplitAssetLoader = new DefaultSplitAssetLoader(packageLite, paramInt);
    try {
      ParseResult<?> parseResult2;
      String str = paramFile.getCanonicalPath();
      AssetManager assetManager = defaultSplitAssetLoader.getBaseAssetManager();
      ParseResult<ParsingPackage> parseResult3 = parseBaseApk(paramParseInput, paramFile, str, assetManager, paramInt);
      if (parseResult3.isError()) {
        parseResult2 = paramParseInput.error(parseResult3);
        IoUtils.closeQuietly(defaultSplitAssetLoader);
        return (ParseResult)parseResult2;
      } 
      ParsingPackage parsingPackage2 = parseResult3.getResult();
      boolean bool = ((PackageParser.PackageLite)parseResult2).use32bitAbi;
      ParsingPackage parsingPackage1 = parsingPackage2.setUse32BitAbi(bool);
      ParseResult<ParsingPackage> parseResult1 = paramParseInput.success(parsingPackage1);
      IoUtils.closeQuietly(defaultSplitAssetLoader);
      return parseResult1;
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to get path: ");
      stringBuilder.append(paramFile);
      ParseResult<?> parseResult1 = paramParseInput.error(-102, stringBuilder.toString(), iOException);
      IoUtils.closeQuietly(defaultSplitAssetLoader);
      return (ParseResult)parseResult1;
    } finally {}
    IoUtils.closeQuietly(defaultSplitAssetLoader);
    throw paramParseInput;
  }
  
  private ParseResult<ParsingPackage> parseBaseApk(ParseInput paramParseInput, File paramFile, String paramString, AssetManager paramAssetManager, int paramInt) {
    String str2, str1 = paramFile.getAbsolutePath();
    if (str1.startsWith("/mnt/expand/")) {
      int j = str1.indexOf('/', "/mnt/expand/".length());
      str2 = str1.substring("/mnt/expand/".length(), j);
    } else {
      str2 = null;
    } 
    int i = paramAssetManager.findCookieForPath(str1);
    if (i == 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed adding asset path: ");
      stringBuilder1.append(str1);
      return paramParseInput.error(-101, stringBuilder1.toString());
    } 
    try {
      XmlResourceParser xmlResourceParser = paramAssetManager.openXmlResourceParser(i, "AndroidManifest.xml");
      try {
        Resources resources = new Resources();
        try {
          this(paramAssetManager, this.mDisplayMetrics, null);
          ParseResult<ParsingPackage> parseResult = parseBaseApk(paramParseInput, str1, paramString, resources, xmlResourceParser, paramInt);
          if (parseResult.isError()) {
            paramInt = parseResult.getErrorCode();
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append(str1);
            stringBuilder1.append(" (at ");
            stringBuilder1.append(xmlResourceParser.getPositionDescription());
            stringBuilder1.append("): ");
            stringBuilder1.append(parseResult.getErrorMessage());
            String str = stringBuilder1.toString();
            ParseResult<?> parseResult1 = paramParseInput.error(paramInt, str);
            if (xmlResourceParser != null) {
              try {
                xmlResourceParser.close();
                return (ParseResult)parseResult1;
              } catch (Exception null) {}
            } else {
              return (ParseResult<ParsingPackage>)exception;
            } 
          } else {
            ParsingPackage parsingPackage = parseResult.getResult();
            if (paramAssetManager.containsAllocatedTable()) {
              ParseResult<?> parseResult2 = paramParseInput.deferError("Targeting R+ (version 30 and above) requires the resources.arsc of installed APKs to be stored uncompressed and aligned on a 4-byte boundary", 132742131L);
              if (parseResult2.isError()) {
                String str = parseResult2.getErrorMessage();
                ParseResult<?> parseResult3 = paramParseInput.error(-124, str);
                if (xmlResourceParser != null)
                  xmlResourceParser.close(); 
                return (ParseResult)parseResult3;
              } 
            } 
            ApkAssets apkAssets = paramAssetManager.getApkAssets()[0];
            if (apkAssets.definesOverlayable()) {
              SparseArray<String> sparseArray = paramAssetManager.getAssignedPackageIdentifiers();
              int j = sparseArray.size();
              for (i = 0; i < j; i++) {
                String str = (String)sparseArray.get(i);
                Map<String, String> map = paramAssetManager.getOverlayableMap(str);
                if (map != null && !map.isEmpty())
                  for (String str3 : map.keySet())
                    parsingPackage.addOverlayable(str3, map.get(str3));  
              } 
            } 
            parsingPackage.setVolumeUuid(str2);
            if ((paramInt & 0x20) != 0) {
              parsingPackage.setSigningDetails(getSigningDetails(parsingPackage, false));
            } else {
              parsingPackage.setSigningDetails(PackageParser.SigningDetails.UNKNOWN);
            } 
            ParseResult<ParsingPackage> parseResult1 = paramParseInput.success(parsingPackage);
            if (xmlResourceParser != null)
              xmlResourceParser.close(); 
            return parseResult1;
          } 
        } finally {}
      } finally {}
      if (xmlResourceParser != null)
        try {
          xmlResourceParser.close();
        } finally {
          paramString = null;
        }  
      throw paramFile;
    } catch (Exception exception) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Failed to read manifest from ");
    stringBuilder.append(str1);
    return paramParseInput.error(-102, stringBuilder.toString(), exception);
  }
  
  private ParseResult<ParsingPackage> parseSplitApk(ParseInput paramParseInput, ParsingPackage paramParsingPackage, int paramInt1, AssetManager paramAssetManager, int paramInt2) {
    StringBuilder stringBuilder;
    String str = paramParsingPackage.getSplitCodePaths()[paramInt1];
    int i = paramAssetManager.findCookieForPath(str);
    if (i == 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Failed adding asset path: ");
      stringBuilder.append(str);
      return paramParseInput.error(-101, stringBuilder.toString());
    } 
    try {
      XmlResourceParser xmlResourceParser = paramAssetManager.openXmlResourceParser(i, "AndroidManifest.xml");
      try {
        Resources resources = new Resources();
        this(paramAssetManager, this.mDisplayMetrics, null);
        ParseResult<?> parseResult = parseSplitApk(paramParseInput, (ParsingPackage)stringBuilder, resources, xmlResourceParser, paramInt2, paramInt1);
        if (parseResult.isError()) {
          paramInt1 = parseResult.getErrorCode();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append(str);
          stringBuilder1.append(" (at ");
          stringBuilder1.append(xmlResourceParser.getPositionDescription());
          stringBuilder1.append("): ");
          stringBuilder1.append(parseResult.getErrorMessage());
          String str1 = stringBuilder1.toString();
          parseResult = paramParseInput.error(paramInt1, str1);
          return (ParseResult)parseResult;
        } 
        return (ParseResult)parseResult;
      } finally {
        if (xmlResourceParser != null)
          try {
            xmlResourceParser.close();
          } finally {
            paramAssetManager = null;
          }  
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Failed to read manifest from ");
      stringBuilder1.append(str);
      return paramParseInput.error(-102, stringBuilder1.toString(), exception);
    } 
  }
  
  private ParseResult<ParsingPackage> parseBaseApk(ParseInput paramParseInput, String paramString1, String paramString2, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt) throws XmlPullParserException, IOException, PackageParser.PackageParserException {
    StringBuilder stringBuilder;
    ParseResult<Pair<String, String>> parseResult = ApkLiteParseUtils.parsePackageSplitNames(paramParseInput, paramXmlResourceParser, paramXmlResourceParser);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    Pair pair = parseResult.getResult();
    String str2 = (String)pair.first;
    String str1 = (String)pair.second;
    if (!TextUtils.isEmpty(str1)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Expected base APK, but found split ");
      stringBuilder.append(str1);
      return paramParseInput.error(-106, stringBuilder.toString());
    } 
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifest);
    try {
      boolean bool = paramXmlResourceParser.getAttributeBooleanValue(null, "coreApp", false);
      ParsingPackage parsingPackage = this.mCallback.startParsingPackage(str2, (String)stringBuilder, paramString2, typedArray, bool);
      try {
        ParseResult<ParsingPackage> parseResult2 = parseBaseApkTags(paramParseInput, parsingPackage, typedArray, paramResources, paramXmlResourceParser, paramInt);
        bool = parseResult2.isError();
        if (bool) {
          typedArray.recycle();
          return parseResult2;
        } 
        ParseResult<ParsingPackage> parseResult1 = paramParseInput.success(parsingPackage);
        typedArray.recycle();
        return parseResult1;
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramParseInput;
  }
  
  private ParseResult<ParsingPackage> parseSplitApk(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt1, int paramInt2) throws XmlPullParserException, IOException, PackageParser.PackageParserException {
    PackageParser.parsePackageSplitNames(paramXmlResourceParser, paramXmlResourceParser);
    boolean bool = false;
    int i = paramXmlResourceParser.getDepth();
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1) {
        ParseResult<?> parseResult;
        if (i + 1 < paramXmlResourceParser.getDepth() || j != 2)
          continue; 
        String str = paramXmlResourceParser.getName();
        if ("application".equals(str)) {
          if (bool) {
            Slog.w("PackageParsing", "<manifest> has more than one <application>");
            parseResult = paramParseInput.success(null);
          } else {
            bool = true;
            parseResult = parseSplitApplication(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser, paramInt1, paramInt2);
          } 
        } else {
          parseResult = ParsingUtils.unknownTag("<manifest>", paramParsingPackage, paramXmlResourceParser, paramParseInput);
        } 
        if (parseResult.isError())
          return paramParseInput.error(parseResult); 
        continue;
      } 
      break;
    } 
    if (!bool) {
      ParseResult<?> parseResult = paramParseInput.deferError("<manifest> does not contain an <application>", 150776642L);
      if (parseResult.isError())
        return paramParseInput.error(parseResult); 
    } 
    return paramParseInput.success(paramParsingPackage);
  }
  
  private ParseResult<ParsingPackage> parseSplitApplication(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt1, int paramInt2) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_2
    //   1: astore #7
    //   3: aload_3
    //   4: aload #4
    //   6: getstatic com/android/internal/R$styleable.AndroidManifestApplication : [I
    //   9: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   12: astore #8
    //   14: aload #8
    //   16: astore #9
    //   18: iconst_1
    //   19: istore #10
    //   21: aload #7
    //   23: iload #6
    //   25: aload #9
    //   27: bipush #7
    //   29: iconst_1
    //   30: invokevirtual getBoolean : (IZ)Z
    //   33: invokeinterface setSplitHasCode : (IZ)Landroid/content/pm/parsing/ParsingPackage;
    //   38: pop
    //   39: aload #9
    //   41: bipush #46
    //   43: invokevirtual getString : (I)Ljava/lang/String;
    //   46: astore #11
    //   48: aload #11
    //   50: ifnull -> 109
    //   53: aload #11
    //   55: invokestatic isValidClassLoaderName : (Ljava/lang/String;)Z
    //   58: ifeq -> 64
    //   61: goto -> 109
    //   64: new java/lang/StringBuilder
    //   67: astore_2
    //   68: aload_2
    //   69: invokespecial <init> : ()V
    //   72: aload_2
    //   73: ldc_w 'Invalid class loader name: '
    //   76: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   79: pop
    //   80: aload_2
    //   81: aload #11
    //   83: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   86: pop
    //   87: aload_1
    //   88: aload_2
    //   89: invokevirtual toString : ()Ljava/lang/String;
    //   92: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   97: astore_1
    //   98: aload #9
    //   100: invokevirtual recycle : ()V
    //   103: aload_1
    //   104: areturn
    //   105: astore_1
    //   106: goto -> 730
    //   109: aload #7
    //   111: iload #6
    //   113: aload #11
    //   115: invokeinterface setSplitClassLoaderName : (ILjava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   120: pop
    //   121: aload #9
    //   123: invokevirtual recycle : ()V
    //   126: aload #4
    //   128: invokeinterface getDepth : ()I
    //   133: istore #12
    //   135: aload #4
    //   137: invokeinterface next : ()I
    //   142: istore #13
    //   144: iload #13
    //   146: iload #10
    //   148: if_icmpeq -> 721
    //   151: iload #13
    //   153: iconst_3
    //   154: if_icmpne -> 175
    //   157: aload #4
    //   159: invokeinterface getDepth : ()I
    //   164: iload #12
    //   166: if_icmple -> 172
    //   169: goto -> 175
    //   172: goto -> 721
    //   175: iload #13
    //   177: iconst_2
    //   178: if_icmpeq -> 184
    //   181: goto -> 135
    //   184: aconst_null
    //   185: astore #8
    //   187: aconst_null
    //   188: astore #14
    //   190: aconst_null
    //   191: astore #11
    //   193: aconst_null
    //   194: astore #15
    //   196: aload #4
    //   198: invokeinterface getName : ()Ljava/lang/String;
    //   203: astore #16
    //   205: iconst_0
    //   206: istore #17
    //   208: iconst_m1
    //   209: istore #13
    //   211: aload #16
    //   213: invokevirtual hashCode : ()I
    //   216: lookupswitch default -> 268, -1655966961 -> 340, -987494927 -> 323, -808719889 -> 305, 790287890 -> 288, 1984153269 -> 271
    //   268: goto -> 354
    //   271: aload #16
    //   273: ldc_w 'service'
    //   276: invokevirtual equals : (Ljava/lang/Object;)Z
    //   279: ifeq -> 268
    //   282: iconst_2
    //   283: istore #13
    //   285: goto -> 354
    //   288: aload #16
    //   290: ldc_w 'activity-alias'
    //   293: invokevirtual equals : (Ljava/lang/Object;)Z
    //   296: ifeq -> 268
    //   299: iconst_4
    //   300: istore #13
    //   302: goto -> 354
    //   305: aload #16
    //   307: ldc_w 'receiver'
    //   310: invokevirtual equals : (Ljava/lang/Object;)Z
    //   313: ifeq -> 268
    //   316: iload #10
    //   318: istore #13
    //   320: goto -> 354
    //   323: aload #16
    //   325: ldc_w 'provider'
    //   328: invokevirtual equals : (Ljava/lang/Object;)Z
    //   331: ifeq -> 268
    //   334: iconst_3
    //   335: istore #13
    //   337: goto -> 354
    //   340: aload #16
    //   342: ldc_w 'activity'
    //   345: invokevirtual equals : (Ljava/lang/Object;)Z
    //   348: ifeq -> 268
    //   351: iconst_0
    //   352: istore #13
    //   354: iload #13
    //   356: ifeq -> 583
    //   359: iload #13
    //   361: iload #10
    //   363: if_icmpeq -> 576
    //   366: iload #13
    //   368: iconst_2
    //   369: if_icmpeq -> 518
    //   372: iload #13
    //   374: iconst_3
    //   375: if_icmpeq -> 456
    //   378: iload #13
    //   380: iconst_4
    //   381: if_icmpeq -> 403
    //   384: aload_0
    //   385: aload_1
    //   386: aload #16
    //   388: aload_2
    //   389: aload_3
    //   390: aload #4
    //   392: invokespecial parseSplitBaseAppChildTags : (Landroid/content/pm/parsing/result/ParseInput;Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;)Landroid/content/pm/parsing/result/ParseResult;
    //   395: astore #11
    //   397: aconst_null
    //   398: astore #8
    //   400: goto -> 671
    //   403: aload #7
    //   405: aload_3
    //   406: aload #4
    //   408: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   411: aload_1
    //   412: invokestatic parseActivityAlias : (Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   415: astore #11
    //   417: aload #15
    //   419: astore #8
    //   421: aload #11
    //   423: invokeinterface isSuccess : ()Z
    //   428: ifeq -> 453
    //   431: aload #11
    //   433: invokeinterface getResult : ()Ljava/lang/Object;
    //   438: checkcast android/content/pm/parsing/component/ParsedActivity
    //   441: astore #8
    //   443: aload #7
    //   445: aload #8
    //   447: invokeinterface addActivity : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   452: pop
    //   453: goto -> 671
    //   456: aload_0
    //   457: getfield mSeparateProcesses : [Ljava/lang/String;
    //   460: astore #11
    //   462: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   465: istore #18
    //   467: aload #11
    //   469: aload_2
    //   470: aload_3
    //   471: aload #4
    //   473: iload #5
    //   475: iload #18
    //   477: aload_1
    //   478: invokestatic parseProvider : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   481: astore #11
    //   483: aload #11
    //   485: invokeinterface isSuccess : ()Z
    //   490: ifeq -> 515
    //   493: aload #11
    //   495: invokeinterface getResult : ()Ljava/lang/Object;
    //   500: checkcast android/content/pm/parsing/component/ParsedProvider
    //   503: astore #8
    //   505: aload #7
    //   507: aload #8
    //   509: invokeinterface addProvider : (Landroid/content/pm/parsing/component/ParsedProvider;)Landroid/content/pm/parsing/ParsingPackage;
    //   514: pop
    //   515: goto -> 671
    //   518: aload_0
    //   519: getfield mSeparateProcesses : [Ljava/lang/String;
    //   522: aload_2
    //   523: aload_3
    //   524: aload #4
    //   526: iload #5
    //   528: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   531: aload_1
    //   532: invokestatic parseService : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   535: astore #11
    //   537: aload #14
    //   539: astore #8
    //   541: aload #11
    //   543: invokeinterface isSuccess : ()Z
    //   548: ifeq -> 573
    //   551: aload #11
    //   553: invokeinterface getResult : ()Ljava/lang/Object;
    //   558: checkcast android/content/pm/parsing/component/ParsedService
    //   561: astore #8
    //   563: aload #7
    //   565: aload #8
    //   567: invokeinterface addService : (Landroid/content/pm/parsing/component/ParsedService;)Landroid/content/pm/parsing/ParsingPackage;
    //   572: pop
    //   573: goto -> 671
    //   576: iload #17
    //   578: istore #13
    //   580: goto -> 586
    //   583: iconst_1
    //   584: istore #13
    //   586: aload_0
    //   587: getfield mSeparateProcesses : [Ljava/lang/String;
    //   590: astore #8
    //   592: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   595: istore #18
    //   597: aload #8
    //   599: aload_2
    //   600: aload_3
    //   601: aload #4
    //   603: iload #5
    //   605: iload #18
    //   607: aload_1
    //   608: invokestatic parseActivityOrReceiver : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   611: astore #15
    //   613: aload #11
    //   615: astore #8
    //   617: aload #15
    //   619: invokeinterface isSuccess : ()Z
    //   624: ifeq -> 667
    //   627: aload #15
    //   629: invokeinterface getResult : ()Ljava/lang/Object;
    //   634: checkcast android/content/pm/parsing/component/ParsedActivity
    //   637: astore #8
    //   639: iload #13
    //   641: ifeq -> 657
    //   644: aload #7
    //   646: aload #8
    //   648: invokeinterface addActivity : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   653: pop
    //   654: goto -> 667
    //   657: aload #7
    //   659: aload #8
    //   661: invokeinterface addReceiver : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   666: pop
    //   667: aload #15
    //   669: astore #11
    //   671: aload #11
    //   673: invokeinterface isError : ()Z
    //   678: ifeq -> 690
    //   681: aload_1
    //   682: aload #11
    //   684: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   689: areturn
    //   690: aload #8
    //   692: ifnull -> 718
    //   695: aload #8
    //   697: invokevirtual getSplitName : ()Ljava/lang/String;
    //   700: ifnonnull -> 718
    //   703: aload #8
    //   705: aload_2
    //   706: invokeinterface getSplitNames : ()[Ljava/lang/String;
    //   711: iload #6
    //   713: aaload
    //   714: invokevirtual setSplitName : (Ljava/lang/String;)Landroid/content/pm/parsing/component/ParsedMainComponent;
    //   717: pop
    //   718: goto -> 135
    //   721: aload_1
    //   722: aload_2
    //   723: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   728: areturn
    //   729: astore_1
    //   730: aload #8
    //   732: invokevirtual recycle : ()V
    //   735: aload_1
    //   736: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #579	-> 0
    //   #581	-> 18
    //   #584	-> 39
    //   #586	-> 48
    //   #590	-> 64
    //   #593	-> 98
    //   #590	-> 103
    //   #593	-> 105
    //   #588	-> 109
    //   #593	-> 121
    //   #594	-> 126
    //   #595	-> 126
    //   #597	-> 135
    //   #599	-> 157
    //   #600	-> 175
    //   #601	-> 181
    //   #604	-> 184
    //   #607	-> 196
    //   #608	-> 205
    //   #609	-> 208
    //   #663	-> 384
    //   #652	-> 403
    //   #654	-> 417
    //   #655	-> 431
    //   #656	-> 443
    //   #657	-> 453
    //   #660	-> 453
    //   #661	-> 453
    //   #641	-> 456
    //   #642	-> 467
    //   #644	-> 483
    //   #645	-> 493
    //   #646	-> 505
    //   #647	-> 515
    //   #649	-> 515
    //   #650	-> 515
    //   #630	-> 518
    //   #633	-> 537
    //   #634	-> 551
    //   #635	-> 563
    //   #636	-> 573
    //   #638	-> 573
    //   #639	-> 573
    //   #609	-> 576
    //   #611	-> 583
    //   #614	-> 586
    //   #615	-> 597
    //   #618	-> 613
    //   #619	-> 627
    //   #620	-> 639
    //   #621	-> 644
    //   #623	-> 657
    //   #625	-> 667
    //   #627	-> 667
    //   #628	-> 671
    //   #667	-> 671
    //   #668	-> 681
    //   #671	-> 690
    //   #676	-> 703
    //   #678	-> 718
    //   #597	-> 721
    //   #680	-> 721
    //   #593	-> 729
    //   #594	-> 735
    // Exception table:
    //   from	to	target	type
    //   21	39	729	finally
    //   39	48	729	finally
    //   53	61	105	finally
    //   64	98	105	finally
    //   109	121	729	finally
  }
  
  private ParseResult parseSplitBaseAppChildTags(ParseInput paramParseInput, String paramString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1964930885:
        if (paramString.equals("uses-package")) {
          b = 3;
          break;
        } 
      case 8960125:
        if (paramString.equals("uses-static-library")) {
          b = 1;
          break;
        } 
      case -1115949454:
        if (paramString.equals("meta-data")) {
          b = 0;
          break;
        } 
      case -1356765254:
        if (paramString.equals("uses-library")) {
          b = 2;
          break;
        } 
    } 
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3)
            return ParsingUtils.unknownTag("<application>", paramParsingPackage, paramXmlResourceParser, paramParseInput); 
          return paramParseInput.success(null);
        } 
        return parseUsesLibrary(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      } 
      return parseUsesStaticLibrary(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
    } 
    Bundle bundle = paramParsingPackage.getMetaData();
    ParseResult<Bundle> parseResult = parseMetaData(paramParsingPackage, paramResources, paramXmlResourceParser, bundle, paramParseInput);
    if (parseResult.isSuccess())
      paramParsingPackage.setMetaData(parseResult.getResult()); 
    return parseResult;
  }
  
  private ParseResult<ParsingPackage> parseBaseApkTags(ParseInput paramParseInput, ParsingPackage paramParsingPackage, TypedArray paramTypedArray, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: aload_2
    //   2: aload_3
    //   3: invokestatic parseSharedUser : (Landroid/content/pm/parsing/result/ParseInput;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/TypedArray;)Landroid/content/pm/parsing/result/ParseResult;
    //   6: astore #7
    //   8: aload #7
    //   10: invokeinterface isError : ()Z
    //   15: ifeq -> 21
    //   18: aload #7
    //   20: areturn
    //   21: aload_2
    //   22: iconst_m1
    //   23: iconst_4
    //   24: aload_3
    //   25: invokestatic anInteger : (IILandroid/content/res/TypedArray;)I
    //   28: invokeinterface setInstallLocation : (I)Landroid/content/pm/parsing/ParsingPackage;
    //   33: astore #7
    //   35: aload #7
    //   37: iconst_1
    //   38: bipush #7
    //   40: aload_3
    //   41: invokestatic anInteger : (IILandroid/content/res/TypedArray;)I
    //   44: invokeinterface setTargetSandboxVersion : (I)Landroid/content/pm/parsing/ParsingPackage;
    //   49: astore_3
    //   50: iload #6
    //   52: bipush #8
    //   54: iand
    //   55: ifeq -> 64
    //   58: iconst_1
    //   59: istore #8
    //   61: goto -> 67
    //   64: iconst_0
    //   65: istore #8
    //   67: aload_3
    //   68: iload #8
    //   70: invokeinterface setExternalStorage : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   75: pop
    //   76: aload #5
    //   78: invokeinterface getDepth : ()I
    //   83: istore #9
    //   85: iconst_0
    //   86: istore #10
    //   88: aload #5
    //   90: invokeinterface next : ()I
    //   95: istore #11
    //   97: iload #11
    //   99: iconst_1
    //   100: if_icmpeq -> 226
    //   103: iload #11
    //   105: iconst_3
    //   106: if_icmpne -> 121
    //   109: aload #5
    //   111: invokeinterface getDepth : ()I
    //   116: iload #9
    //   118: if_icmple -> 226
    //   121: iload #11
    //   123: iconst_2
    //   124: if_icmpeq -> 130
    //   127: goto -> 88
    //   130: aload #5
    //   132: invokeinterface getName : ()Ljava/lang/String;
    //   137: astore_3
    //   138: ldc_w 'application'
    //   141: aload_3
    //   142: invokevirtual equals : (Ljava/lang/Object;)Z
    //   145: ifeq -> 192
    //   148: iload #10
    //   150: ifeq -> 173
    //   153: ldc 'PackageParsing'
    //   155: ldc_w '<manifest> has more than one <application>'
    //   158: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   161: pop
    //   162: aload_1
    //   163: aconst_null
    //   164: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   169: astore_3
    //   170: goto -> 206
    //   173: iconst_1
    //   174: istore #10
    //   176: aload_0
    //   177: aload_1
    //   178: aload_2
    //   179: aload #4
    //   181: aload #5
    //   183: iload #6
    //   185: invokespecial parseBaseApplication : (Landroid/content/pm/parsing/result/ParseInput;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I)Landroid/content/pm/parsing/result/ParseResult;
    //   188: astore_3
    //   189: goto -> 206
    //   192: aload_0
    //   193: aload_3
    //   194: aload_1
    //   195: aload_2
    //   196: aload #4
    //   198: aload #5
    //   200: iload #6
    //   202: invokespecial parseBaseApkTag : (Ljava/lang/String;Landroid/content/pm/parsing/result/ParseInput;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I)Landroid/content/pm/parsing/result/ParseResult;
    //   205: astore_3
    //   206: aload_3
    //   207: invokeinterface isError : ()Z
    //   212: ifeq -> 223
    //   215: aload_1
    //   216: aload_3
    //   217: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   222: areturn
    //   223: goto -> 88
    //   226: iload #10
    //   228: ifne -> 273
    //   231: aload_2
    //   232: invokeinterface getInstrumentations : ()Ljava/util/List;
    //   237: invokestatic size : (Ljava/util/Collection;)I
    //   240: ifne -> 273
    //   243: aload_1
    //   244: ldc_w '<manifest> does not contain an <application> or <instrumentation>'
    //   247: ldc2_w 150776642
    //   250: invokeinterface deferError : (Ljava/lang/String;J)Landroid/content/pm/parsing/result/ParseResult;
    //   255: astore_3
    //   256: aload_3
    //   257: invokeinterface isError : ()Z
    //   262: ifeq -> 273
    //   265: aload_1
    //   266: aload_3
    //   267: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   272: areturn
    //   273: aload_2
    //   274: invokeinterface getAttributions : ()Ljava/util/List;
    //   279: invokestatic isCombinationValid : (Ljava/util/List;)Z
    //   282: ifne -> 297
    //   285: aload_1
    //   286: bipush #-101
    //   288: ldc_w 'Combination <feature> tags are not valid'
    //   291: invokeinterface error : (ILjava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   296: areturn
    //   297: aload_2
    //   298: invokestatic convertNewPermissions : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   301: aload_2
    //   302: invokestatic convertSplitPermissions : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   305: aload_2
    //   306: invokeinterface getTargetSdkVersion : ()I
    //   311: iconst_4
    //   312: if_icmplt -> 369
    //   315: aload_2
    //   316: invokeinterface isSupportsSmallScreens : ()Z
    //   321: ifne -> 373
    //   324: aload_2
    //   325: invokeinterface isSupportsNormalScreens : ()Z
    //   330: ifne -> 373
    //   333: aload_2
    //   334: invokeinterface isSupportsLargeScreens : ()Z
    //   339: ifne -> 373
    //   342: aload_2
    //   343: invokeinterface isSupportsExtraLargeScreens : ()Z
    //   348: ifne -> 373
    //   351: aload_2
    //   352: invokeinterface isResizeable : ()Z
    //   357: ifne -> 373
    //   360: aload_2
    //   361: invokeinterface isAnyDensity : ()Z
    //   366: ifne -> 373
    //   369: aload_2
    //   370: invokestatic adjustPackageToBeUnresizeableAndUnpipable : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   373: aload_1
    //   374: aload_2
    //   375: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   380: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #717	-> 0
    //   #718	-> 8
    //   #719	-> 18
    //   #722	-> 21
    //   #724	-> 35
    //   #727	-> 67
    //   #729	-> 76
    //   #730	-> 76
    //   #732	-> 88
    //   #734	-> 109
    //   #735	-> 121
    //   #736	-> 127
    //   #739	-> 130
    //   #744	-> 138
    //   #745	-> 148
    //   #749	-> 153
    //   #750	-> 162
    //   #753	-> 173
    //   #754	-> 176
    //   #757	-> 192
    //   #760	-> 206
    //   #761	-> 215
    //   #763	-> 223
    //   #765	-> 226
    //   #766	-> 243
    //   #769	-> 256
    //   #770	-> 265
    //   #774	-> 273
    //   #775	-> 285
    //   #781	-> 297
    //   #783	-> 301
    //   #788	-> 305
    //   #789	-> 315
    //   #790	-> 324
    //   #791	-> 333
    //   #792	-> 342
    //   #793	-> 351
    //   #794	-> 360
    //   #795	-> 369
    //   #798	-> 373
  }
  
  private ParseResult parseBaseApkTag(String paramString, ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt) throws IOException, XmlPullParserException {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1818228622:
        if (paramString.equals("compatible-screens")) {
          b = 20;
          break;
        } 
      case 1792785909:
        if (paramString.equals("uses-feature")) {
          b = 11;
          break;
        } 
      case 1705921021:
        if (paramString.equals("uses-permission-sdk-m")) {
          b = 8;
          break;
        } 
      case 1682371816:
        if (paramString.equals("feature-group")) {
          b = 12;
          break;
        } 
      case 1439495522:
        if (paramString.equals("protected-broadcast")) {
          b = 15;
          break;
        } 
      case 1343942321:
        if (paramString.equals("uses-permission-sdk-23")) {
          b = 9;
          break;
        } 
      case 896788286:
        if (paramString.equals("supports-screens")) {
          b = 14;
          break;
        } 
      case 655087462:
        if (paramString.equals("queries")) {
          b = 24;
          break;
        } 
      case 632228327:
        if (paramString.equals("adopt-permissions")) {
          b = 18;
          break;
        } 
      case 599862896:
        if (paramString.equals("uses-permission")) {
          b = 7;
          break;
        } 
      case 544550766:
        if (paramString.equals("instrumentation")) {
          b = 16;
          break;
        } 
      case 454915839:
        if (paramString.equals("key-sets")) {
          b = 1;
          break;
        } 
      case 349565761:
        if (paramString.equals("supports-input")) {
          b = 21;
          break;
        } 
      case 119109844:
        if (paramString.equals("uses-gl-texture")) {
          b = 19;
          break;
        } 
      case -129269526:
        if (paramString.equals("eat-comment")) {
          b = 22;
          break;
        } 
      case -170723071:
        if (paramString.equals("permission-group")) {
          b = 4;
          break;
        } 
      case -266709319:
        if (paramString.equals("uses-sdk")) {
          b = 13;
          break;
        } 
      case -309882753:
        if (paramString.equals("attribution")) {
          b = 3;
          break;
        } 
      case -517618225:
        if (paramString.equals("permission")) {
          b = 5;
          break;
        } 
      case -979207434:
        if (paramString.equals("feature")) {
          b = 2;
          break;
        } 
      case -998269702:
        if (paramString.equals("restrict-update")) {
          b = 23;
          break;
        } 
      case -1091287984:
        if (paramString.equals("overlay")) {
          b = 0;
          break;
        } 
      case -1108197302:
        if (paramString.equals("original-package")) {
          b = 17;
          break;
        } 
      case -1667688228:
        if (paramString.equals("permission-tree")) {
          b = 6;
          break;
        } 
      case -1773650763:
        if (paramString.equals("uses-configuration")) {
          b = 10;
          break;
        } 
    } 
    switch (b) {
      default:
        return ParsingUtils.unknownTag("<manifest>", paramParsingPackage, paramXmlResourceParser, paramParseInput);
      case 24:
        return parseQueries(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 23:
        return parseRestrictUpdateHash(paramInt, paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 19:
      case 20:
      case 21:
      case 22:
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        return paramParseInput.success(paramParsingPackage);
      case 18:
        return parseAdoptPermissions(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 17:
        return parseOriginalPackage(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 16:
        return parseInstrumentation(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 15:
        return parseProtectedBroadcast(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 14:
        return parseSupportScreens(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 13:
        return parseUsesSdk(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 12:
        return parseFeatureGroup(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 11:
        return parseUsesFeature(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 10:
        return parseUsesConfiguration(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 7:
      case 8:
      case 9:
        return parseUsesPermission(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 6:
        return parsePermissionTree(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 5:
        return parsePermission(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 4:
        return parsePermissionGroup(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 2:
      case 3:
        return parseAttribution(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 1:
        return parseKeySets(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 0:
        break;
    } 
    return parseOverlay(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
  }
  
  private static ParseResult<ParsingPackage> parseSharedUser(ParseInput paramParseInput, ParsingPackage paramParsingPackage, TypedArray paramTypedArray) {
    String str1, str2 = nonConfigString(0, 0, paramTypedArray);
    if (TextUtils.isEmpty(str2))
      return paramParseInput.success(paramParsingPackage); 
    if (!"android".equals(paramParsingPackage.getPackageName())) {
      ParseResult parseResult = validateName(paramParseInput, str2, true, true);
      if (parseResult.isError()) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<manifest> specifies bad sharedUserId name \"");
        stringBuilder.append(str2);
        stringBuilder.append("\": ");
        stringBuilder.append(parseResult.getErrorMessage());
        str1 = stringBuilder.toString();
        return paramParseInput.error(-107, str1);
      } 
    } 
    ParsingPackage parsingPackage = str1.setSharedUserId(str2.intern());
    parsingPackage = parsingPackage.setSharedUserLabel(resId(3, paramTypedArray));
    return paramParseInput.success(parsingPackage);
  }
  
  private static ParseResult<ParsingPackage> parseKeySets(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_3
    //   1: invokeinterface getDepth : ()I
    //   6: istore #4
    //   8: new android/util/ArrayMap
    //   11: dup
    //   12: invokespecial <init> : ()V
    //   15: astore #5
    //   17: new android/util/ArraySet
    //   20: dup
    //   21: invokespecial <init> : ()V
    //   24: astore #6
    //   26: new android/util/ArrayMap
    //   29: dup
    //   30: invokespecial <init> : ()V
    //   33: astore #7
    //   35: new android/util/ArraySet
    //   38: dup
    //   39: invokespecial <init> : ()V
    //   42: astore #8
    //   44: aconst_null
    //   45: astore #9
    //   47: iconst_m1
    //   48: istore #10
    //   50: aload_3
    //   51: invokeinterface next : ()I
    //   56: istore #11
    //   58: iload #11
    //   60: iconst_1
    //   61: if_icmpeq -> 830
    //   64: iload #11
    //   66: iconst_3
    //   67: if_icmpne -> 87
    //   70: aload_3
    //   71: invokeinterface getDepth : ()I
    //   76: iload #4
    //   78: if_icmple -> 84
    //   81: goto -> 87
    //   84: goto -> 830
    //   87: iload #11
    //   89: iconst_3
    //   90: if_icmpne -> 116
    //   93: aload_3
    //   94: invokeinterface getDepth : ()I
    //   99: iload #10
    //   101: if_icmpne -> 113
    //   104: aconst_null
    //   105: astore #9
    //   107: iconst_m1
    //   108: istore #10
    //   110: goto -> 50
    //   113: goto -> 570
    //   116: aload_3
    //   117: invokeinterface getName : ()Ljava/lang/String;
    //   122: astore #12
    //   124: aload #12
    //   126: invokevirtual hashCode : ()I
    //   129: istore #11
    //   131: iload #11
    //   133: ldc_w -1369233085
    //   136: if_icmpeq -> 192
    //   139: iload #11
    //   141: ldc_w -816609292
    //   144: if_icmpeq -> 175
    //   147: iload #11
    //   149: ldc_w 1903323387
    //   152: if_icmpeq -> 158
    //   155: goto -> 209
    //   158: aload #12
    //   160: ldc_w 'public-key'
    //   163: invokevirtual equals : (Ljava/lang/Object;)Z
    //   166: ifeq -> 155
    //   169: iconst_1
    //   170: istore #11
    //   172: goto -> 212
    //   175: aload #12
    //   177: ldc_w 'key-set'
    //   180: invokevirtual equals : (Ljava/lang/Object;)Z
    //   183: ifeq -> 155
    //   186: iconst_0
    //   187: istore #11
    //   189: goto -> 212
    //   192: aload #12
    //   194: ldc_w 'upgrade-key-set'
    //   197: invokevirtual equals : (Ljava/lang/Object;)Z
    //   200: ifeq -> 155
    //   203: iconst_2
    //   204: istore #11
    //   206: goto -> 212
    //   209: iconst_m1
    //   210: istore #11
    //   212: iload #11
    //   214: ifeq -> 723
    //   217: iload #11
    //   219: iconst_1
    //   220: if_icmpeq -> 308
    //   223: iload #11
    //   225: iconst_2
    //   226: if_icmpeq -> 262
    //   229: ldc_w '<key-sets>'
    //   232: aload_1
    //   233: aload_3
    //   234: aload_0
    //   235: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   238: astore #12
    //   240: aload #12
    //   242: invokeinterface isError : ()Z
    //   247: ifeq -> 259
    //   250: aload_0
    //   251: aload #12
    //   253: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   258: areturn
    //   259: goto -> 708
    //   262: aload_2
    //   263: aload_3
    //   264: getstatic com/android/internal/R$styleable.AndroidManifestUpgradeKeySet : [I
    //   267: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   270: astore #12
    //   272: aload #12
    //   274: iconst_0
    //   275: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   278: astore #13
    //   280: aload #6
    //   282: aload #13
    //   284: invokevirtual add : (Ljava/lang/Object;)Z
    //   287: pop
    //   288: aload_3
    //   289: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   292: aload #12
    //   294: invokevirtual recycle : ()V
    //   297: goto -> 708
    //   300: astore_0
    //   301: aload #12
    //   303: invokevirtual recycle : ()V
    //   306: aload_0
    //   307: athrow
    //   308: aload #9
    //   310: ifnonnull -> 353
    //   313: new java/lang/StringBuilder
    //   316: dup
    //   317: invokespecial <init> : ()V
    //   320: astore_1
    //   321: aload_1
    //   322: ldc_w 'Improperly nested 'key-set' tag at '
    //   325: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   328: pop
    //   329: aload_1
    //   330: aload_3
    //   331: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   336: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   339: pop
    //   340: aload_1
    //   341: invokevirtual toString : ()Ljava/lang/String;
    //   344: astore_1
    //   345: aload_0
    //   346: aload_1
    //   347: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   352: areturn
    //   353: aload_2
    //   354: aload_3
    //   355: getstatic com/android/internal/R$styleable.AndroidManifestPublicKey : [I
    //   358: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   361: astore #12
    //   363: iconst_0
    //   364: aload #12
    //   366: invokestatic nonResString : (ILandroid/content/res/TypedArray;)Ljava/lang/String;
    //   369: astore #13
    //   371: iconst_1
    //   372: aload #12
    //   374: invokestatic nonResString : (ILandroid/content/res/TypedArray;)Ljava/lang/String;
    //   377: astore #14
    //   379: aload #14
    //   381: ifnonnull -> 464
    //   384: aload #5
    //   386: aload #13
    //   388: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   391: ifnonnull -> 464
    //   394: new java/lang/StringBuilder
    //   397: astore_1
    //   398: aload_1
    //   399: invokespecial <init> : ()V
    //   402: aload_1
    //   403: ldc_w ''public-key' '
    //   406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: aload_1
    //   411: aload #13
    //   413: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   416: pop
    //   417: aload_1
    //   418: ldc_w ' must define a public-key value on first use at '
    //   421: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   424: pop
    //   425: aload_1
    //   426: aload_3
    //   427: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   432: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   435: pop
    //   436: aload_1
    //   437: invokevirtual toString : ()Ljava/lang/String;
    //   440: astore_1
    //   441: aload_0
    //   442: aload_1
    //   443: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   448: astore_0
    //   449: aload #12
    //   451: invokevirtual recycle : ()V
    //   454: aload_0
    //   455: areturn
    //   456: astore_0
    //   457: goto -> 716
    //   460: astore_0
    //   461: goto -> 716
    //   464: aload #14
    //   466: ifnull -> 683
    //   469: aload #14
    //   471: invokestatic parsePublicKey : (Ljava/lang/String;)Ljava/security/PublicKey;
    //   474: astore #14
    //   476: aload #14
    //   478: ifnonnull -> 573
    //   481: new java/lang/StringBuilder
    //   484: astore #13
    //   486: aload #13
    //   488: invokespecial <init> : ()V
    //   491: aload #13
    //   493: ldc_w 'No recognized valid key in 'public-key' tag at '
    //   496: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   499: pop
    //   500: aload #13
    //   502: aload_3
    //   503: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   508: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   511: pop
    //   512: aload #13
    //   514: ldc_w ' key-set '
    //   517: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   520: pop
    //   521: aload #13
    //   523: aload #9
    //   525: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   528: pop
    //   529: aload #13
    //   531: ldc_w ' will not be added to the package's defined key-sets.'
    //   534: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   537: pop
    //   538: aload #13
    //   540: invokevirtual toString : ()Ljava/lang/String;
    //   543: astore #13
    //   545: ldc 'PackageParsing'
    //   547: aload #13
    //   549: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   552: pop
    //   553: aload #8
    //   555: aload #9
    //   557: invokevirtual add : (Ljava/lang/Object;)Z
    //   560: pop
    //   561: aload_3
    //   562: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   565: aload #12
    //   567: invokevirtual recycle : ()V
    //   570: goto -> 50
    //   573: aload #5
    //   575: aload #13
    //   577: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   580: ifnull -> 666
    //   583: aload #5
    //   585: aload #13
    //   587: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   590: checkcast java/security/PublicKey
    //   593: aload #14
    //   595: invokevirtual equals : (Ljava/lang/Object;)Z
    //   598: ifeq -> 604
    //   601: goto -> 666
    //   604: new java/lang/StringBuilder
    //   607: astore_1
    //   608: aload_1
    //   609: invokespecial <init> : ()V
    //   612: aload_1
    //   613: ldc_w 'Value of 'public-key' '
    //   616: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   619: pop
    //   620: aload_1
    //   621: aload #13
    //   623: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   626: pop
    //   627: aload_1
    //   628: ldc_w ' conflicts with previously defined value at '
    //   631: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   634: pop
    //   635: aload_1
    //   636: aload_3
    //   637: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   642: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   645: pop
    //   646: aload_1
    //   647: invokevirtual toString : ()Ljava/lang/String;
    //   650: astore_1
    //   651: aload_0
    //   652: aload_1
    //   653: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   658: astore_0
    //   659: aload #12
    //   661: invokevirtual recycle : ()V
    //   664: aload_0
    //   665: areturn
    //   666: aload #5
    //   668: aload #13
    //   670: aload #14
    //   672: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   675: pop
    //   676: goto -> 683
    //   679: astore_0
    //   680: goto -> 716
    //   683: aload #7
    //   685: aload #9
    //   687: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   690: checkcast android/util/ArraySet
    //   693: aload #13
    //   695: invokevirtual add : (Ljava/lang/Object;)Z
    //   698: pop
    //   699: aload_3
    //   700: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   703: aload #12
    //   705: invokevirtual recycle : ()V
    //   708: goto -> 819
    //   711: astore_0
    //   712: goto -> 716
    //   715: astore_0
    //   716: aload #12
    //   718: invokevirtual recycle : ()V
    //   721: aload_0
    //   722: athrow
    //   723: aload #9
    //   725: ifnull -> 768
    //   728: new java/lang/StringBuilder
    //   731: dup
    //   732: invokespecial <init> : ()V
    //   735: astore_1
    //   736: aload_1
    //   737: ldc_w 'Improperly nested 'key-set' tag at '
    //   740: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   743: pop
    //   744: aload_1
    //   745: aload_3
    //   746: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   751: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   754: pop
    //   755: aload_1
    //   756: invokevirtual toString : ()Ljava/lang/String;
    //   759: astore_1
    //   760: aload_0
    //   761: aload_1
    //   762: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   767: areturn
    //   768: aload_2
    //   769: aload_3
    //   770: getstatic com/android/internal/R$styleable.AndroidManifestKeySet : [I
    //   773: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   776: astore #12
    //   778: aload #12
    //   780: iconst_0
    //   781: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   784: astore #9
    //   786: new android/util/ArraySet
    //   789: astore #13
    //   791: aload #13
    //   793: invokespecial <init> : ()V
    //   796: aload #7
    //   798: aload #9
    //   800: aload #13
    //   802: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   805: pop
    //   806: aload_3
    //   807: invokeinterface getDepth : ()I
    //   812: istore #10
    //   814: aload #12
    //   816: invokevirtual recycle : ()V
    //   819: goto -> 50
    //   822: astore_0
    //   823: aload #12
    //   825: invokevirtual recycle : ()V
    //   828: aload_0
    //   829: athrow
    //   830: aload_1
    //   831: invokeinterface getPackageName : ()Ljava/lang/String;
    //   836: astore_3
    //   837: aload #5
    //   839: invokevirtual keySet : ()Ljava/util/Set;
    //   842: astore_2
    //   843: aload_2
    //   844: aload #7
    //   846: invokevirtual keySet : ()Ljava/util/Set;
    //   849: invokeinterface removeAll : (Ljava/util/Collection;)Z
    //   854: ifeq -> 898
    //   857: new java/lang/StringBuilder
    //   860: dup
    //   861: invokespecial <init> : ()V
    //   864: astore_1
    //   865: aload_1
    //   866: ldc_w 'Package'
    //   869: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   872: pop
    //   873: aload_1
    //   874: aload_3
    //   875: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   878: pop
    //   879: aload_1
    //   880: ldc_w ' AndroidManifest.xml 'key-set' and 'public-key' names must be distinct.'
    //   883: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   886: pop
    //   887: aload_0
    //   888: aload_1
    //   889: invokevirtual toString : ()Ljava/lang/String;
    //   892: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   897: areturn
    //   898: aload #7
    //   900: invokevirtual entrySet : ()Ljava/util/Set;
    //   903: invokeinterface iterator : ()Ljava/util/Iterator;
    //   908: astore #12
    //   910: aload #12
    //   912: invokeinterface hasNext : ()Z
    //   917: ifeq -> 1141
    //   920: aload #12
    //   922: invokeinterface next : ()Ljava/lang/Object;
    //   927: checkcast java/util/Map$Entry
    //   930: astore_2
    //   931: aload_2
    //   932: invokeinterface getKey : ()Ljava/lang/Object;
    //   937: checkcast java/lang/String
    //   940: astore #9
    //   942: aload_2
    //   943: invokeinterface getValue : ()Ljava/lang/Object;
    //   948: checkcast android/util/ArraySet
    //   951: invokevirtual size : ()I
    //   954: ifne -> 1015
    //   957: new java/lang/StringBuilder
    //   960: dup
    //   961: invokespecial <init> : ()V
    //   964: astore_2
    //   965: aload_2
    //   966: ldc_w 'Package'
    //   969: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   972: pop
    //   973: aload_2
    //   974: aload_3
    //   975: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   978: pop
    //   979: aload_2
    //   980: ldc_w ' AndroidManifest.xml 'key-set' '
    //   983: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   986: pop
    //   987: aload_2
    //   988: aload #9
    //   990: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   993: pop
    //   994: aload_2
    //   995: ldc_w ' has no valid associated 'public-key'. Not including in package's defined key-sets.'
    //   998: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1001: pop
    //   1002: ldc 'PackageParsing'
    //   1004: aload_2
    //   1005: invokevirtual toString : ()Ljava/lang/String;
    //   1008: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1011: pop
    //   1012: goto -> 910
    //   1015: aload #8
    //   1017: aload #9
    //   1019: invokevirtual contains : (Ljava/lang/Object;)Z
    //   1022: ifeq -> 1083
    //   1025: new java/lang/StringBuilder
    //   1028: dup
    //   1029: invokespecial <init> : ()V
    //   1032: astore_2
    //   1033: aload_2
    //   1034: ldc_w 'Package'
    //   1037: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1040: pop
    //   1041: aload_2
    //   1042: aload_3
    //   1043: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1046: pop
    //   1047: aload_2
    //   1048: ldc_w ' AndroidManifest.xml 'key-set' '
    //   1051: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1054: pop
    //   1055: aload_2
    //   1056: aload #9
    //   1058: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1061: pop
    //   1062: aload_2
    //   1063: ldc_w ' contained improper 'public-key' tags. Not including in package's defined key-sets.'
    //   1066: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1069: pop
    //   1070: ldc 'PackageParsing'
    //   1072: aload_2
    //   1073: invokevirtual toString : ()Ljava/lang/String;
    //   1076: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   1079: pop
    //   1080: goto -> 910
    //   1083: aload_2
    //   1084: invokeinterface getValue : ()Ljava/lang/Object;
    //   1089: checkcast android/util/ArraySet
    //   1092: invokevirtual iterator : ()Ljava/util/Iterator;
    //   1095: astore_2
    //   1096: aload_2
    //   1097: invokeinterface hasNext : ()Z
    //   1102: ifeq -> 1138
    //   1105: aload_2
    //   1106: invokeinterface next : ()Ljava/lang/Object;
    //   1111: checkcast java/lang/String
    //   1114: astore #7
    //   1116: aload_1
    //   1117: aload #9
    //   1119: aload #5
    //   1121: aload #7
    //   1123: invokevirtual get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   1126: checkcast java/security/PublicKey
    //   1129: invokeinterface addKeySet : (Ljava/lang/String;Ljava/security/PublicKey;)Landroid/content/pm/parsing/ParsingPackage;
    //   1134: pop
    //   1135: goto -> 1096
    //   1138: goto -> 910
    //   1141: aload_1
    //   1142: invokeinterface getKeySetMapping : ()Ljava/util/Map;
    //   1147: invokeinterface keySet : ()Ljava/util/Set;
    //   1152: aload #6
    //   1154: invokeinterface containsAll : (Ljava/util/Collection;)Z
    //   1159: ifeq -> 1179
    //   1162: aload_1
    //   1163: aload #6
    //   1165: invokeinterface setUpgradeKeySets : (Ljava/util/Set;)Landroid/content/pm/parsing/ParsingPackage;
    //   1170: pop
    //   1171: aload_0
    //   1172: aload_1
    //   1173: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   1178: areturn
    //   1179: new java/lang/StringBuilder
    //   1182: dup
    //   1183: invokespecial <init> : ()V
    //   1186: astore_1
    //   1187: aload_1
    //   1188: ldc_w 'Package'
    //   1191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1194: pop
    //   1195: aload_1
    //   1196: aload_3
    //   1197: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1200: pop
    //   1201: aload_1
    //   1202: ldc_w ' AndroidManifest.xml does not define all 'upgrade-key-set's .'
    //   1205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1208: pop
    //   1209: aload_0
    //   1210: aload_1
    //   1211: invokevirtual toString : ()Ljava/lang/String;
    //   1214: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   1219: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #883	-> 0
    //   #884	-> 8
    //   #886	-> 8
    //   #887	-> 8
    //   #888	-> 17
    //   #889	-> 26
    //   #890	-> 35
    //   #891	-> 50
    //   #892	-> 70
    //   #893	-> 87
    //   #894	-> 93
    //   #895	-> 104
    //   #896	-> 107
    //   #894	-> 113
    //   #900	-> 116
    //   #901	-> 124
    //   #975	-> 229
    //   #977	-> 240
    //   #978	-> 250
    //   #977	-> 259
    //   #963	-> 262
    //   #966	-> 272
    //   #968	-> 280
    //   #969	-> 288
    //   #971	-> 292
    //   #972	-> 297
    //   #973	-> 297
    //   #971	-> 300
    //   #972	-> 306
    //   #919	-> 308
    //   #920	-> 313
    //   #921	-> 329
    //   #920	-> 345
    //   #923	-> 353
    //   #926	-> 363
    //   #928	-> 371
    //   #930	-> 379
    //   #931	-> 394
    //   #933	-> 425
    //   #931	-> 441
    //   #959	-> 449
    //   #931	-> 454
    //   #959	-> 456
    //   #930	-> 464
    //   #934	-> 464
    //   #935	-> 469
    //   #936	-> 476
    //   #937	-> 481
    //   #938	-> 500
    //   #937	-> 545
    //   #941	-> 553
    //   #942	-> 561
    //   #959	-> 565
    //   #943	-> 570
    //   #891	-> 570
    //   #945	-> 573
    //   #946	-> 583
    //   #951	-> 604
    //   #953	-> 635
    //   #951	-> 651
    //   #959	-> 659
    //   #951	-> 664
    //   #949	-> 666
    //   #959	-> 679
    //   #934	-> 683
    //   #956	-> 683
    //   #957	-> 699
    //   #959	-> 703
    //   #960	-> 708
    //   #961	-> 708
    //   #982	-> 708
    //   #959	-> 711
    //   #960	-> 721
    //   #903	-> 723
    //   #904	-> 728
    //   #905	-> 744
    //   #904	-> 760
    //   #907	-> 768
    //   #909	-> 778
    //   #911	-> 786
    //   #912	-> 806
    //   #913	-> 806
    //   #915	-> 814
    //   #916	-> 819
    //   #917	-> 819
    //   #982	-> 819
    //   #915	-> 822
    //   #916	-> 828
    //   #891	-> 830
    //   #983	-> 830
    //   #984	-> 837
    //   #985	-> 843
    //   #986	-> 857
    //   #990	-> 898
    //   #991	-> 931
    //   #992	-> 942
    //   #993	-> 957
    //   #996	-> 1012
    //   #997	-> 1015
    //   #998	-> 1025
    //   #1001	-> 1080
    //   #1004	-> 1083
    //   #1005	-> 1116
    //   #1006	-> 1135
    //   #1007	-> 1138
    //   #1008	-> 1141
    //   #1009	-> 1162
    //   #1015	-> 1171
    //   #1011	-> 1179
    // Exception table:
    //   from	to	target	type
    //   272	280	300	finally
    //   280	288	300	finally
    //   288	292	300	finally
    //   363	371	715	finally
    //   371	379	715	finally
    //   384	394	460	finally
    //   394	402	460	finally
    //   402	425	456	finally
    //   425	441	456	finally
    //   441	449	456	finally
    //   469	476	679	finally
    //   481	500	711	finally
    //   500	545	711	finally
    //   545	553	711	finally
    //   553	561	711	finally
    //   561	565	711	finally
    //   573	583	711	finally
    //   583	601	711	finally
    //   604	635	711	finally
    //   635	651	711	finally
    //   651	659	711	finally
    //   666	676	711	finally
    //   683	699	711	finally
    //   699	703	711	finally
    //   778	786	822	finally
    //   786	806	822	finally
    //   806	814	822	finally
  }
  
  private static ParseResult<ParsingPackage> parseAttribution(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    ParseResult<ParsedAttribution> parseResult = ParsedAttributionUtils.parseAttribution(paramResources, paramXmlResourceParser, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.addAttribution(parseResult.getResult()));
  }
  
  private static ParseResult<ParsingPackage> parsePermissionGroup(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    ParseResult<ParsedPermissionGroup> parseResult = ParsedPermissionUtils.parsePermissionGroup(paramParsingPackage, paramResources, paramXmlResourceParser, PackageParser.sUseRoundIcon, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.addPermissionGroup(parseResult.getResult()));
  }
  
  private static ParseResult<ParsingPackage> parsePermission(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    ParseResult<ParsedPermission> parseResult = ParsedPermissionUtils.parsePermission(paramParsingPackage, paramResources, paramXmlResourceParser, PackageParser.sUseRoundIcon, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.addPermission(parseResult.getResult()));
  }
  
  private static ParseResult<ParsingPackage> parsePermissionTree(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    ParseResult<ParsedPermission> parseResult = ParsedPermissionUtils.parsePermissionTree(paramParsingPackage, paramResources, paramXmlResourceParser, PackageParser.sUseRoundIcon, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.addPermission(parseResult.getResult()));
  }
  
  private ParseResult<ParsingPackage> parseUsesPermission(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesPermission);
    try {
      String str1 = typedArray.getNonResourceString(0);
      int i = 0;
      TypedValue typedValue = typedArray.peekValue(1);
      int j = i;
      if (typedValue != null) {
        j = i;
        if (typedValue.type >= 16) {
          j = i;
          if (typedValue.type <= 31)
            j = typedValue.data; 
        } 
      } 
      String str3 = typedArray.getNonConfigurationString(2, 0);
      String str2 = typedArray.getNonConfigurationString(3, 0);
      XmlUtils.skipCurrentTag(paramXmlResourceParser);
      ParseResult<ParsingPackage> parseResult = paramParseInput.success(paramParsingPackage);
      if (str1 == null)
        return parseResult; 
      if (j != 0) {
        i = Build.VERSION.RESOURCES_SDK_INT;
        if (j < i)
          return parseResult; 
      } 
      if (str3 != null && this.mCallback != null) {
        boolean bool = this.mCallback.hasFeature(str3);
        if (!bool)
          return parseResult; 
      } 
      if (str2 != null && this.mCallback != null) {
        Callback callback = this.mCallback;
        boolean bool = callback.hasFeature(str2);
        if (bool)
          return parseResult; 
      } 
      if (!paramParsingPackage.getRequestedPermissions().contains(str1)) {
        paramParsingPackage.addRequestedPermission(str1.intern());
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Ignoring duplicate uses-permissions/uses-permissions-sdk-m: ");
        stringBuilder.append(str1);
        stringBuilder.append(" in package: ");
        stringBuilder.append(paramParsingPackage.getPackageName());
        stringBuilder.append(" at: ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str = stringBuilder.toString();
        Slog.w("PackageParsing", str);
      } 
      return parseResult;
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseUsesConfiguration(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    ConfigurationInfo configurationInfo = new ConfigurationInfo();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesConfiguration);
    try {
      configurationInfo.reqTouchScreen = typedArray.getInt(0, 0);
      configurationInfo.reqKeyboardType = typedArray.getInt(1, 0);
      if (typedArray.getBoolean(2, false))
        configurationInfo.reqInputFeatures = 0x1 | configurationInfo.reqInputFeatures; 
      configurationInfo.reqNavigation = typedArray.getInt(3, 0);
      if (typedArray.getBoolean(4, false))
        configurationInfo.reqInputFeatures |= 0x2; 
      paramParsingPackage.addConfigPreference(configurationInfo);
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseUsesFeature(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    FeatureInfo featureInfo = parseFeatureInfo(paramResources, paramXmlResourceParser);
    paramParsingPackage.addReqFeature(featureInfo);
    if (featureInfo.name == null) {
      ConfigurationInfo configurationInfo = new ConfigurationInfo();
      configurationInfo.reqGlEsVersion = featureInfo.reqGlEsVersion;
      paramParsingPackage.addConfigPreference(configurationInfo);
    } 
    return paramParseInput.success(paramParsingPackage);
  }
  
  private static FeatureInfo parseFeatureInfo(Resources paramResources, AttributeSet paramAttributeSet) {
    FeatureInfo featureInfo = new FeatureInfo();
    TypedArray typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestUsesFeature);
    try {
      featureInfo.name = typedArray.getNonResourceString(0);
      featureInfo.version = typedArray.getInt(3, 0);
      if (featureInfo.name == null)
        featureInfo.reqGlEsVersion = typedArray.getInt(1, 0); 
      if (typedArray.getBoolean(2, true))
        featureInfo.flags |= 0x1; 
      return featureInfo;
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseFeatureGroup(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    // Byte code:
    //   0: new android/content/pm/FeatureGroupInfo
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore #4
    //   9: aconst_null
    //   10: astore #5
    //   12: aload_3
    //   13: invokeinterface getDepth : ()I
    //   18: istore #6
    //   20: aload_3
    //   21: invokeinterface next : ()I
    //   26: istore #7
    //   28: iload #7
    //   30: iconst_1
    //   31: if_icmpeq -> 196
    //   34: iload #7
    //   36: iconst_3
    //   37: if_icmpne -> 51
    //   40: aload_3
    //   41: invokeinterface getDepth : ()I
    //   46: iload #6
    //   48: if_icmple -> 196
    //   51: iload #7
    //   53: iconst_2
    //   54: if_icmpeq -> 60
    //   57: goto -> 20
    //   60: aload_3
    //   61: invokeinterface getName : ()Ljava/lang/String;
    //   66: astore #8
    //   68: aload #8
    //   70: ldc_w 'uses-feature'
    //   73: invokevirtual equals : (Ljava/lang/Object;)Z
    //   76: ifeq -> 110
    //   79: aload_2
    //   80: aload_3
    //   81: invokestatic parseFeatureInfo : (Landroid/content/res/Resources;Landroid/util/AttributeSet;)Landroid/content/pm/FeatureInfo;
    //   84: astore #8
    //   86: aload #8
    //   88: iconst_1
    //   89: aload #8
    //   91: getfield flags : I
    //   94: ior
    //   95: putfield flags : I
    //   98: aload #5
    //   100: aload #8
    //   102: invokestatic add : (Ljava/util/ArrayList;Ljava/lang/Object;)Ljava/util/ArrayList;
    //   105: astore #5
    //   107: goto -> 193
    //   110: new java/lang/StringBuilder
    //   113: dup
    //   114: invokespecial <init> : ()V
    //   117: astore #9
    //   119: aload #9
    //   121: ldc_w 'Unknown element under <feature-group>: '
    //   124: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   127: pop
    //   128: aload #9
    //   130: aload #8
    //   132: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   135: pop
    //   136: aload #9
    //   138: ldc_w ' at '
    //   141: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: aload #9
    //   147: aload_1
    //   148: invokeinterface getBaseCodePath : ()Ljava/lang/String;
    //   153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload #9
    //   159: ldc_w ' '
    //   162: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   165: pop
    //   166: aload #9
    //   168: aload_3
    //   169: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   174: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   177: pop
    //   178: aload #9
    //   180: invokevirtual toString : ()Ljava/lang/String;
    //   183: astore #8
    //   185: ldc 'PackageParsing'
    //   187: aload #8
    //   189: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   192: pop
    //   193: goto -> 20
    //   196: aload #5
    //   198: ifnull -> 232
    //   201: aload #4
    //   203: aload #5
    //   205: invokevirtual size : ()I
    //   208: anewarray android/content/pm/FeatureInfo
    //   211: putfield features : [Landroid/content/pm/FeatureInfo;
    //   214: aload #4
    //   216: aload #5
    //   218: aload #4
    //   220: getfield features : [Landroid/content/pm/FeatureInfo;
    //   223: invokevirtual toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   226: checkcast [Landroid/content/pm/FeatureInfo;
    //   229: putfield features : [Landroid/content/pm/FeatureInfo;
    //   232: aload_1
    //   233: aload #4
    //   235: invokeinterface addFeatureGroup : (Landroid/content/pm/FeatureGroupInfo;)Landroid/content/pm/parsing/ParsingPackage;
    //   240: pop
    //   241: aload_0
    //   242: aload_1
    //   243: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   248: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1197	-> 0
    //   #1198	-> 9
    //   #1199	-> 12
    //   #1201	-> 20
    //   #1203	-> 40
    //   #1204	-> 51
    //   #1205	-> 57
    //   #1208	-> 60
    //   #1209	-> 68
    //   #1210	-> 79
    //   #1213	-> 86
    //   #1214	-> 98
    //   #1215	-> 107
    //   #1216	-> 110
    //   #1218	-> 145
    //   #1219	-> 166
    //   #1216	-> 185
    //   #1221	-> 193
    //   #1223	-> 196
    //   #1224	-> 201
    //   #1225	-> 214
    //   #1228	-> 232
    //   #1229	-> 241
  }
  
  private static ParseResult<ParsingPackage> parseUsesSdk(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    if (PackageParser.SDK_VERSION > 0) {
      TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesSdk);
      int i = 1;
      String str1 = null;
      int j = 0;
      String str2 = null;
      try {
        ParseResult<?> parseResult;
        String str3;
        SparseIntArray sparseIntArray;
        TypedValue typedValue2 = typedArray.peekValue(0);
        int k = i;
        String str4 = str1;
        if (typedValue2 != null)
          if (typedValue2.type == 3 && typedValue2.string != null) {
            str4 = typedValue2.string.toString();
            k = i;
          } else {
            k = typedValue2.data;
            str4 = str1;
          }  
        TypedValue typedValue1 = typedArray.peekValue(1);
        if (typedValue1 != null) {
          if (typedValue1.type == 3 && typedValue1.string != null) {
            String str = typedValue1.string.toString();
            str3 = str4;
            i = j;
            str2 = str;
            if (str4 == null) {
              str3 = str;
              i = j;
              str2 = str;
            } 
          } else {
            i = ((TypedValue)str3).data;
            str3 = str4;
          } 
        } else {
          i = k;
          str2 = str4;
          str3 = str4;
        } 
        ParseResult<Integer> parseResult1 = computeTargetSdkVersion(i, str2, PackageParser.SDK_CODENAMES, paramParseInput);
        if (parseResult1.isError()) {
          parseResult = paramParseInput.error(parseResult1);
          return (ParseResult)parseResult;
        } 
        j = ((Integer)parseResult1.getResult()).intValue();
        parseResult1 = (ParseResult)parseResult.enableDeferredError(paramParsingPackage.getPackageName(), j);
        if (parseResult1.isError()) {
          parseResult = parseResult.error(parseResult1);
          return (ParseResult)parseResult;
        } 
        parseResult1 = computeMinSdkVersion(k, str3, PackageParser.SDK_VERSION, PackageParser.SDK_CODENAMES, (ParseInput)parseResult);
        if (parseResult1.isError()) {
          parseResult = parseResult.error(parseResult1);
          return (ParseResult)parseResult;
        } 
        i = ((Integer)parseResult1.getResult()).intValue();
        ParsingPackage parsingPackage = paramParsingPackage.setMinSdkVersion(i);
        parsingPackage.setTargetSdkVersion(j);
        i = paramXmlResourceParser.getDepth();
        parsingPackage = null;
        while (true) {
          j = paramXmlResourceParser.next();
          if (j != 1 && (j != 3 || 
            paramXmlResourceParser.getDepth() > i)) {
            ParseResult<?> parseResult2;
            if (j == 3 || j == 4)
              continue; 
            if (paramXmlResourceParser.getName().equals("extension-sdk")) {
              if (parsingPackage == null) {
                sparseIntArray = new SparseIntArray();
                this();
              } 
              parseResult2 = parseExtensionSdk((ParseInput)parseResult, paramResources, paramXmlResourceParser, sparseIntArray);
              XmlUtils.skipCurrentTag(paramXmlResourceParser);
            } else {
              parseResult2 = ParsingUtils.unknownTag("<uses-sdk>", paramParsingPackage, paramXmlResourceParser, (ParseInput)parseResult);
            } 
            if (parseResult2.isError()) {
              parseResult = parseResult.error(parseResult2);
              return (ParseResult)parseResult;
            } 
            continue;
          } 
          break;
        } 
        paramParsingPackage.setMinExtensionVersions(exactSizedCopyOfSparseArray(sparseIntArray));
      } finally {
        typedArray.recycle();
      } 
    } 
    return paramParseInput.success(paramParsingPackage);
  }
  
  private static SparseIntArray exactSizedCopyOfSparseArray(SparseIntArray paramSparseIntArray) {
    if (paramSparseIntArray == null)
      return null; 
    SparseIntArray sparseIntArray = new SparseIntArray(paramSparseIntArray.size());
    for (byte b = 0; b < paramSparseIntArray.size(); b++)
      sparseIntArray.put(paramSparseIntArray.keyAt(b), paramSparseIntArray.valueAt(b)); 
    return sparseIntArray;
  }
  
  private static ParseResult<SparseIntArray> parseExtensionSdk(ParseInput paramParseInput, Resources paramResources, XmlResourceParser paramXmlResourceParser, SparseIntArray paramSparseIntArray) {
    StringBuilder stringBuilder;
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestExtensionSdk);
    try {
      int i = typedArray.getInt(0, -1);
      int j = typedArray.getInt(1, -1);
      typedArray.recycle();
      if (i < 0)
        return paramParseInput.error(-108, "<extension-sdk> must specify an sdkVersion >= 0"); 
      if (j < 0)
        return paramParseInput.error(-108, "<extension-sdk> must specify minExtensionVersion >= 0"); 
    } finally {
      stringBuilder.recycle();
    } 
  }
  
  public static ParseResult<Integer> computeMinSdkVersion(int paramInt1, String paramString, int paramInt2, String[] paramArrayOfString, ParseInput paramParseInput) {
    StringBuilder stringBuilder1;
    String str;
    if (paramString == null) {
      if (paramInt1 <= paramInt2)
        return paramParseInput.success(Integer.valueOf(paramInt1)); 
      stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Requires newer sdk version #");
      stringBuilder1.append(paramInt1);
      stringBuilder1.append(" (current version is #");
      stringBuilder1.append(paramInt2);
      stringBuilder1.append(")");
      return paramParseInput.error(-12, stringBuilder1.toString());
    } 
    if (matchTargetCode(paramArrayOfString, (String)stringBuilder1))
      return paramParseInput.success(Integer.valueOf(10000)); 
    if (paramArrayOfString.length > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Requires development platform ");
      stringBuilder.append((String)stringBuilder1);
      stringBuilder.append(" (current platform is any of ");
      stringBuilder.append(Arrays.toString((Object[])paramArrayOfString));
      stringBuilder.append(")");
      str = stringBuilder.toString();
      return paramParseInput.error(-12, str);
    } 
    StringBuilder stringBuilder2 = new StringBuilder();
    stringBuilder2.append("Requires development platform ");
    stringBuilder2.append(str);
    stringBuilder2.append(" but this is a release platform.");
    return paramParseInput.error(-12, stringBuilder2.toString());
  }
  
  public static ParseResult<Integer> computeTargetSdkVersion(int paramInt, String paramString, String[] paramArrayOfString, ParseInput paramParseInput) {
    if (paramString == null)
      return paramParseInput.success(Integer.valueOf(paramInt)); 
    if (matchTargetCode(paramArrayOfString, paramString))
      return paramParseInput.success(Integer.valueOf(10000)); 
    if (paramArrayOfString.length > 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Requires development platform ");
      stringBuilder1.append(paramString);
      stringBuilder1.append(" (current platform is any of ");
      stringBuilder1.append(Arrays.toString((Object[])paramArrayOfString));
      stringBuilder1.append(")");
      paramString = stringBuilder1.toString();
      return paramParseInput.error(-12, paramString);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Requires development platform ");
    stringBuilder.append(paramString);
    stringBuilder.append(" but this is a release platform.");
    return paramParseInput.error(-12, stringBuilder.toString());
  }
  
  private static boolean matchTargetCode(String[] paramArrayOfString, String paramString) {
    int i = paramString.indexOf('.');
    if (i != -1)
      paramString = paramString.substring(0, i); 
    return ArrayUtils.contains((Object[])paramArrayOfString, paramString);
  }
  
  private static ParseResult<ParsingPackage> parseRestrictUpdateHash(int paramInt, ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    if ((paramInt & 0x10) != 0) {
      TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestRestrictUpdate);
      try {
        String str = typedArray.getNonConfigurationString(0, 0);
        if (str != null) {
          int i = str.length();
          byte[] arrayOfByte = new byte[i / 2];
          for (paramInt = 0; paramInt < i; paramInt += 2) {
            int j = paramInt / 2, k = Character.digit(str.charAt(paramInt), 16);
            arrayOfByte[j] = (byte)((k << 4) + Character.digit(str.charAt(paramInt + 1), 16));
          } 
          paramParsingPackage.setRestrictUpdateHash(arrayOfByte);
        } else {
          paramParsingPackage.setRestrictUpdateHash((byte[])null);
        } 
      } finally {
        typedArray.recycle();
      } 
    } 
    return paramParseInput.success(paramParsingPackage);
  }
  
  private static ParseResult<ParsingPackage> parseQueries(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws IOException, XmlPullParserException {
    // Byte code:
    //   0: aload_3
    //   1: invokeinterface getDepth : ()I
    //   6: istore #4
    //   8: aload_3
    //   9: invokeinterface next : ()I
    //   14: istore #5
    //   16: iload #5
    //   18: iconst_1
    //   19: if_icmpeq -> 697
    //   22: iload #5
    //   24: iconst_3
    //   25: if_icmpne -> 39
    //   28: aload_3
    //   29: invokeinterface getDepth : ()I
    //   34: iload #4
    //   36: if_icmple -> 697
    //   39: iload #5
    //   41: iconst_2
    //   42: if_icmpeq -> 48
    //   45: goto -> 8
    //   48: aload_3
    //   49: invokeinterface getName : ()Ljava/lang/String;
    //   54: ldc_w 'intent'
    //   57: invokevirtual equals : (Ljava/lang/Object;)Z
    //   60: ifeq -> 512
    //   63: aconst_null
    //   64: aload_1
    //   65: aload_2
    //   66: aload_3
    //   67: iconst_1
    //   68: iconst_1
    //   69: aload_0
    //   70: invokestatic parseIntentInfo : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   73: astore #6
    //   75: aload #6
    //   77: invokeinterface isError : ()Z
    //   82: ifeq -> 94
    //   85: aload_0
    //   86: aload #6
    //   88: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   93: areturn
    //   94: aload #6
    //   96: invokeinterface getResult : ()Ljava/lang/Object;
    //   101: checkcast android/content/pm/parsing/component/ParsedIntentInfo
    //   104: astore #7
    //   106: aconst_null
    //   107: astore #8
    //   109: aconst_null
    //   110: astore #6
    //   112: aconst_null
    //   113: astore #9
    //   115: aload #7
    //   117: invokevirtual countActions : ()I
    //   120: istore #10
    //   122: aload #7
    //   124: invokevirtual countDataSchemes : ()I
    //   127: istore #11
    //   129: aload #7
    //   131: invokevirtual countDataTypes : ()I
    //   134: istore #12
    //   136: aload #7
    //   138: invokevirtual getHosts : ()[Ljava/lang/String;
    //   141: arraylength
    //   142: istore #13
    //   144: iload #11
    //   146: ifne -> 169
    //   149: iload #12
    //   151: ifne -> 169
    //   154: iload #10
    //   156: ifne -> 169
    //   159: aload_0
    //   160: ldc_w 'intent tags must contain either an action or data.'
    //   163: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   168: areturn
    //   169: iload #10
    //   171: iconst_1
    //   172: if_icmple -> 185
    //   175: aload_0
    //   176: ldc_w 'intent tag may have at most one action.'
    //   179: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   184: areturn
    //   185: iload #12
    //   187: iconst_1
    //   188: if_icmple -> 201
    //   191: aload_0
    //   192: ldc_w 'intent tag may have at most one data type.'
    //   195: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   200: areturn
    //   201: iload #11
    //   203: iconst_1
    //   204: if_icmple -> 217
    //   207: aload_0
    //   208: ldc_w 'intent tag may have at most one data scheme.'
    //   211: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   216: areturn
    //   217: iload #13
    //   219: iconst_1
    //   220: if_icmple -> 233
    //   223: aload_0
    //   224: ldc_w 'intent tag may have at most one data host.'
    //   227: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   232: areturn
    //   233: new android/content/Intent
    //   236: dup
    //   237: invokespecial <init> : ()V
    //   240: astore #14
    //   242: aload #7
    //   244: invokevirtual countCategories : ()I
    //   247: istore #15
    //   249: iconst_0
    //   250: istore #5
    //   252: iload #5
    //   254: iload #15
    //   256: if_icmpge -> 278
    //   259: aload #14
    //   261: aload #7
    //   263: iload #5
    //   265: invokevirtual getCategory : (I)Ljava/lang/String;
    //   268: invokevirtual addCategory : (Ljava/lang/String;)Landroid/content/Intent;
    //   271: pop
    //   272: iinc #5, 1
    //   275: goto -> 252
    //   278: iload #13
    //   280: iconst_1
    //   281: if_icmpne -> 296
    //   284: aload #7
    //   286: invokevirtual getHosts : ()[Ljava/lang/String;
    //   289: iconst_0
    //   290: aaload
    //   291: astore #9
    //   293: goto -> 296
    //   296: iload #11
    //   298: iconst_1
    //   299: if_icmpne -> 353
    //   302: new android/net/Uri$Builder
    //   305: dup
    //   306: invokespecial <init> : ()V
    //   309: astore #8
    //   311: aload #8
    //   313: aload #7
    //   315: iconst_0
    //   316: invokevirtual getDataScheme : (I)Ljava/lang/String;
    //   319: invokevirtual scheme : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   322: astore #8
    //   324: aload #8
    //   326: aload #9
    //   328: invokevirtual authority : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   331: astore #8
    //   333: aload #8
    //   335: ldc_w '/*'
    //   338: invokevirtual path : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   341: astore #8
    //   343: aload #8
    //   345: invokevirtual build : ()Landroid/net/Uri;
    //   348: astore #8
    //   350: goto -> 353
    //   353: iload #12
    //   355: iconst_1
    //   356: if_icmpne -> 472
    //   359: aload #7
    //   361: iconst_0
    //   362: invokevirtual getDataType : (I)Ljava/lang/String;
    //   365: astore #9
    //   367: aload #9
    //   369: astore #6
    //   371: aload #9
    //   373: ldc_w '/'
    //   376: invokevirtual contains : (Ljava/lang/CharSequence;)Z
    //   379: ifne -> 415
    //   382: new java/lang/StringBuilder
    //   385: dup
    //   386: invokespecial <init> : ()V
    //   389: astore #6
    //   391: aload #6
    //   393: aload #9
    //   395: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   398: pop
    //   399: aload #6
    //   401: ldc_w '/*'
    //   404: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   407: pop
    //   408: aload #6
    //   410: invokevirtual toString : ()Ljava/lang/String;
    //   413: astore #6
    //   415: aload #8
    //   417: ifnonnull -> 469
    //   420: new android/net/Uri$Builder
    //   423: dup
    //   424: invokespecial <init> : ()V
    //   427: astore #8
    //   429: aload #8
    //   431: ldc_w 'content'
    //   434: invokevirtual scheme : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   437: astore #8
    //   439: aload #8
    //   441: ldc_w '*'
    //   444: invokevirtual authority : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   447: astore #8
    //   449: aload #8
    //   451: ldc_w '/*'
    //   454: invokevirtual path : (Ljava/lang/String;)Landroid/net/Uri$Builder;
    //   457: astore #8
    //   459: aload #8
    //   461: invokevirtual build : ()Landroid/net/Uri;
    //   464: astore #8
    //   466: goto -> 472
    //   469: goto -> 472
    //   472: aload #14
    //   474: aload #8
    //   476: aload #6
    //   478: invokevirtual setDataAndType : (Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    //   481: pop
    //   482: iload #10
    //   484: iconst_1
    //   485: if_icmpne -> 500
    //   488: aload #14
    //   490: aload #7
    //   492: iconst_0
    //   493: invokevirtual getAction : (I)Ljava/lang/String;
    //   496: invokevirtual setAction : (Ljava/lang/String;)Landroid/content/Intent;
    //   499: pop
    //   500: aload_1
    //   501: aload #14
    //   503: invokeinterface addQueriesIntent : (Landroid/content/Intent;)Landroid/content/pm/parsing/ParsingPackage;
    //   508: pop
    //   509: goto -> 8
    //   512: aload_3
    //   513: invokeinterface getName : ()Ljava/lang/String;
    //   518: ldc_w 'package'
    //   521: invokevirtual equals : (Ljava/lang/Object;)Z
    //   524: ifeq -> 579
    //   527: aload_2
    //   528: aload_3
    //   529: getstatic com/android/internal/R$styleable.AndroidManifestQueriesPackage : [I
    //   532: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   535: astore #6
    //   537: aload #6
    //   539: iconst_0
    //   540: iconst_0
    //   541: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   544: astore #6
    //   546: aload #6
    //   548: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   551: ifeq -> 564
    //   554: aload_0
    //   555: ldc_w 'Package name is missing from package tag.'
    //   558: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   563: areturn
    //   564: aload_1
    //   565: aload #6
    //   567: invokevirtual intern : ()Ljava/lang/String;
    //   570: invokeinterface addQueriesPackage : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   575: pop
    //   576: goto -> 694
    //   579: aload_3
    //   580: invokeinterface getName : ()Ljava/lang/String;
    //   585: ldc_w 'provider'
    //   588: invokevirtual equals : (Ljava/lang/Object;)Z
    //   591: ifeq -> 694
    //   594: aload_2
    //   595: aload_3
    //   596: getstatic com/android/internal/R$styleable.AndroidManifestQueriesProvider : [I
    //   599: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   602: astore #6
    //   604: aload #6
    //   606: iconst_0
    //   607: iconst_0
    //   608: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   611: astore #8
    //   613: aload #8
    //   615: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   618: ifeq -> 640
    //   621: aload_0
    //   622: bipush #-108
    //   624: ldc_w 'Authority missing from provider tag.'
    //   627: invokeinterface error : (ILjava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   632: astore_0
    //   633: aload #6
    //   635: invokevirtual recycle : ()V
    //   638: aload_0
    //   639: areturn
    //   640: new java/util/StringTokenizer
    //   643: astore #9
    //   645: aload #9
    //   647: aload #8
    //   649: ldc_w ';'
    //   652: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   655: aload #9
    //   657: invokevirtual hasMoreElements : ()Z
    //   660: ifeq -> 678
    //   663: aload_1
    //   664: aload #9
    //   666: invokevirtual nextToken : ()Ljava/lang/String;
    //   669: invokeinterface addQueriesProvider : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   674: pop
    //   675: goto -> 655
    //   678: aload #6
    //   680: invokevirtual recycle : ()V
    //   683: goto -> 8
    //   686: astore_0
    //   687: aload #6
    //   689: invokevirtual recycle : ()V
    //   692: aload_0
    //   693: athrow
    //   694: goto -> 8
    //   697: aload_0
    //   698: aload_1
    //   699: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   704: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1495	-> 0
    //   #1497	-> 8
    //   #1499	-> 28
    //   #1500	-> 39
    //   #1501	-> 45
    //   #1503	-> 48
    //   #1504	-> 63
    //   #1506	-> 75
    //   #1507	-> 85
    //   #1510	-> 94
    //   #1512	-> 106
    //   #1513	-> 109
    //   #1514	-> 112
    //   #1515	-> 115
    //   #1516	-> 122
    //   #1517	-> 129
    //   #1518	-> 136
    //   #1519	-> 144
    //   #1520	-> 159
    //   #1522	-> 169
    //   #1523	-> 175
    //   #1525	-> 185
    //   #1526	-> 191
    //   #1528	-> 201
    //   #1529	-> 207
    //   #1531	-> 217
    //   #1532	-> 223
    //   #1534	-> 233
    //   #1535	-> 242
    //   #1536	-> 259
    //   #1535	-> 272
    //   #1538	-> 278
    //   #1539	-> 284
    //   #1538	-> 296
    //   #1541	-> 296
    //   #1542	-> 302
    //   #1543	-> 311
    //   #1544	-> 324
    //   #1545	-> 333
    //   #1546	-> 343
    //   #1541	-> 353
    //   #1548	-> 353
    //   #1549	-> 359
    //   #1552	-> 367
    //   #1553	-> 382
    //   #1555	-> 415
    //   #1556	-> 420
    //   #1557	-> 429
    //   #1558	-> 439
    //   #1559	-> 449
    //   #1560	-> 459
    //   #1555	-> 469
    //   #1548	-> 472
    //   #1563	-> 472
    //   #1564	-> 482
    //   #1565	-> 488
    //   #1567	-> 500
    //   #1568	-> 509
    //   #1569	-> 527
    //   #1571	-> 537
    //   #1573	-> 546
    //   #1574	-> 554
    //   #1576	-> 564
    //   #1577	-> 579
    //   #1578	-> 594
    //   #1581	-> 604
    //   #1583	-> 613
    //   #1584	-> 621
    //   #1594	-> 633
    //   #1584	-> 638
    //   #1589	-> 640
    //   #1590	-> 655
    //   #1591	-> 663
    //   #1594	-> 678
    //   #1595	-> 683
    //   #1596	-> 683
    //   #1594	-> 686
    //   #1595	-> 692
    //   #1577	-> 694
    //   #1598	-> 697
    // Exception table:
    //   from	to	target	type
    //   604	613	686	finally
    //   613	621	686	finally
    //   621	633	686	finally
    //   640	655	686	finally
    //   655	663	686	finally
    //   663	675	686	finally
  }
  
  private ParseResult<ParsingPackage> parseBaseApplication(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_2
    //   1: invokeinterface getPackageName : ()Ljava/lang/String;
    //   6: astore #6
    //   8: aload_2
    //   9: invokeinterface getTargetSdkVersion : ()I
    //   14: istore #7
    //   16: aload_3
    //   17: aload #4
    //   19: getstatic com/android/internal/R$styleable.AndroidManifestApplication : [I
    //   22: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   25: astore #8
    //   27: aload #8
    //   29: ifnonnull -> 53
    //   32: aload_1
    //   33: ldc_w '<application> does not contain any attributes'
    //   36: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   41: astore_1
    //   42: aload #8
    //   44: invokevirtual recycle : ()V
    //   47: aload_1
    //   48: areturn
    //   49: astore_1
    //   50: goto -> 1888
    //   53: aload #8
    //   55: iconst_3
    //   56: iconst_0
    //   57: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   60: astore #9
    //   62: aload #9
    //   64: ifnull -> 167
    //   67: aload_2
    //   68: invokeinterface getPackageName : ()Ljava/lang/String;
    //   73: astore #10
    //   75: aload #10
    //   77: aload #9
    //   79: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   82: astore #9
    //   84: getstatic android/content/pm/PackageManager.APP_DETAILS_ACTIVITY_CLASS_NAME : Ljava/lang/String;
    //   87: aload #9
    //   89: invokevirtual equals : (Ljava/lang/Object;)Z
    //   92: ifeq -> 112
    //   95: aload_1
    //   96: ldc_w '<application> invalid android:name'
    //   99: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   104: astore_1
    //   105: aload #8
    //   107: invokevirtual recycle : ()V
    //   110: aload_1
    //   111: areturn
    //   112: aload #9
    //   114: ifnonnull -> 158
    //   117: new java/lang/StringBuilder
    //   120: astore_2
    //   121: aload_2
    //   122: invokespecial <init> : ()V
    //   125: aload_2
    //   126: ldc_w 'Empty class name in package '
    //   129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload_2
    //   134: aload #10
    //   136: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   139: pop
    //   140: aload_1
    //   141: aload_2
    //   142: invokevirtual toString : ()Ljava/lang/String;
    //   145: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   150: astore_1
    //   151: aload #8
    //   153: invokevirtual recycle : ()V
    //   156: aload_1
    //   157: areturn
    //   158: aload_2
    //   159: aload #9
    //   161: invokeinterface setClassName : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   166: pop
    //   167: aload #8
    //   169: iconst_1
    //   170: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   173: astore #10
    //   175: aload #10
    //   177: ifnull -> 212
    //   180: aload_2
    //   181: aload #10
    //   183: getfield resourceId : I
    //   186: invokeinterface setLabelRes : (I)Landroid/content/pm/parsing/ParsingPackage;
    //   191: pop
    //   192: aload #10
    //   194: getfield resourceId : I
    //   197: ifne -> 212
    //   200: aload_2
    //   201: aload #10
    //   203: invokevirtual coerceToString : ()Ljava/lang/CharSequence;
    //   206: invokeinterface setNonLocalizedLabel : (Ljava/lang/CharSequence;)Landroid/content/pm/parsing/ParsingPackage;
    //   211: pop
    //   212: aload_0
    //   213: aload_2
    //   214: aload #8
    //   216: invokespecial parseBaseAppBasicFlags : (Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/TypedArray;)V
    //   219: sipush #1024
    //   222: iconst_4
    //   223: aload #8
    //   225: invokestatic nonConfigString : (IILandroid/content/res/TypedArray;)Ljava/lang/String;
    //   228: astore #10
    //   230: aload #10
    //   232: ifnull -> 299
    //   235: aload #6
    //   237: aload #10
    //   239: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   242: astore #10
    //   244: aload #10
    //   246: ifnonnull -> 290
    //   249: new java/lang/StringBuilder
    //   252: astore_2
    //   253: aload_2
    //   254: invokespecial <init> : ()V
    //   257: aload_2
    //   258: ldc_w 'Empty class name in package '
    //   261: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   264: pop
    //   265: aload_2
    //   266: aload #6
    //   268: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   271: pop
    //   272: aload_1
    //   273: aload_2
    //   274: invokevirtual toString : ()Ljava/lang/String;
    //   277: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   282: astore_1
    //   283: aload #8
    //   285: invokevirtual recycle : ()V
    //   288: aload_1
    //   289: areturn
    //   290: aload_2
    //   291: aload #10
    //   293: invokeinterface setManageSpaceActivityName : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   298: pop
    //   299: aload_2
    //   300: invokeinterface isAllowBackup : ()Z
    //   305: istore #11
    //   307: iload #11
    //   309: ifeq -> 522
    //   312: sipush #1024
    //   315: bipush #16
    //   317: aload #8
    //   319: invokestatic nonConfigString : (IILandroid/content/res/TypedArray;)Ljava/lang/String;
    //   322: astore #10
    //   324: aload #10
    //   326: ifnull -> 464
    //   329: aload #6
    //   331: aload #10
    //   333: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   336: astore #10
    //   338: aload #10
    //   340: ifnonnull -> 384
    //   343: new java/lang/StringBuilder
    //   346: astore_2
    //   347: aload_2
    //   348: invokespecial <init> : ()V
    //   351: aload_2
    //   352: ldc_w 'Empty class name in package '
    //   355: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   358: pop
    //   359: aload_2
    //   360: aload #6
    //   362: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   365: pop
    //   366: aload_1
    //   367: aload_2
    //   368: invokevirtual toString : ()Ljava/lang/String;
    //   371: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   376: astore_1
    //   377: aload #8
    //   379: invokevirtual recycle : ()V
    //   382: aload_1
    //   383: areturn
    //   384: aload_2
    //   385: aload #10
    //   387: invokeinterface setBackupAgentName : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   392: astore #10
    //   394: aload #10
    //   396: iconst_1
    //   397: bipush #18
    //   399: aload #8
    //   401: invokestatic bool : (ZILandroid/content/res/TypedArray;)Z
    //   404: invokeinterface setKillAfterRestore : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   409: astore #10
    //   411: aload #10
    //   413: iconst_0
    //   414: bipush #21
    //   416: aload #8
    //   418: invokestatic bool : (ZILandroid/content/res/TypedArray;)Z
    //   421: invokeinterface setRestoreAnyVersion : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   426: astore #10
    //   428: aload #10
    //   430: iconst_0
    //   431: bipush #32
    //   433: aload #8
    //   435: invokestatic bool : (ZILandroid/content/res/TypedArray;)Z
    //   438: invokeinterface setFullBackupOnly : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   443: astore #10
    //   445: aload #10
    //   447: iconst_0
    //   448: bipush #40
    //   450: aload #8
    //   452: invokestatic bool : (ZILandroid/content/res/TypedArray;)Z
    //   455: invokeinterface setBackupInForeground : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   460: pop
    //   461: goto -> 464
    //   464: aload #8
    //   466: bipush #35
    //   468: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   471: astore #10
    //   473: aload #10
    //   475: ifnull -> 522
    //   478: aload #10
    //   480: getfield resourceId : I
    //   483: istore #12
    //   485: aload #10
    //   487: getfield resourceId : I
    //   490: ifne -> 510
    //   493: aload #10
    //   495: getfield data : I
    //   498: ifne -> 507
    //   501: iconst_m1
    //   502: istore #12
    //   504: goto -> 510
    //   507: iconst_0
    //   508: istore #12
    //   510: aload_2
    //   511: iload #12
    //   513: invokeinterface setFullBackupContent : (I)Landroid/content/pm/parsing/ParsingPackage;
    //   518: pop
    //   519: goto -> 522
    //   522: aload #8
    //   524: bipush #8
    //   526: iconst_0
    //   527: invokevirtual getBoolean : (IZ)Z
    //   530: istore #11
    //   532: iload #11
    //   534: ifeq -> 586
    //   537: aload #8
    //   539: bipush #45
    //   541: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   544: astore #10
    //   546: aload #10
    //   548: ifnull -> 574
    //   551: aload_0
    //   552: getfield mCallback : Landroid/content/pm/parsing/ParsingPackageUtils$Callback;
    //   555: aload #10
    //   557: invokeinterface hasFeature : (Ljava/lang/String;)Z
    //   562: ifeq -> 568
    //   565: goto -> 574
    //   568: iconst_0
    //   569: istore #11
    //   571: goto -> 577
    //   574: iconst_1
    //   575: istore #11
    //   577: aload_2
    //   578: iload #11
    //   580: invokeinterface setPersistent : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   585: pop
    //   586: aload_2
    //   587: invokeinterface isProfileableByShell : ()Z
    //   592: istore #11
    //   594: iload #11
    //   596: ifne -> 621
    //   599: aload_2
    //   600: invokeinterface isDebuggable : ()Z
    //   605: istore #11
    //   607: iload #11
    //   609: ifeq -> 615
    //   612: goto -> 621
    //   615: iconst_0
    //   616: istore #11
    //   618: goto -> 624
    //   621: iconst_1
    //   622: istore #11
    //   624: aload_2
    //   625: iload #11
    //   627: invokeinterface setProfileableByShell : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   632: pop
    //   633: aload #8
    //   635: bipush #37
    //   637: invokevirtual hasValueOrEmpty : (I)Z
    //   640: istore #11
    //   642: iload #11
    //   644: ifeq -> 668
    //   647: aload_2
    //   648: aload #8
    //   650: bipush #37
    //   652: iconst_1
    //   653: invokevirtual getBoolean : (IZ)Z
    //   656: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   659: invokeinterface setResizeableActivity : (Ljava/lang/Boolean;)Landroid/content/pm/parsing/ParsingPackage;
    //   664: pop
    //   665: goto -> 693
    //   668: iload #7
    //   670: bipush #24
    //   672: if_icmplt -> 681
    //   675: iconst_1
    //   676: istore #11
    //   678: goto -> 684
    //   681: iconst_0
    //   682: istore #11
    //   684: aload_2
    //   685: iload #11
    //   687: invokeinterface setResizeableActivityViaSdkVersion : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   692: pop
    //   693: iload #7
    //   695: bipush #8
    //   697: if_icmplt -> 715
    //   700: aload #8
    //   702: bipush #12
    //   704: sipush #1024
    //   707: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   710: astore #10
    //   712: goto -> 724
    //   715: aload #8
    //   717: bipush #12
    //   719: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   722: astore #10
    //   724: aload #6
    //   726: aload #6
    //   728: aload #10
    //   730: aload_1
    //   731: invokestatic buildTaskAffinityName : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   734: astore #10
    //   736: aload #10
    //   738: invokeinterface isError : ()Z
    //   743: istore #11
    //   745: iload #11
    //   747: ifeq -> 766
    //   750: aload_1
    //   751: aload #10
    //   753: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   758: astore_1
    //   759: aload #8
    //   761: invokevirtual recycle : ()V
    //   764: aload_1
    //   765: areturn
    //   766: aload_2
    //   767: aload #10
    //   769: invokeinterface getResult : ()Ljava/lang/Object;
    //   774: checkcast java/lang/String
    //   777: invokeinterface setTaskAffinity : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   782: pop
    //   783: aload #8
    //   785: bipush #48
    //   787: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   790: astore #10
    //   792: aload #10
    //   794: ifnull -> 861
    //   797: aload #6
    //   799: aload #10
    //   801: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   804: astore #10
    //   806: aload #10
    //   808: ifnonnull -> 852
    //   811: new java/lang/StringBuilder
    //   814: astore_2
    //   815: aload_2
    //   816: invokespecial <init> : ()V
    //   819: aload_2
    //   820: ldc_w 'Empty class name in package '
    //   823: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   826: pop
    //   827: aload_2
    //   828: aload #6
    //   830: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   833: pop
    //   834: aload_1
    //   835: aload_2
    //   836: invokevirtual toString : ()Ljava/lang/String;
    //   839: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   844: astore_1
    //   845: aload #8
    //   847: invokevirtual recycle : ()V
    //   850: aload_1
    //   851: areturn
    //   852: aload_2
    //   853: aload #10
    //   855: invokeinterface setAppComponentFactory : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   860: pop
    //   861: iload #7
    //   863: bipush #8
    //   865: if_icmplt -> 883
    //   868: aload #8
    //   870: bipush #11
    //   872: sipush #1024
    //   875: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   878: astore #10
    //   880: goto -> 892
    //   883: aload #8
    //   885: bipush #11
    //   887: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   890: astore #10
    //   892: aload #6
    //   894: aconst_null
    //   895: aload #10
    //   897: iload #5
    //   899: aload_0
    //   900: getfield mSeparateProcesses : [Ljava/lang/String;
    //   903: aload_1
    //   904: invokestatic buildProcessName : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   907: astore #10
    //   909: aload #10
    //   911: invokeinterface isError : ()Z
    //   916: istore #11
    //   918: iload #11
    //   920: ifeq -> 939
    //   923: aload_1
    //   924: aload #10
    //   926: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   931: astore_1
    //   932: aload #8
    //   934: invokevirtual recycle : ()V
    //   937: aload_1
    //   938: areturn
    //   939: aload #10
    //   941: invokeinterface getResult : ()Ljava/lang/Object;
    //   946: checkcast java/lang/String
    //   949: astore #10
    //   951: aload_2
    //   952: aload #10
    //   954: invokeinterface setProcessName : (Ljava/lang/String;)Landroid/content/pm/parsing/ParsingPackage;
    //   959: pop
    //   960: aload_2
    //   961: invokeinterface isCantSaveState : ()Z
    //   966: istore #11
    //   968: iload #11
    //   970: ifeq -> 1005
    //   973: aload #10
    //   975: ifnull -> 1005
    //   978: aload #10
    //   980: aload #6
    //   982: invokevirtual equals : (Ljava/lang/Object;)Z
    //   985: ifne -> 1005
    //   988: aload_1
    //   989: ldc_w 'cantSaveState applications can not use custom processes'
    //   992: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   997: astore_1
    //   998: aload #8
    //   1000: invokevirtual recycle : ()V
    //   1003: aload_1
    //   1004: areturn
    //   1005: aload_2
    //   1006: invokeinterface getClassLoaderName : ()Ljava/lang/String;
    //   1011: astore #10
    //   1013: aload #10
    //   1015: ifnull -> 1067
    //   1018: aload #10
    //   1020: invokestatic isValidClassLoaderName : (Ljava/lang/String;)Z
    //   1023: ifne -> 1067
    //   1026: new java/lang/StringBuilder
    //   1029: astore_2
    //   1030: aload_2
    //   1031: invokespecial <init> : ()V
    //   1034: aload_2
    //   1035: ldc_w 'Invalid class loader name: '
    //   1038: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1041: pop
    //   1042: aload_2
    //   1043: aload #10
    //   1045: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1048: pop
    //   1049: aload_1
    //   1050: aload_2
    //   1051: invokevirtual toString : ()Ljava/lang/String;
    //   1054: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   1059: astore_1
    //   1060: aload #8
    //   1062: invokevirtual recycle : ()V
    //   1065: aload_1
    //   1066: areturn
    //   1067: iconst_m1
    //   1068: istore #13
    //   1070: aload_2
    //   1071: aload #8
    //   1073: bipush #62
    //   1075: iconst_m1
    //   1076: invokevirtual getInt : (II)I
    //   1079: invokeinterface setGwpAsanMode : (I)Landroid/content/pm/parsing/ParsingPackage;
    //   1084: pop
    //   1085: aload #8
    //   1087: invokevirtual recycle : ()V
    //   1090: iconst_0
    //   1091: istore #7
    //   1093: aload #4
    //   1095: invokeinterface getDepth : ()I
    //   1100: istore #14
    //   1102: iconst_0
    //   1103: istore #15
    //   1105: iconst_0
    //   1106: istore #16
    //   1108: aload #4
    //   1110: invokeinterface next : ()I
    //   1115: istore #12
    //   1117: iload #12
    //   1119: iconst_1
    //   1120: if_icmpeq -> 1767
    //   1123: iload #12
    //   1125: iconst_3
    //   1126: if_icmpne -> 1147
    //   1129: aload #4
    //   1131: invokeinterface getDepth : ()I
    //   1136: iload #14
    //   1138: if_icmple -> 1144
    //   1141: goto -> 1147
    //   1144: goto -> 1767
    //   1147: iload #12
    //   1149: iconst_2
    //   1150: if_icmpeq -> 1156
    //   1153: goto -> 1108
    //   1156: aload #4
    //   1158: invokeinterface getName : ()Ljava/lang/String;
    //   1163: astore #10
    //   1165: iconst_0
    //   1166: istore #17
    //   1168: aload #10
    //   1170: invokevirtual hashCode : ()I
    //   1173: lookupswitch default -> 1224, -1655966961 -> 1295, -987494927 -> 1278, -808719889 -> 1261, 790287890 -> 1244, 1984153269 -> 1227
    //   1224: goto -> 1312
    //   1227: aload #10
    //   1229: ldc_w 'service'
    //   1232: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1235: ifeq -> 1224
    //   1238: iconst_2
    //   1239: istore #12
    //   1241: goto -> 1316
    //   1244: aload #10
    //   1246: ldc_w 'activity-alias'
    //   1249: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1252: ifeq -> 1224
    //   1255: iconst_4
    //   1256: istore #12
    //   1258: goto -> 1316
    //   1261: aload #10
    //   1263: ldc_w 'receiver'
    //   1266: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1269: ifeq -> 1224
    //   1272: iconst_1
    //   1273: istore #12
    //   1275: goto -> 1316
    //   1278: aload #10
    //   1280: ldc_w 'provider'
    //   1283: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1286: ifeq -> 1224
    //   1289: iconst_3
    //   1290: istore #12
    //   1292: goto -> 1316
    //   1295: aload #10
    //   1297: ldc_w 'activity'
    //   1300: invokevirtual equals : (Ljava/lang/Object;)Z
    //   1303: ifeq -> 1224
    //   1306: iconst_0
    //   1307: istore #12
    //   1309: goto -> 1316
    //   1312: iload #13
    //   1314: istore #12
    //   1316: iload #12
    //   1318: ifeq -> 1595
    //   1321: iload #12
    //   1323: iconst_1
    //   1324: if_icmpeq -> 1592
    //   1327: iload #12
    //   1329: iconst_2
    //   1330: if_icmpeq -> 1499
    //   1333: iload #12
    //   1335: iconst_3
    //   1336: if_icmpeq -> 1442
    //   1339: iload #12
    //   1341: iconst_4
    //   1342: if_icmpeq -> 1363
    //   1345: aload_0
    //   1346: aload_1
    //   1347: aload #10
    //   1349: aload_2
    //   1350: aload_3
    //   1351: aload #4
    //   1353: iload #5
    //   1355: invokespecial parseBaseAppChildTag : (Landroid/content/pm/parsing/result/ParseInput;Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;I)Landroid/content/pm/parsing/result/ParseResult;
    //   1358: astore #10
    //   1360: goto -> 1745
    //   1363: aload_2
    //   1364: aload_3
    //   1365: aload #4
    //   1367: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   1370: aload_1
    //   1371: invokestatic parseActivityAlias : (Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   1374: astore #10
    //   1376: iload #7
    //   1378: istore #12
    //   1380: aload #10
    //   1382: invokeinterface isSuccess : ()Z
    //   1387: ifeq -> 1435
    //   1390: aload #10
    //   1392: invokeinterface getResult : ()Ljava/lang/Object;
    //   1397: checkcast android/content/pm/parsing/component/ParsedActivity
    //   1400: astore #8
    //   1402: aload #8
    //   1404: invokevirtual getOrder : ()I
    //   1407: ifeq -> 1416
    //   1410: iconst_1
    //   1411: istore #12
    //   1413: goto -> 1419
    //   1416: iconst_0
    //   1417: istore #12
    //   1419: iload #7
    //   1421: iload #12
    //   1423: ior
    //   1424: istore #12
    //   1426: aload_2
    //   1427: aload #8
    //   1429: invokeinterface addActivity : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   1434: pop
    //   1435: iload #12
    //   1437: istore #7
    //   1439: goto -> 1745
    //   1442: aload_0
    //   1443: getfield mSeparateProcesses : [Ljava/lang/String;
    //   1446: astore #10
    //   1448: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   1451: istore #11
    //   1453: aload #10
    //   1455: aload_2
    //   1456: aload_3
    //   1457: aload #4
    //   1459: iload #5
    //   1461: iload #11
    //   1463: aload_1
    //   1464: invokestatic parseProvider : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   1467: astore #10
    //   1469: aload #10
    //   1471: invokeinterface isSuccess : ()Z
    //   1476: ifeq -> 1496
    //   1479: aload_2
    //   1480: aload #10
    //   1482: invokeinterface getResult : ()Ljava/lang/Object;
    //   1487: checkcast android/content/pm/parsing/component/ParsedProvider
    //   1490: invokeinterface addProvider : (Landroid/content/pm/parsing/component/ParsedProvider;)Landroid/content/pm/parsing/ParsingPackage;
    //   1495: pop
    //   1496: goto -> 1745
    //   1499: aload_0
    //   1500: getfield mSeparateProcesses : [Ljava/lang/String;
    //   1503: astore #10
    //   1505: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   1508: istore #11
    //   1510: aload #10
    //   1512: aload_2
    //   1513: aload_3
    //   1514: aload #4
    //   1516: iload #5
    //   1518: iload #11
    //   1520: aload_1
    //   1521: invokestatic parseService : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   1524: astore #10
    //   1526: iload #16
    //   1528: istore #12
    //   1530: aload #10
    //   1532: invokeinterface isSuccess : ()Z
    //   1537: ifeq -> 1585
    //   1540: aload #10
    //   1542: invokeinterface getResult : ()Ljava/lang/Object;
    //   1547: checkcast android/content/pm/parsing/component/ParsedService
    //   1550: astore #8
    //   1552: aload #8
    //   1554: invokevirtual getOrder : ()I
    //   1557: ifeq -> 1566
    //   1560: iconst_1
    //   1561: istore #12
    //   1563: goto -> 1569
    //   1566: iconst_0
    //   1567: istore #12
    //   1569: iload #16
    //   1571: iload #12
    //   1573: ior
    //   1574: istore #12
    //   1576: aload_2
    //   1577: aload #8
    //   1579: invokeinterface addService : (Landroid/content/pm/parsing/component/ParsedService;)Landroid/content/pm/parsing/ParsingPackage;
    //   1584: pop
    //   1585: iload #12
    //   1587: istore #16
    //   1589: goto -> 1745
    //   1592: goto -> 1598
    //   1595: iconst_1
    //   1596: istore #17
    //   1598: aload_0
    //   1599: getfield mSeparateProcesses : [Ljava/lang/String;
    //   1602: astore #10
    //   1604: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   1607: istore #11
    //   1609: aload #10
    //   1611: aload_2
    //   1612: aload_3
    //   1613: aload #4
    //   1615: iload #5
    //   1617: iload #11
    //   1619: aload_1
    //   1620: invokestatic parseActivityOrReceiver : ([Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;IZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   1623: astore #10
    //   1625: iload #7
    //   1627: istore #12
    //   1629: iload #15
    //   1631: istore #18
    //   1633: aload #10
    //   1635: invokeinterface isSuccess : ()Z
    //   1640: ifeq -> 1737
    //   1643: aload #10
    //   1645: invokeinterface getResult : ()Ljava/lang/Object;
    //   1650: checkcast android/content/pm/parsing/component/ParsedActivity
    //   1653: astore #8
    //   1655: iload #17
    //   1657: ifeq -> 1700
    //   1660: aload #8
    //   1662: invokevirtual getOrder : ()I
    //   1665: ifeq -> 1674
    //   1668: iconst_1
    //   1669: istore #12
    //   1671: goto -> 1677
    //   1674: iconst_0
    //   1675: istore #12
    //   1677: iload #7
    //   1679: iload #12
    //   1681: ior
    //   1682: istore #12
    //   1684: aload_2
    //   1685: aload #8
    //   1687: invokeinterface addActivity : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   1692: pop
    //   1693: iload #15
    //   1695: istore #18
    //   1697: goto -> 1737
    //   1700: aload #8
    //   1702: invokevirtual getOrder : ()I
    //   1705: ifeq -> 1714
    //   1708: iconst_1
    //   1709: istore #12
    //   1711: goto -> 1717
    //   1714: iconst_0
    //   1715: istore #12
    //   1717: iload #15
    //   1719: iload #12
    //   1721: ior
    //   1722: istore #18
    //   1724: aload_2
    //   1725: aload #8
    //   1727: invokeinterface addReceiver : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   1732: pop
    //   1733: iload #7
    //   1735: istore #12
    //   1737: iload #18
    //   1739: istore #15
    //   1741: iload #12
    //   1743: istore #7
    //   1745: aload #10
    //   1747: invokeinterface isError : ()Z
    //   1752: ifeq -> 1764
    //   1755: aload_1
    //   1756: aload #10
    //   1758: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   1763: areturn
    //   1764: goto -> 1108
    //   1767: aload_2
    //   1768: invokeinterface getStaticSharedLibName : ()Ljava/lang/String;
    //   1773: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   1776: ifeq -> 1818
    //   1779: aload_1
    //   1780: aload_2
    //   1781: invokestatic generateAppDetailsHiddenActivity : (Landroid/content/pm/parsing/result/ParseInput;Landroid/content/pm/parsing/ParsingPackage;)Landroid/content/pm/parsing/result/ParseResult;
    //   1784: astore_3
    //   1785: aload_3
    //   1786: invokeinterface isError : ()Z
    //   1791: ifeq -> 1802
    //   1794: aload_1
    //   1795: aload_3
    //   1796: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   1801: areturn
    //   1802: aload_2
    //   1803: aload_3
    //   1804: invokeinterface getResult : ()Ljava/lang/Object;
    //   1809: checkcast android/content/pm/parsing/component/ParsedActivity
    //   1812: invokeinterface addActivity : (Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/ParsingPackage;
    //   1817: pop
    //   1818: iload #7
    //   1820: ifeq -> 1830
    //   1823: aload_2
    //   1824: invokeinterface sortActivities : ()Landroid/content/pm/parsing/ParsingPackage;
    //   1829: pop
    //   1830: iload #15
    //   1832: ifeq -> 1842
    //   1835: aload_2
    //   1836: invokeinterface sortReceivers : ()Landroid/content/pm/parsing/ParsingPackage;
    //   1841: pop
    //   1842: iload #16
    //   1844: ifeq -> 1854
    //   1847: aload_2
    //   1848: invokeinterface sortServices : ()Landroid/content/pm/parsing/ParsingPackage;
    //   1853: pop
    //   1854: aload_2
    //   1855: invokestatic setMaxAspectRatio : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   1858: aload_0
    //   1859: aload_2
    //   1860: invokespecial setMinAspectRatio : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   1863: aload_0
    //   1864: aload_2
    //   1865: invokespecial setSupportsSizeChanges : (Landroid/content/pm/parsing/ParsingPackage;)V
    //   1868: aload_2
    //   1869: aload_2
    //   1870: invokestatic hasDomainURLs : (Landroid/content/pm/parsing/ParsingPackage;)Z
    //   1873: invokeinterface setHasDomainUrls : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   1878: pop
    //   1879: aload_1
    //   1880: aload_2
    //   1881: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   1886: areturn
    //   1887: astore_1
    //   1888: aload #8
    //   1890: invokevirtual recycle : ()V
    //   1893: aload_1
    //   1894: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1615	-> 0
    //   #1616	-> 8
    //   #1618	-> 16
    //   #1623	-> 27
    //   #1624	-> 32
    //   #1804	-> 42
    //   #1624	-> 47
    //   #1804	-> 49
    //   #1627	-> 53
    //   #1629	-> 62
    //   #1630	-> 67
    //   #1631	-> 75
    //   #1632	-> 84
    //   #1633	-> 95
    //   #1804	-> 105
    //   #1633	-> 110
    //   #1634	-> 112
    //   #1635	-> 117
    //   #1804	-> 151
    //   #1635	-> 156
    //   #1638	-> 158
    //   #1641	-> 167
    //   #1642	-> 175
    //   #1643	-> 180
    //   #1644	-> 192
    //   #1645	-> 200
    //   #1649	-> 212
    //   #1651	-> 219
    //   #1653	-> 230
    //   #1654	-> 235
    //   #1657	-> 244
    //   #1658	-> 249
    //   #1804	-> 283
    //   #1658	-> 288
    //   #1661	-> 290
    //   #1664	-> 299
    //   #1668	-> 312
    //   #1670	-> 324
    //   #1671	-> 329
    //   #1672	-> 338
    //   #1673	-> 343
    //   #1804	-> 377
    //   #1673	-> 382
    //   #1681	-> 384
    //   #1682	-> 394
    //   #1684	-> 411
    //   #1686	-> 428
    //   #1688	-> 445
    //   #1670	-> 464
    //   #1692	-> 464
    //   #1694	-> 473
    //   #1696	-> 473
    //   #1697	-> 478
    //   #1699	-> 485
    //   #1705	-> 493
    //   #1708	-> 510
    //   #1664	-> 522
    //   #1715	-> 522
    //   #1717	-> 537
    //   #1719	-> 546
    //   #1725	-> 586
    //   #1727	-> 633
    //   #1728	-> 647
    //   #1731	-> 668
    //   #1736	-> 693
    //   #1737	-> 700
    //   #1744	-> 715
    //   #1748	-> 724
    //   #1750	-> 736
    //   #1751	-> 750
    //   #1804	-> 759
    //   #1751	-> 764
    //   #1754	-> 766
    //   #1755	-> 783
    //   #1757	-> 792
    //   #1758	-> 797
    //   #1759	-> 806
    //   #1760	-> 811
    //   #1804	-> 845
    //   #1760	-> 850
    //   #1763	-> 852
    //   #1767	-> 861
    //   #1768	-> 868
    //   #1775	-> 883
    //   #1778	-> 892
    //   #1780	-> 909
    //   #1781	-> 923
    //   #1804	-> 932
    //   #1781	-> 937
    //   #1784	-> 939
    //   #1785	-> 951
    //   #1787	-> 960
    //   #1790	-> 973
    //   #1791	-> 988
    //   #1804	-> 998
    //   #1791	-> 1003
    //   #1796	-> 1005
    //   #1797	-> 1013
    //   #1798	-> 1018
    //   #1799	-> 1026
    //   #1804	-> 1060
    //   #1799	-> 1065
    //   #1802	-> 1067
    //   #1804	-> 1085
    //   #1805	-> 1090
    //   #1807	-> 1090
    //   #1808	-> 1093
    //   #1809	-> 1093
    //   #1810	-> 1093
    //   #1812	-> 1108
    //   #1814	-> 1129
    //   #1815	-> 1147
    //   #1816	-> 1153
    //   #1820	-> 1156
    //   #1821	-> 1165
    //   #1822	-> 1168
    //   #1878	-> 1345
    //   #1867	-> 1363
    //   #1869	-> 1376
    //   #1870	-> 1390
    //   #1871	-> 1402
    //   #1872	-> 1426
    //   #1875	-> 1435
    //   #1876	-> 1435
    //   #1857	-> 1442
    //   #1858	-> 1453
    //   #1860	-> 1469
    //   #1861	-> 1479
    //   #1864	-> 1496
    //   #1865	-> 1496
    //   #1845	-> 1499
    //   #1846	-> 1510
    //   #1848	-> 1526
    //   #1849	-> 1540
    //   #1850	-> 1552
    //   #1851	-> 1576
    //   #1854	-> 1585
    //   #1855	-> 1585
    //   #1822	-> 1592
    //   #1824	-> 1595
    //   #1827	-> 1598
    //   #1828	-> 1609
    //   #1831	-> 1625
    //   #1832	-> 1643
    //   #1833	-> 1655
    //   #1834	-> 1660
    //   #1835	-> 1684
    //   #1837	-> 1700
    //   #1838	-> 1724
    //   #1842	-> 1737
    //   #1843	-> 1737
    //   #1882	-> 1745
    //   #1883	-> 1755
    //   #1885	-> 1764
    //   #1812	-> 1767
    //   #1887	-> 1767
    //   #1890	-> 1779
    //   #1891	-> 1785
    //   #1896	-> 1794
    //   #1899	-> 1802
    //   #1902	-> 1818
    //   #1903	-> 1823
    //   #1905	-> 1830
    //   #1906	-> 1835
    //   #1908	-> 1842
    //   #1909	-> 1847
    //   #1914	-> 1854
    //   #1915	-> 1858
    //   #1916	-> 1863
    //   #1918	-> 1868
    //   #1920	-> 1879
    //   #1804	-> 1887
    //   #1805	-> 1893
    // Exception table:
    //   from	to	target	type
    //   32	42	49	finally
    //   53	62	1887	finally
    //   67	75	49	finally
    //   75	84	49	finally
    //   84	95	49	finally
    //   95	105	49	finally
    //   117	151	49	finally
    //   158	167	49	finally
    //   167	175	1887	finally
    //   180	192	49	finally
    //   192	200	49	finally
    //   200	212	49	finally
    //   212	219	1887	finally
    //   219	230	1887	finally
    //   235	244	49	finally
    //   249	283	49	finally
    //   290	299	49	finally
    //   299	307	1887	finally
    //   312	324	49	finally
    //   329	338	49	finally
    //   343	377	49	finally
    //   384	394	49	finally
    //   394	411	49	finally
    //   411	428	49	finally
    //   428	445	49	finally
    //   445	461	49	finally
    //   464	473	49	finally
    //   478	485	49	finally
    //   485	493	49	finally
    //   493	501	49	finally
    //   510	519	49	finally
    //   522	532	1887	finally
    //   537	546	49	finally
    //   551	565	49	finally
    //   577	586	49	finally
    //   586	594	1887	finally
    //   599	607	49	finally
    //   624	633	1887	finally
    //   633	642	1887	finally
    //   647	665	49	finally
    //   684	693	1887	finally
    //   700	712	49	finally
    //   715	724	1887	finally
    //   724	736	1887	finally
    //   736	745	1887	finally
    //   750	759	49	finally
    //   766	783	1887	finally
    //   783	792	1887	finally
    //   797	806	49	finally
    //   811	845	49	finally
    //   852	861	49	finally
    //   868	880	49	finally
    //   883	892	1887	finally
    //   892	909	1887	finally
    //   909	918	1887	finally
    //   923	932	49	finally
    //   939	951	1887	finally
    //   951	960	1887	finally
    //   960	968	1887	finally
    //   978	988	49	finally
    //   988	998	49	finally
    //   1005	1013	1887	finally
    //   1018	1026	49	finally
    //   1026	1060	49	finally
    //   1070	1085	1887	finally
  }
  
  private void parseBaseAppBasicFlags(ParsingPackage paramParsingPackage, TypedArray paramTypedArray) {
    boolean bool2;
    int i = paramParsingPackage.getTargetSdkVersion();
    boolean bool1 = true;
    paramParsingPackage = paramParsingPackage.setAllowBackup(bool(true, 17, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setAllowClearUserData(bool(true, 5, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setAllowClearUserDataOnFailedRestore(bool(true, 54, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setAllowNativeHeapPointerTagging(bool(true, 59, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setEnabled(bool(true, 9, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setExtractNativeLibs(bool(true, 34, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setHasCode(bool(true, 7, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setAllowTaskReparenting(bool(false, 14, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setCantSaveState(bool(false, 47, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setCrossProfile(bool(false, 58, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setDebuggable(bool(false, 10, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setDefaultToDeviceProtectedStorage(bool(false, 38, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setDirectBootAware(bool(false, 39, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setForceQueryable(bool(false, 57, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setGame(bool(false, 31, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setHasFragileUserData(bool(false, 50, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setLargeHeap(bool(false, 24, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setMultiArch(bool(false, 33, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setPreserveLegacyExternalStorage(bool(false, 61, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setRequiredForAllUsers(bool(false, 27, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setSupportsRtl(bool(false, 26, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setTestOnly(bool(false, 15, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setUseEmbeddedDex(bool(false, 53, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setUsesNonSdkApi(bool(false, 49, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setVmSafeMode(bool(false, 20, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setAutoRevokePermissions(anInt(60, paramTypedArray));
    if (i >= 29) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramParsingPackage = paramParsingPackage.setAllowAudioPlaybackCapture(bool(bool2, 55, paramTypedArray));
    if (i >= 14) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramParsingPackage = paramParsingPackage.setBaseHardwareAccelerated(bool(bool2, 23, paramTypedArray));
    if (i < 29) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    paramParsingPackage = paramParsingPackage.setRequestLegacyExternalStorage(bool(bool2, 56, paramTypedArray));
    if (i < 28) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    paramParsingPackage = paramParsingPackage.setUsesCleartextTraffic(bool(bool2, 36, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setUiOptions(anInt(25, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setCategory(anInt(-1, 43, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setMaxAspectRatio(aFloat(44, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setMinAspectRatio(aFloat(51, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setBanner(resId(30, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setDescriptionRes(resId(13, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setIconRes(resId(2, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setLogo(resId(22, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setNetworkSecurityConfigRes(resId(41, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setRoundIconRes(resId(42, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setTheme(resId(0, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setClassLoaderName(string(46, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setRequiredAccountType(string(29, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setRestrictedAccountType(string(28, paramTypedArray));
    paramParsingPackage = paramParsingPackage.setZygotePreloadName(string(52, paramTypedArray));
    paramParsingPackage.setPermission(nonConfigString(0, 6, paramTypedArray));
  }
  
  private ParseResult parseBaseAppChildTag(ParseInput paramParseInput, String paramString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt) throws IOException, XmlPullParserException {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1964930885:
        if (paramString.equals("uses-package")) {
          b = 6;
          break;
        } 
      case 178070147:
        if (paramString.equals("profileable")) {
          b = 7;
          break;
        } 
      case 166208699:
        if (paramString.equals("library")) {
          b = 2;
          break;
        } 
      case 8960125:
        if (paramString.equals("uses-static-library")) {
          b = 3;
          break;
        } 
      case -1056667556:
        if (paramString.equals("static-library")) {
          b = 1;
          break;
        } 
      case -1094759587:
        if (paramString.equals("processes")) {
          b = 5;
          break;
        } 
      case -1115949454:
        if (paramString.equals("meta-data")) {
          b = 0;
          break;
        } 
      case -1356765254:
        if (paramString.equals("uses-library")) {
          b = 4;
          break;
        } 
    } 
    switch (b) {
      default:
        return ParsingUtils.unknownTag("<application>", paramParsingPackage, paramXmlResourceParser, paramParseInput);
      case 7:
        return parseProfileable(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 6:
        return paramParseInput.success(null);
      case 5:
        return parseProcesses(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser, this.mSeparateProcesses, paramInt);
      case 4:
        return parseUsesLibrary(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 3:
        return parseUsesStaticLibrary(paramParseInput, paramParsingPackage, paramResources, paramXmlResourceParser);
      case 2:
        return parseLibrary(paramParsingPackage, paramResources, paramXmlResourceParser, paramParseInput);
      case 1:
        return parseStaticLibrary(paramParsingPackage, paramResources, paramXmlResourceParser, paramParseInput);
      case 0:
        break;
    } 
    Bundle bundle = paramParsingPackage.getMetaData();
    ParseResult<Bundle> parseResult = parseMetaData(paramParsingPackage, paramResources, paramXmlResourceParser, bundle, paramParseInput);
    if (parseResult.isSuccess())
      paramParsingPackage.setMetaData(parseResult.getResult()); 
    return parseResult;
  }
  
  private static ParseResult<ParsingPackage> parseStaticLibrary(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestStaticLibrary);
    try {
      ParseResult<?> parseResult;
      StringBuilder stringBuilder;
      String str = typedArray.getNonResourceString(0);
      int i = typedArray.getInt(1, -1);
      int j = typedArray.getInt(2, 0);
      if (str == null || i < 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Bad static-library declaration name: ");
        stringBuilder1.append(str);
        stringBuilder1.append(" version: ");
        stringBuilder1.append(i);
        parseResult = paramParseInput.error(stringBuilder1.toString());
        return (ParseResult)parseResult;
      } 
      if (parseResult.getSharedUserId() != null) {
        parseResult = paramParseInput.error(-107, "sharedUserId not allowed in static shared library");
        return (ParseResult)parseResult;
      } 
      if (parseResult.getStaticSharedLibName() != null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Multiple static-shared libs for package ");
        stringBuilder.append(parseResult.getPackageName());
        String str1 = stringBuilder.toString();
        parseResult = paramParseInput.error(str1);
        return (ParseResult)parseResult;
      } 
      ParsingPackage parsingPackage = parseResult.setStaticSharedLibName(stringBuilder.intern());
      long l = PackageInfo.composeLongVersionCode(j, i);
      parsingPackage = parsingPackage.setStaticSharedLibVersion(l);
      parsingPackage = parsingPackage.setStaticSharedLibrary(true);
      return paramParseInput.success(parsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseLibrary(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestLibrary);
    try {
      String str = typedArray.getNonResourceString(0);
      if (str != null) {
        str = str.intern();
        if (!ArrayUtils.contains(paramParsingPackage.getLibraryNames(), str))
          paramParsingPackage.addLibraryName(str); 
      } 
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseUsesStaticLibrary(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesStaticLibrary);
    try {
      StringBuilder stringBuilder;
      String str1 = typedArray.getNonResourceString(0);
      int i = typedArray.getInt(1, -1);
      String str2 = typedArray.getNonResourceString(2);
      if (str1 == null || i < 0 || str2 == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Bad uses-static-library declaration name: ");
        stringBuilder.append(str1);
        stringBuilder.append(" version: ");
        stringBuilder.append(i);
        stringBuilder.append(" certDigest");
        stringBuilder.append(str2);
        parseResult = paramParseInput.error(stringBuilder.toString());
        return (ParseResult)parseResult;
      } 
      List<String> list = stringBuilder.getUsesStaticLibraries();
      if (list.contains(str1)) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Depending on multiple versions of static library ");
        stringBuilder.append(str1);
        parseResult = parseResult.error(stringBuilder.toString());
        return (ParseResult)parseResult;
      } 
      String str3 = str1.intern();
      str2 = str2.replace(":", "").toLowerCase();
      String[] arrayOfString2 = EmptyArray.STRING;
      if (stringBuilder.getTargetSdkVersion() >= 27) {
        ParseResult<String[]> parseResult1 = parseAdditionalCertificates((ParseInput)parseResult, paramResources, paramXmlResourceParser);
        if (parseResult1.isError()) {
          parseResult = parseResult.error(parseResult1);
          return (ParseResult)parseResult;
        } 
        arrayOfString2 = parseResult1.getResult();
      } 
      String[] arrayOfString1 = new String[arrayOfString2.length + 1];
      arrayOfString1[0] = str2;
      System.arraycopy(arrayOfString2, 0, arrayOfString1, 1, arrayOfString2.length);
      ParsingPackage parsingPackage = stringBuilder.addUsesStaticLibrary(str3);
      long l = i;
      parsingPackage = parsingPackage.addUsesStaticLibraryVersion(l);
      parsingPackage = parsingPackage.addUsesStaticLibraryCertDigests(arrayOfString1);
      ParseResult<?> parseResult = parseResult.success(parsingPackage);
      return (ParseResult)parseResult;
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseUsesLibrary(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestUsesLibrary);
    try {
      String str = typedArray.getNonResourceString(0);
      boolean bool = typedArray.getBoolean(1, true);
      if (str != null) {
        str = str.intern();
        if (bool) {
          ParsingPackage parsingPackage = paramParsingPackage.addUsesLibrary(str);
          parsingPackage.removeUsesOptionalLibrary(str);
        } else if (!ArrayUtils.contains(paramParsingPackage.getUsesLibraries(), str)) {
          paramParsingPackage.addUsesOptionalLibrary(str);
        } 
      } 
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseProcesses(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String[] paramArrayOfString, int paramInt) throws IOException, XmlPullParserException {
    ParseResult<ArrayMap<String, ParsedProcess>> parseResult = ParsedProcessUtils.parseProcesses(paramArrayOfString, paramParsingPackage, paramResources, paramXmlResourceParser, paramInt, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.setProcesses((Map<String, ParsedProcess>)parseResult.getResult()));
  }
  
  private static ParseResult<ParsingPackage> parseProfileable(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProfileable);
    try {
      boolean bool = paramParsingPackage.isProfileableByShell();
      boolean bool1 = false;
      if (bool || 
        bool(false, 0, typedArray))
        bool1 = true; 
      return paramParseInput.success(paramParsingPackage.setProfileableByShell(bool1));
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<String[]> parseAdditionalCertificates(ParseInput paramParseInput, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: getstatic libcore/util/EmptyArray.STRING : [Ljava/lang/String;
    //   3: astore_3
    //   4: aload_2
    //   5: invokeinterface getDepth : ()I
    //   10: istore #4
    //   12: aload_2
    //   13: invokeinterface next : ()I
    //   18: istore #5
    //   20: iload #5
    //   22: iconst_1
    //   23: if_icmpeq -> 192
    //   26: iload #5
    //   28: iconst_3
    //   29: if_icmpne -> 43
    //   32: aload_2
    //   33: invokeinterface getDepth : ()I
    //   38: iload #4
    //   40: if_icmple -> 192
    //   43: iload #5
    //   45: iconst_2
    //   46: if_icmpeq -> 52
    //   49: goto -> 12
    //   52: aload_2
    //   53: invokeinterface getName : ()Ljava/lang/String;
    //   58: astore #6
    //   60: aload_3
    //   61: astore #7
    //   63: aload #6
    //   65: ldc_w 'additional-certificate'
    //   68: invokevirtual equals : (Ljava/lang/Object;)Z
    //   71: ifeq -> 186
    //   74: aload_1
    //   75: aload_2
    //   76: getstatic com/android/internal/R$styleable.AndroidManifestAdditionalCertificate : [I
    //   79: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   82: astore #6
    //   84: aload #6
    //   86: iconst_0
    //   87: invokevirtual getNonResourceString : (I)Ljava/lang/String;
    //   90: astore #7
    //   92: aload #7
    //   94: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   97: ifeq -> 141
    //   100: new java/lang/StringBuilder
    //   103: astore_1
    //   104: aload_1
    //   105: invokespecial <init> : ()V
    //   108: aload_1
    //   109: ldc_w 'Bad additional-certificate declaration with empty certDigest:'
    //   112: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   115: pop
    //   116: aload_1
    //   117: aload #7
    //   119: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   122: pop
    //   123: aload_0
    //   124: aload_1
    //   125: invokevirtual toString : ()Ljava/lang/String;
    //   128: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   133: astore_0
    //   134: aload #6
    //   136: invokevirtual recycle : ()V
    //   139: aload_0
    //   140: areturn
    //   141: aload #7
    //   143: ldc_w ':'
    //   146: ldc_w ''
    //   149: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   152: invokevirtual toLowerCase : ()Ljava/lang/String;
    //   155: astore #7
    //   157: ldc java/lang/String
    //   159: aload_3
    //   160: aload #7
    //   162: invokestatic appendElement : (Ljava/lang/Class;[Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    //   165: checkcast [Ljava/lang/String;
    //   168: astore #7
    //   170: aload #6
    //   172: invokevirtual recycle : ()V
    //   175: goto -> 186
    //   178: astore_0
    //   179: aload #6
    //   181: invokevirtual recycle : ()V
    //   184: aload_0
    //   185: athrow
    //   186: aload #7
    //   188: astore_3
    //   189: goto -> 12
    //   192: aload_0
    //   193: aload_3
    //   194: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   199: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2211	-> 0
    //   #2212	-> 4
    //   #2214	-> 12
    //   #2216	-> 32
    //   #2217	-> 43
    //   #2218	-> 49
    //   #2221	-> 52
    //   #2222	-> 60
    //   #2223	-> 74
    //   #2226	-> 84
    //   #2229	-> 92
    //   #2230	-> 100
    //   #2241	-> 134
    //   #2230	-> 139
    //   #2237	-> 141
    //   #2238	-> 157
    //   #2241	-> 170
    //   #2242	-> 175
    //   #2241	-> 178
    //   #2242	-> 184
    //   #2244	-> 186
    //   #2246	-> 192
    // Exception table:
    //   from	to	target	type
    //   84	92	178	finally
    //   92	100	178	finally
    //   100	134	178	finally
    //   141	157	178	finally
    //   157	170	178	finally
  }
  
  private static ParseResult<ParsedActivity> generateAppDetailsHiddenActivity(ParseInput paramParseInput, ParsingPackage paramParsingPackage) {
    String str1 = paramParsingPackage.getPackageName();
    ParseResult<String> parseResult = ComponentParseUtils.buildTaskAffinityName(str1, str1, ":app_details", paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    String str3 = parseResult.getResult();
    String str2 = paramParsingPackage.getProcessName();
    int i = paramParsingPackage.getUiOptions();
    boolean bool = paramParsingPackage.isBaseHardwareAccelerated();
    return paramParseInput.success(ParsedActivity.makeAppDetailsActivity(str1, str2, i, str3, bool));
  }
  
  private static boolean hasDomainURLs(ParsingPackage paramParsingPackage) {
    List<ParsedActivity> list = paramParsingPackage.getActivities();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ParsedActivity parsedActivity = list.get(b);
      List<ParsedIntentInfo> list1 = parsedActivity.getIntents();
      int j = list1.size();
      for (byte b1 = 0; b1 < j; b1++) {
        ParsedIntentInfo parsedIntentInfo = list1.get(b1);
        if (parsedIntentInfo.hasAction("android.intent.action.VIEW") && 
          parsedIntentInfo.hasAction("android.intent.action.VIEW") && (
          parsedIntentInfo.hasDataScheme("http") || 
          parsedIntentInfo.hasDataScheme("https")))
          return true; 
      } 
    } 
    return false;
  }
  
  private static void setMaxAspectRatio(ParsingPackage paramParsingPackage) {
    float f1;
    if (paramParsingPackage.getTargetSdkVersion() < 26) {
      f1 = 1.86F;
    } else {
      f1 = 0.0F;
    } 
    float f2 = paramParsingPackage.getMaxAspectRatio();
    if (f2 == 0.0F) {
      Bundle bundle = paramParsingPackage.getMetaData();
      f2 = f1;
      if (bundle != null) {
        f2 = f1;
        if (bundle.containsKey("android.max_aspect"))
          f2 = bundle.getFloat("android.max_aspect", f1); 
      } 
    } 
    List<ParsedActivity> list = paramParsingPackage.getActivities();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ParsedActivity parsedActivity = list.get(b);
      if (parsedActivity.getMaxAspectRatio() == null) {
        if (parsedActivity.getMetaData() != null) {
          f1 = parsedActivity.getMetaData().getFloat("android.max_aspect", f2);
        } else {
          f1 = f2;
        } 
        parsedActivity.setMaxAspectRatio(parsedActivity.getResizeMode(), f1);
      } 
    } 
  }
  
  private void setMinAspectRatio(ParsingPackage paramParsingPackage) {
    float f1 = paramParsingPackage.getMinAspectRatio();
    float f2 = 0.0F;
    if (f1 == 0.0F)
      if (paramParsingPackage.getTargetSdkVersion() >= 29) {
        f1 = f2;
      } else {
        Callback callback = this.mCallback;
        if (callback != null && callback.hasFeature("android.hardware.type.watch")) {
          f1 = 1.0F;
        } else {
          f1 = 1.333F;
        } 
      }  
    List<ParsedActivity> list = paramParsingPackage.getActivities();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ParsedActivity parsedActivity = list.get(b);
      if (parsedActivity.getMinAspectRatio() == null)
        parsedActivity.setMinAspectRatio(parsedActivity.getResizeMode(), f1); 
    } 
  }
  
  private void setSupportsSizeChanges(ParsingPackage paramParsingPackage) {
    boolean bool;
    Bundle bundle = paramParsingPackage.getMetaData();
    if (bundle != null && 
      bundle.getBoolean("android.supports_size_changes", false)) {
      bool = true;
    } else {
      bool = false;
    } 
    List<ParsedActivity> list = paramParsingPackage.getActivities();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ParsedActivity parsedActivity = list.get(b);
      if (bool || (parsedActivity.getMetaData() != null && 
        parsedActivity.getMetaData().getBoolean("android.supports_size_changes", false)))
        parsedActivity.setSupportsSizeChanges(true); 
    } 
  }
  
  private static ParseResult<ParsingPackage> parseOverlay(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestResourceOverlay);
    try {
      String str1, str2 = typedArray.getString(1);
      int i = anInt(0, 0, typedArray);
      if (str2 == null) {
        parseResult = paramParseInput.error("<overlay> does not specify a target package");
        return (ParseResult)parseResult;
      } 
      if (i < 0 || i > 9999) {
        parseResult = parseResult.error("<overlay> priority must be between 0 and 9999");
        return (ParseResult)parseResult;
      } 
      String str3 = typedArray.getString(5);
      String str4 = typedArray.getString(6);
      if (!PackageParser.checkRequiredSystemProperties(str3, str4)) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Skipping target and overlay pair ");
        stringBuilder.append(str2);
        stringBuilder.append(" and ");
        stringBuilder.append(paramParsingPackage.getBaseCodePath());
        stringBuilder.append(": overlay ignored due to required system property: ");
        stringBuilder.append(str3);
        stringBuilder.append(" with value: ");
        stringBuilder.append(str4);
        str1 = stringBuilder.toString();
        Slog.i("PackageParsing", str1);
        parseResult = parseResult.skip(str1);
        return (ParseResult)parseResult;
      } 
      ParsingPackage parsingPackage = str1.setOverlay(true);
      parsingPackage = parsingPackage.setOverlayTarget(str2);
      parsingPackage = parsingPackage.setOverlayPriority(i);
      str2 = typedArray.getString(3);
      parsingPackage = parsingPackage.setOverlayTargetName(str2);
      str2 = typedArray.getString(2);
      parsingPackage = parsingPackage.setOverlayCategory(str2);
      boolean bool = bool(false, 4, typedArray);
      parsingPackage = parsingPackage.setOverlayIsStatic(bool);
      ParseResult<?> parseResult = parseResult.success(parsingPackage);
      return (ParseResult)parseResult;
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseProtectedBroadcast(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProtectedBroadcast);
    try {
      String str = nonResString(0, typedArray);
      if (str != null)
        paramParsingPackage.addProtectedBroadcast(str); 
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseSupportScreens(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestSupportsScreens);
    try {
      int i = anInt(0, 6, typedArray);
      int j = anInt(0, 7, typedArray);
      int k = anInt(0, 8, typedArray);
      int m = anInt(1, 1, typedArray);
      paramParsingPackage = paramParsingPackage.setSupportsSmallScreens(m);
      m = anInt(1, 2, typedArray);
      paramParsingPackage = paramParsingPackage.setSupportsNormalScreens(m);
      m = anInt(1, 3, typedArray);
      paramParsingPackage = paramParsingPackage.setSupportsLargeScreens(m);
      m = anInt(1, 5, typedArray);
      paramParsingPackage = paramParsingPackage.setSupportsExtraLargeScreens(m);
      m = anInt(1, 4, typedArray);
      paramParsingPackage = paramParsingPackage.setResizeable(m);
      m = anInt(1, 0, typedArray);
      paramParsingPackage = paramParsingPackage.setAnyDensity(m);
      paramParsingPackage = paramParsingPackage.setRequiresSmallestWidthDp(i);
      paramParsingPackage = paramParsingPackage.setCompatibleWidthLimitDp(j);
      paramParsingPackage = paramParsingPackage.setLargestWidthLimitDp(k);
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseInstrumentation(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) throws XmlPullParserException, IOException {
    ParseResult<ParsedInstrumentation> parseResult = ParsedInstrumentationUtils.parseInstrumentation(paramParsingPackage, paramResources, paramXmlResourceParser, PackageParser.sUseRoundIcon, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    return paramParseInput.success(paramParsingPackage.addInstrumentation(parseResult.getResult()));
  }
  
  private static ParseResult<ParsingPackage> parseOriginalPackage(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
    try {
      String str = typedArray.getNonConfigurationString(0, 0);
      if (!paramParsingPackage.getPackageName().equals(str)) {
        if (paramParsingPackage.getOriginalPackages().isEmpty())
          paramParsingPackage.setRealPackage(paramParsingPackage.getPackageName()); 
        paramParsingPackage.addOriginalPackage(str);
      } 
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsingPackage> parseAdoptPermissions(ParseInput paramParseInput, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestOriginalPackage);
    try {
      String str = nonConfigString(0, 0, typedArray);
      if (str != null)
        paramParsingPackage.addAdoptPermission(str); 
      return paramParseInput.success(paramParsingPackage);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static void convertNewPermissions(ParsingPackage paramParsingPackage) {
    int i = PackageParser.NEW_PERMISSIONS.length;
    StringBuilder stringBuilder = null;
    for (byte b = 0; b < i; b++, stringBuilder = stringBuilder1) {
      PackageParser.NewPermissionInfo newPermissionInfo = PackageParser.NEW_PERMISSIONS[b];
      if (paramParsingPackage.getTargetSdkVersion() >= newPermissionInfo.sdkVersion)
        break; 
      StringBuilder stringBuilder1 = stringBuilder;
      if (!paramParsingPackage.getRequestedPermissions().contains(newPermissionInfo.name)) {
        if (stringBuilder == null) {
          stringBuilder = new StringBuilder(128);
          stringBuilder.append(paramParsingPackage.getPackageName());
          stringBuilder.append(": compat added ");
        } else {
          stringBuilder.append(' ');
        } 
        stringBuilder.append(newPermissionInfo.name);
        ParsingPackage parsingPackage = paramParsingPackage.addRequestedPermission(newPermissionInfo.name);
        String str = newPermissionInfo.name;
        parsingPackage.addImplicitPermission(str);
        stringBuilder1 = stringBuilder;
      } 
    } 
    if (stringBuilder != null)
      Slog.i("PackageParsing", stringBuilder.toString()); 
  }
  
  private static void convertSplitPermissions(ParsingPackage paramParsingPackage) {
    try {
      List<SplitPermissionInfoParcelable> list = ActivityThread.getPermissionManager().getSplitPermissions();
      int i = list.size();
      for (byte b = 0; b < i; b++) {
        SplitPermissionInfoParcelable splitPermissionInfoParcelable = list.get(b);
        List<String> list1 = paramParsingPackage.getRequestedPermissions();
        if (paramParsingPackage.getTargetSdkVersion() < splitPermissionInfoParcelable.getTargetSdk() && 
          list1.contains(splitPermissionInfoParcelable.getSplitPermission())) {
          List<String> list2 = splitPermissionInfoParcelable.getNewPermissions();
          for (byte b1 = 0; b1 < list2.size(); b1++) {
            String str = list2.get(b1);
            if (!list1.contains(str)) {
              ParsingPackage parsingPackage = paramParsingPackage.addRequestedPermission(str);
              parsingPackage.addImplicitPermission(str);
            } 
          } 
        } 
      } 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private static void adjustPackageToBeUnresizeableAndUnpipable(ParsingPackage paramParsingPackage) {
    List<ParsedActivity> list = paramParsingPackage.getActivities();
    int i = list.size();
    for (byte b = 0; b < i; b++) {
      ParsedActivity parsedActivity1 = list.get(b);
      ParsedActivity parsedActivity2 = parsedActivity1.setResizeMode(0);
      parsedActivity2.setFlags(parsedActivity1.getFlags() & 0xFFBFFFFF);
    } 
  }
  
  private static ParseResult validateName(ParseInput paramParseInput, String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_1
    //   1: invokevirtual length : ()I
    //   4: istore #4
    //   6: iconst_0
    //   7: istore #5
    //   9: iconst_1
    //   10: istore #6
    //   12: iconst_0
    //   13: istore #7
    //   15: iload #7
    //   17: iload #4
    //   19: if_icmpge -> 182
    //   22: aload_1
    //   23: iload #7
    //   25: invokevirtual charAt : (I)C
    //   28: istore #8
    //   30: iload #8
    //   32: bipush #97
    //   34: if_icmplt -> 44
    //   37: iload #8
    //   39: bipush #122
    //   41: if_icmple -> 58
    //   44: iload #8
    //   46: bipush #65
    //   48: if_icmplt -> 68
    //   51: iload #8
    //   53: bipush #90
    //   55: if_icmpgt -> 68
    //   58: iconst_0
    //   59: istore #9
    //   61: iload #5
    //   63: istore #10
    //   65: goto -> 126
    //   68: iload #6
    //   70: ifne -> 113
    //   73: iload #8
    //   75: bipush #48
    //   77: if_icmplt -> 95
    //   80: iload #5
    //   82: istore #10
    //   84: iload #6
    //   86: istore #9
    //   88: iload #8
    //   90: bipush #57
    //   92: if_icmple -> 126
    //   95: iload #8
    //   97: bipush #95
    //   99: if_icmpne -> 113
    //   102: iload #5
    //   104: istore #10
    //   106: iload #6
    //   108: istore #9
    //   110: goto -> 126
    //   113: iload #8
    //   115: bipush #46
    //   117: if_icmpne -> 140
    //   120: iconst_1
    //   121: istore #10
    //   123: iconst_1
    //   124: istore #9
    //   126: iinc #7, 1
    //   129: iload #10
    //   131: istore #5
    //   133: iload #9
    //   135: istore #6
    //   137: goto -> 15
    //   140: new java/lang/StringBuilder
    //   143: dup
    //   144: invokespecial <init> : ()V
    //   147: astore_1
    //   148: aload_1
    //   149: ldc_w 'bad character ''
    //   152: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_1
    //   157: iload #8
    //   159: invokevirtual append : (C)Ljava/lang/StringBuilder;
    //   162: pop
    //   163: aload_1
    //   164: ldc_w '''
    //   167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   170: pop
    //   171: aload_0
    //   172: aload_1
    //   173: invokevirtual toString : ()Ljava/lang/String;
    //   176: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   181: areturn
    //   182: iload_3
    //   183: ifeq -> 203
    //   186: aload_1
    //   187: invokestatic isValidExtFilename : (Ljava/lang/String;)Z
    //   190: ifne -> 203
    //   193: aload_0
    //   194: ldc_w 'Invalid filename'
    //   197: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   202: areturn
    //   203: iload #5
    //   205: ifne -> 228
    //   208: iload_2
    //   209: ifne -> 215
    //   212: goto -> 228
    //   215: aload_0
    //   216: ldc_w 'must have at least one '.' separator'
    //   219: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   224: astore_0
    //   225: goto -> 236
    //   228: aload_0
    //   229: aconst_null
    //   230: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   235: astore_0
    //   236: aload_0
    //   237: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2596	-> 0
    //   #2597	-> 6
    //   #2598	-> 9
    //   #2599	-> 12
    //   #2600	-> 22
    //   #2601	-> 30
    //   #2602	-> 58
    //   #2603	-> 61
    //   #2605	-> 68
    //   #2606	-> 73
    //   #2607	-> 102
    //   #2610	-> 113
    //   #2611	-> 120
    //   #2612	-> 123
    //   #2613	-> 126
    //   #2599	-> 126
    //   #2615	-> 140
    //   #2617	-> 182
    //   #2618	-> 193
    //   #2620	-> 203
    //   #2622	-> 215
    //   #2621	-> 228
    //   #2620	-> 236
  }
  
  public static ParseResult<Bundle> parseMetaData(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, Bundle paramBundle, ParseInput paramParseInput) {
    // Byte code:
    //   0: aload_1
    //   1: aload_2
    //   2: getstatic com/android/internal/R$styleable.AndroidManifestMetaData : [I
    //   5: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   8: astore #5
    //   10: aload_3
    //   11: astore_1
    //   12: aload_3
    //   13: ifnonnull -> 24
    //   16: new android/os/Bundle
    //   19: astore_1
    //   20: aload_1
    //   21: invokespecial <init> : ()V
    //   24: iconst_0
    //   25: istore #6
    //   27: iconst_0
    //   28: iconst_0
    //   29: aload #5
    //   31: invokestatic nonConfigString : (IILandroid/content/res/TypedArray;)Ljava/lang/String;
    //   34: astore_3
    //   35: aload_3
    //   36: invokestatic safeIntern : (Ljava/lang/String;)Ljava/lang/String;
    //   39: astore_3
    //   40: aload_3
    //   41: ifnonnull -> 62
    //   44: aload #4
    //   46: ldc_w '<meta-data> requires an android:name attribute'
    //   49: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   54: astore_0
    //   55: aload #5
    //   57: invokevirtual recycle : ()V
    //   60: aload_0
    //   61: areturn
    //   62: aload #5
    //   64: iconst_2
    //   65: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   68: astore #7
    //   70: aload #7
    //   72: ifnull -> 96
    //   75: aload #7
    //   77: getfield resourceId : I
    //   80: ifeq -> 96
    //   83: aload_1
    //   84: aload_3
    //   85: aload #7
    //   87: getfield resourceId : I
    //   90: invokevirtual putInt : (Ljava/lang/String;I)V
    //   93: goto -> 312
    //   96: aload #5
    //   98: iconst_1
    //   99: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   102: astore #7
    //   104: aload #7
    //   106: ifnull -> 328
    //   109: aload #7
    //   111: getfield type : I
    //   114: iconst_3
    //   115: if_icmpne -> 149
    //   118: aload #7
    //   120: invokevirtual coerceToString : ()Ljava/lang/CharSequence;
    //   123: astore_0
    //   124: aload_0
    //   125: ifnull -> 138
    //   128: aload_0
    //   129: invokeinterface toString : ()Ljava/lang/String;
    //   134: astore_0
    //   135: goto -> 140
    //   138: aconst_null
    //   139: astore_0
    //   140: aload_1
    //   141: aload_3
    //   142: aload_0
    //   143: invokevirtual putString : (Ljava/lang/String;Ljava/lang/String;)V
    //   146: goto -> 312
    //   149: aload #7
    //   151: getfield type : I
    //   154: bipush #18
    //   156: if_icmpne -> 180
    //   159: aload #7
    //   161: getfield data : I
    //   164: ifeq -> 170
    //   167: iconst_1
    //   168: istore #6
    //   170: aload_1
    //   171: aload_3
    //   172: iload #6
    //   174: invokevirtual putBoolean : (Ljava/lang/String;Z)V
    //   177: goto -> 312
    //   180: aload #7
    //   182: getfield type : I
    //   185: bipush #16
    //   187: if_icmplt -> 213
    //   190: aload #7
    //   192: getfield type : I
    //   195: bipush #31
    //   197: if_icmpgt -> 213
    //   200: aload_1
    //   201: aload_3
    //   202: aload #7
    //   204: getfield data : I
    //   207: invokevirtual putInt : (Ljava/lang/String;I)V
    //   210: goto -> 312
    //   213: aload #7
    //   215: getfield type : I
    //   218: iconst_4
    //   219: if_icmpne -> 235
    //   222: aload_1
    //   223: aload_3
    //   224: aload #7
    //   226: invokevirtual getFloat : ()F
    //   229: invokevirtual putFloat : (Ljava/lang/String;F)V
    //   232: goto -> 312
    //   235: new java/lang/StringBuilder
    //   238: astore_3
    //   239: aload_3
    //   240: invokespecial <init> : ()V
    //   243: aload_3
    //   244: ldc_w '<meta-data> only supports string, integer, float, color, boolean, and resource reference types: '
    //   247: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   250: pop
    //   251: aload_3
    //   252: aload_2
    //   253: invokeinterface getName : ()Ljava/lang/String;
    //   258: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   261: pop
    //   262: aload_3
    //   263: ldc_w ' at '
    //   266: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   269: pop
    //   270: aload_3
    //   271: aload_0
    //   272: invokeinterface getBaseCodePath : ()Ljava/lang/String;
    //   277: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   280: pop
    //   281: aload_3
    //   282: ldc_w ' '
    //   285: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload_3
    //   290: aload_2
    //   291: invokeinterface getPositionDescription : ()Ljava/lang/String;
    //   296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_3
    //   301: invokevirtual toString : ()Ljava/lang/String;
    //   304: astore_0
    //   305: ldc 'PackageParsing'
    //   307: aload_0
    //   308: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   311: pop
    //   312: aload #4
    //   314: aload_1
    //   315: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   320: astore_0
    //   321: aload #5
    //   323: invokevirtual recycle : ()V
    //   326: aload_0
    //   327: areturn
    //   328: aload #4
    //   330: ldc_w '<meta-data> requires an android:value or android:resource attribute'
    //   333: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   338: astore_0
    //   339: aload #5
    //   341: invokevirtual recycle : ()V
    //   344: aload_0
    //   345: areturn
    //   346: astore_0
    //   347: aload #5
    //   349: invokevirtual recycle : ()V
    //   352: aload_0
    //   353: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2627	-> 0
    //   #2629	-> 10
    //   #2630	-> 16
    //   #2633	-> 24
    //   #2634	-> 24
    //   #2633	-> 35
    //   #2635	-> 40
    //   #2636	-> 44
    //   #2677	-> 55
    //   #2636	-> 60
    //   #2639	-> 62
    //   #2640	-> 70
    //   #2642	-> 83
    //   #2644	-> 96
    //   #2646	-> 104
    //   #2647	-> 109
    //   #2648	-> 118
    //   #2649	-> 124
    //   #2650	-> 146
    //   #2651	-> 159
    //   #2652	-> 180
    //   #2654	-> 200
    //   #2655	-> 213
    //   #2656	-> 222
    //   #2659	-> 235
    //   #2662	-> 251
    //   #2663	-> 270
    //   #2664	-> 289
    //   #2659	-> 305
    //   #2675	-> 312
    //   #2677	-> 321
    //   #2675	-> 326
    //   #2671	-> 328
    //   #2677	-> 339
    //   #2671	-> 344
    //   #2677	-> 346
    //   #2678	-> 352
    // Exception table:
    //   from	to	target	type
    //   16	24	346	finally
    //   27	35	346	finally
    //   35	40	346	finally
    //   44	55	346	finally
    //   62	70	346	finally
    //   75	83	346	finally
    //   83	93	346	finally
    //   96	104	346	finally
    //   109	118	346	finally
    //   118	124	346	finally
    //   128	135	346	finally
    //   140	146	346	finally
    //   149	159	346	finally
    //   159	167	346	finally
    //   170	177	346	finally
    //   180	200	346	finally
    //   200	210	346	finally
    //   213	222	346	finally
    //   222	232	346	finally
    //   235	251	346	finally
    //   251	270	346	finally
    //   270	289	346	finally
    //   289	305	346	finally
    //   305	312	346	finally
    //   312	321	346	finally
    //   328	339	346	finally
  }
  
  public static PackageParser.SigningDetails getSigningDetails(ParsingPackageRead paramParsingPackageRead, boolean paramBoolean) throws PackageParser.PackageParserException {
    PackageParser.SigningDetails signingDetails = PackageParser.SigningDetails.UNKNOWN;
    ParseInput parseInput = ParseTypeImpl.forDefaultParsing().reset();
    Trace.traceBegin(262144L, "collectCertificates");
    try {
      PackageParser.SigningDetails signingDetails1;
      String str = paramParsingPackageRead.getBaseCodePath();
      boolean bool = paramParsingPackageRead.isStaticSharedLibrary();
      int i = paramParsingPackageRead.getTargetSdkVersion();
      ParseResult<PackageParser.SigningDetails> parseResult = getSigningDetails(parseInput, str, paramBoolean, bool, signingDetails, i);
      if (!parseResult.isError()) {
        signingDetails1 = parseResult.getResult();
        String[] arrayOfString = paramParsingPackageRead.getSplitCodePaths();
        bool = ArrayUtils.isEmpty((Object[])arrayOfString);
        PackageParser.SigningDetails signingDetails2 = signingDetails1;
        if (!bool) {
          i = 0;
          try {
            while (i < arrayOfString.length) {
              String str1 = arrayOfString[i];
              bool = paramParsingPackageRead.isStaticSharedLibrary();
              int j = paramParsingPackageRead.getTargetSdkVersion();
              ParseResult<PackageParser.SigningDetails> parseResult1 = getSigningDetails(parseInput, str1, paramBoolean, bool, signingDetails1, j);
              if (!parseResult1.isError()) {
                signingDetails1 = parseResult1.getResult();
                i++;
              } 
              PackageParser.PackageParserException packageParserException1 = new PackageParser.PackageParserException();
              i = signingDetails1.getErrorCode();
              this(i, signingDetails1.getErrorMessage(), signingDetails1.getException());
              throw packageParserException1;
            } 
            signingDetails2 = signingDetails1;
          } finally {}
        } 
        return signingDetails2;
      } 
      PackageParser.PackageParserException packageParserException = new PackageParser.PackageParserException();
      i = signingDetails1.getErrorCode();
      this(i, signingDetails1.getErrorMessage(), signingDetails1.getException());
      throw packageParserException;
    } finally {
      Trace.traceEnd(262144L);
    } 
  }
  
  public static ParseResult<PackageParser.SigningDetails> getSigningDetails(ParseInput paramParseInput, String paramString, boolean paramBoolean1, boolean paramBoolean2, PackageParser.SigningDetails paramSigningDetails, int paramInt) {
    StringBuilder stringBuilder;
    paramInt = ApkSignatureVerifier.getMinimumSignatureSchemeVersionForTargetSdk(paramInt);
    if (paramBoolean2)
      paramInt = 2; 
    if (paramBoolean1)
      try {
        PackageParser.SigningDetails signingDetails1 = ApkSignatureVerifier.unsafeGetCertsWithoutVerification(paramString, 1);
        if (paramSigningDetails == PackageParser.SigningDetails.UNKNOWN)
          return paramParseInput.success(signingDetails1); 
        if (!Signature.areExactMatch(paramSigningDetails.signatures, signingDetails1.signatures)) {
          stringBuilder = new StringBuilder();
          stringBuilder.append(paramString);
          stringBuilder.append(" has mismatched certificates");
          return paramParseInput.error(-104, stringBuilder.toString());
        } 
        return (ParseResult)paramParseInput.success(stringBuilder);
      } catch (android.content.pm.PackageParser.PackageParserException packageParserException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Failed collecting certificates for ");
        stringBuilder1.append(paramString);
        return paramParseInput.error(-103, stringBuilder1.toString(), packageParserException);
      }  
    PackageParser.SigningDetails signingDetails = ApkSignatureVerifier.verify(paramString, paramInt);
    if (packageParserException == PackageParser.SigningDetails.UNKNOWN)
      return paramParseInput.success(signingDetails); 
    if (!Signature.areExactMatch(((PackageParser.SigningDetails)packageParserException).signatures, signingDetails.signatures)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString);
      stringBuilder.append(" has mismatched certificates");
      return paramParseInput.error(-104, stringBuilder.toString());
    } 
    return (ParseResult)paramParseInput.success(stringBuilder);
  }
  
  private static boolean bool(boolean paramBoolean, int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getBoolean(paramInt, paramBoolean);
  }
  
  private static float aFloat(float paramFloat, int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getFloat(paramInt, paramFloat);
  }
  
  private static float aFloat(int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getFloat(paramInt, 0.0F);
  }
  
  private static int anInt(int paramInt1, int paramInt2, TypedArray paramTypedArray) {
    return paramTypedArray.getInt(paramInt2, paramInt1);
  }
  
  private static int anInteger(int paramInt1, int paramInt2, TypedArray paramTypedArray) {
    return paramTypedArray.getInteger(paramInt2, paramInt1);
  }
  
  private static int anInt(int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getInt(paramInt, 0);
  }
  
  private static int resId(int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getResourceId(paramInt, 0);
  }
  
  private static String string(int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getString(paramInt);
  }
  
  private static String nonConfigString(int paramInt1, int paramInt2, TypedArray paramTypedArray) {
    return paramTypedArray.getNonConfigurationString(paramInt2, paramInt1);
  }
  
  private static String nonResString(int paramInt, TypedArray paramTypedArray) {
    return paramTypedArray.getNonResourceString(paramInt);
  }
  
  public static void writeKeySetMapping(Parcel paramParcel, Map<String, ArraySet<PublicKey>> paramMap) {
    if (paramMap == null) {
      paramParcel.writeInt(-1);
      return;
    } 
    int i = paramMap.size();
    paramParcel.writeInt(i);
    for (String str : paramMap.keySet()) {
      paramParcel.writeString(str);
      ArraySet arraySet = paramMap.get(str);
      if (arraySet == null) {
        paramParcel.writeInt(-1);
        continue;
      } 
      int j = arraySet.size();
      paramParcel.writeInt(j);
      for (i = 0; i < j; i++)
        paramParcel.writeSerializable((Serializable)arraySet.valueAt(i)); 
    } 
  }
  
  public static ArrayMap<String, ArraySet<PublicKey>> readKeySetMapping(Parcel paramParcel) {
    int i = paramParcel.readInt();
    if (i == -1)
      return null; 
    ArrayMap<String, ArraySet<PublicKey>> arrayMap = new ArrayMap();
    for (byte b = 0; b < i; b++) {
      String str = paramParcel.readString();
      int j = paramParcel.readInt();
      if (j == -1) {
        arrayMap.put(str, null);
      } else {
        ArraySet arraySet = new ArraySet(j);
        for (byte b1 = 0; b1 < j; b1++) {
          PublicKey publicKey = (PublicKey)paramParcel.readSerializable();
          arraySet.add(publicKey);
        } 
        arrayMap.put(str, arraySet);
      } 
    } 
    return arrayMap;
  }
  
  public static interface Callback {
    boolean hasFeature(String param1String);
    
    ParsingPackage startParsingPackage(String param1String1, String param1String2, String param1String3, TypedArray param1TypedArray, boolean param1Boolean);
  }
}
