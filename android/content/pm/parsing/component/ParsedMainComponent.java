package android.content.pm.parsing.component;

import android.content.pm.parsing.ParsingPackageImpl;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ParsedMainComponent extends OplusBaseParsedMainComponent {
  String splitName;
  
  private String processName;
  
  int order;
  
  boolean exported;
  
  boolean enabled = true;
  
  boolean directBootAware;
  
  public ParsedMainComponent(ParsedMainComponent paramParsedMainComponent) {
    super(paramParsedMainComponent);
    this.processName = paramParsedMainComponent.processName;
    this.directBootAware = paramParsedMainComponent.directBootAware;
    this.enabled = paramParsedMainComponent.enabled;
    this.exported = paramParsedMainComponent.exported;
    this.order = paramParsedMainComponent.order;
    this.splitName = paramParsedMainComponent.splitName;
  }
  
  public ParsedMainComponent setProcessName(String paramString) {
    this.processName = TextUtils.safeIntern(paramString);
    return this;
  }
  
  public ParsedMainComponent setEnabled(boolean paramBoolean) {
    this.enabled = paramBoolean;
    return this;
  }
  
  public String getClassName() {
    return getName();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    ParsingPackageImpl.sForInternedString.parcel(this.processName, paramParcel, paramInt);
    paramParcel.writeBoolean(this.directBootAware);
    paramParcel.writeBoolean(this.enabled);
    paramParcel.writeBoolean(this.exported);
    paramParcel.writeInt(this.order);
    paramParcel.writeString(this.splitName);
  }
  
  protected ParsedMainComponent(Parcel paramParcel) {
    super(paramParcel);
    this.processName = ParsingPackageImpl.sForInternedString.unparcel(paramParcel);
    this.directBootAware = paramParcel.readBoolean();
    this.enabled = paramParcel.readBoolean();
    this.exported = paramParcel.readBoolean();
    this.order = paramParcel.readInt();
    this.splitName = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<ParsedMainComponent> CREATOR = (Parcelable.Creator<ParsedMainComponent>)new Object();
  
  public String getProcessName() {
    return this.processName;
  }
  
  public boolean isDirectBootAware() {
    return this.directBootAware;
  }
  
  public boolean isEnabled() {
    return this.enabled;
  }
  
  public boolean isExported() {
    return this.exported;
  }
  
  public int getOrder() {
    return this.order;
  }
  
  public String getSplitName() {
    return this.splitName;
  }
  
  public ParsedMainComponent setDirectBootAware(boolean paramBoolean) {
    this.directBootAware = paramBoolean;
    return this;
  }
  
  public ParsedMainComponent setExported(boolean paramBoolean) {
    this.exported = paramBoolean;
    return this;
  }
  
  public ParsedMainComponent setSplitName(String paramString) {
    this.splitName = paramString;
    return this;
  }
  
  public ParsedMainComponent() {}
}
