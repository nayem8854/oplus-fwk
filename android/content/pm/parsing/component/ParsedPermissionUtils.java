package android.content.pm.parsing.component;

import android.content.pm.PermissionInfo;
import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.Slog;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedPermissionUtils {
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<ParsedPermission> parsePermission(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    String str1 = paramParsingPackage.getPackageName();
    ParsedPermission parsedPermission = new ParsedPermission();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<");
    stringBuilder.append(paramXmlResourceParser.getName());
    stringBuilder.append(">");
    String str2 = stringBuilder.toString();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermission);
    try {
      ParseResult<ParsedPermission> parseResult = ParsedComponentUtils.parseComponent(parsedPermission, str2, paramParsingPackage, typedArray, paramBoolean, paramParseInput, 8, Integer.valueOf(5), 1, 0, 6, 2, 9);
      paramBoolean = parseResult.isError();
      if (paramBoolean) {
        typedArray.recycle();
        return parseResult;
      } 
      try {
        StringBuilder stringBuilder1;
        IllegalStateException illegalStateException;
        paramBoolean = typedArray.hasValue(10);
        if (paramBoolean)
          try {
            if ("android".equals(str1)) {
              parsedPermission.backgroundPermission = typedArray.getNonResourceString(10);
            } else {
              StringBuilder stringBuilder2 = new StringBuilder();
              this();
              stringBuilder2.append(str1);
              stringBuilder2.append(" defines a background permission. Only the 'android' package can do that.");
              Slog.w("PackageParsing", stringBuilder2.toString());
            } 
          } finally {} 
        parsedPermission.setGroup(typedArray.getNonResourceString(4));
        parsedPermission.requestRes = typedArray.getResourceId(11, 0);
        parsedPermission.protectionLevel = typedArray.getInt(3, 0);
        parsedPermission.flags = typedArray.getInt(7, 0);
        paramBoolean = parsedPermission.isRuntime();
        if (!paramBoolean || !"android".equals(parsedPermission.getPackageName())) {
          parsedPermission.flags &= 0xFFFFFFFB;
          parsedPermission.flags &= 0xFFFFFFF7;
        } else if ((0x4 & parsedPermission.flags) != 0 && (parsedPermission.flags & 0x8) != 0) {
          illegalStateException = new IllegalStateException();
          stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Permission cannot be both soft and hard restricted: ");
          stringBuilder1.append(parsedPermission.getName());
          this(stringBuilder1.toString());
          throw illegalStateException;
        } 
        typedArray.recycle();
        if (parsedPermission.protectionLevel == -1)
          return paramParseInput.error("<permission> does not specify protectionLevel"); 
        parsedPermission.protectionLevel = PermissionInfo.fixProtectionLevel(parsedPermission.protectionLevel);
        if (parsedPermission.getProtectionFlags() != 0 && (
          parsedPermission.protectionLevel & 0x1000) == 0 && (parsedPermission.protectionLevel & 0x2000) == 0 && (parsedPermission.protectionLevel & 0xF) != 2)
          return paramParseInput.error("<permission>  protectionLevel specifies a non-instant flag but is not based on signature type"); 
        return ComponentParseUtils.parseAllMetaData((ParsingPackage)stringBuilder1, (Resources)illegalStateException, paramXmlResourceParser, str2, parsedPermission, paramParseInput);
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramParsingPackage;
  }
  
  public static ParseResult<ParsedPermission> parsePermissionTree(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    ParsedPermission parsedPermission = new ParsedPermission();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<");
    stringBuilder.append(paramXmlResourceParser.getName());
    stringBuilder.append(">");
    String str = stringBuilder.toString();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermissionTree);
    try {
      String str1;
      ParseResult<ParsedPermission> parseResult = ParsedComponentUtils.parseComponent(parsedPermission, str, paramParsingPackage, typedArray, paramBoolean, paramParseInput, 4, null, 1, 0, 3, 2, 5);
      paramBoolean = parseResult.isError();
      if (paramBoolean)
        return parseResult; 
      typedArray.recycle();
      int i = parsedPermission.getName().indexOf('.');
      if (i > 0)
        i = parsedPermission.getName().indexOf('.', i + 1); 
      if (i < 0) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("<permission-tree> name has less than three segments: ");
        stringBuilder1.append(parsedPermission.getName());
        return paramParseInput.error(str1);
      } 
      parsedPermission.protectionLevel = 0;
      return ComponentParseUtils.parseAllMetaData((ParsingPackage)str1, paramResources, paramXmlResourceParser, str, parsedPermission, paramParseInput);
    } finally {
      typedArray.recycle();
    } 
  }
  
  public static ParseResult<ParsedPermissionGroup> parsePermissionGroup(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    ParsedPermissionGroup parsedPermissionGroup = new ParsedPermissionGroup();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<");
    stringBuilder.append(paramXmlResourceParser.getName());
    stringBuilder.append(">");
    String str = stringBuilder.toString();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPermissionGroup);
    try {
      ParseResult<ParsedPermissionGroup> parseResult = ParsedComponentUtils.parseComponent(parsedPermissionGroup, str, paramParsingPackage, typedArray, paramBoolean, paramParseInput, 7, Integer.valueOf(4), 1, 0, 5, 2, 8);
      paramBoolean = parseResult.isError();
      if (paramBoolean) {
        typedArray.recycle();
        return parseResult;
      } 
      try {
        parsedPermissionGroup.requestDetailResourceId = typedArray.getResourceId(12, 0);
        parsedPermissionGroup.backgroundRequestResourceId = typedArray.getResourceId(9, 0);
        parsedPermissionGroup.backgroundRequestDetailResourceId = typedArray.getResourceId(10, 0);
        parsedPermissionGroup.requestRes = typedArray.getResourceId(11, 0);
        parsedPermissionGroup.flags = typedArray.getInt(6, 0);
        parsedPermissionGroup.priority = typedArray.getInt(3, 0);
        typedArray.recycle();
        return ComponentParseUtils.parseAllMetaData(paramParsingPackage, paramResources, paramXmlResourceParser, str, parsedPermissionGroup, paramParseInput);
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramParsingPackage;
  }
}
