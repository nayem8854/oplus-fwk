package android.content.pm.parsing.component;

import android.content.pm.PathPermission;
import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.os.PatternMatcher;
import android.util.Slog;
import com.android.internal.R;
import java.io.IOException;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedProviderUtils {
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<ParsedProvider> parseProvider(String[] paramArrayOfString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, boolean paramBoolean, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    int i = paramParsingPackage.getTargetSdkVersion();
    String str1 = paramParsingPackage.getPackageName();
    ParsedProvider parsedProvider = new ParsedProvider();
    String str2 = paramXmlResourceParser.getName();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestProvider);
    try {
      ParseResult<ParsedProvider> parseResult = ParsedMainComponentUtils.parseMainComponent(parsedProvider, str2, paramArrayOfString, paramParsingPackage, typedArray, paramInt, paramBoolean, paramParseInput, 17, 14, Integer.valueOf(18), Integer.valueOf(6), 1, 0, 15, 2, Integer.valueOf(8), 19, Integer.valueOf(21));
      paramBoolean = parseResult.isError();
      if (paramBoolean) {
        typedArray.recycle();
        return parseResult;
      } 
      try {
        String str = typedArray.getNonConfigurationString(10, 0);
        if (i < 17) {
          paramBoolean = true;
        } else {
          paramBoolean = false;
        } 
        try {
          paramBoolean = typedArray.getBoolean(7, paramBoolean);
          try {
            parsedProvider.exported = paramBoolean;
            parsedProvider.syncable = typedArray.getBoolean(11, false);
            String str3 = typedArray.getNonConfigurationString(3, 0);
            String str4 = typedArray.getNonConfigurationString(4, 0);
            String str5 = str4;
            if (str4 == null)
              str5 = str3; 
            if (str5 == null) {
              try {
                parsedProvider.setReadPermission(paramParsingPackage.getPermission());
              } finally {}
            } else {
              parsedProvider.setReadPermission(str5);
            } 
            str4 = typedArray.getNonConfigurationString(5, 0);
            str5 = str4;
            if (str4 == null)
              str5 = str3; 
            if (str5 == null) {
              parsedProvider.setWritePermission(paramParsingPackage.getPermission());
            } else {
              parsedProvider.setWritePermission(str5);
            } 
            parsedProvider.grantUriPermissions = typedArray.getBoolean(13, false);
            parsedProvider.forceUriPermissions = typedArray.getBoolean(22, false);
            parsedProvider.multiProcess = typedArray.getBoolean(9, false);
            parsedProvider.initOrder = typedArray.getInt(12, 0);
            parsedProvider.flags |= ComponentParseUtils.flag(1073741824, 16, typedArray);
            paramBoolean = typedArray.getBoolean(20, false);
            if (paramBoolean) {
              parsedProvider.flags |= 0x100000;
              try {
                paramParsingPackage.setVisibleToInstantApps(true);
              } finally {}
            } 
            typedArray.recycle();
            if (paramParsingPackage.isCantSaveState())
              if (Objects.equals(parsedProvider.getProcessName(), str1))
                return paramParseInput.error("Heavy-weight applications can not have providers in main process");  
            if (str == null)
              return paramParseInput.error("<provider> does not include authorities attribute"); 
            if (str.length() <= 0)
              return paramParseInput.error("<provider> has empty authorities attribute"); 
            parsedProvider.setAuthority(str);
            return parseProviderTags(paramParsingPackage, str2, paramResources, paramXmlResourceParser, paramBoolean, parsedProvider, paramParseInput);
          } finally {}
        } finally {}
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramArrayOfString;
  }
  
  private static ParseResult<ParsedProvider> parseProviderTags(ParsingPackage paramParsingPackage, String paramString, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParsedProvider paramParsedProvider, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_3
    //   1: invokeinterface getDepth : ()I
    //   6: istore #7
    //   8: aload_3
    //   9: invokeinterface next : ()I
    //   14: istore #8
    //   16: iload #8
    //   18: iconst_1
    //   19: if_icmpeq -> 348
    //   22: iload #8
    //   24: iconst_3
    //   25: if_icmpne -> 45
    //   28: aload_3
    //   29: invokeinterface getDepth : ()I
    //   34: iload #7
    //   36: if_icmple -> 42
    //   39: goto -> 45
    //   42: goto -> 348
    //   45: iload #8
    //   47: iconst_2
    //   48: if_icmpeq -> 54
    //   51: goto -> 8
    //   54: aload_3
    //   55: invokeinterface getName : ()Ljava/lang/String;
    //   60: astore #9
    //   62: iconst_m1
    //   63: istore #8
    //   65: aload #9
    //   67: invokevirtual hashCode : ()I
    //   70: lookupswitch default -> 112, -1814617695 -> 165, -1115949454 -> 148, -1029793847 -> 131, 636171383 -> 115
    //   112: goto -> 179
    //   115: aload #9
    //   117: ldc 'path-permission'
    //   119: invokevirtual equals : (Ljava/lang/Object;)Z
    //   122: ifeq -> 112
    //   125: iconst_3
    //   126: istore #8
    //   128: goto -> 179
    //   131: aload #9
    //   133: ldc_w 'intent-filter'
    //   136: invokevirtual equals : (Ljava/lang/Object;)Z
    //   139: ifeq -> 112
    //   142: iconst_0
    //   143: istore #8
    //   145: goto -> 179
    //   148: aload #9
    //   150: ldc_w 'meta-data'
    //   153: invokevirtual equals : (Ljava/lang/Object;)Z
    //   156: ifeq -> 112
    //   159: iconst_1
    //   160: istore #8
    //   162: goto -> 179
    //   165: aload #9
    //   167: ldc_w 'grant-uri-permission'
    //   170: invokevirtual equals : (Ljava/lang/Object;)Z
    //   173: ifeq -> 112
    //   176: iconst_2
    //   177: istore #8
    //   179: iload #8
    //   181: ifeq -> 260
    //   184: iload #8
    //   186: iconst_1
    //   187: if_icmpeq -> 245
    //   190: iload #8
    //   192: iconst_2
    //   193: if_icmpeq -> 230
    //   196: iload #8
    //   198: iconst_3
    //   199: if_icmpeq -> 215
    //   202: aload_1
    //   203: aload_0
    //   204: aload_3
    //   205: aload #6
    //   207: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   210: astore #9
    //   212: goto -> 325
    //   215: aload #5
    //   217: aload_0
    //   218: aload_2
    //   219: aload_3
    //   220: aload #6
    //   222: invokestatic parsePathPermission : (Landroid/content/pm/parsing/component/ParsedProvider;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   225: astore #9
    //   227: goto -> 325
    //   230: aload #5
    //   232: aload_0
    //   233: aload_2
    //   234: aload_3
    //   235: aload #6
    //   237: invokestatic parseGrantUriPermission : (Landroid/content/pm/parsing/component/ParsedProvider;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   240: astore #9
    //   242: goto -> 325
    //   245: aload #5
    //   247: aload_0
    //   248: aload_2
    //   249: aload_3
    //   250: aload #6
    //   252: invokestatic addMetaData : (Landroid/content/pm/parsing/component/ParsedComponent;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   255: astore #9
    //   257: goto -> 325
    //   260: aload #5
    //   262: aload_0
    //   263: aload_2
    //   264: aload_3
    //   265: iload #4
    //   267: iconst_1
    //   268: iconst_0
    //   269: iconst_0
    //   270: iconst_0
    //   271: aload #6
    //   273: invokestatic parseIntentFilter : (Landroid/content/pm/parsing/component/ParsedMainComponent;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZZZZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   276: astore #9
    //   278: aload #9
    //   280: invokeinterface isSuccess : ()Z
    //   285: ifeq -> 325
    //   288: aload #9
    //   290: invokeinterface getResult : ()Ljava/lang/Object;
    //   295: checkcast android/content/pm/parsing/component/ParsedIntentInfo
    //   298: astore #10
    //   300: aload #5
    //   302: aload #10
    //   304: invokevirtual getOrder : ()I
    //   307: aload #5
    //   309: getfield order : I
    //   312: invokestatic max : (II)I
    //   315: putfield order : I
    //   318: aload #5
    //   320: aload #10
    //   322: invokevirtual addIntent : (Landroid/content/pm/parsing/component/ParsedIntentInfo;)V
    //   325: aload #9
    //   327: invokeinterface isError : ()Z
    //   332: ifeq -> 345
    //   335: aload #6
    //   337: aload #9
    //   339: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   344: areturn
    //   345: goto -> 8
    //   348: aload #6
    //   350: aload #5
    //   352: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   357: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #156	-> 0
    //   #158	-> 8
    //   #160	-> 28
    //   #161	-> 45
    //   #162	-> 51
    //   #165	-> 54
    //   #167	-> 62
    //   #193	-> 202
    //   #189	-> 215
    //   #190	-> 227
    //   #185	-> 230
    //   #186	-> 242
    //   #182	-> 245
    //   #183	-> 257
    //   #169	-> 260
    //   #170	-> 260
    //   #174	-> 278
    //   #175	-> 278
    //   #176	-> 288
    //   #177	-> 300
    //   #178	-> 318
    //   #197	-> 325
    //   #198	-> 335
    //   #200	-> 345
    //   #158	-> 348
    //   #202	-> 348
  }
  
  private static ParseResult<ParsedProvider> parseGrantUriPermission(ParsedProvider paramParsedProvider, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestGrantUriPermission);
    try {
      PatternMatcher arrayOfPatternMatcher[], patternMatcher;
      String str1 = paramXmlResourceParser.getName();
      paramResources = null;
      String str2 = typedArray.getNonConfigurationString(2, 0);
      if (str2 != null) {
        patternMatcher = new PatternMatcher();
        this(str2, 2);
      } else {
        str2 = typedArray.getNonConfigurationString(1, 0);
        if (str2 != null) {
          patternMatcher = new PatternMatcher(str2, 1);
        } else {
          str2 = typedArray.getNonConfigurationString(0, 0);
          if (str2 != null)
            patternMatcher = new PatternMatcher(str2, 0); 
        } 
      } 
      if (patternMatcher != null) {
        if (paramParsedProvider.uriPermissionPatterns == null) {
          paramParsedProvider.uriPermissionPatterns = new PatternMatcher[1];
          paramParsedProvider.uriPermissionPatterns[0] = patternMatcher;
        } else {
          int i = paramParsedProvider.uriPermissionPatterns.length;
          arrayOfPatternMatcher = new PatternMatcher[i + 1];
          System.arraycopy(paramParsedProvider.uriPermissionPatterns, 0, arrayOfPatternMatcher, 0, i);
          arrayOfPatternMatcher[i] = patternMatcher;
          paramParsedProvider.uriPermissionPatterns = arrayOfPatternMatcher;
        } 
        paramParsedProvider.grantUriPermissions = true;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unknown element under <path-permission>: ");
        stringBuilder.append(str1);
        stringBuilder.append(" at ");
        stringBuilder.append(arrayOfPatternMatcher.getBaseCodePath());
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str = stringBuilder.toString();
        Slog.w("PackageParsing", str);
      } 
      return paramParseInput.success(paramParsedProvider);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsedProvider> parsePathPermission(ParsedProvider paramParsedProvider, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestPathPermission);
    try {
      PathPermission arrayOfPathPermission[], pathPermission;
      String str2 = paramXmlResourceParser.getName();
      String str3 = typedArray.getNonConfigurationString(0, 0);
      String str4 = typedArray.getNonConfigurationString(1, 0);
      String str1 = str4;
      if (str4 == null)
        str1 = str3; 
      String str5 = typedArray.getNonConfigurationString(2, 0);
      str4 = str5;
      if (str5 == null)
        str4 = str3; 
      int i = 0;
      str3 = str1;
      if (str1 != null) {
        str3 = str1.intern();
        i = 1;
      } 
      str5 = str4;
      if (str4 != null) {
        str5 = str4.intern();
        i = 1;
      } 
      if (!i) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("No readPermission or writePermission for <path-permission>: ");
        stringBuilder.append(str2);
        stringBuilder.append(" at ");
        stringBuilder.append(paramParsingPackage.getBaseCodePath());
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str = stringBuilder.toString();
        Slog.w("PackageParsing", str);
        parseResult = paramParseInput.success(paramParsedProvider);
        return parseResult;
      } 
      str1 = null;
      str4 = typedArray.getNonConfigurationString(6, 0);
      if (str4 != null) {
        pathPermission = new PathPermission();
        this(str4, 3, str3, str5);
      } else {
        str4 = typedArray.getNonConfigurationString(5, 0);
        if (str4 != null) {
          pathPermission = new PathPermission(str4, 2, str3, str5);
        } else {
          str4 = typedArray.getNonConfigurationString(4, 0);
          if (str4 != null) {
            pathPermission = new PathPermission(str4, 1, str3, str5);
          } else {
            str4 = typedArray.getNonConfigurationString(3, 0);
            if (str4 != null)
              pathPermission = new PathPermission(str4, 0, str3, str5); 
          } 
        } 
      } 
      if (pathPermission != null) {
        if (((ParsedProvider)parseResult).pathPermissions == null) {
          ((ParsedProvider)parseResult).pathPermissions = new PathPermission[1];
          ((ParsedProvider)parseResult).pathPermissions[0] = pathPermission;
        } else {
          i = ((ParsedProvider)parseResult).pathPermissions.length;
          arrayOfPathPermission = new PathPermission[i + 1];
          System.arraycopy(((ParsedProvider)parseResult).pathPermissions, 0, arrayOfPathPermission, 0, i);
          arrayOfPathPermission[i] = pathPermission;
          ((ParsedProvider)parseResult).pathPermissions = arrayOfPathPermission;
        } 
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("No path, pathPrefix, or pathPattern for <path-permission>: ");
        stringBuilder.append(str2);
        stringBuilder.append(" at ");
        stringBuilder.append(arrayOfPathPermission.getBaseCodePath());
        stringBuilder.append(" ");
        stringBuilder.append(paramXmlResourceParser.getPositionDescription());
        String str = stringBuilder.toString();
        Slog.w("PackageParsing", str);
      } 
      ParseResult<ParsedProvider> parseResult = (ParseResult)paramParseInput.success(parseResult);
      return parseResult;
    } finally {
      typedArray.recycle();
    } 
  }
}
