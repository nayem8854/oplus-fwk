package android.content.pm.parsing.component;

import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.ParsingUtils;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.ArrayMap;
import com.android.internal.R;
import com.android.internal.util.CollectionUtils;
import java.io.IOException;
import java.util.Set;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedProcessUtils {
  private static final String TAG = "PackageParsing";
  
  private static ParseResult<Set<String>> parseDenyPermission(Set<String> paramSet, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestDenyPermission);
    try {
      String str = typedArray.getNonConfigurationString(0, 0);
      Set<String> set = paramSet;
      if (str != null) {
        set = paramSet;
        if (str.equals("android.permission.INTERNET"))
          set = CollectionUtils.add(paramSet, str); 
      } 
      typedArray.recycle();
      return paramParseInput.success(set);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<Set<String>> parseAllowPermission(Set<String> paramSet, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestAllowPermission);
    try {
      String str = typedArray.getNonConfigurationString(0, 0);
      Set<String> set = paramSet;
      if (str != null) {
        set = paramSet;
        if (str.equals("android.permission.INTERNET"))
          set = CollectionUtils.remove(paramSet, str); 
      } 
      typedArray.recycle();
      return paramParseInput.success(set);
    } finally {
      typedArray.recycle();
    } 
  }
  
  private static ParseResult<ParsedProcess> parseProcess(Set<String> paramSet, String[] paramArrayOfString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    // Byte code:
    //   0: new android/content/pm/parsing/component/ParsedProcess
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore #7
    //   9: aload_3
    //   10: aload #4
    //   12: getstatic com/android/internal/R$styleable.AndroidManifestProcess : [I
    //   15: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   18: astore #8
    //   20: aload_0
    //   21: ifnull -> 42
    //   24: new android/util/ArraySet
    //   27: astore #9
    //   29: aload #9
    //   31: aload_0
    //   32: invokespecial <init> : (Ljava/util/Collection;)V
    //   35: aload #7
    //   37: aload #9
    //   39: putfield deniedPermissions : Ljava/util/Set;
    //   42: aload #7
    //   44: aload #8
    //   46: iconst_0
    //   47: iconst_0
    //   48: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   51: putfield name : Ljava/lang/String;
    //   54: aload_2
    //   55: invokeinterface getPackageName : ()Ljava/lang/String;
    //   60: astore #9
    //   62: aload_2
    //   63: invokeinterface getPackageName : ()Ljava/lang/String;
    //   68: astore_0
    //   69: aload #7
    //   71: getfield name : Ljava/lang/String;
    //   74: astore #10
    //   76: aload #9
    //   78: aload_0
    //   79: aload #10
    //   81: iload #5
    //   83: aload_1
    //   84: aload #6
    //   86: invokestatic buildProcessName : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/CharSequence;I[Ljava/lang/String;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   89: astore_0
    //   90: aload_0
    //   91: invokeinterface isError : ()Z
    //   96: ifeq -> 115
    //   99: aload #6
    //   101: aload_0
    //   102: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   107: astore_0
    //   108: aload #8
    //   110: invokevirtual recycle : ()V
    //   113: aload_0
    //   114: areturn
    //   115: aload #7
    //   117: aload_0
    //   118: invokeinterface getResult : ()Ljava/lang/Object;
    //   123: checkcast java/lang/String
    //   126: putfield name : Ljava/lang/String;
    //   129: aload #7
    //   131: getfield name : Ljava/lang/String;
    //   134: ifnull -> 451
    //   137: aload #7
    //   139: getfield name : Ljava/lang/String;
    //   142: invokevirtual length : ()I
    //   145: ifgt -> 151
    //   148: goto -> 451
    //   151: aload #7
    //   153: aload #8
    //   155: iconst_1
    //   156: iconst_m1
    //   157: invokevirtual getInt : (II)I
    //   160: putfield gwpAsanMode : I
    //   163: aload #8
    //   165: invokevirtual recycle : ()V
    //   168: aload #4
    //   170: invokeinterface getDepth : ()I
    //   175: istore #11
    //   177: aload #4
    //   179: invokeinterface next : ()I
    //   184: istore #5
    //   186: iload #5
    //   188: iconst_1
    //   189: if_icmpeq -> 441
    //   192: iload #5
    //   194: iconst_3
    //   195: if_icmpne -> 216
    //   198: aload #4
    //   200: invokeinterface getDepth : ()I
    //   205: iload #11
    //   207: if_icmple -> 213
    //   210: goto -> 216
    //   213: goto -> 441
    //   216: iload #5
    //   218: iconst_3
    //   219: if_icmpeq -> 438
    //   222: iload #5
    //   224: iconst_4
    //   225: if_icmpne -> 231
    //   228: goto -> 177
    //   231: aload #4
    //   233: invokeinterface getName : ()Ljava/lang/String;
    //   238: astore_0
    //   239: aload_0
    //   240: invokevirtual hashCode : ()I
    //   243: istore #5
    //   245: iload #5
    //   247: ldc -1239165229
    //   249: if_icmpeq -> 277
    //   252: iload #5
    //   254: ldc 1658008624
    //   256: if_icmpeq -> 262
    //   259: goto -> 292
    //   262: aload_0
    //   263: ldc 'deny-permission'
    //   265: invokevirtual equals : (Ljava/lang/Object;)Z
    //   268: ifeq -> 259
    //   271: iconst_0
    //   272: istore #5
    //   274: goto -> 295
    //   277: aload_0
    //   278: ldc 'allow-permission'
    //   280: invokevirtual equals : (Ljava/lang/Object;)Z
    //   283: ifeq -> 259
    //   286: iconst_1
    //   287: istore #5
    //   289: goto -> 295
    //   292: iconst_m1
    //   293: istore #5
    //   295: iload #5
    //   297: ifeq -> 370
    //   300: iload #5
    //   302: iconst_1
    //   303: if_icmpeq -> 320
    //   306: ldc '<process>'
    //   308: aload_2
    //   309: aload #4
    //   311: aload #6
    //   313: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   316: astore_0
    //   317: goto -> 417
    //   320: aload #7
    //   322: getfield deniedPermissions : Ljava/util/Set;
    //   325: aload_3
    //   326: aload #4
    //   328: aload #6
    //   330: invokestatic parseAllowPermission : (Ljava/util/Set;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   333: astore #8
    //   335: aload #8
    //   337: astore_1
    //   338: aload_1
    //   339: astore_0
    //   340: aload #8
    //   342: invokeinterface isSuccess : ()Z
    //   347: ifeq -> 417
    //   350: aload #7
    //   352: aload #8
    //   354: invokeinterface getResult : ()Ljava/lang/Object;
    //   359: checkcast java/util/Set
    //   362: putfield deniedPermissions : Ljava/util/Set;
    //   365: aload_1
    //   366: astore_0
    //   367: goto -> 417
    //   370: aload #7
    //   372: getfield deniedPermissions : Ljava/util/Set;
    //   375: aload_3
    //   376: aload #4
    //   378: aload #6
    //   380: invokestatic parseDenyPermission : (Ljava/util/Set;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   383: astore #8
    //   385: aload #8
    //   387: astore_1
    //   388: aload_1
    //   389: astore_0
    //   390: aload #8
    //   392: invokeinterface isSuccess : ()Z
    //   397: ifeq -> 417
    //   400: aload #7
    //   402: aload #8
    //   404: invokeinterface getResult : ()Ljava/lang/Object;
    //   409: checkcast java/util/Set
    //   412: putfield deniedPermissions : Ljava/util/Set;
    //   415: aload_1
    //   416: astore_0
    //   417: aload_0
    //   418: invokeinterface isError : ()Z
    //   423: ifeq -> 435
    //   426: aload #6
    //   428: aload_0
    //   429: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   434: areturn
    //   435: goto -> 177
    //   438: goto -> 177
    //   441: aload #6
    //   443: aload #7
    //   445: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   450: areturn
    //   451: aload #6
    //   453: ldc '<process> does not specify android:process'
    //   455: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   460: astore_0
    //   461: aload #8
    //   463: invokevirtual recycle : ()V
    //   466: aload_0
    //   467: areturn
    //   468: astore_0
    //   469: goto -> 473
    //   472: astore_0
    //   473: aload #8
    //   475: invokevirtual recycle : ()V
    //   478: aload_0
    //   479: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #85	-> 0
    //   #86	-> 9
    //   #88	-> 20
    //   #89	-> 24
    //   #92	-> 42
    //   #94	-> 54
    //   #95	-> 54
    //   #94	-> 76
    //   #97	-> 90
    //   #98	-> 99
    //   #109	-> 108
    //   #98	-> 113
    //   #101	-> 115
    //   #103	-> 129
    //   #107	-> 151
    //   #109	-> 163
    //   #110	-> 168
    //   #113	-> 168
    //   #114	-> 177
    //   #115	-> 198
    //   #116	-> 216
    //   #117	-> 228
    //   #122	-> 231
    //   #123	-> 239
    //   #141	-> 306
    //   #133	-> 320
    //   #135	-> 335
    //   #136	-> 338
    //   #137	-> 350
    //   #125	-> 370
    //   #127	-> 385
    //   #128	-> 388
    //   #129	-> 400
    //   #145	-> 417
    //   #146	-> 426
    //   #148	-> 435
    //   #116	-> 438
    //   #114	-> 441
    //   #150	-> 441
    //   #103	-> 451
    //   #104	-> 451
    //   #109	-> 461
    //   #104	-> 466
    //   #109	-> 468
    //   #110	-> 478
    // Exception table:
    //   from	to	target	type
    //   24	42	472	finally
    //   42	54	472	finally
    //   54	76	472	finally
    //   76	90	472	finally
    //   90	99	472	finally
    //   99	108	472	finally
    //   115	129	472	finally
    //   129	148	472	finally
    //   151	163	472	finally
    //   451	461	468	finally
  }
  
  public static ParseResult<ArrayMap<String, ParsedProcess>> parseProcesses(String[] paramArrayOfString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    ArrayMap<String, ParsedProcess> arrayMap = new ArrayMap();
    int i = paramXmlResourceParser.getDepth();
    Set<String> set = null;
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        ParseResult<Set<String>> parseResult;
        if (j == 3 || j == 4)
          continue; 
        String str = paramXmlResourceParser.getName();
        j = -1;
        int k = str.hashCode();
        if (k != -1239165229) {
          if (k != -309518737) {
            if (k == 1658008624 && str.equals("deny-permission"))
              j = 0; 
          } else if (str.equals("process")) {
            j = 2;
          } 
        } else if (str.equals("allow-permission")) {
          j = 1;
        } 
        if (j != 0) {
          if (j != 1) {
            if (j != 2) {
              parseResult = ParsingUtils.unknownTag("<processes>", paramParsingPackage, paramXmlResourceParser, paramParseInput);
            } else {
              ParseResult<ParsedProcess> parseResult2 = parseProcess(set, paramArrayOfString, paramParsingPackage, paramResources, paramXmlResourceParser, paramInt, paramParseInput);
              ParseResult<ParsedProcess> parseResult1 = parseResult2;
              if (parseResult2.isSuccess()) {
                ParsedProcess parsedProcess = parseResult2.getResult();
                if (arrayMap.put(parsedProcess.name, parsedProcess) != null) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("<process> specified existing name '");
                  stringBuilder.append(parsedProcess.name);
                  stringBuilder.append("'");
                  parseResult = paramParseInput.error(stringBuilder.toString());
                } 
              } 
            } 
          } else {
            parseResult = parseAllowPermission(set, paramResources, paramXmlResourceParser, paramParseInput);
            if (parseResult.isSuccess())
              set = parseResult.getResult(); 
          } 
        } else {
          parseResult = parseDenyPermission(set, paramResources, paramXmlResourceParser, paramParseInput);
          if (parseResult.isSuccess())
            set = parseResult.getResult(); 
        } 
        if (parseResult.isError())
          return paramParseInput.error(parseResult); 
        continue;
      } 
      break;
    } 
    return paramParseInput.success(arrayMap);
  }
}
