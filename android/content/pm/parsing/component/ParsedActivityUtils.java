package android.content.pm.parsing.component;

import android.app.ActivityTaskManager;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageParser;
import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.util.AttributeSet;
import com.android.internal.R;
import java.io.IOException;
import java.util.Objects;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedActivityUtils {
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<ParsedActivity> parseActivityOrReceiver(String[] paramArrayOfString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, boolean paramBoolean, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    String str1 = paramParsingPackage.getPackageName();
    ParsedActivity parsedActivity = new ParsedActivity();
    boolean bool = "receiver".equals(paramXmlResourceParser.getName());
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<");
    stringBuilder.append(paramXmlResourceParser.getName());
    stringBuilder.append(">");
    String str2 = stringBuilder.toString();
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestActivity);
    try {
      ParseResult<ParsedActivity> parseResult = ParsedMainComponentUtils.parseMainComponent(parsedActivity, str2, paramArrayOfString, paramParsingPackage, typedArray, paramInt, paramBoolean, paramParseInput, 30, 17, Integer.valueOf(42), Integer.valueOf(5), 2, 1, 23, 3, Integer.valueOf(7), 44, Integer.valueOf(48));
      paramBoolean = parseResult.isError();
      if (paramBoolean) {
        typedArray.recycle();
        return parseResult;
      } 
      if (bool)
        try {
          if (paramParsingPackage.isCantSaveState()) {
            String str = parsedActivity.getProcessName();
            try {
              if (Objects.equals(str, str1)) {
                try {
                  ParseResult<?> parseResult1 = paramParseInput.error("Heavy-weight applications can not have receivers in main process");
                  typedArray.recycle();
                  return (ParseResult)parseResult1;
                } finally {}
                typedArray.recycle();
                throw str;
              } 
            } finally {}
          } 
        } finally {} 
      try {
        paramInt = typedArray.getResourceId(0, 0);
        try {
          String str;
          parsedActivity.theme = paramInt;
          parsedActivity.uiOptions = typedArray.getInt(26, paramParsingPackage.getUiOptions());
          int i = parsedActivity.flags, j = ComponentParseUtils.flag(64, 19, paramParsingPackage.isAllowTaskReparenting(), typedArray);
          int k = ComponentParseUtils.flag(8, 18, typedArray);
          int m = ComponentParseUtils.flag(4, 11, typedArray);
          int n = ComponentParseUtils.flag(32, 13, typedArray);
          int i1 = ComponentParseUtils.flag(256, 22, typedArray);
          int i2 = ComponentParseUtils.flag(2, 10, typedArray);
          int i3 = ComponentParseUtils.flag(2048, 24, typedArray);
          int i4 = ComponentParseUtils.flag(1, 9, typedArray);
          int i5 = ComponentParseUtils.flag(128, 21, typedArray);
          int i6 = ComponentParseUtils.flag(1024, 39, typedArray);
          int i7 = ComponentParseUtils.flag(1024, 29, typedArray);
          paramInt = ComponentParseUtils.flag(16, 12, typedArray);
          parsedActivity.flags = i | j | k | m | n | i1 | i2 | i3 | i4 | i5 | i6 | i7 | paramInt | ComponentParseUtils.flag(536870912, 58, typedArray);
          if (!bool) {
            try {
              paramInt = parsedActivity.flags;
              i = ComponentParseUtils.flag(512, 25, paramParsingPackage.isBaseHardwareAccelerated(), typedArray);
              i7 = ComponentParseUtils.flag(-2147483648, 31, typedArray);
              j = ComponentParseUtils.flag(262144, 57, typedArray);
              m = ComponentParseUtils.flag(8192, 35, typedArray);
              i5 = ComponentParseUtils.flag(4096, 36, typedArray);
              i6 = ComponentParseUtils.flag(16384, 37, typedArray);
              n = ComponentParseUtils.flag(8388608, 51, typedArray);
              k = ComponentParseUtils.flag(4194304, 41, typedArray);
              i4 = ComponentParseUtils.flag(16777216, 52, typedArray);
              parsedActivity.flags = paramInt | i | i7 | j | m | i5 | i6 | n | k | i4 | ComponentParseUtils.flag(33554432, 56, typedArray);
              parsedActivity.privateFlags |= ComponentParseUtils.flag(1, 54, typedArray);
              parsedActivity.colorMode = typedArray.getInt(49, 0);
              parsedActivity.documentLaunchMode = typedArray.getInt(33, 0);
              parsedActivity.launchMode = typedArray.getInt(14, 0);
              parsedActivity.lockTaskLaunchMode = typedArray.getInt(38, 0);
              parsedActivity.maxRecents = typedArray.getInt(34, ActivityTaskManager.getDefaultAppRecentsLimitStatic());
              parsedActivity.persistableMode = typedArray.getInteger(32, 0);
              parsedActivity.requestedVrComponent = typedArray.getString(43);
              parsedActivity.rotationAnimation = typedArray.getInt(46, -1);
              parsedActivity.softInputMode = typedArray.getInt(20, 0);
              m = typedArray.getInt(16, 0);
              paramInt = typedArray.getInt(47, 0);
              parsedActivity.configChanges = PackageParser.getActivityConfigChanges(m, paramInt);
              m = typedArray.getInt(15, -1);
              try {
                paramInt = getActivityResizeMode(paramParsingPackage, typedArray, m);
                parsedActivity.screenOrientation = m;
                parsedActivity.resizeMode = paramInt;
                if (typedArray.hasValue(50) && typedArray.getType(50) == 4) {
                  float f = typedArray.getFloat(50, 0.0F);
                  parsedActivity.setMaxAspectRatio(paramInt, f);
                } 
                if (typedArray.hasValue(53) && typedArray.getType(53) == 4) {
                  float f = typedArray.getFloat(53, 0.0F);
                  parsedActivity.setMinAspectRatio(paramInt, f);
                } 
                str = typedArray.getNonConfigurationString(8, 1024);
              } finally {}
            } finally {}
          } else {
            parsedActivity.launchMode = 0;
            parsedActivity.configChanges = 0;
            parsedActivity.flags |= ComponentParseUtils.flag(1073741824, 28, typedArray);
            str = typedArray.getNonConfigurationString(8, 1024);
          } 
          parsedActivity.taskAffinity = str.getResult();
          paramBoolean = typedArray.getBoolean(45, false);
          if (paramBoolean) {
            parsedActivity.flags |= 0x100000;
            paramParsingPackage.setVisibleToInstantApps(true);
          } 
          try {
            ParseResult<ParsedActivity> parseResult1 = parseActivityOrAlias(parsedActivity, paramParsingPackage, str2, paramXmlResourceParser, paramResources, typedArray, bool, false, paramBoolean, paramParseInput, 27, 4, 6);
            typedArray.recycle();
            return parseResult1;
          } finally {}
        } finally {}
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramArrayOfString;
  }
  
  public static ParseResult<ParsedActivity> parseActivityAlias(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: aload_2
    //   2: getstatic com/android/internal/R$styleable.AndroidManifestActivityAlias : [I
    //   5: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   8: astore #5
    //   10: aload #5
    //   12: bipush #7
    //   14: sipush #1024
    //   17: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   20: astore #6
    //   22: aload #6
    //   24: ifnonnull -> 48
    //   27: aload #4
    //   29: ldc '<activity-alias> does not specify android:targetActivity'
    //   31: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   36: astore_0
    //   37: aload #5
    //   39: invokevirtual recycle : ()V
    //   42: aload_0
    //   43: areturn
    //   44: astore_0
    //   45: goto -> 416
    //   48: aload_0
    //   49: invokeinterface getPackageName : ()Ljava/lang/String;
    //   54: astore #7
    //   56: aload #7
    //   58: aload #6
    //   60: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   63: astore #6
    //   65: aload #6
    //   67: ifnonnull -> 111
    //   70: new java/lang/StringBuilder
    //   73: astore_0
    //   74: aload_0
    //   75: invokespecial <init> : ()V
    //   78: aload_0
    //   79: ldc 'Empty class name in package '
    //   81: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload_0
    //   86: aload #7
    //   88: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   91: pop
    //   92: aload #4
    //   94: aload_0
    //   95: invokevirtual toString : ()Ljava/lang/String;
    //   98: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   103: astore_0
    //   104: aload #5
    //   106: invokevirtual recycle : ()V
    //   109: aload_0
    //   110: areturn
    //   111: aload_0
    //   112: invokeinterface getActivities : ()Ljava/util/List;
    //   117: astore #8
    //   119: aload #8
    //   121: invokestatic size : (Ljava/util/Collection;)I
    //   124: istore #9
    //   126: iconst_0
    //   127: istore #10
    //   129: iload #10
    //   131: iload #9
    //   133: if_icmpge -> 172
    //   136: aload #8
    //   138: iload #10
    //   140: invokeinterface get : (I)Ljava/lang/Object;
    //   145: checkcast android/content/pm/parsing/component/ParsedActivity
    //   148: astore #7
    //   150: aload #6
    //   152: aload #7
    //   154: invokevirtual getName : ()Ljava/lang/String;
    //   157: invokevirtual equals : (Ljava/lang/Object;)Z
    //   160: ifeq -> 166
    //   163: goto -> 175
    //   166: iinc #10, 1
    //   169: goto -> 129
    //   172: aconst_null
    //   173: astore #7
    //   175: aload #7
    //   177: ifnonnull -> 255
    //   180: new java/lang/StringBuilder
    //   183: astore_1
    //   184: aload_1
    //   185: invokespecial <init> : ()V
    //   188: aload_1
    //   189: ldc '<activity-alias> target activity '
    //   191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   194: pop
    //   195: aload_1
    //   196: aload #6
    //   198: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   201: pop
    //   202: aload_1
    //   203: ldc ' not found in manifest with activities = '
    //   205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: pop
    //   209: aload_1
    //   210: aload_0
    //   211: invokeinterface getActivities : ()Ljava/util/List;
    //   216: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload_1
    //   221: ldc ', parsedActivities = '
    //   223: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload_1
    //   228: aload #8
    //   230: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload_1
    //   235: invokevirtual toString : ()Ljava/lang/String;
    //   238: astore_0
    //   239: aload #4
    //   241: aload_0
    //   242: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   247: astore_0
    //   248: aload #5
    //   250: invokevirtual recycle : ()V
    //   253: aload_0
    //   254: areturn
    //   255: aload #6
    //   257: aload #7
    //   259: invokestatic makeAlias : (Ljava/lang/String;Landroid/content/pm/parsing/component/ParsedActivity;)Landroid/content/pm/parsing/component/ParsedActivity;
    //   262: astore #7
    //   264: new java/lang/StringBuilder
    //   267: astore #6
    //   269: aload #6
    //   271: invokespecial <init> : ()V
    //   274: aload #6
    //   276: ldc '<'
    //   278: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   281: pop
    //   282: aload #6
    //   284: aload_2
    //   285: invokeinterface getName : ()Ljava/lang/String;
    //   290: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   293: pop
    //   294: aload #6
    //   296: ldc '>'
    //   298: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   301: pop
    //   302: aload #6
    //   304: invokevirtual toString : ()Ljava/lang/String;
    //   307: astore #6
    //   309: aload #7
    //   311: aload #6
    //   313: aconst_null
    //   314: aload_0
    //   315: aload #5
    //   317: iconst_0
    //   318: iload_3
    //   319: aload #4
    //   321: bipush #10
    //   323: bipush #6
    //   325: aconst_null
    //   326: iconst_4
    //   327: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   330: iconst_1
    //   331: iconst_0
    //   332: bipush #8
    //   334: iconst_2
    //   335: aconst_null
    //   336: bipush #11
    //   338: aconst_null
    //   339: invokestatic parseMainComponent : (Landroid/content/pm/parsing/component/ParsedMainComponent;Ljava/lang/String;[Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/TypedArray;IZLandroid/content/pm/parsing/result/ParseInput;IILjava/lang/Integer;Ljava/lang/Integer;IIIILjava/lang/Integer;ILjava/lang/Integer;)Landroid/content/pm/parsing/result/ParseResult;
    //   342: astore #8
    //   344: aload #8
    //   346: invokeinterface isError : ()Z
    //   351: istore_3
    //   352: iload_3
    //   353: ifeq -> 364
    //   356: aload #5
    //   358: invokevirtual recycle : ()V
    //   361: aload #8
    //   363: areturn
    //   364: aload #7
    //   366: invokevirtual getFlags : ()I
    //   369: ldc 1048576
    //   371: iand
    //   372: ifeq -> 380
    //   375: iconst_1
    //   376: istore_3
    //   377: goto -> 382
    //   380: iconst_0
    //   381: istore_3
    //   382: aload #7
    //   384: aload_0
    //   385: aload #6
    //   387: aload_2
    //   388: aload_1
    //   389: aload #5
    //   391: iconst_0
    //   392: iconst_1
    //   393: iload_3
    //   394: aload #4
    //   396: bipush #9
    //   398: iconst_3
    //   399: iconst_5
    //   400: invokestatic parseActivityOrAlias : (Landroid/content/pm/parsing/component/ParsedActivity;Landroid/content/pm/parsing/ParsingPackage;Ljava/lang/String;Landroid/content/res/XmlResourceParser;Landroid/content/res/Resources;Landroid/content/res/TypedArray;ZZZLandroid/content/pm/parsing/result/ParseInput;III)Landroid/content/pm/parsing/result/ParseResult;
    //   403: astore_0
    //   404: aload #5
    //   406: invokevirtual recycle : ()V
    //   409: aload_0
    //   410: areturn
    //   411: astore_0
    //   412: goto -> 416
    //   415: astore_0
    //   416: aload #5
    //   418: invokevirtual recycle : ()V
    //   421: aload_0
    //   422: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #209	-> 0
    //   #211	-> 10
    //   #214	-> 22
    //   #215	-> 27
    //   #273	-> 37
    //   #215	-> 42
    //   #273	-> 44
    //   #218	-> 48
    //   #219	-> 56
    //   #220	-> 65
    //   #221	-> 70
    //   #273	-> 104
    //   #221	-> 109
    //   #224	-> 111
    //   #226	-> 111
    //   #227	-> 119
    //   #228	-> 126
    //   #229	-> 136
    //   #230	-> 150
    //   #231	-> 163
    //   #232	-> 163
    //   #228	-> 166
    //   #236	-> 175
    //   #237	-> 180
    //   #239	-> 209
    //   #237	-> 239
    //   #273	-> 248
    //   #237	-> 253
    //   #243	-> 255
    //   #244	-> 264
    //   #246	-> 309
    //   #251	-> 309
    //   #246	-> 309
    //   #259	-> 344
    //   #260	-> 356
    //   #273	-> 356
    //   #260	-> 361
    //   #264	-> 364
    //   #265	-> 364
    //   #267	-> 382
    //   #273	-> 404
    //   #267	-> 409
    //   #273	-> 411
    //   #274	-> 421
    // Exception table:
    //   from	to	target	type
    //   10	22	415	finally
    //   27	37	44	finally
    //   48	56	415	finally
    //   56	65	415	finally
    //   70	104	44	finally
    //   111	119	415	finally
    //   119	126	415	finally
    //   136	150	44	finally
    //   150	163	44	finally
    //   180	209	44	finally
    //   209	239	44	finally
    //   239	248	44	finally
    //   255	264	415	finally
    //   264	309	415	finally
    //   309	344	411	finally
    //   344	352	411	finally
    //   364	375	411	finally
    //   382	404	411	finally
  }
  
  private static ParseResult<ParsedActivity> parseActivityOrAlias(ParsedActivity paramParsedActivity, ParsingPackage paramParsingPackage, String paramString, XmlResourceParser paramXmlResourceParser, Resources paramResources, TypedArray paramTypedArray, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3, ParseInput paramParseInput, int paramInt1, int paramInt2, int paramInt3) throws IOException, XmlPullParserException {
    // Byte code:
    //   0: aload #5
    //   2: iload #10
    //   4: sipush #1024
    //   7: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   10: astore #13
    //   12: aload #13
    //   14: ifnull -> 103
    //   17: aload_1
    //   18: invokeinterface getPackageName : ()Ljava/lang/String;
    //   23: astore #14
    //   25: aload #14
    //   27: aload #13
    //   29: invokestatic buildClassName : (Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   32: astore #14
    //   34: aload #14
    //   36: ifnonnull -> 96
    //   39: new java/lang/StringBuilder
    //   42: dup
    //   43: invokespecial <init> : ()V
    //   46: astore #14
    //   48: aload #14
    //   50: ldc 'Activity '
    //   52: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: aload #14
    //   58: aload_0
    //   59: invokevirtual getName : ()Ljava/lang/String;
    //   62: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: aload #14
    //   68: ldc ' specified invalid parentActivityName '
    //   70: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   73: pop
    //   74: aload #14
    //   76: aload #13
    //   78: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   81: pop
    //   82: ldc 'PackageParsing'
    //   84: aload #14
    //   86: invokevirtual toString : ()Ljava/lang/String;
    //   89: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   92: pop
    //   93: goto -> 103
    //   96: aload_0
    //   97: aload #14
    //   99: invokevirtual setParentActivity : (Ljava/lang/String;)Landroid/content/pm/parsing/component/ParsedActivity;
    //   102: pop
    //   103: iconst_0
    //   104: istore #15
    //   106: aload #5
    //   108: iload #11
    //   110: iconst_0
    //   111: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   114: astore #13
    //   116: iload #7
    //   118: ifeq -> 131
    //   121: aload_0
    //   122: aload #13
    //   124: invokevirtual setPermission : (Ljava/lang/String;)Landroid/content/pm/parsing/component/ParsedActivity;
    //   127: pop
    //   128: goto -> 158
    //   131: aload #13
    //   133: ifnull -> 143
    //   136: aload #13
    //   138: astore #14
    //   140: goto -> 151
    //   143: aload_1
    //   144: invokeinterface getPermission : ()Ljava/lang/String;
    //   149: astore #14
    //   151: aload_0
    //   152: aload #14
    //   154: invokevirtual setPermission : (Ljava/lang/String;)Landroid/content/pm/parsing/component/ParsedActivity;
    //   157: pop
    //   158: aload #5
    //   160: iload #12
    //   162: invokevirtual hasValue : (I)Z
    //   165: istore #16
    //   167: iload #16
    //   169: ifeq -> 184
    //   172: aload_0
    //   173: aload #5
    //   175: iload #12
    //   177: iconst_0
    //   178: invokevirtual getBoolean : (IZ)Z
    //   181: putfield exported : Z
    //   184: aload_3
    //   185: invokeinterface getDepth : ()I
    //   190: istore #10
    //   192: aload_3
    //   193: invokeinterface next : ()I
    //   198: istore #11
    //   200: iload #11
    //   202: iconst_1
    //   203: if_icmpeq -> 528
    //   206: iload #11
    //   208: iconst_3
    //   209: if_icmpne -> 229
    //   212: aload_3
    //   213: invokeinterface getDepth : ()I
    //   218: iload #10
    //   220: if_icmple -> 226
    //   223: goto -> 229
    //   226: goto -> 528
    //   229: iload #11
    //   231: iconst_2
    //   232: if_icmpeq -> 238
    //   235: goto -> 192
    //   238: aload_3
    //   239: invokeinterface getName : ()Ljava/lang/String;
    //   244: ldc 'intent-filter'
    //   246: invokevirtual equals : (Ljava/lang/Object;)Z
    //   249: ifeq -> 322
    //   252: aload_1
    //   253: aload_0
    //   254: iload #6
    //   256: iconst_1
    //   257: ixor
    //   258: iload #8
    //   260: aload #4
    //   262: aload_3
    //   263: aload #9
    //   265: invokestatic parseIntentFilter : (Landroid/content/pm/parsing/ParsingPackage;Landroid/content/pm/parsing/component/ParsedActivity;ZZLandroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   268: astore #5
    //   270: aload #5
    //   272: invokeinterface isSuccess : ()Z
    //   277: ifeq -> 319
    //   280: aload #5
    //   282: invokeinterface getResult : ()Ljava/lang/Object;
    //   287: checkcast android/content/pm/parsing/component/ParsedIntentInfo
    //   290: astore #14
    //   292: aload #14
    //   294: ifnull -> 319
    //   297: aload_0
    //   298: aload #14
    //   300: invokevirtual getOrder : ()I
    //   303: aload_0
    //   304: getfield order : I
    //   307: invokestatic max : (II)I
    //   310: putfield order : I
    //   313: aload_0
    //   314: aload #14
    //   316: invokevirtual addIntent : (Landroid/content/pm/parsing/component/ParsedIntentInfo;)V
    //   319: goto -> 505
    //   322: aload_3
    //   323: invokeinterface getName : ()Ljava/lang/String;
    //   328: ldc 'meta-data'
    //   330: invokevirtual equals : (Ljava/lang/Object;)Z
    //   333: ifeq -> 351
    //   336: aload_0
    //   337: aload_1
    //   338: aload #4
    //   340: aload_3
    //   341: aload #9
    //   343: invokestatic addMetaData : (Landroid/content/pm/parsing/component/ParsedComponent;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   346: astore #5
    //   348: goto -> 505
    //   351: iload #6
    //   353: ifne -> 433
    //   356: iload #7
    //   358: ifne -> 433
    //   361: aload_3
    //   362: invokeinterface getName : ()Ljava/lang/String;
    //   367: ldc 'preferred'
    //   369: invokevirtual equals : (Ljava/lang/Object;)Z
    //   372: ifeq -> 433
    //   375: aload_1
    //   376: aload_0
    //   377: iconst_1
    //   378: iload #8
    //   380: aload #4
    //   382: aload_3
    //   383: aload #9
    //   385: invokestatic parseIntentFilter : (Landroid/content/pm/parsing/ParsingPackage;Landroid/content/pm/parsing/component/ParsedActivity;ZZLandroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   388: astore #5
    //   390: aload #5
    //   392: invokeinterface isSuccess : ()Z
    //   397: ifeq -> 430
    //   400: aload #5
    //   402: invokeinterface getResult : ()Ljava/lang/Object;
    //   407: checkcast android/content/pm/parsing/component/ParsedIntentInfo
    //   410: astore #14
    //   412: aload #14
    //   414: ifnull -> 430
    //   417: aload_1
    //   418: aload_0
    //   419: invokevirtual getClassName : ()Ljava/lang/String;
    //   422: aload #14
    //   424: invokeinterface addPreferredActivityFilter : (Ljava/lang/String;Landroid/content/pm/parsing/component/ParsedIntentInfo;)Landroid/content/pm/parsing/ParsingPackage;
    //   429: pop
    //   430: goto -> 505
    //   433: iload #6
    //   435: ifne -> 495
    //   438: iload #7
    //   440: ifne -> 495
    //   443: aload_3
    //   444: invokeinterface getName : ()Ljava/lang/String;
    //   449: ldc_w 'layout'
    //   452: invokevirtual equals : (Ljava/lang/Object;)Z
    //   455: ifeq -> 495
    //   458: aload #4
    //   460: aload_3
    //   461: aload #9
    //   463: invokestatic parseLayout : (Landroid/content/res/Resources;Landroid/util/AttributeSet;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   466: astore #5
    //   468: aload #5
    //   470: invokeinterface isSuccess : ()Z
    //   475: ifeq -> 492
    //   478: aload_0
    //   479: aload #5
    //   481: invokeinterface getResult : ()Ljava/lang/Object;
    //   486: checkcast android/content/pm/ActivityInfo$WindowLayout
    //   489: putfield windowLayout : Landroid/content/pm/ActivityInfo$WindowLayout;
    //   492: goto -> 505
    //   495: aload_2
    //   496: aload_1
    //   497: aload_3
    //   498: aload #9
    //   500: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   503: astore #5
    //   505: aload #5
    //   507: invokeinterface isError : ()Z
    //   512: ifeq -> 525
    //   515: aload #9
    //   517: aload #5
    //   519: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   524: areturn
    //   525: goto -> 192
    //   528: aload_0
    //   529: aload #9
    //   531: invokestatic resolveWindowLayout : (Landroid/content/pm/parsing/component/ParsedActivity;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   534: astore_1
    //   535: aload_1
    //   536: invokeinterface isError : ()Z
    //   541: ifeq -> 553
    //   544: aload #9
    //   546: aload_1
    //   547: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   552: areturn
    //   553: aload_0
    //   554: aload_1
    //   555: invokeinterface getResult : ()Ljava/lang/Object;
    //   560: checkcast android/content/pm/ActivityInfo$WindowLayout
    //   563: putfield windowLayout : Landroid/content/pm/ActivityInfo$WindowLayout;
    //   566: iload #16
    //   568: ifne -> 595
    //   571: aload_0
    //   572: invokevirtual getIntents : ()Ljava/util/List;
    //   575: invokeinterface size : ()I
    //   580: ifle -> 589
    //   583: iconst_1
    //   584: istore #15
    //   586: goto -> 589
    //   589: aload_0
    //   590: iload #15
    //   592: putfield exported : Z
    //   595: aload #9
    //   597: aload_0
    //   598: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   603: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #292	-> 0
    //   #293	-> 12
    //   #294	-> 17
    //   #295	-> 25
    //   #296	-> 34
    //   #297	-> 39
    //   #300	-> 96
    //   #304	-> 103
    //   #305	-> 116
    //   #309	-> 121
    //   #311	-> 131
    //   #314	-> 158
    //   #315	-> 167
    //   #316	-> 172
    //   #319	-> 184
    //   #321	-> 192
    //   #323	-> 212
    //   #324	-> 229
    //   #325	-> 235
    //   #329	-> 238
    //   #330	-> 252
    //   #332	-> 270
    //   #333	-> 280
    //   #334	-> 292
    //   #335	-> 297
    //   #336	-> 313
    //   #356	-> 319
    //   #357	-> 319
    //   #358	-> 336
    //   #359	-> 351
    //   #360	-> 375
    //   #363	-> 390
    //   #364	-> 400
    //   #365	-> 412
    //   #366	-> 417
    //   #369	-> 430
    //   #370	-> 430
    //   #371	-> 458
    //   #373	-> 468
    //   #374	-> 478
    //   #376	-> 492
    //   #377	-> 492
    //   #378	-> 495
    //   #381	-> 505
    //   #382	-> 515
    //   #384	-> 525
    //   #321	-> 528
    //   #386	-> 528
    //   #387	-> 535
    //   #388	-> 544
    //   #390	-> 553
    //   #392	-> 566
    //   #393	-> 571
    //   #396	-> 595
  }
  
  private static ParseResult<ParsedIntentInfo> parseIntentFilter(ParsingPackage paramParsingPackage, ParsedActivity paramParsedActivity, boolean paramBoolean1, boolean paramBoolean2, Resources paramResources, XmlResourceParser paramXmlResourceParser, ParseInput paramParseInput) throws IOException, XmlPullParserException {
    ParseResult<ParsedIntentInfo> parseResult = ParsedMainComponentUtils.parseIntentFilter(paramParsedActivity, paramParsingPackage, paramResources, paramXmlResourceParser, paramBoolean2, true, true, paramBoolean1, true, paramParseInput);
    if (parseResult.isError())
      return paramParseInput.error(parseResult); 
    ParsedIntentInfo parsedIntentInfo = parseResult.getResult();
    if (parsedIntentInfo != null) {
      if (parsedIntentInfo.isVisibleToInstantApp())
        paramParsedActivity.flags |= 0x100000; 
      if (parsedIntentInfo.isImplicitlyVisibleToInstantApp())
        paramParsedActivity.flags |= 0x200000; 
    } 
    return paramParseInput.success(parsedIntentInfo);
  }
  
  private static int getActivityResizeMode(ParsingPackage paramParsingPackage, TypedArray paramTypedArray, int paramInt) {
    Boolean bool = paramParsingPackage.getResizeableActivity();
    boolean bool1 = paramTypedArray.hasValue(40);
    boolean bool2 = true;
    if (bool1 || bool != null) {
      if (bool == null || 
        !bool.booleanValue())
        bool2 = false; 
      if (paramTypedArray.getBoolean(40, bool2))
        return 2; 
      return 0;
    } 
    if (paramParsingPackage.isResizeableActivityViaSdkVersion())
      return 1; 
    if (ActivityInfo.isFixedOrientationPortrait(paramInt))
      return 6; 
    if (ActivityInfo.isFixedOrientationLandscape(paramInt))
      return 5; 
    if (paramInt == 14)
      return 7; 
    return 4;
  }
  
  private static ParseResult<ActivityInfo.WindowLayout> parseLayout(Resources paramResources, AttributeSet paramAttributeSet, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramAttributeSet, R.styleable.AndroidManifestLayout);
    int i = -1;
    float f1 = -1.0F;
    int j = -1;
    float f2 = -1.0F;
    try {
      float f;
      int k = typedArray.getType(3);
      if (k == 6) {
        f = typedArray.getFraction(3, 1, 1, -1.0F);
      } else {
        f = f1;
        if (k == 5) {
          i = typedArray.getDimensionPixelSize(3, -1);
          f = f1;
        } 
      } 
      k = typedArray.getType(4);
      if (k == 6) {
        f1 = typedArray.getFraction(4, 1, 1, -1.0F);
      } else {
        f1 = f2;
        if (k == 5) {
          j = typedArray.getDimensionPixelSize(4, -1);
          f1 = f2;
        } 
      } 
      int m = typedArray.getInt(0, 17);
      int n = typedArray.getDimensionPixelSize(1, -1);
      k = typedArray.getDimensionPixelSize(2, -1);
      ActivityInfo.WindowLayout windowLayout = new ActivityInfo.WindowLayout();
      this(i, f, j, f1, m, n, k);
      try {
        ParseResult<ActivityInfo.WindowLayout> parseResult = paramParseInput.success(windowLayout);
        typedArray.recycle();
        return parseResult;
      } finally {}
    } finally {}
    typedArray.recycle();
    throw paramResources;
  }
  
  private static ParseResult<ActivityInfo.WindowLayout> resolveWindowLayout(ParsedActivity paramParsedActivity, ParseInput paramParseInput) {
    if (paramParsedActivity.metaData == null || !paramParsedActivity.metaData.containsKey("android.activity_window_layout_affinity"))
      return paramParseInput.success(paramParsedActivity.windowLayout); 
    if (paramParsedActivity.windowLayout != null && paramParsedActivity.windowLayout.windowLayoutAffinity != null)
      return paramParseInput.success(paramParsedActivity.windowLayout); 
    String str = paramParsedActivity.metaData.getString("android.activity_window_layout_affinity");
    ActivityInfo.WindowLayout windowLayout2 = paramParsedActivity.windowLayout;
    ActivityInfo.WindowLayout windowLayout1 = windowLayout2;
    if (windowLayout2 == null)
      windowLayout1 = new ActivityInfo.WindowLayout(-1, -1.0F, -1, -1.0F, 0, -1, -1); 
    windowLayout1.windowLayoutAffinity = str;
    return paramParseInput.success(windowLayout1);
  }
}
