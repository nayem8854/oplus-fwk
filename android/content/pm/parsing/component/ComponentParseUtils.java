package android.content.pm.parsing.component;

import android.content.pm.PackageParser;
import android.content.pm.PackageUserState;
import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.ParsingUtils;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.text.TextUtils;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class ComponentParseUtils {
  private static final String TAG = "PackageParsing";
  
  public static boolean isImplicitlyExposedIntent(ParsedIntentInfo paramParsedIntentInfo) {
    return (paramParsedIntentInfo.hasCategory("android.intent.category.BROWSABLE") || 
      paramParsedIntentInfo.hasAction("android.intent.action.SEND") || 
      paramParsedIntentInfo.hasAction("android.intent.action.SENDTO") || 
      paramParsedIntentInfo.hasAction("android.intent.action.SEND_MULTIPLE"));
  }
  
  static <Component extends ParsedComponent> ParseResult<Component> parseAllMetaData(ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, String paramString, Component paramComponent, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    int i = paramXmlResourceParser.getDepth();
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        ParseResult<?> parseResult;
        if (j != 2)
          continue; 
        if ("meta-data".equals(paramXmlResourceParser.getName())) {
          parseResult = ParsedComponentUtils.addMetaData((ParsedComponent)paramComponent, paramParsingPackage, paramResources, paramXmlResourceParser, paramParseInput);
        } else {
          parseResult = ParsingUtils.unknownTag(paramString, paramParsingPackage, paramXmlResourceParser, paramParseInput);
        } 
        if (parseResult.isError())
          return paramParseInput.error(parseResult); 
        continue;
      } 
      break;
    } 
    return paramParseInput.success(paramComponent);
  }
  
  public static ParseResult<String> buildProcessName(String paramString1, String paramString2, CharSequence paramCharSequence, int paramInt, String[] paramArrayOfString, ParseInput paramParseInput) {
    if ((paramInt & 0x2) != 0 && !"system".contentEquals(paramCharSequence)) {
      if (paramString2 != null)
        paramString1 = paramString2; 
      return paramParseInput.success(paramString1);
    } 
    if (paramArrayOfString != null)
      for (paramInt = paramArrayOfString.length - 1; paramInt >= 0; paramInt--) {
        String str = paramArrayOfString[paramInt];
        if (str.equals(paramString1) || str.equals(paramString2) || str.contentEquals(paramCharSequence))
          return paramParseInput.success(paramString1); 
      }  
    if (paramCharSequence == null || paramCharSequence.length() <= 0)
      return paramParseInput.success(paramString2); 
    ParseResult<String> parseResult = buildCompoundName(paramString1, paramCharSequence, "process", paramParseInput);
    return paramParseInput.success(TextUtils.safeIntern(parseResult.getResult()));
  }
  
  public static ParseResult<String> buildTaskAffinityName(String paramString1, String paramString2, CharSequence paramCharSequence, ParseInput paramParseInput) {
    if (paramCharSequence == null)
      return paramParseInput.success(paramString2); 
    if (paramCharSequence.length() <= 0)
      return paramParseInput.success(null); 
    return buildCompoundName(paramString1, paramCharSequence, "taskAffinity", paramParseInput);
  }
  
  public static ParseResult<String> buildCompoundName(String paramString1, CharSequence paramCharSequence, String paramString2, ParseInput paramParseInput) {
    StringBuilder stringBuilder;
    paramCharSequence = paramCharSequence.toString();
    char c = paramCharSequence.charAt(0);
    if (paramString1 != null && c == ':') {
      if (paramCharSequence.length() < 2) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Bad ");
        stringBuilder1.append(paramString2);
        stringBuilder1.append(" name ");
        stringBuilder1.append((String)paramCharSequence);
        stringBuilder1.append(" in package ");
        stringBuilder1.append(paramString1);
        stringBuilder1.append(": must be at least two characters");
        return paramParseInput.error(stringBuilder1.toString());
      } 
      String str1 = paramCharSequence.substring(1);
      String str2 = PackageParser.validateName(str1, false, false);
      if (str2 != null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Invalid ");
        stringBuilder1.append(paramString2);
        stringBuilder1.append(" name ");
        stringBuilder1.append((String)paramCharSequence);
        stringBuilder1.append(" in package ");
        stringBuilder1.append(paramString1);
        stringBuilder1.append(": ");
        stringBuilder1.append(str2);
        return paramParseInput.error(stringBuilder1.toString());
      } 
      stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append((String)paramCharSequence);
      return paramParseInput.success(stringBuilder.toString());
    } 
    String str = PackageParser.validateName((String)paramCharSequence, true, false);
    if (str != null && !"system".equals(paramCharSequence)) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid ");
      stringBuilder1.append((String)stringBuilder);
      stringBuilder1.append(" name ");
      stringBuilder1.append((String)paramCharSequence);
      stringBuilder1.append(" in package ");
      stringBuilder1.append(paramString1);
      stringBuilder1.append(": ");
      stringBuilder1.append(str);
      return paramParseInput.error(stringBuilder1.toString());
    } 
    return (ParseResult)paramParseInput.success(paramCharSequence);
  }
  
  public static int flag(int paramInt1, int paramInt2, TypedArray paramTypedArray) {
    int i = 0;
    if (paramTypedArray.getBoolean(paramInt2, false))
      i = paramInt1; 
    return i;
  }
  
  public static int flag(int paramInt1, int paramInt2, boolean paramBoolean, TypedArray paramTypedArray) {
    if (!paramTypedArray.getBoolean(paramInt2, paramBoolean))
      paramInt1 = 0; 
    return paramInt1;
  }
  
  public static CharSequence getNonLocalizedLabel(ParsedComponent paramParsedComponent) {
    return paramParsedComponent.nonLocalizedLabel;
  }
  
  public static int getIcon(ParsedComponent paramParsedComponent) {
    return paramParsedComponent.icon;
  }
  
  public static boolean isMatch(PackageUserState paramPackageUserState, boolean paramBoolean1, boolean paramBoolean2, ParsedMainComponent paramParsedMainComponent, int paramInt) {
    boolean bool1 = paramParsedMainComponent.isEnabled();
    boolean bool2 = paramParsedMainComponent.isDirectBootAware();
    String str = paramParsedMainComponent.getName();
    return paramPackageUserState.isMatch(paramBoolean1, paramBoolean2, bool1, bool2, str, paramInt);
  }
  
  public static boolean isEnabled(PackageUserState paramPackageUserState, boolean paramBoolean, ParsedMainComponent paramParsedMainComponent, int paramInt) {
    boolean bool = paramParsedMainComponent.isEnabled();
    String str = paramParsedMainComponent.getName();
    return paramPackageUserState.isEnabled(paramBoolean, bool, str, paramInt);
  }
}
