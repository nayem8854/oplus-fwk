package android.content.pm.parsing.component;

import android.os.Parcel;
import android.os.Parcelable;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusBaseParsedMainComponent extends ParsedComponent {
  public OplusBaseParsedMainComponent() {}
  
  public OplusBaseParsedMainComponent(ParsedComponent paramParsedComponent) {
    super(paramParsedComponent);
    setOplusFlags(paramParsedComponent);
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.oplusFlags);
  }
  
  protected OplusBaseParsedMainComponent(Parcel paramParcel) {
    super(paramParcel);
    this.oplusFlags = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<OplusBaseParsedMainComponent> CREATOR = (Parcelable.Creator<OplusBaseParsedMainComponent>)new Object();
  
  public int oplusFlags;
  
  private void setOplusFlags(ParsedComponent paramParsedComponent) {
    paramParsedComponent = (OplusBaseParsedMainComponent)OplusTypeCastingHelper.typeCasting(OplusBaseParsedMainComponent.class, paramParsedComponent);
    if (paramParsedComponent != null)
      this.oplusFlags = ((OplusBaseParsedMainComponent)paramParsedComponent).oplusFlags; 
  }
}
