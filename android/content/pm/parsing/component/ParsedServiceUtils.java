package android.content.pm.parsing.component;

import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedServiceUtils {
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<ParsedService> parseService(String[] paramArrayOfString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, int paramInt, boolean paramBoolean, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: invokeinterface getPackageName : ()Ljava/lang/String;
    //   6: astore #7
    //   8: new android/content/pm/parsing/component/ParsedService
    //   11: dup
    //   12: invokespecial <init> : ()V
    //   15: astore #8
    //   17: aload_3
    //   18: invokeinterface getName : ()Ljava/lang/String;
    //   23: astore #9
    //   25: aload_2
    //   26: aload_3
    //   27: getstatic com/android/internal/R$styleable.AndroidManifestService : [I
    //   30: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   33: astore #10
    //   35: aload #9
    //   37: astore #11
    //   39: aload #8
    //   41: aload #9
    //   43: aload_0
    //   44: aload_1
    //   45: aload #10
    //   47: iload #4
    //   49: iload #5
    //   51: aload #6
    //   53: bipush #12
    //   55: bipush #7
    //   57: bipush #13
    //   59: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   62: iconst_4
    //   63: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   66: iconst_1
    //   67: iconst_0
    //   68: bipush #8
    //   70: iconst_2
    //   71: bipush #6
    //   73: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   76: bipush #15
    //   78: bipush #17
    //   80: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   83: invokestatic parseMainComponent : (Landroid/content/pm/parsing/component/ParsedMainComponent;Ljava/lang/String;[Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/TypedArray;IZLandroid/content/pm/parsing/result/ParseInput;IILjava/lang/Integer;Ljava/lang/Integer;IIIILjava/lang/Integer;ILjava/lang/Integer;)Landroid/content/pm/parsing/result/ParseResult;
    //   86: astore_0
    //   87: aload_0
    //   88: invokeinterface isError : ()Z
    //   93: istore #5
    //   95: iload #5
    //   97: ifeq -> 107
    //   100: aload #10
    //   102: invokevirtual recycle : ()V
    //   105: aload_0
    //   106: areturn
    //   107: aload #10
    //   109: iconst_5
    //   110: invokevirtual hasValue : (I)Z
    //   113: istore #5
    //   115: iload #5
    //   117: ifeq -> 147
    //   120: aload #10
    //   122: iconst_5
    //   123: iconst_0
    //   124: invokevirtual getBoolean : (IZ)Z
    //   127: istore #12
    //   129: aload #8
    //   131: iload #12
    //   133: putfield exported : Z
    //   136: goto -> 147
    //   139: astore_0
    //   140: goto -> 651
    //   143: astore_0
    //   144: goto -> 651
    //   147: aload #10
    //   149: iconst_3
    //   150: iconst_0
    //   151: invokevirtual getNonConfigurationString : (II)Ljava/lang/String;
    //   154: astore_0
    //   155: aload_0
    //   156: ifnull -> 162
    //   159: goto -> 169
    //   162: aload_1
    //   163: invokeinterface getPermission : ()Ljava/lang/String;
    //   168: astore_0
    //   169: aload #8
    //   171: aload_0
    //   172: invokevirtual setPermission : (Ljava/lang/String;)Landroid/content/pm/parsing/component/ParsedMainComponent;
    //   175: pop
    //   176: aload #8
    //   178: aload #10
    //   180: bipush #19
    //   182: iconst_0
    //   183: invokevirtual getInt : (II)I
    //   186: putfield foregroundServiceType : I
    //   189: aload #8
    //   191: getfield flags : I
    //   194: istore #13
    //   196: iconst_1
    //   197: istore #4
    //   199: iconst_1
    //   200: bipush #9
    //   202: aload #10
    //   204: invokestatic flag : (IILandroid/content/res/TypedArray;)I
    //   207: istore #14
    //   209: iconst_2
    //   210: istore #15
    //   212: iconst_2
    //   213: bipush #10
    //   215: aload #10
    //   217: invokestatic flag : (IILandroid/content/res/TypedArray;)I
    //   220: istore #16
    //   222: iconst_4
    //   223: bipush #14
    //   225: aload #10
    //   227: invokestatic flag : (IILandroid/content/res/TypedArray;)I
    //   230: istore #17
    //   232: bipush #8
    //   234: bipush #18
    //   236: aload #10
    //   238: invokestatic flag : (IILandroid/content/res/TypedArray;)I
    //   241: istore #18
    //   243: aload #8
    //   245: iload #13
    //   247: iload #14
    //   249: iload #16
    //   251: ior
    //   252: iload #17
    //   254: ior
    //   255: iload #18
    //   257: ior
    //   258: ldc 1073741824
    //   260: bipush #11
    //   262: aload #10
    //   264: invokestatic flag : (IILandroid/content/res/TypedArray;)I
    //   267: ior
    //   268: ior
    //   269: putfield flags : I
    //   272: aload #10
    //   274: bipush #16
    //   276: iconst_0
    //   277: invokevirtual getBoolean : (IZ)Z
    //   280: istore #12
    //   282: iload #12
    //   284: ifeq -> 315
    //   287: aload #8
    //   289: aload #8
    //   291: getfield flags : I
    //   294: ldc 1048576
    //   296: ior
    //   297: putfield flags : I
    //   300: aload_1
    //   301: iconst_1
    //   302: invokeinterface setVisibleToInstantApps : (Z)Landroid/content/pm/parsing/ParsingPackage;
    //   307: pop
    //   308: goto -> 315
    //   311: astore_0
    //   312: goto -> 140
    //   315: aload #10
    //   317: invokevirtual recycle : ()V
    //   320: aload_1
    //   321: invokeinterface isCantSaveState : ()Z
    //   326: ifeq -> 355
    //   329: aload #8
    //   331: invokevirtual getProcessName : ()Ljava/lang/String;
    //   334: aload #7
    //   336: invokestatic equals : (Ljava/lang/Object;Ljava/lang/Object;)Z
    //   339: ifeq -> 352
    //   342: aload #6
    //   344: ldc 'Heavy-weight applications can not have services in main process'
    //   346: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   351: areturn
    //   352: goto -> 355
    //   355: aload #6
    //   357: astore #9
    //   359: aload_3
    //   360: invokeinterface getDepth : ()I
    //   365: istore #18
    //   367: aload_3
    //   368: invokeinterface next : ()I
    //   373: istore #17
    //   375: iload #17
    //   377: iload #4
    //   379: if_icmpeq -> 598
    //   382: iload #17
    //   384: iconst_3
    //   385: if_icmpne -> 405
    //   388: aload_3
    //   389: invokeinterface getDepth : ()I
    //   394: iload #18
    //   396: if_icmple -> 402
    //   399: goto -> 405
    //   402: goto -> 598
    //   405: iload #17
    //   407: iload #15
    //   409: if_icmpeq -> 415
    //   412: goto -> 367
    //   415: aload_3
    //   416: invokeinterface getName : ()Ljava/lang/String;
    //   421: astore_0
    //   422: iconst_m1
    //   423: istore #17
    //   425: aload_0
    //   426: invokevirtual hashCode : ()I
    //   429: istore #14
    //   431: iload #14
    //   433: ldc -1115949454
    //   435: if_icmpeq -> 463
    //   438: iload #14
    //   440: ldc -1029793847
    //   442: if_icmpeq -> 448
    //   445: goto -> 476
    //   448: aload_0
    //   449: ldc 'intent-filter'
    //   451: invokevirtual equals : (Ljava/lang/Object;)Z
    //   454: ifeq -> 445
    //   457: iconst_0
    //   458: istore #17
    //   460: goto -> 476
    //   463: aload_0
    //   464: ldc 'meta-data'
    //   466: invokevirtual equals : (Ljava/lang/Object;)Z
    //   469: ifeq -> 445
    //   472: iload #4
    //   474: istore #17
    //   476: iload #17
    //   478: ifeq -> 515
    //   481: iload #17
    //   483: iload #4
    //   485: if_icmpeq -> 501
    //   488: aload #11
    //   490: aload_1
    //   491: aload_3
    //   492: aload #9
    //   494: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   497: astore_0
    //   498: goto -> 577
    //   501: aload #8
    //   503: aload_1
    //   504: aload_2
    //   505: aload_3
    //   506: aload #9
    //   508: invokestatic addMetaData : (Landroid/content/pm/parsing/component/ParsedComponent;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   511: astore_0
    //   512: goto -> 577
    //   515: aload #8
    //   517: aload_1
    //   518: aload_2
    //   519: aload_3
    //   520: iload #12
    //   522: iconst_1
    //   523: iconst_0
    //   524: iconst_0
    //   525: iconst_0
    //   526: aload #6
    //   528: invokestatic parseIntentFilter : (Landroid/content/pm/parsing/component/ParsedMainComponent;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZZZZZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   531: astore_0
    //   532: aload_0
    //   533: invokeinterface isSuccess : ()Z
    //   538: ifeq -> 577
    //   541: aload_0
    //   542: invokeinterface getResult : ()Ljava/lang/Object;
    //   547: checkcast android/content/pm/parsing/component/ParsedIntentInfo
    //   550: astore #10
    //   552: aload #8
    //   554: aload #10
    //   556: invokevirtual getOrder : ()I
    //   559: aload #8
    //   561: getfield order : I
    //   564: invokestatic max : (II)I
    //   567: putfield order : I
    //   570: aload #8
    //   572: aload #10
    //   574: invokevirtual addIntent : (Landroid/content/pm/parsing/component/ParsedIntentInfo;)V
    //   577: aload_0
    //   578: invokeinterface isError : ()Z
    //   583: ifeq -> 595
    //   586: aload #9
    //   588: aload_0
    //   589: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   594: areturn
    //   595: goto -> 367
    //   598: iload #5
    //   600: ifne -> 629
    //   603: aload #8
    //   605: invokevirtual getIntents : ()Ljava/util/List;
    //   608: invokeinterface size : ()I
    //   613: ifle -> 619
    //   616: goto -> 622
    //   619: iconst_0
    //   620: istore #4
    //   622: aload #8
    //   624: iload #4
    //   626: putfield exported : Z
    //   629: aload #9
    //   631: aload #8
    //   633: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   638: areturn
    //   639: astore_0
    //   640: goto -> 651
    //   643: astore_0
    //   644: goto -> 651
    //   647: astore_0
    //   648: goto -> 651
    //   651: aload #10
    //   653: invokevirtual recycle : ()V
    //   656: aload_0
    //   657: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #54	-> 0
    //   #55	-> 8
    //   #56	-> 17
    //   #58	-> 25
    //   #60	-> 35
    //   #64	-> 35
    //   #65	-> 35
    //   #70	-> 35
    //   #72	-> 35
    //   #60	-> 35
    //   #75	-> 87
    //   #76	-> 100
    //   #111	-> 100
    //   #76	-> 105
    //   #79	-> 107
    //   #80	-> 115
    //   #81	-> 120
    //   #111	-> 139
    //   #80	-> 147
    //   #85	-> 147
    //   #87	-> 155
    //   #89	-> 176
    //   #93	-> 189
    //   #95	-> 209
    //   #97	-> 222
    //   #99	-> 232
    //   #101	-> 243
    //   #104	-> 272
    //   #106	-> 282
    //   #107	-> 287
    //   #108	-> 300
    //   #111	-> 311
    //   #106	-> 315
    //   #111	-> 315
    //   #112	-> 320
    //   #114	-> 320
    //   #117	-> 329
    //   #118	-> 342
    //   #117	-> 352
    //   #114	-> 355
    //   #122	-> 355
    //   #124	-> 367
    //   #126	-> 388
    //   #127	-> 405
    //   #128	-> 412
    //   #132	-> 415
    //   #150	-> 488
    //   #147	-> 501
    //   #148	-> 512
    //   #134	-> 515
    //   #135	-> 515
    //   #139	-> 532
    //   #140	-> 532
    //   #141	-> 541
    //   #142	-> 552
    //   #143	-> 570
    //   #154	-> 577
    //   #155	-> 586
    //   #157	-> 595
    //   #124	-> 598
    //   #159	-> 598
    //   #160	-> 603
    //   #163	-> 629
    //   #111	-> 639
    //   #112	-> 656
    // Exception table:
    //   from	to	target	type
    //   39	87	647	finally
    //   87	95	647	finally
    //   107	115	643	finally
    //   120	129	143	finally
    //   129	136	139	finally
    //   147	155	639	finally
    //   162	169	639	finally
    //   169	176	639	finally
    //   176	189	639	finally
    //   189	196	639	finally
    //   199	209	639	finally
    //   212	222	639	finally
    //   222	232	639	finally
    //   232	243	639	finally
    //   243	272	639	finally
    //   272	282	639	finally
    //   287	300	311	finally
    //   300	308	139	finally
  }
}
