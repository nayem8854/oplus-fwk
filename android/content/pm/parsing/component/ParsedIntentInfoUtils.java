package android.content.pm.parsing.component;

import android.content.IntentFilter;
import android.content.pm.parsing.ParsingPackage;
import android.content.pm.parsing.result.ParseInput;
import android.content.pm.parsing.result.ParseResult;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import com.android.internal.R;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;

public class ParsedIntentInfoUtils {
  private static final String TAG = "PackageParsing";
  
  public static ParseResult<ParsedIntentInfo> parseIntentInfo(String paramString, ParsingPackage paramParsingPackage, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean1, boolean paramBoolean2, ParseInput paramParseInput) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: new android/content/pm/parsing/component/ParsedIntentInfo
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore #7
    //   9: aload_2
    //   10: aload_3
    //   11: getstatic com/android/internal/R$styleable.AndroidManifestIntentFilter : [I
    //   14: invokevirtual obtainAttributes : (Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;
    //   17: astore_0
    //   18: aload #7
    //   20: aload_0
    //   21: iconst_2
    //   22: iconst_0
    //   23: invokevirtual getInt : (II)I
    //   26: invokevirtual setPriority : (I)V
    //   29: aload #7
    //   31: aload_0
    //   32: iconst_3
    //   33: iconst_0
    //   34: invokevirtual getInt : (II)I
    //   37: invokevirtual setOrder : (I)V
    //   40: aload_0
    //   41: iconst_0
    //   42: invokevirtual peekValue : (I)Landroid/util/TypedValue;
    //   45: astore #8
    //   47: aload #8
    //   49: ifnull -> 80
    //   52: aload #7
    //   54: aload #8
    //   56: getfield resourceId : I
    //   59: putfield labelRes : I
    //   62: aload #8
    //   64: getfield resourceId : I
    //   67: ifne -> 80
    //   70: aload #7
    //   72: aload #8
    //   74: invokevirtual coerceToString : ()Ljava/lang/CharSequence;
    //   77: putfield nonLocalizedLabel : Ljava/lang/CharSequence;
    //   80: getstatic android/content/pm/PackageParser.sUseRoundIcon : Z
    //   83: ifeq -> 98
    //   86: aload #7
    //   88: aload_0
    //   89: bipush #7
    //   91: iconst_0
    //   92: invokevirtual getResourceId : (II)I
    //   95: putfield icon : I
    //   98: aload #7
    //   100: getfield icon : I
    //   103: ifne -> 117
    //   106: aload #7
    //   108: aload_0
    //   109: iconst_1
    //   110: iconst_0
    //   111: invokevirtual getResourceId : (II)I
    //   114: putfield icon : I
    //   117: iload #5
    //   119: ifeq -> 134
    //   122: aload #7
    //   124: aload_0
    //   125: bipush #6
    //   127: iconst_0
    //   128: invokevirtual getBoolean : (IZ)Z
    //   131: invokevirtual setAutoVerify : (Z)V
    //   134: aload_0
    //   135: invokevirtual recycle : ()V
    //   138: aload_3
    //   139: invokeinterface getDepth : ()I
    //   144: istore #9
    //   146: aload_3
    //   147: invokeinterface next : ()I
    //   152: istore #10
    //   154: iload #10
    //   156: iconst_1
    //   157: if_icmpeq -> 487
    //   160: iload #10
    //   162: iconst_3
    //   163: if_icmpne -> 183
    //   166: aload_3
    //   167: invokeinterface getDepth : ()I
    //   172: iload #9
    //   174: if_icmple -> 180
    //   177: goto -> 183
    //   180: goto -> 487
    //   183: iload #10
    //   185: iconst_2
    //   186: if_icmpeq -> 192
    //   189: goto -> 146
    //   192: aload_3
    //   193: invokeinterface getName : ()Ljava/lang/String;
    //   198: astore_0
    //   199: iconst_m1
    //   200: istore #10
    //   202: aload_0
    //   203: invokevirtual hashCode : ()I
    //   206: istore #11
    //   208: iload #11
    //   210: ldc -1422950858
    //   212: if_icmpeq -> 262
    //   215: iload #11
    //   217: ldc 3076010
    //   219: if_icmpeq -> 247
    //   222: iload #11
    //   224: ldc 50511102
    //   226: if_icmpeq -> 232
    //   229: goto -> 274
    //   232: aload_0
    //   233: ldc 'category'
    //   235: invokevirtual equals : (Ljava/lang/Object;)Z
    //   238: ifeq -> 229
    //   241: iconst_1
    //   242: istore #10
    //   244: goto -> 274
    //   247: aload_0
    //   248: ldc 'data'
    //   250: invokevirtual equals : (Ljava/lang/Object;)Z
    //   253: ifeq -> 229
    //   256: iconst_2
    //   257: istore #10
    //   259: goto -> 274
    //   262: aload_0
    //   263: ldc 'action'
    //   265: invokevirtual equals : (Ljava/lang/Object;)Z
    //   268: ifeq -> 229
    //   271: iconst_0
    //   272: istore #10
    //   274: iload #10
    //   276: ifeq -> 394
    //   279: iload #10
    //   281: iconst_1
    //   282: if_icmpeq -> 319
    //   285: iload #10
    //   287: iconst_2
    //   288: if_icmpeq -> 304
    //   291: ldc '<intent-filter>'
    //   293: aload_1
    //   294: aload_3
    //   295: aload #6
    //   297: invokestatic unknownTag : (Ljava/lang/String;Landroid/content/pm/parsing/ParsingPackage;Landroid/content/res/XmlResourceParser;Landroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   300: astore_0
    //   301: goto -> 466
    //   304: aload #7
    //   306: aload_2
    //   307: aload_3
    //   308: iload #4
    //   310: aload #6
    //   312: invokestatic parseData : (Landroid/content/pm/parsing/component/ParsedIntentInfo;Landroid/content/res/Resources;Landroid/content/res/XmlResourceParser;ZLandroid/content/pm/parsing/result/ParseInput;)Landroid/content/pm/parsing/result/ParseResult;
    //   315: astore_0
    //   316: goto -> 466
    //   319: aload_3
    //   320: ldc 'http://schemas.android.com/apk/res/android'
    //   322: ldc 'name'
    //   324: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   329: astore_0
    //   330: aload_0
    //   331: ifnonnull -> 347
    //   334: aload #6
    //   336: ldc 'No value supplied for <android:name>'
    //   338: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   343: astore_0
    //   344: goto -> 466
    //   347: aload_0
    //   348: invokevirtual isEmpty : ()Z
    //   351: ifeq -> 376
    //   354: aload #7
    //   356: aload_0
    //   357: invokevirtual addCategory : (Ljava/lang/String;)V
    //   360: aload #6
    //   362: ldc 'No value supplied for <android:name>'
    //   364: ldc2_w 151163173
    //   367: invokeinterface deferError : (Ljava/lang/String;J)Landroid/content/pm/parsing/result/ParseResult;
    //   372: astore_0
    //   373: goto -> 466
    //   376: aload #7
    //   378: aload_0
    //   379: invokevirtual addCategory : (Ljava/lang/String;)V
    //   382: aload #6
    //   384: aconst_null
    //   385: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   390: astore_0
    //   391: goto -> 466
    //   394: aload_3
    //   395: ldc 'http://schemas.android.com/apk/res/android'
    //   397: ldc 'name'
    //   399: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   404: astore_0
    //   405: aload_0
    //   406: ifnonnull -> 422
    //   409: aload #6
    //   411: ldc 'No value supplied for <android:name>'
    //   413: invokeinterface error : (Ljava/lang/String;)Landroid/content/pm/parsing/result/ParseResult;
    //   418: astore_0
    //   419: goto -> 466
    //   422: aload_0
    //   423: invokevirtual isEmpty : ()Z
    //   426: ifeq -> 451
    //   429: aload #7
    //   431: aload_0
    //   432: invokevirtual addAction : (Ljava/lang/String;)V
    //   435: aload #6
    //   437: ldc 'No value supplied for <android:name>'
    //   439: ldc2_w 151163173
    //   442: invokeinterface deferError : (Ljava/lang/String;J)Landroid/content/pm/parsing/result/ParseResult;
    //   447: astore_0
    //   448: goto -> 466
    //   451: aload #7
    //   453: aload_0
    //   454: invokevirtual addAction : (Ljava/lang/String;)V
    //   457: aload #6
    //   459: aconst_null
    //   460: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   465: astore_0
    //   466: aload_0
    //   467: invokeinterface isError : ()Z
    //   472: ifeq -> 484
    //   475: aload #6
    //   477: aload_0
    //   478: invokeinterface error : (Landroid/content/pm/parsing/result/ParseResult;)Landroid/content/pm/parsing/result/ParseResult;
    //   483: areturn
    //   484: goto -> 146
    //   487: aload #7
    //   489: aload #7
    //   491: ldc 'android.intent.category.DEFAULT'
    //   493: invokevirtual hasCategory : (Ljava/lang/String;)Z
    //   496: putfield hasDefault : Z
    //   499: aload #6
    //   501: aload #7
    //   503: invokeinterface success : (Ljava/lang/Object;)Landroid/content/pm/parsing/result/ParseResult;
    //   508: areturn
    //   509: astore_1
    //   510: aload_0
    //   511: invokevirtual recycle : ()V
    //   514: aload_1
    //   515: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #53	-> 0
    //   #54	-> 9
    //   #56	-> 18
    //   #57	-> 29
    //   #59	-> 40
    //   #60	-> 47
    //   #61	-> 52
    //   #62	-> 62
    //   #63	-> 70
    //   #67	-> 80
    //   #68	-> 86
    //   #72	-> 98
    //   #73	-> 106
    //   #76	-> 117
    //   #77	-> 122
    //   #82	-> 134
    //   #83	-> 138
    //   #84	-> 138
    //   #86	-> 146
    //   #88	-> 166
    //   #89	-> 183
    //   #90	-> 189
    //   #94	-> 192
    //   #95	-> 199
    //   #132	-> 291
    //   #129	-> 304
    //   #130	-> 316
    //   #113	-> 319
    //   #115	-> 330
    //   #116	-> 334
    //   #117	-> 347
    //   #118	-> 354
    //   #120	-> 360
    //   #123	-> 376
    //   #124	-> 382
    //   #126	-> 391
    //   #97	-> 394
    //   #99	-> 405
    //   #100	-> 409
    //   #101	-> 422
    //   #102	-> 429
    //   #104	-> 435
    //   #107	-> 451
    //   #108	-> 457
    //   #110	-> 466
    //   #136	-> 466
    //   #137	-> 475
    //   #139	-> 484
    //   #86	-> 487
    //   #141	-> 487
    //   #158	-> 499
    //   #82	-> 509
    //   #83	-> 514
    // Exception table:
    //   from	to	target	type
    //   18	29	509	finally
    //   29	40	509	finally
    //   40	47	509	finally
    //   52	62	509	finally
    //   62	70	509	finally
    //   70	80	509	finally
    //   80	86	509	finally
    //   86	98	509	finally
    //   98	106	509	finally
    //   106	117	509	finally
    //   122	134	509	finally
  }
  
  private static ParseResult<ParsedIntentInfo> parseData(ParsedIntentInfo paramParsedIntentInfo, Resources paramResources, XmlResourceParser paramXmlResourceParser, boolean paramBoolean, ParseInput paramParseInput) {
    TypedArray typedArray = paramResources.obtainAttributes(paramXmlResourceParser, R.styleable.AndroidManifestData);
    try {
      String str1 = typedArray.getNonConfigurationString(0, 0);
      if (str1 != null)
        try {
          paramParsedIntentInfo.addDataType(str1);
        } catch (android.content.IntentFilter.MalformedMimeTypeException malformedMimeTypeException) {
          parseResult = paramParseInput.error(malformedMimeTypeException.toString());
          return (ParseResult)parseResult;
        }  
      str1 = typedArray.getNonConfigurationString(10, 0);
      if (str1 != null)
        parseResult.addMimeGroup(str1); 
      str1 = typedArray.getNonConfigurationString(1, 0);
      if (str1 != null)
        parseResult.addDataScheme(str1); 
      str1 = typedArray.getNonConfigurationString(7, 0);
      if (str1 != null)
        parseResult.addDataSchemeSpecificPart(str1, 0); 
      str1 = typedArray.getNonConfigurationString(8, 0);
      if (str1 != null)
        parseResult.addDataSchemeSpecificPart(str1, 1); 
      str1 = typedArray.getNonConfigurationString(9, 0);
      if (str1 != null) {
        if (!paramBoolean) {
          parseResult = paramParseInput.error("sspPattern not allowed here; ssp must be literal");
          return (ParseResult)parseResult;
        } 
        parseResult.addDataSchemeSpecificPart(str1, 2);
      } 
      String str2 = typedArray.getNonConfigurationString(2, 0);
      str1 = typedArray.getNonConfigurationString(3, 0);
      if (str2 != null)
        parseResult.addDataAuthority(str2, str1); 
      str1 = typedArray.getNonConfigurationString(4, 0);
      if (str1 != null)
        parseResult.addDataPath(str1, 0); 
      str1 = typedArray.getNonConfigurationString(5, 0);
      if (str1 != null)
        parseResult.addDataPath(str1, 1); 
      str1 = typedArray.getNonConfigurationString(6, 0);
      if (str1 != null) {
        if (!paramBoolean) {
          parseResult = paramParseInput.error("pathPattern not allowed here; path must be literal");
          return (ParseResult)parseResult;
        } 
        parseResult.addDataPath(str1, 2);
      } 
      str1 = typedArray.getNonConfigurationString(11, 0);
      if (str1 != null) {
        if (!paramBoolean) {
          parseResult = paramParseInput.error("pathAdvancedPattern not allowed here; path must be literal");
          return (ParseResult)parseResult;
        } 
        parseResult.addDataPath(str1, 3);
      } 
      ParseResult<?> parseResult = paramParseInput.success(null);
      return (ParseResult)parseResult;
    } finally {
      typedArray.recycle();
    } 
  }
}
