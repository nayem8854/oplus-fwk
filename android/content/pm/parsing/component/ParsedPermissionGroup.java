package android.content.pm.parsing.component;

import android.os.Parcel;
import android.os.Parcelable;

public class ParsedPermissionGroup extends ParsedComponent {
  public void setPriority(int paramInt) {
    this.priority = paramInt;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("PermissionGroup{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(" ");
    stringBuilder.append(getName());
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.requestDetailResourceId);
    paramParcel.writeInt(this.backgroundRequestResourceId);
    paramParcel.writeInt(this.backgroundRequestDetailResourceId);
    paramParcel.writeInt(this.requestRes);
    paramParcel.writeInt(this.priority);
  }
  
  public ParsedPermissionGroup() {}
  
  protected ParsedPermissionGroup(Parcel paramParcel) {
    super(paramParcel);
    this.requestDetailResourceId = paramParcel.readInt();
    this.backgroundRequestResourceId = paramParcel.readInt();
    this.backgroundRequestDetailResourceId = paramParcel.readInt();
    this.requestRes = paramParcel.readInt();
    this.priority = paramParcel.readInt();
  }
  
  public static final Parcelable.Creator<ParsedPermissionGroup> CREATOR = (Parcelable.Creator<ParsedPermissionGroup>)new Object();
  
  int backgroundRequestDetailResourceId;
  
  int backgroundRequestResourceId;
  
  int priority;
  
  int requestDetailResourceId;
  
  int requestRes;
  
  public int getRequestDetailResourceId() {
    return this.requestDetailResourceId;
  }
  
  public int getBackgroundRequestResourceId() {
    return this.backgroundRequestResourceId;
  }
  
  public int getBackgroundRequestDetailResourceId() {
    return this.backgroundRequestDetailResourceId;
  }
  
  public int getRequestRes() {
    return this.requestRes;
  }
  
  public int getPriority() {
    return this.priority;
  }
}
