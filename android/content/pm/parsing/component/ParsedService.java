package android.content.pm.parsing.component;

import android.content.ComponentName;
import android.content.pm.parsing.ParsingPackageImpl;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class ParsedService extends ParsedMainComponent {
  public ParsedService(ParsedService paramParsedService) {
    super(paramParsedService);
    this.foregroundServiceType = paramParsedService.foregroundServiceType;
    this.permission = paramParsedService.permission;
  }
  
  public ParsedMainComponent setPermission(String paramString) {
    if (TextUtils.isEmpty(paramString)) {
      paramString = null;
    } else {
      paramString = paramString.intern();
    } 
    this.permission = paramString;
    return this;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("Service{");
    stringBuilder.append(Integer.toHexString(System.identityHashCode(this)));
    stringBuilder.append(' ');
    ComponentName.appendShortString(stringBuilder, getPackageName(), getName());
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    super.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.foregroundServiceType);
    ParsingPackageImpl.sForInternedString.parcel(this.permission, paramParcel, paramInt);
  }
  
  public ParsedService() {}
  
  protected ParsedService(Parcel paramParcel) {
    super(paramParcel);
    this.foregroundServiceType = paramParcel.readInt();
    this.permission = ParsingPackageImpl.sForInternedString.unparcel(paramParcel);
  }
  
  public static final Parcelable.Creator<ParsedService> CREATOR = (Parcelable.Creator<ParsedService>)new Object();
  
  int foregroundServiceType;
  
  private String permission;
  
  public int getForegroundServiceType() {
    return this.foregroundServiceType;
  }
  
  public String getPermission() {
    return this.permission;
  }
}
