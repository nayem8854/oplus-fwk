package android.content.pm.parsing.result;

public interface ParseResult<ResultType> {
  int getErrorCode();
  
  String getErrorMessage();
  
  Exception getException();
  
  ResultType getResult();
  
  boolean isError();
  
  boolean isSuccess();
}
