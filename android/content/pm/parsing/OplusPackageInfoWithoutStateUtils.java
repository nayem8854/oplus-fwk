package android.content.pm.parsing;

import android.content.pm.ActivityInfo;
import android.content.pm.OplusBaseActivityInfo;
import android.content.pm.parsing.component.OplusBaseParsedMainComponent;
import android.content.pm.parsing.component.ParsedActivity;
import com.oplus.util.OplusTypeCastingHelper;

public class OplusPackageInfoWithoutStateUtils {
  public static void generateActivityInfoUnchecked(ActivityInfo paramActivityInfo, ParsedActivity paramParsedActivity) {
    OplusBaseActivityInfo oplusBaseActivityInfo = (OplusBaseActivityInfo)OplusTypeCastingHelper.typeCasting(OplusBaseActivityInfo.class, paramActivityInfo);
    OplusBaseParsedMainComponent oplusBaseParsedMainComponent = (OplusBaseParsedMainComponent)OplusTypeCastingHelper.typeCasting(OplusBaseParsedMainComponent.class, paramParsedActivity);
    if (oplusBaseActivityInfo == null || oplusBaseParsedMainComponent == null)
      return; 
    oplusBaseActivityInfo.oplusFlags = oplusBaseParsedMainComponent.oplusFlags;
  }
}
