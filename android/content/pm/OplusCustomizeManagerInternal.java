package android.content.pm;

import java.util.List;

public abstract class OplusCustomizeManagerInternal {
  public abstract List<String> getInstallSourceList();
  
  public abstract boolean isAppInstallAllowed(String paramString);
  
  public abstract boolean isInstallSourceEnable();
  
  public abstract void sendBroadcastForArmy();
}
