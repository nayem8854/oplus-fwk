package android.content.pm;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.os.Environment;
import android.os.Handler;
import android.os.UserHandle;
import android.os.UserManager;
import android.util.AtomicFile;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.Xml;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.FastXmlSerializer;
import com.google.android.collect.Maps;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import libcore.io.IoUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public abstract class RegisteredServicesCache<V> {
  private static final boolean DEBUG = false;
  
  protected static final String REGISTERED_SERVICES_DIR = "registered_services";
  
  private static final String TAG = "PackageManager";
  
  private final String mAttributesName;
  
  public final Context mContext;
  
  private final BroadcastReceiver mExternalReceiver;
  
  private Handler mHandler;
  
  private final String mInterfaceName;
  
  private RegisteredServicesCacheListener<V> mListener;
  
  private final String mMetaDataName;
  
  private final BroadcastReceiver mPackageReceiver;
  
  private final XmlSerializerAndParser<V> mSerializerAndParser;
  
  protected final Object mServicesLock = new Object();
  
  private final BroadcastReceiver mUserRemovedReceiver;
  
  private final SparseArray<UserServices<V>> mUserServices = new SparseArray(2);
  
  private static class UserServices<V> {
    private UserServices() {}
    
    final Map<V, Integer> persistentServices = Maps.newHashMap();
    
    Map<V, RegisteredServicesCache.ServiceInfo<V>> services = null;
    
    boolean mPersistentServicesFileDidNotExist = true;
    
    boolean mBindInstantServiceAllowed = false;
  }
  
  private UserServices<V> findOrCreateUserLocked(int paramInt) {
    return findOrCreateUserLocked(paramInt, true);
  }
  
  private UserServices<V> findOrCreateUserLocked(int paramInt, boolean paramBoolean) {
    UserServices<V> userServices1 = (UserServices)this.mUserServices.get(paramInt);
    UserServices<V> userServices2 = userServices1;
    if (userServices1 == null) {
      UserServices<V> userServices = new UserServices();
      this.mUserServices.put(paramInt, userServices);
      userServices2 = userServices;
      if (paramBoolean) {
        userServices2 = userServices;
        if (this.mSerializerAndParser != null) {
          UserInfo userInfo = getUser(paramInt);
          userServices2 = userServices;
          if (userInfo != null) {
            AtomicFile atomicFile = createFileForUser(userInfo.id);
            userServices2 = userServices;
            if (atomicFile.getBaseFile().exists()) {
              userServices2 = null;
              userServices1 = null;
              try {
                FileInputStream fileInputStream3 = atomicFile.openRead();
                FileInputStream fileInputStream1 = fileInputStream3, fileInputStream2 = fileInputStream3;
                readPersistentServicesLocked(fileInputStream3);
                fileInputStream2 = fileInputStream3;
                IoUtils.closeQuietly(fileInputStream2);
                userServices2 = userServices;
              } catch (Exception exception) {
                userServices1 = userServices2;
                StringBuilder stringBuilder = new StringBuilder();
                userServices1 = userServices2;
                this();
                userServices1 = userServices2;
                stringBuilder.append("Error reading persistent services for user ");
                userServices1 = userServices2;
                stringBuilder.append(userInfo.id);
                userServices1 = userServices2;
                Log.w("PackageManager", stringBuilder.toString(), exception);
                IoUtils.closeQuietly((AutoCloseable)userServices2);
                userServices2 = userServices;
              } finally {}
            } 
          } 
        } 
      } 
    } 
    return userServices2;
  }
  
  private void handlePackageEvent(Intent paramIntent, int paramInt) {
    int i;
    String str = paramIntent.getAction();
    if ("android.intent.action.PACKAGE_REMOVED".equals(str) || 
      "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(str)) {
      i = 1;
    } else {
      i = 0;
    } 
    boolean bool = paramIntent.getBooleanExtra("android.intent.extra.REPLACING", false);
    if (!i || !bool) {
      int[] arrayOfInt1, arrayOfInt2 = null;
      if ("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE".equals(str) || 
        "android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE".equals(str)) {
        arrayOfInt1 = paramIntent.getIntArrayExtra("android.intent.extra.changed_uid_list");
      } else {
        i = arrayOfInt1.getIntExtra("android.intent.extra.UID", -1);
        arrayOfInt1 = arrayOfInt2;
        if (i > 0)
          arrayOfInt1 = new int[] { i }; 
      } 
      generateServicesMap(arrayOfInt1, paramInt);
    } 
  }
  
  public RegisteredServicesCache(Context paramContext, String paramString1, String paramString2, String paramString3, XmlSerializerAndParser<V> paramXmlSerializerAndParser) {
    this.mPackageReceiver = (BroadcastReceiver)new Object(this);
    this.mExternalReceiver = (BroadcastReceiver)new Object(this);
    this.mUserRemovedReceiver = (BroadcastReceiver)new Object(this);
    this.mContext = paramContext;
    this.mInterfaceName = paramString1;
    this.mMetaDataName = paramString2;
    this.mAttributesName = paramString3;
    this.mSerializerAndParser = paramXmlSerializerAndParser;
    migrateIfNecessaryLocked();
    IntentFilter intentFilter1 = new IntentFilter();
    intentFilter1.addAction("android.intent.action.PACKAGE_ADDED");
    intentFilter1.addAction("android.intent.action.PACKAGE_CHANGED");
    intentFilter1.addAction("android.intent.action.PACKAGE_REMOVED");
    intentFilter1.addDataScheme("package");
    this.mContext.registerReceiverAsUser(this.mPackageReceiver, UserHandle.ALL, intentFilter1, null, null);
    intentFilter1 = new IntentFilter();
    intentFilter1.addAction("android.intent.action.EXTERNAL_APPLICATIONS_AVAILABLE");
    intentFilter1.addAction("android.intent.action.EXTERNAL_APPLICATIONS_UNAVAILABLE");
    this.mContext.registerReceiver(this.mExternalReceiver, intentFilter1);
    IntentFilter intentFilter2 = new IntentFilter();
    intentFilter1.addAction("android.intent.action.USER_REMOVED");
    this.mContext.registerReceiver(this.mUserRemovedReceiver, intentFilter2);
  }
  
  public void invalidateCache(int paramInt) {
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      userServices.services = null;
      onServicesChangedLocked(paramInt);
      return;
    } 
  }
  
  public void dump(FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString, int paramInt) {
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      if (userServices.services != null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("RegisteredServicesCache: ");
        stringBuilder.append(userServices.services.size());
        stringBuilder.append(" services");
        paramPrintWriter.println(stringBuilder.toString());
        for (ServiceInfo<V> serviceInfo : userServices.services.values()) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("  ");
          stringBuilder1.append(serviceInfo);
          paramPrintWriter.println(stringBuilder1.toString());
        } 
      } else {
        paramPrintWriter.println("RegisteredServicesCache: services not loaded");
      } 
      return;
    } 
  }
  
  public RegisteredServicesCacheListener<V> getListener() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mListener : Landroid/content/pm/RegisteredServicesCacheListener;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #262	-> 0
    //   #263	-> 2
    //   #264	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public void setListener(RegisteredServicesCacheListener<V> paramRegisteredServicesCacheListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_2
    //   1: astore_3
    //   2: aload_2
    //   3: ifnonnull -> 21
    //   6: new android/os/Handler
    //   9: dup
    //   10: aload_0
    //   11: getfield mContext : Landroid/content/Context;
    //   14: invokevirtual getMainLooper : ()Landroid/os/Looper;
    //   17: invokespecial <init> : (Landroid/os/Looper;)V
    //   20: astore_3
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: aload_3
    //   25: putfield mHandler : Landroid/os/Handler;
    //   28: aload_0
    //   29: aload_1
    //   30: putfield mListener : Landroid/content/pm/RegisteredServicesCacheListener;
    //   33: aload_0
    //   34: monitorexit
    //   35: return
    //   36: astore_1
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_1
    //   40: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #268	-> 0
    //   #269	-> 6
    //   #271	-> 21
    //   #272	-> 23
    //   #273	-> 28
    //   #274	-> 33
    //   #275	-> 35
    //   #274	-> 36
    // Exception table:
    //   from	to	target	type
    //   23	28	36	finally
    //   28	33	36	finally
    //   33	35	36	finally
    //   37	39	36	finally
  }
  
  private void notifyListener(V paramV, int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mListener : Landroid/content/pm/RegisteredServicesCacheListener;
    //   6: astore #4
    //   8: aload_0
    //   9: getfield mHandler : Landroid/os/Handler;
    //   12: astore #5
    //   14: aload_0
    //   15: monitorexit
    //   16: aload #4
    //   18: ifnonnull -> 22
    //   21: return
    //   22: aload #5
    //   24: new android/content/pm/_$$Lambda$RegisteredServicesCache$lDXmLhKoG7lZpIyDOuPYOrjzDYY
    //   27: dup
    //   28: aload #4
    //   30: aload_1
    //   31: iload_2
    //   32: iload_3
    //   33: invokespecial <init> : (Landroid/content/pm/RegisteredServicesCacheListener;Ljava/lang/Object;IZ)V
    //   36: invokevirtual post : (Ljava/lang/Runnable;)Z
    //   39: pop
    //   40: return
    //   41: astore_1
    //   42: aload_0
    //   43: monitorexit
    //   44: aload_1
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #283	-> 0
    //   #284	-> 2
    //   #285	-> 8
    //   #286	-> 14
    //   #287	-> 16
    //   #288	-> 21
    //   #291	-> 22
    //   #292	-> 22
    //   #299	-> 40
    //   #286	-> 41
    // Exception table:
    //   from	to	target	type
    //   2	8	41	finally
    //   8	14	41	finally
    //   14	16	41	finally
    //   42	44	41	finally
  }
  
  public static class ServiceInfo<V> {
    public final ComponentInfo componentInfo;
    
    public final ComponentName componentName;
    
    public final V type;
    
    public final int uid;
    
    public ServiceInfo(V param1V, ComponentInfo param1ComponentInfo, ComponentName param1ComponentName) {
      byte b;
      this.type = param1V;
      this.componentInfo = param1ComponentInfo;
      this.componentName = param1ComponentName;
      if (param1ComponentInfo != null) {
        b = param1ComponentInfo.applicationInfo.uid;
      } else {
        b = -1;
      } 
      this.uid = b;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ServiceInfo: ");
      stringBuilder.append(this.type);
      stringBuilder.append(", ");
      stringBuilder.append(this.componentName);
      stringBuilder.append(", uid ");
      stringBuilder.append(this.uid);
      return stringBuilder.toString();
    }
  }
  
  public ServiceInfo<V> getServiceInfo(V paramV, int paramInt) {
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      if (userServices.services == null)
        generateServicesMap(null, paramInt); 
      return userServices.services.get(paramV);
    } 
  }
  
  public Collection<ServiceInfo<V>> getAllServices(int paramInt) {
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      if (userServices.services == null)
        generateServicesMap(null, paramInt); 
      ArrayList<?> arrayList = new ArrayList();
      Map<V, ServiceInfo<V>> map = userServices.services;
      this(map.values());
      return (Collection)Collections.unmodifiableCollection(arrayList);
    } 
  }
  
  public void updateServices(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mServicesLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: iload_1
    //   9: invokespecial findOrCreateUserLocked : (I)Landroid/content/pm/RegisteredServicesCache$UserServices;
    //   12: astore_3
    //   13: aload_3
    //   14: getfield services : Ljava/util/Map;
    //   17: ifnonnull -> 23
    //   20: aload_2
    //   21: monitorexit
    //   22: return
    //   23: new java/util/ArrayList
    //   26: astore #4
    //   28: aload #4
    //   30: aload_3
    //   31: getfield services : Ljava/util/Map;
    //   34: invokeinterface values : ()Ljava/util/Collection;
    //   39: invokespecial <init> : (Ljava/util/Collection;)V
    //   42: aload_2
    //   43: monitorexit
    //   44: aconst_null
    //   45: astore_2
    //   46: aload #4
    //   48: invokeinterface iterator : ()Ljava/util/Iterator;
    //   53: astore #5
    //   55: aload #5
    //   57: invokeinterface hasNext : ()Z
    //   62: ifeq -> 177
    //   65: aload #5
    //   67: invokeinterface next : ()Ljava/lang/Object;
    //   72: checkcast android/content/pm/RegisteredServicesCache$ServiceInfo
    //   75: astore #6
    //   77: aload #6
    //   79: getfield componentInfo : Landroid/content/pm/ComponentInfo;
    //   82: getfield applicationInfo : Landroid/content/pm/ApplicationInfo;
    //   85: getfield versionCode : I
    //   88: i2l
    //   89: lstore #7
    //   91: aload #6
    //   93: getfield componentInfo : Landroid/content/pm/ComponentInfo;
    //   96: getfield packageName : Ljava/lang/String;
    //   99: astore_3
    //   100: aconst_null
    //   101: astore #4
    //   103: aload_0
    //   104: getfield mContext : Landroid/content/Context;
    //   107: invokevirtual getPackageManager : ()Landroid/content/pm/PackageManager;
    //   110: aload_3
    //   111: iconst_0
    //   112: iload_1
    //   113: invokevirtual getApplicationInfoAsUser : (Ljava/lang/String;II)Landroid/content/pm/ApplicationInfo;
    //   116: astore_3
    //   117: aload_3
    //   118: astore #4
    //   120: goto -> 124
    //   123: astore_3
    //   124: aload #4
    //   126: ifnull -> 143
    //   129: aload_2
    //   130: astore_3
    //   131: aload #4
    //   133: getfield versionCode : I
    //   136: i2l
    //   137: lload #7
    //   139: lcmp
    //   140: ifeq -> 172
    //   143: aload_2
    //   144: astore #4
    //   146: aload_2
    //   147: ifnonnull -> 159
    //   150: new android/util/IntArray
    //   153: dup
    //   154: invokespecial <init> : ()V
    //   157: astore #4
    //   159: aload #4
    //   161: aload #6
    //   163: getfield uid : I
    //   166: invokevirtual add : (I)V
    //   169: aload #4
    //   171: astore_3
    //   172: aload_3
    //   173: astore_2
    //   174: goto -> 55
    //   177: aload_2
    //   178: ifnull -> 199
    //   181: aload_2
    //   182: invokevirtual size : ()I
    //   185: ifle -> 199
    //   188: aload_2
    //   189: invokevirtual toArray : ()[I
    //   192: astore_2
    //   193: aload_0
    //   194: aload_2
    //   195: iload_1
    //   196: invokespecial generateServicesMap : ([II)V
    //   199: return
    //   200: astore #4
    //   202: aload_2
    //   203: monitorexit
    //   204: aload #4
    //   206: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #365	-> 0
    //   #366	-> 7
    //   #368	-> 13
    //   #369	-> 20
    //   #371	-> 23
    //   #372	-> 42
    //   #373	-> 44
    //   #374	-> 46
    //   #375	-> 77
    //   #376	-> 91
    //   #377	-> 100
    //   #379	-> 103
    //   #382	-> 120
    //   #380	-> 123
    //   #384	-> 124
    //   #389	-> 143
    //   #390	-> 150
    //   #392	-> 159
    //   #394	-> 172
    //   #395	-> 177
    //   #396	-> 188
    //   #397	-> 193
    //   #399	-> 199
    //   #372	-> 200
    // Exception table:
    //   from	to	target	type
    //   7	13	200	finally
    //   13	20	200	finally
    //   20	22	200	finally
    //   23	42	200	finally
    //   42	44	200	finally
    //   103	117	123	android/content/pm/PackageManager$NameNotFoundException
    //   202	204	200	finally
  }
  
  public boolean getBindInstantServiceAllowed(int paramInt) {
    this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_BIND_INSTANT_SERVICE", "getBindInstantServiceAllowed");
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      return userServices.mBindInstantServiceAllowed;
    } 
  }
  
  public void setBindInstantServiceAllowed(int paramInt, boolean paramBoolean) {
    this.mContext.enforceCallingOrSelfPermission("android.permission.MANAGE_BIND_INSTANT_SERVICE", "setBindInstantServiceAllowed");
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      userServices.mBindInstantServiceAllowed = paramBoolean;
      return;
    } 
  }
  
  protected boolean inSystemImage(int paramInt) {
    String[] arrayOfString = this.mContext.getPackageManager().getPackagesForUid(paramInt);
    if (arrayOfString != null)
      for (int i = arrayOfString.length; paramInt < i; ) {
        String str = arrayOfString[paramInt];
        try {
          Context context = this.mContext;
          PackageInfo packageInfo = context.getPackageManager().getPackageInfo(str, 0);
          int j = packageInfo.applicationInfo.flags;
          if ((j & 0x1) != 0)
            return true; 
          paramInt++;
        } catch (NameNotFoundException nameNotFoundException) {
          return false;
        } 
      }  
    return false;
  }
  
  protected List<ResolveInfo> queryIntentServices(int paramInt) {
    PackageManager packageManager = this.mContext.getPackageManager();
    int i = 786560;
    synchronized (this.mServicesLock) {
      UserServices<V> userServices = findOrCreateUserLocked(paramInt);
      if (userServices.mBindInstantServiceAllowed)
        i = 0xC0080 | 0x800000; 
      return packageManager.queryIntentServicesAsUser(new Intent(this.mInterfaceName), i, paramInt);
    } 
  }
  
  private void generateServicesMap(int[] paramArrayOfint, int paramInt) {
    // Byte code:
    //   0: new java/util/ArrayList
    //   3: dup
    //   4: invokespecial <init> : ()V
    //   7: astore_3
    //   8: aload_0
    //   9: iload_2
    //   10: invokevirtual queryIntentServices : (I)Ljava/util/List;
    //   13: astore #4
    //   15: aload #4
    //   17: invokeinterface iterator : ()Ljava/util/Iterator;
    //   22: astore #5
    //   24: aload #5
    //   26: invokeinterface hasNext : ()Z
    //   31: ifeq -> 160
    //   34: aload #5
    //   36: invokeinterface next : ()Ljava/lang/Object;
    //   41: checkcast android/content/pm/ResolveInfo
    //   44: astore #4
    //   46: aload_0
    //   47: aload #4
    //   49: invokevirtual parseServiceInfo : (Landroid/content/pm/ResolveInfo;)Landroid/content/pm/RegisteredServicesCache$ServiceInfo;
    //   52: astore #6
    //   54: aload #6
    //   56: ifnonnull -> 103
    //   59: new java/lang/StringBuilder
    //   62: astore #6
    //   64: aload #6
    //   66: invokespecial <init> : ()V
    //   69: aload #6
    //   71: ldc_w 'Unable to load service info '
    //   74: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   77: pop
    //   78: aload #6
    //   80: aload #4
    //   82: invokevirtual toString : ()Ljava/lang/String;
    //   85: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   88: pop
    //   89: ldc 'PackageManager'
    //   91: aload #6
    //   93: invokevirtual toString : ()Ljava/lang/String;
    //   96: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   99: pop
    //   100: goto -> 24
    //   103: aload_3
    //   104: aload #6
    //   106: invokevirtual add : (Ljava/lang/Object;)Z
    //   109: pop
    //   110: goto -> 157
    //   113: astore #6
    //   115: new java/lang/StringBuilder
    //   118: dup
    //   119: invokespecial <init> : ()V
    //   122: astore #7
    //   124: aload #7
    //   126: ldc_w 'Unable to load service info '
    //   129: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   132: pop
    //   133: aload #7
    //   135: aload #4
    //   137: invokevirtual toString : ()Ljava/lang/String;
    //   140: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   143: pop
    //   144: ldc 'PackageManager'
    //   146: aload #7
    //   148: invokevirtual toString : ()Ljava/lang/String;
    //   151: aload #6
    //   153: invokestatic w : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   156: pop
    //   157: goto -> 24
    //   160: aload_0
    //   161: getfield mServicesLock : Ljava/lang/Object;
    //   164: astore #4
    //   166: aload #4
    //   168: monitorenter
    //   169: aload_0
    //   170: iload_2
    //   171: invokespecial findOrCreateUserLocked : (I)Landroid/content/pm/RegisteredServicesCache$UserServices;
    //   174: astore #5
    //   176: aload #5
    //   178: getfield services : Ljava/util/Map;
    //   181: ifnonnull -> 190
    //   184: iconst_1
    //   185: istore #8
    //   187: goto -> 193
    //   190: iconst_0
    //   191: istore #8
    //   193: iload #8
    //   195: ifeq -> 206
    //   198: aload #5
    //   200: invokestatic newHashMap : ()Ljava/util/HashMap;
    //   203: putfield services : Ljava/util/Map;
    //   206: new java/lang/StringBuilder
    //   209: invokespecial <init> : ()V
    //   212: iconst_0
    //   213: istore #9
    //   215: aload_3
    //   216: invokevirtual iterator : ()Ljava/util/Iterator;
    //   219: astore #10
    //   221: aload #10
    //   223: invokeinterface hasNext : ()Z
    //   228: ifeq -> 475
    //   231: aload #10
    //   233: invokeinterface next : ()Ljava/lang/Object;
    //   238: checkcast android/content/pm/RegisteredServicesCache$ServiceInfo
    //   241: astore #7
    //   243: aload #5
    //   245: getfield persistentServices : Ljava/util/Map;
    //   248: aload #7
    //   250: getfield type : Ljava/lang/Object;
    //   253: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   258: checkcast java/lang/Integer
    //   261: astore #11
    //   263: aload #11
    //   265: ifnonnull -> 348
    //   268: iconst_1
    //   269: istore #12
    //   271: aload #5
    //   273: getfield services : Ljava/util/Map;
    //   276: aload #7
    //   278: getfield type : Ljava/lang/Object;
    //   281: aload #7
    //   283: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   288: pop
    //   289: aload #5
    //   291: getfield persistentServices : Ljava/util/Map;
    //   294: aload #7
    //   296: getfield type : Ljava/lang/Object;
    //   299: aload #7
    //   301: getfield uid : I
    //   304: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   307: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   312: pop
    //   313: aload #5
    //   315: getfield mPersistentServicesFileDidNotExist : Z
    //   318: ifeq -> 330
    //   321: iload #12
    //   323: istore #9
    //   325: iload #8
    //   327: ifne -> 472
    //   330: aload_0
    //   331: aload #7
    //   333: getfield type : Ljava/lang/Object;
    //   336: iload_2
    //   337: iconst_0
    //   338: invokespecial notifyListener : (Ljava/lang/Object;IZ)V
    //   341: iload #12
    //   343: istore #9
    //   345: goto -> 472
    //   348: aload #11
    //   350: invokevirtual intValue : ()I
    //   353: aload #7
    //   355: getfield uid : I
    //   358: if_icmpne -> 382
    //   361: aload #5
    //   363: getfield services : Ljava/util/Map;
    //   366: aload #7
    //   368: getfield type : Ljava/lang/Object;
    //   371: aload #7
    //   373: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   378: pop
    //   379: goto -> 472
    //   382: aload_0
    //   383: aload #7
    //   385: getfield uid : I
    //   388: invokevirtual inSystemImage : (I)Z
    //   391: ifne -> 416
    //   394: aload #7
    //   396: getfield type : Ljava/lang/Object;
    //   399: astore #6
    //   401: aload_0
    //   402: aload_3
    //   403: aload #6
    //   405: aload #11
    //   407: invokevirtual intValue : ()I
    //   410: invokespecial containsTypeAndUid : (Ljava/util/ArrayList;Ljava/lang/Object;I)Z
    //   413: ifne -> 472
    //   416: aload #5
    //   418: getfield services : Ljava/util/Map;
    //   421: aload #7
    //   423: getfield type : Ljava/lang/Object;
    //   426: aload #7
    //   428: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   433: pop
    //   434: aload #5
    //   436: getfield persistentServices : Ljava/util/Map;
    //   439: aload #7
    //   441: getfield type : Ljava/lang/Object;
    //   444: aload #7
    //   446: getfield uid : I
    //   449: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   452: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   457: pop
    //   458: aload_0
    //   459: aload #7
    //   461: getfield type : Ljava/lang/Object;
    //   464: iload_2
    //   465: iconst_0
    //   466: invokespecial notifyListener : (Ljava/lang/Object;IZ)V
    //   469: iconst_1
    //   470: istore #9
    //   472: goto -> 221
    //   475: invokestatic newArrayList : ()Ljava/util/ArrayList;
    //   478: astore #7
    //   480: aload #5
    //   482: getfield persistentServices : Ljava/util/Map;
    //   485: invokeinterface keySet : ()Ljava/util/Set;
    //   490: invokeinterface iterator : ()Ljava/util/Iterator;
    //   495: astore #10
    //   497: aload #10
    //   499: invokeinterface hasNext : ()Z
    //   504: ifeq -> 574
    //   507: aload #10
    //   509: invokeinterface next : ()Ljava/lang/Object;
    //   514: astore #11
    //   516: aload_0
    //   517: aload_3
    //   518: aload #11
    //   520: invokespecial containsType : (Ljava/util/ArrayList;Ljava/lang/Object;)Z
    //   523: ifne -> 571
    //   526: aload #5
    //   528: getfield persistentServices : Ljava/util/Map;
    //   531: astore #6
    //   533: aload #6
    //   535: aload #11
    //   537: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   542: checkcast java/lang/Integer
    //   545: invokevirtual intValue : ()I
    //   548: istore #8
    //   550: aload_0
    //   551: aload_1
    //   552: iload #8
    //   554: invokespecial containsUid : ([II)Z
    //   557: ifeq -> 571
    //   560: aload #7
    //   562: aload #11
    //   564: invokevirtual add : (Ljava/lang/Object;)Z
    //   567: pop
    //   568: goto -> 571
    //   571: goto -> 497
    //   574: aload #7
    //   576: invokevirtual iterator : ()Ljava/util/Iterator;
    //   579: astore_3
    //   580: aload_3
    //   581: invokeinterface hasNext : ()Z
    //   586: ifeq -> 633
    //   589: aload_3
    //   590: invokeinterface next : ()Ljava/lang/Object;
    //   595: astore_1
    //   596: iconst_1
    //   597: istore #9
    //   599: aload #5
    //   601: getfield persistentServices : Ljava/util/Map;
    //   604: aload_1
    //   605: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   610: pop
    //   611: aload #5
    //   613: getfield services : Ljava/util/Map;
    //   616: aload_1
    //   617: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
    //   622: pop
    //   623: aload_0
    //   624: aload_1
    //   625: iload_2
    //   626: iconst_1
    //   627: invokespecial notifyListener : (Ljava/lang/Object;IZ)V
    //   630: goto -> 580
    //   633: iload #9
    //   635: ifeq -> 650
    //   638: aload_0
    //   639: iload_2
    //   640: invokevirtual onServicesChangedLocked : (I)V
    //   643: aload_0
    //   644: aload #5
    //   646: iload_2
    //   647: invokespecial writePersistentServicesLocked : (Landroid/content/pm/RegisteredServicesCache$UserServices;I)V
    //   650: aload #4
    //   652: monitorexit
    //   653: return
    //   654: astore_1
    //   655: aload #4
    //   657: monitorexit
    //   658: aload_1
    //   659: athrow
    //   660: astore_1
    //   661: goto -> 655
    // Line number table:
    //   Java source line number -> byte code offset
    //   #476	-> 0
    //   #477	-> 8
    //   #478	-> 15
    //   #480	-> 46
    //   #481	-> 54
    //   #482	-> 59
    //   #483	-> 100
    //   #485	-> 103
    //   #488	-> 110
    //   #486	-> 113
    //   #487	-> 115
    //   #489	-> 157
    //   #491	-> 160
    //   #492	-> 169
    //   #493	-> 176
    //   #494	-> 193
    //   #495	-> 198
    //   #498	-> 206
    //   #499	-> 212
    //   #500	-> 215
    //   #510	-> 243
    //   #511	-> 263
    //   #515	-> 268
    //   #516	-> 271
    //   #517	-> 289
    //   #518	-> 313
    //   #519	-> 330
    //   #521	-> 348
    //   #525	-> 361
    //   #526	-> 382
    //   #527	-> 401
    //   #537	-> 416
    //   #538	-> 416
    //   #539	-> 434
    //   #540	-> 458
    //   #548	-> 472
    //   #550	-> 475
    //   #551	-> 480
    //   #554	-> 516
    //   #555	-> 533
    //   #556	-> 560
    //   #554	-> 571
    //   #558	-> 571
    //   #559	-> 574
    //   #563	-> 596
    //   #564	-> 599
    //   #565	-> 611
    //   #566	-> 623
    //   #567	-> 630
    //   #587	-> 633
    //   #588	-> 638
    //   #589	-> 643
    //   #591	-> 650
    //   #592	-> 653
    //   #591	-> 654
    // Exception table:
    //   from	to	target	type
    //   46	54	113	org/xmlpull/v1/XmlPullParserException
    //   46	54	113	java/io/IOException
    //   59	100	113	org/xmlpull/v1/XmlPullParserException
    //   59	100	113	java/io/IOException
    //   103	110	113	org/xmlpull/v1/XmlPullParserException
    //   103	110	113	java/io/IOException
    //   169	176	654	finally
    //   176	184	654	finally
    //   198	206	654	finally
    //   206	212	654	finally
    //   215	221	654	finally
    //   221	243	654	finally
    //   243	263	654	finally
    //   271	289	654	finally
    //   289	313	654	finally
    //   313	321	654	finally
    //   330	341	654	finally
    //   348	361	654	finally
    //   361	379	654	finally
    //   382	401	654	finally
    //   401	416	654	finally
    //   416	434	654	finally
    //   434	458	654	finally
    //   458	469	654	finally
    //   475	480	654	finally
    //   480	497	654	finally
    //   497	516	654	finally
    //   516	533	654	finally
    //   533	550	654	finally
    //   550	560	660	finally
    //   560	568	660	finally
    //   574	580	660	finally
    //   580	596	660	finally
    //   599	611	660	finally
    //   611	623	660	finally
    //   623	630	660	finally
    //   638	643	660	finally
    //   643	650	660	finally
    //   650	653	660	finally
    //   655	658	660	finally
  }
  
  protected void onServicesChangedLocked(int paramInt) {}
  
  private boolean containsUid(int[] paramArrayOfint, int paramInt) {
    return (paramArrayOfint == null || ArrayUtils.contains(paramArrayOfint, paramInt));
  }
  
  private boolean containsType(ArrayList<ServiceInfo<V>> paramArrayList, V paramV) {
    byte b;
    int i;
    for (b = 0, i = paramArrayList.size(); b < i; b++) {
      if (((ServiceInfo)paramArrayList.get(b)).type.equals(paramV))
        return true; 
    } 
    return false;
  }
  
  private boolean containsTypeAndUid(ArrayList<ServiceInfo<V>> paramArrayList, V paramV, int paramInt) {
    byte b;
    int i;
    for (b = 0, i = paramArrayList.size(); b < i; b++) {
      ServiceInfo serviceInfo = paramArrayList.get(b);
      if (serviceInfo.type.equals(paramV) && serviceInfo.uid == paramInt)
        return true; 
    } 
    return false;
  }
  
  protected ServiceInfo<V> parseServiceInfo(ResolveInfo paramResolveInfo) throws XmlPullParserException, IOException {
    ServiceInfo serviceInfo = paramResolveInfo.serviceInfo;
    ComponentName componentName = new ComponentName(serviceInfo.packageName, serviceInfo.name);
    PackageManager packageManager = this.mContext.getPackageManager();
    XmlResourceParser xmlResourceParser1 = null, xmlResourceParser2 = null;
    try {
      XmlResourceParser xmlResourceParser = serviceInfo.loadXmlMetaData(packageManager, this.mMetaDataName);
      if (xmlResourceParser != null) {
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        AttributeSet attributeSet = Xml.asAttributeSet(xmlResourceParser);
        while (true) {
          xmlResourceParser2 = xmlResourceParser;
          xmlResourceParser1 = xmlResourceParser;
          int i = xmlResourceParser.next();
          if (i != 1 && i != 2)
            continue; 
          break;
        } 
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        String str = xmlResourceParser.getName();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        if (this.mAttributesName.equals(str)) {
          xmlResourceParser2 = xmlResourceParser;
          xmlResourceParser1 = xmlResourceParser;
          packageManager = (PackageManager)parseServiceAttributes(packageManager.getResourcesForApplication(serviceInfo.applicationInfo), serviceInfo.packageName, attributeSet);
          if (packageManager == null) {
            if (xmlResourceParser != null)
              xmlResourceParser.close(); 
            return null;
          } 
          xmlResourceParser2 = xmlResourceParser;
          xmlResourceParser1 = xmlResourceParser;
          ServiceInfo serviceInfo2 = paramResolveInfo.serviceInfo;
          xmlResourceParser2 = xmlResourceParser;
          xmlResourceParser1 = xmlResourceParser;
          ServiceInfo<PackageManager> serviceInfo1 = new ServiceInfo<>(packageManager, serviceInfo2, componentName);
          if (xmlResourceParser != null)
            xmlResourceParser.close(); 
          return (ServiceInfo)serviceInfo1;
        } 
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        XmlPullParserException xmlPullParserException1 = new XmlPullParserException();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        StringBuilder stringBuilder1 = new StringBuilder();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        this();
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        stringBuilder1.append("Meta-data does not start with ");
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        stringBuilder1.append(this.mAttributesName);
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        stringBuilder1.append(" tag");
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        this(stringBuilder1.toString());
        xmlResourceParser2 = xmlResourceParser;
        xmlResourceParser1 = xmlResourceParser;
        throw xmlPullParserException1;
      } 
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      XmlPullParserException xmlPullParserException = new XmlPullParserException();
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      this();
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      stringBuilder.append("No ");
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      stringBuilder.append(this.mMetaDataName);
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      stringBuilder.append(" meta-data");
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      this(stringBuilder.toString());
      xmlResourceParser2 = xmlResourceParser;
      xmlResourceParser1 = xmlResourceParser;
      throw xmlPullParserException;
    } catch (NameNotFoundException nameNotFoundException) {
      xmlResourceParser2 = xmlResourceParser1;
      XmlPullParserException xmlPullParserException = new XmlPullParserException();
      xmlResourceParser2 = xmlResourceParser1;
      StringBuilder stringBuilder = new StringBuilder();
      xmlResourceParser2 = xmlResourceParser1;
      this();
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder.append("Unable to load resources for pacakge ");
      xmlResourceParser2 = xmlResourceParser1;
      stringBuilder.append(serviceInfo.packageName);
      xmlResourceParser2 = xmlResourceParser1;
      this(stringBuilder.toString());
      xmlResourceParser2 = xmlResourceParser1;
      throw xmlPullParserException;
    } finally {}
    if (xmlResourceParser2 != null)
      xmlResourceParser2.close(); 
    throw paramResolveInfo;
  }
  
  private void readPersistentServicesLocked(InputStream paramInputStream) throws XmlPullParserException, IOException {
    XmlPullParser xmlPullParser = Xml.newPullParser();
    xmlPullParser.setInput(paramInputStream, StandardCharsets.UTF_8.name());
    int i = xmlPullParser.getEventType();
    while (i != 2 && i != 1)
      i = xmlPullParser.next(); 
    String str = xmlPullParser.getName();
    if ("services".equals(str)) {
      int j;
      i = xmlPullParser.next();
      do {
        if (i == 2 && xmlPullParser.getDepth() == 2) {
          str = xmlPullParser.getName();
          if ("service".equals(str)) {
            str = (String)this.mSerializerAndParser.createFromXml(xmlPullParser);
            if (str == null)
              break; 
            String str1 = xmlPullParser.getAttributeValue(null, "uid");
            i = Integer.parseInt(str1);
            int k = UserHandle.getUserId(i);
            UserServices<V> userServices = findOrCreateUserLocked(k, false);
            userServices.persistentServices.put((V)str, Integer.valueOf(i));
          } 
        } 
        j = xmlPullParser.next();
        i = j;
      } while (j != 1);
    } 
  }
  
  private void migrateIfNecessaryLocked() {
    if (this.mSerializerAndParser == null)
      return; 
    File file = new File(getDataDirectory(), "system");
    file = new File(file, "registered_services");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mInterfaceName);
    stringBuilder.append(".xml");
    AtomicFile atomicFile = new AtomicFile(new File(file, stringBuilder.toString()));
    boolean bool = atomicFile.getBaseFile().exists();
    if (bool) {
      stringBuilder = new StringBuilder();
      stringBuilder.append(this.mInterfaceName);
      stringBuilder.append(".xml.migrated");
      File file1 = new File(file, stringBuilder.toString());
      if (!file1.exists()) {
        FileInputStream fileInputStream;
        file = null;
        stringBuilder = null;
        try {
          FileInputStream fileInputStream2 = atomicFile.openRead();
          FileInputStream fileInputStream1 = fileInputStream2;
          fileInputStream = fileInputStream2;
          this.mUserServices.clear();
          fileInputStream1 = fileInputStream2;
          fileInputStream = fileInputStream2;
          readPersistentServicesLocked(fileInputStream2);
          fileInputStream = fileInputStream2;
          IoUtils.closeQuietly(fileInputStream);
        } catch (Exception exception) {
          FileInputStream fileInputStream1 = fileInputStream;
          Log.w("PackageManager", "Error reading persistent services, starting from scratch", exception);
          IoUtils.closeQuietly(fileInputStream);
        } finally {}
        try {
          for (UserInfo userInfo : getUsers()) {
            UserServices<V> userServices = (UserServices)this.mUserServices.get(userInfo.id);
            if (userServices != null)
              writePersistentServicesLocked(userServices, userInfo.id); 
          } 
          file1.createNewFile();
        } catch (Exception exception) {
          Log.w("PackageManager", "Migration failed", exception);
        } 
        this.mUserServices.clear();
      } 
    } 
  }
  
  private void writePersistentServicesLocked(UserServices<V> paramUserServices, int paramInt) {
    if (this.mSerializerAndParser == null)
      return; 
    AtomicFile atomicFile = createFileForUser(paramInt);
    FileOutputStream fileOutputStream = null;
    try {
      FileOutputStream fileOutputStream1 = atomicFile.startWrite();
      fileOutputStream = fileOutputStream1;
      FastXmlSerializer fastXmlSerializer = new FastXmlSerializer();
      fileOutputStream = fileOutputStream1;
      this();
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.setOutput(fileOutputStream1, StandardCharsets.UTF_8.name());
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.startDocument(null, Boolean.valueOf(true));
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.startTag(null, "services");
      fileOutputStream = fileOutputStream1;
      Iterator<Map.Entry> iterator = paramUserServices.persistentServices.entrySet().iterator();
      while (true) {
        fileOutputStream = fileOutputStream1;
        if (iterator.hasNext()) {
          fileOutputStream = fileOutputStream1;
          Map.Entry entry = iterator.next();
          fileOutputStream = fileOutputStream1;
          fastXmlSerializer.startTag(null, "service");
          fileOutputStream = fileOutputStream1;
          fastXmlSerializer.attribute(null, "uid", Integer.toString(((Integer)entry.getValue()).intValue()));
          fileOutputStream = fileOutputStream1;
          this.mSerializerAndParser.writeAsXml((V)entry.getKey(), (XmlSerializer)fastXmlSerializer);
          fileOutputStream = fileOutputStream1;
          fastXmlSerializer.endTag(null, "service");
          continue;
        } 
        break;
      } 
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.endTag(null, "services");
      fileOutputStream = fileOutputStream1;
      fastXmlSerializer.endDocument();
      fileOutputStream = fileOutputStream1;
      atomicFile.finishWrite(fileOutputStream1);
    } catch (IOException iOException) {
      Log.w("PackageManager", "Error writing accounts", iOException);
      if (fileOutputStream != null)
        atomicFile.failWrite(fileOutputStream); 
    } 
  }
  
  protected void onUserRemoved(int paramInt) {
    synchronized (this.mServicesLock) {
      this.mUserServices.remove(paramInt);
      return;
    } 
  }
  
  protected List<UserInfo> getUsers() {
    return UserManager.get(this.mContext).getUsers(true);
  }
  
  protected UserInfo getUser(int paramInt) {
    return UserManager.get(this.mContext).getUserInfo(paramInt);
  }
  
  private AtomicFile createFileForUser(int paramInt) {
    File file = getUserSystemDirectory(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("registered_services/");
    stringBuilder.append(this.mInterfaceName);
    stringBuilder.append(".xml");
    file = new File(file, stringBuilder.toString());
    return new AtomicFile(file);
  }
  
  protected File getUserSystemDirectory(int paramInt) {
    return Environment.getUserSystemDirectory(paramInt);
  }
  
  protected File getDataDirectory() {
    return Environment.getDataDirectory();
  }
  
  protected Map<V, Integer> getPersistentServices(int paramInt) {
    return (findOrCreateUserLocked(paramInt)).persistentServices;
  }
  
  public abstract V parseServiceAttributes(Resources paramResources, String paramString, AttributeSet paramAttributeSet);
}
