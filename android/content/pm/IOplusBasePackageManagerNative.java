package android.content.pm;

import oplus.app.IOplusCommonManager;

public interface IOplusBasePackageManagerNative extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.content.pm.IPackageManagerNative";
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
