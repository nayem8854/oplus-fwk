package android.content.pm;

import android.os.Parcelable;

public abstract class OplusBaseUserInfo implements Parcelable {
  public static final int FLAG_MULTI_APP = 1073741824;
  
  public static final int FLAG_MULTI_SYSTEM = 536870912;
}
