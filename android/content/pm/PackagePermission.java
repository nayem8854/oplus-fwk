package android.content.pm;

import android.os.Parcel;
import android.os.Parcelable;

public class PackagePermission implements Parcelable {
  public PackagePermission() {}
  
  protected PackagePermission(Parcel paramParcel) {
    readFromParcel(paramParcel);
  }
  
  public static final Parcelable.Creator<PackagePermission> CREATOR = new Parcelable.Creator<PackagePermission>() {
      public PackagePermission createFromParcel(Parcel param1Parcel) {
        PackagePermission packagePermission = new PackagePermission();
        packagePermission.readFromParcel(param1Parcel);
        return packagePermission;
      }
      
      public PackagePermission[] newArray(int param1Int) {
        return new PackagePermission[param1Int];
      }
    };
  
  public long mAccept;
  
  public int mId;
  
  public String mPackageName;
  
  public long mPrompt;
  
  public long mReject;
  
  public int mTrust;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeLong(this.mAccept);
    paramParcel.writeLong(this.mReject);
    paramParcel.writeLong(this.mPrompt);
    paramParcel.writeInt(this.mTrust);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mId = paramParcel.readInt();
    this.mPackageName = paramParcel.readString();
    this.mAccept = paramParcel.readLong();
    this.mReject = paramParcel.readLong();
    this.mPrompt = paramParcel.readLong();
    this.mTrust = paramParcel.readInt();
  }
  
  public PackagePermission copy() {
    PackagePermission packagePermission = new PackagePermission();
    packagePermission.mId = this.mId;
    packagePermission.mPackageName = this.mPackageName;
    packagePermission.mAccept = this.mAccept;
    packagePermission.mReject = this.mReject;
    packagePermission.mPrompt = this.mPrompt;
    packagePermission.mTrust = this.mTrust;
    return packagePermission;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("[mPackageName=");
    stringBuilder.append(this.mPackageName);
    stringBuilder.append(", mAccept=");
    stringBuilder.append(this.mAccept);
    stringBuilder.append(", mReject=");
    stringBuilder.append(this.mReject);
    stringBuilder.append(", mPrompt=");
    stringBuilder.append(this.mPrompt);
    stringBuilder.append(", mTrust=");
    stringBuilder.append(this.mTrust);
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
}
