package android.content.om;

import oplus.app.IOplusCommonManager;

public interface IOplusBaseOverlayManager extends IOplusCommonManager {
  public static final String DESCRIPTOR = "android.content.om.IOverlayManager";
  
  public static final int OPLUS_FIRST_CALL_TRANSACTION = 10001;
}
