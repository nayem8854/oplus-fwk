package android.content.om;

import android.annotation.NonNull;
import com.android.internal.util.AnnotationValidations;
import java.util.Objects;

public final class OverlayableInfo {
  public final String actor;
  
  public final String name;
  
  public OverlayableInfo(String paramString1, String paramString2) {
    this.name = paramString1;
    AnnotationValidations.validate(NonNull.class, null, paramString1);
    this.actor = paramString2;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str1 = this.name, str2 = ((OverlayableInfo)paramObject).name;
    if (Objects.equals(str1, str2)) {
      str1 = this.actor;
      paramObject = ((OverlayableInfo)paramObject).actor;
      if (Objects.equals(str1, paramObject))
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = Objects.hashCode(this.name);
    int j = Objects.hashCode(this.actor);
    return (1 * 31 + i) * 31 + j;
  }
  
  @Deprecated
  private void __metadata() {}
}
