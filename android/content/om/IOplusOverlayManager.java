package android.content.om;

import android.os.RemoteException;

public interface IOplusOverlayManager extends IOplusBaseOverlayManager {
  public static final int SET_LANGUAGE_ENABLE_TRANSACTION = 10002;
  
  void setLanguageEnable(String paramString, int paramInt) throws RemoteException;
}
