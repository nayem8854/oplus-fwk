package android.content.om;

import android.os.Parcel;
import android.os.RemoteException;

public class OplusOverlayManager extends OplusBaseOverlayManager implements IOplusOverlayManager {
  public void setLanguageEnable(String paramString, int paramInt) throws RemoteException {
    Parcel parcel1 = Parcel.obtain();
    Parcel parcel2 = Parcel.obtain();
    try {
      parcel1.writeInterfaceToken("android.content.om.IOverlayManager");
      parcel1.writeString(paramString);
      parcel1.writeInt(paramInt);
      this.mRemote.transact(10002, parcel1, parcel2, 0);
      parcel2.readException();
      return;
    } finally {
      parcel1.recycle();
      parcel2.recycle();
    } 
  }
}
