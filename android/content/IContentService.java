package android.content;

import android.accounts.Account;
import android.database.IContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import java.util.List;

public interface IContentService extends IInterface {
  void addPeriodicSync(Account paramAccount, String paramString, Bundle paramBundle, long paramLong) throws RemoteException;
  
  void addStatusChangeListener(int paramInt, ISyncStatusObserver paramISyncStatusObserver) throws RemoteException;
  
  void cancelRequest(SyncRequest paramSyncRequest) throws RemoteException;
  
  void cancelSync(Account paramAccount, String paramString, ComponentName paramComponentName) throws RemoteException;
  
  void cancelSyncAsUser(Account paramAccount, String paramString, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  Bundle getCache(String paramString, Uri paramUri, int paramInt) throws RemoteException;
  
  List<SyncInfo> getCurrentSyncs() throws RemoteException;
  
  List<SyncInfo> getCurrentSyncsAsUser(int paramInt) throws RemoteException;
  
  int getIsSyncable(Account paramAccount, String paramString) throws RemoteException;
  
  int getIsSyncableAsUser(Account paramAccount, String paramString, int paramInt) throws RemoteException;
  
  boolean getMasterSyncAutomatically() throws RemoteException;
  
  boolean getMasterSyncAutomaticallyAsUser(int paramInt) throws RemoteException;
  
  List<PeriodicSync> getPeriodicSyncs(Account paramAccount, String paramString, ComponentName paramComponentName) throws RemoteException;
  
  String[] getSyncAdapterPackagesForAuthorityAsUser(String paramString, int paramInt) throws RemoteException;
  
  SyncAdapterType[] getSyncAdapterTypes() throws RemoteException;
  
  SyncAdapterType[] getSyncAdapterTypesAsUser(int paramInt) throws RemoteException;
  
  boolean getSyncAutomatically(Account paramAccount, String paramString) throws RemoteException;
  
  boolean getSyncAutomaticallyAsUser(Account paramAccount, String paramString, int paramInt) throws RemoteException;
  
  SyncStatusInfo getSyncStatus(Account paramAccount, String paramString, ComponentName paramComponentName) throws RemoteException;
  
  SyncStatusInfo getSyncStatusAsUser(Account paramAccount, String paramString, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean isSyncActive(Account paramAccount, String paramString, ComponentName paramComponentName) throws RemoteException;
  
  boolean isSyncPending(Account paramAccount, String paramString, ComponentName paramComponentName) throws RemoteException;
  
  boolean isSyncPendingAsUser(Account paramAccount, String paramString, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  void notifyChange(Uri[] paramArrayOfUri, IContentObserver paramIContentObserver, boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, String paramString) throws RemoteException;
  
  void onDbCorruption(String paramString1, String paramString2, String paramString3) throws RemoteException;
  
  void putCache(String paramString, Uri paramUri, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void registerContentObserver(Uri paramUri, boolean paramBoolean, IContentObserver paramIContentObserver, int paramInt1, int paramInt2) throws RemoteException;
  
  void removePeriodicSync(Account paramAccount, String paramString, Bundle paramBundle) throws RemoteException;
  
  void removeStatusChangeListener(ISyncStatusObserver paramISyncStatusObserver) throws RemoteException;
  
  void requestSync(Account paramAccount, String paramString1, Bundle paramBundle, String paramString2) throws RemoteException;
  
  void resetTodayStats() throws RemoteException;
  
  void setIsSyncable(Account paramAccount, String paramString, int paramInt) throws RemoteException;
  
  void setIsSyncableAsUser(Account paramAccount, String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void setMasterSyncAutomatically(boolean paramBoolean) throws RemoteException;
  
  void setMasterSyncAutomaticallyAsUser(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setSyncAutomatically(Account paramAccount, String paramString, boolean paramBoolean) throws RemoteException;
  
  void setSyncAutomaticallyAsUser(Account paramAccount, String paramString, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void sync(SyncRequest paramSyncRequest, String paramString) throws RemoteException;
  
  void syncAsUser(SyncRequest paramSyncRequest, int paramInt, String paramString) throws RemoteException;
  
  void unregisterContentObserver(IContentObserver paramIContentObserver) throws RemoteException;
  
  class Default implements IContentService {
    public void unregisterContentObserver(IContentObserver param1IContentObserver) throws RemoteException {}
    
    public void registerContentObserver(Uri param1Uri, boolean param1Boolean, IContentObserver param1IContentObserver, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void notifyChange(Uri[] param1ArrayOfUri, IContentObserver param1IContentObserver, boolean param1Boolean, int param1Int1, int param1Int2, int param1Int3, String param1String) throws RemoteException {}
    
    public void requestSync(Account param1Account, String param1String1, Bundle param1Bundle, String param1String2) throws RemoteException {}
    
    public void sync(SyncRequest param1SyncRequest, String param1String) throws RemoteException {}
    
    public void syncAsUser(SyncRequest param1SyncRequest, int param1Int, String param1String) throws RemoteException {}
    
    public void cancelSync(Account param1Account, String param1String, ComponentName param1ComponentName) throws RemoteException {}
    
    public void cancelSyncAsUser(Account param1Account, String param1String, ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void cancelRequest(SyncRequest param1SyncRequest) throws RemoteException {}
    
    public boolean getSyncAutomatically(Account param1Account, String param1String) throws RemoteException {
      return false;
    }
    
    public boolean getSyncAutomaticallyAsUser(Account param1Account, String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public void setSyncAutomatically(Account param1Account, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void setSyncAutomaticallyAsUser(Account param1Account, String param1String, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public List<PeriodicSync> getPeriodicSyncs(Account param1Account, String param1String, ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public void addPeriodicSync(Account param1Account, String param1String, Bundle param1Bundle, long param1Long) throws RemoteException {}
    
    public void removePeriodicSync(Account param1Account, String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public int getIsSyncable(Account param1Account, String param1String) throws RemoteException {
      return 0;
    }
    
    public int getIsSyncableAsUser(Account param1Account, String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public void setIsSyncable(Account param1Account, String param1String, int param1Int) throws RemoteException {}
    
    public void setIsSyncableAsUser(Account param1Account, String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setMasterSyncAutomatically(boolean param1Boolean) throws RemoteException {}
    
    public void setMasterSyncAutomaticallyAsUser(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public boolean getMasterSyncAutomatically() throws RemoteException {
      return false;
    }
    
    public boolean getMasterSyncAutomaticallyAsUser(int param1Int) throws RemoteException {
      return false;
    }
    
    public List<SyncInfo> getCurrentSyncs() throws RemoteException {
      return null;
    }
    
    public List<SyncInfo> getCurrentSyncsAsUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public SyncAdapterType[] getSyncAdapterTypes() throws RemoteException {
      return null;
    }
    
    public SyncAdapterType[] getSyncAdapterTypesAsUser(int param1Int) throws RemoteException {
      return null;
    }
    
    public String[] getSyncAdapterPackagesForAuthorityAsUser(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isSyncActive(Account param1Account, String param1String, ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public SyncStatusInfo getSyncStatus(Account param1Account, String param1String, ComponentName param1ComponentName) throws RemoteException {
      return null;
    }
    
    public SyncStatusInfo getSyncStatusAsUser(Account param1Account, String param1String, ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean isSyncPending(Account param1Account, String param1String, ComponentName param1ComponentName) throws RemoteException {
      return false;
    }
    
    public boolean isSyncPendingAsUser(Account param1Account, String param1String, ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return false;
    }
    
    public void addStatusChangeListener(int param1Int, ISyncStatusObserver param1ISyncStatusObserver) throws RemoteException {}
    
    public void removeStatusChangeListener(ISyncStatusObserver param1ISyncStatusObserver) throws RemoteException {}
    
    public void putCache(String param1String, Uri param1Uri, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public Bundle getCache(String param1String, Uri param1Uri, int param1Int) throws RemoteException {
      return null;
    }
    
    public void resetTodayStats() throws RemoteException {}
    
    public void onDbCorruption(String param1String1, String param1String2, String param1String3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IContentService {
    private static final String DESCRIPTOR = "android.content.IContentService";
    
    static final int TRANSACTION_addPeriodicSync = 15;
    
    static final int TRANSACTION_addStatusChangeListener = 35;
    
    static final int TRANSACTION_cancelRequest = 9;
    
    static final int TRANSACTION_cancelSync = 7;
    
    static final int TRANSACTION_cancelSyncAsUser = 8;
    
    static final int TRANSACTION_getCache = 38;
    
    static final int TRANSACTION_getCurrentSyncs = 25;
    
    static final int TRANSACTION_getCurrentSyncsAsUser = 26;
    
    static final int TRANSACTION_getIsSyncable = 17;
    
    static final int TRANSACTION_getIsSyncableAsUser = 18;
    
    static final int TRANSACTION_getMasterSyncAutomatically = 23;
    
    static final int TRANSACTION_getMasterSyncAutomaticallyAsUser = 24;
    
    static final int TRANSACTION_getPeriodicSyncs = 14;
    
    static final int TRANSACTION_getSyncAdapterPackagesForAuthorityAsUser = 29;
    
    static final int TRANSACTION_getSyncAdapterTypes = 27;
    
    static final int TRANSACTION_getSyncAdapterTypesAsUser = 28;
    
    static final int TRANSACTION_getSyncAutomatically = 10;
    
    static final int TRANSACTION_getSyncAutomaticallyAsUser = 11;
    
    static final int TRANSACTION_getSyncStatus = 31;
    
    static final int TRANSACTION_getSyncStatusAsUser = 32;
    
    static final int TRANSACTION_isSyncActive = 30;
    
    static final int TRANSACTION_isSyncPending = 33;
    
    static final int TRANSACTION_isSyncPendingAsUser = 34;
    
    static final int TRANSACTION_notifyChange = 3;
    
    static final int TRANSACTION_onDbCorruption = 40;
    
    static final int TRANSACTION_putCache = 37;
    
    static final int TRANSACTION_registerContentObserver = 2;
    
    static final int TRANSACTION_removePeriodicSync = 16;
    
    static final int TRANSACTION_removeStatusChangeListener = 36;
    
    static final int TRANSACTION_requestSync = 4;
    
    static final int TRANSACTION_resetTodayStats = 39;
    
    static final int TRANSACTION_setIsSyncable = 19;
    
    static final int TRANSACTION_setIsSyncableAsUser = 20;
    
    static final int TRANSACTION_setMasterSyncAutomatically = 21;
    
    static final int TRANSACTION_setMasterSyncAutomaticallyAsUser = 22;
    
    static final int TRANSACTION_setSyncAutomatically = 12;
    
    static final int TRANSACTION_setSyncAutomaticallyAsUser = 13;
    
    static final int TRANSACTION_sync = 5;
    
    static final int TRANSACTION_syncAsUser = 6;
    
    static final int TRANSACTION_unregisterContentObserver = 1;
    
    public Stub() {
      attachInterface(this, "android.content.IContentService");
    }
    
    public static IContentService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.content.IContentService");
      if (iInterface != null && iInterface instanceof IContentService)
        return (IContentService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return (IBinder)this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 40:
          return "onDbCorruption";
        case 39:
          return "resetTodayStats";
        case 38:
          return "getCache";
        case 37:
          return "putCache";
        case 36:
          return "removeStatusChangeListener";
        case 35:
          return "addStatusChangeListener";
        case 34:
          return "isSyncPendingAsUser";
        case 33:
          return "isSyncPending";
        case 32:
          return "getSyncStatusAsUser";
        case 31:
          return "getSyncStatus";
        case 30:
          return "isSyncActive";
        case 29:
          return "getSyncAdapterPackagesForAuthorityAsUser";
        case 28:
          return "getSyncAdapterTypesAsUser";
        case 27:
          return "getSyncAdapterTypes";
        case 26:
          return "getCurrentSyncsAsUser";
        case 25:
          return "getCurrentSyncs";
        case 24:
          return "getMasterSyncAutomaticallyAsUser";
        case 23:
          return "getMasterSyncAutomatically";
        case 22:
          return "setMasterSyncAutomaticallyAsUser";
        case 21:
          return "setMasterSyncAutomatically";
        case 20:
          return "setIsSyncableAsUser";
        case 19:
          return "setIsSyncable";
        case 18:
          return "getIsSyncableAsUser";
        case 17:
          return "getIsSyncable";
        case 16:
          return "removePeriodicSync";
        case 15:
          return "addPeriodicSync";
        case 14:
          return "getPeriodicSyncs";
        case 13:
          return "setSyncAutomaticallyAsUser";
        case 12:
          return "setSyncAutomatically";
        case 11:
          return "getSyncAutomaticallyAsUser";
        case 10:
          return "getSyncAutomatically";
        case 9:
          return "cancelRequest";
        case 8:
          return "cancelSyncAsUser";
        case 7:
          return "cancelSync";
        case 6:
          return "syncAsUser";
        case 5:
          return "sync";
        case 4:
          return "requestSync";
        case 3:
          return "notifyChange";
        case 2:
          return "registerContentObserver";
        case 1:
          break;
      } 
      return "unregisterContentObserver";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int i;
        String str3;
        Bundle bundle;
        ISyncStatusObserver iSyncStatusObserver;
        SyncStatusInfo syncStatusInfo;
        String[] arrayOfString;
        SyncAdapterType[] arrayOfSyncAdapterType;
        List<SyncInfo> list1;
        String str2;
        List<PeriodicSync> list;
        String str1, str4;
        Uri[] arrayOfUri;
        IContentObserver iContentObserver2;
        String str5;
        IContentObserver iContentObserver3;
        String str6;
        long l;
        int n;
        boolean bool5 = false, bool6 = false, bool7 = false, bool8 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 40:
            param1Parcel1.enforceInterface("android.content.IContentService");
            str4 = param1Parcel1.readString();
            str5 = param1Parcel1.readString();
            str3 = param1Parcel1.readString();
            onDbCorruption(str4, str5, str3);
            param1Parcel2.writeNoException();
            return true;
          case 39:
            str3.enforceInterface("android.content.IContentService");
            resetTodayStats();
            param1Parcel2.writeNoException();
            return true;
          case 38:
            str3.enforceInterface("android.content.IContentService");
            str4 = str3.readString();
            if (str3.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str5 = null;
            } 
            param1Int1 = str3.readInt();
            bundle = getCache(str4, (Uri)str5, param1Int1);
            param1Parcel2.writeNoException();
            if (bundle != null) {
              param1Parcel2.writeInt(1);
              bundle.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 37:
            bundle.enforceInterface("android.content.IContentService");
            str6 = bundle.readString();
            if (bundle.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              str5 = null;
            } 
            if (bundle.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)bundle);
            } else {
              str4 = null;
            } 
            param1Int1 = bundle.readInt();
            putCache(str6, (Uri)str5, (Bundle)str4, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 36:
            bundle.enforceInterface("android.content.IContentService");
            iSyncStatusObserver = ISyncStatusObserver.Stub.asInterface(bundle.readStrongBinder());
            removeStatusChangeListener(iSyncStatusObserver);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            iSyncStatusObserver.enforceInterface("android.content.IContentService");
            param1Int1 = iSyncStatusObserver.readInt();
            iSyncStatusObserver = ISyncStatusObserver.Stub.asInterface(iSyncStatusObserver.readStrongBinder());
            addStatusChangeListener(param1Int1, iSyncStatusObserver);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            iSyncStatusObserver.enforceInterface("android.content.IContentService");
            if (iSyncStatusObserver.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              str5 = null;
            } 
            str6 = iSyncStatusObserver.readString();
            if (iSyncStatusObserver.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              str4 = null;
            } 
            param1Int1 = iSyncStatusObserver.readInt();
            bool4 = isSyncPendingAsUser((Account)str5, str6, (ComponentName)str4, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 33:
            iSyncStatusObserver.enforceInterface("android.content.IContentService");
            if (iSyncStatusObserver.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              str5 = null;
            } 
            str4 = iSyncStatusObserver.readString();
            if (iSyncStatusObserver.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              iSyncStatusObserver = null;
            } 
            bool4 = isSyncPending((Account)str5, str4, (ComponentName)iSyncStatusObserver);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 32:
            iSyncStatusObserver.enforceInterface("android.content.IContentService");
            if (iSyncStatusObserver.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              str5 = null;
            } 
            str6 = iSyncStatusObserver.readString();
            if (iSyncStatusObserver.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)iSyncStatusObserver);
            } else {
              str4 = null;
            } 
            m = iSyncStatusObserver.readInt();
            syncStatusInfo = getSyncStatusAsUser((Account)str5, str6, (ComponentName)str4, m);
            param1Parcel2.writeNoException();
            if (syncStatusInfo != null) {
              param1Parcel2.writeInt(1);
              syncStatusInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 31:
            syncStatusInfo.enforceInterface("android.content.IContentService");
            if (syncStatusInfo.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)syncStatusInfo);
            } else {
              str5 = null;
            } 
            str4 = syncStatusInfo.readString();
            if (syncStatusInfo.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)syncStatusInfo);
            } else {
              syncStatusInfo = null;
            } 
            syncStatusInfo = getSyncStatus((Account)str5, str4, (ComponentName)syncStatusInfo);
            param1Parcel2.writeNoException();
            if (syncStatusInfo != null) {
              param1Parcel2.writeInt(1);
              syncStatusInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 30:
            syncStatusInfo.enforceInterface("android.content.IContentService");
            if (syncStatusInfo.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)syncStatusInfo);
            } else {
              str5 = null;
            } 
            str4 = syncStatusInfo.readString();
            if (syncStatusInfo.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)syncStatusInfo);
            } else {
              syncStatusInfo = null;
            } 
            bool3 = isSyncActive((Account)str5, str4, (ComponentName)syncStatusInfo);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 29:
            syncStatusInfo.enforceInterface("android.content.IContentService");
            str5 = syncStatusInfo.readString();
            k = syncStatusInfo.readInt();
            arrayOfString = getSyncAdapterPackagesForAuthorityAsUser(str5, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringArray(arrayOfString);
            return true;
          case 28:
            arrayOfString.enforceInterface("android.content.IContentService");
            k = arrayOfString.readInt();
            arrayOfSyncAdapterType = getSyncAdapterTypesAsUser(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfSyncAdapterType, 1);
            return true;
          case 27:
            arrayOfSyncAdapterType.enforceInterface("android.content.IContentService");
            arrayOfSyncAdapterType = getSyncAdapterTypes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedArray((Parcelable[])arrayOfSyncAdapterType, 1);
            return true;
          case 26:
            arrayOfSyncAdapterType.enforceInterface("android.content.IContentService");
            k = arrayOfSyncAdapterType.readInt();
            list1 = getCurrentSyncsAsUser(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 25:
            list1.enforceInterface("android.content.IContentService");
            list1 = getCurrentSyncs();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 24:
            list1.enforceInterface("android.content.IContentService");
            k = list1.readInt();
            bool2 = getMasterSyncAutomaticallyAsUser(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 23:
            list1.enforceInterface("android.content.IContentService");
            bool2 = getMasterSyncAutomatically();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 22:
            list1.enforceInterface("android.content.IContentService");
            bool7 = bool8;
            if (list1.readInt() != 0)
              bool7 = true; 
            j = list1.readInt();
            setMasterSyncAutomaticallyAsUser(bool7, j);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            list1.enforceInterface("android.content.IContentService");
            bool7 = bool5;
            if (list1.readInt() != 0)
              bool7 = true; 
            setMasterSyncAutomatically(bool7);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            list1.enforceInterface("android.content.IContentService");
            if (list1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str5 = null;
            } 
            str4 = list1.readString();
            j = list1.readInt();
            param1Int2 = list1.readInt();
            setIsSyncableAsUser((Account)str5, str4, j, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            list1.enforceInterface("android.content.IContentService");
            if (list1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str5 = null;
            } 
            str4 = list1.readString();
            j = list1.readInt();
            setIsSyncable((Account)str5, str4, j);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            list1.enforceInterface("android.content.IContentService");
            if (list1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str5 = null;
            } 
            str4 = list1.readString();
            j = list1.readInt();
            j = getIsSyncableAsUser((Account)str5, str4, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 17:
            list1.enforceInterface("android.content.IContentService");
            if (list1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str5 = null;
            } 
            str2 = list1.readString();
            j = getIsSyncable((Account)str5, str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(j);
            return true;
          case 16:
            str2.enforceInterface("android.content.IContentService");
            if (str2.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            removePeriodicSync((Account)str5, str4, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("android.content.IContentService");
            if (str2.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            str6 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str4 = null;
            } 
            l = str2.readLong();
            addPeriodicSync((Account)str5, str6, (Bundle)str4, l);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str2.enforceInterface("android.content.IContentService");
            if (str2.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str5 = null;
            } 
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            list = getPeriodicSyncs((Account)str5, str4, (ComponentName)str2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 13:
            list.enforceInterface("android.content.IContentService");
            if (list.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list);
            } else {
              str5 = null;
            } 
            str4 = list.readString();
            bool7 = bool6;
            if (list.readInt() != 0)
              bool7 = true; 
            j = list.readInt();
            setSyncAutomaticallyAsUser((Account)str5, str4, bool7, j);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            list.enforceInterface("android.content.IContentService");
            if (list.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list);
            } else {
              str5 = null;
            } 
            str4 = list.readString();
            if (list.readInt() != 0)
              bool7 = true; 
            setSyncAutomatically((Account)str5, str4, bool7);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            list.enforceInterface("android.content.IContentService");
            if (list.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list);
            } else {
              str5 = null;
            } 
            str4 = list.readString();
            j = list.readInt();
            bool1 = getSyncAutomaticallyAsUser((Account)str5, str4, j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 10:
            list.enforceInterface("android.content.IContentService");
            if (list.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)list);
            } else {
              str5 = null;
            } 
            str1 = list.readString();
            bool1 = getSyncAutomatically((Account)str5, str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 9:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              SyncRequest syncRequest = (SyncRequest)SyncRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            cancelRequest((SyncRequest)str1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            str6 = str1.readString();
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            i = str1.readInt();
            cancelSyncAsUser((Account)str5, str6, (ComponentName)str4, i);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            str4 = str1.readString();
            if (str1.readInt() != 0) {
              ComponentName componentName = (ComponentName)ComponentName.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            cancelSync((Account)str5, str4, (ComponentName)str1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              SyncRequest syncRequest = (SyncRequest)SyncRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            i = str1.readInt();
            str1 = str1.readString();
            syncAsUser((SyncRequest)str5, i, str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              SyncRequest syncRequest = (SyncRequest)SyncRequest.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            str1 = str1.readString();
            sync((SyncRequest)str5, str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              Account account = (Account)Account.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str5 = null;
            } 
            str6 = str1.readString();
            if (str1.readInt() != 0) {
              Bundle bundle1 = (Bundle)Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str4 = null;
            } 
            str1 = str1.readString();
            requestSync((Account)str5, str6, (Bundle)str4, str1);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.content.IContentService");
            arrayOfUri = (Uri[])str1.createTypedArray(Uri.CREATOR);
            iContentObserver3 = IContentObserver.Stub.asInterface(str1.readStrongBinder());
            if (str1.readInt() != 0) {
              bool7 = true;
            } else {
              bool7 = false;
            } 
            i = str1.readInt();
            param1Int2 = str1.readInt();
            n = str1.readInt();
            str1 = str1.readString();
            notifyChange(arrayOfUri, iContentObserver3, bool7, i, param1Int2, n, str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.content.IContentService");
            if (str1.readInt() != 0) {
              Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              iContentObserver3 = null;
            } 
            if (str1.readInt() != 0) {
              bool7 = true;
            } else {
              bool7 = false;
            } 
            iContentObserver2 = IContentObserver.Stub.asInterface(str1.readStrongBinder());
            param1Int2 = str1.readInt();
            i = str1.readInt();
            registerContentObserver((Uri)iContentObserver3, bool7, iContentObserver2, param1Int2, i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.content.IContentService");
        IContentObserver iContentObserver1 = IContentObserver.Stub.asInterface(str1.readStrongBinder());
        unregisterContentObserver(iContentObserver1);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.content.IContentService");
      return true;
    }
    
    private static class Proxy implements IContentService {
      public static IContentService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.content.IContentService";
      }
      
      public void unregisterContentObserver(IContentObserver param2IContentObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2IContentObserver != null) {
            iBinder = param2IContentObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().unregisterContentObserver(param2IContentObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerContentObserver(Uri param2Uri, boolean param2Boolean, IContentObserver param2IContentObserver, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = true;
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          if (param2IContentObserver != null) {
            iBinder = param2IContentObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().registerContentObserver(param2Uri, param2Boolean, param2IContentObserver, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifyChange(Uri[] param2ArrayOfUri, IContentObserver param2IContentObserver, boolean param2Boolean, int param2Int1, int param2Int2, int param2Int3, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          try {
            IBinder iBinder;
            boolean bool;
            parcel1.writeTypedArray((Parcelable[])param2ArrayOfUri, 0);
            if (param2IContentObserver != null) {
              iBinder = param2IContentObserver.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            if (param2Boolean) {
              bool = true;
            } else {
              bool = false;
            } 
            parcel1.writeInt(bool);
            try {
              parcel1.writeInt(param2Int1);
              try {
                parcel1.writeInt(param2Int2);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    parcel1.writeString(param2String);
                    boolean bool1 = this.mRemote.transact(3, parcel1, parcel2, 0);
                    if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
                      IContentService.Stub.getDefaultImpl().notifyChange(param2ArrayOfUri, param2IContentObserver, param2Boolean, param2Int1, param2Int2, param2Int3, param2String);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ArrayOfUri;
      }
      
      public void requestSync(Account param2Account, String param2String1, Bundle param2Bundle, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String1);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().requestSync(param2Account, param2String1, param2Bundle, param2String2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sync(SyncRequest param2SyncRequest, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2SyncRequest != null) {
            parcel1.writeInt(1);
            param2SyncRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().sync(param2SyncRequest, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void syncAsUser(SyncRequest param2SyncRequest, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2SyncRequest != null) {
            parcel1.writeInt(1);
            param2SyncRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().syncAsUser(param2SyncRequest, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelSync(Account param2Account, String param2String, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().cancelSync(param2Account, param2String, param2ComponentName);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelSyncAsUser(Account param2Account, String param2String, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().cancelSyncAsUser(param2Account, param2String, param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void cancelRequest(SyncRequest param2SyncRequest) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2SyncRequest != null) {
            parcel1.writeInt(1);
            param2SyncRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().cancelRequest(param2SyncRequest);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSyncAutomatically(Account param2Account, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool1 = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool2 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().getSyncAutomatically(param2Account, param2String);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getSyncAutomaticallyAsUser(Account param2Account, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool1 = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().getSyncAutomaticallyAsUser(param2Account, param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSyncAutomatically(Account param2Account, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setSyncAutomatically(param2Account, param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSyncAutomaticallyAsUser(Account param2Account, String param2String, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (!param2Boolean)
            bool = false; 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setSyncAutomaticallyAsUser(param2Account, param2String, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<PeriodicSync> getPeriodicSyncs(Account param2Account, String param2String, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getPeriodicSyncs(param2Account, param2String, param2ComponentName); 
          parcel2.readException();
          return parcel2.createTypedArrayList(PeriodicSync.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addPeriodicSync(Account param2Account, String param2String, Bundle param2Bundle, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().addPeriodicSync(param2Account, param2String, param2Bundle, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removePeriodicSync(Account param2Account, String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().removePeriodicSync(param2Account, param2String, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getIsSyncable(Account param2Account, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getIsSyncable(param2Account, param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getIsSyncableAsUser(Account param2Account, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            param2Int = IContentService.Stub.getDefaultImpl().getIsSyncableAsUser(param2Account, param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIsSyncable(Account param2Account, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setIsSyncable(param2Account, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setIsSyncableAsUser(Account param2Account, String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setIsSyncableAsUser(param2Account, param2String, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMasterSyncAutomatically(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setMasterSyncAutomatically(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMasterSyncAutomaticallyAsUser(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool1 && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().setMasterSyncAutomaticallyAsUser(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getMasterSyncAutomatically() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().getMasterSyncAutomatically();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean getMasterSyncAutomaticallyAsUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(24, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().getMasterSyncAutomaticallyAsUser(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SyncInfo> getCurrentSyncs() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getCurrentSyncs(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SyncInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<SyncInfo> getCurrentSyncsAsUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getCurrentSyncsAsUser(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(SyncInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SyncAdapterType[] getSyncAdapterTypes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getSyncAdapterTypes(); 
          parcel2.readException();
          return (SyncAdapterType[])parcel2.createTypedArray(SyncAdapterType.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SyncAdapterType[] getSyncAdapterTypesAsUser(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getSyncAdapterTypesAsUser(param2Int); 
          parcel2.readException();
          return (SyncAdapterType[])parcel2.createTypedArray(SyncAdapterType.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String[] getSyncAdapterPackagesForAuthorityAsUser(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getSyncAdapterPackagesForAuthorityAsUser(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createStringArray();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSyncActive(Account param2Account, String param2String, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool1 = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().isSyncActive(param2Account, param2String, param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SyncStatusInfo getSyncStatus(Account param2Account, String param2String, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getSyncStatus(param2Account, param2String, param2ComponentName); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SyncStatusInfo syncStatusInfo = (SyncStatusInfo)SyncStatusInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Account = null;
          } 
          return (SyncStatusInfo)param2Account;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public SyncStatusInfo getSyncStatusAsUser(Account param2Account, String param2String, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getSyncStatusAsUser(param2Account, param2String, param2ComponentName, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            SyncStatusInfo syncStatusInfo = (SyncStatusInfo)SyncStatusInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2Account = null;
          } 
          return (SyncStatusInfo)param2Account;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSyncPending(Account param2Account, String param2String, ComponentName param2ComponentName) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool1 = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().isSyncPending(param2Account, param2String, param2ComponentName);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSyncPendingAsUser(Account param2Account, String param2String, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool1 = true;
          if (param2Account != null) {
            parcel1.writeInt(1);
            param2Account.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeString(param2String);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool2 && IContentService.Stub.getDefaultImpl() != null) {
            bool1 = IContentService.Stub.getDefaultImpl().isSyncPendingAsUser(param2Account, param2String, param2ComponentName, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addStatusChangeListener(int param2Int, ISyncStatusObserver param2ISyncStatusObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeInt(param2Int);
          if (param2ISyncStatusObserver != null) {
            iBinder = param2ISyncStatusObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().addStatusChangeListener(param2Int, param2ISyncStatusObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeStatusChangeListener(ISyncStatusObserver param2ISyncStatusObserver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.content.IContentService");
          if (param2ISyncStatusObserver != null) {
            iBinder = param2ISyncStatusObserver.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().removeStatusChangeListener(param2ISyncStatusObserver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void putCache(String param2String, Uri param2Uri, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().putCache(param2String, param2Uri, param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getCache(String param2String, Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null)
            return IContentService.Stub.getDefaultImpl().getCache(param2String, param2Uri, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            Bundle bundle = (Bundle)Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (Bundle)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void resetTodayStats() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().resetTodayStats();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void onDbCorruption(String param2String1, String param2String2, String param2String3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.content.IContentService");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeString(param2String3);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && IContentService.Stub.getDefaultImpl() != null) {
            IContentService.Stub.getDefaultImpl().onDbCorruption(param2String1, param2String2, param2String3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IContentService param1IContentService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IContentService != null) {
          Proxy.sDefaultImpl = param1IContentService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IContentService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
