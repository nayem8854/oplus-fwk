package android.content;

import java.util.ArrayList;

public final class MimeTypeFilter {
  private static boolean mimeTypeAgainstFilter(String[] paramArrayOfString1, String[] paramArrayOfString2) {
    if (paramArrayOfString2.length == 2) {
      if (!paramArrayOfString2[0].isEmpty() && !paramArrayOfString2[1].isEmpty()) {
        if (paramArrayOfString1.length != 2)
          return false; 
        if (!"*".equals(paramArrayOfString2[0])) {
          String str1 = paramArrayOfString2[0], str2 = paramArrayOfString1[0];
          if (!str1.equals(str2))
            return false; 
        } 
        if (!"*".equals(paramArrayOfString2[1])) {
          String str2 = paramArrayOfString2[1], str1 = paramArrayOfString1[1];
          if (!str2.equals(str1))
            return false; 
        } 
        return true;
      } 
      throw new IllegalArgumentException("Ill-formatted MIME type filter. Type or subtype empty.");
    } 
    throw new IllegalArgumentException("Ill-formatted MIME type filter. Must be type/subtype.");
  }
  
  public static boolean matches(String paramString1, String paramString2) {
    if (paramString1 == null)
      return false; 
    String[] arrayOfString1 = paramString1.split("/");
    String[] arrayOfString2 = paramString2.split("/");
    return mimeTypeAgainstFilter(arrayOfString1, arrayOfString2);
  }
  
  public static String matches(String paramString, String[] paramArrayOfString) {
    if (paramString == null)
      return null; 
    String[] arrayOfString = paramString.split("/");
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      String[] arrayOfString1 = str.split("/");
      if (mimeTypeAgainstFilter(arrayOfString, arrayOfString1))
        return str; 
      b++;
    } 
    return null;
  }
  
  public static String matches(String[] paramArrayOfString, String paramString) {
    if (paramArrayOfString == null)
      return null; 
    String[] arrayOfString = paramString.split("/");
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      String[] arrayOfString1 = str.split("/");
      if (mimeTypeAgainstFilter(arrayOfString1, arrayOfString))
        return str; 
      b++;
    } 
    return null;
  }
  
  public static String[] matchesMany(String[] paramArrayOfString, String paramString) {
    byte b = 0;
    if (paramArrayOfString == null)
      return new String[0]; 
    ArrayList<String> arrayList = new ArrayList();
    String[] arrayOfString = paramString.split("/");
    for (int i = paramArrayOfString.length; b < i; ) {
      String str = paramArrayOfString[b];
      String[] arrayOfString1 = str.split("/");
      if (mimeTypeAgainstFilter(arrayOfString1, arrayOfString))
        arrayList.add(str); 
      b++;
    } 
    return arrayList.<String>toArray(new String[arrayList.size()]);
  }
}
