package android.content;

import android.os.Parcel;
import android.os.Parcelable;

public abstract class UndoOperation<DATA> implements Parcelable {
  UndoOwner mOwner;
  
  public UndoOperation(UndoOwner paramUndoOwner) {
    this.mOwner = paramUndoOwner;
  }
  
  protected UndoOperation(Parcel paramParcel, ClassLoader paramClassLoader) {}
  
  public UndoOwner getOwner() {
    return this.mOwner;
  }
  
  public DATA getOwnerData() {
    return (DATA)this.mOwner.getData();
  }
  
  public boolean matchOwner(UndoOwner paramUndoOwner) {
    boolean bool;
    if (paramUndoOwner == getOwner()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean hasData() {
    return true;
  }
  
  public boolean allowMerge() {
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public abstract void commit();
  
  public abstract void redo();
  
  public abstract void undo();
}
