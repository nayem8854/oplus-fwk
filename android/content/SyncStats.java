package android.content;

import android.os.Parcel;
import android.os.Parcelable;

public class SyncStats implements Parcelable {
  public SyncStats() {
    this.numAuthExceptions = 0L;
    this.numIoExceptions = 0L;
    this.numParseExceptions = 0L;
    this.numConflictDetectedExceptions = 0L;
    this.numInserts = 0L;
    this.numUpdates = 0L;
    this.numDeletes = 0L;
    this.numEntries = 0L;
    this.numSkippedEntries = 0L;
  }
  
  public SyncStats(Parcel paramParcel) {
    this.numAuthExceptions = paramParcel.readLong();
    this.numIoExceptions = paramParcel.readLong();
    this.numParseExceptions = paramParcel.readLong();
    this.numConflictDetectedExceptions = paramParcel.readLong();
    this.numInserts = paramParcel.readLong();
    this.numUpdates = paramParcel.readLong();
    this.numDeletes = paramParcel.readLong();
    this.numEntries = paramParcel.readLong();
    this.numSkippedEntries = paramParcel.readLong();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(" stats [");
    if (this.numAuthExceptions > 0L) {
      stringBuilder.append(" numAuthExceptions: ");
      stringBuilder.append(this.numAuthExceptions);
    } 
    if (this.numIoExceptions > 0L) {
      stringBuilder.append(" numIoExceptions: ");
      stringBuilder.append(this.numIoExceptions);
    } 
    if (this.numParseExceptions > 0L) {
      stringBuilder.append(" numParseExceptions: ");
      stringBuilder.append(this.numParseExceptions);
    } 
    if (this.numConflictDetectedExceptions > 0L) {
      stringBuilder.append(" numConflictDetectedExceptions: ");
      stringBuilder.append(this.numConflictDetectedExceptions);
    } 
    if (this.numInserts > 0L) {
      stringBuilder.append(" numInserts: ");
      stringBuilder.append(this.numInserts);
    } 
    if (this.numUpdates > 0L) {
      stringBuilder.append(" numUpdates: ");
      stringBuilder.append(this.numUpdates);
    } 
    if (this.numDeletes > 0L) {
      stringBuilder.append(" numDeletes: ");
      stringBuilder.append(this.numDeletes);
    } 
    if (this.numEntries > 0L) {
      stringBuilder.append(" numEntries: ");
      stringBuilder.append(this.numEntries);
    } 
    if (this.numSkippedEntries > 0L) {
      stringBuilder.append(" numSkippedEntries: ");
      stringBuilder.append(this.numSkippedEntries);
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public void clear() {
    this.numAuthExceptions = 0L;
    this.numIoExceptions = 0L;
    this.numParseExceptions = 0L;
    this.numConflictDetectedExceptions = 0L;
    this.numInserts = 0L;
    this.numUpdates = 0L;
    this.numDeletes = 0L;
    this.numEntries = 0L;
    this.numSkippedEntries = 0L;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeLong(this.numAuthExceptions);
    paramParcel.writeLong(this.numIoExceptions);
    paramParcel.writeLong(this.numParseExceptions);
    paramParcel.writeLong(this.numConflictDetectedExceptions);
    paramParcel.writeLong(this.numInserts);
    paramParcel.writeLong(this.numUpdates);
    paramParcel.writeLong(this.numDeletes);
    paramParcel.writeLong(this.numEntries);
    paramParcel.writeLong(this.numSkippedEntries);
  }
  
  public static final Parcelable.Creator<SyncStats> CREATOR = new Parcelable.Creator<SyncStats>() {
      public SyncStats createFromParcel(Parcel param1Parcel) {
        return new SyncStats(param1Parcel);
      }
      
      public SyncStats[] newArray(int param1Int) {
        return new SyncStats[param1Int];
      }
    };
  
  public long numAuthExceptions;
  
  public long numConflictDetectedExceptions;
  
  public long numDeletes;
  
  public long numEntries;
  
  public long numInserts;
  
  public long numIoExceptions;
  
  public long numParseExceptions;
  
  public long numSkippedEntries;
  
  public long numUpdates;
}
