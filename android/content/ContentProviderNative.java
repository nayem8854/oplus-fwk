package android.content;

import android.content.res.AssetFileDescriptor;
import android.database.BulkCursorDescriptor;
import android.database.Cursor;
import android.database.CursorToBulkCursorAdaptor;
import android.database.DatabaseUtils;
import android.database.IContentObserver;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.ICancellationSignal;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.Parcelable;
import android.os.RemoteCallback;
import android.os.RemoteException;
import java.util.ArrayList;

public abstract class ContentProviderNative extends Binder implements IContentProvider {
  public ContentProviderNative() {
    attachInterface(this, "android.content.IContentProvider");
  }
  
  public static IContentProvider asInterface(IBinder paramIBinder) {
    if (paramIBinder == null)
      return null; 
    IContentProvider iContentProvider = (IContentProvider)paramIBinder.queryLocalInterface("android.content.IContentProvider");
    if (iContentProvider != null)
      return iContentProvider; 
    return new ContentProviderProxy(paramIBinder);
  }
  
  public boolean onTransact(int paramInt1, Parcel paramParcel1, Parcel paramParcel2, int paramInt2) throws RemoteException {
    String str1, arrayOfString[];
    boolean bool = false;
    if (paramInt1 != 1) {
      if (paramInt1 != 2) {
        if (paramInt1 != 3) {
          if (paramInt1 != 4) {
            ContentValues[] arrayOfContentValues;
            if (paramInt1 != 10) {
              RemoteCallback remoteCallback;
              IBinder iBinder6;
              ICancellationSignal iCancellationSignal4;
              Uri uri5;
              ICancellationSignal iCancellationSignal3;
              IBinder iBinder5;
              ICancellationSignal iCancellationSignal2;
              AssetFileDescriptor assetFileDescriptor2;
              String str10, arrayOfString1[];
              Bundle bundle2;
              ContentProviderResult[] arrayOfContentProviderResult;
              IBinder iBinder4;
              ICancellationSignal iCancellationSignal1;
              AssetFileDescriptor assetFileDescriptor1;
              IBinder iBinder3;
              ParcelFileDescriptor parcelFileDescriptor;
              String str13;
              Uri uri6;
              String str12;
              ArrayList<ContentProviderOperation> arrayList;
              Uri uri9;
              Bundle bundle3;
              String str16;
              Uri uri8;
              String str15, str18;
              Uri uri10;
              String str17;
              boolean bool1;
              Bundle bundle4;
              String str19;
              IBinder iBinder7;
              ICancellationSignal iCancellationSignal5;
              switch (paramInt1) {
                default:
                  switch (paramInt1) {
                    default:
                      return super.onTransact(paramInt1, paramParcel1, paramParcel2, paramInt2);
                    case 30:
                      try {
                        paramParcel1.enforceInterface("android.content.IContentProvider");
                        String str20 = paramParcel1.readString();
                        String str21 = paramParcel1.readString();
                        Uri uri11 = (Uri)Uri.CREATOR.createFromParcel(paramParcel1);
                        RemoteCallback remoteCallback1 = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel(paramParcel1);
                        canonicalizeAsync(str20, str21, uri11, remoteCallback1);
                        return true;
                      } catch (Exception exception) {
                        DatabaseUtils.writeExceptionToParcel(paramParcel2, exception);
                        return true;
                      } 
                    case 29:
                      exception.enforceInterface("android.content.IContentProvider");
                      uri9 = (Uri)Uri.CREATOR.createFromParcel((Parcel)exception);
                      remoteCallback = (RemoteCallback)RemoteCallback.CREATOR.createFromParcel((Parcel)exception);
                      getTypeAsync(uri9, remoteCallback);
                      return true;
                    case 28:
                      remoteCallback.enforceInterface("android.content.IContentProvider");
                      str14 = remoteCallback.readString();
                      str13 = remoteCallback.readString();
                      uri9 = (Uri)Uri.CREATOR.createFromParcel((Parcel)remoteCallback);
                      paramInt1 = remoteCallback.readInt();
                      paramInt2 = remoteCallback.readInt();
                      paramInt1 = checkUriPermission(str14, str13, uri9, paramInt1, paramInt2);
                      paramParcel2.writeNoException();
                      paramParcel2.writeInt(paramInt1);
                      return true;
                    case 27:
                      remoteCallback.enforceInterface("android.content.IContentProvider");
                      str14 = remoteCallback.readString();
                      str18 = remoteCallback.readString();
                      uri6 = (Uri)Uri.CREATOR.createFromParcel((Parcel)remoteCallback);
                      bundle3 = remoteCallback.readBundle();
                      iBinder6 = remoteCallback.readStrongBinder();
                      iCancellationSignal4 = ICancellationSignal.Stub.asInterface(iBinder6);
                      bool1 = refresh(str14, str18, uri6, bundle3, iCancellationSignal4);
                      paramParcel2.writeNoException();
                      if (bool1) {
                        paramInt1 = bool;
                      } else {
                        paramInt1 = -1;
                      } 
                      paramParcel2.writeInt(paramInt1);
                      return true;
                    case 26:
                      iCancellationSignal4.enforceInterface("android.content.IContentProvider");
                      str16 = iCancellationSignal4.readString();
                      str14 = iCancellationSignal4.readString();
                      uri5 = (Uri)Uri.CREATOR.createFromParcel((Parcel)iCancellationSignal4);
                      uri5 = uncanonicalize(str16, str14, uri5);
                      paramParcel2.writeNoException();
                      Uri.writeToParcel(paramParcel2, uri5);
                      return true;
                    case 25:
                      uri5.enforceInterface("android.content.IContentProvider");
                      str16 = uri5.readString();
                      str14 = uri5.readString();
                      uri5 = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri5);
                      uri5 = canonicalize(str16, str14, uri5);
                      paramParcel2.writeNoException();
                      Uri.writeToParcel(paramParcel2, uri5);
                      return true;
                    case 24:
                      uri5.enforceInterface("android.content.IContentProvider");
                      iCancellationSignal3 = createCancellationSignal();
                      paramParcel2.writeNoException();
                      paramParcel2.writeStrongBinder(iCancellationSignal3.asBinder());
                      return true;
                    case 23:
                      iCancellationSignal3.enforceInterface("android.content.IContentProvider");
                      str18 = iCancellationSignal3.readString();
                      str16 = iCancellationSignal3.readString();
                      uri6 = (Uri)Uri.CREATOR.createFromParcel((Parcel)iCancellationSignal3);
                      str14 = iCancellationSignal3.readString();
                      bundle4 = iCancellationSignal3.readBundle();
                      iBinder5 = iCancellationSignal3.readStrongBinder();
                      iCancellationSignal2 = ICancellationSignal.Stub.asInterface(iBinder5);
                      assetFileDescriptor2 = openTypedAssetFile(str18, str16, uri6, str14, bundle4, iCancellationSignal2);
                      paramParcel2.writeNoException();
                      if (assetFileDescriptor2 != null) {
                        paramParcel2.writeInt(1);
                        assetFileDescriptor2.writeToParcel(paramParcel2, 1);
                      } else {
                        paramParcel2.writeInt(0);
                      } 
                      return true;
                    case 22:
                      assetFileDescriptor2.enforceInterface("android.content.IContentProvider");
                      uri8 = (Uri)Uri.CREATOR.createFromParcel((Parcel)assetFileDescriptor2);
                      str10 = assetFileDescriptor2.readString();
                      arrayOfString1 = getStreamTypes(uri8, str10);
                      paramParcel2.writeNoException();
                      paramParcel2.writeStringArray(arrayOfString1);
                      return true;
                    case 21:
                      arrayOfString1.enforceInterface("android.content.IContentProvider");
                      str14 = arrayOfString1.readString();
                      str19 = arrayOfString1.readString();
                      str12 = arrayOfString1.readString();
                      str15 = arrayOfString1.readString();
                      str18 = arrayOfString1.readString();
                      bundle2 = arrayOfString1.readBundle();
                      bundle2 = call(str14, str19, str12, str15, str18, bundle2);
                      paramParcel2.writeNoException();
                      paramParcel2.writeBundle(bundle2);
                      return true;
                    case 20:
                      break;
                  } 
                  bundle2.enforceInterface("android.content.IContentProvider");
                  str15 = bundle2.readString();
                  str18 = bundle2.readString();
                  str14 = bundle2.readString();
                  paramInt2 = bundle2.readInt();
                  arrayList = new ArrayList();
                  this(paramInt2);
                  for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++)
                    arrayList.add(paramInt1, (ContentProviderOperation)ContentProviderOperation.CREATOR.createFromParcel((Parcel)bundle2)); 
                  arrayOfContentProviderResult = applyBatch(str15, str18, str14, arrayList);
                  paramParcel2.writeNoException();
                  paramParcel2.writeTypedArray((Parcelable[])arrayOfContentProviderResult, 0);
                  return true;
                case 15:
                  arrayOfContentProviderResult.enforceInterface("android.content.IContentProvider");
                  str11 = arrayOfContentProviderResult.readString();
                  str14 = arrayOfContentProviderResult.readString();
                  uri10 = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfContentProviderResult);
                  str15 = arrayOfContentProviderResult.readString();
                  iBinder4 = arrayOfContentProviderResult.readStrongBinder();
                  iCancellationSignal1 = ICancellationSignal.Stub.asInterface(iBinder4);
                  assetFileDescriptor1 = openAssetFile(str11, str14, uri10, str15, iCancellationSignal1);
                  paramParcel2.writeNoException();
                  if (assetFileDescriptor1 != null) {
                    paramParcel2.writeInt(1);
                    assetFileDescriptor1.writeToParcel(paramParcel2, 1);
                  } else {
                    paramParcel2.writeInt(0);
                  } 
                  return true;
                case 14:
                  assetFileDescriptor1.enforceInterface("android.content.IContentProvider");
                  str14 = assetFileDescriptor1.readString();
                  str11 = assetFileDescriptor1.readString();
                  uri7 = (Uri)Uri.CREATOR.createFromParcel((Parcel)assetFileDescriptor1);
                  str17 = assetFileDescriptor1.readString();
                  iBinder7 = assetFileDescriptor1.readStrongBinder();
                  iCancellationSignal5 = ICancellationSignal.Stub.asInterface(iBinder7);
                  iBinder3 = assetFileDescriptor1.readStrongBinder();
                  parcelFileDescriptor = openFile(str14, str11, uri7, str17, iCancellationSignal5, iBinder3);
                  paramParcel2.writeNoException();
                  if (parcelFileDescriptor != null) {
                    paramParcel2.writeInt(1);
                    parcelFileDescriptor.writeToParcel(paramParcel2, 1);
                  } else {
                    paramParcel2.writeInt(0);
                  } 
                  return true;
                case 13:
                  break;
              } 
              parcelFileDescriptor.enforceInterface("android.content.IContentProvider");
              String str11 = parcelFileDescriptor.readString();
              String str14 = parcelFileDescriptor.readString();
              Uri uri7 = (Uri)Uri.CREATOR.createFromParcel((Parcel)parcelFileDescriptor);
              arrayOfContentValues = (ContentValues[])parcelFileDescriptor.createTypedArray(ContentValues.CREATOR);
              paramInt1 = bulkInsert(str11, str14, uri7, arrayOfContentValues);
              paramParcel2.writeNoException();
              paramParcel2.writeInt(paramInt1);
              return true;
            } 
            arrayOfContentValues.enforceInterface("android.content.IContentProvider");
            String str9 = arrayOfContentValues.readString();
            String str8 = arrayOfContentValues.readString();
            Uri uri4 = (Uri)Uri.CREATOR.createFromParcel((Parcel)arrayOfContentValues);
            ContentValues contentValues = (ContentValues)ContentValues.CREATOR.createFromParcel((Parcel)arrayOfContentValues);
            bundle1 = arrayOfContentValues.readBundle();
            paramInt1 = update(str9, str8, uri4, contentValues, bundle1);
            paramParcel2.writeNoException();
            paramParcel2.writeInt(paramInt1);
            return true;
          } 
          bundle1.enforceInterface("android.content.IContentProvider");
          String str7 = bundle1.readString();
          String str6 = bundle1.readString();
          Uri uri3 = (Uri)Uri.CREATOR.createFromParcel((Parcel)bundle1);
          bundle1 = bundle1.readBundle();
          paramInt1 = delete(str7, str6, uri3, bundle1);
          paramParcel2.writeNoException();
          paramParcel2.writeInt(paramInt1);
          return true;
        } 
        bundle1.enforceInterface("android.content.IContentProvider");
        String str5 = bundle1.readString();
        String str4 = bundle1.readString();
        Uri uri2 = (Uri)Uri.CREATOR.createFromParcel((Parcel)bundle1);
        arrayOfString = (String[])ContentValues.CREATOR.createFromParcel((Parcel)bundle1);
        Bundle bundle1 = bundle1.readBundle();
        uri1 = insert(str5, str4, uri2, (ContentValues)arrayOfString, bundle1);
        paramParcel2.writeNoException();
        Uri.writeToParcel(paramParcel2, uri1);
        return true;
      } 
      uri1.enforceInterface("android.content.IContentProvider");
      Uri uri1 = (Uri)Uri.CREATOR.createFromParcel((Parcel)uri1);
      str1 = getType(uri1);
      paramParcel2.writeNoException();
      paramParcel2.writeString(str1);
      return true;
    } 
    str1.enforceInterface("android.content.IContentProvider");
    String str2 = str1.readString();
    String str3 = str1.readString();
    Uri uri = (Uri)Uri.CREATOR.createFromParcel((Parcel)str1);
    paramInt2 = str1.readInt();
    if (paramInt2 > 0) {
      arrayOfString = new String[paramInt2];
      for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++)
        arrayOfString[paramInt1] = str1.readString(); 
    } else {
      arrayOfString = null;
    } 
    Bundle bundle = str1.readBundle();
    IBinder iBinder2 = str1.readStrongBinder();
    IContentObserver iContentObserver = IContentObserver.Stub.asInterface(iBinder2);
    IBinder iBinder1 = str1.readStrongBinder();
    ICancellationSignal iCancellationSignal = ICancellationSignal.Stub.asInterface(iBinder1);
    Cursor cursor = query(str2, str3, uri, arrayOfString, bundle, iCancellationSignal);
    if (cursor != null) {
      CursorToBulkCursorAdaptor cursorToBulkCursorAdaptor;
      str3 = null;
      Cursor cursor1 = cursor;
      String str = str3;
      try {
        CursorToBulkCursorAdaptor cursorToBulkCursorAdaptor1 = new CursorToBulkCursorAdaptor();
        cursor1 = cursor;
        str = str3;
        this(cursor, iContentObserver, getProviderName());
        cursor = null;
        cursor1 = cursor;
        cursorToBulkCursorAdaptor = cursorToBulkCursorAdaptor1;
        BulkCursorDescriptor bulkCursorDescriptor = cursorToBulkCursorAdaptor1.getBulkCursorDescriptor();
        cursorToBulkCursorAdaptor1 = null;
        cursor1 = cursor;
        cursorToBulkCursorAdaptor = cursorToBulkCursorAdaptor1;
        paramParcel2.writeNoException();
        cursor1 = cursor;
        cursorToBulkCursorAdaptor = cursorToBulkCursorAdaptor1;
        paramParcel2.writeInt(1);
        cursor1 = cursor;
        cursorToBulkCursorAdaptor = cursorToBulkCursorAdaptor1;
        bulkCursorDescriptor.writeToParcel(paramParcel2, 1);
      } finally {
        if (cursorToBulkCursorAdaptor != null)
          cursorToBulkCursorAdaptor.close(); 
        if (cursor1 != null)
          cursor1.close(); 
      } 
    } else {
      paramParcel2.writeNoException();
      paramParcel2.writeInt(0);
    } 
    return true;
  }
  
  public IBinder asBinder() {
    return (IBinder)this;
  }
  
  public abstract String getProviderName();
}
