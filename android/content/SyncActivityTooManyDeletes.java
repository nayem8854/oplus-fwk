package android.content;

import android.accounts.Account;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class SyncActivityTooManyDeletes extends Activity implements AdapterView.OnItemClickListener {
  private Account mAccount;
  
  private String mAuthority;
  
  private long mNumDeletes;
  
  private String mProvider;
  
  protected void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    paramBundle = getIntent().getExtras();
    if (paramBundle == null) {
      finish();
      return;
    } 
    this.mNumDeletes = paramBundle.getLong("numDeletes");
    this.mAccount = (Account)paramBundle.getParcelable("account");
    this.mAuthority = paramBundle.getString("authority");
    this.mProvider = paramBundle.getString("provider");
    CharSequence charSequence2 = getResources().getText(17041381);
    CharSequence charSequence1 = getResources().getText(17041380);
    ArrayAdapter arrayAdapter = new ArrayAdapter((Context)this, 17367043, 16908308, (Object[])new CharSequence[] { charSequence2, charSequence1 });
    ListView listView = new ListView((Context)this);
    listView.setAdapter((ListAdapter)arrayAdapter);
    listView.setItemsCanFocus(true);
    listView.setOnItemClickListener(this);
    TextView textView = new TextView((Context)this);
    CharSequence charSequence3 = getResources().getText(17041383);
    charSequence3 = charSequence3.toString();
    long l = this.mNumDeletes;
    String str1 = this.mProvider, str2 = this.mAccount.name;
    textView.setText(String.format((String)charSequence3, new Object[] { Long.valueOf(l), str1, str2 }));
    LinearLayout linearLayout = new LinearLayout((Context)this);
    linearLayout.setOrientation(1);
    LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2, 0.0F);
    linearLayout.addView((View)textView, (ViewGroup.LayoutParams)layoutParams);
    linearLayout.addView((View)listView, (ViewGroup.LayoutParams)layoutParams);
    setContentView((View)linearLayout);
  }
  
  public void onItemClick(AdapterView<?> paramAdapterView, View paramView, int paramInt, long paramLong) {
    if (paramInt == 0)
      startSyncReallyDelete(); 
    finish();
  }
  
  private void startSyncReallyDelete() {
    Bundle bundle = new Bundle();
    bundle.putBoolean("deletions_override", true);
    bundle.putBoolean("force", true);
    bundle.putBoolean("expedited", true);
    bundle.putBoolean("upload", true);
    ContentResolver.requestSync(this.mAccount, this.mAuthority, bundle);
  }
  
  private void startSyncUndoDeletes() {
    Bundle bundle = new Bundle();
    bundle.putBoolean("discard_deletions", true);
    bundle.putBoolean("force", true);
    bundle.putBoolean("expedited", true);
    bundle.putBoolean("upload", true);
    ContentResolver.requestSync(this.mAccount, this.mAuthority, bundle);
  }
}
