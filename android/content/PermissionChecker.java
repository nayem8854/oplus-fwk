package android.content;

import android.app.AppOpsManager;
import android.content.pm.PackageManager;
import android.content.pm.PermissionInfo;
import android.os.Binder;
import android.os.Process;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public final class PermissionChecker {
  public static final int PERMISSION_GRANTED = 0;
  
  public static final int PERMISSION_HARD_DENIED = -1;
  
  public static final int PERMISSION_SOFT_DENIED = -2;
  
  public static final int PID_UNKNOWN = -1;
  
  public static int checkPermissionForDataDelivery(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4) {
    return checkPermissionCommon(paramContext, paramString1, paramInt1, paramInt2, paramString2, paramString3, paramString4, true);
  }
  
  public static int checkPermissionForPreflight(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2) {
    return checkPermissionCommon(paramContext, paramString1, paramInt1, paramInt2, paramString2, null, null, false);
  }
  
  public static int checkSelfPermissionForDataDelivery(Context paramContext, String paramString1, String paramString2) {
    int i = Process.myPid();
    int j = Process.myUid();
    String str1 = paramContext.getPackageName(), str2 = paramContext.getAttributionTag();
    return checkPermissionForDataDelivery(paramContext, paramString1, i, j, str1, str2, paramString2);
  }
  
  public static int checkSelfPermissionForPreflight(Context paramContext, String paramString) {
    int i = Process.myPid();
    int j = Process.myUid();
    String str = paramContext.getPackageName();
    return checkPermissionForPreflight(paramContext, paramString, i, j, str);
  }
  
  public static int checkCallingPermissionForDataDelivery(Context paramContext, String paramString1, String paramString2, String paramString3, String paramString4) {
    if (Binder.getCallingPid() == Process.myPid())
      return -1; 
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    return checkPermissionForDataDelivery(paramContext, paramString1, i, j, paramString2, paramString3, paramString4);
  }
  
  public static int checkCallingPermissionForPreflight(Context paramContext, String paramString1, String paramString2) {
    if (Binder.getCallingPid() == Process.myPid())
      return -1; 
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    return checkPermissionForPreflight(paramContext, paramString1, i, j, paramString2);
  }
  
  public static int checkCallingOrSelfPermissionForDataDelivery(Context paramContext, String paramString1, String paramString2, String paramString3) {
    String str;
    if (Binder.getCallingPid() == Process.myPid()) {
      str = paramContext.getPackageName();
    } else {
      str = null;
    } 
    if (Binder.getCallingPid() == Process.myPid())
      paramString2 = paramContext.getAttributionTag(); 
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    return checkPermissionForDataDelivery(paramContext, paramString1, i, j, str, paramString2, paramString3);
  }
  
  public static int checkCallingOrSelfPermissionForPreflight(Context paramContext, String paramString) {
    String str;
    if (Binder.getCallingPid() == Process.myPid()) {
      str = paramContext.getPackageName();
    } else {
      str = null;
    } 
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    return checkPermissionForPreflight(paramContext, paramString, i, j, str);
  }
  
  static int checkPermissionCommon(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4, boolean paramBoolean) {
    try {
      PackageManager packageManager = paramContext.getPackageManager();
      try {
        PermissionInfo permissionInfo = packageManager.getPermissionInfo(paramString1, 0);
        if (paramString2 == null) {
          String[] arrayOfString = paramContext.getPackageManager().getPackagesForUid(paramInt2);
          if (arrayOfString != null && arrayOfString.length > 0)
            paramString2 = arrayOfString[0]; 
        } 
        if (permissionInfo.isAppOp())
          return checkAppOpPermission(paramContext, paramString1, paramInt1, paramInt2, paramString2, paramString3, paramString4, paramBoolean); 
        if (permissionInfo.isRuntime())
          return checkRuntimePermission(paramContext, paramString1, paramInt1, paramInt2, paramString2, paramString3, paramString4, paramBoolean); 
        return paramContext.checkPermission(paramString1, paramInt1, paramInt2);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {}
    return -1;
  }
  
  private static int checkAppOpPermission(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4, boolean paramBoolean) {
    int i;
    String str = AppOpsManager.permissionToOp(paramString1);
    byte b = -1;
    if (str == null || paramString2 == null)
      return -1; 
    AppOpsManager appOpsManager = paramContext.<AppOpsManager>getSystemService(AppOpsManager.class);
    if (paramBoolean) {
      i = appOpsManager.noteProxyOpNoThrow(str, paramString2, paramInt2, paramString3, paramString4);
    } else {
      i = appOpsManager.unsafeCheckOpRawNoThrow(str, paramInt2, paramString2);
    } 
    if (i != 0)
      if (i != 3) {
        if (i != 4)
          return -1; 
      } else {
        if (paramContext.checkPermission(paramString1, paramInt1, paramInt2) == 0) {
          paramInt1 = 0;
        } else {
          paramInt1 = b;
        } 
        return paramInt1;
      }  
    return 0;
  }
  
  private static int checkRuntimePermission(Context paramContext, String paramString1, int paramInt1, int paramInt2, String paramString2, String paramString3, String paramString4, boolean paramBoolean) {
    if (paramContext.checkPermission(paramString1, paramInt1, paramInt2) == -1)
      return -1; 
    paramString1 = AppOpsManager.permissionToOp(paramString1);
    if (paramString1 == null || paramString2 == null)
      return 0; 
    AppOpsManager appOpsManager = paramContext.<AppOpsManager>getSystemService(AppOpsManager.class);
    if (paramBoolean) {
      paramInt1 = appOpsManager.noteProxyOpNoThrow(paramString1, paramString2, paramInt2, paramString3, paramString4);
    } else {
      paramInt1 = appOpsManager.unsafeCheckOpRawNoThrow(paramString1, paramInt2, paramString2);
    } 
    if (paramInt1 != 0 && paramInt1 != 4)
      return -2; 
    return 0;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PermissionResult {}
}
