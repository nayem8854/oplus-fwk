package android.content;

import android.os.IBinder;

public interface ServiceConnection {
  default void onBindingDied(ComponentName paramComponentName) {}
  
  default void onNullBinding(ComponentName paramComponentName) {}
  
  void onServiceConnected(ComponentName paramComponentName, IBinder paramIBinder);
  
  void onServiceDisconnected(ComponentName paramComponentName);
}
