package android.content.res;

import android.app.WindowConfiguration;
import android.os.Build;
import android.os.LocaleList;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.proto.ProtoInputStream;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Locale;
import oplus.content.res.OplusExtraConfiguration;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class Configuration extends OplusBaseConfiguration implements Parcelable, Comparable<Configuration> {
  public static final int ASSETS_SEQ_UNDEFINED = 0;
  
  public static final int COLOR_MODE_HDR_MASK = 12;
  
  public static final int COLOR_MODE_HDR_NO = 4;
  
  public static final int COLOR_MODE_HDR_SHIFT = 2;
  
  public static final int COLOR_MODE_HDR_UNDEFINED = 0;
  
  public static final int COLOR_MODE_HDR_YES = 8;
  
  public static final int COLOR_MODE_UNDEFINED = 0;
  
  public static final int COLOR_MODE_WIDE_COLOR_GAMUT_MASK = 3;
  
  public static final int COLOR_MODE_WIDE_COLOR_GAMUT_NO = 1;
  
  public static final int COLOR_MODE_WIDE_COLOR_GAMUT_UNDEFINED = 0;
  
  public static final int COLOR_MODE_WIDE_COLOR_GAMUT_YES = 2;
  
  public static final Parcelable.Creator<Configuration> CREATOR;
  
  public static final int DENSITY_DPI_ANY = 65534;
  
  public static final int DENSITY_DPI_NONE = 65535;
  
  public static final int DENSITY_DPI_UNDEFINED = 0;
  
  public static final Configuration EMPTY = new Configuration();
  
  public static final int HARDKEYBOARDHIDDEN_NO = 1;
  
  public static final int HARDKEYBOARDHIDDEN_UNDEFINED = 0;
  
  public static final int HARDKEYBOARDHIDDEN_YES = 2;
  
  public static final int KEYBOARDHIDDEN_NO = 1;
  
  public static final int KEYBOARDHIDDEN_SOFT = 3;
  
  public static final int KEYBOARDHIDDEN_UNDEFINED = 0;
  
  public static final int KEYBOARDHIDDEN_YES = 2;
  
  public static final int KEYBOARD_12KEY = 3;
  
  public static final int KEYBOARD_NOKEYS = 1;
  
  public static final int KEYBOARD_QWERTY = 2;
  
  public static final int KEYBOARD_UNDEFINED = 0;
  
  public static final int MNC_ZERO = 65535;
  
  public static final int NATIVE_CONFIG_COLOR_MODE = 65536;
  
  public static final int NATIVE_CONFIG_DENSITY = 256;
  
  public static final int NATIVE_CONFIG_KEYBOARD = 16;
  
  public static final int NATIVE_CONFIG_KEYBOARD_HIDDEN = 32;
  
  public static final int NATIVE_CONFIG_LAYOUTDIR = 16384;
  
  public static final int NATIVE_CONFIG_LOCALE = 4;
  
  public static final int NATIVE_CONFIG_MCC = 1;
  
  public static final int NATIVE_CONFIG_MNC = 2;
  
  public static final int NATIVE_CONFIG_NAVIGATION = 64;
  
  public static final int NATIVE_CONFIG_ORIENTATION = 128;
  
  public static final int NATIVE_CONFIG_SCREEN_LAYOUT = 2048;
  
  public static final int NATIVE_CONFIG_SCREEN_SIZE = 512;
  
  public static final int NATIVE_CONFIG_SMALLEST_SCREEN_SIZE = 8192;
  
  public static final int NATIVE_CONFIG_TOUCHSCREEN = 8;
  
  public static final int NATIVE_CONFIG_UI_MODE = 4096;
  
  public static final int NATIVE_CONFIG_VERSION = 1024;
  
  public static final int NAVIGATIONHIDDEN_NO = 1;
  
  public static final int NAVIGATIONHIDDEN_UNDEFINED = 0;
  
  public static final int NAVIGATIONHIDDEN_YES = 2;
  
  public static final int NAVIGATION_DPAD = 2;
  
  public static final int NAVIGATION_NONAV = 1;
  
  public static final int NAVIGATION_TRACKBALL = 3;
  
  public static final int NAVIGATION_UNDEFINED = 0;
  
  public static final int NAVIGATION_WHEEL = 4;
  
  public static final int ORIENTATION_LANDSCAPE = 2;
  
  public static final int ORIENTATION_PORTRAIT = 1;
  
  @Deprecated
  public static final int ORIENTATION_SQUARE = 3;
  
  public static final int ORIENTATION_UNDEFINED = 0;
  
  public static final int SCREENLAYOUT_COMPAT_NEEDED = 268435456;
  
  public static final int SCREENLAYOUT_LAYOUTDIR_LTR = 64;
  
  public static final int SCREENLAYOUT_LAYOUTDIR_MASK = 192;
  
  public static final int SCREENLAYOUT_LAYOUTDIR_RTL = 128;
  
  public static final int SCREENLAYOUT_LAYOUTDIR_SHIFT = 6;
  
  public static final int SCREENLAYOUT_LAYOUTDIR_UNDEFINED = 0;
  
  public static final int SCREENLAYOUT_LONG_MASK = 48;
  
  public static final int SCREENLAYOUT_LONG_NO = 16;
  
  public static final int SCREENLAYOUT_LONG_UNDEFINED = 0;
  
  public static final int SCREENLAYOUT_LONG_YES = 32;
  
  public static final int SCREENLAYOUT_ROUND_MASK = 768;
  
  public static final int SCREENLAYOUT_ROUND_NO = 256;
  
  public static final int SCREENLAYOUT_ROUND_SHIFT = 8;
  
  public static final int SCREENLAYOUT_ROUND_UNDEFINED = 0;
  
  public static final int SCREENLAYOUT_ROUND_YES = 512;
  
  public static final int SCREENLAYOUT_SIZE_LARGE = 3;
  
  public static final int SCREENLAYOUT_SIZE_MASK = 15;
  
  public static final int SCREENLAYOUT_SIZE_NORMAL = 2;
  
  public static final int SCREENLAYOUT_SIZE_SMALL = 1;
  
  public static final int SCREENLAYOUT_SIZE_UNDEFINED = 0;
  
  public static final int SCREENLAYOUT_SIZE_XLARGE = 4;
  
  public static final int SCREENLAYOUT_UNDEFINED = 0;
  
  public static final int SCREEN_HEIGHT_DP_UNDEFINED = 0;
  
  public static final int SCREEN_WIDTH_DP_UNDEFINED = 0;
  
  public static final int SMALLEST_SCREEN_WIDTH_DP_UNDEFINED = 0;
  
  private static final String TAG = "Configuration";
  
  public static final int TOUCHSCREEN_FINGER = 3;
  
  public static final int TOUCHSCREEN_NOTOUCH = 1;
  
  @Deprecated
  public static final int TOUCHSCREEN_STYLUS = 2;
  
  public static final int TOUCHSCREEN_UNDEFINED = 0;
  
  public static final int UI_MODE_NIGHT_MASK = 48;
  
  public static final int UI_MODE_NIGHT_NO = 16;
  
  public static final int UI_MODE_NIGHT_UNDEFINED = 0;
  
  public static final int UI_MODE_NIGHT_YES = 32;
  
  public static final int UI_MODE_TYPE_APPLIANCE = 5;
  
  public static final int UI_MODE_TYPE_CAR = 3;
  
  public static final int UI_MODE_TYPE_DESK = 2;
  
  public static final int UI_MODE_TYPE_MASK = 15;
  
  public static final int UI_MODE_TYPE_NORMAL = 1;
  
  public static final int UI_MODE_TYPE_TELEVISION = 4;
  
  public static final int UI_MODE_TYPE_UNDEFINED = 0;
  
  public static final int UI_MODE_TYPE_VR_HEADSET = 7;
  
  public static final int UI_MODE_TYPE_WATCH = 6;
  
  private static final String XML_ATTR_APP_BOUNDS = "app_bounds";
  
  private static final String XML_ATTR_COLOR_MODE = "clrMod";
  
  private static final String XML_ATTR_DENSITY = "density";
  
  private static final String XML_ATTR_FONT_SCALE = "fs";
  
  private static final String XML_ATTR_HARD_KEYBOARD_HIDDEN = "hardKeyHid";
  
  private static final String XML_ATTR_KEYBOARD = "key";
  
  private static final String XML_ATTR_KEYBOARD_HIDDEN = "keyHid";
  
  private static final String XML_ATTR_LOCALES = "locales";
  
  private static final String XML_ATTR_MCC = "mcc";
  
  private static final String XML_ATTR_MNC = "mnc";
  
  private static final String XML_ATTR_NAVIGATION = "nav";
  
  private static final String XML_ATTR_NAVIGATION_HIDDEN = "navHid";
  
  private static final String XML_ATTR_ORIENTATION = "ori";
  
  private static final String XML_ATTR_ROTATION = "rot";
  
  private static final String XML_ATTR_SCREEN_HEIGHT = "height";
  
  private static final String XML_ATTR_SCREEN_LAYOUT = "scrLay";
  
  private static final String XML_ATTR_SCREEN_WIDTH = "width";
  
  private static final String XML_ATTR_SMALLEST_WIDTH = "sw";
  
  private static final String XML_ATTR_TOUCHSCREEN = "touch";
  
  private static final String XML_ATTR_UI_MODE = "ui";
  
  public int assetsSeq;
  
  public int colorMode;
  
  public int compatScreenHeightDp;
  
  public int compatScreenWidthDp;
  
  public int compatSmallestScreenWidthDp;
  
  public int densityDpi;
  
  public float fontScale;
  
  public int hardKeyboardHidden;
  
  public int keyboard;
  
  public int keyboardHidden;
  
  @Deprecated
  public Locale locale;
  
  private LocaleList mLocaleList;
  
  public int mcc;
  
  public int mnc;
  
  public int navigation;
  
  public int navigationHidden;
  
  public int orientation;
  
  public int screenHeightDp;
  
  public int screenLayout;
  
  public int screenWidthDp;
  
  public int seq;
  
  public int smallestScreenWidthDp;
  
  public int touchscreen;
  
  public int uiMode;
  
  public boolean userSetLocale;
  
  public final WindowConfiguration windowConfiguration = new WindowConfiguration();
  
  public static int resetScreenLayout(int paramInt) {
    return 0xEFFFFFC0 & paramInt | 0x24;
  }
  
  public static int reduceScreenLayout(int paramInt1, int paramInt2, int paramInt3) {
    byte b;
    boolean bool;
    if (paramInt2 < 470) {
      b = 1;
      paramInt2 = 0;
      bool = false;
    } else {
      if (paramInt2 >= 960 && paramInt3 >= 720) {
        b = 4;
      } else if (paramInt2 >= 640 && paramInt3 >= 480) {
        b = 3;
      } else {
        b = 2;
      } 
      if (paramInt3 > 321 || paramInt2 > 570) {
        bool = true;
      } else {
        bool = false;
      } 
      if (paramInt2 * 3 / 5 >= paramInt3 - 1) {
        paramInt2 = 1;
      } else {
        paramInt2 = 0;
      } 
    } 
    paramInt3 = paramInt1;
    if (paramInt2 == 0)
      paramInt3 = paramInt1 & 0xFFFFFFCF | 0x10; 
    paramInt1 = paramInt3;
    if (bool)
      paramInt1 = paramInt3 | 0x10000000; 
    paramInt2 = paramInt1;
    if (b < (paramInt1 & 0xF))
      paramInt2 = paramInt1 & 0xFFFFFFF0 | b; 
    return paramInt2;
  }
  
  public static String configurationDiffToString(int paramInt) {
    ArrayList<String> arrayList = new ArrayList();
    if ((paramInt & 0x1) != 0)
      arrayList.add("CONFIG_MCC"); 
    if ((paramInt & 0x2) != 0)
      arrayList.add("CONFIG_MNC"); 
    if ((paramInt & 0x4) != 0)
      arrayList.add("CONFIG_LOCALE"); 
    if ((paramInt & 0x8) != 0)
      arrayList.add("CONFIG_TOUCHSCREEN"); 
    if ((paramInt & 0x10) != 0)
      arrayList.add("CONFIG_KEYBOARD"); 
    if ((paramInt & 0x20) != 0)
      arrayList.add("CONFIG_KEYBOARD_HIDDEN"); 
    if ((paramInt & 0x40) != 0)
      arrayList.add("CONFIG_NAVIGATION"); 
    if ((paramInt & 0x80) != 0)
      arrayList.add("CONFIG_ORIENTATION"); 
    if ((paramInt & 0x100) != 0)
      arrayList.add("CONFIG_SCREEN_LAYOUT"); 
    if ((paramInt & 0x4000) != 0)
      arrayList.add("CONFIG_COLOR_MODE"); 
    if ((paramInt & 0x200) != 0)
      arrayList.add("CONFIG_UI_MODE"); 
    if ((paramInt & 0x400) != 0)
      arrayList.add("CONFIG_SCREEN_SIZE"); 
    if ((paramInt & 0x800) != 0)
      arrayList.add("CONFIG_SMALLEST_SCREEN_SIZE"); 
    if ((paramInt & 0x1000) != 0)
      arrayList.add("CONFIG_DENSITY"); 
    if ((paramInt & 0x2000) != 0)
      arrayList.add("CONFIG_LAYOUT_DIRECTION"); 
    if ((0x40000000 & paramInt) != 0)
      arrayList.add("CONFIG_FONT_SCALE"); 
    if ((Integer.MIN_VALUE & paramInt) != 0)
      arrayList.add("CONFIG_ASSETS_PATHS"); 
    if ((0x20000000 & paramInt) != 0)
      arrayList.add("CONFIG_WINDOW_CONFIGURATION"); 
    StringBuilder stringBuilder = new StringBuilder("{");
    int i;
    for (paramInt = 0, i = arrayList.size(); paramInt < i; paramInt++) {
      stringBuilder.append(arrayList.get(paramInt));
      if (paramInt != i - 1)
        stringBuilder.append(", "); 
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public boolean isLayoutSizeAtLeast(int paramInt) {
    int i = this.screenLayout & 0xF;
    boolean bool = false;
    if (i == 0)
      return false; 
    if (i >= paramInt)
      bool = true; 
    return bool;
  }
  
  public Configuration() {
    this.mOplusExtraConfiguration = new OplusExtraConfiguration();
    unset();
  }
  
  public Configuration(Configuration paramConfiguration) {
    this.mOplusExtraConfiguration = new OplusExtraConfiguration();
    setTo(paramConfiguration);
  }
  
  private void fixUpLocaleList() {
    // Byte code:
    //   0: aload_0
    //   1: getfield locale : Ljava/util/Locale;
    //   4: ifnonnull -> 17
    //   7: aload_0
    //   8: getfield mLocaleList : Landroid/os/LocaleList;
    //   11: invokevirtual isEmpty : ()Z
    //   14: ifeq -> 43
    //   17: aload_0
    //   18: getfield locale : Ljava/util/Locale;
    //   21: astore_1
    //   22: aload_1
    //   23: ifnull -> 81
    //   26: aload_0
    //   27: getfield mLocaleList : Landroid/os/LocaleList;
    //   30: astore_2
    //   31: aload_1
    //   32: aload_2
    //   33: iconst_0
    //   34: invokevirtual get : (I)Ljava/util/Locale;
    //   37: invokevirtual equals : (Ljava/lang/Object;)Z
    //   40: ifne -> 81
    //   43: aload_0
    //   44: getfield locale : Ljava/util/Locale;
    //   47: ifnonnull -> 57
    //   50: invokestatic getEmptyLocaleList : ()Landroid/os/LocaleList;
    //   53: astore_1
    //   54: goto -> 76
    //   57: new android/os/LocaleList
    //   60: dup
    //   61: iconst_1
    //   62: anewarray java/util/Locale
    //   65: dup
    //   66: iconst_0
    //   67: aload_0
    //   68: getfield locale : Ljava/util/Locale;
    //   71: aastore
    //   72: invokespecial <init> : ([Ljava/util/Locale;)V
    //   75: astore_1
    //   76: aload_0
    //   77: aload_1
    //   78: putfield mLocaleList : Landroid/os/LocaleList;
    //   81: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #930	-> 0
    //   #931	-> 31
    //   #932	-> 43
    //   #934	-> 81
  }
  
  public void setTo(Configuration paramConfiguration) {
    this.fontScale = paramConfiguration.fontScale;
    this.mcc = paramConfiguration.mcc;
    this.mnc = paramConfiguration.mnc;
    Locale locale = paramConfiguration.locale;
    if (locale == null) {
      locale = null;
    } else {
      locale = (Locale)locale.clone();
    } 
    this.locale = locale;
    paramConfiguration.fixUpLocaleList();
    this.mLocaleList = paramConfiguration.mLocaleList;
    this.userSetLocale = paramConfiguration.userSetLocale;
    this.touchscreen = paramConfiguration.touchscreen;
    this.keyboard = paramConfiguration.keyboard;
    this.keyboardHidden = paramConfiguration.keyboardHidden;
    this.hardKeyboardHidden = paramConfiguration.hardKeyboardHidden;
    this.navigation = paramConfiguration.navigation;
    this.navigationHidden = paramConfiguration.navigationHidden;
    this.orientation = paramConfiguration.orientation;
    this.screenLayout = paramConfiguration.screenLayout;
    this.colorMode = paramConfiguration.colorMode;
    this.uiMode = paramConfiguration.uiMode;
    this.screenWidthDp = paramConfiguration.screenWidthDp;
    this.screenHeightDp = paramConfiguration.screenHeightDp;
    this.smallestScreenWidthDp = paramConfiguration.smallestScreenWidthDp;
    this.densityDpi = paramConfiguration.densityDpi;
    this.compatScreenWidthDp = paramConfiguration.compatScreenWidthDp;
    this.compatScreenHeightDp = paramConfiguration.compatScreenHeightDp;
    this.compatSmallestScreenWidthDp = paramConfiguration.compatSmallestScreenWidthDp;
    this.assetsSeq = paramConfiguration.assetsSeq;
    this.seq = paramConfiguration.seq;
    this.windowConfiguration.setTo(paramConfiguration.windowConfiguration);
    this.mOplusExtraConfiguration.setTo(paramConfiguration.mOplusExtraConfiguration);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("{");
    stringBuilder.append(this.fontScale);
    stringBuilder.append(" ");
    if (this.mcc != 0) {
      stringBuilder.append("??");
      stringBuilder.append("mcc");
    } else {
      stringBuilder.append("?mcc");
    } 
    if (this.mnc != 0) {
      stringBuilder.append("??");
      stringBuilder.append("mnc");
    } else {
      stringBuilder.append("?mnc");
    } 
    fixUpLocaleList();
    if (!this.mLocaleList.isEmpty()) {
      stringBuilder.append(" ");
      stringBuilder.append(this.mLocaleList);
    } else {
      stringBuilder.append(" ?localeList");
    } 
    int i = this.screenLayout & 0xC0;
    if (i != 0) {
      if (i != 64) {
        if (i != 128) {
          stringBuilder.append(" layoutDir=");
          stringBuilder.append(i >> 6);
        } else {
          stringBuilder.append(" ldrtl");
        } 
      } else {
        stringBuilder.append(" ldltr");
      } 
    } else {
      stringBuilder.append(" ?layoutDir");
    } 
    if (this.smallestScreenWidthDp != 0) {
      stringBuilder.append(" sw");
      stringBuilder.append(this.smallestScreenWidthDp);
      stringBuilder.append("dp");
    } else {
      stringBuilder.append(" ?swdp");
    } 
    if (this.screenWidthDp != 0) {
      stringBuilder.append(" w");
      stringBuilder.append(this.screenWidthDp);
      stringBuilder.append("dp");
    } else {
      stringBuilder.append(" ?wdp");
    } 
    if (this.screenHeightDp != 0) {
      stringBuilder.append(" h");
      stringBuilder.append(this.screenHeightDp);
      stringBuilder.append("dp");
    } else {
      stringBuilder.append(" ?hdp");
    } 
    if (this.densityDpi != 0) {
      stringBuilder.append(" ");
      stringBuilder.append(this.densityDpi);
      stringBuilder.append("dpi");
    } else {
      stringBuilder.append(" ?density");
    } 
    i = this.screenLayout & 0xF;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              stringBuilder.append(" layoutSize=");
              stringBuilder.append(this.screenLayout & 0xF);
            } else {
              stringBuilder.append(" xlrg");
            } 
          } else {
            stringBuilder.append(" lrg");
          } 
        } else {
          stringBuilder.append(" nrml");
        } 
      } else {
        stringBuilder.append(" smll");
      } 
    } else {
      stringBuilder.append(" ?lsize");
    } 
    i = this.screenLayout & 0x30;
    if (i != 0) {
      if (i != 16)
        if (i != 32) {
          stringBuilder.append(" layoutLong=");
          stringBuilder.append(this.screenLayout & 0x30);
        } else {
          stringBuilder.append(" long");
        }  
    } else {
      stringBuilder.append(" ?long");
    } 
    i = this.colorMode & 0xC;
    if (i != 0) {
      if (i != 4)
        if (i != 8) {
          stringBuilder.append(" dynamicRange=");
          stringBuilder.append(this.colorMode & 0xC);
        } else {
          stringBuilder.append(" hdr");
        }  
    } else {
      stringBuilder.append(" ?ldr");
    } 
    i = this.colorMode & 0x3;
    if (i != 0) {
      if (i != 1)
        if (i != 2) {
          stringBuilder.append(" wideColorGamut=");
          stringBuilder.append(this.colorMode & 0x3);
        } else {
          stringBuilder.append(" widecg");
        }  
    } else {
      stringBuilder.append(" ?wideColorGamut");
    } 
    i = this.orientation;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          stringBuilder.append(" orien=");
          stringBuilder.append(this.orientation);
        } else {
          stringBuilder.append(" land");
        } 
      } else {
        stringBuilder.append(" port");
      } 
    } else {
      stringBuilder.append(" ?orien");
    } 
    switch (this.uiMode & 0xF) {
      default:
        stringBuilder.append(" uimode=");
        stringBuilder.append(this.uiMode & 0xF);
        break;
      case 7:
        stringBuilder.append(" vrheadset");
        break;
      case 6:
        stringBuilder.append(" watch");
        break;
      case 5:
        stringBuilder.append(" appliance");
        break;
      case 4:
        stringBuilder.append(" television");
        break;
      case 3:
        stringBuilder.append(" car");
        break;
      case 2:
        stringBuilder.append(" desk");
        break;
      case 1:
        break;
      case 0:
        stringBuilder.append(" ?uimode");
        break;
    } 
    i = this.uiMode & 0x30;
    if (i != 0) {
      if (i != 16)
        if (i != 32) {
          stringBuilder.append(" night=");
          stringBuilder.append(this.uiMode & 0x30);
        } else {
          stringBuilder.append(" night");
        }  
    } else {
      stringBuilder.append(" ?night");
    } 
    i = this.touchscreen;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            stringBuilder.append(" touch=");
            stringBuilder.append(this.touchscreen);
          } else {
            stringBuilder.append(" finger");
          } 
        } else {
          stringBuilder.append(" stylus");
        } 
      } else {
        stringBuilder.append(" -touch");
      } 
    } else {
      stringBuilder.append(" ?touch");
    } 
    i = this.keyboard;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            stringBuilder.append(" keys=");
            stringBuilder.append(this.keyboard);
          } else {
            stringBuilder.append(" 12key");
          } 
        } else {
          stringBuilder.append(" qwerty");
        } 
      } else {
        stringBuilder.append(" -keyb");
      } 
    } else {
      stringBuilder.append(" ?keyb");
    } 
    i = this.keyboardHidden;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            stringBuilder.append("/");
            stringBuilder.append(this.keyboardHidden);
          } else {
            stringBuilder.append("/s");
          } 
        } else {
          stringBuilder.append("/h");
        } 
      } else {
        stringBuilder.append("/v");
      } 
    } else {
      stringBuilder.append("/?");
    } 
    i = this.hardKeyboardHidden;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          stringBuilder.append("/");
          stringBuilder.append(this.hardKeyboardHidden);
        } else {
          stringBuilder.append("/h");
        } 
      } else {
        stringBuilder.append("/v");
      } 
    } else {
      stringBuilder.append("/?");
    } 
    i = this.navigation;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i != 4) {
              stringBuilder.append(" nav=");
              stringBuilder.append(this.navigation);
            } else {
              stringBuilder.append(" wheel");
            } 
          } else {
            stringBuilder.append(" tball");
          } 
        } else {
          stringBuilder.append(" dpad");
        } 
      } else {
        stringBuilder.append(" -nav");
      } 
    } else {
      stringBuilder.append(" ?nav");
    } 
    i = this.navigationHidden;
    if (i != 0) {
      if (i != 1) {
        if (i != 2) {
          stringBuilder.append("/");
          stringBuilder.append(this.navigationHidden);
        } else {
          stringBuilder.append("/h");
        } 
      } else {
        stringBuilder.append("/v");
      } 
    } else {
      stringBuilder.append("/?");
    } 
    stringBuilder.append(" winConfig=");
    stringBuilder.append(this.windowConfiguration);
    if (this.assetsSeq != 0) {
      stringBuilder.append(" as.");
      stringBuilder.append(this.assetsSeq);
    } 
    if (this.seq != 0) {
      stringBuilder.append(" s.");
      stringBuilder.append(this.seq);
    } 
    stringBuilder.append(this.mOplusExtraConfiguration.toString());
    stringBuilder.append('}');
    return stringBuilder.toString();
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong, boolean paramBoolean1, boolean paramBoolean2) {
    paramLong = paramProtoOutputStream.start(paramLong);
    if (!paramBoolean2) {
      paramProtoOutputStream.write(1108101562369L, this.fontScale);
      paramProtoOutputStream.write(1155346202626L, this.mcc);
      paramProtoOutputStream.write(1155346202627L, this.mnc);
      LocaleList localeList = this.mLocaleList;
      if (localeList != null)
        paramProtoOutputStream.write(1138166333460L, localeList.toLanguageTags()); 
      paramProtoOutputStream.write(1155346202629L, this.screenLayout);
      paramProtoOutputStream.write(1155346202630L, this.colorMode);
      paramProtoOutputStream.write(1155346202631L, this.touchscreen);
      paramProtoOutputStream.write(1155346202632L, this.keyboard);
      paramProtoOutputStream.write(1155346202633L, this.keyboardHidden);
      paramProtoOutputStream.write(1155346202634L, this.hardKeyboardHidden);
      paramProtoOutputStream.write(1155346202635L, this.navigation);
      paramProtoOutputStream.write(1155346202636L, this.navigationHidden);
      paramProtoOutputStream.write(1155346202638L, this.uiMode);
      paramProtoOutputStream.write(1155346202641L, this.smallestScreenWidthDp);
      paramProtoOutputStream.write(1155346202642L, this.densityDpi);
      if (!paramBoolean1) {
        WindowConfiguration windowConfiguration = this.windowConfiguration;
        if (windowConfiguration != null)
          windowConfiguration.dumpDebug(paramProtoOutputStream, 1146756268051L); 
      } 
    } 
    paramProtoOutputStream.write(1155346202637L, this.orientation);
    paramProtoOutputStream.write(1155346202639L, this.screenWidthDp);
    paramProtoOutputStream.write(1155346202640L, this.screenHeightDp);
    paramProtoOutputStream.end(paramLong);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    dumpDebug(paramProtoOutputStream, paramLong, false, false);
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong, boolean paramBoolean) {
    dumpDebug(paramProtoOutputStream, paramLong, false, paramBoolean);
  }
  
  public void readFromProto(ProtoInputStream paramProtoInputStream, long paramLong) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: astore #4
    //   3: aload_1
    //   4: astore #5
    //   6: ldc_w ';variant-'
    //   9: astore #6
    //   11: ldc_w ';country-'
    //   14: astore #7
    //   16: ldc_w ''
    //   19: astore #8
    //   21: aload_1
    //   22: lload_2
    //   23: invokevirtual start : (J)J
    //   26: lstore_2
    //   27: new java/util/ArrayList
    //   30: dup
    //   31: invokespecial <init> : ()V
    //   34: astore #9
    //   36: aload_1
    //   37: invokevirtual nextField : ()I
    //   40: istore #10
    //   42: iload #10
    //   44: iconst_m1
    //   45: if_icmpeq -> 1409
    //   48: aload_1
    //   49: invokevirtual getFieldNumber : ()I
    //   52: istore #10
    //   54: iload #10
    //   56: tableswitch default -> 152, 1 -> 1348, 2 -> 1320, 3 -> 1270, 4 -> 437, 5 -> 414, 6 -> 398, 7 -> 382, 8 -> 366, 9 -> 350, 10 -> 334, 11 -> 318, 12 -> 302, 13 -> 286, 14 -> 270, 15 -> 254, 16 -> 238, 17 -> 222, 18 -> 206, 19 -> 190, 20 -> 155
    //   152: goto -> 1399
    //   155: aload #4
    //   157: aload #5
    //   159: ldc2_w 1138166333460
    //   162: invokevirtual readString : (J)Ljava/lang/String;
    //   165: invokestatic forLanguageTags : (Ljava/lang/String;)Landroid/os/LocaleList;
    //   168: invokevirtual setLocales : (Landroid/os/LocaleList;)V
    //   171: goto -> 1399
    //   174: astore #11
    //   176: ldc 'Configuration'
    //   178: ldc_w 'error parsing locale list in configuration.'
    //   181: aload #11
    //   183: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   186: pop
    //   187: goto -> 1399
    //   190: aload #4
    //   192: getfield windowConfiguration : Landroid/app/WindowConfiguration;
    //   195: aload #5
    //   197: ldc2_w 1146756268051
    //   200: invokevirtual readFromProto : (Landroid/util/proto/ProtoInputStream;J)V
    //   203: goto -> 1399
    //   206: aload #4
    //   208: aload #5
    //   210: ldc2_w 1155346202642
    //   213: invokevirtual readInt : (J)I
    //   216: putfield densityDpi : I
    //   219: goto -> 1399
    //   222: aload #4
    //   224: aload #5
    //   226: ldc2_w 1155346202641
    //   229: invokevirtual readInt : (J)I
    //   232: putfield smallestScreenWidthDp : I
    //   235: goto -> 1399
    //   238: aload #4
    //   240: aload #5
    //   242: ldc2_w 1155346202640
    //   245: invokevirtual readInt : (J)I
    //   248: putfield screenHeightDp : I
    //   251: goto -> 1399
    //   254: aload #4
    //   256: aload #5
    //   258: ldc2_w 1155346202639
    //   261: invokevirtual readInt : (J)I
    //   264: putfield screenWidthDp : I
    //   267: goto -> 1399
    //   270: aload #4
    //   272: aload #5
    //   274: ldc2_w 1155346202638
    //   277: invokevirtual readInt : (J)I
    //   280: putfield uiMode : I
    //   283: goto -> 1399
    //   286: aload #4
    //   288: aload #5
    //   290: ldc2_w 1155346202637
    //   293: invokevirtual readInt : (J)I
    //   296: putfield orientation : I
    //   299: goto -> 1399
    //   302: aload #4
    //   304: aload #5
    //   306: ldc2_w 1155346202636
    //   309: invokevirtual readInt : (J)I
    //   312: putfield navigationHidden : I
    //   315: goto -> 1399
    //   318: aload #4
    //   320: aload #5
    //   322: ldc2_w 1155346202635
    //   325: invokevirtual readInt : (J)I
    //   328: putfield navigation : I
    //   331: goto -> 1399
    //   334: aload #4
    //   336: aload #5
    //   338: ldc2_w 1155346202634
    //   341: invokevirtual readInt : (J)I
    //   344: putfield hardKeyboardHidden : I
    //   347: goto -> 1399
    //   350: aload #4
    //   352: aload #5
    //   354: ldc2_w 1155346202633
    //   357: invokevirtual readInt : (J)I
    //   360: putfield keyboardHidden : I
    //   363: goto -> 1399
    //   366: aload #4
    //   368: aload #5
    //   370: ldc2_w 1155346202632
    //   373: invokevirtual readInt : (J)I
    //   376: putfield keyboard : I
    //   379: goto -> 1399
    //   382: aload #4
    //   384: aload #5
    //   386: ldc2_w 1155346202631
    //   389: invokevirtual readInt : (J)I
    //   392: putfield touchscreen : I
    //   395: goto -> 1399
    //   398: aload #4
    //   400: aload #5
    //   402: ldc2_w 1155346202630
    //   405: invokevirtual readInt : (J)I
    //   408: putfield colorMode : I
    //   411: goto -> 1399
    //   414: aload #4
    //   416: aload #5
    //   418: ldc2_w 1155346202629
    //   421: invokevirtual readInt : (J)I
    //   424: putfield screenLayout : I
    //   427: goto -> 1399
    //   430: astore_1
    //   431: lload_2
    //   432: lstore #12
    //   434: goto -> 1462
    //   437: aload #5
    //   439: ldc2_w 2246267895812
    //   442: invokevirtual start : (J)J
    //   445: lstore #12
    //   447: aload #8
    //   449: astore #14
    //   451: aload #8
    //   453: astore #15
    //   455: aload #8
    //   457: astore #16
    //   459: aload #8
    //   461: astore #11
    //   463: aload_1
    //   464: invokevirtual nextField : ()I
    //   467: istore #10
    //   469: iload #10
    //   471: iconst_m1
    //   472: if_icmpeq -> 586
    //   475: aload_1
    //   476: invokevirtual getFieldNumber : ()I
    //   479: istore #10
    //   481: iload #10
    //   483: iconst_1
    //   484: if_icmpeq -> 559
    //   487: iload #10
    //   489: iconst_2
    //   490: if_icmpeq -> 542
    //   493: iload #10
    //   495: iconst_3
    //   496: if_icmpeq -> 525
    //   499: iload #10
    //   501: iconst_4
    //   502: if_icmpeq -> 508
    //   505: goto -> 573
    //   508: aload #5
    //   510: ldc2_w 1138166333444
    //   513: invokevirtual readString : (J)Ljava/lang/String;
    //   516: astore #4
    //   518: aload #4
    //   520: astore #11
    //   522: goto -> 573
    //   525: aload #5
    //   527: ldc2_w 1138166333443
    //   530: invokevirtual readString : (J)Ljava/lang/String;
    //   533: astore #4
    //   535: aload #4
    //   537: astore #16
    //   539: goto -> 573
    //   542: aload #5
    //   544: ldc2_w 1138166333442
    //   547: invokevirtual readString : (J)Ljava/lang/String;
    //   550: astore #4
    //   552: aload #4
    //   554: astore #15
    //   556: goto -> 573
    //   559: aload #5
    //   561: ldc2_w 1138166333441
    //   564: invokevirtual readString : (J)Ljava/lang/String;
    //   567: astore #4
    //   569: aload #4
    //   571: astore #14
    //   573: goto -> 463
    //   576: astore #5
    //   578: goto -> 912
    //   581: astore #5
    //   583: goto -> 907
    //   586: aload #5
    //   588: lload #12
    //   590: invokevirtual end : (J)V
    //   593: new java/util/Locale$Builder
    //   596: astore #5
    //   598: aload #5
    //   600: invokespecial <init> : ()V
    //   603: aload #5
    //   605: aload #14
    //   607: invokevirtual setLanguage : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   610: astore #5
    //   612: aload #5
    //   614: aload #15
    //   616: invokevirtual setRegion : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   619: astore #5
    //   621: aload #5
    //   623: aload #16
    //   625: invokevirtual setVariant : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   628: astore #5
    //   630: aload #5
    //   632: aload #11
    //   634: invokevirtual setScript : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   637: astore #5
    //   639: aload #5
    //   641: invokevirtual build : ()Ljava/util/Locale;
    //   644: astore #4
    //   646: aload #9
    //   648: aload #4
    //   650: invokeinterface indexOf : (Ljava/lang/Object;)I
    //   655: istore #10
    //   657: iload #10
    //   659: iconst_m1
    //   660: if_icmpeq -> 735
    //   663: new java/lang/StringBuilder
    //   666: astore #5
    //   668: aload #5
    //   670: invokespecial <init> : ()V
    //   673: aload #5
    //   675: ldc_w 'Repeated locale ('
    //   678: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   681: pop
    //   682: aload #5
    //   684: aload #9
    //   686: iload #10
    //   688: invokeinterface get : (I)Ljava/lang/Object;
    //   693: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   696: pop
    //   697: aload #5
    //   699: ldc_w ') found when trying to add: '
    //   702: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   705: pop
    //   706: aload #5
    //   708: aload #4
    //   710: invokevirtual toString : ()Ljava/lang/String;
    //   713: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   716: pop
    //   717: aload #5
    //   719: invokevirtual toString : ()Ljava/lang/String;
    //   722: astore #5
    //   724: ldc 'Configuration'
    //   726: aload #5
    //   728: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   731: pop
    //   732: goto -> 745
    //   735: aload #9
    //   737: aload #4
    //   739: invokeinterface add : (Ljava/lang/Object;)Z
    //   744: pop
    //   745: goto -> 862
    //   748: astore #5
    //   750: goto -> 775
    //   753: astore #5
    //   755: goto -> 775
    //   758: astore #5
    //   760: goto -> 775
    //   763: astore #5
    //   765: goto -> 775
    //   768: astore #5
    //   770: goto -> 775
    //   773: astore #5
    //   775: new java/lang/StringBuilder
    //   778: astore #5
    //   780: aload #5
    //   782: invokespecial <init> : ()V
    //   785: aload #5
    //   787: ldc_w 'readFromProto error building locale with: language-'
    //   790: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   793: pop
    //   794: aload #5
    //   796: aload #14
    //   798: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   801: pop
    //   802: aload #5
    //   804: aload #7
    //   806: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   809: pop
    //   810: aload #5
    //   812: aload #15
    //   814: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   817: pop
    //   818: aload #5
    //   820: aload #6
    //   822: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   825: pop
    //   826: aload #5
    //   828: aload #16
    //   830: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   833: pop
    //   834: aload #5
    //   836: ldc_w ';script-'
    //   839: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   842: pop
    //   843: aload #5
    //   845: aload #11
    //   847: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   850: pop
    //   851: ldc 'Configuration'
    //   853: aload #5
    //   855: invokevirtual toString : ()Ljava/lang/String;
    //   858: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   861: pop
    //   862: aload_0
    //   863: astore #4
    //   865: aload_1
    //   866: astore #5
    //   868: goto -> 1399
    //   871: astore #4
    //   873: aload_0
    //   874: astore #5
    //   876: aload_1
    //   877: astore #8
    //   879: aload #4
    //   881: astore_1
    //   882: goto -> 1385
    //   885: astore #5
    //   887: aload_1
    //   888: astore #8
    //   890: aload #5
    //   892: astore_1
    //   893: aload #8
    //   895: astore #5
    //   897: goto -> 1261
    //   900: astore #5
    //   902: goto -> 912
    //   905: astore #5
    //   907: aload #5
    //   909: athrow
    //   910: astore #5
    //   912: aload_1
    //   913: astore #4
    //   915: aload_1
    //   916: lload #12
    //   918: invokevirtual end : (J)V
    //   921: aload_1
    //   922: astore #4
    //   924: new java/util/Locale$Builder
    //   927: astore #8
    //   929: aload_1
    //   930: astore #4
    //   932: aload #8
    //   934: invokespecial <init> : ()V
    //   937: aload_1
    //   938: astore #4
    //   940: aload #8
    //   942: aload #14
    //   944: invokevirtual setLanguage : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   947: astore #8
    //   949: aload_1
    //   950: astore #4
    //   952: aload #8
    //   954: aload #15
    //   956: invokevirtual setRegion : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   959: astore #8
    //   961: aload_1
    //   962: astore #4
    //   964: aload #8
    //   966: aload #16
    //   968: invokevirtual setVariant : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   971: astore #8
    //   973: aload_1
    //   974: astore #4
    //   976: aload #8
    //   978: aload #11
    //   980: invokevirtual setScript : (Ljava/lang/String;)Ljava/util/Locale$Builder;
    //   983: astore #8
    //   985: aload_1
    //   986: astore #4
    //   988: aload #8
    //   990: invokevirtual build : ()Ljava/util/Locale;
    //   993: astore #8
    //   995: aload_1
    //   996: astore #4
    //   998: aload #9
    //   1000: aload #8
    //   1002: invokeinterface indexOf : (Ljava/lang/Object;)I
    //   1007: istore #10
    //   1009: iload #10
    //   1011: iconst_m1
    //   1012: if_icmpeq -> 1111
    //   1015: aload_1
    //   1016: astore #4
    //   1018: new java/lang/StringBuilder
    //   1021: astore #17
    //   1023: aload_1
    //   1024: astore #4
    //   1026: aload #17
    //   1028: invokespecial <init> : ()V
    //   1031: aload_1
    //   1032: astore #4
    //   1034: aload #17
    //   1036: ldc_w 'Repeated locale ('
    //   1039: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1042: pop
    //   1043: aload_1
    //   1044: astore #4
    //   1046: aload #17
    //   1048: aload #9
    //   1050: iload #10
    //   1052: invokeinterface get : (I)Ljava/lang/Object;
    //   1057: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   1060: pop
    //   1061: aload_1
    //   1062: astore #4
    //   1064: aload #17
    //   1066: ldc_w ') found when trying to add: '
    //   1069: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1072: pop
    //   1073: aload_1
    //   1074: astore #4
    //   1076: aload #17
    //   1078: aload #8
    //   1080: invokevirtual toString : ()Ljava/lang/String;
    //   1083: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1086: pop
    //   1087: aload_1
    //   1088: astore #4
    //   1090: aload #17
    //   1092: invokevirtual toString : ()Ljava/lang/String;
    //   1095: astore #8
    //   1097: aload_1
    //   1098: astore #4
    //   1100: ldc 'Configuration'
    //   1102: aload #8
    //   1104: invokestatic wtf : (Ljava/lang/String;Ljava/lang/String;)I
    //   1107: pop
    //   1108: goto -> 1124
    //   1111: aload_1
    //   1112: astore #4
    //   1114: aload #9
    //   1116: aload #8
    //   1118: invokeinterface add : (Ljava/lang/Object;)Z
    //   1123: pop
    //   1124: goto -> 1254
    //   1127: astore #8
    //   1129: goto -> 1134
    //   1132: astore #8
    //   1134: aload_1
    //   1135: astore #4
    //   1137: new java/lang/StringBuilder
    //   1140: astore #8
    //   1142: aload_1
    //   1143: astore #4
    //   1145: aload #8
    //   1147: invokespecial <init> : ()V
    //   1150: aload_1
    //   1151: astore #4
    //   1153: aload #8
    //   1155: ldc_w 'readFromProto error building locale with: language-'
    //   1158: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1161: pop
    //   1162: aload_1
    //   1163: astore #4
    //   1165: aload #8
    //   1167: aload #14
    //   1169: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1172: pop
    //   1173: aload_1
    //   1174: astore #4
    //   1176: aload #8
    //   1178: aload #7
    //   1180: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1183: pop
    //   1184: aload_1
    //   1185: astore #4
    //   1187: aload #8
    //   1189: aload #15
    //   1191: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1194: pop
    //   1195: aload_1
    //   1196: astore #4
    //   1198: aload #8
    //   1200: aload #6
    //   1202: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1205: pop
    //   1206: aload_1
    //   1207: astore #4
    //   1209: aload #8
    //   1211: aload #16
    //   1213: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1216: pop
    //   1217: aload_1
    //   1218: astore #4
    //   1220: aload #8
    //   1222: ldc_w ';script-'
    //   1225: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1228: pop
    //   1229: aload_1
    //   1230: astore #4
    //   1232: aload #8
    //   1234: aload #11
    //   1236: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   1239: pop
    //   1240: aload_1
    //   1241: astore #4
    //   1243: ldc 'Configuration'
    //   1245: aload #8
    //   1247: invokevirtual toString : ()Ljava/lang/String;
    //   1250: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   1253: pop
    //   1254: aload_1
    //   1255: astore #4
    //   1257: aload #5
    //   1259: athrow
    //   1260: astore_1
    //   1261: aload_0
    //   1262: astore #4
    //   1264: lload_2
    //   1265: lstore #12
    //   1267: goto -> 1462
    //   1270: aload #5
    //   1272: astore #16
    //   1274: aload #16
    //   1276: astore #4
    //   1278: aload #16
    //   1280: ldc2_w 1155346202627
    //   1283: invokevirtual readInt : (J)I
    //   1286: istore #10
    //   1288: aload_0
    //   1289: astore #4
    //   1291: aload #4
    //   1293: astore #11
    //   1295: aload #4
    //   1297: iload #10
    //   1299: putfield mnc : I
    //   1302: aload #16
    //   1304: astore #5
    //   1306: goto -> 1399
    //   1309: astore_1
    //   1310: aload_0
    //   1311: astore #5
    //   1313: aload #4
    //   1315: astore #8
    //   1317: goto -> 1385
    //   1320: aload #5
    //   1322: astore #16
    //   1324: aload #4
    //   1326: astore #11
    //   1328: aload #4
    //   1330: aload #16
    //   1332: ldc2_w 1155346202626
    //   1335: invokevirtual readInt : (J)I
    //   1338: putfield mcc : I
    //   1341: aload #16
    //   1343: astore #5
    //   1345: goto -> 1399
    //   1348: aload #5
    //   1350: astore #16
    //   1352: aload #4
    //   1354: astore #11
    //   1356: aload #4
    //   1358: aload #16
    //   1360: ldc2_w 1108101562369
    //   1363: invokevirtual readFloat : (J)F
    //   1366: putfield fontScale : F
    //   1369: aload #16
    //   1371: astore #5
    //   1373: goto -> 1399
    //   1376: astore_1
    //   1377: aload #5
    //   1379: astore #8
    //   1381: aload #11
    //   1383: astore #5
    //   1385: lload_2
    //   1386: lstore #12
    //   1388: aload #5
    //   1390: astore #4
    //   1392: aload #8
    //   1394: astore #5
    //   1396: goto -> 1462
    //   1399: goto -> 36
    //   1402: astore_1
    //   1403: lload_2
    //   1404: lstore #12
    //   1406: goto -> 1462
    //   1409: aload #9
    //   1411: invokeinterface size : ()I
    //   1416: ifle -> 1451
    //   1419: aload #4
    //   1421: new android/os/LocaleList
    //   1424: dup
    //   1425: aload #9
    //   1427: aload #9
    //   1429: invokeinterface size : ()I
    //   1434: anewarray java/util/Locale
    //   1437: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   1442: checkcast [Ljava/util/Locale;
    //   1445: invokespecial <init> : ([Ljava/util/Locale;)V
    //   1448: invokevirtual setLocales : (Landroid/os/LocaleList;)V
    //   1451: aload #5
    //   1453: lload_2
    //   1454: invokevirtual end : (J)V
    //   1457: return
    //   1458: astore_1
    //   1459: lload_2
    //   1460: lstore #12
    //   1462: aload #9
    //   1464: invokeinterface size : ()I
    //   1469: ifle -> 1504
    //   1472: aload #4
    //   1474: new android/os/LocaleList
    //   1477: dup
    //   1478: aload #9
    //   1480: aload #9
    //   1482: invokeinterface size : ()I
    //   1487: anewarray java/util/Locale
    //   1490: invokeinterface toArray : ([Ljava/lang/Object;)[Ljava/lang/Object;
    //   1495: checkcast [Ljava/util/Locale;
    //   1498: invokespecial <init> : ([Ljava/util/Locale;)V
    //   1501: invokevirtual setLocales : (Landroid/os/LocaleList;)V
    //   1504: aload #5
    //   1506: lload_2
    //   1507: invokevirtual end : (J)V
    //   1510: aload_1
    //   1511: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1219	-> 0
    //   #1220	-> 27
    //   #1222	-> 36
    //   #1223	-> 48
    //   #1335	-> 155
    //   #1339	-> 171
    //   #1337	-> 174
    //   #1338	-> 176
    //   #1331	-> 190
    //   #1332	-> 203
    //   #1328	-> 206
    //   #1329	-> 219
    //   #1325	-> 222
    //   #1326	-> 235
    //   #1322	-> 238
    //   #1323	-> 251
    //   #1319	-> 254
    //   #1320	-> 267
    //   #1316	-> 270
    //   #1317	-> 283
    //   #1313	-> 286
    //   #1314	-> 299
    //   #1310	-> 302
    //   #1311	-> 315
    //   #1307	-> 318
    //   #1308	-> 331
    //   #1304	-> 334
    //   #1305	-> 347
    //   #1301	-> 350
    //   #1302	-> 363
    //   #1298	-> 366
    //   #1299	-> 379
    //   #1295	-> 382
    //   #1296	-> 395
    //   #1292	-> 398
    //   #1293	-> 411
    //   #1289	-> 414
    //   #1290	-> 427
    //   #1345	-> 430
    //   #1236	-> 437
    //   #1237	-> 447
    //   #1238	-> 447
    //   #1239	-> 447
    //   #1240	-> 447
    //   #1242	-> 463
    //   #1244	-> 475
    //   #1256	-> 508
    //   #1253	-> 525
    //   #1254	-> 535
    //   #1250	-> 542
    //   #1251	-> 552
    //   #1246	-> 559
    //   #1248	-> 569
    //   #1257	-> 573
    //   #1264	-> 576
    //   #1260	-> 581
    //   #1264	-> 586
    //   #1266	-> 593
    //   #1267	-> 603
    //   #1268	-> 612
    //   #1269	-> 621
    //   #1270	-> 630
    //   #1271	-> 639
    //   #1274	-> 646
    //   #1275	-> 657
    //   #1276	-> 663
    //   #1277	-> 706
    //   #1276	-> 724
    //   #1279	-> 735
    //   #1285	-> 745
    //   #1281	-> 748
    //   #1282	-> 775
    //   #1286	-> 862
    //   #1287	-> 862
    //   #1345	-> 871
    //   #1264	-> 900
    //   #1260	-> 905
    //   #1262	-> 907
    //   #1264	-> 910
    //   #1266	-> 921
    //   #1267	-> 937
    //   #1268	-> 949
    //   #1269	-> 961
    //   #1270	-> 973
    //   #1271	-> 985
    //   #1274	-> 995
    //   #1275	-> 1009
    //   #1276	-> 1015
    //   #1277	-> 1073
    //   #1276	-> 1097
    //   #1279	-> 1111
    //   #1285	-> 1124
    //   #1281	-> 1127
    //   #1282	-> 1134
    //   #1286	-> 1254
    //   #1345	-> 1260
    //   #1231	-> 1270
    //   #1232	-> 1302
    //   #1345	-> 1309
    //   #1228	-> 1320
    //   #1229	-> 1341
    //   #1225	-> 1348
    //   #1226	-> 1369
    //   #1345	-> 1376
    //   #1340	-> 1399
    //   #1345	-> 1402
    //   #1347	-> 1419
    //   #1349	-> 1451
    //   #1350	-> 1457
    //   #1351	-> 1457
    //   #1345	-> 1458
    //   #1347	-> 1472
    //   #1349	-> 1504
    //   #1350	-> 1510
    // Exception table:
    //   from	to	target	type
    //   36	42	1458	finally
    //   48	54	1402	finally
    //   155	171	174	java/lang/Exception
    //   155	171	430	finally
    //   176	187	430	finally
    //   190	203	430	finally
    //   206	219	430	finally
    //   222	235	430	finally
    //   238	251	430	finally
    //   254	267	430	finally
    //   270	283	430	finally
    //   286	299	430	finally
    //   302	315	430	finally
    //   318	331	430	finally
    //   334	347	430	finally
    //   350	363	430	finally
    //   366	379	430	finally
    //   382	395	430	finally
    //   398	411	430	finally
    //   414	427	430	finally
    //   437	447	1260	finally
    //   463	469	905	android/util/proto/WireTypeMismatchException
    //   463	469	900	finally
    //   475	481	581	android/util/proto/WireTypeMismatchException
    //   475	481	576	finally
    //   508	518	581	android/util/proto/WireTypeMismatchException
    //   508	518	576	finally
    //   525	535	581	android/util/proto/WireTypeMismatchException
    //   525	535	576	finally
    //   542	552	581	android/util/proto/WireTypeMismatchException
    //   542	552	576	finally
    //   559	569	581	android/util/proto/WireTypeMismatchException
    //   559	569	576	finally
    //   586	593	885	finally
    //   593	603	773	java/util/IllformedLocaleException
    //   593	603	885	finally
    //   603	612	768	java/util/IllformedLocaleException
    //   603	612	885	finally
    //   612	621	763	java/util/IllformedLocaleException
    //   612	621	885	finally
    //   621	630	758	java/util/IllformedLocaleException
    //   621	630	885	finally
    //   630	639	753	java/util/IllformedLocaleException
    //   630	639	871	finally
    //   639	646	753	java/util/IllformedLocaleException
    //   639	646	871	finally
    //   646	657	753	java/util/IllformedLocaleException
    //   646	657	871	finally
    //   663	706	748	java/util/IllformedLocaleException
    //   663	706	871	finally
    //   706	724	748	java/util/IllformedLocaleException
    //   706	724	871	finally
    //   724	732	748	java/util/IllformedLocaleException
    //   724	732	871	finally
    //   735	745	748	java/util/IllformedLocaleException
    //   735	745	871	finally
    //   775	862	871	finally
    //   907	910	910	finally
    //   915	921	1309	finally
    //   924	929	1132	java/util/IllformedLocaleException
    //   924	929	1309	finally
    //   932	937	1132	java/util/IllformedLocaleException
    //   932	937	1309	finally
    //   940	949	1132	java/util/IllformedLocaleException
    //   940	949	1309	finally
    //   952	961	1132	java/util/IllformedLocaleException
    //   952	961	1309	finally
    //   964	973	1132	java/util/IllformedLocaleException
    //   964	973	1309	finally
    //   976	985	1132	java/util/IllformedLocaleException
    //   976	985	1309	finally
    //   988	995	1132	java/util/IllformedLocaleException
    //   988	995	1309	finally
    //   998	1009	1132	java/util/IllformedLocaleException
    //   998	1009	1309	finally
    //   1018	1023	1127	java/util/IllformedLocaleException
    //   1018	1023	1309	finally
    //   1026	1031	1127	java/util/IllformedLocaleException
    //   1026	1031	1309	finally
    //   1034	1043	1127	java/util/IllformedLocaleException
    //   1034	1043	1309	finally
    //   1046	1061	1127	java/util/IllformedLocaleException
    //   1046	1061	1309	finally
    //   1064	1073	1127	java/util/IllformedLocaleException
    //   1064	1073	1309	finally
    //   1076	1087	1127	java/util/IllformedLocaleException
    //   1076	1087	1309	finally
    //   1090	1097	1127	java/util/IllformedLocaleException
    //   1090	1097	1309	finally
    //   1100	1108	1127	java/util/IllformedLocaleException
    //   1100	1108	1309	finally
    //   1114	1124	1127	java/util/IllformedLocaleException
    //   1114	1124	1309	finally
    //   1137	1142	1309	finally
    //   1145	1150	1309	finally
    //   1153	1162	1309	finally
    //   1165	1173	1309	finally
    //   1176	1184	1309	finally
    //   1187	1195	1309	finally
    //   1198	1206	1309	finally
    //   1209	1217	1309	finally
    //   1220	1229	1309	finally
    //   1232	1240	1309	finally
    //   1243	1254	1309	finally
    //   1257	1260	1309	finally
    //   1278	1288	1309	finally
    //   1295	1302	1376	finally
    //   1328	1341	1376	finally
    //   1356	1369	1376	finally
  }
  
  public void writeResConfigToProto(ProtoOutputStream paramProtoOutputStream, long paramLong, DisplayMetrics paramDisplayMetrics) {
    int i, j;
    if (paramDisplayMetrics.widthPixels >= paramDisplayMetrics.heightPixels) {
      i = paramDisplayMetrics.widthPixels;
      j = paramDisplayMetrics.heightPixels;
    } else {
      i = paramDisplayMetrics.heightPixels;
      j = paramDisplayMetrics.widthPixels;
    } 
    paramLong = paramProtoOutputStream.start(paramLong);
    dumpDebug(paramProtoOutputStream, 1146756268033L);
    paramProtoOutputStream.write(1155346202626L, Build.VERSION.RESOURCES_SDK_INT);
    paramProtoOutputStream.write(1155346202627L, i);
    paramProtoOutputStream.write(1155346202628L, j);
    paramProtoOutputStream.end(paramLong);
  }
  
  public static String uiModeToString(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case 7:
        return "UI_MODE_TYPE_VR_HEADSET";
      case 6:
        return "UI_MODE_TYPE_WATCH";
      case 5:
        return "UI_MODE_TYPE_APPLIANCE";
      case 4:
        return "UI_MODE_TYPE_TELEVISION";
      case 3:
        return "UI_MODE_TYPE_CAR";
      case 2:
        return "UI_MODE_TYPE_DESK";
      case 1:
        return "UI_MODE_TYPE_NORMAL";
      case 0:
        break;
    } 
    return "UI_MODE_TYPE_UNDEFINED";
  }
  
  public void setToDefaults() {
    this.fontScale = 1.0F;
    this.mnc = 0;
    this.mcc = 0;
    this.mLocaleList = LocaleList.getEmptyLocaleList();
    this.locale = null;
    this.userSetLocale = false;
    this.touchscreen = 0;
    this.keyboard = 0;
    this.keyboardHidden = 0;
    this.hardKeyboardHidden = 0;
    this.navigation = 0;
    this.navigationHidden = 0;
    this.orientation = 0;
    this.screenLayout = 0;
    this.colorMode = 0;
    this.uiMode = 0;
    this.compatScreenWidthDp = 0;
    this.screenWidthDp = 0;
    this.compatScreenHeightDp = 0;
    this.screenHeightDp = 0;
    this.compatSmallestScreenWidthDp = 0;
    this.smallestScreenWidthDp = 0;
    this.densityDpi = 0;
    this.assetsSeq = 0;
    this.seq = 0;
    this.windowConfiguration.setToDefaults();
    this.mOplusExtraConfiguration.setToDefaults();
  }
  
  public void unset() {
    setToDefaults();
    this.fontScale = 0.0F;
  }
  
  @Deprecated
  public void makeDefault() {
    setToDefaults();
  }
  
  public int updateFrom(Configuration paramConfiguration) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_1
    //   3: getfield fontScale : F
    //   6: fstore_3
    //   7: iload_2
    //   8: istore #4
    //   10: fload_3
    //   11: fconst_0
    //   12: fcmpl
    //   13: ifle -> 40
    //   16: iload_2
    //   17: istore #4
    //   19: aload_0
    //   20: getfield fontScale : F
    //   23: fload_3
    //   24: fcmpl
    //   25: ifeq -> 40
    //   28: iconst_0
    //   29: ldc_w 1073741824
    //   32: ior
    //   33: istore #4
    //   35: aload_0
    //   36: fload_3
    //   37: putfield fontScale : F
    //   40: aload_1
    //   41: getfield mcc : I
    //   44: istore #5
    //   46: iload #4
    //   48: istore_2
    //   49: iload #5
    //   51: ifeq -> 77
    //   54: iload #4
    //   56: istore_2
    //   57: aload_0
    //   58: getfield mcc : I
    //   61: iload #5
    //   63: if_icmpeq -> 77
    //   66: iload #4
    //   68: iconst_1
    //   69: ior
    //   70: istore_2
    //   71: aload_0
    //   72: iload #5
    //   74: putfield mcc : I
    //   77: aload_1
    //   78: getfield mnc : I
    //   81: istore #5
    //   83: iload_2
    //   84: istore #4
    //   86: iload #5
    //   88: ifeq -> 114
    //   91: iload_2
    //   92: istore #4
    //   94: aload_0
    //   95: getfield mnc : I
    //   98: iload #5
    //   100: if_icmpeq -> 114
    //   103: iload_2
    //   104: iconst_2
    //   105: ior
    //   106: istore #4
    //   108: aload_0
    //   109: iload #5
    //   111: putfield mnc : I
    //   114: aload_0
    //   115: invokespecial fixUpLocaleList : ()V
    //   118: aload_1
    //   119: invokespecial fixUpLocaleList : ()V
    //   122: iload #4
    //   124: istore_2
    //   125: aload_1
    //   126: getfield mLocaleList : Landroid/os/LocaleList;
    //   129: invokevirtual isEmpty : ()Z
    //   132: ifne -> 214
    //   135: iload #4
    //   137: istore_2
    //   138: aload_0
    //   139: getfield mLocaleList : Landroid/os/LocaleList;
    //   142: aload_1
    //   143: getfield mLocaleList : Landroid/os/LocaleList;
    //   146: invokevirtual equals : (Ljava/lang/Object;)Z
    //   149: ifne -> 214
    //   152: iload #4
    //   154: iconst_4
    //   155: ior
    //   156: istore #4
    //   158: aload_0
    //   159: aload_1
    //   160: getfield mLocaleList : Landroid/os/LocaleList;
    //   163: putfield mLocaleList : Landroid/os/LocaleList;
    //   166: iload #4
    //   168: istore_2
    //   169: aload_1
    //   170: getfield locale : Ljava/util/Locale;
    //   173: aload_0
    //   174: getfield locale : Ljava/util/Locale;
    //   177: invokevirtual equals : (Ljava/lang/Object;)Z
    //   180: ifne -> 214
    //   183: aload_1
    //   184: getfield locale : Ljava/util/Locale;
    //   187: invokevirtual clone : ()Ljava/lang/Object;
    //   190: checkcast java/util/Locale
    //   193: astore #6
    //   195: aload_0
    //   196: aload #6
    //   198: putfield locale : Ljava/util/Locale;
    //   201: iload #4
    //   203: sipush #8192
    //   206: ior
    //   207: istore_2
    //   208: aload_0
    //   209: aload #6
    //   211: invokevirtual setLayoutDirection : (Ljava/util/Locale;)V
    //   214: aload_1
    //   215: getfield screenLayout : I
    //   218: sipush #192
    //   221: iand
    //   222: istore #5
    //   224: iload_2
    //   225: istore #4
    //   227: iload #5
    //   229: ifeq -> 272
    //   232: aload_0
    //   233: getfield screenLayout : I
    //   236: istore #7
    //   238: iload_2
    //   239: istore #4
    //   241: iload #5
    //   243: iload #7
    //   245: sipush #192
    //   248: iand
    //   249: if_icmpeq -> 272
    //   252: aload_0
    //   253: iload #7
    //   255: sipush #-193
    //   258: iand
    //   259: iload #5
    //   261: ior
    //   262: putfield screenLayout : I
    //   265: iload_2
    //   266: sipush #8192
    //   269: ior
    //   270: istore #4
    //   272: iload #4
    //   274: istore #5
    //   276: aload_1
    //   277: getfield userSetLocale : Z
    //   280: ifeq -> 312
    //   283: aload_0
    //   284: getfield userSetLocale : Z
    //   287: ifeq -> 301
    //   290: iload #4
    //   292: istore #5
    //   294: iload #4
    //   296: iconst_4
    //   297: iand
    //   298: ifeq -> 312
    //   301: iload #4
    //   303: iconst_4
    //   304: ior
    //   305: istore #5
    //   307: aload_0
    //   308: iconst_1
    //   309: putfield userSetLocale : Z
    //   312: aload_1
    //   313: getfield touchscreen : I
    //   316: istore #4
    //   318: iload #5
    //   320: istore_2
    //   321: iload #4
    //   323: ifeq -> 350
    //   326: iload #5
    //   328: istore_2
    //   329: aload_0
    //   330: getfield touchscreen : I
    //   333: iload #4
    //   335: if_icmpeq -> 350
    //   338: iload #5
    //   340: bipush #8
    //   342: ior
    //   343: istore_2
    //   344: aload_0
    //   345: iload #4
    //   347: putfield touchscreen : I
    //   350: aload_1
    //   351: getfield keyboard : I
    //   354: istore #4
    //   356: iload_2
    //   357: istore #5
    //   359: iload #4
    //   361: ifeq -> 388
    //   364: iload_2
    //   365: istore #5
    //   367: aload_0
    //   368: getfield keyboard : I
    //   371: iload #4
    //   373: if_icmpeq -> 388
    //   376: iload_2
    //   377: bipush #16
    //   379: ior
    //   380: istore #5
    //   382: aload_0
    //   383: iload #4
    //   385: putfield keyboard : I
    //   388: aload_1
    //   389: getfield keyboardHidden : I
    //   392: istore_2
    //   393: iload #5
    //   395: istore #4
    //   397: iload_2
    //   398: ifeq -> 425
    //   401: iload #5
    //   403: istore #4
    //   405: aload_0
    //   406: getfield keyboardHidden : I
    //   409: iload_2
    //   410: if_icmpeq -> 425
    //   413: iload #5
    //   415: bipush #32
    //   417: ior
    //   418: istore #4
    //   420: aload_0
    //   421: iload_2
    //   422: putfield keyboardHidden : I
    //   425: aload_1
    //   426: getfield hardKeyboardHidden : I
    //   429: istore #5
    //   431: iload #4
    //   433: istore_2
    //   434: iload #5
    //   436: ifeq -> 463
    //   439: iload #4
    //   441: istore_2
    //   442: aload_0
    //   443: getfield hardKeyboardHidden : I
    //   446: iload #5
    //   448: if_icmpeq -> 463
    //   451: iload #4
    //   453: bipush #32
    //   455: ior
    //   456: istore_2
    //   457: aload_0
    //   458: iload #5
    //   460: putfield hardKeyboardHidden : I
    //   463: aload_1
    //   464: getfield navigation : I
    //   467: istore #5
    //   469: iload_2
    //   470: istore #4
    //   472: iload #5
    //   474: ifeq -> 501
    //   477: iload_2
    //   478: istore #4
    //   480: aload_0
    //   481: getfield navigation : I
    //   484: iload #5
    //   486: if_icmpeq -> 501
    //   489: iload_2
    //   490: bipush #64
    //   492: ior
    //   493: istore #4
    //   495: aload_0
    //   496: iload #5
    //   498: putfield navigation : I
    //   501: aload_1
    //   502: getfield navigationHidden : I
    //   505: istore #5
    //   507: iload #4
    //   509: istore_2
    //   510: iload #5
    //   512: ifeq -> 539
    //   515: iload #4
    //   517: istore_2
    //   518: aload_0
    //   519: getfield navigationHidden : I
    //   522: iload #5
    //   524: if_icmpeq -> 539
    //   527: iload #4
    //   529: bipush #32
    //   531: ior
    //   532: istore_2
    //   533: aload_0
    //   534: iload #5
    //   536: putfield navigationHidden : I
    //   539: aload_1
    //   540: getfield orientation : I
    //   543: istore #5
    //   545: iload_2
    //   546: istore #4
    //   548: iload #5
    //   550: ifeq -> 578
    //   553: iload_2
    //   554: istore #4
    //   556: aload_0
    //   557: getfield orientation : I
    //   560: iload #5
    //   562: if_icmpeq -> 578
    //   565: iload_2
    //   566: sipush #128
    //   569: ior
    //   570: istore #4
    //   572: aload_0
    //   573: iload #5
    //   575: putfield orientation : I
    //   578: aload_1
    //   579: getfield screenLayout : I
    //   582: istore #5
    //   584: iload #4
    //   586: istore_2
    //   587: iload #5
    //   589: bipush #15
    //   591: iand
    //   592: ifeq -> 639
    //   595: aload_0
    //   596: getfield screenLayout : I
    //   599: istore #7
    //   601: iload #4
    //   603: istore_2
    //   604: iload #5
    //   606: bipush #15
    //   608: iand
    //   609: iload #7
    //   611: bipush #15
    //   613: iand
    //   614: if_icmpeq -> 639
    //   617: iload #4
    //   619: sipush #256
    //   622: ior
    //   623: istore_2
    //   624: aload_0
    //   625: iload #5
    //   627: bipush #15
    //   629: iand
    //   630: iload #7
    //   632: bipush #-16
    //   634: iand
    //   635: ior
    //   636: putfield screenLayout : I
    //   639: aload_1
    //   640: getfield screenLayout : I
    //   643: istore #7
    //   645: iload_2
    //   646: istore #5
    //   648: iload #7
    //   650: bipush #48
    //   652: iand
    //   653: ifeq -> 700
    //   656: aload_0
    //   657: getfield screenLayout : I
    //   660: istore #4
    //   662: iload_2
    //   663: istore #5
    //   665: iload #7
    //   667: bipush #48
    //   669: iand
    //   670: iload #4
    //   672: bipush #48
    //   674: iand
    //   675: if_icmpeq -> 700
    //   678: iload_2
    //   679: sipush #256
    //   682: ior
    //   683: istore #5
    //   685: aload_0
    //   686: iload #7
    //   688: bipush #48
    //   690: iand
    //   691: iload #4
    //   693: bipush #-49
    //   695: iand
    //   696: ior
    //   697: putfield screenLayout : I
    //   700: aload_1
    //   701: getfield screenLayout : I
    //   704: istore_2
    //   705: iload #5
    //   707: istore #4
    //   709: iload_2
    //   710: sipush #768
    //   713: iand
    //   714: ifeq -> 765
    //   717: aload_0
    //   718: getfield screenLayout : I
    //   721: istore #7
    //   723: iload #5
    //   725: istore #4
    //   727: iload_2
    //   728: sipush #768
    //   731: iand
    //   732: iload #7
    //   734: sipush #768
    //   737: iand
    //   738: if_icmpeq -> 765
    //   741: iload #5
    //   743: sipush #256
    //   746: ior
    //   747: istore #4
    //   749: aload_0
    //   750: iload_2
    //   751: sipush #768
    //   754: iand
    //   755: iload #7
    //   757: sipush #-769
    //   760: iand
    //   761: ior
    //   762: putfield screenLayout : I
    //   765: aload_1
    //   766: getfield screenLayout : I
    //   769: istore #7
    //   771: aload_0
    //   772: getfield screenLayout : I
    //   775: istore #5
    //   777: iload #4
    //   779: istore_2
    //   780: iload #7
    //   782: ldc 268435456
    //   784: iand
    //   785: iload #5
    //   787: ldc 268435456
    //   789: iand
    //   790: if_icmpeq -> 824
    //   793: iload #4
    //   795: istore_2
    //   796: iload #7
    //   798: ifeq -> 824
    //   801: iload #4
    //   803: sipush #256
    //   806: ior
    //   807: istore_2
    //   808: aload_0
    //   809: iload #7
    //   811: ldc 268435456
    //   813: iand
    //   814: ldc_w -268435457
    //   817: iload #5
    //   819: iand
    //   820: ior
    //   821: putfield screenLayout : I
    //   824: aload_1
    //   825: getfield colorMode : I
    //   828: istore #4
    //   830: iload_2
    //   831: istore #5
    //   833: iload #4
    //   835: iconst_3
    //   836: iand
    //   837: ifeq -> 881
    //   840: aload_0
    //   841: getfield colorMode : I
    //   844: istore #7
    //   846: iload_2
    //   847: istore #5
    //   849: iload #4
    //   851: iconst_3
    //   852: iand
    //   853: iload #7
    //   855: iconst_3
    //   856: iand
    //   857: if_icmpeq -> 881
    //   860: iload_2
    //   861: sipush #16384
    //   864: ior
    //   865: istore #5
    //   867: aload_0
    //   868: iload #4
    //   870: iconst_3
    //   871: iand
    //   872: iload #7
    //   874: bipush #-4
    //   876: iand
    //   877: ior
    //   878: putfield colorMode : I
    //   881: aload_1
    //   882: getfield colorMode : I
    //   885: istore #7
    //   887: iload #5
    //   889: istore #4
    //   891: iload #7
    //   893: bipush #12
    //   895: iand
    //   896: ifeq -> 942
    //   899: aload_0
    //   900: getfield colorMode : I
    //   903: istore_2
    //   904: iload #5
    //   906: istore #4
    //   908: iload #7
    //   910: bipush #12
    //   912: iand
    //   913: iload_2
    //   914: bipush #12
    //   916: iand
    //   917: if_icmpeq -> 942
    //   920: iload #5
    //   922: sipush #16384
    //   925: ior
    //   926: istore #4
    //   928: aload_0
    //   929: iload #7
    //   931: bipush #12
    //   933: iand
    //   934: iload_2
    //   935: bipush #-13
    //   937: iand
    //   938: ior
    //   939: putfield colorMode : I
    //   942: aload_1
    //   943: getfield uiMode : I
    //   946: istore #5
    //   948: iload #4
    //   950: istore_2
    //   951: iload #5
    //   953: ifeq -> 1040
    //   956: aload_0
    //   957: getfield uiMode : I
    //   960: istore #7
    //   962: iload #4
    //   964: istore_2
    //   965: iload #7
    //   967: iload #5
    //   969: if_icmpeq -> 1040
    //   972: iload #4
    //   974: sipush #512
    //   977: ior
    //   978: istore #4
    //   980: iload #5
    //   982: bipush #15
    //   984: iand
    //   985: ifeq -> 1003
    //   988: aload_0
    //   989: iload #5
    //   991: bipush #15
    //   993: iand
    //   994: iload #7
    //   996: bipush #-16
    //   998: iand
    //   999: ior
    //   1000: putfield uiMode : I
    //   1003: aload_1
    //   1004: getfield uiMode : I
    //   1007: istore #5
    //   1009: iload #4
    //   1011: istore_2
    //   1012: iload #5
    //   1014: bipush #48
    //   1016: iand
    //   1017: ifeq -> 1040
    //   1020: aload_0
    //   1021: iload #5
    //   1023: bipush #48
    //   1025: iand
    //   1026: aload_0
    //   1027: getfield uiMode : I
    //   1030: bipush #-49
    //   1032: iand
    //   1033: ior
    //   1034: putfield uiMode : I
    //   1037: iload #4
    //   1039: istore_2
    //   1040: aload_1
    //   1041: getfield screenWidthDp : I
    //   1044: istore #5
    //   1046: iload_2
    //   1047: istore #4
    //   1049: iload #5
    //   1051: ifeq -> 1079
    //   1054: iload_2
    //   1055: istore #4
    //   1057: aload_0
    //   1058: getfield screenWidthDp : I
    //   1061: iload #5
    //   1063: if_icmpeq -> 1079
    //   1066: iload_2
    //   1067: sipush #1024
    //   1070: ior
    //   1071: istore #4
    //   1073: aload_0
    //   1074: iload #5
    //   1076: putfield screenWidthDp : I
    //   1079: aload_1
    //   1080: getfield screenHeightDp : I
    //   1083: istore #5
    //   1085: iload #4
    //   1087: istore_2
    //   1088: iload #5
    //   1090: ifeq -> 1118
    //   1093: iload #4
    //   1095: istore_2
    //   1096: aload_0
    //   1097: getfield screenHeightDp : I
    //   1100: iload #5
    //   1102: if_icmpeq -> 1118
    //   1105: iload #4
    //   1107: sipush #1024
    //   1110: ior
    //   1111: istore_2
    //   1112: aload_0
    //   1113: iload #5
    //   1115: putfield screenHeightDp : I
    //   1118: aload_1
    //   1119: getfield smallestScreenWidthDp : I
    //   1122: istore #5
    //   1124: iload_2
    //   1125: istore #4
    //   1127: iload #5
    //   1129: ifeq -> 1157
    //   1132: iload_2
    //   1133: istore #4
    //   1135: aload_0
    //   1136: getfield smallestScreenWidthDp : I
    //   1139: iload #5
    //   1141: if_icmpeq -> 1157
    //   1144: iload_2
    //   1145: sipush #2048
    //   1148: ior
    //   1149: istore #4
    //   1151: aload_0
    //   1152: iload #5
    //   1154: putfield smallestScreenWidthDp : I
    //   1157: aload_1
    //   1158: getfield densityDpi : I
    //   1161: istore #5
    //   1163: iload #4
    //   1165: istore_2
    //   1166: iload #5
    //   1168: ifeq -> 1196
    //   1171: iload #4
    //   1173: istore_2
    //   1174: aload_0
    //   1175: getfield densityDpi : I
    //   1178: iload #5
    //   1180: if_icmpeq -> 1196
    //   1183: iload #4
    //   1185: sipush #4096
    //   1188: ior
    //   1189: istore_2
    //   1190: aload_0
    //   1191: iload #5
    //   1193: putfield densityDpi : I
    //   1196: aload_1
    //   1197: getfield compatScreenWidthDp : I
    //   1200: istore #4
    //   1202: iload #4
    //   1204: ifeq -> 1213
    //   1207: aload_0
    //   1208: iload #4
    //   1210: putfield compatScreenWidthDp : I
    //   1213: aload_1
    //   1214: getfield compatScreenHeightDp : I
    //   1217: istore #4
    //   1219: iload #4
    //   1221: ifeq -> 1230
    //   1224: aload_0
    //   1225: iload #4
    //   1227: putfield compatScreenHeightDp : I
    //   1230: aload_1
    //   1231: getfield compatSmallestScreenWidthDp : I
    //   1234: istore #4
    //   1236: iload #4
    //   1238: ifeq -> 1247
    //   1241: aload_0
    //   1242: iload #4
    //   1244: putfield compatSmallestScreenWidthDp : I
    //   1247: aload_1
    //   1248: getfield assetsSeq : I
    //   1251: istore #5
    //   1253: iload_2
    //   1254: istore #4
    //   1256: iload #5
    //   1258: ifeq -> 1286
    //   1261: iload_2
    //   1262: istore #4
    //   1264: iload #5
    //   1266: aload_0
    //   1267: getfield assetsSeq : I
    //   1270: if_icmpeq -> 1286
    //   1273: iload_2
    //   1274: ldc_w -2147483648
    //   1277: ior
    //   1278: istore #4
    //   1280: aload_0
    //   1281: iload #5
    //   1283: putfield assetsSeq : I
    //   1286: aload_1
    //   1287: getfield seq : I
    //   1290: istore_2
    //   1291: iload_2
    //   1292: ifeq -> 1300
    //   1295: aload_0
    //   1296: iload_2
    //   1297: putfield seq : I
    //   1300: iload #4
    //   1302: istore_2
    //   1303: aload_0
    //   1304: getfield windowConfiguration : Landroid/app/WindowConfiguration;
    //   1307: aload_1
    //   1308: getfield windowConfiguration : Landroid/app/WindowConfiguration;
    //   1311: invokevirtual updateFrom : (Landroid/app/WindowConfiguration;)I
    //   1314: ifeq -> 1324
    //   1317: iload #4
    //   1319: ldc_w 536870912
    //   1322: ior
    //   1323: istore_2
    //   1324: aload_0
    //   1325: getfield mOplusExtraConfiguration : Loplus/content/res/OplusExtraConfiguration;
    //   1328: aload_1
    //   1329: getfield mOplusExtraConfiguration : Loplus/content/res/OplusExtraConfiguration;
    //   1332: invokevirtual updateFrom : (Loplus/content/res/OplusExtraConfiguration;)I
    //   1335: istore #4
    //   1337: iload_2
    //   1338: iload #4
    //   1340: ior
    //   1341: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1465	-> 0
    //   #1466	-> 2
    //   #1467	-> 28
    //   #1468	-> 35
    //   #1470	-> 40
    //   #1471	-> 66
    //   #1472	-> 71
    //   #1474	-> 77
    //   #1475	-> 103
    //   #1476	-> 108
    //   #1478	-> 114
    //   #1479	-> 118
    //   #1480	-> 122
    //   #1481	-> 152
    //   #1482	-> 158
    //   #1484	-> 166
    //   #1485	-> 183
    //   #1487	-> 201
    //   #1490	-> 208
    //   #1493	-> 214
    //   #1494	-> 224
    //   #1496	-> 252
    //   #1497	-> 265
    //   #1499	-> 272
    //   #1501	-> 301
    //   #1502	-> 307
    //   #1504	-> 312
    //   #1506	-> 338
    //   #1507	-> 344
    //   #1509	-> 350
    //   #1511	-> 376
    //   #1512	-> 382
    //   #1514	-> 388
    //   #1516	-> 413
    //   #1517	-> 420
    //   #1519	-> 425
    //   #1521	-> 451
    //   #1522	-> 457
    //   #1524	-> 463
    //   #1526	-> 489
    //   #1527	-> 495
    //   #1529	-> 501
    //   #1531	-> 527
    //   #1532	-> 533
    //   #1534	-> 539
    //   #1536	-> 565
    //   #1537	-> 572
    //   #1539	-> 578
    //   #1542	-> 617
    //   #1543	-> 624
    //   #1546	-> 639
    //   #1549	-> 678
    //   #1550	-> 685
    //   #1553	-> 700
    //   #1556	-> 741
    //   #1557	-> 749
    //   #1560	-> 765
    //   #1563	-> 801
    //   #1564	-> 808
    //   #1568	-> 824
    //   #1572	-> 860
    //   #1573	-> 867
    //   #1577	-> 881
    //   #1580	-> 920
    //   #1581	-> 928
    //   #1585	-> 942
    //   #1587	-> 972
    //   #1588	-> 980
    //   #1589	-> 988
    //   #1592	-> 1003
    //   #1593	-> 1020
    //   #1597	-> 1040
    //   #1599	-> 1066
    //   #1600	-> 1073
    //   #1602	-> 1079
    //   #1604	-> 1105
    //   #1605	-> 1112
    //   #1607	-> 1118
    //   #1609	-> 1144
    //   #1610	-> 1151
    //   #1612	-> 1157
    //   #1614	-> 1183
    //   #1615	-> 1190
    //   #1617	-> 1196
    //   #1618	-> 1207
    //   #1620	-> 1213
    //   #1621	-> 1224
    //   #1623	-> 1230
    //   #1624	-> 1241
    //   #1626	-> 1247
    //   #1627	-> 1273
    //   #1628	-> 1280
    //   #1630	-> 1286
    //   #1631	-> 1295
    //   #1633	-> 1300
    //   #1634	-> 1317
    //   #1638	-> 1324
    //   #1640	-> 1337
  }
  
  public void setTo(Configuration paramConfiguration, int paramInt1, int paramInt2) {
    if ((0x40000000 & paramInt1) != 0)
      this.fontScale = paramConfiguration.fontScale; 
    if ((paramInt1 & 0x1) != 0)
      this.mcc = paramConfiguration.mcc; 
    if ((paramInt1 & 0x2) != 0)
      this.mnc = paramConfiguration.mnc; 
    if ((paramInt1 & 0x4) != 0) {
      LocaleList localeList = paramConfiguration.mLocaleList;
      if (!localeList.isEmpty())
        this.locale = (Locale)paramConfiguration.locale.clone(); 
    } 
    if ((paramInt1 & 0x2000) != 0) {
      int i = paramConfiguration.screenLayout;
      this.screenLayout = this.screenLayout & 0xFFFFFF3F | i & 0xC0;
    } 
    if ((paramInt1 & 0x4) != 0)
      this.userSetLocale = paramConfiguration.userSetLocale; 
    if ((paramInt1 & 0x8) != 0)
      this.touchscreen = paramConfiguration.touchscreen; 
    if ((paramInt1 & 0x10) != 0)
      this.keyboard = paramConfiguration.keyboard; 
    if ((paramInt1 & 0x20) != 0) {
      this.keyboardHidden = paramConfiguration.keyboardHidden;
      this.hardKeyboardHidden = paramConfiguration.hardKeyboardHidden;
      this.navigationHidden = paramConfiguration.navigationHidden;
    } 
    if ((paramInt1 & 0x40) != 0)
      this.navigation = paramConfiguration.navigation; 
    if ((paramInt1 & 0x80) != 0)
      this.orientation = paramConfiguration.orientation; 
    if ((paramInt1 & 0x100) != 0)
      this.screenLayout |= paramConfiguration.screenLayout & 0xFFFFFF3F; 
    if ((paramInt1 & 0x4000) != 0)
      this.colorMode = paramConfiguration.colorMode; 
    if ((paramInt1 & 0x200) != 0)
      this.uiMode = paramConfiguration.uiMode; 
    if ((paramInt1 & 0x400) != 0) {
      this.screenWidthDp = paramConfiguration.screenWidthDp;
      this.screenHeightDp = paramConfiguration.screenHeightDp;
    } 
    if ((paramInt1 & 0x800) != 0)
      this.smallestScreenWidthDp = paramConfiguration.smallestScreenWidthDp; 
    if ((paramInt1 & 0x1000) != 0)
      this.densityDpi = paramConfiguration.densityDpi; 
    if ((Integer.MIN_VALUE & paramInt1) != 0)
      this.assetsSeq = paramConfiguration.assetsSeq; 
    if ((0x20000000 & paramInt1) != 0)
      this.windowConfiguration.setTo(paramConfiguration.windowConfiguration, paramInt2); 
  }
  
  public int diff(Configuration paramConfiguration) {
    return diff(paramConfiguration, false, false);
  }
  
  public int diffPublicOnly(Configuration paramConfiguration) {
    return diff(paramConfiguration, false, true);
  }
  
  public int diff(Configuration paramConfiguration, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: iconst_0
    //   1: istore #4
    //   3: iload_2
    //   4: ifne -> 20
    //   7: iload #4
    //   9: istore #5
    //   11: aload_1
    //   12: getfield fontScale : F
    //   15: fconst_0
    //   16: fcmpl
    //   17: ifle -> 43
    //   20: iload #4
    //   22: istore #5
    //   24: aload_0
    //   25: getfield fontScale : F
    //   28: aload_1
    //   29: getfield fontScale : F
    //   32: fcmpl
    //   33: ifeq -> 43
    //   36: iconst_0
    //   37: ldc_w 1073741824
    //   40: ior
    //   41: istore #5
    //   43: iload_2
    //   44: ifne -> 58
    //   47: iload #5
    //   49: istore #4
    //   51: aload_1
    //   52: getfield mcc : I
    //   55: ifeq -> 79
    //   58: iload #5
    //   60: istore #4
    //   62: aload_0
    //   63: getfield mcc : I
    //   66: aload_1
    //   67: getfield mcc : I
    //   70: if_icmpeq -> 79
    //   73: iload #5
    //   75: iconst_1
    //   76: ior
    //   77: istore #4
    //   79: iload_2
    //   80: ifne -> 94
    //   83: iload #4
    //   85: istore #5
    //   87: aload_1
    //   88: getfield mnc : I
    //   91: ifeq -> 115
    //   94: iload #4
    //   96: istore #5
    //   98: aload_0
    //   99: getfield mnc : I
    //   102: aload_1
    //   103: getfield mnc : I
    //   106: if_icmpeq -> 115
    //   109: iload #4
    //   111: iconst_2
    //   112: ior
    //   113: istore #5
    //   115: aload_0
    //   116: invokespecial fixUpLocaleList : ()V
    //   119: aload_1
    //   120: invokespecial fixUpLocaleList : ()V
    //   123: iload_2
    //   124: ifne -> 141
    //   127: iload #5
    //   129: istore #4
    //   131: aload_1
    //   132: getfield mLocaleList : Landroid/os/LocaleList;
    //   135: invokevirtual isEmpty : ()Z
    //   138: ifne -> 177
    //   141: aload_0
    //   142: getfield mLocaleList : Landroid/os/LocaleList;
    //   145: astore #6
    //   147: aload_1
    //   148: getfield mLocaleList : Landroid/os/LocaleList;
    //   151: astore #7
    //   153: iload #5
    //   155: istore #4
    //   157: aload #6
    //   159: aload #7
    //   161: invokevirtual equals : (Ljava/lang/Object;)Z
    //   164: ifne -> 177
    //   167: iload #5
    //   169: iconst_4
    //   170: ior
    //   171: sipush #8192
    //   174: ior
    //   175: istore #4
    //   177: aload_1
    //   178: getfield screenLayout : I
    //   181: sipush #192
    //   184: iand
    //   185: istore #8
    //   187: iload_2
    //   188: ifne -> 200
    //   191: iload #4
    //   193: istore #5
    //   195: iload #8
    //   197: ifeq -> 225
    //   200: iload #4
    //   202: istore #5
    //   204: iload #8
    //   206: aload_0
    //   207: getfield screenLayout : I
    //   210: sipush #192
    //   213: iand
    //   214: if_icmpeq -> 225
    //   217: iload #4
    //   219: sipush #8192
    //   222: ior
    //   223: istore #5
    //   225: iload_2
    //   226: ifne -> 240
    //   229: iload #5
    //   231: istore #8
    //   233: aload_1
    //   234: getfield touchscreen : I
    //   237: ifeq -> 262
    //   240: iload #5
    //   242: istore #8
    //   244: aload_0
    //   245: getfield touchscreen : I
    //   248: aload_1
    //   249: getfield touchscreen : I
    //   252: if_icmpeq -> 262
    //   255: iload #5
    //   257: bipush #8
    //   259: ior
    //   260: istore #8
    //   262: iload_2
    //   263: ifne -> 277
    //   266: iload #8
    //   268: istore #4
    //   270: aload_1
    //   271: getfield keyboard : I
    //   274: ifeq -> 299
    //   277: iload #8
    //   279: istore #4
    //   281: aload_0
    //   282: getfield keyboard : I
    //   285: aload_1
    //   286: getfield keyboard : I
    //   289: if_icmpeq -> 299
    //   292: iload #8
    //   294: bipush #16
    //   296: ior
    //   297: istore #4
    //   299: iload_2
    //   300: ifne -> 314
    //   303: iload #4
    //   305: istore #5
    //   307: aload_1
    //   308: getfield keyboardHidden : I
    //   311: ifeq -> 336
    //   314: iload #4
    //   316: istore #5
    //   318: aload_0
    //   319: getfield keyboardHidden : I
    //   322: aload_1
    //   323: getfield keyboardHidden : I
    //   326: if_icmpeq -> 336
    //   329: iload #4
    //   331: bipush #32
    //   333: ior
    //   334: istore #5
    //   336: iload_2
    //   337: ifne -> 351
    //   340: iload #5
    //   342: istore #4
    //   344: aload_1
    //   345: getfield hardKeyboardHidden : I
    //   348: ifeq -> 373
    //   351: iload #5
    //   353: istore #4
    //   355: aload_0
    //   356: getfield hardKeyboardHidden : I
    //   359: aload_1
    //   360: getfield hardKeyboardHidden : I
    //   363: if_icmpeq -> 373
    //   366: iload #5
    //   368: bipush #32
    //   370: ior
    //   371: istore #4
    //   373: iload_2
    //   374: ifne -> 388
    //   377: iload #4
    //   379: istore #5
    //   381: aload_1
    //   382: getfield navigation : I
    //   385: ifeq -> 410
    //   388: iload #4
    //   390: istore #5
    //   392: aload_0
    //   393: getfield navigation : I
    //   396: aload_1
    //   397: getfield navigation : I
    //   400: if_icmpeq -> 410
    //   403: iload #4
    //   405: bipush #64
    //   407: ior
    //   408: istore #5
    //   410: iload_2
    //   411: ifne -> 425
    //   414: iload #5
    //   416: istore #8
    //   418: aload_1
    //   419: getfield navigationHidden : I
    //   422: ifeq -> 447
    //   425: iload #5
    //   427: istore #8
    //   429: aload_0
    //   430: getfield navigationHidden : I
    //   433: aload_1
    //   434: getfield navigationHidden : I
    //   437: if_icmpeq -> 447
    //   440: iload #5
    //   442: bipush #32
    //   444: ior
    //   445: istore #8
    //   447: iload_2
    //   448: ifne -> 462
    //   451: iload #8
    //   453: istore #4
    //   455: aload_1
    //   456: getfield orientation : I
    //   459: ifeq -> 485
    //   462: iload #8
    //   464: istore #4
    //   466: aload_0
    //   467: getfield orientation : I
    //   470: aload_1
    //   471: getfield orientation : I
    //   474: if_icmpeq -> 485
    //   477: iload #8
    //   479: sipush #128
    //   482: ior
    //   483: istore #4
    //   485: iload_2
    //   486: ifne -> 503
    //   489: iload #4
    //   491: istore #5
    //   493: aload_1
    //   494: getfield screenLayout : I
    //   497: invokestatic getScreenLayoutNoDirection : (I)I
    //   500: ifeq -> 544
    //   503: aload_0
    //   504: getfield screenLayout : I
    //   507: istore #5
    //   509: iload #5
    //   511: invokestatic getScreenLayoutNoDirection : (I)I
    //   514: istore #8
    //   516: aload_1
    //   517: getfield screenLayout : I
    //   520: istore #9
    //   522: iload #4
    //   524: istore #5
    //   526: iload #8
    //   528: iload #9
    //   530: invokestatic getScreenLayoutNoDirection : (I)I
    //   533: if_icmpeq -> 544
    //   536: iload #4
    //   538: sipush #256
    //   541: ior
    //   542: istore #5
    //   544: iload_2
    //   545: ifne -> 562
    //   548: iload #5
    //   550: istore #4
    //   552: aload_1
    //   553: getfield colorMode : I
    //   556: bipush #12
    //   558: iand
    //   559: ifeq -> 591
    //   562: iload #5
    //   564: istore #4
    //   566: aload_0
    //   567: getfield colorMode : I
    //   570: bipush #12
    //   572: iand
    //   573: aload_1
    //   574: getfield colorMode : I
    //   577: bipush #12
    //   579: iand
    //   580: if_icmpeq -> 591
    //   583: iload #5
    //   585: sipush #16384
    //   588: ior
    //   589: istore #4
    //   591: iload_2
    //   592: ifne -> 608
    //   595: iload #4
    //   597: istore #8
    //   599: aload_1
    //   600: getfield colorMode : I
    //   603: iconst_3
    //   604: iand
    //   605: ifeq -> 635
    //   608: iload #4
    //   610: istore #8
    //   612: aload_0
    //   613: getfield colorMode : I
    //   616: iconst_3
    //   617: iand
    //   618: aload_1
    //   619: getfield colorMode : I
    //   622: iconst_3
    //   623: iand
    //   624: if_icmpeq -> 635
    //   627: iload #4
    //   629: sipush #16384
    //   632: ior
    //   633: istore #8
    //   635: iload_2
    //   636: ifne -> 650
    //   639: iload #8
    //   641: istore #5
    //   643: aload_1
    //   644: getfield uiMode : I
    //   647: ifeq -> 673
    //   650: iload #8
    //   652: istore #5
    //   654: aload_0
    //   655: getfield uiMode : I
    //   658: aload_1
    //   659: getfield uiMode : I
    //   662: if_icmpeq -> 673
    //   665: iload #8
    //   667: sipush #512
    //   670: ior
    //   671: istore #5
    //   673: iload_2
    //   674: ifne -> 688
    //   677: iload #5
    //   679: istore #4
    //   681: aload_1
    //   682: getfield screenWidthDp : I
    //   685: ifeq -> 711
    //   688: iload #5
    //   690: istore #4
    //   692: aload_0
    //   693: getfield screenWidthDp : I
    //   696: aload_1
    //   697: getfield screenWidthDp : I
    //   700: if_icmpeq -> 711
    //   703: iload #5
    //   705: sipush #1024
    //   708: ior
    //   709: istore #4
    //   711: iload_2
    //   712: ifne -> 726
    //   715: iload #4
    //   717: istore #5
    //   719: aload_1
    //   720: getfield screenHeightDp : I
    //   723: ifeq -> 749
    //   726: iload #4
    //   728: istore #5
    //   730: aload_0
    //   731: getfield screenHeightDp : I
    //   734: aload_1
    //   735: getfield screenHeightDp : I
    //   738: if_icmpeq -> 749
    //   741: iload #4
    //   743: sipush #1024
    //   746: ior
    //   747: istore #5
    //   749: iload_2
    //   750: ifne -> 764
    //   753: iload #5
    //   755: istore #4
    //   757: aload_1
    //   758: getfield smallestScreenWidthDp : I
    //   761: ifeq -> 787
    //   764: iload #5
    //   766: istore #4
    //   768: aload_0
    //   769: getfield smallestScreenWidthDp : I
    //   772: aload_1
    //   773: getfield smallestScreenWidthDp : I
    //   776: if_icmpeq -> 787
    //   779: iload #5
    //   781: sipush #2048
    //   784: ior
    //   785: istore #4
    //   787: iload_2
    //   788: ifne -> 802
    //   791: iload #4
    //   793: istore #5
    //   795: aload_1
    //   796: getfield densityDpi : I
    //   799: ifeq -> 825
    //   802: iload #4
    //   804: istore #5
    //   806: aload_0
    //   807: getfield densityDpi : I
    //   810: aload_1
    //   811: getfield densityDpi : I
    //   814: if_icmpeq -> 825
    //   817: iload #4
    //   819: sipush #4096
    //   822: ior
    //   823: istore #5
    //   825: iload_2
    //   826: ifne -> 840
    //   829: iload #5
    //   831: istore #4
    //   833: aload_1
    //   834: getfield assetsSeq : I
    //   837: ifeq -> 863
    //   840: iload #5
    //   842: istore #4
    //   844: aload_0
    //   845: getfield assetsSeq : I
    //   848: aload_1
    //   849: getfield assetsSeq : I
    //   852: if_icmpeq -> 863
    //   855: iload #5
    //   857: ldc_w -2147483648
    //   860: ior
    //   861: istore #4
    //   863: iload #4
    //   865: istore #5
    //   867: iload_3
    //   868: ifne -> 908
    //   871: aload_0
    //   872: getfield windowConfiguration : Landroid/app/WindowConfiguration;
    //   875: astore #6
    //   877: aload_1
    //   878: getfield windowConfiguration : Landroid/app/WindowConfiguration;
    //   881: astore #7
    //   883: iload #4
    //   885: istore #5
    //   887: aload #6
    //   889: aload #7
    //   891: iload_2
    //   892: invokevirtual diff : (Landroid/app/WindowConfiguration;Z)J
    //   895: lconst_0
    //   896: lcmp
    //   897: ifeq -> 908
    //   900: iload #4
    //   902: ldc_w 536870912
    //   905: ior
    //   906: istore #5
    //   908: aload_0
    //   909: getfield mOplusExtraConfiguration : Loplus/content/res/OplusExtraConfiguration;
    //   912: aload_1
    //   913: getfield mOplusExtraConfiguration : Loplus/content/res/OplusExtraConfiguration;
    //   916: invokevirtual diff : (Loplus/content/res/OplusExtraConfiguration;)I
    //   919: istore #4
    //   921: iload #5
    //   923: iload #4
    //   925: ior
    //   926: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1770	-> 0
    //   #1771	-> 3
    //   #1772	-> 36
    //   #1774	-> 43
    //   #1775	-> 73
    //   #1777	-> 79
    //   #1778	-> 109
    //   #1780	-> 115
    //   #1781	-> 119
    //   #1782	-> 123
    //   #1783	-> 153
    //   #1784	-> 167
    //   #1785	-> 167
    //   #1787	-> 177
    //   #1788	-> 187
    //   #1790	-> 217
    //   #1792	-> 225
    //   #1794	-> 255
    //   #1796	-> 262
    //   #1798	-> 292
    //   #1800	-> 299
    //   #1802	-> 329
    //   #1804	-> 336
    //   #1806	-> 366
    //   #1808	-> 373
    //   #1810	-> 403
    //   #1812	-> 410
    //   #1814	-> 440
    //   #1816	-> 447
    //   #1818	-> 477
    //   #1820	-> 485
    //   #1822	-> 509
    //   #1823	-> 522
    //   #1824	-> 536
    //   #1826	-> 544
    //   #1830	-> 583
    //   #1832	-> 591
    //   #1837	-> 627
    //   #1839	-> 635
    //   #1841	-> 665
    //   #1843	-> 673
    //   #1845	-> 703
    //   #1847	-> 711
    //   #1849	-> 741
    //   #1851	-> 749
    //   #1853	-> 779
    //   #1855	-> 787
    //   #1857	-> 817
    //   #1859	-> 825
    //   #1861	-> 855
    //   #1865	-> 863
    //   #1866	-> 883
    //   #1867	-> 900
    //   #1871	-> 908
    //   #1873	-> 921
  }
  
  public static boolean needNewResources(int paramInt1, int paramInt2) {
    return ((paramInt1 & (Integer.MIN_VALUE | paramInt2 | 0x40000000)) != 0 || 
      OplusExtraConfiguration.needNewResources(paramInt1) || 
      OplusExtraConfiguration.needAccessNewResources(paramInt1));
  }
  
  public boolean isOtherSeqNewer(Configuration paramConfiguration) {
    boolean bool = false;
    if (paramConfiguration == null)
      return false; 
    int i = paramConfiguration.seq;
    if (i == 0)
      return true; 
    int j = this.seq;
    if (j == 0)
      return true; 
    i -= j;
    if (i > 65536)
      return false; 
    if (i > 0)
      bool = true; 
    return bool;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeFloat(this.fontScale);
    paramParcel.writeInt(this.mcc);
    paramParcel.writeInt(this.mnc);
    fixUpLocaleList();
    paramParcel.writeParcelable((Parcelable)this.mLocaleList, paramInt);
    if (this.userSetLocale) {
      paramParcel.writeInt(1);
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.touchscreen);
    paramParcel.writeInt(this.keyboard);
    paramParcel.writeInt(this.keyboardHidden);
    paramParcel.writeInt(this.hardKeyboardHidden);
    paramParcel.writeInt(this.navigation);
    paramParcel.writeInt(this.navigationHidden);
    paramParcel.writeInt(this.orientation);
    paramParcel.writeInt(this.screenLayout);
    paramParcel.writeInt(this.colorMode);
    paramParcel.writeInt(this.uiMode);
    paramParcel.writeInt(this.screenWidthDp);
    paramParcel.writeInt(this.screenHeightDp);
    paramParcel.writeInt(this.smallestScreenWidthDp);
    paramParcel.writeInt(this.densityDpi);
    paramParcel.writeInt(this.compatScreenWidthDp);
    paramParcel.writeInt(this.compatScreenHeightDp);
    paramParcel.writeInt(this.compatSmallestScreenWidthDp);
    paramParcel.writeValue(this.windowConfiguration);
    paramParcel.writeInt(this.assetsSeq);
    paramParcel.writeInt(this.seq);
    this.mOplusExtraConfiguration.writeToParcel(paramParcel, paramInt);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.fontScale = paramParcel.readFloat();
    this.mcc = paramParcel.readInt();
    this.mnc = paramParcel.readInt();
    LocaleList localeList = (LocaleList)paramParcel.readParcelable(LocaleList.class.getClassLoader());
    boolean bool = false;
    this.locale = localeList.get(0);
    if (paramParcel.readInt() == 1)
      bool = true; 
    this.userSetLocale = bool;
    this.touchscreen = paramParcel.readInt();
    this.keyboard = paramParcel.readInt();
    this.keyboardHidden = paramParcel.readInt();
    this.hardKeyboardHidden = paramParcel.readInt();
    this.navigation = paramParcel.readInt();
    this.navigationHidden = paramParcel.readInt();
    this.orientation = paramParcel.readInt();
    this.screenLayout = paramParcel.readInt();
    this.colorMode = paramParcel.readInt();
    this.uiMode = paramParcel.readInt();
    this.screenWidthDp = paramParcel.readInt();
    this.screenHeightDp = paramParcel.readInt();
    this.smallestScreenWidthDp = paramParcel.readInt();
    this.densityDpi = paramParcel.readInt();
    this.compatScreenWidthDp = paramParcel.readInt();
    this.compatScreenHeightDp = paramParcel.readInt();
    this.compatSmallestScreenWidthDp = paramParcel.readInt();
    this.windowConfiguration.setTo((WindowConfiguration)paramParcel.readValue(null));
    this.assetsSeq = paramParcel.readInt();
    this.seq = paramParcel.readInt();
    this.mOplusExtraConfiguration.readFromParcel(paramParcel);
  }
  
  static {
    CREATOR = new Parcelable.Creator<Configuration>() {
        public Configuration createFromParcel(Parcel param1Parcel) {
          return new Configuration(param1Parcel);
        }
        
        public Configuration[] newArray(int param1Int) {
          return new Configuration[param1Int];
        }
      };
  }
  
  private Configuration(Parcel paramParcel) {
    this.mOplusExtraConfiguration = new OplusExtraConfiguration();
    readFromParcel(paramParcel);
  }
  
  public boolean isNightModeActive() {
    boolean bool;
    if ((this.uiMode & 0x30) == 32) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public int compareTo(Configuration paramConfiguration) {
    float f1 = this.fontScale;
    float f2 = paramConfiguration.fontScale;
    if (f1 < f2)
      return -1; 
    if (f1 > f2)
      return 1; 
    int i = this.mcc - paramConfiguration.mcc;
    if (i != 0)
      return i; 
    i = this.mnc - paramConfiguration.mnc;
    if (i != 0)
      return i; 
    fixUpLocaleList();
    paramConfiguration.fixUpLocaleList();
    if (this.mLocaleList.isEmpty()) {
      if (!paramConfiguration.mLocaleList.isEmpty())
        return 1; 
    } else {
      if (paramConfiguration.mLocaleList.isEmpty())
        return -1; 
      int j = Math.min(this.mLocaleList.size(), paramConfiguration.mLocaleList.size());
      for (i = 0; i < j; i++) {
        Locale locale1 = this.mLocaleList.get(i);
        Locale locale2 = paramConfiguration.mLocaleList.get(i);
        int k = locale1.getLanguage().compareTo(locale2.getLanguage());
        if (k != 0)
          return k; 
        k = locale1.getCountry().compareTo(locale2.getCountry());
        if (k != 0)
          return k; 
        k = locale1.getVariant().compareTo(locale2.getVariant());
        if (k != 0)
          return k; 
        k = locale1.toLanguageTag().compareTo(locale2.toLanguageTag());
        if (k != 0)
          return k; 
      } 
      i = this.mLocaleList.size() - paramConfiguration.mLocaleList.size();
      if (i != 0)
        return i; 
    } 
    i = this.touchscreen - paramConfiguration.touchscreen;
    if (i != 0)
      return i; 
    i = this.keyboard - paramConfiguration.keyboard;
    if (i != 0)
      return i; 
    i = this.keyboardHidden - paramConfiguration.keyboardHidden;
    if (i != 0)
      return i; 
    i = this.hardKeyboardHidden - paramConfiguration.hardKeyboardHidden;
    if (i != 0)
      return i; 
    i = this.navigation - paramConfiguration.navigation;
    if (i != 0)
      return i; 
    i = this.navigationHidden - paramConfiguration.navigationHidden;
    if (i != 0)
      return i; 
    i = this.orientation - paramConfiguration.orientation;
    if (i != 0)
      return i; 
    i = this.colorMode - paramConfiguration.colorMode;
    if (i != 0)
      return i; 
    i = this.screenLayout - paramConfiguration.screenLayout;
    if (i != 0)
      return i; 
    i = this.uiMode - paramConfiguration.uiMode;
    if (i != 0)
      return i; 
    i = this.screenWidthDp - paramConfiguration.screenWidthDp;
    if (i != 0)
      return i; 
    i = this.screenHeightDp - paramConfiguration.screenHeightDp;
    if (i != 0)
      return i; 
    i = this.smallestScreenWidthDp - paramConfiguration.smallestScreenWidthDp;
    if (i != 0)
      return i; 
    i = this.densityDpi - paramConfiguration.densityDpi;
    if (i != 0)
      return i; 
    i = this.assetsSeq - paramConfiguration.assetsSeq;
    if (i != 0)
      return i; 
    i = this.windowConfiguration.compareTo(paramConfiguration.windowConfiguration);
    if (i != 0)
      return i; 
    i = this.mOplusExtraConfiguration.compareTo(paramConfiguration.mOplusExtraConfiguration);
    return i;
  }
  
  public boolean equals(Configuration paramConfiguration) {
    boolean bool = false;
    if (paramConfiguration == null)
      return false; 
    if (paramConfiguration == this)
      return true; 
    if (compareTo(paramConfiguration) == 0)
      bool = true; 
    return bool;
  }
  
  public boolean equals(Object paramObject) {
    try {
      return equals((Configuration)paramObject);
    } catch (ClassCastException classCastException) {
      return false;
    } 
  }
  
  public int hashCode() {
    int i = Float.floatToIntBits(this.fontScale);
    int j = this.mcc;
    int k = this.mnc;
    int m = this.mLocaleList.hashCode();
    int n = this.touchscreen;
    int i1 = this.keyboard;
    int i2 = this.keyboardHidden;
    int i3 = this.hardKeyboardHidden;
    int i4 = this.navigation;
    int i5 = this.navigationHidden;
    int i6 = this.orientation;
    int i7 = this.screenLayout;
    int i8 = this.colorMode;
    int i9 = this.uiMode;
    int i10 = this.screenWidthDp;
    int i11 = this.screenHeightDp;
    int i12 = this.smallestScreenWidthDp;
    int i13 = this.densityDpi;
    int i14 = this.assetsSeq;
    return this.mOplusExtraConfiguration.hashCode() + ((((((((((((((((((17 * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2) * 31 + i3) * 31 + i4) * 31 + i5) * 31 + i6) * 31 + i7) * 31 + i8) * 31 + i9) * 31 + i10) * 31 + i11) * 31 + i12) * 31 + i13) * 31 + i14;
  }
  
  public LocaleList getLocales() {
    fixUpLocaleList();
    return this.mLocaleList;
  }
  
  public void setLocales(LocaleList paramLocaleList) {
    if (paramLocaleList == null)
      paramLocaleList = LocaleList.getEmptyLocaleList(); 
    this.mLocaleList = paramLocaleList;
    Locale locale = paramLocaleList.get(0);
    setLayoutDirection(locale);
  }
  
  public void setLocale(Locale paramLocale) {
    LocaleList localeList;
    if (paramLocale == null) {
      localeList = LocaleList.getEmptyLocaleList();
    } else {
      localeList = new LocaleList(new Locale[] { (Locale)localeList });
    } 
    setLocales(localeList);
  }
  
  public void clearLocales() {
    this.mLocaleList = LocaleList.getEmptyLocaleList();
    this.locale = null;
  }
  
  public int getLayoutDirection() {
    boolean bool;
    if ((this.screenLayout & 0xC0) == 128) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void setLayoutDirection(Locale paramLocale) {
    int i = TextUtils.getLayoutDirectionFromLocale(paramLocale);
    this.screenLayout = this.screenLayout & 0xFFFFFF3F | i + 1 << 6;
  }
  
  private static int getScreenLayoutNoDirection(int paramInt) {
    return paramInt & 0xFFFFFF3F;
  }
  
  public boolean isScreenRound() {
    boolean bool;
    if ((this.screenLayout & 0x300) == 512) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isScreenWideColorGamut() {
    boolean bool;
    if ((this.colorMode & 0x3) == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isScreenHdr() {
    boolean bool;
    if ((this.colorMode & 0xC) == 8) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static String localesToResourceQualifier(LocaleList paramLocaleList) {
    StringBuilder stringBuilder = new StringBuilder();
    for (byte b = 0; b < paramLocaleList.size(); b++) {
      Locale locale = paramLocaleList.get(b);
      int i = locale.getLanguage().length();
      if (i != 0) {
        int j = locale.getScript().length();
        int k = locale.getCountry().length();
        int m = locale.getVariant().length();
        if (stringBuilder.length() != 0)
          stringBuilder.append(","); 
        if (i == 2 && j == 0 && (k == 0 || k == 2) && m == 0) {
          stringBuilder.append(locale.getLanguage());
          if (k == 2) {
            stringBuilder.append("-r");
            stringBuilder.append(locale.getCountry());
          } 
        } else {
          stringBuilder.append("b+");
          stringBuilder.append(locale.getLanguage());
          if (j != 0) {
            stringBuilder.append("+");
            stringBuilder.append(locale.getScript());
          } 
          if (k != 0) {
            stringBuilder.append("+");
            stringBuilder.append(locale.getCountry());
          } 
          if (m != 0) {
            stringBuilder.append("+");
            stringBuilder.append(locale.getVariant());
          } 
        } 
      } 
    } 
    return stringBuilder.toString();
  }
  
  public static String resourceQualifierString(Configuration paramConfiguration) {
    return resourceQualifierString(paramConfiguration, null);
  }
  
  public static String resourceQualifierString(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics) {
    ArrayList<String> arrayList = new ArrayList();
    if (paramConfiguration.mcc != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("mcc");
      stringBuilder1.append(paramConfiguration.mcc);
      arrayList.add(stringBuilder1.toString());
      if (paramConfiguration.mnc != 0) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("mnc");
        stringBuilder1.append(paramConfiguration.mnc);
        arrayList.add(stringBuilder1.toString());
      } 
    } 
    if (!paramConfiguration.mLocaleList.isEmpty()) {
      String str = localesToResourceQualifier(paramConfiguration.mLocaleList);
      if (!str.isEmpty())
        arrayList.add(str); 
    } 
    int i = paramConfiguration.screenLayout & 0xC0;
    if (i != 64) {
      if (i == 128)
        arrayList.add("ldrtl"); 
    } else {
      arrayList.add("ldltr");
    } 
    if (paramConfiguration.smallestScreenWidthDp != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("sw");
      stringBuilder1.append(paramConfiguration.smallestScreenWidthDp);
      stringBuilder1.append("dp");
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramConfiguration.screenWidthDp != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("w");
      stringBuilder1.append(paramConfiguration.screenWidthDp);
      stringBuilder1.append("dp");
      arrayList.add(stringBuilder1.toString());
    } 
    if (paramConfiguration.screenHeightDp != 0) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("h");
      stringBuilder1.append(paramConfiguration.screenHeightDp);
      stringBuilder1.append("dp");
      arrayList.add(stringBuilder1.toString());
    } 
    i = paramConfiguration.screenLayout & 0xF;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i == 4)
            arrayList.add("xlarge"); 
        } else {
          arrayList.add("large");
        } 
      } else {
        arrayList.add("normal");
      } 
    } else {
      arrayList.add("small");
    } 
    i = paramConfiguration.screenLayout & 0x30;
    if (i != 16) {
      if (i == 32)
        arrayList.add("long"); 
    } else {
      arrayList.add("notlong");
    } 
    i = paramConfiguration.screenLayout & 0x300;
    if (i != 256) {
      if (i == 512)
        arrayList.add("round"); 
    } else {
      arrayList.add("notround");
    } 
    i = paramConfiguration.colorMode & 0x3;
    if (i != 1) {
      if (i == 2)
        arrayList.add("widecg"); 
    } else {
      arrayList.add("nowidecg");
    } 
    i = paramConfiguration.colorMode & 0xC;
    if (i != 4) {
      if (i == 8)
        arrayList.add("highdr"); 
    } else {
      arrayList.add("lowdr");
    } 
    i = paramConfiguration.orientation;
    if (i != 1) {
      if (i == 2)
        arrayList.add("land"); 
    } else {
      arrayList.add("port");
    } 
    switch (paramConfiguration.uiMode & 0xF) {
      case 7:
        arrayList.add("vrheadset");
        break;
      case 6:
        arrayList.add("watch");
        break;
      case 5:
        arrayList.add("appliance");
        break;
      case 4:
        arrayList.add("television");
        break;
      case 3:
        arrayList.add("car");
        break;
      case 2:
        arrayList.add("desk");
        break;
    } 
    i = paramConfiguration.uiMode & 0x30;
    if (i != 16) {
      if (i == 32)
        arrayList.add("night"); 
    } else {
      arrayList.add("notnight");
    } 
    i = paramConfiguration.densityDpi;
    if (i != 0)
      if (i != 120) {
        if (i != 160) {
          if (i != 213) {
            if (i != 240) {
              if (i != 320) {
                if (i != 480) {
                  if (i != 640) {
                    StringBuilder stringBuilder1;
                    switch (i) {
                      default:
                        stringBuilder1 = new StringBuilder();
                        stringBuilder1.append(paramConfiguration.densityDpi);
                        stringBuilder1.append("dpi");
                        arrayList.add(stringBuilder1.toString());
                        break;
                      case 65535:
                        arrayList.add("nodpi");
                        break;
                      case 65534:
                        arrayList.add("anydpi");
                        break;
                    } 
                  } else {
                    arrayList.add("xxxhdpi");
                  } 
                } else {
                  arrayList.add("xxhdpi");
                } 
              } else {
                arrayList.add("xhdpi");
              } 
            } else {
              arrayList.add("hdpi");
            } 
          } else {
            arrayList.add("tvdpi");
          } 
        } else {
          arrayList.add("mdpi");
        } 
      } else {
        arrayList.add("ldpi");
      }  
    i = paramConfiguration.touchscreen;
    if (i != 1) {
      if (i == 3)
        arrayList.add("finger"); 
    } else {
      arrayList.add("notouch");
    } 
    i = paramConfiguration.keyboardHidden;
    if (i != 1) {
      if (i != 2) {
        if (i == 3)
          arrayList.add("keyssoft"); 
      } else {
        arrayList.add("keyshidden");
      } 
    } else {
      arrayList.add("keysexposed");
    } 
    i = paramConfiguration.keyboard;
    if (i != 1) {
      if (i != 2) {
        if (i == 3)
          arrayList.add("12key"); 
      } else {
        arrayList.add("qwerty");
      } 
    } else {
      arrayList.add("nokeys");
    } 
    i = paramConfiguration.navigationHidden;
    if (i != 1) {
      if (i == 2)
        arrayList.add("navhidden"); 
    } else {
      arrayList.add("navexposed");
    } 
    i = paramConfiguration.navigation;
    if (i != 1) {
      if (i != 2) {
        if (i != 3) {
          if (i == 4)
            arrayList.add("wheel"); 
        } else {
          arrayList.add("trackball");
        } 
      } else {
        arrayList.add("dpad");
      } 
    } else {
      arrayList.add("nonav");
    } 
    if (paramDisplayMetrics != null) {
      int j;
      if (paramDisplayMetrics.widthPixels >= paramDisplayMetrics.heightPixels) {
        i = paramDisplayMetrics.widthPixels;
        j = paramDisplayMetrics.heightPixels;
      } else {
        i = paramDisplayMetrics.heightPixels;
        j = paramDisplayMetrics.widthPixels;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(i);
      stringBuilder1.append("x");
      stringBuilder1.append(j);
      arrayList.add(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("v");
    stringBuilder.append(Build.VERSION.RESOURCES_SDK_INT);
    arrayList.add(stringBuilder.toString());
    return TextUtils.join("-", arrayList);
  }
  
  public static Configuration generateDelta(Configuration paramConfiguration1, Configuration paramConfiguration2) {
    Configuration configuration = new Configuration();
    float f1 = paramConfiguration1.fontScale, f2 = paramConfiguration2.fontScale;
    if (f1 != f2)
      configuration.fontScale = f2; 
    int i = paramConfiguration1.mcc, j = paramConfiguration2.mcc;
    if (i != j)
      configuration.mcc = j; 
    i = paramConfiguration1.mnc;
    j = paramConfiguration2.mnc;
    if (i != j)
      configuration.mnc = j; 
    paramConfiguration1.fixUpLocaleList();
    paramConfiguration2.fixUpLocaleList();
    if (!paramConfiguration1.mLocaleList.equals(paramConfiguration2.mLocaleList)) {
      configuration.mLocaleList = paramConfiguration2.mLocaleList;
      configuration.locale = paramConfiguration2.locale;
    } 
    i = paramConfiguration1.touchscreen;
    j = paramConfiguration2.touchscreen;
    if (i != j)
      configuration.touchscreen = j; 
    i = paramConfiguration1.keyboard;
    j = paramConfiguration2.keyboard;
    if (i != j)
      configuration.keyboard = j; 
    i = paramConfiguration1.keyboardHidden;
    j = paramConfiguration2.keyboardHidden;
    if (i != j)
      configuration.keyboardHidden = j; 
    i = paramConfiguration1.navigation;
    j = paramConfiguration2.navigation;
    if (i != j)
      configuration.navigation = j; 
    j = paramConfiguration1.navigationHidden;
    i = paramConfiguration2.navigationHidden;
    if (j != i)
      configuration.navigationHidden = i; 
    i = paramConfiguration1.orientation;
    j = paramConfiguration2.orientation;
    if (i != j)
      configuration.orientation = j; 
    j = paramConfiguration1.screenLayout;
    i = paramConfiguration2.screenLayout;
    if ((j & 0xF) != (i & 0xF))
      configuration.screenLayout |= i & 0xF; 
    i = paramConfiguration1.screenLayout;
    j = paramConfiguration2.screenLayout;
    if ((i & 0xC0) != (j & 0xC0))
      configuration.screenLayout |= j & 0xC0; 
    i = paramConfiguration1.screenLayout;
    j = paramConfiguration2.screenLayout;
    if ((i & 0x30) != (j & 0x30))
      configuration.screenLayout |= j & 0x30; 
    i = paramConfiguration1.screenLayout;
    j = paramConfiguration2.screenLayout;
    if ((i & 0x300) != (j & 0x300))
      configuration.screenLayout |= j & 0x300; 
    i = paramConfiguration1.colorMode;
    j = paramConfiguration2.colorMode;
    if ((i & 0x3) != (j & 0x3))
      configuration.colorMode |= j & 0x3; 
    j = paramConfiguration1.colorMode;
    i = paramConfiguration2.colorMode;
    if ((j & 0xC) != (i & 0xC))
      configuration.colorMode |= i & 0xC; 
    i = paramConfiguration1.uiMode;
    j = paramConfiguration2.uiMode;
    if ((i & 0xF) != (j & 0xF))
      configuration.uiMode |= j & 0xF; 
    i = paramConfiguration1.uiMode;
    j = paramConfiguration2.uiMode;
    if ((i & 0x30) != (j & 0x30))
      configuration.uiMode |= j & 0x30; 
    i = paramConfiguration1.screenWidthDp;
    j = paramConfiguration2.screenWidthDp;
    if (i != j)
      configuration.screenWidthDp = j; 
    j = paramConfiguration1.screenHeightDp;
    i = paramConfiguration2.screenHeightDp;
    if (j != i)
      configuration.screenHeightDp = i; 
    i = paramConfiguration1.smallestScreenWidthDp;
    j = paramConfiguration2.smallestScreenWidthDp;
    if (i != j)
      configuration.smallestScreenWidthDp = j; 
    j = paramConfiguration1.densityDpi;
    i = paramConfiguration2.densityDpi;
    if (j != i)
      configuration.densityDpi = i; 
    j = paramConfiguration1.assetsSeq;
    i = paramConfiguration2.assetsSeq;
    if (j != i)
      configuration.assetsSeq = i; 
    if (!paramConfiguration1.windowConfiguration.equals(paramConfiguration2.windowConfiguration))
      configuration.windowConfiguration.setTo(paramConfiguration2.windowConfiguration); 
    return configuration;
  }
  
  public static void readXmlAttrs(XmlPullParser paramXmlPullParser, Configuration paramConfiguration) throws XmlPullParserException, IOException {
    int i = XmlUtils.readIntAttribute(paramXmlPullParser, "fs", 0);
    paramConfiguration.fontScale = Float.intBitsToFloat(i);
    paramConfiguration.mcc = XmlUtils.readIntAttribute(paramXmlPullParser, "mcc", 0);
    paramConfiguration.mnc = XmlUtils.readIntAttribute(paramXmlPullParser, "mnc", 0);
    String str = XmlUtils.readStringAttribute(paramXmlPullParser, "locales");
    LocaleList localeList = LocaleList.forLanguageTags(str);
    paramConfiguration.locale = localeList.get(0);
    paramConfiguration.touchscreen = XmlUtils.readIntAttribute(paramXmlPullParser, "touch", 0);
    paramConfiguration.keyboard = XmlUtils.readIntAttribute(paramXmlPullParser, "key", 0);
    paramConfiguration.keyboardHidden = XmlUtils.readIntAttribute(paramXmlPullParser, "keyHid", 0);
    paramConfiguration.hardKeyboardHidden = XmlUtils.readIntAttribute(paramXmlPullParser, "hardKeyHid", 0);
    paramConfiguration.navigation = XmlUtils.readIntAttribute(paramXmlPullParser, "nav", 0);
    paramConfiguration.navigationHidden = XmlUtils.readIntAttribute(paramXmlPullParser, "navHid", 0);
    paramConfiguration.orientation = XmlUtils.readIntAttribute(paramXmlPullParser, "ori", 0);
    paramConfiguration.screenLayout = XmlUtils.readIntAttribute(paramXmlPullParser, "scrLay", 0);
    paramConfiguration.colorMode = XmlUtils.readIntAttribute(paramXmlPullParser, "clrMod", 0);
    paramConfiguration.uiMode = XmlUtils.readIntAttribute(paramXmlPullParser, "ui", 0);
    paramConfiguration.screenWidthDp = XmlUtils.readIntAttribute(paramXmlPullParser, "width", 0);
    paramConfiguration.screenHeightDp = XmlUtils.readIntAttribute(paramXmlPullParser, "height", 0);
    paramConfiguration.smallestScreenWidthDp = XmlUtils.readIntAttribute(paramXmlPullParser, "sw", 0);
    paramConfiguration.densityDpi = XmlUtils.readIntAttribute(paramXmlPullParser, "density", 0);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class NativeConfig implements Annotation {}
  
  class Orientation implements Annotation {}
}
