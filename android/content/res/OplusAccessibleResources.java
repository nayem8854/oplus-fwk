package android.content.res;

import java.lang.ref.WeakReference;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipFile;
import oplus.content.res.OplusExtraConfiguration;

public class OplusAccessibleResources {
  private static Map<String, WeakReference<OplusAccessibleResources>> mPackageCaches = new ConcurrentHashMap<>();
  
  private String mPackageName = null;
  
  private OplusAccessibleFile mAccessible = null;
  
  private OplusMaterialFile mMaterialFile = null;
  
  private OplusBaseResourcesImpl mResources = null;
  
  private boolean mHasValues = false;
  
  private boolean mHasDrawables = false;
  
  private boolean mHasColorValues = false;
  
  private boolean mNightMode = false;
  
  public OplusAccessibleResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    this.mPackageName = paramString;
    this.mResources = paramOplusBaseResourcesImpl;
    checkUpdate();
  }
  
  public static OplusAccessibleResources getAccessResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    OplusAccessibleResources oplusAccessibleResources1 = null;
    OplusAccessibleResources oplusAccessibleResources2 = oplusAccessibleResources1;
    if (mPackageCaches.containsKey(paramString)) {
      WeakReference<OplusAccessibleResources> weakReference = mPackageCaches.get(paramString);
      oplusAccessibleResources2 = oplusAccessibleResources1;
      if (weakReference != null)
        oplusAccessibleResources2 = weakReference.get(); 
    } 
    if (oplusAccessibleResources2 != null) {
      oplusAccessibleResources2.setResources(paramOplusBaseResourcesImpl);
      oplusAccessibleResources2.checkUpdate();
    } else {
      oplusAccessibleResources2 = new OplusAccessibleResources(paramOplusBaseResourcesImpl, paramString);
      mPackageCaches.put(paramString, new WeakReference<>(oplusAccessibleResources2));
    } 
    return oplusAccessibleResources2;
  }
  
  public void checkUpdate() {
    checkAssetUpdate();
    checkColorUpdate();
  }
  
  public void setResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    this.mResources = paramOplusBaseResourcesImpl;
  }
  
  public boolean hasDrawables() {
    return this.mHasDrawables;
  }
  
  public boolean hasValues() {
    return this.mHasValues | this.mHasColorValues;
  }
  
  public Integer getAccessibleInt(int paramInt) {
    OplusMaterialFile oplusMaterialFile = this.mMaterialFile;
    if (oplusMaterialFile != null && this.mHasColorValues) {
      if (!oplusMaterialFile.isLoadValue())
        this.mMaterialFile.initMaterial(); 
      return this.mMaterialFile.getInt(paramInt);
    } 
    OplusAccessibleFile oplusAccessibleFile = this.mAccessible;
    if (oplusAccessibleFile != null && this.mHasValues) {
      if (!oplusAccessibleFile.hasValues())
        this.mAccessible.initValue(); 
      return this.mAccessible.getInt(paramInt);
    } 
    return null;
  }
  
  public CharSequence getAccessibleChars(int paramInt) {
    OplusAccessibleFile oplusAccessibleFile = this.mAccessible;
    if (oplusAccessibleFile != null && this.mHasValues)
      return oplusAccessibleFile.getCharSequence(paramInt); 
    return null;
  }
  
  public OplusThemeZipFile.ThemeFileInfo getAccessibleStream(int paramInt, String paramString) {
    OplusAccessibleFile oplusAccessibleFile = this.mAccessible;
    if (oplusAccessibleFile != null && this.mHasDrawables)
      return oplusAccessibleFile.getAssetInputStream(paramInt, paramString); 
    return null;
  }
  
  private boolean isAssetEnable() {
    OplusExtraConfiguration oplusExtraConfiguration = this.mResources.getSystemConfiguration().getOplusExtraConfiguration();
    boolean bool1 = false;
    if (oplusExtraConfiguration == null)
      return false; 
    int i = oplusExtraConfiguration.mAccessibleChanged;
    OplusBaseResourcesImpl oplusBaseResourcesImpl = this.mResources;
    boolean bool = OplusBaseFile.isNightMode(oplusBaseResourcesImpl.typeCasting(oplusBaseResourcesImpl));
    boolean bool2 = bool1;
    if (i > 0) {
      bool2 = bool1;
      if (!bool)
        bool2 = true; 
    } 
    return bool2;
  }
  
  private boolean isMaterialColorEnable() {
    OplusExtraConfiguration oplusExtraConfiguration = this.mResources.getSystemConfiguration().getOplusExtraConfiguration();
    if (oplusExtraConfiguration == null)
      return false; 
    long l = oplusExtraConfiguration.mMaterialColor;
    if ((l & 0x20000L) == 131072L || (l & 0x100000L) == 1048576L)
      return true; 
    return false;
  }
  
  private void checkAssetUpdate() {
    if (!isAssetEnable()) {
      OplusAccessibleFile oplusAccessibleFile1 = this.mAccessible;
      if (oplusAccessibleFile1 != null)
        oplusAccessibleFile1.clearCache((ZipFile)null); 
      this.mHasDrawables = false;
      this.mHasValues = false;
      return;
    } 
    OplusAccessibleFile oplusAccessibleFile = OplusAccessibleFile.getAssetFile(this.mPackageName, this.mResources);
    if (oplusAccessibleFile != null) {
      this.mHasValues = oplusAccessibleFile.hasAssetValues();
      this.mHasDrawables = this.mAccessible.hasDrawables();
    } else {
      this.mHasValues = false;
      this.mHasDrawables = false;
    } 
  }
  
  private void checkColorUpdate() {
    if (!isMaterialColorEnable()) {
      OplusMaterialFile oplusMaterialFile1 = this.mMaterialFile;
      if (oplusMaterialFile1 != null)
        oplusMaterialFile1.clears(); 
      this.mHasColorValues = false;
      return;
    } 
    OplusMaterialFile oplusMaterialFile = OplusMaterialFile.getMaterialFile(this.mPackageName, this.mResources);
    if (oplusMaterialFile != null && oplusMaterialFile.isMetaEnable()) {
      this.mMaterialFile.clears();
      this.mHasColorValues = true;
    } else {
      this.mHasColorValues = false;
    } 
  }
}
