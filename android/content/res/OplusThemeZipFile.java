package android.content.res;

import android.util.Log;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import oplus.content.res.OplusExtraConfiguration;
import oplus.util.OplusDisplayUtils;

public class OplusThemeZipFile extends OplusBaseFile {
  private static final String[] RESOURCES_PATHS = new String[] { "res/drawable", "framework-res/res/drawable", "oppo-framework-res/res/drawable", "res/drawable" };
  
  static {
    ASSETS_THEME_VALUE_FILES = new String[] { "assets/colors.xml", "framework-res/assets/colors.xml", "oppo-framework-res/assets/colors.xml", "assets/values-cn/colors.xml", "assets/values-en/colors.xml", "assets/values-tw/colors.xml" };
    mEntryCache = new ConcurrentHashMap<>();
  }
  
  private long mLastModifyTime = -1L;
  
  private String mPath = null;
  
  private OplusThemeResources.MetaData mMetaData = null;
  
  private MultiZipFile mZipFile = null;
  
  protected boolean mHasInit = false;
  
  private static final int ASSETS_THEME_FILE_INDEX_CN = 3;
  
  private static final int ASSETS_THEME_FILE_INDEX_EN = 4;
  
  private static final int ASSETS_THEME_FILE_INDEX_TW = 5;
  
  private static final int ASSETS_THEME_FILE_USE_COUNT = 3;
  
  private static final String[] ASSETS_THEME_VALUE_FILES;
  
  private static final String TAG = "OplusThemeZipFile";
  
  private static final ConcurrentHashMap<String, ZipEntry> mEntryCache;
  
  class ThemeFileInfo {
    public int mDensity;
    
    public InputStream mInput;
    
    public long mSize;
    
    ThemeFileInfo(OplusThemeZipFile this$0, long param1Long) {
      this.mInput = (InputStream)this$0;
      this.mSize = param1Long;
    }
  }
  
  public OplusThemeZipFile(String paramString1, OplusThemeResources.MetaData paramMetaData, String paramString2, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    super(paramString2, paramOplusBaseResourcesImpl, paramMetaData.supportInt, paramMetaData.supportCharSequence, paramMetaData.supportFile);
    this.mLastModifyTime = -1L;
    this.mPath = paramString1;
    this.mMetaData = paramMetaData;
  }
  
  public boolean initZipFile() {
    checkPathForUser();
    boolean bool = false;
    if (this.mZipFile != null)
      clear(); 
    openZipFile();
    if (this.mPackageName.equals("android") || this.mPackageName.equals("oplus")) {
      loadThemeValues(0, this.mZipFile);
      bool = true;
      this.mHasInit = bool;
      return bool;
    } 
    if (this.mZipFile != null) {
      for (byte b = 0; b < 3; b++)
        loadThemeValues(b, this.mZipFile); 
      bool = true;
    } 
    this.mHasInit = bool;
    return bool;
  }
  
  public ThemeFileInfo getInputStream(String paramString) {
    return getInputStream(0, paramString);
  }
  
  public ThemeFileInfo getInputStream(int paramInt, String paramString) {
    ThemeFileInfo themeFileInfo1;
    if (!this.mMetaData.supportFile || this.mZipFile == null)
      return null; 
    if (paramString.endsWith(".xml")) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString.substring(0, paramString.lastIndexOf(".")));
      stringBuilder.append(".png");
      String str = stringBuilder.toString();
      ThemeFileInfo themeFileInfo = getInputStreamInner(paramInt, str, this.mZipFile, false);
      themeFileInfo1 = themeFileInfo;
      if (themeFileInfo == null) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str.substring(0, str.lastIndexOf(".")));
        stringBuilder1.append(".9.png");
        String str1 = stringBuilder1.toString();
        themeFileInfo1 = getInputStreamInner(paramInt, str1, this.mZipFile, false);
      } 
      return themeFileInfo1;
    } 
    ThemeFileInfo themeFileInfo3 = getInputStreamInner(paramInt, (String)themeFileInfo1, this.mZipFile, false);
    ThemeFileInfo themeFileInfo2 = themeFileInfo3;
    if (themeFileInfo1.endsWith(".9.png")) {
      themeFileInfo2 = themeFileInfo3;
      if (themeFileInfo3 == null) {
        String str = themeFileInfo1.replace(".9.png", ".png");
        themeFileInfo2 = getInputStreamInner(paramInt, str, this.mZipFile, false);
      } 
    } 
    return themeFileInfo2;
  }
  
  public ThemeFileInfo getIconInputStream(String paramString) {
    if (this.mMetaData.supportFile) {
      MultiZipFile multiZipFile = this.mZipFile;
      if (multiZipFile != null) {
        ThemeFileInfo themeFileInfo2 = getInputStreamInner(0, paramString, multiZipFile, true);
        ThemeFileInfo themeFileInfo1 = themeFileInfo2;
        if (paramString.endsWith(".xml")) {
          themeFileInfo1 = themeFileInfo2;
          if (themeFileInfo2 == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(paramString.substring(0, paramString.lastIndexOf(".")));
            stringBuilder.append(".png");
            paramString = stringBuilder.toString();
            themeFileInfo1 = getInputStreamInner(0, paramString, this.mZipFile, true);
          } 
        } 
        return themeFileInfo1;
      } 
    } 
    return null;
  }
  
  public boolean isZipFileValid(boolean paramBoolean) {
    // Byte code:
    //   0: iconst_1
    //   1: istore_2
    //   2: iload_1
    //   3: ifne -> 8
    //   6: iconst_1
    //   7: ireturn
    //   8: new java/io/File
    //   11: dup
    //   12: aload_0
    //   13: getfield mPath : Ljava/lang/String;
    //   16: invokespecial <init> : (Ljava/lang/String;)V
    //   19: astore_3
    //   20: aload_3
    //   21: invokevirtual lastModified : ()J
    //   24: lstore #4
    //   26: aload_3
    //   27: invokevirtual exists : ()Z
    //   30: ifeq -> 60
    //   33: aload_0
    //   34: getfield mLastModifyTime : J
    //   37: lstore #6
    //   39: iload_2
    //   40: istore_1
    //   41: lload #6
    //   43: ldc2_w -1
    //   46: lcmp
    //   47: ifeq -> 139
    //   50: iload_2
    //   51: istore_1
    //   52: lload #6
    //   54: lload #4
    //   56: lcmp
    //   57: ifeq -> 139
    //   60: iconst_0
    //   61: istore_2
    //   62: iload_2
    //   63: istore_1
    //   64: getstatic android/content/res/OplusThemeZipFile.DEBUG_THEME : Z
    //   67: ifeq -> 139
    //   70: new java/lang/StringBuilder
    //   73: dup
    //   74: invokespecial <init> : ()V
    //   77: astore_3
    //   78: aload_3
    //   79: ldc_w 'check zip invalid: '
    //   82: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   85: pop
    //   86: aload_3
    //   87: aload_0
    //   88: getfield mPath : Ljava/lang/String;
    //   91: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   94: pop
    //   95: aload_3
    //   96: ldc_w ' mLastModifyTime= '
    //   99: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   102: pop
    //   103: aload_3
    //   104: aload_0
    //   105: getfield mLastModifyTime : J
    //   108: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   111: pop
    //   112: aload_3
    //   113: ldc_w ' modifyTime= '
    //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: aload_3
    //   121: lload #4
    //   123: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: ldc 'OplusThemeZipFile'
    //   129: aload_3
    //   130: invokevirtual toString : ()Ljava/lang/String;
    //   133: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   136: pop
    //   137: iload_2
    //   138: istore_1
    //   139: iload_1
    //   140: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #165	-> 0
    //   #166	-> 2
    //   #167	-> 6
    //   #169	-> 8
    //   #170	-> 20
    //   #171	-> 26
    //   #172	-> 60
    //   #173	-> 62
    //   #176	-> 139
  }
  
  public boolean containsEntry(String paramString) {
    boolean bool;
    MultiZipFile multiZipFile = this.mZipFile;
    if (multiZipFile != null && multiZipFile.getEntry(paramString) != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean exists() {
    return (new File(this.mPath)).exists();
  }
  
  protected static OplusThemeZipFile getThemeZipFile(OplusThemeResources.MetaData paramMetaData, String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    // Byte code:
    //   0: ldc android/content/res/OplusThemeZipFile
    //   2: monitorenter
    //   3: aload_0
    //   4: ifnonnull -> 12
    //   7: ldc android/content/res/OplusThemeZipFile
    //   9: monitorexit
    //   10: aconst_null
    //   11: areturn
    //   12: getstatic android/content/res/OplusThemeResources.SYSTEM_THEME_PATH : Ljava/lang/String;
    //   15: aload_0
    //   16: getfield themePath : Ljava/lang/String;
    //   19: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   22: ifne -> 189
    //   25: getstatic android/content/res/OplusThemeResources.CUSTOM_THEME_PATH : Ljava/lang/String;
    //   28: aload_0
    //   29: getfield themePath : Ljava/lang/String;
    //   32: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   35: ifeq -> 41
    //   38: goto -> 189
    //   41: iconst_0
    //   42: istore_3
    //   43: ldc 'framework-res'
    //   45: aload_1
    //   46: invokevirtual equals : (Ljava/lang/Object;)Z
    //   49: ifne -> 76
    //   52: ldc 'oppo-framework-res'
    //   54: aload_1
    //   55: invokevirtual equals : (Ljava/lang/Object;)Z
    //   58: ifeq -> 64
    //   61: goto -> 76
    //   64: aload_2
    //   65: invokevirtual getSystemConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   68: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   71: astore #4
    //   73: goto -> 85
    //   76: aload_2
    //   77: invokevirtual getConfiguration : ()Landroid/content/res/OplusBaseConfiguration;
    //   80: invokevirtual getOplusExtraConfiguration : ()Loplus/content/res/OplusExtraConfiguration;
    //   83: astore #4
    //   85: aload #4
    //   87: ifnull -> 96
    //   90: aload #4
    //   92: getfield mUserId : I
    //   95: istore_3
    //   96: iload_3
    //   97: ifgt -> 137
    //   100: new java/lang/StringBuilder
    //   103: astore #4
    //   105: aload #4
    //   107: invokespecial <init> : ()V
    //   110: aload #4
    //   112: aload_0
    //   113: getfield themePath : Ljava/lang/String;
    //   116: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   119: pop
    //   120: aload #4
    //   122: aload_1
    //   123: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload #4
    //   129: invokevirtual toString : ()Ljava/lang/String;
    //   132: astore #4
    //   134: goto -> 223
    //   137: new java/lang/StringBuilder
    //   140: astore #4
    //   142: aload #4
    //   144: invokespecial <init> : ()V
    //   147: aload #4
    //   149: aload_0
    //   150: getfield themePath : Ljava/lang/String;
    //   153: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   156: pop
    //   157: aload #4
    //   159: iload_3
    //   160: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   163: pop
    //   164: aload #4
    //   166: ldc '/'
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload #4
    //   174: aload_1
    //   175: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   178: pop
    //   179: aload #4
    //   181: invokevirtual toString : ()Ljava/lang/String;
    //   184: astore #4
    //   186: goto -> 223
    //   189: new java/lang/StringBuilder
    //   192: astore #4
    //   194: aload #4
    //   196: invokespecial <init> : ()V
    //   199: aload #4
    //   201: aload_0
    //   202: getfield themePath : Ljava/lang/String;
    //   205: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   208: pop
    //   209: aload #4
    //   211: aload_1
    //   212: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   215: pop
    //   216: aload #4
    //   218: invokevirtual toString : ()Ljava/lang/String;
    //   221: astore #4
    //   223: iconst_0
    //   224: istore #5
    //   226: new java/io/File
    //   229: astore #6
    //   231: aload #6
    //   233: aload #4
    //   235: invokespecial <init> : (Ljava/lang/String;)V
    //   238: iload #5
    //   240: istore_3
    //   241: aload #6
    //   243: invokevirtual exists : ()Z
    //   246: ifeq -> 266
    //   249: aload #6
    //   251: invokevirtual isDirectory : ()Z
    //   254: istore #7
    //   256: iload #5
    //   258: istore_3
    //   259: iload #7
    //   261: ifne -> 266
    //   264: iconst_1
    //   265: istore_3
    //   266: goto -> 327
    //   269: astore_0
    //   270: goto -> 429
    //   273: astore #8
    //   275: iload #5
    //   277: istore_3
    //   278: getstatic android/content/res/OplusThemeZipFile.DEBUG_THEME : Z
    //   281: ifeq -> 266
    //   284: new java/lang/StringBuilder
    //   287: astore #6
    //   289: aload #6
    //   291: invokespecial <init> : ()V
    //   294: aload #6
    //   296: ldc 'getThemeZipFile Exception e: '
    //   298: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   301: pop
    //   302: aload #6
    //   304: aload #8
    //   306: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   309: pop
    //   310: ldc 'OplusThemeZipFile'
    //   312: aload #6
    //   314: invokevirtual toString : ()Ljava/lang/String;
    //   317: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   320: pop
    //   321: iload #5
    //   323: istore_3
    //   324: goto -> 266
    //   327: iload_3
    //   328: ifne -> 336
    //   331: ldc android/content/res/OplusThemeZipFile
    //   333: monitorexit
    //   334: aconst_null
    //   335: areturn
    //   336: getstatic android/content/res/OplusThemeZipFile.sCacheFiles : Ljava/util/Map;
    //   339: aload #4
    //   341: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   346: checkcast java/lang/ref/WeakReference
    //   349: astore #6
    //   351: aload #6
    //   353: ifnull -> 369
    //   356: aload #6
    //   358: invokevirtual get : ()Ljava/lang/Object;
    //   361: checkcast android/content/res/OplusThemeZipFile
    //   364: astore #6
    //   366: goto -> 372
    //   369: aconst_null
    //   370: astore #6
    //   372: aload #6
    //   374: ifnull -> 383
    //   377: ldc android/content/res/OplusThemeZipFile
    //   379: monitorexit
    //   380: aload #6
    //   382: areturn
    //   383: new android/content/res/OplusThemeZipFile
    //   386: astore #6
    //   388: aload #6
    //   390: aload #4
    //   392: aload_0
    //   393: aload_1
    //   394: invokestatic getPackageName : (Ljava/lang/String;)Ljava/lang/String;
    //   397: aload_2
    //   398: invokespecial <init> : (Ljava/lang/String;Landroid/content/res/OplusThemeResources$MetaData;Ljava/lang/String;Landroid/content/res/OplusBaseResourcesImpl;)V
    //   401: new java/lang/ref/WeakReference
    //   404: astore_0
    //   405: aload_0
    //   406: aload #6
    //   408: invokespecial <init> : (Ljava/lang/Object;)V
    //   411: getstatic android/content/res/OplusThemeZipFile.sCacheFiles : Ljava/util/Map;
    //   414: aload #4
    //   416: aload_0
    //   417: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   422: pop
    //   423: ldc android/content/res/OplusThemeZipFile
    //   425: monitorexit
    //   426: aload #6
    //   428: areturn
    //   429: aload_0
    //   430: athrow
    //   431: astore_0
    //   432: ldc android/content/res/OplusThemeZipFile
    //   434: monitorexit
    //   435: aload_0
    //   436: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #198	-> 3
    //   #199	-> 7
    //   #201	-> 12
    //   #202	-> 12
    //   #205	-> 41
    //   #206	-> 43
    //   #207	-> 43
    //   #210	-> 64
    //   #208	-> 76
    //   #212	-> 85
    //   #213	-> 90
    //   #215	-> 96
    //   #216	-> 100
    //   #218	-> 137
    //   #203	-> 189
    //   #222	-> 223
    //   #223	-> 226
    //   #225	-> 238
    //   #226	-> 264
    //   #231	-> 266
    //   #232	-> 266
    //   #231	-> 269
    //   #228	-> 273
    //   #229	-> 275
    //   #234	-> 327
    //   #235	-> 331
    //   #237	-> 336
    //   #238	-> 336
    //   #239	-> 351
    //   #240	-> 356
    //   #242	-> 369
    //   #245	-> 372
    //   #246	-> 377
    //   #248	-> 383
    //   #249	-> 401
    //   #250	-> 401
    //   #251	-> 411
    //   #255	-> 423
    //   #231	-> 429
    //   #232	-> 429
    //   #197	-> 431
    // Exception table:
    //   from	to	target	type
    //   12	38	431	finally
    //   43	61	431	finally
    //   64	73	431	finally
    //   76	85	431	finally
    //   90	96	431	finally
    //   100	134	431	finally
    //   137	186	431	finally
    //   189	223	431	finally
    //   226	238	431	finally
    //   241	256	273	java/lang/Exception
    //   241	256	269	finally
    //   278	321	269	finally
    //   336	351	431	finally
    //   356	366	431	finally
    //   383	401	431	finally
    //   401	411	431	finally
    //   411	423	431	finally
    //   429	431	431	finally
  }
  
  protected boolean hasZipDrawables() {
    boolean bool2, bool1 = false;
    boolean bool = false;
    MultiZipFile multiZipFile = this.mZipFile;
    if (multiZipFile == null)
      return false; 
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/content/res/OplusThemeZipFile}.Landroid/content/res/OplusThemeZipFile$MultiZipFile;}, name=null} */
    try {
      Enumeration<? extends ZipEntry> enumeration = this.mZipFile.entries();
      while (true) {
        bool2 = bool;
        if (enumeration.hasMoreElements()) {
          ZipEntry zipEntry = enumeration.nextElement();
          if (zipEntry != null && !zipEntry.isDirectory() && zipEntry.getName() != null) {
            bool2 = zipEntry.getName().contains(RESOURCES_PATHS[0]);
            if (bool2) {
              bool2 = true;
              break;
            } 
          } 
          continue;
        } 
        break;
      } 
    } catch (IllegalArgumentException illegalArgumentException) {
      bool2 = bool1;
      if (DEBUG_THEME) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Exception when hasZipDrawables, msg = ");
        stringBuilder.append(illegalArgumentException.toString());
        Log.e("OplusThemeZipFile", stringBuilder.toString());
        bool2 = bool1;
      } 
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/content/res/OplusThemeZipFile}.Landroid/content/res/OplusThemeZipFile$MultiZipFile;}, name=null} */
    return bool2;
  }
  
  protected void clear() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_0
    //   4: getfield mZipFile : Landroid/content/res/OplusThemeZipFile$MultiZipFile;
    //   7: invokevirtual clean : (Ljava/util/zip/ZipFile;)V
    //   10: getstatic android/content/res/OplusThemeZipFile.mEntryCache : Ljava/util/concurrent/ConcurrentHashMap;
    //   13: invokevirtual clear : ()V
    //   16: aload_0
    //   17: iconst_0
    //   18: putfield mHasInit : Z
    //   21: aload_0
    //   22: monitorexit
    //   23: return
    //   24: astore_1
    //   25: aload_0
    //   26: monitorexit
    //   27: aload_1
    //   28: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #283	-> 2
    //   #284	-> 10
    //   #285	-> 16
    //   #286	-> 21
    //   #282	-> 24
    // Exception table:
    //   from	to	target	type
    //   2	10	24	finally
    //   10	16	24	finally
    //   16	21	24	finally
  }
  
  private ThemeFileInfo getInputStreamInner(int paramInt, String paramString, MultiZipFile paramMultiZipFile, boolean paramBoolean) {
    ThemeFileInfo themeFileInfo1 = getZipInputStream(paramString, paramMultiZipFile, paramBoolean);
    ThemeFileInfo themeFileInfo2 = themeFileInfo1;
    if (themeFileInfo1 == null) {
      themeFileInfo2 = themeFileInfo1;
      if (paramMultiZipFile != null) {
        String str = RESOURCES_PATHS[paramInt];
        paramInt = paramString.lastIndexOf("/");
        themeFileInfo2 = themeFileInfo1;
        if (paramInt > 0) {
          String str1 = paramString.substring(paramInt);
          paramInt = 0;
          while (true) {
            themeFileInfo2 = themeFileInfo1;
            if (paramInt < sDensities.length) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(str);
              int i = sDensities[paramInt];
              stringBuilder.append(OplusDisplayUtils.getDensitySuffix(i));
              stringBuilder.append(str1);
              String str2 = stringBuilder.toString();
              if (!paramString.equalsIgnoreCase(str2)) {
                themeFileInfo1 = getZipInputStream(str2, paramMultiZipFile, paramBoolean);
                if (themeFileInfo1 != null) {
                  themeFileInfo2 = themeFileInfo1;
                  if (sDensities[paramInt] > 1) {
                    themeFileInfo1.mDensity = sDensities[paramInt];
                    themeFileInfo2 = themeFileInfo1;
                  } 
                  break;
                } 
              } 
              paramInt++;
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    return themeFileInfo2;
  }
  
  private ThemeFileInfo getZipInputStream(String paramString, MultiZipFile paramMultiZipFile, boolean paramBoolean) {
    ZipEntry zipEntry1 = null, zipEntry2 = null;
    if (paramMultiZipFile == null)
      return null; 
    ZipEntry zipEntry3 = mEntryCache.get(paramString);
    ZipEntry zipEntry4 = zipEntry3;
    if (zipEntry3 == null) {
      zipEntry4 = zipEntry3;
      if (isZipFileValid(paramBoolean))
        zipEntry4 = this.mZipFile.getEntry(paramString); 
    } 
    zipEntry3 = zipEntry1;
    if (zipEntry4 != null)
      try {
        if (!isZipFileValid(paramBoolean))
          return null; 
        InputStream inputStream = paramMultiZipFile.getInputStream(zipEntry4);
        zipEntry3 = zipEntry2;
        if (inputStream != null) {
          mEntryCache.put(paramString, zipEntry4);
          ThemeFileInfo themeFileInfo = new ThemeFileInfo();
          this(inputStream, zipEntry4.getSize());
        } 
      } catch (Exception exception) {
        zipEntry3 = zipEntry1;
        if (DEBUG_THEME) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("OplusThemeZipFile Exception e: ");
          stringBuilder.append(exception);
          stringBuilder.append(" path= ");
          stringBuilder.append(paramString);
          stringBuilder.append(" file= ");
          stringBuilder.append(paramMultiZipFile);
          Log.e("OplusThemeZipFile", stringBuilder.toString());
          zipEntry3 = zipEntry1;
        } 
      }  
    return (ThemeFileInfo)zipEntry3;
  }
  
  private void openZipFile() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/io/File
    //   5: astore_1
    //   6: aload_1
    //   7: aload_0
    //   8: getfield mPath : Ljava/lang/String;
    //   11: invokespecial <init> : (Ljava/lang/String;)V
    //   14: aload_1
    //   15: invokevirtual exists : ()Z
    //   18: ifeq -> 128
    //   21: aload_1
    //   22: invokevirtual isDirectory : ()Z
    //   25: ifne -> 128
    //   28: aload_1
    //   29: invokevirtual lastModified : ()J
    //   32: lstore_2
    //   33: aload_0
    //   34: lload_2
    //   35: putfield mLastModifyTime : J
    //   38: lload_2
    //   39: ldc2_w -1
    //   42: lcmp
    //   43: ifeq -> 133
    //   46: new android/content/res/OplusThemeZipFile$MultiZipFile
    //   49: astore #4
    //   51: aload #4
    //   53: aload_0
    //   54: aload_1
    //   55: invokespecial <init> : (Landroid/content/res/OplusThemeZipFile;Ljava/io/File;)V
    //   58: aload_0
    //   59: aload #4
    //   61: putfield mZipFile : Landroid/content/res/OplusThemeZipFile$MultiZipFile;
    //   64: goto -> 133
    //   67: astore #4
    //   69: getstatic android/content/res/OplusThemeZipFile.DEBUG_THEME : Z
    //   72: ifeq -> 125
    //   75: new java/lang/StringBuilder
    //   78: astore_1
    //   79: aload_1
    //   80: invokespecial <init> : ()V
    //   83: aload_1
    //   84: ldc_w 'openZipFile Exception e: '
    //   87: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   90: pop
    //   91: aload_1
    //   92: aload #4
    //   94: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   97: pop
    //   98: aload_1
    //   99: ldc_w ' path= '
    //   102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   105: pop
    //   106: aload_1
    //   107: aload_0
    //   108: getfield mPath : Ljava/lang/String;
    //   111: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: ldc 'OplusThemeZipFile'
    //   117: aload_1
    //   118: invokevirtual toString : ()Ljava/lang/String;
    //   121: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   124: pop
    //   125: goto -> 133
    //   128: aload_0
    //   129: aconst_null
    //   130: putfield mZipFile : Landroid/content/res/OplusThemeZipFile$MultiZipFile;
    //   133: aload_0
    //   134: monitorexit
    //   135: return
    //   136: astore #4
    //   138: aload_0
    //   139: monitorexit
    //   140: aload #4
    //   142: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #348	-> 2
    //   #349	-> 14
    //   #350	-> 28
    //   #351	-> 38
    //   #353	-> 46
    //   #356	-> 64
    //   #354	-> 67
    //   #355	-> 69
    //   #356	-> 125
    //   #359	-> 128
    //   #361	-> 133
    //   #347	-> 136
    // Exception table:
    //   from	to	target	type
    //   2	14	136	finally
    //   14	28	136	finally
    //   28	38	136	finally
    //   46	64	67	java/lang/Exception
    //   46	64	136	finally
    //   69	125	136	finally
    //   128	133	136	finally
  }
  
  private void loadThemeValues(int paramInt, MultiZipFile paramMultiZipFile) {
    int i = sDensities.length;
    String str = OplusDisplayUtils.getDensitySuffix(sDensities[i - 1]);
    ThemeFileInfo themeFileInfo = getZipInputStream(String.format(ASSETS_THEME_VALUE_FILES[paramInt], new Object[] { str }), paramMultiZipFile, false);
    parseXmlStream(paramInt, themeFileInfo);
  }
  
  class MultiZipFile extends ZipFile {
    final OplusThemeZipFile this$0;
    
    public MultiZipFile(File param1File) throws IOException {
      super(param1File);
    }
  }
  
  private void checkPathForUser() {
    if ("com.android.systemui".equals(this.mPackageName) && "/data/theme/".equals(this.mMetaData.themePath)) {
      int i;
      OplusExtraConfiguration oplusExtraConfiguration = this.mBaseResources.getSystemConfiguration().getOplusExtraConfiguration();
      if (oplusExtraConfiguration == null) {
        i = 0;
      } else {
        i = oplusExtraConfiguration.mUserId;
      } 
      if (i <= 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mMetaData.themePath);
        stringBuilder.append(this.mPackageName);
        this.mPath = stringBuilder.toString();
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mMetaData.themePath);
        stringBuilder.append(i);
        stringBuilder.append("/");
        stringBuilder.append(this.mPackageName);
        this.mPath = stringBuilder.toString();
      } 
    } 
  }
}
