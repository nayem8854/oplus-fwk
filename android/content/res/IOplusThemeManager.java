package android.content.res;

import android.common.IOplusCommonFeature;
import android.common.OplusFeatureList;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.os.UserHandle;
import android.util.TypedValue;
import java.io.InputStream;

public interface IOplusThemeManager extends IOplusCommonFeature {
  public static final IOplusThemeManager DEFAULT = (IOplusThemeManager)new Object();
  
  default OplusFeatureList.OplusIndex index() {
    return OplusFeatureList.OplusIndex.IOplusThemeManager;
  }
  
  default IOplusThemeManager getDefault() {
    return DEFAULT;
  }
  
  default void getValue(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, TypedValue paramTypedValue, boolean paramBoolean) {}
  
  default void init(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {}
  
  default void init(OplusBaseResources paramOplusBaseResources, String paramString) {}
  
  default InputStream openRawResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, TypedValue paramTypedValue) {
    return null;
  }
  
  default int updateExConfiguration(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, Configuration paramConfiguration) {
    return 0;
  }
  
  default void checkUpdate(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, boolean paramBoolean) {}
  
  default TypedArray replaceTypedArray(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, TypedArray paramTypedArray) {
    return paramTypedArray;
  }
  
  default void getExValue(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, TypedValue paramTypedValue, boolean paramBoolean) {}
  
  default Drawable loadOverlayDrawable(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, Resources paramResources, TypedValue paramTypedValue, int paramInt) {
    return null;
  }
  
  default CharSequence getText(OplusBaseResources paramOplusBaseResources, int paramInt, CharSequence paramCharSequence) {
    return paramCharSequence;
  }
  
  default Drawable getDefaultActivityIcon(Context paramContext, OplusBaseResources paramOplusBaseResources) {
    return paramContext.getDrawable(17301651);
  }
  
  default Drawable loadPackageItemIcon(PackageItemInfo paramPackageItemInfo, PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    return paramPackageManager.loadItemIcon(paramPackageItemInfo, paramApplicationInfo);
  }
  
  default Drawable loadResolveIcon(ResolveInfo paramResolveInfo, PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    return paramPackageManager.getDrawable(paramString, paramInt, paramApplicationInfo);
  }
  
  default Drawable getBadgedIcon(LauncherActivityInfo paramLauncherActivityInfo, int paramInt, PackageManager paramPackageManager, UserHandle paramUserHandle, ActivityInfo paramActivityInfo) {
    Drawable drawable = paramLauncherActivityInfo.getIcon(paramInt);
    return paramPackageManager.getUserBadgedIcon(drawable, paramUserHandle);
  }
  
  default boolean isOplusIcons() {
    return false;
  }
  
  default boolean supportUxIcon(PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, String paramString) {
    return false;
  }
  
  default Drawable getDrawableFromUxIcon(PackageManager paramPackageManager, String paramString1, String paramString2, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    return paramPackageManager.getDrawable(paramString1, paramInt, paramApplicationInfo);
  }
  
  default Drawable getDrawableForApp(Drawable paramDrawable, boolean paramBoolean) {
    return paramDrawable;
  }
  
  default Drawable getDrawableForApp(Resources paramResources, OplusBaseResources paramOplusBaseResources, Drawable paramDrawable, boolean paramBoolean) {
    return paramDrawable;
  }
  
  default long getIconConfigFromSettings(ContentResolver paramContentResolver, Context paramContext, int paramInt) {
    return -1L;
  }
  
  default void setIconConfigToSettings(ContentResolver paramContentResolver, long paramLong, int paramInt) {}
  
  default void updateExtraConfigForUxIcon(int paramInt) {}
  
  default boolean supportUxOnline(PackageManager paramPackageManager, String paramString) {
    return false;
  }
  
  default void onCleanupUserForTheme(int paramInt) {}
  
  default Drawable loadOverlayResolverDrawable(PackageManager paramPackageManager, String paramString1, int paramInt, ApplicationInfo paramApplicationInfo, String paramString2) {
    return paramPackageManager.getDrawable(paramString1, paramInt, paramApplicationInfo);
  }
  
  default boolean shouldReportExtraConfig(int paramInt1, int paramInt2) {
    return false;
  }
  
  default void setDarkFilterToDrawable(Drawable paramDrawable) {}
}
