package android.content.res;

public abstract class ComplexColor {
  private int mChangingConfigurations;
  
  public boolean isStateful() {
    return false;
  }
  
  final void setBaseChangingConfigurations(int paramInt) {
    this.mChangingConfigurations = paramInt;
  }
  
  public int getChangingConfigurations() {
    return this.mChangingConfigurations;
  }
  
  public abstract boolean canApplyTheme();
  
  public abstract ConstantState<ComplexColor> getConstantState();
  
  public abstract int getDefaultColor();
  
  public abstract ComplexColor obtainForTheme(Resources.Theme paramTheme);
}
