package android.content.res;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Annotation;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannedString;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.BulletSpan;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.LineHeightSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.TypefaceSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.SparseArray;
import java.io.Closeable;

public final class StringBlock implements Closeable {
  private static final String TAG = "AssetManager";
  
  private static final boolean localLOGV = false;
  
  private final long mNative;
  
  private boolean mOpen = true;
  
  private final boolean mOwnsNative;
  
  private SparseArray<CharSequence> mSparseStrings;
  
  private CharSequence[] mStrings;
  
  StyleIDs mStyleIDs = null;
  
  private final boolean mUseSparse;
  
  public StringBlock(byte[] paramArrayOfbyte, boolean paramBoolean) {
    this.mNative = nativeCreate(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    this.mUseSparse = paramBoolean;
    this.mOwnsNative = true;
  }
  
  public StringBlock(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean) {
    this.mNative = nativeCreate(paramArrayOfbyte, paramInt1, paramInt2);
    this.mUseSparse = paramBoolean;
    this.mOwnsNative = true;
  }
  
  public CharSequence get(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mStrings : [Ljava/lang/CharSequence;
    //   6: ifnull -> 27
    //   9: aload_0
    //   10: getfield mStrings : [Ljava/lang/CharSequence;
    //   13: iload_1
    //   14: aaload
    //   15: astore_2
    //   16: aload_2
    //   17: ifnull -> 24
    //   20: aload_0
    //   21: monitorexit
    //   22: aload_2
    //   23: areturn
    //   24: goto -> 103
    //   27: aload_0
    //   28: getfield mSparseStrings : Landroid/util/SparseArray;
    //   31: ifnull -> 57
    //   34: aload_0
    //   35: getfield mSparseStrings : Landroid/util/SparseArray;
    //   38: iload_1
    //   39: invokevirtual get : (I)Ljava/lang/Object;
    //   42: checkcast java/lang/CharSequence
    //   45: astore_2
    //   46: aload_2
    //   47: ifnull -> 54
    //   50: aload_0
    //   51: monitorexit
    //   52: aload_2
    //   53: areturn
    //   54: goto -> 103
    //   57: aload_0
    //   58: getfield mNative : J
    //   61: invokestatic nativeGetSize : (J)I
    //   64: istore_3
    //   65: aload_0
    //   66: getfield mUseSparse : Z
    //   69: ifeq -> 95
    //   72: iload_3
    //   73: sipush #250
    //   76: if_icmple -> 95
    //   79: new android/util/SparseArray
    //   82: astore_2
    //   83: aload_2
    //   84: invokespecial <init> : ()V
    //   87: aload_0
    //   88: aload_2
    //   89: putfield mSparseStrings : Landroid/util/SparseArray;
    //   92: goto -> 103
    //   95: aload_0
    //   96: iload_3
    //   97: anewarray java/lang/CharSequence
    //   100: putfield mStrings : [Ljava/lang/CharSequence;
    //   103: aload_0
    //   104: getfield mNative : J
    //   107: iload_1
    //   108: invokestatic nativeGetString : (JI)Ljava/lang/String;
    //   111: astore #4
    //   113: aload #4
    //   115: astore_2
    //   116: aload_0
    //   117: getfield mNative : J
    //   120: iload_1
    //   121: invokestatic nativeGetStyle : (JI)[I
    //   124: astore #5
    //   126: aload #5
    //   128: ifnull -> 590
    //   131: aload_0
    //   132: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   135: ifnonnull -> 151
    //   138: new android/content/res/StringBlock$StyleIDs
    //   141: astore_2
    //   142: aload_2
    //   143: invokespecial <init> : ()V
    //   146: aload_0
    //   147: aload_2
    //   148: putfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   151: iconst_0
    //   152: istore_3
    //   153: iload_3
    //   154: aload #5
    //   156: arraylength
    //   157: if_icmpge -> 577
    //   160: aload #5
    //   162: iload_3
    //   163: iaload
    //   164: istore #6
    //   166: iload #6
    //   168: aload_0
    //   169: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   172: invokestatic access$000 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   175: if_icmpeq -> 571
    //   178: iload #6
    //   180: aload_0
    //   181: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   184: invokestatic access$100 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   187: if_icmpeq -> 571
    //   190: aload_0
    //   191: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   194: astore_2
    //   195: iload #6
    //   197: aload_2
    //   198: invokestatic access$200 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   201: if_icmpeq -> 571
    //   204: iload #6
    //   206: aload_0
    //   207: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   210: invokestatic access$300 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   213: if_icmpeq -> 571
    //   216: aload_0
    //   217: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   220: astore_2
    //   221: iload #6
    //   223: aload_2
    //   224: invokestatic access$400 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   227: if_icmpeq -> 571
    //   230: iload #6
    //   232: aload_0
    //   233: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   236: invokestatic access$500 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   239: if_icmpeq -> 571
    //   242: aload_0
    //   243: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   246: astore_2
    //   247: iload #6
    //   249: aload_2
    //   250: invokestatic access$600 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   253: if_icmpeq -> 571
    //   256: iload #6
    //   258: aload_0
    //   259: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   262: invokestatic access$700 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   265: if_icmpeq -> 571
    //   268: aload_0
    //   269: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   272: astore_2
    //   273: iload #6
    //   275: aload_2
    //   276: invokestatic access$800 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   279: if_icmpeq -> 571
    //   282: iload #6
    //   284: aload_0
    //   285: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   288: invokestatic access$900 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   291: if_icmpeq -> 571
    //   294: aload_0
    //   295: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   298: astore_2
    //   299: iload #6
    //   301: aload_2
    //   302: invokestatic access$1000 : (Landroid/content/res/StringBlock$StyleIDs;)I
    //   305: if_icmpne -> 311
    //   308: goto -> 571
    //   311: aload_0
    //   312: getfield mNative : J
    //   315: iload #6
    //   317: invokestatic nativeGetString : (JI)Ljava/lang/String;
    //   320: astore_2
    //   321: aload_2
    //   322: ldc_w 'b'
    //   325: invokevirtual equals : (Ljava/lang/Object;)Z
    //   328: ifeq -> 344
    //   331: aload_0
    //   332: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   335: iload #6
    //   337: invokestatic access$002 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   340: pop
    //   341: goto -> 571
    //   344: aload_2
    //   345: ldc_w 'i'
    //   348: invokevirtual equals : (Ljava/lang/Object;)Z
    //   351: ifeq -> 367
    //   354: aload_0
    //   355: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   358: iload #6
    //   360: invokestatic access$102 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   363: pop
    //   364: goto -> 571
    //   367: aload_2
    //   368: ldc_w 'u'
    //   371: invokevirtual equals : (Ljava/lang/Object;)Z
    //   374: ifeq -> 390
    //   377: aload_0
    //   378: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   381: iload #6
    //   383: invokestatic access$202 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   386: pop
    //   387: goto -> 571
    //   390: aload_2
    //   391: ldc_w 'tt'
    //   394: invokevirtual equals : (Ljava/lang/Object;)Z
    //   397: ifeq -> 413
    //   400: aload_0
    //   401: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   404: iload #6
    //   406: invokestatic access$302 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   409: pop
    //   410: goto -> 571
    //   413: aload_2
    //   414: ldc_w 'big'
    //   417: invokevirtual equals : (Ljava/lang/Object;)Z
    //   420: ifeq -> 436
    //   423: aload_0
    //   424: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   427: iload #6
    //   429: invokestatic access$402 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   432: pop
    //   433: goto -> 571
    //   436: aload_2
    //   437: ldc_w 'small'
    //   440: invokevirtual equals : (Ljava/lang/Object;)Z
    //   443: ifeq -> 459
    //   446: aload_0
    //   447: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   450: iload #6
    //   452: invokestatic access$502 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   455: pop
    //   456: goto -> 571
    //   459: aload_2
    //   460: ldc_w 'sup'
    //   463: invokevirtual equals : (Ljava/lang/Object;)Z
    //   466: ifeq -> 482
    //   469: aload_0
    //   470: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   473: iload #6
    //   475: invokestatic access$702 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   478: pop
    //   479: goto -> 571
    //   482: aload_2
    //   483: ldc_w 'sub'
    //   486: invokevirtual equals : (Ljava/lang/Object;)Z
    //   489: ifeq -> 505
    //   492: aload_0
    //   493: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   496: iload #6
    //   498: invokestatic access$602 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   501: pop
    //   502: goto -> 571
    //   505: aload_2
    //   506: ldc_w 'strike'
    //   509: invokevirtual equals : (Ljava/lang/Object;)Z
    //   512: ifeq -> 528
    //   515: aload_0
    //   516: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   519: iload #6
    //   521: invokestatic access$802 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   524: pop
    //   525: goto -> 571
    //   528: aload_2
    //   529: ldc_w 'li'
    //   532: invokevirtual equals : (Ljava/lang/Object;)Z
    //   535: ifeq -> 551
    //   538: aload_0
    //   539: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   542: iload #6
    //   544: invokestatic access$902 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   547: pop
    //   548: goto -> 571
    //   551: aload_2
    //   552: ldc_w 'marquee'
    //   555: invokevirtual equals : (Ljava/lang/Object;)Z
    //   558: ifeq -> 571
    //   561: aload_0
    //   562: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   565: iload #6
    //   567: invokestatic access$1002 : (Landroid/content/res/StringBlock$StyleIDs;I)I
    //   570: pop
    //   571: iinc #3, 3
    //   574: goto -> 153
    //   577: aload_0
    //   578: aload #4
    //   580: aload #5
    //   582: aload_0
    //   583: getfield mStyleIDs : Landroid/content/res/StringBlock$StyleIDs;
    //   586: invokespecial applyStyles : (Ljava/lang/String;[ILandroid/content/res/StringBlock$StyleIDs;)Ljava/lang/CharSequence;
    //   589: astore_2
    //   590: aload_0
    //   591: getfield mStrings : [Ljava/lang/CharSequence;
    //   594: ifnull -> 607
    //   597: aload_0
    //   598: getfield mStrings : [Ljava/lang/CharSequence;
    //   601: iload_1
    //   602: aload_2
    //   603: aastore
    //   604: goto -> 616
    //   607: aload_0
    //   608: getfield mSparseStrings : Landroid/util/SparseArray;
    //   611: iload_1
    //   612: aload_2
    //   613: invokevirtual put : (ILjava/lang/Object;)V
    //   616: aload_0
    //   617: monitorexit
    //   618: aload_2
    //   619: areturn
    //   620: astore_2
    //   621: aload_0
    //   622: monitorexit
    //   623: aload_2
    //   624: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #91	-> 0
    //   #92	-> 2
    //   #93	-> 9
    //   #94	-> 16
    //   #95	-> 20
    //   #97	-> 24
    //   #98	-> 34
    //   #99	-> 46
    //   #100	-> 50
    //   #102	-> 54
    //   #103	-> 57
    //   #104	-> 65
    //   #105	-> 79
    //   #107	-> 95
    //   #110	-> 103
    //   #111	-> 113
    //   #112	-> 116
    //   #115	-> 126
    //   #116	-> 131
    //   #117	-> 138
    //   #122	-> 151
    //   #123	-> 160
    //   #125	-> 166
    //   #126	-> 195
    //   #127	-> 221
    //   #128	-> 247
    //   #129	-> 273
    //   #130	-> 299
    //   #132	-> 308
    //   #135	-> 311
    //   #137	-> 321
    //   #138	-> 331
    //   #139	-> 344
    //   #140	-> 354
    //   #141	-> 367
    //   #142	-> 377
    //   #143	-> 390
    //   #144	-> 400
    //   #145	-> 413
    //   #146	-> 423
    //   #147	-> 436
    //   #148	-> 446
    //   #149	-> 459
    //   #150	-> 469
    //   #151	-> 482
    //   #152	-> 492
    //   #153	-> 505
    //   #154	-> 515
    //   #155	-> 528
    //   #156	-> 538
    //   #157	-> 551
    //   #158	-> 561
    //   #122	-> 571
    //   #162	-> 577
    //   #164	-> 590
    //   #165	-> 607
    //   #166	-> 616
    //   #167	-> 620
    // Exception table:
    //   from	to	target	type
    //   2	9	620	finally
    //   9	16	620	finally
    //   20	22	620	finally
    //   27	34	620	finally
    //   34	46	620	finally
    //   50	52	620	finally
    //   57	65	620	finally
    //   65	72	620	finally
    //   79	92	620	finally
    //   95	103	620	finally
    //   103	113	620	finally
    //   116	126	620	finally
    //   131	138	620	finally
    //   138	151	620	finally
    //   153	160	620	finally
    //   166	195	620	finally
    //   195	221	620	finally
    //   221	247	620	finally
    //   247	273	620	finally
    //   273	299	620	finally
    //   299	308	620	finally
    //   311	321	620	finally
    //   321	331	620	finally
    //   331	341	620	finally
    //   344	354	620	finally
    //   354	364	620	finally
    //   367	377	620	finally
    //   377	387	620	finally
    //   390	400	620	finally
    //   400	410	620	finally
    //   413	423	620	finally
    //   423	433	620	finally
    //   436	446	620	finally
    //   446	456	620	finally
    //   459	469	620	finally
    //   469	479	620	finally
    //   482	492	620	finally
    //   492	502	620	finally
    //   505	515	620	finally
    //   515	525	620	finally
    //   528	538	620	finally
    //   538	548	620	finally
    //   551	561	620	finally
    //   561	571	620	finally
    //   577	590	620	finally
    //   590	604	620	finally
    //   607	616	620	finally
    //   616	618	620	finally
    //   621	623	620	finally
  }
  
  protected void finalize() throws Throwable {
    try {
      super.finalize();
      return;
    } finally {
      close();
    } 
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mOpen : Z
    //   6: ifeq -> 28
    //   9: aload_0
    //   10: iconst_0
    //   11: putfield mOpen : Z
    //   14: aload_0
    //   15: getfield mOwnsNative : Z
    //   18: ifeq -> 28
    //   21: aload_0
    //   22: getfield mNative : J
    //   25: invokestatic nativeDestroy : (J)V
    //   28: aload_0
    //   29: monitorexit
    //   30: return
    //   31: astore_1
    //   32: aload_0
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #181	-> 0
    //   #182	-> 2
    //   #183	-> 9
    //   #185	-> 14
    //   #186	-> 21
    //   #189	-> 28
    //   #190	-> 30
    //   #189	-> 31
    // Exception table:
    //   from	to	target	type
    //   2	9	31	finally
    //   9	14	31	finally
    //   14	21	31	finally
    //   21	28	31	finally
    //   28	30	31	finally
    //   32	34	31	finally
  }
  
  static final class StyleIDs {
    private int boldId = -1;
    
    private int italicId = -1;
    
    private int underlineId = -1;
    
    private int ttId = -1;
    
    private int bigId = -1;
    
    private int smallId = -1;
    
    private int subId = -1;
    
    private int supId = -1;
    
    private int strikeId = -1;
    
    private int listItemId = -1;
    
    private int marqueeId = -1;
  }
  
  private CharSequence applyStyles(String paramString, int[] paramArrayOfint, StyleIDs paramStyleIDs) {
    if (paramArrayOfint.length == 0)
      return paramString; 
    SpannableString spannableString = new SpannableString(paramString);
    byte b = 0;
    while (b < paramArrayOfint.length) {
      int i = paramArrayOfint[b];
      if (i == paramStyleIDs.boldId) {
        spannableString.setSpan(new StyleSpan(1), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.italicId) {
        spannableString.setSpan(new StyleSpan(2), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.underlineId) {
        spannableString.setSpan(new UnderlineSpan(), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.ttId) {
        spannableString.setSpan(new TypefaceSpan("monospace"), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.bigId) {
        spannableString.setSpan(new RelativeSizeSpan(1.25F), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.smallId) {
        spannableString.setSpan(new RelativeSizeSpan(0.8F), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.subId) {
        spannableString.setSpan(new SubscriptSpan(), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.supId) {
        spannableString.setSpan(new SuperscriptSpan(), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.strikeId) {
        spannableString.setSpan(new StrikethroughSpan(), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
      } else if (i == paramStyleIDs.listItemId) {
        addParagraphSpan((Spannable)spannableString, new BulletSpan(10), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1);
      } else if (i == paramStyleIDs.marqueeId) {
        spannableString.setSpan(TextUtils.TruncateAt.MARQUEE, paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 18);
      } else {
        String str = nativeGetString(this.mNative, i);
        if (str.startsWith("font;")) {
          String str1 = subtag(str, ";height=");
          if (str1 != null) {
            i = Integer.parseInt(str1);
            addParagraphSpan((Spannable)spannableString, new Height(i), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1);
          } 
          str1 = subtag(str, ";size=");
          if (str1 != null) {
            i = Integer.parseInt(str1);
            spannableString.setSpan(new AbsoluteSizeSpan(i, true), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
          } 
          str1 = subtag(str, ";fgcolor=");
          if (str1 != null)
            spannableString.setSpan(getColor(str1, true), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33); 
          str1 = subtag(str, ";color=");
          if (str1 != null)
            spannableString.setSpan(getColor(str1, true), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33); 
          str1 = subtag(str, ";bgcolor=");
          if (str1 != null)
            spannableString.setSpan(getColor(str1, false), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33); 
          str = subtag(str, ";face=");
          if (str != null)
            spannableString.setSpan(new TypefaceSpan(str), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33); 
        } else if (str.startsWith("a;")) {
          str = subtag(str, ";href=");
          if (str != null)
            spannableString.setSpan(new URLSpan(str), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33); 
        } else if (str.startsWith("annotation;")) {
          int j = str.length();
          int k;
          for (k = str.indexOf(';'); k < j; k = i) {
            int m = str.indexOf('=', k);
            if (m < 0)
              break; 
            int n = str.indexOf(';', m);
            i = n;
            if (n < 0)
              i = j; 
            String str2 = str.substring(k + 1, m);
            String str1 = str.substring(m + 1, i);
            spannableString.setSpan(new Annotation(str2, str1), paramArrayOfint[b + 1], paramArrayOfint[b + 2] + 1, 33);
          } 
        } 
      } 
      b += 3;
    } 
    return (CharSequence)new SpannedString((CharSequence)spannableString);
  }
  
  private static CharacterStyle getColor(String paramString, boolean paramBoolean) {
    int i = -16777216;
    int j = i;
    if (!TextUtils.isEmpty(paramString)) {
      ColorStateList colorStateList;
      if (paramString.startsWith("@")) {
        Resources resources = Resources.getSystem();
        paramString = paramString.substring(1);
        int k = resources.getIdentifier(paramString, "color", "android");
        j = i;
        if (k != 0) {
          colorStateList = resources.getColorStateList(k, null);
          if (paramBoolean)
            return (CharacterStyle)new TextAppearanceSpan(null, 0, 0, colorStateList, null); 
          j = colorStateList.getDefaultColor();
        } 
      } else {
        try {
          j = Color.parseColor((String)colorStateList);
        } catch (IllegalArgumentException illegalArgumentException) {
          j = -16777216;
        } 
      } 
    } 
    if (paramBoolean)
      return (CharacterStyle)new ForegroundColorSpan(j); 
    return (CharacterStyle)new BackgroundColorSpan(j);
  }
  
  private static void addParagraphSpan(Spannable paramSpannable, Object paramObject, int paramInt1, int paramInt2) {
    int i = paramSpannable.length();
    int j = paramInt1;
    if (paramInt1 != 0) {
      j = paramInt1;
      if (paramInt1 != i) {
        j = paramInt1;
        if (paramSpannable.charAt(paramInt1 - 1) != '\n') {
          j = paramInt1;
          while (true) {
            j = paramInt1 = j - 1;
            if (paramInt1 > 0) {
              j = paramInt1;
              if (paramSpannable.charAt(paramInt1 - 1) == '\n') {
                j = paramInt1;
                break;
              } 
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    paramInt1 = paramInt2;
    if (paramInt2 != 0) {
      paramInt1 = paramInt2;
      if (paramInt2 != i) {
        paramInt1 = paramInt2;
        if (paramSpannable.charAt(paramInt2 - 1) != '\n') {
          paramInt1 = paramInt2;
          while (true) {
            paramInt1 = paramInt2 = paramInt1 + 1;
            if (paramInt2 < i) {
              paramInt1 = paramInt2;
              if (paramSpannable.charAt(paramInt2 - 1) == '\n') {
                paramInt1 = paramInt2;
                break;
              } 
              continue;
            } 
            break;
          } 
        } 
      } 
    } 
    paramSpannable.setSpan(paramObject, j, paramInt1, 51);
  }
  
  private static String subtag(String paramString1, String paramString2) {
    int i = paramString1.indexOf(paramString2);
    if (i < 0)
      return null; 
    int j = i + paramString2.length();
    i = paramString1.indexOf(';', j);
    if (i < 0)
      return paramString1.substring(j); 
    return paramString1.substring(j, i);
  }
  
  class Height implements LineHeightSpan.WithDensity {
    private static float sProportion = 0.0F;
    
    private int mSize;
    
    public Height(StringBlock this$0) {
      this.mSize = this$0;
    }
    
    public void chooseHeight(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3, int param1Int4, Paint.FontMetricsInt param1FontMetricsInt) {
      chooseHeight(param1CharSequence, param1Int1, param1Int2, param1Int3, param1Int4, param1FontMetricsInt, null);
    }
    
    public void chooseHeight(CharSequence param1CharSequence, int param1Int1, int param1Int2, int param1Int3, int param1Int4, Paint.FontMetricsInt param1FontMetricsInt, TextPaint param1TextPaint) {
      param1Int2 = this.mSize;
      param1Int1 = param1Int2;
      if (param1TextPaint != null)
        param1Int1 = (int)(param1Int2 * param1TextPaint.density); 
      if (param1FontMetricsInt.bottom - param1FontMetricsInt.top < param1Int1) {
        param1FontMetricsInt.top = param1FontMetricsInt.bottom - param1Int1;
        param1FontMetricsInt.ascent -= param1Int1;
      } else {
        if (sProportion == 0.0F) {
          Paint paint = new Paint();
          paint.setTextSize(100.0F);
          Rect rect = new Rect();
          paint.getTextBounds("ABCDEFG", 0, 7, rect);
          sProportion = rect.top / paint.ascent();
        } 
        param1Int2 = (int)Math.ceil((-param1FontMetricsInt.top * sProportion));
        if (param1Int1 - param1FontMetricsInt.descent >= param1Int2) {
          param1FontMetricsInt.top = param1FontMetricsInt.bottom - param1Int1;
          param1FontMetricsInt.ascent = param1FontMetricsInt.descent - param1Int1;
        } else if (param1Int1 >= param1Int2) {
          param1FontMetricsInt.ascent = param1Int2 = -param1Int2;
          param1FontMetricsInt.top = param1Int2;
          param1FontMetricsInt.descent = param1Int1 = param1FontMetricsInt.top + param1Int1;
          param1FontMetricsInt.bottom = param1Int1;
        } else {
          param1FontMetricsInt.ascent = param1Int1 = -param1Int1;
          param1FontMetricsInt.top = param1Int1;
          param1FontMetricsInt.descent = 0;
          param1FontMetricsInt.bottom = 0;
        } 
      } 
    }
  }
  
  public StringBlock(long paramLong, boolean paramBoolean) {
    this.mNative = paramLong;
    this.mUseSparse = paramBoolean;
    this.mOwnsNative = false;
  }
  
  private static native long nativeCreate(byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  private static native void nativeDestroy(long paramLong);
  
  private static native int nativeGetSize(long paramLong);
  
  private static native String nativeGetString(long paramLong, int paramInt);
  
  private static native int[] nativeGetStyle(long paramLong, int paramInt);
}
