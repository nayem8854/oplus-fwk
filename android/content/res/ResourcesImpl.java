package android.content.res;

import android.animation.Animator;
import android.animation.StateListAnimator;
import android.common.OplusFeatureCache;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.ImageDecoder;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ColorStateListDrawable;
import android.graphics.drawable.Drawable;
import android.icu.text.PluralRules;
import android.os.LocaleList;
import android.os.Process;
import android.os.SystemClock;
import android.os.SystemProperties;
import android.os.Trace;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.TypedValue;
import android.util.Xml;
import android.view.DisplayAdjustments;
import com.android.internal.util.GrowingArrayUtils;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Locale;
import java.util.function.Supplier;
import org.xmlpull.v1.XmlPullParserException;

public class ResourcesImpl extends OplusBaseResourcesImpl {
  public static final boolean TRACE_FOR_DETAILED_PRELOAD = SystemProperties.getBoolean("debug.trace_resource_preload", false);
  
  private static final Object sSync = new Object();
  
  static {
    sPreloadedColorDrawables = new LongSparseArray();
    sPreloadedComplexColors = new LongSparseArray();
    LongSparseArray[] arrayOfLongSparseArray = new LongSparseArray[2];
    arrayOfLongSparseArray[0] = new LongSparseArray();
    sPreloadedDrawables[1] = new LongSparseArray();
  }
  
  private final Object mAccessLock = new Object();
  
  private final Configuration mTmpConfig = new Configuration();
  
  private final DrawableCache mDrawableCache = new DrawableCache();
  
  private final DrawableCache mColorDrawableCache = new DrawableCache();
  
  private final ConfigurationBoundResourceCache<ComplexColor> mComplexColorCache = new ConfigurationBoundResourceCache<>();
  
  private final ConfigurationBoundResourceCache<Animator> mAnimatorCache = new ConfigurationBoundResourceCache<>();
  
  private final ConfigurationBoundResourceCache<StateListAnimator> mStateListAnimatorCache = new ConfigurationBoundResourceCache<>();
  
  private static final boolean DEBUG_CONFIG = false;
  
  private static final boolean DEBUG_LOAD = false;
  
  private static final int ID_OTHER = 16777220;
  
  static final String TAG = "Resources";
  
  static final String TAG_PRELOAD = "Resources.preload";
  
  private static final boolean TRACE_FOR_MISS_PRELOAD = false;
  
  private static final boolean TRACE_FOR_PRELOAD = false;
  
  private static final int XML_BLOCK_CACHE_SIZE = 4;
  
  private static int sPreloadTracingNumLoadedDrawables;
  
  private static boolean sPreloaded;
  
  private static final LongSparseArray<Drawable.ConstantState> sPreloadedColorDrawables;
  
  private static final LongSparseArray<ConstantState<ComplexColor>> sPreloadedComplexColors;
  
  private static final LongSparseArray<Drawable.ConstantState>[] sPreloadedDrawables;
  
  final AssetManager mAssets;
  
  private final int[] mCachedXmlBlockCookies;
  
  private final String[] mCachedXmlBlockFiles;
  
  private final XmlBlock[] mCachedXmlBlocks;
  
  private final Configuration mConfiguration;
  
  private final DisplayAdjustments mDisplayAdjustments;
  
  private int mLastCachedXmlBlockIndex;
  
  private final ThreadLocal<LookupStack> mLookupStack;
  
  private final DisplayMetrics mMetrics;
  
  private PluralRules mPluralRule;
  
  private long mPreloadTracingPreloadStartTime;
  
  private long mPreloadTracingStartBitmapCount;
  
  private long mPreloadTracingStartBitmapSize;
  
  private boolean mPreloading;
  
  public ResourcesImpl(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration, DisplayAdjustments paramDisplayAdjustments) {
    -$.Lambda.ResourcesImpl.h3PTRX185BeQl8SVC2_w9arp5Og h3PTRX185BeQl8SVC2_w9arp5Og = _$$Lambda$ResourcesImpl$h3PTRX185BeQl8SVC2_w9arp5Og.INSTANCE;
    this.mLookupStack = ThreadLocal.withInitial((Supplier<? extends LookupStack>)h3PTRX185BeQl8SVC2_w9arp5Og);
    this.mLastCachedXmlBlockIndex = -1;
    this.mCachedXmlBlockCookies = new int[4];
    this.mCachedXmlBlockFiles = new String[4];
    this.mCachedXmlBlocks = new XmlBlock[4];
    this.mMetrics = new DisplayMetrics();
    this.mConfiguration = new Configuration();
    this.mAssets = paramAssetManager;
    this.mMetrics.setToDefaults();
    this.mDisplayAdjustments = paramDisplayAdjustments;
    this.mConfiguration.setToDefaults();
    updateConfiguration(paramConfiguration, paramDisplayMetrics, paramDisplayAdjustments.getCompatibilityInfo());
    ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).init(this, (String)null);
  }
  
  public DisplayAdjustments getDisplayAdjustments() {
    return this.mDisplayAdjustments;
  }
  
  public AssetManager getAssets() {
    return this.mAssets;
  }
  
  DisplayMetrics getDisplayMetrics() {
    return this.mMetrics;
  }
  
  Configuration getConfiguration() {
    return this.mConfiguration;
  }
  
  Configuration[] getSizeConfigurations() {
    return this.mAssets.getSizeConfigurations();
  }
  
  CompatibilityInfo getCompatibilityInfo() {
    return this.mDisplayAdjustments.getCompatibilityInfo();
  }
  
  private PluralRules getPluralRule() {
    synchronized (sSync) {
      if (this.mPluralRule == null)
        this.mPluralRule = PluralRules.forLocale(this.mConfiguration.getLocales().get(0)); 
      return this.mPluralRule;
    } 
  }
  
  void getValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean) throws Resources.NotFoundException {
    boolean bool = this.mAssets.getResourceValue(paramInt, 0, paramTypedValue, paramBoolean);
    if (bool) {
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).getValue(this, paramInt, paramTypedValue, paramBoolean);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  void getValueForDensity(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean) throws Resources.NotFoundException {
    boolean bool = this.mAssets.getResourceValue(paramInt1, paramInt2, paramTypedValue, paramBoolean);
    if (bool) {
      ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).getValue(this, paramInt1, paramTypedValue, paramBoolean);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt1));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  void getValue(String paramString, TypedValue paramTypedValue, boolean paramBoolean) throws Resources.NotFoundException {
    int i = getIdentifier(paramString, "string", null);
    if (i != 0) {
      getValue(i, paramTypedValue, paramBoolean);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("String resource name ");
    stringBuilder.append(paramString);
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  int getIdentifier(String paramString1, String paramString2, String paramString3) {
    if (paramString1 != null)
      try {
        return Integer.parseInt(paramString1);
      } catch (Exception exception) {
        return this.mAssets.getResourceIdentifier(paramString1, paramString2, paramString3);
      }  
    throw new NullPointerException("name is null");
  }
  
  String getResourceName(int paramInt) throws Resources.NotFoundException {
    String str = this.mAssets.getResourceName(paramInt);
    if (str != null)
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to find resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  String getResourcePackageName(int paramInt) throws Resources.NotFoundException {
    String str = this.mAssets.getResourcePackageName(paramInt);
    if (str != null)
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to find resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  String getResourceTypeName(int paramInt) throws Resources.NotFoundException {
    String str = this.mAssets.getResourceTypeName(paramInt);
    if (str != null)
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to find resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  String getResourceEntryName(int paramInt) throws Resources.NotFoundException {
    String str = this.mAssets.getResourceEntryName(paramInt);
    if (str != null)
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unable to find resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  String getLastResourceResolution() throws Resources.NotFoundException {
    String str = this.mAssets.getLastResourceResolution();
    if (str != null)
      return str; 
    throw new Resources.NotFoundException("Associated AssetManager hasn't resolved a resource");
  }
  
  CharSequence getQuantityText(int paramInt1, int paramInt2) throws Resources.NotFoundException {
    PluralRules pluralRules = getPluralRule();
    AssetManager assetManager = this.mAssets;
    double d = paramInt2;
    int i = attrForQuantityCode(pluralRules.select(d));
    CharSequence charSequence = assetManager.getResourceBagText(paramInt1, i);
    if (charSequence != null)
      return charSequence; 
    charSequence = this.mAssets.getResourceBagText(paramInt1, 16777220);
    if (charSequence != null)
      return charSequence; 
    charSequence = new StringBuilder();
    charSequence.append("Plural resource ID #0x");
    charSequence.append(Integer.toHexString(paramInt1));
    charSequence.append(" quantity=");
    charSequence.append(paramInt2);
    charSequence.append(" item=");
    d = paramInt2;
    charSequence.append(pluralRules.select(d));
    throw new Resources.NotFoundException(charSequence.toString());
  }
  
  private static int attrForQuantityCode(String paramString) {
    byte b;
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 3735208:
        if (paramString.equals("zero")) {
          b = 0;
          break;
        } 
      case 3343967:
        if (paramString.equals("many")) {
          b = 4;
          break;
        } 
      case 115276:
        if (paramString.equals("two")) {
          b = 2;
          break;
        } 
      case 110182:
        if (paramString.equals("one")) {
          b = 1;
          break;
        } 
      case 101272:
        if (paramString.equals("few")) {
          b = 3;
          break;
        } 
    } 
    if (b != 0) {
      if (b != 1) {
        if (b != 2) {
          if (b != 3) {
            if (b != 4)
              return 16777220; 
            return 16777225;
          } 
          return 16777224;
        } 
        return 16777223;
      } 
      return 16777222;
    } 
    return 16777221;
  }
  
  AssetFileDescriptor openRawResourceFd(int paramInt, TypedValue paramTypedValue) throws Resources.NotFoundException {
    getValue(paramInt, paramTypedValue, true);
    try {
      return this.mAssets.openNonAssetFd(paramTypedValue.assetCookie, paramTypedValue.string.toString());
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("File ");
      stringBuilder.append(paramTypedValue.string.toString());
      stringBuilder.append(" from resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      throw new Resources.NotFoundException(stringBuilder.toString(), exception);
    } 
  }
  
  InputStream openRawResource(int paramInt, TypedValue paramTypedValue) throws Resources.NotFoundException {
    getValue(paramInt, paramTypedValue, true);
    InputStream inputStream = ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).openRawResource(this, paramInt, paramTypedValue);
    if (inputStream != null)
      return inputStream; 
    try {
      return this.mAssets.openNonAsset(paramTypedValue.assetCookie, paramTypedValue.string.toString(), 2);
    } catch (Exception exception) {
      String str;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("File ");
      if (paramTypedValue.string == null) {
        str = "(null)";
      } else {
        str = ((TypedValue)str).string.toString();
      } 
      stringBuilder.append(str);
      stringBuilder.append(" from resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      Resources.NotFoundException notFoundException = new Resources.NotFoundException(stringBuilder.toString());
      notFoundException.initCause(exception);
      throw notFoundException;
    } 
  }
  
  ConfigurationBoundResourceCache<Animator> getAnimatorCache() {
    return this.mAnimatorCache;
  }
  
  ConfigurationBoundResourceCache<StateListAnimator> getStateListAnimatorCache() {
    return this.mStateListAnimatorCache;
  }
  
  public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo) {
    // Byte code:
    //   0: ldc2_w 8192
    //   3: ldc_w 'ResourcesImpl#updateConfiguration'
    //   6: invokestatic traceBegin : (JLjava/lang/String;)V
    //   9: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   12: iconst_0
    //   13: anewarray java/lang/Object
    //   16: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   19: checkcast android/content/res/IOplusThemeManager
    //   22: aload_0
    //   23: aload_1
    //   24: invokeinterface updateExConfiguration : (Landroid/content/res/OplusBaseResourcesImpl;Landroid/content/res/Configuration;)I
    //   29: istore #4
    //   31: aload_0
    //   32: getfield mAccessLock : Ljava/lang/Object;
    //   35: astore #5
    //   37: aload #5
    //   39: monitorenter
    //   40: aload_3
    //   41: ifnull -> 59
    //   44: aload_0
    //   45: getfield mDisplayAdjustments : Landroid/view/DisplayAdjustments;
    //   48: aload_3
    //   49: invokevirtual setCompatibilityInfo : (Landroid/content/res/CompatibilityInfo;)V
    //   52: goto -> 59
    //   55: astore_1
    //   56: goto -> 759
    //   59: aload_2
    //   60: ifnull -> 71
    //   63: aload_0
    //   64: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   67: aload_2
    //   68: invokevirtual setTo : (Landroid/util/DisplayMetrics;)V
    //   71: aload_0
    //   72: getfield mDisplayAdjustments : Landroid/view/DisplayAdjustments;
    //   75: invokevirtual getCompatibilityInfo : ()Landroid/content/res/CompatibilityInfo;
    //   78: aload_0
    //   79: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   82: invokevirtual applyToDisplayMetrics : (Landroid/util/DisplayMetrics;)V
    //   85: aload_0
    //   86: aload_1
    //   87: invokevirtual calcConfigChanges : (Landroid/content/res/Configuration;)I
    //   90: istore #6
    //   92: aload_0
    //   93: getfield mConfiguration : Landroid/content/res/Configuration;
    //   96: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   99: astore_2
    //   100: aload_2
    //   101: invokevirtual isEmpty : ()Z
    //   104: istore #7
    //   106: iload #7
    //   108: ifeq -> 123
    //   111: invokestatic getDefault : ()Landroid/os/LocaleList;
    //   114: astore_2
    //   115: aload_0
    //   116: getfield mConfiguration : Landroid/content/res/Configuration;
    //   119: aload_2
    //   120: invokevirtual setLocales : (Landroid/os/LocaleList;)V
    //   123: iload #6
    //   125: iconst_4
    //   126: iand
    //   127: ifeq -> 219
    //   130: aload_2
    //   131: invokevirtual size : ()I
    //   134: iconst_1
    //   135: if_icmple -> 219
    //   138: aload_0
    //   139: getfield mAssets : Landroid/content/res/AssetManager;
    //   142: invokevirtual getNonSystemLocales : ()[Ljava/lang/String;
    //   145: astore_3
    //   146: aload_3
    //   147: astore_1
    //   148: aload_3
    //   149: invokestatic isPseudoLocalesOnly : ([Ljava/lang/String;)Z
    //   152: ifeq -> 174
    //   155: aload_0
    //   156: getfield mAssets : Landroid/content/res/AssetManager;
    //   159: invokevirtual getLocales : ()[Ljava/lang/String;
    //   162: astore_3
    //   163: aload_3
    //   164: astore_1
    //   165: aload_3
    //   166: invokestatic isPseudoLocalesOnly : ([Ljava/lang/String;)Z
    //   169: ifeq -> 174
    //   172: aconst_null
    //   173: astore_1
    //   174: aload_1
    //   175: ifnull -> 219
    //   178: aload_2
    //   179: aload_1
    //   180: invokevirtual getFirstMatchWithEnglishSupported : ([Ljava/lang/String;)Ljava/util/Locale;
    //   183: astore_1
    //   184: aload_1
    //   185: ifnull -> 219
    //   188: aload_1
    //   189: aload_2
    //   190: iconst_0
    //   191: invokevirtual get : (I)Ljava/util/Locale;
    //   194: if_acmpeq -> 219
    //   197: aload_0
    //   198: getfield mConfiguration : Landroid/content/res/Configuration;
    //   201: astore #8
    //   203: new android/os/LocaleList
    //   206: astore_3
    //   207: aload_3
    //   208: aload_1
    //   209: aload_2
    //   210: invokespecial <init> : (Ljava/util/Locale;Landroid/os/LocaleList;)V
    //   213: aload #8
    //   215: aload_3
    //   216: invokevirtual setLocales : (Landroid/os/LocaleList;)V
    //   219: aload_0
    //   220: getfield mConfiguration : Landroid/content/res/Configuration;
    //   223: getfield densityDpi : I
    //   226: istore #9
    //   228: iload #9
    //   230: ifeq -> 266
    //   233: aload_0
    //   234: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   237: aload_0
    //   238: getfield mConfiguration : Landroid/content/res/Configuration;
    //   241: getfield densityDpi : I
    //   244: putfield densityDpi : I
    //   247: aload_0
    //   248: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   251: aload_0
    //   252: getfield mConfiguration : Landroid/content/res/Configuration;
    //   255: getfield densityDpi : I
    //   258: i2f
    //   259: ldc_w 0.00625
    //   262: fmul
    //   263: putfield density : F
    //   266: aload_0
    //   267: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   270: astore_1
    //   271: aload_0
    //   272: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   275: getfield density : F
    //   278: fstore #10
    //   280: aload_0
    //   281: getfield mConfiguration : Landroid/content/res/Configuration;
    //   284: getfield fontScale : F
    //   287: fstore #11
    //   289: fload #11
    //   291: fconst_0
    //   292: fcmpl
    //   293: ifeq -> 308
    //   296: aload_0
    //   297: getfield mConfiguration : Landroid/content/res/Configuration;
    //   300: getfield fontScale : F
    //   303: fstore #11
    //   305: goto -> 311
    //   308: fconst_1
    //   309: fstore #11
    //   311: aload_1
    //   312: fload #10
    //   314: fload #11
    //   316: fmul
    //   317: putfield scaledDensity : F
    //   320: aload_0
    //   321: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   324: getfield widthPixels : I
    //   327: istore #9
    //   329: aload_0
    //   330: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   333: getfield heightPixels : I
    //   336: istore #12
    //   338: iload #9
    //   340: iload #12
    //   342: if_icmplt -> 366
    //   345: aload_0
    //   346: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   349: getfield widthPixels : I
    //   352: istore #12
    //   354: aload_0
    //   355: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   358: getfield heightPixels : I
    //   361: istore #9
    //   363: goto -> 384
    //   366: aload_0
    //   367: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   370: getfield heightPixels : I
    //   373: istore #12
    //   375: aload_0
    //   376: getfield mMetrics : Landroid/util/DisplayMetrics;
    //   379: getfield widthPixels : I
    //   382: istore #9
    //   384: aload_0
    //   385: getfield mConfiguration : Landroid/content/res/Configuration;
    //   388: getfield keyboardHidden : I
    //   391: istore #13
    //   393: iload #13
    //   395: iconst_1
    //   396: if_icmpne -> 420
    //   399: aload_0
    //   400: getfield mConfiguration : Landroid/content/res/Configuration;
    //   403: getfield hardKeyboardHidden : I
    //   406: istore #13
    //   408: iload #13
    //   410: iconst_2
    //   411: if_icmpne -> 420
    //   414: iconst_3
    //   415: istore #13
    //   417: goto -> 429
    //   420: aload_0
    //   421: getfield mConfiguration : Landroid/content/res/Configuration;
    //   424: getfield keyboardHidden : I
    //   427: istore #13
    //   429: aload_0
    //   430: getfield mAssets : Landroid/content/res/AssetManager;
    //   433: astore_1
    //   434: aload_0
    //   435: getfield mConfiguration : Landroid/content/res/Configuration;
    //   438: getfield mcc : I
    //   441: istore #14
    //   443: aload_0
    //   444: getfield mConfiguration : Landroid/content/res/Configuration;
    //   447: getfield mnc : I
    //   450: istore #15
    //   452: aload_0
    //   453: getfield mConfiguration : Landroid/content/res/Configuration;
    //   456: astore_2
    //   457: aload_2
    //   458: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   461: iconst_0
    //   462: invokevirtual get : (I)Ljava/util/Locale;
    //   465: invokevirtual toLanguageTag : ()Ljava/lang/String;
    //   468: invokestatic adjustLanguageTag : (Ljava/lang/String;)Ljava/lang/String;
    //   471: astore_2
    //   472: aload_0
    //   473: getfield mConfiguration : Landroid/content/res/Configuration;
    //   476: getfield orientation : I
    //   479: istore #16
    //   481: aload_0
    //   482: getfield mConfiguration : Landroid/content/res/Configuration;
    //   485: getfield touchscreen : I
    //   488: istore #17
    //   490: aload_0
    //   491: getfield mConfiguration : Landroid/content/res/Configuration;
    //   494: getfield densityDpi : I
    //   497: istore #18
    //   499: aload_0
    //   500: getfield mConfiguration : Landroid/content/res/Configuration;
    //   503: getfield keyboard : I
    //   506: istore #19
    //   508: aload_0
    //   509: getfield mConfiguration : Landroid/content/res/Configuration;
    //   512: getfield navigation : I
    //   515: istore #20
    //   517: aload_0
    //   518: getfield mConfiguration : Landroid/content/res/Configuration;
    //   521: getfield smallestScreenWidthDp : I
    //   524: istore #21
    //   526: aload_0
    //   527: getfield mConfiguration : Landroid/content/res/Configuration;
    //   530: getfield screenWidthDp : I
    //   533: istore #22
    //   535: aload_0
    //   536: getfield mConfiguration : Landroid/content/res/Configuration;
    //   539: getfield screenHeightDp : I
    //   542: istore #23
    //   544: aload_0
    //   545: getfield mConfiguration : Landroid/content/res/Configuration;
    //   548: getfield screenLayout : I
    //   551: istore #24
    //   553: aload_0
    //   554: getfield mConfiguration : Landroid/content/res/Configuration;
    //   557: getfield uiMode : I
    //   560: istore #25
    //   562: aload_0
    //   563: getfield mConfiguration : Landroid/content/res/Configuration;
    //   566: getfield colorMode : I
    //   569: istore #26
    //   571: getstatic android/os/Build$VERSION.RESOURCES_SDK_INT : I
    //   574: istore #27
    //   576: aload_1
    //   577: iload #14
    //   579: iload #15
    //   581: aload_2
    //   582: iload #16
    //   584: iload #17
    //   586: iload #18
    //   588: iload #19
    //   590: iload #13
    //   592: iload #20
    //   594: iload #12
    //   596: iload #9
    //   598: iload #21
    //   600: iload #22
    //   602: iload #23
    //   604: iload #24
    //   606: iload #25
    //   608: iload #26
    //   610: iload #27
    //   612: invokevirtual setConfiguration : (IILjava/lang/String;IIIIIIIIIIIIIII)V
    //   615: iload #6
    //   617: iload #4
    //   619: ior
    //   620: istore #9
    //   622: aload_0
    //   623: getfield mDrawableCache : Landroid/content/res/DrawableCache;
    //   626: iload #9
    //   628: invokevirtual onConfigurationChange : (I)V
    //   631: aload_0
    //   632: getfield mColorDrawableCache : Landroid/content/res/DrawableCache;
    //   635: iload #9
    //   637: invokevirtual onConfigurationChange : (I)V
    //   640: aload_0
    //   641: getfield mComplexColorCache : Landroid/content/res/ConfigurationBoundResourceCache;
    //   644: iload #9
    //   646: invokevirtual onConfigurationChange : (I)V
    //   649: aload_0
    //   650: getfield mAnimatorCache : Landroid/content/res/ConfigurationBoundResourceCache;
    //   653: iload #9
    //   655: invokevirtual onConfigurationChange : (I)V
    //   658: aload_0
    //   659: getfield mStateListAnimatorCache : Landroid/content/res/ConfigurationBoundResourceCache;
    //   662: iload #9
    //   664: invokevirtual onConfigurationChange : (I)V
    //   667: aload_0
    //   668: invokevirtual flushLayoutCache : ()V
    //   671: aload #5
    //   673: monitorexit
    //   674: getstatic android/content/res/ResourcesImpl.sSync : Ljava/lang/Object;
    //   677: astore_2
    //   678: aload_2
    //   679: monitorenter
    //   680: aload_0
    //   681: getfield mPluralRule : Landroid/icu/text/PluralRules;
    //   684: astore_1
    //   685: aload_1
    //   686: ifnull -> 714
    //   689: aload_0
    //   690: aload_0
    //   691: getfield mConfiguration : Landroid/content/res/Configuration;
    //   694: invokevirtual getLocales : ()Landroid/os/LocaleList;
    //   697: iconst_0
    //   698: invokevirtual get : (I)Ljava/util/Locale;
    //   701: invokestatic forLocale : (Ljava/util/Locale;)Landroid/icu/text/PluralRules;
    //   704: putfield mPluralRule : Landroid/icu/text/PluralRules;
    //   707: goto -> 714
    //   710: astore_1
    //   711: goto -> 746
    //   714: aload_2
    //   715: monitorexit
    //   716: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   719: iconst_0
    //   720: anewarray java/lang/Object
    //   723: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   726: checkcast android/content/res/IOplusThemeManager
    //   729: aload_0
    //   730: iload #4
    //   732: iconst_0
    //   733: invokeinterface checkUpdate : (Landroid/content/res/OplusBaseResourcesImpl;IZ)V
    //   738: ldc2_w 8192
    //   741: invokestatic traceEnd : (J)V
    //   744: return
    //   745: astore_1
    //   746: aload_2
    //   747: monitorexit
    //   748: aload_1
    //   749: athrow
    //   750: astore_1
    //   751: goto -> 746
    //   754: astore_1
    //   755: goto -> 759
    //   758: astore_1
    //   759: aload #5
    //   761: monitorexit
    //   762: aload_1
    //   763: athrow
    //   764: astore_1
    //   765: goto -> 759
    //   768: astore_1
    //   769: ldc2_w 8192
    //   772: invokestatic traceEnd : (J)V
    //   775: aload_1
    //   776: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #403	-> 0
    //   #407	-> 9
    //   #409	-> 31
    //   #417	-> 40
    //   #418	-> 44
    //   #532	-> 55
    //   #420	-> 59
    //   #421	-> 63
    //   #432	-> 71
    //   #434	-> 85
    //   #437	-> 92
    //   #438	-> 100
    //   #439	-> 111
    //   #440	-> 115
    //   #443	-> 123
    //   #444	-> 130
    //   #447	-> 138
    //   #448	-> 146
    //   #450	-> 155
    //   #451	-> 163
    //   #452	-> 172
    //   #456	-> 174
    //   #457	-> 178
    //   #459	-> 184
    //   #460	-> 197
    //   #466	-> 219
    //   #467	-> 233
    //   #468	-> 247
    //   #473	-> 266
    //   #474	-> 280
    //   #477	-> 320
    //   #478	-> 345
    //   #479	-> 354
    //   #482	-> 366
    //   #484	-> 375
    //   #488	-> 384
    //   #491	-> 414
    //   #493	-> 420
    //   #496	-> 429
    //   #497	-> 457
    //   #496	-> 576
    //   #522	-> 615
    //   #523	-> 622
    //   #524	-> 631
    //   #525	-> 640
    //   #526	-> 649
    //   #527	-> 658
    //   #531	-> 667
    //   #532	-> 671
    //   #533	-> 674
    //   #534	-> 680
    //   #535	-> 689
    //   #537	-> 710
    //   #540	-> 716
    //   #543	-> 738
    //   #544	-> 744
    //   #545	-> 744
    //   #537	-> 745
    //   #532	-> 754
    //   #543	-> 768
    //   #544	-> 775
    // Exception table:
    //   from	to	target	type
    //   9	31	768	finally
    //   31	40	768	finally
    //   44	52	55	finally
    //   63	71	55	finally
    //   71	85	758	finally
    //   85	92	758	finally
    //   92	100	758	finally
    //   100	106	758	finally
    //   111	115	55	finally
    //   115	123	55	finally
    //   130	138	55	finally
    //   138	146	55	finally
    //   148	155	55	finally
    //   155	163	55	finally
    //   165	172	55	finally
    //   178	184	55	finally
    //   188	197	55	finally
    //   197	219	55	finally
    //   219	228	758	finally
    //   233	247	55	finally
    //   247	266	55	finally
    //   266	280	758	finally
    //   280	289	758	finally
    //   296	305	55	finally
    //   311	320	758	finally
    //   320	338	758	finally
    //   345	354	55	finally
    //   354	363	55	finally
    //   366	375	758	finally
    //   375	384	758	finally
    //   384	393	758	finally
    //   399	408	55	finally
    //   420	429	758	finally
    //   429	457	758	finally
    //   457	535	758	finally
    //   535	576	754	finally
    //   576	615	754	finally
    //   622	631	754	finally
    //   631	640	754	finally
    //   640	649	754	finally
    //   649	658	754	finally
    //   658	667	754	finally
    //   667	671	754	finally
    //   671	674	754	finally
    //   674	680	768	finally
    //   680	685	745	finally
    //   689	707	710	finally
    //   714	716	745	finally
    //   716	738	768	finally
    //   746	748	750	finally
    //   748	750	768	finally
    //   759	762	764	finally
    //   762	764	768	finally
  }
  
  public int calcConfigChanges(Configuration paramConfiguration) {
    if (paramConfiguration == null)
      return -1; 
    this.mTmpConfig.setTo(paramConfiguration);
    int i = paramConfiguration.densityDpi;
    int j = i;
    if (i == 0)
      j = this.mMetrics.noncompatDensityDpi; 
    this.mDisplayAdjustments.getCompatibilityInfo().applyToConfiguration(j, this.mTmpConfig);
    if (this.mTmpConfig.getLocales().isEmpty())
      this.mTmpConfig.setLocales(LocaleList.getDefault()); 
    return this.mConfiguration.updateFrom(this.mTmpConfig);
  }
  
  private static String adjustLanguageTag(String paramString) {
    String str;
    int i = paramString.indexOf('-');
    if (i == -1) {
      String str1 = "";
      str = paramString;
      paramString = str1;
    } else {
      str = paramString.substring(0, i);
      paramString = paramString.substring(i);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(Locale.adjustLanguageCode(str));
    stringBuilder.append(paramString);
    return stringBuilder.toString();
  }
  
  public void flushLayoutCache() {
    synchronized (this.mCachedXmlBlocks) {
      Arrays.fill(this.mCachedXmlBlockCookies, 0);
      Arrays.fill((Object[])this.mCachedXmlBlockFiles, (Object)null);
      XmlBlock[] arrayOfXmlBlock = this.mCachedXmlBlocks;
      for (byte b = 0; b < 4; b++) {
        XmlBlock xmlBlock = arrayOfXmlBlock[b];
        if (xmlBlock != null)
          xmlBlock.close(); 
      } 
      Arrays.fill((Object[])arrayOfXmlBlock, (Object)null);
      return;
    } 
  }
  
  public void clearAllCaches() {
    synchronized (this.mAccessLock) {
      this.mDrawableCache.clear();
      this.mColorDrawableCache.clear();
      this.mComplexColorCache.clear();
      this.mAnimatorCache.clear();
      this.mStateListAnimatorCache.clear();
      flushLayoutCache();
      return;
    } 
  }
  
  Drawable loadDrawable(Resources paramResources, TypedValue paramTypedValue, int paramInt1, int paramInt2, Resources.Theme paramTheme) throws Resources.NotFoundException {
    boolean bool;
    if (paramInt2 == 0 || paramTypedValue.density == this.mMetrics.densityDpi) {
      bool = true;
    } else {
      bool = false;
    } 
    if (paramInt2 > 0 && paramTypedValue.density > 0 && paramTypedValue.density != 65535)
      if (paramTypedValue.density == paramInt2) {
        paramTypedValue.density = this.mMetrics.densityDpi;
      } else {
        paramTypedValue.density = paramTypedValue.density * this.mMetrics.densityDpi / paramInt2;
      }  
    try {
      Drawable drawable1;
      DrawableCache drawableCache;
      long l;
      boolean bool1;
      Drawable.ConstantState constantState;
      Drawable drawable2;
      boolean bool2;
      if (paramTypedValue.type >= 28 && paramTypedValue.type <= 31) {
        drawableCache = this.mColorDrawableCache;
        l = paramTypedValue.data;
        bool1 = true;
      } else {
        drawableCache = this.mDrawableCache;
        long l1 = paramTypedValue.assetCookie;
        l = paramTypedValue.data;
        bool1 = false;
        l = l1 << 32L | l;
      } 
      if (!this.mPreloading && bool) {
        drawable2 = drawableCache.getInstance(l, paramResources, paramTheme);
        if (drawable2 != null) {
          drawable2.setChangingConfigurations(paramTypedValue.changingConfigurations);
          return drawable2;
        } 
      } 
      if (bool1) {
        constantState = (Drawable.ConstantState)sPreloadedColorDrawables.get(l);
      } else {
        constantState = (Drawable.ConstantState)sPreloadedDrawables[this.mConfiguration.getLayoutDirection()].get(l);
      } 
      if (constantState != null) {
        if (TRACE_FOR_DETAILED_PRELOAD)
          if (paramInt1 >>> 24 == 1 && Process.myUid() != 0) {
            String str = getResourceName(paramInt1);
            if (str != null) {
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Hit preloaded FW drawable #");
              stringBuilder.append(Integer.toHexString(paramInt1));
              stringBuilder.append(" ");
              stringBuilder.append(str);
              str = stringBuilder.toString();
              Log.d("Resources.preload", str);
            } 
          }  
        drawable2 = constantState.newDrawable(paramResources);
      } else if (bool1) {
        drawable2 = new ColorDrawable(paramTypedValue.data);
      } else {
        drawable2 = loadDrawableForCookie(paramResources, paramTypedValue, paramInt1, paramInt2);
      } 
      if (drawable2 instanceof android.graphics.drawable.DrawableContainer) {
        paramInt2 = 1;
      } else {
        paramInt2 = 0;
      } 
      if (drawable2 != null && drawable2.canApplyTheme()) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      Drawable drawable3 = drawable2;
      if (bool2) {
        drawable3 = drawable2;
        if (paramTheme != null) {
          drawable3 = drawable2.mutate();
          drawable3.applyTheme(paramTheme);
          drawable3.clearMutated();
        } 
      } 
      if (drawable3 != null) {
        drawable3.setChangingConfigurations(paramTypedValue.changingConfigurations);
        if (bool) {
          try {
            cacheDrawable(paramTypedValue, bool1, drawableCache, paramTheme, bool2, l, drawable3);
            drawable1 = drawable3;
            if (paramInt2 != 0) {
              Drawable.ConstantState constantState1 = drawable3.getConstantState();
              drawable1 = drawable3;
              if (constantState1 != null)
                drawable1 = constantState1.newDrawable(paramResources); 
            } 
          } catch (Exception exception) {}
        } else {
          drawable1 = drawable3;
        } 
      } else {
        drawable1 = drawable3;
      } 
      return drawable1;
    } catch (Exception exception) {
      String str;
      try {
        str = getResourceName(paramInt1);
      } catch (NotFoundException notFoundException) {
        str = "(missing name)";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Drawable ");
      stringBuilder.append(str);
      stringBuilder.append(" with resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt1));
      exception = new Resources.NotFoundException(stringBuilder.toString(), exception);
      exception.setStackTrace(new StackTraceElement[0]);
      throw exception;
    } 
  }
  
  private void cacheDrawable(TypedValue paramTypedValue, boolean paramBoolean1, DrawableCache paramDrawableCache, Resources.Theme paramTheme, boolean paramBoolean2, long paramLong, Drawable paramDrawable) {
    Drawable.ConstantState constantState = paramDrawable.getConstantState();
    if (constantState == null)
      return; 
    if (this.mPreloading) {
      int i = constantState.getChangingConfigurations();
      if (paramBoolean1) {
        if (verifyPreloadConfig(i, 0, paramTypedValue.resourceId, "drawable"))
          sPreloadedColorDrawables.put(paramLong, constantState); 
      } else if (verifyPreloadConfig(i, 8192, paramTypedValue.resourceId, "drawable")) {
        if ((i & 0x2000) == 0) {
          sPreloadedDrawables[0].put(paramLong, constantState);
          sPreloadedDrawables[1].put(paramLong, constantState);
        } else {
          sPreloadedDrawables[this.mConfiguration.getLayoutDirection()].put(paramLong, constantState);
        } 
      } 
    } else {
      synchronized (this.mAccessLock) {
        paramDrawableCache.put(paramLong, paramTheme, constantState, paramBoolean2);
        return;
      } 
    } 
  }
  
  private boolean verifyPreloadConfig(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    if ((0xBFFFEFFF & paramInt1 & (paramInt2 ^ 0xFFFFFFFF)) != 0) {
      String str;
      try {
        str = getResourceName(paramInt3);
      } catch (NotFoundException notFoundException) {
        str = "?";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Preloaded ");
      stringBuilder.append(paramString);
      stringBuilder.append(" resource #0x");
      stringBuilder.append(Integer.toHexString(paramInt3));
      stringBuilder.append(" (");
      stringBuilder.append(str);
      stringBuilder.append(") that varies with configuration!!");
      paramString = stringBuilder.toString();
      Log.w("Resources", paramString);
      return false;
    } 
    return true;
  }
  
  private Drawable decodeImageDrawable(AssetManager.AssetInputStream paramAssetInputStream, Resources paramResources, TypedValue paramTypedValue) {
    ImageDecoder.AssetInputStreamSource assetInputStreamSource = new ImageDecoder.AssetInputStreamSource(paramAssetInputStream, paramResources, paramTypedValue);
    try {
      return ImageDecoder.decodeDrawable(assetInputStreamSource, (ImageDecoder.OnHeaderDecodedListener)_$$Lambda$ResourcesImpl$99dm2ENnzo9b0SIUjUj2Kl3pi90.INSTANCE);
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  private Drawable loadDrawableForCookie(Resources paramResources, TypedValue paramTypedValue, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_2
    //   1: getfield string : Ljava/lang/CharSequence;
    //   4: ifnull -> 604
    //   7: aload_2
    //   8: getfield string : Ljava/lang/CharSequence;
    //   11: invokeinterface toString : ()Ljava/lang/String;
    //   16: astore #5
    //   18: getstatic android/content/res/ResourcesImpl.TRACE_FOR_DETAILED_PRELOAD : Z
    //   21: ifeq -> 47
    //   24: invokestatic nanoTime : ()J
    //   27: lstore #6
    //   29: getstatic android/graphics/Bitmap.sPreloadTracingNumInstantiatedBitmaps : I
    //   32: istore #8
    //   34: getstatic android/graphics/Bitmap.sPreloadTracingTotalBitmapsSize : J
    //   37: lstore #9
    //   39: getstatic android/content/res/ResourcesImpl.sPreloadTracingNumLoadedDrawables : I
    //   42: istore #11
    //   44: goto -> 59
    //   47: lconst_0
    //   48: lstore #6
    //   50: iconst_0
    //   51: istore #8
    //   53: lconst_0
    //   54: lstore #9
    //   56: iconst_0
    //   57: istore #11
    //   59: ldc2_w 8192
    //   62: aload #5
    //   64: invokestatic traceBegin : (JLjava/lang/String;)V
    //   67: aload_0
    //   68: getfield mLookupStack : Ljava/lang/ThreadLocal;
    //   71: invokevirtual get : ()Ljava/lang/Object;
    //   74: checkcast android/content/res/ResourcesImpl$LookupStack
    //   77: astore #12
    //   79: aload #12
    //   81: iload_3
    //   82: invokevirtual contains : (I)Z
    //   85: ifne -> 520
    //   88: aload #12
    //   90: iload_3
    //   91: invokevirtual push : (I)V
    //   94: getstatic android/content/res/IOplusThemeManager.DEFAULT : Landroid/content/res/IOplusThemeManager;
    //   97: iconst_0
    //   98: anewarray java/lang/Object
    //   101: invokestatic getOrCreate : (Landroid/common/IOplusCommonFeature;[Ljava/lang/Object;)Landroid/common/IOplusCommonFeature;
    //   104: checkcast android/content/res/IOplusThemeManager
    //   107: aload_0
    //   108: aload_1
    //   109: aload_2
    //   110: iload_3
    //   111: invokeinterface loadOverlayDrawable : (Landroid/content/res/OplusBaseResourcesImpl;Landroid/content/res/Resources;Landroid/util/TypedValue;I)Landroid/graphics/drawable/Drawable;
    //   116: astore #13
    //   118: aload #13
    //   120: ifnonnull -> 230
    //   123: aload #5
    //   125: ldc_w '.xml'
    //   128: invokevirtual endsWith : (Ljava/lang/String;)Z
    //   131: ifeq -> 191
    //   134: aload_0
    //   135: iload_3
    //   136: invokevirtual getResourceTypeName : (I)Ljava/lang/String;
    //   139: astore #13
    //   141: aload #13
    //   143: ifnull -> 176
    //   146: aload #13
    //   148: ldc_w 'color'
    //   151: invokevirtual equals : (Ljava/lang/Object;)Z
    //   154: istore #14
    //   156: iload #14
    //   158: ifeq -> 176
    //   161: aload_0
    //   162: aload_1
    //   163: aload_2
    //   164: iload_3
    //   165: iload #4
    //   167: aload #5
    //   169: invokespecial loadColorOrXmlDrawable : (Landroid/content/res/Resources;Landroid/util/TypedValue;IILjava/lang/String;)Landroid/graphics/drawable/Drawable;
    //   172: astore_1
    //   173: goto -> 188
    //   176: aload_0
    //   177: aload_1
    //   178: aload_2
    //   179: iload_3
    //   180: iload #4
    //   182: aload #5
    //   184: invokespecial loadXmlDrawable : (Landroid/content/res/Resources;Landroid/util/TypedValue;IILjava/lang/String;)Landroid/graphics/drawable/Drawable;
    //   187: astore_1
    //   188: goto -> 233
    //   191: aload_0
    //   192: getfield mAssets : Landroid/content/res/AssetManager;
    //   195: aload_2
    //   196: getfield assetCookie : I
    //   199: aload #5
    //   201: iconst_2
    //   202: invokevirtual openNonAsset : (ILjava/lang/String;I)Ljava/io/InputStream;
    //   205: astore #13
    //   207: aload #13
    //   209: checkcast android/content/res/AssetManager$AssetInputStream
    //   212: astore #13
    //   214: aload_0
    //   215: aload #13
    //   217: aload_1
    //   218: aload_2
    //   219: invokespecial decodeImageDrawable : (Landroid/content/res/AssetManager$AssetInputStream;Landroid/content/res/Resources;Landroid/util/TypedValue;)Landroid/graphics/drawable/Drawable;
    //   222: astore_1
    //   223: goto -> 233
    //   226: astore_1
    //   227: goto -> 513
    //   230: aload #13
    //   232: astore_1
    //   233: aload #12
    //   235: invokevirtual pop : ()V
    //   238: ldc2_w 8192
    //   241: invokestatic traceEnd : (J)V
    //   244: getstatic android/content/res/ResourcesImpl.TRACE_FOR_DETAILED_PRELOAD : Z
    //   247: ifeq -> 510
    //   250: iload_3
    //   251: bipush #24
    //   253: iushr
    //   254: iconst_1
    //   255: if_icmpne -> 510
    //   258: aload_0
    //   259: iload_3
    //   260: invokevirtual getResourceName : (I)Ljava/lang/String;
    //   263: astore #12
    //   265: aload #12
    //   267: ifnull -> 510
    //   270: invokestatic nanoTime : ()J
    //   273: lstore #15
    //   275: getstatic android/graphics/Bitmap.sPreloadTracingNumInstantiatedBitmaps : I
    //   278: istore #17
    //   280: getstatic android/graphics/Bitmap.sPreloadTracingTotalBitmapsSize : J
    //   283: lstore #18
    //   285: getstatic android/content/res/ResourcesImpl.sPreloadTracingNumLoadedDrawables : I
    //   288: istore #20
    //   290: iconst_1
    //   291: istore #4
    //   293: iload #20
    //   295: iconst_1
    //   296: iadd
    //   297: putstatic android/content/res/ResourcesImpl.sPreloadTracingNumLoadedDrawables : I
    //   300: invokestatic myUid : ()I
    //   303: ifne -> 309
    //   306: goto -> 312
    //   309: iconst_0
    //   310: istore #4
    //   312: new java/lang/StringBuilder
    //   315: dup
    //   316: invokespecial <init> : ()V
    //   319: astore #13
    //   321: iload #4
    //   323: ifeq -> 333
    //   326: ldc_w 'Preloaded FW drawable #'
    //   329: astore_2
    //   330: goto -> 337
    //   333: ldc_w 'Loaded non-preloaded FW drawable #'
    //   336: astore_2
    //   337: aload #13
    //   339: aload_2
    //   340: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   343: pop
    //   344: aload #13
    //   346: iload_3
    //   347: invokestatic toHexString : (I)Ljava/lang/String;
    //   350: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   353: pop
    //   354: aload #13
    //   356: ldc_w ' '
    //   359: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   362: pop
    //   363: aload #13
    //   365: aload #12
    //   367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload #13
    //   373: ldc_w ' '
    //   376: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   379: pop
    //   380: aload #13
    //   382: aload #5
    //   384: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   387: pop
    //   388: aload #13
    //   390: ldc_w ' '
    //   393: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   396: pop
    //   397: aload #13
    //   399: aload_1
    //   400: invokevirtual getClass : ()Ljava/lang/Class;
    //   403: invokevirtual getCanonicalName : ()Ljava/lang/String;
    //   406: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   409: pop
    //   410: aload #13
    //   412: ldc_w ' #nested_drawables= '
    //   415: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   418: pop
    //   419: aload #13
    //   421: iload #20
    //   423: iload #11
    //   425: isub
    //   426: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   429: pop
    //   430: aload #13
    //   432: ldc_w ' #bitmaps= '
    //   435: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   438: pop
    //   439: aload #13
    //   441: iload #17
    //   443: iload #8
    //   445: isub
    //   446: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   449: pop
    //   450: aload #13
    //   452: ldc_w ' total_bitmap_size= '
    //   455: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   458: pop
    //   459: aload #13
    //   461: lload #18
    //   463: lload #9
    //   465: lsub
    //   466: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   469: pop
    //   470: aload #13
    //   472: ldc_w ' in[us] '
    //   475: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   478: pop
    //   479: aload #13
    //   481: lload #15
    //   483: lload #6
    //   485: lsub
    //   486: ldc2_w 1000
    //   489: ldiv
    //   490: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   493: pop
    //   494: aload #13
    //   496: invokevirtual toString : ()Ljava/lang/String;
    //   499: astore_2
    //   500: ldc 'Resources.preload'
    //   502: aload_2
    //   503: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   506: pop
    //   507: goto -> 510
    //   510: aload_1
    //   511: areturn
    //   512: astore_1
    //   513: aload #12
    //   515: invokevirtual pop : ()V
    //   518: aload_1
    //   519: athrow
    //   520: new java/lang/Exception
    //   523: astore_1
    //   524: aload_1
    //   525: ldc_w 'Recursive reference in drawable'
    //   528: invokespecial <init> : (Ljava/lang/String;)V
    //   531: aload_1
    //   532: athrow
    //   533: astore_1
    //   534: goto -> 538
    //   537: astore_1
    //   538: ldc2_w 8192
    //   541: invokestatic traceEnd : (J)V
    //   544: new java/lang/StringBuilder
    //   547: dup
    //   548: invokespecial <init> : ()V
    //   551: astore_2
    //   552: aload_2
    //   553: ldc_w 'File '
    //   556: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   559: pop
    //   560: aload_2
    //   561: aload #5
    //   563: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   566: pop
    //   567: aload_2
    //   568: ldc_w ' from drawable resource ID #0x'
    //   571: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   574: pop
    //   575: aload_2
    //   576: iload_3
    //   577: invokestatic toHexString : (I)Ljava/lang/String;
    //   580: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   583: pop
    //   584: new android/content/res/Resources$NotFoundException
    //   587: dup
    //   588: aload_2
    //   589: invokevirtual toString : ()Ljava/lang/String;
    //   592: invokespecial <init> : (Ljava/lang/String;)V
    //   595: astore_2
    //   596: aload_2
    //   597: aload_1
    //   598: invokevirtual initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   601: pop
    //   602: aload_2
    //   603: athrow
    //   604: new java/lang/StringBuilder
    //   607: dup
    //   608: invokespecial <init> : ()V
    //   611: astore_1
    //   612: aload_1
    //   613: ldc_w 'Resource "'
    //   616: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   619: pop
    //   620: aload_1
    //   621: aload_0
    //   622: iload_3
    //   623: invokevirtual getResourceName : (I)Ljava/lang/String;
    //   626: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   629: pop
    //   630: aload_1
    //   631: ldc_w '" ('
    //   634: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   637: pop
    //   638: aload_1
    //   639: iload_3
    //   640: invokestatic toHexString : (I)Ljava/lang/String;
    //   643: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   646: pop
    //   647: aload_1
    //   648: ldc_w ') is not a Drawable (color or path): '
    //   651: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   654: pop
    //   655: aload_1
    //   656: aload_2
    //   657: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   660: pop
    //   661: new android/content/res/Resources$NotFoundException
    //   664: dup
    //   665: aload_1
    //   666: invokevirtual toString : ()Ljava/lang/String;
    //   669: invokespecial <init> : (Ljava/lang/String;)V
    //   672: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #873	-> 0
    //   #878	-> 7
    //   #892	-> 18
    //   #893	-> 18
    //   #894	-> 18
    //   #895	-> 18
    //   #896	-> 18
    //   #897	-> 24
    //   #898	-> 29
    //   #899	-> 34
    //   #900	-> 39
    //   #896	-> 47
    //   #915	-> 59
    //   #916	-> 67
    //   #919	-> 79
    //   #922	-> 88
    //   #926	-> 94
    //   #927	-> 118
    //   #928	-> 123
    //   #929	-> 134
    //   #930	-> 141
    //   #931	-> 161
    //   #930	-> 176
    //   #933	-> 176
    //   #935	-> 188
    //   #936	-> 191
    //   #938	-> 207
    //   #939	-> 214
    //   #944	-> 226
    //   #927	-> 230
    //   #944	-> 233
    //   #945	-> 238
    //   #952	-> 238
    //   #953	-> 238
    //   #955	-> 244
    //   #956	-> 250
    //   #957	-> 258
    //   #958	-> 265
    //   #959	-> 270
    //   #960	-> 275
    //   #962	-> 280
    //   #964	-> 285
    //   #967	-> 290
    //   #969	-> 300
    //   #971	-> 312
    //   #972	-> 321
    //   #973	-> 333
    //   #974	-> 344
    //   #977	-> 397
    //   #971	-> 500
    //   #958	-> 510
    //   #986	-> 510
    //   #944	-> 512
    //   #945	-> 518
    //   #920	-> 520
    //   #946	-> 533
    //   #947	-> 538
    //   #948	-> 544
    //   #949	-> 575
    //   #950	-> 596
    //   #951	-> 602
    //   #874	-> 604
    //   #875	-> 638
    // Exception table:
    //   from	to	target	type
    //   79	88	537	java/lang/Exception
    //   79	88	537	java/lang/StackOverflowError
    //   88	94	537	java/lang/Exception
    //   88	94	537	java/lang/StackOverflowError
    //   94	118	512	finally
    //   123	134	512	finally
    //   134	141	512	finally
    //   146	156	512	finally
    //   161	173	226	finally
    //   176	188	226	finally
    //   191	207	226	finally
    //   207	214	226	finally
    //   214	223	226	finally
    //   233	238	533	java/lang/Exception
    //   233	238	533	java/lang/StackOverflowError
    //   513	518	533	java/lang/Exception
    //   513	518	533	java/lang/StackOverflowError
    //   518	520	533	java/lang/Exception
    //   518	520	533	java/lang/StackOverflowError
    //   520	533	533	java/lang/Exception
    //   520	533	533	java/lang/StackOverflowError
  }
  
  private Drawable loadColorOrXmlDrawable(Resources paramResources, TypedValue paramTypedValue, int paramInt1, int paramInt2, String paramString) {
    try {
      ColorStateList colorStateList = loadColorStateList(paramResources, paramTypedValue, paramInt1, null);
      return new ColorStateListDrawable(colorStateList);
    } catch (NotFoundException notFoundException) {
      try {
        return loadXmlDrawable(paramResources, paramTypedValue, paramInt1, paramInt2, paramString);
      } catch (Exception exception) {
        throw notFoundException;
      } 
    } 
  }
  
  private Drawable loadXmlDrawable(Resources paramResources, TypedValue paramTypedValue, int paramInt1, int paramInt2, String paramString) throws IOException, XmlPullParserException {
    int i = paramTypedValue.assetCookie;
    XmlResourceParser xmlResourceParser = loadXmlResourceParser(paramString, paramInt1, i, "drawable");
    try {
      return Drawable.createFromXmlForDensity(paramResources, xmlResourceParser, paramInt2, null);
    } finally {
      if (xmlResourceParser != null)
        try {
          xmlResourceParser.close();
        } finally {
          xmlResourceParser = null;
        }  
    } 
  }
  
  public Typeface loadFont(Resources paramResources, TypedValue paramTypedValue, int paramInt) {
    if (paramTypedValue.string != null) {
      String str = paramTypedValue.string.toString();
      if (!str.startsWith("res/"))
        return null; 
      Typeface typeface = Typeface.findFromCache(this.mAssets, str);
      if (typeface != null)
        return typeface; 
      Trace.traceBegin(8192L, str);
      try {
        XmlResourceParser xmlResourceParser;
        if (str.endsWith("xml")) {
          xmlResourceParser = loadXmlResourceParser(str, paramInt, paramTypedValue.assetCookie, "font");
          FontResourcesParser.FamilyResourceEntry familyResourceEntry = FontResourcesParser.parse(xmlResourceParser, paramResources);
          if (familyResourceEntry == null) {
            Trace.traceEnd(8192L);
            return null;
          } 
          Typeface typeface2 = Typeface.createFromResources(familyResourceEntry, this.mAssets, str);
          Trace.traceEnd(8192L);
          return typeface2;
        } 
        Typeface.Builder builder = new Typeface.Builder();
        this(this.mAssets, str, false, ((TypedValue)xmlResourceParser).assetCookie);
        Typeface typeface1 = builder.build();
        Trace.traceEnd(8192L);
        return typeface1;
      } catch (XmlPullParserException xmlPullParserException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to parse xml resource ");
        stringBuilder1.append(str);
        Log.e("Resources", stringBuilder1.toString(), (Throwable)xmlPullParserException);
      } catch (IOException iOException) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("Failed to read xml resource ");
        stringBuilder1.append(str);
        Log.e("Resources", stringBuilder1.toString(), iOException);
      } finally {}
      Trace.traceEnd(8192L);
      return null;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Resource \"");
    stringBuilder.append(getResourceName(paramInt));
    stringBuilder.append("\" (");
    stringBuilder.append(Integer.toHexString(paramInt));
    stringBuilder.append(") is not a Font: ");
    stringBuilder.append(iOException);
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  private ComplexColor loadComplexColorFromName(Resources paramResources, Resources.Theme paramTheme, TypedValue paramTypedValue, int paramInt) {
    long l = paramTypedValue.assetCookie << 32L | paramTypedValue.data;
    ConfigurationBoundResourceCache<ComplexColor> configurationBoundResourceCache = this.mComplexColorCache;
    ComplexColor complexColor1 = configurationBoundResourceCache.getInstance(l, paramResources, paramTheme);
    if (complexColor1 != null)
      return complexColor1; 
    LongSparseArray<ConstantState<ComplexColor>> longSparseArray = sPreloadedComplexColors;
    ConstantState<ComplexColor> constantState = (ConstantState)longSparseArray.get(l);
    if (constantState != null)
      complexColor1 = constantState.newInstance(paramResources, paramTheme); 
    ComplexColor complexColor2 = complexColor1;
    if (complexColor1 == null)
      complexColor2 = loadComplexColorForCookie(paramResources, paramTypedValue, paramInt, paramTheme); 
    if (complexColor2 != null) {
      complexColor2.setBaseChangingConfigurations(paramTypedValue.changingConfigurations);
      if (this.mPreloading) {
        if (verifyPreloadConfig(complexColor2.getChangingConfigurations(), 0, paramTypedValue.resourceId, "color"))
          sPreloadedComplexColors.put(l, complexColor2.getConstantState()); 
      } else {
        configurationBoundResourceCache.put(l, paramTheme, complexColor2.getConstantState());
      } 
    } 
    return complexColor2;
  }
  
  ComplexColor loadComplexColor(Resources paramResources, TypedValue paramTypedValue, int paramInt, Resources.Theme paramTheme) {
    long l1 = paramTypedValue.assetCookie, l2 = paramTypedValue.data;
    if (paramTypedValue.type >= 28 && paramTypedValue.type <= 31)
      return getColorStateListFromInt(paramTypedValue, l1 << 32L | l2); 
    String str = paramTypedValue.string.toString();
    if (str.endsWith(".xml"))
      try {
        return loadComplexColorFromName(paramResources, paramTheme, paramTypedValue, paramInt);
      } catch (Exception exception) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("File ");
        stringBuilder1.append(str);
        stringBuilder1.append(" from complex color resource ID #0x");
        stringBuilder1.append(Integer.toHexString(paramInt));
        Resources.NotFoundException notFoundException = new Resources.NotFoundException(stringBuilder1.toString());
        notFoundException.initCause(exception);
        throw notFoundException;
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("File ");
    stringBuilder.append(str);
    stringBuilder.append(" from drawable resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    stringBuilder.append(": .xml extension required");
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  ColorStateList loadColorStateList(Resources paramResources, TypedValue paramTypedValue, int paramInt, Resources.Theme paramTheme) throws Resources.NotFoundException {
    long l1 = paramTypedValue.assetCookie, l2 = paramTypedValue.data;
    if (paramTypedValue.type >= 28 && paramTypedValue.type <= 31)
      return getColorStateListFromInt(paramTypedValue, l1 << 32L | l2); 
    ComplexColor complexColor = loadComplexColorFromName(paramResources, paramTheme, paramTypedValue, paramInt);
    if (complexColor != null && complexColor instanceof ColorStateList)
      return (ColorStateList)complexColor; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't find ColorStateList from drawable resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new Resources.NotFoundException(stringBuilder.toString());
  }
  
  private ColorStateList getColorStateListFromInt(TypedValue paramTypedValue, long paramLong) {
    LongSparseArray<ConstantState<ComplexColor>> longSparseArray = sPreloadedComplexColors;
    ConstantState<ColorStateList> constantState = (ConstantState)longSparseArray.get(paramLong);
    if (constantState != null)
      return constantState.newInstance(); 
    ColorStateList colorStateList = ColorStateList.valueOf(paramTypedValue.data);
    if (this.mPreloading && 
      verifyPreloadConfig(paramTypedValue.changingConfigurations, 0, paramTypedValue.resourceId, "color"))
      sPreloadedComplexColors.put(paramLong, colorStateList.getConstantState()); 
    return colorStateList;
  }
  
  private ComplexColor loadComplexColorForCookie(Resources paramResources, TypedValue paramTypedValue, int paramInt, Resources.Theme paramTheme) {
    Resources.NotFoundException notFoundException;
    if (paramTypedValue.string != null) {
      String str = paramTypedValue.string.toString();
      TypedValue typedValue = null;
      Trace.traceBegin(8192L, str);
      if (str.endsWith(".xml"))
        try {
          int i;
          XmlResourceParser xmlResourceParser = loadXmlResourceParser(str, paramInt, paramTypedValue.assetCookie, "ComplexColor");
          AttributeSet attributeSet = Xml.asAttributeSet(xmlResourceParser);
          while (true) {
            i = xmlResourceParser.next();
            if (i != 2 && i != 1)
              continue; 
            break;
          } 
          if (i == 2) {
            ColorStateList colorStateList;
            String str1 = xmlResourceParser.getName();
            if (str1.equals("gradient")) {
              GradientColor gradientColor = GradientColor.createFromXmlInner(paramResources, xmlResourceParser, attributeSet, paramTheme);
            } else {
              paramTypedValue = typedValue;
              if (str1.equals("selector"))
                colorStateList = ColorStateList.createFromXmlInner(paramResources, xmlResourceParser, attributeSet, paramTheme); 
            } 
            xmlResourceParser.close();
            Trace.traceEnd(8192L);
            return colorStateList;
          } 
          XmlPullParserException xmlPullParserException = new XmlPullParserException();
          this("No start tag found");
          throw xmlPullParserException;
        } catch (Exception exception) {
          Trace.traceEnd(8192L);
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("File ");
          stringBuilder2.append(str);
          stringBuilder2.append(" from ComplexColor resource ID #0x");
          stringBuilder2.append(Integer.toHexString(paramInt));
          notFoundException = new Resources.NotFoundException(stringBuilder2.toString());
          notFoundException.initCause(exception);
          throw notFoundException;
        }  
      Trace.traceEnd(8192L);
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("File ");
      stringBuilder1.append(str);
      stringBuilder1.append(" from drawable resource ID #0x");
      stringBuilder1.append(Integer.toHexString(paramInt));
      stringBuilder1.append(": .xml extension required");
      throw new Resources.NotFoundException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Can't convert to ComplexColor: type=0x");
    stringBuilder.append(((TypedValue)notFoundException).type);
    throw new UnsupportedOperationException(stringBuilder.toString());
  }
  
  XmlResourceParser loadXmlResourceParser(String paramString1, int paramInt1, int paramInt2, String paramString2) throws Resources.NotFoundException {
    // Byte code:
    //   0: iload_2
    //   1: ifeq -> 284
    //   4: aload_0
    //   5: getfield mCachedXmlBlocks : [Landroid/content/res/XmlBlock;
    //   8: astore #5
    //   10: aload #5
    //   12: monitorenter
    //   13: aload_0
    //   14: getfield mCachedXmlBlockCookies : [I
    //   17: astore #6
    //   19: aload_0
    //   20: getfield mCachedXmlBlockFiles : [Ljava/lang/String;
    //   23: astore #7
    //   25: aload_0
    //   26: getfield mCachedXmlBlocks : [Landroid/content/res/XmlBlock;
    //   29: astore #8
    //   31: aload #7
    //   33: arraylength
    //   34: istore #9
    //   36: iconst_0
    //   37: istore #10
    //   39: iload #10
    //   41: iload #9
    //   43: if_icmpge -> 102
    //   46: aload #6
    //   48: iload #10
    //   50: iaload
    //   51: iload_3
    //   52: if_icmpne -> 96
    //   55: aload #7
    //   57: iload #10
    //   59: aaload
    //   60: ifnull -> 96
    //   63: aload #7
    //   65: iload #10
    //   67: aaload
    //   68: astore #11
    //   70: aload #11
    //   72: aload_1
    //   73: invokevirtual equals : (Ljava/lang/Object;)Z
    //   76: ifeq -> 96
    //   79: aload #8
    //   81: iload #10
    //   83: aaload
    //   84: iload_2
    //   85: invokevirtual newParser : (I)Landroid/content/res/XmlResourceParser;
    //   88: astore #6
    //   90: aload #5
    //   92: monitorexit
    //   93: aload #6
    //   95: areturn
    //   96: iinc #10, 1
    //   99: goto -> 39
    //   102: aload_0
    //   103: getfield mAssets : Landroid/content/res/AssetManager;
    //   106: iload_3
    //   107: aload_1
    //   108: invokevirtual openXmlBlockAsset : (ILjava/lang/String;)Landroid/content/res/XmlBlock;
    //   111: astore #12
    //   113: aload #12
    //   115: ifnull -> 185
    //   118: aload_0
    //   119: getfield mLastCachedXmlBlockIndex : I
    //   122: iconst_1
    //   123: iadd
    //   124: iload #9
    //   126: irem
    //   127: istore #10
    //   129: aload_0
    //   130: iload #10
    //   132: putfield mLastCachedXmlBlockIndex : I
    //   135: aload #8
    //   137: iload #10
    //   139: aaload
    //   140: astore #11
    //   142: aload #11
    //   144: ifnull -> 152
    //   147: aload #11
    //   149: invokevirtual close : ()V
    //   152: aload #6
    //   154: iload #10
    //   156: iload_3
    //   157: iastore
    //   158: aload #7
    //   160: iload #10
    //   162: aload_1
    //   163: aastore
    //   164: aload #8
    //   166: iload #10
    //   168: aload #12
    //   170: aastore
    //   171: aload #12
    //   173: iload_2
    //   174: invokevirtual newParser : (I)Landroid/content/res/XmlResourceParser;
    //   177: astore #6
    //   179: aload #5
    //   181: monitorexit
    //   182: aload #6
    //   184: areturn
    //   185: aload #5
    //   187: monitorexit
    //   188: goto -> 284
    //   191: astore #6
    //   193: aload #5
    //   195: monitorexit
    //   196: aload #6
    //   198: athrow
    //   199: astore #5
    //   201: new java/lang/StringBuilder
    //   204: dup
    //   205: invokespecial <init> : ()V
    //   208: astore #6
    //   210: aload #6
    //   212: ldc_w 'File '
    //   215: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: pop
    //   219: aload #6
    //   221: aload_1
    //   222: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   225: pop
    //   226: aload #6
    //   228: ldc_w ' from xml type '
    //   231: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload #6
    //   237: aload #4
    //   239: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: aload #6
    //   245: ldc_w ' resource ID #0x'
    //   248: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload #6
    //   254: iload_2
    //   255: invokestatic toHexString : (I)Ljava/lang/String;
    //   258: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   261: pop
    //   262: new android/content/res/Resources$NotFoundException
    //   265: dup
    //   266: aload #6
    //   268: invokevirtual toString : ()Ljava/lang/String;
    //   271: invokespecial <init> : (Ljava/lang/String;)V
    //   274: astore_1
    //   275: aload_1
    //   276: aload #5
    //   278: invokevirtual initCause : (Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   281: pop
    //   282: aload_1
    //   283: athrow
    //   284: new java/lang/StringBuilder
    //   287: dup
    //   288: invokespecial <init> : ()V
    //   291: astore #5
    //   293: aload #5
    //   295: ldc_w 'File '
    //   298: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   301: pop
    //   302: aload #5
    //   304: aload_1
    //   305: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   308: pop
    //   309: aload #5
    //   311: ldc_w ' from xml type '
    //   314: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   317: pop
    //   318: aload #5
    //   320: aload #4
    //   322: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   325: pop
    //   326: aload #5
    //   328: ldc_w ' resource ID #0x'
    //   331: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   334: pop
    //   335: aload #5
    //   337: iload_2
    //   338: invokestatic toHexString : (I)Ljava/lang/String;
    //   341: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   344: pop
    //   345: new android/content/res/Resources$NotFoundException
    //   348: dup
    //   349: aload #5
    //   351: invokevirtual toString : ()Ljava/lang/String;
    //   354: invokespecial <init> : (Ljava/lang/String;)V
    //   357: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1289	-> 0
    //   #1291	-> 4
    //   #1292	-> 13
    //   #1293	-> 19
    //   #1294	-> 25
    //   #1296	-> 31
    //   #1297	-> 36
    //   #1298	-> 46
    //   #1299	-> 70
    //   #1300	-> 79
    //   #1297	-> 96
    //   #1306	-> 102
    //   #1307	-> 113
    //   #1308	-> 118
    //   #1309	-> 129
    //   #1310	-> 135
    //   #1311	-> 142
    //   #1312	-> 147
    //   #1314	-> 152
    //   #1315	-> 158
    //   #1316	-> 164
    //   #1317	-> 171
    //   #1319	-> 185
    //   #1325	-> 188
    //   #1319	-> 191
    //   #1320	-> 199
    //   #1321	-> 201
    //   #1322	-> 252
    //   #1323	-> 275
    //   #1324	-> 282
    //   #1328	-> 284
    //   #1329	-> 335
    // Exception table:
    //   from	to	target	type
    //   4	13	199	java/lang/Exception
    //   13	19	191	finally
    //   19	25	191	finally
    //   25	31	191	finally
    //   31	36	191	finally
    //   70	79	191	finally
    //   79	93	191	finally
    //   102	113	191	finally
    //   118	129	191	finally
    //   129	135	191	finally
    //   147	152	191	finally
    //   171	182	191	finally
    //   185	188	191	finally
    //   193	196	191	finally
    //   196	199	199	java/lang/Exception
  }
  
  public final void startPreloading() {
    synchronized (sSync) {
      if (!sPreloaded) {
        sPreloaded = true;
        this.mPreloading = true;
        this.mConfiguration.densityDpi = DisplayMetrics.DENSITY_DEVICE;
        updateConfiguration(null, null, null);
        if (TRACE_FOR_DETAILED_PRELOAD) {
          this.mPreloadTracingPreloadStartTime = SystemClock.uptimeMillis();
          this.mPreloadTracingStartBitmapSize = Bitmap.sPreloadTracingTotalBitmapsSize;
          this.mPreloadTracingStartBitmapCount = Bitmap.sPreloadTracingNumInstantiatedBitmaps;
          Log.d("Resources.preload", "Preload starting");
        } 
        return;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Resources already preloaded");
      throw illegalStateException;
    } 
  }
  
  void finishPreloading() {
    if (this.mPreloading) {
      if (TRACE_FOR_DETAILED_PRELOAD) {
        long l1 = SystemClock.uptimeMillis(), l2 = this.mPreloadTracingPreloadStartTime;
        long l3 = Bitmap.sPreloadTracingTotalBitmapsSize, l4 = this.mPreloadTracingStartBitmapSize;
        long l5 = Bitmap.sPreloadTracingNumInstantiatedBitmaps, l6 = this.mPreloadTracingStartBitmapCount;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Preload finished, ");
        stringBuilder.append(l5 - l6);
        stringBuilder.append(" bitmaps of ");
        stringBuilder.append(l3 - l4);
        stringBuilder.append(" bytes in ");
        stringBuilder.append(l1 - l2);
        stringBuilder.append(" ms");
        Log.d("Resources.preload", stringBuilder.toString());
      } 
      this.mPreloading = false;
      flushLayoutCache();
    } 
  }
  
  static int getAttributeSetSourceResId(AttributeSet paramAttributeSet) {
    if (paramAttributeSet == null || !(paramAttributeSet instanceof XmlBlock.Parser))
      return 0; 
    return ((XmlBlock.Parser)paramAttributeSet).getSourceResId();
  }
  
  LongSparseArray<Drawable.ConstantState> getPreloadedDrawables() {
    return sPreloadedDrawables[0];
  }
  
  ThemeImpl newThemeImpl() {
    return new ThemeImpl();
  }
  
  ThemeImpl newThemeImpl(Resources.ThemeKey paramThemeKey) {
    ThemeImpl themeImpl = new ThemeImpl();
    themeImpl.mKey.setTo(paramThemeKey);
    themeImpl.rebase();
    return themeImpl;
  }
  
  class ThemeImpl {
    private final AssetManager mAssets;
    
    private final Resources.ThemeKey mKey = new Resources.ThemeKey();
    
    private final long mTheme;
    
    private int mThemeResId = 0;
    
    final ResourcesImpl this$0;
    
    ThemeImpl() {
      AssetManager assetManager = ResourcesImpl.this.mAssets;
      this.mTheme = assetManager.createTheme();
    }
    
    protected void finalize() throws Throwable {
      super.finalize();
      this.mAssets.releaseTheme(this.mTheme);
    }
    
    Resources.ThemeKey getKey() {
      return this.mKey;
    }
    
    long getNativeTheme() {
      return this.mTheme;
    }
    
    int getAppliedStyleResId() {
      return this.mThemeResId;
    }
    
    void applyStyle(int param1Int, boolean param1Boolean) {
      synchronized (this.mKey) {
        this.mAssets.applyStyleToTheme(this.mTheme, param1Int, param1Boolean);
        this.mThemeResId = param1Int;
        this.mKey.append(param1Int, param1Boolean);
        return;
      } 
    }
    
    void setTo(ThemeImpl param1ThemeImpl) {
      synchronized (this.mKey) {
        synchronized (param1ThemeImpl.mKey) {
          this.mAssets.setThemeTo(this.mTheme, param1ThemeImpl.mAssets, param1ThemeImpl.mTheme);
          this.mThemeResId = param1ThemeImpl.mThemeResId;
          this.mKey.setTo(param1ThemeImpl.getKey());
          return;
        } 
      } 
    }
    
    TypedArray obtainStyledAttributes(Resources.Theme param1Theme, AttributeSet param1AttributeSet, int[] param1ArrayOfint, int param1Int1, int param1Int2) {
      Resources.ThemeKey themeKey = this.mKey;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/content/res/Resources}.Landroid/content/res/Resources$ThemeKey;}, name=null} */
      try {
        int i = param1ArrayOfint.length;
        TypedArray typedArray = TypedArray.obtain(param1Theme.getResources(), i);
        param1AttributeSet = param1AttributeSet;
        this.mAssets.applyStyle(this.mTheme, param1Int1, param1Int2, (XmlBlock.Parser)param1AttributeSet, param1ArrayOfint, typedArray.mDataAddress, typedArray.mIndicesAddress);
        try {
          typedArray.mTheme = param1Theme;
          typedArray.mXml = (XmlBlock.Parser)param1AttributeSet;
          TypedArray typedArray1 = ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).replaceTypedArray(ResourcesImpl.this, typedArray);
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/content/res/Resources}.Landroid/content/res/Resources$ThemeKey;}, name=null} */
          return typedArray1;
        } finally {}
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=InnerObjectType{ObjectType{android/content/res/Resources}.Landroid/content/res/Resources$ThemeKey;}, name=null} */
      throw param1Theme;
    }
    
    TypedArray resolveAttributes(Resources.Theme param1Theme, int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      synchronized (this.mKey) {
        int i = param1ArrayOfint2.length;
        if (param1ArrayOfint1 != null && i == param1ArrayOfint1.length) {
          TypedArray typedArray = TypedArray.obtain(param1Theme.getResources(), i);
          this.mAssets.resolveAttrs(this.mTheme, 0, 0, param1ArrayOfint1, param1ArrayOfint2, typedArray.mData, typedArray.mIndices);
          typedArray.mTheme = param1Theme;
          typedArray.mXml = null;
          return ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).replaceTypedArray(ResourcesImpl.this, typedArray);
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("Base attribute values must the same length as attrs");
        throw illegalArgumentException;
      } 
    }
    
    boolean resolveAttribute(int param1Int, TypedValue param1TypedValue, boolean param1Boolean) {
      synchronized (this.mKey) {
        param1Boolean = this.mAssets.getThemeValue(this.mTheme, param1Int, param1TypedValue, param1Boolean);
        return param1Boolean;
      } 
    }
    
    int[] getAllAttributes() {
      return this.mAssets.getStyleAttributes(getAppliedStyleResId());
    }
    
    int getChangingConfigurations() {
      synchronized (this.mKey) {
        long l = this.mTheme;
        int i = AssetManager.nativeThemeGetChangingConfigurations(l);
        i = ActivityInfo.activityInfoConfigNativeToJava(i);
        return i;
      } 
    }
    
    public void dump(int param1Int, String param1String1, String param1String2) {
      synchronized (this.mKey) {
        this.mAssets.dumpTheme(this.mTheme, param1Int, param1String1, param1String2);
        return;
      } 
    }
    
    String[] getTheme() {
      synchronized (this.mKey) {
        int i = this.mKey.mCount;
        String[] arrayOfString = new String[i * 2];
        byte b = 0;
        i--;
        for (; b < arrayOfString.length; b += 2, i--) {
          String str;
          int j = this.mKey.mResId[i];
          boolean bool = this.mKey.mForce[i];
          try {
            arrayOfString[b] = ResourcesImpl.this.getResourceName(j);
          } catch (NotFoundException notFoundException) {
            arrayOfString[b] = Integer.toHexString(b);
          } 
          if (bool) {
            str = "forced";
          } else {
            str = "not forced";
          } 
          arrayOfString[b + 1] = str;
        } 
        return arrayOfString;
      } 
    }
    
    void rebase() {
      synchronized (this.mKey) {
        AssetManager.nativeThemeClear(this.mTheme);
        for (byte b = 0; b < this.mKey.mCount; b++) {
          int i = this.mKey.mResId[b];
          boolean bool = this.mKey.mForce[b];
          this.mAssets.applyStyleToTheme(this.mTheme, i, bool);
        } 
        return;
      } 
    }
    
    public int[] getAttributeResolutionStack(int param1Int1, int param1Int2, int param1Int3) {
      synchronized (this.mKey) {
        return this.mAssets.getAttributeResolutionStack(this.mTheme, param1Int1, param1Int2, param1Int3);
      } 
    }
  }
  
  class LookupStack {
    private int[] mIds = new int[4];
    
    private int mSize = 0;
    
    public void push(int param1Int) {
      this.mIds = GrowingArrayUtils.append(this.mIds, this.mSize, param1Int);
      this.mSize++;
    }
    
    public boolean contains(int param1Int) {
      for (byte b = 0; b < this.mSize; b++) {
        if (this.mIds[b] == param1Int)
          return true; 
      } 
      return false;
    }
    
    public void pop() {
      this.mSize--;
    }
    
    private LookupStack() {}
  }
  
  public void clearCache() {
    super.clearCache();
    sPreloadedDrawables[0].clear();
    sPreloadedComplexColors.clear();
    sPreloadedColorDrawables.clear();
  }
  
  public Configuration getSystemConfiguration() {
    return Resources.getSystem().getConfiguration();
  }
}
