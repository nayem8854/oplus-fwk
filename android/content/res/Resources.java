package android.content.res;

import android.animation.Animator;
import android.animation.StateListAnimator;
import android.common.OplusFeatureCache;
import android.content.res.loader.ResourcesLoader;
import android.graphics.Movie;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableInflater;
import android.os.Bundle;
import android.util.ArraySet;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.Pools;
import android.util.TypedValue;
import android.view.DisplayAdjustments;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewHierarchyEncoder;
import com.android.internal.R;
import com.android.internal.util.ArrayUtils;
import com.android.internal.util.GrowingArrayUtils;
import com.android.internal.util.Preconditions;
import com.android.internal.util.XmlUtils;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;
import org.xmlpull.v1.XmlPullParserException;

public class Resources extends OplusBaseResources {
  private static final Object sSync = new Object();
  
  private final Object mUpdateLock = new Object();
  
  static {
    mSystem = null;
  }
  
  final Pools.SynchronizedPool<TypedArray> mTypedArrayPool = new Pools.SynchronizedPool(5);
  
  private final Object mTmpValueLock = new Object();
  
  private TypedValue mTmpValue = new TypedValue();
  
  private UpdateCallbacks mCallbacks = null;
  
  private final ArrayList<WeakReference<Theme>> mThemeRefs = new ArrayList<>();
  
  private int mThemeRefsNextFlushSize = 32;
  
  public static final int ID_NULL = 0;
  
  private static final int MIN_THEME_REFS_FLUSH_SIZE = 32;
  
  static final String TAG = "Resources";
  
  static Resources mSystem;
  
  private int mBaseApkAssetsSize;
  
  final ClassLoader mClassLoader;
  
  private DrawableInflater mDrawableInflater;
  
  private DisplayAdjustments mOverrideDisplayAdjustments;
  
  private ResourcesImpl mResourcesImpl;
  
  public static int selectDefaultTheme(int paramInt1, int paramInt2) {
    return selectSystemTheme(paramInt1, paramInt2, 16973829, 16973931, 16974120, 16974143);
  }
  
  public static int selectSystemTheme(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    if (paramInt1 != 0)
      return paramInt1; 
    if (paramInt2 < 11)
      return paramInt3; 
    if (paramInt2 < 14)
      return paramInt4; 
    if (paramInt2 < 24)
      return paramInt5; 
    return paramInt6;
  }
  
  public static Resources getSystem() {
    synchronized (sSync) {
      Resources resources1 = mSystem;
      Resources resources2 = resources1;
      if (resources1 == null) {
        resources2 = new Resources();
        this();
        mSystem = resources2;
      } 
      return resources2;
    } 
  }
  
  class NotFoundException extends RuntimeException {
    public NotFoundException() {}
    
    public NotFoundException(Resources this$0) {
      super((String)this$0);
    }
    
    public NotFoundException(Resources this$0, Exception param1Exception) {
      super((String)this$0, param1Exception);
    }
  }
  
  class AssetManagerUpdateHandler implements UpdateCallbacks {
    final Resources this$0;
    
    public void onLoadersChanged(Resources param1Resources, List<ResourcesLoader> param1List) {
      boolean bool;
      if (Resources.this == param1Resources) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkArgument(bool);
      ResourcesImpl resourcesImpl = Resources.this.mResourcesImpl;
      resourcesImpl.clearAllCaches();
      resourcesImpl.getAssets().setLoaders(param1List);
    }
    
    public void onLoaderUpdated(ResourcesLoader param1ResourcesLoader) {
      ResourcesImpl resourcesImpl = Resources.this.mResourcesImpl;
      AssetManager assetManager = resourcesImpl.getAssets();
      if (assetManager.getLoaders().contains(param1ResourcesLoader)) {
        resourcesImpl.clearAllCaches();
        assetManager.setLoaders(assetManager.getLoaders());
      } 
    }
  }
  
  @Deprecated
  public Resources(AssetManager paramAssetManager, DisplayMetrics paramDisplayMetrics, Configuration paramConfiguration) {
    this(null);
    this.mResourcesImpl = new ResourcesImpl(paramAssetManager, paramDisplayMetrics, paramConfiguration, new DisplayAdjustments());
  }
  
  public Resources(ClassLoader paramClassLoader) {
    if (paramClassLoader == null)
      paramClassLoader = ClassLoader.getSystemClassLoader(); 
    this.mClassLoader = paramClassLoader;
  }
  
  private Resources() {
    this(null);
    DisplayMetrics displayMetrics = new DisplayMetrics();
    displayMetrics.setToDefaults();
    Configuration configuration = new Configuration();
    configuration.setToDefaults();
    this.mResourcesImpl = new ResourcesImpl(AssetManager.getSystem(), displayMetrics, configuration, new DisplayAdjustments());
  }
  
  public void setImpl(ResourcesImpl paramResourcesImpl) {
    if (paramResourcesImpl == this.mResourcesImpl)
      return; 
    this.mBaseApkAssetsSize = ArrayUtils.size((Object[])paramResourcesImpl.getAssets().getApkAssets());
    this.mResourcesImpl = paramResourcesImpl;
    synchronized (this.mThemeRefs) {
      int i = this.mThemeRefs.size();
      for (byte b = 0; b < i; b++) {
        WeakReference<Theme> weakReference = this.mThemeRefs.get(b);
        if (weakReference != null) {
          Theme theme = weakReference.get();
        } else {
          weakReference = null;
        } 
        if (weakReference != null)
          weakReference.setImpl(this.mResourcesImpl.newThemeImpl(weakReference.getKey())); 
      } 
      return;
    } 
  }
  
  public void setCallbacks(UpdateCallbacks paramUpdateCallbacks) {
    if (this.mCallbacks == null) {
      this.mCallbacks = paramUpdateCallbacks;
      return;
    } 
    throw new IllegalStateException("callback already registered");
  }
  
  public ResourcesImpl getImpl() {
    return this.mResourcesImpl;
  }
  
  public ClassLoader getClassLoader() {
    return this.mClassLoader;
  }
  
  public final DrawableInflater getDrawableInflater() {
    if (this.mDrawableInflater == null)
      this.mDrawableInflater = new DrawableInflater(this, this.mClassLoader); 
    return this.mDrawableInflater;
  }
  
  public ConfigurationBoundResourceCache<Animator> getAnimatorCache() {
    return this.mResourcesImpl.getAnimatorCache();
  }
  
  public ConfigurationBoundResourceCache<StateListAnimator> getStateListAnimatorCache() {
    return this.mResourcesImpl.getStateListAnimatorCache();
  }
  
  public CharSequence getText(int paramInt) throws NotFoundException {
    CharSequence charSequence = this.mResourcesImpl.getAssets().getResourceText(paramInt);
    if (charSequence != null)
      return charSequence; 
    charSequence = new StringBuilder();
    charSequence.append("String resource ID #0x");
    charSequence.append(Integer.toHexString(paramInt));
    throw new NotFoundException(charSequence.toString());
  }
  
  public Typeface getFont(int paramInt) throws NotFoundException {
    StringBuilder stringBuilder;
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      Typeface typeface = resourcesImpl.loadFont(this, typedValue, paramInt);
      if (typeface != null)
        return typeface; 
      releaseTempTypedValue(typedValue);
      stringBuilder = new StringBuilder();
      stringBuilder.append("Font resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      throw new NotFoundException(stringBuilder.toString());
    } finally {
      releaseTempTypedValue((TypedValue)stringBuilder);
    } 
  }
  
  Typeface getFont(TypedValue paramTypedValue, int paramInt) throws NotFoundException {
    return this.mResourcesImpl.loadFont(this, paramTypedValue, paramInt);
  }
  
  public void preloadFonts(int paramInt) {
    TypedArray typedArray = obtainTypedArray(paramInt);
    try {
      int i = typedArray.length();
      for (paramInt = 0; paramInt < i; paramInt++)
        typedArray.getFont(paramInt); 
      return;
    } finally {
      typedArray.recycle();
    } 
  }
  
  public CharSequence getQuantityText(int paramInt1, int paramInt2) throws NotFoundException {
    return this.mResourcesImpl.getQuantityText(paramInt1, paramInt2);
  }
  
  public String getString(int paramInt) throws NotFoundException {
    return getText(paramInt).toString();
  }
  
  public String getString(int paramInt, Object... paramVarArgs) throws NotFoundException {
    String str = getString(paramInt);
    return String.format(this.mResourcesImpl.getConfiguration().getLocales().get(0), str, paramVarArgs);
  }
  
  public String getQuantityString(int paramInt1, int paramInt2, Object... paramVarArgs) throws NotFoundException {
    String str = getQuantityText(paramInt1, paramInt2).toString();
    return String.format(this.mResourcesImpl.getConfiguration().getLocales().get(0), str, paramVarArgs);
  }
  
  public String getQuantityString(int paramInt1, int paramInt2) throws NotFoundException {
    return getQuantityText(paramInt1, paramInt2).toString();
  }
  
  public CharSequence getText(int paramInt, CharSequence paramCharSequence) {
    CharSequence charSequence;
    if (paramInt != 0) {
      charSequence = this.mResourcesImpl.getAssets().getResourceText(paramInt);
    } else {
      charSequence = null;
    } 
    if (charSequence != null)
      paramCharSequence = charSequence; 
    return paramCharSequence;
  }
  
  public CharSequence[] getTextArray(int paramInt) throws NotFoundException {
    CharSequence[] arrayOfCharSequence = this.mResourcesImpl.getAssets().getResourceTextArray(paramInt);
    if (arrayOfCharSequence != null)
      return arrayOfCharSequence; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Text array resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new NotFoundException(stringBuilder.toString());
  }
  
  public String[] getStringArray(int paramInt) throws NotFoundException {
    String[] arrayOfString = this.mResourcesImpl.getAssets().getResourceStringArray(paramInt);
    if (arrayOfString != null)
      return arrayOfString; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("String array resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new NotFoundException(stringBuilder.toString());
  }
  
  public int[] getIntArray(int paramInt) throws NotFoundException {
    int[] arrayOfInt = this.mResourcesImpl.getAssets().getResourceIntArray(paramInt);
    if (arrayOfInt != null)
      return arrayOfInt; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Int array resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new NotFoundException(stringBuilder.toString());
  }
  
  public TypedArray obtainTypedArray(int paramInt) throws NotFoundException {
    ResourcesImpl resourcesImpl = this.mResourcesImpl;
    int i = resourcesImpl.getAssets().getResourceArraySize(paramInt);
    if (i >= 0) {
      TypedArray typedArray = TypedArray.obtain(this, i);
      typedArray.mLength = resourcesImpl.getAssets().getResourceArray(paramInt, typedArray.mData);
      typedArray.mIndices[0] = 0;
      return typedArray;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Array resource ID #0x");
    stringBuilder.append(Integer.toHexString(paramInt));
    throw new NotFoundException(stringBuilder.toString());
  }
  
  public float getDimension(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type == 5)
        return TypedValue.complexToDimension(typedValue.data, resourcesImpl.getDisplayMetrics()); 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public int getDimensionPixelOffset(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type == 5) {
        paramInt = typedValue.data;
        DisplayMetrics displayMetrics = resourcesImpl.getDisplayMetrics();
        paramInt = TypedValue.complexToDimensionPixelOffset(paramInt, displayMetrics);
        return paramInt;
      } 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public int getDimensionPixelSize(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type == 5) {
        paramInt = TypedValue.complexToDimensionPixelSize(typedValue.data, resourcesImpl.getDisplayMetrics());
        return paramInt;
      } 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public float getFraction(int paramInt1, int paramInt2, int paramInt3) {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      this.mResourcesImpl.getValue(paramInt1, typedValue, true);
      if (typedValue.type == 6)
        return TypedValue.complexToFraction(typedValue.data, paramInt2, paramInt3); 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt1));
      stringBuilder.append(" type #0x");
      paramInt1 = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt1));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  @Deprecated
  public Drawable getDrawable(int paramInt) throws NotFoundException {
    Drawable drawable = getDrawable(paramInt, null);
    if (drawable != null && drawable.canApplyTheme()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Drawable ");
      stringBuilder.append(getResourceName(paramInt));
      stringBuilder.append(" has unresolved theme attributes! Consider using Resources.getDrawable(int, Theme) or Context.getDrawable(int).");
      Log.w("Resources", stringBuilder.toString(), new RuntimeException());
    } 
    return drawable;
  }
  
  public Drawable getDrawable(int paramInt, Theme paramTheme) throws NotFoundException {
    return getDrawableForDensity(paramInt, 0, paramTheme);
  }
  
  @Deprecated
  public Drawable getDrawableForDensity(int paramInt1, int paramInt2) throws NotFoundException {
    return getDrawableForDensity(paramInt1, paramInt2, null);
  }
  
  public Drawable getDrawableForDensity(int paramInt1, int paramInt2, Theme paramTheme) {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValueForDensity(paramInt1, paramInt2, typedValue, true);
      return loadDrawable(typedValue, paramInt1, paramInt2, paramTheme);
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  Drawable loadDrawable(TypedValue paramTypedValue, int paramInt1, int paramInt2, Theme paramTheme) throws NotFoundException {
    return this.mResourcesImpl.loadDrawable(this, paramTypedValue, paramInt1, paramInt2, paramTheme);
  }
  
  @Deprecated
  public Movie getMovie(int paramInt) throws NotFoundException {
    InputStream inputStream = openRawResource(paramInt);
    Movie movie = Movie.decodeStream(inputStream);
    try {
      inputStream.close();
    } catch (IOException iOException) {}
    return movie;
  }
  
  @Deprecated
  public int getColor(int paramInt) throws NotFoundException {
    return getColor(paramInt, null);
  }
  
  public int getColor(int paramInt, Theme paramTheme) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type >= 16 && typedValue.type <= 31) {
        paramInt = typedValue.data;
        return paramInt;
      } 
      if (typedValue.type == 3) {
        ColorStateList colorStateList = resourcesImpl.loadColorStateList(this, typedValue, paramInt, paramTheme);
        paramInt = colorStateList.getDefaultColor();
        return paramInt;
      } 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  @Deprecated
  public ColorStateList getColorStateList(int paramInt) throws NotFoundException {
    ColorStateList colorStateList = getColorStateList(paramInt, null);
    if (colorStateList != null && colorStateList.canApplyTheme()) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("ColorStateList ");
      stringBuilder.append(getResourceName(paramInt));
      stringBuilder.append(" has unresolved theme attributes! Consider using Resources.getColorStateList(int, Theme) or Context.getColorStateList(int).");
      Log.w("Resources", stringBuilder.toString(), new RuntimeException());
    } 
    return colorStateList;
  }
  
  public ColorStateList getColorStateList(int paramInt, Theme paramTheme) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      return resourcesImpl.loadColorStateList(this, typedValue, paramInt, paramTheme);
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  ColorStateList loadColorStateList(TypedValue paramTypedValue, int paramInt, Theme paramTheme) throws NotFoundException {
    return this.mResourcesImpl.loadColorStateList(this, paramTypedValue, paramInt, paramTheme);
  }
  
  public ComplexColor loadComplexColor(TypedValue paramTypedValue, int paramInt, Theme paramTheme) {
    return this.mResourcesImpl.loadComplexColor(this, paramTypedValue, paramInt, paramTheme);
  }
  
  public boolean getBoolean(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      boolean bool = true;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type >= 16 && typedValue.type <= 31) {
        paramInt = typedValue.data;
        if (paramInt == 0)
          bool = false; 
        return bool;
      } 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public int getInteger(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      this.mResourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type >= 16 && typedValue.type <= 31) {
        paramInt = typedValue.data;
        return paramInt;
      } 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public float getFloat(int paramInt) {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      this.mResourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type == 4)
        return typedValue.getFloat(); 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public XmlResourceParser getLayout(int paramInt) throws NotFoundException {
    return loadXmlResourceParser(paramInt, "layout");
  }
  
  public XmlResourceParser getAnimation(int paramInt) throws NotFoundException {
    return loadXmlResourceParser(paramInt, "anim");
  }
  
  public XmlResourceParser getXml(int paramInt) throws NotFoundException {
    return loadXmlResourceParser(paramInt, "xml");
  }
  
  public InputStream openRawResource(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      return openRawResource(paramInt, typedValue);
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  private TypedValue obtainTempTypedValue() {
    null = null;
    synchronized (this.mTmpValueLock) {
      if (this.mTmpValue != null) {
        null = this.mTmpValue;
        this.mTmpValue = null;
      } 
      if (null == null)
        return new TypedValue(); 
      return null;
    } 
  }
  
  private void releaseTempTypedValue(TypedValue paramTypedValue) {
    synchronized (this.mTmpValueLock) {
      if (this.mTmpValue == null)
        this.mTmpValue = paramTypedValue; 
      return;
    } 
  }
  
  public InputStream openRawResource(int paramInt, TypedValue paramTypedValue) throws NotFoundException {
    return this.mResourcesImpl.openRawResource(paramInt, paramTypedValue);
  }
  
  public AssetFileDescriptor openRawResourceFd(int paramInt) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      return this.mResourcesImpl.openRawResourceFd(paramInt, typedValue);
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  public void getValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean) throws NotFoundException {
    this.mResourcesImpl.getValue(paramInt, paramTypedValue, paramBoolean);
  }
  
  public void getValueForDensity(int paramInt1, int paramInt2, TypedValue paramTypedValue, boolean paramBoolean) throws NotFoundException {
    this.mResourcesImpl.getValueForDensity(paramInt1, paramInt2, paramTypedValue, paramBoolean);
  }
  
  public void getValue(String paramString, TypedValue paramTypedValue, boolean paramBoolean) throws NotFoundException {
    this.mResourcesImpl.getValue(paramString, paramTypedValue, paramBoolean);
  }
  
  public static int getAttributeSetSourceResId(AttributeSet paramAttributeSet) {
    return ResourcesImpl.getAttributeSetSourceResId(paramAttributeSet);
  }
  
  class Theme {
    private ResourcesImpl.ThemeImpl mThemeImpl;
    
    final Resources this$0;
    
    private Theme() {}
    
    void setImpl(ResourcesImpl.ThemeImpl param1ThemeImpl) {
      this.mThemeImpl = param1ThemeImpl;
    }
    
    public void applyStyle(int param1Int, boolean param1Boolean) {
      this.mThemeImpl.applyStyle(param1Int, param1Boolean);
    }
    
    public void setTo(Theme param1Theme) {
      this.mThemeImpl.setTo(param1Theme.mThemeImpl);
    }
    
    public TypedArray obtainStyledAttributes(int[] param1ArrayOfint) {
      return this.mThemeImpl.obtainStyledAttributes(this, null, param1ArrayOfint, 0, 0);
    }
    
    public TypedArray obtainStyledAttributes(int param1Int, int[] param1ArrayOfint) throws Resources.NotFoundException {
      return this.mThemeImpl.obtainStyledAttributes(this, null, param1ArrayOfint, 0, param1Int);
    }
    
    public TypedArray obtainStyledAttributes(AttributeSet param1AttributeSet, int[] param1ArrayOfint, int param1Int1, int param1Int2) {
      return this.mThemeImpl.obtainStyledAttributes(this, param1AttributeSet, param1ArrayOfint, param1Int1, param1Int2);
    }
    
    public TypedArray resolveAttributes(int[] param1ArrayOfint1, int[] param1ArrayOfint2) {
      return this.mThemeImpl.resolveAttributes(this, param1ArrayOfint1, param1ArrayOfint2);
    }
    
    public boolean resolveAttribute(int param1Int, TypedValue param1TypedValue, boolean param1Boolean) {
      return this.mThemeImpl.resolveAttribute(param1Int, param1TypedValue, param1Boolean);
    }
    
    public int[] getAllAttributes() {
      return this.mThemeImpl.getAllAttributes();
    }
    
    public Resources getResources() {
      return Resources.this;
    }
    
    public Drawable getDrawable(int param1Int) throws Resources.NotFoundException {
      return Resources.this.getDrawable(param1Int, this);
    }
    
    public int getChangingConfigurations() {
      return this.mThemeImpl.getChangingConfigurations();
    }
    
    public void dump(int param1Int, String param1String1, String param1String2) {
      this.mThemeImpl.dump(param1Int, param1String1, param1String2);
    }
    
    long getNativeTheme() {
      return this.mThemeImpl.getNativeTheme();
    }
    
    int getAppliedStyleResId() {
      return this.mThemeImpl.getAppliedStyleResId();
    }
    
    public Resources.ThemeKey getKey() {
      return this.mThemeImpl.getKey();
    }
    
    private String getResourceNameFromHexString(String param1String) {
      return Resources.this.getResourceName(Integer.parseInt(param1String, 16));
    }
    
    @ExportedProperty(category = "theme", hasAdjacentMapping = true)
    public String[] getTheme() {
      return this.mThemeImpl.getTheme();
    }
    
    public void encode(ViewHierarchyEncoder param1ViewHierarchyEncoder) {
      param1ViewHierarchyEncoder.beginObject(this);
      String[] arrayOfString = getTheme();
      for (byte b = 0; b < arrayOfString.length; b += 2)
        param1ViewHierarchyEncoder.addProperty(arrayOfString[b], arrayOfString[b + 1]); 
      param1ViewHierarchyEncoder.endObject();
    }
    
    public void rebase() {
      this.mThemeImpl.rebase();
    }
    
    public int getExplicitStyle(AttributeSet param1AttributeSet) {
      TypedValue typedValue;
      if (param1AttributeSet == null)
        return 0; 
      int i = param1AttributeSet.getStyleAttribute();
      if (i == 0)
        return 0; 
      String str = getResources().getResourceTypeName(i);
      if ("attr".equals(str)) {
        typedValue = new TypedValue();
        boolean bool = resolveAttribute(i, typedValue, true);
        if (bool)
          return typedValue.resourceId; 
      } else if ("style".equals(typedValue)) {
        return i;
      } 
      return 0;
    }
    
    public int[] getAttributeResolutionStack(int param1Int1, int param1Int2, int param1Int3) {
      int[] arrayOfInt = this.mThemeImpl.getAttributeResolutionStack(param1Int1, param1Int2, param1Int3);
      if (arrayOfInt == null)
        return new int[0]; 
      return arrayOfInt;
    }
  }
  
  class ThemeKey implements Cloneable {
    int mCount;
    
    boolean[] mForce;
    
    private int mHashCode;
    
    int[] mResId;
    
    ThemeKey() {
      this.mHashCode = 0;
    }
    
    public void append(int param1Int, boolean param1Boolean) {
      if (this.mResId == null)
        this.mResId = new int[4]; 
      if (this.mForce == null)
        this.mForce = new boolean[4]; 
      this.mResId = GrowingArrayUtils.append(this.mResId, this.mCount, param1Int);
      this.mForce = GrowingArrayUtils.append(this.mForce, this.mCount, param1Boolean);
      this.mCount++;
      this.mHashCode = (this.mHashCode * 31 + param1Int) * 31 + param1Boolean;
    }
    
    public void setTo(ThemeKey param1ThemeKey) {
      int[] arrayOfInt = param1ThemeKey.mResId;
      boolean[] arrayOfBoolean2 = null;
      if (arrayOfInt == null) {
        arrayOfInt = null;
      } else {
        arrayOfInt = (int[])arrayOfInt.clone();
      } 
      this.mResId = arrayOfInt;
      boolean[] arrayOfBoolean1 = param1ThemeKey.mForce;
      if (arrayOfBoolean1 == null) {
        arrayOfBoolean1 = arrayOfBoolean2;
      } else {
        arrayOfBoolean1 = (boolean[])arrayOfBoolean1.clone();
      } 
      this.mForce = arrayOfBoolean1;
      this.mCount = param1ThemeKey.mCount;
      this.mHashCode = param1ThemeKey.mHashCode;
    }
    
    public int hashCode() {
      return this.mHashCode;
    }
    
    public boolean equals(Object param1Object) {
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass() || hashCode() != param1Object.hashCode())
        return false; 
      param1Object = param1Object;
      if (this.mCount != ((ThemeKey)param1Object).mCount)
        return false; 
      int i = this.mCount;
      for (byte b = 0; b < i; b++) {
        if (this.mResId[b] != ((ThemeKey)param1Object).mResId[b] || this.mForce[b] != ((ThemeKey)param1Object).mForce[b])
          return false; 
      } 
      return true;
    }
    
    public ThemeKey clone() {
      ThemeKey themeKey = new ThemeKey();
      themeKey.mResId = this.mResId;
      themeKey.mForce = this.mForce;
      themeKey.mCount = this.mCount;
      themeKey.mHashCode = this.mHashCode;
      return themeKey;
    }
  }
  
  public final Theme newTheme() {
    null = new Theme();
    null.setImpl(this.mResourcesImpl.newThemeImpl());
    synchronized (this.mThemeRefs) {
      ArrayList<WeakReference<Theme>> arrayList = this.mThemeRefs;
      WeakReference<Theme> weakReference = new WeakReference();
      this((T)null);
      arrayList.add(weakReference);
      if (this.mThemeRefs.size() > this.mThemeRefsNextFlushSize) {
        this.mThemeRefs.removeIf((Predicate<? super WeakReference<Theme>>)_$$Lambda$Resources$4msWUw7LKsgLexLZjIfWa4oguq4.INSTANCE);
        ArrayList<WeakReference<Theme>> arrayList1 = this.mThemeRefs;
        int i = arrayList1.size();
        this.mThemeRefsNextFlushSize = Math.max(32, i * 2);
      } 
      return null;
    } 
  }
  
  public TypedArray obtainAttributes(AttributeSet paramAttributeSet, int[] paramArrayOfint) {
    int i = paramArrayOfint.length;
    TypedArray typedArray = TypedArray.obtain(this, i);
    paramAttributeSet = paramAttributeSet;
    this.mResourcesImpl.getAssets().retrieveAttributes((XmlBlock.Parser)paramAttributeSet, paramArrayOfint, typedArray.mData, typedArray.mIndices);
    typedArray.mXml = (XmlBlock.Parser)paramAttributeSet;
    return ((IOplusThemeManager)OplusFeatureCache.<IOplusThemeManager>getOrCreate(IOplusThemeManager.DEFAULT, new Object[0])).replaceTypedArray(this.mResourcesImpl, typedArray);
  }
  
  @Deprecated
  public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics) {
    updateConfiguration(paramConfiguration, paramDisplayMetrics, null);
  }
  
  public void updateConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo) {
    this.mResourcesImpl.updateConfiguration(paramConfiguration, paramDisplayMetrics, paramCompatibilityInfo);
  }
  
  public static void updateSystemConfiguration(Configuration paramConfiguration, DisplayMetrics paramDisplayMetrics, CompatibilityInfo paramCompatibilityInfo) {
    Resources resources = mSystem;
    if (resources != null)
      resources.updateConfiguration(paramConfiguration, paramDisplayMetrics, paramCompatibilityInfo); 
  }
  
  public DisplayMetrics getDisplayMetrics() {
    return this.mResourcesImpl.getDisplayMetrics();
  }
  
  public DisplayAdjustments getDisplayAdjustments() {
    DisplayAdjustments displayAdjustments = this.mOverrideDisplayAdjustments;
    if (displayAdjustments != null)
      return displayAdjustments; 
    return this.mResourcesImpl.getDisplayAdjustments();
  }
  
  public void overrideDisplayAdjustments(Consumer<DisplayAdjustments> paramConsumer) {
    if (paramConsumer != null) {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      DisplayAdjustments displayAdjustments = new DisplayAdjustments(resourcesImpl.getDisplayAdjustments());
      paramConsumer.accept(displayAdjustments);
    } else {
      this.mOverrideDisplayAdjustments = null;
    } 
  }
  
  public boolean hasOverrideDisplayAdjustments() {
    boolean bool;
    if (this.mOverrideDisplayAdjustments != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public Configuration getConfiguration() {
    return this.mResourcesImpl.getConfiguration();
  }
  
  public Configuration[] getSizeConfigurations() {
    return this.mResourcesImpl.getSizeConfigurations();
  }
  
  public CompatibilityInfo getCompatibilityInfo() {
    return this.mResourcesImpl.getCompatibilityInfo();
  }
  
  public void setCompatibilityInfo(CompatibilityInfo paramCompatibilityInfo) {
    if (paramCompatibilityInfo != null)
      this.mResourcesImpl.updateConfiguration(null, null, paramCompatibilityInfo); 
  }
  
  public int getIdentifier(String paramString1, String paramString2, String paramString3) {
    return this.mResourcesImpl.getIdentifier(paramString1, paramString2, paramString3);
  }
  
  public static boolean resourceHasPackage(int paramInt) {
    boolean bool;
    if (paramInt >>> 24 != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public String getResourceName(int paramInt) throws NotFoundException {
    return this.mResourcesImpl.getResourceName(paramInt);
  }
  
  public String getResourcePackageName(int paramInt) throws NotFoundException {
    return this.mResourcesImpl.getResourcePackageName(paramInt);
  }
  
  public String getResourceTypeName(int paramInt) throws NotFoundException {
    return this.mResourcesImpl.getResourceTypeName(paramInt);
  }
  
  public String getResourceEntryName(int paramInt) throws NotFoundException {
    return this.mResourcesImpl.getResourceEntryName(paramInt);
  }
  
  public String getLastResourceResolution() throws NotFoundException {
    return this.mResourcesImpl.getLastResourceResolution();
  }
  
  public void parseBundleExtras(XmlResourceParser paramXmlResourceParser, Bundle paramBundle) throws XmlPullParserException, IOException {
    int i = paramXmlResourceParser.getDepth();
    while (true) {
      int j = paramXmlResourceParser.next();
      if (j != 1 && (j != 3 || 
        paramXmlResourceParser.getDepth() > i)) {
        if (j == 3 || j == 4)
          continue; 
        String str = paramXmlResourceParser.getName();
        if (str.equals("extra")) {
          parseBundleExtra("extra", paramXmlResourceParser, paramBundle);
          XmlUtils.skipCurrentTag(paramXmlResourceParser);
          continue;
        } 
        XmlUtils.skipCurrentTag(paramXmlResourceParser);
        continue;
      } 
      break;
    } 
  }
  
  public void parseBundleExtra(String paramString, AttributeSet paramAttributeSet, Bundle paramBundle) throws XmlPullParserException {
    CharSequence charSequence;
    TypedArray typedArray = obtainAttributes(paramAttributeSet, R.styleable.Extra);
    boolean bool = false;
    String str = typedArray.getString(0);
    if (str != null) {
      TypedValue typedValue = typedArray.peekValue(1);
      if (typedValue != null) {
        if (typedValue.type == 3) {
          charSequence = typedValue.coerceToString();
          paramBundle.putCharSequence(str, charSequence);
        } else if (typedValue.type == 18) {
          if (typedValue.data != 0)
            bool = true; 
          paramBundle.putBoolean(str, bool);
        } else if (typedValue.type >= 16 && typedValue.type <= 31) {
          paramBundle.putInt(str, typedValue.data);
        } else if (typedValue.type == 4) {
          paramBundle.putFloat(str, typedValue.getFloat());
        } else {
          typedArray.recycle();
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("<");
          stringBuilder2.append((String)charSequence);
          stringBuilder2.append("> only supports string, integer, float, color, and boolean at ");
          stringBuilder2.append(paramAttributeSet.getPositionDescription());
          throw new XmlPullParserException(stringBuilder2.toString());
        } 
        typedArray.recycle();
        return;
      } 
      typedArray.recycle();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("<");
      stringBuilder1.append((String)charSequence);
      stringBuilder1.append("> requires an android:value or android:resource attribute at ");
      stringBuilder1.append(paramAttributeSet.getPositionDescription());
      throw new XmlPullParserException(stringBuilder1.toString());
    } 
    typedArray.recycle();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<");
    stringBuilder.append((String)charSequence);
    stringBuilder.append("> requires an android:name attribute at ");
    stringBuilder.append(paramAttributeSet.getPositionDescription());
    throw new XmlPullParserException(stringBuilder.toString());
  }
  
  public final AssetManager getAssets() {
    return this.mResourcesImpl.getAssets();
  }
  
  public final void flushLayoutCache() {
    this.mResourcesImpl.flushLayoutCache();
  }
  
  public final void startPreloading() {
    this.mResourcesImpl.startPreloading();
  }
  
  public final void finishPreloading() {
    this.mResourcesImpl.finishPreloading();
  }
  
  public LongSparseArray<Drawable.ConstantState> getPreloadedDrawables() {
    return this.mResourcesImpl.getPreloadedDrawables();
  }
  
  XmlResourceParser loadXmlResourceParser(int paramInt, String paramString) throws NotFoundException {
    TypedValue typedValue = obtainTempTypedValue();
    try {
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      resourcesImpl.getValue(paramInt, typedValue, true);
      if (typedValue.type == 3)
        return loadXmlResourceParser(typedValue.string.toString(), paramInt, typedValue.assetCookie, paramString); 
      NotFoundException notFoundException = new NotFoundException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Resource ID #0x");
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" type #0x");
      paramInt = typedValue.type;
      stringBuilder.append(Integer.toHexString(paramInt));
      stringBuilder.append(" is not valid");
      this(stringBuilder.toString());
      throw notFoundException;
    } finally {
      releaseTempTypedValue(typedValue);
    } 
  }
  
  XmlResourceParser loadXmlResourceParser(String paramString1, int paramInt1, int paramInt2, String paramString2) throws NotFoundException {
    return this.mResourcesImpl.loadXmlResourceParser(paramString1, paramInt1, paramInt2, paramString2);
  }
  
  public int calcConfigChanges(Configuration paramConfiguration) {
    return this.mResourcesImpl.calcConfigChanges(paramConfiguration);
  }
  
  public static TypedArray obtainAttributes(Resources paramResources, Theme paramTheme, AttributeSet paramAttributeSet, int[] paramArrayOfint) {
    if (paramTheme == null)
      return paramResources.obtainAttributes(paramAttributeSet, paramArrayOfint); 
    return paramTheme.obtainStyledAttributes(paramAttributeSet, paramArrayOfint, 0, 0);
  }
  
  private void checkCallbacksRegistered() {
    if (this.mCallbacks == null)
      this.mCallbacks = new AssetManagerUpdateHandler(); 
  }
  
  public List<ResourcesLoader> getLoaders() {
    return this.mResourcesImpl.getAssets().getLoaders();
  }
  
  public void addLoaders(ResourcesLoader... paramVarArgs) {
    synchronized (this.mUpdateLock) {
      checkCallbacksRegistered();
      ArrayList<ResourcesLoader> arrayList = new ArrayList();
      ResourcesImpl resourcesImpl = this.mResourcesImpl;
      this((Collection)resourcesImpl.getAssets().getLoaders());
      ArraySet arraySet = new ArraySet();
      this(arrayList);
      int i;
      for (i = 0; i < paramVarArgs.length; i++) {
        ResourcesLoader resourcesLoader = paramVarArgs[i];
        if (!arraySet.contains(resourcesLoader))
          arrayList.add(resourcesLoader); 
      } 
      if (arraySet.size() == arrayList.size())
        return; 
      this.mCallbacks.onLoadersChanged(this, arrayList);
      int j;
      for (i = arraySet.size(), j = arrayList.size(); i < j; i++)
        ((ResourcesLoader)arrayList.get(i)).registerOnProvidersChangedCallback(this, this.mCallbacks); 
      return;
    } 
  }
  
  public void removeLoaders(ResourcesLoader... paramVarArgs) {
    synchronized (this.mUpdateLock) {
      checkCallbacksRegistered();
      ArraySet arraySet = new ArraySet();
      this((Object[])paramVarArgs);
      ArrayList<ResourcesLoader> arrayList = new ArrayList();
      this();
      List<ResourcesLoader> list = this.mResourcesImpl.getAssets().getLoaders();
      byte b;
      int i;
      for (b = 0, i = list.size(); b < i; b++) {
        ResourcesLoader resourcesLoader = list.get(b);
        if (!arraySet.contains(resourcesLoader))
          arrayList.add(resourcesLoader); 
      } 
      if (list.size() == arrayList.size())
        return; 
      this.mCallbacks.onLoadersChanged(this, arrayList);
      for (b = 0; b < paramVarArgs.length; b++)
        paramVarArgs[b].unregisterOnProvidersChangedCallback(this); 
      return;
    } 
  }
  
  public void clearLoaders() {
    synchronized (this.mUpdateLock) {
      checkCallbacksRegistered();
      List<?> list = Collections.emptyList();
      List<ResourcesLoader> list1 = this.mResourcesImpl.getAssets().getLoaders();
      this.mCallbacks.onLoadersChanged(this, (List)list);
      for (ResourcesLoader resourcesLoader : list1)
        resourcesLoader.unregisterOnProvidersChangedCallback(this); 
      return;
    } 
  }
  
  public OplusBaseResourcesImpl getOplusBaseResImpl() {
    return getImpl();
  }
  
  public static interface UpdateCallbacks extends ResourcesLoader.UpdateCallbacks {
    void onLoadersChanged(Resources param1Resources, List<ResourcesLoader> param1List);
  }
}
