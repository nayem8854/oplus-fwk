package android.content.res;

import com.oplus.theme.OplusThemeUtil;
import oplus.content.res.OplusExtraConfiguration;

public class OplusThemeResources {
  private final boolean debug = true;
  
  private boolean mIsHasWrapped = false;
  
  private OplusThemeZipFile mPackageZipFile = null;
  
  private OplusBaseResourcesImpl mResources = null;
  
  private OplusThemeResources mWrapped = null;
  
  public static final String SYSTEM_THEME_PATH = OplusThemeUtil.getDefaultThemePath();
  
  static {
    CUSTOM_THEME_PATH = OplusThemeUtil.CUSTOM_THEME_PATH;
    MetaData[] arrayOfMetaData = new MetaData[2];
    CUSTOM_THEME_META = new MetaData[2];
    arrayOfMetaData[0] = new MetaData(SYSTEM_THEME_PATH, true, true, true);
    DEFAULT_THEME_META[1] = new MetaData("/data/theme/", true, true, true);
    CUSTOM_THEME_META[0] = new MetaData(SYSTEM_THEME_PATH, true, true, true);
    CUSTOM_THEME_META[1] = new MetaData(CUSTOM_THEME_PATH, true, true, true);
    sThemeMetaPath = DEFAULT_THEME_META;
  }
  
  protected static final class MetaData {
    public boolean supportCharSequence = true;
    
    public boolean supportFile = true;
    
    public boolean supportInt = true;
    
    public String themePath = null;
    
    public MetaData(String param1String, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      this.themePath = param1String;
    }
  }
  
  private MetaData mMetaData = null;
  
  private String mPackageName = null;
  
  public static final MetaData[] CUSTOM_THEME_META;
  
  public static final String CUSTOM_THEME_PATH;
  
  public static final int CUSTON_FLAG = 256;
  
  public static final MetaData[] DEFAULT_THEME_META;
  
  public static final String FRAMEWORK_NAME = "framework-res";
  
  public static final String FRAMEWORK_PACKAGE = "android";
  
  public static final String ICONS_NAME = "icons";
  
  public static final String LAUNCHER_PACKAGE = "com.oppo.launcher";
  
  public static final String LOCKSCREEN_PACKAGE = "lockscreen";
  
  public static final String OPLUS_NAME = "oppo-framework-res";
  
  public static final String OPLUS_PACKAGE = "oplus";
  
  public static final String SYSTEMUI = "com.android.systemui";
  
  private static final String TAG = "OplusThemeResources";
  
  public static final String THIRD_THEME_PATH = "/data/theme/";
  
  private static OplusThemeResourcesSystem sSystem;
  
  public static MetaData[] sThemeMetaPath;
  
  private boolean mHasDrawable;
  
  private boolean mHasValue;
  
  public OplusThemeResources(OplusThemeResources paramOplusThemeResources, OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString, MetaData paramMetaData) {
    if (paramOplusThemeResources != null)
      if (paramOplusThemeResources.mPackageZipFile != null) {
        this.mWrapped = paramOplusThemeResources;
      } else {
        this.mWrapped = null;
      }  
    this.mMetaData = paramMetaData;
    this.mPackageName = paramString;
    this.mResources = paramOplusBaseResourcesImpl;
    checkUpdate();
  }
  
  public boolean hasValues() {
    return this.mHasValue;
  }
  
  public static OplusThemeResourcesSystem getSystem(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    if (sSystem == null)
      sSystem = OplusThemeResourcesSystem.getTopLevelThemeResources(paramOplusBaseResourcesImpl); 
    return sSystem;
  }
  
  public static OplusThemeResourcesSystem getSystem() {
    return sSystem;
  }
  
  public static OplusThemeResources getTopLevelThemeResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    setThemePath(paramOplusBaseResourcesImpl, paramString);
    OplusThemeResources oplusThemeResources = null;
    for (byte b = 0; b < sThemeMetaPath.length; b++)
      oplusThemeResources = new OplusThemeResources(oplusThemeResources, paramOplusBaseResourcesImpl, paramString, sThemeMetaPath[b]); 
    return oplusThemeResources;
  }
  
  public static void setThemePath(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    OplusExtraConfiguration oplusExtraConfiguration;
    if (paramOplusBaseResourcesImpl == null)
      return; 
    if ("framework-res".equals(paramString) || "oppo-framework-res".equals(paramString)) {
      oplusExtraConfiguration = paramOplusBaseResourcesImpl.getConfiguration().getOplusExtraConfiguration();
    } else {
      oplusExtraConfiguration = oplusExtraConfiguration.getSystemConfiguration().getOplusExtraConfiguration();
    } 
    if (oplusExtraConfiguration == null)
      return; 
    long l = oplusExtraConfiguration.mThemeChangedFlags;
    if ((l & 0x100L) == 256L) {
      sThemeMetaPath = CUSTOM_THEME_META;
    } else {
      sThemeMetaPath = DEFAULT_THEME_META;
    } 
  }
  
  public boolean checkUpdate() {
    // Byte code:
    //   0: aload_0
    //   1: aload_0
    //   2: getfield mResources : Landroid/content/res/OplusBaseResourcesImpl;
    //   5: aload_0
    //   6: getfield mPackageName : Ljava/lang/String;
    //   9: invokespecial checkMetaPath : (Landroid/content/res/OplusBaseResourcesImpl;Ljava/lang/String;)V
    //   12: aload_0
    //   13: getfield mResources : Landroid/content/res/OplusBaseResourcesImpl;
    //   16: astore_1
    //   17: aload_1
    //   18: aload_1
    //   19: invokevirtual typeCasting : (Landroid/content/res/OplusBaseResourcesImpl;)Landroid/content/res/ResourcesImpl;
    //   22: aload_0
    //   23: getfield mPackageName : Ljava/lang/String;
    //   26: invokestatic rejectTheme : (Landroid/content/res/ResourcesImpl;Ljava/lang/String;)Z
    //   29: istore_2
    //   30: iload_2
    //   31: ifne -> 56
    //   34: aload_0
    //   35: aload_0
    //   36: getfield mMetaData : Landroid/content/res/OplusThemeResources$MetaData;
    //   39: aload_0
    //   40: getfield mPackageName : Ljava/lang/String;
    //   43: aload_0
    //   44: getfield mResources : Landroid/content/res/OplusBaseResourcesImpl;
    //   47: invokestatic getThemeZipFile : (Landroid/content/res/OplusThemeResources$MetaData;Ljava/lang/String;Landroid/content/res/OplusBaseResourcesImpl;)Landroid/content/res/OplusThemeZipFile;
    //   50: putfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   53: goto -> 114
    //   56: aload_0
    //   57: getfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   60: ifnull -> 114
    //   63: ldc '/data/theme/'
    //   65: aload_0
    //   66: getfield mMetaData : Landroid/content/res/OplusThemeResources$MetaData;
    //   69: getfield themePath : Ljava/lang/String;
    //   72: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   75: ifne -> 98
    //   78: getstatic android/content/res/OplusThemeResources.CUSTOM_THEME_PATH : Ljava/lang/String;
    //   81: astore_3
    //   82: aload_0
    //   83: getfield mMetaData : Landroid/content/res/OplusThemeResources$MetaData;
    //   86: getfield themePath : Ljava/lang/String;
    //   89: astore_1
    //   90: aload_3
    //   91: aload_1
    //   92: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   95: ifeq -> 114
    //   98: iload_2
    //   99: ifeq -> 114
    //   102: aload_0
    //   103: getfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   106: invokevirtual clear : ()V
    //   109: aload_0
    //   110: aconst_null
    //   111: putfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   114: aload_0
    //   115: getfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   118: astore_1
    //   119: aload_1
    //   120: ifnull -> 138
    //   123: aload_1
    //   124: aload_0
    //   125: getfield mResources : Landroid/content/res/OplusBaseResourcesImpl;
    //   128: invokevirtual setResource : (Landroid/content/res/OplusBaseResourcesImpl;)V
    //   131: aload_0
    //   132: getfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   135: invokevirtual clear : ()V
    //   138: aload_0
    //   139: getfield mWrapped : Landroid/content/res/OplusThemeResources;
    //   142: astore_1
    //   143: iconst_1
    //   144: istore #4
    //   146: aload_1
    //   147: ifnull -> 156
    //   150: iconst_1
    //   151: istore #5
    //   153: goto -> 159
    //   156: iconst_0
    //   157: istore #5
    //   159: aload_0
    //   160: iload #5
    //   162: putfield mIsHasWrapped : Z
    //   165: iload #4
    //   167: istore_2
    //   168: aload_0
    //   169: getfield mPackageZipFile : Landroid/content/res/OplusThemeZipFile;
    //   172: ifnonnull -> 188
    //   175: iload #5
    //   177: ifeq -> 186
    //   180: iload #4
    //   182: istore_2
    //   183: goto -> 188
    //   186: iconst_0
    //   187: istore_2
    //   188: aload_0
    //   189: iload_2
    //   190: putfield mHasValue : Z
    //   193: aload_0
    //   194: iload_2
    //   195: putfield mHasDrawable : Z
    //   198: iload_2
    //   199: iload_2
    //   200: ior
    //   201: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #151	-> 0
    //   #152	-> 12
    //   #153	-> 30
    //   #154	-> 34
    //   #156	-> 56
    //   #157	-> 90
    //   #158	-> 102
    //   #159	-> 109
    //   #162	-> 114
    //   #163	-> 123
    //   #164	-> 131
    //   #167	-> 138
    //   #168	-> 165
    //   #169	-> 193
    //   #171	-> 198
  }
  
  private void checkMetaPath(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    if (paramOplusBaseResourcesImpl != null) {
      MetaData metaData = this.mMetaData;
      if (metaData != DEFAULT_THEME_META[0] && metaData != CUSTOM_THEME_META[0]) {
        OplusExtraConfiguration oplusExtraConfiguration;
        if ("framework-res".equals(paramString) || "oppo-framework-res".equals(paramString)) {
          oplusExtraConfiguration = paramOplusBaseResourcesImpl.getConfiguration().getOplusExtraConfiguration();
        } else {
          oplusExtraConfiguration = oplusExtraConfiguration.getSystemConfiguration().getOplusExtraConfiguration();
        } 
        if (oplusExtraConfiguration == null)
          return; 
        long l = oplusExtraConfiguration.mThemeChangedFlags;
        if ((l & 0x100L) == 256L) {
          this.mMetaData = CUSTOM_THEME_META[1];
        } else {
          this.mMetaData = DEFAULT_THEME_META[1];
        } 
        return;
      } 
    } 
  }
  
  private boolean hasValuesInner() {
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile == null || !oplusThemeZipFile.hasValues()) {
      if (this.mIsHasWrapped) {
        OplusThemeResources oplusThemeResources = this.mWrapped;
        if (oplusThemeResources.hasValuesInner())
          return true; 
      } 
      return false;
    } 
    return true;
  }
  
  protected boolean isMutiPackage() {
    return false;
  }
  
  public boolean containsEntry(String paramString) {
    boolean bool = false;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null) {
      boolean bool1 = oplusThemeZipFile.containsEntry(paramString);
      bool = bool1;
      if (!bool1) {
        bool = bool1;
        if (!this.mPackageZipFile.exists()) {
          OplusThemeResources oplusThemeResources = this.mWrapped;
          bool = bool1;
          if (oplusThemeResources != null)
            bool = oplusThemeResources.mPackageZipFile.containsEntry(paramString); 
        } 
      } 
    } 
    return bool;
  }
  
  public CharSequence getThemeCharSequence(int paramInt) {
    return getThemeCharSequenceInner(paramInt);
  }
  
  protected CharSequence getThemeCharSequenceInner(int paramInt) {
    CharSequence charSequence1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null) {
      checkAndInitZip(oplusThemeZipFile);
      charSequence1 = this.mPackageZipFile.getCharSequence(paramInt);
    } 
    CharSequence charSequence2 = charSequence1;
    if (charSequence1 == null) {
      charSequence2 = charSequence1;
      if (this.mIsHasWrapped) {
        OplusThemeResources oplusThemeResources = this.mWrapped;
        charSequence2 = charSequence1;
        if (oplusThemeResources != null) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          charSequence2 = this.mWrapped.mPackageZipFile.getCharSequence(paramInt);
        } 
      } 
    } 
    return charSequence2;
  }
  
  public OplusThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString) {
    return getThemeFileStream(paramString);
  }
  
  public OplusThemeZipFile.ThemeFileInfo getPackageThemeFileStream(int paramInt, String paramString) {
    return getPackageThemeFileStreamInner(paramInt, paramString);
  }
  
  public OplusThemeZipFile.ThemeFileInfo getThemeFileStream(String paramString) {
    return getThemeFileStreamInner(paramString);
  }
  
  public OplusThemeZipFile.ThemeFileInfo getThemeFileStream(String paramString, boolean paramBoolean) {
    return getThemeFileStreamInner(paramString, paramBoolean);
  }
  
  protected OplusThemeZipFile.ThemeFileInfo getThemeFileStreamInner(String paramString, boolean paramBoolean) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    OplusThemeZipFile.ThemeFileInfo themeFileInfo2 = themeFileInfo1;
    if (oplusThemeZipFile != null) {
      themeFileInfo2 = themeFileInfo1;
      if (!paramBoolean) {
        checkAndInitZip(oplusThemeZipFile);
        themeFileInfo2 = this.mPackageZipFile.getInputStream(paramString);
      } 
    } 
    OplusThemeResources oplusThemeResources = this.mWrapped;
    themeFileInfo1 = themeFileInfo2;
    if (oplusThemeResources != null) {
      themeFileInfo1 = themeFileInfo2;
      if (this.mIsHasWrapped) {
        themeFileInfo1 = themeFileInfo2;
        if (paramBoolean) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          themeFileInfo1 = this.mWrapped.mPackageZipFile.getInputStream(paramString);
        } 
      } 
    } 
    return themeFileInfo1;
  }
  
  protected OplusThemeZipFile.ThemeFileInfo getThemeFileStreamInner(String paramString) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null) {
      checkAndInitZip(oplusThemeZipFile);
      themeFileInfo1 = this.mPackageZipFile.getInputStream(paramString);
    } 
    OplusThemeResources oplusThemeResources = this.mWrapped;
    OplusThemeZipFile.ThemeFileInfo themeFileInfo2 = themeFileInfo1;
    if (oplusThemeResources != null) {
      themeFileInfo2 = themeFileInfo1;
      if (themeFileInfo1 == null) {
        themeFileInfo2 = themeFileInfo1;
        if (this.mIsHasWrapped) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          themeFileInfo2 = this.mWrapped.mPackageZipFile.getInputStream(paramString);
        } 
      } 
    } 
    return themeFileInfo2;
  }
  
  protected OplusThemeZipFile.ThemeFileInfo getPackageThemeFileStreamInner(int paramInt, String paramString) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null) {
      checkAndInitZip(oplusThemeZipFile);
      themeFileInfo1 = this.mPackageZipFile.getInputStream(paramInt, paramString);
    } 
    OplusThemeResources oplusThemeResources = this.mWrapped;
    OplusThemeZipFile.ThemeFileInfo themeFileInfo2 = themeFileInfo1;
    if (oplusThemeResources != null) {
      themeFileInfo2 = themeFileInfo1;
      if (themeFileInfo1 == null) {
        themeFileInfo2 = themeFileInfo1;
        if (this.mIsHasWrapped) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          themeFileInfo2 = this.mWrapped.mPackageZipFile.getInputStream(paramInt, paramString);
        } 
      } 
    } 
    return themeFileInfo2;
  }
  
  protected OplusThemeZipFile.ThemeFileInfo getIconFileStream(String paramString, boolean paramBoolean) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    OplusThemeZipFile.ThemeFileInfo themeFileInfo2 = themeFileInfo1;
    if (oplusThemeZipFile != null) {
      themeFileInfo2 = themeFileInfo1;
      if (!paramBoolean) {
        checkAndInitZip(oplusThemeZipFile);
        themeFileInfo2 = this.mPackageZipFile.getIconInputStream(paramString);
      } 
    } 
    OplusThemeResources oplusThemeResources = this.mWrapped;
    themeFileInfo1 = themeFileInfo2;
    if (oplusThemeResources != null) {
      themeFileInfo1 = themeFileInfo2;
      if (this.mIsHasWrapped) {
        themeFileInfo1 = themeFileInfo2;
        if (paramBoolean) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          themeFileInfo1 = this.mWrapped.mPackageZipFile.getIconInputStream(paramString);
        } 
      } 
    } 
    return themeFileInfo1;
  }
  
  public Integer getThemeInt(int paramInt) {
    return getThemeIntInner(paramInt);
  }
  
  protected Integer getThemeIntInner(int paramInt) {
    Integer integer1 = null;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null) {
      checkAndInitZip(oplusThemeZipFile);
      integer1 = this.mPackageZipFile.getInt(paramInt);
    } 
    Integer integer2 = integer1;
    if (integer1 == null) {
      integer2 = integer1;
      if (this.mIsHasWrapped) {
        OplusThemeResources oplusThemeResources = this.mWrapped;
        integer2 = integer1;
        if (oplusThemeResources != null) {
          checkAndInitZip(oplusThemeResources.mPackageZipFile);
          integer2 = this.mWrapped.mPackageZipFile.getInt(paramInt);
        } 
      } 
    } 
    return integer2;
  }
  
  public boolean hasDrawables() {
    return this.mHasDrawable;
  }
  
  protected boolean hasDrawableInner() {
    boolean bool1 = false;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null)
      bool1 = oplusThemeZipFile.hasZipDrawables(); 
    boolean bool2 = bool1;
    if (!bool1) {
      bool2 = bool1;
      if (this.mIsHasWrapped) {
        OplusThemeResources oplusThemeResources = this.mWrapped;
        bool2 = bool1;
        if (oplusThemeResources != null)
          bool2 = oplusThemeResources.mPackageZipFile.hasZipDrawables(); 
      } 
    } 
    return bool2;
  }
  
  public void setResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    this.mResources = paramOplusBaseResourcesImpl;
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null)
      oplusThemeZipFile.setResource(paramOplusBaseResourcesImpl); 
  }
  
  public boolean checkThemePackageExit() {
    OplusThemeZipFile oplusThemeZipFile = this.mPackageZipFile;
    if (oplusThemeZipFile != null)
      return oplusThemeZipFile.exists(); 
    return true;
  }
  
  private void checkAndInitZip(OplusThemeZipFile paramOplusThemeZipFile) {
    if (paramOplusThemeZipFile == null)
      return; 
    if (!paramOplusThemeZipFile.mHasInit)
      paramOplusThemeZipFile.initZipFile(); 
  }
}
