package android.content.res;

import android.graphics.drawable.Drawable;

class DrawableCache extends ThemedResourceCache<Drawable.ConstantState> {
  public Drawable getInstance(long paramLong, Resources paramResources, Resources.Theme paramTheme) {
    Drawable.ConstantState constantState = get(paramLong, paramTheme);
    if (constantState != null)
      return constantState.newDrawable(paramResources, paramTheme); 
    return null;
  }
  
  public boolean shouldInvalidateEntry(Drawable.ConstantState paramConstantState, int paramInt) {
    return Configuration.needNewResources(paramInt, paramConstantState.getChangingConfigurations());
  }
}
