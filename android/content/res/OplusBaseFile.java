package android.content.res;

import android.os.SystemProperties;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.util.XmlUtils;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.zip.ZipFile;
import oplus.util.OplusDisplayUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class OplusBaseFile {
  protected static final boolean DEBUG_THEME = SystemProperties.getBoolean("persist.sys.assert.panic", false);
  
  protected static ArrayList<String> sNightWhites = new ArrayList<>();
  
  static {
    sDensity = DisplayMetrics.DENSITY_DEVICE_STABLE;
    sCacheFiles = new ConcurrentHashMap<>();
    sNightWhites.add("icons");
    sNightWhites.add("lockscreen");
    sNightWhites.add("com.oppo.launcher");
    sDensities = OplusDisplayUtils.getBestDensityOrder(sDensity);
  }
  
  protected SparseArray mIntegers = null;
  
  protected SparseArray mCharSequences = null;
  
  protected ResourcesImpl mResources = null;
  
  protected OplusBaseResourcesImpl mBaseResources = null;
  
  protected String mPackageName = null;
  
  private boolean mSupportChar = false;
  
  private boolean mSupportFile = false;
  
  private boolean mSupportInt = false;
  
  protected static final String ACCESSIBLE = "accessible";
  
  private static final String ATTR_NAME = "name";
  
  private static final String ATTR_PACKAGE = "package";
  
  protected static final long FFFFFF8000000000 = -549755813888L;
  
  protected static final long FFFFFFFF80000000 = -2147483648L;
  
  private static final int INPUT_STREAM_CACHE_BYTE_COUNT = 8192;
  
  protected static final String NINE_SUFFIX = ".9.png";
  
  protected static final String OPLUS_XML = "colors.xml";
  
  protected static final String PATH_DIVIDER = "/";
  
  protected static final String PATH_SUFFIX = "#*.png";
  
  protected static final String PNG_SUFFIX = ".png";
  
  protected static final String TAG = "OplusBaseFile";
  
  protected static final String TAG_BOOLEAN = "bool";
  
  protected static final String TAG_COLOR = "color";
  
  protected static final String TAG_DIMEN = "dimen";
  
  protected static final String TAG_DRAWABLE = "drawable";
  
  protected static final String TAG_INTEGER = "integer";
  
  protected static final String TAG_STRING = "string";
  
  private static final String TRUE = "true";
  
  protected static final String XML_SUFFIX = ".xml";
  
  protected static Map<String, WeakReference<OplusBaseFile>> sCacheFiles;
  
  protected static int[] sDensities;
  
  protected static int sDensity;
  
  public OplusBaseFile(String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    this.mIntegers = new SparseArray();
    this.mCharSequences = new SparseArray();
    this.mPackageName = paramString;
    this.mSupportInt = paramBoolean1;
    this.mSupportChar = paramBoolean2;
    this.mSupportFile = paramBoolean3;
    this.mBaseResources = paramOplusBaseResourcesImpl;
    this.mResources = paramOplusBaseResourcesImpl.typeCasting(paramOplusBaseResourcesImpl);
  }
  
  public void setResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    this.mBaseResources = paramOplusBaseResourcesImpl;
    this.mResources = paramOplusBaseResourcesImpl.typeCasting(paramOplusBaseResourcesImpl);
  }
  
  public static boolean rejectTheme(ResourcesImpl paramResourcesImpl, String paramString) {
    boolean bool;
    if (isNightMode(paramResourcesImpl) && !sNightWhites.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static boolean isNightMode(ResourcesImpl paramResourcesImpl) {
    boolean bool = false;
    Configuration configuration = paramResourcesImpl.getConfiguration();
    if (configuration == null)
      return false; 
    if ((configuration.uiMode & 0x30) == 32)
      bool = true; 
    return bool;
  }
  
  protected static String getPackageName(String paramString) {
    if (!"framework-res".equals(paramString) && !"icons".equals(paramString)) {
      if ("oppo-framework-res".equals(paramString) || "lockscreen".equals(paramString)) {
        paramString = "oplus";
        return paramString;
      } 
      return paramString;
    } 
    paramString = "android";
    return paramString;
  }
  
  protected CharSequence getCharSequence(int paramInt) {
    return (CharSequence)this.mCharSequences.get(paramInt);
  }
  
  protected Integer getInt(int paramInt) {
    return (Integer)this.mIntegers.get(paramInt);
  }
  
  protected boolean hasValues() {
    boolean bool = false;
    if (this.mIntegers.size() > 0 || this.mCharSequences.size() > 0)
      bool = true; 
    return bool;
  }
  
  protected void clean(ZipFile paramZipFile) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: aload_1
    //   4: invokevirtual closeZipFile : (Ljava/util/zip/ZipFile;)V
    //   7: aload_0
    //   8: getfield mIntegers : Landroid/util/SparseArray;
    //   11: invokevirtual clear : ()V
    //   14: aload_0
    //   15: getfield mCharSequences : Landroid/util/SparseArray;
    //   18: invokevirtual clear : ()V
    //   21: getstatic android/content/res/OplusBaseFile.sCacheFiles : Ljava/util/Map;
    //   24: invokeinterface clear : ()V
    //   29: aload_0
    //   30: monitorexit
    //   31: return
    //   32: astore_1
    //   33: aload_0
    //   34: monitorexit
    //   35: aload_1
    //   36: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #172	-> 2
    //   #173	-> 7
    //   #174	-> 14
    //   #175	-> 21
    //   #176	-> 29
    //   #171	-> 32
    // Exception table:
    //   from	to	target	type
    //   2	7	32	finally
    //   7	14	32	finally
    //   14	21	32	finally
    //   21	29	32	finally
  }
  
  protected void closeZipFile(ZipFile paramZipFile) {
    /* monitor enter ThisExpression{ObjectType{android/content/res/OplusBaseFile}} */
    if (paramZipFile != null)
      try {
        paramZipFile.close();
      } catch (Exception exception) {
        if (DEBUG_THEME) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("OplusThemeZipFile Exception exception: ");
          stringBuilder.append(exception);
          Log.w("OplusBaseFile", stringBuilder.toString());
        } 
      } finally {} 
    /* monitor exit ThisExpression{ObjectType{android/content/res/OplusBaseFile}} */
  }
  
  protected void parseXmlStream(int paramInt, OplusThemeZipFile.ThemeFileInfo paramThemeFileInfo) {
    if (paramThemeFileInfo == null)
      return; 
    InputStream inputStream1 = null, inputStream2 = null;
    BufferedInputStream bufferedInputStream1 = null, bufferedInputStream2 = null;
    BufferedInputStream bufferedInputStream3 = bufferedInputStream2, bufferedInputStream4 = bufferedInputStream1;
    try {
      InputStream inputStream = paramThemeFileInfo.mInput;
      inputStream2 = inputStream;
      bufferedInputStream3 = bufferedInputStream2;
      inputStream1 = inputStream;
      bufferedInputStream4 = bufferedInputStream1;
      XmlPullParser xmlPullParser = XmlPullParserFactory.newInstance().newPullParser();
      inputStream2 = inputStream;
      bufferedInputStream3 = bufferedInputStream2;
      inputStream1 = inputStream;
      bufferedInputStream4 = bufferedInputStream1;
      BufferedInputStream bufferedInputStream = new BufferedInputStream();
      inputStream2 = inputStream;
      bufferedInputStream3 = bufferedInputStream2;
      inputStream1 = inputStream;
      bufferedInputStream4 = bufferedInputStream1;
      this(inputStream, 8192);
      inputStream2 = inputStream;
      bufferedInputStream3 = bufferedInputStream;
      inputStream1 = inputStream;
      bufferedInputStream4 = bufferedInputStream;
      xmlPullParser.setInput(bufferedInputStream, null);
      inputStream2 = inputStream;
      bufferedInputStream3 = bufferedInputStream;
      inputStream1 = inputStream;
      bufferedInputStream4 = bufferedInputStream;
      mergeThemeValues(paramInt, xmlPullParser);
      try {
        bufferedInputStream.close();
      } catch (IOException iOException) {
        if (DEBUG_THEME) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("bufferedinputstream IOException happened when loadThemeValues, msg = ");
          stringBuilder.append(iOException.toString());
          String str = stringBuilder.toString();
          Log.e("OplusBaseFile", str);
        } 
      } 
      if (inputStream != null)
        try {
          inputStream.close();
        } catch (IOException iOException) {
          StringBuilder stringBuilder;
          if (DEBUG_THEME) {
            stringBuilder = new StringBuilder();
          } else {
            return;
          } 
          stringBuilder.append("in IOException happened when loadThemeValues, msg = ");
          stringBuilder.append(iOException.toString());
          Log.e("OplusBaseFile", stringBuilder.toString());
        }  
    } catch (XmlPullParserException xmlPullParserException) {
      inputStream2 = inputStream1;
      bufferedInputStream3 = bufferedInputStream4;
      if (DEBUG_THEME) {
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        StringBuilder stringBuilder = new StringBuilder();
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        this();
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        stringBuilder.append("XmlPullParserException happened when loadThemeValues, msg = ");
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        stringBuilder.append(xmlPullParserException.toString());
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        String str = stringBuilder.toString();
        inputStream2 = inputStream1;
        bufferedInputStream3 = bufferedInputStream4;
        Log.e("OplusBaseFile", str);
      } 
      if (bufferedInputStream4 != null)
        try {
          bufferedInputStream4.close();
        } catch (IOException iOException) {
          if (DEBUG_THEME) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("bufferedinputstream IOException happened when loadThemeValues, msg = ");
            stringBuilder.append(iOException.toString());
            String str = stringBuilder.toString();
            Log.e("OplusBaseFile", str);
          } 
        }  
      if (inputStream1 != null)
        try {
          inputStream1.close();
        } catch (IOException iOException) {
          StringBuilder stringBuilder;
          if (DEBUG_THEME) {
            stringBuilder = new StringBuilder();
          } else {
            return;
          } 
          stringBuilder.append("in IOException happened when loadThemeValues, msg = ");
          stringBuilder.append(iOException.toString());
          Log.e("OplusBaseFile", stringBuilder.toString());
        }  
    } finally {}
  }
  
  private void mergeThemeValues(int paramInt, XmlPullParser paramXmlPullParser) {
    String str = null;
    CharSequence charSequence = null;
    try {
      int i = paramXmlPullParser.getEventType();
      while (i != 1) {
        String str1;
        CharSequence charSequence1;
        if (i != 2) {
          str1 = str;
          charSequence1 = charSequence;
        } else {
          String str2 = paramXmlPullParser.getName().trim();
          if (TextUtils.isEmpty(str2)) {
            str1 = str;
            charSequence1 = charSequence;
          } else {
            int j = paramXmlPullParser.getAttributeCount();
            str1 = str;
            charSequence1 = charSequence;
            if (j > 0) {
              for (i = 0; i < j; i++, charSequence = charSequence2) {
                CharSequence charSequence2;
                str1 = paramXmlPullParser.getAttributeName(i).trim();
                if (str1.equals("name")) {
                  charSequence2 = paramXmlPullParser.getAttributeValue(i);
                } else {
                  charSequence2 = charSequence;
                  if (str1.equals("package")) {
                    str = paramXmlPullParser.getAttributeValue(i);
                    charSequence2 = charSequence;
                  } 
                } 
              } 
              String str3 = paramXmlPullParser.nextText();
              str1 = str;
              charSequence1 = charSequence;
              if (!TextUtils.isEmpty(charSequence))
                if (TextUtils.isEmpty(str3)) {
                  str1 = str;
                  charSequence1 = charSequence;
                } else {
                  String str4 = str;
                  if (TextUtils.isEmpty(str))
                    if (paramInt == 0 || paramInt > 2) {
                      str4 = this.mPackageName;
                    } else if (paramInt == 1) {
                      str4 = "android";
                    } else {
                      str4 = str;
                      if (paramInt == 2)
                        str4 = "oplus"; 
                    }  
                  i = this.mResources.getIdentifier((String)charSequence, str2, str4);
                  if (i <= 0) {
                    str1 = str4;
                    charSequence1 = charSequence;
                  } else if (!str2.equals("bool")) {
                    if (str2.equals("color") || 
                      str2.equals("integer") || 
                      str2.equals("drawable")) {
                      str1 = str4;
                      charSequence1 = charSequence;
                      if (this.mSupportInt) {
                        j = this.mIntegers.indexOfKey(i);
                        str1 = str4;
                        charSequence1 = charSequence;
                        if (j < 0) {
                          try {
                            SparseArray sparseArray = this.mIntegers;
                            str1 = str3.trim();
                            j = XmlUtils.convertValueToUnsignedInt(str1, 0);
                            sparseArray.put(i, Integer.valueOf(j));
                          } catch (NumberFormatException numberFormatException) {
                            if (DEBUG_THEME) {
                              StringBuilder stringBuilder = new StringBuilder();
                              this();
                              stringBuilder.append("mergeThemeValues NumberFormatException happened when loadThemeValues, msg = ");
                              stringBuilder.append(numberFormatException.getMessage());
                              String str5 = stringBuilder.toString();
                              Log.e("OplusBaseFile", str5);
                            } 
                          } 
                          str1 = str4;
                          charSequence1 = charSequence;
                        } 
                      } 
                    } else if (str2.equals("string")) {
                      str1 = str4;
                      charSequence1 = charSequence;
                      if (this.mSupportChar) {
                        str1 = str4;
                        charSequence1 = charSequence;
                        if (this.mCharSequences.indexOfKey(i) < 0) {
                          this.mCharSequences.put(i, str3);
                          str1 = str4;
                          charSequence1 = charSequence;
                        } 
                      } 
                    } else {
                      str1 = str4;
                      charSequence1 = charSequence;
                      if (str2.equals("dimen")) {
                        str1 = str4;
                        charSequence1 = charSequence;
                        if (this.mSupportInt) {
                          SparseArray sparseArray = this.mIntegers;
                          str1 = str4;
                          charSequence1 = charSequence;
                          if (sparseArray.indexOfKey(i) < 0) {
                            ResourcesImpl resourcesImpl = this.mResources;
                            str1 = str3.toString();
                            Integer integer = parseDimension(resourcesImpl, str1);
                            if (integer != null)
                              this.mIntegers.put(i, integer); 
                            str1 = str4;
                            charSequence1 = charSequence;
                          } 
                        } 
                      } 
                    } 
                  } else {
                    str1 = str4;
                    charSequence1 = charSequence;
                    if (this.mSupportInt) {
                      str1 = str4;
                      charSequence1 = charSequence;
                      if (this.mIntegers.indexOfKey(i) < 0)
                        if ("true".equals(str3.trim())) {
                          this.mIntegers.put(i, Integer.valueOf(1));
                          str1 = str4;
                          charSequence1 = charSequence;
                        } else {
                          this.mIntegers.put(i, Integer.valueOf(0));
                          charSequence1 = charSequence;
                          str1 = str4;
                        }  
                    } 
                  } 
                }  
            } 
          } 
        } 
        i = paramXmlPullParser.next();
        str = str1;
        charSequence = charSequence1;
      } 
    } catch (XmlPullParserException xmlPullParserException) {
      if (DEBUG_THEME) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mergeThemeValues XmlPullParserException happened when loadThemeValues, msg = ");
        stringBuilder.append(xmlPullParserException.getMessage());
        String str1 = stringBuilder.toString();
        Log.e("OplusBaseFile", str1);
      } 
    } catch (IOException iOException) {
      if (DEBUG_THEME) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("mergeThemeValues IOException happened when loadThemeValues, msg = ");
        stringBuilder.append(iOException.getMessage());
        String str1 = stringBuilder.toString();
        Log.e("OplusBaseFile", str1);
      } 
    } 
  }
  
  private Integer parseDimension(ResourcesImpl paramResourcesImpl, String paramString) {
    byte b1 = -4;
    int i = -3;
    byte b2 = -2;
    byte b3 = 0;
    while (true) {
      if (b3 < paramString.length()) {
        char c = paramString.charAt(b3);
        byte b4 = b1;
        if (b1 == -4) {
          b4 = b1;
          if (c >= '0') {
            b4 = b1;
            if (c <= '9')
              b4 = b3; 
          } 
        } 
        int j = i;
        if (i == -3) {
          j = i;
          if (c == '.')
            j = b3; 
        } 
        byte b5 = b2;
        if (j != -3) {
          b5 = b2;
          if (c >= '0') {
            b5 = b2;
            if (c <= '9')
              b5 = b3; 
          } 
        } 
        if (c >= 'a' && c <= 'z') {
          i = j;
          b2 = b5;
          break;
        } 
        b3++;
        b1 = b4;
        i = j;
        b2 = b5;
        continue;
      } 
      b3 = -1;
      break;
    } 
    if (b3 == -1 || (i >= b2 && b2 != -2) || b2 >= b3)
      return null; 
    boolean bool = false;
    try {
      float f1 = Float.parseFloat(paramString.substring(0, b3));
      if (f1 < 0.0F)
        bool = true; 
      float f2 = f1;
      if (bool)
        f2 = -f1; 
      long l = (long)(8388608.0F * f2 + 0.5F);
      if ((0x7FFFFFL & l) == 0L) {
        b2 = 0;
        i = 23;
      } else if ((0xFFFFFFFFFF800000L & l) == 0L) {
        b2 = 3;
        i = 0;
      } else if ((0xFFFFFFFF80000000L & l) == 0L) {
        b2 = 2;
        i = 8;
      } else if ((0xFFFFFF8000000000L & l) == 0L) {
        b2 = 1;
        i = 16;
      } else {
        b2 = 0;
        i = 23;
      } 
      String str = paramString.substring(b3);
      if (str.equals("px")) {
        b3 = 0;
      } else if (str.equals("dp") || str.equals("dip")) {
        b3 = 1;
      } else if (str.equals("sp")) {
        b3 = 2;
      } else if (str.equals("pt")) {
        b3 = 3;
      } else if (str.equals("in")) {
        b3 = 4;
      } else if (str.equals("mm")) {
        b3 = 5;
      } else {
        return null;
      } 
      int j = (int)(l >> i & 0xFFFFFFL);
      i = j;
      if (bool)
        i = -j & 0xFFFFFF; 
      return Integer.valueOf(i << 8 | b2 << 4 | b3 << 0);
    } catch (NumberFormatException numberFormatException) {
      return null;
    } 
  }
}
