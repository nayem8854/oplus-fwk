package android.content.res;

public class ConfigurationBoundResourceCache<T> extends ThemedResourceCache<ConstantState<T>> {
  public T getInstance(long paramLong, Resources paramResources, Resources.Theme paramTheme) {
    ConstantState<T> constantState = (ConstantState)get(paramLong, paramTheme);
    if (constantState != null)
      return constantState.newInstance(paramResources, paramTheme); 
    return null;
  }
  
  public boolean shouldInvalidateEntry(ConstantState<T> paramConstantState, int paramInt) {
    return Configuration.needNewResources(paramInt, paramConstantState.getChangingConfigurations());
  }
}
