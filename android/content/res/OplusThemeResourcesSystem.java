package android.content.res;

import java.io.File;

public class OplusThemeResourcesSystem extends OplusThemeResources {
  private static final String TAG = "OplusThemeResourcesSystem";
  
  private static OplusThemeResources sOppo;
  
  private final boolean DEBUG = true;
  
  public OplusThemeResourcesSystem(OplusThemeResourcesSystem paramOplusThemeResourcesSystem, OplusBaseResourcesImpl paramOplusBaseResourcesImpl, OplusThemeResources.MetaData paramMetaData) {
    super(paramOplusThemeResourcesSystem, paramOplusBaseResourcesImpl, "framework-res", paramMetaData);
  }
  
  public static OplusThemeResourcesSystem getTopLevelThemeResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    sOppo = OplusThemeResources.getTopLevelThemeResources(paramOplusBaseResourcesImpl, "oppo-framework-res");
    OplusThemeResourcesSystem oplusThemeResourcesSystem = null;
    setThemePath(paramOplusBaseResourcesImpl, "framework-res");
    for (byte b = 0; b < sThemeMetaPath.length; b++)
      oplusThemeResourcesSystem = new OplusThemeResourcesSystem(oplusThemeResourcesSystem, paramOplusBaseResourcesImpl, sThemeMetaPath[b]); 
    return oplusThemeResourcesSystem;
  }
  
  public boolean checkUpdate() {
    sOppo.checkUpdate();
    return super.checkUpdate();
  }
  
  public CharSequence getThemeCharSequence(int paramInt) {
    CharSequence charSequence1 = null;
    OplusThemeResources oplusThemeResources = sOppo;
    if (oplusThemeResources != null)
      charSequence1 = oplusThemeResources.getThemeCharSequence(paramInt); 
    CharSequence charSequence2 = charSequence1;
    if (charSequence1 == null)
      charSequence2 = getThemeCharSequenceInner(paramInt); 
    return charSequence2;
  }
  
  private OplusThemeZipFile.ThemeFileInfo getThemeFileStreamSystem(String paramString1, String paramString2) {
    return getThemeFileStreamInner(paramString1);
  }
  
  private OplusThemeZipFile.ThemeFileInfo getThemeFileStreamOPPO(String paramString1, String paramString2) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo;
    paramString2 = null;
    OplusThemeResources oplusThemeResources = sOppo;
    if (oplusThemeResources != null)
      themeFileInfo = oplusThemeResources.getThemeFileStream(paramString1); 
    return themeFileInfo;
  }
  
  public OplusThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo;
    String str = null;
    if (paramString != null) {
      str = paramString.substring(paramString.lastIndexOf('/') + 1);
      if (2 == paramInt) {
        themeFileInfo = getThemeFileStreamOPPO(paramString, str);
      } else {
        themeFileInfo = getThemeFileStreamSystem(paramString, (String)themeFileInfo);
      } 
    } 
    return themeFileInfo;
  }
  
  public Integer getThemeInt(int paramInt1, int paramInt2) {
    Integer integer1 = null;
    Integer integer2 = integer1;
    if (paramInt2 == 2) {
      OplusThemeResources oplusThemeResources = sOppo;
      integer2 = integer1;
      if (oplusThemeResources != null)
        integer2 = oplusThemeResources.getThemeInt(paramInt1); 
    } 
    integer1 = integer2;
    if (integer2 == null)
      integer1 = getThemeIntInner(paramInt1); 
    return integer1;
  }
  
  public boolean hasValues() {
    if (super.hasValues() || sOppo.hasValues())
      return true; 
    return false;
  }
  
  public boolean hasDrawables() {
    return (super.hasDrawables() || sOppo.hasDrawables());
  }
  
  public File getLockscreenWallpaper() {
    return null;
  }
  
  public void setResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    sOppo.setResource(paramOplusBaseResourcesImpl);
    super.setResource(paramOplusBaseResourcesImpl);
  }
}
