package android.content.res;

import android.content.res.loader.ResourcesLoader;
import android.text.TextUtils;
import java.util.Arrays;
import java.util.Objects;

public final class ResourcesKey {
  public final CompatibilityInfo mCompatInfo;
  
  public final int mDisplayId;
  
  private final int mHash;
  
  public final String[] mLibDirs;
  
  public final ResourcesLoader[] mLoaders;
  
  public final String[] mOverlayDirs;
  
  public final Configuration mOverrideConfiguration;
  
  public final String mResDir;
  
  public final String[] mSplitResDirs;
  
  public ResourcesKey(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo, ResourcesLoader[] paramArrayOfResourcesLoader) {
    CompatibilityInfo compatibilityInfo;
    this.mResDir = paramString;
    this.mSplitResDirs = paramArrayOfString1;
    this.mOverlayDirs = paramArrayOfString2;
    this.mLibDirs = paramArrayOfString3;
    if (paramArrayOfResourcesLoader != null && paramArrayOfResourcesLoader.length == 0)
      paramArrayOfResourcesLoader = null; 
    this.mLoaders = paramArrayOfResourcesLoader;
    this.mDisplayId = paramInt;
    if (paramConfiguration == null)
      paramConfiguration = Configuration.EMPTY; 
    this.mOverrideConfiguration = new Configuration(paramConfiguration);
    if (paramCompatibilityInfo != null) {
      compatibilityInfo = paramCompatibilityInfo;
    } else {
      compatibilityInfo = CompatibilityInfo.DEFAULT_COMPATIBILITY_INFO;
    } 
    this.mCompatInfo = compatibilityInfo;
    paramInt = Objects.hashCode(this.mResDir);
    int i = Arrays.hashCode((Object[])this.mSplitResDirs);
    int j = Arrays.hashCode((Object[])this.mOverlayDirs);
    int k = Arrays.hashCode((Object[])this.mLibDirs);
    int m = this.mDisplayId;
    int n = Objects.hashCode(this.mOverrideConfiguration);
    int i1 = Objects.hashCode(this.mCompatInfo);
    int i2 = Arrays.hashCode((Object[])this.mLoaders);
    this.mHash = (((((((17 * 31 + paramInt) * 31 + i) * 31 + j) * 31 + k) * 31 + m) * 31 + n) * 31 + i1) * 31 + i2;
  }
  
  public ResourcesKey(String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2, String[] paramArrayOfString3, int paramInt, Configuration paramConfiguration, CompatibilityInfo paramCompatibilityInfo) {
    this(paramString, paramArrayOfString1, paramArrayOfString2, paramArrayOfString3, paramInt, paramConfiguration, paramCompatibilityInfo, null);
  }
  
  public boolean hasOverrideConfiguration() {
    return Configuration.EMPTY.equals(this.mOverrideConfiguration) ^ true;
  }
  
  public boolean isPathReferenced(String paramString) {
    String str = this.mResDir;
    boolean bool = true;
    if (str != null && str.startsWith(paramString))
      return true; 
    if (!anyStartsWith(this.mSplitResDirs, paramString) && !anyStartsWith(this.mOverlayDirs, paramString)) {
      String[] arrayOfString = this.mLibDirs;
      if (!anyStartsWith(arrayOfString, paramString))
        bool = false; 
    } 
    return bool;
  }
  
  private static boolean anyStartsWith(String[] paramArrayOfString, String paramString) {
    if (paramArrayOfString != null) {
      int i;
      byte b;
      for (i = paramArrayOfString.length, b = 0; b < i; ) {
        String str = paramArrayOfString[b];
        if (str != null && str.startsWith(paramString))
          return true; 
        b++;
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    return this.mHash;
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof ResourcesKey))
      return false; 
    paramObject = paramObject;
    if (this.mHash != ((ResourcesKey)paramObject).mHash)
      return false; 
    if (!Objects.equals(this.mResDir, ((ResourcesKey)paramObject).mResDir))
      return false; 
    if (!Arrays.equals((Object[])this.mSplitResDirs, (Object[])((ResourcesKey)paramObject).mSplitResDirs))
      return false; 
    if (!Arrays.equals((Object[])this.mOverlayDirs, (Object[])((ResourcesKey)paramObject).mOverlayDirs))
      return false; 
    if (!Arrays.equals((Object[])this.mLibDirs, (Object[])((ResourcesKey)paramObject).mLibDirs))
      return false; 
    if (this.mDisplayId != ((ResourcesKey)paramObject).mDisplayId)
      return false; 
    if (!Objects.equals(this.mOverrideConfiguration, ((ResourcesKey)paramObject).mOverrideConfiguration))
      return false; 
    if (!Objects.equals(this.mCompatInfo, ((ResourcesKey)paramObject).mCompatInfo))
      return false; 
    if (!Arrays.equals((Object[])this.mLoaders, (Object[])((ResourcesKey)paramObject).mLoaders))
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = (new StringBuilder()).append("ResourcesKey{");
    stringBuilder.append(" mHash=");
    stringBuilder.append(Integer.toHexString(this.mHash));
    stringBuilder.append(" mResDir=");
    stringBuilder.append(this.mResDir);
    stringBuilder.append(" mSplitDirs=[");
    String[] arrayOfString = this.mSplitResDirs;
    if (arrayOfString != null)
      stringBuilder.append(TextUtils.join(",", (Object[])arrayOfString)); 
    stringBuilder.append("]");
    stringBuilder.append(" mOverlayDirs=[");
    arrayOfString = this.mOverlayDirs;
    if (arrayOfString != null)
      stringBuilder.append(TextUtils.join(",", (Object[])arrayOfString)); 
    stringBuilder.append("]");
    stringBuilder.append(" mLibDirs=[");
    arrayOfString = this.mLibDirs;
    if (arrayOfString != null)
      stringBuilder.append(TextUtils.join(",", (Object[])arrayOfString)); 
    stringBuilder.append("]");
    stringBuilder.append(" mDisplayId=");
    stringBuilder.append(this.mDisplayId);
    stringBuilder.append(" mOverrideConfig=");
    stringBuilder.append(Configuration.resourceQualifierString(this.mOverrideConfiguration));
    stringBuilder.append(" mCompatInfo=");
    stringBuilder.append(this.mCompatInfo);
    stringBuilder.append(" mLoaders=[");
    ResourcesLoader[] arrayOfResourcesLoader = this.mLoaders;
    if (arrayOfResourcesLoader != null)
      stringBuilder.append(TextUtils.join(",", (Object[])arrayOfResourcesLoader)); 
    stringBuilder.append("]}");
    return stringBuilder.toString();
  }
}
