package android.content.res;

import android.app.ActivityThread;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseArray;
import com.android.internal.graphics.ColorUtils;
import java.util.Enumeration;
import java.util.Vector;
import oplus.content.res.OplusExtraConfiguration;

public class OplusMaterialFile extends OplusBaseFile {
  private static Vector<String> mColorList = new Vector<>();
  
  private boolean mMetaEnable = false;
  
  private boolean mLoadValue = false;
  
  private static final String BAR_DISABLED_COLOR = "BarDisabledColor";
  
  private static final String BLUE = "Blue";
  
  private static final String COLOR_TEXT = "colorText";
  
  private static final String COLOR_THEME_INDENTIFIER = "colorThemeIdentifier";
  
  private static final String GREEN = "Green";
  
  private static final String HIGH_LIGHT = "Highlight";
  
  private static final String INNER_DISBLED_COLOR = "InnerCircleDisabledColor";
  
  private static final String LIGHT_TINT_NORMAL = "TintLightNormal";
  
  private static final String LIGHT_TINT_PRESSED = "TintLightPressed";
  
  private static final String NXI_COLOR = "nxcolor";
  
  private static final String NX_COLOR = "NXcolor";
  
  protected static final int OPLUS_CUSTOM_FALG = 131072;
  
  protected static final String OPLUS_MATERIAL_ENABLE = "color_material_enable";
  
  protected static final int OPLUS_ONLINE_FALG = 1048576;
  
  private static final String ORANGE = "Orange";
  
  private static final String PURPLE = "Purple";
  
  private static final String RED = "Red";
  
  private static final String SKYBLUE = "Skyblue";
  
  private static final String SWITCH_CHECKED = "switchChecked";
  
  private static final String TINT_NORMAL = "TintControlNormal";
  
  private static final String TINT_PRESSED = "TintControlPressed";
  
  private static final String YELLOW = "Yellow";
  
  public OplusMaterialFile(String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    super(paramString, paramOplusBaseResourcesImpl, true, true, false);
    mColorList.add("Green");
    mColorList.add("Red");
    mColorList.add("Yellow");
    mColorList.add("Skyblue");
    mColorList.add("Blue");
    mColorList.add("Orange");
    mColorList.add("Purple");
    this.mMetaEnable = isColorMetaEnable(paramString);
  }
  
  public boolean isLoadValue() {
    return this.mLoadValue;
  }
  
  public boolean isMetaEnable() {
    return this.mMetaEnable;
  }
  
  protected static OplusMaterialFile getMaterialFile(String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    // Byte code:
    //   0: ldc android/content/res/OplusMaterialFile
    //   2: monitorenter
    //   3: aload_0
    //   4: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   7: ifne -> 88
    //   10: aload_1
    //   11: ifnonnull -> 17
    //   14: goto -> 88
    //   17: getstatic android/content/res/OplusMaterialFile.sCacheFiles : Ljava/util/Map;
    //   20: aload_0
    //   21: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   26: checkcast java/lang/ref/WeakReference
    //   29: astore_2
    //   30: aconst_null
    //   31: astore_3
    //   32: aload_2
    //   33: ifnull -> 44
    //   36: aload_2
    //   37: invokevirtual get : ()Ljava/lang/Object;
    //   40: checkcast android/content/res/OplusMaterialFile
    //   43: astore_3
    //   44: aload_3
    //   45: ifnull -> 53
    //   48: ldc android/content/res/OplusMaterialFile
    //   50: monitorexit
    //   51: aload_3
    //   52: areturn
    //   53: new android/content/res/OplusMaterialFile
    //   56: astore_3
    //   57: aload_3
    //   58: aload_0
    //   59: aload_1
    //   60: invokespecial <init> : (Ljava/lang/String;Landroid/content/res/OplusBaseResourcesImpl;)V
    //   63: new java/lang/ref/WeakReference
    //   66: astore_1
    //   67: aload_1
    //   68: aload_3
    //   69: invokespecial <init> : (Ljava/lang/Object;)V
    //   72: getstatic android/content/res/OplusMaterialFile.sCacheFiles : Ljava/util/Map;
    //   75: aload_0
    //   76: aload_1
    //   77: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   82: pop
    //   83: ldc android/content/res/OplusMaterialFile
    //   85: monitorexit
    //   86: aload_3
    //   87: areturn
    //   88: ldc android/content/res/OplusMaterialFile
    //   90: monitorexit
    //   91: aconst_null
    //   92: areturn
    //   93: astore_0
    //   94: ldc android/content/res/OplusMaterialFile
    //   96: monitorexit
    //   97: aload_0
    //   98: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #92	-> 3
    //   #95	-> 17
    //   #96	-> 30
    //   #97	-> 32
    //   #98	-> 36
    //   #100	-> 44
    //   #101	-> 48
    //   #103	-> 53
    //   #104	-> 63
    //   #105	-> 63
    //   #106	-> 72
    //   #108	-> 83
    //   #93	-> 88
    //   #91	-> 93
    // Exception table:
    //   from	to	target	type
    //   3	10	93	finally
    //   17	30	93	finally
    //   36	44	93	finally
    //   53	63	93	finally
    //   63	72	93	finally
    //   72	83	93	finally
  }
  
  protected void clears() {
    this.mIntegers.clear();
    this.mLoadValue = false;
  }
  
  protected void initMaterial() {
    String str1 = "switchChecked", str2 = "TintLightPressed", str3 = "TintLightNormal", str4 = "TintControlPressed", str5 = "TintControlNormal", str6 = "NXcolor";
    int i = 0;
    try {
      boolean bool = isNightMode(this.mBaseResources.typeCasting(this.mBaseResources));
      OplusExtraConfiguration oplusExtraConfiguration = this.mBaseResources.getSystemConfiguration().getOplusExtraConfiguration();
      if (oplusExtraConfiguration == null)
        return; 
      Long long_1 = Long.valueOf(oplusExtraConfiguration.mMaterialColor);
      if ((long_1.longValue() & 0x20000L) == 131072L) {
        Long long_ = Long.valueOf(long_1.longValue() >> 24L);
        i = long_.intValue() & 0xFFFFFFFF;
      } else {
        long_1.longValue();
      } 
      int j = calculatorColor(i, 0.05F);
      int k = calculatorAlpha(i, 0.15F);
      int m = calculatorColor(k, 0.05F);
      int n = calculatorAlpha(i, 0.3F);
      int i1 = calculatorColor(n, 0.05F);
      int i2 = calculatorAlpha(i, 0.3F);
      Enumeration<String> enumeration = mColorList.elements();
      Long long_2 = long_1;
      String str = str6;
      while (enumeration.hasMoreElements()) {
        str6 = enumeration.nextElement();
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("color");
        stringBuilder1.append(str6);
        stringBuilder1.append(str5);
        String str7 = stringBuilder1.toString();
        StringBuilder stringBuilder2 = new StringBuilder();
        this();
        stringBuilder2.append("color");
        stringBuilder2.append(str6);
        stringBuilder2.append(str4);
        String str8 = stringBuilder2.toString();
        StringBuilder stringBuilder3 = new StringBuilder();
        this();
        stringBuilder3.append("color");
        stringBuilder3.append(str6);
        stringBuilder3.append(str3);
        String str9 = stringBuilder3.toString();
        StringBuilder stringBuilder4 = new StringBuilder();
        this();
        stringBuilder4.append("color");
        stringBuilder4.append(str6);
        stringBuilder4.append(str2);
        String str10 = stringBuilder4.toString();
        StringBuilder stringBuilder5 = new StringBuilder();
        this();
        stringBuilder5.append("colorText");
        stringBuilder5.append(str6);
        stringBuilder5.append("Highlight");
        String str11 = stringBuilder5.toString();
        StringBuilder stringBuilder6 = new StringBuilder();
        this();
        stringBuilder6.append(str1);
        stringBuilder6.append(str6);
        stringBuilder6.append("BarDisabledColor");
        String str12 = stringBuilder6.toString();
        StringBuilder stringBuilder7 = new StringBuilder();
        this();
        stringBuilder7.append(str1);
        stringBuilder7.append(str6);
        stringBuilder7.append("InnerCircleDisabledColor");
        String str13 = stringBuilder7.toString();
        StringBuilder stringBuilder8 = new StringBuilder();
        this();
        stringBuilder8.append(str);
        stringBuilder8.append(str6);
        stringBuilder8.append(str5);
        String str14 = stringBuilder8.toString();
        StringBuilder stringBuilder9 = new StringBuilder();
        this();
        stringBuilder9.append(str);
        stringBuilder9.append(str6);
        stringBuilder9.append(str4);
        String str15 = stringBuilder9.toString();
        StringBuilder stringBuilder10 = new StringBuilder();
        this();
        stringBuilder10.append(str);
        stringBuilder10.append(str6);
        stringBuilder10.append(str3);
        String str16 = stringBuilder10.toString();
        StringBuilder stringBuilder11 = new StringBuilder();
        this();
        stringBuilder11.append(str);
        stringBuilder11.append(str6);
        stringBuilder11.append(str2);
        str6 = stringBuilder11.toString();
        int i3 = this.mResources.getIdentifier(str7, "color", this.mPackageName);
        int i4 = this.mResources.getIdentifier(str8, "color", this.mPackageName);
        int i5 = this.mResources.getIdentifier(str9, "color", this.mPackageName);
        int i6 = this.mResources.getIdentifier(str10, "color", this.mPackageName);
        int i7 = this.mResources.getIdentifier(str11, "color", this.mPackageName);
        int i8 = this.mResources.getIdentifier(str12, "color", this.mPackageName);
        int i9 = this.mResources.getIdentifier(str13, "color", this.mPackageName);
        int i10 = this.mResources.getIdentifier(str14, "color", this.mPackageName);
        int i11 = this.mResources.getIdentifier(str15, "color", this.mPackageName);
        int i12 = this.mResources.getIdentifier(str16, "color", this.mPackageName);
        int i13 = this.mResources.getIdentifier(str6, "color", this.mPackageName);
        if (i3 > 0 && this.mIntegers.indexOfKey(i3) < 0)
          this.mIntegers.put(i3, Integer.valueOf(i)); 
        if (i4 > 0 && this.mIntegers.indexOfKey(i4) < 0)
          this.mIntegers.put(i4, Integer.valueOf(j)); 
        if (i5 > 0 && this.mIntegers.indexOfKey(i5) < 0) {
          SparseArray sparseArray = this.mIntegers;
          if (bool) {
            i3 = k;
          } else {
            i3 = n;
          } 
          sparseArray.put(i5, Integer.valueOf(i3));
        } 
        if (i6 > 0 && this.mIntegers.indexOfKey(i6) < 0) {
          SparseArray sparseArray = this.mIntegers;
          if (bool) {
            i3 = m;
          } else {
            i3 = i1;
          } 
          sparseArray.put(i6, Integer.valueOf(i3));
        } 
        if (i7 > 0 && this.mIntegers.indexOfKey(i7) < 0)
          this.mIntegers.put(i7, Integer.valueOf(i2)); 
        if (i8 > 0 && this.mIntegers.indexOfKey(i8) < 0)
          this.mIntegers.put(i8, Integer.valueOf(i2)); 
        if (i9 > 0 && this.mIntegers.indexOfKey(i9) < 0)
          this.mIntegers.put(i9, Integer.valueOf(i2)); 
        if (i10 > 0 && this.mIntegers.indexOfKey(i10) < 0)
          this.mIntegers.put(i10, Integer.valueOf(i)); 
        if (i11 > 0 && this.mIntegers.indexOfKey(i11) < 0)
          this.mIntegers.put(i11, Integer.valueOf(j)); 
        if (i12 > 0 && this.mIntegers.indexOfKey(i12) < 0) {
          SparseArray sparseArray = this.mIntegers;
          if (bool) {
            i3 = m;
          } else {
            i3 = i1;
          } 
          sparseArray.put(i12, Integer.valueOf(i3));
        } 
        if (i13 > 0 && this.mIntegers.indexOfKey(i13) < 0) {
          SparseArray sparseArray = this.mIntegers;
          if (bool) {
            i3 = m;
          } else {
            i3 = i1;
          } 
          sparseArray.put(i13, Integer.valueOf(i3));
        } 
      } 
      if (this.mIntegers.size() > 0)
        this.mLoadValue = true; 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getInnerColor e: ");
      stringBuilder.append(exception.toString());
      Log.e("OplusBaseFile", stringBuilder.toString());
    } 
  }
  
  private int calculatorColor(int paramInt, float paramFloat) {
    float[] arrayOfFloat = new float[3];
    ColorUtils.colorToHSL(paramInt, arrayOfFloat);
    if (paramFloat < 0.1F)
      paramFloat = arrayOfFloat[2] - paramFloat; 
    arrayOfFloat[2] = paramFloat;
    return ColorUtils.HSLToColor(arrayOfFloat);
  }
  
  private int calculatorAlpha(int paramInt, float paramFloat) {
    int i = Color.red(paramInt);
    int j = Color.green(paramInt);
    paramInt = Color.blue(paramInt);
    return Color.argb(paramFloat, i, j, paramInt);
  }
  
  private boolean isColorMetaEnable(String paramString) {
    boolean bool2, bool1 = false;
    boolean bool = false;
    try {
      int i;
      OplusExtraConfiguration oplusExtraConfiguration = this.mBaseResources.getSystemConfiguration().getOplusExtraConfiguration();
      if (oplusExtraConfiguration == null) {
        i = 0;
      } else {
        i = oplusExtraConfiguration.mUserId;
      } 
      ApplicationInfo applicationInfo = ActivityThread.getPackageManager().getApplicationInfo(paramString, 128, i);
      bool2 = bool;
      if (applicationInfo != null) {
        bool2 = bool;
        if (applicationInfo.metaData != null)
          bool2 = applicationInfo.metaData.getBoolean("color_material_enable"); 
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("isColorMetaEnable exception: ");
      stringBuilder.append(exception.toString());
      Log.e("OplusBaseFile", stringBuilder.toString());
      bool2 = bool1;
    } 
    return bool2;
  }
}
