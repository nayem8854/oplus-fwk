package android.content.res.loader;

import android.content.res.ApkAssets;
import android.util.ArrayMap;
import android.util.ArraySet;
import com.android.internal.util.ArrayUtils;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ResourcesLoader {
  private ResourcesProvider[] mProviders;
  
  private ResourcesProvider[] mPreviousProviders;
  
  private final Object mLock = new Object();
  
  private ArrayMap<WeakReference<Object>, UpdateCallbacks> mChangeCallbacks = new ArrayMap();
  
  private ApkAssets[] mApkAssets;
  
  public List<ResourcesProvider> getProviders() {
    synchronized (this.mLock) {
      List<ResourcesProvider> list;
      if (this.mProviders == null) {
        list = Collections.emptyList();
      } else {
        list = Arrays.asList(this.mProviders);
      } 
      return list;
    } 
  }
  
  public void addProvider(ResourcesProvider paramResourcesProvider) {
    synchronized (this.mLock) {
      this.mProviders = (ResourcesProvider[])ArrayUtils.appendElement(ResourcesProvider.class, (Object[])this.mProviders, paramResourcesProvider);
      notifyProvidersChangedLocked();
      return;
    } 
  }
  
  public void removeProvider(ResourcesProvider paramResourcesProvider) {
    synchronized (this.mLock) {
      this.mProviders = (ResourcesProvider[])ArrayUtils.removeElement(ResourcesProvider.class, (Object[])this.mProviders, paramResourcesProvider);
      notifyProvidersChangedLocked();
      return;
    } 
  }
  
  public void setProviders(List<ResourcesProvider> paramList) {
    synchronized (this.mLock) {
      this.mProviders = paramList.<ResourcesProvider>toArray(new ResourcesProvider[0]);
      notifyProvidersChangedLocked();
      return;
    } 
  }
  
  public void clearProviders() {
    synchronized (this.mLock) {
      this.mProviders = null;
      notifyProvidersChangedLocked();
      return;
    } 
  }
  
  public List<ApkAssets> getApkAssets() {
    synchronized (this.mLock) {
      if (this.mApkAssets == null)
        return (List)Collections.emptyList(); 
      return Arrays.asList(this.mApkAssets);
    } 
  }
  
  public void registerOnProvidersChangedCallback(Object paramObject, UpdateCallbacks paramUpdateCallbacks) {
    synchronized (this.mLock) {
      ArrayMap<WeakReference<Object>, UpdateCallbacks> arrayMap = this.mChangeCallbacks;
      WeakReference weakReference = new WeakReference();
      this((T)paramObject);
      arrayMap.put(weakReference, paramUpdateCallbacks);
      return;
    } 
  }
  
  public void unregisterOnProvidersChangedCallback(Object paramObject) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: iconst_0
    //   8: istore_3
    //   9: aload_0
    //   10: getfield mChangeCallbacks : Landroid/util/ArrayMap;
    //   13: invokevirtual size : ()I
    //   16: istore #4
    //   18: iload_3
    //   19: iload #4
    //   21: if_icmpge -> 64
    //   24: aload_0
    //   25: getfield mChangeCallbacks : Landroid/util/ArrayMap;
    //   28: iload_3
    //   29: invokevirtual keyAt : (I)Ljava/lang/Object;
    //   32: checkcast java/lang/ref/WeakReference
    //   35: astore #5
    //   37: aload_1
    //   38: aload #5
    //   40: invokevirtual get : ()Ljava/lang/Object;
    //   43: if_acmpne -> 58
    //   46: aload_0
    //   47: getfield mChangeCallbacks : Landroid/util/ArrayMap;
    //   50: iload_3
    //   51: invokevirtual removeAt : (I)Ljava/lang/Object;
    //   54: pop
    //   55: aload_2
    //   56: monitorexit
    //   57: return
    //   58: iinc #3, 1
    //   61: goto -> 18
    //   64: aload_2
    //   65: monitorexit
    //   66: return
    //   67: astore_1
    //   68: aload_2
    //   69: monitorexit
    //   70: aload_1
    //   71: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #191	-> 0
    //   #192	-> 7
    //   #193	-> 24
    //   #194	-> 37
    //   #195	-> 46
    //   #196	-> 55
    //   #192	-> 58
    //   #199	-> 64
    //   #200	-> 66
    //   #199	-> 67
    // Exception table:
    //   from	to	target	type
    //   9	18	67	finally
    //   24	37	67	finally
    //   37	46	67	finally
    //   46	55	67	finally
    //   55	57	67	finally
    //   64	66	67	finally
    //   68	70	67	finally
  }
  
  private static boolean arrayEquals(ResourcesProvider[] paramArrayOfResourcesProvider1, ResourcesProvider[] paramArrayOfResourcesProvider2) {
    if (paramArrayOfResourcesProvider1 == paramArrayOfResourcesProvider2)
      return true; 
    if (paramArrayOfResourcesProvider1 == null || paramArrayOfResourcesProvider2 == null)
      return false; 
    if (paramArrayOfResourcesProvider1.length != paramArrayOfResourcesProvider2.length)
      return false; 
    byte b;
    int i;
    for (b = 0, i = paramArrayOfResourcesProvider1.length; b < i; b++) {
      if (paramArrayOfResourcesProvider1[b] != paramArrayOfResourcesProvider2[b])
        return false; 
    } 
    return true;
  }
  
  private void notifyProvidersChangedLocked() {
    ArraySet arraySet = new ArraySet();
    if (arrayEquals(this.mPreviousProviders, this.mProviders))
      return; 
    ResourcesProvider[] arrayOfResourcesProvider1 = this.mProviders;
    if (arrayOfResourcesProvider1 == null || arrayOfResourcesProvider1.length == 0) {
      this.mApkAssets = null;
    } else {
      this.mApkAssets = new ApkAssets[arrayOfResourcesProvider1.length];
      byte b;
      int k;
      for (b = 0, k = arrayOfResourcesProvider1.length; b < k; b++) {
        this.mProviders[b].incrementRefCount();
        this.mApkAssets[b] = this.mProviders[b].getApkAssets();
      } 
    } 
    ResourcesProvider[] arrayOfResourcesProvider2 = this.mPreviousProviders;
    if (arrayOfResourcesProvider2 != null) {
      byte b;
      int k;
      for (k = arrayOfResourcesProvider2.length, b = 0; b < k; ) {
        ResourcesProvider resourcesProvider = arrayOfResourcesProvider2[b];
        resourcesProvider.decrementRefCount();
        b++;
      } 
    } 
    this.mPreviousProviders = this.mProviders;
    int i;
    for (i = this.mChangeCallbacks.size() - 1; i >= 0; i--) {
      WeakReference weakReference = (WeakReference)this.mChangeCallbacks.keyAt(i);
      if (weakReference.get() == null) {
        this.mChangeCallbacks.removeAt(i);
      } else {
        arraySet.add(this.mChangeCallbacks.valueAt(i));
      } 
    } 
    int j;
    for (i = 0, j = arraySet.size(); i < j; i++)
      ((UpdateCallbacks)arraySet.valueAt(i)).onLoaderUpdated(this); 
  }
  
  public static interface UpdateCallbacks {
    void onLoaderUpdated(ResourcesLoader param1ResourcesLoader);
  }
}
