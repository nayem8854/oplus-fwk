package android.content.res;

import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipFile;
import oplus.util.OplusDisplayUtils;

public class OplusAccessibleFile extends OplusBaseFile {
  private static final String[] ASSET_FILES = new String[] { "accessible/res/values", "accessible/res/drawable" };
  
  private static final int INDEX_COLORS = 0;
  
  private static final int INDEX_XHDPI = 1;
  
  private static final int INDEX_XXHDPI = 2;
  
  public OplusAccessibleFile(String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    super(paramString, paramOplusBaseResourcesImpl, true, true, true);
  }
  
  public boolean initValue() {
    clearCache((ZipFile)null);
    loadAssetValues(0);
    return true;
  }
  
  public void clearCache(ZipFile paramZipFile) {
    clean(paramZipFile);
  }
  
  protected static OplusAccessibleFile getAssetFile(String paramString, OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    // Byte code:
    //   0: ldc android/content/res/OplusAccessibleFile
    //   2: monitorenter
    //   3: aload_0
    //   4: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   7: ifne -> 155
    //   10: aload_1
    //   11: ifnonnull -> 17
    //   14: goto -> 155
    //   17: aload_0
    //   18: invokestatic getPackageName : (Ljava/lang/String;)Ljava/lang/String;
    //   21: astore_2
    //   22: new java/lang/StringBuilder
    //   25: astore_0
    //   26: aload_0
    //   27: invokespecial <init> : ()V
    //   30: aload_0
    //   31: aload_2
    //   32: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload_0
    //   37: ldc ':/'
    //   39: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload_0
    //   44: ldc 'accessible'
    //   46: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   49: pop
    //   50: aload_0
    //   51: invokevirtual toString : ()Ljava/lang/String;
    //   54: astore_3
    //   55: aconst_null
    //   56: astore #4
    //   58: aload #4
    //   60: astore_0
    //   61: getstatic android/content/res/OplusAccessibleFile.sCacheFiles : Ljava/util/Map;
    //   64: aload_3
    //   65: invokeinterface containsKey : (Ljava/lang/Object;)Z
    //   70: ifeq -> 104
    //   73: getstatic android/content/res/OplusAccessibleFile.sCacheFiles : Ljava/util/Map;
    //   76: aload_3
    //   77: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
    //   82: checkcast java/lang/ref/WeakReference
    //   85: astore #5
    //   87: aload #4
    //   89: astore_0
    //   90: aload #5
    //   92: ifnull -> 104
    //   95: aload #5
    //   97: invokevirtual get : ()Ljava/lang/Object;
    //   100: checkcast android/content/res/OplusAccessibleFile
    //   103: astore_0
    //   104: aload_0
    //   105: ifnull -> 116
    //   108: aload_0
    //   109: aload_1
    //   110: invokevirtual setResource : (Landroid/content/res/OplusBaseResourcesImpl;)V
    //   113: goto -> 150
    //   116: new android/content/res/OplusAccessibleFile
    //   119: astore_0
    //   120: aload_0
    //   121: aload_2
    //   122: aload_1
    //   123: invokespecial <init> : (Ljava/lang/String;Landroid/content/res/OplusBaseResourcesImpl;)V
    //   126: getstatic android/content/res/OplusAccessibleFile.sCacheFiles : Ljava/util/Map;
    //   129: astore #4
    //   131: new java/lang/ref/WeakReference
    //   134: astore_1
    //   135: aload_1
    //   136: aload_0
    //   137: invokespecial <init> : (Ljava/lang/Object;)V
    //   140: aload #4
    //   142: aload_3
    //   143: aload_1
    //   144: invokeinterface put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   149: pop
    //   150: ldc android/content/res/OplusAccessibleFile
    //   152: monitorexit
    //   153: aload_0
    //   154: areturn
    //   155: ldc android/content/res/OplusAccessibleFile
    //   157: monitorexit
    //   158: aconst_null
    //   159: areturn
    //   160: astore_0
    //   161: ldc android/content/res/OplusAccessibleFile
    //   163: monitorexit
    //   164: aload_0
    //   165: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #67	-> 3
    //   #70	-> 17
    //   #71	-> 22
    //   #72	-> 55
    //   #73	-> 58
    //   #74	-> 73
    //   #75	-> 87
    //   #76	-> 95
    //   #79	-> 104
    //   #80	-> 108
    //   #82	-> 116
    //   #83	-> 126
    //   #85	-> 150
    //   #68	-> 155
    //   #66	-> 160
    // Exception table:
    //   from	to	target	type
    //   3	10	160	finally
    //   17	22	160	finally
    //   22	55	160	finally
    //   61	73	160	finally
    //   73	87	160	finally
    //   95	104	160	finally
    //   108	113	160	finally
    //   116	126	160	finally
    //   126	150	160	finally
  }
  
  protected OplusThemeZipFile.ThemeFileInfo getAssetInputStream(int paramInt, String paramString) {
    OplusThemeZipFile.ThemeFileInfo themeFileInfo;
    InputStream inputStream2 = null;
    if (TextUtils.isEmpty(paramString) || paramString.endsWith(".xml"))
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("accessible");
    stringBuilder.append("/");
    paramInt = sDensity;
    stringBuilder.append(OplusDisplayUtils.getDrawbleDensityFolder(paramInt));
    stringBuilder.append(paramString.substring(paramString.lastIndexOf("/")));
    String str = stringBuilder.toString();
    InputStream inputStream1 = getAssetPathStream(this.mResources.getAssets(), str);
    if (inputStream1 != null) {
      themeFileInfo = new OplusThemeZipFile.ThemeFileInfo(inputStream1, 0L);
    } else {
      paramInt = 0;
      while (true) {
        inputStream1 = inputStream2;
        if (paramInt < sDensities.length) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("accessible");
          stringBuilder1.append("/");
          int i = sDensities[paramInt];
          stringBuilder1.append(OplusDisplayUtils.getDrawbleDensityFolder(i));
          stringBuilder1.append(str.substring(str.lastIndexOf("/")));
          String str1 = stringBuilder1.toString();
          if (!str.equalsIgnoreCase(str1)) {
            InputStream inputStream = getAssetPathStream(this.mResources.getAssets(), str1);
            if (inputStream != null) {
              OplusThemeZipFile.ThemeFileInfo themeFileInfo1 = new OplusThemeZipFile.ThemeFileInfo(inputStream, 0L);
              themeFileInfo = themeFileInfo1;
              if (sDensities[paramInt] > 1) {
                themeFileInfo1.mDensity = sDensities[paramInt];
                themeFileInfo = themeFileInfo1;
              } 
              break;
            } 
          } 
          paramInt++;
          continue;
        } 
        break;
      } 
    } 
    return themeFileInfo;
  }
  
  protected boolean hasDrawables() {
    boolean bool3;
    AssetManager assetManager = this.mResources.getAssets();
    boolean bool1 = false, bool2 = false;
    if (assetManager == null)
      return false; 
    byte b = 0;
    while (true) {
      bool3 = bool2;
      try {
        if (b < sDensities.length) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("accessible");
          stringBuilder.append("/");
          int i = sDensities[b];
          stringBuilder.append(OplusDisplayUtils.getDrawbleDensityFolder(i));
          String str = stringBuilder.toString();
          String[] arrayOfString = assetManager.list(str);
          i = arrayOfString.length;
          if (i > 0) {
            bool3 = true;
            break;
          } 
          b++;
          continue;
        } 
        break;
      } catch (IOException iOException) {
        bool3 = bool1;
        if (DEBUG_THEME) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("hasAssetDrawables: asset list exception ");
          stringBuilder.append(iOException.toString());
          Log.e("OplusBaseFile", stringBuilder.toString());
          bool3 = bool1;
        } 
      } 
      return bool3;
    } 
    return bool3;
  }
  
  public boolean hasAssetValues() {
    AssetManager assetManager = this.mResources.getAssets();
    if (assetManager == null)
      return false; 
    try {
      String[] arrayOfString = assetManager.list(ASSET_FILES[0]);
      if (arrayOfString != null) {
        int i = arrayOfString.length;
        if (i > 0)
          return true; 
      } 
    } catch (IOException iOException) {
      if (DEBUG_THEME) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("hasAssetValues: asset list exception ");
        stringBuilder.append(iOException.toString());
        Log.e("OplusBaseFile", stringBuilder.toString());
      } 
    } 
    return false;
  }
  
  private void loadAssetValues(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(ASSET_FILES[paramInt]);
    stringBuilder.append("/");
    stringBuilder.append("colors.xml");
    String str = stringBuilder.toString();
    InputStream inputStream = getAssetPathStream(this.mResources.getAssets(), str);
    OplusThemeZipFile.ThemeFileInfo themeFileInfo = new OplusThemeZipFile.ThemeFileInfo(inputStream, paramInt);
    parseXmlStream(paramInt, themeFileInfo);
  }
  
  private InputStream getAssetPathStream(AssetManager paramAssetManager, String paramString) {
    if (paramAssetManager == null)
      return null; 
    IOException iOException2 = null;
    try {
      InputStream inputStream = paramAssetManager.open(paramString);
    } catch (IOException iOException1) {
      iOException1 = iOException2;
    } 
    return (InputStream)iOException1;
  }
}
