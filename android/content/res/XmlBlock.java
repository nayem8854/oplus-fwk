package android.content.res;

import android.util.TypedValue;
import com.android.internal.util.XmlUtils;
import dalvik.annotation.optimization.FastNative;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import org.xmlpull.v1.XmlPullParserException;

final class XmlBlock implements AutoCloseable {
  private static final boolean DEBUG = false;
  
  private final AssetManager mAssets;
  
  private long mNative;
  
  private boolean mOpen;
  
  private int mOpenCount;
  
  final StringBlock mStrings;
  
  public XmlBlock(byte[] paramArrayOfbyte) {
    this.mOpen = true;
    this.mOpenCount = 1;
    this.mAssets = null;
    long l = nativeCreate(paramArrayOfbyte, 0, paramArrayOfbyte.length);
    this.mStrings = new StringBlock(nativeGetStringBlock(l), false);
  }
  
  public XmlBlock(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    this.mOpen = true;
    this.mOpenCount = 1;
    this.mAssets = null;
    long l = nativeCreate(paramArrayOfbyte, paramInt1, paramInt2);
    this.mStrings = new StringBlock(nativeGetStringBlock(l), false);
  }
  
  public void close() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mOpen : Z
    //   6: ifeq -> 18
    //   9: aload_0
    //   10: iconst_0
    //   11: putfield mOpen : Z
    //   14: aload_0
    //   15: invokespecial decOpenCountLocked : ()V
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #59	-> 0
    //   #60	-> 2
    //   #61	-> 9
    //   #62	-> 14
    //   #64	-> 18
    //   #65	-> 20
    //   #64	-> 21
    // Exception table:
    //   from	to	target	type
    //   2	9	21	finally
    //   9	14	21	finally
    //   14	18	21	finally
    //   18	20	21	finally
    //   22	24	21	finally
  }
  
  private void decOpenCountLocked() {
    int i = this.mOpenCount - 1;
    if (i == 0) {
      nativeDestroy(this.mNative);
      this.mNative = 0L;
      AssetManager assetManager = this.mAssets;
      if (assetManager != null)
        assetManager.xmlBlockGone(hashCode()); 
    } 
  }
  
  public XmlResourceParser newParser() {
    return newParser(0);
  }
  
  public XmlResourceParser newParser(int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mNative : J
    //   6: lconst_0
    //   7: lcmp
    //   8: ifeq -> 33
    //   11: new android/content/res/XmlBlock$Parser
    //   14: astore_2
    //   15: aload_2
    //   16: aload_0
    //   17: aload_0
    //   18: getfield mNative : J
    //   21: iload_1
    //   22: invokestatic nativeCreateParseState : (JI)J
    //   25: aload_0
    //   26: invokespecial <init> : (Landroid/content/res/XmlBlock;JLandroid/content/res/XmlBlock;)V
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_2
    //   32: areturn
    //   33: aload_0
    //   34: monitorexit
    //   35: aconst_null
    //   36: areturn
    //   37: astore_2
    //   38: aload_0
    //   39: monitorexit
    //   40: aload_2
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #87	-> 0
    //   #88	-> 2
    //   #89	-> 11
    //   #91	-> 33
    //   #92	-> 37
    // Exception table:
    //   from	to	target	type
    //   2	11	37	finally
    //   11	31	37	finally
    //   33	35	37	finally
    //   38	40	37	finally
  }
  
  class Parser implements XmlResourceParser {
    private final XmlBlock mBlock;
    
    private boolean mDecNextDepth;
    
    private int mDepth;
    
    private int mEventType;
    
    long mParseState;
    
    private boolean mStarted;
    
    final XmlBlock this$0;
    
    Parser(long param1Long, XmlBlock param1XmlBlock1) {
      this.mStarted = false;
      this.mDecNextDepth = false;
      this.mDepth = 0;
      this.mEventType = 0;
      this.mParseState = param1Long;
      this.mBlock = param1XmlBlock1;
      XmlBlock.access$008(param1XmlBlock1);
    }
    
    public int getSourceResId() {
      return XmlBlock.nativeGetSourceResId(this.mParseState);
    }
    
    public void setFeature(String param1String, boolean param1Boolean) throws XmlPullParserException {
      if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(param1String) && param1Boolean)
        return; 
      if ("http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes".equals(param1String) && param1Boolean)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unsupported feature: ");
      stringBuilder.append(param1String);
      throw new XmlPullParserException(stringBuilder.toString());
    }
    
    public boolean getFeature(String param1String) {
      if ("http://xmlpull.org/v1/doc/features.html#process-namespaces".equals(param1String))
        return true; 
      if ("http://xmlpull.org/v1/doc/features.html#report-namespace-prefixes".equals(param1String))
        return true; 
      return false;
    }
    
    public void setProperty(String param1String, Object param1Object) throws XmlPullParserException {
      throw new XmlPullParserException("setProperty() not supported");
    }
    
    public Object getProperty(String param1String) {
      return null;
    }
    
    public void setInput(Reader param1Reader) throws XmlPullParserException {
      throw new XmlPullParserException("setInput() not supported");
    }
    
    public void setInput(InputStream param1InputStream, String param1String) throws XmlPullParserException {
      throw new XmlPullParserException("setInput() not supported");
    }
    
    public void defineEntityReplacementText(String param1String1, String param1String2) throws XmlPullParserException {
      throw new XmlPullParserException("defineEntityReplacementText() not supported");
    }
    
    public String getNamespacePrefix(int param1Int) throws XmlPullParserException {
      throw new XmlPullParserException("getNamespacePrefix() not supported");
    }
    
    public String getInputEncoding() {
      return null;
    }
    
    public String getNamespace(String param1String) {
      throw new RuntimeException("getNamespace() not supported");
    }
    
    public int getNamespaceCount(int param1Int) throws XmlPullParserException {
      throw new XmlPullParserException("getNamespaceCount() not supported");
    }
    
    public String getPositionDescription() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Binary XML file line #");
      stringBuilder.append(getLineNumber());
      return stringBuilder.toString();
    }
    
    public String getNamespaceUri(int param1Int) throws XmlPullParserException {
      throw new XmlPullParserException("getNamespaceUri() not supported");
    }
    
    public int getColumnNumber() {
      return -1;
    }
    
    public int getDepth() {
      return this.mDepth;
    }
    
    public String getText() {
      String str;
      int i = XmlBlock.nativeGetText(this.mParseState);
      if (i >= 0) {
        str = XmlBlock.this.mStrings.get(i).toString();
      } else {
        str = null;
      } 
      return str;
    }
    
    public int getLineNumber() {
      return XmlBlock.nativeGetLineNumber(this.mParseState);
    }
    
    public int getEventType() throws XmlPullParserException {
      return this.mEventType;
    }
    
    public boolean isWhitespace() throws XmlPullParserException {
      return false;
    }
    
    public String getPrefix() {
      throw new RuntimeException("getPrefix not supported");
    }
    
    public char[] getTextCharacters(int[] param1ArrayOfint) {
      String str = getText();
      char[] arrayOfChar = null;
      if (str != null) {
        param1ArrayOfint[0] = 0;
        param1ArrayOfint[1] = str.length();
        arrayOfChar = new char[str.length()];
        str.getChars(0, str.length(), arrayOfChar, 0);
      } 
      return arrayOfChar;
    }
    
    public String getNamespace() {
      String str;
      int i = XmlBlock.nativeGetNamespace(this.mParseState);
      if (i >= 0) {
        str = XmlBlock.this.mStrings.get(i).toString();
      } else {
        str = "";
      } 
      return str;
    }
    
    public String getName() {
      String str;
      int i = XmlBlock.nativeGetName(this.mParseState);
      if (i >= 0) {
        str = XmlBlock.this.mStrings.get(i).toString();
      } else {
        str = null;
      } 
      return str;
    }
    
    public String getAttributeNamespace(int param1Int) {
      int i = XmlBlock.nativeGetAttributeNamespace(this.mParseState, param1Int);
      if (i >= 0)
        return XmlBlock.this.mStrings.get(i).toString(); 
      if (i == -1)
        return ""; 
      throw new IndexOutOfBoundsException(String.valueOf(param1Int));
    }
    
    public String getAttributeName(int param1Int) {
      int i = XmlBlock.nativeGetAttributeName(this.mParseState, param1Int);
      if (i >= 0)
        return XmlBlock.this.mStrings.get(i).toString(); 
      throw new IndexOutOfBoundsException(String.valueOf(param1Int));
    }
    
    public String getAttributePrefix(int param1Int) {
      throw new RuntimeException("getAttributePrefix not supported");
    }
    
    public boolean isEmptyElementTag() throws XmlPullParserException {
      return false;
    }
    
    public int getAttributeCount() {
      byte b;
      if (this.mEventType == 2) {
        b = XmlBlock.nativeGetAttributeCount(this.mParseState);
      } else {
        b = -1;
      } 
      return b;
    }
    
    public String getAttributeValue(int param1Int) {
      int i = XmlBlock.nativeGetAttributeStringValue(this.mParseState, param1Int);
      if (i >= 0)
        return XmlBlock.this.mStrings.get(i).toString(); 
      i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int);
      if (i != 0) {
        param1Int = XmlBlock.nativeGetAttributeData(this.mParseState, param1Int);
        return TypedValue.coerceToString(i, param1Int);
      } 
      throw new IndexOutOfBoundsException(String.valueOf(param1Int));
    }
    
    public String getAttributeType(int param1Int) {
      return "CDATA";
    }
    
    public boolean isAttributeDefault(int param1Int) {
      return false;
    }
    
    public int nextToken() throws XmlPullParserException, IOException {
      return next();
    }
    
    public String getAttributeValue(String param1String1, String param1String2) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeValue(i); 
      return null;
    }
    
    public int next() throws XmlPullParserException, IOException {
      if (!this.mStarted) {
        this.mStarted = true;
        return 0;
      } 
      long l = this.mParseState;
      if (l == 0L)
        return 1; 
      int i = XmlBlock.nativeNext(l);
      if (this.mDecNextDepth) {
        this.mDepth--;
        this.mDecNextDepth = false;
      } 
      if (i != 2) {
        if (i == 3)
          this.mDecNextDepth = true; 
      } else {
        this.mDepth++;
      } 
      this.mEventType = i;
      if (i == 1)
        close(); 
      return i;
    }
    
    public void require(int param1Int, String param1String1, String param1String2) throws XmlPullParserException, IOException {
      if (param1Int == getEventType() && (param1String1 == null || param1String1.equals(getNamespace())) && (param1String2 == null || param1String2.equals(getName())))
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("expected ");
      stringBuilder.append(TYPES[param1Int]);
      stringBuilder.append(getPositionDescription());
      throw new XmlPullParserException(stringBuilder.toString());
    }
    
    public String nextText() throws XmlPullParserException, IOException {
      if (getEventType() == 2) {
        int i = next();
        if (i == 4) {
          String str = getText();
          i = next();
          if (i == 3)
            return str; 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append(getPositionDescription());
          stringBuilder2.append(": event TEXT it must be immediately followed by END_TAG");
          throw new XmlPullParserException(stringBuilder2.toString(), this, null);
        } 
        if (i == 3)
          return ""; 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(getPositionDescription());
        stringBuilder1.append(": parser must be on START_TAG or TEXT to read text");
        throw new XmlPullParserException(stringBuilder1.toString(), this, null);
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getPositionDescription());
      stringBuilder.append(": parser must be on START_TAG to read next text");
      throw new XmlPullParserException(stringBuilder.toString(), this, null);
    }
    
    public int nextTag() throws XmlPullParserException, IOException {
      int i = next();
      int j = i;
      if (i == 4) {
        j = i;
        if (isWhitespace())
          j = next(); 
      } 
      if (j == 2 || j == 3)
        return j; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(getPositionDescription());
      stringBuilder.append(": expected start or end tag");
      throw new XmlPullParserException(stringBuilder.toString(), this, null);
    }
    
    public int getAttributeNameResource(int param1Int) {
      return XmlBlock.nativeGetAttributeResource(this.mParseState, param1Int);
    }
    
    public int getAttributeListValue(String param1String1, String param1String2, String[] param1ArrayOfString, int param1Int) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeListValue(i, param1ArrayOfString, param1Int); 
      return param1Int;
    }
    
    public boolean getAttributeBooleanValue(String param1String1, String param1String2, boolean param1Boolean) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeBooleanValue(i, param1Boolean); 
      return param1Boolean;
    }
    
    public int getAttributeResourceValue(String param1String1, String param1String2, int param1Int) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeResourceValue(i, param1Int); 
      return param1Int;
    }
    
    public int getAttributeIntValue(String param1String1, String param1String2, int param1Int) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeIntValue(i, param1Int); 
      return param1Int;
    }
    
    public int getAttributeUnsignedIntValue(String param1String1, String param1String2, int param1Int) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeUnsignedIntValue(i, param1Int); 
      return param1Int;
    }
    
    public float getAttributeFloatValue(String param1String1, String param1String2, float param1Float) {
      int i = XmlBlock.nativeGetAttributeIndex(this.mParseState, param1String1, param1String2);
      if (i >= 0)
        return getAttributeFloatValue(i, param1Float); 
      return param1Float;
    }
    
    public int getAttributeListValue(int param1Int1, String[] param1ArrayOfString, int param1Int2) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int1);
      param1Int1 = XmlBlock.nativeGetAttributeData(this.mParseState, param1Int1);
      if (i == 3) {
        StringBlock stringBlock = XmlBlock.this.mStrings;
        CharSequence charSequence = stringBlock.get(param1Int1);
        return XmlUtils.convertValueToList(charSequence, param1ArrayOfString, param1Int2);
      } 
      return param1Int1;
    }
    
    public boolean getAttributeBooleanValue(int param1Int, boolean param1Boolean) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int);
      if (i >= 16 && i <= 31) {
        if (XmlBlock.nativeGetAttributeData(this.mParseState, param1Int) != 0) {
          param1Boolean = true;
        } else {
          param1Boolean = false;
        } 
        return param1Boolean;
      } 
      return param1Boolean;
    }
    
    public int getAttributeResourceValue(int param1Int1, int param1Int2) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int1);
      if (i == 1)
        return XmlBlock.nativeGetAttributeData(this.mParseState, param1Int1); 
      return param1Int2;
    }
    
    public int getAttributeIntValue(int param1Int1, int param1Int2) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int1);
      if (i >= 16 && i <= 31)
        return XmlBlock.nativeGetAttributeData(this.mParseState, param1Int1); 
      return param1Int2;
    }
    
    public int getAttributeUnsignedIntValue(int param1Int1, int param1Int2) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int1);
      if (i >= 16 && i <= 31)
        return XmlBlock.nativeGetAttributeData(this.mParseState, param1Int1); 
      return param1Int2;
    }
    
    public float getAttributeFloatValue(int param1Int, float param1Float) {
      int i = XmlBlock.nativeGetAttributeDataType(this.mParseState, param1Int);
      if (i == 4) {
        long l = this.mParseState;
        param1Int = XmlBlock.nativeGetAttributeData(l, param1Int);
        return Float.intBitsToFloat(param1Int);
      } 
      throw new RuntimeException("not a float!");
    }
    
    public String getIdAttribute() {
      String str;
      int i = XmlBlock.nativeGetIdAttribute(this.mParseState);
      if (i >= 0) {
        str = XmlBlock.this.mStrings.get(i).toString();
      } else {
        str = null;
      } 
      return str;
    }
    
    public String getClassAttribute() {
      String str;
      int i = XmlBlock.nativeGetClassAttribute(this.mParseState);
      if (i >= 0) {
        str = XmlBlock.this.mStrings.get(i).toString();
      } else {
        str = null;
      } 
      return str;
    }
    
    public int getIdAttributeResourceValue(int param1Int) {
      return getAttributeResourceValue(null, "id", param1Int);
    }
    
    public int getStyleAttribute() {
      return XmlBlock.nativeGetStyleAttribute(this.mParseState);
    }
    
    public void close() {
      synchronized (this.mBlock) {
        if (this.mParseState != 0L) {
          XmlBlock.nativeDestroyParseState(this.mParseState);
          this.mParseState = 0L;
          this.mBlock.decOpenCountLocked();
        } 
        return;
      } 
    }
    
    protected void finalize() throws Throwable {
      close();
    }
    
    final CharSequence getPooledString(int param1Int) {
      return XmlBlock.this.mStrings.get(param1Int);
    }
  }
  
  protected void finalize() throws Throwable {
    close();
  }
  
  XmlBlock(AssetManager paramAssetManager, long paramLong) {
    this.mOpen = true;
    this.mOpenCount = 1;
    this.mAssets = paramAssetManager;
    this.mNative = paramLong;
    this.mStrings = new StringBlock(nativeGetStringBlock(paramLong), false);
  }
  
  private static final native long nativeCreate(byte[] paramArrayOfbyte, int paramInt1, int paramInt2);
  
  private static final native long nativeCreateParseState(long paramLong, int paramInt);
  
  private static final native void nativeDestroy(long paramLong);
  
  private static final native void nativeDestroyParseState(long paramLong);
  
  @FastNative
  private static final native int nativeGetAttributeCount(long paramLong);
  
  @FastNative
  private static final native int nativeGetAttributeData(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetAttributeDataType(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetAttributeIndex(long paramLong, String paramString1, String paramString2);
  
  @FastNative
  private static final native int nativeGetAttributeName(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetAttributeNamespace(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetAttributeResource(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetAttributeStringValue(long paramLong, int paramInt);
  
  @FastNative
  private static final native int nativeGetClassAttribute(long paramLong);
  
  @FastNative
  private static final native int nativeGetIdAttribute(long paramLong);
  
  @FastNative
  private static final native int nativeGetLineNumber(long paramLong);
  
  @FastNative
  static final native int nativeGetName(long paramLong);
  
  @FastNative
  private static final native int nativeGetNamespace(long paramLong);
  
  @FastNative
  private static final native int nativeGetSourceResId(long paramLong);
  
  private static final native long nativeGetStringBlock(long paramLong);
  
  @FastNative
  private static final native int nativeGetStyleAttribute(long paramLong);
  
  @FastNative
  private static final native int nativeGetText(long paramLong);
  
  @FastNative
  static final native int nativeNext(long paramLong);
}
