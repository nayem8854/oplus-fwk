package android.content.res;

import android.app.OplusBaseApplicationPackageManager;
import android.app.OplusThemeHelper;
import android.app.OplusUXIconLoadHelper;
import android.app.OplusUxIconConfigParser;
import android.app.OplusUxIconConstants;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.LauncherActivityInfo;
import android.content.pm.PackageItemInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.graphics.drawable.Drawable;
import android.os.Debug;
import android.os.FileUtils;
import android.os.SystemProperties;
import android.os.Trace;
import android.os.UserHandle;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import java.io.File;
import java.io.InputStream;
import oplus.content.res.OplusExtraConfiguration;

public class OplusThemeManager implements IOplusThemeManager {
  private static final String TAG = "OplusThemeManager";
  
  private static volatile OplusThemeManager sInstance = null;
  
  public static OplusThemeManager getInstance() {
    // Byte code:
    //   0: getstatic android/content/res/OplusThemeManager.sInstance : Landroid/content/res/OplusThemeManager;
    //   3: ifnonnull -> 39
    //   6: ldc android/content/res/OplusThemeManager
    //   8: monitorenter
    //   9: getstatic android/content/res/OplusThemeManager.sInstance : Landroid/content/res/OplusThemeManager;
    //   12: ifnonnull -> 27
    //   15: new android/content/res/OplusThemeManager
    //   18: astore_0
    //   19: aload_0
    //   20: invokespecial <init> : ()V
    //   23: aload_0
    //   24: putstatic android/content/res/OplusThemeManager.sInstance : Landroid/content/res/OplusThemeManager;
    //   27: ldc android/content/res/OplusThemeManager
    //   29: monitorexit
    //   30: goto -> 39
    //   33: astore_0
    //   34: ldc android/content/res/OplusThemeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: athrow
    //   39: getstatic android/content/res/OplusThemeManager.sInstance : Landroid/content/res/OplusThemeManager;
    //   42: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #68	-> 0
    //   #69	-> 6
    //   #70	-> 9
    //   #71	-> 15
    //   #73	-> 27
    //   #75	-> 39
    // Exception table:
    //   from	to	target	type
    //   9	15	33	finally
    //   15	27	33	finally
    //   27	30	33	finally
    //   34	37	33	finally
  }
  
  public void getValue(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, TypedValue paramTypedValue, boolean paramBoolean) {
    paramOplusBaseResourcesImpl.getExValue(paramInt, paramTypedValue, paramBoolean);
  }
  
  public void init(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    if (paramOplusBaseResourcesImpl != null)
      paramOplusBaseResourcesImpl.init(paramString); 
  }
  
  public void init(OplusBaseResources paramOplusBaseResources, String paramString) {
    if (paramOplusBaseResources != null && paramOplusBaseResources.getOplusBaseResImpl() != null)
      paramOplusBaseResources.getOplusBaseResImpl().init(paramString); 
  }
  
  public InputStream openRawResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, TypedValue paramTypedValue) {
    return paramOplusBaseResourcesImpl.openThemeRawResource(paramInt, paramTypedValue);
  }
  
  public int updateExConfiguration(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, Configuration paramConfiguration) {
    return paramOplusBaseResourcesImpl.updateExConfiguration(paramOplusBaseResourcesImpl, paramConfiguration);
  }
  
  public void checkUpdate(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, int paramInt, boolean paramBoolean) {
    paramOplusBaseResourcesImpl.checkUpdate(paramInt, paramBoolean);
  }
  
  public TypedArray replaceTypedArray(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, TypedArray paramTypedArray) {
    return paramOplusBaseResourcesImpl.replaceTypedArray(paramTypedArray);
  }
  
  public Drawable loadOverlayDrawable(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, Resources paramResources, TypedValue paramTypedValue, int paramInt) {
    return paramOplusBaseResourcesImpl.loadOverlayDrawable(paramResources, paramTypedValue, paramInt);
  }
  
  public CharSequence getText(OplusBaseResources paramOplusBaseResources, int paramInt, CharSequence paramCharSequence) {
    CharSequence charSequence = paramOplusBaseResources.getOplusBaseResImpl().getThemeCharSequence(paramInt);
    if (charSequence != null)
      return charSequence; 
    return paramCharSequence;
  }
  
  public Drawable getDefaultActivityIcon(Context paramContext, OplusBaseResources paramOplusBaseResources) {
    Trace.traceBegin(8192L, "#UxIcon.getDefaultActivityIcon");
    if ("android.content.cts".equals(paramContext.getPackageName()))
      return paramContext.getDrawable(17301651); 
    Drawable drawable1 = paramContext.getDrawable(17629184);
    Drawable drawable2 = drawable1;
    if (drawable1 != null)
      if (OplusUXIconLoadHelper.supportUxIcon(paramContext.getPackageManager(), paramContext.getApplicationInfo(), paramContext.getPackageName())) {
        drawable2 = OplusUXIconLoadHelper.getUxIconDrawable(paramContext.getResources(), paramOplusBaseResources, drawable1, false);
      } else {
        drawable2 = OplusThemeHelper.getDrawableByConvert(paramOplusBaseResources, paramContext.getResources(), drawable1);
      }  
    Trace.traceEnd(8192L);
    return drawable2;
  }
  
  public Drawable loadPackageItemIcon(PackageItemInfo paramPackageItemInfo, PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    if (paramBoolean && 
      paramPackageManager instanceof OplusBaseApplicationPackageManager)
      return ((OplusBaseApplicationPackageManager)paramPackageManager).loadItemIcon(paramPackageItemInfo, paramApplicationInfo, true); 
    return paramPackageManager.loadItemIcon(paramPackageItemInfo, paramApplicationInfo);
  }
  
  private Drawable loadDrawableFromTheme(ResolveInfo paramResolveInfo, PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    Drawable drawable;
    ServiceInfo serviceInfo;
    if (paramBoolean) {
      paramBoolean = OplusUXIconLoadHelper.supportUxIcon(paramPackageManager, paramApplicationInfo, paramString);
      String str = null;
      if (paramBoolean) {
        if (paramResolveInfo.activityInfo != null)
          str = paramResolveInfo.activityInfo.name; 
        drawable = OplusUXIconLoadHelper.getDrawable(paramPackageManager, paramString, str, paramInt, paramApplicationInfo, true);
      } else {
        drawable = OplusThemeHelper.getDrawable((OplusBaseApplicationPackageManager)paramPackageManager, paramString, paramInt, paramApplicationInfo, null);
      } 
      return drawable;
    } 
    if (((ResolveInfo)drawable).activityInfo != null) {
      ActivityInfo activityInfo = ((ResolveInfo)drawable).activityInfo;
    } else {
      serviceInfo = ((ResolveInfo)drawable).serviceInfo;
    } 
    return OplusThemeHelper.getDrawable(paramPackageManager, paramString, ((ResolveInfo)drawable).icon, paramApplicationInfo, (PackageItemInfo)serviceInfo, OplusThemeHelper.isCustomizedIcon(((ResolveInfo)drawable).filter));
  }
  
  public Drawable loadResolveIcon(ResolveInfo paramResolveInfo, PackageManager paramPackageManager, String paramString, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    Drawable drawable2 = loadDrawableFromTheme(paramResolveInfo, paramPackageManager, paramString, paramInt, paramApplicationInfo, paramBoolean);
    Drawable drawable1 = drawable2;
    if (drawable2 == null)
      drawable1 = paramPackageManager.getDrawable(paramString, paramInt, paramApplicationInfo); 
    return drawable1;
  }
  
  public Drawable getBadgedIcon(LauncherActivityInfo paramLauncherActivityInfo, int paramInt, PackageManager paramPackageManager, UserHandle paramUserHandle, ActivityInfo paramActivityInfo) {
    Drawable drawable2 = paramActivityInfo.loadIcon(paramPackageManager);
    Drawable drawable1 = drawable2;
    if (drawable2 == null)
      drawable1 = paramActivityInfo.loadDefaultIcon(paramPackageManager); 
    return paramPackageManager.getUserBadgedIcon(drawable1, paramUserHandle);
  }
  
  public boolean isOplusIcons() {
    return true;
  }
  
  public boolean supportUxIcon(PackageManager paramPackageManager, ApplicationInfo paramApplicationInfo, String paramString) {
    return OplusUXIconLoadHelper.supportUxIcon(paramPackageManager, paramApplicationInfo, paramString);
  }
  
  public Drawable getDrawableFromUxIcon(PackageManager paramPackageManager, String paramString1, String paramString2, int paramInt, ApplicationInfo paramApplicationInfo, boolean paramBoolean) {
    return OplusUXIconLoadHelper.getDrawable(paramPackageManager, paramString1, paramString2, paramInt, paramApplicationInfo, paramBoolean);
  }
  
  public Drawable getDrawableForApp(Resources paramResources, OplusBaseResources paramOplusBaseResources, Drawable paramDrawable, boolean paramBoolean) {
    return OplusUXIconLoadHelper.getUxIconDrawable(paramResources, paramOplusBaseResources, paramDrawable, paramBoolean);
  }
  
  public long getIconConfigFromSettings(ContentResolver paramContentResolver, Context paramContext, int paramInt) {
    long l1 = Settings.System.getLongForUser(paramContentResolver, "key_ux_icon_config", -1L, paramInt);
    long l2 = l1;
    if (l1 == -1L) {
      if (paramInt == 0 && SystemProperties.getInt("persist.sys.themeflag.uxicon", 0) == 0)
        SystemProperties.set("persist.sys.themeflag.uxicon", String.valueOf(-1)); 
      boolean bool = paramContext.getPackageManager().hasSystemFeature("oppo.version.exp");
      l2 = OplusUxIconConfigParser.getDefaultIconConfig(bool, paramContext.getResources());
      Settings.System.putLongForUser(paramContentResolver, "key_ux_icon_config", l2, paramInt);
    } 
    return l2;
  }
  
  public void setIconConfigToSettings(ContentResolver paramContentResolver, long paramLong, int paramInt) {
    Settings.System.putLongForUser(paramContentResolver, "key_ux_icon_config", paramLong, paramInt);
  }
  
  public void updateExtraConfigForUxIcon(int paramInt) {
    OplusUXIconLoadHelper.updateExtraConfig(paramInt);
    if (OplusUxIconConstants.DEBUG_UX_ICON) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("updateExtraConfigForUxIcon changes = ");
      stringBuilder.append(paramInt);
      stringBuilder.append("; callers:");
      stringBuilder.append(Debug.getCallers(10));
      Log.i("OplusThemeManager", stringBuilder.toString());
    } 
  }
  
  public boolean supportUxOnline(PackageManager paramPackageManager, String paramString) {
    return OplusUXIconLoadHelper.supportUxOnline(paramPackageManager, paramString);
  }
  
  public void onCleanupUserForTheme(int paramInt) {
    if (paramInt != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("/data/theme/");
      stringBuilder.append(paramInt);
      File file = new File(stringBuilder.toString());
      if (file.exists())
        FileUtils.deleteContentsAndDir(file); 
    } 
  }
  
  public Drawable loadOverlayResolverDrawable(PackageManager paramPackageManager, String paramString1, int paramInt, ApplicationInfo paramApplicationInfo, String paramString2) {
    Drawable drawable1;
    if (OplusUXIconLoadHelper.supportUxIcon(paramPackageManager, paramApplicationInfo, paramString1)) {
      drawable1 = OplusUXIconLoadHelper.loadOverlayResolverDrawable(paramPackageManager, paramString1, paramInt, paramApplicationInfo, paramString2);
    } else {
      drawable1 = OplusThemeHelper.getDrawable((OplusBaseApplicationPackageManager)paramPackageManager, paramString1, paramInt, paramApplicationInfo, null);
    } 
    Drawable drawable2 = drawable1;
    if (drawable1 == null)
      drawable2 = paramPackageManager.getDrawable(paramString1, paramInt, paramApplicationInfo); 
    return drawable2;
  }
  
  public boolean shouldReportExtraConfig(int paramInt1, int paramInt2) {
    return OplusExtraConfiguration.shouldReportExtra(paramInt1, paramInt2);
  }
  
  public void setDarkFilterToDrawable(Drawable paramDrawable) {
    OplusUXIconLoadHelper.setDarkFilterToDrawable(paramDrawable);
  }
}
