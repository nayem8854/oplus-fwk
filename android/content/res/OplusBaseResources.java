package android.content.res;

import android.graphics.drawable.Drawable;

public abstract class OplusBaseResources {
  public void setIsThemeChanged(boolean paramBoolean) {
    getOplusBaseResImpl().setIsThemeChanged(paramBoolean);
  }
  
  public boolean getThemeChanged() {
    return getOplusBaseResImpl().getThemeChanged();
  }
  
  public Drawable loadIcon(int paramInt) {
    return loadIcon(paramInt, null, true);
  }
  
  public CharSequence getThemeCharSequence(int paramInt) {
    return getOplusBaseResImpl().getThemeCharSequence(paramInt);
  }
  
  public Drawable loadIcon(int paramInt, boolean paramBoolean) {
    return loadIcon(paramInt, null, paramBoolean);
  }
  
  public Drawable loadIcon(int paramInt, String paramString) {
    return loadIcon(paramInt, paramString, true);
  }
  
  public void init(String paramString) {
    getOplusBaseResImpl().init(paramString);
  }
  
  public Drawable loadIcon(int paramInt, String paramString, boolean paramBoolean) {
    return getOplusBaseResImpl().loadIcon(typeCasting(this), paramInt, paramString, paramBoolean);
  }
  
  private Resources typeCasting(OplusBaseResources paramOplusBaseResources) {
    return typeCasting(Resources.class, paramOplusBaseResources);
  }
  
  private <T> T typeCasting(Class<T> paramClass, Object paramObject) {
    if (paramObject != null && paramClass.isInstance(paramObject))
      return (T)paramObject; 
    return null;
  }
  
  public abstract OplusBaseConfiguration getConfiguration();
  
  public abstract OplusBaseResourcesImpl getOplusBaseResImpl();
}
