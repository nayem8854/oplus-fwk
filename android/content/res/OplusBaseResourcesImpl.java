package android.content.res;

import android.app.OplusThemeHelper;
import android.graphics.drawable.Drawable;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.util.TypedValue;
import com.oplus.theme.OplusAppIconInfo;
import java.io.InputStream;
import oplus.content.res.OplusExtraConfiguration;

public abstract class OplusBaseResourcesImpl {
  protected boolean mIsHasValues = false;
  
  protected boolean mIsHasDrawables = false;
  
  protected boolean mIsHasSystemValues = false;
  
  protected boolean mIsHasSystemDrawables = false;
  
  protected boolean mIsHasAcessValues = false;
  
  protected boolean mIsHasAcessDrawables = false;
  
  protected SparseArray<CharSequence> mCharSequences = new SparseArray();
  
  protected SparseIntArray mCookies = new SparseIntArray();
  
  protected SparseArray<Integer> mIntegers = new SparseArray();
  
  protected SparseArray<Boolean> mSkipFiles = new SparseArray();
  
  protected SparseArray<CharSequence> mSkipIcons = new SparseArray();
  
  protected boolean mThemeChanged = false;
  
  protected SparseArray<Boolean> mLoadArrary = new SparseArray();
  
  public static final int COOKIE_TYPE_FRAMEWORK = 1;
  
  public static final int COOKIE_TYPE_OPLUS = 2;
  
  public static final int COOKIE_TYPE_OTHERPACKAGE = 3;
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "OplusBaseResourcesImpl";
  
  protected OplusAccessibleResources mAccessibleResources;
  
  protected OplusThemeResourcesPackage mIconThemeResources;
  
  private String mName;
  
  protected OplusThemeResourcesSystem mSystemThemeResources;
  
  protected OplusThemeResources mThemeResources;
  
  void clearCache() {
    this.mThemeChanged = true;
  }
  
  public void init(String paramString) {
    this.mName = paramString;
    initThemeResource(paramString);
    OplusThemeResources oplusThemeResources = this.mThemeResources;
    if (oplusThemeResources != null) {
      this.mIsHasValues = oplusThemeResources.hasValues();
      this.mIsHasDrawables = this.mThemeResources.hasDrawables();
    } 
    oplusThemeResources = this.mSystemThemeResources;
    if (oplusThemeResources != null) {
      this.mIsHasSystemValues = oplusThemeResources.hasValues();
      this.mIsHasSystemDrawables = this.mSystemThemeResources.hasDrawables();
    } 
    OplusAccessibleResources oplusAccessibleResources = this.mAccessibleResources;
    if (oplusAccessibleResources != null) {
      this.mIsHasAcessValues = oplusAccessibleResources.hasValues();
      this.mIsHasAcessDrawables = this.mAccessibleResources.hasDrawables();
    } 
  }
  
  public Drawable loadOverlayDrawable(Resources paramResources, TypedValue paramTypedValue, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsHasDrawables : Z
    //   6: ifne -> 31
    //   9: aload_0
    //   10: getfield mIsHasAcessDrawables : Z
    //   13: ifne -> 31
    //   16: aload_0
    //   17: getfield mIsHasSystemDrawables : Z
    //   20: istore #4
    //   22: iload #4
    //   24: ifne -> 31
    //   27: aload_0
    //   28: monitorexit
    //   29: aconst_null
    //   30: areturn
    //   31: aload_2
    //   32: getfield string : Ljava/lang/CharSequence;
    //   35: invokeinterface toString : ()Ljava/lang/String;
    //   40: astore #5
    //   42: aload #5
    //   44: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   47: istore #4
    //   49: iload #4
    //   51: ifeq -> 58
    //   54: aload_0
    //   55: monitorexit
    //   56: aconst_null
    //   57: areturn
    //   58: aconst_null
    //   59: astore #6
    //   61: aconst_null
    //   62: astore #7
    //   64: aconst_null
    //   65: astore #8
    //   67: aload_0
    //   68: getfield mSkipFiles : Landroid/util/SparseArray;
    //   71: iload_3
    //   72: invokevirtual get : (I)Ljava/lang/Object;
    //   75: ifnonnull -> 400
    //   78: aconst_null
    //   79: astore #9
    //   81: aload_0
    //   82: aload_2
    //   83: getfield assetCookie : I
    //   86: iload_3
    //   87: invokespecial getCookieType : (II)I
    //   90: istore #10
    //   92: aload #9
    //   94: astore #7
    //   96: aload_0
    //   97: getfield mIsHasDrawables : Z
    //   100: ifeq -> 127
    //   103: aload #9
    //   105: astore #7
    //   107: aload_0
    //   108: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   111: ifnull -> 127
    //   114: aload_0
    //   115: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   118: iload #10
    //   120: aload #5
    //   122: invokevirtual getThemeFileStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   125: astore #7
    //   127: aload #7
    //   129: astore #9
    //   131: aload_0
    //   132: getfield mIsHasSystemDrawables : Z
    //   135: ifeq -> 181
    //   138: aload #7
    //   140: astore #9
    //   142: aload #7
    //   144: ifnonnull -> 181
    //   147: aload #7
    //   149: astore #9
    //   151: aload_0
    //   152: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   155: ifnull -> 181
    //   158: aload #7
    //   160: astore #9
    //   162: iload #10
    //   164: iconst_3
    //   165: if_icmpge -> 181
    //   168: aload_0
    //   169: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   172: iload #10
    //   174: aload #5
    //   176: invokevirtual getThemeFileStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   179: astore #9
    //   181: aload #9
    //   183: astore #11
    //   185: aload_0
    //   186: getfield mIsHasAcessDrawables : Z
    //   189: ifeq -> 225
    //   192: aload #9
    //   194: astore #11
    //   196: aload #9
    //   198: ifnonnull -> 225
    //   201: aload #9
    //   203: astore #11
    //   205: aload_0
    //   206: getfield mAccessibleResources : Landroid/content/res/OplusAccessibleResources;
    //   209: ifnull -> 225
    //   212: aload_0
    //   213: getfield mAccessibleResources : Landroid/content/res/OplusAccessibleResources;
    //   216: iload #10
    //   218: aload #5
    //   220: invokevirtual getAccessibleStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   223: astore #11
    //   225: aload #11
    //   227: ifnonnull -> 246
    //   230: aload_0
    //   231: getfield mSkipFiles : Landroid/util/SparseArray;
    //   234: iload_3
    //   235: iconst_1
    //   236: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   239: invokevirtual put : (ILjava/lang/Object;)V
    //   242: aload_0
    //   243: monitorexit
    //   244: aconst_null
    //   245: areturn
    //   246: new android/graphics/BitmapFactory$Options
    //   249: astore #7
    //   251: aload #7
    //   253: invokespecial <init> : ()V
    //   256: aload #11
    //   258: getfield mDensity : I
    //   261: ifne -> 275
    //   264: aload #7
    //   266: getstatic android/util/DisplayMetrics.DENSITY_DEVICE_STABLE : I
    //   269: putfield inDensity : I
    //   272: goto -> 285
    //   275: aload #7
    //   277: aload #11
    //   279: getfield mDensity : I
    //   282: putfield inDensity : I
    //   285: aload #11
    //   287: getfield mInput : Ljava/io/InputStream;
    //   290: astore #9
    //   292: aload_1
    //   293: aload_2
    //   294: aload #9
    //   296: aload #5
    //   298: aload #7
    //   300: invokestatic createFromResourceStream : (Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    //   303: astore_1
    //   304: aload_1
    //   305: astore #7
    //   307: aload #11
    //   309: ifnull -> 333
    //   312: aload_1
    //   313: astore #7
    //   315: aload #11
    //   317: getfield mInput : Ljava/io/InputStream;
    //   320: invokevirtual close : ()V
    //   323: aload_1
    //   324: astore #7
    //   326: goto -> 333
    //   329: astore_1
    //   330: goto -> 400
    //   333: goto -> 400
    //   336: astore_1
    //   337: goto -> 378
    //   340: astore_1
    //   341: ldc 'OplusBaseResourcesImpl'
    //   343: ldc_w 'out of memory !!'
    //   346: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   349: pop
    //   350: aload #6
    //   352: astore #7
    //   354: aload #11
    //   356: ifnull -> 333
    //   359: aload #8
    //   361: astore #7
    //   363: aload #11
    //   365: getfield mInput : Ljava/io/InputStream;
    //   368: invokevirtual close : ()V
    //   371: aload #6
    //   373: astore #7
    //   375: goto -> 333
    //   378: aload #11
    //   380: ifnull -> 398
    //   383: aload #11
    //   385: getfield mInput : Ljava/io/InputStream;
    //   388: invokevirtual close : ()V
    //   391: goto -> 398
    //   394: astore_2
    //   395: goto -> 398
    //   398: aload_1
    //   399: athrow
    //   400: aload #7
    //   402: ifnull -> 417
    //   405: aload_0
    //   406: getfield mLoadArrary : Landroid/util/SparseArray;
    //   409: iload_3
    //   410: iconst_1
    //   411: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   414: invokevirtual put : (ILjava/lang/Object;)V
    //   417: aload_0
    //   418: monitorexit
    //   419: aload #7
    //   421: areturn
    //   422: astore_1
    //   423: aload_0
    //   424: monitorexit
    //   425: aload_1
    //   426: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #103	-> 2
    //   #104	-> 27
    //   #106	-> 31
    //   #107	-> 42
    //   #108	-> 54
    //   #110	-> 58
    //   #111	-> 67
    //   #112	-> 78
    //   #113	-> 81
    //   #115	-> 92
    //   #116	-> 114
    //   #118	-> 127
    //   #119	-> 168
    //   #121	-> 181
    //   #122	-> 212
    //   #124	-> 225
    //   #125	-> 230
    //   #126	-> 242
    //   #129	-> 246
    //   #130	-> 256
    //   #131	-> 264
    //   #133	-> 275
    //   #135	-> 285
    //   #136	-> 292
    //   #141	-> 304
    //   #142	-> 312
    //   #144	-> 329
    //   #146	-> 330
    //   #145	-> 333
    //   #140	-> 336
    //   #137	-> 340
    //   #138	-> 341
    //   #141	-> 350
    //   #142	-> 359
    //   #141	-> 378
    //   #142	-> 383
    //   #144	-> 394
    //   #145	-> 398
    //   #146	-> 398
    //   #149	-> 400
    //   #150	-> 405
    //   #152	-> 417
    //   #102	-> 422
    // Exception table:
    //   from	to	target	type
    //   2	22	422	finally
    //   31	42	422	finally
    //   42	49	422	finally
    //   67	78	422	finally
    //   81	92	422	finally
    //   96	103	422	finally
    //   107	114	422	finally
    //   114	127	422	finally
    //   131	138	422	finally
    //   151	158	422	finally
    //   168	181	422	finally
    //   185	192	422	finally
    //   205	212	422	finally
    //   212	225	422	finally
    //   230	242	422	finally
    //   246	256	340	java/lang/OutOfMemoryError
    //   246	256	336	finally
    //   256	264	340	java/lang/OutOfMemoryError
    //   256	264	336	finally
    //   264	272	340	java/lang/OutOfMemoryError
    //   264	272	336	finally
    //   275	285	340	java/lang/OutOfMemoryError
    //   275	285	336	finally
    //   285	292	340	java/lang/OutOfMemoryError
    //   285	292	336	finally
    //   292	304	340	java/lang/OutOfMemoryError
    //   292	304	336	finally
    //   315	323	329	java/lang/Exception
    //   315	323	422	finally
    //   341	350	336	finally
    //   363	371	329	java/lang/Exception
    //   363	371	422	finally
    //   383	391	394	java/lang/Exception
    //   383	391	422	finally
    //   398	400	422	finally
    //   405	417	422	finally
  }
  
  public TypedArray replaceTypedArray(TypedArray paramTypedArray) {
    if (this.mIsHasValues || this.mIsHasAcessValues || this.mIsHasSystemValues) {
      int[] arrayOfInt = paramTypedArray.mData;
      int i = paramTypedArray.mValue.assetCookie;
      byte b = 0;
      while (b < arrayOfInt.length) {
        int j = arrayOfInt[b + 0];
        int k = arrayOfInt[b + 3];
        if ((j >= 16 && j <= 31) || j == 5) {
          Integer integer = getThemeInt(k, i);
          if (integer != null) {
            arrayOfInt[b + 1] = integer.intValue();
          } else if (j != 5) {
            integer = getColorValue(k, i);
            if (integer != null)
              arrayOfInt[b + 1] = integer.intValue(); 
          } 
        } 
        b += 7;
      } 
    } 
    return paramTypedArray;
  }
  
  public void setIsThemeChanged(boolean paramBoolean) {
    this.mThemeChanged = paramBoolean;
  }
  
  public boolean getThemeChanged() {
    return this.mThemeChanged;
  }
  
  public Drawable loadIcon(Resources paramResources, int paramInt, String paramString, boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aconst_null
    //   3: astore #5
    //   5: aconst_null
    //   6: astore #6
    //   8: aconst_null
    //   9: astore #7
    //   11: new android/util/TypedValue
    //   14: astore #8
    //   16: aload #8
    //   18: invokespecial <init> : ()V
    //   21: aload_0
    //   22: iload_2
    //   23: aload #8
    //   25: iconst_1
    //   26: invokevirtual getValue : (ILandroid/util/TypedValue;Z)V
    //   29: aload #8
    //   31: getfield string : Ljava/lang/CharSequence;
    //   34: invokeinterface toString : ()Ljava/lang/String;
    //   39: astore #9
    //   41: aload #9
    //   43: astore #10
    //   45: aload_3
    //   46: ifnull -> 76
    //   49: aload #9
    //   51: aload #9
    //   53: ldc_w '/'
    //   56: invokevirtual lastIndexOf : (Ljava/lang/String;)I
    //   59: iconst_1
    //   60: iadd
    //   61: invokevirtual substring : (I)Ljava/lang/String;
    //   64: astore #10
    //   66: aload #9
    //   68: aload #10
    //   70: aload_3
    //   71: invokevirtual replace : (Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    //   74: astore #10
    //   76: aload #6
    //   78: astore #9
    //   80: aload_0
    //   81: getfield mSkipIcons : Landroid/util/SparseArray;
    //   84: iload_2
    //   85: invokevirtual get : (I)Ljava/lang/Object;
    //   88: ifnonnull -> 284
    //   91: aload_0
    //   92: getfield mIconThemeResources : Landroid/content/res/OplusThemeResourcesPackage;
    //   95: ifnonnull -> 109
    //   98: aload_0
    //   99: aload_0
    //   100: ldc_w 'icons'
    //   103: invokestatic getThemeResources : (Landroid/content/res/OplusBaseResourcesImpl;Ljava/lang/String;)Landroid/content/res/OplusThemeResourcesPackage;
    //   106: putfield mIconThemeResources : Landroid/content/res/OplusThemeResourcesPackage;
    //   109: aload_0
    //   110: getfield mIconThemeResources : Landroid/content/res/OplusThemeResourcesPackage;
    //   113: aload #10
    //   115: iload #4
    //   117: invokevirtual getIconFileStream : (Ljava/lang/String;Z)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   120: astore #11
    //   122: aload #11
    //   124: ifnonnull -> 141
    //   127: aload_0
    //   128: getfield mSkipIcons : Landroid/util/SparseArray;
    //   131: iload_2
    //   132: aload #10
    //   134: invokevirtual put : (ILjava/lang/Object;)V
    //   137: aload_0
    //   138: monitorexit
    //   139: aconst_null
    //   140: areturn
    //   141: aload #7
    //   143: astore_3
    //   144: aload #11
    //   146: ifnull -> 258
    //   149: new android/graphics/BitmapFactory$Options
    //   152: astore_3
    //   153: aload_3
    //   154: invokespecial <init> : ()V
    //   157: aload #11
    //   159: getfield mDensity : I
    //   162: ifne -> 175
    //   165: aload_3
    //   166: getstatic android/util/DisplayMetrics.DENSITY_DEVICE_STABLE : I
    //   169: putfield inDensity : I
    //   172: goto -> 184
    //   175: aload_3
    //   176: aload #11
    //   178: getfield mDensity : I
    //   181: putfield inDensity : I
    //   184: aload #11
    //   186: getfield mInput : Ljava/io/InputStream;
    //   189: astore #9
    //   191: aload_1
    //   192: aload #8
    //   194: aload #9
    //   196: aload #10
    //   198: aload_3
    //   199: invokestatic createFromResourceStream : (Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/drawable/Drawable;
    //   202: astore_3
    //   203: goto -> 258
    //   206: astore_1
    //   207: aload #11
    //   209: ifnull -> 227
    //   212: aload #11
    //   214: getfield mInput : Ljava/io/InputStream;
    //   217: invokevirtual close : ()V
    //   220: goto -> 227
    //   223: astore_3
    //   224: goto -> 227
    //   227: aload_1
    //   228: athrow
    //   229: astore_1
    //   230: aload #6
    //   232: astore #9
    //   234: aload #11
    //   236: ifnull -> 284
    //   239: aload #5
    //   241: astore #9
    //   243: aload #11
    //   245: getfield mInput : Ljava/io/InputStream;
    //   248: invokevirtual close : ()V
    //   251: aload #6
    //   253: astore #9
    //   255: goto -> 284
    //   258: aload_3
    //   259: astore #9
    //   261: aload #11
    //   263: ifnull -> 284
    //   266: aload_3
    //   267: astore #9
    //   269: aload #11
    //   271: getfield mInput : Ljava/io/InputStream;
    //   274: invokevirtual close : ()V
    //   277: aload_3
    //   278: astore #9
    //   280: goto -> 284
    //   283: astore_1
    //   284: aload_0
    //   285: monitorexit
    //   286: aload #9
    //   288: areturn
    //   289: astore_1
    //   290: aload_0
    //   291: monitorexit
    //   292: aload_1
    //   293: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #189	-> 2
    //   #190	-> 11
    //   #191	-> 11
    //   #192	-> 21
    //   #193	-> 29
    //   #194	-> 41
    //   #195	-> 49
    //   #196	-> 66
    //   #198	-> 76
    //   #199	-> 91
    //   #200	-> 98
    //   #202	-> 109
    //   #203	-> 122
    //   #204	-> 127
    //   #205	-> 137
    //   #208	-> 141
    //   #209	-> 149
    //   #210	-> 157
    //   #211	-> 165
    //   #213	-> 175
    //   #215	-> 184
    //   #216	-> 191
    //   #220	-> 206
    //   #221	-> 207
    //   #222	-> 212
    //   #224	-> 223
    //   #225	-> 227
    //   #226	-> 227
    //   #218	-> 229
    //   #221	-> 230
    //   #222	-> 239
    //   #221	-> 258
    //   #222	-> 266
    //   #224	-> 283
    //   #226	-> 284
    //   #228	-> 284
    //   #188	-> 289
    // Exception table:
    //   from	to	target	type
    //   11	21	289	finally
    //   21	29	289	finally
    //   29	41	289	finally
    //   49	66	289	finally
    //   66	76	289	finally
    //   80	91	289	finally
    //   91	98	289	finally
    //   98	109	289	finally
    //   109	122	289	finally
    //   127	137	289	finally
    //   149	157	229	java/lang/OutOfMemoryError
    //   149	157	206	finally
    //   157	165	229	java/lang/OutOfMemoryError
    //   157	165	206	finally
    //   165	172	229	java/lang/OutOfMemoryError
    //   165	172	206	finally
    //   175	184	229	java/lang/OutOfMemoryError
    //   175	184	206	finally
    //   184	191	229	java/lang/OutOfMemoryError
    //   184	191	206	finally
    //   191	203	229	java/lang/OutOfMemoryError
    //   191	203	206	finally
    //   212	220	223	java/io/IOException
    //   212	220	289	finally
    //   227	229	289	finally
    //   243	251	283	java/io/IOException
    //   243	251	289	finally
    //   269	277	283	java/io/IOException
    //   269	277	289	finally
  }
  
  public InputStream openThemeRawResource(int paramInt, TypedValue paramTypedValue) throws Resources.NotFoundException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsHasDrawables : Z
    //   6: ifne -> 29
    //   9: aload_0
    //   10: getfield mIsHasAcessDrawables : Z
    //   13: ifne -> 29
    //   16: aload_0
    //   17: getfield mIsHasSystemDrawables : Z
    //   20: istore_3
    //   21: iload_3
    //   22: ifne -> 29
    //   25: aload_0
    //   26: monitorexit
    //   27: aconst_null
    //   28: areturn
    //   29: aconst_null
    //   30: astore #4
    //   32: aload_0
    //   33: getfield mSkipFiles : Landroid/util/SparseArray;
    //   36: iload_1
    //   37: invokevirtual get : (I)Ljava/lang/Object;
    //   40: ifnonnull -> 216
    //   43: aload_2
    //   44: getfield string : Ljava/lang/CharSequence;
    //   47: invokeinterface toString : ()Ljava/lang/String;
    //   52: astore #5
    //   54: aload_0
    //   55: aload_2
    //   56: getfield assetCookie : I
    //   59: iload_1
    //   60: invokespecial getCookieType : (II)I
    //   63: istore #6
    //   65: aload #4
    //   67: astore #7
    //   69: aload_0
    //   70: getfield mIsHasDrawables : Z
    //   73: ifeq -> 100
    //   76: aload #4
    //   78: astore #7
    //   80: aload_0
    //   81: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   84: ifnull -> 100
    //   87: aload_0
    //   88: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   91: iload #6
    //   93: aload #5
    //   95: invokevirtual getThemeFileStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   98: astore #7
    //   100: aload #7
    //   102: astore_2
    //   103: aload_0
    //   104: getfield mIsHasSystemDrawables : Z
    //   107: ifeq -> 149
    //   110: aload #7
    //   112: astore_2
    //   113: aload #7
    //   115: ifnonnull -> 149
    //   118: aload #7
    //   120: astore_2
    //   121: aload_0
    //   122: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   125: ifnull -> 149
    //   128: aload #7
    //   130: astore_2
    //   131: iload #6
    //   133: iconst_3
    //   134: if_icmpge -> 149
    //   137: aload_0
    //   138: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   141: iload #6
    //   143: aload #5
    //   145: invokevirtual getThemeFileStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   148: astore_2
    //   149: aload_2
    //   150: astore #7
    //   152: aload_0
    //   153: getfield mIsHasAcessDrawables : Z
    //   156: ifeq -> 189
    //   159: aload_2
    //   160: astore #7
    //   162: aload_2
    //   163: ifnonnull -> 189
    //   166: aload_2
    //   167: astore #7
    //   169: aload_0
    //   170: getfield mAccessibleResources : Landroid/content/res/OplusAccessibleResources;
    //   173: ifnull -> 189
    //   176: aload_0
    //   177: getfield mAccessibleResources : Landroid/content/res/OplusAccessibleResources;
    //   180: iload #6
    //   182: aload #5
    //   184: invokevirtual getAccessibleStream : (ILjava/lang/String;)Landroid/content/res/OplusThemeZipFile$ThemeFileInfo;
    //   187: astore #7
    //   189: aload #7
    //   191: ifnull -> 204
    //   194: aload #7
    //   196: getfield mInput : Ljava/io/InputStream;
    //   199: astore_2
    //   200: aload_0
    //   201: monitorexit
    //   202: aload_2
    //   203: areturn
    //   204: aload_0
    //   205: getfield mSkipFiles : Landroid/util/SparseArray;
    //   208: iload_1
    //   209: iconst_1
    //   210: invokestatic valueOf : (Z)Ljava/lang/Boolean;
    //   213: invokevirtual put : (ILjava/lang/Object;)V
    //   216: aload_0
    //   217: monitorexit
    //   218: aconst_null
    //   219: areturn
    //   220: astore_2
    //   221: aload_0
    //   222: monitorexit
    //   223: aload_2
    //   224: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #233	-> 2
    //   #234	-> 25
    //   #236	-> 29
    //   #237	-> 32
    //   #238	-> 32
    //   #239	-> 43
    //   #240	-> 54
    //   #241	-> 65
    //   #242	-> 87
    //   #244	-> 100
    //   #245	-> 137
    //   #247	-> 149
    //   #248	-> 176
    //   #250	-> 189
    //   #251	-> 194
    //   #252	-> 200
    //   #254	-> 204
    //   #256	-> 216
    //   #232	-> 220
    // Exception table:
    //   from	to	target	type
    //   2	21	220	finally
    //   32	43	220	finally
    //   43	54	220	finally
    //   54	65	220	finally
    //   69	76	220	finally
    //   80	87	220	finally
    //   87	100	220	finally
    //   103	110	220	finally
    //   121	128	220	finally
    //   137	149	220	finally
    //   152	159	220	finally
    //   169	176	220	finally
    //   176	189	220	finally
    //   194	200	220	finally
    //   204	216	220	finally
  }
  
  public CharSequence getThemeCharSequence(int paramInt) {
    if (!this.mIsHasValues)
      return null; 
    CharSequence charSequence1 = null, charSequence2 = null;
    CharSequence charSequence3 = charSequence1;
    try {
      int i = this.mCharSequences.indexOfKey(paramInt);
      if (i >= 0) {
        charSequence3 = charSequence1;
        charSequence2 = (CharSequence)this.mCharSequences.valueAt(i);
      } else {
        charSequence3 = charSequence1;
        if (this.mThemeResources != null) {
          charSequence3 = charSequence1;
          charSequence2 = this.mThemeResources.getThemeCharSequence(paramInt);
        } 
      } 
      if (charSequence2 != null) {
        charSequence3 = charSequence2;
        this.mCharSequences.put(paramInt, charSequence2);
      } 
    } catch (Exception exception) {
      Log.e("OplusBaseResourcesImpl", "getThemeCharSequence exception: ", exception);
      charSequence2 = charSequence3;
    } 
    return charSequence2;
  }
  
  public SparseArray<Boolean> getLoadArrary() {
    return this.mLoadArrary;
  }
  
  public boolean isHasDrawables() {
    return (this.mIsHasDrawables || this.mIsHasAcessDrawables || this.mIsHasSystemDrawables);
  }
  
  public int updateExConfiguration(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, Configuration paramConfiguration) {
    byte b = 0;
    paramOplusBaseResourcesImpl = typeCasting(paramOplusBaseResourcesImpl);
    int i = b;
    if (paramOplusBaseResourcesImpl != null) {
      Configuration configuration = paramOplusBaseResourcesImpl.getConfiguration();
      i = b;
      if (configuration != null) {
        i = b;
        if (paramConfiguration != null)
          i = configuration.diff(paramConfiguration); 
      } 
    } 
    return i;
  }
  
  public void checkUpdate(int paramInt, boolean paramBoolean) {
    boolean bool = OplusExtraConfiguration.needNewResources(paramInt);
    paramBoolean = OplusExtraConfiguration.needAccessNewResources(paramInt);
    if ((this.mThemeResources != null || this.mSystemThemeResources != null || this.mIconThemeResources != null) && bool) {
      clear();
      this.mIsHasValues = false;
      this.mIsHasDrawables = false;
      this.mIsHasSystemValues = false;
      this.mIsHasSystemDrawables = false;
      OplusThemeHelper.reset();
      OplusAppIconInfo.reset();
      OplusThemeResources oplusThemeResources = this.mThemeResources;
      if (oplusThemeResources != null) {
        oplusThemeResources.setResource(this);
        this.mThemeResources.checkUpdate();
        this.mIsHasValues = this.mThemeResources.hasValues();
        this.mIsHasDrawables = this.mThemeResources.hasDrawables();
      } 
      oplusThemeResources = this.mSystemThemeResources;
      if (oplusThemeResources != null) {
        oplusThemeResources.checkUpdate();
        this.mIsHasSystemValues = this.mSystemThemeResources.hasValues();
        this.mIsHasSystemDrawables = this.mSystemThemeResources.hasDrawables();
      } 
      oplusThemeResources = this.mIconThemeResources;
      if (oplusThemeResources != null)
        oplusThemeResources.checkUpdate(); 
    } 
    if (this.mAccessibleResources != null && paramBoolean) {
      clear();
      this.mIsHasAcessValues = false;
      this.mIsHasAcessDrawables = false;
      this.mAccessibleResources.checkUpdate();
      this.mIsHasAcessValues = this.mAccessibleResources.hasValues();
      this.mIsHasAcessDrawables = this.mAccessibleResources.hasDrawables();
    } 
  }
  
  public void getExValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean) {
    if (!this.mIsHasValues && !this.mIsHasAcessValues && !this.mIsHasSystemValues)
      return; 
    if ((paramTypedValue.type >= 16 && paramTypedValue.type <= 31) || paramTypedValue.type == 5) {
      Integer integer = getThemeInt(paramInt, paramTypedValue.resourceId, paramTypedValue.assetCookie);
      if (integer != null) {
        paramTypedValue.data = integer.intValue();
      } else if (paramTypedValue.type != 5) {
        integer = getColorValue(paramInt, paramTypedValue.resourceId, paramTypedValue.assetCookie);
        if (integer != null)
          paramTypedValue.data = integer.intValue(); 
      } 
    } 
  }
  
  public void initThemeResource(String paramString) {
    if (TextUtils.isEmpty(paramString) || "android".equals(paramString) || "oplus".equals(paramString)) {
      this.mSystemThemeResources = OplusThemeResources.getSystem(this);
      return;
    } 
    int i = StrictMode.allowThreadDiskWritesMask();
    try {
      this.mThemeResources = OplusThemeResourcesPackage.getThemeResources(this, paramString);
      this.mAccessibleResources = OplusAccessibleResources.getAccessResources(this, paramString);
    } finally {
      StrictMode.setThreadPolicyMask(i);
    } 
  }
  
  public ResourcesImpl typeCasting(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    return typeCasting(ResourcesImpl.class, paramOplusBaseResourcesImpl);
  }
  
  private <T> T typeCasting(Class<T> paramClass, Object paramObject) {
    if (paramObject != null && paramClass.isInstance(paramObject))
      return (T)paramObject; 
    return null;
  }
  
  private int getCookieType(int paramInt1, int paramInt2) {
    if (paramInt2 == 0)
      return paramInt2; 
    int i = this.mCookies.get(paramInt2);
    paramInt1 = i;
    if (i == 0) {
      int j = i;
      try {
        String str = getResourcePackageName(paramInt2);
        j = i;
        if ("android".equals(str)) {
          paramInt1 = 1;
        } else {
          j = i;
          if ("oplus".equals(str)) {
            paramInt1 = 2;
          } else {
            paramInt1 = 3;
          } 
        } 
        j = paramInt1;
        this.mCookies.put(paramInt2, paramInt1);
      } catch (NotFoundException notFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getCookieType. e = ");
        stringBuilder.append(notFoundException);
        Log.e("OplusBaseResourcesImpl", stringBuilder.toString());
        paramInt1 = j;
      } 
    } 
    return paramInt1;
  }
  
  private Integer getThemeInt(int paramInt1, int paramInt2) {
    return getThemeInt(paramInt1, 0, paramInt2);
  }
  
  private Integer getThemeInt(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mIsHasValues : Z
    //   6: ifne -> 24
    //   9: aload_0
    //   10: getfield mIsHasSystemValues : Z
    //   13: istore #4
    //   15: iload #4
    //   17: ifne -> 24
    //   20: aload_0
    //   21: monitorexit
    //   22: aconst_null
    //   23: areturn
    //   24: aconst_null
    //   25: astore #5
    //   27: aconst_null
    //   28: astore #6
    //   30: aload #5
    //   32: astore #7
    //   34: aload_0
    //   35: getfield mIntegers : Landroid/util/SparseArray;
    //   38: iload_1
    //   39: invokevirtual indexOfKey : (I)I
    //   42: istore #8
    //   44: iload #8
    //   46: iflt -> 70
    //   49: aload #5
    //   51: astore #7
    //   53: aload_0
    //   54: getfield mIntegers : Landroid/util/SparseArray;
    //   57: iload #8
    //   59: invokevirtual valueAt : (I)Ljava/lang/Object;
    //   62: checkcast java/lang/Integer
    //   65: astore #5
    //   67: goto -> 306
    //   70: aload #6
    //   72: astore #9
    //   74: aload #5
    //   76: astore #7
    //   78: aload_0
    //   79: getfield mIsHasValues : Z
    //   82: ifeq -> 154
    //   85: aload #6
    //   87: astore #9
    //   89: aload #5
    //   91: astore #7
    //   93: aload_0
    //   94: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   97: ifnull -> 154
    //   100: aload #5
    //   102: astore #7
    //   104: aload_0
    //   105: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   108: iload_1
    //   109: invokevirtual getThemeInt : (I)Ljava/lang/Integer;
    //   112: astore #5
    //   114: aload #5
    //   116: astore #9
    //   118: aload #5
    //   120: ifnonnull -> 154
    //   123: aload #5
    //   125: astore #9
    //   127: iload_2
    //   128: ifeq -> 154
    //   131: aload #5
    //   133: astore #9
    //   135: iload_2
    //   136: iload_1
    //   137: if_icmpeq -> 154
    //   140: aload #5
    //   142: astore #7
    //   144: aload_0
    //   145: getfield mThemeResources : Landroid/content/res/OplusThemeResources;
    //   148: iload_2
    //   149: invokevirtual getThemeInt : (I)Ljava/lang/Integer;
    //   152: astore #9
    //   154: aload #9
    //   156: astore #5
    //   158: aload #9
    //   160: astore #7
    //   162: aload_0
    //   163: getfield mIsHasSystemValues : Z
    //   166: ifeq -> 292
    //   169: aload #9
    //   171: astore #5
    //   173: aload #9
    //   175: ifnonnull -> 292
    //   178: aload #9
    //   180: astore #5
    //   182: aload #9
    //   184: astore #7
    //   186: aload_0
    //   187: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   190: ifnull -> 292
    //   193: aload #9
    //   195: astore #7
    //   197: aload_0
    //   198: iload_3
    //   199: iload_1
    //   200: invokespecial getCookieType : (II)I
    //   203: istore #8
    //   205: aload #9
    //   207: astore #6
    //   209: iload #8
    //   211: iconst_3
    //   212: if_icmpge -> 231
    //   215: aload #9
    //   217: astore #7
    //   219: aload_0
    //   220: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   223: iload_1
    //   224: iload #8
    //   226: invokevirtual getThemeInt : (II)Ljava/lang/Integer;
    //   229: astore #6
    //   231: aload #6
    //   233: astore #7
    //   235: aload_0
    //   236: iload_3
    //   237: iload_2
    //   238: invokespecial getCookieType : (II)I
    //   241: istore_3
    //   242: aload #6
    //   244: astore #5
    //   246: aload #6
    //   248: ifnonnull -> 292
    //   251: aload #6
    //   253: astore #5
    //   255: iload_2
    //   256: ifeq -> 292
    //   259: aload #6
    //   261: astore #5
    //   263: iload_2
    //   264: iload_1
    //   265: if_icmpeq -> 292
    //   268: aload #6
    //   270: astore #5
    //   272: iload_3
    //   273: iconst_3
    //   274: if_icmpge -> 292
    //   277: aload #6
    //   279: astore #7
    //   281: aload_0
    //   282: getfield mSystemThemeResources : Landroid/content/res/OplusThemeResourcesSystem;
    //   285: iload_2
    //   286: iload_3
    //   287: invokevirtual getThemeInt : (II)Ljava/lang/Integer;
    //   290: astore #5
    //   292: aload #5
    //   294: astore #7
    //   296: aload_0
    //   297: getfield mIntegers : Landroid/util/SparseArray;
    //   300: iload_1
    //   301: aload #5
    //   303: invokevirtual put : (ILjava/lang/Object;)V
    //   306: aload #5
    //   308: astore #7
    //   310: goto -> 352
    //   313: astore #5
    //   315: new java/lang/StringBuilder
    //   318: astore #9
    //   320: aload #9
    //   322: invokespecial <init> : ()V
    //   325: aload #9
    //   327: ldc 'getThemeInt. e = '
    //   329: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   332: pop
    //   333: aload #9
    //   335: aload #5
    //   337: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   340: pop
    //   341: ldc 'OplusBaseResourcesImpl'
    //   343: aload #9
    //   345: invokevirtual toString : ()Ljava/lang/String;
    //   348: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   351: pop
    //   352: aload_0
    //   353: monitorexit
    //   354: aload #7
    //   356: areturn
    //   357: astore #7
    //   359: aload_0
    //   360: monitorexit
    //   361: aload #7
    //   363: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #418	-> 2
    //   #419	-> 20
    //   #421	-> 24
    //   #423	-> 30
    //   #424	-> 44
    //   #425	-> 49
    //   #427	-> 70
    //   #428	-> 100
    //   #429	-> 114
    //   #430	-> 140
    //   #433	-> 154
    //   #434	-> 193
    //   #435	-> 205
    //   #436	-> 215
    //   #438	-> 231
    //   #439	-> 242
    //   #440	-> 277
    //   #443	-> 292
    //   #447	-> 306
    //   #445	-> 313
    //   #446	-> 315
    //   #448	-> 352
    //   #417	-> 357
    // Exception table:
    //   from	to	target	type
    //   2	15	357	finally
    //   34	44	313	java/lang/Exception
    //   34	44	357	finally
    //   53	67	313	java/lang/Exception
    //   53	67	357	finally
    //   78	85	313	java/lang/Exception
    //   78	85	357	finally
    //   93	100	313	java/lang/Exception
    //   93	100	357	finally
    //   104	114	313	java/lang/Exception
    //   104	114	357	finally
    //   144	154	313	java/lang/Exception
    //   144	154	357	finally
    //   162	169	313	java/lang/Exception
    //   162	169	357	finally
    //   186	193	313	java/lang/Exception
    //   186	193	357	finally
    //   197	205	313	java/lang/Exception
    //   197	205	357	finally
    //   219	231	313	java/lang/Exception
    //   219	231	357	finally
    //   235	242	313	java/lang/Exception
    //   235	242	357	finally
    //   281	292	313	java/lang/Exception
    //   281	292	357	finally
    //   296	306	313	java/lang/Exception
    //   296	306	357	finally
    //   315	352	357	finally
  }
  
  private Integer getColorValue(int paramInt1, int paramInt2) {
    return getColorValue(paramInt1, 0, paramInt2);
  }
  
  private Integer getColorValue(int paramInt1, int paramInt2, int paramInt3) {
    Integer integer4;
    if (!this.mIsHasAcessValues || paramInt1 == 0)
      return null; 
    Integer integer1 = null, integer2 = null;
    Integer integer3 = integer1;
    try {
      int i = this.mIntegers.indexOfKey(paramInt1);
      if (i >= 0) {
        integer3 = integer1;
        integer4 = (Integer)this.mIntegers.valueAt(i);
      } else {
        integer3 = integer1;
        i = getCookieType(paramInt3, paramInt1);
        integer4 = integer2;
        integer3 = integer1;
        if (this.mAccessibleResources != null) {
          integer4 = integer2;
          if (i == 3) {
            integer3 = integer1;
            integer2 = this.mAccessibleResources.getAccessibleInt(paramInt1);
            integer4 = integer2;
            if (integer2 == null) {
              integer4 = integer2;
              if (paramInt2 != 0) {
                integer4 = integer2;
                if (paramInt2 != paramInt1) {
                  integer3 = integer2;
                  paramInt3 = getCookieType(paramInt3, paramInt2);
                  integer4 = integer2;
                  if (paramInt3 == 3) {
                    integer3 = integer2;
                    integer4 = this.mAccessibleResources.getAccessibleInt(paramInt2);
                  } 
                } 
              } 
            } 
          } 
        } 
        integer3 = integer4;
        this.mIntegers.put(paramInt1, integer4);
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getColorValue. e = ");
      stringBuilder.append(exception);
      Log.e("OplusBaseResourcesImpl", stringBuilder.toString());
      integer4 = integer3;
    } 
    return integer4;
  }
  
  private void clear() {
    clearCache();
    this.mIntegers.clear();
    this.mCharSequences.clear();
    this.mSkipFiles.clear();
    this.mSkipIcons.clear();
    this.mLoadArrary.clear();
    this.mCookies.clear();
  }
  
  abstract OplusBaseConfiguration getConfiguration();
  
  abstract String getResourceName(int paramInt);
  
  abstract String getResourcePackageName(int paramInt);
  
  public abstract OplusBaseConfiguration getSystemConfiguration();
  
  abstract void getValue(int paramInt, TypedValue paramTypedValue, boolean paramBoolean);
}
