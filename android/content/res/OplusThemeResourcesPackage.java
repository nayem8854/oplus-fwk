package android.content.res;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class OplusThemeResourcesPackage extends OplusThemeResources {
  private final boolean DEBUG = true;
  
  private static final Map<String, WeakReference<OplusThemeResourcesPackage>> sPackageResources = new HashMap<>();
  
  private static final String TAG = "OplusThemeResourcesPackage";
  
  public OplusThemeResourcesPackage(OplusThemeResourcesPackage paramOplusThemeResourcesPackage, OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString, OplusThemeResources.MetaData paramMetaData) {
    super(paramOplusThemeResourcesPackage, paramOplusBaseResourcesImpl, paramString, paramMetaData);
  }
  
  public static OplusThemeResourcesPackage getThemeResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    Map<String, WeakReference<OplusThemeResourcesPackage>> map = null;
    synchronized (sPackageResources) {
      Map<String, WeakReference<OplusThemeResourcesPackage>> map1;
      OplusThemeResourcesPackage oplusThemeResourcesPackage;
      Map<String, WeakReference<OplusThemeResourcesPackage>> map2;
      if (sPackageResources.containsKey(paramString)) {
        map = sPackageResources;
        oplusThemeResourcesPackage = ((WeakReference<OplusThemeResourcesPackage>)map.get(paramString)).get();
      } 
      if (oplusThemeResourcesPackage == null) {
        OplusThemeResourcesPackage oplusThemeResourcesPackage1 = getTopLevelThemeResources(paramOplusBaseResourcesImpl, paramString);
        map2 = sPackageResources;
        WeakReference<OplusThemeResourcesPackage> weakReference = new WeakReference();
        this((T)oplusThemeResourcesPackage1);
        map2.put(paramString, weakReference);
      } else {
        map1 = map2;
        if (!map2.checkThemePackageExit()) {
          map2.checkUpdate();
          sPackageResources.remove(paramString);
          map1 = map2;
        } 
      } 
      return (OplusThemeResourcesPackage)map1;
    } 
  }
  
  public static OplusThemeResourcesPackage getTopLevelThemeResources(OplusBaseResourcesImpl paramOplusBaseResourcesImpl, String paramString) {
    setThemePath(paramOplusBaseResourcesImpl, paramString);
    OplusThemeResourcesPackage oplusThemeResourcesPackage = null;
    for (byte b = 0; b < sThemeMetaPath.length; b++)
      oplusThemeResourcesPackage = new OplusThemeResourcesPackage(oplusThemeResourcesPackage, paramOplusBaseResourcesImpl, paramString, sThemeMetaPath[b]); 
    return oplusThemeResourcesPackage;
  }
  
  public CharSequence getThemeCharSequence(int paramInt) {
    CharSequence charSequence1 = super.getThemeCharSequence(paramInt);
    CharSequence charSequence2 = charSequence1;
    if (charSequence1 == null) {
      charSequence2 = charSequence1;
      if (getSystem() != null)
        charSequence2 = getSystem().getThemeCharSequence(paramInt); 
    } 
    return charSequence2;
  }
  
  public OplusThemeZipFile.ThemeFileInfo getThemeFileStream(int paramInt, String paramString) {
    return getPackageThemeFileStream(paramInt, paramString);
  }
  
  protected boolean isMutiPackage() {
    return true;
  }
  
  public void setResource(OplusBaseResourcesImpl paramOplusBaseResourcesImpl) {
    super.setResource(paramOplusBaseResourcesImpl);
  }
}
