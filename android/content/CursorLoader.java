package android.content;

import android.database.Cursor;
import android.net.Uri;
import android.os.CancellationSignal;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Arrays;

@Deprecated
public class CursorLoader extends AsyncTaskLoader<Cursor> {
  CancellationSignal mCancellationSignal;
  
  Cursor mCursor;
  
  final Loader<Cursor>.ForceLoadContentObserver mObserver;
  
  String[] mProjection;
  
  String mSelection;
  
  String[] mSelectionArgs;
  
  String mSortOrder;
  
  Uri mUri;
  
  public Cursor loadInBackground() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual isLoadInBackgroundCanceled : ()Z
    //   6: ifne -> 125
    //   9: new android/os/CancellationSignal
    //   12: astore_1
    //   13: aload_1
    //   14: invokespecial <init> : ()V
    //   17: aload_0
    //   18: aload_1
    //   19: putfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_0
    //   25: invokevirtual getContext : ()Landroid/content/Context;
    //   28: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   31: aload_0
    //   32: getfield mUri : Landroid/net/Uri;
    //   35: aload_0
    //   36: getfield mProjection : [Ljava/lang/String;
    //   39: aload_0
    //   40: getfield mSelection : Ljava/lang/String;
    //   43: aload_0
    //   44: getfield mSelectionArgs : [Ljava/lang/String;
    //   47: aload_0
    //   48: getfield mSortOrder : Ljava/lang/String;
    //   51: aload_0
    //   52: getfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   55: invokevirtual query : (Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    //   58: astore_2
    //   59: aload_2
    //   60: ifnull -> 92
    //   63: aload_2
    //   64: invokeinterface getCount : ()I
    //   69: pop
    //   70: aload_2
    //   71: aload_0
    //   72: getfield mObserver : Landroid/content/Loader$ForceLoadContentObserver;
    //   75: invokeinterface registerContentObserver : (Landroid/database/ContentObserver;)V
    //   80: goto -> 92
    //   83: astore_1
    //   84: aload_2
    //   85: invokeinterface close : ()V
    //   90: aload_1
    //   91: athrow
    //   92: aload_0
    //   93: monitorenter
    //   94: aload_0
    //   95: aconst_null
    //   96: putfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   99: aload_0
    //   100: monitorexit
    //   101: aload_2
    //   102: areturn
    //   103: astore_1
    //   104: aload_0
    //   105: monitorexit
    //   106: aload_1
    //   107: athrow
    //   108: astore_1
    //   109: aload_0
    //   110: monitorenter
    //   111: aload_0
    //   112: aconst_null
    //   113: putfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   116: aload_0
    //   117: monitorexit
    //   118: aload_1
    //   119: athrow
    //   120: astore_1
    //   121: aload_0
    //   122: monitorexit
    //   123: aload_1
    //   124: athrow
    //   125: new android/os/OperationCanceledException
    //   128: astore_1
    //   129: aload_1
    //   130: invokespecial <init> : ()V
    //   133: aload_1
    //   134: athrow
    //   135: astore_1
    //   136: aload_0
    //   137: monitorexit
    //   138: aload_1
    //   139: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #64	-> 0
    //   #65	-> 2
    //   #68	-> 9
    //   #69	-> 22
    //   #71	-> 24
    //   #73	-> 59
    //   #76	-> 63
    //   #77	-> 70
    //   #81	-> 80
    //   #78	-> 83
    //   #79	-> 84
    //   #80	-> 90
    //   #83	-> 92
    //   #85	-> 92
    //   #86	-> 94
    //   #87	-> 99
    //   #83	-> 101
    //   #87	-> 103
    //   #85	-> 108
    //   #86	-> 111
    //   #87	-> 116
    //   #88	-> 118
    //   #87	-> 120
    //   #66	-> 125
    //   #69	-> 135
    // Exception table:
    //   from	to	target	type
    //   2	9	135	finally
    //   9	22	135	finally
    //   22	24	135	finally
    //   24	59	108	finally
    //   63	70	83	java/lang/RuntimeException
    //   63	70	108	finally
    //   70	80	83	java/lang/RuntimeException
    //   70	80	108	finally
    //   84	90	108	finally
    //   90	92	108	finally
    //   94	99	103	finally
    //   99	101	103	finally
    //   104	106	103	finally
    //   111	116	120	finally
    //   116	118	120	finally
    //   121	123	120	finally
    //   125	135	135	finally
    //   136	138	135	finally
  }
  
  public void cancelLoadInBackground() {
    // Byte code:
    //   0: aload_0
    //   1: invokespecial cancelLoadInBackground : ()V
    //   4: aload_0
    //   5: monitorenter
    //   6: aload_0
    //   7: getfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   10: ifnull -> 20
    //   13: aload_0
    //   14: getfield mCancellationSignal : Landroid/os/CancellationSignal;
    //   17: invokevirtual cancel : ()V
    //   20: aload_0
    //   21: monitorexit
    //   22: return
    //   23: astore_1
    //   24: aload_0
    //   25: monitorexit
    //   26: aload_1
    //   27: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #93	-> 0
    //   #95	-> 4
    //   #96	-> 6
    //   #97	-> 13
    //   #99	-> 20
    //   #100	-> 22
    //   #99	-> 23
    // Exception table:
    //   from	to	target	type
    //   6	13	23	finally
    //   13	20	23	finally
    //   20	22	23	finally
    //   24	26	23	finally
  }
  
  public void deliverResult(Cursor paramCursor) {
    if (isReset()) {
      if (paramCursor != null)
        paramCursor.close(); 
      return;
    } 
    Cursor cursor = this.mCursor;
    this.mCursor = paramCursor;
    if (isStarted())
      super.deliverResult(paramCursor); 
    if (cursor != null && cursor != paramCursor && !cursor.isClosed())
      cursor.close(); 
  }
  
  public CursorLoader(Context paramContext) {
    super(paramContext);
    this.mObserver = new Loader.ForceLoadContentObserver(this);
  }
  
  public CursorLoader(Context paramContext, Uri paramUri, String[] paramArrayOfString1, String paramString1, String[] paramArrayOfString2, String paramString2) {
    super(paramContext);
    this.mObserver = new Loader.ForceLoadContentObserver(this);
    this.mUri = paramUri;
    this.mProjection = paramArrayOfString1;
    this.mSelection = paramString1;
    this.mSelectionArgs = paramArrayOfString2;
    this.mSortOrder = paramString2;
  }
  
  protected void onStartLoading() {
    Cursor cursor = this.mCursor;
    if (cursor != null)
      deliverResult(cursor); 
    if (takeContentChanged() || this.mCursor == null)
      forceLoad(); 
  }
  
  protected void onStopLoading() {
    cancelLoad();
  }
  
  public void onCanceled(Cursor paramCursor) {
    if (paramCursor != null && !paramCursor.isClosed())
      paramCursor.close(); 
  }
  
  protected void onReset() {
    super.onReset();
    onStopLoading();
    Cursor cursor = this.mCursor;
    if (cursor != null && !cursor.isClosed())
      this.mCursor.close(); 
    this.mCursor = null;
  }
  
  public Uri getUri() {
    return this.mUri;
  }
  
  public void setUri(Uri paramUri) {
    this.mUri = paramUri;
  }
  
  public String[] getProjection() {
    return this.mProjection;
  }
  
  public void setProjection(String[] paramArrayOfString) {
    this.mProjection = paramArrayOfString;
  }
  
  public String getSelection() {
    return this.mSelection;
  }
  
  public void setSelection(String paramString) {
    this.mSelection = paramString;
  }
  
  public String[] getSelectionArgs() {
    return this.mSelectionArgs;
  }
  
  public void setSelectionArgs(String[] paramArrayOfString) {
    this.mSelectionArgs = paramArrayOfString;
  }
  
  public String getSortOrder() {
    return this.mSortOrder;
  }
  
  public void setSortOrder(String paramString) {
    this.mSortOrder = paramString;
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    super.dump(paramString, paramFileDescriptor, paramPrintWriter, paramArrayOfString);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mUri=");
    paramPrintWriter.println(this.mUri);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mProjection=");
    paramPrintWriter.println(Arrays.toString((Object[])this.mProjection));
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSelection=");
    paramPrintWriter.println(this.mSelection);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSelectionArgs=");
    paramPrintWriter.println(Arrays.toString((Object[])this.mSelectionArgs));
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mSortOrder=");
    paramPrintWriter.println(this.mSortOrder);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mCursor=");
    paramPrintWriter.println(this.mCursor);
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mContentChanged=");
    paramPrintWriter.println(this.mContentChanged);
  }
}
