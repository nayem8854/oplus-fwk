package android.content;

import android.os.Parcel;

public class OplusBaseIntent {
  int mOppoUserId = 0;
  
  int mIsFromGameSpace = 0;
  
  int mIsForFreeForm = 0;
  
  int mStackId = 0;
  
  int mPid = -1;
  
  int mUid = -1;
  
  int mCallingUid = -1;
  
  public static final int FLAG_RECEIVER_OPLUSQUEUE = 524288;
  
  public static final int FLAG_RECEIVER_QUEUE_PRIOR = 1048576;
  
  public static final int OPLUS_FLAG_ACTIVITY_KEEP_RESUM_WHEN_SLEEPING = 1073741824;
  
  public static final int OPLUS_FLAG_ACTIVITY_SECURE_POLICY = -2147483648;
  
  public static final int OPLUS_FLAG_MULTI_APP_DIRECT_MULTI_APP = 4096;
  
  public static final int OPLUS_FLAG_MULTI_APP_SKIP_CHOOSER = 2048;
  
  public static final int OPLUS_FLAG_MUTIL_APP = 1024;
  
  public static final int OPLUS_FLAG_MUTIL_CHOOSER = 512;
  
  public static final int OPLUS_FLAG_RECEIVER_OPLUSQUEUE = 2;
  
  public static final int OPLUS_FLAG_RECEIVER_QUEUE_PRIOR = 1;
  
  int mOppoFlags;
  
  OplusBaseIntent(OplusBaseIntent paramOplusBaseIntent, int paramInt) {
    this.mOppoUserId = paramOplusBaseIntent.mOppoUserId;
    this.mIsFromGameSpace = paramOplusBaseIntent.mIsFromGameSpace;
    this.mIsForFreeForm = paramOplusBaseIntent.mIsForFreeForm;
    this.mStackId = paramOplusBaseIntent.mStackId;
    this.mOppoFlags = paramOplusBaseIntent.mOppoFlags;
    this.mCallingUid = paramOplusBaseIntent.mCallingUid;
  }
  
  public int fillIn(OplusBaseIntent paramOplusBaseIntent, int paramInt) {
    this.mOppoUserId = paramOplusBaseIntent.mOppoUserId;
    this.mIsFromGameSpace = paramOplusBaseIntent.mIsFromGameSpace;
    this.mIsForFreeForm = paramOplusBaseIntent.mIsForFreeForm;
    this.mStackId = paramOplusBaseIntent.mStackId;
    this.mOppoFlags |= paramOplusBaseIntent.mOppoFlags;
    this.mCallingUid = paramOplusBaseIntent.mCallingUid;
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mOppoUserId);
    paramParcel.writeInt(this.mIsFromGameSpace);
    paramParcel.writeInt(this.mIsForFreeForm);
    paramParcel.writeInt(this.mStackId);
    paramParcel.writeInt(this.mOppoFlags);
    paramParcel.writeInt(this.mCallingUid);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mOppoUserId = paramParcel.readInt();
    this.mIsFromGameSpace = paramParcel.readInt();
    this.mIsForFreeForm = paramParcel.readInt();
    this.mStackId = paramParcel.readInt();
    this.mOppoFlags = paramParcel.readInt();
    this.mCallingUid = paramParcel.readInt();
  }
  
  public int getOplusUserId() {
    return this.mOppoUserId;
  }
  
  public void setOplusUserId(int paramInt) {
    this.mOppoUserId = paramInt;
  }
  
  public int getIsFromGameSpace() {
    return this.mIsFromGameSpace;
  }
  
  public void setIsFromGameSpace(int paramInt) {
    this.mIsFromGameSpace = paramInt;
  }
  
  public int getIsForFreeForm() {
    return this.mIsForFreeForm;
  }
  
  public void setIsForFreeForm(int paramInt) {
    this.mIsForFreeForm = paramInt;
  }
  
  public int getLaunchStackId() {
    return this.mStackId;
  }
  
  public void setLaunchStackId(int paramInt) {
    this.mStackId = paramInt;
  }
  
  public int getOplusFlags() {
    return this.mOppoFlags;
  }
  
  public void setOplusFlags(int paramInt) {
    this.mOppoFlags = paramInt;
  }
  
  public void addOplusFlags(int paramInt) {
    this.mOppoFlags |= paramInt;
  }
  
  public void removeOplusFlags(int paramInt) {
    this.mOppoFlags &= paramInt ^ 0xFFFFFFFF;
  }
  
  public void setPid(int paramInt) {
    this.mPid = paramInt;
  }
  
  public int getPid() {
    return this.mPid;
  }
  
  public void setUid(int paramInt) {
    this.mUid = paramInt;
  }
  
  public int getUid() {
    return this.mUid;
  }
  
  public void setCallingUid(int paramInt) {
    this.mCallingUid = paramInt;
  }
  
  public int getCallingUid() {
    return this.mCallingUid;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    if (this.mOppoFlags != 0) {
      stringBuilder.append(" oflg=0x");
      stringBuilder.append(Integer.toHexString(this.mOppoFlags));
    } 
    if (this.mOppoUserId != 0) {
      stringBuilder.append(" ouserid=");
      stringBuilder.append(this.mOppoUserId);
    } 
    if (this.mIsForFreeForm != 0) {
      stringBuilder.append(" freeform=");
      stringBuilder.append(this.mIsForFreeForm);
    } 
    if (this.mIsFromGameSpace != 0) {
      stringBuilder.append(" gs=");
      stringBuilder.append(this.mIsFromGameSpace);
    } 
    if (this.mStackId != 0) {
      stringBuilder.append(" stackid=");
      stringBuilder.append(this.mStackId);
    } 
    if (this.mCallingUid != -1) {
      stringBuilder.append(" mCallingUid=");
      stringBuilder.append(this.mCallingUid);
    } 
    return stringBuilder.toString();
  }
  
  OplusBaseIntent() {}
}
