package android.content;

import android.os.Parcel;

public class OplusBaseJobParameters {
  private int cpuLevel;
  
  private String oppoExtraStr;
  
  public void setCpuLevel(int paramInt) {
    this.cpuLevel = paramInt;
  }
  
  public int getCpuLevel() {
    return this.cpuLevel;
  }
  
  public void setOplusExtraStr(String paramString) {
    this.oppoExtraStr = paramString;
  }
  
  public String getOplusExtraStr() {
    return this.oppoExtraStr;
  }
  
  public OplusBaseJobParameters() {}
  
  public OplusBaseJobParameters(Parcel paramParcel) {}
  
  public void initJobParameters(Parcel paramParcel) {
    this.cpuLevel = paramParcel.readInt();
    this.oppoExtraStr = paramParcel.readString();
  }
  
  public void writeToParcelJobParameters(Parcel paramParcel) {
    paramParcel.writeInt(this.cpuLevel);
    paramParcel.writeString(this.oppoExtraStr);
  }
}
