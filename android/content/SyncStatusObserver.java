package android.content;

public interface SyncStatusObserver {
  void onStatusChanged(int paramInt);
}
