package android.content.integrity;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public abstract class AtomicFormula extends IntegrityFormula {
  public static final int APP_CERTIFICATE = 1;
  
  public static final int EQ = 0;
  
  public static final int GT = 1;
  
  public static final int GTE = 2;
  
  public static final int INSTALLER_CERTIFICATE = 3;
  
  public static final int INSTALLER_NAME = 2;
  
  public static final int PACKAGE_NAME = 0;
  
  public static final int PRE_INSTALLED = 5;
  
  public static final int STAMP_CERTIFICATE_HASH = 7;
  
  public static final int STAMP_TRUSTED = 6;
  
  public static final int VERSION_CODE = 4;
  
  private final int mKey;
  
  public AtomicFormula(int paramInt) {
    Preconditions.checkArgument(isValidKey(paramInt), String.format("Unknown key: %d", new Object[] { Integer.valueOf(paramInt) }));
    this.mKey = paramInt;
  }
  
  class LongAtomicFormula extends AtomicFormula implements Parcelable {
    public LongAtomicFormula(AtomicFormula this$0) {
      super(this$0);
      boolean bool;
      if (this$0 == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with LongAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      this.mValue = null;
      this.mOperator = null;
    }
    
    public LongAtomicFormula(AtomicFormula this$0, int param1Int1, long param1Long) {
      super(this$0);
      if (this$0 == 4) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with LongAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      boolean bool = isValidOperator(param1Int1);
      str = String.format("Unknown operator: %d", new Object[] { Integer.valueOf(param1Int1) });
      Preconditions.checkArgument(bool, str);
      this.mOperator = Integer.valueOf(param1Int1);
      this.mValue = Long.valueOf(param1Long);
    }
    
    LongAtomicFormula(AtomicFormula this$0) {
      super(this$0.readInt());
      this.mValue = Long.valueOf(this$0.readLong());
      this.mOperator = Integer.valueOf(this$0.readInt());
    }
    
    public static final Parcelable.Creator<LongAtomicFormula> CREATOR = new Parcelable.Creator<LongAtomicFormula>() {
        public AtomicFormula.LongAtomicFormula createFromParcel(Parcel param1Parcel) {
          return new AtomicFormula.LongAtomicFormula(param1Parcel);
        }
        
        public AtomicFormula.LongAtomicFormula[] newArray(int param1Int) {
          return new AtomicFormula.LongAtomicFormula[param1Int];
        }
      };
    
    private final Integer mOperator;
    
    private final Long mValue;
    
    public int getTag() {
      return 2;
    }
    
    public boolean matches(AppInstallMetadata param1AppInstallMetadata) {
      Long long_ = this.mValue;
      boolean bool1 = false, bool2 = false, bool3 = false;
      if (long_ == null || this.mOperator == null)
        return false; 
      long l = getLongMetadataValue(param1AppInstallMetadata, getKey());
      int i = this.mOperator.intValue();
      if (i != 0) {
        if (i != 1) {
          if (i == 2) {
            bool2 = bool3;
            if (l >= this.mValue.longValue())
              bool2 = true; 
            return bool2;
          } 
          Integer integer = this.mOperator;
          throw new IllegalArgumentException(String.format("Unexpected operator %d", new Object[] { integer }));
        } 
        bool2 = bool1;
        if (l > this.mValue.longValue())
          bool2 = true; 
        return bool2;
      } 
      if (l == this.mValue.longValue())
        bool2 = true; 
      return bool2;
    }
    
    public boolean isAppCertificateFormula() {
      return false;
    }
    
    public boolean isInstallerFormula() {
      return false;
    }
    
    public String toString() {
      if (this.mValue == null || this.mOperator == null)
        return String.format("(%s)", new Object[] { keyToString(getKey()) }); 
      String str1 = keyToString(getKey()), str2 = operatorToString(this.mOperator.intValue());
      Long long_ = this.mValue;
      return String.format("(%s %s %s)", new Object[] { str1, str2, long_ });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (getKey() != param1Object.getKey() || this.mValue != ((LongAtomicFormula)param1Object).mValue || this.mOperator != ((LongAtomicFormula)param1Object).mOperator)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(getKey()), this.mOperator, this.mValue });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      if (this.mValue != null && this.mOperator != null) {
        param1Parcel.writeInt(getKey());
        param1Parcel.writeLong(this.mValue.longValue());
        param1Parcel.writeInt(this.mOperator.intValue());
        return;
      } 
      throw new IllegalStateException("Cannot write an empty LongAtomicFormula.");
    }
    
    public Long getValue() {
      return this.mValue;
    }
    
    public Integer getOperator() {
      return this.mOperator;
    }
    
    private static boolean isValidOperator(int param1Int) {
      boolean bool1 = true, bool2 = bool1;
      if (param1Int != 0) {
        bool2 = bool1;
        if (param1Int != 1)
          if (param1Int == 2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          }  
      } 
      return bool2;
    }
    
    private static long getLongMetadataValue(AppInstallMetadata param1AppInstallMetadata, int param1Int) {
      if (param1Int == 4)
        return param1AppInstallMetadata.getVersionCode(); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unexpected key in IntAtomicFormula");
      stringBuilder.append(param1Int);
      throw new IllegalStateException(stringBuilder.toString());
    }
  }
  
  class null implements Parcelable.Creator<LongAtomicFormula> {
    public AtomicFormula.LongAtomicFormula createFromParcel(Parcel param1Parcel) {
      return new AtomicFormula.LongAtomicFormula(param1Parcel);
    }
    
    public AtomicFormula.LongAtomicFormula[] newArray(int param1Int) {
      return new AtomicFormula.LongAtomicFormula[param1Int];
    }
  }
  
  class StringAtomicFormula extends AtomicFormula implements Parcelable {
    public StringAtomicFormula(AtomicFormula this$0) {
      super(this$0);
      boolean bool;
      if (this$0 == null || this$0 == true || this$0 == 3 || this$0 == 2 || this$0 == 7) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with StringAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      this.mValue = null;
      this.mIsHashedValue = null;
    }
    
    public StringAtomicFormula(AtomicFormula this$0, String param1String, boolean param1Boolean) {
      super(this$0);
      boolean bool;
      if (this$0 == null || this$0 == true || this$0 == 3 || this$0 == 2 || this$0 == 7) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with StringAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      this.mValue = param1String;
      this.mIsHashedValue = Boolean.valueOf(param1Boolean);
    }
    
    public StringAtomicFormula(AtomicFormula this$0, String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: iload_1
      //   2: invokespecial <init> : (I)V
      //   5: iconst_0
      //   6: istore_3
      //   7: iload_1
      //   8: ifeq -> 41
      //   11: iload_1
      //   12: iconst_1
      //   13: if_icmpeq -> 41
      //   16: iload_1
      //   17: iconst_3
      //   18: if_icmpeq -> 41
      //   21: iload_1
      //   22: iconst_2
      //   23: if_icmpeq -> 41
      //   26: iload_1
      //   27: bipush #7
      //   29: if_icmpne -> 35
      //   32: goto -> 41
      //   35: iconst_0
      //   36: istore #4
      //   38: goto -> 44
      //   41: iconst_1
      //   42: istore #4
      //   44: iload_1
      //   45: invokestatic keyToString : (I)Ljava/lang/String;
      //   48: astore #5
      //   50: ldc 'Key %s cannot be used with StringAtomicFormula'
      //   52: iconst_1
      //   53: anewarray java/lang/Object
      //   56: dup
      //   57: iconst_0
      //   58: aload #5
      //   60: aastore
      //   61: invokestatic format : (Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
      //   64: astore #5
      //   66: iload #4
      //   68: aload #5
      //   70: invokestatic checkArgument : (ZLjava/lang/Object;)V
      //   73: iload_1
      //   74: aload_2
      //   75: invokestatic hashValue : (ILjava/lang/String;)Ljava/lang/String;
      //   78: astore #5
      //   80: aload_0
      //   81: aload #5
      //   83: putfield mValue : Ljava/lang/String;
      //   86: iload_1
      //   87: iconst_1
      //   88: if_icmpeq -> 114
      //   91: iload_1
      //   92: iconst_3
      //   93: if_icmpeq -> 114
      //   96: iload_1
      //   97: bipush #7
      //   99: if_icmpeq -> 114
      //   102: iload_3
      //   103: istore #4
      //   105: aload #5
      //   107: aload_2
      //   108: invokevirtual equals : (Ljava/lang/Object;)Z
      //   111: ifne -> 117
      //   114: iconst_1
      //   115: istore #4
      //   117: aload_0
      //   118: iload #4
      //   120: invokestatic valueOf : (Z)Ljava/lang/Boolean;
      //   123: putfield mIsHashedValue : Ljava/lang/Boolean;
      //   126: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #360	-> 0
      //   #361	-> 5
      //   #368	-> 44
      //   #367	-> 50
      //   #361	-> 66
      //   #369	-> 73
      //   #370	-> 86
      //   #374	-> 102
      //   #371	-> 117
      //   #375	-> 126
    }
    
    StringAtomicFormula(AtomicFormula this$0) {
      super(this$0.readInt());
      boolean bool;
      this.mValue = this$0.readStringNoHelper();
      if (this$0.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mIsHashedValue = Boolean.valueOf(bool);
    }
    
    public static final Parcelable.Creator<StringAtomicFormula> CREATOR = new Parcelable.Creator<StringAtomicFormula>() {
        public AtomicFormula.StringAtomicFormula createFromParcel(Parcel param1Parcel) {
          return new AtomicFormula.StringAtomicFormula(param1Parcel);
        }
        
        public AtomicFormula.StringAtomicFormula[] newArray(int param1Int) {
          return new AtomicFormula.StringAtomicFormula[param1Int];
        }
      };
    
    private final Boolean mIsHashedValue;
    
    private final String mValue;
    
    public int getTag() {
      return 1;
    }
    
    public boolean matches(AppInstallMetadata param1AppInstallMetadata) {
      if (this.mValue == null || this.mIsHashedValue == null)
        return false; 
      return getMetadataValue(param1AppInstallMetadata, getKey()).contains(this.mValue);
    }
    
    public boolean isAppCertificateFormula() {
      int i = getKey();
      boolean bool = true;
      if (i != 1)
        bool = false; 
      return bool;
    }
    
    public boolean isInstallerFormula() {
      return (getKey() == 2 || getKey() == 3);
    }
    
    public String toString() {
      if (this.mValue == null || this.mIsHashedValue == null)
        return String.format("(%s)", new Object[] { keyToString(getKey()) }); 
      return String.format("(%s %s %s)", new Object[] { keyToString(getKey()), operatorToString(0), this.mValue });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (getKey() != param1Object.getKey() || !Objects.equals(this.mValue, ((StringAtomicFormula)param1Object).mValue))
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(getKey()), this.mValue });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      if (this.mValue != null && this.mIsHashedValue != null) {
        param1Parcel.writeInt(getKey());
        param1Parcel.writeStringNoHelper(this.mValue);
        param1Parcel.writeByte((byte)this.mIsHashedValue.booleanValue());
        return;
      } 
      throw new IllegalStateException("Cannot write an empty StringAtomicFormula.");
    }
    
    public String getValue() {
      return this.mValue;
    }
    
    public Boolean getIsHashedValue() {
      return this.mIsHashedValue;
    }
    
    private static List<String> getMetadataValue(AppInstallMetadata param1AppInstallMetadata, int param1Int) {
      StringBuilder stringBuilder;
      if (param1Int != 0) {
        if (param1Int != 1) {
          if (param1Int != 2) {
            if (param1Int != 3) {
              if (param1Int == 7)
                return Collections.singletonList(param1AppInstallMetadata.getStampCertificateHash()); 
              stringBuilder = new StringBuilder();
              stringBuilder.append("Unexpected key in StringAtomicFormula: ");
              stringBuilder.append(param1Int);
              throw new IllegalStateException(stringBuilder.toString());
            } 
            return stringBuilder.getInstallerCertificates();
          } 
          return Collections.singletonList(stringBuilder.getInstallerName());
        } 
        return stringBuilder.getAppCertificates();
      } 
      return Collections.singletonList(stringBuilder.getPackageName());
    }
    
    private static String hashValue(int param1Int, String param1String) {
      if (param1String.length() > 32 && (
        param1Int == 0 || param1Int == 2))
        return hash(param1String); 
      return param1String;
    }
    
    private static String hash(String param1String) {
      try {
        MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
        byte[] arrayOfByte = messageDigest.digest(param1String.getBytes(StandardCharsets.UTF_8));
        return IntegrityUtils.getHexDigest(arrayOfByte);
      } catch (NoSuchAlgorithmException noSuchAlgorithmException) {
        throw new RuntimeException("SHA-256 algorithm not found", noSuchAlgorithmException);
      } 
    }
  }
  
  class null implements Parcelable.Creator<StringAtomicFormula> {
    public AtomicFormula.StringAtomicFormula createFromParcel(Parcel param1Parcel) {
      return new AtomicFormula.StringAtomicFormula(param1Parcel);
    }
    
    public AtomicFormula.StringAtomicFormula[] newArray(int param1Int) {
      return new AtomicFormula.StringAtomicFormula[param1Int];
    }
  }
  
  class BooleanAtomicFormula extends AtomicFormula implements Parcelable {
    public BooleanAtomicFormula(AtomicFormula this$0) {
      super(this$0);
      boolean bool;
      if (this$0 == 5 || this$0 == 6) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with BooleanAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      this.mValue = null;
    }
    
    public BooleanAtomicFormula(AtomicFormula this$0, boolean param1Boolean) {
      super(this$0);
      boolean bool;
      if (this$0 == 5 || this$0 == 6) {
        bool = true;
      } else {
        bool = false;
      } 
      String str = keyToString(this$0);
      str = String.format("Key %s cannot be used with BooleanAtomicFormula", new Object[] { str });
      Preconditions.checkArgument(bool, str);
      this.mValue = Boolean.valueOf(param1Boolean);
    }
    
    BooleanAtomicFormula(AtomicFormula this$0) {
      super(this$0.readInt());
      boolean bool;
      if (this$0.readByte() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mValue = Boolean.valueOf(bool);
    }
    
    public static final Parcelable.Creator<BooleanAtomicFormula> CREATOR = new Parcelable.Creator<BooleanAtomicFormula>() {
        public AtomicFormula.BooleanAtomicFormula createFromParcel(Parcel param1Parcel) {
          return new AtomicFormula.BooleanAtomicFormula(param1Parcel);
        }
        
        public AtomicFormula.BooleanAtomicFormula[] newArray(int param1Int) {
          return new AtomicFormula.BooleanAtomicFormula[param1Int];
        }
      };
    
    private final Boolean mValue;
    
    public int getTag() {
      return 3;
    }
    
    public boolean matches(AppInstallMetadata param1AppInstallMetadata) {
      Boolean bool = this.mValue;
      boolean bool1 = false;
      if (bool == null)
        return false; 
      if (getBooleanMetadataValue(param1AppInstallMetadata, getKey()) == this.mValue.booleanValue())
        bool1 = true; 
      return bool1;
    }
    
    public boolean isAppCertificateFormula() {
      return false;
    }
    
    public boolean isInstallerFormula() {
      return false;
    }
    
    public String toString() {
      if (this.mValue == null)
        return String.format("(%s)", new Object[] { keyToString(getKey()) }); 
      return String.format("(%s %s %s)", new Object[] { keyToString(getKey()), operatorToString(0), this.mValue });
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (getKey() != param1Object.getKey() || this.mValue != ((BooleanAtomicFormula)param1Object).mValue)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(getKey()), this.mValue });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      if (this.mValue != null) {
        param1Parcel.writeInt(getKey());
        param1Parcel.writeByte((byte)this.mValue.booleanValue());
        return;
      } 
      throw new IllegalStateException("Cannot write an empty BooleanAtomicFormula.");
    }
    
    public Boolean getValue() {
      return this.mValue;
    }
    
    private static boolean getBooleanMetadataValue(AppInstallMetadata param1AppInstallMetadata, int param1Int) {
      StringBuilder stringBuilder;
      if (param1Int != 5) {
        if (param1Int == 6)
          return param1AppInstallMetadata.isStampTrusted(); 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected key in BooleanAtomicFormula: ");
        stringBuilder.append(param1Int);
        throw new IllegalStateException(stringBuilder.toString());
      } 
      return stringBuilder.isPreInstalled();
    }
  }
  
  class null implements Parcelable.Creator<BooleanAtomicFormula> {
    public AtomicFormula.BooleanAtomicFormula createFromParcel(Parcel param1Parcel) {
      return new AtomicFormula.BooleanAtomicFormula(param1Parcel);
    }
    
    public AtomicFormula.BooleanAtomicFormula[] newArray(int param1Int) {
      return new AtomicFormula.BooleanAtomicFormula[param1Int];
    }
  }
  
  public int getKey() {
    return this.mKey;
  }
  
  static String keyToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown key ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      case 7:
        return "STAMP_CERTIFICATE_HASH";
      case 6:
        return "STAMP_TRUSTED";
      case 5:
        return "PRE_INSTALLED";
      case 4:
        return "VERSION_CODE";
      case 3:
        return "INSTALLER_CERTIFICATE";
      case 2:
        return "INSTALLER_NAME";
      case 1:
        return "APP_CERTIFICATE";
      case 0:
        break;
    } 
    return "PACKAGE_NAME";
  }
  
  static String operatorToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return "GTE"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown operator ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "GT";
    } 
    return "EQ";
  }
  
  private static boolean isValidKey(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 0) {
      bool2 = bool1;
      if (paramInt != 1) {
        bool2 = bool1;
        if (paramInt != 4) {
          bool2 = bool1;
          if (paramInt != 2) {
            bool2 = bool1;
            if (paramInt != 3) {
              bool2 = bool1;
              if (paramInt != 5) {
                bool2 = bool1;
                if (paramInt != 6)
                  if (paramInt == 7) {
                    bool2 = bool1;
                  } else {
                    bool2 = false;
                  }  
              } 
            } 
          } 
        } 
      } 
    } 
    return bool2;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Key implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Operator implements Annotation {}
}
