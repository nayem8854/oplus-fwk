package android.content.integrity;

import android.annotation.SystemApi;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@SystemApi
public class RuleSet {
  private final List<Rule> mRules;
  
  private final String mVersion;
  
  private RuleSet(String paramString, List<Rule> paramList) {
    this.mVersion = paramString;
    this.mRules = Collections.unmodifiableList(paramList);
  }
  
  public String getVersion() {
    return this.mVersion;
  }
  
  public List<Rule> getRules() {
    return this.mRules;
  }
  
  public static class Builder {
    private List<Rule> mRules = new ArrayList<>();
    
    private String mVersion;
    
    public Builder setVersion(String param1String) {
      this.mVersion = param1String;
      return this;
    }
    
    public Builder addRules(List<Rule> param1List) {
      this.mRules.addAll(param1List);
      return this;
    }
    
    public RuleSet build() {
      Objects.requireNonNull(this.mVersion);
      return new RuleSet(this.mVersion, this.mRules);
    }
  }
}
