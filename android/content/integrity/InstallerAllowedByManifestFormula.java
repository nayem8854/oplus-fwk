package android.content.integrity;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;
import java.util.Map;

public class InstallerAllowedByManifestFormula extends IntegrityFormula implements Parcelable {
  public InstallerAllowedByManifestFormula() {}
  
  private InstallerAllowedByManifestFormula(Parcel paramParcel) {}
  
  public static final Parcelable.Creator<InstallerAllowedByManifestFormula> CREATOR = new Parcelable.Creator<InstallerAllowedByManifestFormula>() {
      public InstallerAllowedByManifestFormula createFromParcel(Parcel param1Parcel) {
        return new InstallerAllowedByManifestFormula(param1Parcel);
      }
      
      public InstallerAllowedByManifestFormula[] newArray(int param1Int) {
        return new InstallerAllowedByManifestFormula[param1Int];
      }
    };
  
  public static final String INSTALLER_CERTIFICATE_NOT_EVALUATED = "";
  
  public int getTag() {
    return 4;
  }
  
  public boolean matches(AppInstallMetadata paramAppInstallMetadata) {
    Map<String, String> map = paramAppInstallMetadata.getAllowedInstallersAndCertificates();
    return (map.isEmpty() || 
      installerInAllowedInstallersFromManifest(paramAppInstallMetadata, map));
  }
  
  public boolean isAppCertificateFormula() {
    return false;
  }
  
  public boolean isInstallerFormula() {
    return true;
  }
  
  private static boolean installerInAllowedInstallersFromManifest(AppInstallMetadata paramAppInstallMetadata, Map<String, String> paramMap) {
    String str = paramAppInstallMetadata.getInstallerName();
    if (!paramMap.containsKey(str))
      return false; 
    str = paramMap.get(str);
    if (!str.equals("")) {
      List<String> list = paramAppInstallMetadata.getInstallerCertificates();
      paramAppInstallMetadata = (AppInstallMetadata)paramMap.get(paramAppInstallMetadata.getInstallerName());
      return list.contains(paramAppInstallMetadata);
    } 
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {}
}
