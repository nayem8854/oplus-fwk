package android.content.integrity;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class CompoundFormula extends IntegrityFormula implements Parcelable {
  public static final int AND = 0;
  
  public static final Parcelable.Creator<CompoundFormula> CREATOR = new Parcelable.Creator<CompoundFormula>() {
      public CompoundFormula createFromParcel(Parcel param1Parcel) {
        return new CompoundFormula(param1Parcel);
      }
      
      public CompoundFormula[] newArray(int param1Int) {
        return new CompoundFormula[param1Int];
      }
    };
  
  public static final int NOT = 2;
  
  public static final int OR = 1;
  
  private final int mConnector;
  
  private final List<IntegrityFormula> mFormulas;
  
  public CompoundFormula(int paramInt, List<IntegrityFormula> paramList) {
    boolean bool = isValidConnector(paramInt);
    String str = String.format("Unknown connector: %d", new Object[] { Integer.valueOf(paramInt) });
    Preconditions.checkArgument(bool, str);
    validateFormulas(paramInt, paramList);
    this.mConnector = paramInt;
    this.mFormulas = Collections.unmodifiableList(paramList);
  }
  
  CompoundFormula(Parcel paramParcel) {
    boolean bool;
    this.mConnector = paramParcel.readInt();
    int i = paramParcel.readInt();
    if (i >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Must have non-negative length. Got ");
    stringBuilder.append(i);
    Preconditions.checkArgument(bool, stringBuilder.toString());
    this.mFormulas = new ArrayList<>(i);
    for (byte b = 0; b < i; b++)
      this.mFormulas.add(IntegrityFormula.readFromParcel(paramParcel)); 
    validateFormulas(this.mConnector, this.mFormulas);
  }
  
  public int getConnector() {
    return this.mConnector;
  }
  
  public List<IntegrityFormula> getFormulas() {
    return this.mFormulas;
  }
  
  public int getTag() {
    return 0;
  }
  
  public boolean matches(AppInstallMetadata paramAppInstallMetadata) {
    _$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc _$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc;
    int i = getConnector();
    if (i != 0) {
      StringBuilder stringBuilder;
      if (i != 1) {
        if (i == 2)
          return ((IntegrityFormula)getFormulas().get(0)).matches(paramAppInstallMetadata) ^ true; 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown connector ");
        stringBuilder.append(getConnector());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      Stream<IntegrityFormula> stream1 = getFormulas().stream();
      _$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc = new _$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc((AppInstallMetadata)stringBuilder);
      return stream1.anyMatch(_$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc);
    } 
    Stream<IntegrityFormula> stream = getFormulas().stream();
    _$$Lambda$CompoundFormula$eA9NEY6uQ5Etti30l5BCpddAf1g _$$Lambda$CompoundFormula$eA9NEY6uQ5Etti30l5BCpddAf1g = new _$$Lambda$CompoundFormula$eA9NEY6uQ5Etti30l5BCpddAf1g((AppInstallMetadata)_$$Lambda$CompoundFormula$pW6rbPB_I2Vr7qv1hY_yfhAK2Fc);
    return stream.allMatch(_$$Lambda$CompoundFormula$eA9NEY6uQ5Etti30l5BCpddAf1g);
  }
  
  public boolean isAppCertificateFormula() {
    return getFormulas().stream().anyMatch((Predicate)_$$Lambda$CompoundFormula$M_8xxsgv81_KQXrkqgoTu52hdSc.INSTANCE);
  }
  
  public boolean isInstallerFormula() {
    return getFormulas().stream().anyMatch((Predicate)_$$Lambda$CompoundFormula$uDdqwcHo8K9__Cad__RPzb_jKiw.INSTANCE);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    if (this.mFormulas.size() == 1) {
      stringBuilder.append(String.format("%s ", new Object[] { connectorToString(this.mConnector) }));
      stringBuilder.append(((IntegrityFormula)this.mFormulas.get(0)).toString());
    } else {
      for (byte b = 0; b < this.mFormulas.size(); b++) {
        if (b > 0)
          stringBuilder.append(String.format(" %s ", new Object[] { connectorToString(this.mConnector) })); 
        stringBuilder.append(((IntegrityFormula)this.mFormulas.get(b)).toString());
      } 
    } 
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mConnector != ((CompoundFormula)paramObject).mConnector || !this.mFormulas.equals(((CompoundFormula)paramObject).mFormulas))
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mConnector), this.mFormulas });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mConnector);
    paramParcel.writeInt(this.mFormulas.size());
    for (IntegrityFormula integrityFormula : this.mFormulas)
      IntegrityFormula.writeToParcel(integrityFormula, paramParcel, paramInt); 
  }
  
  private static void validateFormulas(int paramInt, List<IntegrityFormula> paramList) {
    String str;
    if (paramInt != 0 && paramInt != 1) {
      if (paramInt == 2) {
        boolean bool;
        if (paramList.size() == 1) {
          bool = true;
        } else {
          bool = false;
        } 
        str = connectorToString(paramInt);
        str = String.format("Connector %s must have 1 formula only", new Object[] { str });
        Preconditions.checkArgument(bool, str);
      } 
    } else {
      boolean bool;
      if (str.size() >= 2) {
        bool = true;
      } else {
        bool = false;
      } 
      str = connectorToString(paramInt);
      str = String.format("Connector %s must have at least 2 formulas", new Object[] { str });
      Preconditions.checkArgument(bool, str);
    } 
  }
  
  private static String connectorToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt == 2)
          return "NOT"; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown connector ");
        stringBuilder.append(paramInt);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return "OR";
    } 
    return "AND";
  }
  
  private static boolean isValidConnector(int paramInt) {
    boolean bool1 = true, bool2 = bool1;
    if (paramInt != 0) {
      bool2 = bool1;
      if (paramInt != 1)
        if (paramInt == 2) {
          bool2 = bool1;
        } else {
          bool2 = false;
        }  
    } 
    return bool2;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Connector implements Annotation {}
}
