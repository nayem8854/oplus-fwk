package android.content;

public interface IOplusContextEx {
  public static final String NETWORKING_CONTROL_SERVICE = "networking_control";
  
  public static final String STORAGEHEALTHINFO_SERVICE = "storage_healthinfo";
}
