package android.content;

import android.database.ContentObserver;
import android.os.Handler;
import android.util.DebugUtils;
import java.io.FileDescriptor;
import java.io.PrintWriter;

@Deprecated
public class Loader<D> {
  boolean mStarted = false;
  
  boolean mAbandoned = false;
  
  boolean mReset = true;
  
  boolean mContentChanged = false;
  
  boolean mProcessingChange = false;
  
  Context mContext;
  
  int mId;
  
  OnLoadCompleteListener<D> mListener;
  
  OnLoadCanceledListener<D> mOnLoadCanceledListener;
  
  @Deprecated
  class ForceLoadContentObserver extends ContentObserver {
    final Loader this$0;
    
    public ForceLoadContentObserver() {
      super(new Handler());
    }
    
    public boolean deliverSelfNotifications() {
      return true;
    }
    
    public void onChange(boolean param1Boolean) {
      Loader.this.onContentChanged();
    }
  }
  
  public Loader(Context paramContext) {
    this.mContext = paramContext.getApplicationContext();
  }
  
  public void deliverResult(D paramD) {
    OnLoadCompleteListener<D> onLoadCompleteListener = this.mListener;
    if (onLoadCompleteListener != null)
      onLoadCompleteListener.onLoadComplete(this, paramD); 
  }
  
  public void deliverCancellation() {
    OnLoadCanceledListener<D> onLoadCanceledListener = this.mOnLoadCanceledListener;
    if (onLoadCanceledListener != null)
      onLoadCanceledListener.onLoadCanceled(this); 
  }
  
  public Context getContext() {
    return this.mContext;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public void registerListener(int paramInt, OnLoadCompleteListener<D> paramOnLoadCompleteListener) {
    if (this.mListener == null) {
      this.mListener = paramOnLoadCompleteListener;
      this.mId = paramInt;
      return;
    } 
    throw new IllegalStateException("There is already a listener registered");
  }
  
  public void unregisterListener(OnLoadCompleteListener<D> paramOnLoadCompleteListener) {
    OnLoadCompleteListener<D> onLoadCompleteListener = this.mListener;
    if (onLoadCompleteListener != null) {
      if (onLoadCompleteListener == paramOnLoadCompleteListener) {
        this.mListener = null;
        return;
      } 
      throw new IllegalArgumentException("Attempting to unregister the wrong listener");
    } 
    throw new IllegalStateException("No listener register");
  }
  
  public void registerOnLoadCanceledListener(OnLoadCanceledListener<D> paramOnLoadCanceledListener) {
    if (this.mOnLoadCanceledListener == null) {
      this.mOnLoadCanceledListener = paramOnLoadCanceledListener;
      return;
    } 
    throw new IllegalStateException("There is already a listener registered");
  }
  
  public void unregisterOnLoadCanceledListener(OnLoadCanceledListener<D> paramOnLoadCanceledListener) {
    OnLoadCanceledListener<D> onLoadCanceledListener = this.mOnLoadCanceledListener;
    if (onLoadCanceledListener != null) {
      if (onLoadCanceledListener == paramOnLoadCanceledListener) {
        this.mOnLoadCanceledListener = null;
        return;
      } 
      throw new IllegalArgumentException("Attempting to unregister the wrong listener");
    } 
    throw new IllegalStateException("No listener register");
  }
  
  public boolean isStarted() {
    return this.mStarted;
  }
  
  public boolean isAbandoned() {
    return this.mAbandoned;
  }
  
  public boolean isReset() {
    return this.mReset;
  }
  
  public final void startLoading() {
    this.mStarted = true;
    this.mReset = false;
    this.mAbandoned = false;
    onStartLoading();
  }
  
  protected void onStartLoading() {}
  
  public boolean cancelLoad() {
    return onCancelLoad();
  }
  
  protected boolean onCancelLoad() {
    return false;
  }
  
  public void forceLoad() {
    onForceLoad();
  }
  
  protected void onForceLoad() {}
  
  public void stopLoading() {
    this.mStarted = false;
    onStopLoading();
  }
  
  protected void onStopLoading() {}
  
  public void abandon() {
    this.mAbandoned = true;
    onAbandon();
  }
  
  protected void onAbandon() {}
  
  public void reset() {
    onReset();
    this.mReset = true;
    this.mStarted = false;
    this.mAbandoned = false;
    this.mContentChanged = false;
    this.mProcessingChange = false;
  }
  
  protected void onReset() {}
  
  public boolean takeContentChanged() {
    boolean bool = this.mContentChanged;
    this.mContentChanged = false;
    this.mProcessingChange |= bool;
    return bool;
  }
  
  public void commitContentChanged() {
    this.mProcessingChange = false;
  }
  
  public void rollbackContentChanged() {
    if (this.mProcessingChange)
      onContentChanged(); 
  }
  
  public void onContentChanged() {
    if (this.mStarted) {
      forceLoad();
    } else {
      this.mContentChanged = true;
    } 
  }
  
  public String dataToString(D paramD) {
    StringBuilder stringBuilder = new StringBuilder(64);
    DebugUtils.buildShortClassTag(paramD, stringBuilder);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(64);
    DebugUtils.buildShortClassTag(this, stringBuilder);
    stringBuilder.append(" id=");
    stringBuilder.append(this.mId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void dump(String paramString, FileDescriptor paramFileDescriptor, PrintWriter paramPrintWriter, String[] paramArrayOfString) {
    paramPrintWriter.print(paramString);
    paramPrintWriter.print("mId=");
    paramPrintWriter.print(this.mId);
    paramPrintWriter.print(" mListener=");
    paramPrintWriter.println(this.mListener);
    if (this.mStarted || this.mContentChanged || this.mProcessingChange) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mStarted=");
      paramPrintWriter.print(this.mStarted);
      paramPrintWriter.print(" mContentChanged=");
      paramPrintWriter.print(this.mContentChanged);
      paramPrintWriter.print(" mProcessingChange=");
      paramPrintWriter.println(this.mProcessingChange);
    } 
    if (this.mAbandoned || this.mReset) {
      paramPrintWriter.print(paramString);
      paramPrintWriter.print("mAbandoned=");
      paramPrintWriter.print(this.mAbandoned);
      paramPrintWriter.print(" mReset=");
      paramPrintWriter.println(this.mReset);
    } 
  }
  
  @Deprecated
  public static interface OnLoadCanceledListener<D> {
    void onLoadCanceled(Loader<D> param1Loader);
  }
  
  @Deprecated
  public static interface OnLoadCompleteListener<D> {
    void onLoadComplete(Loader<D> param1Loader, D param1D);
  }
}
