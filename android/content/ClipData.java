package android.content;

import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.StrictMode;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.URLSpan;
import android.util.Log;
import android.util.proto.ProtoOutputStream;
import com.android.internal.util.ArrayUtils;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import libcore.io.IoUtils;

public class ClipData implements Parcelable {
  public static final Parcelable.Creator<ClipData> CREATOR;
  
  static final String[] MIMETYPES_TEXT_HTML;
  
  static final String[] MIMETYPES_TEXT_INTENT;
  
  static final String[] MIMETYPES_TEXT_PLAIN = new String[] { "text/plain" };
  
  static final String[] MIMETYPES_TEXT_URILIST;
  
  final ClipDescription mClipDescription;
  
  final Bitmap mIcon;
  
  final ArrayList<Item> mItems;
  
  static {
    MIMETYPES_TEXT_HTML = new String[] { "text/html" };
    MIMETYPES_TEXT_URILIST = new String[] { "text/uri-list" };
    MIMETYPES_TEXT_INTENT = new String[] { "text/vnd.android.intent" };
    CREATOR = new Parcelable.Creator<ClipData>() {
        public ClipData createFromParcel(Parcel param1Parcel) {
          return new ClipData(param1Parcel);
        }
        
        public ClipData[] newArray(int param1Int) {
          return new ClipData[param1Int];
        }
      };
  }
  
  class Item {
    final String mHtmlText;
    
    final Intent mIntent;
    
    final CharSequence mText;
    
    Uri mUri;
    
    public Item(ClipData this$0) {
      this.mText = ((Item)this$0).mText;
      this.mHtmlText = ((Item)this$0).mHtmlText;
      this.mIntent = ((Item)this$0).mIntent;
      this.mUri = ((Item)this$0).mUri;
    }
    
    public Item(ClipData this$0) {
      this.mText = (CharSequence)this$0;
      this.mHtmlText = null;
      this.mIntent = null;
      this.mUri = null;
    }
    
    public Item(ClipData this$0, String param1String) {
      this.mText = (CharSequence)this$0;
      this.mHtmlText = param1String;
      this.mIntent = null;
      this.mUri = null;
    }
    
    public Item(ClipData this$0) {
      this.mText = null;
      this.mHtmlText = null;
      this.mIntent = (Intent)this$0;
      this.mUri = null;
    }
    
    public Item(ClipData this$0) {
      this.mText = null;
      this.mHtmlText = null;
      this.mIntent = null;
      this.mUri = (Uri)this$0;
    }
    
    public Item(ClipData this$0, Intent param1Intent, Uri param1Uri) {
      this.mText = (CharSequence)this$0;
      this.mHtmlText = null;
      this.mIntent = param1Intent;
      this.mUri = param1Uri;
    }
    
    public Item(ClipData this$0, String param1String, Intent param1Intent, Uri param1Uri) {
      if (param1String == null || this$0 != null) {
        this.mText = (CharSequence)this$0;
        this.mHtmlText = param1String;
        this.mIntent = param1Intent;
        this.mUri = param1Uri;
        return;
      } 
      throw new IllegalArgumentException("Plain text must be supplied if HTML text is supplied");
    }
    
    public CharSequence getText() {
      return this.mText;
    }
    
    public String getHtmlText() {
      return this.mHtmlText;
    }
    
    public Intent getIntent() {
      return this.mIntent;
    }
    
    public Uri getUri() {
      return this.mUri;
    }
    
    public CharSequence coerceToText(Context param1Context) {
      CharSequence charSequence = getText();
      if (charSequence != null)
        return charSequence; 
      Uri uri = getUri();
      if (uri != null) {
        ContentResolver contentResolver = param1Context.getContentResolver();
        AssetFileDescriptor assetFileDescriptor1 = null, assetFileDescriptor2 = null;
        FileInputStream fileInputStream = null;
        CharSequence charSequence1 = null;
        CharSequence charSequence2 = null;
        Context context = null;
        AssetFileDescriptor assetFileDescriptor3 = assetFileDescriptor2;
        charSequence = charSequence1;
        param1Context = context;
        try {
          AssetFileDescriptor assetFileDescriptor = contentResolver.openTypedAssetFileDescriptor(uri, "text/*", null);
        } catch (SecurityException securityException) {
          assetFileDescriptor3 = assetFileDescriptor2;
          charSequence = charSequence1;
          param1Context = context;
          Log.w("ClipData", "Failure opening stream", securityException);
        } catch (FileNotFoundException|RuntimeException fileNotFoundException) {
        
        } finally {}
        if (assetFileDescriptor1 != null) {
          InputStreamReader inputStreamReader;
          assetFileDescriptor3 = assetFileDescriptor1;
          charSequence = charSequence1;
          param1Context = context;
          charSequence1 = charSequence2;
          try {
            FileInputStream fileInputStream2 = assetFileDescriptor1.createInputStream();
            assetFileDescriptor3 = assetFileDescriptor1;
            FileInputStream fileInputStream1 = fileInputStream2;
            param1Context = context;
            fileInputStream = fileInputStream2;
            charSequence1 = charSequence2;
            InputStreamReader inputStreamReader2 = new InputStreamReader();
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream2;
            param1Context = context;
            fileInputStream = fileInputStream2;
            charSequence1 = charSequence2;
            this(fileInputStream2, "UTF-8");
            InputStreamReader inputStreamReader3 = inputStreamReader2;
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream2;
            InputStreamReader inputStreamReader1 = inputStreamReader3;
            fileInputStream = fileInputStream2;
            inputStreamReader = inputStreamReader3;
            charSequence2 = new StringBuilder();
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream2;
            inputStreamReader1 = inputStreamReader3;
            fileInputStream = fileInputStream2;
            inputStreamReader = inputStreamReader3;
            super(128);
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream2;
            inputStreamReader1 = inputStreamReader3;
            fileInputStream = fileInputStream2;
            inputStreamReader = inputStreamReader3;
            char[] arrayOfChar = new char[8192];
            while (true) {
              assetFileDescriptor3 = assetFileDescriptor1;
              fileInputStream1 = fileInputStream2;
              inputStreamReader1 = inputStreamReader3;
              fileInputStream = fileInputStream2;
              inputStreamReader = inputStreamReader3;
              int i = inputStreamReader3.read(arrayOfChar);
              if (i > 0) {
                assetFileDescriptor3 = assetFileDescriptor1;
                fileInputStream1 = fileInputStream2;
                inputStreamReader1 = inputStreamReader3;
                fileInputStream = fileInputStream2;
                inputStreamReader = inputStreamReader3;
                charSequence2.append(arrayOfChar, 0, i);
                continue;
              } 
              break;
            } 
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream2;
            inputStreamReader1 = inputStreamReader3;
            fileInputStream = fileInputStream2;
            inputStreamReader = inputStreamReader3;
            charSequence2 = charSequence2.toString();
            IoUtils.closeQuietly(assetFileDescriptor1);
            IoUtils.closeQuietly(fileInputStream2);
            IoUtils.closeQuietly(inputStreamReader3);
            return charSequence2;
          } catch (IOException iOException) {
            assetFileDescriptor3 = assetFileDescriptor1;
            FileInputStream fileInputStream1 = fileInputStream;
            InputStreamReader inputStreamReader1 = inputStreamReader;
            Log.w("ClipData", "Failure loading text", iOException);
            assetFileDescriptor3 = assetFileDescriptor1;
            fileInputStream1 = fileInputStream;
            inputStreamReader1 = inputStreamReader;
            String str1 = iOException.toString();
            IoUtils.closeQuietly(assetFileDescriptor1);
            IoUtils.closeQuietly(fileInputStream);
            IoUtils.closeQuietly(inputStreamReader);
            return str1;
          } 
        } 
        IoUtils.closeQuietly(assetFileDescriptor1);
        IoUtils.closeQuietly(null);
        IoUtils.closeQuietly(null);
        String str = uri.getScheme();
        if ("content".equals(str) || "android.resource".equals(str) || "file".equals(str))
          return ""; 
        return uri.toString();
      } 
      Intent intent = getIntent();
      if (intent != null)
        return intent.toUri(1); 
      return "";
    }
    
    public CharSequence coerceToStyledText(Context param1Context) {
      CharSequence charSequence = getText();
      if (charSequence instanceof Spanned)
        return charSequence; 
      String str = getHtmlText();
      if (str != null)
        try {
          Spanned spanned = Html.fromHtml(str);
          if (spanned != null)
            return (CharSequence)spanned; 
        } catch (RuntimeException runtimeException) {} 
      if (charSequence != null)
        return charSequence; 
      return coerceToHtmlOrStyledText(param1Context, true);
    }
    
    public String coerceToHtmlText(Context param1Context) {
      String str = getHtmlText();
      if (str != null)
        return str; 
      CharSequence charSequence2 = getText();
      if (charSequence2 != null) {
        if (charSequence2 instanceof Spanned)
          return Html.toHtml((Spanned)charSequence2); 
        return Html.escapeHtml(charSequence2);
      } 
      CharSequence charSequence1 = coerceToHtmlOrStyledText(param1Context, false);
      if (charSequence1 != null) {
        charSequence1 = charSequence1.toString();
      } else {
        charSequence1 = null;
      } 
      return (String)charSequence1;
    }
    
    private CharSequence coerceToHtmlOrStyledText(Context param1Context, boolean param1Boolean) {
      if (this.mUri != null) {
        String[] arrayOfString = null;
        try {
          String[] arrayOfString1 = param1Context.getContentResolver().getStreamTypes(this.mUri, "text/*");
        } catch (SecurityException securityException) {}
        boolean bool1 = false, bool2 = false;
        int i = 0, j = 0;
        String str2 = "text/html";
        if (arrayOfString != null) {
          int k = arrayOfString.length;
          byte b = 0;
          while (true) {
            bool1 = bool2;
            i = j;
            if (b < k) {
              String str = arrayOfString[b];
              if ("text/html".equals(str)) {
                bool1 = true;
              } else {
                bool1 = bool2;
                if (str.startsWith("text/")) {
                  j = 1;
                  bool1 = bool2;
                } 
              } 
              b++;
              bool2 = bool1;
              continue;
            } 
            break;
          } 
        } 
        if (bool1 || i) {
          IOException iOException1, iOException2, iOException3;
          FileInputStream fileInputStream2 = null, fileInputStream3 = null, fileInputStream4 = null;
          String[] arrayOfString1 = null;
          arrayOfString = arrayOfString1;
          FileInputStream fileInputStream1 = fileInputStream2, fileInputStream5 = fileInputStream3, fileInputStream6 = fileInputStream4;
          try {
            String str3;
            ContentResolver contentResolver = param1Context.getContentResolver();
            arrayOfString = arrayOfString1;
            fileInputStream1 = fileInputStream2;
            fileInputStream5 = fileInputStream3;
            fileInputStream6 = fileInputStream4;
            Uri uri = this.mUri;
            if (bool1) {
              str3 = str2;
            } else {
              str3 = "text/plain";
            } 
            arrayOfString = arrayOfString1;
            fileInputStream1 = fileInputStream2;
            fileInputStream5 = fileInputStream3;
            fileInputStream6 = fileInputStream4;
            AssetFileDescriptor assetFileDescriptor = contentResolver.openTypedAssetFileDescriptor(uri, str3, null);
            arrayOfString = arrayOfString1;
            fileInputStream1 = fileInputStream2;
            fileInputStream5 = fileInputStream3;
            fileInputStream6 = fileInputStream4;
            FileInputStream fileInputStream7 = assetFileDescriptor.createInputStream();
            FileInputStream fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            InputStreamReader inputStreamReader = new InputStreamReader();
            fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            this(fileInputStream7, "UTF-8");
            fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            StringBuilder stringBuilder = new StringBuilder();
            fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            this(128);
            fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            char[] arrayOfChar = new char[8192];
            while (true) {
              fileInputStream8 = fileInputStream7;
              fileInputStream1 = fileInputStream7;
              fileInputStream5 = fileInputStream7;
              fileInputStream6 = fileInputStream7;
              j = inputStreamReader.read(arrayOfChar);
              if (j > 0) {
                fileInputStream8 = fileInputStream7;
                fileInputStream1 = fileInputStream7;
                fileInputStream5 = fileInputStream7;
                fileInputStream6 = fileInputStream7;
                stringBuilder.append(arrayOfChar, 0, j);
                continue;
              } 
              break;
            } 
            fileInputStream8 = fileInputStream7;
            fileInputStream1 = fileInputStream7;
            fileInputStream5 = fileInputStream7;
            fileInputStream6 = fileInputStream7;
            String str4 = stringBuilder.toString();
            if (bool1) {
              if (param1Boolean) {
                fileInputStream8 = fileInputStream7;
                fileInputStream1 = fileInputStream7;
                fileInputStream5 = fileInputStream7;
                fileInputStream6 = fileInputStream7;
                try {
                  String str;
                  Spanned spanned = Html.fromHtml(str4);
                  if (spanned != null) {
                    Spanned spanned1 = spanned;
                  } else {
                    str = str4;
                  } 
                  if (fileInputStream7 != null)
                    try {
                      fileInputStream7.close();
                    } catch (IOException null) {} 
                  return str;
                } catch (RuntimeException runtimeException) {
                  if (iOException4 != null)
                    try {
                      iOException4.close();
                    } catch (IOException null) {} 
                  return str4;
                } 
              } 
              IOException iOException6 = iOException4, iOException7 = iOException4, iOException8 = iOException4, iOException9 = iOException4;
              str4 = str4.toString();
              if (iOException4 != null)
                try {
                  iOException4.close();
                } catch (IOException null) {} 
              return str4;
            } 
            if (param1Boolean) {
              if (iOException4 != null)
                try {
                  iOException4.close();
                } catch (IOException iOException4) {} 
              return str4;
            } 
            IOException iOException5 = iOException4;
            iOException1 = iOException4;
            iOException2 = iOException4;
            iOException3 = iOException4;
            str4 = Html.escapeHtml(str4);
            if (iOException4 != null)
              try {
                iOException4.close();
              } catch (IOException iOException) {} 
            return str4;
          } catch (SecurityException securityException) {
            IOException iOException = iOException3;
            Log.w("ClipData", "Failure opening stream", securityException);
            if (iOException3 != null)
              iOException3.close(); 
          } catch (FileNotFoundException fileNotFoundException) {
            if (iOException2 != null)
              try {
                iOException2.close();
              } catch (IOException iOException) {} 
          } catch (IOException iOException4) {
            IOException iOException5 = iOException1;
            Log.w("ClipData", "Failure loading text", iOException4);
            iOException5 = iOException1;
            String str = Html.escapeHtml(iOException4.toString());
            if (iOException1 != null)
              try {
                iOException1.close();
              } catch (IOException iOException) {} 
            return str;
          } finally {}
        } 
        String str1 = this.mUri.getScheme();
        if ("content".equals(str1) || "android.resource".equals(str1) || "file".equals(str1))
          return ""; 
        if (param1Boolean)
          return uriToStyledText(this.mUri.toString()); 
        return uriToHtml(this.mUri.toString());
      } 
      Intent intent = this.mIntent;
      if (intent != null) {
        if (param1Boolean)
          return uriToStyledText(intent.toUri(1)); 
        return uriToHtml(intent.toUri(1));
      } 
      return "";
    }
    
    private String uriToHtml(String param1String) {
      StringBuilder stringBuilder = new StringBuilder(256);
      stringBuilder.append("<a href=\"");
      stringBuilder.append(Html.escapeHtml(param1String));
      stringBuilder.append("\">");
      stringBuilder.append(Html.escapeHtml(param1String));
      stringBuilder.append("</a>");
      return stringBuilder.toString();
    }
    
    private CharSequence uriToStyledText(String param1String) {
      SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
      spannableStringBuilder.append(param1String);
      spannableStringBuilder.setSpan(new URLSpan(param1String), 0, spannableStringBuilder.length(), 33);
      return (CharSequence)spannableStringBuilder;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder(128);
      stringBuilder.append("ClipData.Item { ");
      toShortString(stringBuilder);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public void toShortString(StringBuilder param1StringBuilder) {
      if (this.mHtmlText != null) {
        param1StringBuilder.append("H:");
        param1StringBuilder.append(this.mHtmlText);
      } else if (this.mText != null) {
        param1StringBuilder.append("T:");
        param1StringBuilder.append(this.mText);
      } else if (this.mUri != null) {
        param1StringBuilder.append("U:");
        param1StringBuilder.append(this.mUri);
      } else if (this.mIntent != null) {
        param1StringBuilder.append("I:");
        this.mIntent.toShortString(param1StringBuilder, true, true, true, true);
      } else {
        param1StringBuilder.append("NULL");
      } 
    }
    
    public void toShortSummaryString(StringBuilder param1StringBuilder) {
      if (this.mHtmlText != null) {
        param1StringBuilder.append("HTML");
      } else if (this.mText != null) {
        param1StringBuilder.append("TEXT");
      } else if (this.mUri != null) {
        param1StringBuilder.append("U:");
        param1StringBuilder.append(this.mUri);
      } else if (this.mIntent != null) {
        param1StringBuilder.append("I:");
        this.mIntent.toShortString(param1StringBuilder, true, true, true, true);
      } else {
        param1StringBuilder.append("NULL");
      } 
    }
    
    public void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1Long = param1ProtoOutputStream.start(param1Long);
      String str = this.mHtmlText;
      if (str != null) {
        param1ProtoOutputStream.write(1138166333441L, str);
      } else {
        CharSequence charSequence = this.mText;
        if (charSequence != null) {
          param1ProtoOutputStream.write(1138166333442L, charSequence.toString());
        } else {
          Uri uri = this.mUri;
          if (uri != null) {
            param1ProtoOutputStream.write(1138166333443L, uri.toString());
          } else {
            Intent intent = this.mIntent;
            if (intent != null) {
              intent.dumpDebug(param1ProtoOutputStream, 1146756268036L, true, true, true, true);
            } else {
              param1ProtoOutputStream.write(1133871366149L, true);
            } 
          } 
        } 
      } 
      param1ProtoOutputStream.end(param1Long);
    }
  }
  
  public ClipData(CharSequence paramCharSequence, String[] paramArrayOfString, Item paramItem) {
    this.mClipDescription = new ClipDescription(paramCharSequence, paramArrayOfString);
    if (paramItem != null) {
      this.mIcon = null;
      ArrayList<Item> arrayList = new ArrayList();
      arrayList.add(paramItem);
      return;
    } 
    throw new NullPointerException("item is null");
  }
  
  public ClipData(ClipDescription paramClipDescription, Item paramItem) {
    this.mClipDescription = paramClipDescription;
    if (paramItem != null) {
      this.mIcon = null;
      ArrayList<Item> arrayList = new ArrayList();
      arrayList.add(paramItem);
      return;
    } 
    throw new NullPointerException("item is null");
  }
  
  public ClipData(ClipDescription paramClipDescription, ArrayList<Item> paramArrayList) {
    this.mClipDescription = paramClipDescription;
    if (paramArrayList != null) {
      this.mIcon = null;
      this.mItems = paramArrayList;
      return;
    } 
    throw new NullPointerException("item is null");
  }
  
  public ClipData(ClipData paramClipData) {
    this.mClipDescription = paramClipData.mClipDescription;
    this.mIcon = paramClipData.mIcon;
    this.mItems = new ArrayList<>(paramClipData.mItems);
  }
  
  public static ClipData newPlainText(CharSequence paramCharSequence1, CharSequence paramCharSequence2) {
    Item item = new Item(paramCharSequence2);
    return new ClipData(paramCharSequence1, MIMETYPES_TEXT_PLAIN, item);
  }
  
  public static ClipData newHtmlText(CharSequence paramCharSequence1, CharSequence paramCharSequence2, String paramString) {
    Item item = new Item(paramCharSequence2, paramString);
    return new ClipData(paramCharSequence1, MIMETYPES_TEXT_HTML, item);
  }
  
  public static ClipData newIntent(CharSequence paramCharSequence, Intent paramIntent) {
    Item item = new Item(paramIntent);
    return new ClipData(paramCharSequence, MIMETYPES_TEXT_INTENT, item);
  }
  
  public static ClipData newUri(ContentResolver paramContentResolver, CharSequence paramCharSequence, Uri paramUri) {
    Item item = new Item(paramUri);
    String[] arrayOfString = getMimeTypes(paramContentResolver, paramUri);
    return new ClipData(paramCharSequence, arrayOfString, item);
  }
  
  private static String[] getMimeTypes(ContentResolver paramContentResolver, Uri paramUri) {
    String[] arrayOfString2 = null;
    if ("content".equals(paramUri.getScheme())) {
      String str = paramContentResolver.getType(paramUri);
      String[] arrayOfString = paramContentResolver.getStreamTypes(paramUri, "*/*");
      arrayOfString2 = arrayOfString;
      if (str != null)
        if (arrayOfString == null) {
          arrayOfString2 = new String[] { str };
        } else {
          arrayOfString2 = arrayOfString;
          if (!ArrayUtils.contains((Object[])arrayOfString, str)) {
            arrayOfString2 = new String[arrayOfString.length + 1];
            arrayOfString2[0] = str;
            System.arraycopy(arrayOfString, 0, arrayOfString2, 1, arrayOfString.length);
          } 
        }  
    } 
    String[] arrayOfString1 = arrayOfString2;
    if (arrayOfString2 == null)
      arrayOfString1 = MIMETYPES_TEXT_URILIST; 
    return arrayOfString1;
  }
  
  public static ClipData newRawUri(CharSequence paramCharSequence, Uri paramUri) {
    Item item = new Item(paramUri);
    return new ClipData(paramCharSequence, MIMETYPES_TEXT_URILIST, item);
  }
  
  public ClipDescription getDescription() {
    return this.mClipDescription;
  }
  
  public void addItem(Item paramItem) {
    if (paramItem != null) {
      this.mItems.add(paramItem);
      return;
    } 
    throw new NullPointerException("item is null");
  }
  
  public void addItem(ContentResolver paramContentResolver, Item paramItem) {
    addItem(paramItem);
    if (paramItem.getHtmlText() != null) {
      this.mClipDescription.addMimeTypes(MIMETYPES_TEXT_HTML);
    } else if (paramItem.getText() != null) {
      this.mClipDescription.addMimeTypes(MIMETYPES_TEXT_PLAIN);
    } 
    if (paramItem.getIntent() != null)
      this.mClipDescription.addMimeTypes(MIMETYPES_TEXT_INTENT); 
    if (paramItem.getUri() != null)
      this.mClipDescription.addMimeTypes(getMimeTypes(paramContentResolver, paramItem.getUri())); 
  }
  
  public Bitmap getIcon() {
    return this.mIcon;
  }
  
  public int getItemCount() {
    return this.mItems.size();
  }
  
  public Item getItemAt(int paramInt) {
    return this.mItems.get(paramInt);
  }
  
  public void setItemAt(int paramInt, Item paramItem) {
    this.mItems.set(paramInt, paramItem);
  }
  
  public void prepareToLeaveProcess(boolean paramBoolean) {
    prepareToLeaveProcess(paramBoolean, 1);
  }
  
  public void prepareToLeaveProcess(boolean paramBoolean, int paramInt) {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      Item item = this.mItems.get(b);
      if (item.mIntent != null)
        item.mIntent.prepareToLeaveProcess(paramBoolean); 
      if (item.mUri != null && paramBoolean) {
        if (StrictMode.vmFileUriExposureEnabled())
          item.mUri.checkFileUriExposed("ClipData.Item.getUri()"); 
        if (StrictMode.vmContentUriWithoutPermissionEnabled())
          item.mUri.checkContentUriWithoutPermission("ClipData.Item.getUri()", paramInt); 
      } 
    } 
  }
  
  public void prepareToEnterProcess() {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      Item item = this.mItems.get(b);
      if (item.mIntent != null)
        item.mIntent.prepareToEnterProcess(); 
    } 
  }
  
  public void fixUris(int paramInt) {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      Item item = this.mItems.get(b);
      if (item.mIntent != null)
        item.mIntent.fixUris(paramInt); 
      if (item.mUri != null)
        item.mUri = ContentProvider.maybeAddUserId(item.mUri, paramInt); 
    } 
  }
  
  public void fixUrisLight(int paramInt) {
    int i = this.mItems.size();
    for (byte b = 0; b < i; b++) {
      Item item = this.mItems.get(b);
      if (item.mIntent != null) {
        Uri uri = item.mIntent.getData();
        if (uri != null)
          item.mIntent.setData(ContentProvider.maybeAddUserId(uri, paramInt)); 
      } 
      if (item.mUri != null)
        item.mUri = ContentProvider.maybeAddUserId(item.mUri, paramInt); 
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("ClipData { ");
    toShortString(stringBuilder);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public void toShortString(StringBuilder paramStringBuilder) {
    ClipDescription clipDescription = this.mClipDescription;
    if (clipDescription != null) {
      b1 = clipDescription.toShortString(paramStringBuilder) ^ true;
    } else {
      b1 = 1;
    } 
    byte b2 = b1;
    if (this.mIcon != null) {
      if (!b1)
        paramStringBuilder.append(' '); 
      b2 = 0;
      paramStringBuilder.append("I:");
      paramStringBuilder.append(this.mIcon.getWidth());
      paramStringBuilder.append('x');
      paramStringBuilder.append(this.mIcon.getHeight());
    } 
    for (byte b1 = 0; b1 < this.mItems.size(); b1++) {
      if (b2 == 0)
        paramStringBuilder.append(' '); 
      b2 = 0;
      paramStringBuilder.append('{');
      ((Item)this.mItems.get(b1)).toShortString(paramStringBuilder);
      paramStringBuilder.append('}');
    } 
  }
  
  public void toShortStringShortItems(StringBuilder paramStringBuilder, boolean paramBoolean) {
    if (this.mItems.size() > 0) {
      if (!paramBoolean)
        paramStringBuilder.append(' '); 
      for (byte b = 0; b < this.mItems.size(); b++)
        paramStringBuilder.append("{...}"); 
    } 
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    long l = paramProtoOutputStream.start(paramLong);
    ClipDescription clipDescription = this.mClipDescription;
    if (clipDescription != null)
      clipDescription.dumpDebug(paramProtoOutputStream, 1146756268033L); 
    if (this.mIcon != null) {
      paramLong = paramProtoOutputStream.start(1146756268034L);
      paramProtoOutputStream.write(1120986464257L, this.mIcon.getWidth());
      paramProtoOutputStream.write(1120986464258L, this.mIcon.getHeight());
      paramProtoOutputStream.end(paramLong);
    } 
    for (byte b = 0; b < this.mItems.size(); b++)
      ((Item)this.mItems.get(b)).dumpDebug(paramProtoOutputStream, 2246267895811L); 
    paramProtoOutputStream.end(l);
  }
  
  public void collectUris(List<Uri> paramList) {
    for (byte b = 0; b < this.mItems.size(); b++) {
      Item item = getItemAt(b);
      if (item.getUri() != null)
        paramList.add(item.getUri()); 
      Intent intent = item.getIntent();
      if (intent != null) {
        if (intent.getData() != null)
          paramList.add(intent.getData()); 
        if (intent.getClipData() != null)
          intent.getClipData().collectUris(paramList); 
      } 
    } 
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mClipDescription.writeToParcel(paramParcel, paramInt);
    if (this.mIcon != null) {
      paramParcel.writeInt(1);
      this.mIcon.writeToParcel(paramParcel, paramInt);
    } else {
      paramParcel.writeInt(0);
    } 
    int i = this.mItems.size();
    paramParcel.writeInt(i);
    for (byte b = 0; b < i; b++) {
      Item item = this.mItems.get(b);
      TextUtils.writeToParcel(item.mText, paramParcel, paramInt);
      paramParcel.writeString8(item.mHtmlText);
      if (item.mIntent != null) {
        paramParcel.writeInt(1);
        item.mIntent.writeToParcel(paramParcel, paramInt);
      } else {
        paramParcel.writeInt(0);
      } 
      if (item.mUri != null) {
        paramParcel.writeInt(1);
        item.mUri.writeToParcel(paramParcel, paramInt);
      } else {
        paramParcel.writeInt(0);
      } 
    } 
  }
  
  ClipData(Parcel paramParcel) {
    this.mClipDescription = new ClipDescription(paramParcel);
    if (paramParcel.readInt() != 0) {
      this.mIcon = (Bitmap)Bitmap.CREATOR.createFromParcel(paramParcel);
    } else {
      this.mIcon = null;
    } 
    this.mItems = new ArrayList<>();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      Intent intent;
      Uri uri;
      CharSequence charSequence = (CharSequence)TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
      String str = paramParcel.readString8();
      if (paramParcel.readInt() != 0) {
        intent = (Intent)Intent.CREATOR.createFromParcel(paramParcel);
      } else {
        intent = null;
      } 
      if (paramParcel.readInt() != 0) {
        uri = (Uri)Uri.CREATOR.createFromParcel(paramParcel);
      } else {
        uri = null;
      } 
      this.mItems.add(new Item(charSequence, str, intent, uri));
    } 
  }
}
