package android.content;

import android.os.Parcel;
import android.os.Parcelable;
import com.android.internal.util.Preconditions;
import java.io.PrintWriter;

public final class LocusId implements Parcelable {
  public LocusId(String paramString) {
    this.mId = (String)Preconditions.checkStringNotEmpty(paramString, "id cannot be empty");
  }
  
  public String getId() {
    return this.mId;
  }
  
  public int hashCode() {
    int i;
    String str = this.mId;
    if (str == null) {
      i = 0;
    } else {
      i = str.hashCode();
    } 
    return 1 * 31 + i;
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    String str = this.mId;
    if (str == null) {
      if (((LocusId)paramObject).mId != null)
        return false; 
    } else if (!str.equals(((LocusId)paramObject).mId)) {
      return false;
    } 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("LocusId[");
    stringBuilder.append(getSanitizedId());
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public void dump(PrintWriter paramPrintWriter) {
    paramPrintWriter.print("id:");
    paramPrintWriter.println(getSanitizedId());
  }
  
  private String getSanitizedId() {
    int i = this.mId.length();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(i);
    stringBuilder.append("_chars");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
  }
  
  public static final Parcelable.Creator<LocusId> CREATOR = new Parcelable.Creator<LocusId>() {
      public LocusId createFromParcel(Parcel param1Parcel) {
        return new LocusId(param1Parcel.readString());
      }
      
      public LocusId[] newArray(int param1Int) {
        return new LocusId[param1Int];
      }
    };
  
  private final String mId;
}
