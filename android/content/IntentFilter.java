package android.content;

import android.annotation.SystemApi;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.PatternMatcher;
import android.util.AndroidException;
import android.util.Printer;
import android.util.proto.ProtoOutputStream;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.BiConsumer;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlSerializer;

public class IntentFilter implements Parcelable {
  private ArrayList<String> mCategories = null;
  
  private ArrayList<String> mDataSchemes = null;
  
  private ArrayList<PatternMatcher> mDataSchemeSpecificParts = null;
  
  private ArrayList<AuthorityEntry> mDataAuthorities = null;
  
  private ArrayList<PatternMatcher> mDataPaths = null;
  
  private ArrayList<String> mStaticDataTypes = null;
  
  private ArrayList<String> mDataTypes = null;
  
  private ArrayList<String> mMimeGroups = null;
  
  public IntentFilter() {
    this.mHasStaticPartialTypes = false;
    this.mHasDynamicPartialTypes = false;
    this.mPriority = 0;
    this.mActions = new ArrayList<>();
  }
  
  public IntentFilter(IntentFilter paramIntentFilter) {
    this.mHasStaticPartialTypes = false;
    this.mHasDynamicPartialTypes = false;
    this.mPriority = paramIntentFilter.mPriority;
    this.mOrder = paramIntentFilter.mOrder;
    this.mActions = new ArrayList<>(paramIntentFilter.mActions);
    if (paramIntentFilter.mCategories != null)
      this.mCategories = new ArrayList<>(paramIntentFilter.mCategories); 
    if (paramIntentFilter.mStaticDataTypes != null)
      this.mStaticDataTypes = new ArrayList<>(paramIntentFilter.mStaticDataTypes); 
    if (paramIntentFilter.mDataTypes != null)
      this.mDataTypes = new ArrayList<>(paramIntentFilter.mDataTypes); 
    if (paramIntentFilter.mDataSchemes != null)
      this.mDataSchemes = new ArrayList<>(paramIntentFilter.mDataSchemes); 
    if (paramIntentFilter.mDataSchemeSpecificParts != null)
      this.mDataSchemeSpecificParts = new ArrayList<>(paramIntentFilter.mDataSchemeSpecificParts); 
    if (paramIntentFilter.mDataAuthorities != null)
      this.mDataAuthorities = new ArrayList<>(paramIntentFilter.mDataAuthorities); 
    if (paramIntentFilter.mDataPaths != null)
      this.mDataPaths = new ArrayList<>(paramIntentFilter.mDataPaths); 
    if (paramIntentFilter.mMimeGroups != null)
      this.mMimeGroups = new ArrayList<>(paramIntentFilter.mMimeGroups); 
    this.mHasStaticPartialTypes = paramIntentFilter.mHasStaticPartialTypes;
    this.mHasDynamicPartialTypes = paramIntentFilter.mHasDynamicPartialTypes;
    this.mVerifyState = paramIntentFilter.mVerifyState;
    this.mInstantAppVisibility = paramIntentFilter.mInstantAppVisibility;
  }
  
  public IntentFilter(Parcel paramParcel) {
    boolean bool1 = false;
    this.mHasStaticPartialTypes = false;
    this.mHasDynamicPartialTypes = false;
    ArrayList<String> arrayList = new ArrayList();
    paramParcel.readStringList(arrayList);
    if (paramParcel.readInt() != 0) {
      this.mCategories = arrayList = new ArrayList<>();
      paramParcel.readStringList(arrayList);
    } 
    if (paramParcel.readInt() != 0) {
      this.mDataSchemes = arrayList = new ArrayList<>();
      paramParcel.readStringList(arrayList);
    } 
    if (paramParcel.readInt() != 0) {
      this.mStaticDataTypes = arrayList = new ArrayList<>();
      paramParcel.readStringList(arrayList);
    } 
    if (paramParcel.readInt() != 0) {
      this.mDataTypes = arrayList = new ArrayList<>();
      paramParcel.readStringList(arrayList);
    } 
    if (paramParcel.readInt() != 0) {
      this.mMimeGroups = arrayList = new ArrayList<>();
      paramParcel.readStringList(arrayList);
    } 
    int i = paramParcel.readInt();
    if (i > 0) {
      this.mDataSchemeSpecificParts = new ArrayList<>(i);
      for (byte b = 0; b < i; b++)
        this.mDataSchemeSpecificParts.add(new PatternMatcher(paramParcel)); 
    } 
    i = paramParcel.readInt();
    if (i > 0) {
      this.mDataAuthorities = new ArrayList<>(i);
      for (byte b = 0; b < i; b++)
        this.mDataAuthorities.add(new AuthorityEntry(paramParcel)); 
    } 
    i = paramParcel.readInt();
    if (i > 0) {
      this.mDataPaths = new ArrayList<>(i);
      for (byte b = 0; b < i; b++)
        this.mDataPaths.add(new PatternMatcher(paramParcel)); 
    } 
    this.mPriority = paramParcel.readInt();
    if (paramParcel.readInt() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHasStaticPartialTypes = bool2;
    if (paramParcel.readInt() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHasDynamicPartialTypes = bool2;
    boolean bool2 = bool1;
    if (paramParcel.readInt() > 0)
      bool2 = true; 
    setAutoVerify(bool2);
    setVisibilityToInstantApp(paramParcel.readInt());
    this.mOrder = paramParcel.readInt();
  }
  
  public IntentFilter(String paramString) {
    this.mHasStaticPartialTypes = false;
    this.mHasDynamicPartialTypes = false;
    this.mPriority = 0;
    this.mActions = new ArrayList<>();
    addAction(paramString);
  }
  
  public IntentFilter(String paramString1, String paramString2) throws MalformedMimeTypeException {
    this.mHasStaticPartialTypes = false;
    this.mHasDynamicPartialTypes = false;
    this.mPriority = 0;
    this.mActions = new ArrayList<>();
    addAction(paramString1);
    addDataType(paramString2);
  }
  
  private static int findStringInSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfint, int paramInt) {
    if (paramArrayOfString == null)
      return -1; 
    int i = paramArrayOfint[paramInt];
    for (paramInt = 0; paramInt < i; paramInt++) {
      if (paramArrayOfString[paramInt].equals(paramString))
        return paramInt; 
    } 
    return -1;
  }
  
  private static String[] addStringToSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfint, int paramInt) {
    if (findStringInSet(paramArrayOfString, paramString, paramArrayOfint, paramInt) >= 0)
      return paramArrayOfString; 
    if (paramArrayOfString == null) {
      paramArrayOfString = new String[2];
      paramArrayOfString[0] = paramString;
      paramArrayOfint[paramInt] = 1;
      return paramArrayOfString;
    } 
    int i = paramArrayOfint[paramInt];
    if (i < paramArrayOfString.length) {
      paramArrayOfString[i] = paramString;
      paramArrayOfint[paramInt] = i + 1;
      return paramArrayOfString;
    } 
    String[] arrayOfString = new String[i * 3 / 2 + 2];
    System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, i);
    arrayOfString[i] = paramString;
    paramArrayOfint[paramInt] = i + 1;
    return arrayOfString;
  }
  
  private static String[] removeStringFromSet(String[] paramArrayOfString, String paramString, int[] paramArrayOfint, int paramInt) {
    int i = findStringInSet(paramArrayOfString, paramString, paramArrayOfint, paramInt);
    if (i < 0)
      return paramArrayOfString; 
    int j = paramArrayOfint[paramInt];
    if (j > paramArrayOfString.length / 4) {
      int k = j - i + 1;
      if (k > 0)
        System.arraycopy(paramArrayOfString, i + 1, paramArrayOfString, i, k); 
      paramArrayOfString[j - 1] = null;
      paramArrayOfint[paramInt] = j - 1;
      return paramArrayOfString;
    } 
    String[] arrayOfString = new String[paramArrayOfString.length / 3];
    if (i > 0)
      System.arraycopy(paramArrayOfString, 0, arrayOfString, 0, i); 
    if (i + 1 < j)
      System.arraycopy(paramArrayOfString, i + 1, arrayOfString, i, j - i + 1); 
    return arrayOfString;
  }
  
  public static class MalformedMimeTypeException extends AndroidException {
    public MalformedMimeTypeException() {}
    
    public MalformedMimeTypeException(String param1String) {
      super(param1String);
    }
  }
  
  public static IntentFilter create(String paramString1, String paramString2) {
    try {
      return new IntentFilter(paramString1, paramString2);
    } catch (MalformedMimeTypeException malformedMimeTypeException) {
      throw new RuntimeException("Bad MIME type", malformedMimeTypeException);
    } 
  }
  
  public final void setPriority(int paramInt) {
    this.mPriority = paramInt;
  }
  
  public final int getPriority() {
    return this.mPriority;
  }
  
  @SystemApi
  public final void setOrder(int paramInt) {
    this.mOrder = paramInt;
  }
  
  @SystemApi
  public final int getOrder() {
    return this.mOrder;
  }
  
  public final void setAutoVerify(boolean paramBoolean) {
    int i = this.mVerifyState & 0xFFFFFFFE;
    if (paramBoolean)
      this.mVerifyState = i | 0x1; 
  }
  
  public final boolean getAutoVerify() {
    int i = this.mVerifyState;
    boolean bool = true;
    if ((i & 0x1) != 1)
      bool = false; 
    return bool;
  }
  
  public final boolean handleAllWebDataURI() {
    null = hasCategory("android.intent.category.APP_BROWSER");
    boolean bool = false;
    if (!null) {
      null = bool;
      if (handlesWebUris(false)) {
        null = bool;
        if (countDataAuthorities() == 0)
          return true; 
      } 
      return null;
    } 
    return true;
  }
  
  public final boolean handlesWebUris(boolean paramBoolean) {
    if (hasAction("android.intent.action.VIEW") && hasCategory("android.intent.category.BROWSABLE")) {
      ArrayList<String> arrayList = this.mDataSchemes;
      if (arrayList != null)
        if (arrayList.size() != 0) {
          int i = this.mDataSchemes.size();
          for (byte b = 0; b < i; b++) {
            boolean bool;
            String str = this.mDataSchemes.get(b);
            if ("http".equals(str) || "https".equals(str)) {
              bool = true;
            } else {
              bool = false;
            } 
            if (paramBoolean) {
              if (!bool)
                return false; 
            } else if (bool) {
              return true;
            } 
          } 
          return paramBoolean;
        }  
    } 
    return false;
  }
  
  public final boolean needsVerification() {
    boolean bool = getAutoVerify();
    boolean bool1 = true;
    if (!bool || !handlesWebUris(true))
      bool1 = false; 
    return bool1;
  }
  
  public final boolean isVerified() {
    int i = this.mVerifyState;
    boolean bool = false;
    if ((i & 0x100) == 256) {
      if ((i & 0x10) == 16)
        bool = true; 
      return bool;
    } 
    return false;
  }
  
  public void setVerified(boolean paramBoolean) {
    int i = this.mVerifyState | 0x100;
    this.mVerifyState = i &= 0xFFFFEFFF;
    if (paramBoolean)
      this.mVerifyState = i | 0x1000; 
  }
  
  public void setVisibilityToInstantApp(int paramInt) {
    this.mInstantAppVisibility = paramInt;
  }
  
  public int getVisibilityToInstantApp() {
    return this.mInstantAppVisibility;
  }
  
  public boolean isVisibleToInstantApp() {
    boolean bool;
    if (this.mInstantAppVisibility != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isExplicitlyVisibleToInstantApp() {
    int i = this.mInstantAppVisibility;
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  public boolean isImplicitlyVisibleToInstantApp() {
    boolean bool;
    if (this.mInstantAppVisibility == 2) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void addAction(String paramString) {
    if (!this.mActions.contains(paramString))
      this.mActions.add(paramString.intern()); 
  }
  
  public final int countActions() {
    return this.mActions.size();
  }
  
  public final String getAction(int paramInt) {
    return this.mActions.get(paramInt);
  }
  
  public final boolean hasAction(String paramString) {
    boolean bool;
    if (paramString != null && this.mActions.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean matchAction(String paramString) {
    return matchAction(paramString, false, null);
  }
  
  private boolean matchAction(String paramString, boolean paramBoolean, Collection<String> paramCollection) {
    if (paramBoolean && "*".equals(paramString)) {
      if (paramCollection == null)
        return this.mActions.isEmpty() ^ true; 
      for (int i = this.mActions.size() - 1; i >= 0; i--) {
        if (!paramCollection.contains(this.mActions.get(i)))
          return true; 
      } 
      return false;
    } 
    if (paramCollection != null && paramCollection.contains(paramString))
      return false; 
    return hasAction(paramString);
  }
  
  public final Iterator<String> actionsIterator() {
    ArrayList<String> arrayList = this.mActions;
    if (arrayList != null) {
      Iterator<String> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<String>)arrayList;
  }
  
  public final void addDataType(String paramString) throws MalformedMimeTypeException {
    processMimeType(paramString, new _$$Lambda$IntentFilter$fvZpjl2C1djVISORSFvcX__NkJo(this));
  }
  
  public final void addDynamicDataType(String paramString) throws MalformedMimeTypeException {
    processMimeType(paramString, new _$$Lambda$IntentFilter$WX75RVXUnG63zh_f133zF3i8Szs(this));
  }
  
  private void processMimeType(String paramString, BiConsumer<String, Boolean> paramBiConsumer) throws MalformedMimeTypeException {
    int i = paramString.indexOf('/');
    int j = paramString.length();
    if (i > 0 && j >= i + 2) {
      String str1 = paramString;
      boolean bool1 = false;
      String str2 = str1;
      boolean bool2 = bool1;
      if (j == i + 2) {
        str2 = str1;
        bool2 = bool1;
        if (paramString.charAt(i + 1) == '*') {
          str2 = paramString.substring(0, i);
          bool2 = true;
        } 
      } 
      paramBiConsumer.accept(str2, Boolean.valueOf(bool2));
      return;
    } 
    throw new MalformedMimeTypeException(paramString);
  }
  
  public final void clearDynamicDataTypes() {
    ArrayList<String> arrayList = this.mDataTypes;
    if (arrayList == null)
      return; 
    if (this.mStaticDataTypes != null) {
      arrayList.clear();
      this.mDataTypes.addAll(this.mStaticDataTypes);
    } else {
      this.mDataTypes = null;
    } 
    this.mHasDynamicPartialTypes = false;
  }
  
  public int countStaticDataTypes() {
    boolean bool;
    ArrayList<String> arrayList = this.mStaticDataTypes;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean hasDataType(String paramString) {
    boolean bool;
    if (this.mDataTypes != null && findMimeType(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean hasExactDataType(String paramString) {
    boolean bool;
    ArrayList<String> arrayList = this.mDataTypes;
    if (arrayList != null && arrayList.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean hasExactDynamicDataType(String paramString) {
    boolean bool;
    if (hasExactDataType(paramString) && !hasExactStaticDataType(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean hasExactStaticDataType(String paramString) {
    boolean bool;
    ArrayList<String> arrayList = this.mStaticDataTypes;
    if (arrayList != null && arrayList.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final int countDataTypes() {
    boolean bool;
    ArrayList<String> arrayList = this.mDataTypes;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final String getDataType(int paramInt) {
    return this.mDataTypes.get(paramInt);
  }
  
  public final Iterator<String> typesIterator() {
    ArrayList<String> arrayList = this.mDataTypes;
    if (arrayList != null) {
      Iterator<String> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<String>)arrayList;
  }
  
  public final List<String> dataTypes() {
    List list;
    if (this.mDataTypes != null) {
      list = new ArrayList<>(this.mDataTypes);
    } else {
      list = null;
    } 
    return list;
  }
  
  public final void addMimeGroup(String paramString) {
    if (this.mMimeGroups == null)
      this.mMimeGroups = new ArrayList<>(); 
    if (!this.mMimeGroups.contains(paramString))
      this.mMimeGroups.add(paramString); 
  }
  
  public final boolean hasMimeGroup(String paramString) {
    boolean bool;
    ArrayList<String> arrayList = this.mMimeGroups;
    if (arrayList != null && arrayList.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final String getMimeGroup(int paramInt) {
    return this.mMimeGroups.get(paramInt);
  }
  
  public final int countMimeGroups() {
    boolean bool;
    ArrayList<String> arrayList = this.mMimeGroups;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final Iterator<String> mimeGroupsIterator() {
    ArrayList<String> arrayList = this.mMimeGroups;
    if (arrayList != null) {
      Iterator<String> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<String>)arrayList;
  }
  
  public final void addDataScheme(String paramString) {
    if (this.mDataSchemes == null)
      this.mDataSchemes = new ArrayList<>(); 
    if (!this.mDataSchemes.contains(paramString))
      this.mDataSchemes.add(paramString.intern()); 
  }
  
  public final int countDataSchemes() {
    boolean bool;
    ArrayList<String> arrayList = this.mDataSchemes;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final String getDataScheme(int paramInt) {
    return this.mDataSchemes.get(paramInt);
  }
  
  public final boolean hasDataScheme(String paramString) {
    boolean bool;
    ArrayList<String> arrayList = this.mDataSchemes;
    if (arrayList != null && arrayList.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final Iterator<String> schemesIterator() {
    ArrayList<String> arrayList = this.mDataSchemes;
    if (arrayList != null) {
      Iterator<String> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<String>)arrayList;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class InstantAppVisibility implements Annotation {}
  
  class AuthorityEntry {
    private final String mHost;
    
    private final String mOrigHost;
    
    private final int mPort;
    
    private final boolean mWild;
    
    public AuthorityEntry(IntentFilter this$0, String param1String1) {
      String str;
      this.mOrigHost = (String)this$0;
      int i = this$0.length();
      boolean bool1 = false, bool2 = bool1;
      if (i > 0) {
        bool2 = bool1;
        if (this$0.charAt(0) == '*')
          bool2 = true; 
      } 
      this.mWild = bool2;
      if (bool2)
        str = this$0.substring(1).intern(); 
      this.mHost = str;
      if (param1String1 != null) {
        i = Integer.parseInt(param1String1);
      } else {
        i = -1;
      } 
      this.mPort = i;
    }
    
    AuthorityEntry(IntentFilter this$0) {
      boolean bool;
      this.mOrigHost = this$0.readString();
      this.mHost = this$0.readString();
      if (this$0.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.mWild = bool;
      this.mPort = this$0.readInt();
    }
    
    void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeString(this.mOrigHost);
      param1Parcel.writeString(this.mHost);
      param1Parcel.writeInt(this.mWild);
      param1Parcel.writeInt(this.mPort);
    }
    
    void dumpDebug(ProtoOutputStream param1ProtoOutputStream, long param1Long) {
      param1Long = param1ProtoOutputStream.start(param1Long);
      param1ProtoOutputStream.write(1138166333441L, this.mHost);
      param1ProtoOutputStream.write(1133871366146L, this.mWild);
      param1ProtoOutputStream.write(1120986464259L, this.mPort);
      param1ProtoOutputStream.end(param1Long);
    }
    
    public String getHost() {
      return this.mOrigHost;
    }
    
    public int getPort() {
      return this.mPort;
    }
    
    public boolean match(AuthorityEntry param1AuthorityEntry) {
      if (this.mWild != param1AuthorityEntry.mWild)
        return false; 
      if (!this.mHost.equals(param1AuthorityEntry.mHost))
        return false; 
      if (this.mPort != param1AuthorityEntry.mPort)
        return false; 
      return true;
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object instanceof AuthorityEntry) {
        param1Object = param1Object;
        return match((AuthorityEntry)param1Object);
      } 
      return false;
    }
    
    public int match(Uri param1Uri) {
      return match(param1Uri, false);
    }
    
    public int match(Uri param1Uri, boolean param1Boolean) {
      String str = param1Uri.getHost();
      if (str == null) {
        if (param1Boolean && this.mWild)
          return 3145728; 
        return -2;
      } 
      if (!param1Boolean || !"*".equals(str)) {
        String str1 = str;
        if (this.mWild) {
          if (str.length() < this.mHost.length())
            return -2; 
          str1 = str.substring(str.length() - this.mHost.length());
        } 
        if (str1.compareToIgnoreCase(this.mHost) != 0)
          return -2; 
      } 
      if (!param1Boolean) {
        int i = this.mPort;
        if (i >= 0) {
          if (i != param1Uri.getPort())
            return -2; 
          return 4194304;
        } 
      } 
      return 3145728;
    }
  }
  
  public final void addDataSchemeSpecificPart(String paramString, int paramInt) {
    addDataSchemeSpecificPart(new PatternMatcher(paramString, paramInt));
  }
  
  public final void addDataSchemeSpecificPart(PatternMatcher paramPatternMatcher) {
    if (this.mDataSchemeSpecificParts == null)
      this.mDataSchemeSpecificParts = new ArrayList<>(); 
    this.mDataSchemeSpecificParts.add(paramPatternMatcher);
  }
  
  public final int countDataSchemeSpecificParts() {
    boolean bool;
    ArrayList<PatternMatcher> arrayList = this.mDataSchemeSpecificParts;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final PatternMatcher getDataSchemeSpecificPart(int paramInt) {
    return this.mDataSchemeSpecificParts.get(paramInt);
  }
  
  public final boolean hasDataSchemeSpecificPart(String paramString) {
    return hasDataSchemeSpecificPart(paramString, false);
  }
  
  private boolean hasDataSchemeSpecificPart(String paramString, boolean paramBoolean) {
    if (this.mDataSchemeSpecificParts == null)
      return false; 
    if (paramBoolean && "*".equals(paramString) && this.mDataSchemeSpecificParts.size() > 0)
      return true; 
    int i = this.mDataSchemeSpecificParts.size();
    for (byte b = 0; b < i; b++) {
      PatternMatcher patternMatcher = this.mDataSchemeSpecificParts.get(b);
      if (patternMatcher.match(paramString))
        return true; 
    } 
    return false;
  }
  
  public final boolean hasDataSchemeSpecificPart(PatternMatcher paramPatternMatcher) {
    ArrayList<PatternMatcher> arrayList = this.mDataSchemeSpecificParts;
    if (arrayList == null)
      return false; 
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      PatternMatcher patternMatcher = this.mDataSchemeSpecificParts.get(b);
      if (patternMatcher.getType() == paramPatternMatcher.getType() && patternMatcher.getPath().equals(paramPatternMatcher.getPath()))
        return true; 
    } 
    return false;
  }
  
  public final Iterator<PatternMatcher> schemeSpecificPartsIterator() {
    ArrayList<PatternMatcher> arrayList = this.mDataSchemeSpecificParts;
    if (arrayList != null) {
      Iterator<PatternMatcher> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<PatternMatcher>)arrayList;
  }
  
  public final void addDataAuthority(String paramString1, String paramString2) {
    String str = paramString2;
    if (paramString2 != null)
      str = paramString2.intern(); 
    addDataAuthority(new AuthorityEntry(paramString1.intern(), str));
  }
  
  public final void addDataAuthority(AuthorityEntry paramAuthorityEntry) {
    if (this.mDataAuthorities == null)
      this.mDataAuthorities = new ArrayList<>(); 
    this.mDataAuthorities.add(paramAuthorityEntry);
  }
  
  public final int countDataAuthorities() {
    boolean bool;
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final AuthorityEntry getDataAuthority(int paramInt) {
    return this.mDataAuthorities.get(paramInt);
  }
  
  public final boolean hasDataAuthority(Uri paramUri) {
    boolean bool;
    if (matchDataAuthority(paramUri) >= 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean hasDataAuthority(AuthorityEntry paramAuthorityEntry) {
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList == null)
      return false; 
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      if (((AuthorityEntry)this.mDataAuthorities.get(b)).match(paramAuthorityEntry))
        return true; 
    } 
    return false;
  }
  
  public final Iterator<AuthorityEntry> authoritiesIterator() {
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList != null) {
      Iterator<AuthorityEntry> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<AuthorityEntry>)arrayList;
  }
  
  public final void addDataPath(String paramString, int paramInt) {
    addDataPath(new PatternMatcher(paramString.intern(), paramInt));
  }
  
  public final void addDataPath(PatternMatcher paramPatternMatcher) {
    if (this.mDataPaths == null)
      this.mDataPaths = new ArrayList<>(); 
    this.mDataPaths.add(paramPatternMatcher);
  }
  
  public final int countDataPaths() {
    boolean bool;
    ArrayList<PatternMatcher> arrayList = this.mDataPaths;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final PatternMatcher getDataPath(int paramInt) {
    return this.mDataPaths.get(paramInt);
  }
  
  public final boolean hasDataPath(String paramString) {
    return hasDataPath(paramString, false);
  }
  
  private boolean hasDataPath(String paramString, boolean paramBoolean) {
    if (this.mDataPaths == null)
      return false; 
    if (paramBoolean && "/*".equals(paramString))
      return true; 
    int i = this.mDataPaths.size();
    for (byte b = 0; b < i; b++) {
      PatternMatcher patternMatcher = this.mDataPaths.get(b);
      if (patternMatcher.match(paramString))
        return true; 
    } 
    return false;
  }
  
  public final boolean hasDataPath(PatternMatcher paramPatternMatcher) {
    ArrayList<PatternMatcher> arrayList = this.mDataPaths;
    if (arrayList == null)
      return false; 
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      PatternMatcher patternMatcher = this.mDataPaths.get(b);
      if (patternMatcher.getType() == paramPatternMatcher.getType() && patternMatcher.getPath().equals(paramPatternMatcher.getPath()))
        return true; 
    } 
    return false;
  }
  
  public final Iterator<PatternMatcher> pathsIterator() {
    ArrayList<PatternMatcher> arrayList = this.mDataPaths;
    if (arrayList != null) {
      Iterator<PatternMatcher> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<PatternMatcher>)arrayList;
  }
  
  public final int matchDataAuthority(Uri paramUri) {
    return matchDataAuthority(paramUri, false);
  }
  
  public final int matchDataAuthority(Uri paramUri, boolean paramBoolean) {
    if (paramUri != null) {
      ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
      if (arrayList != null) {
        int i = arrayList.size();
        for (byte b = 0; b < i; b++) {
          AuthorityEntry authorityEntry = this.mDataAuthorities.get(b);
          int j = authorityEntry.match(paramUri, paramBoolean);
          if (j >= 0)
            return j; 
        } 
        return -2;
      } 
    } 
    return -2;
  }
  
  public final int matchData(String paramString1, String paramString2, Uri paramUri) {
    return matchData(paramString1, paramString2, paramUri, false);
  }
  
  private int matchData(String paramString1, String paramString2, Uri paramUri, boolean paramBoolean) {
    ArrayList<PatternMatcher> arrayList;
    boolean bool;
    if (paramBoolean && countMimeGroups() != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    ArrayList<String> arrayList1 = this.mDataTypes;
    ArrayList<String> arrayList2 = this.mDataSchemes;
    int i = 1048576;
    int j = -2;
    if (!bool && arrayList1 == null && arrayList2 == null) {
      if (paramString1 == null && paramUri == null)
        j = 1081344; 
      return j;
    } 
    String str = "";
    if (arrayList2 != null) {
      if (paramString2 != null)
        str = paramString2; 
      if (arrayList2.contains(str) || (paramBoolean && "*".equals(paramString2))) {
        i = 2097152;
        arrayList = this.mDataSchemeSpecificParts;
        j = i;
        if (arrayList != null) {
          j = i;
          if (paramUri != null)
            if (hasDataSchemeSpecificPart(paramUri.getSchemeSpecificPart(), paramBoolean)) {
              j = 5767168;
            } else {
              j = -2;
            }  
        } 
        i = j;
        if (j != 5767168) {
          ArrayList<AuthorityEntry> arrayList3 = this.mDataAuthorities;
          i = j;
          if (arrayList3 != null) {
            i = matchDataAuthority(paramUri, paramBoolean);
            if (i >= 0) {
              arrayList = this.mDataPaths;
              if (arrayList != null)
                if (hasDataPath(paramUri.getPath(), paramBoolean)) {
                  i = 5242880;
                } else {
                  return -2;
                }  
            } else {
              return -2;
            } 
          } 
        } 
        if (i == -2)
          return -2; 
        j = i;
      } else {
        return -2;
      } 
    } else {
      j = i;
      if (arrayList != null) {
        j = i;
        if (!"".equals(arrayList)) {
          j = i;
          if (!"content".equals(arrayList)) {
            j = i;
            if (!"file".equals(arrayList))
              if (paramBoolean) {
                j = i;
                if (!"*".equals(arrayList))
                  return -2; 
              } else {
                return -2;
              }  
          } 
        } 
      } 
    } 
    if (bool)
      return 6291456; 
    if (arrayList1 != null) {
      if (findMimeType(paramString1)) {
        j = 6291456;
      } else {
        return -1;
      } 
    } else if (paramString1 != null) {
      return -1;
    } 
    return 32768 + j;
  }
  
  public final void addCategory(String paramString) {
    if (this.mCategories == null)
      this.mCategories = new ArrayList<>(); 
    if (!this.mCategories.contains(paramString))
      this.mCategories.add(paramString.intern()); 
  }
  
  public final int countCategories() {
    boolean bool;
    ArrayList<String> arrayList = this.mCategories;
    if (arrayList != null) {
      bool = arrayList.size();
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final String getCategory(int paramInt) {
    return this.mCategories.get(paramInt);
  }
  
  public final boolean hasCategory(String paramString) {
    boolean bool;
    ArrayList<String> arrayList = this.mCategories;
    if (arrayList != null && arrayList.contains(paramString)) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final Iterator<String> categoriesIterator() {
    ArrayList<String> arrayList = this.mCategories;
    if (arrayList != null) {
      Iterator<String> iterator = arrayList.iterator();
    } else {
      arrayList = null;
    } 
    return (Iterator<String>)arrayList;
  }
  
  public final String matchCategories(Set<String> paramSet) {
    Set set = null;
    if (paramSet == null)
      return null; 
    Iterator<String> iterator = paramSet.iterator();
    if (this.mCategories == null) {
      String str;
      paramSet = set;
      if (iterator.hasNext())
        str = iterator.next(); 
      return str;
    } 
    while (iterator.hasNext()) {
      String str = iterator.next();
      if (!this.mCategories.contains(str))
        return str; 
    } 
    return null;
  }
  
  public final int match(ContentResolver paramContentResolver, Intent paramIntent, boolean paramBoolean, String paramString) {
    String str1;
    if (paramBoolean) {
      str1 = paramIntent.resolveType(paramContentResolver);
    } else {
      str1 = paramIntent.getType();
    } 
    String str2 = paramIntent.getAction(), str3 = paramIntent.getScheme();
    Uri uri = paramIntent.getData();
    Set<String> set = paramIntent.getCategories();
    return match(str2, str1, str3, uri, set, paramString);
  }
  
  public final int match(String paramString1, String paramString2, String paramString3, Uri paramUri, Set<String> paramSet, String paramString4) {
    return match(paramString1, paramString2, paramString3, paramUri, paramSet, paramString4, false, null);
  }
  
  public final int match(String paramString1, String paramString2, String paramString3, Uri paramUri, Set<String> paramSet, String paramString4, boolean paramBoolean, Collection<String> paramCollection) {
    if (paramString1 != null && !matchAction(paramString1, paramBoolean, paramCollection))
      return -3; 
    int i = matchData(paramString2, paramString3, paramUri, paramBoolean);
    if (i < 0)
      return i; 
    paramString1 = matchCategories(paramSet);
    if (paramString1 != null)
      return -4; 
    return i;
  }
  
  public void writeToXml(XmlSerializer paramXmlSerializer) throws IOException {
    if (getAutoVerify())
      paramXmlSerializer.attribute(null, "autoVerify", Boolean.toString(true)); 
    int i = countActions();
    byte b;
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "action");
      paramXmlSerializer.attribute(null, "name", this.mActions.get(b));
      paramXmlSerializer.endTag(null, "action");
    } 
    i = countCategories();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "cat");
      paramXmlSerializer.attribute(null, "name", this.mCategories.get(b));
      paramXmlSerializer.endTag(null, "cat");
    } 
    writeDataTypesToXml(paramXmlSerializer);
    i = countMimeGroups();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "group");
      paramXmlSerializer.attribute(null, "name", this.mMimeGroups.get(b));
      paramXmlSerializer.endTag(null, "group");
    } 
    i = countDataSchemes();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "scheme");
      paramXmlSerializer.attribute(null, "name", this.mDataSchemes.get(b));
      paramXmlSerializer.endTag(null, "scheme");
    } 
    i = countDataSchemeSpecificParts();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "ssp");
      PatternMatcher patternMatcher = this.mDataSchemeSpecificParts.get(b);
      int j = patternMatcher.getType();
      if (j != 0) {
        if (j != 1) {
          if (j != 2) {
            if (j == 3)
              paramXmlSerializer.attribute(null, "aglob", patternMatcher.getPath()); 
          } else {
            paramXmlSerializer.attribute(null, "sglob", patternMatcher.getPath());
          } 
        } else {
          paramXmlSerializer.attribute(null, "prefix", patternMatcher.getPath());
        } 
      } else {
        paramXmlSerializer.attribute(null, "literal", patternMatcher.getPath());
      } 
      paramXmlSerializer.endTag(null, "ssp");
    } 
    i = countDataAuthorities();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "auth");
      AuthorityEntry authorityEntry = this.mDataAuthorities.get(b);
      paramXmlSerializer.attribute(null, "host", authorityEntry.getHost());
      if (authorityEntry.getPort() >= 0)
        paramXmlSerializer.attribute(null, "port", Integer.toString(authorityEntry.getPort())); 
      paramXmlSerializer.endTag(null, "auth");
    } 
    i = countDataPaths();
    for (b = 0; b < i; b++) {
      paramXmlSerializer.startTag(null, "path");
      PatternMatcher patternMatcher = this.mDataPaths.get(b);
      int j = patternMatcher.getType();
      if (j != 0) {
        if (j != 1) {
          if (j != 2) {
            if (j == 3)
              paramXmlSerializer.attribute(null, "aglob", patternMatcher.getPath()); 
          } else {
            paramXmlSerializer.attribute(null, "sglob", patternMatcher.getPath());
          } 
        } else {
          paramXmlSerializer.attribute(null, "prefix", patternMatcher.getPath());
        } 
      } else {
        paramXmlSerializer.attribute(null, "literal", patternMatcher.getPath());
      } 
      paramXmlSerializer.endTag(null, "path");
    } 
  }
  
  private void writeDataTypesToXml(XmlSerializer paramXmlSerializer) throws IOException {
    byte b2;
    ArrayList<String> arrayList = this.mStaticDataTypes;
    if (arrayList == null)
      return; 
    byte b1 = 0;
    Iterator<String> iterator = arrayList.iterator();
    while (true) {
      b2 = b1;
      if (iterator.hasNext()) {
        String str = iterator.next();
        while (!((String)this.mDataTypes.get(b1)).equals(str)) {
          writeDataTypeToXml(paramXmlSerializer, this.mDataTypes.get(b1), "type");
          b1++;
        } 
        writeDataTypeToXml(paramXmlSerializer, str, "staticType");
        b1++;
        continue;
      } 
      break;
    } 
    while (b2 < this.mDataTypes.size()) {
      writeDataTypeToXml(paramXmlSerializer, this.mDataTypes.get(b2), "type");
      b2++;
    } 
  }
  
  private void writeDataTypeToXml(XmlSerializer paramXmlSerializer, String paramString1, String paramString2) throws IOException {
    paramXmlSerializer.startTag(null, paramString2);
    String str = paramString1;
    if (paramString1.indexOf('/') < 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(paramString1);
      stringBuilder.append("/*");
      str = stringBuilder.toString();
    } 
    paramXmlSerializer.attribute(null, "name", str);
    paramXmlSerializer.endTag(null, paramString2);
  }
  
  public void readFromXml(XmlPullParser paramXmlPullParser) throws XmlPullParserException, IOException {
    // Byte code:
    //   0: aload_1
    //   1: aconst_null
    //   2: ldc 'autoVerify'
    //   4: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   9: astore_2
    //   10: aload_2
    //   11: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
    //   14: ifeq -> 22
    //   17: iconst_0
    //   18: istore_3
    //   19: goto -> 27
    //   22: aload_2
    //   23: invokestatic getBoolean : (Ljava/lang/String;)Z
    //   26: istore_3
    //   27: aload_0
    //   28: iload_3
    //   29: invokevirtual setAutoVerify : (Z)V
    //   32: aload_1
    //   33: invokeinterface getDepth : ()I
    //   38: istore #4
    //   40: aload_1
    //   41: invokeinterface next : ()I
    //   46: istore #5
    //   48: iload #5
    //   50: iconst_1
    //   51: if_icmpeq -> 577
    //   54: iload #5
    //   56: iconst_3
    //   57: if_icmpne -> 71
    //   60: aload_1
    //   61: invokeinterface getDepth : ()I
    //   66: iload #4
    //   68: if_icmple -> 577
    //   71: iload #5
    //   73: iconst_3
    //   74: if_icmpeq -> 40
    //   77: iload #5
    //   79: iconst_4
    //   80: if_icmpne -> 86
    //   83: goto -> 40
    //   86: aload_1
    //   87: invokeinterface getName : ()Ljava/lang/String;
    //   92: astore_2
    //   93: aload_2
    //   94: ldc 'action'
    //   96: invokevirtual equals : (Ljava/lang/Object;)Z
    //   99: ifeq -> 124
    //   102: aload_1
    //   103: aconst_null
    //   104: ldc 'name'
    //   106: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   111: astore_2
    //   112: aload_2
    //   113: ifnull -> 121
    //   116: aload_0
    //   117: aload_2
    //   118: invokevirtual addAction : (Ljava/lang/String;)V
    //   121: goto -> 570
    //   124: aload_2
    //   125: ldc 'cat'
    //   127: invokevirtual equals : (Ljava/lang/Object;)Z
    //   130: ifeq -> 155
    //   133: aload_1
    //   134: aconst_null
    //   135: ldc 'name'
    //   137: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   142: astore_2
    //   143: aload_2
    //   144: ifnull -> 152
    //   147: aload_0
    //   148: aload_2
    //   149: invokevirtual addCategory : (Ljava/lang/String;)V
    //   152: goto -> 570
    //   155: aload_2
    //   156: ldc 'staticType'
    //   158: invokevirtual equals : (Ljava/lang/Object;)Z
    //   161: ifeq -> 190
    //   164: aload_1
    //   165: aconst_null
    //   166: ldc 'name'
    //   168: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   173: astore_2
    //   174: aload_2
    //   175: ifnull -> 187
    //   178: aload_0
    //   179: aload_2
    //   180: invokevirtual addDataType : (Ljava/lang/String;)V
    //   183: goto -> 187
    //   186: astore_2
    //   187: goto -> 570
    //   190: aload_2
    //   191: ldc 'type'
    //   193: invokevirtual equals : (Ljava/lang/Object;)Z
    //   196: ifeq -> 225
    //   199: aload_1
    //   200: aconst_null
    //   201: ldc 'name'
    //   203: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   208: astore_2
    //   209: aload_2
    //   210: ifnull -> 222
    //   213: aload_0
    //   214: aload_2
    //   215: invokevirtual addDynamicDataType : (Ljava/lang/String;)V
    //   218: goto -> 222
    //   221: astore_2
    //   222: goto -> 570
    //   225: aload_2
    //   226: ldc 'group'
    //   228: invokevirtual equals : (Ljava/lang/Object;)Z
    //   231: ifeq -> 256
    //   234: aload_1
    //   235: aconst_null
    //   236: ldc 'name'
    //   238: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   243: astore_2
    //   244: aload_2
    //   245: ifnull -> 253
    //   248: aload_0
    //   249: aload_2
    //   250: invokevirtual addMimeGroup : (Ljava/lang/String;)V
    //   253: goto -> 570
    //   256: aload_2
    //   257: ldc 'scheme'
    //   259: invokevirtual equals : (Ljava/lang/Object;)Z
    //   262: ifeq -> 287
    //   265: aload_1
    //   266: aconst_null
    //   267: ldc 'name'
    //   269: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   274: astore_2
    //   275: aload_2
    //   276: ifnull -> 284
    //   279: aload_0
    //   280: aload_2
    //   281: invokevirtual addDataScheme : (Ljava/lang/String;)V
    //   284: goto -> 570
    //   287: aload_2
    //   288: ldc 'ssp'
    //   290: invokevirtual equals : (Ljava/lang/Object;)Z
    //   293: ifeq -> 388
    //   296: aload_1
    //   297: aconst_null
    //   298: ldc 'literal'
    //   300: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   305: astore_2
    //   306: aload_2
    //   307: ifnull -> 319
    //   310: aload_0
    //   311: aload_2
    //   312: iconst_0
    //   313: invokevirtual addDataSchemeSpecificPart : (Ljava/lang/String;I)V
    //   316: goto -> 385
    //   319: aload_1
    //   320: aconst_null
    //   321: ldc 'prefix'
    //   323: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   328: astore_2
    //   329: aload_2
    //   330: ifnull -> 342
    //   333: aload_0
    //   334: aload_2
    //   335: iconst_1
    //   336: invokevirtual addDataSchemeSpecificPart : (Ljava/lang/String;I)V
    //   339: goto -> 385
    //   342: aload_1
    //   343: aconst_null
    //   344: ldc 'sglob'
    //   346: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   351: astore_2
    //   352: aload_2
    //   353: ifnull -> 365
    //   356: aload_0
    //   357: aload_2
    //   358: iconst_2
    //   359: invokevirtual addDataSchemeSpecificPart : (Ljava/lang/String;I)V
    //   362: goto -> 385
    //   365: aload_1
    //   366: aconst_null
    //   367: ldc 'aglob'
    //   369: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   374: astore_2
    //   375: aload_2
    //   376: ifnull -> 385
    //   379: aload_0
    //   380: aload_2
    //   381: iconst_3
    //   382: invokevirtual addDataSchemeSpecificPart : (Ljava/lang/String;I)V
    //   385: goto -> 570
    //   388: aload_2
    //   389: ldc 'auth'
    //   391: invokevirtual equals : (Ljava/lang/Object;)Z
    //   394: ifeq -> 432
    //   397: aload_1
    //   398: aconst_null
    //   399: ldc 'host'
    //   401: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   406: astore_2
    //   407: aload_1
    //   408: aconst_null
    //   409: ldc 'port'
    //   411: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   416: astore #6
    //   418: aload_2
    //   419: ifnull -> 429
    //   422: aload_0
    //   423: aload_2
    //   424: aload #6
    //   426: invokevirtual addDataAuthority : (Ljava/lang/String;Ljava/lang/String;)V
    //   429: goto -> 570
    //   432: aload_2
    //   433: ldc 'path'
    //   435: invokevirtual equals : (Ljava/lang/Object;)Z
    //   438: ifeq -> 533
    //   441: aload_1
    //   442: aconst_null
    //   443: ldc 'literal'
    //   445: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   450: astore_2
    //   451: aload_2
    //   452: ifnull -> 464
    //   455: aload_0
    //   456: aload_2
    //   457: iconst_0
    //   458: invokevirtual addDataPath : (Ljava/lang/String;I)V
    //   461: goto -> 530
    //   464: aload_1
    //   465: aconst_null
    //   466: ldc 'prefix'
    //   468: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   473: astore_2
    //   474: aload_2
    //   475: ifnull -> 487
    //   478: aload_0
    //   479: aload_2
    //   480: iconst_1
    //   481: invokevirtual addDataPath : (Ljava/lang/String;I)V
    //   484: goto -> 530
    //   487: aload_1
    //   488: aconst_null
    //   489: ldc 'sglob'
    //   491: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   496: astore_2
    //   497: aload_2
    //   498: ifnull -> 510
    //   501: aload_0
    //   502: aload_2
    //   503: iconst_2
    //   504: invokevirtual addDataPath : (Ljava/lang/String;I)V
    //   507: goto -> 530
    //   510: aload_1
    //   511: aconst_null
    //   512: ldc 'aglob'
    //   514: invokeinterface getAttributeValue : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   519: astore_2
    //   520: aload_2
    //   521: ifnull -> 530
    //   524: aload_0
    //   525: aload_2
    //   526: iconst_3
    //   527: invokevirtual addDataPath : (Ljava/lang/String;I)V
    //   530: goto -> 570
    //   533: new java/lang/StringBuilder
    //   536: dup
    //   537: invokespecial <init> : ()V
    //   540: astore #6
    //   542: aload #6
    //   544: ldc_w 'Unknown tag parsing IntentFilter: '
    //   547: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   550: pop
    //   551: aload #6
    //   553: aload_2
    //   554: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   557: pop
    //   558: ldc_w 'IntentFilter'
    //   561: aload #6
    //   563: invokevirtual toString : ()Ljava/lang/String;
    //   566: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   569: pop
    //   570: aload_1
    //   571: invokestatic skipCurrentTag : (Lorg/xmlpull/v1/XmlPullParser;)V
    //   574: goto -> 40
    //   577: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1992	-> 0
    //   #1993	-> 10
    //   #1995	-> 32
    //   #1997	-> 40
    //   #1999	-> 60
    //   #2000	-> 71
    //   #2002	-> 83
    //   #2005	-> 86
    //   #2006	-> 93
    //   #2007	-> 102
    //   #2008	-> 112
    //   #2009	-> 116
    //   #2011	-> 121
    //   #2012	-> 133
    //   #2013	-> 143
    //   #2014	-> 147
    //   #2016	-> 152
    //   #2017	-> 164
    //   #2018	-> 174
    //   #2020	-> 178
    //   #2022	-> 183
    //   #2021	-> 186
    //   #2024	-> 187
    //   #2025	-> 199
    //   #2026	-> 209
    //   #2028	-> 213
    //   #2030	-> 218
    //   #2029	-> 221
    //   #2032	-> 222
    //   #2033	-> 234
    //   #2034	-> 244
    //   #2035	-> 248
    //   #2037	-> 253
    //   #2038	-> 265
    //   #2039	-> 275
    //   #2040	-> 279
    //   #2042	-> 284
    //   #2043	-> 296
    //   #2044	-> 306
    //   #2045	-> 310
    //   #2046	-> 319
    //   #2047	-> 333
    //   #2048	-> 342
    //   #2049	-> 356
    //   #2050	-> 365
    //   #2051	-> 379
    //   #2053	-> 385
    //   #2054	-> 397
    //   #2055	-> 407
    //   #2056	-> 418
    //   #2057	-> 422
    //   #2059	-> 429
    //   #2060	-> 441
    //   #2061	-> 451
    //   #2062	-> 455
    //   #2063	-> 464
    //   #2064	-> 478
    //   #2065	-> 487
    //   #2066	-> 501
    //   #2067	-> 510
    //   #2068	-> 524
    //   #2070	-> 530
    //   #2071	-> 533
    //   #2073	-> 570
    //   #2074	-> 574
    //   #2075	-> 577
    // Exception table:
    //   from	to	target	type
    //   178	183	186	android/content/IntentFilter$MalformedMimeTypeException
    //   213	218	221	android/content/IntentFilter$MalformedMimeTypeException
  }
  
  public void dumpDebug(ProtoOutputStream paramProtoOutputStream, long paramLong) {
    paramLong = paramProtoOutputStream.start(paramLong);
    if (this.mActions.size() > 0) {
      Iterator<String> iterator = this.mActions.iterator();
      while (iterator.hasNext())
        paramProtoOutputStream.write(2237677961217L, iterator.next()); 
    } 
    ArrayList<String> arrayList4 = this.mCategories;
    if (arrayList4 != null) {
      Iterator<String> iterator = arrayList4.iterator();
      while (iterator.hasNext())
        paramProtoOutputStream.write(2237677961218L, iterator.next()); 
    } 
    arrayList4 = this.mDataSchemes;
    if (arrayList4 != null) {
      Iterator<String> iterator = arrayList4.iterator();
      while (iterator.hasNext())
        paramProtoOutputStream.write(2237677961219L, iterator.next()); 
    } 
    ArrayList<PatternMatcher> arrayList3 = this.mDataSchemeSpecificParts;
    if (arrayList3 != null) {
      Iterator<PatternMatcher> iterator = arrayList3.iterator();
      while (iterator.hasNext())
        ((PatternMatcher)iterator.next()).dumpDebug(paramProtoOutputStream, 2246267895812L); 
    } 
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList != null) {
      Iterator<AuthorityEntry> iterator = arrayList.iterator();
      while (iterator.hasNext())
        ((AuthorityEntry)iterator.next()).dumpDebug(paramProtoOutputStream, 2246267895813L); 
    } 
    ArrayList<PatternMatcher> arrayList2 = this.mDataPaths;
    if (arrayList2 != null) {
      Iterator<PatternMatcher> iterator = arrayList2.iterator();
      while (iterator.hasNext())
        ((PatternMatcher)iterator.next()).dumpDebug(paramProtoOutputStream, 2246267895814L); 
    } 
    ArrayList<String> arrayList1 = this.mDataTypes;
    if (arrayList1 != null) {
      Iterator<String> iterator = arrayList1.iterator();
      while (iterator.hasNext())
        paramProtoOutputStream.write(2237677961223L, iterator.next()); 
    } 
    arrayList1 = this.mMimeGroups;
    if (arrayList1 != null) {
      Iterator<String> iterator = arrayList1.iterator();
      while (iterator.hasNext())
        paramProtoOutputStream.write(2237677961227L, iterator.next()); 
    } 
    if (this.mPriority != 0 || hasPartialTypes()) {
      paramProtoOutputStream.write(1120986464264L, this.mPriority);
      paramProtoOutputStream.write(1133871366153L, hasPartialTypes());
    } 
    paramProtoOutputStream.write(1133871366154L, getAutoVerify());
    paramProtoOutputStream.end(paramLong);
  }
  
  public void dump(Printer paramPrinter, String paramString) {
    StringBuilder stringBuilder = new StringBuilder(256);
    if (this.mActions.size() > 0) {
      Iterator<String> iterator = this.mActions.iterator();
      while (iterator.hasNext()) {
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Action: \"");
        stringBuilder.append(iterator.next());
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    ArrayList<String> arrayList4 = this.mCategories;
    if (arrayList4 != null) {
      Iterator<String> iterator = arrayList4.iterator();
      while (iterator.hasNext()) {
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Category: \"");
        stringBuilder.append(iterator.next());
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    arrayList4 = this.mDataSchemes;
    if (arrayList4 != null) {
      Iterator<String> iterator = arrayList4.iterator();
      while (iterator.hasNext()) {
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Scheme: \"");
        stringBuilder.append(iterator.next());
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    ArrayList<PatternMatcher> arrayList3 = this.mDataSchemeSpecificParts;
    if (arrayList3 != null) {
      Iterator<PatternMatcher> iterator = arrayList3.iterator();
      while (iterator.hasNext()) {
        PatternMatcher patternMatcher = iterator.next();
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Ssp: \"");
        stringBuilder.append(patternMatcher);
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList != null) {
      Iterator<AuthorityEntry> iterator = arrayList.iterator();
      while (iterator.hasNext()) {
        AuthorityEntry authorityEntry = iterator.next();
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Authority: \"");
        stringBuilder.append(authorityEntry.mHost);
        stringBuilder.append("\": ");
        stringBuilder.append(authorityEntry.mPort);
        if (authorityEntry.mWild)
          stringBuilder.append(" WILD"); 
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    ArrayList<PatternMatcher> arrayList2 = this.mDataPaths;
    if (arrayList2 != null) {
      Iterator<PatternMatcher> iterator = arrayList2.iterator();
      while (iterator.hasNext()) {
        PatternMatcher patternMatcher = iterator.next();
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Path: \"");
        stringBuilder.append(patternMatcher);
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    ArrayList<String> arrayList1 = this.mStaticDataTypes;
    if (arrayList1 != null) {
      Iterator<String> iterator = arrayList1.iterator();
      while (iterator.hasNext()) {
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("StaticType: \"");
        stringBuilder.append(iterator.next());
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    arrayList1 = this.mDataTypes;
    if (arrayList1 != null) {
      Iterator<String> iterator = arrayList1.iterator();
      while (iterator.hasNext()) {
        String str = iterator.next();
        if (hasExactStaticDataType(str))
          continue; 
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("Type: \"");
        stringBuilder.append(str);
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    arrayList1 = this.mMimeGroups;
    if (arrayList1 != null) {
      Iterator<String> iterator = arrayList1.iterator();
      while (iterator.hasNext()) {
        stringBuilder.setLength(0);
        stringBuilder.append(paramString);
        stringBuilder.append("MimeGroup: \"");
        stringBuilder.append(iterator.next());
        stringBuilder.append("\"");
        paramPrinter.println(stringBuilder.toString());
      } 
    } 
    if (this.mPriority != 0 || this.mOrder != 0 || hasPartialTypes()) {
      stringBuilder.setLength(0);
      stringBuilder.append(paramString);
      stringBuilder.append("mPriority=");
      stringBuilder.append(this.mPriority);
      stringBuilder.append(", mOrder=");
      stringBuilder.append(this.mOrder);
      stringBuilder.append(", mHasStaticPartialTypes=");
      stringBuilder.append(this.mHasStaticPartialTypes);
      stringBuilder.append(", mHasDynamicPartialTypes=");
      stringBuilder.append(this.mHasDynamicPartialTypes);
      paramPrinter.println(stringBuilder.toString());
    } 
    if (getAutoVerify()) {
      stringBuilder.setLength(0);
      stringBuilder.append(paramString);
      stringBuilder.append("AutoVerify=");
      stringBuilder.append(getAutoVerify());
      paramPrinter.println(stringBuilder.toString());
    } 
  }
  
  public static final Parcelable.Creator<IntentFilter> CREATOR = new Parcelable.Creator<IntentFilter>() {
      public IntentFilter createFromParcel(Parcel param1Parcel) {
        return new IntentFilter(param1Parcel);
      }
      
      public IntentFilter[] newArray(int param1Int) {
        return new IntentFilter[param1Int];
      }
    };
  
  private static final String ACTION_STR = "action";
  
  private static final String AGLOB_STR = "aglob";
  
  private static final String AUTH_STR = "auth";
  
  private static final String AUTO_VERIFY_STR = "autoVerify";
  
  private static final String CAT_STR = "cat";
  
  private static final String GROUP_STR = "group";
  
  private static final String HOST_STR = "host";
  
  private static final String LITERAL_STR = "literal";
  
  public static final int MATCH_ADJUSTMENT_MASK = 65535;
  
  public static final int MATCH_ADJUSTMENT_NORMAL = 32768;
  
  public static final int MATCH_CATEGORY_EMPTY = 1048576;
  
  public static final int MATCH_CATEGORY_HOST = 3145728;
  
  public static final int MATCH_CATEGORY_MASK = 268369920;
  
  public static final int MATCH_CATEGORY_PATH = 5242880;
  
  public static final int MATCH_CATEGORY_PORT = 4194304;
  
  public static final int MATCH_CATEGORY_SCHEME = 2097152;
  
  public static final int MATCH_CATEGORY_SCHEME_SPECIFIC_PART = 5767168;
  
  public static final int MATCH_CATEGORY_TYPE = 6291456;
  
  private static final String NAME_STR = "name";
  
  public static final int NO_MATCH_ACTION = -3;
  
  public static final int NO_MATCH_CATEGORY = -4;
  
  public static final int NO_MATCH_DATA = -2;
  
  public static final int NO_MATCH_TYPE = -1;
  
  private static final String PATH_STR = "path";
  
  private static final String PORT_STR = "port";
  
  private static final String PREFIX_STR = "prefix";
  
  public static final String SCHEME_HTTP = "http";
  
  public static final String SCHEME_HTTPS = "https";
  
  private static final String SCHEME_STR = "scheme";
  
  private static final String SGLOB_STR = "sglob";
  
  private static final String SSP_STR = "ssp";
  
  private static final int STATE_NEED_VERIFY = 16;
  
  private static final int STATE_NEED_VERIFY_CHECKED = 256;
  
  private static final int STATE_VERIFIED = 4096;
  
  private static final int STATE_VERIFY_AUTO = 1;
  
  private static final String STATIC_TYPE_STR = "staticType";
  
  public static final int SYSTEM_HIGH_PRIORITY = 1000;
  
  public static final int SYSTEM_LOW_PRIORITY = -1000;
  
  private static final String TYPE_STR = "type";
  
  public static final int VISIBILITY_EXPLICIT = 1;
  
  public static final int VISIBILITY_IMPLICIT = 2;
  
  public static final int VISIBILITY_NONE = 0;
  
  public static final String WILDCARD = "*";
  
  public static final String WILDCARD_PATH = "/*";
  
  private final ArrayList<String> mActions;
  
  private boolean mHasDynamicPartialTypes;
  
  private boolean mHasStaticPartialTypes;
  
  private int mInstantAppVisibility;
  
  private int mOrder;
  
  private int mPriority;
  
  private int mVerifyState;
  
  public final int describeContents() {
    return 0;
  }
  
  public final void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringList(this.mActions);
    if (this.mCategories != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStringList(this.mCategories);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mDataSchemes != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStringList(this.mDataSchemes);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mStaticDataTypes != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStringList(this.mStaticDataTypes);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mDataTypes != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStringList(this.mDataTypes);
    } else {
      paramParcel.writeInt(0);
    } 
    if (this.mMimeGroups != null) {
      paramParcel.writeInt(1);
      paramParcel.writeStringList(this.mMimeGroups);
    } else {
      paramParcel.writeInt(0);
    } 
    ArrayList<PatternMatcher> arrayList2 = this.mDataSchemeSpecificParts;
    if (arrayList2 != null) {
      int i = arrayList2.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        ((PatternMatcher)this.mDataSchemeSpecificParts.get(b)).writeToParcel(paramParcel, paramInt); 
    } else {
      paramParcel.writeInt(0);
    } 
    ArrayList<AuthorityEntry> arrayList = this.mDataAuthorities;
    if (arrayList != null) {
      int i = arrayList.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        ((AuthorityEntry)this.mDataAuthorities.get(b)).writeToParcel(paramParcel); 
    } else {
      paramParcel.writeInt(0);
    } 
    ArrayList<PatternMatcher> arrayList1 = this.mDataPaths;
    if (arrayList1 != null) {
      int i = arrayList1.size();
      paramParcel.writeInt(i);
      for (byte b = 0; b < i; b++)
        ((PatternMatcher)this.mDataPaths.get(b)).writeToParcel(paramParcel, paramInt); 
    } else {
      paramParcel.writeInt(0);
    } 
    paramParcel.writeInt(this.mPriority);
    paramParcel.writeInt(this.mHasStaticPartialTypes);
    paramParcel.writeInt(this.mHasDynamicPartialTypes);
    paramParcel.writeInt(getAutoVerify());
    paramParcel.writeInt(this.mInstantAppVisibility);
    paramParcel.writeInt(this.mOrder);
  }
  
  public boolean debugCheck() {
    return true;
  }
  
  private boolean hasPartialTypes() {
    return (this.mHasStaticPartialTypes || this.mHasDynamicPartialTypes);
  }
  
  private final boolean findMimeType(String paramString) {
    ArrayList<String> arrayList = this.mDataTypes;
    if (paramString == null)
      return false; 
    if (arrayList.contains(paramString))
      return true; 
    int i = paramString.length();
    if (i == 3 && paramString.equals("*/*"))
      return arrayList.isEmpty() ^ true; 
    if (hasPartialTypes() && arrayList.contains("*"))
      return true; 
    int j = paramString.indexOf('/');
    if (j > 0) {
      if (hasPartialTypes() && arrayList.contains(paramString.substring(0, j)))
        return true; 
      if (i == j + 2 && paramString.charAt(j + 1) == '*') {
        int k = arrayList.size();
        for (i = 0; i < k; i++) {
          String str = arrayList.get(i);
          if (paramString.regionMatches(0, str, 0, j + 1))
            return true; 
        } 
      } 
    } 
    return false;
  }
  
  public ArrayList<String> getHostsList() {
    ArrayList<String> arrayList = new ArrayList();
    Iterator<AuthorityEntry> iterator = authoritiesIterator();
    if (iterator != null)
      while (iterator.hasNext()) {
        AuthorityEntry authorityEntry = iterator.next();
        arrayList.add(authorityEntry.getHost());
      }  
    return arrayList;
  }
  
  public String[] getHosts() {
    ArrayList<String> arrayList = getHostsList();
    return arrayList.<String>toArray(new String[arrayList.size()]);
  }
}
