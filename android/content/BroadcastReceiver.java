package android.content;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.ActivityThread;
import android.app.IActivityManager;
import android.app.QueuedWork;
import android.os.Bundle;
import android.os.Debug;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.UserHandle;
import android.util.Log;
import android.util.Slog;

public abstract class BroadcastReceiver {
  private boolean mDebugUnregister;
  
  private PendingResult mPendingResult;
  
  public static class PendingResult {
    public static final int TYPE_COMPONENT = 0;
    
    public static final int TYPE_REGISTERED = 1;
    
    public static final int TYPE_UNREGISTERED = 2;
    
    boolean mAbortBroadcast;
    
    boolean mFinished;
    
    final int mFlags;
    
    int mHasCode;
    
    final boolean mInitialStickyHint;
    
    final boolean mOrderedHint;
    
    int mResultCode;
    
    String mResultData;
    
    Bundle mResultExtras;
    
    final int mSendingUser;
    
    final IBinder mToken;
    
    final int mType;
    
    public PendingResult(int param1Int1, String param1String, Bundle param1Bundle, int param1Int2, boolean param1Boolean1, boolean param1Boolean2, IBinder param1IBinder, int param1Int3, int param1Int4) {
      this.mResultCode = param1Int1;
      this.mResultData = param1String;
      this.mResultExtras = param1Bundle;
      this.mType = param1Int2;
      this.mOrderedHint = param1Boolean1;
      this.mInitialStickyHint = param1Boolean2;
      this.mToken = param1IBinder;
      this.mSendingUser = param1Int3;
      this.mFlags = param1Int4;
    }
    
    public final void setHascode(int param1Int) {
      this.mHasCode = param1Int;
    }
    
    public final boolean getOrder() {
      return this.mOrderedHint;
    }
    
    public final void setBroadcastState(int param1Int1, int param1Int2) {
      if ((0x80000 & param1Int1) != 0) {
        if ((param1Int1 & 0x10000000) != 0) {
          if (ActivityThread.DEBUG_BROADCAST) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("mOppoFgBrState ");
            stringBuilder.append(param1Int2);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          ActivityThread.mOppoFgBrState = param1Int2;
        } else {
          if (ActivityThread.DEBUG_BROADCAST) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("mOppoBgBrState ");
            stringBuilder.append(param1Int2);
            Slog.v("ActivityThread", stringBuilder.toString());
          } 
          ActivityThread.mOppoBgBrState = param1Int2;
        } 
      } else if ((param1Int1 & 0x10000000) != 0) {
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("mFgBrState ");
          stringBuilder.append(param1Int2);
          Slog.v("ActivityThread", stringBuilder.toString());
        } 
        ActivityThread.mFgBrState = param1Int2;
      } else {
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("mBgBrState ");
          stringBuilder.append(param1Int2);
          Slog.v("ActivityThread", stringBuilder.toString());
        } 
        ActivityThread.mBgBrState = param1Int2;
      } 
    }
    
    public final void setResultCode(int param1Int) {
      checkSynchronousHint();
      this.mResultCode = param1Int;
    }
    
    public final int getResultCode() {
      return this.mResultCode;
    }
    
    public final void setResultData(String param1String) {
      checkSynchronousHint();
      this.mResultData = param1String;
    }
    
    public final String getResultData() {
      return this.mResultData;
    }
    
    public final void setResultExtras(Bundle param1Bundle) {
      checkSynchronousHint();
      this.mResultExtras = param1Bundle;
    }
    
    public final Bundle getResultExtras(boolean param1Boolean) {
      Bundle bundle1 = this.mResultExtras;
      if (!param1Boolean)
        return bundle1; 
      Bundle bundle2 = bundle1;
      if (bundle1 == null) {
        bundle2 = bundle1 = new Bundle();
        this.mResultExtras = bundle1;
      } 
      return bundle2;
    }
    
    public final void setResult(int param1Int, String param1String, Bundle param1Bundle) {
      checkSynchronousHint();
      this.mResultCode = param1Int;
      this.mResultData = param1String;
      this.mResultExtras = param1Bundle;
    }
    
    public final boolean getAbortBroadcast() {
      return this.mAbortBroadcast;
    }
    
    public final void abortBroadcast() {
      checkSynchronousHint();
      this.mAbortBroadcast = true;
    }
    
    public final void clearAbortBroadcast() {
      this.mAbortBroadcast = false;
    }
    
    public final void finish() {
      if (ActivityThread.DEBUG_BROADCAST) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Finishing broadcast to mType ");
        stringBuilder.append(this.mType);
        Slog.i("ActivityThread", stringBuilder.toString());
      } 
      int i = this.mType;
      if (i == 0) {
        final IActivityManager mgr = ActivityManager.getService();
        if (QueuedWork.hasPendingWork()) {
          QueuedWork.queue(new Runnable() {
                final BroadcastReceiver.PendingResult this$0;
                
                final IActivityManager val$mgr;
                
                public void run() {
                  if (ActivityThread.DEBUG_BROADCAST) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Finishing broadcast after work to component ");
                    stringBuilder.append(BroadcastReceiver.PendingResult.this.mToken);
                    Slog.i("ActivityThread", stringBuilder.toString());
                  } 
                  BroadcastReceiver.PendingResult.this.sendFinished(mgr);
                }
              }false);
        } else {
          if (ActivityThread.DEBUG_BROADCAST) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Finishing broadcast to component ");
            stringBuilder.append(this.mToken);
            Slog.i("ActivityThread", stringBuilder.toString());
          } 
          sendFinished(iActivityManager);
        } 
      } else if (this.mOrderedHint && i != 2) {
        if (ActivityThread.DEBUG_BROADCAST) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Finishing broadcast to ");
          stringBuilder.append(this.mToken);
          Slog.i("ActivityThread", stringBuilder.toString());
        } 
        final IActivityManager mgr = ActivityManager.getService();
        sendFinished(iActivityManager);
      } 
    }
    
    public void setExtrasClassLoader(ClassLoader param1ClassLoader) {
      Bundle bundle = this.mResultExtras;
      if (bundle != null)
        bundle.setClassLoader(param1ClassLoader); 
    }
    
    public void sendFinished(IActivityManager param1IActivityManager) {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mFinished : Z
      //   6: ifne -> 110
      //   9: aload_0
      //   10: iconst_1
      //   11: putfield mFinished : Z
      //   14: aload_0
      //   15: getfield mResultExtras : Landroid/os/Bundle;
      //   18: ifnull -> 30
      //   21: aload_0
      //   22: getfield mResultExtras : Landroid/os/Bundle;
      //   25: iconst_0
      //   26: invokevirtual setAllowFds : (Z)Z
      //   29: pop
      //   30: aload_0
      //   31: getfield mOrderedHint : Z
      //   34: ifeq -> 79
      //   37: aload_0
      //   38: aload_0
      //   39: getfield mFlags : I
      //   42: iconst_0
      //   43: invokevirtual setBroadcastState : (II)V
      //   46: aload_1
      //   47: aload_0
      //   48: getfield mToken : Landroid/os/IBinder;
      //   51: aload_0
      //   52: getfield mResultCode : I
      //   55: aload_0
      //   56: getfield mResultData : Ljava/lang/String;
      //   59: aload_0
      //   60: getfield mResultExtras : Landroid/os/Bundle;
      //   63: aload_0
      //   64: getfield mAbortBroadcast : Z
      //   67: aload_0
      //   68: getfield mFlags : I
      //   71: invokeinterface finishReceiver : (Landroid/os/IBinder;ILjava/lang/String;Landroid/os/Bundle;ZI)V
      //   76: goto -> 103
      //   79: new android/app/OplusActivityManager
      //   82: astore_1
      //   83: aload_1
      //   84: invokespecial <init> : ()V
      //   87: aload_1
      //   88: aload_0
      //   89: getfield mToken : Landroid/os/IBinder;
      //   92: aload_0
      //   93: getfield mHasCode : I
      //   96: iconst_0
      //   97: aconst_null
      //   98: aconst_null
      //   99: iconst_0
      //   100: invokevirtual finishNotOrderReceiver : (Landroid/os/IBinder;IILjava/lang/String;Landroid/os/Bundle;Z)V
      //   103: goto -> 107
      //   106: astore_1
      //   107: aload_0
      //   108: monitorexit
      //   109: return
      //   110: new java/lang/IllegalStateException
      //   113: astore_1
      //   114: aload_1
      //   115: ldc 'Broadcast already finished'
      //   117: invokespecial <init> : (Ljava/lang/String;)V
      //   120: aload_1
      //   121: athrow
      //   122: astore_1
      //   123: aload_0
      //   124: monitorexit
      //   125: aload_1
      //   126: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #321	-> 0
      //   #322	-> 2
      //   #325	-> 9
      //   #328	-> 14
      //   #329	-> 21
      //   #331	-> 30
      //   #334	-> 37
      //   #336	-> 46
      //   #347	-> 79
      //   #351	-> 103
      //   #350	-> 106
      //   #352	-> 107
      //   #353	-> 109
      //   #323	-> 110
      //   #352	-> 122
      // Exception table:
      //   from	to	target	type
      //   2	9	122	finally
      //   9	14	122	finally
      //   14	21	106	android/os/RemoteException
      //   14	21	122	finally
      //   21	30	106	android/os/RemoteException
      //   21	30	122	finally
      //   30	37	106	android/os/RemoteException
      //   30	37	122	finally
      //   37	46	106	android/os/RemoteException
      //   37	46	122	finally
      //   46	76	106	android/os/RemoteException
      //   46	76	122	finally
      //   79	103	106	android/os/RemoteException
      //   79	103	122	finally
      //   107	109	122	finally
      //   110	122	122	finally
      //   123	125	122	finally
    }
    
    public int getSendingUserId() {
      return this.mSendingUser;
    }
    
    void checkSynchronousHint() {
      if (this.mOrderedHint || this.mInitialStickyHint)
        return; 
      RuntimeException runtimeException = new RuntimeException("BroadcastReceiver trying to return result during a non-ordered broadcast");
      runtimeException.fillInStackTrace();
      Log.e("BroadcastReceiver", runtimeException.getMessage(), runtimeException);
    }
  }
  
  class null implements Runnable {
    final BroadcastReceiver.PendingResult this$0;
    
    final IActivityManager val$mgr;
    
    public void run() {
      if (ActivityThread.DEBUG_BROADCAST) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Finishing broadcast after work to component ");
        stringBuilder.append(this.this$0.mToken);
        Slog.i("ActivityThread", stringBuilder.toString());
      } 
      this.this$0.sendFinished(mgr);
    }
  }
  
  public final PendingResult goAsync() {
    if (ActivityThread.DEBUG_BROADCAST) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("goAsync ");
      stringBuilder.append(this.mPendingResult);
      stringBuilder.append(" this ");
      stringBuilder.append(this);
      stringBuilder.append(" ");
      stringBuilder.append(Debug.getCallers(4));
      String str = stringBuilder.toString();
      Slog.i("ActivityThread", str);
    } 
    PendingResult pendingResult = this.mPendingResult;
    this.mPendingResult = null;
    return pendingResult;
  }
  
  public IBinder peekService(Context paramContext, Intent paramIntent) {
    IActivityManager iActivityManager = ActivityManager.getService();
    RemoteException remoteException2 = null;
    try {
      paramIntent.prepareToLeaveProcess(paramContext);
      ContentResolver contentResolver = paramContext.getContentResolver();
      String str2 = paramIntent.resolveTypeIfNeeded(contentResolver);
      String str1 = paramContext.getOpPackageName();
      IBinder iBinder = iActivityManager.peekService(paramIntent, str2, str1);
    } catch (RemoteException remoteException1) {
      remoteException1 = remoteException2;
    } 
    return (IBinder)remoteException1;
  }
  
  public final void setResultCode(int paramInt) {
    checkSynchronousHint();
    this.mPendingResult.mResultCode = paramInt;
  }
  
  public final int getResultCode() {
    boolean bool;
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      bool = pendingResult.mResultCode;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void setResultData(String paramString) {
    checkSynchronousHint();
    this.mPendingResult.mResultData = paramString;
  }
  
  public final String getResultData() {
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      String str = pendingResult.mResultData;
    } else {
      pendingResult = null;
    } 
    return (String)pendingResult;
  }
  
  public final void setResultExtras(Bundle paramBundle) {
    checkSynchronousHint();
    this.mPendingResult.mResultExtras = paramBundle;
  }
  
  public final Bundle getResultExtras(boolean paramBoolean) {
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult == null)
      return null; 
    Bundle bundle2 = pendingResult.mResultExtras;
    if (!paramBoolean)
      return bundle2; 
    Bundle bundle1 = bundle2;
    if (bundle2 == null) {
      PendingResult pendingResult1 = this.mPendingResult;
      bundle1 = bundle2 = new Bundle();
      pendingResult1.mResultExtras = bundle2;
    } 
    return bundle1;
  }
  
  public final void setResult(int paramInt, String paramString, Bundle paramBundle) {
    checkSynchronousHint();
    this.mPendingResult.mResultCode = paramInt;
    this.mPendingResult.mResultData = paramString;
    this.mPendingResult.mResultExtras = paramBundle;
  }
  
  public final boolean getAbortBroadcast() {
    boolean bool;
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      bool = pendingResult.mAbortBroadcast;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void abortBroadcast() {
    checkSynchronousHint();
    this.mPendingResult.mAbortBroadcast = true;
  }
  
  public final void clearAbortBroadcast() {
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null)
      pendingResult.mAbortBroadcast = false; 
  }
  
  public final boolean isOrderedBroadcast() {
    boolean bool;
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      bool = pendingResult.mOrderedHint;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean isInitialStickyBroadcast() {
    boolean bool;
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      bool = pendingResult.mInitialStickyHint;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final void setOrderedHint(boolean paramBoolean) {}
  
  public final void setPendingResult(PendingResult paramPendingResult) {
    if (ActivityThread.DEBUG_BROADCAST) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("setPendingResult ");
      stringBuilder.append(paramPendingResult);
      stringBuilder.append(" this ");
      stringBuilder.append(this);
      Slog.i("ActivityThread", stringBuilder.toString());
    } 
    this.mPendingResult = paramPendingResult;
  }
  
  public final PendingResult getPendingResult() {
    if (ActivityThread.DEBUG_BROADCAST) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("getPendingResult ");
      stringBuilder.append(this.mPendingResult);
      stringBuilder.append(" this ");
      stringBuilder.append(this);
      Slog.i("ActivityThread", stringBuilder.toString());
    } 
    return this.mPendingResult;
  }
  
  @SystemApi
  public final UserHandle getSendingUser() {
    return UserHandle.of(getSendingUserId());
  }
  
  public int getSendingUserId() {
    return this.mPendingResult.mSendingUser;
  }
  
  public final void setDebugUnregister(boolean paramBoolean) {
    this.mDebugUnregister = paramBoolean;
  }
  
  public final boolean getDebugUnregister() {
    return this.mDebugUnregister;
  }
  
  void checkSynchronousHint() {
    PendingResult pendingResult = this.mPendingResult;
    if (pendingResult != null) {
      if (pendingResult.mOrderedHint || this.mPendingResult.mInitialStickyHint)
        return; 
      RuntimeException runtimeException = new RuntimeException("BroadcastReceiver trying to return result during a non-ordered broadcast");
      runtimeException.fillInStackTrace();
      Log.e("BroadcastReceiver", runtimeException.getMessage(), runtimeException);
      return;
    } 
    throw new IllegalStateException("Call while result is not pending");
  }
  
  public abstract void onReceive(Context paramContext, Intent paramIntent);
}
