package android.content;

import android.os.OplusThermalState;
import java.util.List;

public abstract class OplusBatteryStatsInternal {
  public abstract List<String> getUid0ProcessListImpl();
  
  public abstract List<String> getUid1kProcessListImpl();
  
  public abstract List<String> getUidPowerListImpl();
  
  public abstract void noteScreenBrightnessModeChangedImpl(boolean paramBoolean);
  
  public abstract void restOpppBatteryStatsImpl();
  
  public abstract void setThermalConfigImpl();
  
  public abstract void setThermalStateImpl(OplusThermalState paramOplusThermalState);
}
