package android.media;

import android.util.Log;
import java.util.Vector;

class WebVttParser {
  private static final String TAG = "WebVttParser";
  
  private String mBuffer;
  
  private TextTrackCue mCue;
  
  private Vector<String> mCueTexts;
  
  private WebVttCueListener mListener;
  
  private final Phase mParseCueId;
  
  private final Phase mParseCueText;
  
  private final Phase mParseCueTime;
  
  private final Phase mParseHeader;
  
  private final Phase mParseStart;
  
  private Phase mPhase;
  
  private final Phase mSkipRest;
  
  WebVttParser(WebVttCueListener paramWebVttCueListener) {
    this.mSkipRest = (Phase)new Object(this);
    this.mParseStart = (Phase)new Object(this);
    this.mParseHeader = (Phase)new Object(this);
    this.mParseCueId = (Phase)new Object(this);
    this.mParseCueTime = (Phase)new Object(this);
    this.mParseCueText = (Phase)new Object(this);
    this.mPhase = this.mParseStart;
    this.mBuffer = "";
    this.mListener = paramWebVttCueListener;
    this.mCueTexts = new Vector<>();
  }
  
  public static float parseFloatPercentage(String paramString) throws NumberFormatException {
    if (paramString.endsWith("%")) {
      paramString = paramString.substring(0, paramString.length() - 1);
      if (!paramString.matches(".*[^0-9.].*"))
        try {
          float f = Float.parseFloat(paramString);
          if (f >= 0.0F && f <= 100.0F)
            return f; 
          NumberFormatException numberFormatException = new NumberFormatException();
          this("is out of range");
          throw numberFormatException;
        } catch (NumberFormatException numberFormatException) {
          throw new NumberFormatException("is not a number");
        }  
      throw new NumberFormatException("contains an invalid character");
    } 
    throw new NumberFormatException("does not end in %");
  }
  
  public static int parseIntPercentage(String paramString) throws NumberFormatException {
    if (paramString.endsWith("%")) {
      paramString = paramString.substring(0, paramString.length() - 1);
      if (!paramString.matches(".*[^0-9].*"))
        try {
          int i = Integer.parseInt(paramString);
          if (i >= 0 && i <= 100)
            return i; 
          NumberFormatException numberFormatException = new NumberFormatException();
          this("is out of range");
          throw numberFormatException;
        } catch (NumberFormatException numberFormatException) {
          throw new NumberFormatException("is not a number");
        }  
      throw new NumberFormatException("contains an invalid character");
    } 
    throw new NumberFormatException("does not end in %");
  }
  
  public static long parseTimestampMs(String paramString) throws NumberFormatException {
    if (paramString.matches("(\\d+:)?[0-5]\\d:[0-5]\\d\\.\\d{3}")) {
      String[] arrayOfString2 = paramString.split("\\.", 2);
      long l = 0L;
      String[] arrayOfString1;
      byte b;
      int i;
      for (b = 0, arrayOfString1 = arrayOfString2[0].split(":"), i = arrayOfString1.length; b < i; ) {
        String str = arrayOfString1[b];
        l = 60L * l + Long.parseLong(str);
        b++;
      } 
      return 1000L * l + Long.parseLong(arrayOfString2[1]);
    } 
    throw new NumberFormatException("has invalid format");
  }
  
  public static String timeToString(long paramLong) {
    long l1 = paramLong / 3600000L;
    long l2 = paramLong / 60000L, l3 = paramLong / 1000L;
    return String.format("%d:%02d:%02d.%03d", new Object[] { Long.valueOf(l1), Long.valueOf(l2 % 60L), Long.valueOf(l3 % 60L), Long.valueOf(paramLong % 1000L) });
  }
  
  public void parse(String paramString) {
    boolean bool = false;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mBuffer);
    stringBuilder.append(paramString.replace("\000", "�"));
    this.mBuffer = paramString = stringBuilder.toString().replace("\r\n", "\n");
    if (paramString.endsWith("\r")) {
      bool = true;
      paramString = this.mBuffer;
      this.mBuffer = paramString.substring(0, paramString.length() - 1);
    } 
    String[] arrayOfString = this.mBuffer.split("[\r\n]");
    for (byte b = 0; b < arrayOfString.length - 1; b++)
      this.mPhase.parse(arrayOfString[b]); 
    this.mBuffer = arrayOfString[arrayOfString.length - 1];
    if (bool) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(this.mBuffer);
      stringBuilder1.append("\r");
      this.mBuffer = stringBuilder1.toString();
    } 
  }
  
  public void eos() {
    if (this.mBuffer.endsWith("\r")) {
      String str = this.mBuffer;
      this.mBuffer = str.substring(0, str.length() - 1);
    } 
    this.mPhase.parse(this.mBuffer);
    this.mBuffer = "";
    yieldCue();
    this.mPhase = this.mParseStart;
  }
  
  public void yieldCue() {
    if (this.mCue != null && this.mCueTexts.size() > 0) {
      this.mCue.mStrings = new String[this.mCueTexts.size()];
      this.mCueTexts.toArray(this.mCue.mStrings);
      this.mCueTexts.clear();
      this.mListener.onCueParsed(this.mCue);
    } 
    this.mCue = null;
  }
  
  private void log_warning(String paramString1, String paramString2, String paramString3, String paramString4, String paramString5) {
    String str = getClass().getName();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" '");
    stringBuilder.append(paramString2);
    stringBuilder.append("' ");
    stringBuilder.append(paramString3);
    stringBuilder.append(" ('");
    stringBuilder.append(paramString5);
    stringBuilder.append("' ");
    stringBuilder.append(paramString4);
    stringBuilder.append(")");
    Log.w(str, stringBuilder.toString());
  }
  
  private void log_warning(String paramString1, String paramString2, String paramString3, String paramString4) {
    String str = getClass().getName();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" '");
    stringBuilder.append(paramString2);
    stringBuilder.append("' ");
    stringBuilder.append(paramString3);
    stringBuilder.append(" ('");
    stringBuilder.append(paramString4);
    stringBuilder.append("')");
    Log.w(str, stringBuilder.toString());
  }
  
  private void log_warning(String paramString1, String paramString2) {
    String str = getClass().getName();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(" ('");
    stringBuilder.append(paramString2);
    stringBuilder.append("')");
    Log.w(str, stringBuilder.toString());
  }
  
  static interface Phase {
    void parse(String param1String);
  }
}
