package android.media;

public class AudioPort {
  public static final int ROLE_NONE = 0;
  
  public static final int ROLE_SINK = 2;
  
  public static final int ROLE_SOURCE = 1;
  
  private static final String TAG = "AudioPort";
  
  public static final int TYPE_DEVICE = 1;
  
  public static final int TYPE_NONE = 0;
  
  public static final int TYPE_SESSION = 3;
  
  public static final int TYPE_SUBMIX = 2;
  
  private AudioPortConfig mActiveConfig;
  
  private final int[] mChannelIndexMasks;
  
  private final int[] mChannelMasks;
  
  private final int[] mFormats;
  
  private final AudioGain[] mGains;
  
  AudioHandle mHandle;
  
  private final String mName;
  
  protected final int mRole;
  
  private final int[] mSamplingRates;
  
  AudioPort(AudioHandle paramAudioHandle, int paramInt, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, int[] paramArrayOfint4, AudioGain[] paramArrayOfAudioGain) {
    this.mHandle = paramAudioHandle;
    this.mRole = paramInt;
    this.mName = paramString;
    this.mSamplingRates = paramArrayOfint1;
    this.mChannelMasks = paramArrayOfint2;
    this.mChannelIndexMasks = paramArrayOfint3;
    this.mFormats = paramArrayOfint4;
    this.mGains = paramArrayOfAudioGain;
  }
  
  AudioHandle handle() {
    return this.mHandle;
  }
  
  public int id() {
    return this.mHandle.id();
  }
  
  public int role() {
    return this.mRole;
  }
  
  public String name() {
    return this.mName;
  }
  
  public int[] samplingRates() {
    return this.mSamplingRates;
  }
  
  public int[] channelMasks() {
    return this.mChannelMasks;
  }
  
  public int[] channelIndexMasks() {
    return this.mChannelIndexMasks;
  }
  
  public int[] formats() {
    return this.mFormats;
  }
  
  public AudioGain[] gains() {
    return this.mGains;
  }
  
  AudioGain gain(int paramInt) {
    if (paramInt >= 0) {
      AudioGain[] arrayOfAudioGain = this.mGains;
      if (paramInt < arrayOfAudioGain.length)
        return arrayOfAudioGain[paramInt]; 
    } 
    return null;
  }
  
  public AudioPortConfig buildConfig(int paramInt1, int paramInt2, int paramInt3, AudioGainConfig paramAudioGainConfig) {
    return new AudioPortConfig(this, paramInt1, paramInt2, paramInt3, paramAudioGainConfig);
  }
  
  public AudioPortConfig activeConfig() {
    return this.mActiveConfig;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null || !(paramObject instanceof AudioPort))
      return false; 
    paramObject = paramObject;
    return this.mHandle.equals(paramObject.handle());
  }
  
  public int hashCode() {
    return this.mHandle.hashCode();
  }
  
  public String toString() {
    String str = Integer.toString(this.mRole);
    int i = this.mRole;
    if (i != 0) {
      if (i != 1) {
        if (i == 2)
          str = "SINK"; 
      } else {
        str = "SOURCE";
      } 
    } else {
      str = "NONE";
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{mHandle: ");
    stringBuilder.append(this.mHandle);
    stringBuilder.append(", mRole: ");
    stringBuilder.append(str);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
