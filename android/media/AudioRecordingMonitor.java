package android.media;

import java.util.concurrent.Executor;

public interface AudioRecordingMonitor {
  AudioRecordingConfiguration getActiveRecordingConfiguration();
  
  void registerAudioRecordingCallback(Executor paramExecutor, AudioManager.AudioRecordingCallback paramAudioRecordingCallback);
  
  void unregisterAudioRecordingCallback(AudioManager.AudioRecordingCallback paramAudioRecordingCallback);
}
