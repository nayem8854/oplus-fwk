package android.media;

class TextTrackCue extends SubtitleTrack.Cue {
  String mId = "";
  
  boolean mPauseOnExit = false;
  
  int mWritingDirection = 100;
  
  String mRegionId = "";
  
  boolean mSnapToLines = true;
  
  Integer mLinePosition = null;
  
  int mTextPosition = 50;
  
  int mSize = 100;
  
  int mAlignment = 200;
  
  TextTrackCueSpan[][] mLines = null;
  
  TextTrackRegion mRegion = null;
  
  static final int ALIGNMENT_END = 202;
  
  static final int ALIGNMENT_LEFT = 203;
  
  static final int ALIGNMENT_MIDDLE = 200;
  
  static final int ALIGNMENT_RIGHT = 204;
  
  static final int ALIGNMENT_START = 201;
  
  private static final String TAG = "TTCue";
  
  static final int WRITING_DIRECTION_HORIZONTAL = 100;
  
  static final int WRITING_DIRECTION_VERTICAL_LR = 102;
  
  static final int WRITING_DIRECTION_VERTICAL_RL = 101;
  
  boolean mAutoLinePosition;
  
  String[] mStrings;
  
  public boolean equals(Object paramObject) {
    // Byte code:
    //   0: aload_1
    //   1: instanceof android/media/TextTrackCue
    //   4: ifne -> 9
    //   7: iconst_0
    //   8: ireturn
    //   9: aload_0
    //   10: aload_1
    //   11: if_acmpne -> 16
    //   14: iconst_1
    //   15: ireturn
    //   16: aload_1
    //   17: checkcast android/media/TextTrackCue
    //   20: astore_1
    //   21: aload_0
    //   22: getfield mId : Ljava/lang/String;
    //   25: aload_1
    //   26: getfield mId : Ljava/lang/String;
    //   29: invokevirtual equals : (Ljava/lang/Object;)Z
    //   32: ifeq -> 195
    //   35: aload_0
    //   36: getfield mPauseOnExit : Z
    //   39: aload_1
    //   40: getfield mPauseOnExit : Z
    //   43: if_icmpne -> 195
    //   46: aload_0
    //   47: getfield mWritingDirection : I
    //   50: aload_1
    //   51: getfield mWritingDirection : I
    //   54: if_icmpne -> 195
    //   57: aload_0
    //   58: getfield mRegionId : Ljava/lang/String;
    //   61: astore_2
    //   62: aload_1
    //   63: getfield mRegionId : Ljava/lang/String;
    //   66: astore_3
    //   67: aload_2
    //   68: aload_3
    //   69: invokevirtual equals : (Ljava/lang/Object;)Z
    //   72: ifeq -> 195
    //   75: aload_0
    //   76: getfield mSnapToLines : Z
    //   79: aload_1
    //   80: getfield mSnapToLines : Z
    //   83: if_icmpne -> 195
    //   86: aload_0
    //   87: getfield mAutoLinePosition : Z
    //   90: aload_1
    //   91: getfield mAutoLinePosition : Z
    //   94: if_icmpne -> 195
    //   97: aload_0
    //   98: getfield mAutoLinePosition : Z
    //   101: ifne -> 143
    //   104: aload_0
    //   105: getfield mLinePosition : Ljava/lang/Integer;
    //   108: ifnull -> 129
    //   111: aload_0
    //   112: getfield mLinePosition : Ljava/lang/Integer;
    //   115: astore_3
    //   116: aload_1
    //   117: getfield mLinePosition : Ljava/lang/Integer;
    //   120: astore_2
    //   121: aload_3
    //   122: aload_2
    //   123: invokevirtual equals : (Ljava/lang/Object;)Z
    //   126: ifne -> 143
    //   129: aload_0
    //   130: getfield mLinePosition : Ljava/lang/Integer;
    //   133: ifnonnull -> 195
    //   136: aload_1
    //   137: getfield mLinePosition : Ljava/lang/Integer;
    //   140: ifnonnull -> 195
    //   143: aload_0
    //   144: getfield mTextPosition : I
    //   147: aload_1
    //   148: getfield mTextPosition : I
    //   151: if_icmpne -> 195
    //   154: aload_0
    //   155: getfield mSize : I
    //   158: aload_1
    //   159: getfield mSize : I
    //   162: if_icmpne -> 195
    //   165: aload_0
    //   166: getfield mAlignment : I
    //   169: aload_1
    //   170: getfield mAlignment : I
    //   173: if_icmpne -> 195
    //   176: aload_0
    //   177: getfield mLines : [[Landroid/media/TextTrackCueSpan;
    //   180: arraylength
    //   181: aload_1
    //   182: getfield mLines : [[Landroid/media/TextTrackCueSpan;
    //   185: arraylength
    //   186: if_icmpne -> 195
    //   189: iconst_1
    //   190: istore #4
    //   192: goto -> 198
    //   195: iconst_0
    //   196: istore #4
    //   198: iload #4
    //   200: iconst_1
    //   201: if_icmpne -> 249
    //   204: iconst_0
    //   205: istore #5
    //   207: iload #5
    //   209: aload_0
    //   210: getfield mLines : [[Landroid/media/TextTrackCueSpan;
    //   213: arraylength
    //   214: if_icmpge -> 249
    //   217: aload_0
    //   218: getfield mLines : [[Landroid/media/TextTrackCueSpan;
    //   221: iload #5
    //   223: aaload
    //   224: aload_1
    //   225: getfield mLines : [[Landroid/media/TextTrackCueSpan;
    //   228: iload #5
    //   230: aaload
    //   231: invokestatic equals : ([Ljava/lang/Object;[Ljava/lang/Object;)Z
    //   234: istore #6
    //   236: iload #6
    //   238: ifne -> 243
    //   241: iconst_0
    //   242: ireturn
    //   243: iinc #5, 1
    //   246: goto -> 207
    //   249: iload #4
    //   251: ireturn
    //   252: astore_1
    //   253: iconst_0
    //   254: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #423	-> 0
    //   #424	-> 7
    //   #426	-> 9
    //   #427	-> 14
    //   #431	-> 16
    //   #432	-> 21
    //   #435	-> 67
    //   #439	-> 121
    //   #445	-> 198
    //   #446	-> 204
    //   #447	-> 217
    //   #448	-> 241
    //   #446	-> 243
    //   #452	-> 249
    //   #453	-> 252
    //   #454	-> 253
    // Exception table:
    //   from	to	target	type
    //   16	21	252	java/lang/IncompatibleClassChangeError
    //   21	67	252	java/lang/IncompatibleClassChangeError
    //   67	121	252	java/lang/IncompatibleClassChangeError
    //   121	129	252	java/lang/IncompatibleClassChangeError
    //   129	143	252	java/lang/IncompatibleClassChangeError
    //   143	189	252	java/lang/IncompatibleClassChangeError
    //   207	217	252	java/lang/IncompatibleClassChangeError
    //   217	236	252	java/lang/IncompatibleClassChangeError
  }
  
  public StringBuilder appendStringsToBuilder(StringBuilder paramStringBuilder) {
    if (this.mStrings == null) {
      paramStringBuilder.append("null");
    } else {
      paramStringBuilder.append("[");
      boolean bool = true;
      for (String str : this.mStrings) {
        if (!bool)
          paramStringBuilder.append(", "); 
        if (str == null) {
          paramStringBuilder.append("null");
        } else {
          paramStringBuilder.append("\"");
          paramStringBuilder.append(str);
          paramStringBuilder.append("\"");
        } 
        bool = false;
      } 
      paramStringBuilder.append("]");
    } 
    return paramStringBuilder;
  }
  
  public StringBuilder appendLinesToBuilder(StringBuilder paramStringBuilder) {
    TextTrackCueSpan[][] arrayOfTextTrackCueSpan = this.mLines;
    String str = "null";
    if (arrayOfTextTrackCueSpan == null) {
      paramStringBuilder.append("null");
    } else {
      paramStringBuilder.append("[");
      boolean bool = true;
      for (TextTrackCueSpan[] arrayOfTextTrackCueSpan1 : this.mLines) {
        if (!bool)
          paramStringBuilder.append(", "); 
        if (arrayOfTextTrackCueSpan1 == null) {
          paramStringBuilder.append(str);
        } else {
          paramStringBuilder.append("\"");
          boolean bool1 = true;
          long l = -1L;
          int i;
          byte b;
          for (i = arrayOfTextTrackCueSpan1.length, b = 0; b < i; ) {
            TextTrackCueSpan textTrackCueSpan = arrayOfTextTrackCueSpan1[b];
            if (!bool1)
              paramStringBuilder.append(" "); 
            long l1 = l;
            if (textTrackCueSpan.mTimestampMs != l) {
              paramStringBuilder.append("<");
              l1 = textTrackCueSpan.mTimestampMs;
              paramStringBuilder.append(WebVttParser.timeToString(l1));
              paramStringBuilder.append(">");
              l1 = textTrackCueSpan.mTimestampMs;
            } 
            paramStringBuilder.append(textTrackCueSpan.mText);
            bool1 = false;
            b++;
            l = l1;
          } 
          paramStringBuilder.append("\"");
        } 
        bool = false;
      } 
      paramStringBuilder.append("]");
    } 
    return paramStringBuilder;
  }
  
  public String toString() {
    String str3;
    Integer integer;
    String str2;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(WebVttParser.timeToString(this.mStartTimeMs));
    stringBuilder.append(" --> ");
    stringBuilder.append(WebVttParser.timeToString(this.mEndTimeMs));
    stringBuilder.append(" {id:\"");
    stringBuilder.append(this.mId);
    stringBuilder.append("\", pauseOnExit:");
    stringBuilder.append(this.mPauseOnExit);
    stringBuilder.append(", direction:");
    int i = this.mWritingDirection;
    String str1 = "INVALID";
    if (i == 100) {
      str3 = "horizontal";
    } else if (i == 102) {
      str3 = "vertical_lr";
    } else if (i == 101) {
      str3 = "vertical_rl";
    } else {
      str3 = "INVALID";
    } 
    stringBuilder.append(str3);
    stringBuilder.append(", regionId:\"");
    stringBuilder.append(this.mRegionId);
    stringBuilder.append("\", snapToLines:");
    stringBuilder.append(this.mSnapToLines);
    stringBuilder.append(", linePosition:");
    if (this.mAutoLinePosition) {
      str3 = "auto";
    } else {
      integer = this.mLinePosition;
    } 
    stringBuilder.append(integer);
    stringBuilder.append(", textPosition:");
    stringBuilder.append(this.mTextPosition);
    stringBuilder.append(", size:");
    stringBuilder.append(this.mSize);
    stringBuilder.append(", alignment:");
    i = this.mAlignment;
    if (i == 202) {
      str2 = "end";
    } else if (i == 203) {
      str2 = "left";
    } else if (i == 200) {
      str2 = "middle";
    } else if (i == 204) {
      str2 = "right";
    } else {
      str2 = str1;
      if (i == 201)
        str2 = "start"; 
    } 
    stringBuilder.append(str2);
    stringBuilder.append(", text:");
    appendStringsToBuilder(stringBuilder).append("}");
    return stringBuilder.toString();
  }
  
  public int hashCode() {
    return toString().hashCode();
  }
  
  public void onTime(long paramLong) {
    TextTrackCueSpan[][] arrayOfTextTrackCueSpan;
    int i;
    byte b;
    for (arrayOfTextTrackCueSpan = this.mLines, i = arrayOfTextTrackCueSpan.length, b = 0; b < i; ) {
      for (TextTrackCueSpan textTrackCueSpan : arrayOfTextTrackCueSpan[b]) {
        boolean bool;
        if (paramLong >= textTrackCueSpan.mTimestampMs) {
          bool = true;
        } else {
          bool = false;
        } 
        textTrackCueSpan.mEnabled = bool;
      } 
      b++;
    } 
  }
}
