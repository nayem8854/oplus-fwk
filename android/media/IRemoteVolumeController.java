package android.media;

import android.media.session.MediaSession;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteVolumeController extends IInterface {
  void remoteVolumeChanged(MediaSession.Token paramToken, int paramInt) throws RemoteException;
  
  void updateRemoteController(MediaSession.Token paramToken) throws RemoteException;
  
  class Default implements IRemoteVolumeController {
    public void remoteVolumeChanged(MediaSession.Token param1Token, int param1Int) throws RemoteException {}
    
    public void updateRemoteController(MediaSession.Token param1Token) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteVolumeController {
    private static final String DESCRIPTOR = "android.media.IRemoteVolumeController";
    
    static final int TRANSACTION_remoteVolumeChanged = 1;
    
    static final int TRANSACTION_updateRemoteController = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IRemoteVolumeController");
    }
    
    public static IRemoteVolumeController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRemoteVolumeController");
      if (iInterface != null && iInterface instanceof IRemoteVolumeController)
        return (IRemoteVolumeController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "updateRemoteController";
      } 
      return "remoteVolumeChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.IRemoteVolumeController");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.IRemoteVolumeController");
        if (param1Parcel1.readInt() != 0) {
          MediaSession.Token token = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        updateRemoteController((MediaSession.Token)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IRemoteVolumeController");
      if (param1Parcel1.readInt() != 0) {
        MediaSession.Token token = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      param1Int1 = param1Parcel1.readInt();
      remoteVolumeChanged((MediaSession.Token)param1Parcel2, param1Int1);
      return true;
    }
    
    private static class Proxy implements IRemoteVolumeController {
      public static IRemoteVolumeController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRemoteVolumeController";
      }
      
      public void remoteVolumeChanged(MediaSession.Token param2Token, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteVolumeController");
          if (param2Token != null) {
            parcel.writeInt(1);
            param2Token.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRemoteVolumeController.Stub.getDefaultImpl() != null) {
            IRemoteVolumeController.Stub.getDefaultImpl().remoteVolumeChanged(param2Token, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void updateRemoteController(MediaSession.Token param2Token) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteVolumeController");
          if (param2Token != null) {
            parcel.writeInt(1);
            param2Token.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IRemoteVolumeController.Stub.getDefaultImpl() != null) {
            IRemoteVolumeController.Stub.getDefaultImpl().updateRemoteController(param2Token);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteVolumeController param1IRemoteVolumeController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteVolumeController != null) {
          Proxy.sDefaultImpl = param1IRemoteVolumeController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteVolumeController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
