package android.media;

import android.content.Context;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MediaRouter2Manager {
  private static final Object sLock = new Object();
  
  final CopyOnWriteArrayList<CallbackRecord> mCallbackRecords = new CopyOnWriteArrayList<>();
  
  private final Object mRoutesLock = new Object();
  
  private final Map<String, MediaRoute2Info> mRoutes = new HashMap<>();
  
  final ConcurrentMap<String, List<String>> mPreferredFeaturesMap = new ConcurrentHashMap<>();
  
  private final AtomicInteger mNextRequestId = new AtomicInteger(1);
  
  private final CopyOnWriteArrayList<TransferRequest> mTransferRequests = new CopyOnWriteArrayList<>();
  
  public static final int REQUEST_ID_NONE = 0;
  
  private static final String TAG = "MR2Manager";
  
  public static final int TRANSFER_TIMEOUT_MS = 30000;
  
  private static MediaRouter2Manager sInstance;
  
  private Client mClient;
  
  private final Context mContext;
  
  final Handler mHandler;
  
  private final IMediaRouterService mMediaRouterService;
  
  private final MediaSessionManager mMediaSessionManager;
  
  final String mPackageName;
  
  public static MediaRouter2Manager getInstance(Context paramContext) {
    Objects.requireNonNull(paramContext, "context must not be null");
    synchronized (sLock) {
      if (sInstance == null) {
        MediaRouter2Manager mediaRouter2Manager = new MediaRouter2Manager();
        this(paramContext);
        sInstance = mediaRouter2Manager;
      } 
      return sInstance;
    } 
  }
  
  private MediaRouter2Manager(Context paramContext) {
    this.mContext = paramContext.getApplicationContext();
    IBinder iBinder = ServiceManager.getService("media_router");
    this.mMediaRouterService = IMediaRouterService.Stub.asInterface(iBinder);
    this.mMediaSessionManager = (MediaSessionManager)paramContext.getSystemService("media_session");
    this.mPackageName = this.mContext.getPackageName();
    Handler handler = new Handler(paramContext.getMainLooper());
    handler.post(new _$$Lambda$MediaRouter2Manager$X76xPaxqgD0WPDz_D1Lxk5kiurY(this));
  }
  
  public void registerCallback(Executor paramExecutor, Callback paramCallback) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramCallback, "callback must not be null");
    CallbackRecord callbackRecord = new CallbackRecord(paramExecutor, paramCallback);
    if (!this.mCallbackRecords.addIfAbsent(callbackRecord)) {
      Log.w("MR2Manager", "Ignoring to register the same callback twice.");
      return;
    } 
  }
  
  public void unregisterCallback(Callback paramCallback) {
    Objects.requireNonNull(paramCallback, "callback must not be null");
    if (!this.mCallbackRecords.remove(new CallbackRecord(null, paramCallback))) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unregisterCallback: Ignore unknown callback. ");
      stringBuilder.append(paramCallback);
      Log.w("MR2Manager", stringBuilder.toString());
      return;
    } 
  }
  
  public MediaController getMediaControllerForRoutingSession(RoutingSessionInfo paramRoutingSessionInfo) {
    for (MediaController mediaController : this.mMediaSessionManager.getActiveSessions(null)) {
      if (areSessionsMatched(mediaController, paramRoutingSessionInfo))
        return mediaController; 
    } 
    return null;
  }
  
  public List<MediaRoute2Info> getAvailableRoutes(String paramString) {
    Objects.requireNonNull(paramString, "packageName must not be null");
    List<RoutingSessionInfo> list = getRoutingSessions(paramString);
    return getAvailableRoutesForRoutingSession(list.get(list.size() - 1));
  }
  
  public List<MediaRoute2Info> getAvailableRoutesForRoutingSession(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    ArrayList<MediaRoute2Info> arrayList = new ArrayList();
    String str = paramRoutingSessionInfo.getClientPackageName();
    List<?> list2 = this.mPreferredFeaturesMap.get(str);
    List<?> list1 = list2;
    if (list2 == null)
      list1 = Collections.emptyList(); 
    synchronized (this.mRoutesLock) {
      for (MediaRoute2Info mediaRoute2Info : this.mRoutes.values()) {
        if (mediaRoute2Info.hasAnyFeatures((Collection)list1) || 
          paramRoutingSessionInfo.getSelectedRoutes().contains(mediaRoute2Info.getId()) || 
          paramRoutingSessionInfo.getTransferableRoutes().contains(mediaRoute2Info.getId()))
          arrayList.add(mediaRoute2Info); 
      } 
      return arrayList;
    } 
  }
  
  public RoutingSessionInfo getSystemRoutingSession() {
    for (RoutingSessionInfo routingSessionInfo : getActiveSessions()) {
      if (routingSessionInfo.isSystemSession())
        return routingSessionInfo; 
    } 
    throw new IllegalStateException("No system routing session");
  }
  
  public RoutingSessionInfo getRoutingSessionForMediaController(MediaController paramMediaController) {
    MediaController.PlaybackInfo playbackInfo = paramMediaController.getPlaybackInfo();
    if (playbackInfo == null)
      return null; 
    if (playbackInfo.getPlaybackType() == 1) {
      RoutingSessionInfo.Builder builder2 = new RoutingSessionInfo.Builder(getSystemRoutingSession());
      RoutingSessionInfo.Builder builder1 = builder2.setClientPackageName(paramMediaController.getPackageName());
      return builder1.build();
    } 
    for (RoutingSessionInfo routingSessionInfo : getActiveSessions()) {
      if (!routingSessionInfo.isSystemSession() && 
        areSessionsMatched(paramMediaController, routingSessionInfo))
        return routingSessionInfo; 
    } 
    return null;
  }
  
  public List<RoutingSessionInfo> getRoutingSessions(String paramString) {
    Objects.requireNonNull(paramString, "packageName must not be null");
    ArrayList<RoutingSessionInfo> arrayList = new ArrayList();
    for (RoutingSessionInfo routingSessionInfo : getActiveSessions()) {
      if (routingSessionInfo.isSystemSession()) {
        RoutingSessionInfo.Builder builder = new RoutingSessionInfo.Builder(routingSessionInfo);
        builder = builder.setClientPackageName(paramString);
        routingSessionInfo = builder.build();
        arrayList.add(routingSessionInfo);
        continue;
      } 
      if (TextUtils.equals(routingSessionInfo.getClientPackageName(), paramString))
        arrayList.add(routingSessionInfo); 
    } 
    return arrayList;
  }
  
  public List<RoutingSessionInfo> getActiveSessions() {
    Client client = getOrCreateClient();
    if (client != null)
      try {
        return this.mMediaRouterService.getActiveSessions(client);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "Unable to get sessions. Service probably died.", (Throwable)remoteException);
      }  
    return Collections.emptyList();
  }
  
  public List<MediaRoute2Info> getAllRoutes() {
    null = new ArrayList();
    synchronized (this.mRoutesLock) {
      null.addAll(this.mRoutes.values());
      return null;
    } 
  }
  
  public void selectRoute(String paramString, MediaRoute2Info paramMediaRoute2Info) {
    Objects.requireNonNull(paramString, "packageName must not be null");
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Selecting route. packageName= ");
    stringBuilder.append(paramString);
    stringBuilder.append(", route=");
    stringBuilder.append(paramMediaRoute2Info);
    Log.v("MR2Manager", stringBuilder.toString());
    List<RoutingSessionInfo> list = getRoutingSessions(paramString);
    RoutingSessionInfo routingSessionInfo = list.get(list.size() - 1);
    transfer(routingSessionInfo, paramMediaRoute2Info);
  }
  
  public void transfer(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Transferring routing session. session= ");
    stringBuilder.append(paramRoutingSessionInfo);
    stringBuilder.append(", route=");
    stringBuilder.append(paramMediaRoute2Info);
    Log.v("MR2Manager", stringBuilder.toString());
    synchronized (this.mRoutesLock) {
      if (!this.mRoutes.containsKey(paramMediaRoute2Info.getId())) {
        StringBuilder stringBuilder1 = new StringBuilder();
        this();
        stringBuilder1.append("transfer: Ignoring an unknown route id=");
        stringBuilder1.append(paramMediaRoute2Info.getId());
        Log.w("MR2Manager", stringBuilder1.toString());
        notifyTransferFailed(paramRoutingSessionInfo, paramMediaRoute2Info);
        return;
      } 
      if (paramRoutingSessionInfo.getTransferableRoutes().contains(paramMediaRoute2Info.getId())) {
        transferToRoute(paramRoutingSessionInfo, paramMediaRoute2Info);
      } else {
        requestCreateSession(paramRoutingSessionInfo, paramMediaRoute2Info);
      } 
      return;
    } 
  }
  
  public void setRouteVolume(MediaRoute2Info paramMediaRoute2Info, int paramInt) {
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    if (paramMediaRoute2Info.getVolumeHandling() == 0) {
      Log.w("MR2Manager", "setRouteVolume: the route has fixed volume. Ignoring.");
      return;
    } 
    if (paramInt < 0 || paramInt > paramMediaRoute2Info.getVolumeMax()) {
      Log.w("MR2Manager", "setRouteVolume: the target volume is out of range. Ignoring");
      return;
    } 
    Client client = getOrCreateClient();
    if (client != null)
      try {
        int i = this.mNextRequestId.getAndIncrement();
        this.mMediaRouterService.setRouteVolumeWithManager(client, i, paramMediaRoute2Info, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "Unable to set route volume.", (Throwable)remoteException);
      }  
  }
  
  public void setSessionVolume(RoutingSessionInfo paramRoutingSessionInfo, int paramInt) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    if (paramRoutingSessionInfo.getVolumeHandling() == 0) {
      Log.w("MR2Manager", "setSessionVolume: the route has fixed volume. Ignoring.");
      return;
    } 
    if (paramInt < 0 || paramInt > paramRoutingSessionInfo.getVolumeMax()) {
      Log.w("MR2Manager", "setSessionVolume: the target volume is out of range. Ignoring");
      return;
    } 
    Client client = getOrCreateClient();
    if (client != null)
      try {
        int i = this.mNextRequestId.getAndIncrement();
        IMediaRouterService iMediaRouterService = this.mMediaRouterService;
        String str = paramRoutingSessionInfo.getId();
        iMediaRouterService.setSessionVolumeWithManager(client, i, str, paramInt);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "Unable to set session volume.", (Throwable)remoteException);
      }  
  }
  
  void addRoutesOnHandler(List<MediaRoute2Info> paramList) {
    synchronized (this.mRoutesLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList)
        this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info); 
      if (paramList.size() > 0)
        notifyRoutesAdded(paramList); 
      return;
    } 
  }
  
  void removeRoutesOnHandler(List<MediaRoute2Info> paramList) {
    synchronized (this.mRoutesLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList)
        this.mRoutes.remove(mediaRoute2Info.getId()); 
      if (paramList.size() > 0)
        notifyRoutesRemoved(paramList); 
      return;
    } 
  }
  
  void changeRoutesOnHandler(List<MediaRoute2Info> paramList) {
    synchronized (this.mRoutesLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList)
        this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info); 
      if (paramList.size() > 0)
        notifyRoutesChanged(paramList); 
      return;
    } 
  }
  
  void createSessionOnHandler(int paramInt, RoutingSessionInfo paramRoutingSessionInfo) {
    String str1;
    TransferRequest transferRequest2, transferRequest1 = null;
    Iterator<TransferRequest> iterator = this.mTransferRequests.iterator();
    while (true) {
      transferRequest2 = transferRequest1;
      if (iterator.hasNext()) {
        transferRequest2 = iterator.next();
        if (transferRequest2.mRequestId == paramInt)
          break; 
        continue;
      } 
      break;
    } 
    if (transferRequest2 == null)
      return; 
    this.mTransferRequests.remove(transferRequest2);
    MediaRoute2Info mediaRoute2Info = transferRequest2.mTargetRoute;
    if (paramRoutingSessionInfo == null) {
      notifyTransferFailed(transferRequest2.mOldSessionInfo, mediaRoute2Info);
      return;
    } 
    if (!paramRoutingSessionInfo.getSelectedRoutes().contains(mediaRoute2Info.getId())) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The session does not contain the requested route. (requestedRouteId=");
      stringBuilder.append(mediaRoute2Info.getId());
      stringBuilder.append(", actualRoutes=");
      stringBuilder.append(paramRoutingSessionInfo.getSelectedRoutes());
      stringBuilder.append(")");
      str1 = stringBuilder.toString();
      Log.w("MR2Manager", str1);
      notifyTransferFailed(transferRequest2.mOldSessionInfo, mediaRoute2Info);
      return;
    } 
    String str3 = mediaRoute2Info.getProviderId();
    String str2 = str1.getProviderId();
    if (!TextUtils.equals(str3, str2)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The session's provider ID does not match the requested route's. (requested route's providerId=");
      stringBuilder.append(mediaRoute2Info.getProviderId());
      stringBuilder.append(", actual providerId=");
      stringBuilder.append(str1.getProviderId());
      stringBuilder.append(")");
      str1 = stringBuilder.toString();
      Log.w("MR2Manager", str1);
      notifyTransferFailed(transferRequest2.mOldSessionInfo, mediaRoute2Info);
      return;
    } 
    notifyTransferred(transferRequest2.mOldSessionInfo, (RoutingSessionInfo)str1);
  }
  
  void handleFailureOnHandler(int paramInt1, int paramInt2) {
    TransferRequest transferRequest2, transferRequest1 = null;
    Iterator<TransferRequest> iterator = this.mTransferRequests.iterator();
    while (true) {
      transferRequest2 = transferRequest1;
      if (iterator.hasNext()) {
        transferRequest2 = iterator.next();
        if (transferRequest2.mRequestId == paramInt1)
          break; 
        continue;
      } 
      break;
    } 
    if (transferRequest2 != null) {
      this.mTransferRequests.remove(transferRequest2);
      notifyTransferFailed(transferRequest2.mOldSessionInfo, transferRequest2.mTargetRoute);
      return;
    } 
    notifyRequestFailed(paramInt2);
  }
  
  void handleSessionsUpdatedOnHandler(RoutingSessionInfo paramRoutingSessionInfo) {
    for (TransferRequest transferRequest : this.mTransferRequests) {
      String str = transferRequest.mOldSessionInfo.getId();
      if (!TextUtils.equals(str, paramRoutingSessionInfo.getId()))
        continue; 
      if (paramRoutingSessionInfo.getSelectedRoutes().contains(transferRequest.mTargetRoute.getId())) {
        this.mTransferRequests.remove(transferRequest);
        notifyTransferred(transferRequest.mOldSessionInfo, paramRoutingSessionInfo);
        break;
      } 
    } 
    notifySessionUpdated(paramRoutingSessionInfo);
  }
  
  private void notifyRoutesAdded(List<MediaRoute2Info> paramList) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$EHLJd_emeyLDkF_AwpiyC8_kDdc(callbackRecord, paramList)); 
  }
  
  private void notifyRoutesRemoved(List<MediaRoute2Info> paramList) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$okgw16i6uRs7ol_81WusfPkAk9U(callbackRecord, paramList)); 
  }
  
  private void notifyRoutesChanged(List<MediaRoute2Info> paramList) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$VaiYDmtXUs_bgZtD2f2UpEjT_20(callbackRecord, paramList)); 
  }
  
  void notifySessionUpdated(RoutingSessionInfo paramRoutingSessionInfo) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$Wc_lT54fkpr3qSlIG7DSi5PeSVM(callbackRecord, paramRoutingSessionInfo)); 
  }
  
  void notifySessionReleased(RoutingSessionInfo paramRoutingSessionInfo) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$KUHGEkWGlaVfd3Zi4oigZVFdRC4(callbackRecord, paramRoutingSessionInfo)); 
  }
  
  void notifyRequestFailed(int paramInt) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$r2vzZsEyRJDQIJgQnYSaKy7jmX4(callbackRecord, paramInt)); 
  }
  
  void notifyTransferred(RoutingSessionInfo paramRoutingSessionInfo1, RoutingSessionInfo paramRoutingSessionInfo2) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$woOi9vOXaxYUsZXZ_elbMeI3NFw(callbackRecord, paramRoutingSessionInfo1, paramRoutingSessionInfo2)); 
  }
  
  void notifyTransferFailed(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$U66215BPr7th85YZ9Hpd5OVMOgU(callbackRecord, paramRoutingSessionInfo, paramMediaRoute2Info)); 
  }
  
  void updatePreferredFeatures(String paramString, List<String> paramList) {
    if (paramList == null) {
      this.mPreferredFeaturesMap.remove(paramString);
      return;
    } 
    List list = this.mPreferredFeaturesMap.put(paramString, paramList);
    if ((list == null && paramList.size() == 0) || 
      Objects.equals(paramList, list))
      return; 
    for (CallbackRecord callbackRecord : this.mCallbackRecords)
      callbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2Manager$bwWxXKc_6BGO9bmWeLY4XfIX2lw(callbackRecord, paramString, paramList)); 
  }
  
  public List<MediaRoute2Info> getSelectedRoutes(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    synchronized (this.mRoutesLock) {
      Stream<String> stream = paramRoutingSessionInfo.getSelectedRoutes().stream();
      Map<String, MediaRoute2Info> map = this.mRoutes;
      Objects.requireNonNull(map);
      _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA = new _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA();
      this(map);
      Stream<?> stream2 = stream.map(_$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA);
      -$.Lambda.fo3R-Przkq5mg2wxR3lAN3cgNY fo3R-Przkq5mg2wxR3lAN3cgNY = _$$Lambda$8fo3R_Przkq5mg2wxR3lAN3cgNY.INSTANCE;
      Stream<?> stream1 = stream2.filter((Predicate<?>)fo3R-Przkq5mg2wxR3lAN3cgNY);
      return stream1.collect((Collector)Collectors.toList());
    } 
  }
  
  public List<MediaRoute2Info> getSelectableRoutes(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    List<String> list = paramRoutingSessionInfo.getSelectedRoutes();
    synchronized (this.mRoutesLock) {
      Stream<String> stream4 = paramRoutingSessionInfo.getSelectableRoutes().stream();
      _$$Lambda$MediaRouter2Manager$EwA0DuTJgJEWQVNlePEqNDMh4_s _$$Lambda$MediaRouter2Manager$EwA0DuTJgJEWQVNlePEqNDMh4_s = new _$$Lambda$MediaRouter2Manager$EwA0DuTJgJEWQVNlePEqNDMh4_s();
      this(list);
      Stream<String> stream2 = stream4.filter(_$$Lambda$MediaRouter2Manager$EwA0DuTJgJEWQVNlePEqNDMh4_s);
      Map<String, MediaRoute2Info> map = this.mRoutes;
      Objects.requireNonNull(map);
      _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA = new _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA();
      this(map);
      Stream<?> stream3 = stream2.map(_$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA);
      -$.Lambda.fo3R-Przkq5mg2wxR3lAN3cgNY fo3R-Przkq5mg2wxR3lAN3cgNY = _$$Lambda$8fo3R_Przkq5mg2wxR3lAN3cgNY.INSTANCE;
      Stream<?> stream1 = stream3.filter((Predicate<?>)fo3R-Przkq5mg2wxR3lAN3cgNY);
      return stream1.collect((Collector)Collectors.toList());
    } 
  }
  
  public List<MediaRoute2Info> getDeselectableRoutes(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    List<String> list = paramRoutingSessionInfo.getSelectedRoutes();
    synchronized (this.mRoutesLock) {
      Stream<String> stream4 = paramRoutingSessionInfo.getDeselectableRoutes().stream();
      _$$Lambda$MediaRouter2Manager$kLakEUmZ1WmAktt2ie1ETgzRYRw _$$Lambda$MediaRouter2Manager$kLakEUmZ1WmAktt2ie1ETgzRYRw = new _$$Lambda$MediaRouter2Manager$kLakEUmZ1WmAktt2ie1ETgzRYRw();
      this(list);
      Stream<String> stream2 = stream4.filter(_$$Lambda$MediaRouter2Manager$kLakEUmZ1WmAktt2ie1ETgzRYRw);
      Map<String, MediaRoute2Info> map = this.mRoutes;
      Objects.requireNonNull(map);
      _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA = new _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA();
      this(map);
      Stream<?> stream3 = stream2.map(_$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA);
      -$.Lambda.fo3R-Przkq5mg2wxR3lAN3cgNY fo3R-Przkq5mg2wxR3lAN3cgNY = _$$Lambda$8fo3R_Przkq5mg2wxR3lAN3cgNY.INSTANCE;
      Stream<?> stream1 = stream3.filter((Predicate<?>)fo3R-Przkq5mg2wxR3lAN3cgNY);
      return stream1.collect((Collector)Collectors.toList());
    } 
  }
  
  public void selectRoute(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    StringBuilder stringBuilder;
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    if (paramRoutingSessionInfo.getSelectedRoutes().contains(paramMediaRoute2Info.getId())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring selecting a route that is already selected. route=");
      stringBuilder.append(paramMediaRoute2Info);
      Log.w("MR2Manager", stringBuilder.toString());
      return;
    } 
    if (!stringBuilder.getSelectableRoutes().contains(paramMediaRoute2Info.getId())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring selecting a non-selectable route=");
      stringBuilder.append(paramMediaRoute2Info);
      Log.w("MR2Manager", stringBuilder.toString());
      return;
    } 
    Client client = getOrCreateClient();
    if (client != null)
      try {
        int i = this.mNextRequestId.getAndIncrement();
        IMediaRouterService iMediaRouterService = this.mMediaRouterService;
        String str = stringBuilder.getId();
        iMediaRouterService.selectRouteWithManager(client, i, str, paramMediaRoute2Info);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "selectRoute: Failed to send a request.", (Throwable)remoteException);
      }  
  }
  
  public void deselectRoute(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    StringBuilder stringBuilder;
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    if (!paramRoutingSessionInfo.getSelectedRoutes().contains(paramMediaRoute2Info.getId())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring deselecting a route that is not selected. route=");
      stringBuilder.append(paramMediaRoute2Info);
      Log.w("MR2Manager", stringBuilder.toString());
      return;
    } 
    if (!stringBuilder.getDeselectableRoutes().contains(paramMediaRoute2Info.getId())) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring deselecting a non-deselectable route=");
      stringBuilder.append(paramMediaRoute2Info);
      Log.w("MR2Manager", stringBuilder.toString());
      return;
    } 
    Client client = getOrCreateClient();
    if (client != null)
      try {
        int i = this.mNextRequestId.getAndIncrement();
        IMediaRouterService iMediaRouterService = this.mMediaRouterService;
        String str = stringBuilder.getId();
        iMediaRouterService.deselectRouteWithManager(client, i, str, paramMediaRoute2Info);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "deselectRoute: Failed to send a request.", (Throwable)remoteException);
      }  
  }
  
  public void releaseSession(RoutingSessionInfo paramRoutingSessionInfo) {
    Objects.requireNonNull(paramRoutingSessionInfo, "sessionInfo must not be null");
    Client client = getOrCreateClient();
    if (client != null)
      try {
        int i = this.mNextRequestId.getAndIncrement();
        IMediaRouterService iMediaRouterService = this.mMediaRouterService;
        String str = paramRoutingSessionInfo.getId();
        iMediaRouterService.releaseSessionWithManager(client, i, str);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "releaseSession: Failed to send a request", (Throwable)remoteException);
      }  
  }
  
  private void transferToRoute(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    int i = createTransferRequest(paramRoutingSessionInfo, paramMediaRoute2Info);
    Client client = getOrCreateClient();
    if (client != null)
      try {
        IMediaRouterService iMediaRouterService = this.mMediaRouterService;
        String str = paramRoutingSessionInfo.getId();
        iMediaRouterService.transferToRouteWithManager(client, i, str, paramMediaRoute2Info);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "transferToRoute: Failed to send a request.", (Throwable)remoteException);
      }  
  }
  
  private void requestCreateSession(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    if (TextUtils.isEmpty(paramRoutingSessionInfo.getClientPackageName())) {
      Log.w("MR2Manager", "requestCreateSession: Can't create a session without package name.");
      notifyTransferFailed(paramRoutingSessionInfo, paramMediaRoute2Info);
      return;
    } 
    int i = createTransferRequest(paramRoutingSessionInfo, paramMediaRoute2Info);
    Client client = getOrCreateClient();
    if (client != null)
      try {
        this.mMediaRouterService.requestCreateSessionWithManager(client, i, paramRoutingSessionInfo, paramMediaRoute2Info);
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "requestCreateSession: Failed to send a request", (Throwable)remoteException);
      }  
  }
  
  private int createTransferRequest(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) {
    int i = this.mNextRequestId.getAndIncrement();
    TransferRequest transferRequest = new TransferRequest(i, paramRoutingSessionInfo, paramMediaRoute2Info);
    this.mTransferRequests.add(transferRequest);
    -$.Lambda.MediaRouter2Manager.A_6LAn0k2ifEMlid1ntuc9gWf74 a_6LAn0k2ifEMlid1ntuc9gWf74 = _$$Lambda$MediaRouter2Manager$A_6LAn0k2ifEMlid1ntuc9gWf74.INSTANCE;
    Message message = PooledLambda.obtainMessage((BiConsumer)a_6LAn0k2ifEMlid1ntuc9gWf74, this, transferRequest);
    this.mHandler.sendMessageDelayed(message, 30000L);
    return i;
  }
  
  private void handleTransferTimeout(TransferRequest paramTransferRequest) {
    boolean bool = this.mTransferRequests.remove(paramTransferRequest);
    if (bool)
      notifyTransferFailed(paramTransferRequest.mOldSessionInfo, paramTransferRequest.mTargetRoute); 
  }
  
  private boolean areSessionsMatched(MediaController paramMediaController, RoutingSessionInfo paramRoutingSessionInfo) {
    MediaController.PlaybackInfo playbackInfo = paramMediaController.getPlaybackInfo();
    boolean bool = false;
    if (playbackInfo == null)
      return false; 
    String str = playbackInfo.getVolumeControlId();
    if (str == null)
      return false; 
    if (TextUtils.equals(str, paramRoutingSessionInfo.getId()))
      return true; 
    if (TextUtils.equals(str, paramRoutingSessionInfo.getOriginalId())) {
      String str1 = paramMediaController.getPackageName();
      String str2 = paramRoutingSessionInfo.getOwnerPackageName();
      if (TextUtils.equals(str1, str2))
        bool = true; 
    } 
    return bool;
  }
  
  private Client getOrCreateClient() {
    synchronized (sLock) {
      if (this.mClient != null)
        return this.mClient; 
      Client client = new Client();
      this(this);
      try {
        this.mMediaRouterService.registerManager(client, this.mPackageName);
        this.mClient = client;
        return client;
      } catch (RemoteException remoteException) {
        Log.e("MR2Manager", "Unable to register media router manager.", (Throwable)remoteException);
        return null;
      } 
    } 
  }
  
  public static class Callback {
    public void onRoutesAdded(List<MediaRoute2Info> param1List) {}
    
    public void onRoutesRemoved(List<MediaRoute2Info> param1List) {}
    
    public void onRoutesChanged(List<MediaRoute2Info> param1List) {}
    
    public void onSessionUpdated(RoutingSessionInfo param1RoutingSessionInfo) {}
    
    public void onSessionReleased(RoutingSessionInfo param1RoutingSessionInfo) {}
    
    public void onTransferred(RoutingSessionInfo param1RoutingSessionInfo1, RoutingSessionInfo param1RoutingSessionInfo2) {}
    
    public void onTransferFailed(RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info) {}
    
    public void onPreferredFeaturesChanged(String param1String, List<String> param1List) {}
    
    public void onRequestFailed(int param1Int) {}
  }
  
  final class CallbackRecord {
    public final MediaRouter2Manager.Callback mCallback;
    
    public final Executor mExecutor;
    
    final MediaRouter2Manager this$0;
    
    CallbackRecord(Executor param1Executor, MediaRouter2Manager.Callback param1Callback) {
      this.mExecutor = param1Executor;
      this.mCallback = param1Callback;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof CallbackRecord))
        return false; 
      if (this.mCallback != ((CallbackRecord)param1Object).mCallback)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mCallback.hashCode();
    }
  }
  
  static final class TransferRequest {
    public final RoutingSessionInfo mOldSessionInfo;
    
    public final int mRequestId;
    
    public final MediaRoute2Info mTargetRoute;
    
    TransferRequest(int param1Int, RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info) {
      this.mRequestId = param1Int;
      this.mOldSessionInfo = param1RoutingSessionInfo;
      this.mTargetRoute = param1MediaRoute2Info;
    }
  }
  
  class Client extends IMediaRouter2Manager.Stub {
    final MediaRouter2Manager this$0;
    
    public void notifySessionCreated(int param1Int, RoutingSessionInfo param1RoutingSessionInfo) {
      Handler handler = MediaRouter2Manager.this.mHandler;
      -$.Lambda.PToJpxlbOimKtSkpvLwcwYz6Bio pToJpxlbOimKtSkpvLwcwYz6Bio = _$$Lambda$PToJpxlbOimKtSkpvLwcwYz6Bio.INSTANCE;
      MediaRouter2Manager mediaRouter2Manager = MediaRouter2Manager.this;
      handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)pToJpxlbOimKtSkpvLwcwYz6Bio, mediaRouter2Manager, Integer.valueOf(param1Int), param1RoutingSessionInfo));
    }
    
    public void notifySessionUpdated(RoutingSessionInfo param1RoutingSessionInfo) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$wPcK_ftqvhyA5Kgnc6jiR01yNiY.INSTANCE, MediaRouter2Manager.this, param1RoutingSessionInfo));
    }
    
    public void notifySessionReleased(RoutingSessionInfo param1RoutingSessionInfo) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$Z_tCdSCLbZ5gJYJTtsJCc8wAzw4.INSTANCE, MediaRouter2Manager.this, param1RoutingSessionInfo));
    }
    
    public void notifyRequestFailed(int param1Int1, int param1Int2) {
      Handler handler = MediaRouter2Manager.this.mHandler;
      -$.Lambda.t94cM9ipj3vOV_Gh5MlP8wygpjU t94cM9ipj3vOV_Gh5MlP8wygpjU = _$$Lambda$t94cM9ipj3vOV_Gh5MlP8wygpjU.INSTANCE;
      MediaRouter2Manager mediaRouter2Manager = MediaRouter2Manager.this;
      handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)t94cM9ipj3vOV_Gh5MlP8wygpjU, mediaRouter2Manager, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2)));
    }
    
    public void notifyPreferredFeaturesChanged(String param1String, List<String> param1List) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$uUtNQdlApH9llAedmk42jVBM_FE.INSTANCE, MediaRouter2Manager.this, param1String, param1List));
    }
    
    public void notifyRoutesAdded(List<MediaRoute2Info> param1List) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$gt7p_JyIeiypjCr_OUbbXtG3M2E.INSTANCE, MediaRouter2Manager.this, param1List));
    }
    
    public void notifyRoutesRemoved(List<MediaRoute2Info> param1List) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$_MF8vjrGAjipPRcLld_WGC6Gj3M.INSTANCE, MediaRouter2Manager.this, param1List));
    }
    
    public void notifyRoutesChanged(List<MediaRoute2Info> param1List) {
      MediaRouter2Manager.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$S7cvg7fUGYEd7NUzH3t58CJRbbE.INSTANCE, MediaRouter2Manager.this, param1List));
    }
  }
}
