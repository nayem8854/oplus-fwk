package android.media;

import android.annotation.SystemApi;

@SystemApi
public class PlayerProxy {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "PlayerProxy";
  
  private final AudioPlaybackConfiguration mConf;
  
  PlayerProxy(AudioPlaybackConfiguration paramAudioPlaybackConfiguration) {
    if (paramAudioPlaybackConfiguration != null) {
      this.mConf = paramAudioPlaybackConfiguration;
      return;
    } 
    throw new IllegalArgumentException("Illegal null AudioPlaybackConfiguration");
  }
  
  @SystemApi
  public void start() {
    try {
      this.mConf.getIPlayer().start();
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for start operation, player already released?", nullPointerException);
    } 
  }
  
  @SystemApi
  public void pause() {
    try {
      this.mConf.getIPlayer().pause();
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for pause operation, player already released?", nullPointerException);
    } 
  }
  
  @SystemApi
  public void stop() {
    try {
      this.mConf.getIPlayer().stop();
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for stop operation, player already released?", nullPointerException);
    } 
  }
  
  @SystemApi
  public void setVolume(float paramFloat) {
    try {
      this.mConf.getIPlayer().setVolume(paramFloat);
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for setVolume operation, player already released?", nullPointerException);
    } 
  }
  
  @SystemApi
  public void setPan(float paramFloat) {
    try {
      this.mConf.getIPlayer().setPan(paramFloat);
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for setPan operation, player already released?", nullPointerException);
    } 
  }
  
  @SystemApi
  public void setStartDelayMs(int paramInt) {
    try {
      this.mConf.getIPlayer().setStartDelayMs(paramInt);
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for setStartDelayMs operation, player already released?", nullPointerException);
    } 
  }
  
  public void applyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) {
    try {
      this.mConf.getIPlayer().applyVolumeShaper(paramConfiguration, paramOperation);
      return;
    } catch (NullPointerException|android.os.RemoteException nullPointerException) {
      throw new IllegalStateException("No player to proxy for applyVolumeShaper operation, player already released?", nullPointerException);
    } 
  }
}
