package android.media;

import android.content.Context;

public class WebVttRenderer extends SubtitleController.Renderer {
  private final Context mContext;
  
  private WebVttRenderingWidget mRenderingWidget;
  
  public WebVttRenderer(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public boolean supports(MediaFormat paramMediaFormat) {
    if (paramMediaFormat.containsKey("mime"))
      return paramMediaFormat.getString("mime").equals("text/vtt"); 
    return false;
  }
  
  public SubtitleTrack createTrack(MediaFormat paramMediaFormat) {
    if (this.mRenderingWidget == null)
      this.mRenderingWidget = new WebVttRenderingWidget(this.mContext); 
    return new WebVttTrack(this.mRenderingWidget, paramMediaFormat);
  }
}
