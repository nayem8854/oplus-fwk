package android.media;

import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ImageDecoder;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Environment;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.util.Log;
import android.util.Size;
import com.android.internal.util.ArrayUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.function.ToIntFunction;
import libcore.io.IoUtils;

public class ThumbnailUtils {
  private static final int OPTIONS_NONE = 0;
  
  public static final int OPTIONS_RECYCLE_INPUT = 2;
  
  private static final int OPTIONS_SCALE_UP = 1;
  
  private static final String TAG = "ThumbnailUtils";
  
  @Deprecated
  public static final int TARGET_SIZE_MICRO_THUMBNAIL = 96;
  
  private static Size convertKind(int paramInt) {
    return MediaStore.Images.Thumbnails.getKindSize(paramInt);
  }
  
  class Resizer implements ImageDecoder.OnHeaderDecodedListener {
    private final CancellationSignal signal;
    
    private final Size size;
    
    public Resizer(ThumbnailUtils this$0, CancellationSignal param1CancellationSignal) {
      this.size = (Size)this$0;
      this.signal = param1CancellationSignal;
    }
    
    public void onHeaderDecoded(ImageDecoder param1ImageDecoder, ImageDecoder.ImageInfo param1ImageInfo, ImageDecoder.Source param1Source) {
      CancellationSignal cancellationSignal = this.signal;
      if (cancellationSignal != null)
        cancellationSignal.throwIfCanceled(); 
      param1ImageDecoder.setAllocator(1);
      int i = param1ImageInfo.getSize().getWidth() / this.size.getWidth();
      int j = param1ImageInfo.getSize().getHeight() / this.size.getHeight();
      j = Math.max(i, j);
      if (j > 1)
        param1ImageDecoder.setTargetSampleSize(j); 
    }
  }
  
  @Deprecated
  public static Bitmap createAudioThumbnail(String paramString, int paramInt) {
    try {
      File file = new File();
      this(paramString);
      return createAudioThumbnail(file, convertKind(paramInt), null);
    } catch (IOException iOException) {
      Log.w("ThumbnailUtils", iOException);
      return null;
    } 
  }
  
  public static Bitmap createAudioThumbnail(File paramFile, Size paramSize, CancellationSignal paramCancellationSignal) throws IOException {
    if (paramCancellationSignal != null)
      paramCancellationSignal.throwIfCanceled(); 
    Resizer resizer = new Resizer(paramSize, paramCancellationSignal);
    try {
      _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk;
      MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
      this();
      try {
        Bitmap bitmap;
        mediaMetadataRetriever.setDataSource(paramFile.getAbsolutePath());
        byte[] arrayOfByte = mediaMetadataRetriever.getEmbeddedPicture();
        if (arrayOfByte != null) {
          bitmap = ImageDecoder.decodeBitmap(ImageDecoder.createSource(arrayOfByte), resizer);
          return bitmap;
        } 
        mediaMetadataRetriever.close();
        if (!"unknown".equals(Environment.getExternalStorageState((File)bitmap))) {
          File file = bitmap.getParentFile();
          if (file != null) {
            File file1 = file.getParentFile();
          } else {
            mediaMetadataRetriever = null;
          } 
          if (file == null || 
            !file.getName().equals(Environment.DIRECTORY_DOWNLOADS)) {
            if (mediaMetadataRetriever == null || 
              !"unknown".equals(Environment.getExternalStorageState((File)mediaMetadataRetriever))) {
              File[] arrayOfFile = ArrayUtils.defeatNullable(bitmap.getParentFile().listFiles((FilenameFilter)_$$Lambda$ThumbnailUtils$P13h9YbyD69p6ss1gYpoef43_MU.INSTANCE));
              -$.Lambda.ThumbnailUtils.qOH5vebuTwPi2G92PTa6rgwKGoc qOH5vebuTwPi2G92PTa6rgwKGoc = _$$Lambda$ThumbnailUtils$qOH5vebuTwPi2G92PTa6rgwKGoc.INSTANCE;
              _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk = new _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk((ToIntFunction)qOH5vebuTwPi2G92PTa6rgwKGoc);
              File file1 = Arrays.<File>asList(arrayOfFile).stream().max(_$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk).orElse(null);
              if (file1 != null)
                return ImageDecoder.decodeBitmap(ImageDecoder.createSource(file1), resizer); 
              throw new IOException("No album art found");
            } 
            throw new IOException("No thumbnails in top-level directories");
          } 
          throw new IOException("No thumbnails in Downloads directories");
        } 
        throw new IOException("No embedded album art found");
      } finally {
        try {
          _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk.close();
        } finally {
          _$$Lambda$ThumbnailUtils$HhGKNQZck57eO__Paj6KyQm6lCk = null;
        } 
      } 
    } catch (RuntimeException runtimeException) {
      throw new IOException("Failed to create thumbnail", runtimeException);
    } 
  }
  
  @Deprecated
  public static Bitmap createImageThumbnail(String paramString, int paramInt) {
    try {
      File file = new File();
      this(paramString);
      return createImageThumbnail(file, convertKind(paramInt), null);
    } catch (IOException iOException) {
      Log.w("ThumbnailUtils", iOException);
      return null;
    } 
  }
  
  public static Bitmap createImageThumbnail(File paramFile, Size paramSize, CancellationSignal paramCancellationSignal) throws IOException {
    // Byte code:
    //   0: aload_2
    //   1: ifnull -> 8
    //   4: aload_2
    //   5: invokevirtual throwIfCanceled : ()V
    //   8: new android/media/ThumbnailUtils$Resizer
    //   11: dup
    //   12: aload_1
    //   13: aload_2
    //   14: invokespecial <init> : (Landroid/util/Size;Landroid/os/CancellationSignal;)V
    //   17: astore_3
    //   18: aload_0
    //   19: invokevirtual getName : ()Ljava/lang/String;
    //   22: invokestatic getMimeTypeForFile : (Ljava/lang/String;)Ljava/lang/String;
    //   25: astore #4
    //   27: aconst_null
    //   28: astore #5
    //   30: aload #4
    //   32: invokestatic isExifMimeType : (Ljava/lang/String;)Z
    //   35: ifeq -> 107
    //   38: new android/media/ExifInterface
    //   41: dup
    //   42: aload_0
    //   43: invokespecial <init> : (Ljava/io/File;)V
    //   46: astore #6
    //   48: aload #6
    //   50: ldc 'Orientation'
    //   52: iconst_0
    //   53: invokevirtual getAttributeInt : (Ljava/lang/String;I)I
    //   56: istore #7
    //   58: iload #7
    //   60: iconst_3
    //   61: if_icmpeq -> 99
    //   64: iload #7
    //   66: bipush #6
    //   68: if_icmpeq -> 92
    //   71: iload #7
    //   73: bipush #8
    //   75: if_icmpeq -> 84
    //   78: iconst_0
    //   79: istore #7
    //   81: goto -> 113
    //   84: sipush #270
    //   87: istore #7
    //   89: goto -> 113
    //   92: bipush #90
    //   94: istore #7
    //   96: goto -> 113
    //   99: sipush #180
    //   102: istore #7
    //   104: goto -> 113
    //   107: iconst_0
    //   108: istore #7
    //   110: aconst_null
    //   111: astore #6
    //   113: aload #4
    //   115: ldc 'image/heif'
    //   117: invokevirtual equals : (Ljava/lang/Object;)Z
    //   120: ifne -> 153
    //   123: aload #4
    //   125: ldc 'image/heif-sequence'
    //   127: invokevirtual equals : (Ljava/lang/Object;)Z
    //   130: ifne -> 153
    //   133: aload #4
    //   135: ldc 'image/heic'
    //   137: invokevirtual equals : (Ljava/lang/Object;)Z
    //   140: ifne -> 153
    //   143: aload #4
    //   145: ldc 'image/heic-sequence'
    //   147: invokevirtual equals : (Ljava/lang/Object;)Z
    //   150: ifeq -> 222
    //   153: new android/media/MediaMetadataRetriever
    //   156: astore #4
    //   158: aload #4
    //   160: invokespecial <init> : ()V
    //   163: aload #4
    //   165: aload_0
    //   166: invokevirtual getAbsolutePath : ()Ljava/lang/String;
    //   169: invokevirtual setDataSource : (Ljava/lang/String;)V
    //   172: new android/media/MediaMetadataRetriever$BitmapParams
    //   175: astore #5
    //   177: aload #5
    //   179: invokespecial <init> : ()V
    //   182: aload_1
    //   183: invokevirtual getWidth : ()I
    //   186: istore #8
    //   188: aload_1
    //   189: invokevirtual getWidth : ()I
    //   192: istore #9
    //   194: aload_1
    //   195: invokevirtual getHeight : ()I
    //   198: istore #10
    //   200: aload #4
    //   202: iconst_m1
    //   203: aload #5
    //   205: iload #8
    //   207: iload #9
    //   209: iload #10
    //   211: imul
    //   212: invokevirtual getThumbnailImageAtIndex : (ILandroid/media/MediaMetadataRetriever$BitmapParams;II)Landroid/graphics/Bitmap;
    //   215: astore #5
    //   217: aload #4
    //   219: invokevirtual close : ()V
    //   222: aload #5
    //   224: astore_1
    //   225: aload #5
    //   227: ifnonnull -> 277
    //   230: aload #5
    //   232: astore_1
    //   233: aload #6
    //   235: ifnull -> 277
    //   238: aload #6
    //   240: invokevirtual getThumbnailBytes : ()[B
    //   243: astore #6
    //   245: aload #5
    //   247: astore_1
    //   248: aload #6
    //   250: ifnull -> 277
    //   253: aload #6
    //   255: invokestatic createSource : ([B)Landroid/graphics/ImageDecoder$Source;
    //   258: aload_3
    //   259: invokestatic decodeBitmap : (Landroid/graphics/ImageDecoder$Source;Landroid/graphics/ImageDecoder$OnHeaderDecodedListener;)Landroid/graphics/Bitmap;
    //   262: astore_1
    //   263: goto -> 277
    //   266: astore_1
    //   267: ldc 'ThumbnailUtils'
    //   269: aload_1
    //   270: invokestatic w : (Ljava/lang/String;Ljava/lang/Throwable;)I
    //   273: pop
    //   274: aload #5
    //   276: astore_1
    //   277: aload_2
    //   278: ifnull -> 285
    //   281: aload_2
    //   282: invokevirtual throwIfCanceled : ()V
    //   285: aload_1
    //   286: ifnonnull -> 300
    //   289: aload_0
    //   290: invokestatic createSource : (Ljava/io/File;)Landroid/graphics/ImageDecoder$Source;
    //   293: aload_3
    //   294: invokestatic decodeBitmap : (Landroid/graphics/ImageDecoder$Source;Landroid/graphics/ImageDecoder$OnHeaderDecodedListener;)Landroid/graphics/Bitmap;
    //   297: astore_0
    //   298: aload_0
    //   299: areturn
    //   300: aload_1
    //   301: astore_0
    //   302: iload #7
    //   304: ifeq -> 363
    //   307: aload_1
    //   308: astore_0
    //   309: aload_1
    //   310: ifnull -> 363
    //   313: aload_1
    //   314: invokevirtual getWidth : ()I
    //   317: istore #10
    //   319: aload_1
    //   320: invokevirtual getHeight : ()I
    //   323: istore #9
    //   325: new android/graphics/Matrix
    //   328: dup
    //   329: invokespecial <init> : ()V
    //   332: astore_0
    //   333: aload_0
    //   334: iload #7
    //   336: i2f
    //   337: iload #10
    //   339: iconst_2
    //   340: idiv
    //   341: i2f
    //   342: iload #9
    //   344: iconst_2
    //   345: idiv
    //   346: i2f
    //   347: invokevirtual setRotate : (FFF)V
    //   350: aload_1
    //   351: iconst_0
    //   352: iconst_0
    //   353: iload #10
    //   355: iload #9
    //   357: aload_0
    //   358: iconst_0
    //   359: invokestatic createBitmap : (Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    //   362: astore_0
    //   363: aload_0
    //   364: areturn
    //   365: astore_0
    //   366: aload #4
    //   368: invokevirtual close : ()V
    //   371: goto -> 380
    //   374: astore_1
    //   375: aload_0
    //   376: aload_1
    //   377: invokevirtual addSuppressed : (Ljava/lang/Throwable;)V
    //   380: aload_0
    //   381: athrow
    //   382: astore_0
    //   383: new java/io/IOException
    //   386: dup
    //   387: ldc 'Failed to create thumbnail'
    //   389: aload_0
    //   390: invokespecial <init> : (Ljava/lang/String;Ljava/lang/Throwable;)V
    //   393: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #248	-> 0
    //   #250	-> 8
    //   #251	-> 18
    //   #252	-> 27
    //   #253	-> 30
    //   #254	-> 30
    //   #257	-> 30
    //   #258	-> 38
    //   #259	-> 48
    //   #267	-> 84
    //   #261	-> 92
    //   #262	-> 92
    //   #264	-> 99
    //   #265	-> 99
    //   #257	-> 107
    //   #272	-> 113
    //   #273	-> 123
    //   #274	-> 133
    //   #275	-> 143
    //   #276	-> 153
    //   #277	-> 163
    //   #278	-> 172
    //   #279	-> 182
    //   #280	-> 188
    //   #278	-> 200
    //   #281	-> 217
    //   #283	-> 222
    //   #286	-> 222
    //   #287	-> 238
    //   #288	-> 245
    //   #290	-> 253
    //   #293	-> 263
    //   #291	-> 266
    //   #292	-> 267
    //   #298	-> 277
    //   #300	-> 285
    //   #301	-> 289
    //   #303	-> 298
    //   #307	-> 300
    //   #308	-> 313
    //   #309	-> 319
    //   #311	-> 325
    //   #312	-> 333
    //   #313	-> 350
    //   #316	-> 363
    //   #276	-> 365
    //   #281	-> 382
    //   #282	-> 383
    // Exception table:
    //   from	to	target	type
    //   153	163	382	java/lang/RuntimeException
    //   163	172	365	finally
    //   172	182	365	finally
    //   182	188	365	finally
    //   188	200	365	finally
    //   200	217	365	finally
    //   217	222	382	java/lang/RuntimeException
    //   253	263	266	android/graphics/ImageDecoder$DecodeException
    //   366	371	374	finally
    //   375	380	382	java/lang/RuntimeException
    //   380	382	382	java/lang/RuntimeException
  }
  
  @Deprecated
  public static Bitmap createVideoThumbnail(String paramString, int paramInt) {
    try {
      File file = new File();
      this(paramString);
      return createVideoThumbnail(file, convertKind(paramInt), null);
    } catch (IOException iOException) {
      Log.w("ThumbnailUtils", iOException);
      return null;
    } 
  }
  
  public static Bitmap createVideoThumbnail(File paramFile, Size paramSize, CancellationSignal paramCancellationSignal) throws IOException {
    if (paramCancellationSignal != null)
      paramCancellationSignal.throwIfCanceled(); 
    Resizer resizer = new Resizer(paramSize, paramCancellationSignal);
    try {
      MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
      this();
      try {
        mediaMetadataRetriever.setDataSource(paramFile.getAbsolutePath());
        byte[] arrayOfByte = mediaMetadataRetriever.getEmbeddedPicture();
        if (arrayOfByte != null) {
          bitmap = ImageDecoder.decodeBitmap(ImageDecoder.createSource(arrayOfByte), resizer);
          return bitmap;
        } 
        MediaMetadataRetriever.BitmapParams bitmapParams = new MediaMetadataRetriever.BitmapParams();
        this();
        bitmapParams.setPreferredConfig(Bitmap.Config.ARGB_8888);
        int i = Integer.parseInt(mediaMetadataRetriever.extractMetadata(18));
        int j = Integer.parseInt(mediaMetadataRetriever.extractMetadata(19));
        long l = Long.parseLong(mediaMetadataRetriever.extractMetadata(9)) * 1000L / 2L;
        if (paramSize.getWidth() > i && paramSize.getHeight() > j) {
          bitmap = mediaMetadataRetriever.getFrameAtTime(l, 2, bitmapParams);
          Objects.requireNonNull(bitmap);
          bitmap = bitmap;
          return bitmap;
        } 
        j = paramSize.getWidth();
        i = paramSize.getHeight();
        Bitmap bitmap = mediaMetadataRetriever.getScaledFrameAtTime(l, 2, j, i, (MediaMetadataRetriever.BitmapParams)bitmap);
        Objects.requireNonNull(bitmap);
        bitmap = bitmap;
        return bitmap;
      } finally {
        try {
          mediaMetadataRetriever.close();
        } finally {
          paramSize = null;
        } 
      } 
    } catch (RuntimeException runtimeException) {
      throw new IOException("Failed to create thumbnail", runtimeException);
    } 
  }
  
  public static Bitmap extractThumbnail(Bitmap paramBitmap, int paramInt1, int paramInt2) {
    return extractThumbnail(paramBitmap, paramInt1, paramInt2, 0);
  }
  
  public static Bitmap extractThumbnail(Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3) {
    float f;
    if (paramBitmap == null)
      return null; 
    if (paramBitmap.getWidth() < paramBitmap.getHeight()) {
      f = paramInt1 / paramBitmap.getWidth();
    } else {
      f = paramInt2 / paramBitmap.getHeight();
    } 
    Matrix matrix = new Matrix();
    matrix.setScale(f, f);
    paramBitmap = transform(matrix, paramBitmap, paramInt1, paramInt2, paramInt3 | 0x1);
    return paramBitmap;
  }
  
  @Deprecated
  private static int computeSampleSize(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2) {
    return 1;
  }
  
  @Deprecated
  private static int computeInitialSampleSize(BitmapFactory.Options paramOptions, int paramInt1, int paramInt2) {
    return 1;
  }
  
  @Deprecated
  private static void closeSilently(ParcelFileDescriptor paramParcelFileDescriptor) {
    IoUtils.closeQuietly(paramParcelFileDescriptor);
  }
  
  @Deprecated
  private static ParcelFileDescriptor makeInputStream(Uri paramUri, ContentResolver paramContentResolver) {
    try {
      return paramContentResolver.openFileDescriptor(paramUri, "r");
    } catch (IOException iOException) {
      return null;
    } 
  }
  
  @Deprecated
  private static Bitmap transform(Matrix paramMatrix, Bitmap paramBitmap, int paramInt1, int paramInt2, int paramInt3) {
    Bitmap bitmap1;
    int i = 1;
    if ((paramInt3 & 0x1) != 0) {
      j = 1;
    } else {
      j = 0;
    } 
    if ((paramInt3 & 0x2) != 0) {
      paramInt3 = i;
    } else {
      paramInt3 = 0;
    } 
    int k = paramBitmap.getWidth() - paramInt1;
    i = paramBitmap.getHeight() - paramInt2;
    if (!j && (k < 0 || i < 0)) {
      bitmap1 = Bitmap.createBitmap(paramInt1, paramInt2, Bitmap.Config.ARGB_8888);
      Canvas canvas = new Canvas(bitmap1);
      j = Math.max(0, k / 2);
      i = Math.max(0, i / 2);
      k = Math.min(paramInt1, paramBitmap.getWidth());
      Rect rect1 = new Rect(j, i, k + j, Math.min(paramInt2, paramBitmap.getHeight()) + i);
      i = (paramInt1 - rect1.width()) / 2;
      j = (paramInt2 - rect1.height()) / 2;
      Rect rect2 = new Rect(i, j, paramInt1 - i, paramInt2 - j);
      canvas.drawBitmap(paramBitmap, rect1, rect2, null);
      if (paramInt3 != 0)
        paramBitmap.recycle(); 
      canvas.setBitmap(null);
      return bitmap1;
    } 
    float f1 = paramBitmap.getWidth();
    float f2 = paramBitmap.getHeight();
    float f3 = f1 / f2;
    float f4 = paramInt1 / paramInt2;
    if (f3 > f4) {
      f1 = paramInt2 / f2;
      if (f1 < 0.9F || f1 > 1.0F) {
        bitmap1.setScale(f1, f1);
      } else {
        bitmap1 = null;
      } 
    } else {
      f1 = paramInt1 / f1;
      if (f1 < 0.9F || f1 > 1.0F) {
        bitmap1.setScale(f1, f1);
      } else {
        bitmap1 = null;
      } 
    } 
    if (bitmap1 != null) {
      j = paramBitmap.getWidth();
      i = paramBitmap.getHeight();
      bitmap1 = Bitmap.createBitmap(paramBitmap, 0, 0, j, i, (Matrix)bitmap1, true);
    } else {
      bitmap1 = paramBitmap;
    } 
    if (paramInt3 != 0 && bitmap1 != paramBitmap)
      paramBitmap.recycle(); 
    int j = Math.max(0, bitmap1.getWidth() - paramInt1);
    i = Math.max(0, bitmap1.getHeight() - paramInt2);
    Bitmap bitmap2 = Bitmap.createBitmap(bitmap1, j / 2, i / 2, paramInt1, paramInt2);
    if (bitmap2 != bitmap1 && (
      paramInt3 != 0 || bitmap1 != paramBitmap))
      bitmap1.recycle(); 
    return bitmap2;
  }
  
  @Deprecated
  private static class SizedThumbnailBitmap {
    public Bitmap mBitmap;
    
    public byte[] mThumbnailData;
    
    public int mThumbnailHeight;
    
    public int mThumbnailWidth;
  }
  
  @Deprecated
  private static void createThumbnailFromEXIF(String paramString, int paramInt1, int paramInt2, SizedThumbnailBitmap paramSizedThumbnailBitmap) {}
}
