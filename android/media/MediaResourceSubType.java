package android.media;

public @interface MediaResourceSubType {
  public static final int kAudioCodec = 1;
  
  public static final int kUnspecifiedSubType = 0;
  
  public static final int kVideoCodec = 2;
}
