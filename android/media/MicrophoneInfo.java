package android.media;

import android.util.Pair;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.List;

public final class MicrophoneInfo {
  public static final int CHANNEL_MAPPING_DIRECT = 1;
  
  public static final int CHANNEL_MAPPING_PROCESSED = 2;
  
  public static final int DIRECTIONALITY_BI_DIRECTIONAL = 2;
  
  public static final int DIRECTIONALITY_CARDIOID = 3;
  
  public static final int DIRECTIONALITY_HYPER_CARDIOID = 4;
  
  public static final int DIRECTIONALITY_OMNI = 1;
  
  public static final int DIRECTIONALITY_SUPER_CARDIOID = 5;
  
  public static final int DIRECTIONALITY_UNKNOWN = 0;
  
  public static final int GROUP_UNKNOWN = -1;
  
  public static final int INDEX_IN_THE_GROUP_UNKNOWN = -1;
  
  public static final int LOCATION_MAINBODY = 1;
  
  public static final int LOCATION_MAINBODY_MOVABLE = 2;
  
  public static final int LOCATION_PERIPHERAL = 3;
  
  public static final int LOCATION_UNKNOWN = 0;
  
  public static final Coordinate3F ORIENTATION_UNKNOWN;
  
  public static final Coordinate3F POSITION_UNKNOWN = new Coordinate3F(-3.4028235E38F, -3.4028235E38F, -3.4028235E38F);
  
  public static final float SENSITIVITY_UNKNOWN = -3.4028235E38F;
  
  public static final float SPL_UNKNOWN = -3.4028235E38F;
  
  private String mAddress;
  
  private List<Pair<Integer, Integer>> mChannelMapping;
  
  private String mDeviceId;
  
  private int mDirectionality;
  
  private List<Pair<Float, Float>> mFrequencyResponse;
  
  private int mGroup;
  
  private int mIndexInTheGroup;
  
  private int mLocation;
  
  private float mMaxSpl;
  
  private float mMinSpl;
  
  private Coordinate3F mOrientation;
  
  private int mPortId;
  
  private Coordinate3F mPosition;
  
  private float mSensitivity;
  
  private int mType;
  
  static {
    ORIENTATION_UNKNOWN = new Coordinate3F(0.0F, 0.0F, 0.0F);
  }
  
  MicrophoneInfo(String paramString1, int paramInt1, String paramString2, int paramInt2, int paramInt3, int paramInt4, Coordinate3F paramCoordinate3F1, Coordinate3F paramCoordinate3F2, List<Pair<Float, Float>> paramList, List<Pair<Integer, Integer>> paramList1, float paramFloat1, float paramFloat2, float paramFloat3, int paramInt5) {
    this.mDeviceId = paramString1;
    this.mType = paramInt1;
    this.mAddress = paramString2;
    this.mLocation = paramInt2;
    this.mGroup = paramInt3;
    this.mIndexInTheGroup = paramInt4;
    this.mPosition = paramCoordinate3F1;
    this.mOrientation = paramCoordinate3F2;
    this.mFrequencyResponse = paramList;
    this.mChannelMapping = paramList1;
    this.mSensitivity = paramFloat1;
    this.mMaxSpl = paramFloat2;
    this.mMinSpl = paramFloat3;
    this.mDirectionality = paramInt5;
  }
  
  public String getDescription() {
    return this.mDeviceId;
  }
  
  public int getId() {
    return this.mPortId;
  }
  
  public int getInternalDeviceType() {
    return this.mType;
  }
  
  public int getType() {
    return AudioDeviceInfo.convertInternalDeviceToDeviceType(this.mType);
  }
  
  public String getAddress() {
    return this.mAddress;
  }
  
  public int getLocation() {
    return this.mLocation;
  }
  
  public int getGroup() {
    return this.mGroup;
  }
  
  public int getIndexInTheGroup() {
    return this.mIndexInTheGroup;
  }
  
  public Coordinate3F getPosition() {
    return this.mPosition;
  }
  
  public Coordinate3F getOrientation() {
    return this.mOrientation;
  }
  
  public List<Pair<Float, Float>> getFrequencyResponse() {
    return this.mFrequencyResponse;
  }
  
  public List<Pair<Integer, Integer>> getChannelMapping() {
    return this.mChannelMapping;
  }
  
  public float getSensitivity() {
    return this.mSensitivity;
  }
  
  public float getMaxSpl() {
    return this.mMaxSpl;
  }
  
  public float getMinSpl() {
    return this.mMinSpl;
  }
  
  public int getDirectionality() {
    return this.mDirectionality;
  }
  
  public void setId(int paramInt) {
    this.mPortId = paramInt;
  }
  
  public void setChannelMapping(List<Pair<Integer, Integer>> paramList) {
    this.mChannelMapping = paramList;
  }
  
  public static final class Coordinate3F {
    public final float x;
    
    public final float y;
    
    public final float z;
    
    Coordinate3F(float param1Float1, float param1Float2, float param1Float3) {
      this.x = param1Float1;
      this.y = param1Float2;
      this.z = param1Float3;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof Coordinate3F))
        return false; 
      param1Object = param1Object;
      if (this.x != ((Coordinate3F)param1Object).x || this.y != ((Coordinate3F)param1Object).y || this.z != ((Coordinate3F)param1Object).z)
        bool = false; 
      return bool;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MicrophoneDirectionality {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface MicrophoneLocation {}
}
