package android.media;

import java.util.Set;

public interface AudioMetadataReadMap {
  <T> boolean containsKey(AudioMetadata.Key<T> paramKey);
  
  AudioMetadataMap dup();
  
  <T> T get(AudioMetadata.Key<T> paramKey);
  
  Set<AudioMetadata.Key<?>> keySet();
  
  int size();
}
