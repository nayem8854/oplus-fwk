package android.media;

import android.content.Context;
import android.media.audiofx.AudioEffect;
import android.media.audiopolicy.AudioMix;
import android.telephony.TelephonyManager;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public class AudioSystem {
  public static final int AUDIO_FORMAT_AAC = 67108864;
  
  public static final int AUDIO_FORMAT_APTX = 536870912;
  
  public static final int AUDIO_FORMAT_APTX_ADAPTIVE = 654311424;
  
  public static final int AUDIO_FORMAT_APTX_HD = 553648128;
  
  public static final int AUDIO_FORMAT_APTX_TWSP = 704643072;
  
  public static final int AUDIO_FORMAT_CELT = 637534208;
  
  public static final int AUDIO_FORMAT_DEFAULT = 0;
  
  public static final int AUDIO_FORMAT_INVALID = -1;
  
  public static final int AUDIO_FORMAT_LDAC = 587202560;
  
  public static final int AUDIO_FORMAT_LHDC = 671088640;
  
  public static final int AUDIO_FORMAT_LHDC_LL = 687865856;
  
  public static final int AUDIO_FORMAT_SBC = 520093696;
  
  public static final int AUDIO_HW_SYNC_INVALID = 0;
  
  public static final int AUDIO_SESSION_ALLOCATE = 0;
  
  public static final int AUDIO_STATUS_ERROR = 1;
  
  public static final int AUDIO_STATUS_OK = 0;
  
  public static final int AUDIO_STATUS_SERVER_DIED = 100;
  
  public static final int BAD_VALUE = -2;
  
  public static final int DEAD_OBJECT = -6;
  
  private static final boolean DEBUG_VOLUME = false;
  
  public static final int DEFAULT_MUTE_STREAMS_AFFECTED = 111;
  
  public static int[] DEFAULT_STREAM_VOLUME;
  
  public static final Set<Integer> DEVICE_ALL_HDMI_SYSTEM_AUDIO_AND_SPEAKER_SET;
  
  public static final int DEVICE_BIT_DEFAULT = 1073741824;
  
  public static final int DEVICE_BIT_IN = -2147483648;
  
  public static final Set<Integer> DEVICE_IN_ALL_SCO_SET;
  
  public static final Set<Integer> DEVICE_IN_ALL_SET;
  
  public static final Set<Integer> DEVICE_IN_ALL_USB_SET;
  
  public static final int DEVICE_IN_AMBIENT = -2147483646;
  
  public static final String DEVICE_IN_AMBIENT_NAME = "ambient";
  
  public static final int DEVICE_IN_ANLG_DOCK_HEADSET = -2147483136;
  
  public static final String DEVICE_IN_ANLG_DOCK_HEADSET_NAME = "analog_dock";
  
  public static final int DEVICE_IN_AUX_DIGITAL = -2147483616;
  
  public static final String DEVICE_IN_AUX_DIGITAL_NAME = "aux_digital";
  
  public static final int DEVICE_IN_BACK_MIC = -2147483520;
  
  public static final String DEVICE_IN_BACK_MIC_NAME = "back_mic";
  
  public static final int DEVICE_IN_BLUETOOTH_A2DP = -2147352576;
  
  public static final String DEVICE_IN_BLUETOOTH_A2DP_NAME = "bt_a2dp";
  
  public static final int DEVICE_IN_BLUETOOTH_BLE = -2080374784;
  
  public static final String DEVICE_IN_BLUETOOTH_BLE_NAME = "bt_ble";
  
  public static final int DEVICE_IN_BLUETOOTH_SCO_HEADSET = -2147483640;
  
  public static final String DEVICE_IN_BLUETOOTH_SCO_HEADSET_NAME = "bt_sco_hs";
  
  public static final int DEVICE_IN_BUILTIN_MIC = -2147483644;
  
  public static final String DEVICE_IN_BUILTIN_MIC_NAME = "mic";
  
  public static final int DEVICE_IN_BUS = -2146435072;
  
  public static final String DEVICE_IN_BUS_NAME = "bus";
  
  public static final int DEVICE_IN_COMMUNICATION = -2147483647;
  
  public static final String DEVICE_IN_COMMUNICATION_NAME = "communication";
  
  public static final int DEVICE_IN_DEFAULT = -1073741824;
  
  public static final int DEVICE_IN_DGTL_DOCK_HEADSET = -2147482624;
  
  public static final String DEVICE_IN_DGTL_DOCK_HEADSET_NAME = "digital_dock";
  
  public static final int DEVICE_IN_ECHO_REFERENCE = -1879048192;
  
  public static final String DEVICE_IN_ECHO_REFERENCE_NAME = "echo_reference";
  
  public static final int DEVICE_IN_FM_TUNER = -2147475456;
  
  public static final String DEVICE_IN_FM_TUNER_NAME = "fm_tuner";
  
  public static final int DEVICE_IN_HDMI = -2147483616;
  
  public static final int DEVICE_IN_HDMI_ARC = -2013265920;
  
  public static final String DEVICE_IN_HDMI_ARC_NAME = "hdmi_arc";
  
  public static final int DEVICE_IN_IP = -2146959360;
  
  public static final String DEVICE_IN_IP_NAME = "ip";
  
  public static final int DEVICE_IN_LINE = -2147450880;
  
  public static final String DEVICE_IN_LINE_NAME = "line";
  
  public static final int DEVICE_IN_LOOPBACK = -2147221504;
  
  public static final String DEVICE_IN_LOOPBACK_NAME = "loopback";
  
  public static final int DEVICE_IN_PROXY = -2130706432;
  
  public static final String DEVICE_IN_PROXY_NAME = "proxy";
  
  public static final int DEVICE_IN_REMOTE_SUBMIX = -2147483392;
  
  public static final String DEVICE_IN_REMOTE_SUBMIX_NAME = "remote_submix";
  
  public static final int DEVICE_IN_SPDIF = -2147418112;
  
  public static final String DEVICE_IN_SPDIF_NAME = "spdif";
  
  public static final int DEVICE_IN_TELEPHONY_RX = -2147483584;
  
  public static final String DEVICE_IN_TELEPHONY_RX_NAME = "telephony_rx";
  
  public static final int DEVICE_IN_TV_TUNER = -2147467264;
  
  public static final String DEVICE_IN_TV_TUNER_NAME = "tv_tuner";
  
  public static final int DEVICE_IN_USB_ACCESSORY = -2147481600;
  
  public static final String DEVICE_IN_USB_ACCESSORY_NAME = "usb_accessory";
  
  public static final int DEVICE_IN_USB_DEVICE = -2147479552;
  
  public static final String DEVICE_IN_USB_DEVICE_NAME = "usb_device";
  
  public static final int DEVICE_IN_USB_HEADSET = -2113929216;
  
  public static final String DEVICE_IN_USB_HEADSET_NAME = "usb_headset";
  
  public static final int DEVICE_IN_VOICE_CALL = -2147483584;
  
  public static final int DEVICE_IN_WIRED_HEADSET = -2147483632;
  
  public static final String DEVICE_IN_WIRED_HEADSET_NAME = "headset";
  
  public static final int DEVICE_NONE = 0;
  
  public static final Set<Integer> DEVICE_OUT_ALL_A2DP_SET;
  
  public static final Set<Integer> DEVICE_OUT_ALL_HDMI_SYSTEM_AUDIO_SET;
  
  public static final Set<Integer> DEVICE_OUT_ALL_SCO_SET;
  
  public static final Set<Integer> DEVICE_OUT_ALL_SET;
  
  public static final int DEVICE_OUT_ALL_USB = 67133440;
  
  public static final Set<Integer> DEVICE_OUT_ALL_USB_SET;
  
  public static final int DEVICE_OUT_ANLG_DOCK_HEADSET = 2048;
  
  public static final String DEVICE_OUT_ANLG_DOCK_HEADSET_NAME = "analog_dock";
  
  public static final int DEVICE_OUT_AUX_DIGITAL = 1024;
  
  public static final String DEVICE_OUT_AUX_DIGITAL_NAME = "aux_digital";
  
  public static final int DEVICE_OUT_AUX_LINE = 2097152;
  
  public static final String DEVICE_OUT_AUX_LINE_NAME = "aux_line";
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP = 128;
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES = 256;
  
  public static final String DEVICE_OUT_BLUETOOTH_A2DP_HEADPHONES_NAME = "bt_a2dp_hp";
  
  public static final String DEVICE_OUT_BLUETOOTH_A2DP_NAME = "bt_a2dp";
  
  public static final int DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER = 512;
  
  public static final String DEVICE_OUT_BLUETOOTH_A2DP_SPEAKER_NAME = "bt_a2dp_spk";
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO = 16;
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO_CARKIT = 64;
  
  public static final String DEVICE_OUT_BLUETOOTH_SCO_CARKIT_NAME = "bt_sco_carkit";
  
  public static final int DEVICE_OUT_BLUETOOTH_SCO_HEADSET = 32;
  
  public static final String DEVICE_OUT_BLUETOOTH_SCO_HEADSET_NAME = "bt_sco_hs";
  
  public static final String DEVICE_OUT_BLUETOOTH_SCO_NAME = "bt_sco";
  
  public static final int DEVICE_OUT_BUS = 16777216;
  
  public static final String DEVICE_OUT_BUS_NAME = "bus";
  
  public static final int DEVICE_OUT_DEFAULT = 1073741824;
  
  public static final int DEVICE_OUT_DGTL_DOCK_HEADSET = 4096;
  
  public static final String DEVICE_OUT_DGTL_DOCK_HEADSET_NAME = "digital_dock";
  
  public static final int DEVICE_OUT_EARPIECE = 1;
  
  public static final String DEVICE_OUT_EARPIECE_NAME = "earpiece";
  
  public static final int DEVICE_OUT_FM = 1048576;
  
  public static final String DEVICE_OUT_FM_NAME = "fm_transmitter";
  
  public static final int DEVICE_OUT_HDMI = 1024;
  
  public static final int DEVICE_OUT_HDMI_ARC = 262144;
  
  public static final String DEVICE_OUT_HDMI_ARC_NAME = "hmdi_arc";
  
  public static final String DEVICE_OUT_HDMI_NAME = "hdmi";
  
  public static final int DEVICE_OUT_HEARING_AID = 134217728;
  
  public static final String DEVICE_OUT_HEARING_AID_NAME = "hearing_aid_out";
  
  public static final int DEVICE_OUT_IP = 8388608;
  
  public static final String DEVICE_OUT_IP_NAME = "ip";
  
  public static final int DEVICE_OUT_LINE = 131072;
  
  public static final String DEVICE_OUT_LINE_NAME = "line";
  
  public static final int DEVICE_OUT_PROXY = 33554432;
  
  public static final String DEVICE_OUT_PROXY_NAME = "proxy";
  
  public static final int DEVICE_OUT_REMOTE_SUBMIX = 32768;
  
  public static final String DEVICE_OUT_REMOTE_SUBMIX_NAME = "remote_submix";
  
  public static final int DEVICE_OUT_SPDIF = 524288;
  
  public static final String DEVICE_OUT_SPDIF_NAME = "spdif";
  
  public static final int DEVICE_OUT_SPEAKER = 2;
  
  public static final String DEVICE_OUT_SPEAKER_NAME = "speaker";
  
  public static final int DEVICE_OUT_SPEAKER_SAFE = 4194304;
  
  public static final String DEVICE_OUT_SPEAKER_SAFE_NAME = "speaker_safe";
  
  public static final int DEVICE_OUT_TELEPHONY_TX = 65536;
  
  public static final String DEVICE_OUT_TELEPHONY_TX_NAME = "telephony_tx";
  
  public static final int DEVICE_OUT_USB_ACCESSORY = 8192;
  
  public static final String DEVICE_OUT_USB_ACCESSORY_NAME = "usb_accessory";
  
  public static final int DEVICE_OUT_USB_DEVICE = 16384;
  
  public static final String DEVICE_OUT_USB_DEVICE_NAME = "usb_device";
  
  public static final int DEVICE_OUT_USB_HEADSET = 67108864;
  
  public static final String DEVICE_OUT_USB_HEADSET_NAME = "usb_headset";
  
  public static final int DEVICE_OUT_WIRED_HEADPHONE = 8;
  
  public static final String DEVICE_OUT_WIRED_HEADPHONE_NAME = "headphone";
  
  public static final int DEVICE_OUT_WIRED_HEADSET = 4;
  
  public static final String DEVICE_OUT_WIRED_HEADSET_NAME = "headset";
  
  public static final int DEVICE_STATE_AVAILABLE = 1;
  
  public static final int DEVICE_STATE_UNAVAILABLE = 0;
  
  private static final int DYNAMIC_POLICY_EVENT_MIX_STATE_UPDATE = 0;
  
  public static final int ERROR = -1;
  
  public static final int FORCE_ANALOG_DOCK = 8;
  
  public static final int FORCE_BT_A2DP = 4;
  
  public static final int FORCE_BT_CAR_DOCK = 6;
  
  public static final int FORCE_BT_DESK_DOCK = 7;
  
  public static final int FORCE_BT_SCO = 3;
  
  public static final int FORCE_DEFAULT = 0;
  
  public static final int FORCE_DIGITAL_DOCK = 9;
  
  public static final int FORCE_ENCODED_SURROUND_ALWAYS = 14;
  
  public static final int FORCE_ENCODED_SURROUND_MANUAL = 15;
  
  public static final int FORCE_ENCODED_SURROUND_NEVER = 13;
  
  public static final int FORCE_HDMI_SYSTEM_AUDIO_ENFORCED = 12;
  
  public static final int FORCE_HEADPHONES = 2;
  
  public static final int FORCE_NONE = 0;
  
  public static final int FORCE_NO_BT_A2DP = 10;
  
  public static final int FORCE_SPEAKER = 1;
  
  public static final int FORCE_SYSTEM_ENFORCED = 11;
  
  public static final int FORCE_WIRED_ACCESSORY = 5;
  
  public static final int FOR_COMMUNICATION = 0;
  
  public static final int FOR_DOCK = 3;
  
  public static final int FOR_ENCODED_SURROUND = 6;
  
  public static final int FOR_HDMI_SYSTEM_AUDIO = 5;
  
  public static final int FOR_MEDIA = 1;
  
  public static final int FOR_RECORD = 2;
  
  public static final int FOR_SYSTEM = 4;
  
  public static final int FOR_VIBRATE_RINGING = 7;
  
  public static final int INVALID_OPERATION = -3;
  
  public static final String IN_VOICE_COMM_FOCUS_ID = "AudioFocus_For_Phone_Ring_And_Calls";
  
  public static final String LEGACY_REMOTE_SUBMIX_ADDRESS = "0";
  
  private static final int MAX_DEVICE_ROUTING = 4;
  
  public static final int MODE_CALL_SCREENING = 4;
  
  public static final int MODE_CURRENT = -1;
  
  public static final int MODE_INVALID = -2;
  
  public static final int MODE_IN_CALL = 2;
  
  public static final int MODE_IN_COMMUNICATION = 3;
  
  public static final int MODE_NORMAL = 0;
  
  public static final int MODE_RINGTONE = 1;
  
  static final int NATIVE_EVENT_ROUTING_CHANGE = 1000;
  
  public static final int NO_INIT = -5;
  
  private static final int NUM_DEVICE_STATES = 1;
  
  public static final int NUM_FORCE_CONFIG = 16;
  
  private static final int NUM_FORCE_USE = 8;
  
  public static final int NUM_MODES = 5;
  
  public static final int NUM_STREAMS = 5;
  
  private static final int NUM_STREAM_TYPES = 12;
  
  private AudioSystem() {
    throw new UnsupportedOperationException("Trying to instantiate AudioSystem");
  }
  
  public static final int OUT_CHANNEL_COUNT_MAX = native_get_FCC_8();
  
  public static final int PERMISSION_DENIED = -4;
  
  public static final int PHONE_STATE_INCALL = 2;
  
  public static final int PHONE_STATE_OFFCALL = 0;
  
  public static final int PHONE_STATE_RINGING = 1;
  
  public static final int PLATFORM_DEFAULT = 0;
  
  public static final int PLATFORM_TELEVISION = 2;
  
  public static final int PLATFORM_VOICE = 1;
  
  public static final int PLAY_SOUND_DELAY = 300;
  
  @Deprecated
  public static final int ROUTE_ALL = -1;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH = 4;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH_A2DP = 16;
  
  @Deprecated
  public static final int ROUTE_BLUETOOTH_SCO = 4;
  
  @Deprecated
  public static final int ROUTE_EARPIECE = 1;
  
  @Deprecated
  public static final int ROUTE_HEADSET = 8;
  
  @Deprecated
  public static final int ROUTE_SPEAKER = 2;
  
  public static final int STREAM_ACCESSIBILITY = 10;
  
  public static final int STREAM_ALARM = 4;
  
  public static final int STREAM_ASSISTANT = 11;
  
  public static final int STREAM_BLUETOOTH_SCO = 6;
  
  public static final int STREAM_DEFAULT = -1;
  
  public static final int STREAM_DTMF = 8;
  
  public static final int STREAM_MUSIC = 3;
  
  public static final int getNumStreamTypes() {
    return 12;
  }
  
  public static final String[] STREAM_NAMES = new String[] { 
      "STREAM_VOICE_CALL", "STREAM_SYSTEM", "STREAM_RING", "STREAM_MUSIC", "STREAM_ALARM", "STREAM_NOTIFICATION", "STREAM_BLUETOOTH_SCO", "STREAM_SYSTEM_ENFORCED", "STREAM_DTMF", "STREAM_TTS", 
      "STREAM_ACCESSIBILITY", "STREAM_ASSISTANT" };
  
  public static final int STREAM_NOTIFICATION = 5;
  
  public static final int STREAM_RING = 2;
  
  public static final int STREAM_SYSTEM = 1;
  
  public static final int STREAM_SYSTEM_ENFORCED = 7;
  
  public static final int STREAM_TTS = 9;
  
  public static final int STREAM_VOICE_CALL = 0;
  
  public static final int SUCCESS = 0;
  
  public static final int SYNC_EVENT_NONE = 0;
  
  public static final int SYNC_EVENT_PRESENTATION_COMPLETE = 1;
  
  private static final String TAG = "AudioSystem";
  
  public static final int WOULD_BLOCK = -7;
  
  private static DynamicPolicyCallback sDynPolicyCallback;
  
  private static ErrorCallback sErrorCallback;
  
  private static AudioRecordingCallback sRecordingCallback;
  
  public static String modeToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unknown mode (");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 4:
        return "MODE_CALL_SCREENING";
      case 3:
        return "MODE_IN_COMMUNICATION";
      case 2:
        return "MODE_IN_CALL";
      case 1:
        return "MODE_RINGTONE";
      case 0:
        return "MODE_NORMAL";
      case -1:
        return "MODE_CURRENT";
      case -2:
        break;
    } 
    return "MODE_INVALID";
  }
  
  public static int audioFormatToBluetoothSourceCodec(int paramInt) {
    if (paramInt != 67108864) {
      if (paramInt != 520093696) {
        if (paramInt != 536870912) {
          if (paramInt != 553648128) {
            if (paramInt != 587202560) {
              if (paramInt != 637534208) {
                if (paramInt != 654311424) {
                  if (paramInt != 704643072) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Unknown audio format 0x");
                    stringBuilder.append(Integer.toHexString(paramInt));
                    stringBuilder.append(" for conversion to BT codec");
                    Log.e("AudioSystem", stringBuilder.toString());
                    return 1000000;
                  } 
                  return 6;
                } 
                return 4;
              } 
              return 11;
            } 
            return 5;
          } 
          return 3;
        } 
        return 2;
      } 
      return 0;
    } 
    return 1;
  }
  
  public static int bluetoothCodecToAudioFormat(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown BT codec 0x");
        stringBuilder.append(Integer.toHexString(paramInt));
        stringBuilder.append(" for conversion to audio format");
        Log.e("AudioSystem", stringBuilder.toString());
        return 0;
      case 11:
        return 637534208;
      case 7:
      case 8:
      case 9:
        return 671088640;
      case 6:
        return 704643072;
      case 5:
        return 587202560;
      case 4:
        return 654311424;
      case 3:
        return 553648128;
      case 2:
        return 536870912;
      case 1:
        return 67108864;
      case 0:
        break;
    } 
    return 520093696;
  }
  
  public static String audioFormatToString(int paramInt) {
    if (paramInt != 167772160) {
      if (paramInt != 167772161) {
        StringBuilder stringBuilder;
        switch (paramInt) {
          default:
            switch (paramInt) {
              default:
                switch (paramInt) {
                  default:
                    switch (paramInt) {
                      default:
                        switch (paramInt) {
                          default:
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("AUDIO_FORMAT_(");
                            stringBuilder.append(paramInt);
                            stringBuilder.append(")");
                            return stringBuilder.toString();
                          case 603979779:
                            return "AUDIO_FORMAT_MAT_2_1";
                          case 603979778:
                            return "AUDIO_FORMAT_MAT_2_0";
                          case 603979777:
                            return "AUDIO_FORMAT_MAT_1_0";
                          case 603979776:
                            break;
                        } 
                        return "AUDIO_FORMAT_MAT";
                      case 503316482:
                        return "AUDIO_FORMAT_AAC_ADTS_LC";
                      case 503316481:
                        return "AUDIO_FORMAT_AAC_ADTS_MAIN";
                      case 503316480:
                        break;
                    } 
                    return "AUDIO_FORMAT_AAC_ADTS";
                  case 67108866:
                    return "AUDIO_FORMAT_AAC_LC";
                  case 67108865:
                    return "AUDIO_FORMAT_AAC_MAIN";
                  case 67108864:
                    break;
                } 
                return "AUDIO_FORMAT_AAC";
              case 704643072:
                return "AUDIO_FORMAT_APTX_TWSP";
              case 687865856:
                return "AUDIO_FORMAT_LHDC_LL";
              case 671088640:
                return "AUDIO_FORMAT_LHDC";
              case 654311424:
                return "AUDIO_FORMAT_APTX_ADAPTIVE";
              case 637534208:
                return "AUDIO_FORMAT_CELT";
              case 620757248:
                return "AUDIO_FORMAT_AAC_LATM_HE_V2";
              case 620757008:
                return "AUDIO_FORMAT_AAC_LATM_HE_V1";
              case 620756994:
                return "AUDIO_FORMAT_AAC_LATM_LC";
              case 620756992:
                return "AUDIO_FORMAT_AAC_LATM";
              case 587202560:
                return "AUDIO_FORMAT_LDAC";
              case 570425344:
                return "AUDIO_FORMAT_AC4";
              case 553648128:
                return "AUDIO_FORMAT_APTX_HD";
              case 536870912:
                return "AUDIO_FORMAT_APTX";
              case 520093696:
                return "AUDIO_FORMAT_SBC";
              case 503317248:
                return "AUDIO_FORMAT_AAC_ADTS_XHE";
              case 503316992:
                return "AUDIO_FORMAT_AAC_ADTS_ELD";
              case 503316736:
                return "AUDIO_FORMAT_AAC_ADTS_HE_V2";
              case 503316608:
                return "AUDIO_FORMAT_AAC_ADTS_LD";
              case 503316544:
                return "AUDIO_FORMAT_AAC_ADTS_ERLC";
              case 503316512:
                return "AUDIO_FORMAT_AAC_ADTS_SCALABLE";
              case 503316496:
                return "AUDIO_FORMAT_AAC_ADTS_HE_V1";
              case 503316488:
                return "AUDIO_FORMAT_AAC_ADTS_LTP";
              case 503316484:
                return "AUDIO_FORMAT_AAC_ADTS_SSR";
              case 486539264:
                return "AUDIO_FORMAT_APE";
              case 469762048:
                return "AUDIO_FORMAT_ALAC";
              case 452984832:
                return "AUDIO_FORMAT_FLAC";
              case 436207616:
                return "AUDIO_FORMAT_DSD";
              case 419430400:
                return "AUDIO_FORMAT_QCELP";
              case 402653184:
                return "AUDIO_FORMAT_MP2";
              case 385875968:
                return "AUDIO_FORMAT_AMR_WB_PLUS";
              case 369098752:
                return "AUDIO_FORMAT_WMA_PRO";
              case 352321536:
                return "AUDIO_FORMAT_WMA";
              case 335544320:
                return "AUDIO_FORMAT_AAC_ADIF";
              case 318767104:
                return "AUDIO_FORMAT_EVRCNW";
              case 301989888:
                return "AUDIO_FORMAT_EVRCWB";
              case 285212672:
                return "AUDIO_FORMAT_EVRCB";
              case 268435456:
                return "AUDIO_FORMAT_EVRC";
              case 234881024:
                return "AUDIO_FORMAT_DOLBY_TRUEHD";
              case 218103808:
                return "AUDIO_FORMAT_IEC61937";
              case 201326592:
                return "AUDIO_FORMAT_DTS_HD";
              case 184549376:
                return "AUDIO_FORMAT_DTS";
              case 150994944:
                return "AUDIO_FORMAT_AC3";
              case 134217728:
                return "AUDIO_FORMAT_OPUS";
              case 117440512:
                return "AUDIO_FORMAT_VORBIS";
              case 100663296:
                return "AUDIO_FORMAT_HE_AAC_V2";
              case 83886080:
                return "AUDIO_FORMAT_HE_AAC_V1";
              case 67109632:
                return "AUDIO_FORMAT_AAC_XHE";
              case 67109376:
                return "AUDIO_FORMAT_AAC_ELD";
              case 67109120:
                return "AUDIO_FORMAT_AAC_HE_V2";
              case 67108992:
                return "AUDIO_FORMAT_AAC_LD";
              case 67108928:
                return "AUDIO_FORMAT_AAC_ERLC";
              case 67108896:
                return "AUDIO_FORMAT_AAC_SCALABLE";
              case 67108880:
                return "AUDIO_FORMAT_AAC_HE_V1";
              case 67108872:
                return "AUDIO_FORMAT_AAC_LTP";
              case 67108868:
                return "AUDIO_FORMAT_AAC_SSR";
              case 50331648:
                return "AUDIO_FORMAT_AMR_WB";
              case 33554432:
                return "AUDIO_FORMAT_AMR_NB";
              case 16777216:
                break;
            } 
            return "AUDIO_FORMAT_MP3";
          case 6:
            return "AUDIO_FORMAT_PCM_24_BIT_PACKED";
          case 5:
            return "AUDIO_FORMAT_PCM_FLOAT";
          case 4:
            return "AUDIO_FORMAT_PCM_8_24_BIT";
          case 3:
            return "AUDIO_FORMAT_PCM_32_BIT";
          case 2:
            return "AUDIO_FORMAT_PCM_8_BIT";
          case 1:
            return "AUDIO_FORMAT_PCM_16_BIT";
          case 0:
            return "AUDIO_FORMAT_DEFAULT";
          case -1:
            break;
        } 
        return "AUDIO_FORMAT_INVALID";
      } 
      return "AUDIO_FORMAT_E_AC3_JOC";
    } 
    return "AUDIO_FORMAT_E_AC3";
  }
  
  public static void setErrorCallback(ErrorCallback paramErrorCallback) {
    // Byte code:
    //   0: ldc android/media/AudioSystem
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic android/media/AudioSystem.sErrorCallback : Landroid/media/AudioSystem$ErrorCallback;
    //   7: aload_0
    //   8: ifnull -> 20
    //   11: aload_0
    //   12: invokestatic checkAudioFlinger : ()I
    //   15: invokeinterface onError : (I)V
    //   20: ldc android/media/AudioSystem
    //   22: monitorexit
    //   23: return
    //   24: astore_0
    //   25: ldc android/media/AudioSystem
    //   27: monitorexit
    //   28: aload_0
    //   29: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #582	-> 0
    //   #583	-> 3
    //   #584	-> 7
    //   #585	-> 11
    //   #587	-> 20
    //   #588	-> 23
    //   #587	-> 24
    // Exception table:
    //   from	to	target	type
    //   3	7	24	finally
    //   11	20	24	finally
    //   20	23	24	finally
    //   25	28	24	finally
  }
  
  private static void errorCallbackFromNative(int paramInt) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_1
    //   2: ldc android/media/AudioSystem
    //   4: monitorenter
    //   5: getstatic android/media/AudioSystem.sErrorCallback : Landroid/media/AudioSystem$ErrorCallback;
    //   8: ifnull -> 15
    //   11: getstatic android/media/AudioSystem.sErrorCallback : Landroid/media/AudioSystem$ErrorCallback;
    //   14: astore_1
    //   15: ldc android/media/AudioSystem
    //   17: monitorexit
    //   18: aload_1
    //   19: ifnull -> 29
    //   22: aload_1
    //   23: iload_0
    //   24: invokeinterface onError : (I)V
    //   29: return
    //   30: astore_1
    //   31: ldc android/media/AudioSystem
    //   33: monitorexit
    //   34: aload_1
    //   35: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #593	-> 0
    //   #594	-> 2
    //   #595	-> 5
    //   #596	-> 11
    //   #598	-> 15
    //   #599	-> 18
    //   #600	-> 22
    //   #602	-> 29
    //   #598	-> 30
    // Exception table:
    //   from	to	target	type
    //   5	11	30	finally
    //   11	15	30	finally
    //   15	18	30	finally
    //   31	34	30	finally
  }
  
  public static void setDynamicPolicyCallback(DynamicPolicyCallback paramDynamicPolicyCallback) {
    // Byte code:
    //   0: ldc android/media/AudioSystem
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic android/media/AudioSystem.sDynPolicyCallback : Landroid/media/AudioSystem$DynamicPolicyCallback;
    //   7: invokestatic native_register_dynamic_policy_callback : ()V
    //   10: ldc android/media/AudioSystem
    //   12: monitorexit
    //   13: return
    //   14: astore_0
    //   15: ldc android/media/AudioSystem
    //   17: monitorexit
    //   18: aload_0
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #622	-> 0
    //   #623	-> 3
    //   #624	-> 7
    //   #625	-> 10
    //   #626	-> 13
    //   #625	-> 14
    // Exception table:
    //   from	to	target	type
    //   3	7	14	finally
    //   7	10	14	finally
    //   10	13	14	finally
    //   15	18	14	finally
  }
  
  private static void dynamicPolicyCallbackFromNative(int paramInt1, String paramString, int paramInt2) {
    // Byte code:
    //   0: aconst_null
    //   1: astore_3
    //   2: ldc android/media/AudioSystem
    //   4: monitorenter
    //   5: getstatic android/media/AudioSystem.sDynPolicyCallback : Landroid/media/AudioSystem$DynamicPolicyCallback;
    //   8: ifnull -> 15
    //   11: getstatic android/media/AudioSystem.sDynPolicyCallback : Landroid/media/AudioSystem$DynamicPolicyCallback;
    //   14: astore_3
    //   15: ldc android/media/AudioSystem
    //   17: monitorexit
    //   18: aload_3
    //   19: ifnull -> 70
    //   22: iload_0
    //   23: ifeq -> 62
    //   26: new java/lang/StringBuilder
    //   29: dup
    //   30: invokespecial <init> : ()V
    //   33: astore_1
    //   34: aload_1
    //   35: ldc_w 'dynamicPolicyCallbackFromNative: unknown event '
    //   38: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   41: pop
    //   42: aload_1
    //   43: iload_0
    //   44: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   47: pop
    //   48: ldc_w 'AudioSystem'
    //   51: aload_1
    //   52: invokevirtual toString : ()Ljava/lang/String;
    //   55: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   58: pop
    //   59: goto -> 70
    //   62: aload_3
    //   63: aload_1
    //   64: iload_2
    //   65: invokeinterface onDynamicPolicyMixStateUpdate : (Ljava/lang/String;I)V
    //   70: return
    //   71: astore_1
    //   72: ldc android/media/AudioSystem
    //   74: monitorexit
    //   75: aload_1
    //   76: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #631	-> 0
    //   #632	-> 2
    //   #633	-> 5
    //   #634	-> 11
    //   #636	-> 15
    //   #637	-> 18
    //   #638	-> 22
    //   #643	-> 26
    //   #640	-> 62
    //   #646	-> 70
    //   #636	-> 71
    // Exception table:
    //   from	to	target	type
    //   5	11	71	finally
    //   11	15	71	finally
    //   15	18	71	finally
    //   72	75	71	finally
  }
  
  public static void setRecordingCallback(AudioRecordingCallback paramAudioRecordingCallback) {
    // Byte code:
    //   0: ldc android/media/AudioSystem
    //   2: monitorenter
    //   3: aload_0
    //   4: putstatic android/media/AudioSystem.sRecordingCallback : Landroid/media/AudioSystem$AudioRecordingCallback;
    //   7: invokestatic native_register_recording_callback : ()V
    //   10: ldc android/media/AudioSystem
    //   12: monitorexit
    //   13: return
    //   14: astore_0
    //   15: ldc android/media/AudioSystem
    //   17: monitorexit
    //   18: aload_0
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #683	-> 0
    //   #684	-> 3
    //   #685	-> 7
    //   #686	-> 10
    //   #687	-> 13
    //   #686	-> 14
    // Exception table:
    //   from	to	target	type
    //   3	7	14	finally
    //   7	10	14	finally
    //   10	13	14	finally
    //   15	18	14	finally
  }
  
  private static void recordingCallbackFromNative(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean, int[] paramArrayOfint, AudioEffect.Descriptor[] paramArrayOfDescriptor1, AudioEffect.Descriptor[] paramArrayOfDescriptor2, int paramInt7) {
    /* monitor enter TypeReferenceDotClassExpression{ObjectType{android/media/AudioSystem}} */
    try {
      AudioRecordingCallback audioRecordingCallback = sRecordingCallback;
      try {
        /* monitor exit TypeReferenceDotClassExpression{ObjectType{android/media/AudioSystem}} */
        if (paramArrayOfDescriptor1.length != 0)
          String str = (paramArrayOfDescriptor1[0]).name; 
        if (paramArrayOfDescriptor2.length == 0) {
          String str = "None";
        } else {
          String str = (paramArrayOfDescriptor2[0]).name;
        } 
        if (audioRecordingCallback != null) {
          ArrayList<AudioPatch> arrayList = new ArrayList();
          if (AudioManager.listAudioPatches(arrayList) == 0) {
            boolean bool2, bool1 = false;
            int i = paramArrayOfint[6];
            Iterator<AudioPatch> iterator = arrayList.iterator();
            while (true) {
              bool2 = bool1;
              if (iterator.hasNext()) {
                AudioPatch audioPatch = iterator.next();
                if (audioPatch.id() == i) {
                  bool2 = true;
                  break;
                } 
                continue;
              } 
              break;
            } 
            if (!bool2)
              AudioManager.resetAudioPortGeneration(); 
          } 
          audioRecordingCallback.onRecordingConfigurationChanged(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, paramBoolean, paramArrayOfint, paramArrayOfDescriptor1, paramArrayOfDescriptor2, paramInt7, "");
        } 
        return;
      } finally {}
    } finally {}
    /* monitor exit TypeReferenceDotClassExpression{ObjectType{android/media/AudioSystem}} */
    throw paramArrayOfint;
  }
  
  public static String audioSystemErrorToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unknown error:");
        stringBuilder.append(paramInt);
        return stringBuilder.toString();
      case 0:
        return "SUCCESS";
      case -1:
        return "ERROR";
      case -2:
        return "BAD_VALUE";
      case -3:
        return "INVALID_OPERATION";
      case -4:
        return "PERMISSION_DENIED";
      case -5:
        return "NO_INIT";
      case -6:
        return "DEAD_OBJECT";
      case -7:
        break;
    } 
    return "WOULD_BLOCK";
  }
  
  static {
    HashSet<Integer> hashSet2 = new HashSet();
    hashSet2.add(Integer.valueOf(1));
    Set<Integer> set2 = DEVICE_OUT_ALL_SET;
    Integer integer2 = Integer.valueOf(2);
    set2.add(integer2);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(4));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(8));
    Set<Integer> set4 = DEVICE_OUT_ALL_SET;
    Integer integer4 = Integer.valueOf(16);
    set4.add(integer4);
    Set<Integer> set5 = DEVICE_OUT_ALL_SET;
    Integer integer5 = Integer.valueOf(32);
    set5.add(integer5);
    Set<Integer> set6 = DEVICE_OUT_ALL_SET;
    Integer integer6 = Integer.valueOf(64);
    set6.add(integer6);
    Set<Integer> set7 = DEVICE_OUT_ALL_SET;
    Integer integer7 = Integer.valueOf(128);
    set7.add(integer7);
    Set<Integer> set8 = DEVICE_OUT_ALL_SET;
    Integer integer8 = Integer.valueOf(256);
    set8.add(integer8);
    set8 = DEVICE_OUT_ALL_SET;
    Integer integer10 = Integer.valueOf(512);
    set8.add(integer10);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(1024));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(2048));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(4096));
    Set<Integer> set9 = DEVICE_OUT_ALL_SET;
    Integer integer9 = Integer.valueOf(8192);
    set9.add(integer9);
    set9 = DEVICE_OUT_ALL_SET;
    Integer integer12 = Integer.valueOf(16384);
    set9.add(integer12);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(32768));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(65536));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(131072));
    Set<Integer> set10 = DEVICE_OUT_ALL_SET;
    Integer integer11 = Integer.valueOf(262144);
    set10.add(integer11);
    Set<Integer> set11 = DEVICE_OUT_ALL_SET;
    Integer integer13 = Integer.valueOf(524288);
    set11.add(integer13);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(1048576));
    Set<Integer> set12 = DEVICE_OUT_ALL_SET;
    Integer integer14 = Integer.valueOf(2097152);
    set12.add(integer14);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(4194304));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(8388608));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(16777216));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(33554432));
    Set<Integer> set13 = DEVICE_OUT_ALL_SET;
    Integer integer15 = Integer.valueOf(67108864);
    set13.add(integer15);
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(134217728));
    DEVICE_OUT_ALL_SET.add(Integer.valueOf(1073741824));
    DEVICE_OUT_ALL_A2DP_SET = set13 = new HashSet<>();
    set13.add(integer7);
    DEVICE_OUT_ALL_A2DP_SET.add(integer8);
    DEVICE_OUT_ALL_A2DP_SET.add(integer10);
    HashSet<Integer> hashSet5 = new HashSet();
    hashSet5.add(integer4);
    DEVICE_OUT_ALL_SCO_SET.add(integer5);
    DEVICE_OUT_ALL_SCO_SET.add(integer6);
    HashSet<Integer> hashSet4 = new HashSet();
    hashSet4.add(integer9);
    DEVICE_OUT_ALL_USB_SET.add(integer12);
    DEVICE_OUT_ALL_USB_SET.add(integer15);
    DEVICE_OUT_ALL_HDMI_SYSTEM_AUDIO_SET = hashSet4 = new HashSet<>();
    hashSet4.add(integer14);
    DEVICE_OUT_ALL_HDMI_SYSTEM_AUDIO_SET.add(integer11);
    DEVICE_OUT_ALL_HDMI_SYSTEM_AUDIO_SET.add(integer13);
    DEVICE_ALL_HDMI_SYSTEM_AUDIO_AND_SPEAKER_SET = hashSet4 = new HashSet<>();
    hashSet4.addAll(DEVICE_OUT_ALL_HDMI_SYSTEM_AUDIO_SET);
    DEVICE_ALL_HDMI_SYSTEM_AUDIO_AND_SPEAKER_SET.add(integer2);
    HashSet<Integer> hashSet1 = new HashSet();
    hashSet1.add(Integer.valueOf(-2147483647));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483646));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483644));
    Set<Integer> set1 = DEVICE_IN_ALL_SET;
    Integer integer3 = Integer.valueOf(-2147483640);
    set1.add(integer3);
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483632));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483616));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483584));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483520));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483392));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147483136));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147482624));
    Set<Integer> set3 = DEVICE_IN_ALL_SET;
    Integer integer1 = Integer.valueOf(-2147481600);
    set3.add(integer1);
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147479552));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147475456));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147467264));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147450880));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147418112));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147352576));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2147221504));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2146959360));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2146435072));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2130706432));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2113929216));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2080374784));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-2013265920));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-1879048192));
    DEVICE_IN_ALL_SET.add(Integer.valueOf(-1073741824));
    DEVICE_IN_ALL_SCO_SET = set3 = new HashSet<>();
    set3.add(integer3);
    HashSet<Integer> hashSet3 = new HashSet();
    hashSet3.add(integer1);
    DEVICE_IN_ALL_USB_SET.add(Integer.valueOf(-2147479552));
    DEVICE_IN_ALL_USB_SET.add(Integer.valueOf(-2113929216));
    DEFAULT_STREAM_VOLUME = new int[] { 
        5, 14, 14, 10, 12, 11, 8, 16, 12, 12, 
        11, 11 };
  }
  
  public static String deviceStateToString(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unknown state (");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      } 
      return "DEVICE_STATE_AVAILABLE";
    } 
    return "DEVICE_STATE_UNAVAILABLE";
  }
  
  public static String getOutputDeviceName(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        switch (paramInt) {
          default:
            return Integer.toString(paramInt);
          case 134217728:
            return "hearing_aid_out";
          case 67108864:
            return "usb_headset";
          case 33554432:
            return "proxy";
          case 16777216:
            return "bus";
          case 8388608:
            return "ip";
          case 4194304:
            return "speaker_safe";
          case 2097152:
            return "aux_line";
          case 1048576:
            return "fm_transmitter";
          case 524288:
            return "spdif";
          case 262144:
            return "hmdi_arc";
          case 131072:
            return "line";
          case 65536:
            return "telephony_tx";
          case 32768:
            return "remote_submix";
          case 16384:
            return "usb_device";
          case 8192:
            return "usb_accessory";
          case 4096:
            return "digital_dock";
          case 2048:
            return "analog_dock";
          case 1024:
            return "hdmi";
          case 512:
            return "bt_a2dp_spk";
          case 256:
            return "bt_a2dp_hp";
          case 128:
            return "bt_a2dp";
          case 64:
            return "bt_sco_carkit";
          case 32:
            return "bt_sco_hs";
          case 16:
            return "bt_sco";
          case 8:
            return "headphone";
          case 4:
            break;
        } 
        return "headset";
      } 
      return "speaker";
    } 
    return "earpiece";
  }
  
  public static String getInputDeviceName(int paramInt) {
    switch (paramInt) {
      default:
        return Integer.toString(paramInt);
      case -1879048192:
        return "echo_reference";
      case -2013265920:
        return "hdmi_arc";
      case -2080374784:
        return "bt_ble";
      case -2113929216:
        return "usb_headset";
      case -2130706432:
        return "proxy";
      case -2146435072:
        return "bus";
      case -2146959360:
        return "ip";
      case -2147221504:
        return "loopback";
      case -2147352576:
        return "bt_a2dp";
      case -2147418112:
        return "spdif";
      case -2147450880:
        return "line";
      case -2147467264:
        return "tv_tuner";
      case -2147475456:
        return "fm_tuner";
      case -2147479552:
        return "usb_device";
      case -2147481600:
        return "usb_accessory";
      case -2147482624:
        return "digital_dock";
      case -2147483136:
        return "analog_dock";
      case -2147483392:
        return "remote_submix";
      case -2147483520:
        return "back_mic";
      case -2147483584:
        return "telephony_rx";
      case -2147483616:
        return "aux_digital";
      case -2147483632:
        return "headset";
      case -2147483640:
        return "bt_sco_hs";
      case -2147483644:
        return "mic";
      case -2147483646:
        return "ambient";
      case -2147483647:
        break;
    } 
    return "communication";
  }
  
  public static String getDeviceName(int paramInt) {
    if ((Integer.MIN_VALUE & paramInt) != 0)
      return getInputDeviceName(paramInt); 
    return getOutputDeviceName(paramInt);
  }
  
  public static String forceUseConfigToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unknown config (");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 15:
        return "FORCE_ENCODED_SURROUND_MANUAL";
      case 14:
        return "FORCE_ENCODED_SURROUND_ALWAYS";
      case 13:
        return "FORCE_ENCODED_SURROUND_NEVER";
      case 12:
        return "FORCE_HDMI_SYSTEM_AUDIO_ENFORCED";
      case 11:
        return "FORCE_SYSTEM_ENFORCED";
      case 10:
        return "FORCE_NO_BT_A2DP";
      case 9:
        return "FORCE_DIGITAL_DOCK";
      case 8:
        return "FORCE_ANALOG_DOCK";
      case 7:
        return "FORCE_BT_DESK_DOCK";
      case 6:
        return "FORCE_BT_CAR_DOCK";
      case 5:
        return "FORCE_WIRED_ACCESSORY";
      case 4:
        return "FORCE_BT_A2DP";
      case 3:
        return "FORCE_BT_SCO";
      case 2:
        return "FORCE_HEADPHONES";
      case 1:
        return "FORCE_SPEAKER";
      case 0:
        break;
    } 
    return "FORCE_NONE";
  }
  
  public static String forceUseUsageToString(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("unknown usage (");
        stringBuilder.append(paramInt);
        stringBuilder.append(")");
        return stringBuilder.toString();
      case 7:
        return "FOR_VIBRATE_RINGING";
      case 6:
        return "FOR_ENCODED_SURROUND";
      case 5:
        return "FOR_HDMI_SYSTEM_AUDIO";
      case 4:
        return "FOR_SYSTEM";
      case 3:
        return "FOR_DOCK";
      case 2:
        return "FOR_RECORD";
      case 1:
        return "FOR_MEDIA";
      case 0:
        break;
    } 
    return "FOR_COMMUNICATION";
  }
  
  public static int setStreamVolumeIndexAS(int paramInt1, int paramInt2, int paramInt3) {
    return setStreamVolumeIndex(paramInt1, paramInt2, paramInt3);
  }
  
  public static int setPhoneState(int paramInt) {
    Log.w("AudioSystem", "Do not use this method! Use AudioManager.setMode() instead.");
    return 0;
  }
  
  public static ArrayList<AudioDeviceAttributes> getDevicesForAttributes(AudioAttributes paramAudioAttributes) {
    StringBuilder stringBuilder;
    Objects.requireNonNull(paramAudioAttributes);
    AudioDeviceAttributes[] arrayOfAudioDeviceAttributes = new AudioDeviceAttributes[4];
    int i = getDevicesForAttributes(paramAudioAttributes, arrayOfAudioDeviceAttributes);
    ArrayList<AudioDeviceAttributes> arrayList = new ArrayList();
    if (i != 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("error ");
      stringBuilder.append(i);
      stringBuilder.append(" in getDevicesForAttributes for ");
      stringBuilder.append(paramAudioAttributes);
      Log.e("AudioSystem", stringBuilder.toString());
      return arrayList;
    } 
    for (int j = stringBuilder.length; i < j; ) {
      StringBuilder stringBuilder1 = stringBuilder[i];
      if (stringBuilder1 != null)
        arrayList.add(stringBuilder1); 
      i++;
    } 
    return arrayList;
  }
  
  static boolean isOffloadSupported(AudioFormat paramAudioFormat, AudioAttributes paramAudioAttributes) {
    int i = paramAudioFormat.getEncoding(), j = paramAudioFormat.getSampleRate();
    int k = paramAudioFormat.getChannelMask(), m = paramAudioFormat.getChannelIndexMask();
    int n = paramAudioAttributes.getVolumeControlStream();
    return native_is_offload_supported(i, j, k, m, n);
  }
  
  public static int setPreferredDeviceForStrategy(int paramInt, AudioDeviceAttributes paramAudioDeviceAttributes) {
    int i = AudioDeviceInfo.convertDeviceTypeToInternalDevice(paramAudioDeviceAttributes.getType());
    String str = paramAudioDeviceAttributes.getAddress();
    return setPreferredDeviceForStrategy(paramInt, i, str);
  }
  
  public static int getValueForVibrateSetting(int paramInt1, int paramInt2, int paramInt3) {
    return paramInt1 & (3 << paramInt2 * 2 ^ 0xFFFFFFFF) | (paramInt3 & 0x3) << paramInt2 * 2;
  }
  
  public static int getDefaultStreamVolume(int paramInt) {
    return DEFAULT_STREAM_VOLUME[paramInt];
  }
  
  public static String streamToString(int paramInt) {
    if (paramInt >= 0) {
      String[] arrayOfString = STREAM_NAMES;
      if (paramInt < arrayOfString.length)
        return arrayOfString[paramInt]; 
    } 
    if (paramInt == Integer.MIN_VALUE)
      return "USE_DEFAULT_STREAM_TYPE"; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("UNKNOWN_STREAM_");
    stringBuilder.append(paramInt);
    return stringBuilder.toString();
  }
  
  public static int getPlatformType(Context paramContext) {
    TelephonyManager telephonyManager = (TelephonyManager)paramContext.getSystemService("phone");
    if (telephonyManager.isVoiceCapable())
      return 1; 
    if (paramContext.getPackageManager().hasSystemFeature("android.software.leanback"))
      return 2; 
    return 0;
  }
  
  public static boolean isSingleVolume(Context paramContext) {
    null = paramContext.getResources().getBoolean(17891529);
    return (getPlatformType(paramContext) == 2 || null);
  }
  
  public static Set<Integer> generateAudioDeviceTypesSet(int paramInt) {
    Set<Integer> set;
    HashSet<Integer> hashSet = new HashSet();
    if ((Integer.MIN_VALUE & paramInt) == 0) {
      set = DEVICE_OUT_ALL_SET;
    } else {
      set = DEVICE_IN_ALL_SET;
    } 
    for (Iterator<Integer> iterator = set.iterator(); iterator.hasNext(); ) {
      int i = ((Integer)iterator.next()).intValue();
      if ((paramInt & i) == i)
        hashSet.add(Integer.valueOf(i)); 
    } 
    return hashSet;
  }
  
  public static Set<Integer> intersectionAudioDeviceTypes(Set<Integer> paramSet1, Set<Integer> paramSet2) {
    paramSet1 = new HashSet<>(paramSet1);
    paramSet1.retainAll(paramSet2);
    return paramSet1;
  }
  
  public static boolean isSingleAudioDeviceType(Set<Integer> paramSet, int paramInt) {
    int i = paramSet.size();
    boolean bool = true;
    if (i != 1 || !paramSet.contains(Integer.valueOf(paramInt)))
      bool = false; 
    return bool;
  }
  
  public static native int checkAudioFlinger();
  
  public static native int createAudioPatch(AudioPatch[] paramArrayOfAudioPatch, AudioPortConfig[] paramArrayOfAudioPortConfig1, AudioPortConfig[] paramArrayOfAudioPortConfig2);
  
  public static native int getAudioHwSyncForSession(int paramInt);
  
  public static native int getDeviceConnectionState(int paramInt, String paramString);
  
  private static native int getDevicesForAttributes(AudioAttributes paramAudioAttributes, AudioDeviceAttributes[] paramArrayOfAudioDeviceAttributes);
  
  public static native int getDevicesForStream(int paramInt);
  
  public static native int getForceUse(int paramInt);
  
  public static native int getHwOffloadEncodingFormatsSupportedForA2DP(ArrayList<Integer> paramArrayList);
  
  public static native float getMasterBalance();
  
  public static native boolean getMasterMono();
  
  public static native boolean getMasterMute();
  
  public static native float getMasterVolume();
  
  public static native int getMaxVolumeIndexForAttributes(AudioAttributes paramAudioAttributes);
  
  public static native int getMicrophones(ArrayList<MicrophoneInfo> paramArrayList);
  
  public static native int getMinVolumeIndexForAttributes(AudioAttributes paramAudioAttributes);
  
  public static native int getOutputLatency(int paramInt);
  
  public static native String getParameters(String paramString);
  
  public static native int getPreferredDeviceForStrategy(int paramInt, AudioDeviceAttributes[] paramArrayOfAudioDeviceAttributes);
  
  public static native int getPrimaryOutputFrameCount();
  
  public static native int getPrimaryOutputSamplingRate();
  
  public static native float getStreamVolumeDB(int paramInt1, int paramInt2, int paramInt3);
  
  public static native int getStreamVolumeIndex(int paramInt1, int paramInt2);
  
  public static native int getSurroundFormats(Map<Integer, Boolean> paramMap, boolean paramBoolean);
  
  public static native int getVolumeIndexForAttributes(AudioAttributes paramAudioAttributes, int paramInt);
  
  public static native int handleDeviceConfigChange(int paramInt1, String paramString1, String paramString2, int paramInt2);
  
  public static native int initStreamVolume(int paramInt1, int paramInt2, int paramInt3);
  
  public static native boolean isCallScreeningModeSupported();
  
  public static native boolean isHapticPlaybackSupported();
  
  public static native boolean isMicrophoneMuted();
  
  public static native boolean isSourceActive(int paramInt);
  
  public static native boolean isStreamActive(int paramInt1, int paramInt2);
  
  public static native boolean isStreamActiveRemotely(int paramInt1, int paramInt2);
  
  public static native int listAudioPatches(ArrayList<AudioPatch> paramArrayList, int[] paramArrayOfint);
  
  public static native int listAudioPorts(ArrayList<AudioPort> paramArrayList, int[] paramArrayOfint);
  
  public static native int muteMicrophone(boolean paramBoolean);
  
  private static native int native_get_FCC_8();
  
  private static native boolean native_is_offload_supported(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5);
  
  private static final native void native_register_dynamic_policy_callback();
  
  private static final native void native_register_recording_callback();
  
  public static native int newAudioPlayerId();
  
  public static native int newAudioRecorderId();
  
  public static native int newAudioSessionId();
  
  public static native int registerPolicyMixes(ArrayList<AudioMix> paramArrayList, boolean paramBoolean);
  
  public static native int releaseAudioPatch(AudioPatch paramAudioPatch);
  
  public static native int removePreferredDeviceForStrategy(int paramInt);
  
  public static native int removeUidDeviceAffinities(int paramInt);
  
  public static native int removeUserIdDeviceAffinities(int paramInt);
  
  public static native int setA11yServicesUids(int[] paramArrayOfint);
  
  public static native int setAllowedCapturePolicy(int paramInt1, int paramInt2);
  
  public static native int setAssistantUid(int paramInt);
  
  public static native int setAudioHalPids(int[] paramArrayOfint);
  
  public static native int setAudioPortConfig(AudioPortConfig paramAudioPortConfig);
  
  public static native int setCurrentImeUid(int paramInt);
  
  public static native int setDeviceConnectionState(int paramInt1, int paramInt2, String paramString1, String paramString2, int paramInt3);
  
  public static native int setForceUse(int paramInt1, int paramInt2);
  
  public static native int setLowRamDevice(boolean paramBoolean, long paramLong);
  
  public static native int setMasterBalance(float paramFloat);
  
  public static native int setMasterMono(boolean paramBoolean);
  
  public static native int setMasterMute(boolean paramBoolean);
  
  public static native int setMasterVolume(float paramFloat);
  
  public static native int setParameters(String paramString);
  
  public static native int setPhoneState(int paramInt1, int paramInt2);
  
  private static native int setPreferredDeviceForStrategy(int paramInt1, int paramInt2, String paramString);
  
  public static native int setRttEnabled(boolean paramBoolean);
  
  private static native int setStreamVolumeIndex(int paramInt1, int paramInt2, int paramInt3);
  
  public static native int setSupportedSystemUsages(int[] paramArrayOfint);
  
  public static native int setSurroundFormatEnabled(int paramInt, boolean paramBoolean);
  
  public static native int setUidDeviceAffinities(int paramInt, int[] paramArrayOfint, String[] paramArrayOfString);
  
  public static native int setUserIdDeviceAffinities(int paramInt, int[] paramArrayOfint, String[] paramArrayOfString);
  
  public static native int setVolumeIndexForAttributes(AudioAttributes paramAudioAttributes, int paramInt1, int paramInt2);
  
  public static native int startAudioSource(AudioPortConfig paramAudioPortConfig, AudioAttributes paramAudioAttributes);
  
  public static native int stopAudioSource(int paramInt);
  
  public static native int systemReady();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AudioFormatNativeEnumForBtCodec {}
  
  public static interface AudioRecordingCallback {
    void onRecordingConfigurationChanged(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, boolean param1Boolean, int[] param1ArrayOfint, AudioEffect.Descriptor[] param1ArrayOfDescriptor1, AudioEffect.Descriptor[] param1ArrayOfDescriptor2, int param1Int7, String param1String);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface AudioSystemError {}
  
  public static interface DynamicPolicyCallback {
    void onDynamicPolicyMixStateUpdate(String param1String, int param1Int);
  }
  
  public static interface ErrorCallback {
    void onError(int param1Int);
  }
}
