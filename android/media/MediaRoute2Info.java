package android.media;

import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public final class MediaRoute2Info implements Parcelable {
  public static final int CONNECTION_STATE_CONNECTED = 2;
  
  public static final int CONNECTION_STATE_CONNECTING = 1;
  
  public static final int CONNECTION_STATE_DISCONNECTED = 0;
  
  public static final Parcelable.Creator<MediaRoute2Info> CREATOR = new Parcelable.Creator<MediaRoute2Info>() {
      public MediaRoute2Info createFromParcel(Parcel param1Parcel) {
        return new MediaRoute2Info(param1Parcel);
      }
      
      public MediaRoute2Info[] newArray(int param1Int) {
        return new MediaRoute2Info[param1Int];
      }
    };
  
  public static final String FEATURE_LIVE_AUDIO = "android.media.route.feature.LIVE_AUDIO";
  
  public static final String FEATURE_LIVE_VIDEO = "android.media.route.feature.LIVE_VIDEO";
  
  public static final String FEATURE_LOCAL_PLAYBACK = "android.media.route.feature.LOCAL_PLAYBACK";
  
  public static final String FEATURE_REMOTE_AUDIO_PLAYBACK = "android.media.route.feature.REMOTE_AUDIO_PLAYBACK";
  
  public static final String FEATURE_REMOTE_GROUP_PLAYBACK = "android.media.route.feature.REMOTE_GROUP_PLAYBACK";
  
  public static final String FEATURE_REMOTE_PLAYBACK = "android.media.route.feature.REMOTE_PLAYBACK";
  
  public static final String FEATURE_REMOTE_VIDEO_PLAYBACK = "android.media.route.feature.REMOTE_VIDEO_PLAYBACK";
  
  public static final int PLAYBACK_VOLUME_FIXED = 0;
  
  public static final int PLAYBACK_VOLUME_VARIABLE = 1;
  
  public static final int TYPE_BLUETOOTH_A2DP = 8;
  
  public static final int TYPE_BUILTIN_SPEAKER = 2;
  
  public static final int TYPE_DOCK = 13;
  
  public static final int TYPE_GROUP = 2000;
  
  public static final int TYPE_HDMI = 9;
  
  public static final int TYPE_HEARING_AID = 23;
  
  public static final int TYPE_REMOTE_SPEAKER = 1002;
  
  public static final int TYPE_REMOTE_TV = 1001;
  
  public static final int TYPE_UNKNOWN = 0;
  
  public static final int TYPE_USB_ACCESSORY = 12;
  
  public static final int TYPE_USB_DEVICE = 11;
  
  public static final int TYPE_USB_HEADSET = 22;
  
  public static final int TYPE_WIRED_HEADPHONES = 4;
  
  public static final int TYPE_WIRED_HEADSET = 3;
  
  final String mAddress;
  
  final String mClientPackageName;
  
  final int mConnectionState;
  
  final CharSequence mDescription;
  
  final Bundle mExtras;
  
  final List<String> mFeatures;
  
  final Uri mIconUri;
  
  final String mId;
  
  final boolean mIsSystem;
  
  final CharSequence mName;
  
  final String mProviderId;
  
  final int mType;
  
  final int mVolume;
  
  final int mVolumeHandling;
  
  final int mVolumeMax;
  
  MediaRoute2Info(Builder paramBuilder) {
    this.mId = paramBuilder.mId;
    this.mName = paramBuilder.mName;
    this.mFeatures = paramBuilder.mFeatures;
    this.mType = paramBuilder.mType;
    this.mIsSystem = paramBuilder.mIsSystem;
    this.mIconUri = paramBuilder.mIconUri;
    this.mDescription = paramBuilder.mDescription;
    this.mConnectionState = paramBuilder.mConnectionState;
    this.mClientPackageName = paramBuilder.mClientPackageName;
    this.mVolumeHandling = paramBuilder.mVolumeHandling;
    this.mVolumeMax = paramBuilder.mVolumeMax;
    this.mVolume = paramBuilder.mVolume;
    this.mAddress = paramBuilder.mAddress;
    this.mExtras = paramBuilder.mExtras;
    this.mProviderId = paramBuilder.mProviderId;
  }
  
  MediaRoute2Info(Parcel paramParcel) {
    this.mId = paramParcel.readString();
    this.mName = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mFeatures = paramParcel.createStringArrayList();
    this.mType = paramParcel.readInt();
    this.mIsSystem = paramParcel.readBoolean();
    this.mIconUri = paramParcel.<Uri>readParcelable(null);
    this.mDescription = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mConnectionState = paramParcel.readInt();
    this.mClientPackageName = paramParcel.readString();
    this.mVolumeHandling = paramParcel.readInt();
    this.mVolumeMax = paramParcel.readInt();
    this.mVolume = paramParcel.readInt();
    this.mAddress = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
    this.mProviderId = paramParcel.readString();
  }
  
  public String getId() {
    String str = this.mProviderId;
    if (str != null)
      return MediaRouter2Utils.toUniqueId(str, this.mId); 
    return this.mId;
  }
  
  public CharSequence getName() {
    return this.mName;
  }
  
  public List<String> getFeatures() {
    return this.mFeatures;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public boolean isSystemRoute() {
    return this.mIsSystem;
  }
  
  public Uri getIconUri() {
    return this.mIconUri;
  }
  
  public CharSequence getDescription() {
    return this.mDescription;
  }
  
  public int getConnectionState() {
    return this.mConnectionState;
  }
  
  public String getClientPackageName() {
    return this.mClientPackageName;
  }
  
  public int getVolumeHandling() {
    return this.mVolumeHandling;
  }
  
  public int getVolumeMax() {
    return this.mVolumeMax;
  }
  
  public int getVolume() {
    return this.mVolume;
  }
  
  public String getAddress() {
    return this.mAddress;
  }
  
  public Bundle getExtras() {
    Bundle bundle;
    if (this.mExtras == null) {
      bundle = null;
    } else {
      bundle = new Bundle(this.mExtras);
    } 
    return bundle;
  }
  
  public String getOriginalId() {
    return this.mId;
  }
  
  public String getProviderId() {
    return this.mProviderId;
  }
  
  public boolean hasAnyFeatures(Collection<String> paramCollection) {
    Objects.requireNonNull(paramCollection, "features must not be null");
    for (String str : paramCollection) {
      if (getFeatures().contains(str))
        return true; 
    } 
    return false;
  }
  
  public boolean isValid() {
    if (TextUtils.isEmpty(getId()) || TextUtils.isEmpty(getName()) || 
      TextUtils.isEmpty(getProviderId()))
      return false; 
    return true;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof MediaRoute2Info))
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mId, ((MediaRoute2Info)paramObject).mId)) {
      CharSequence charSequence1 = this.mName, charSequence2 = ((MediaRoute2Info)paramObject).mName;
      if (Objects.equals(charSequence1, charSequence2)) {
        List<String> list2 = this.mFeatures, list1 = ((MediaRoute2Info)paramObject).mFeatures;
        if (Objects.equals(list2, list1) && this.mType == ((MediaRoute2Info)paramObject).mType && this.mIsSystem == ((MediaRoute2Info)paramObject).mIsSystem) {
          Uri uri1 = this.mIconUri, uri2 = ((MediaRoute2Info)paramObject).mIconUri;
          if (Objects.equals(uri1, uri2)) {
            CharSequence charSequence4 = this.mDescription, charSequence3 = ((MediaRoute2Info)paramObject).mDescription;
            if (Objects.equals(charSequence4, charSequence3) && this.mConnectionState == ((MediaRoute2Info)paramObject).mConnectionState) {
              charSequence4 = this.mClientPackageName;
              charSequence3 = ((MediaRoute2Info)paramObject).mClientPackageName;
              if (Objects.equals(charSequence4, charSequence3) && this.mVolumeHandling == ((MediaRoute2Info)paramObject).mVolumeHandling && this.mVolumeMax == ((MediaRoute2Info)paramObject).mVolumeMax && this.mVolume == ((MediaRoute2Info)paramObject).mVolume) {
                charSequence3 = this.mAddress;
                charSequence4 = ((MediaRoute2Info)paramObject).mAddress;
                if (Objects.equals(charSequence3, charSequence4)) {
                  charSequence4 = this.mProviderId;
                  paramObject = ((MediaRoute2Info)paramObject).mProviderId;
                  if (Objects.equals(charSequence4, paramObject))
                    return null; 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    String str1 = this.mId;
    CharSequence charSequence1 = this.mName;
    List<String> list = this.mFeatures;
    int i = this.mType;
    boolean bool = this.mIsSystem;
    Uri uri = this.mIconUri;
    CharSequence charSequence2 = this.mDescription;
    int j = this.mConnectionState;
    String str2 = this.mClientPackageName;
    int k = this.mVolumeHandling, m = this.mVolumeMax, n = this.mVolume;
    String str3 = this.mAddress, str4 = this.mProviderId;
    return Objects.hash(new Object[] { 
          str1, charSequence1, list, Integer.valueOf(i), Boolean.valueOf(bool), uri, charSequence2, Integer.valueOf(j), str2, Integer.valueOf(k), 
          Integer.valueOf(m), Integer.valueOf(n), str3, str4 });
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaRoute2Info{ ");
    stringBuilder.append("id=");
    stringBuilder.append(getId());
    stringBuilder.append(", name=");
    stringBuilder.append(getName());
    stringBuilder.append(", features=");
    stringBuilder.append(getFeatures());
    stringBuilder.append(", iconUri=");
    stringBuilder.append(getIconUri());
    stringBuilder.append(", description=");
    stringBuilder.append(getDescription());
    stringBuilder.append(", connectionState=");
    stringBuilder.append(getConnectionState());
    stringBuilder.append(", clientPackageName=");
    stringBuilder.append(getClientPackageName());
    stringBuilder.append(", volumeHandling=");
    stringBuilder.append(getVolumeHandling());
    stringBuilder.append(", volumeMax=");
    stringBuilder.append(getVolumeMax());
    stringBuilder.append(", volume=");
    stringBuilder.append(getVolume());
    stringBuilder.append(", providerId=");
    stringBuilder.append(getProviderId());
    stringBuilder = stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mId);
    TextUtils.writeToParcel(this.mName, paramParcel, paramInt);
    paramParcel.writeStringList(this.mFeatures);
    paramParcel.writeInt(this.mType);
    paramParcel.writeBoolean(this.mIsSystem);
    paramParcel.writeParcelable(this.mIconUri, paramInt);
    TextUtils.writeToParcel(this.mDescription, paramParcel, paramInt);
    paramParcel.writeInt(this.mConnectionState);
    paramParcel.writeString(this.mClientPackageName);
    paramParcel.writeInt(this.mVolumeHandling);
    paramParcel.writeInt(this.mVolumeMax);
    paramParcel.writeInt(this.mVolume);
    paramParcel.writeString(this.mAddress);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeString(this.mProviderId);
  }
  
  class Builder {
    String mAddress;
    
    String mClientPackageName;
    
    int mConnectionState;
    
    CharSequence mDescription;
    
    Bundle mExtras;
    
    final List<String> mFeatures;
    
    Uri mIconUri;
    
    final String mId;
    
    boolean mIsSystem;
    
    final CharSequence mName;
    
    String mProviderId;
    
    int mType = 0;
    
    int mVolume;
    
    int mVolumeHandling = 0;
    
    int mVolumeMax;
    
    public Builder(CharSequence param1CharSequence) {
      if (!TextUtils.isEmpty((CharSequence)MediaRoute2Info.this)) {
        if (!TextUtils.isEmpty(param1CharSequence)) {
          this.mId = (String)MediaRoute2Info.this;
          this.mName = param1CharSequence;
          this.mFeatures = new ArrayList<>();
          return;
        } 
        throw new IllegalArgumentException("name must not be empty");
      } 
      throw new IllegalArgumentException("id must not be empty");
    }
    
    public Builder() {
      this(MediaRoute2Info.this.mId, MediaRoute2Info.this);
    }
    
    public Builder(MediaRoute2Info param1MediaRoute2Info) {
      if (!TextUtils.isEmpty((CharSequence)MediaRoute2Info.this)) {
        Objects.requireNonNull(param1MediaRoute2Info, "routeInfo must not be null");
        this.mId = (String)MediaRoute2Info.this;
        this.mName = param1MediaRoute2Info.mName;
        this.mFeatures = new ArrayList<>(param1MediaRoute2Info.mFeatures);
        this.mType = param1MediaRoute2Info.mType;
        this.mIsSystem = param1MediaRoute2Info.mIsSystem;
        this.mIconUri = param1MediaRoute2Info.mIconUri;
        this.mDescription = param1MediaRoute2Info.mDescription;
        this.mConnectionState = param1MediaRoute2Info.mConnectionState;
        this.mClientPackageName = param1MediaRoute2Info.mClientPackageName;
        this.mVolumeHandling = param1MediaRoute2Info.mVolumeHandling;
        this.mVolumeMax = param1MediaRoute2Info.mVolumeMax;
        this.mVolume = param1MediaRoute2Info.mVolume;
        this.mAddress = param1MediaRoute2Info.mAddress;
        if (param1MediaRoute2Info.mExtras != null)
          this.mExtras = new Bundle(param1MediaRoute2Info.mExtras); 
        this.mProviderId = param1MediaRoute2Info.mProviderId;
        return;
      } 
      throw new IllegalArgumentException("id must not be empty");
    }
    
    public Builder addFeature(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mFeatures.add(param1String);
        return this;
      } 
      throw new IllegalArgumentException("feature must not be null or empty");
    }
    
    public Builder addFeatures(Collection<String> param1Collection) {
      Objects.requireNonNull(param1Collection, "features must not be null");
      for (String str : param1Collection)
        addFeature(str); 
      return this;
    }
    
    public Builder clearFeatures() {
      this.mFeatures.clear();
      return this;
    }
    
    public Builder setType(int param1Int) {
      this.mType = param1Int;
      return this;
    }
    
    public Builder setSystemRoute(boolean param1Boolean) {
      this.mIsSystem = param1Boolean;
      return this;
    }
    
    public Builder setIconUri(Uri param1Uri) {
      this.mIconUri = param1Uri;
      return this;
    }
    
    public Builder setDescription(CharSequence param1CharSequence) {
      this.mDescription = param1CharSequence;
      return this;
    }
    
    public Builder setConnectionState(int param1Int) {
      this.mConnectionState = param1Int;
      return this;
    }
    
    public Builder setClientPackageName(String param1String) {
      this.mClientPackageName = param1String;
      return this;
    }
    
    public Builder setVolumeHandling(int param1Int) {
      this.mVolumeHandling = param1Int;
      return this;
    }
    
    public Builder setVolumeMax(int param1Int) {
      this.mVolumeMax = param1Int;
      return this;
    }
    
    public Builder setVolume(int param1Int) {
      this.mVolume = param1Int;
      return this;
    }
    
    public Builder setAddress(String param1String) {
      this.mAddress = param1String;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      if (param1Bundle == null) {
        this.mExtras = null;
        return this;
      } 
      this.mExtras = new Bundle(param1Bundle);
      return this;
    }
    
    public Builder setProviderId(String param1String) {
      if (!TextUtils.isEmpty(param1String)) {
        this.mProviderId = param1String;
        return this;
      } 
      throw new IllegalArgumentException("providerId must not be null or empty");
    }
    
    public MediaRoute2Info build() {
      if (!this.mFeatures.isEmpty())
        return new MediaRoute2Info(this); 
      throw new IllegalArgumentException("features must not be empty!");
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ConnectionState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PlaybackVolume implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
