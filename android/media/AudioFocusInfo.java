package android.media;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.util.Objects;

@SystemApi
public final class AudioFocusInfo implements Parcelable {
  public AudioFocusInfo(AudioAttributes paramAudioAttributes, int paramInt1, String paramString1, String paramString2, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    String str1;
    this.mGenCount = -1L;
    if (paramAudioAttributes == null)
      paramAudioAttributes = (new AudioAttributes.Builder()).build(); 
    this.mAttributes = paramAudioAttributes;
    this.mClientUid = paramInt1;
    String str2 = "";
    if (paramString1 == null) {
      str1 = "";
    } else {
      str1 = paramString1;
    } 
    this.mClientId = str1;
    if (paramString2 == null)
      paramString2 = str2; 
    this.mPackageName = paramString2;
    this.mGainRequest = paramInt2;
    this.mLossReceived = paramInt3;
    this.mFlags = paramInt4;
    this.mSdkTarget = paramInt5;
  }
  
  public void setGen(long paramLong) {
    this.mGenCount = paramLong;
  }
  
  public long getGen() {
    return this.mGenCount;
  }
  
  public AudioAttributes getAttributes() {
    return this.mAttributes;
  }
  
  public int getClientUid() {
    return this.mClientUid;
  }
  
  public String getClientId() {
    return this.mClientId;
  }
  
  public String getPackageName() {
    return this.mPackageName;
  }
  
  public int getGainRequest() {
    return this.mGainRequest;
  }
  
  public int getLossReceived() {
    return this.mLossReceived;
  }
  
  public int getSdkTarget() {
    return this.mSdkTarget;
  }
  
  public void clearLossReceived() {
    this.mLossReceived = 0;
  }
  
  public int getFlags() {
    return this.mFlags;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mAttributes.writeToParcel(paramParcel, paramInt);
    paramParcel.writeInt(this.mClientUid);
    paramParcel.writeString(this.mClientId);
    paramParcel.writeString(this.mPackageName);
    paramParcel.writeInt(this.mGainRequest);
    paramParcel.writeInt(this.mLossReceived);
    paramParcel.writeInt(this.mFlags);
    paramParcel.writeInt(this.mSdkTarget);
    paramParcel.writeLong(this.mGenCount);
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mAttributes, Integer.valueOf(this.mClientUid), this.mClientId, this.mPackageName, Integer.valueOf(this.mGainRequest), Integer.valueOf(this.mFlags) });
  }
  
  public boolean equals(Object paramObject) {
    if (this == paramObject)
      return true; 
    if (paramObject == null)
      return false; 
    if (getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (!this.mAttributes.equals(((AudioFocusInfo)paramObject).mAttributes))
      return false; 
    if (this.mClientUid != ((AudioFocusInfo)paramObject).mClientUid)
      return false; 
    if (!this.mClientId.equals(((AudioFocusInfo)paramObject).mClientId))
      return false; 
    if (!this.mPackageName.equals(((AudioFocusInfo)paramObject).mPackageName))
      return false; 
    if (this.mGainRequest != ((AudioFocusInfo)paramObject).mGainRequest)
      return false; 
    if (this.mLossReceived != ((AudioFocusInfo)paramObject).mLossReceived)
      return false; 
    if (this.mFlags != ((AudioFocusInfo)paramObject).mFlags)
      return false; 
    if (this.mSdkTarget != ((AudioFocusInfo)paramObject).mSdkTarget)
      return false; 
    return true;
  }
  
  public static final Parcelable.Creator<AudioFocusInfo> CREATOR = new Parcelable.Creator<AudioFocusInfo>() {
      public AudioFocusInfo createFromParcel(Parcel param1Parcel) {
        Parcelable.Creator<AudioAttributes> creator = AudioAttributes.CREATOR;
        AudioAttributes audioAttributes = creator.createFromParcel(param1Parcel);
        int i = param1Parcel.readInt();
        String str1 = param1Parcel.readString();
        String str2 = param1Parcel.readString();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        AudioFocusInfo audioFocusInfo = new AudioFocusInfo(audioAttributes, i, str1, str2, j, k, m, param1Parcel.readInt());
        audioFocusInfo.setGen(param1Parcel.readLong());
        return audioFocusInfo;
      }
      
      public AudioFocusInfo[] newArray(int param1Int) {
        return new AudioFocusInfo[param1Int];
      }
    };
  
  private final AudioAttributes mAttributes;
  
  private final String mClientId;
  
  private final int mClientUid;
  
  private int mFlags;
  
  private int mGainRequest;
  
  private long mGenCount;
  
  private int mLossReceived;
  
  private final String mPackageName;
  
  private final int mSdkTarget;
}
