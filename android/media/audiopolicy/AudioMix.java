package android.media.audiopolicy;

import android.annotation.SystemApi;
import android.media.AudioDeviceInfo;
import android.media.AudioFormat;
import android.media.AudioSystem;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public class AudioMix {
  private AudioMixingRule mRule;
  
  private int mRouteFlags;
  
  private int mMixType = -1;
  
  int mMixState = -1;
  
  private AudioFormat mFormat;
  
  final int mDeviceSystemType;
  
  String mDeviceAddress;
  
  int mCallbackFlags;
  
  private static final int ROUTE_FLAG_SUPPORTED = 3;
  
  public static final int ROUTE_FLAG_RENDER = 1;
  
  public static final int ROUTE_FLAG_LOOP_BACK_RENDER = 3;
  
  public static final int ROUTE_FLAG_LOOP_BACK = 2;
  
  private static final int PRIVILEDGED_CAPTURE_MAX_SAMPLE_RATE = 16000;
  
  private static final int PRIVILEDGED_CAPTURE_MAX_CHANNEL_NUMBER = 1;
  
  private static final int PRIVILEDGED_CAPTURE_MAX_BYTES_PER_SAMPLE = 2;
  
  public static final int MIX_TYPE_RECORDERS = 1;
  
  public static final int MIX_TYPE_PLAYERS = 0;
  
  public static final int MIX_TYPE_INVALID = -1;
  
  public static final int MIX_STATE_MIXING = 1;
  
  public static final int MIX_STATE_IDLE = 0;
  
  public static final int MIX_STATE_DISABLED = -1;
  
  public static final int CALLBACK_FLAG_NOTIFY_ACTIVITY = 1;
  
  private static final int CALLBACK_FLAGS_ALL = 1;
  
  private AudioMix(AudioMixingRule paramAudioMixingRule, AudioFormat paramAudioFormat, int paramInt1, int paramInt2, int paramInt3, String paramString) {
    this.mRule = paramAudioMixingRule;
    this.mFormat = paramAudioFormat;
    this.mRouteFlags = paramInt1;
    this.mMixType = paramAudioMixingRule.getTargetMixType();
    this.mCallbackFlags = paramInt2;
    this.mDeviceSystemType = paramInt3;
    if (paramString == null)
      paramString = new String(""); 
    this.mDeviceAddress = paramString;
  }
  
  public int getMixState() {
    return this.mMixState;
  }
  
  public int getRouteFlags() {
    return this.mRouteFlags;
  }
  
  public AudioFormat getFormat() {
    return this.mFormat;
  }
  
  public AudioMixingRule getRule() {
    return this.mRule;
  }
  
  public int getMixType() {
    return this.mMixType;
  }
  
  void setRegistration(String paramString) {
    this.mDeviceAddress = paramString;
  }
  
  public String getRegistration() {
    return this.mDeviceAddress;
  }
  
  public boolean isAffectingUsage(int paramInt) {
    return this.mRule.isAffectingUsage(paramInt);
  }
  
  public boolean containsMatchAttributeRuleForUsage(int paramInt) {
    return this.mRule.containsMatchAttributeRuleForUsage(paramInt);
  }
  
  public boolean isRoutedToDevice(int paramInt, String paramString) {
    if ((this.mRouteFlags & 0x1) != 1)
      return false; 
    if (paramInt != this.mDeviceSystemType)
      return false; 
    if (!paramString.equals(this.mDeviceAddress))
      return false; 
    return true;
  }
  
  public static String canBeUsedForPrivilegedCapture(AudioFormat paramAudioFormat) {
    StringBuilder stringBuilder;
    int i = paramAudioFormat.getSampleRate();
    if (i > 16000 || i <= 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Privileged audio capture sample rate ");
      stringBuilder.append(i);
      stringBuilder.append(" can not be over ");
      stringBuilder.append(16000);
      stringBuilder.append("kHz");
      return stringBuilder.toString();
    } 
    i = stringBuilder.getChannelCount();
    if (i > 1 || i <= 0) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Privileged audio capture channel count ");
      stringBuilder.append(i);
      stringBuilder.append(" can not be over ");
      stringBuilder.append(1);
      return stringBuilder.toString();
    } 
    i = stringBuilder.getEncoding();
    if (!AudioFormat.isPublicEncoding(i) || !AudioFormat.isEncodingLinearPcm(i)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Privileged audio capture encoding ");
      stringBuilder.append(i);
      stringBuilder.append("is not linear");
      return stringBuilder.toString();
    } 
    if (AudioFormat.getBytesPerSample(i) > 2) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("Privileged audio capture encoding ");
      stringBuilder.append(i);
      stringBuilder.append(" can not be over ");
      stringBuilder.append(2);
      stringBuilder.append(" bytes per sample");
      return stringBuilder.toString();
    } 
    return null;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mRouteFlags != ((AudioMix)paramObject).mRouteFlags || this.mRule != ((AudioMix)paramObject).mRule || this.mMixType != ((AudioMix)paramObject).mMixType || this.mFormat != ((AudioMix)paramObject).mFormat)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRouteFlags), this.mRule, Integer.valueOf(this.mMixType), this.mFormat });
  }
  
  public static class Builder {
    private AudioMixingRule mRule = null;
    
    private AudioFormat mFormat = null;
    
    private int mRouteFlags = 0;
    
    private int mCallbackFlags = 0;
    
    private int mDeviceSystemType = 0;
    
    private String mDeviceAddress = null;
    
    public Builder(AudioMixingRule param1AudioMixingRule) throws IllegalArgumentException {
      if (param1AudioMixingRule != null) {
        this.mRule = param1AudioMixingRule;
        return;
      } 
      throw new IllegalArgumentException("Illegal null AudioMixingRule argument");
    }
    
    Builder setMixingRule(AudioMixingRule param1AudioMixingRule) throws IllegalArgumentException {
      if (param1AudioMixingRule != null) {
        this.mRule = param1AudioMixingRule;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioMixingRule argument");
    }
    
    Builder setCallbackFlags(int param1Int) throws IllegalArgumentException {
      if (param1Int == 0 || (param1Int & 0x1) != 0) {
        this.mCallbackFlags = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Illegal callback flags 0x");
      stringBuilder.append(Integer.toHexString(param1Int).toUpperCase());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    Builder setDevice(int param1Int, String param1String) {
      this.mDeviceSystemType = param1Int;
      this.mDeviceAddress = param1String;
      return this;
    }
    
    public Builder setFormat(AudioFormat param1AudioFormat) throws IllegalArgumentException {
      if (param1AudioFormat != null) {
        this.mFormat = param1AudioFormat;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioFormat argument");
    }
    
    public Builder setRouteFlags(int param1Int) throws IllegalArgumentException {
      if (param1Int != 0) {
        if ((param1Int & 0x3) != 0) {
          if ((param1Int & 0xFFFFFFFC) == 0) {
            this.mRouteFlags = param1Int;
            return this;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Unknown route flags 0x");
          stringBuilder1.append(Integer.toHexString(param1Int));
          stringBuilder1.append("when configuring an AudioMix");
          throw new IllegalArgumentException(stringBuilder1.toString());
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid route flags 0x");
        stringBuilder.append(Integer.toHexString(param1Int));
        stringBuilder.append("when configuring an AudioMix");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new IllegalArgumentException("Illegal empty route flags");
    }
    
    public Builder setDevice(AudioDeviceInfo param1AudioDeviceInfo) throws IllegalArgumentException {
      if (param1AudioDeviceInfo != null) {
        if (param1AudioDeviceInfo.isSink()) {
          this.mDeviceSystemType = AudioDeviceInfo.convertDeviceTypeToInternalDevice(param1AudioDeviceInfo.getType());
          this.mDeviceAddress = param1AudioDeviceInfo.getAddress();
          return this;
        } 
        throw new IllegalArgumentException("Unsupported device type on mix, not a sink");
      } 
      throw new IllegalArgumentException("Illegal null AudioDeviceInfo argument");
    }
    
    public AudioMix build() throws IllegalArgumentException {
      if (this.mRule != null) {
        if (this.mRouteFlags == 0)
          this.mRouteFlags = 2; 
        if (this.mFormat == null) {
          int j = AudioSystem.getPrimaryOutputSamplingRate();
          int k = j;
          if (j <= 0)
            k = 44100; 
          this.mFormat = (new AudioFormat.Builder()).setSampleRate(k).build();
        } 
        int i = this.mDeviceSystemType;
        if (i != 0 && i != 32768 && i != -2147483392) {
          if ((this.mRouteFlags & 0x1) != 0) {
            if (this.mRule.getTargetMixType() != 0)
              throw new IllegalArgumentException("Unsupported device on non-playback mix"); 
          } else {
            throw new IllegalArgumentException("Can't have audio device without flag ROUTE_FLAG_RENDER");
          } 
        } else {
          i = this.mRouteFlags;
          if ((i & 0x3) != 1) {
            if ((i & 0x2) == 2)
              if (this.mRule.getTargetMixType() == 0) {
                this.mDeviceSystemType = 32768;
              } else if (this.mRule.getTargetMixType() == 1) {
                this.mDeviceSystemType = -2147483392;
              } else {
                throw new IllegalArgumentException("Unknown mixing rule type");
              }  
          } else {
            throw new IllegalArgumentException("Can't have flag ROUTE_FLAG_RENDER without an audio device");
          } 
        } 
        if (this.mRule.allowPrivilegedPlaybackCapture()) {
          String str = AudioMix.canBeUsedForPrivilegedCapture(this.mFormat);
          if (str != null)
            throw new IllegalArgumentException(str); 
        } 
        return new AudioMix(this.mRule, this.mFormat, this.mRouteFlags, this.mCallbackFlags, this.mDeviceSystemType, this.mDeviceAddress);
      } 
      throw new IllegalArgumentException("Illegal null AudioMixingRule");
    }
    
    Builder() {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RouteFlags {}
}
