package android.media.audiopolicy;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioDeviceInfo;
import android.media.AudioFocusInfo;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.IAudioService;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import android.util.Slog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@SystemApi
public class AudioPolicy {
  private static final boolean DEBUG = false;
  
  public static final int FOCUS_POLICY_DUCKING_DEFAULT = 0;
  
  public static final int FOCUS_POLICY_DUCKING_IN_APP = 0;
  
  public static final int FOCUS_POLICY_DUCKING_IN_POLICY = 1;
  
  private static final int MSG_FOCUS_ABANDON = 5;
  
  private static final int MSG_FOCUS_GRANT = 1;
  
  private static final int MSG_FOCUS_LOSS = 2;
  
  private static final int MSG_FOCUS_REQUEST = 4;
  
  private static final int MSG_MIX_STATE_UPDATE = 3;
  
  private static final int MSG_POLICY_STATUS_CHANGE = 0;
  
  private static final int MSG_VOL_ADJUST = 6;
  
  public static final int POLICY_STATUS_REGISTERED = 2;
  
  public static final int POLICY_STATUS_UNREGISTERED = 1;
  
  private static final String TAG = "AudioPolicy";
  
  private static IAudioService sService;
  
  private ArrayList<WeakReference<AudioRecord>> mCaptors;
  
  private AudioPolicyConfig mConfig;
  
  private Context mContext;
  
  private final EventHandler mEventHandler;
  
  private AudioPolicyFocusListener mFocusListener;
  
  private ArrayList<WeakReference<AudioTrack>> mInjectors;
  
  private boolean mIsFocusPolicy;
  
  private boolean mIsTestFocusPolicy;
  
  private final Object mLock = new Object();
  
  private final IAudioPolicyCallback mPolicyCb;
  
  private final MediaProjection mProjection;
  
  private String mRegistrationId;
  
  private int mStatus;
  
  private AudioPolicyStatusListener mStatusListener;
  
  private final AudioPolicyVolumeCallback mVolCb;
  
  public AudioPolicyConfig getConfig() {
    return this.mConfig;
  }
  
  public boolean hasFocusListener() {
    boolean bool;
    if (this.mFocusListener != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean isFocusPolicy() {
    return this.mIsFocusPolicy;
  }
  
  public boolean isTestFocusPolicy() {
    return this.mIsTestFocusPolicy;
  }
  
  public boolean isVolumeController() {
    boolean bool;
    if (this.mVolCb != null) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public MediaProjection getMediaProjection() {
    return this.mProjection;
  }
  
  public static class Builder {
    private Context mContext;
    
    private AudioPolicy.AudioPolicyFocusListener mFocusListener;
    
    private boolean mIsFocusPolicy = false;
    
    private boolean mIsTestFocusPolicy = false;
    
    private Looper mLooper;
    
    private ArrayList<AudioMix> mMixes;
    
    private MediaProjection mProjection;
    
    private AudioPolicy.AudioPolicyStatusListener mStatusListener;
    
    private AudioPolicy.AudioPolicyVolumeCallback mVolCb;
    
    public Builder(Context param1Context) {
      this.mMixes = new ArrayList<>();
      this.mContext = param1Context;
    }
    
    public Builder addMix(AudioMix param1AudioMix) throws IllegalArgumentException {
      if (param1AudioMix != null) {
        this.mMixes.add(param1AudioMix);
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioMix argument");
    }
    
    public Builder setLooper(Looper param1Looper) throws IllegalArgumentException {
      if (param1Looper != null) {
        this.mLooper = param1Looper;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null Looper argument");
    }
    
    public void setAudioPolicyFocusListener(AudioPolicy.AudioPolicyFocusListener param1AudioPolicyFocusListener) {
      this.mFocusListener = param1AudioPolicyFocusListener;
    }
    
    public Builder setIsAudioFocusPolicy(boolean param1Boolean) {
      this.mIsFocusPolicy = param1Boolean;
      return this;
    }
    
    public Builder setIsTestFocusPolicy(boolean param1Boolean) {
      this.mIsTestFocusPolicy = param1Boolean;
      return this;
    }
    
    public void setAudioPolicyStatusListener(AudioPolicy.AudioPolicyStatusListener param1AudioPolicyStatusListener) {
      this.mStatusListener = param1AudioPolicyStatusListener;
    }
    
    public Builder setAudioPolicyVolumeCallback(AudioPolicy.AudioPolicyVolumeCallback param1AudioPolicyVolumeCallback) {
      if (param1AudioPolicyVolumeCallback != null) {
        this.mVolCb = param1AudioPolicyVolumeCallback;
        return this;
      } 
      throw new IllegalArgumentException("Invalid null volume callback");
    }
    
    public Builder setMediaProjection(MediaProjection param1MediaProjection) {
      if (param1MediaProjection != null) {
        this.mProjection = param1MediaProjection;
        return this;
      } 
      throw new IllegalArgumentException("Invalid null volume callback");
    }
    
    public AudioPolicy build() {
      if (this.mStatusListener != null)
        for (AudioMix audioMix : this.mMixes)
          audioMix.mCallbackFlags |= 0x1;  
      if (!this.mIsFocusPolicy || this.mFocusListener != null)
        return new AudioPolicy(new AudioPolicyConfig(this.mMixes), this.mContext, this.mLooper, this.mFocusListener, this.mStatusListener, this.mIsFocusPolicy, this.mIsTestFocusPolicy, this.mVolCb, this.mProjection); 
      throw new IllegalStateException("Cannot be a focus policy without an AudioPolicyFocusListener");
    }
  }
  
  public int attachMixes(List<AudioMix> paramList) {
    if (paramList != null)
      synchronized (this.mLock) {
        if (this.mStatus == 2) {
          ArrayList<AudioMix> arrayList = new ArrayList();
          this(paramList.size());
          for (Iterator<AudioMix> iterator = paramList.iterator(); iterator.hasNext(); ) {
            AudioMix audioMix = iterator.next();
            if (audioMix != null) {
              arrayList.add(audioMix);
              continue;
            } 
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            this("Illegal null AudioMix in attachMixes");
            throw illegalArgumentException;
          } 
          AudioPolicyConfig audioPolicyConfig = new AudioPolicyConfig();
          this(arrayList);
          IAudioService iAudioService = getService();
          try {
            int i = iAudioService.addMixForPolicy(audioPolicyConfig, cb());
            if (i == 0)
              this.mConfig.add(arrayList); 
            return i;
          } catch (RemoteException remoteException) {
            Log.e("AudioPolicy", "Dead object in attachMixes", (Throwable)remoteException);
            return -1;
          } 
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Cannot alter unregistered AudioPolicy");
        throw illegalStateException;
      }  
    throw new IllegalArgumentException("Illegal null list of AudioMix");
  }
  
  public int detachMixes(List<AudioMix> paramList) {
    if (paramList != null)
      synchronized (this.mLock) {
        if (this.mStatus == 2) {
          ArrayList<AudioMix> arrayList = new ArrayList();
          this(paramList.size());
          for (Iterator<AudioMix> iterator = paramList.iterator(); iterator.hasNext(); ) {
            AudioMix audioMix = iterator.next();
            if (audioMix != null) {
              arrayList.add(audioMix);
              continue;
            } 
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            this("Illegal null AudioMix in detachMixes");
            throw illegalArgumentException;
          } 
          AudioPolicyConfig audioPolicyConfig = new AudioPolicyConfig();
          this(arrayList);
          IAudioService iAudioService = getService();
          try {
            int i = iAudioService.removeMixForPolicy(audioPolicyConfig, cb());
            if (i == 0)
              this.mConfig.remove(arrayList); 
            return i;
          } catch (RemoteException remoteException) {
            Log.e("AudioPolicy", "Dead object in detachMixes", (Throwable)remoteException);
            return -1;
          } 
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Cannot alter unregistered AudioPolicy");
        throw illegalStateException;
      }  
    throw new IllegalArgumentException("Illegal null list of AudioMix");
  }
  
  @SystemApi
  public boolean setUidDeviceAffinity(int paramInt, List<AudioDeviceInfo> paramList) {
    if (paramList != null)
      synchronized (this.mLock) {
        if (this.mStatus == 2) {
          int[] arrayOfInt = new int[paramList.size()];
          String[] arrayOfString = new String[paramList.size()];
          byte b = 0;
          for (AudioDeviceInfo audioDeviceInfo : paramList) {
            if (audioDeviceInfo != null) {
              arrayOfInt[b] = AudioDeviceInfo.convertDeviceTypeToInternalDevice(audioDeviceInfo.getType());
              arrayOfString[b] = audioDeviceInfo.getAddress();
              b++;
              continue;
            } 
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            this("Illegal null AudioDeviceInfo in setUidDeviceAffinity");
            throw illegalArgumentException;
          } 
          IAudioService iAudioService = getService();
          boolean bool = false;
          try {
            paramInt = iAudioService.setUidDeviceAffinity(cb(), paramInt, arrayOfInt, arrayOfString);
            if (paramInt == 0)
              bool = true; 
            return bool;
          } catch (RemoteException remoteException) {
            Log.e("AudioPolicy", "Dead object in setUidDeviceAffinity", (Throwable)remoteException);
            return false;
          } 
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Cannot use unregistered AudioPolicy");
        throw illegalStateException;
      }  
    throw new IllegalArgumentException("Illegal null list of audio devices");
  }
  
  @SystemApi
  public boolean removeUidDeviceAffinity(int paramInt) {
    synchronized (this.mLock) {
      if (this.mStatus == 2) {
        IAudioService iAudioService = getService();
        boolean bool = false;
        try {
          paramInt = iAudioService.removeUidDeviceAffinity(cb(), paramInt);
          if (paramInt == 0)
            bool = true; 
          return bool;
        } catch (RemoteException remoteException) {
          Log.e("AudioPolicy", "Dead object in removeUidDeviceAffinity", (Throwable)remoteException);
          return false;
        } 
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot use unregistered AudioPolicy");
      throw illegalStateException;
    } 
  }
  
  @SystemApi
  public boolean removeUserIdDeviceAffinity(int paramInt) {
    synchronized (this.mLock) {
      if (this.mStatus == 2) {
        IAudioService iAudioService = getService();
        boolean bool = false;
        try {
          paramInt = iAudioService.removeUserIdDeviceAffinity(cb(), paramInt);
          if (paramInt == 0)
            bool = true; 
          return bool;
        } catch (RemoteException remoteException) {
          Log.e("AudioPolicy", "Dead object in removeUserIdDeviceAffinity", (Throwable)remoteException);
          return false;
        } 
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot use unregistered AudioPolicy");
      throw illegalStateException;
    } 
  }
  
  @SystemApi
  public boolean setUserIdDeviceAffinity(int paramInt, List<AudioDeviceInfo> paramList) {
    Objects.requireNonNull(paramList, "Illegal null list of audio devices");
    synchronized (this.mLock) {
      if (this.mStatus == 2) {
        int[] arrayOfInt = new int[paramList.size()];
        String[] arrayOfString = new String[paramList.size()];
        byte b = 0;
        for (Iterator<AudioDeviceInfo> iterator = paramList.iterator(); iterator.hasNext(); ) {
          AudioDeviceInfo audioDeviceInfo = iterator.next();
          if (audioDeviceInfo != null) {
            arrayOfInt[b] = AudioDeviceInfo.convertDeviceTypeToInternalDevice(audioDeviceInfo.getType());
            arrayOfString[b] = audioDeviceInfo.getAddress();
            b++;
            continue;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          this("Illegal null AudioDeviceInfo in setUserIdDeviceAffinity");
          throw illegalArgumentException;
        } 
        IAudioService iAudioService = getService();
        boolean bool = false;
        try {
          paramInt = iAudioService.setUserIdDeviceAffinity(cb(), paramInt, arrayOfInt, arrayOfString);
          if (paramInt == 0)
            bool = true; 
          return bool;
        } catch (RemoteException remoteException) {
          Log.e("AudioPolicy", "Dead object in setUserIdDeviceAffinity", (Throwable)remoteException);
          return false;
        } 
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Cannot use unregistered AudioPolicy");
      throw illegalStateException;
    } 
  }
  
  public void setRegistration(String paramString) {
    synchronized (this.mLock) {
      this.mRegistrationId = paramString;
      this.mConfig.setRegistration(paramString);
      if (paramString != null) {
        this.mStatus = 2;
      } else {
        this.mStatus = 1;
      } 
      sendMsg(0);
      return;
    } 
  }
  
  private boolean policyReadyToUse() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_1
    //   5: aload_1
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mStatus : I
    //   11: iconst_2
    //   12: if_icmpeq -> 28
    //   15: ldc 'AudioPolicy'
    //   17: ldc_w 'Cannot use unregistered AudioPolicy'
    //   20: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   23: pop
    //   24: aload_1
    //   25: monitorexit
    //   26: iconst_0
    //   27: ireturn
    //   28: aload_0
    //   29: getfield mRegistrationId : Ljava/lang/String;
    //   32: ifnonnull -> 48
    //   35: ldc 'AudioPolicy'
    //   37: ldc_w 'Cannot use unregistered AudioPolicy'
    //   40: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   43: pop
    //   44: aload_1
    //   45: monitorexit
    //   46: iconst_0
    //   47: ireturn
    //   48: aload_1
    //   49: monitorexit
    //   50: aload_0
    //   51: ldc_w 'android.permission.MODIFY_AUDIO_ROUTING'
    //   54: invokespecial checkCallingOrSelfPermission : (Ljava/lang/String;)I
    //   57: ifne -> 65
    //   60: iconst_1
    //   61: istore_2
    //   62: goto -> 67
    //   65: iconst_0
    //   66: istore_2
    //   67: aload_0
    //   68: getfield mProjection : Landroid/media/projection/MediaProjection;
    //   71: ifnull -> 97
    //   74: aload_0
    //   75: getfield mProjection : Landroid/media/projection/MediaProjection;
    //   78: invokevirtual getProjection : ()Landroid/media/projection/IMediaProjection;
    //   81: invokeinterface canProjectAudio : ()Z
    //   86: istore_3
    //   87: iload_3
    //   88: ifeq -> 97
    //   91: iconst_1
    //   92: istore #4
    //   94: goto -> 100
    //   97: iconst_0
    //   98: istore #4
    //   100: aload_0
    //   101: invokespecial isLoopbackRenderPolicy : ()Z
    //   104: ifeq -> 112
    //   107: iload #4
    //   109: ifne -> 187
    //   112: iload_2
    //   113: ifne -> 187
    //   116: new java/lang/StringBuilder
    //   119: dup
    //   120: invokespecial <init> : ()V
    //   123: astore #5
    //   125: aload #5
    //   127: ldc_w 'Cannot use AudioPolicy for pid '
    //   130: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload #5
    //   136: invokestatic getCallingPid : ()I
    //   139: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   142: pop
    //   143: aload #5
    //   145: ldc_w ' / uid '
    //   148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: pop
    //   152: aload #5
    //   154: invokestatic getCallingUid : ()I
    //   157: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   160: pop
    //   161: aload #5
    //   163: ldc_w ', needs MODIFY_AUDIO_ROUTING or MediaProjection that can project audio.'
    //   166: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   169: pop
    //   170: aload #5
    //   172: invokevirtual toString : ()Ljava/lang/String;
    //   175: astore #5
    //   177: ldc 'AudioPolicy'
    //   179: aload #5
    //   181: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   184: pop
    //   185: iconst_0
    //   186: ireturn
    //   187: iconst_1
    //   188: ireturn
    //   189: astore #5
    //   191: ldc 'AudioPolicy'
    //   193: ldc_w 'Failed to check if MediaProjection#canProjectAudio'
    //   196: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   199: pop
    //   200: aload #5
    //   202: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
    //   205: athrow
    //   206: astore #5
    //   208: aload_1
    //   209: monitorexit
    //   210: aload #5
    //   212: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #570	-> 0
    //   #571	-> 7
    //   #572	-> 15
    //   #573	-> 24
    //   #575	-> 28
    //   #576	-> 35
    //   #577	-> 44
    //   #579	-> 48
    //   #582	-> 50
    //   #583	-> 50
    //   #587	-> 67
    //   #591	-> 100
    //   #593	-> 100
    //   #594	-> 116
    //   #595	-> 152
    //   #594	-> 177
    //   #597	-> 185
    //   #599	-> 187
    //   #588	-> 189
    //   #589	-> 191
    //   #590	-> 200
    //   #579	-> 206
    // Exception table:
    //   from	to	target	type
    //   7	15	206	finally
    //   15	24	206	finally
    //   24	26	206	finally
    //   28	35	206	finally
    //   35	44	206	finally
    //   44	46	206	finally
    //   48	50	206	finally
    //   67	87	189	android/os/RemoteException
    //   208	210	206	finally
  }
  
  private boolean isLoopbackRenderPolicy() {
    synchronized (this.mLock) {
      return this.mConfig.mMixes.stream().allMatch((Predicate)_$$Lambda$AudioPolicy$_ztOT0FT3tzGMUr4lm1gv6dBE4c.INSTANCE);
    } 
  }
  
  private int checkCallingOrSelfPermission(String paramString) {
    Context context = this.mContext;
    if (context != null)
      return context.checkCallingOrSelfPermission(paramString); 
    Slog.v("AudioPolicy", "Null context, checking permission via ActivityManager");
    int i = Binder.getCallingPid();
    int j = Binder.getCallingUid();
    try {
      return ActivityManager.getService().checkPermission(paramString, i, j);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void checkMixReadyToUse(AudioMix paramAudioMix, boolean paramBoolean) throws IllegalArgumentException {
    String str;
    if (paramAudioMix == null) {
      if (paramBoolean) {
        str = "Invalid null AudioMix for AudioTrack creation";
      } else {
        str = "Invalid null AudioMix for AudioRecord creation";
      } 
      throw new IllegalArgumentException(str);
    } 
    if (this.mConfig.mMixes.contains(str)) {
      if ((str.getRouteFlags() & 0x2) == 2) {
        if (!paramBoolean || str.getMixType() == 1) {
          if (paramBoolean || str.getMixType() == 0)
            return; 
          throw new IllegalArgumentException("Invalid AudioMix: not defined for capturing playback");
        } 
        throw new IllegalArgumentException("Invalid AudioMix: not defined for being a recording source");
      } 
      throw new IllegalArgumentException("Invalid AudioMix: not defined for loop back");
    } 
    throw new IllegalArgumentException("Invalid mix: not part of this policy");
  }
  
  public int getFocusDuckingBehavior() {
    return this.mConfig.mDuckingPolicy;
  }
  
  public int setFocusDuckingBehavior(int paramInt) throws IllegalArgumentException, IllegalStateException {
    if (paramInt == 0 || paramInt == 1)
      synchronized (this.mLock) {
        if (this.mStatus == 2) {
          if (paramInt != 1 || this.mFocusListener != null) {
            IAudioService iAudioService = getService();
            try {
              IAudioPolicyCallback iAudioPolicyCallback = cb();
              int i = iAudioService.setFocusPropertiesForPolicy(paramInt, iAudioPolicyCallback);
              if (i == 0)
                this.mConfig.mDuckingPolicy = paramInt; 
              return i;
            } catch (RemoteException remoteException) {
              Log.e("AudioPolicy", "Dead object in setFocusPropertiesForPolicy for behavior", (Throwable)remoteException);
              return -1;
            } 
          } 
          IllegalStateException illegalStateException1 = new IllegalStateException();
          this("Cannot handle ducking without an audio focus listener");
          throw illegalStateException1;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("Cannot change ducking behavior for unregistered policy");
        throw illegalStateException;
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid ducking behavior ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public AudioRecord createAudioRecordSink(AudioMix paramAudioMix) throws IllegalArgumentException {
    if (!policyReadyToUse()) {
      Log.e("AudioPolicy", "Cannot create AudioRecord sink for AudioMix");
      return null;
    } 
    checkMixReadyToUse(paramAudioMix, false);
    AudioFormat.Builder builder = new AudioFormat.Builder(paramAudioMix.getFormat());
    int i = paramAudioMix.getFormat().getChannelMask();
    builder = builder.setChannelMask(AudioFormat.inChannelMaskFromOutChannelMask(i));
    AudioFormat audioFormat = builder.build();
    AudioAttributes.Builder builder1 = new AudioAttributes.Builder();
    builder1 = builder1.setInternalCapturePreset(8);
    builder1 = builder1.addTag(addressForTag(paramAudioMix));
    builder1 = builder1.addTag("fixedVolume");
    AudioAttributes audioAttributes = builder1.build();
    i = paramAudioMix.getFormat().getSampleRate();
    int j = paramAudioMix.getFormat().getEncoding();
    null = new AudioRecord(audioAttributes, audioFormat, AudioRecord.getMinBufferSize(i, 12, j), 0);
    synchronized (this.mLock) {
      if (this.mCaptors == null) {
        ArrayList<WeakReference<AudioRecord>> arrayList1 = new ArrayList();
        this(1);
        this.mCaptors = arrayList1;
      } 
      ArrayList<WeakReference<AudioRecord>> arrayList = this.mCaptors;
      WeakReference<AudioRecord> weakReference = new WeakReference();
      this((T)null);
      arrayList.add(weakReference);
      return null;
    } 
  }
  
  public AudioTrack createAudioTrackSource(AudioMix paramAudioMix) throws IllegalArgumentException {
    if (!policyReadyToUse()) {
      Log.e("AudioPolicy", "Cannot create AudioTrack source for AudioMix");
      return null;
    } 
    checkMixReadyToUse(paramAudioMix, true);
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setUsage(15);
    builder = builder.addTag(addressForTag(paramAudioMix));
    AudioAttributes audioAttributes = builder.build();
    AudioFormat audioFormat = paramAudioMix.getFormat();
    int i = paramAudioMix.getFormat().getSampleRate();
    int j = paramAudioMix.getFormat().getChannelMask(), k = paramAudioMix.getFormat().getEncoding();
    null = new AudioTrack(audioAttributes, audioFormat, AudioTrack.getMinBufferSize(i, j, k), 1, 0);
    synchronized (this.mLock) {
      if (this.mInjectors == null) {
        ArrayList<WeakReference<AudioTrack>> arrayList1 = new ArrayList();
        this(1);
        this.mInjectors = arrayList1;
      } 
      ArrayList<WeakReference<AudioTrack>> arrayList = this.mInjectors;
      WeakReference<AudioTrack> weakReference = new WeakReference();
      this((T)null);
      arrayList.add(weakReference);
      return null;
    } 
  }
  
  public void invalidateCaptorsAndInjectors() {
    if (!policyReadyToUse())
      return; 
    synchronized (this.mLock) {
      if (this.mInjectors != null)
        for (WeakReference<AudioTrack> weakReference : this.mInjectors) {
          AudioTrack audioTrack = weakReference.get();
          if (audioTrack == null)
            break; 
          try {
            audioTrack.stop();
            audioTrack.flush();
          } catch (IllegalStateException illegalStateException) {}
        }  
      if (this.mCaptors != null)
        for (WeakReference<AudioRecord> weakReference : this.mCaptors) {
          AudioRecord audioRecord = weakReference.get();
          if (audioRecord == null)
            break; 
          try {
            audioRecord.stop();
          } catch (IllegalStateException illegalStateException) {}
        }  
      return;
    } 
  }
  
  public int getStatus() {
    return this.mStatus;
  }
  
  public static abstract class AudioPolicyStatusListener {
    public void onStatusChange() {}
    
    public void onMixStateUpdate(AudioMix param1AudioMix) {}
  }
  
  public static abstract class AudioPolicyFocusListener {
    public void onAudioFocusGrant(AudioFocusInfo param1AudioFocusInfo, int param1Int) {}
    
    public void onAudioFocusLoss(AudioFocusInfo param1AudioFocusInfo, boolean param1Boolean) {}
    
    public void onAudioFocusRequest(AudioFocusInfo param1AudioFocusInfo, int param1Int) {}
    
    public void onAudioFocusAbandon(AudioFocusInfo param1AudioFocusInfo) {}
  }
  
  public static abstract class AudioPolicyVolumeCallback {
    public void onVolumeAdjustment(int param1Int) {}
  }
  
  private void onPolicyStatusChange() {
    synchronized (this.mLock) {
      if (this.mStatusListener == null)
        return; 
      AudioPolicyStatusListener audioPolicyStatusListener = this.mStatusListener;
      audioPolicyStatusListener.onStatusChange();
      return;
    } 
  }
  
  public IAudioPolicyCallback cb() {
    return this.mPolicyCb;
  }
  
  private AudioPolicy(AudioPolicyConfig paramAudioPolicyConfig, Context paramContext, Looper paramLooper, AudioPolicyFocusListener paramAudioPolicyFocusListener, AudioPolicyStatusListener paramAudioPolicyStatusListener, boolean paramBoolean1, boolean paramBoolean2, AudioPolicyVolumeCallback paramAudioPolicyVolumeCallback, MediaProjection paramMediaProjection) {
    this.mPolicyCb = (IAudioPolicyCallback)new Object(this);
    this.mConfig = paramAudioPolicyConfig;
    this.mStatus = 1;
    this.mContext = paramContext;
    Looper looper = paramLooper;
    if (paramLooper == null)
      looper = Looper.getMainLooper(); 
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      this.mEventHandler = null;
      Log.e("AudioPolicy", "No event handler due to looper without a thread");
    } 
    this.mFocusListener = paramAudioPolicyFocusListener;
    this.mStatusListener = paramAudioPolicyStatusListener;
    this.mIsFocusPolicy = paramBoolean1;
    this.mIsTestFocusPolicy = paramBoolean2;
    this.mVolCb = paramAudioPolicyVolumeCallback;
    this.mProjection = paramMediaProjection;
  }
  
  class EventHandler extends Handler {
    final AudioPolicy this$0;
    
    public EventHandler(AudioPolicy param1AudioPolicy1, Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      StringBuilder stringBuilder;
      switch (param1Message.what) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown event ");
          stringBuilder.append(param1Message.what);
          Log.e("AudioPolicy", stringBuilder.toString());
          return;
        case 6:
          if (AudioPolicy.this.mVolCb != null) {
            AudioPolicy.this.mVolCb.onVolumeAdjustment(param1Message.arg1);
          } else {
            Log.e("AudioPolicy", "Invalid null volume event");
          } 
          return;
        case 5:
          if (AudioPolicy.this.mFocusListener != null) {
            AudioPolicy.this.mFocusListener.onAudioFocusAbandon((AudioFocusInfo)param1Message.obj);
          } else {
            Log.e("AudioPolicy", "Invalid null focus listener for focus abandon event");
          } 
          return;
        case 4:
          if (AudioPolicy.this.mFocusListener != null) {
            AudioPolicy.this.mFocusListener.onAudioFocusRequest((AudioFocusInfo)param1Message.obj, param1Message.arg1);
          } else {
            Log.e("AudioPolicy", "Invalid null focus listener for focus request event");
          } 
          return;
        case 3:
          if (AudioPolicy.this.mStatusListener != null)
            AudioPolicy.this.mStatusListener.onMixStateUpdate((AudioMix)param1Message.obj); 
          return;
        case 2:
          if (AudioPolicy.this.mFocusListener != null) {
            boolean bool;
            AudioPolicy.AudioPolicyFocusListener audioPolicyFocusListener = AudioPolicy.this.mFocusListener;
            AudioFocusInfo audioFocusInfo = (AudioFocusInfo)param1Message.obj;
            if (param1Message.arg1 != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            audioPolicyFocusListener.onAudioFocusLoss(audioFocusInfo, bool);
          } 
          return;
        case 1:
          if (AudioPolicy.this.mFocusListener != null)
            AudioPolicy.this.mFocusListener.onAudioFocusGrant((AudioFocusInfo)param1Message.obj, param1Message.arg1); 
          return;
        case 0:
          break;
      } 
      AudioPolicy.this.onPolicyStatusChange();
    }
  }
  
  private static String addressForTag(AudioMix paramAudioMix) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("addr=");
    stringBuilder.append(paramAudioMix.getRegistration());
    return stringBuilder.toString();
  }
  
  private void sendMsg(int paramInt) {
    EventHandler eventHandler = this.mEventHandler;
    if (eventHandler != null)
      eventHandler.sendEmptyMessage(paramInt); 
  }
  
  private void sendMsg(int paramInt1, Object paramObject, int paramInt2) {
    EventHandler eventHandler = this.mEventHandler;
    if (eventHandler != null) {
      paramObject = eventHandler.obtainMessage(paramInt1, paramInt2, 0, paramObject);
      eventHandler.sendMessage((Message)paramObject);
    } 
  }
  
  private static IAudioService getService() {
    IAudioService iAudioService2 = sService;
    if (iAudioService2 != null)
      return iAudioService2; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService1 = IAudioService.Stub.asInterface(iBinder);
    return iAudioService1;
  }
  
  public String toLogFriendlyString() {
    String str = new String("android.media.audiopolicy.AudioPolicy:\n");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str);
    stringBuilder.append("config=");
    stringBuilder.append(this.mConfig.toLogFriendlyString());
    return stringBuilder.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PolicyStatus {}
}
