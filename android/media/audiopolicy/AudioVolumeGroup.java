package android.media.audiopolicy;

import android.annotation.SystemApi;
import android.media.AudioAttributes;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SystemApi
public final class AudioVolumeGroup implements Parcelable {
  public static final Parcelable.Creator<AudioVolumeGroup> CREATOR;
  
  public static final int DEFAULT_VOLUME_GROUP = -1;
  
  private static final String TAG = "AudioVolumeGroup";
  
  private static List<AudioVolumeGroup> sAudioVolumeGroups;
  
  private static final Object sLock = new Object();
  
  private final AudioAttributes[] mAudioAttributes;
  
  private int mId;
  
  private int[] mLegacyStreamTypes;
  
  private final String mName;
  
  public static List<AudioVolumeGroup> getAudioVolumeGroups() {
    if (sAudioVolumeGroups == null)
      synchronized (sLock) {
        if (sAudioVolumeGroups == null)
          sAudioVolumeGroups = initializeAudioVolumeGroups(); 
      }  
    return sAudioVolumeGroups;
  }
  
  private static List<AudioVolumeGroup> initializeAudioVolumeGroups() {
    ArrayList<AudioVolumeGroup> arrayList = new ArrayList();
    int i = native_list_audio_volume_groups(arrayList);
    if (i != 0)
      Log.w("AudioVolumeGroup", ": listAudioVolumeGroups failed"); 
    return arrayList;
  }
  
  AudioVolumeGroup(String paramString, int paramInt, AudioAttributes[] paramArrayOfAudioAttributes, int[] paramArrayOfint) {
    Preconditions.checkNotNull(paramString, "name must not be null");
    Preconditions.checkNotNull(paramArrayOfAudioAttributes, "audioAttributes must not be null");
    Preconditions.checkNotNull(paramArrayOfint, "legacyStreamTypes must not be null");
    this.mName = paramString;
    this.mId = paramInt;
    this.mAudioAttributes = paramArrayOfAudioAttributes;
    this.mLegacyStreamTypes = paramArrayOfint;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    AudioVolumeGroup audioVolumeGroup = (AudioVolumeGroup)paramObject;
    if (this.mName == audioVolumeGroup.mName && this.mId == audioVolumeGroup.mId) {
      paramObject = this.mAudioAttributes;
      AudioAttributes[] arrayOfAudioAttributes = audioVolumeGroup.mAudioAttributes;
      if (paramObject.equals(arrayOfAudioAttributes))
        return null; 
    } 
    return false;
  }
  
  public List<AudioAttributes> getAudioAttributes() {
    return Arrays.asList(this.mAudioAttributes);
  }
  
  public int[] getLegacyStreamTypes() {
    return this.mLegacyStreamTypes;
  }
  
  public String name() {
    return this.mName;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mAudioAttributes.length);
    AudioAttributes[] arrayOfAudioAttributes;
    int i, j, k;
    for (arrayOfAudioAttributes = this.mAudioAttributes, i = arrayOfAudioAttributes.length, j = 0, k = 0; k < i; ) {
      AudioAttributes audioAttributes = arrayOfAudioAttributes[k];
      audioAttributes.writeToParcel(paramParcel, paramInt | 0x1);
      k++;
    } 
    paramParcel.writeInt(this.mLegacyStreamTypes.length);
    for (int[] arrayOfInt = this.mLegacyStreamTypes; paramInt < k; ) {
      j = arrayOfInt[paramInt];
      paramParcel.writeInt(j);
      paramInt++;
    } 
  }
  
  static {
    CREATOR = new Parcelable.Creator<AudioVolumeGroup>() {
        public AudioVolumeGroup createFromParcel(Parcel param1Parcel) {
          Preconditions.checkNotNull(param1Parcel, "in Parcel must not be null");
          String str = param1Parcel.readString();
          int i = param1Parcel.readInt();
          int j = param1Parcel.readInt();
          AudioAttributes[] arrayOfAudioAttributes = new AudioAttributes[j];
          byte b;
          for (b = 0; b < j; b++)
            arrayOfAudioAttributes[b] = AudioAttributes.CREATOR.createFromParcel(param1Parcel); 
          j = param1Parcel.readInt();
          int[] arrayOfInt = new int[j];
          for (b = 0; b < j; b++)
            arrayOfInt[b] = param1Parcel.readInt(); 
          return new AudioVolumeGroup(str, i, arrayOfAudioAttributes, arrayOfInt);
        }
        
        public AudioVolumeGroup[] newArray(int param1Int) {
          return new AudioVolumeGroup[param1Int];
        }
      };
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("\n Name: ");
    stringBuilder.append(this.mName);
    stringBuilder.append(" Id: ");
    stringBuilder.append(Integer.toString(this.mId));
    stringBuilder.append("\n     Supported Audio Attributes:");
    int i, j, k;
    for (AudioAttributes[] arrayOfAudioAttributes = this.mAudioAttributes; k < i; ) {
      AudioAttributes audioAttributes = arrayOfAudioAttributes[k];
      stringBuilder.append("\n       -");
      stringBuilder.append(audioAttributes.toString());
      k++;
    } 
    stringBuilder.append("\n     Supported Legacy Stream Types: { ");
    for (int[] arrayOfInt = this.mLegacyStreamTypes; k < i; ) {
      j = arrayOfInt[k];
      stringBuilder.append(Integer.toString(j));
      stringBuilder.append(" ");
      k++;
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  private static native int native_list_audio_volume_groups(ArrayList<AudioVolumeGroup> paramArrayList);
}
