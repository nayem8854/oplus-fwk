package android.media.audiopolicy;

import android.media.AudioFormat;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.util.ArrayList;
import java.util.Objects;

public class AudioPolicyConfig implements Parcelable {
  protected int mDuckingPolicy = 0;
  
  private String mRegistrationId = null;
  
  private int mMixCounter = 0;
  
  protected AudioPolicyConfig(AudioPolicyConfig paramAudioPolicyConfig) {
    this.mMixes = paramAudioPolicyConfig.mMixes;
  }
  
  AudioPolicyConfig(ArrayList<AudioMix> paramArrayList) {
    this.mMixes = paramArrayList;
  }
  
  public void addMix(AudioMix paramAudioMix) throws IllegalArgumentException {
    if (paramAudioMix != null) {
      this.mMixes.add(paramAudioMix);
      return;
    } 
    throw new IllegalArgumentException("Illegal null AudioMix argument");
  }
  
  public ArrayList<AudioMix> getMixes() {
    return this.mMixes;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mMixes });
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mMixes.size());
    for (AudioMix audioMix : this.mMixes) {
      paramParcel.writeInt(audioMix.getRouteFlags());
      paramParcel.writeInt(audioMix.mCallbackFlags);
      paramParcel.writeInt(audioMix.mDeviceSystemType);
      paramParcel.writeString(audioMix.mDeviceAddress);
      paramParcel.writeInt(audioMix.getFormat().getSampleRate());
      paramParcel.writeInt(audioMix.getFormat().getEncoding());
      paramParcel.writeInt(audioMix.getFormat().getChannelMask());
      paramParcel.writeBoolean(audioMix.getRule().allowPrivilegedPlaybackCapture());
      paramParcel.writeBoolean(audioMix.getRule().voiceCommunicationCaptureAllowed());
      ArrayList<AudioMixingRule.AudioMixMatchCriterion> arrayList = audioMix.getRule().getCriteria();
      paramParcel.writeInt(arrayList.size());
      for (AudioMixingRule.AudioMixMatchCriterion audioMixMatchCriterion : arrayList)
        audioMixMatchCriterion.writeToParcel(paramParcel); 
    } 
  }
  
  private AudioPolicyConfig(Parcel paramParcel) {
    this.mMixes = new ArrayList<>();
    int i = paramParcel.readInt();
    for (byte b = 0; b < i; b++) {
      AudioMix.Builder builder = new AudioMix.Builder();
      int j = paramParcel.readInt();
      builder.setRouteFlags(j);
      builder.setCallbackFlags(paramParcel.readInt());
      builder.setDevice(paramParcel.readInt(), paramParcel.readString());
      int k = paramParcel.readInt();
      int m = paramParcel.readInt();
      j = paramParcel.readInt();
      AudioFormat.Builder builder2 = (new AudioFormat.Builder()).setSampleRate(k);
      AudioFormat audioFormat = builder2.setChannelMask(j).setEncoding(m).build();
      builder.setFormat(audioFormat);
      AudioMixingRule.Builder builder1 = new AudioMixingRule.Builder();
      builder1.allowPrivilegedPlaybackCapture(paramParcel.readBoolean());
      builder1.voiceCommunicationCaptureAllowed(paramParcel.readBoolean());
      m = paramParcel.readInt();
      for (j = 0; j < m; j++)
        builder1.addRuleFromParcel(paramParcel); 
      builder.setMixingRule(builder1.build());
      this.mMixes.add(builder.build());
    } 
  }
  
  public static final Parcelable.Creator<AudioPolicyConfig> CREATOR = new Parcelable.Creator<AudioPolicyConfig>() {
      public AudioPolicyConfig createFromParcel(Parcel param1Parcel) {
        return new AudioPolicyConfig(param1Parcel);
      }
      
      public AudioPolicyConfig[] newArray(int param1Int) {
        return new AudioPolicyConfig[param1Int];
      }
    };
  
  private static final String TAG = "AudioPolicyConfig";
  
  protected final ArrayList<AudioMix> mMixes;
  
  public String toLogFriendlyString() {
    String str1 = new String("android.media.audiopolicy.AudioPolicyConfig:\n");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(str1);
    stringBuilder.append(this.mMixes.size());
    stringBuilder.append(" AudioMix: ");
    stringBuilder.append(this.mRegistrationId);
    stringBuilder.append("\n");
    String str2 = stringBuilder.toString();
    for (AudioMix audioMix : this.mMixes) {
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append(str2);
      stringBuilder3.append("* route flags=0x");
      stringBuilder3.append(Integer.toHexString(audioMix.getRouteFlags()));
      stringBuilder3.append("\n");
      str2 = stringBuilder3.toString();
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(str2);
      stringBuilder3.append("  rate=");
      stringBuilder3.append(audioMix.getFormat().getSampleRate());
      stringBuilder3.append("Hz\n");
      str2 = stringBuilder3.toString();
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(str2);
      stringBuilder3.append("  encoding=");
      stringBuilder3.append(audioMix.getFormat().getEncoding());
      stringBuilder3.append("\n");
      str2 = stringBuilder3.toString();
      stringBuilder3 = new StringBuilder();
      stringBuilder3.append(str2);
      stringBuilder3.append("  channels=0x");
      String str4 = stringBuilder3.toString();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(str4);
      stringBuilder1.append(Integer.toHexString(audioMix.getFormat().getChannelMask()).toUpperCase());
      stringBuilder1.append("\n");
      String str3 = stringBuilder1.toString();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(str3);
      stringBuilder2.append("  ignore playback capture opt out=");
      stringBuilder2.append(audioMix.getRule().allowPrivilegedPlaybackCapture());
      stringBuilder2.append("\n");
      str3 = stringBuilder2.toString();
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append(str3);
      stringBuilder2.append("  allow voice communication capture=");
      stringBuilder2.append(audioMix.getRule().voiceCommunicationCaptureAllowed());
      stringBuilder2.append("\n");
      str3 = stringBuilder2.toString();
      ArrayList<AudioMixingRule.AudioMixMatchCriterion> arrayList = audioMix.getRule().getCriteria();
      for (AudioMixingRule.AudioMixMatchCriterion audioMixMatchCriterion : arrayList) {
        int i = audioMixMatchCriterion.mRule;
        if (i != 1) {
          if (i != 2) {
            if (i != 4) {
              if (i != 8) {
                if (i != 32772) {
                  if (i != 32776) {
                    StringBuilder stringBuilder5;
                    StringBuilder stringBuilder7;
                    String str;
                    StringBuilder stringBuilder6;
                    switch (i) {
                      default:
                        stringBuilder4 = new StringBuilder();
                        stringBuilder4.append(str3);
                        stringBuilder4.append("invalid rule!");
                        str3 = stringBuilder4.toString();
                        break;
                      case 32770:
                        stringBuilder7 = new StringBuilder();
                        stringBuilder7.append(str3);
                        stringBuilder7.append("  exclude capture preset ");
                        str = stringBuilder7.toString();
                        stringBuilder5 = new StringBuilder();
                        stringBuilder5.append(str);
                        stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mAttr.getCapturePreset());
                        str2 = stringBuilder5.toString();
                        break;
                      case 32769:
                        stringBuilder6 = new StringBuilder();
                        stringBuilder6.append(str2);
                        stringBuilder6.append("  exclude usage ");
                        str2 = stringBuilder6.toString();
                        stringBuilder6 = new StringBuilder();
                        stringBuilder6.append(str2);
                        stringBuilder6.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mAttr.usageToString());
                        str2 = stringBuilder6.toString();
                        break;
                    } 
                  } else {
                    StringBuilder stringBuilder5 = new StringBuilder();
                    stringBuilder5.append(str2);
                    stringBuilder5.append("  exclude userId ");
                    str2 = stringBuilder5.toString();
                    stringBuilder5 = new StringBuilder();
                    stringBuilder5.append(str2);
                    stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mIntProp);
                    str2 = stringBuilder5.toString();
                  } 
                } else {
                  StringBuilder stringBuilder5 = new StringBuilder();
                  stringBuilder5.append(str2);
                  stringBuilder5.append("  exclude UID ");
                  str2 = stringBuilder5.toString();
                  stringBuilder5 = new StringBuilder();
                  stringBuilder5.append(str2);
                  stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mIntProp);
                  str2 = stringBuilder5.toString();
                } 
              } else {
                StringBuilder stringBuilder6 = new StringBuilder();
                stringBuilder6.append(str2);
                stringBuilder6.append("  match userId ");
                String str = stringBuilder6.toString();
                StringBuilder stringBuilder5 = new StringBuilder();
                stringBuilder5.append(str);
                stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mIntProp);
                str2 = stringBuilder5.toString();
              } 
            } else {
              StringBuilder stringBuilder5 = new StringBuilder();
              stringBuilder5.append(str2);
              stringBuilder5.append("  match UID ");
              str2 = stringBuilder5.toString();
              stringBuilder5 = new StringBuilder();
              stringBuilder5.append(str2);
              stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mIntProp);
              str2 = stringBuilder5.toString();
            } 
          } else {
            StringBuilder stringBuilder6 = new StringBuilder();
            stringBuilder6.append(str2);
            stringBuilder6.append("  match capture preset ");
            String str = stringBuilder6.toString();
            StringBuilder stringBuilder5 = new StringBuilder();
            stringBuilder5.append(str);
            stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mAttr.getCapturePreset());
            str2 = stringBuilder5.toString();
          } 
        } else {
          StringBuilder stringBuilder5 = new StringBuilder();
          stringBuilder5.append(str2);
          stringBuilder5.append("  match usage ");
          str2 = stringBuilder5.toString();
          stringBuilder5 = new StringBuilder();
          stringBuilder5.append(str2);
          stringBuilder5.append(((AudioMixingRule.AudioMixMatchCriterion)stringBuilder4).mAttr.usageToString());
          str2 = stringBuilder5.toString();
        } 
        StringBuilder stringBuilder4 = new StringBuilder();
        stringBuilder4.append(str2);
        stringBuilder4.append("\n");
        str2 = stringBuilder4.toString();
      } 
    } 
    return str2;
  }
  
  protected void setRegistration(String paramString) {
    boolean bool2;
    String str = this.mRegistrationId;
    boolean bool1 = false;
    if (str == null || str.isEmpty()) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if (paramString == null || paramString.isEmpty())
      bool1 = true; 
    if (!bool2 && !bool1 && !this.mRegistrationId.equals(paramString)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid registration transition from ");
      stringBuilder.append(this.mRegistrationId);
      stringBuilder.append(" to ");
      stringBuilder.append(paramString);
      Log.e("AudioPolicyConfig", stringBuilder.toString());
      return;
    } 
    if (paramString == null)
      paramString = ""; 
    this.mRegistrationId = paramString;
    for (AudioMix audioMix : this.mMixes)
      setMixRegistration(audioMix); 
  }
  
  private void setMixRegistration(AudioMix paramAudioMix) {
    if (!this.mRegistrationId.isEmpty()) {
      if ((paramAudioMix.getRouteFlags() & 0x2) == 2) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(this.mRegistrationId);
        stringBuilder.append("mix");
        stringBuilder.append(mixTypeId(paramAudioMix.getMixType()));
        stringBuilder.append(":");
        stringBuilder.append(this.mMixCounter);
        paramAudioMix.setRegistration(stringBuilder.toString());
      } else if ((paramAudioMix.getRouteFlags() & 0x1) == 1) {
        paramAudioMix.setRegistration(paramAudioMix.mDeviceAddress);
      } 
    } else {
      paramAudioMix.setRegistration("");
    } 
    this.mMixCounter++;
  }
  
  protected void add(ArrayList<AudioMix> paramArrayList) {
    for (AudioMix audioMix : paramArrayList) {
      setMixRegistration(audioMix);
      this.mMixes.add(audioMix);
    } 
  }
  
  protected void remove(ArrayList<AudioMix> paramArrayList) {
    for (AudioMix audioMix : paramArrayList)
      this.mMixes.remove(audioMix); 
  }
  
  private static String mixTypeId(int paramInt) {
    if (paramInt == 0)
      return "p"; 
    if (paramInt == 1)
      return "r"; 
    return "i";
  }
  
  protected String getRegistration() {
    return this.mRegistrationId;
  }
}
