package android.media.audiopolicy;

import android.media.AudioFocusInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAudioPolicyCallback extends IInterface {
  void notifyAudioFocusAbandon(AudioFocusInfo paramAudioFocusInfo) throws RemoteException;
  
  void notifyAudioFocusGrant(AudioFocusInfo paramAudioFocusInfo, int paramInt) throws RemoteException;
  
  void notifyAudioFocusLoss(AudioFocusInfo paramAudioFocusInfo, boolean paramBoolean) throws RemoteException;
  
  void notifyAudioFocusRequest(AudioFocusInfo paramAudioFocusInfo, int paramInt) throws RemoteException;
  
  void notifyMixStateUpdate(String paramString, int paramInt) throws RemoteException;
  
  void notifyUnregistration() throws RemoteException;
  
  void notifyVolumeAdjust(int paramInt) throws RemoteException;
  
  class Default implements IAudioPolicyCallback {
    public void notifyAudioFocusGrant(AudioFocusInfo param1AudioFocusInfo, int param1Int) throws RemoteException {}
    
    public void notifyAudioFocusLoss(AudioFocusInfo param1AudioFocusInfo, boolean param1Boolean) throws RemoteException {}
    
    public void notifyAudioFocusRequest(AudioFocusInfo param1AudioFocusInfo, int param1Int) throws RemoteException {}
    
    public void notifyAudioFocusAbandon(AudioFocusInfo param1AudioFocusInfo) throws RemoteException {}
    
    public void notifyMixStateUpdate(String param1String, int param1Int) throws RemoteException {}
    
    public void notifyVolumeAdjust(int param1Int) throws RemoteException {}
    
    public void notifyUnregistration() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAudioPolicyCallback {
    private static final String DESCRIPTOR = "android.media.audiopolicy.IAudioPolicyCallback";
    
    static final int TRANSACTION_notifyAudioFocusAbandon = 4;
    
    static final int TRANSACTION_notifyAudioFocusGrant = 1;
    
    static final int TRANSACTION_notifyAudioFocusLoss = 2;
    
    static final int TRANSACTION_notifyAudioFocusRequest = 3;
    
    static final int TRANSACTION_notifyMixStateUpdate = 5;
    
    static final int TRANSACTION_notifyUnregistration = 7;
    
    static final int TRANSACTION_notifyVolumeAdjust = 6;
    
    public Stub() {
      attachInterface(this, "android.media.audiopolicy.IAudioPolicyCallback");
    }
    
    public static IAudioPolicyCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.audiopolicy.IAudioPolicyCallback");
      if (iInterface != null && iInterface instanceof IAudioPolicyCallback)
        return (IAudioPolicyCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "notifyUnregistration";
        case 6:
          return "notifyVolumeAdjust";
        case 5:
          return "notifyMixStateUpdate";
        case 4:
          return "notifyAudioFocusAbandon";
        case 3:
          return "notifyAudioFocusRequest";
        case 2:
          return "notifyAudioFocusLoss";
        case 1:
          break;
      } 
      return "notifyAudioFocusGrant";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            notifyUnregistration();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            param1Int1 = param1Parcel1.readInt();
            notifyVolumeAdjust(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            notifyMixStateUpdate(str, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            if (param1Parcel1.readInt() != 0) {
              AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyAudioFocusAbandon((AudioFocusInfo)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            if (param1Parcel1.readInt() != 0) {
              AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            notifyAudioFocusRequest((AudioFocusInfo)str, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
            if (param1Parcel1.readInt() != 0) {
              AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            notifyAudioFocusLoss((AudioFocusInfo)str, bool);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.audiopolicy.IAudioPolicyCallback");
        if (param1Parcel1.readInt() != 0) {
          AudioFocusInfo audioFocusInfo = AudioFocusInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          str = null;
        } 
        param1Int1 = param1Parcel1.readInt();
        notifyAudioFocusGrant((AudioFocusInfo)str, param1Int1);
        return true;
      } 
      str.writeString("android.media.audiopolicy.IAudioPolicyCallback");
      return true;
    }
    
    private static class Proxy implements IAudioPolicyCallback {
      public static IAudioPolicyCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.audiopolicy.IAudioPolicyCallback";
      }
      
      public void notifyAudioFocusGrant(AudioFocusInfo param2AudioFocusInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          if (param2AudioFocusInfo != null) {
            parcel.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyAudioFocusGrant(param2AudioFocusInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyAudioFocusLoss(AudioFocusInfo param2AudioFocusInfo, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          boolean bool = false;
          if (param2AudioFocusInfo != null) {
            parcel.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyAudioFocusLoss(param2AudioFocusInfo, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyAudioFocusRequest(AudioFocusInfo param2AudioFocusInfo, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          if (param2AudioFocusInfo != null) {
            parcel.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyAudioFocusRequest(param2AudioFocusInfo, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyAudioFocusAbandon(AudioFocusInfo param2AudioFocusInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          if (param2AudioFocusInfo != null) {
            parcel.writeInt(1);
            param2AudioFocusInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyAudioFocusAbandon(param2AudioFocusInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyMixStateUpdate(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyMixStateUpdate(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyVolumeAdjust(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyVolumeAdjust(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyUnregistration() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.audiopolicy.IAudioPolicyCallback");
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IAudioPolicyCallback.Stub.getDefaultImpl() != null) {
            IAudioPolicyCallback.Stub.getDefaultImpl().notifyUnregistration();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAudioPolicyCallback param1IAudioPolicyCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAudioPolicyCallback != null) {
          Proxy.sDefaultImpl = param1IAudioPolicyCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAudioPolicyCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
