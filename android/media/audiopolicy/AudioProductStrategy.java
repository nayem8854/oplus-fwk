package android.media.audiopolicy;

import android.annotation.SystemApi;
import android.media.AudioAttributes;
import android.media.AudioSystem;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@SystemApi
public final class AudioProductStrategy implements Parcelable {
  public static final Parcelable.Creator<AudioProductStrategy> CREATOR;
  
  public static final int DEFAULT_GROUP = -1;
  
  private static final String TAG = "AudioProductStrategy";
  
  private static List<AudioProductStrategy> sAudioProductStrategies;
  
  public static final AudioAttributes sDefaultAttributes;
  
  private static final Object sLock = new Object();
  
  private final AudioAttributesGroup[] mAudioAttributesGroups;
  
  private int mId;
  
  private final String mName;
  
  public static List<AudioProductStrategy> getAudioProductStrategies() {
    if (sAudioProductStrategies == null)
      synchronized (sLock) {
        if (sAudioProductStrategies == null)
          sAudioProductStrategies = initializeAudioProductStrategies(); 
      }  
    return sAudioProductStrategies;
  }
  
  public static AudioProductStrategy getAudioProductStrategyWithId(int paramInt) {
    synchronized (sLock) {
      if (sAudioProductStrategies == null)
        sAudioProductStrategies = initializeAudioProductStrategies(); 
      for (AudioProductStrategy audioProductStrategy : sAudioProductStrategies) {
        if (audioProductStrategy.getId() == paramInt)
          return audioProductStrategy; 
      } 
      return null;
    } 
  }
  
  @SystemApi
  public static AudioProductStrategy createInvalidAudioProductStrategy(int paramInt) {
    return new AudioProductStrategy("dummy strategy", paramInt, new AudioAttributesGroup[0]);
  }
  
  public static AudioAttributes getAudioAttributesForStrategyWithLegacyStreamType(int paramInt) {
    for (AudioProductStrategy audioProductStrategy : getAudioProductStrategies()) {
      AudioAttributes audioAttributes = audioProductStrategy.getAudioAttributesForLegacyStreamType(paramInt);
      if (audioAttributes != null)
        return audioAttributes; 
    } 
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setContentType(0);
    return builder.setUsage(0).build();
  }
  
  public static int getLegacyStreamTypeForStrategyWithAudioAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "AudioAttributes must not be null");
    for (Iterator<AudioProductStrategy> iterator = getAudioProductStrategies().iterator(); iterator.hasNext(); ) {
      AudioProductStrategy audioProductStrategy = iterator.next();
      if (audioProductStrategy.supportsAudioAttributes(paramAudioAttributes)) {
        int i = audioProductStrategy.getLegacyStreamTypeForAudioAttributes(paramAudioAttributes);
        if (i == -1) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Attributes ");
          stringBuilder.append(paramAudioAttributes.toString());
          stringBuilder.append(" ported by strategy ");
          stringBuilder.append(audioProductStrategy.getId());
          stringBuilder.append(" has no stream type associated, DO NOT USE STREAM TO CONTROL THE VOLUME");
          String str = stringBuilder.toString();
          Log.w("AudioProductStrategy", str);
          return 3;
        } 
        if (i < AudioSystem.getNumStreamTypes())
          return i; 
      } 
    } 
    return 3;
  }
  
  private static List<AudioProductStrategy> initializeAudioProductStrategies() {
    ArrayList<AudioProductStrategy> arrayList = new ArrayList();
    int i = native_list_audio_product_strategies(arrayList);
    if (i != 0)
      Log.w("AudioProductStrategy", ": initializeAudioProductStrategies failed"); 
    return arrayList;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    AudioProductStrategy audioProductStrategy = (AudioProductStrategy)paramObject;
    if (this.mName == audioProductStrategy.mName && this.mId == audioProductStrategy.mId) {
      paramObject = this.mAudioAttributesGroups;
      AudioAttributesGroup[] arrayOfAudioAttributesGroup = audioProductStrategy.mAudioAttributesGroups;
      if (paramObject.equals(arrayOfAudioAttributesGroup))
        return null; 
    } 
    return false;
  }
  
  private AudioProductStrategy(String paramString, int paramInt, AudioAttributesGroup[] paramArrayOfAudioAttributesGroup) {
    Preconditions.checkNotNull(paramString, "name must not be null");
    Preconditions.checkNotNull(paramArrayOfAudioAttributesGroup, "AudioAttributesGroups must not be null");
    this.mName = paramString;
    this.mId = paramInt;
    this.mAudioAttributesGroups = paramArrayOfAudioAttributesGroup;
  }
  
  @SystemApi
  public int getId() {
    return this.mId;
  }
  
  @SystemApi
  public AudioAttributes getAudioAttributes() {
    AudioAttributes audioAttributes;
    AudioAttributesGroup[] arrayOfAudioAttributesGroup = this.mAudioAttributesGroups;
    if (arrayOfAudioAttributesGroup.length == 0) {
      audioAttributes = (new AudioAttributes.Builder()).build();
    } else {
      audioAttributes = audioAttributes[0].getAudioAttributes();
    } 
    return audioAttributes;
  }
  
  public AudioAttributes getAudioAttributesForLegacyStreamType(int paramInt) {
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups) {
      if (audioAttributesGroup.supportsStreamType(paramInt))
        return audioAttributesGroup.getAudioAttributes(); 
    } 
    return null;
  }
  
  public int getLegacyStreamTypeForAudioAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "AudioAttributes must not be null");
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups) {
      if (audioAttributesGroup.supportsAttributes(paramAudioAttributes))
        return audioAttributesGroup.getStreamType(); 
    } 
    return -1;
  }
  
  @SystemApi
  public boolean supportsAudioAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "AudioAttributes must not be null");
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups) {
      if (audioAttributesGroup.supportsAttributes(paramAudioAttributes))
        return true; 
    } 
    return false;
  }
  
  public int getVolumeGroupIdForLegacyStreamType(int paramInt) {
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups) {
      if (audioAttributesGroup.supportsStreamType(paramInt))
        return audioAttributesGroup.getVolumeGroupId(); 
    } 
    return -1;
  }
  
  public int getVolumeGroupIdForAudioAttributes(AudioAttributes paramAudioAttributes) {
    Preconditions.checkNotNull(paramAudioAttributes, "AudioAttributes must not be null");
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups) {
      if (audioAttributesGroup.supportsAttributes(paramAudioAttributes))
        return audioAttributesGroup.getVolumeGroupId(); 
    } 
    return -1;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mName);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mAudioAttributesGroups.length);
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups)
      audioAttributesGroup.writeToParcel(paramParcel, paramInt); 
  }
  
  static {
    CREATOR = new Parcelable.Creator<AudioProductStrategy>() {
        public AudioProductStrategy createFromParcel(Parcel param1Parcel) {
          String str = param1Parcel.readString();
          int i = param1Parcel.readInt();
          int j = param1Parcel.readInt();
          AudioProductStrategy.AudioAttributesGroup[] arrayOfAudioAttributesGroup = new AudioProductStrategy.AudioAttributesGroup[j];
          for (byte b = 0; b < j; b++)
            arrayOfAudioAttributesGroup[b] = AudioProductStrategy.AudioAttributesGroup.CREATOR.createFromParcel(param1Parcel); 
          return new AudioProductStrategy(str, i, arrayOfAudioAttributesGroup);
        }
        
        public AudioProductStrategy[] newArray(int param1Int) {
          return new AudioProductStrategy[param1Int];
        }
      };
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setCapturePreset(0);
    sDefaultAttributes = builder.build();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("\n Name: ");
    stringBuilder.append(this.mName);
    stringBuilder.append(" Id: ");
    stringBuilder.append(Integer.toString(this.mId));
    for (AudioAttributesGroup audioAttributesGroup : this.mAudioAttributesGroups)
      stringBuilder.append(audioAttributesGroup.toString()); 
    return stringBuilder.toString();
  }
  
  private static boolean attributesMatches(AudioAttributes paramAudioAttributes1, AudioAttributes paramAudioAttributes2) {
    Preconditions.checkNotNull(paramAudioAttributes1, "refAttr must not be null");
    Preconditions.checkNotNull(paramAudioAttributes2, "attr must not be null");
    String str1 = TextUtils.join(";", paramAudioAttributes1.getTags());
    String str2 = TextUtils.join(";", paramAudioAttributes2.getTags());
    boolean bool = paramAudioAttributes1.equals(sDefaultAttributes);
    boolean bool1 = false;
    if (bool)
      return false; 
    if ((paramAudioAttributes1.getSystemUsage() == 0 || 
      paramAudioAttributes2.getSystemUsage() == paramAudioAttributes1.getSystemUsage()) && (
      paramAudioAttributes1.getContentType() == 0 || 
      paramAudioAttributes2.getContentType() == paramAudioAttributes1.getContentType()) && (
      paramAudioAttributes1.getAllFlags() == 0 || (
      paramAudioAttributes2.getAllFlags() != 0 && (
      paramAudioAttributes2.getAllFlags() & paramAudioAttributes1.getAllFlags()) == paramAudioAttributes1.getAllFlags())) && (
      str1.length() == 0 || str1.equals(str2)))
      bool1 = true; 
    return bool1;
  }
  
  private static native int native_list_audio_product_strategies(ArrayList<AudioProductStrategy> paramArrayList);
  
  private static final class AudioAttributesGroup implements Parcelable {
    AudioAttributesGroup(int param1Int1, int param1Int2, AudioAttributes[] param1ArrayOfAudioAttributes) {
      this.mVolumeGroupId = param1Int1;
      this.mLegacyStreamType = param1Int2;
      this.mAudioAttributes = param1ArrayOfAudioAttributes;
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || getClass() != param1Object.getClass())
        return false; 
      AudioAttributesGroup audioAttributesGroup = (AudioAttributesGroup)param1Object;
      if (this.mVolumeGroupId == audioAttributesGroup.mVolumeGroupId && this.mLegacyStreamType == audioAttributesGroup.mLegacyStreamType) {
        param1Object = this.mAudioAttributes;
        AudioAttributes[] arrayOfAudioAttributes = audioAttributesGroup.mAudioAttributes;
        if (param1Object.equals(arrayOfAudioAttributes))
          return null; 
      } 
      return false;
    }
    
    public int getStreamType() {
      return this.mLegacyStreamType;
    }
    
    public int getVolumeGroupId() {
      return this.mVolumeGroupId;
    }
    
    public AudioAttributes getAudioAttributes() {
      AudioAttributes audioAttributes, arrayOfAudioAttributes[] = this.mAudioAttributes;
      if (arrayOfAudioAttributes.length == 0) {
        audioAttributes = (new AudioAttributes.Builder()).build();
      } else {
        audioAttributes = audioAttributes[0];
      } 
      return audioAttributes;
    }
    
    public boolean supportsAttributes(AudioAttributes param1AudioAttributes) {
      for (AudioAttributes audioAttributes : this.mAudioAttributes) {
        if (audioAttributes.equals(param1AudioAttributes) || AudioProductStrategy.attributesMatches(audioAttributes, param1AudioAttributes))
          return true; 
      } 
      return false;
    }
    
    public boolean supportsStreamType(int param1Int) {
      boolean bool;
      if (this.mLegacyStreamType == param1Int) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mVolumeGroupId);
      param1Parcel.writeInt(this.mLegacyStreamType);
      param1Parcel.writeInt(this.mAudioAttributes.length);
      for (AudioAttributes audioAttributes : this.mAudioAttributes)
        audioAttributes.writeToParcel(param1Parcel, param1Int | 0x1); 
    }
    
    public static final Parcelable.Creator<AudioAttributesGroup> CREATOR = new Parcelable.Creator<AudioAttributesGroup>() {
        public AudioProductStrategy.AudioAttributesGroup createFromParcel(Parcel param2Parcel) {
          int i = param2Parcel.readInt();
          int j = param2Parcel.readInt();
          int k = param2Parcel.readInt();
          AudioAttributes[] arrayOfAudioAttributes = new AudioAttributes[k];
          for (byte b = 0; b < k; b++)
            arrayOfAudioAttributes[b] = AudioAttributes.CREATOR.createFromParcel(param2Parcel); 
          return new AudioProductStrategy.AudioAttributesGroup(i, j, arrayOfAudioAttributes);
        }
        
        public AudioProductStrategy.AudioAttributesGroup[] newArray(int param2Int) {
          return new AudioProductStrategy.AudioAttributesGroup[param2Int];
        }
      };
    
    private final AudioAttributes[] mAudioAttributes;
    
    private int mLegacyStreamType;
    
    private int mVolumeGroupId;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("\n    Legacy Stream Type: ");
      stringBuilder.append(Integer.toString(this.mLegacyStreamType));
      stringBuilder.append(" Volume Group Id: ");
      stringBuilder.append(Integer.toString(this.mVolumeGroupId));
      for (AudioAttributes audioAttributes : this.mAudioAttributes) {
        stringBuilder.append("\n    -");
        stringBuilder.append(audioAttributes.toString());
      } 
      return stringBuilder.toString();
    }
  }
  
  class null implements Parcelable.Creator<AudioAttributesGroup> {
    public AudioProductStrategy.AudioAttributesGroup createFromParcel(Parcel param1Parcel) {
      int i = param1Parcel.readInt();
      int j = param1Parcel.readInt();
      int k = param1Parcel.readInt();
      AudioAttributes[] arrayOfAudioAttributes = new AudioAttributes[k];
      for (byte b = 0; b < k; b++)
        arrayOfAudioAttributes[b] = AudioAttributes.CREATOR.createFromParcel(param1Parcel); 
      return new AudioProductStrategy.AudioAttributesGroup(i, j, arrayOfAudioAttributes);
    }
    
    public AudioProductStrategy.AudioAttributesGroup[] newArray(int param1Int) {
      return new AudioProductStrategy.AudioAttributesGroup[param1Int];
    }
  }
}
