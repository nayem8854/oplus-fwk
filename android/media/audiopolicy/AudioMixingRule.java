package android.media.audiopolicy;

import android.annotation.SystemApi;
import android.media.AudioAttributes;
import android.os.Parcel;
import android.util.Log;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

@SystemApi
public class AudioMixingRule {
  public static final int RULE_EXCLUDE_ATTRIBUTE_CAPTURE_PRESET = 32770;
  
  public static final int RULE_EXCLUDE_ATTRIBUTE_USAGE = 32769;
  
  public static final int RULE_EXCLUDE_UID = 32772;
  
  public static final int RULE_EXCLUDE_USERID = 32776;
  
  private static final int RULE_EXCLUSION_MASK = 32768;
  
  public static final int RULE_MATCH_ATTRIBUTE_CAPTURE_PRESET = 2;
  
  public static final int RULE_MATCH_ATTRIBUTE_USAGE = 1;
  
  public static final int RULE_MATCH_UID = 4;
  
  public static final int RULE_MATCH_USERID = 8;
  
  private boolean mAllowPrivilegedPlaybackCapture;
  
  private final ArrayList<AudioMixMatchCriterion> mCriteria;
  
  private final int mTargetMixType;
  
  private boolean mVoiceCommunicationCaptureAllowed;
  
  private AudioMixingRule(int paramInt, ArrayList<AudioMixMatchCriterion> paramArrayList, boolean paramBoolean1, boolean paramBoolean2) {
    this.mAllowPrivilegedPlaybackCapture = false;
    this.mVoiceCommunicationCaptureAllowed = false;
    this.mCriteria = paramArrayList;
    this.mTargetMixType = paramInt;
    this.mAllowPrivilegedPlaybackCapture = paramBoolean1;
    this.mVoiceCommunicationCaptureAllowed = paramBoolean2;
  }
  
  public static final class AudioMixMatchCriterion {
    final AudioAttributes mAttr;
    
    final int mIntProp;
    
    final int mRule;
    
    AudioMixMatchCriterion(AudioAttributes param1AudioAttributes, int param1Int) {
      this.mAttr = param1AudioAttributes;
      this.mIntProp = Integer.MIN_VALUE;
      this.mRule = param1Int;
    }
    
    AudioMixMatchCriterion(Integer param1Integer, int param1Int) {
      this.mAttr = null;
      this.mIntProp = param1Integer.intValue();
      this.mRule = param1Int;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mAttr, Integer.valueOf(this.mIntProp), Integer.valueOf(this.mRule) });
    }
    
    void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeInt(this.mRule);
      int i = this.mRule & 0xFFFF7FFF;
      if (i != 1) {
        if (i != 2) {
          if (i != 4 && i != 8) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unknown match rule");
            stringBuilder.append(i);
            stringBuilder.append(" when writing to Parcel");
            Log.e("AudioMixMatchCriterion", stringBuilder.toString());
            param1Parcel.writeInt(-1);
          } else {
            param1Parcel.writeInt(this.mIntProp);
          } 
        } else {
          param1Parcel.writeInt(this.mAttr.getCapturePreset());
        } 
      } else {
        param1Parcel.writeInt(this.mAttr.getUsage());
      } 
    }
    
    public AudioAttributes getAudioAttributes() {
      return this.mAttr;
    }
    
    public int getIntProp() {
      return this.mIntProp;
    }
    
    public int getRule() {
      return this.mRule;
    }
  }
  
  boolean isAffectingUsage(int paramInt) {
    for (AudioMixMatchCriterion audioMixMatchCriterion : this.mCriteria) {
      if ((audioMixMatchCriterion.mRule & 0x1) != 0 && audioMixMatchCriterion.mAttr != null) {
        AudioAttributes audioAttributes = audioMixMatchCriterion.mAttr;
        if (audioAttributes.getUsage() == paramInt)
          return true; 
      } 
    } 
    return false;
  }
  
  boolean containsMatchAttributeRuleForUsage(int paramInt) {
    for (AudioMixMatchCriterion audioMixMatchCriterion : this.mCriteria) {
      if (audioMixMatchCriterion.mRule == 1 && audioMixMatchCriterion.mAttr != null) {
        AudioAttributes audioAttributes = audioMixMatchCriterion.mAttr;
        if (audioAttributes.getUsage() == paramInt)
          return true; 
      } 
    } 
    return false;
  }
  
  private static boolean areCriteriaEquivalent(ArrayList<AudioMixMatchCriterion> paramArrayList1, ArrayList<AudioMixMatchCriterion> paramArrayList2) {
    boolean bool = false;
    if (paramArrayList1 == null || paramArrayList2 == null)
      return false; 
    if (paramArrayList1 == paramArrayList2)
      return true; 
    if (paramArrayList1.size() != paramArrayList2.size())
      return false; 
    if (paramArrayList1.hashCode() == paramArrayList2.hashCode())
      bool = true; 
    return bool;
  }
  
  int getTargetMixType() {
    return this.mTargetMixType;
  }
  
  public ArrayList<AudioMixMatchCriterion> getCriteria() {
    return this.mCriteria;
  }
  
  public boolean allowPrivilegedPlaybackCapture() {
    return this.mAllowPrivilegedPlaybackCapture;
  }
  
  public boolean voiceCommunicationCaptureAllowed() {
    return this.mVoiceCommunicationCaptureAllowed;
  }
  
  public void setVoiceCommunicationCaptureAllowed(boolean paramBoolean) {
    this.mVoiceCommunicationCaptureAllowed = paramBoolean;
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    paramObject = paramObject;
    if (this.mTargetMixType == ((AudioMixingRule)paramObject).mTargetMixType) {
      ArrayList<AudioMixMatchCriterion> arrayList1 = this.mCriteria, arrayList2 = ((AudioMixingRule)paramObject).mCriteria;
      if (areCriteriaEquivalent(arrayList1, arrayList2) && this.mAllowPrivilegedPlaybackCapture == ((AudioMixingRule)paramObject).mAllowPrivilegedPlaybackCapture && this.mVoiceCommunicationCaptureAllowed == ((AudioMixingRule)paramObject).mVoiceCommunicationCaptureAllowed)
        return null; 
    } 
    return false;
  }
  
  public int hashCode() {
    int i = this.mTargetMixType;
    ArrayList<AudioMixMatchCriterion> arrayList = this.mCriteria;
    boolean bool1 = this.mAllowPrivilegedPlaybackCapture;
    boolean bool2 = this.mVoiceCommunicationCaptureAllowed;
    return Objects.hash(new Object[] { Integer.valueOf(i), arrayList, Boolean.valueOf(bool1), Boolean.valueOf(bool2) });
  }
  
  private static boolean isValidSystemApiRule(int paramInt) {
    if (paramInt != 1 && paramInt != 2 && paramInt != 4 && paramInt != 8)
      return false; 
    return true;
  }
  
  private static boolean isValidAttributesSystemApiRule(int paramInt) {
    if (paramInt != 1 && paramInt != 2)
      return false; 
    return true;
  }
  
  private static boolean isValidRule(int paramInt) {
    paramInt = 0xFFFF7FFF & paramInt;
    if (paramInt != 1 && paramInt != 2 && paramInt != 4 && paramInt != 8)
      return false; 
    return true;
  }
  
  private static boolean isPlayerRule(int paramInt) {
    paramInt = 0xFFFF7FFF & paramInt;
    if (paramInt != 1 && paramInt != 4 && paramInt != 8)
      return false; 
    return true;
  }
  
  private static boolean isAudioAttributeRule(int paramInt) {
    if (paramInt != 1 && paramInt != 2)
      return false; 
    return true;
  }
  
  public static class Builder {
    private int mTargetMixType = -1;
    
    private boolean mAllowPrivilegedPlaybackCapture = false;
    
    private boolean mVoiceCommunicationCaptureAllowed = false;
    
    private ArrayList<AudioMixingRule.AudioMixMatchCriterion> mCriteria;
    
    public Builder() {
      this.mCriteria = new ArrayList<>();
    }
    
    public Builder addRule(AudioAttributes param1AudioAttributes, int param1Int) throws IllegalArgumentException {
      if (AudioMixingRule.isValidAttributesSystemApiRule(param1Int))
        return checkAddRuleObjInternal(param1Int, param1AudioAttributes); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Illegal rule value ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder excludeRule(AudioAttributes param1AudioAttributes, int param1Int) throws IllegalArgumentException {
      if (AudioMixingRule.isValidAttributesSystemApiRule(param1Int))
        return checkAddRuleObjInternal(0x8000 | param1Int, param1AudioAttributes); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Illegal rule value ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder addMixRule(int param1Int, Object param1Object) throws IllegalArgumentException {
      if (AudioMixingRule.isValidSystemApiRule(param1Int))
        return checkAddRuleObjInternal(param1Int, param1Object); 
      param1Object = new StringBuilder();
      param1Object.append("Illegal rule value ");
      param1Object.append(param1Int);
      throw new IllegalArgumentException(param1Object.toString());
    }
    
    public Builder excludeMixRule(int param1Int, Object param1Object) throws IllegalArgumentException {
      if (AudioMixingRule.isValidSystemApiRule(param1Int))
        return checkAddRuleObjInternal(0x8000 | param1Int, param1Object); 
      param1Object = new StringBuilder();
      param1Object.append("Illegal rule value ");
      param1Object.append(param1Int);
      throw new IllegalArgumentException(param1Object.toString());
    }
    
    public Builder allowPrivilegedPlaybackCapture(boolean param1Boolean) {
      this.mAllowPrivilegedPlaybackCapture = param1Boolean;
      return this;
    }
    
    public Builder voiceCommunicationCaptureAllowed(boolean param1Boolean) {
      this.mVoiceCommunicationCaptureAllowed = param1Boolean;
      return this;
    }
    
    private Builder checkAddRuleObjInternal(int param1Int, Object param1Object) throws IllegalArgumentException {
      if (param1Object != null) {
        if (AudioMixingRule.isValidRule(param1Int)) {
          if (AudioMixingRule.isAudioAttributeRule(0xFFFF7FFF & param1Int)) {
            if (param1Object instanceof AudioAttributes)
              return addRuleInternal((AudioAttributes)param1Object, null, param1Int); 
            throw new IllegalArgumentException("Invalid AudioAttributes argument");
          } 
          if (param1Object instanceof Integer)
            return addRuleInternal(null, (Integer)param1Object, param1Int); 
          throw new IllegalArgumentException("Invalid Integer argument");
        } 
        param1Object = new StringBuilder();
        param1Object.append("Illegal rule value ");
        param1Object.append(param1Int);
        throw new IllegalArgumentException(param1Object.toString());
      } 
      throw new IllegalArgumentException("Illegal null argument for mixing rule");
    }
    
    private Builder addRuleInternal(AudioAttributes param1AudioAttributes, Integer param1Integer, int param1Int) throws IllegalArgumentException {
      int i = this.mTargetMixType;
      if (i == -1) {
        if (AudioMixingRule.isPlayerRule(param1Int)) {
          this.mTargetMixType = 0;
        } else {
          this.mTargetMixType = 1;
        } 
      } else if ((i == 0 && !AudioMixingRule.isPlayerRule(param1Int)) || (this.mTargetMixType == 1 && AudioMixingRule
        .isPlayerRule(param1Int))) {
        throw new IllegalArgumentException("Incompatible rule for mix");
      } 
      synchronized (this.mCriteria) {
        IllegalStateException illegalStateException;
        StringBuilder stringBuilder;
        Iterator<AudioMixingRule.AudioMixMatchCriterion> iterator = this.mCriteria.iterator();
        i = param1Int & 0xFFFF7FFF;
        while (iterator.hasNext()) {
          IllegalArgumentException illegalArgumentException1, illegalArgumentException2;
          AudioMixingRule.AudioMixMatchCriterion audioMixMatchCriterion1 = iterator.next();
          if ((audioMixMatchCriterion1.mRule & 0xFFFF7FFF) != i)
            continue; 
          if (i != 1) {
            StringBuilder stringBuilder1;
            if (i != 2) {
              if (i != 4) {
                if (i != 8)
                  continue; 
                if (audioMixMatchCriterion1.mIntProp == param1Integer.intValue()) {
                  if (audioMixMatchCriterion1.mRule == param1Int)
                    return this; 
                  illegalArgumentException1 = new IllegalArgumentException();
                  stringBuilder1 = new StringBuilder();
                  this();
                  stringBuilder1.append("Contradictory rule exists for userId ");
                  stringBuilder1.append(param1Integer);
                  this(stringBuilder1.toString());
                  throw illegalArgumentException1;
                } 
                continue;
              } 
              if (((AudioMixingRule.AudioMixMatchCriterion)stringBuilder1).mIntProp == param1Integer.intValue()) {
                if (((AudioMixingRule.AudioMixMatchCriterion)stringBuilder1).mRule == param1Int)
                  return this; 
                illegalArgumentException1 = new IllegalArgumentException();
                stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("Contradictory rule exists for UID ");
                stringBuilder1.append(param1Integer);
                this(stringBuilder1.toString());
                throw illegalArgumentException1;
              } 
              continue;
            } 
            if (((AudioMixingRule.AudioMixMatchCriterion)stringBuilder1).mAttr.getCapturePreset() == illegalArgumentException1.getCapturePreset()) {
              if (((AudioMixingRule.AudioMixMatchCriterion)stringBuilder1).mRule == param1Int)
                return this; 
              illegalArgumentException2 = new IllegalArgumentException();
              stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Contradictory rule exists for ");
              stringBuilder.append(illegalArgumentException1);
              this(stringBuilder.toString());
              throw illegalArgumentException2;
            } 
            continue;
          } 
          if (((AudioMixingRule.AudioMixMatchCriterion)illegalArgumentException2).mAttr.getUsage() == illegalArgumentException1.getUsage()) {
            if (((AudioMixingRule.AudioMixMatchCriterion)illegalArgumentException2).mRule == param1Int)
              return this; 
            illegalArgumentException2 = new IllegalArgumentException();
            stringBuilder = new StringBuilder();
            this();
            stringBuilder.append("Contradictory rule exists for ");
            stringBuilder.append(illegalArgumentException1);
            this(stringBuilder.toString());
            throw illegalArgumentException2;
          } 
        } 
        if (i != 1 && i != 2) {
          if (i == 4 || i == 8) {
            ArrayList<AudioMixingRule.AudioMixMatchCriterion> arrayList1 = this.mCriteria;
            AudioMixingRule.AudioMixMatchCriterion audioMixMatchCriterion1 = new AudioMixingRule.AudioMixMatchCriterion();
            this((Integer)stringBuilder, param1Int);
            arrayList1.add(audioMixMatchCriterion1);
            return this;
          } 
          illegalStateException = new IllegalStateException();
          this("Unreachable code in addRuleInternal()");
          throw illegalStateException;
        } 
        ArrayList<AudioMixingRule.AudioMixMatchCriterion> arrayList = this.mCriteria;
        AudioMixingRule.AudioMixMatchCriterion audioMixMatchCriterion = new AudioMixingRule.AudioMixMatchCriterion();
        this((AudioAttributes)illegalStateException, param1Int);
        arrayList.add(audioMixMatchCriterion);
        return this;
      } 
    }
    
    Builder addRuleFromParcel(Parcel param1Parcel) throws IllegalArgumentException {
      AudioAttributes audioAttributes;
      int i = param1Parcel.readInt();
      int j = 0xFFFF7FFF & i;
      Parcel parcel = null;
      Integer integer = null;
      if (j != 1) {
        StringBuilder stringBuilder;
        if (j != 2) {
          if (j == 4 || j == 8) {
            integer = new Integer(param1Parcel.readInt());
            param1Parcel = parcel;
            return addRuleInternal((AudioAttributes)param1Parcel, integer, i);
          } 
          param1Parcel.readInt();
          stringBuilder = new StringBuilder();
          stringBuilder.append("Illegal rule value ");
          stringBuilder.append(i);
          stringBuilder.append(" in parcel");
          throw new IllegalArgumentException(stringBuilder.toString());
        } 
        j = stringBuilder.readInt();
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        audioAttributes = builder.setInternalCapturePreset(j).build();
      } else {
        j = audioAttributes.readInt();
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        audioAttributes = builder.setUsage(j).build();
      } 
      return addRuleInternal(audioAttributes, integer, i);
    }
    
    public AudioMixingRule build() {
      return new AudioMixingRule(this.mTargetMixType, this.mCriteria, this.mAllowPrivilegedPlaybackCapture, this.mVoiceCommunicationCaptureAllowed);
    }
  }
}
