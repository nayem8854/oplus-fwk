package android.media.audiopolicy;

import android.media.AudioManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Message;
import java.util.ArrayList;

public class AudioVolumeGroupChangeHandler {
  private static final int AUDIOVOLUMEGROUP_EVENT_NEW_LISTENER = 4;
  
  private static final int AUDIOVOLUMEGROUP_EVENT_VOLUME_CHANGED = 1000;
  
  private static final String TAG = "AudioVolumeGroupChangeHandler";
  
  private Handler mHandler;
  
  private HandlerThread mHandlerThread;
  
  private long mJniCallback;
  
  private final ArrayList<AudioManager.VolumeGroupCallback> mListeners = new ArrayList<>();
  
  public void init() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mHandler : Landroid/os/Handler;
    //   6: ifnull -> 12
    //   9: aload_0
    //   10: monitorexit
    //   11: return
    //   12: new android/os/HandlerThread
    //   15: astore_1
    //   16: aload_1
    //   17: ldc 'AudioVolumeGroupChangeHandler'
    //   19: invokespecial <init> : (Ljava/lang/String;)V
    //   22: aload_0
    //   23: aload_1
    //   24: putfield mHandlerThread : Landroid/os/HandlerThread;
    //   27: aload_1
    //   28: invokevirtual start : ()V
    //   31: aload_0
    //   32: getfield mHandlerThread : Landroid/os/HandlerThread;
    //   35: invokevirtual getLooper : ()Landroid/os/Looper;
    //   38: ifnonnull -> 49
    //   41: aload_0
    //   42: aconst_null
    //   43: putfield mHandler : Landroid/os/Handler;
    //   46: aload_0
    //   47: monitorexit
    //   48: return
    //   49: new android/media/audiopolicy/AudioVolumeGroupChangeHandler$1
    //   52: astore_1
    //   53: aload_1
    //   54: aload_0
    //   55: aload_0
    //   56: getfield mHandlerThread : Landroid/os/HandlerThread;
    //   59: invokevirtual getLooper : ()Landroid/os/Looper;
    //   62: invokespecial <init> : (Landroid/media/audiopolicy/AudioVolumeGroupChangeHandler;Landroid/os/Looper;)V
    //   65: aload_0
    //   66: aload_1
    //   67: putfield mHandler : Landroid/os/Handler;
    //   70: new java/lang/ref/WeakReference
    //   73: astore_1
    //   74: aload_1
    //   75: aload_0
    //   76: invokespecial <init> : (Ljava/lang/Object;)V
    //   79: aload_0
    //   80: aload_1
    //   81: invokespecial native_setup : (Ljava/lang/Object;)V
    //   84: aload_0
    //   85: monitorexit
    //   86: return
    //   87: astore_1
    //   88: aload_0
    //   89: monitorexit
    //   90: aload_1
    //   91: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #58	-> 0
    //   #59	-> 2
    //   #60	-> 9
    //   #63	-> 12
    //   #64	-> 27
    //   #66	-> 31
    //   #67	-> 41
    //   #68	-> 46
    //   #70	-> 49
    //   #104	-> 70
    //   #105	-> 84
    //   #106	-> 86
    //   #105	-> 87
    // Exception table:
    //   from	to	target	type
    //   2	9	87	finally
    //   9	11	87	finally
    //   12	27	87	finally
    //   27	31	87	finally
    //   31	41	87	finally
    //   41	46	87	finally
    //   46	48	87	finally
    //   49	70	87	finally
    //   70	84	87	finally
    //   84	86	87	finally
    //   88	90	87	finally
  }
  
  protected void finalize() {
    native_finalize();
    if (this.mHandlerThread.isAlive())
      this.mHandlerThread.quit(); 
  }
  
  public void registerListener(AudioManager.VolumeGroupCallback paramVolumeGroupCallback) {
    // Byte code:
    //   0: aload_1
    //   1: ldc 'volume group callback shall not be null'
    //   3: invokestatic checkNotNull : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mListeners : Ljava/util/ArrayList;
    //   13: aload_1
    //   14: invokevirtual add : (Ljava/lang/Object;)Z
    //   17: pop
    //   18: aload_0
    //   19: monitorexit
    //   20: aload_0
    //   21: getfield mHandler : Landroid/os/Handler;
    //   24: astore_2
    //   25: aload_2
    //   26: ifnull -> 47
    //   29: aload_2
    //   30: iconst_4
    //   31: iconst_0
    //   32: iconst_0
    //   33: aload_1
    //   34: invokevirtual obtainMessage : (IIILjava/lang/Object;)Landroid/os/Message;
    //   37: astore_1
    //   38: aload_0
    //   39: getfield mHandler : Landroid/os/Handler;
    //   42: aload_1
    //   43: invokevirtual sendMessage : (Landroid/os/Message;)Z
    //   46: pop
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #123	-> 0
    //   #124	-> 7
    //   #125	-> 9
    //   #126	-> 18
    //   #127	-> 20
    //   #128	-> 29
    //   #130	-> 38
    //   #132	-> 47
    //   #126	-> 48
    // Exception table:
    //   from	to	target	type
    //   9	18	48	finally
    //   18	20	48	finally
    //   49	51	48	finally
  }
  
  public void unregisterListener(AudioManager.VolumeGroupCallback paramVolumeGroupCallback) {
    // Byte code:
    //   0: aload_1
    //   1: ldc 'volume group callback shall not be null'
    //   3: invokestatic checkNotNull : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   6: pop
    //   7: aload_0
    //   8: monitorenter
    //   9: aload_0
    //   10: getfield mListeners : Ljava/util/ArrayList;
    //   13: aload_1
    //   14: invokevirtual remove : (Ljava/lang/Object;)Z
    //   17: pop
    //   18: aload_0
    //   19: monitorexit
    //   20: return
    //   21: astore_1
    //   22: aload_0
    //   23: monitorexit
    //   24: aload_1
    //   25: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #138	-> 0
    //   #139	-> 7
    //   #140	-> 9
    //   #141	-> 18
    //   #142	-> 20
    //   #141	-> 21
    // Exception table:
    //   from	to	target	type
    //   9	18	21	finally
    //   18	20	21	finally
    //   22	24	21	finally
  }
  
  Handler handler() {
    return this.mHandler;
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = paramObject1;
    paramObject1 = paramObject1.get();
    if (paramObject1 == null)
      return; 
    if (paramObject1 != null) {
      paramObject1 = paramObject1.handler();
      if (paramObject1 != null) {
        paramObject2 = paramObject1.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
        paramObject1.sendMessage((Message)paramObject2);
      } 
    } 
  }
  
  private native void native_finalize();
  
  private native void native_setup(Object paramObject);
}
