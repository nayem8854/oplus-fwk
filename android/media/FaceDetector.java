package android.media;

import android.graphics.Bitmap;
import android.graphics.PointF;
import android.util.Log;

public class FaceDetector {
  public class Face {
    public static final float CONFIDENCE_THRESHOLD = 0.4F;
    
    public static final int EULER_X = 0;
    
    public static final int EULER_Y = 1;
    
    public static final int EULER_Z = 2;
    
    private float mConfidence;
    
    private float mEyesDist;
    
    private float mMidPointX;
    
    private float mMidPointY;
    
    private float mPoseEulerX;
    
    private float mPoseEulerY;
    
    private float mPoseEulerZ;
    
    final FaceDetector this$0;
    
    public float confidence() {
      return this.mConfidence;
    }
    
    public void getMidPoint(PointF param1PointF) {
      param1PointF.set(this.mMidPointX, this.mMidPointY);
    }
    
    public float eyesDistance() {
      return this.mEyesDist;
    }
    
    public float pose(int param1Int) {
      if (param1Int == 0)
        return this.mPoseEulerX; 
      if (param1Int == 1)
        return this.mPoseEulerY; 
      if (param1Int == 2)
        return this.mPoseEulerZ; 
      throw new IllegalArgumentException();
    }
    
    private Face() {}
  }
  
  public FaceDetector(int paramInt1, int paramInt2, int paramInt3) {
    if (!sInitialized)
      return; 
    fft_initialize(paramInt1, paramInt2, paramInt3);
    this.mWidth = paramInt1;
    this.mHeight = paramInt2;
    this.mMaxFaces = paramInt3;
    this.mBWBuffer = new byte[paramInt1 * paramInt2];
  }
  
  public int findFaces(Bitmap paramBitmap, Face[] paramArrayOfFace) {
    if (!sInitialized)
      return 0; 
    if (paramBitmap.getWidth() == this.mWidth && paramBitmap.getHeight() == this.mHeight) {
      if (paramArrayOfFace.length >= this.mMaxFaces) {
        int i = fft_detect(paramBitmap);
        int j = i;
        if (i >= this.mMaxFaces)
          j = this.mMaxFaces; 
        for (i = 0; i < j; i++) {
          if (paramArrayOfFace[i] == null)
            paramArrayOfFace[i] = new Face(); 
          fft_get_face(paramArrayOfFace[i], i);
        } 
        return j;
      } 
      throw new IllegalArgumentException("faces[] smaller than maxFaces");
    } 
    throw new IllegalArgumentException("bitmap size doesn't match initialization");
  }
  
  protected void finalize() throws Throwable {
    fft_destroy();
  }
  
  private static boolean sInitialized = false;
  
  private byte[] mBWBuffer;
  
  private long mDCR;
  
  private long mFD;
  
  private int mHeight;
  
  private int mMaxFaces;
  
  private long mSDK;
  
  private int mWidth;
  
  static {
    try {
      System.loadLibrary("FFTEm");
      nativeClassInit();
      sInitialized = true;
    } catch (UnsatisfiedLinkError unsatisfiedLinkError) {
      Log.d("FFTEm", "face detection library not found!");
    } 
  }
  
  private native void fft_destroy();
  
  private native int fft_detect(Bitmap paramBitmap);
  
  private native void fft_get_face(Face paramFace, int paramInt);
  
  private native int fft_initialize(int paramInt1, int paramInt2, int paramInt3);
  
  private static native void nativeClassInit();
}
