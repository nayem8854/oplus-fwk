package android.media;

import android.hardware.Camera;
import java.util.Arrays;
import java.util.HashMap;

public class CameraProfile {
  public static final int QUALITY_HIGH = 2;
  
  public static final int QUALITY_LOW = 0;
  
  public static final int QUALITY_MEDIUM = 1;
  
  private static final HashMap<Integer, int[]> sCache = (HashMap)new HashMap<>();
  
  public static int getJpegEncodingQualityParameter(int paramInt) {
    int i = Camera.getNumberOfCameras();
    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    for (byte b = 0; b < i; b++) {
      Camera.getCameraInfo(b, cameraInfo);
      if (cameraInfo.facing == 0)
        return getJpegEncodingQualityParameter(b, paramInt); 
    } 
    return 0;
  }
  
  public static int getJpegEncodingQualityParameter(int paramInt1, int paramInt2) {
    if (paramInt2 >= 0 && paramInt2 <= 2)
      synchronized (sCache) {
        int[] arrayOfInt1 = sCache.get(Integer.valueOf(paramInt1));
        int[] arrayOfInt2 = arrayOfInt1;
        if (arrayOfInt1 == null) {
          arrayOfInt2 = getImageEncodingQualityLevels(paramInt1);
          sCache.put(Integer.valueOf(paramInt1), arrayOfInt2);
        } 
        paramInt1 = arrayOfInt2[paramInt2];
        return paramInt1;
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported quality level: ");
    stringBuilder.append(paramInt2);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private static int[] getImageEncodingQualityLevels(int paramInt) {
    int i = native_get_num_image_encoding_quality_levels(paramInt);
    if (i == 3) {
      int[] arrayOfInt = new int[i];
      for (byte b = 0; b < i; b++)
        arrayOfInt[b] = native_get_image_encoding_quality_level(paramInt, b); 
      Arrays.sort(arrayOfInt);
      return arrayOfInt;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unexpected Jpeg encoding quality levels ");
    stringBuilder.append(i);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  private static final native int native_get_image_encoding_quality_level(int paramInt1, int paramInt2);
  
  private static final native int native_get_num_image_encoding_quality_levels(int paramInt);
  
  private static final native void native_init();
}
