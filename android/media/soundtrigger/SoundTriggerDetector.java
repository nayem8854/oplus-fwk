package android.media.soundtrigger;

import android.annotation.SystemApi;
import android.hardware.soundtrigger.IRecognitionStatusCallback;
import android.hardware.soundtrigger.SoundTrigger;
import android.media.AudioFormat;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.Slog;
import com.android.internal.app.ISoundTriggerService;
import java.io.PrintWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.UUID;

@SystemApi
public final class SoundTriggerDetector {
  private static final boolean DBG = false;
  
  private static final int MSG_AVAILABILITY_CHANGED = 1;
  
  private static final int MSG_DETECTION_ERROR = 3;
  
  private static final int MSG_DETECTION_PAUSE = 4;
  
  private static final int MSG_DETECTION_RESUME = 5;
  
  private static final int MSG_SOUND_TRIGGER_DETECTED = 2;
  
  public static final int RECOGNITION_FLAG_ALLOW_MULTIPLE_TRIGGERS = 2;
  
  public static final int RECOGNITION_FLAG_CAPTURE_TRIGGER_AUDIO = 1;
  
  public static final int RECOGNITION_FLAG_ENABLE_AUDIO_ECHO_CANCELLATION = 4;
  
  public static final int RECOGNITION_FLAG_ENABLE_AUDIO_NOISE_SUPPRESSION = 8;
  
  public static final int RECOGNITION_FLAG_NONE = 0;
  
  private static final String TAG = "SoundTriggerDetector";
  
  private final Callback mCallback;
  
  private final Handler mHandler;
  
  private final Object mLock = new Object();
  
  private final RecognitionCallback mRecognitionCallback;
  
  private final UUID mSoundModelId;
  
  private final ISoundTriggerService mSoundTriggerService;
  
  public static class EventPayload {
    private final AudioFormat mAudioFormat;
    
    private final boolean mCaptureAvailable;
    
    private final int mCaptureSession;
    
    private final byte[] mData;
    
    private final boolean mTriggerAvailable;
    
    private EventPayload(boolean param1Boolean1, boolean param1Boolean2, AudioFormat param1AudioFormat, int param1Int, byte[] param1ArrayOfbyte) {
      this.mTriggerAvailable = param1Boolean1;
      this.mCaptureAvailable = param1Boolean2;
      this.mCaptureSession = param1Int;
      this.mAudioFormat = param1AudioFormat;
      this.mData = param1ArrayOfbyte;
    }
    
    public AudioFormat getCaptureAudioFormat() {
      return this.mAudioFormat;
    }
    
    public byte[] getTriggerAudio() {
      if (this.mTriggerAvailable)
        return this.mData; 
      return null;
    }
    
    public byte[] getData() {
      if (!this.mTriggerAvailable)
        return this.mData; 
      return null;
    }
    
    public Integer getCaptureSession() {
      if (this.mCaptureAvailable)
        return Integer.valueOf(this.mCaptureSession); 
      return null;
    }
  }
  
  public static abstract class Callback {
    public abstract void onAvailabilityChanged(int param1Int);
    
    public abstract void onDetected(SoundTriggerDetector.EventPayload param1EventPayload);
    
    public abstract void onError();
    
    public abstract void onRecognitionPaused();
    
    public abstract void onRecognitionResumed();
  }
  
  SoundTriggerDetector(ISoundTriggerService paramISoundTriggerService, UUID paramUUID, Callback paramCallback, Handler paramHandler) {
    this.mSoundTriggerService = paramISoundTriggerService;
    this.mSoundModelId = paramUUID;
    this.mCallback = paramCallback;
    if (paramHandler == null) {
      this.mHandler = new MyHandler();
    } else {
      this.mHandler = new MyHandler(paramHandler.getLooper());
    } 
    this.mRecognitionCallback = new RecognitionCallback();
  }
  
  public boolean startRecognition(int paramInt) {
    boolean bool2, bool3, bool1 = false;
    if ((paramInt & 0x1) != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    if ((paramInt & 0x2) != 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    int i = 0;
    if ((paramInt & 0x4) != 0)
      i = false | true; 
    int j = i;
    if ((paramInt & 0x8) != 0)
      j = i | 0x2; 
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(this.mSoundModelId);
      RecognitionCallback recognitionCallback = this.mRecognitionCallback;
      SoundTrigger.RecognitionConfig recognitionConfig = new SoundTrigger.RecognitionConfig();
      this(bool2, bool3, null, null, j);
      paramInt = iSoundTriggerService.startRecognition(parcelUuid, recognitionCallback, recognitionConfig);
      bool2 = bool1;
      if (paramInt == 0)
        bool2 = true; 
      return bool2;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public boolean stopRecognition() {
    boolean bool = false;
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(this.mSoundModelId);
      int i = iSoundTriggerService.stopRecognition(parcelUuid, this.mRecognitionCallback);
      if (i == 0)
        bool = true; 
      return bool;
    } catch (RemoteException remoteException) {
      return false;
    } 
  }
  
  public void dump(String paramString, PrintWriter paramPrintWriter) {
    synchronized (this.mLock) {
      return;
    } 
  }
  
  class RecognitionCallback extends IRecognitionStatusCallback.Stub {
    final SoundTriggerDetector this$0;
    
    private RecognitionCallback() {}
    
    public void onGenericSoundTriggerDetected(SoundTrigger.GenericRecognitionEvent param1GenericRecognitionEvent) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onGenericSoundTriggerDetected()");
      stringBuilder.append(param1GenericRecognitionEvent);
      Slog.d("SoundTriggerDetector", stringBuilder.toString());
      Message message = Message.obtain(SoundTriggerDetector.this.mHandler, 2, new SoundTriggerDetector.EventPayload(param1GenericRecognitionEvent.triggerInData, param1GenericRecognitionEvent.captureAvailable, param1GenericRecognitionEvent.captureFormat, param1GenericRecognitionEvent.captureSession, param1GenericRecognitionEvent.data));
      message.sendToTarget();
    }
    
    public void onKeyphraseDetected(SoundTrigger.KeyphraseRecognitionEvent param1KeyphraseRecognitionEvent) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ignoring onKeyphraseDetected() called for ");
      stringBuilder.append(param1KeyphraseRecognitionEvent);
      Slog.e("SoundTriggerDetector", stringBuilder.toString());
    }
    
    public void onError(int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("onError()");
      stringBuilder.append(param1Int);
      Slog.d("SoundTriggerDetector", stringBuilder.toString());
      SoundTriggerDetector.this.mHandler.sendEmptyMessage(3);
    }
    
    public void onRecognitionPaused() {
      Slog.d("SoundTriggerDetector", "onRecognitionPaused()");
      SoundTriggerDetector.this.mHandler.sendEmptyMessage(4);
    }
    
    public void onRecognitionResumed() {
      Slog.d("SoundTriggerDetector", "onRecognitionResumed()");
      SoundTriggerDetector.this.mHandler.sendEmptyMessage(5);
    }
  }
  
  class MyHandler extends Handler {
    final SoundTriggerDetector this$0;
    
    MyHandler() {}
    
    MyHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (SoundTriggerDetector.this.mCallback == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Received message: ");
        stringBuilder.append(param1Message.what);
        stringBuilder.append(" for NULL callback.");
        Slog.w("SoundTriggerDetector", stringBuilder.toString());
        return;
      } 
      int i = param1Message.what;
      if (i != 2) {
        if (i != 3) {
          if (i != 4) {
            if (i != 5) {
              super.handleMessage(param1Message);
            } else {
              SoundTriggerDetector.this.mCallback.onRecognitionResumed();
            } 
          } else {
            SoundTriggerDetector.this.mCallback.onRecognitionPaused();
          } 
        } else {
          SoundTriggerDetector.this.mCallback.onError();
        } 
      } else {
        SoundTriggerDetector.this.mCallback.onDetected((SoundTriggerDetector.EventPayload)param1Message.obj);
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RecognitionFlags {}
}
