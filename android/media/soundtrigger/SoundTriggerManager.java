package android.media.soundtrigger;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.hardware.soundtrigger.SoundTrigger;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.provider.Settings;
import android.util.Slog;
import com.android.internal.app.ISoundTriggerService;
import com.android.internal.util.Preconditions;
import java.util.HashMap;
import java.util.Objects;
import java.util.UUID;

@SystemApi
public final class SoundTriggerManager {
  private static final boolean DBG = false;
  
  public static final String EXTRA_MESSAGE_TYPE = "android.media.soundtrigger.MESSAGE_TYPE";
  
  public static final String EXTRA_RECOGNITION_EVENT = "android.media.soundtrigger.RECOGNITION_EVENT";
  
  public static final String EXTRA_STATUS = "android.media.soundtrigger.STATUS";
  
  public static final int FLAG_MESSAGE_TYPE_RECOGNITION_ERROR = 1;
  
  public static final int FLAG_MESSAGE_TYPE_RECOGNITION_EVENT = 0;
  
  public static final int FLAG_MESSAGE_TYPE_RECOGNITION_PAUSED = 2;
  
  public static final int FLAG_MESSAGE_TYPE_RECOGNITION_RESUMED = 3;
  
  public static final int FLAG_MESSAGE_TYPE_UNKNOWN = -1;
  
  private static final String TAG = "SoundTriggerManager";
  
  private final Context mContext;
  
  private final HashMap<UUID, SoundTriggerDetector> mReceiverInstanceMap;
  
  private final ISoundTriggerService mSoundTriggerService;
  
  public SoundTriggerManager(Context paramContext, ISoundTriggerService paramISoundTriggerService) {
    this.mSoundTriggerService = paramISoundTriggerService;
    this.mContext = paramContext;
    this.mReceiverInstanceMap = new HashMap<>();
  }
  
  public void updateModel(Model paramModel) {
    try {
      this.mSoundTriggerService.updateSoundModel(paramModel.getGenericSoundModel());
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public Model getModel(UUID paramUUID) {
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      SoundTrigger.GenericSoundModel genericSoundModel = iSoundTriggerService.getSoundModel(parcelUuid);
      if (genericSoundModel == null)
        return null; 
      return new Model(genericSoundModel);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void deleteModel(UUID paramUUID) {
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      iSoundTriggerService.deleteSoundModel(parcelUuid);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SoundTriggerDetector createSoundTriggerDetector(UUID paramUUID, SoundTriggerDetector.Callback paramCallback, Handler paramHandler) {
    if (paramUUID == null)
      return null; 
    SoundTriggerDetector soundTriggerDetector2 = this.mReceiverInstanceMap.get(paramUUID);
    SoundTriggerDetector soundTriggerDetector1 = new SoundTriggerDetector(this.mSoundTriggerService, paramUUID, paramCallback, paramHandler);
    this.mReceiverInstanceMap.put(paramUUID, soundTriggerDetector1);
    return soundTriggerDetector1;
  }
  
  public static class Model {
    private SoundTrigger.GenericSoundModel mGenericSoundModel;
    
    Model(SoundTrigger.GenericSoundModel param1GenericSoundModel) {
      this.mGenericSoundModel = param1GenericSoundModel;
    }
    
    public static Model create(UUID param1UUID1, UUID param1UUID2, byte[] param1ArrayOfbyte, int param1Int) {
      Objects.requireNonNull(param1UUID1);
      Objects.requireNonNull(param1UUID2);
      return new Model(new SoundTrigger.GenericSoundModel(param1UUID1, param1UUID2, param1ArrayOfbyte, param1Int));
    }
    
    public static Model create(UUID param1UUID1, UUID param1UUID2, byte[] param1ArrayOfbyte) {
      return create(param1UUID1, param1UUID2, param1ArrayOfbyte, -1);
    }
    
    public UUID getModelUuid() {
      return this.mGenericSoundModel.getUuid();
    }
    
    public UUID getVendorUuid() {
      return this.mGenericSoundModel.getVendorUuid();
    }
    
    public int getVersion() {
      return this.mGenericSoundModel.getVersion();
    }
    
    public byte[] getModelData() {
      return this.mGenericSoundModel.getData();
    }
    
    SoundTrigger.GenericSoundModel getGenericSoundModel() {
      return this.mGenericSoundModel;
    }
  }
  
  public int loadSoundModel(SoundTrigger.SoundModel paramSoundModel) {
    if (paramSoundModel == null)
      return Integer.MIN_VALUE; 
    try {
      null = paramSoundModel.getType();
      if (null != 0) {
        if (null != 1) {
          Slog.e("SoundTriggerManager", "Unkown model type");
          return Integer.MIN_VALUE;
        } 
        return this.mSoundTriggerService.loadGenericSoundModel((SoundTrigger.GenericSoundModel)paramSoundModel);
      } 
      return this.mSoundTriggerService.loadKeyphraseSoundModel((SoundTrigger.KeyphraseSoundModel)paramSoundModel);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int startRecognition(UUID paramUUID, Bundle paramBundle, ComponentName paramComponentName, SoundTrigger.RecognitionConfig paramRecognitionConfig) {
    Preconditions.checkNotNull(paramUUID);
    Preconditions.checkNotNull(paramComponentName);
    Preconditions.checkNotNull(paramRecognitionConfig);
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.startRecognitionForService(parcelUuid, paramBundle, paramComponentName, paramRecognitionConfig);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int stopRecognition(UUID paramUUID) {
    if (paramUUID == null)
      return Integer.MIN_VALUE; 
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.stopRecognitionForService(parcelUuid);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int unloadSoundModel(UUID paramUUID) {
    if (paramUUID == null)
      return Integer.MIN_VALUE; 
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.unloadSoundModel(parcelUuid);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isRecognitionActive(UUID paramUUID) {
    if (paramUUID == null)
      return false; 
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.isRecognitionActive(parcelUuid);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getDetectionServiceOperationsTimeout() {
    try {
      return Settings.Global.getInt(this.mContext.getContentResolver(), "sound_trigger_detection_service_op_timeout");
    } catch (android.provider.Settings.SettingNotFoundException settingNotFoundException) {
      return Integer.MAX_VALUE;
    } 
  }
  
  public int getModelState(UUID paramUUID) {
    if (paramUUID == null)
      return Integer.MIN_VALUE; 
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.getModelState(parcelUuid);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SoundTrigger.ModuleProperties getModuleProperties() {
    try {
      return this.mSoundTriggerService.getModuleProperties();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int setParameter(UUID paramUUID, int paramInt1, int paramInt2) {
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.setParameter(parcelUuid, paramInt1, paramInt2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getParameter(UUID paramUUID, int paramInt) {
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.getParameter(parcelUuid, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public SoundTrigger.ModelParamRange queryParameter(UUID paramUUID, int paramInt) {
    try {
      ISoundTriggerService iSoundTriggerService = this.mSoundTriggerService;
      ParcelUuid parcelUuid = new ParcelUuid();
      this(paramUUID);
      return iSoundTriggerService.queryParameter(parcelUuid, paramInt);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
}
