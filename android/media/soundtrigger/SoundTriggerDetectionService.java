package android.media.soundtrigger;

import android.annotation.SystemApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.soundtrigger.SoundTrigger;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.ParcelUuid;
import android.os.RemoteException;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.QuintConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.UUID;

@SystemApi
public abstract class SoundTriggerDetectionService extends Service {
  private static final String LOG_TAG = SoundTriggerDetectionService.class.getSimpleName();
  
  private final Object mLock = new Object();
  
  private final ArrayMap<UUID, ISoundTriggerDetectionServiceClient> mClients = new ArrayMap();
  
  private static final boolean DEBUG = false;
  
  private Handler mHandler;
  
  protected final void attachBaseContext(Context paramContext) {
    super.attachBaseContext(paramContext);
    this.mHandler = new Handler(paramContext.getMainLooper());
  }
  
  private void setClient(UUID paramUUID, Bundle paramBundle, ISoundTriggerDetectionServiceClient paramISoundTriggerDetectionServiceClient) {
    synchronized (this.mLock) {
      this.mClients.put(paramUUID, paramISoundTriggerDetectionServiceClient);
      onConnected(paramUUID, paramBundle);
      return;
    } 
  }
  
  private void removeClient(UUID paramUUID, Bundle paramBundle) {
    synchronized (this.mLock) {
      this.mClients.remove(paramUUID);
      onDisconnected(paramUUID, paramBundle);
      return;
    } 
  }
  
  public void onConnected(UUID paramUUID, Bundle paramBundle) {}
  
  public void onDisconnected(UUID paramUUID, Bundle paramBundle) {}
  
  public void onGenericRecognitionEvent(UUID paramUUID, Bundle paramBundle, int paramInt, SoundTrigger.RecognitionEvent paramRecognitionEvent) {
    operationFinished(paramUUID, paramInt);
  }
  
  public void onError(UUID paramUUID, Bundle paramBundle, int paramInt1, int paramInt2) {
    operationFinished(paramUUID, paramInt1);
  }
  
  public final void operationFinished(UUID paramUUID, int paramInt) {
    try {
      synchronized (this.mLock) {
        StringBuilder stringBuilder;
        ISoundTriggerDetectionServiceClient iSoundTriggerDetectionServiceClient = (ISoundTriggerDetectionServiceClient)this.mClients.get(paramUUID);
        if (iSoundTriggerDetectionServiceClient == null) {
          String str = LOG_TAG;
          stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("operationFinished called, but no client for ");
          stringBuilder.append(paramUUID);
          stringBuilder.append(". Was this called after onDisconnected?");
          Log.w(str, stringBuilder.toString());
          return;
        } 
        stringBuilder.onOpFinished(paramInt);
      } 
    } catch (RemoteException remoteException) {
      String str = LOG_TAG;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("operationFinished, remote exception for client ");
      stringBuilder.append(paramUUID);
      Log.e(str, stringBuilder.toString(), (Throwable)remoteException);
    } 
  }
  
  public final IBinder onBind(Intent paramIntent) {
    return new ISoundTriggerDetectionService.Stub() {
        private final Object mBinderLock = new Object();
        
        public final ArrayMap<UUID, Bundle> mParams = new ArrayMap();
        
        final SoundTriggerDetectionService this$0;
        
        public void setClient(ParcelUuid param1ParcelUuid, Bundle param1Bundle, ISoundTriggerDetectionServiceClient param1ISoundTriggerDetectionServiceClient) {
          UUID uUID = param1ParcelUuid.getUuid();
          synchronized (this.mBinderLock) {
            this.mParams.put(uUID, param1Bundle);
            SoundTriggerDetectionService.this.mHandler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)_$$Lambda$SoundTriggerDetectionService$1$LlOo7TiZplZCgGhS07DqYHocFcw.INSTANCE, SoundTriggerDetectionService.this, uUID, param1Bundle, param1ISoundTriggerDetectionServiceClient));
            return;
          } 
        }
        
        public void removeClient(ParcelUuid param1ParcelUuid) {
          UUID uUID = param1ParcelUuid.getUuid();
          synchronized (this.mBinderLock) {
            Bundle bundle = (Bundle)this.mParams.remove(uUID);
            SoundTriggerDetectionService.this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$SoundTriggerDetectionService$1$pKR4r0FzOzoVczcnvLQIZNjkZZw.INSTANCE, SoundTriggerDetectionService.this, uUID, bundle));
            return;
          } 
        }
        
        public void onGenericRecognitionEvent(ParcelUuid param1ParcelUuid, int param1Int, SoundTrigger.GenericRecognitionEvent param1GenericRecognitionEvent) {
          UUID uUID = param1ParcelUuid.getUuid();
          synchronized (this.mBinderLock) {
            Bundle bundle = (Bundle)this.mParams.get(uUID);
            null = SoundTriggerDetectionService.this.mHandler;
            -$.Lambda.ISQYIYPBRBIOLBUJy7rrJW-SiJg iSQYIYPBRBIOLBUJy7rrJW-SiJg = _$$Lambda$ISQYIYPBRBIOLBUJy7rrJW_SiJg.INSTANCE;
            SoundTriggerDetectionService soundTriggerDetectionService = SoundTriggerDetectionService.this;
            Message message = PooledLambda.obtainMessage((QuintConsumer)iSQYIYPBRBIOLBUJy7rrJW-SiJg, soundTriggerDetectionService, uUID, bundle, Integer.valueOf(param1Int), param1GenericRecognitionEvent);
            null.sendMessage(message);
            return;
          } 
        }
        
        public void onError(ParcelUuid param1ParcelUuid, int param1Int1, int param1Int2) {
          UUID uUID = param1ParcelUuid.getUuid();
          synchronized (this.mBinderLock) {
            Bundle bundle = (Bundle)this.mParams.get(uUID);
            Handler handler = SoundTriggerDetectionService.this.mHandler;
            null = _$$Lambda$oNgT3sYhSGVWlnU92bECo_ULGeY.INSTANCE;
            SoundTriggerDetectionService soundTriggerDetectionService = SoundTriggerDetectionService.this;
            handler.sendMessage(PooledLambda.obtainMessage((QuintConsumer)null, soundTriggerDetectionService, uUID, bundle, Integer.valueOf(param1Int1), Integer.valueOf(param1Int2)));
            return;
          } 
        }
        
        public void onStopOperation(ParcelUuid param1ParcelUuid, int param1Int) {
          null = param1ParcelUuid.getUuid();
          synchronized (this.mBinderLock) {
            Bundle bundle = (Bundle)this.mParams.get(null);
            Handler handler = SoundTriggerDetectionService.this.mHandler;
            null = _$$Lambda$bPGNpvkCtpPW14oaI3pxn1e6JtQ.INSTANCE;
            SoundTriggerDetectionService soundTriggerDetectionService = SoundTriggerDetectionService.this;
            handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)null, soundTriggerDetectionService, null, bundle, Integer.valueOf(param1Int)));
            return;
          } 
        }
      };
  }
  
  public boolean onUnbind(Intent paramIntent) {
    this.mClients.clear();
    return false;
  }
  
  public abstract void onStopOperation(UUID paramUUID, Bundle paramBundle, int paramInt);
}
