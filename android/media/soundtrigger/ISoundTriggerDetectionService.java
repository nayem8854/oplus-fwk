package android.media.soundtrigger;

import android.hardware.soundtrigger.SoundTrigger;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelUuid;
import android.os.RemoteException;

public interface ISoundTriggerDetectionService extends IInterface {
  void onError(ParcelUuid paramParcelUuid, int paramInt1, int paramInt2) throws RemoteException;
  
  void onGenericRecognitionEvent(ParcelUuid paramParcelUuid, int paramInt, SoundTrigger.GenericRecognitionEvent paramGenericRecognitionEvent) throws RemoteException;
  
  void onStopOperation(ParcelUuid paramParcelUuid, int paramInt) throws RemoteException;
  
  void removeClient(ParcelUuid paramParcelUuid) throws RemoteException;
  
  void setClient(ParcelUuid paramParcelUuid, Bundle paramBundle, ISoundTriggerDetectionServiceClient paramISoundTriggerDetectionServiceClient) throws RemoteException;
  
  class Default implements ISoundTriggerDetectionService {
    public void setClient(ParcelUuid param1ParcelUuid, Bundle param1Bundle, ISoundTriggerDetectionServiceClient param1ISoundTriggerDetectionServiceClient) throws RemoteException {}
    
    public void removeClient(ParcelUuid param1ParcelUuid) throws RemoteException {}
    
    public void onGenericRecognitionEvent(ParcelUuid param1ParcelUuid, int param1Int, SoundTrigger.GenericRecognitionEvent param1GenericRecognitionEvent) throws RemoteException {}
    
    public void onError(ParcelUuid param1ParcelUuid, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onStopOperation(ParcelUuid param1ParcelUuid, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISoundTriggerDetectionService {
    private static final String DESCRIPTOR = "android.media.soundtrigger.ISoundTriggerDetectionService";
    
    static final int TRANSACTION_onError = 4;
    
    static final int TRANSACTION_onGenericRecognitionEvent = 3;
    
    static final int TRANSACTION_onStopOperation = 5;
    
    static final int TRANSACTION_removeClient = 2;
    
    static final int TRANSACTION_setClient = 1;
    
    public Stub() {
      attachInterface(this, "android.media.soundtrigger.ISoundTriggerDetectionService");
    }
    
    public static ISoundTriggerDetectionService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
      if (iInterface != null && iInterface instanceof ISoundTriggerDetectionService)
        return (ISoundTriggerDetectionService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onStopOperation";
            } 
            return "onError";
          } 
          return "onGenericRecognitionEvent";
        } 
        return "removeClient";
      } 
      return "setClient";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Bundle bundle;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.media.soundtrigger.ISoundTriggerDetectionService");
                return true;
              } 
              param1Parcel1.enforceInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
              if (param1Parcel1.readInt() != 0) {
                ParcelUuid parcelUuid = ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel2 = null;
              } 
              param1Int1 = param1Parcel1.readInt();
              onStopOperation((ParcelUuid)param1Parcel2, param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
            if (param1Parcel1.readInt() != 0) {
              ParcelUuid parcelUuid = ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onError((ParcelUuid)param1Parcel2, param1Int1, param1Int2);
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param1Parcel1.readInt() != 0) {
            ParcelUuid parcelUuid = ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel2 = null;
          } 
          param1Int1 = param1Parcel1.readInt();
          if (param1Parcel1.readInt() != 0) {
            SoundTrigger.GenericRecognitionEvent genericRecognitionEvent = SoundTrigger.GenericRecognitionEvent.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onGenericRecognitionEvent((ParcelUuid)param1Parcel2, param1Int1, (SoundTrigger.GenericRecognitionEvent)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
        if (param1Parcel1.readInt() != 0) {
          ParcelUuid parcelUuid = ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        removeClient((ParcelUuid)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.soundtrigger.ISoundTriggerDetectionService");
      if (param1Parcel1.readInt() != 0) {
        ParcelUuid parcelUuid = ParcelUuid.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
      } else {
        bundle = null;
      } 
      ISoundTriggerDetectionServiceClient iSoundTriggerDetectionServiceClient = ISoundTriggerDetectionServiceClient.Stub.asInterface(param1Parcel1.readStrongBinder());
      setClient((ParcelUuid)param1Parcel2, bundle, iSoundTriggerDetectionServiceClient);
      return true;
    }
    
    private static class Proxy implements ISoundTriggerDetectionService {
      public static ISoundTriggerDetectionService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.soundtrigger.ISoundTriggerDetectionService";
      }
      
      public void setClient(ParcelUuid param2ParcelUuid, Bundle param2Bundle, ISoundTriggerDetectionServiceClient param2ISoundTriggerDetectionServiceClient) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param2ParcelUuid != null) {
            parcel.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ISoundTriggerDetectionServiceClient != null) {
            iBinder = param2ISoundTriggerDetectionServiceClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerDetectionService.Stub.getDefaultImpl() != null) {
            ISoundTriggerDetectionService.Stub.getDefaultImpl().setClient(param2ParcelUuid, param2Bundle, param2ISoundTriggerDetectionServiceClient);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeClient(ParcelUuid param2ParcelUuid) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param2ParcelUuid != null) {
            parcel.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerDetectionService.Stub.getDefaultImpl() != null) {
            ISoundTriggerDetectionService.Stub.getDefaultImpl().removeClient(param2ParcelUuid);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onGenericRecognitionEvent(ParcelUuid param2ParcelUuid, int param2Int, SoundTrigger.GenericRecognitionEvent param2GenericRecognitionEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param2ParcelUuid != null) {
            parcel.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          if (param2GenericRecognitionEvent != null) {
            parcel.writeInt(1);
            param2GenericRecognitionEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerDetectionService.Stub.getDefaultImpl() != null) {
            ISoundTriggerDetectionService.Stub.getDefaultImpl().onGenericRecognitionEvent(param2ParcelUuid, param2Int, param2GenericRecognitionEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(ParcelUuid param2ParcelUuid, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param2ParcelUuid != null) {
            parcel.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerDetectionService.Stub.getDefaultImpl() != null) {
            ISoundTriggerDetectionService.Stub.getDefaultImpl().onError(param2ParcelUuid, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStopOperation(ParcelUuid param2ParcelUuid, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.soundtrigger.ISoundTriggerDetectionService");
          if (param2ParcelUuid != null) {
            parcel.writeInt(1);
            param2ParcelUuid.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ISoundTriggerDetectionService.Stub.getDefaultImpl() != null) {
            ISoundTriggerDetectionService.Stub.getDefaultImpl().onStopOperation(param2ParcelUuid, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISoundTriggerDetectionService param1ISoundTriggerDetectionService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISoundTriggerDetectionService != null) {
          Proxy.sDefaultImpl = param1ISoundTriggerDetectionService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISoundTriggerDetectionService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
