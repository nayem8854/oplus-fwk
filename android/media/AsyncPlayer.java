package android.media;

import android.content.Context;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;
import java.util.LinkedList;

public class AsyncPlayer {
  private static final int PLAY = 1;
  
  private static final int STOP = 2;
  
  private static final boolean mDebug = false;
  
  private static final class Command {
    AudioAttributes attributes;
    
    int code;
    
    Context context;
    
    boolean looping;
    
    long requestTime;
    
    Uri uri;
    
    private Command() {}
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("{ code=");
      stringBuilder.append(this.code);
      stringBuilder.append(" looping=");
      stringBuilder.append(this.looping);
      stringBuilder.append(" attr=");
      stringBuilder.append(this.attributes);
      stringBuilder.append(" uri=");
      stringBuilder.append(this.uri);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
  }
  
  private final LinkedList<Command> mCmdQueue = new LinkedList<>();
  
  private MediaPlayer mPlayer;
  
  private void startSound(Command paramCommand) {
    try {
      MediaPlayer mediaPlayer = new MediaPlayer();
      this();
      mediaPlayer.setAudioAttributes(paramCommand.attributes);
      mediaPlayer.setDataSource(paramCommand.context, paramCommand.uri);
      mediaPlayer.setLooping(paramCommand.looping);
      mediaPlayer.prepare();
      mediaPlayer.start();
      if (this.mPlayer != null)
        this.mPlayer.release(); 
      this.mPlayer = mediaPlayer;
      long l = SystemClock.uptimeMillis() - paramCommand.requestTime;
      if (l > 1000L) {
        String str = this.mTag;
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Notification sound delayed by ");
        stringBuilder.append(l);
        stringBuilder.append("msecs");
        Log.w(str, stringBuilder.toString());
      } 
    } catch (Exception exception) {
      String str = this.mTag;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error loading sound for ");
      stringBuilder.append(paramCommand.uri);
      Log.w(str, stringBuilder.toString(), exception);
    } 
  }
  
  private final class Thread extends Thread {
    final AsyncPlayer this$0;
    
    Thread() {
      super(stringBuilder.toString());
    }
    
    public void run() {
      while (true) {
        synchronized (AsyncPlayer.this.mCmdQueue) {
          AsyncPlayer.Command command = AsyncPlayer.this.mCmdQueue.removeFirst();
          int i = command.code;
          if (i != 1) {
            if (i == 2)
              if (AsyncPlayer.this.mPlayer != null) {
                long l = SystemClock.uptimeMillis() - command.requestTime;
                if (l > 1000L) {
                  String str = AsyncPlayer.this.mTag;
                  null = new StringBuilder();
                  null.append("Notification stop delayed by ");
                  null.append(l);
                  null.append("msecs");
                  Log.w(str, null.toString());
                } 
                AsyncPlayer.this.mPlayer.stop();
                AsyncPlayer.this.mPlayer.release();
                AsyncPlayer.access$302(AsyncPlayer.this, null);
              } else {
                Log.w(AsyncPlayer.this.mTag, "STOP command without a player");
              }  
          } else {
            AsyncPlayer.this.startSound((AsyncPlayer.Command)null);
          } 
          synchronized (AsyncPlayer.this.mCmdQueue) {
            if (AsyncPlayer.this.mCmdQueue.size() == 0) {
              AsyncPlayer.access$402(AsyncPlayer.this, null);
              AsyncPlayer.this.releaseWakeLock();
              return;
            } 
          } 
        } 
      } 
    }
  }
  
  private int mState = 2;
  
  private String mTag;
  
  private Thread mThread;
  
  private PowerManager.WakeLock mWakeLock;
  
  public AsyncPlayer(String paramString) {
    if (paramString != null) {
      this.mTag = paramString;
    } else {
      this.mTag = "AsyncPlayer";
    } 
  }
  
  public void play(Context paramContext, Uri paramUri, boolean paramBoolean, int paramInt) {
    PlayerBase.deprecateStreamTypeForPlayback(paramInt, "AsyncPlayer", "play()");
    if (paramContext == null || paramUri == null)
      return; 
    try {
      AudioAttributes.Builder builder = new AudioAttributes.Builder();
      this();
      AudioAttributes audioAttributes = builder.setInternalLegacyStreamType(paramInt).build();
      play(paramContext, paramUri, paramBoolean, audioAttributes);
    } catch (IllegalArgumentException illegalArgumentException) {
      Log.e(this.mTag, "Call to deprecated AsyncPlayer.play() method caused:", illegalArgumentException);
    } 
  }
  
  public void play(Context paramContext, Uri paramUri, boolean paramBoolean, AudioAttributes paramAudioAttributes) throws IllegalArgumentException {
    if (paramContext != null && paramUri != null && paramAudioAttributes != null) {
      Command command = new Command();
      command.requestTime = SystemClock.uptimeMillis();
      command.code = 1;
      command.context = paramContext;
      command.uri = paramUri;
      command.looping = paramBoolean;
      command.attributes = paramAudioAttributes;
      synchronized (this.mCmdQueue) {
        enqueueLocked(command);
        this.mState = 1;
        return;
      } 
    } 
    throw new IllegalArgumentException("Illegal null AsyncPlayer.play() argument");
  }
  
  public void stop() {
    synchronized (this.mCmdQueue) {
      if (this.mState != 2) {
        Command command = new Command();
        this();
        command.requestTime = SystemClock.uptimeMillis();
        command.code = 2;
        enqueueLocked(command);
        this.mState = 2;
      } 
      return;
    } 
  }
  
  private void enqueueLocked(Command paramCommand) {
    this.mCmdQueue.add(paramCommand);
    if (this.mThread == null) {
      acquireWakeLock();
      Thread thread = new Thread();
      thread.start();
    } 
  }
  
  public void setUsesWakeLock(Context paramContext) {
    if (this.mWakeLock == null && this.mThread == null) {
      PowerManager powerManager = (PowerManager)paramContext.getSystemService("power");
      this.mWakeLock = powerManager.newWakeLock(1, this.mTag);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("assertion failed mWakeLock=");
    stringBuilder.append(this.mWakeLock);
    stringBuilder.append(" mThread=");
    stringBuilder.append(this.mThread);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  private void acquireWakeLock() {
    PowerManager.WakeLock wakeLock = this.mWakeLock;
    if (wakeLock != null)
      wakeLock.acquire(); 
  }
  
  private void releaseWakeLock() {
    PowerManager.WakeLock wakeLock = this.mWakeLock;
    if (wakeLock != null)
      wakeLock.release(); 
  }
}
