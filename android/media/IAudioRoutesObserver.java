package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAudioRoutesObserver extends IInterface {
  void dispatchAudioRoutesChanged(AudioRoutesInfo paramAudioRoutesInfo) throws RemoteException;
  
  class Default implements IAudioRoutesObserver {
    public void dispatchAudioRoutesChanged(AudioRoutesInfo param1AudioRoutesInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAudioRoutesObserver {
    private static final String DESCRIPTOR = "android.media.IAudioRoutesObserver";
    
    static final int TRANSACTION_dispatchAudioRoutesChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IAudioRoutesObserver");
    }
    
    public static IAudioRoutesObserver asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IAudioRoutesObserver");
      if (iInterface != null && iInterface instanceof IAudioRoutesObserver)
        return (IAudioRoutesObserver)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "dispatchAudioRoutesChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IAudioRoutesObserver");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IAudioRoutesObserver");
      if (param1Parcel1.readInt() != 0) {
        AudioRoutesInfo audioRoutesInfo = AudioRoutesInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      dispatchAudioRoutesChanged((AudioRoutesInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IAudioRoutesObserver {
      public static IAudioRoutesObserver sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IAudioRoutesObserver";
      }
      
      public void dispatchAudioRoutesChanged(AudioRoutesInfo param2AudioRoutesInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioRoutesObserver");
          if (param2AudioRoutesInfo != null) {
            parcel.writeInt(1);
            param2AudioRoutesInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAudioRoutesObserver.Stub.getDefaultImpl() != null) {
            IAudioRoutesObserver.Stub.getDefaultImpl().dispatchAudioRoutesChanged(param2AudioRoutesInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAudioRoutesObserver param1IAudioRoutesObserver) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAudioRoutesObserver != null) {
          Proxy.sDefaultImpl = param1IAudioRoutesObserver;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAudioRoutesObserver getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
