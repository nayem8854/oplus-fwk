package android.media.session;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.SystemClock;
import android.text.TextUtils;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public final class PlaybackState implements Parcelable {
  public static final long ACTION_FAST_FORWARD = 64L;
  
  public static final long ACTION_PAUSE = 2L;
  
  public static final long ACTION_PLAY = 4L;
  
  public static final long ACTION_PLAY_FROM_MEDIA_ID = 1024L;
  
  public static final long ACTION_PLAY_FROM_SEARCH = 2048L;
  
  public static final long ACTION_PLAY_FROM_URI = 8192L;
  
  public static final long ACTION_PLAY_PAUSE = 512L;
  
  public static final long ACTION_PREPARE = 16384L;
  
  public static final long ACTION_PREPARE_FROM_MEDIA_ID = 32768L;
  
  public static final long ACTION_PREPARE_FROM_SEARCH = 65536L;
  
  public static final long ACTION_PREPARE_FROM_URI = 131072L;
  
  public static final long ACTION_REWIND = 8L;
  
  public static final long ACTION_SEEK_TO = 256L;
  
  public static final long ACTION_SET_RATING = 128L;
  
  public static final long ACTION_SKIP_TO_NEXT = 32L;
  
  public static final long ACTION_SKIP_TO_PREVIOUS = 16L;
  
  public static final long ACTION_SKIP_TO_QUEUE_ITEM = 4096L;
  
  public static final long ACTION_STOP = 1L;
  
  private PlaybackState(int paramInt, long paramLong1, long paramLong2, float paramFloat, long paramLong3, long paramLong4, List<CustomAction> paramList, long paramLong5, CharSequence paramCharSequence, Bundle paramBundle) {
    this.mState = paramInt;
    this.mPosition = paramLong1;
    this.mSpeed = paramFloat;
    this.mUpdateTime = paramLong2;
    this.mBufferedPosition = paramLong3;
    this.mActions = paramLong4;
    this.mCustomActions = new ArrayList<>(paramList);
    this.mActiveItemId = paramLong5;
    this.mErrorMessage = paramCharSequence;
    this.mExtras = paramBundle;
  }
  
  private PlaybackState(Parcel paramParcel) {
    this.mState = paramParcel.readInt();
    this.mPosition = paramParcel.readLong();
    this.mSpeed = paramParcel.readFloat();
    this.mUpdateTime = paramParcel.readLong();
    this.mBufferedPosition = paramParcel.readLong();
    this.mActions = paramParcel.readLong();
    this.mCustomActions = paramParcel.createTypedArrayList(CustomAction.CREATOR);
    this.mActiveItemId = paramParcel.readLong();
    this.mErrorMessage = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mExtras = paramParcel.readBundle();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder("PlaybackState {");
    stringBuilder.append("state=");
    stringBuilder.append(this.mState);
    stringBuilder.append(", position=");
    stringBuilder.append(this.mPosition);
    stringBuilder.append(", buffered position=");
    stringBuilder.append(this.mBufferedPosition);
    stringBuilder.append(", speed=");
    stringBuilder.append(this.mSpeed);
    stringBuilder.append(", updated=");
    stringBuilder.append(this.mUpdateTime);
    stringBuilder.append(", actions=");
    stringBuilder.append(this.mActions);
    stringBuilder.append(", custom actions=");
    stringBuilder.append(this.mCustomActions);
    stringBuilder.append(", active item id=");
    stringBuilder.append(this.mActiveItemId);
    stringBuilder.append(", error=");
    stringBuilder.append(this.mErrorMessage);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mState);
    paramParcel.writeLong(this.mPosition);
    paramParcel.writeFloat(this.mSpeed);
    paramParcel.writeLong(this.mUpdateTime);
    paramParcel.writeLong(this.mBufferedPosition);
    paramParcel.writeLong(this.mActions);
    paramParcel.writeTypedList(this.mCustomActions);
    paramParcel.writeLong(this.mActiveItemId);
    TextUtils.writeToParcel(this.mErrorMessage, paramParcel, 0);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public int getState() {
    return this.mState;
  }
  
  public long getPosition() {
    return this.mPosition;
  }
  
  public long getBufferedPosition() {
    return this.mBufferedPosition;
  }
  
  public float getPlaybackSpeed() {
    return this.mSpeed;
  }
  
  public long getActions() {
    return this.mActions;
  }
  
  public List<CustomAction> getCustomActions() {
    return this.mCustomActions;
  }
  
  public CharSequence getErrorMessage() {
    return this.mErrorMessage;
  }
  
  public long getLastPositionUpdateTime() {
    return this.mUpdateTime;
  }
  
  public long getActiveQueueItemId() {
    return this.mActiveItemId;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public static final Parcelable.Creator<PlaybackState> CREATOR = new Parcelable.Creator<PlaybackState>() {
      public PlaybackState createFromParcel(Parcel param1Parcel) {
        return new PlaybackState(param1Parcel);
      }
      
      public PlaybackState[] newArray(int param1Int) {
        return new PlaybackState[param1Int];
      }
    };
  
  public static final long PLAYBACK_POSITION_UNKNOWN = -1L;
  
  public static final int STATE_BUFFERING = 6;
  
  public static final int STATE_CONNECTING = 8;
  
  public static final int STATE_ERROR = 7;
  
  public static final int STATE_FAST_FORWARDING = 4;
  
  public static final int STATE_NONE = 0;
  
  public static final int STATE_PAUSED = 2;
  
  public static final int STATE_PLAYING = 3;
  
  public static final int STATE_REWINDING = 5;
  
  public static final int STATE_SKIPPING_TO_NEXT = 10;
  
  public static final int STATE_SKIPPING_TO_PREVIOUS = 9;
  
  public static final int STATE_SKIPPING_TO_QUEUE_ITEM = 11;
  
  public static final int STATE_STOPPED = 1;
  
  private static final String TAG = "PlaybackState";
  
  private final long mActions;
  
  private final long mActiveItemId;
  
  private final long mBufferedPosition;
  
  private List<CustomAction> mCustomActions;
  
  private final CharSequence mErrorMessage;
  
  private final Bundle mExtras;
  
  private final long mPosition;
  
  private final float mSpeed;
  
  private final int mState;
  
  private final long mUpdateTime;
  
  public static final class CustomAction implements Parcelable {
    private CustomAction(String param1String, CharSequence param1CharSequence, int param1Int, Bundle param1Bundle) {
      this.mAction = param1String;
      this.mName = param1CharSequence;
      this.mIcon = param1Int;
      this.mExtras = param1Bundle;
    }
    
    private CustomAction(Parcel param1Parcel) {
      this.mAction = param1Parcel.readString();
      this.mName = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel);
      this.mIcon = param1Parcel.readInt();
      this.mExtras = param1Parcel.readBundle();
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.mAction);
      TextUtils.writeToParcel(this.mName, param1Parcel, param1Int);
      param1Parcel.writeInt(this.mIcon);
      param1Parcel.writeBundle(this.mExtras);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<CustomAction> CREATOR = new Parcelable.Creator<CustomAction>() {
        public PlaybackState.CustomAction createFromParcel(Parcel param2Parcel) {
          return new PlaybackState.CustomAction(param2Parcel);
        }
        
        public PlaybackState.CustomAction[] newArray(int param2Int) {
          return new PlaybackState.CustomAction[param2Int];
        }
      };
    
    private final String mAction;
    
    private final Bundle mExtras;
    
    private final int mIcon;
    
    private final CharSequence mName;
    
    public String getAction() {
      return this.mAction;
    }
    
    public CharSequence getName() {
      return this.mName;
    }
    
    public int getIcon() {
      return this.mIcon;
    }
    
    public Bundle getExtras() {
      return this.mExtras;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Action:mName='");
      stringBuilder.append(this.mName);
      stringBuilder.append(", mIcon=");
      stringBuilder.append(this.mIcon);
      stringBuilder.append(", mExtras=");
      stringBuilder.append(this.mExtras);
      return stringBuilder.toString();
    }
    
    class Builder {
      private final String mAction;
      
      private Bundle mExtras;
      
      private final int mIcon;
      
      private final CharSequence mName;
      
      public Builder(PlaybackState.CustomAction this$0, CharSequence param2CharSequence, int param2Int) {
        if (!TextUtils.isEmpty((CharSequence)this$0)) {
          if (!TextUtils.isEmpty(param2CharSequence)) {
            if (param2Int != 0) {
              this.mAction = (String)this$0;
              this.mName = param2CharSequence;
              this.mIcon = param2Int;
              return;
            } 
            throw new IllegalArgumentException("You must specify an icon resource id to build a CustomAction.");
          } 
          throw new IllegalArgumentException("You must specify a name to build a CustomAction.");
        } 
        throw new IllegalArgumentException("You must specify an action to build a CustomAction.");
      }
      
      public Builder setExtras(Bundle param2Bundle) {
        this.mExtras = param2Bundle;
        return this;
      }
      
      public PlaybackState.CustomAction build() {
        return new PlaybackState.CustomAction(this.mAction, this.mName, this.mIcon, this.mExtras);
      }
    }
  }
  
  class null implements Parcelable.Creator<CustomAction> {
    public PlaybackState.CustomAction createFromParcel(Parcel param1Parcel) {
      return new PlaybackState.CustomAction(param1Parcel);
    }
    
    public PlaybackState.CustomAction[] newArray(int param1Int) {
      return new PlaybackState.CustomAction[param1Int];
    }
  }
  
  class Builder {
    private final String mAction;
    
    private Bundle mExtras;
    
    private final int mIcon;
    
    private final CharSequence mName;
    
    public Builder(PlaybackState this$0, CharSequence param1CharSequence, int param1Int) {
      if (!TextUtils.isEmpty((CharSequence)this$0)) {
        if (!TextUtils.isEmpty(param1CharSequence)) {
          if (param1Int != 0) {
            this.mAction = (String)this$0;
            this.mName = param1CharSequence;
            this.mIcon = param1Int;
            return;
          } 
          throw new IllegalArgumentException("You must specify an icon resource id to build a CustomAction.");
        } 
        throw new IllegalArgumentException("You must specify a name to build a CustomAction.");
      } 
      throw new IllegalArgumentException("You must specify an action to build a CustomAction.");
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public PlaybackState.CustomAction build() {
      return new PlaybackState.CustomAction(this.mAction, this.mName, this.mIcon, this.mExtras);
    }
  }
  
  class Builder {
    private long mUpdateTime;
    
    private int mState;
    
    private float mSpeed;
    
    private long mPosition;
    
    private Bundle mExtras;
    
    private CharSequence mErrorMessage;
    
    private final List<PlaybackState.CustomAction> mCustomActions = new ArrayList<>();
    
    private long mBufferedPosition;
    
    private long mActiveItemId = -1L;
    
    private long mActions;
    
    public Builder(PlaybackState this$0) {
      if (this$0 == null)
        return; 
      this.mState = this$0.mState;
      this.mPosition = this$0.mPosition;
      this.mBufferedPosition = this$0.mBufferedPosition;
      this.mSpeed = this$0.mSpeed;
      this.mActions = this$0.mActions;
      if (this$0.mCustomActions != null)
        this.mCustomActions.addAll(this$0.mCustomActions); 
      this.mErrorMessage = this$0.mErrorMessage;
      this.mUpdateTime = this$0.mUpdateTime;
      this.mActiveItemId = this$0.mActiveItemId;
      this.mExtras = this$0.mExtras;
    }
    
    public Builder setState(int param1Int, long param1Long1, float param1Float, long param1Long2) {
      this.mState = param1Int;
      this.mPosition = param1Long1;
      this.mUpdateTime = param1Long2;
      this.mSpeed = param1Float;
      return this;
    }
    
    public Builder setState(int param1Int, long param1Long, float param1Float) {
      return setState(param1Int, param1Long, param1Float, SystemClock.elapsedRealtime());
    }
    
    public Builder setActions(long param1Long) {
      this.mActions = param1Long;
      return this;
    }
    
    public Builder addCustomAction(String param1String1, String param1String2, int param1Int) {
      return addCustomAction(new PlaybackState.CustomAction(param1String1, param1String2, param1Int, null));
    }
    
    public Builder addCustomAction(PlaybackState.CustomAction param1CustomAction) {
      if (param1CustomAction != null) {
        this.mCustomActions.add(param1CustomAction);
        return this;
      } 
      throw new IllegalArgumentException("You may not add a null CustomAction to PlaybackState.");
    }
    
    public Builder setBufferedPosition(long param1Long) {
      this.mBufferedPosition = param1Long;
      return this;
    }
    
    public Builder setActiveQueueItemId(long param1Long) {
      this.mActiveItemId = param1Long;
      return this;
    }
    
    public Builder setErrorMessage(CharSequence param1CharSequence) {
      this.mErrorMessage = param1CharSequence;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public PlaybackState build() {
      return new PlaybackState(this.mState, this.mPosition, this.mUpdateTime, this.mSpeed, this.mBufferedPosition, this.mActions, this.mCustomActions, this.mActiveItemId, this.mErrorMessage, this.mExtras);
    }
    
    public Builder() {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Actions implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class State implements Annotation {}
}
