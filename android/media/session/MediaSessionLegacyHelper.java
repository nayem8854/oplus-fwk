package android.media.session;

import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaMetadata;
import android.media.Rating;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;

public class MediaSessionLegacyHelper {
  private static final boolean DEBUG = Log.isLoggable("MediaSessionHelper", 3);
  
  private static final String TAG = "MediaSessionHelper";
  
  private static MediaSessionLegacyHelper sInstance;
  
  private static final Object sLock = new Object();
  
  private Context mContext;
  
  private Handler mHandler = new Handler(Looper.getMainLooper());
  
  private MediaSessionManager mSessionManager;
  
  private ArrayMap<PendingIntent, SessionHolder> mSessions = new ArrayMap();
  
  private MediaSessionLegacyHelper(Context paramContext) {
    this.mContext = paramContext;
    this.mSessionManager = (MediaSessionManager)paramContext.getSystemService("media_session");
  }
  
  public static MediaSessionLegacyHelper getHelper(Context paramContext) {
    synchronized (sLock) {
      if (sInstance == null) {
        MediaSessionLegacyHelper mediaSessionLegacyHelper = new MediaSessionLegacyHelper();
        this(paramContext.getApplicationContext());
        sInstance = mediaSessionLegacyHelper;
      } 
      return sInstance;
    } 
  }
  
  public static Bundle getOldMetadata(MediaMetadata paramMediaMetadata, int paramInt1, int paramInt2) {
    try {
      return getOldMetadataImpl(paramMediaMetadata, paramInt1, paramInt2);
    } catch (Exception exception) {
      Log.e("MediaSessionHelper", "getOldMetadata failed, return an empty one.", exception);
      return new Bundle();
    } 
  }
  
  private static boolean containsKey(MediaMetadata paramMediaMetadata, String paramString) {
    if (paramMediaMetadata == null)
      return false; 
    if (paramString == null)
      return false; 
    try {
      return paramMediaMetadata.containsKey(paramString);
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("containsKey:");
      stringBuilder.append(paramString);
      Log.w("MediaSessionHelper", stringBuilder.toString(), exception);
      return false;
    } 
  }
  
  private static Bundle getOldMetadataImpl(MediaMetadata paramMediaMetadata, int paramInt1, int paramInt2) {
    boolean bool;
    if (paramInt1 != -1 && paramInt2 != -1) {
      bool = true;
    } else {
      bool = false;
    } 
    Bundle bundle = new Bundle();
    if (containsKey(paramMediaMetadata, "android.media.metadata.ALBUM")) {
      String str = paramMediaMetadata.getString("android.media.metadata.ALBUM");
      bundle.putString(String.valueOf(1), str);
    } 
    if (bool && containsKey(paramMediaMetadata, "android.media.metadata.ART")) {
      Bitmap bitmap = paramMediaMetadata.getBitmap("android.media.metadata.ART");
      bitmap = scaleBitmapIfTooBig(bitmap, paramInt1, paramInt2);
      bundle.putParcelable(String.valueOf(100), (Parcelable)bitmap);
    } else if (bool && containsKey(paramMediaMetadata, "android.media.metadata.ALBUM_ART")) {
      Bitmap bitmap = paramMediaMetadata.getBitmap("android.media.metadata.ALBUM_ART");
      bitmap = scaleBitmapIfTooBig(bitmap, paramInt1, paramInt2);
      bundle.putParcelable(String.valueOf(100), (Parcelable)bitmap);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.ALBUM_ARTIST")) {
      String str = paramMediaMetadata.getString("android.media.metadata.ALBUM_ARTIST");
      bundle.putString(String.valueOf(13), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.ARTIST")) {
      String str = paramMediaMetadata.getString("android.media.metadata.ARTIST");
      bundle.putString(String.valueOf(2), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.AUTHOR")) {
      String str = paramMediaMetadata.getString("android.media.metadata.AUTHOR");
      bundle.putString(String.valueOf(3), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.COMPILATION")) {
      String str = paramMediaMetadata.getString("android.media.metadata.COMPILATION");
      bundle.putString(String.valueOf(15), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.COMPOSER")) {
      String str = paramMediaMetadata.getString("android.media.metadata.COMPOSER");
      bundle.putString(String.valueOf(4), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.DATE")) {
      String str = paramMediaMetadata.getString("android.media.metadata.DATE");
      bundle.putString(String.valueOf(5), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.DISC_NUMBER")) {
      long l = paramMediaMetadata.getLong("android.media.metadata.DISC_NUMBER");
      bundle.putLong(String.valueOf(14), l);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.DURATION")) {
      long l = paramMediaMetadata.getLong("android.media.metadata.DURATION");
      bundle.putLong(String.valueOf(9), l);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.GENRE")) {
      String str = paramMediaMetadata.getString("android.media.metadata.GENRE");
      bundle.putString(String.valueOf(6), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.NUM_TRACKS")) {
      long l = paramMediaMetadata.getLong("android.media.metadata.NUM_TRACKS");
      bundle.putLong(String.valueOf(10), l);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.RATING")) {
      Rating rating = paramMediaMetadata.getRating("android.media.metadata.RATING");
      bundle.putParcelable(String.valueOf(101), rating);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.USER_RATING")) {
      Rating rating = paramMediaMetadata.getRating("android.media.metadata.USER_RATING");
      bundle.putParcelable(String.valueOf(268435457), rating);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.TITLE")) {
      String str = paramMediaMetadata.getString("android.media.metadata.TITLE");
      bundle.putString(String.valueOf(7), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.TRACK_NUMBER")) {
      long l = paramMediaMetadata.getLong("android.media.metadata.TRACK_NUMBER");
      bundle.putLong(String.valueOf(0), l);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.WRITER")) {
      String str = paramMediaMetadata.getString("android.media.metadata.WRITER");
      bundle.putString(String.valueOf(11), str);
    } 
    if (containsKey(paramMediaMetadata, "android.media.metadata.YEAR")) {
      String str = paramMediaMetadata.getString("android.media.metadata.YEAR");
      bundle.putString(String.valueOf(8), str);
    } 
    return bundle;
  }
  
  public MediaSession getSession(PendingIntent paramPendingIntent) {
    MediaSession mediaSession;
    SessionHolder sessionHolder = (SessionHolder)this.mSessions.get(paramPendingIntent);
    if (sessionHolder == null) {
      sessionHolder = null;
    } else {
      mediaSession = sessionHolder.mSession;
    } 
    return mediaSession;
  }
  
  public void sendMediaButtonEvent(KeyEvent paramKeyEvent, boolean paramBoolean) {
    if (paramKeyEvent == null) {
      Log.w("MediaSessionHelper", "Tried to send a null key event. Ignoring.");
      return;
    } 
    this.mSessionManager.dispatchMediaKeyEvent(paramKeyEvent, paramBoolean);
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("dispatched media key ");
      stringBuilder.append(paramKeyEvent);
      Log.d("MediaSessionHelper", stringBuilder.toString());
    } 
  }
  
  public void sendVolumeKeyEvent(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean) {
    if (paramKeyEvent == null) {
      Log.w("MediaSessionHelper", "Tried to send a null key event. Ignoring.");
      return;
    } 
    this.mSessionManager.dispatchVolumeKeyEvent(paramKeyEvent, paramInt, paramBoolean);
  }
  
  public void sendAdjustVolumeBy(int paramInt1, int paramInt2, int paramInt3) {
    this.mSessionManager.dispatchAdjustVolume(paramInt1, paramInt2, paramInt3);
    if (DEBUG)
      Log.d("MediaSessionHelper", "dispatched volume adjustment"); 
  }
  
  public boolean isGlobalPriorityActive() {
    return this.mSessionManager.isGlobalPriorityActive();
  }
  
  public void addRccListener(PendingIntent paramPendingIntent, MediaSession.Callback paramCallback) {
    if (paramPendingIntent == null) {
      Log.w("MediaSessionHelper", "Pending intent was null, can't add rcc listener.");
      return;
    } 
    SessionHolder sessionHolder = getHolder(paramPendingIntent, true);
    if (sessionHolder == null)
      return; 
    if (sessionHolder.mRccListener != null && 
      sessionHolder.mRccListener == paramCallback) {
      if (DEBUG)
        Log.d("MediaSessionHelper", "addRccListener listener already added."); 
      return;
    } 
    sessionHolder.mRccListener = paramCallback;
    sessionHolder.mFlags |= 0x2;
    sessionHolder.mSession.setFlags(sessionHolder.mFlags);
    sessionHolder.update();
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Added rcc listener for ");
      stringBuilder.append(paramPendingIntent);
      stringBuilder.append(".");
      Log.d("MediaSessionHelper", stringBuilder.toString());
    } 
  }
  
  public void removeRccListener(PendingIntent paramPendingIntent) {
    if (paramPendingIntent == null)
      return; 
    SessionHolder sessionHolder = getHolder(paramPendingIntent, false);
    if (sessionHolder != null && sessionHolder.mRccListener != null) {
      sessionHolder.mRccListener = null;
      sessionHolder.mFlags &= 0xFFFFFFFD;
      sessionHolder.mSession.setFlags(sessionHolder.mFlags);
      sessionHolder.update();
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Removed rcc listener for ");
        stringBuilder.append(paramPendingIntent);
        stringBuilder.append(".");
        Log.d("MediaSessionHelper", stringBuilder.toString());
      } 
    } 
  }
  
  public void addMediaButtonListener(PendingIntent paramPendingIntent, ComponentName paramComponentName, Context paramContext) {
    if (paramPendingIntent == null) {
      Log.w("MediaSessionHelper", "Pending intent was null, can't addMediaButtonListener.");
      return;
    } 
    SessionHolder sessionHolder = getHolder(paramPendingIntent, true);
    if (sessionHolder == null)
      return; 
    if (sessionHolder.mMediaButtonListener != null)
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("addMediaButtonListener already added ");
        stringBuilder.append(paramPendingIntent);
        Log.d("MediaSessionHelper", stringBuilder.toString());
      }  
    sessionHolder.mMediaButtonListener = new MediaButtonListener(paramPendingIntent, paramContext);
    sessionHolder.mFlags = 0x1 | sessionHolder.mFlags;
    sessionHolder.mSession.setFlags(sessionHolder.mFlags);
    sessionHolder.mSession.setMediaButtonReceiver(paramPendingIntent);
    sessionHolder.update();
    if (DEBUG) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("addMediaButtonListener added ");
      stringBuilder.append(paramPendingIntent);
      Log.d("MediaSessionHelper", stringBuilder.toString());
    } 
  }
  
  public void removeMediaButtonListener(PendingIntent paramPendingIntent) {
    if (paramPendingIntent == null)
      return; 
    SessionHolder sessionHolder = getHolder(paramPendingIntent, false);
    if (sessionHolder != null && sessionHolder.mMediaButtonListener != null) {
      sessionHolder.mFlags &= 0xFFFFFFFE;
      sessionHolder.mSession.setFlags(sessionHolder.mFlags);
      sessionHolder.mMediaButtonListener = null;
      sessionHolder.update();
      if (DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("removeMediaButtonListener removed ");
        stringBuilder.append(paramPendingIntent);
        Log.d("MediaSessionHelper", stringBuilder.toString());
      } 
    } 
  }
  
  private static Bitmap scaleBitmapIfTooBig(Bitmap paramBitmap, int paramInt1, int paramInt2) {
    // Byte code:
    //   0: aload_0
    //   1: astore_3
    //   2: aload_3
    //   3: astore #4
    //   5: aload_3
    //   6: ifnull -> 161
    //   9: aload_0
    //   10: invokevirtual getWidth : ()I
    //   13: istore #5
    //   15: aload_0
    //   16: invokevirtual getHeight : ()I
    //   19: istore #6
    //   21: iload #5
    //   23: iload_1
    //   24: if_icmpgt -> 36
    //   27: aload_3
    //   28: astore #4
    //   30: iload #6
    //   32: iload_2
    //   33: if_icmple -> 161
    //   36: iload_1
    //   37: i2f
    //   38: iload #5
    //   40: i2f
    //   41: fdiv
    //   42: iload_2
    //   43: i2f
    //   44: iload #6
    //   46: i2f
    //   47: fdiv
    //   48: invokestatic min : (FF)F
    //   51: fstore #7
    //   53: iload #5
    //   55: i2f
    //   56: fload #7
    //   58: fmul
    //   59: invokestatic round : (F)I
    //   62: istore_1
    //   63: iload #6
    //   65: i2f
    //   66: fload #7
    //   68: fmul
    //   69: invokestatic round : (F)I
    //   72: istore_2
    //   73: aload_0
    //   74: invokevirtual getConfig : ()Landroid/graphics/Bitmap$Config;
    //   77: astore #4
    //   79: aload #4
    //   81: astore_0
    //   82: aload #4
    //   84: ifnonnull -> 91
    //   87: getstatic android/graphics/Bitmap$Config.ARGB_8888 : Landroid/graphics/Bitmap$Config;
    //   90: astore_0
    //   91: iload_1
    //   92: iload_2
    //   93: aload_0
    //   94: invokestatic createBitmap : (IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    //   97: astore #4
    //   99: new android/graphics/Canvas
    //   102: dup
    //   103: aload #4
    //   105: invokespecial <init> : (Landroid/graphics/Bitmap;)V
    //   108: astore #8
    //   110: new android/graphics/Paint
    //   113: dup
    //   114: invokespecial <init> : ()V
    //   117: astore_0
    //   118: aload_0
    //   119: iconst_1
    //   120: invokevirtual setAntiAlias : (Z)V
    //   123: aload_0
    //   124: iconst_1
    //   125: invokevirtual setFilterBitmap : (Z)V
    //   128: new android/graphics/RectF
    //   131: dup
    //   132: fconst_0
    //   133: fconst_0
    //   134: aload #4
    //   136: invokevirtual getWidth : ()I
    //   139: i2f
    //   140: aload #4
    //   142: invokevirtual getHeight : ()I
    //   145: i2f
    //   146: invokespecial <init> : (FFFF)V
    //   149: astore #9
    //   151: aload #8
    //   153: aload_3
    //   154: aconst_null
    //   155: aload #9
    //   157: aload_0
    //   158: invokevirtual drawBitmap : (Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V
    //   161: aload #4
    //   163: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #414	-> 0
    //   #415	-> 9
    //   #416	-> 15
    //   #417	-> 21
    //   #418	-> 36
    //   #419	-> 53
    //   #420	-> 63
    //   #421	-> 73
    //   #422	-> 79
    //   #423	-> 87
    //   #425	-> 91
    //   #426	-> 99
    //   #427	-> 110
    //   #428	-> 118
    //   #429	-> 123
    //   #430	-> 128
    //   #431	-> 128
    //   #430	-> 151
    //   #432	-> 161
    //   #435	-> 161
  }
  
  private SessionHolder getHolder(PendingIntent paramPendingIntent, boolean paramBoolean) {
    SessionHolder sessionHolder1 = (SessionHolder)this.mSessions.get(paramPendingIntent);
    SessionHolder sessionHolder2 = sessionHolder1;
    if (sessionHolder1 == null) {
      sessionHolder2 = sessionHolder1;
      if (paramBoolean) {
        Context context = this.mContext;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MediaSessionHelper-");
        stringBuilder.append(paramPendingIntent.getCreatorPackage());
        MediaSession mediaSession = new MediaSession(context, stringBuilder.toString());
        mediaSession.setActive(true);
        sessionHolder2 = new SessionHolder(mediaSession, paramPendingIntent);
        this.mSessions.put(paramPendingIntent, sessionHolder2);
      } 
    } 
    return sessionHolder2;
  }
  
  private static void sendKeyEvent(PendingIntent paramPendingIntent, Context paramContext, Intent paramIntent) {
    try {
      paramPendingIntent.send(paramContext, 0, paramIntent);
      return;
    } catch (android.app.PendingIntent.CanceledException canceledException) {
      Log.e("MediaSessionHelper", "Error sending media key down event:", (Throwable)canceledException);
      return;
    } 
  }
  
  class MediaButtonListener extends MediaSession.Callback {
    private final Context mContext;
    
    private final PendingIntent mPendingIntent;
    
    public MediaButtonListener(MediaSessionLegacyHelper this$0, Context param1Context) {
      this.mPendingIntent = (PendingIntent)this$0;
      this.mContext = param1Context;
    }
    
    public boolean onMediaButtonEvent(Intent param1Intent) {
      MediaSessionLegacyHelper.sendKeyEvent(this.mPendingIntent, this.mContext, param1Intent);
      return true;
    }
    
    public void onPlay() {
      sendKeyEvent(126);
    }
    
    public void onPause() {
      sendKeyEvent(127);
    }
    
    public void onSkipToNext() {
      sendKeyEvent(87);
    }
    
    public void onSkipToPrevious() {
      sendKeyEvent(88);
    }
    
    public void onFastForward() {
      sendKeyEvent(90);
    }
    
    public void onRewind() {
      sendKeyEvent(89);
    }
    
    public void onStop() {
      sendKeyEvent(86);
    }
    
    private void sendKeyEvent(int param1Int) {
      KeyEvent keyEvent = new KeyEvent(0, param1Int);
      Intent intent = new Intent("android.intent.action.MEDIA_BUTTON");
      intent.addFlags(268435456);
      intent.putExtra("android.intent.extra.KEY_EVENT", (Parcelable)keyEvent);
      MediaSessionLegacyHelper.sendKeyEvent(this.mPendingIntent, this.mContext, intent);
      keyEvent = new KeyEvent(1, param1Int);
      intent.putExtra("android.intent.extra.KEY_EVENT", (Parcelable)keyEvent);
      MediaSessionLegacyHelper.sendKeyEvent(this.mPendingIntent, this.mContext, intent);
      if (MediaSessionLegacyHelper.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sent ");
        stringBuilder.append(param1Int);
        stringBuilder.append(" to pending intent ");
        stringBuilder.append(this.mPendingIntent);
        Log.d("MediaSessionHelper", stringBuilder.toString());
      } 
    }
  }
  
  private class SessionHolder {
    public SessionCallback mCb;
    
    public int mFlags;
    
    public MediaSessionLegacyHelper.MediaButtonListener mMediaButtonListener;
    
    public final PendingIntent mPi;
    
    public MediaSession.Callback mRccListener;
    
    public final MediaSession mSession;
    
    final MediaSessionLegacyHelper this$0;
    
    public SessionHolder(MediaSession param1MediaSession, PendingIntent param1PendingIntent) {
      this.mSession = param1MediaSession;
      this.mPi = param1PendingIntent;
    }
    
    public void update() {
      if (this.mMediaButtonListener == null && this.mRccListener == null) {
        this.mSession.setCallback(null);
        this.mSession.release();
        this.mCb = null;
        MediaSessionLegacyHelper.this.mSessions.remove(this.mPi);
      } else if (this.mCb == null) {
        this.mCb = new SessionCallback();
        Handler handler = new Handler(Looper.getMainLooper());
        this.mSession.setCallback(this.mCb, handler);
      } 
    }
    
    class SessionCallback extends MediaSession.Callback {
      final MediaSessionLegacyHelper.SessionHolder this$1;
      
      private SessionCallback() {}
      
      public boolean onMediaButtonEvent(Intent param2Intent) {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onMediaButtonEvent(param2Intent); 
        return true;
      }
      
      public void onPlay() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onPlay(); 
      }
      
      public void onPause() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onPause(); 
      }
      
      public void onSkipToNext() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onSkipToNext(); 
      }
      
      public void onSkipToPrevious() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onSkipToPrevious(); 
      }
      
      public void onFastForward() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onFastForward(); 
      }
      
      public void onRewind() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onRewind(); 
      }
      
      public void onStop() {
        if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onStop(); 
      }
      
      public void onSeekTo(long param2Long) {
        if (MediaSessionLegacyHelper.SessionHolder.this.mRccListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mRccListener.onSeekTo(param2Long); 
      }
      
      public void onSetRating(Rating param2Rating) {
        if (MediaSessionLegacyHelper.SessionHolder.this.mRccListener != null)
          MediaSessionLegacyHelper.SessionHolder.this.mRccListener.onSetRating(param2Rating); 
      }
    }
  }
  
  class SessionCallback extends MediaSession.Callback {
    final MediaSessionLegacyHelper.SessionHolder this$1;
    
    private SessionCallback() {}
    
    public boolean onMediaButtonEvent(Intent param1Intent) {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onMediaButtonEvent(param1Intent); 
      return true;
    }
    
    public void onPlay() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onPlay(); 
    }
    
    public void onPause() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onPause(); 
    }
    
    public void onSkipToNext() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onSkipToNext(); 
    }
    
    public void onSkipToPrevious() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onSkipToPrevious(); 
    }
    
    public void onFastForward() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onFastForward(); 
    }
    
    public void onRewind() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onRewind(); 
    }
    
    public void onStop() {
      if (MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mMediaButtonListener.onStop(); 
    }
    
    public void onSeekTo(long param1Long) {
      if (MediaSessionLegacyHelper.SessionHolder.this.mRccListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mRccListener.onSeekTo(param1Long); 
    }
    
    public void onSetRating(Rating param1Rating) {
      if (MediaSessionLegacyHelper.SessionHolder.this.mRccListener != null)
        MediaSessionLegacyHelper.SessionHolder.this.mRccListener.onSetRating(param1Rating); 
    }
  }
}
