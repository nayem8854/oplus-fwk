package android.media.session;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IOnMediaKeyEventSessionChangedListener extends IInterface {
  void onMediaKeyEventSessionChanged(String paramString, MediaSession.Token paramToken) throws RemoteException;
  
  class Default implements IOnMediaKeyEventSessionChangedListener {
    public void onMediaKeyEventSessionChanged(String param1String, MediaSession.Token param1Token) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnMediaKeyEventSessionChangedListener {
    private static final String DESCRIPTOR = "android.media.session.IOnMediaKeyEventSessionChangedListener";
    
    static final int TRANSACTION_onMediaKeyEventSessionChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.session.IOnMediaKeyEventSessionChangedListener");
    }
    
    public static IOnMediaKeyEventSessionChangedListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.IOnMediaKeyEventSessionChangedListener");
      if (iInterface != null && iInterface instanceof IOnMediaKeyEventSessionChangedListener)
        return (IOnMediaKeyEventSessionChangedListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onMediaKeyEventSessionChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.session.IOnMediaKeyEventSessionChangedListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.session.IOnMediaKeyEventSessionChangedListener");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        MediaSession.Token token = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onMediaKeyEventSessionChanged(str, (MediaSession.Token)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOnMediaKeyEventSessionChangedListener {
      public static IOnMediaKeyEventSessionChangedListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.IOnMediaKeyEventSessionChangedListener";
      }
      
      public void onMediaKeyEventSessionChanged(String param2String, MediaSession.Token param2Token) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.IOnMediaKeyEventSessionChangedListener");
          parcel.writeString(param2String);
          if (param2Token != null) {
            parcel.writeInt(1);
            param2Token.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOnMediaKeyEventSessionChangedListener.Stub.getDefaultImpl() != null) {
            IOnMediaKeyEventSessionChangedListener.Stub.getDefaultImpl().onMediaKeyEventSessionChanged(param2String, param2Token);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnMediaKeyEventSessionChangedListener param1IOnMediaKeyEventSessionChangedListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnMediaKeyEventSessionChangedListener != null) {
          Proxy.sDefaultImpl = param1IOnMediaKeyEventSessionChangedListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnMediaKeyEventSessionChangedListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
