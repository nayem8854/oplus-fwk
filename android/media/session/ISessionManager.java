package android.media.session;

import android.content.ComponentName;
import android.content.pm.ParceledListSlice;
import android.media.IRemoteVolumeController;
import android.media.Session2Token;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.KeyEvent;
import java.util.List;

public interface ISessionManager extends IInterface {
  void addOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener paramIOnMediaKeyEventDispatchedListener) throws RemoteException;
  
  void addOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener paramIOnMediaKeyEventSessionChangedListener) throws RemoteException;
  
  void addSession2TokensListener(ISession2TokensListener paramISession2TokensListener, int paramInt) throws RemoteException;
  
  void addSessionsListener(IActiveSessionsListener paramIActiveSessionsListener, ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  ISession createSession(String paramString1, ISessionCallback paramISessionCallback, String paramString2, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void dispatchAdjustVolume(String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void dispatchMediaKeyEvent(String paramString, boolean paramBoolean1, KeyEvent paramKeyEvent, boolean paramBoolean2) throws RemoteException;
  
  boolean dispatchMediaKeyEventToSessionAsSystemService(String paramString, MediaSession.Token paramToken, KeyEvent paramKeyEvent) throws RemoteException;
  
  void dispatchVolumeKeyEvent(String paramString1, String paramString2, boolean paramBoolean1, KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean2) throws RemoteException;
  
  void dispatchVolumeKeyEventToSessionAsSystemService(String paramString1, String paramString2, MediaSession.Token paramToken, KeyEvent paramKeyEvent) throws RemoteException;
  
  ParceledListSlice getSession2Tokens(int paramInt) throws RemoteException;
  
  int getSessionPolicies(MediaSession.Token paramToken) throws RemoteException;
  
  List<MediaSession.Token> getSessions(ComponentName paramComponentName, int paramInt) throws RemoteException;
  
  boolean isGlobalPriorityActive() throws RemoteException;
  
  boolean isTrusted(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void notifySession2Created(Session2Token paramSession2Token) throws RemoteException;
  
  void registerRemoteVolumeController(IRemoteVolumeController paramIRemoteVolumeController) throws RemoteException;
  
  void removeOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener paramIOnMediaKeyEventDispatchedListener) throws RemoteException;
  
  void removeOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener paramIOnMediaKeyEventSessionChangedListener) throws RemoteException;
  
  void removeSession2TokensListener(ISession2TokensListener paramISession2TokensListener) throws RemoteException;
  
  void removeSessionsListener(IActiveSessionsListener paramIActiveSessionsListener) throws RemoteException;
  
  void setCustomMediaKeyDispatcherForTesting(String paramString) throws RemoteException;
  
  void setCustomSessionPolicyProviderForTesting(String paramString) throws RemoteException;
  
  void setOnMediaKeyListener(IOnMediaKeyListener paramIOnMediaKeyListener) throws RemoteException;
  
  void setOnVolumeKeyLongPressListener(IOnVolumeKeyLongPressListener paramIOnVolumeKeyLongPressListener) throws RemoteException;
  
  void setSessionPolicies(MediaSession.Token paramToken, int paramInt) throws RemoteException;
  
  void unregisterRemoteVolumeController(IRemoteVolumeController paramIRemoteVolumeController) throws RemoteException;
  
  class Default implements ISessionManager {
    public ISession createSession(String param1String1, ISessionCallback param1ISessionCallback, String param1String2, Bundle param1Bundle, int param1Int) throws RemoteException {
      return null;
    }
    
    public void notifySession2Created(Session2Token param1Session2Token) throws RemoteException {}
    
    public List<MediaSession.Token> getSessions(ComponentName param1ComponentName, int param1Int) throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getSession2Tokens(int param1Int) throws RemoteException {
      return null;
    }
    
    public void dispatchMediaKeyEvent(String param1String, boolean param1Boolean1, KeyEvent param1KeyEvent, boolean param1Boolean2) throws RemoteException {}
    
    public boolean dispatchMediaKeyEventToSessionAsSystemService(String param1String, MediaSession.Token param1Token, KeyEvent param1KeyEvent) throws RemoteException {
      return false;
    }
    
    public void dispatchVolumeKeyEvent(String param1String1, String param1String2, boolean param1Boolean1, KeyEvent param1KeyEvent, int param1Int, boolean param1Boolean2) throws RemoteException {}
    
    public void dispatchVolumeKeyEventToSessionAsSystemService(String param1String1, String param1String2, MediaSession.Token param1Token, KeyEvent param1KeyEvent) throws RemoteException {}
    
    public void dispatchAdjustVolume(String param1String1, String param1String2, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void addSessionsListener(IActiveSessionsListener param1IActiveSessionsListener, ComponentName param1ComponentName, int param1Int) throws RemoteException {}
    
    public void removeSessionsListener(IActiveSessionsListener param1IActiveSessionsListener) throws RemoteException {}
    
    public void addSession2TokensListener(ISession2TokensListener param1ISession2TokensListener, int param1Int) throws RemoteException {}
    
    public void removeSession2TokensListener(ISession2TokensListener param1ISession2TokensListener) throws RemoteException {}
    
    public void registerRemoteVolumeController(IRemoteVolumeController param1IRemoteVolumeController) throws RemoteException {}
    
    public void unregisterRemoteVolumeController(IRemoteVolumeController param1IRemoteVolumeController) throws RemoteException {}
    
    public boolean isGlobalPriorityActive() throws RemoteException {
      return false;
    }
    
    public void addOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener param1IOnMediaKeyEventDispatchedListener) throws RemoteException {}
    
    public void removeOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener param1IOnMediaKeyEventDispatchedListener) throws RemoteException {}
    
    public void addOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener param1IOnMediaKeyEventSessionChangedListener) throws RemoteException {}
    
    public void removeOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener param1IOnMediaKeyEventSessionChangedListener) throws RemoteException {}
    
    public void setOnVolumeKeyLongPressListener(IOnVolumeKeyLongPressListener param1IOnVolumeKeyLongPressListener) throws RemoteException {}
    
    public void setOnMediaKeyListener(IOnMediaKeyListener param1IOnMediaKeyListener) throws RemoteException {}
    
    public boolean isTrusted(String param1String, int param1Int1, int param1Int2) throws RemoteException {
      return false;
    }
    
    public void setCustomMediaKeyDispatcherForTesting(String param1String) throws RemoteException {}
    
    public void setCustomSessionPolicyProviderForTesting(String param1String) throws RemoteException {}
    
    public int getSessionPolicies(MediaSession.Token param1Token) throws RemoteException {
      return 0;
    }
    
    public void setSessionPolicies(MediaSession.Token param1Token, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISessionManager {
    private static final String DESCRIPTOR = "android.media.session.ISessionManager";
    
    static final int TRANSACTION_addOnMediaKeyEventDispatchedListener = 17;
    
    static final int TRANSACTION_addOnMediaKeyEventSessionChangedListener = 19;
    
    static final int TRANSACTION_addSession2TokensListener = 12;
    
    static final int TRANSACTION_addSessionsListener = 10;
    
    static final int TRANSACTION_createSession = 1;
    
    static final int TRANSACTION_dispatchAdjustVolume = 9;
    
    static final int TRANSACTION_dispatchMediaKeyEvent = 5;
    
    static final int TRANSACTION_dispatchMediaKeyEventToSessionAsSystemService = 6;
    
    static final int TRANSACTION_dispatchVolumeKeyEvent = 7;
    
    static final int TRANSACTION_dispatchVolumeKeyEventToSessionAsSystemService = 8;
    
    static final int TRANSACTION_getSession2Tokens = 4;
    
    static final int TRANSACTION_getSessionPolicies = 26;
    
    static final int TRANSACTION_getSessions = 3;
    
    static final int TRANSACTION_isGlobalPriorityActive = 16;
    
    static final int TRANSACTION_isTrusted = 23;
    
    static final int TRANSACTION_notifySession2Created = 2;
    
    static final int TRANSACTION_registerRemoteVolumeController = 14;
    
    static final int TRANSACTION_removeOnMediaKeyEventDispatchedListener = 18;
    
    static final int TRANSACTION_removeOnMediaKeyEventSessionChangedListener = 20;
    
    static final int TRANSACTION_removeSession2TokensListener = 13;
    
    static final int TRANSACTION_removeSessionsListener = 11;
    
    static final int TRANSACTION_setCustomMediaKeyDispatcherForTesting = 24;
    
    static final int TRANSACTION_setCustomSessionPolicyProviderForTesting = 25;
    
    static final int TRANSACTION_setOnMediaKeyListener = 22;
    
    static final int TRANSACTION_setOnVolumeKeyLongPressListener = 21;
    
    static final int TRANSACTION_setSessionPolicies = 27;
    
    static final int TRANSACTION_unregisterRemoteVolumeController = 15;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISessionManager");
    }
    
    public static ISessionManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISessionManager");
      if (iInterface != null && iInterface instanceof ISessionManager)
        return (ISessionManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 27:
          return "setSessionPolicies";
        case 26:
          return "getSessionPolicies";
        case 25:
          return "setCustomSessionPolicyProviderForTesting";
        case 24:
          return "setCustomMediaKeyDispatcherForTesting";
        case 23:
          return "isTrusted";
        case 22:
          return "setOnMediaKeyListener";
        case 21:
          return "setOnVolumeKeyLongPressListener";
        case 20:
          return "removeOnMediaKeyEventSessionChangedListener";
        case 19:
          return "addOnMediaKeyEventSessionChangedListener";
        case 18:
          return "removeOnMediaKeyEventDispatchedListener";
        case 17:
          return "addOnMediaKeyEventDispatchedListener";
        case 16:
          return "isGlobalPriorityActive";
        case 15:
          return "unregisterRemoteVolumeController";
        case 14:
          return "registerRemoteVolumeController";
        case 13:
          return "removeSession2TokensListener";
        case 12:
          return "addSession2TokensListener";
        case 11:
          return "removeSessionsListener";
        case 10:
          return "addSessionsListener";
        case 9:
          return "dispatchAdjustVolume";
        case 8:
          return "dispatchVolumeKeyEventToSessionAsSystemService";
        case 7:
          return "dispatchVolumeKeyEvent";
        case 6:
          return "dispatchMediaKeyEventToSessionAsSystemService";
        case 5:
          return "dispatchMediaKeyEvent";
        case 4:
          return "getSession2Tokens";
        case 3:
          return "getSessions";
        case 2:
          return "notifySession2Created";
        case 1:
          break;
      } 
      return "createSession";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool2;
        int j;
        boolean bool1;
        String str1;
        IOnMediaKeyListener iOnMediaKeyListener;
        IOnVolumeKeyLongPressListener iOnVolumeKeyLongPressListener;
        IOnMediaKeyEventSessionChangedListener iOnMediaKeyEventSessionChangedListener;
        IOnMediaKeyEventDispatchedListener iOnMediaKeyEventDispatchedListener;
        IRemoteVolumeController iRemoteVolumeController;
        ISession2TokensListener iSession2TokensListener1;
        IActiveSessionsListener iActiveSessionsListener1;
        ParceledListSlice parceledListSlice;
        List<MediaSession.Token> list;
        MediaSession.Token token;
        String str3;
        ISession2TokensListener iSession2TokensListener2;
        String str2;
        IActiveSessionsListener iActiveSessionsListener2;
        int k;
        String str5;
        boolean bool4, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 27:
            param1Parcel1.enforceInterface("android.media.session.ISessionManager");
            if (param1Parcel1.readInt() != 0) {
              token = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
            } else {
              token = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            setSessionPolicies(token, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            param1Parcel1.enforceInterface("android.media.session.ISessionManager");
            if (param1Parcel1.readInt() != 0) {
              MediaSession.Token token1 = MediaSession.Token.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            param1Int1 = getSessionPolicies((MediaSession.Token)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 25:
            param1Parcel1.enforceInterface("android.media.session.ISessionManager");
            str1 = param1Parcel1.readString();
            setCustomSessionPolicyProviderForTesting(str1);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str1.enforceInterface("android.media.session.ISessionManager");
            str1 = str1.readString();
            setCustomMediaKeyDispatcherForTesting(str1);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str1.enforceInterface("android.media.session.ISessionManager");
            str3 = str1.readString();
            param1Int2 = str1.readInt();
            param1Int1 = str1.readInt();
            bool2 = isTrusted(str3, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 22:
            str1.enforceInterface("android.media.session.ISessionManager");
            iOnMediaKeyListener = IOnMediaKeyListener.Stub.asInterface(str1.readStrongBinder());
            setOnMediaKeyListener(iOnMediaKeyListener);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            iOnMediaKeyListener.enforceInterface("android.media.session.ISessionManager");
            iOnVolumeKeyLongPressListener = IOnVolumeKeyLongPressListener.Stub.asInterface(iOnMediaKeyListener.readStrongBinder());
            setOnVolumeKeyLongPressListener(iOnVolumeKeyLongPressListener);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            iOnVolumeKeyLongPressListener.enforceInterface("android.media.session.ISessionManager");
            iOnMediaKeyEventSessionChangedListener = IOnMediaKeyEventSessionChangedListener.Stub.asInterface(iOnVolumeKeyLongPressListener.readStrongBinder());
            removeOnMediaKeyEventSessionChangedListener(iOnMediaKeyEventSessionChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            iOnMediaKeyEventSessionChangedListener.enforceInterface("android.media.session.ISessionManager");
            iOnMediaKeyEventSessionChangedListener = IOnMediaKeyEventSessionChangedListener.Stub.asInterface(iOnMediaKeyEventSessionChangedListener.readStrongBinder());
            addOnMediaKeyEventSessionChangedListener(iOnMediaKeyEventSessionChangedListener);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            iOnMediaKeyEventSessionChangedListener.enforceInterface("android.media.session.ISessionManager");
            iOnMediaKeyEventDispatchedListener = IOnMediaKeyEventDispatchedListener.Stub.asInterface(iOnMediaKeyEventSessionChangedListener.readStrongBinder());
            removeOnMediaKeyEventDispatchedListener(iOnMediaKeyEventDispatchedListener);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            iOnMediaKeyEventDispatchedListener.enforceInterface("android.media.session.ISessionManager");
            iOnMediaKeyEventDispatchedListener = IOnMediaKeyEventDispatchedListener.Stub.asInterface(iOnMediaKeyEventDispatchedListener.readStrongBinder());
            addOnMediaKeyEventDispatchedListener(iOnMediaKeyEventDispatchedListener);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            iOnMediaKeyEventDispatchedListener.enforceInterface("android.media.session.ISessionManager");
            bool2 = isGlobalPriorityActive();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 15:
            iOnMediaKeyEventDispatchedListener.enforceInterface("android.media.session.ISessionManager");
            iRemoteVolumeController = IRemoteVolumeController.Stub.asInterface(iOnMediaKeyEventDispatchedListener.readStrongBinder());
            unregisterRemoteVolumeController(iRemoteVolumeController);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            iRemoteVolumeController.enforceInterface("android.media.session.ISessionManager");
            iRemoteVolumeController = IRemoteVolumeController.Stub.asInterface(iRemoteVolumeController.readStrongBinder());
            registerRemoteVolumeController(iRemoteVolumeController);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            iRemoteVolumeController.enforceInterface("android.media.session.ISessionManager");
            iSession2TokensListener1 = ISession2TokensListener.Stub.asInterface(iRemoteVolumeController.readStrongBinder());
            removeSession2TokensListener(iSession2TokensListener1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iSession2TokensListener1.enforceInterface("android.media.session.ISessionManager");
            iSession2TokensListener2 = ISession2TokensListener.Stub.asInterface(iSession2TokensListener1.readStrongBinder());
            j = iSession2TokensListener1.readInt();
            addSession2TokensListener(iSession2TokensListener2, j);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            iSession2TokensListener1.enforceInterface("android.media.session.ISessionManager");
            iActiveSessionsListener1 = IActiveSessionsListener.Stub.asInterface(iSession2TokensListener1.readStrongBinder());
            removeSessionsListener(iActiveSessionsListener1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            iActiveSessionsListener2 = IActiveSessionsListener.Stub.asInterface(iActiveSessionsListener1.readStrongBinder());
            if (iActiveSessionsListener1.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              iSession2TokensListener2 = null;
            } 
            j = iActiveSessionsListener1.readInt();
            addSessionsListener(iActiveSessionsListener2, (ComponentName)iSession2TokensListener2, j);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            str4 = iActiveSessionsListener1.readString();
            str2 = iActiveSessionsListener1.readString();
            k = iActiveSessionsListener1.readInt();
            param1Int2 = iActiveSessionsListener1.readInt();
            j = iActiveSessionsListener1.readInt();
            dispatchAdjustVolume(str4, str2, k, param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            str4 = iActiveSessionsListener1.readString();
            str5 = iActiveSessionsListener1.readString();
            if (iActiveSessionsListener1.readInt() != 0) {
              MediaSession.Token token1 = MediaSession.Token.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              str2 = null;
            } 
            if (iActiveSessionsListener1.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              iActiveSessionsListener1 = null;
            } 
            dispatchVolumeKeyEventToSessionAsSystemService(str4, str5, (MediaSession.Token)str2, (KeyEvent)iActiveSessionsListener1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            str4 = iActiveSessionsListener1.readString();
            str5 = iActiveSessionsListener1.readString();
            if (iActiveSessionsListener1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            if (iActiveSessionsListener1.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              str2 = null;
            } 
            j = iActiveSessionsListener1.readInt();
            if (iActiveSessionsListener1.readInt() != 0) {
              bool3 = true;
            } else {
              bool3 = false;
            } 
            dispatchVolumeKeyEvent(str4, str5, bool4, (KeyEvent)str2, j, bool3);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            str4 = iActiveSessionsListener1.readString();
            if (iActiveSessionsListener1.readInt() != 0) {
              MediaSession.Token token1 = MediaSession.Token.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              str2 = null;
            } 
            if (iActiveSessionsListener1.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              iActiveSessionsListener1 = null;
            } 
            bool1 = dispatchMediaKeyEventToSessionAsSystemService(str4, (MediaSession.Token)str2, (KeyEvent)iActiveSessionsListener1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 5:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            str4 = iActiveSessionsListener1.readString();
            if (iActiveSessionsListener1.readInt() != 0) {
              bool4 = true;
            } else {
              bool4 = false;
            } 
            if (iActiveSessionsListener1.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iActiveSessionsListener1);
            } else {
              str2 = null;
            } 
            if (iActiveSessionsListener1.readInt() != 0)
              bool3 = true; 
            dispatchMediaKeyEvent(str4, bool4, (KeyEvent)str2, bool3);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iActiveSessionsListener1.enforceInterface("android.media.session.ISessionManager");
            i = iActiveSessionsListener1.readInt();
            parceledListSlice = getSession2Tokens(i);
            param1Parcel2.writeNoException();
            if (parceledListSlice != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            parceledListSlice.enforceInterface("android.media.session.ISessionManager");
            if (parceledListSlice.readInt() != 0) {
              ComponentName componentName = ComponentName.CREATOR.createFromParcel((Parcel)parceledListSlice);
            } else {
              str2 = null;
            } 
            i = parceledListSlice.readInt();
            list = getSessions((ComponentName)str2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 2:
            list.enforceInterface("android.media.session.ISessionManager");
            if (list.readInt() != 0) {
              Session2Token session2Token = Session2Token.CREATOR.createFromParcel((Parcel)list);
            } else {
              list = null;
            } 
            notifySession2Created((Session2Token)list);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        list.enforceInterface("android.media.session.ISessionManager");
        String str6 = list.readString();
        ISessionCallback iSessionCallback = ISessionCallback.Stub.asInterface(list.readStrongBinder());
        String str4 = list.readString();
        if (list.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)list);
        } else {
          str2 = null;
        } 
        int i = list.readInt();
        ISession iSession = createSession(str6, iSessionCallback, str4, (Bundle)str2, i);
        param1Parcel2.writeNoException();
        if (iSession != null) {
          IBinder iBinder = iSession.asBinder();
        } else {
          iSession = null;
        } 
        param1Parcel2.writeStrongBinder((IBinder)iSession);
        return true;
      } 
      param1Parcel2.writeString("android.media.session.ISessionManager");
      return true;
    }
    
    private static class Proxy implements ISessionManager {
      public static ISessionManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISessionManager";
      }
      
      public ISession createSession(String param2String1, ISessionCallback param2ISessionCallback, String param2String2, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String1);
          if (param2ISessionCallback != null) {
            iBinder = param2ISessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null)
            return ISessionManager.Stub.getDefaultImpl().createSession(param2String1, param2ISessionCallback, param2String2, param2Bundle, param2Int); 
          parcel2.readException();
          return ISession.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void notifySession2Created(Session2Token param2Session2Token) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2Session2Token != null) {
            parcel1.writeInt(1);
            param2Session2Token.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().notifySession2Created(param2Session2Token);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<MediaSession.Token> getSessions(ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null)
            return ISessionManager.Stub.getDefaultImpl().getSessions(param2ComponentName, param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(MediaSession.Token.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getSession2Tokens(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            parceledListSlice = ISessionManager.Stub.getDefaultImpl().getSession2Tokens(param2Int);
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispatchMediaKeyEvent(String param2String, boolean param2Boolean1, KeyEvent param2KeyEvent, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool2;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Boolean1) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          if (param2KeyEvent != null) {
            parcel1.writeInt(1);
            param2KeyEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Boolean2) {
            bool2 = bool1;
          } else {
            bool2 = false;
          } 
          parcel1.writeInt(bool2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().dispatchMediaKeyEvent(param2String, param2Boolean1, param2KeyEvent, param2Boolean2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean dispatchMediaKeyEventToSessionAsSystemService(String param2String, MediaSession.Token param2Token, KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Token != null) {
            parcel1.writeInt(1);
            param2Token.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2KeyEvent != null) {
            parcel1.writeInt(1);
            param2KeyEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool2 && ISessionManager.Stub.getDefaultImpl() != null) {
            bool1 = ISessionManager.Stub.getDefaultImpl().dispatchMediaKeyEventToSessionAsSystemService(param2String, param2Token, param2KeyEvent);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispatchVolumeKeyEvent(String param2String1, String param2String2, boolean param2Boolean1, KeyEvent param2KeyEvent, int param2Int, boolean param2Boolean2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          try {
            parcel1.writeString(param2String1);
            try {
              boolean bool2;
              parcel1.writeString(param2String2);
              boolean bool1 = true;
              if (param2Boolean1) {
                bool2 = true;
              } else {
                bool2 = false;
              } 
              parcel1.writeInt(bool2);
              if (param2KeyEvent != null) {
                parcel1.writeInt(1);
                param2KeyEvent.writeToParcel(parcel1, 0);
              } else {
                parcel1.writeInt(0);
              } 
              try {
                parcel1.writeInt(param2Int);
                if (param2Boolean2) {
                  bool2 = bool1;
                } else {
                  bool2 = false;
                } 
                parcel1.writeInt(bool2);
                try {
                  boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
                  if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
                    ISessionManager.Stub.getDefaultImpl().dispatchVolumeKeyEvent(param2String1, param2String2, param2Boolean1, param2KeyEvent, param2Int, param2Boolean2);
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } 
                  parcel2.readException();
                  parcel2.recycle();
                  parcel1.recycle();
                  return;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2String1;
      }
      
      public void dispatchVolumeKeyEventToSessionAsSystemService(String param2String1, String param2String2, MediaSession.Token param2Token, KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Token != null) {
            parcel1.writeInt(1);
            param2Token.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2KeyEvent != null) {
            parcel1.writeInt(1);
            param2KeyEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().dispatchVolumeKeyEventToSessionAsSystemService(param2String1, param2String2, param2Token, param2KeyEvent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispatchAdjustVolume(String param2String1, String param2String2, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().dispatchAdjustVolume(param2String1, param2String2, param2Int1, param2Int2, param2Int3);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addSessionsListener(IActiveSessionsListener param2IActiveSessionsListener, ComponentName param2ComponentName, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IActiveSessionsListener != null) {
            iBinder = param2IActiveSessionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2ComponentName != null) {
            parcel1.writeInt(1);
            param2ComponentName.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().addSessionsListener(param2IActiveSessionsListener, param2ComponentName, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSessionsListener(IActiveSessionsListener param2IActiveSessionsListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IActiveSessionsListener != null) {
            iBinder = param2IActiveSessionsListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().removeSessionsListener(param2IActiveSessionsListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addSession2TokensListener(ISession2TokensListener param2ISession2TokensListener, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2ISession2TokensListener != null) {
            iBinder = param2ISession2TokensListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().addSession2TokensListener(param2ISession2TokensListener, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeSession2TokensListener(ISession2TokensListener param2ISession2TokensListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2ISession2TokensListener != null) {
            iBinder = param2ISession2TokensListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().removeSession2TokensListener(param2ISession2TokensListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRemoteVolumeController(IRemoteVolumeController param2IRemoteVolumeController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IRemoteVolumeController != null) {
            iBinder = param2IRemoteVolumeController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().registerRemoteVolumeController(param2IRemoteVolumeController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterRemoteVolumeController(IRemoteVolumeController param2IRemoteVolumeController) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IRemoteVolumeController != null) {
            iBinder = param2IRemoteVolumeController.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().unregisterRemoteVolumeController(param2IRemoteVolumeController);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isGlobalPriorityActive() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(16, parcel1, parcel2, 0);
          if (!bool2 && ISessionManager.Stub.getDefaultImpl() != null) {
            bool1 = ISessionManager.Stub.getDefaultImpl().isGlobalPriorityActive();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener param2IOnMediaKeyEventDispatchedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnMediaKeyEventDispatchedListener != null) {
            iBinder = param2IOnMediaKeyEventDispatchedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().addOnMediaKeyEventDispatchedListener(param2IOnMediaKeyEventDispatchedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnMediaKeyEventDispatchedListener(IOnMediaKeyEventDispatchedListener param2IOnMediaKeyEventDispatchedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnMediaKeyEventDispatchedListener != null) {
            iBinder = param2IOnMediaKeyEventDispatchedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().removeOnMediaKeyEventDispatchedListener(param2IOnMediaKeyEventDispatchedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener param2IOnMediaKeyEventSessionChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnMediaKeyEventSessionChangedListener != null) {
            iBinder = param2IOnMediaKeyEventSessionChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().addOnMediaKeyEventSessionChangedListener(param2IOnMediaKeyEventSessionChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOnMediaKeyEventSessionChangedListener(IOnMediaKeyEventSessionChangedListener param2IOnMediaKeyEventSessionChangedListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnMediaKeyEventSessionChangedListener != null) {
            iBinder = param2IOnMediaKeyEventSessionChangedListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().removeOnMediaKeyEventSessionChangedListener(param2IOnMediaKeyEventSessionChangedListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOnVolumeKeyLongPressListener(IOnVolumeKeyLongPressListener param2IOnVolumeKeyLongPressListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnVolumeKeyLongPressListener != null) {
            iBinder = param2IOnVolumeKeyLongPressListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().setOnVolumeKeyLongPressListener(param2IOnVolumeKeyLongPressListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setOnMediaKeyListener(IOnMediaKeyListener param2IOnMediaKeyListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2IOnMediaKeyListener != null) {
            iBinder = param2IOnMediaKeyListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().setOnMediaKeyListener(param2IOnMediaKeyListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isTrusted(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(23, parcel1, parcel2, 0);
          if (!bool2 && ISessionManager.Stub.getDefaultImpl() != null) {
            bool1 = ISessionManager.Stub.getDefaultImpl().isTrusted(param2String, param2Int1, param2Int2);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCustomMediaKeyDispatcherForTesting(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().setCustomMediaKeyDispatcherForTesting(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCustomSessionPolicyProviderForTesting(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().setCustomSessionPolicyProviderForTesting(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getSessionPolicies(MediaSession.Token param2Token) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2Token != null) {
            parcel1.writeInt(1);
            param2Token.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null)
            return ISessionManager.Stub.getDefaultImpl().getSessionPolicies(param2Token); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSessionPolicies(MediaSession.Token param2Token, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionManager");
          if (param2Token != null) {
            parcel1.writeInt(1);
            param2Token.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ISessionManager.Stub.getDefaultImpl() != null) {
            ISessionManager.Stub.getDefaultImpl().setSessionPolicies(param2Token, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISessionManager param1ISessionManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISessionManager != null) {
          Proxy.sDefaultImpl = param1ISessionManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISessionManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
