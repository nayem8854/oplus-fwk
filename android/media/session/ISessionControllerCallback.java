package android.media.session;

import android.content.pm.ParceledListSlice;
import android.media.MediaMetadata;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ISessionControllerCallback extends IInterface {
  void onEvent(String paramString, Bundle paramBundle) throws RemoteException;
  
  void onExtrasChanged(Bundle paramBundle) throws RemoteException;
  
  void onMetadataChanged(MediaMetadata paramMediaMetadata) throws RemoteException;
  
  void onPlaybackStateChanged(PlaybackState paramPlaybackState) throws RemoteException;
  
  void onQueueChanged(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void onQueueTitleChanged(CharSequence paramCharSequence) throws RemoteException;
  
  void onSessionDestroyed() throws RemoteException;
  
  void onVolumeInfoChanged(MediaController.PlaybackInfo paramPlaybackInfo) throws RemoteException;
  
  class Default implements ISessionControllerCallback {
    public void onEvent(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void onSessionDestroyed() throws RemoteException {}
    
    public void onPlaybackStateChanged(PlaybackState param1PlaybackState) throws RemoteException {}
    
    public void onMetadataChanged(MediaMetadata param1MediaMetadata) throws RemoteException {}
    
    public void onQueueChanged(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void onQueueTitleChanged(CharSequence param1CharSequence) throws RemoteException {}
    
    public void onExtrasChanged(Bundle param1Bundle) throws RemoteException {}
    
    public void onVolumeInfoChanged(MediaController.PlaybackInfo param1PlaybackInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISessionControllerCallback {
    private static final String DESCRIPTOR = "android.media.session.ISessionControllerCallback";
    
    static final int TRANSACTION_onEvent = 1;
    
    static final int TRANSACTION_onExtrasChanged = 7;
    
    static final int TRANSACTION_onMetadataChanged = 4;
    
    static final int TRANSACTION_onPlaybackStateChanged = 3;
    
    static final int TRANSACTION_onQueueChanged = 5;
    
    static final int TRANSACTION_onQueueTitleChanged = 6;
    
    static final int TRANSACTION_onSessionDestroyed = 2;
    
    static final int TRANSACTION_onVolumeInfoChanged = 8;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISessionControllerCallback");
    }
    
    public static ISessionControllerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISessionControllerCallback");
      if (iInterface != null && iInterface instanceof ISessionControllerCallback)
        return (ISessionControllerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "onVolumeInfoChanged";
        case 7:
          return "onExtrasChanged";
        case 6:
          return "onQueueTitleChanged";
        case 5:
          return "onQueueChanged";
        case 4:
          return "onMetadataChanged";
        case 3:
          return "onPlaybackStateChanged";
        case 2:
          return "onSessionDestroyed";
        case 1:
          break;
      } 
      return "onEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              MediaController.PlaybackInfo playbackInfo = MediaController.PlaybackInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onVolumeInfoChanged((MediaController.PlaybackInfo)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onExtrasChanged((Bundle)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onQueueTitleChanged((CharSequence)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onQueueChanged((ParceledListSlice)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              MediaMetadata mediaMetadata = MediaMetadata.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onMetadataChanged((MediaMetadata)param1Parcel1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            if (param1Parcel1.readInt() != 0) {
              PlaybackState playbackState = PlaybackState.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPlaybackStateChanged((PlaybackState)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
            onSessionDestroyed();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.session.ISessionControllerCallback");
        str = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onEvent(str, (Bundle)param1Parcel1);
        return true;
      } 
      str.writeString("android.media.session.ISessionControllerCallback");
      return true;
    }
    
    private static class Proxy implements ISessionControllerCallback {
      public static ISessionControllerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISessionControllerCallback";
      }
      
      public void onEvent(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onEvent(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionDestroyed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onSessionDestroyed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPlaybackStateChanged(PlaybackState param2PlaybackState) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2PlaybackState != null) {
            parcel.writeInt(1);
            param2PlaybackState.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onPlaybackStateChanged(param2PlaybackState);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onMetadataChanged(MediaMetadata param2MediaMetadata) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2MediaMetadata != null) {
            parcel.writeInt(1);
            param2MediaMetadata.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onMetadataChanged(param2MediaMetadata);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onQueueChanged(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2ParceledListSlice != null) {
            parcel.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onQueueChanged(param2ParceledListSlice);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onQueueTitleChanged(CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2CharSequence != null) {
            parcel.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onQueueTitleChanged(param2CharSequence);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onExtrasChanged(Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onExtrasChanged(param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVolumeInfoChanged(MediaController.PlaybackInfo param2PlaybackInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionControllerCallback");
          if (param2PlaybackInfo != null) {
            parcel.writeInt(1);
            param2PlaybackInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ISessionControllerCallback.Stub.getDefaultImpl() != null) {
            ISessionControllerCallback.Stub.getDefaultImpl().onVolumeInfoChanged(param2PlaybackInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISessionControllerCallback param1ISessionControllerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISessionControllerCallback != null) {
          Proxy.sDefaultImpl = param1ISessionControllerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISessionControllerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
