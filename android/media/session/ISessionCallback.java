package android.media.session;

import android.content.Intent;
import android.media.Rating;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;

public interface ISessionCallback extends IInterface {
  void onAdjustVolume(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onCommand(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void onCustomAction(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onFastForward(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onMediaButton(String paramString, int paramInt1, int paramInt2, Intent paramIntent, int paramInt3, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void onMediaButtonFromController(String paramString, int paramInt1, int paramInt2, Intent paramIntent) throws RemoteException;
  
  void onNext(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onPause(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onPlay(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onPlayFromMediaId(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onPlayFromSearch(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onPlayFromUri(String paramString, int paramInt1, int paramInt2, Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void onPrepare(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onPrepareFromMediaId(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onPrepareFromSearch(String paramString1, int paramInt1, int paramInt2, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void onPrepareFromUri(String paramString, int paramInt1, int paramInt2, Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void onPrevious(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onRate(String paramString, int paramInt1, int paramInt2, Rating paramRating) throws RemoteException;
  
  void onRewind(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  void onSeekTo(String paramString, int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  void onSetPlaybackSpeed(String paramString, int paramInt1, int paramInt2, float paramFloat) throws RemoteException;
  
  void onSetVolumeTo(String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void onSkipToTrack(String paramString, int paramInt1, int paramInt2, long paramLong) throws RemoteException;
  
  void onStop(String paramString, int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements ISessionCallback {
    public void onCommand(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void onMediaButton(String param1String, int param1Int1, int param1Int2, Intent param1Intent, int param1Int3, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public void onMediaButtonFromController(String param1String, int param1Int1, int param1Int2, Intent param1Intent) throws RemoteException {}
    
    public void onPrepare(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onPrepareFromMediaId(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onPrepareFromSearch(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onPrepareFromUri(String param1String, int param1Int1, int param1Int2, Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void onPlay(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onPlayFromMediaId(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onPlayFromSearch(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onPlayFromUri(String param1String, int param1Int1, int param1Int2, Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void onSkipToTrack(String param1String, int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void onPause(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onStop(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onNext(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onPrevious(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onFastForward(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onRewind(String param1String, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onSeekTo(String param1String, int param1Int1, int param1Int2, long param1Long) throws RemoteException {}
    
    public void onRate(String param1String, int param1Int1, int param1Int2, Rating param1Rating) throws RemoteException {}
    
    public void onSetPlaybackSpeed(String param1String, int param1Int1, int param1Int2, float param1Float) throws RemoteException {}
    
    public void onCustomAction(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void onAdjustVolume(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void onSetVolumeTo(String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISessionCallback {
    private static final String DESCRIPTOR = "android.media.session.ISessionCallback";
    
    static final int TRANSACTION_onAdjustVolume = 23;
    
    static final int TRANSACTION_onCommand = 1;
    
    static final int TRANSACTION_onCustomAction = 22;
    
    static final int TRANSACTION_onFastForward = 17;
    
    static final int TRANSACTION_onMediaButton = 2;
    
    static final int TRANSACTION_onMediaButtonFromController = 3;
    
    static final int TRANSACTION_onNext = 15;
    
    static final int TRANSACTION_onPause = 13;
    
    static final int TRANSACTION_onPlay = 8;
    
    static final int TRANSACTION_onPlayFromMediaId = 9;
    
    static final int TRANSACTION_onPlayFromSearch = 10;
    
    static final int TRANSACTION_onPlayFromUri = 11;
    
    static final int TRANSACTION_onPrepare = 4;
    
    static final int TRANSACTION_onPrepareFromMediaId = 5;
    
    static final int TRANSACTION_onPrepareFromSearch = 6;
    
    static final int TRANSACTION_onPrepareFromUri = 7;
    
    static final int TRANSACTION_onPrevious = 16;
    
    static final int TRANSACTION_onRate = 20;
    
    static final int TRANSACTION_onRewind = 18;
    
    static final int TRANSACTION_onSeekTo = 19;
    
    static final int TRANSACTION_onSetPlaybackSpeed = 21;
    
    static final int TRANSACTION_onSetVolumeTo = 24;
    
    static final int TRANSACTION_onSkipToTrack = 12;
    
    static final int TRANSACTION_onStop = 14;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISessionCallback");
    }
    
    public static ISessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISessionCallback");
      if (iInterface != null && iInterface instanceof ISessionCallback)
        return (ISessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 24:
          return "onSetVolumeTo";
        case 23:
          return "onAdjustVolume";
        case 22:
          return "onCustomAction";
        case 21:
          return "onSetPlaybackSpeed";
        case 20:
          return "onRate";
        case 19:
          return "onSeekTo";
        case 18:
          return "onRewind";
        case 17:
          return "onFastForward";
        case 16:
          return "onPrevious";
        case 15:
          return "onNext";
        case 14:
          return "onStop";
        case 13:
          return "onPause";
        case 12:
          return "onSkipToTrack";
        case 11:
          return "onPlayFromUri";
        case 10:
          return "onPlayFromSearch";
        case 9:
          return "onPlayFromMediaId";
        case 8:
          return "onPlay";
        case 7:
          return "onPrepareFromUri";
        case 6:
          return "onPrepareFromSearch";
        case 5:
          return "onPrepareFromMediaId";
        case 4:
          return "onPrepare";
        case 3:
          return "onMediaButtonFromController";
        case 2:
          return "onMediaButton";
        case 1:
          break;
      } 
      return "onCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        int i;
        float f;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 24:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onSetVolumeTo(str, i, param1Int1, param1Int2);
            return true;
          case 23:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onAdjustVolume(str, param1Int1, i, param1Int2);
            return true;
          case 22:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onCustomAction(str, param1Int1, param1Int2, str1, (Bundle)param1Parcel1);
            return true;
          case 21:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            f = param1Parcel1.readFloat();
            onSetPlaybackSpeed(str, param1Int1, param1Int2, f);
            return true;
          case 20:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Rating rating = Rating.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRate(str, param1Int2, param1Int1, (Rating)param1Parcel1);
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            l = param1Parcel1.readLong();
            onSeekTo(str, param1Int1, param1Int2, l);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onRewind(str, param1Int2, param1Int1);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onFastForward(str, param1Int2, param1Int1);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onPrevious(str, param1Int1, param1Int2);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onNext(str, param1Int1, param1Int2);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onStop(str, param1Int2, param1Int1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onPause(str, param1Int1, param1Int2);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            l = param1Parcel1.readLong();
            onSkipToTrack(str, param1Int2, param1Int1, l);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str1 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPlayFromUri(str1, param1Int2, param1Int1, (Uri)str, (Bundle)param1Parcel1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPlayFromSearch(str1, param1Int1, param1Int2, str, (Bundle)param1Parcel1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str1 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPlayFromMediaId(str1, param1Int2, param1Int1, str, (Bundle)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onPlay(str, param1Int2, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str1 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrepareFromUri(str1, param1Int2, param1Int1, (Uri)str, (Bundle)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrepareFromSearch(str, param1Int2, param1Int1, str1, (Bundle)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onPrepareFromMediaId(str, param1Int2, param1Int1, str1, (Bundle)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onPrepare(str, param1Int1, param1Int2);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Intent intent = Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onMediaButtonFromController(str, param1Int2, param1Int1, (Intent)param1Parcel1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
            str1 = param1Parcel1.readString();
            i = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Intent intent = Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              str = null;
            } 
            param1Int2 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onMediaButton(str1, i, param1Int1, (Intent)str, param1Int2, (ResultReceiver)param1Parcel1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.session.ISessionCallback");
        String str1 = param1Parcel1.readString();
        param1Int2 = param1Parcel1.readInt();
        param1Int1 = param1Parcel1.readInt();
        String str2 = param1Parcel1.readString();
        if (param1Parcel1.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
        } else {
          str = null;
        } 
        if (param1Parcel1.readInt() != 0) {
          ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onCommand(str1, param1Int2, param1Int1, str2, (Bundle)str, (ResultReceiver)param1Parcel1);
        return true;
      } 
      str.writeString("android.media.session.ISessionCallback");
      return true;
    }
    
    private static class Proxy implements ISessionCallback {
      public static ISessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISessionCallback";
      }
      
      public void onCommand(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          try {
            parcel.writeString(param2String1);
            try {
              parcel.writeInt(param2Int1);
              try {
                parcel.writeInt(param2Int2);
                try {
                  parcel.writeString(param2String2);
                  if (param2Bundle != null) {
                    parcel.writeInt(1);
                    param2Bundle.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  if (param2ResultReceiver != null) {
                    parcel.writeInt(1);
                    param2ResultReceiver.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  try {
                    boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
                    if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
                      ISessionCallback.Stub.getDefaultImpl().onCommand(param2String1, param2Int1, param2Int2, param2String2, param2Bundle, param2ResultReceiver);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String1;
      }
      
      public void onMediaButton(String param2String, int param2Int1, int param2Int2, Intent param2Intent, int param2Int3, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          try {
            parcel.writeString(param2String);
            try {
              parcel.writeInt(param2Int1);
              try {
                parcel.writeInt(param2Int2);
                if (param2Intent != null) {
                  parcel.writeInt(1);
                  param2Intent.writeToParcel(parcel, 0);
                } else {
                  parcel.writeInt(0);
                } 
                try {
                  parcel.writeInt(param2Int3);
                  if (param2ResultReceiver != null) {
                    parcel.writeInt(1);
                    param2ResultReceiver.writeToParcel(parcel, 0);
                  } else {
                    parcel.writeInt(0);
                  } 
                  try {
                    boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
                    if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
                      ISessionCallback.Stub.getDefaultImpl().onMediaButton(param2String, param2Int1, param2Int2, param2Intent, param2Int3, param2ResultReceiver);
                      parcel.recycle();
                      return;
                    } 
                    parcel.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2String;
      }
      
      public void onMediaButtonFromController(String param2String, int param2Int1, int param2Int2, Intent param2Intent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Intent != null) {
            parcel.writeInt(1);
            param2Intent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onMediaButtonFromController(param2String, param2Int1, param2Int2, param2Intent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrepare(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPrepare(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrepareFromMediaId(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPrepareFromMediaId(param2String1, param2Int1, param2Int2, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrepareFromSearch(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPrepareFromSearch(param2String1, param2Int1, param2Int2, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrepareFromUri(String param2String, int param2Int1, int param2Int2, Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPrepareFromUri(param2String, param2Int1, param2Int2, param2Uri, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPlay(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPlay(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPlayFromMediaId(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPlayFromMediaId(param2String1, param2Int1, param2Int2, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPlayFromSearch(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPlayFromSearch(param2String1, param2Int1, param2Int2, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPlayFromUri(String param2String, int param2Int1, int param2Int2, Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPlayFromUri(param2String, param2Int1, param2Int2, param2Uri, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSkipToTrack(String param2String, int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onSkipToTrack(param2String, param2Int1, param2Int2, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPause(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPause(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStop(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onStop(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onNext(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onNext(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onPrevious(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onPrevious(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onFastForward(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onFastForward(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRewind(String param2String, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(18, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onRewind(param2String, param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSeekTo(String param2String, int param2Int1, int param2Int2, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(19, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onSeekTo(param2String, param2Int1, param2Int2, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRate(String param2String, int param2Int1, int param2Int2, Rating param2Rating) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          if (param2Rating != null) {
            parcel.writeInt(1);
            param2Rating.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onRate(param2String, param2Int1, param2Int2, param2Rating);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetPlaybackSpeed(String param2String, int param2Int1, int param2Int2, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(21, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onSetPlaybackSpeed(param2String, param2Int1, param2Int2, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onCustomAction(String param2String1, int param2Int1, int param2Int2, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String1);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeString(param2String2);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(22, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onCustomAction(param2String1, param2Int1, param2Int2, param2String2, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onAdjustVolume(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(23, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onAdjustVolume(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSetVolumeTo(String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISessionCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(24, parcel, (Parcel)null, 1);
          if (!bool && ISessionCallback.Stub.getDefaultImpl() != null) {
            ISessionCallback.Stub.getDefaultImpl().onSetVolumeTo(param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISessionCallback param1ISessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISessionCallback != null) {
          Proxy.sDefaultImpl = param1ISessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
