package android.media.session;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ParceledListSlice;
import android.media.AudioAttributes;
import android.media.MediaDescription;
import android.media.MediaMetadata;
import android.media.Rating;
import android.media.VolumeProvider;
import android.net.Uri;
import android.os.BadParcelableException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Process;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.Objects;

public final class MediaSession {
  private VolumeProvider mVolumeProvider;
  
  private final Token mSessionToken;
  
  private PlaybackState mPlaybackState;
  
  private final int mMaxBitmapSize;
  
  private final Object mLock = new Object();
  
  private final MediaController mController;
  
  private final CallbackStub mCbStub;
  
  private CallbackMessageHandler mCallback;
  
  private final ISession mBinder;
  
  private boolean mActive = false;
  
  static final String TAG = "MediaSession";
  
  public static final int INVALID_UID = -1;
  
  public static final int INVALID_PID = -1;
  
  @Deprecated
  public static final int FLAG_HANDLES_TRANSPORT_CONTROLS = 2;
  
  @Deprecated
  public static final int FLAG_HANDLES_MEDIA_BUTTONS = 1;
  
  public static final int FLAG_EXCLUSIVE_GLOBAL_PRIORITY = 65536;
  
  public MediaSession(Context paramContext, String paramString) {
    this(paramContext, paramString, null);
  }
  
  public MediaSession(Context paramContext, String paramString, Bundle paramBundle) {
    if (paramContext != null) {
      if (!TextUtils.isEmpty(paramString)) {
        if (!hasCustomParcelable(paramBundle)) {
          this.mMaxBitmapSize = paramContext.getResources().getDimensionPixelSize(17105064);
          this.mCbStub = new CallbackStub(this);
          MediaSessionManager mediaSessionManager = (MediaSessionManager)paramContext.getSystemService("media_session");
          try {
            this.mBinder = mediaSessionManager.createSession(this.mCbStub, paramString, paramBundle);
            Token token = new Token();
            this(Process.myUid(), this.mBinder.getController());
            this.mSessionToken = token;
            MediaController mediaController = new MediaController();
            this(paramContext, token);
            this.mController = mediaController;
            return;
          } catch (RemoteException remoteException) {
            throw new RuntimeException("Remote error creating session.", remoteException);
          } 
        } 
        throw new IllegalArgumentException("sessionInfo shouldn't contain any custom parcelables");
      } 
      throw new IllegalArgumentException("tag cannot be null or empty");
    } 
    throw new IllegalArgumentException("context cannot be null.");
  }
  
  public void setCallback(Callback paramCallback) {
    setCallback(paramCallback, null);
  }
  
  public void setCallback(Callback paramCallback, Handler paramHandler) {
    synchronized (this.mLock) {
      if (this.mCallback != null) {
        Callback.access$102(this.mCallback.mCallback, null);
        this.mCallback.removeCallbacksAndMessages(null);
      } 
      if (paramCallback == null) {
        this.mCallback = null;
        return;
      } 
      Handler handler = paramHandler;
      if (paramHandler == null) {
        handler = new Handler();
        this();
      } 
      Callback.access$102(paramCallback, this);
      paramHandler = new CallbackMessageHandler();
      super(this, handler.getLooper(), paramCallback);
      this.mCallback = (CallbackMessageHandler)paramHandler;
      return;
    } 
  }
  
  public void setSessionActivity(PendingIntent paramPendingIntent) {
    try {
      this.mBinder.setLaunchPendingIntent(paramPendingIntent);
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Failure in setLaunchPendingIntent.", (Throwable)remoteException);
    } 
  }
  
  public void setMediaButtonReceiver(PendingIntent paramPendingIntent) {
    try {
      this.mBinder.setMediaButtonReceiver(paramPendingIntent);
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Failure in setMediaButtonReceiver.", (Throwable)remoteException);
    } 
  }
  
  public void setFlags(int paramInt) {
    try {
      this.mBinder.setFlags(paramInt);
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Failure in setFlags.", (Throwable)remoteException);
    } 
  }
  
  public void setPlaybackToLocal(AudioAttributes paramAudioAttributes) {
    if (paramAudioAttributes != null) {
      try {
        this.mBinder.setPlaybackToLocal(paramAudioAttributes);
      } catch (RemoteException remoteException) {
        Log.wtf("MediaSession", "Failure in setPlaybackToLocal.", (Throwable)remoteException);
      } 
      return;
    } 
    throw new IllegalArgumentException("Attributes cannot be null for local playback.");
  }
  
  public void setPlaybackToRemote(VolumeProvider paramVolumeProvider) {
    if (paramVolumeProvider != null)
      synchronized (this.mLock) {
        this.mVolumeProvider = paramVolumeProvider;
        paramVolumeProvider.setCallback((VolumeProvider.Callback)new Object(this));
        try {
          null = this.mBinder;
          int i = paramVolumeProvider.getVolumeControl();
          int j = paramVolumeProvider.getMaxVolume();
          String str = paramVolumeProvider.getVolumeControlId();
          null.setPlaybackToRemote(i, j, str);
          this.mBinder.setCurrentVolume(paramVolumeProvider.getCurrentVolume());
        } catch (RemoteException remoteException) {
          Log.wtf("MediaSession", "Failure in setPlaybackToRemote.", (Throwable)remoteException);
        } 
        return;
      }  
    throw new IllegalArgumentException("volumeProvider may not be null!");
  }
  
  public void setActive(boolean paramBoolean) {
    if (this.mActive == paramBoolean)
      return; 
    try {
      this.mBinder.setActive(paramBoolean);
      this.mActive = paramBoolean;
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Failure in setActive.", (Throwable)remoteException);
    } 
  }
  
  public boolean isActive() {
    return this.mActive;
  }
  
  public void sendSessionEvent(String paramString, Bundle paramBundle) {
    if (!TextUtils.isEmpty(paramString)) {
      try {
        this.mBinder.sendEvent(paramString, paramBundle);
      } catch (RemoteException remoteException) {
        Log.wtf("MediaSession", "Error sending event", (Throwable)remoteException);
      } 
      return;
    } 
    throw new IllegalArgumentException("event cannot be null or empty");
  }
  
  public void release() {
    try {
      this.mBinder.destroySession();
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Error releasing session: ", (Throwable)remoteException);
    } 
  }
  
  public Token getSessionToken() {
    return this.mSessionToken;
  }
  
  public MediaController getController() {
    return this.mController;
  }
  
  public void setPlaybackState(PlaybackState paramPlaybackState) {
    this.mPlaybackState = paramPlaybackState;
    try {
      this.mBinder.setPlaybackState(paramPlaybackState);
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Dead object in setPlaybackState.", (Throwable)remoteException);
    } 
  }
  
  public void setMetadata(MediaMetadata paramMediaMetadata) {
    long l1 = -1L;
    int i = 0;
    MediaDescription mediaDescription = null;
    long l2 = l1;
    MediaMetadata mediaMetadata = paramMediaMetadata;
    if (paramMediaMetadata != null) {
      mediaMetadata = (new MediaMetadata.Builder(paramMediaMetadata, this.mMaxBitmapSize)).build();
      l2 = l1;
      if (mediaMetadata.containsKey("android.media.metadata.DURATION"))
        l2 = mediaMetadata.getLong("android.media.metadata.DURATION"); 
      i = mediaMetadata.size();
      mediaDescription = mediaMetadata.getDescription();
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("size=");
    stringBuilder.append(i);
    stringBuilder.append(", description=");
    stringBuilder.append(mediaDescription);
    String str = stringBuilder.toString();
    try {
      this.mBinder.setMetadata(mediaMetadata, l2, str);
    } catch (RemoteException remoteException) {
      Log.wtf("MediaSession", "Dead object in setPlaybackState.", (Throwable)remoteException);
    } 
  }
  
  public void setQueue(List<QueueItem> paramList) {
    try {
      ParceledListSlice parceledListSlice;
      ISession iSession = this.mBinder;
      if (paramList == null) {
        paramList = null;
      } else {
        parceledListSlice = new ParceledListSlice(paramList);
      } 
      iSession.setQueue(parceledListSlice);
    } catch (RemoteException remoteException) {
      Log.wtf("Dead object in setQueue.", (Throwable)remoteException);
    } 
  }
  
  public void setQueueTitle(CharSequence paramCharSequence) {
    try {
      this.mBinder.setQueueTitle(paramCharSequence);
    } catch (RemoteException remoteException) {
      Log.wtf("Dead object in setQueueTitle.", (Throwable)remoteException);
    } 
  }
  
  public void setRatingType(int paramInt) {
    try {
      this.mBinder.setRatingType(paramInt);
    } catch (RemoteException remoteException) {
      Log.e("MediaSession", "Error in setRatingType.", (Throwable)remoteException);
    } 
  }
  
  public void setExtras(Bundle paramBundle) {
    try {
      this.mBinder.setExtras(paramBundle);
    } catch (RemoteException remoteException) {
      Log.wtf("Dead object in setExtras.", (Throwable)remoteException);
    } 
  }
  
  public final MediaSessionManager.RemoteUserInfo getCurrentControllerInfo() {
    CallbackMessageHandler callbackMessageHandler = this.mCallback;
    if (callbackMessageHandler != null && callbackMessageHandler.mCurrentControllerInfo != null)
      return this.mCallback.mCurrentControllerInfo; 
    throw new IllegalStateException("This should be called inside of MediaSession.Callback methods");
  }
  
  public void notifyRemoteVolumeChanged(VolumeProvider paramVolumeProvider) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 52
    //   11: aload_1
    //   12: aload_0
    //   13: getfield mVolumeProvider : Landroid/media/VolumeProvider;
    //   16: if_acmpeq -> 22
    //   19: goto -> 52
    //   22: aload_2
    //   23: monitorexit
    //   24: aload_0
    //   25: getfield mBinder : Landroid/media/session/ISession;
    //   28: aload_1
    //   29: invokevirtual getCurrentVolume : ()I
    //   32: invokeinterface setCurrentVolume : (I)V
    //   37: goto -> 51
    //   40: astore_1
    //   41: ldc 'MediaSession'
    //   43: ldc_w 'Error in notifyVolumeChanged'
    //   46: aload_1
    //   47: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   50: pop
    //   51: return
    //   52: ldc 'MediaSession'
    //   54: ldc_w 'Received update from stale volume provider'
    //   57: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   60: pop
    //   61: aload_2
    //   62: monitorexit
    //   63: return
    //   64: astore_1
    //   65: aload_2
    //   66: monitorexit
    //   67: aload_1
    //   68: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #572	-> 0
    //   #573	-> 7
    //   #577	-> 22
    //   #579	-> 24
    //   #582	-> 37
    //   #580	-> 40
    //   #581	-> 41
    //   #583	-> 51
    //   #574	-> 52
    //   #575	-> 61
    //   #577	-> 64
    // Exception table:
    //   from	to	target	type
    //   11	19	64	finally
    //   22	24	64	finally
    //   24	37	40	android/os/RemoteException
    //   52	61	64	finally
    //   61	63	64	finally
    //   65	67	64	finally
  }
  
  public String getCallingPackage() {
    CallbackMessageHandler callbackMessageHandler = this.mCallback;
    if (callbackMessageHandler != null && callbackMessageHandler.mCurrentControllerInfo != null)
      return this.mCallback.mCurrentControllerInfo.getPackageName(); 
    return null;
  }
  
  public static boolean isActiveState(int paramInt) {
    switch (paramInt) {
      default:
        return false;
      case 3:
      case 4:
      case 5:
      case 6:
      case 8:
      case 9:
      case 10:
        break;
    } 
    return true;
  }
  
  static boolean hasCustomParcelable(Bundle paramBundle) {
    if (paramBundle == null)
      return false; 
    Parcel parcel1 = null, parcel2 = null;
    try {
      Parcel parcel = Parcel.obtain();
      parcel2 = parcel;
      parcel1 = parcel;
      parcel.writeBundle(paramBundle);
      parcel2 = parcel;
      parcel1 = parcel;
      parcel.setDataPosition(0);
      parcel2 = parcel;
      parcel1 = parcel;
      paramBundle = parcel.readBundle(null);
      parcel2 = parcel;
      parcel1 = parcel;
      paramBundle.size();
      if (parcel != null)
        parcel.recycle(); 
      return false;
    } catch (BadParcelableException badParcelableException) {
      parcel2 = parcel1;
      Log.d("MediaSession", "Custom parcelable in bundle.", (Throwable)badParcelableException);
      if (parcel1 != null)
        parcel1.recycle(); 
      return true;
    } finally {}
    if (parcel2 != null)
      parcel2.recycle(); 
    throw paramBundle;
  }
  
  void dispatchPrepare(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 3, null, null);
  }
  
  void dispatchPrepareFromMediaId(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 4, paramString, paramBundle);
  }
  
  void dispatchPrepareFromSearch(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 5, paramString, paramBundle);
  }
  
  void dispatchPrepareFromUri(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, Uri paramUri, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 6, paramUri, paramBundle);
  }
  
  void dispatchPlay(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 7, null, null);
  }
  
  void dispatchPlayFromMediaId(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 8, paramString, paramBundle);
  }
  
  void dispatchPlayFromSearch(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 9, paramString, paramBundle);
  }
  
  void dispatchPlayFromUri(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, Uri paramUri, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 10, paramUri, paramBundle);
  }
  
  void dispatchSkipToItem(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, long paramLong) {
    postToCallback(paramRemoteUserInfo, 11, Long.valueOf(paramLong), null);
  }
  
  void dispatchPause(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 12, null, null);
  }
  
  void dispatchStop(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 13, null, null);
  }
  
  void dispatchNext(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 14, null, null);
  }
  
  void dispatchPrevious(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 15, null, null);
  }
  
  void dispatchFastForward(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 16, null, null);
  }
  
  void dispatchRewind(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo) {
    postToCallback(paramRemoteUserInfo, 17, null, null);
  }
  
  void dispatchSeekTo(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, long paramLong) {
    postToCallback(paramRemoteUserInfo, 18, Long.valueOf(paramLong), null);
  }
  
  void dispatchRate(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, Rating paramRating) {
    postToCallback(paramRemoteUserInfo, 19, paramRating, null);
  }
  
  void dispatchSetPlaybackSpeed(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, float paramFloat) {
    postToCallback(paramRemoteUserInfo, 20, Float.valueOf(paramFloat), null);
  }
  
  void dispatchCustomAction(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle) {
    postToCallback(paramRemoteUserInfo, 21, paramString, paramBundle);
  }
  
  void dispatchMediaButton(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, Intent paramIntent) {
    postToCallback(paramRemoteUserInfo, 2, paramIntent, null);
  }
  
  void dispatchMediaButtonDelayed(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, Intent paramIntent, long paramLong) {
    postToCallbackDelayed(paramRemoteUserInfo, 24, paramIntent, null, paramLong);
  }
  
  void dispatchAdjustVolume(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, int paramInt) {
    postToCallback(paramRemoteUserInfo, 22, Integer.valueOf(paramInt), null);
  }
  
  void dispatchSetVolumeTo(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, int paramInt) {
    postToCallback(paramRemoteUserInfo, 23, Integer.valueOf(paramInt), null);
  }
  
  void dispatchCommand(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, String paramString, Bundle paramBundle, ResultReceiver paramResultReceiver) {
    Command command = new Command(paramString, paramBundle, paramResultReceiver);
    postToCallback(paramRemoteUserInfo, 1, command, null);
  }
  
  void postToCallback(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, int paramInt, Object paramObject, Bundle paramBundle) {
    postToCallbackDelayed(paramRemoteUserInfo, paramInt, paramObject, paramBundle, 0L);
  }
  
  void postToCallbackDelayed(MediaSessionManager.RemoteUserInfo paramRemoteUserInfo, int paramInt, Object paramObject, Bundle paramBundle, long paramLong) {
    synchronized (this.mLock) {
      if (this.mCallback != null)
        this.mCallback.post(paramRemoteUserInfo, paramInt, paramObject, paramBundle, paramLong); 
      return;
    } 
  }
  
  class Token implements Parcelable {
    public Token(MediaSession this$0, ISessionController param1ISessionController) {
      this.mUid = this$0;
      this.mBinder = param1ISessionController;
    }
    
    Token(MediaSession this$0) {
      this.mUid = this$0.readInt();
      this.mBinder = ISessionController.Stub.asInterface(this$0.readStrongBinder());
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mUid);
      param1Parcel.writeStrongBinder(this.mBinder.asBinder());
    }
    
    public int hashCode() {
      int j, i = this.mUid;
      ISessionController iSessionController = this.mBinder;
      if (iSessionController == null) {
        j = 0;
      } else {
        j = iSessionController.asBinder().hashCode();
      } 
      return i * 31 + j;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null)
        return false; 
      if (getClass() != param1Object.getClass())
        return false; 
      param1Object = param1Object;
      if (this.mUid != ((Token)param1Object).mUid)
        return false; 
      ISessionController iSessionController = this.mBinder;
      if (iSessionController == null || ((Token)param1Object).mBinder == null) {
        if (this.mBinder != ((Token)param1Object).mBinder)
          bool = false; 
        return bool;
      } 
      return Objects.equals(iSessionController.asBinder(), ((Token)param1Object).mBinder.asBinder());
    }
    
    public int getUid() {
      return this.mUid;
    }
    
    public ISessionController getBinder() {
      return this.mBinder;
    }
    
    public static final Parcelable.Creator<Token> CREATOR = new Parcelable.Creator<Token>() {
        public MediaSession.Token createFromParcel(Parcel param2Parcel) {
          return new MediaSession.Token(param2Parcel);
        }
        
        public MediaSession.Token[] newArray(int param2Int) {
          return new MediaSession.Token[param2Int];
        }
      };
    
    private final ISessionController mBinder;
    
    private final int mUid;
  }
  
  public static abstract class Callback {
    private MediaSession.CallbackMessageHandler mHandler;
    
    private boolean mMediaPlayPauseKeyPending;
    
    private MediaSession mSession;
    
    public void onCommand(String param1String, Bundle param1Bundle, ResultReceiver param1ResultReceiver) {}
    
    public boolean onMediaButtonEvent(Intent param1Intent) {
      if (this.mSession != null && this.mHandler != null && 
        "android.intent.action.MEDIA_BUTTON".equals(param1Intent.getAction())) {
        KeyEvent keyEvent = (KeyEvent)param1Intent.getParcelableExtra("android.intent.extra.KEY_EVENT");
        if (keyEvent != null && keyEvent.getAction() == 0) {
          long l;
          PlaybackState playbackState = this.mSession.mPlaybackState;
          if (playbackState == null) {
            l = 0L;
          } else {
            l = playbackState.getActions();
          } 
          int i = keyEvent.getKeyCode();
          if (i != 79 && i != 85) {
            handleMediaPlayPauseKeySingleTapIfPending();
            i = keyEvent.getKeyCode();
            if (i != 126) {
              if (i != 127) {
                switch (i) {
                  default:
                    return false;
                  case 90:
                    if ((0x40L & l) != 0L) {
                      onFastForward();
                      return true;
                    } 
                  case 89:
                    if ((0x8L & l) != 0L) {
                      onRewind();
                      return true;
                    } 
                  case 88:
                    if ((0x10L & l) != 0L) {
                      onSkipToPrevious();
                      return true;
                    } 
                  case 87:
                    if ((l & 0x20L) != 0L) {
                      onSkipToNext();
                      return true;
                    } 
                  case 86:
                    break;
                } 
                if ((0x1L & l) != 0L) {
                  onStop();
                  return true;
                } 
              } 
              if ((0x2L & l) != 0L) {
                onPause();
                return true;
              } 
            } 
            if ((0x4L & l) != 0L) {
              onPlay();
              return true;
            } 
          } 
          if (keyEvent.getRepeatCount() > 0) {
            handleMediaPlayPauseKeySingleTapIfPending();
          } else if (this.mMediaPlayPauseKeyPending) {
            this.mHandler.removeMessages(24);
            this.mMediaPlayPauseKeyPending = false;
            if ((l & 0x20L) != 0L)
              onSkipToNext(); 
          } else {
            this.mMediaPlayPauseKeyPending = true;
            MediaSession mediaSession = this.mSession;
            MediaSessionManager.RemoteUserInfo remoteUserInfo = mediaSession.getCurrentControllerInfo();
            l = ViewConfiguration.getDoubleTapTimeout();
            mediaSession.dispatchMediaButtonDelayed(remoteUserInfo, param1Intent, l);
          } 
          return true;
        } 
      } 
    }
    
    private void handleMediaPlayPauseKeySingleTapIfPending() {
      long l;
      boolean bool2, bool3;
      if (!this.mMediaPlayPauseKeyPending)
        return; 
      boolean bool1 = false;
      this.mMediaPlayPauseKeyPending = false;
      this.mHandler.removeMessages(24);
      PlaybackState playbackState = this.mSession.mPlaybackState;
      if (playbackState == null) {
        l = 0L;
      } else {
        l = playbackState.getActions();
      } 
      if (playbackState != null && 
        playbackState.getState() == 3) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((0x204L & l) != 0L) {
        bool3 = true;
      } else {
        bool3 = false;
      } 
      if ((0x202L & l) != 0L)
        bool1 = true; 
      if (bool2 && bool1) {
        onPause();
      } else if (!bool2 && bool3) {
        onPlay();
      } 
    }
    
    public void onPrepare() {}
    
    public void onPrepareFromMediaId(String param1String, Bundle param1Bundle) {}
    
    public void onPrepareFromSearch(String param1String, Bundle param1Bundle) {}
    
    public void onPrepareFromUri(Uri param1Uri, Bundle param1Bundle) {}
    
    public void onPlay() {}
    
    public void onPlayFromSearch(String param1String, Bundle param1Bundle) {}
    
    public void onPlayFromMediaId(String param1String, Bundle param1Bundle) {}
    
    public void onPlayFromUri(Uri param1Uri, Bundle param1Bundle) {}
    
    public void onSkipToQueueItem(long param1Long) {}
    
    public void onPause() {}
    
    public void onSkipToNext() {}
    
    public void onSkipToPrevious() {}
    
    public void onFastForward() {}
    
    public void onRewind() {}
    
    public void onStop() {}
    
    public void onSeekTo(long param1Long) {}
    
    public void onSetRating(Rating param1Rating) {}
    
    public void onSetPlaybackSpeed(float param1Float) {}
    
    public void onCustomAction(String param1String, Bundle param1Bundle) {}
  }
  
  class CallbackStub extends ISessionCallback.Stub {
    private WeakReference<MediaSession> mMediaSession;
    
    public CallbackStub(MediaSession this$0) {
      this.mMediaSession = new WeakReference<>(this$0);
    }
    
    private static MediaSessionManager.RemoteUserInfo createRemoteUserInfo(String param1String, int param1Int1, int param1Int2) {
      return new MediaSessionManager.RemoteUserInfo(param1String, param1Int1, param1Int2);
    }
    
    public void onCommand(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle, ResultReceiver param1ResultReceiver) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchCommand(createRemoteUserInfo(param1String1, param1Int1, param1Int2), param1String2, param1Bundle, param1ResultReceiver); 
    }
    
    public void onMediaButton(String param1String, int param1Int1, int param1Int2, Intent param1Intent, int param1Int3, ResultReceiver param1ResultReceiver) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        try {
          mediaSession.dispatchMediaButton(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Intent);
        } finally {
          if (param1ResultReceiver != null)
            param1ResultReceiver.send(param1Int3, null); 
        }  
      if (param1ResultReceiver != null)
        param1ResultReceiver.send(param1Int3, null); 
    }
    
    public void onMediaButtonFromController(String param1String, int param1Int1, int param1Int2, Intent param1Intent) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchMediaButton(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Intent); 
    }
    
    public void onPrepare(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPrepare(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onPrepareFromMediaId(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null) {
        MediaSessionManager.RemoteUserInfo remoteUserInfo = createRemoteUserInfo(param1String1, param1Int1, param1Int2);
        mediaSession.dispatchPrepareFromMediaId(remoteUserInfo, param1String2, param1Bundle);
      } 
    }
    
    public void onPrepareFromSearch(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null) {
        MediaSessionManager.RemoteUserInfo remoteUserInfo = createRemoteUserInfo(param1String1, param1Int1, param1Int2);
        mediaSession.dispatchPrepareFromSearch(remoteUserInfo, param1String2, param1Bundle);
      } 
    }
    
    public void onPrepareFromUri(String param1String, int param1Int1, int param1Int2, Uri param1Uri, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPrepareFromUri(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Uri, param1Bundle); 
    }
    
    public void onPlay(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPlay(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onPlayFromMediaId(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPlayFromMediaId(createRemoteUserInfo(param1String1, param1Int1, param1Int2), param1String2, param1Bundle); 
    }
    
    public void onPlayFromSearch(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPlayFromSearch(createRemoteUserInfo(param1String1, param1Int1, param1Int2), param1String2, param1Bundle); 
    }
    
    public void onPlayFromUri(String param1String, int param1Int1, int param1Int2, Uri param1Uri, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPlayFromUri(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Uri, param1Bundle); 
    }
    
    public void onSkipToTrack(String param1String, int param1Int1, int param1Int2, long param1Long) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchSkipToItem(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Long); 
    }
    
    public void onPause(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPause(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onStop(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchStop(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onNext(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchNext(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onPrevious(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchPrevious(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onFastForward(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchFastForward(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onRewind(String param1String, int param1Int1, int param1Int2) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchRewind(createRemoteUserInfo(param1String, param1Int1, param1Int2)); 
    }
    
    public void onSeekTo(String param1String, int param1Int1, int param1Int2, long param1Long) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchSeekTo(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Long); 
    }
    
    public void onRate(String param1String, int param1Int1, int param1Int2, Rating param1Rating) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchRate(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Rating); 
    }
    
    public void onSetPlaybackSpeed(String param1String, int param1Int1, int param1Int2, float param1Float) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null) {
        MediaSessionManager.RemoteUserInfo remoteUserInfo = createRemoteUserInfo(param1String, param1Int1, param1Int2);
        mediaSession.dispatchSetPlaybackSpeed(remoteUserInfo, param1Float);
      } 
    }
    
    public void onCustomAction(String param1String1, int param1Int1, int param1Int2, String param1String2, Bundle param1Bundle) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchCustomAction(createRemoteUserInfo(param1String1, param1Int1, param1Int2), param1String2, param1Bundle); 
    }
    
    public void onAdjustVolume(String param1String, int param1Int1, int param1Int2, int param1Int3) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchAdjustVolume(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Int3); 
    }
    
    public void onSetVolumeTo(String param1String, int param1Int1, int param1Int2, int param1Int3) {
      MediaSession mediaSession = this.mMediaSession.get();
      if (mediaSession != null)
        mediaSession.dispatchSetVolumeTo(createRemoteUserInfo(param1String, param1Int1, param1Int2), param1Int3); 
    }
  }
  
  class QueueItem implements Parcelable {
    public QueueItem(MediaSession this$0, long param1Long) {
      if (this$0 != null) {
        if (param1Long != -1L) {
          this.mDescription = (MediaDescription)this$0;
          this.mId = param1Long;
          return;
        } 
        throw new IllegalArgumentException("Id cannot be QueueItem.UNKNOWN_ID");
      } 
      throw new IllegalArgumentException("Description cannot be null.");
    }
    
    private QueueItem(MediaSession this$0) {
      this.mDescription = MediaDescription.CREATOR.createFromParcel((Parcel)this$0);
      this.mId = this$0.readLong();
    }
    
    public MediaDescription getDescription() {
      return this.mDescription;
    }
    
    public long getQueueId() {
      return this.mId;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      this.mDescription.writeToParcel(param1Parcel, param1Int);
      param1Parcel.writeLong(this.mId);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public static final Parcelable.Creator<QueueItem> CREATOR = new Parcelable.Creator<QueueItem>() {
        public MediaSession.QueueItem createFromParcel(Parcel param2Parcel) {
          return new MediaSession.QueueItem();
        }
        
        public MediaSession.QueueItem[] newArray(int param2Int) {
          return new MediaSession.QueueItem[param2Int];
        }
      };
    
    public static final int UNKNOWN_ID = -1;
    
    private final MediaDescription mDescription;
    
    private final long mId;
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MediaSession.QueueItem {Description=");
      stringBuilder.append(this.mDescription);
      stringBuilder.append(", Id=");
      stringBuilder.append(this.mId);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object == null)
        return false; 
      if (!(param1Object instanceof QueueItem))
        return false; 
      param1Object = param1Object;
      if (this.mId != ((QueueItem)param1Object).mId)
        return false; 
      if (!Objects.equals(this.mDescription, ((QueueItem)param1Object).mDescription))
        return false; 
      return true;
    }
  }
  
  private static final class Command {
    public final String command;
    
    public final Bundle extras;
    
    public final ResultReceiver stub;
    
    Command(String param1String, Bundle param1Bundle, ResultReceiver param1ResultReceiver) {
      this.command = param1String;
      this.extras = param1Bundle;
      this.stub = param1ResultReceiver;
    }
  }
  
  class CallbackMessageHandler extends Handler {
    private static final int MSG_ADJUST_VOLUME = 22;
    
    private static final int MSG_COMMAND = 1;
    
    private static final int MSG_CUSTOM_ACTION = 21;
    
    private static final int MSG_FAST_FORWARD = 16;
    
    private static final int MSG_MEDIA_BUTTON = 2;
    
    private static final int MSG_NEXT = 14;
    
    private static final int MSG_PAUSE = 12;
    
    private static final int MSG_PLAY = 7;
    
    private static final int MSG_PLAY_MEDIA_ID = 8;
    
    private static final int MSG_PLAY_PAUSE_KEY_DOUBLE_TAP_TIMEOUT = 24;
    
    private static final int MSG_PLAY_SEARCH = 9;
    
    private static final int MSG_PLAY_URI = 10;
    
    private static final int MSG_PREPARE = 3;
    
    private static final int MSG_PREPARE_MEDIA_ID = 4;
    
    private static final int MSG_PREPARE_SEARCH = 5;
    
    private static final int MSG_PREPARE_URI = 6;
    
    private static final int MSG_PREVIOUS = 15;
    
    private static final int MSG_RATE = 19;
    
    private static final int MSG_REWIND = 17;
    
    private static final int MSG_SEEK_TO = 18;
    
    private static final int MSG_SET_PLAYBACK_SPEED = 20;
    
    private static final int MSG_SET_VOLUME = 23;
    
    private static final int MSG_SKIP_TO_ITEM = 11;
    
    private static final int MSG_STOP = 13;
    
    private MediaSession.Callback mCallback;
    
    private MediaSessionManager.RemoteUserInfo mCurrentControllerInfo;
    
    final MediaSession this$0;
    
    CallbackMessageHandler(Looper param1Looper, MediaSession.Callback param1Callback) {
      super(param1Looper);
      this.mCallback = param1Callback;
      MediaSession.Callback.access$502(param1Callback, this);
    }
    
    void post(MediaSessionManager.RemoteUserInfo param1RemoteUserInfo, int param1Int, Object param1Object, Bundle param1Bundle, long param1Long) {
      Pair pair = Pair.create(param1RemoteUserInfo, param1Object);
      Message message = obtainMessage(param1Int, pair);
      message.setAsynchronous(true);
      message.setData(param1Bundle);
      if (param1Long > 0L) {
        sendMessageDelayed(message, param1Long);
      } else {
        sendMessage(message);
      } 
    }
    
    public void handleMessage(Message param1Message) {
      MediaSession.Command command;
      this.mCurrentControllerInfo = (MediaSessionManager.RemoteUserInfo)((Pair)param1Message.obj).first;
      null = ((Pair)param1Message.obj).second;
      switch (param1Message.what) {
        case 24:
          this.mCallback.handleMediaPlayPauseKeySingleTapIfPending();
          break;
        case 23:
          synchronized (MediaSession.this.mLock) {
            VolumeProvider volumeProvider = MediaSession.this.mVolumeProvider;
            if (volumeProvider != null)
              volumeProvider.onSetVolumeTo(((Integer)null).intValue()); 
          } 
          break;
        case 22:
          synchronized (MediaSession.this.mLock) {
            VolumeProvider volumeProvider = MediaSession.this.mVolumeProvider;
            if (volumeProvider != null)
              volumeProvider.onAdjustVolume(((Integer)SYNTHETIC_LOCAL_VARIABLE_2).intValue()); 
          } 
          break;
        case 21:
          this.mCallback.onCustomAction((String)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 20:
          this.mCallback.onSetPlaybackSpeed(((Float)SYNTHETIC_LOCAL_VARIABLE_2).floatValue());
          break;
        case 19:
          this.mCallback.onSetRating((Rating)SYNTHETIC_LOCAL_VARIABLE_2);
          break;
        case 18:
          this.mCallback.onSeekTo(((Long)SYNTHETIC_LOCAL_VARIABLE_2).longValue());
          break;
        case 17:
          this.mCallback.onRewind();
          break;
        case 16:
          this.mCallback.onFastForward();
          break;
        case 15:
          this.mCallback.onSkipToPrevious();
          break;
        case 14:
          this.mCallback.onSkipToNext();
          break;
        case 13:
          this.mCallback.onStop();
          break;
        case 12:
          this.mCallback.onPause();
          break;
        case 11:
          this.mCallback.onSkipToQueueItem(((Long)SYNTHETIC_LOCAL_VARIABLE_2).longValue());
          break;
        case 10:
          this.mCallback.onPlayFromUri((Uri)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 9:
          this.mCallback.onPlayFromSearch((String)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 8:
          this.mCallback.onPlayFromMediaId((String)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 7:
          this.mCallback.onPlay();
          break;
        case 6:
          this.mCallback.onPrepareFromUri((Uri)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 5:
          this.mCallback.onPrepareFromSearch((String)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 4:
          this.mCallback.onPrepareFromMediaId((String)SYNTHETIC_LOCAL_VARIABLE_2, param1Message.getData());
          break;
        case 3:
          this.mCallback.onPrepare();
          break;
        case 2:
          this.mCallback.onMediaButtonEvent((Intent)SYNTHETIC_LOCAL_VARIABLE_2);
          break;
        case 1:
          command = (MediaSession.Command)SYNTHETIC_LOCAL_VARIABLE_2;
          this.mCallback.onCommand(command.command, command.extras, command.stub);
          break;
      } 
      this.mCurrentControllerInfo = null;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SessionFlags {}
}
