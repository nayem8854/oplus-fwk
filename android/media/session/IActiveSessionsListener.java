package android.media.session;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IActiveSessionsListener extends IInterface {
  void onActiveSessionsChanged(List<MediaSession.Token> paramList) throws RemoteException;
  
  class Default implements IActiveSessionsListener {
    public void onActiveSessionsChanged(List<MediaSession.Token> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IActiveSessionsListener {
    private static final String DESCRIPTOR = "android.media.session.IActiveSessionsListener";
    
    static final int TRANSACTION_onActiveSessionsChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.session.IActiveSessionsListener");
    }
    
    public static IActiveSessionsListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.IActiveSessionsListener");
      if (iInterface != null && iInterface instanceof IActiveSessionsListener)
        return (IActiveSessionsListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onActiveSessionsChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.session.IActiveSessionsListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.session.IActiveSessionsListener");
      ArrayList<MediaSession.Token> arrayList = param1Parcel1.createTypedArrayList(MediaSession.Token.CREATOR);
      onActiveSessionsChanged(arrayList);
      return true;
    }
    
    private static class Proxy implements IActiveSessionsListener {
      public static IActiveSessionsListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.IActiveSessionsListener";
      }
      
      public void onActiveSessionsChanged(List<MediaSession.Token> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.IActiveSessionsListener");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IActiveSessionsListener.Stub.getDefaultImpl() != null) {
            IActiveSessionsListener.Stub.getDefaultImpl().onActiveSessionsChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IActiveSessionsListener param1IActiveSessionsListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IActiveSessionsListener != null) {
          Proxy.sDefaultImpl = param1IActiveSessionsListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IActiveSessionsListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
