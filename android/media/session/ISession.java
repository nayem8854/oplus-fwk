package android.media.session;

import android.app.PendingIntent;
import android.content.pm.ParceledListSlice;
import android.media.AudioAttributes;
import android.media.MediaMetadata;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.text.TextUtils;

public interface ISession extends IInterface {
  void destroySession() throws RemoteException;
  
  ISessionController getController() throws RemoteException;
  
  void sendEvent(String paramString, Bundle paramBundle) throws RemoteException;
  
  void setActive(boolean paramBoolean) throws RemoteException;
  
  void setCurrentVolume(int paramInt) throws RemoteException;
  
  void setExtras(Bundle paramBundle) throws RemoteException;
  
  void setFlags(int paramInt) throws RemoteException;
  
  void setLaunchPendingIntent(PendingIntent paramPendingIntent) throws RemoteException;
  
  void setMediaButtonReceiver(PendingIntent paramPendingIntent) throws RemoteException;
  
  void setMetadata(MediaMetadata paramMediaMetadata, long paramLong, String paramString) throws RemoteException;
  
  void setPlaybackState(PlaybackState paramPlaybackState) throws RemoteException;
  
  void setPlaybackToLocal(AudioAttributes paramAudioAttributes) throws RemoteException;
  
  void setPlaybackToRemote(int paramInt1, int paramInt2, String paramString) throws RemoteException;
  
  void setQueue(ParceledListSlice paramParceledListSlice) throws RemoteException;
  
  void setQueueTitle(CharSequence paramCharSequence) throws RemoteException;
  
  void setRatingType(int paramInt) throws RemoteException;
  
  class Default implements ISession {
    public void sendEvent(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public ISessionController getController() throws RemoteException {
      return null;
    }
    
    public void setFlags(int param1Int) throws RemoteException {}
    
    public void setActive(boolean param1Boolean) throws RemoteException {}
    
    public void setMediaButtonReceiver(PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void setLaunchPendingIntent(PendingIntent param1PendingIntent) throws RemoteException {}
    
    public void destroySession() throws RemoteException {}
    
    public void setMetadata(MediaMetadata param1MediaMetadata, long param1Long, String param1String) throws RemoteException {}
    
    public void setPlaybackState(PlaybackState param1PlaybackState) throws RemoteException {}
    
    public void setQueue(ParceledListSlice param1ParceledListSlice) throws RemoteException {}
    
    public void setQueueTitle(CharSequence param1CharSequence) throws RemoteException {}
    
    public void setExtras(Bundle param1Bundle) throws RemoteException {}
    
    public void setRatingType(int param1Int) throws RemoteException {}
    
    public void setPlaybackToLocal(AudioAttributes param1AudioAttributes) throws RemoteException {}
    
    public void setPlaybackToRemote(int param1Int1, int param1Int2, String param1String) throws RemoteException {}
    
    public void setCurrentVolume(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISession {
    private static final String DESCRIPTOR = "android.media.session.ISession";
    
    static final int TRANSACTION_destroySession = 7;
    
    static final int TRANSACTION_getController = 2;
    
    static final int TRANSACTION_sendEvent = 1;
    
    static final int TRANSACTION_setActive = 4;
    
    static final int TRANSACTION_setCurrentVolume = 16;
    
    static final int TRANSACTION_setExtras = 12;
    
    static final int TRANSACTION_setFlags = 3;
    
    static final int TRANSACTION_setLaunchPendingIntent = 6;
    
    static final int TRANSACTION_setMediaButtonReceiver = 5;
    
    static final int TRANSACTION_setMetadata = 8;
    
    static final int TRANSACTION_setPlaybackState = 9;
    
    static final int TRANSACTION_setPlaybackToLocal = 14;
    
    static final int TRANSACTION_setPlaybackToRemote = 15;
    
    static final int TRANSACTION_setQueue = 10;
    
    static final int TRANSACTION_setQueueTitle = 11;
    
    static final int TRANSACTION_setRatingType = 13;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISession");
    }
    
    public static ISession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISession");
      if (iInterface != null && iInterface instanceof ISession)
        return (ISession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "setCurrentVolume";
        case 15:
          return "setPlaybackToRemote";
        case 14:
          return "setPlaybackToLocal";
        case 13:
          return "setRatingType";
        case 12:
          return "setExtras";
        case 11:
          return "setQueueTitle";
        case 10:
          return "setQueue";
        case 9:
          return "setPlaybackState";
        case 8:
          return "setMetadata";
        case 7:
          return "destroySession";
        case 6:
          return "setLaunchPendingIntent";
        case 5:
          return "setMediaButtonReceiver";
        case 4:
          return "setActive";
        case 3:
          return "setFlags";
        case 2:
          return "getController";
        case 1:
          break;
      } 
      return "sendEvent";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        String str1;
        ISessionController iSessionController;
        MediaMetadata mediaMetadata;
        long l;
        boolean bool;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("android.media.session.ISession");
            param1Int1 = param1Parcel1.readInt();
            setCurrentVolume(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.session.ISession");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            setPlaybackToRemote(param1Int2, param1Int1, str1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setPlaybackToLocal((AudioAttributes)str1);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.media.session.ISession");
            param1Int1 = str1.readInt();
            setRatingType(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setExtras((Bundle)str1);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              CharSequence charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setQueueTitle(str1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              ParceledListSlice parceledListSlice = ParceledListSlice.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setQueue((ParceledListSlice)str1);
            param1Parcel2.writeNoException();
            return true;
          case 9:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              PlaybackState playbackState = PlaybackState.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setPlaybackState((PlaybackState)str1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              mediaMetadata = MediaMetadata.CREATOR.createFromParcel((Parcel)str1);
            } else {
              mediaMetadata = null;
            } 
            l = str1.readLong();
            str1 = str1.readString();
            setMetadata(mediaMetadata, l, str1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            str1.enforceInterface("android.media.session.ISession");
            destroySession();
            param1Parcel2.writeNoException();
            return true;
          case 6:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setLaunchPendingIntent((PendingIntent)str1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              PendingIntent pendingIntent = PendingIntent.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setMediaButtonReceiver((PendingIntent)str1);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            str1.enforceInterface("android.media.session.ISession");
            if (str1.readInt() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            setActive(bool);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            str1.enforceInterface("android.media.session.ISession");
            param1Int1 = str1.readInt();
            setFlags(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.media.session.ISession");
            iSessionController = getController();
            param1Parcel2.writeNoException();
            if (iSessionController != null) {
              IBinder iBinder = iSessionController.asBinder();
            } else {
              iSessionController = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iSessionController);
            return true;
          case 1:
            break;
        } 
        iSessionController.enforceInterface("android.media.session.ISession");
        String str2 = iSessionController.readString();
        if (iSessionController.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iSessionController);
        } else {
          iSessionController = null;
        } 
        sendEvent(str2, (Bundle)iSessionController);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.media.session.ISession");
      return true;
    }
    
    private static class Proxy implements ISession {
      public static ISession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISession";
      }
      
      public void sendEvent(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().sendEvent(param2String, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ISessionController getController() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null)
            return ISession.Stub.getDefaultImpl().getController(); 
          parcel2.readException();
          return ISessionController.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFlags(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setFlags(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setActive(boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool1 && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setActive(param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMediaButtonReceiver(PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setMediaButtonReceiver(param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLaunchPendingIntent(PendingIntent param2PendingIntent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2PendingIntent != null) {
            parcel1.writeInt(1);
            param2PendingIntent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setLaunchPendingIntent(param2PendingIntent);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void destroySession() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().destroySession();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMetadata(MediaMetadata param2MediaMetadata, long param2Long, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2MediaMetadata != null) {
            parcel1.writeInt(1);
            param2MediaMetadata.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeLong(param2Long);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setMetadata(param2MediaMetadata, param2Long, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPlaybackState(PlaybackState param2PlaybackState) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2PlaybackState != null) {
            parcel1.writeInt(1);
            param2PlaybackState.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setPlaybackState(param2PlaybackState);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setQueue(ParceledListSlice param2ParceledListSlice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2ParceledListSlice != null) {
            parcel1.writeInt(1);
            param2ParceledListSlice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setQueue(param2ParceledListSlice);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setQueueTitle(CharSequence param2CharSequence) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2CharSequence != null) {
            parcel1.writeInt(1);
            TextUtils.writeToParcel(param2CharSequence, parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setQueueTitle(param2CharSequence);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setExtras(Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setExtras(param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRatingType(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setRatingType(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPlaybackToLocal(AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          if (param2AudioAttributes != null) {
            parcel1.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setPlaybackToLocal(param2AudioAttributes);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPlaybackToRemote(int param2Int1, int param2Int2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setPlaybackToRemote(param2Int1, param2Int2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCurrentVolume(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISession");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ISession.Stub.getDefaultImpl() != null) {
            ISession.Stub.getDefaultImpl().setCurrentVolume(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISession param1ISession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISession != null) {
          Proxy.sDefaultImpl = param1ISession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
