package android.media.session;

import android.app.PendingIntent;
import android.content.pm.ParceledListSlice;
import android.media.MediaMetadata;
import android.media.Rating;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.text.TextUtils;
import android.view.KeyEvent;

public interface ISessionController extends IInterface {
  void adjustVolume(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  void fastForward(String paramString) throws RemoteException;
  
  Bundle getExtras() throws RemoteException;
  
  long getFlags() throws RemoteException;
  
  PendingIntent getLaunchPendingIntent() throws RemoteException;
  
  MediaMetadata getMetadata() throws RemoteException;
  
  String getPackageName() throws RemoteException;
  
  PlaybackState getPlaybackState() throws RemoteException;
  
  ParceledListSlice getQueue() throws RemoteException;
  
  CharSequence getQueueTitle() throws RemoteException;
  
  int getRatingType() throws RemoteException;
  
  Bundle getSessionInfo() throws RemoteException;
  
  String getTag() throws RemoteException;
  
  MediaController.PlaybackInfo getVolumeAttributes() throws RemoteException;
  
  void next(String paramString) throws RemoteException;
  
  void pause(String paramString) throws RemoteException;
  
  void play(String paramString) throws RemoteException;
  
  void playFromMediaId(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void playFromSearch(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void playFromUri(String paramString, Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void prepare(String paramString) throws RemoteException;
  
  void prepareFromMediaId(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void prepareFromSearch(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  void prepareFromUri(String paramString, Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void previous(String paramString) throws RemoteException;
  
  void rate(String paramString, Rating paramRating) throws RemoteException;
  
  void registerCallback(String paramString, ISessionControllerCallback paramISessionControllerCallback) throws RemoteException;
  
  void rewind(String paramString) throws RemoteException;
  
  void seekTo(String paramString, long paramLong) throws RemoteException;
  
  void sendCommand(String paramString1, String paramString2, Bundle paramBundle, ResultReceiver paramResultReceiver) throws RemoteException;
  
  void sendCustomAction(String paramString1, String paramString2, Bundle paramBundle) throws RemoteException;
  
  boolean sendMediaButton(String paramString, KeyEvent paramKeyEvent) throws RemoteException;
  
  void setPlaybackSpeed(String paramString, float paramFloat) throws RemoteException;
  
  void setVolumeTo(String paramString1, String paramString2, int paramInt1, int paramInt2) throws RemoteException;
  
  void skipToQueueItem(String paramString, long paramLong) throws RemoteException;
  
  void stop(String paramString) throws RemoteException;
  
  void unregisterCallback(ISessionControllerCallback paramISessionControllerCallback) throws RemoteException;
  
  class Default implements ISessionController {
    public void sendCommand(String param1String1, String param1String2, Bundle param1Bundle, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public boolean sendMediaButton(String param1String, KeyEvent param1KeyEvent) throws RemoteException {
      return false;
    }
    
    public void registerCallback(String param1String, ISessionControllerCallback param1ISessionControllerCallback) throws RemoteException {}
    
    public void unregisterCallback(ISessionControllerCallback param1ISessionControllerCallback) throws RemoteException {}
    
    public String getPackageName() throws RemoteException {
      return null;
    }
    
    public String getTag() throws RemoteException {
      return null;
    }
    
    public Bundle getSessionInfo() throws RemoteException {
      return null;
    }
    
    public PendingIntent getLaunchPendingIntent() throws RemoteException {
      return null;
    }
    
    public long getFlags() throws RemoteException {
      return 0L;
    }
    
    public MediaController.PlaybackInfo getVolumeAttributes() throws RemoteException {
      return null;
    }
    
    public void adjustVolume(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setVolumeTo(String param1String1, String param1String2, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void prepare(String param1String) throws RemoteException {}
    
    public void prepareFromMediaId(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void prepareFromSearch(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void prepareFromUri(String param1String, Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void play(String param1String) throws RemoteException {}
    
    public void playFromMediaId(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void playFromSearch(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public void playFromUri(String param1String, Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void skipToQueueItem(String param1String, long param1Long) throws RemoteException {}
    
    public void pause(String param1String) throws RemoteException {}
    
    public void stop(String param1String) throws RemoteException {}
    
    public void next(String param1String) throws RemoteException {}
    
    public void previous(String param1String) throws RemoteException {}
    
    public void fastForward(String param1String) throws RemoteException {}
    
    public void rewind(String param1String) throws RemoteException {}
    
    public void seekTo(String param1String, long param1Long) throws RemoteException {}
    
    public void rate(String param1String, Rating param1Rating) throws RemoteException {}
    
    public void setPlaybackSpeed(String param1String, float param1Float) throws RemoteException {}
    
    public void sendCustomAction(String param1String1, String param1String2, Bundle param1Bundle) throws RemoteException {}
    
    public MediaMetadata getMetadata() throws RemoteException {
      return null;
    }
    
    public PlaybackState getPlaybackState() throws RemoteException {
      return null;
    }
    
    public ParceledListSlice getQueue() throws RemoteException {
      return null;
    }
    
    public CharSequence getQueueTitle() throws RemoteException {
      return null;
    }
    
    public Bundle getExtras() throws RemoteException {
      return null;
    }
    
    public int getRatingType() throws RemoteException {
      return 0;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISessionController {
    private static final String DESCRIPTOR = "android.media.session.ISessionController";
    
    static final int TRANSACTION_adjustVolume = 11;
    
    static final int TRANSACTION_fastForward = 26;
    
    static final int TRANSACTION_getExtras = 36;
    
    static final int TRANSACTION_getFlags = 9;
    
    static final int TRANSACTION_getLaunchPendingIntent = 8;
    
    static final int TRANSACTION_getMetadata = 32;
    
    static final int TRANSACTION_getPackageName = 5;
    
    static final int TRANSACTION_getPlaybackState = 33;
    
    static final int TRANSACTION_getQueue = 34;
    
    static final int TRANSACTION_getQueueTitle = 35;
    
    static final int TRANSACTION_getRatingType = 37;
    
    static final int TRANSACTION_getSessionInfo = 7;
    
    static final int TRANSACTION_getTag = 6;
    
    static final int TRANSACTION_getVolumeAttributes = 10;
    
    static final int TRANSACTION_next = 24;
    
    static final int TRANSACTION_pause = 22;
    
    static final int TRANSACTION_play = 17;
    
    static final int TRANSACTION_playFromMediaId = 18;
    
    static final int TRANSACTION_playFromSearch = 19;
    
    static final int TRANSACTION_playFromUri = 20;
    
    static final int TRANSACTION_prepare = 13;
    
    static final int TRANSACTION_prepareFromMediaId = 14;
    
    static final int TRANSACTION_prepareFromSearch = 15;
    
    static final int TRANSACTION_prepareFromUri = 16;
    
    static final int TRANSACTION_previous = 25;
    
    static final int TRANSACTION_rate = 29;
    
    static final int TRANSACTION_registerCallback = 3;
    
    static final int TRANSACTION_rewind = 27;
    
    static final int TRANSACTION_seekTo = 28;
    
    static final int TRANSACTION_sendCommand = 1;
    
    static final int TRANSACTION_sendCustomAction = 31;
    
    static final int TRANSACTION_sendMediaButton = 2;
    
    static final int TRANSACTION_setPlaybackSpeed = 30;
    
    static final int TRANSACTION_setVolumeTo = 12;
    
    static final int TRANSACTION_skipToQueueItem = 21;
    
    static final int TRANSACTION_stop = 23;
    
    static final int TRANSACTION_unregisterCallback = 4;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISessionController");
    }
    
    public static ISessionController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISessionController");
      if (iInterface != null && iInterface instanceof ISessionController)
        return (ISessionController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 37:
          return "getRatingType";
        case 36:
          return "getExtras";
        case 35:
          return "getQueueTitle";
        case 34:
          return "getQueue";
        case 33:
          return "getPlaybackState";
        case 32:
          return "getMetadata";
        case 31:
          return "sendCustomAction";
        case 30:
          return "setPlaybackSpeed";
        case 29:
          return "rate";
        case 28:
          return "seekTo";
        case 27:
          return "rewind";
        case 26:
          return "fastForward";
        case 25:
          return "previous";
        case 24:
          return "next";
        case 23:
          return "stop";
        case 22:
          return "pause";
        case 21:
          return "skipToQueueItem";
        case 20:
          return "playFromUri";
        case 19:
          return "playFromSearch";
        case 18:
          return "playFromMediaId";
        case 17:
          return "play";
        case 16:
          return "prepareFromUri";
        case 15:
          return "prepareFromSearch";
        case 14:
          return "prepareFromMediaId";
        case 13:
          return "prepare";
        case 12:
          return "setVolumeTo";
        case 11:
          return "adjustVolume";
        case 10:
          return "getVolumeAttributes";
        case 9:
          return "getFlags";
        case 8:
          return "getLaunchPendingIntent";
        case 7:
          return "getSessionInfo";
        case 6:
          return "getTag";
        case 5:
          return "getPackageName";
        case 4:
          return "unregisterCallback";
        case 3:
          return "registerCallback";
        case 2:
          return "sendMediaButton";
        case 1:
          break;
      } 
      return "sendCommand";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        Bundle bundle2;
        CharSequence charSequence;
        ParceledListSlice parceledListSlice;
        PlaybackState playbackState;
        MediaMetadata mediaMetadata;
        String str2;
        MediaController.PlaybackInfo playbackInfo;
        PendingIntent pendingIntent;
        Bundle bundle1;
        String str1;
        ISessionControllerCallback iSessionControllerCallback;
        String str3;
        float f;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 37:
            param1Parcel1.enforceInterface("android.media.session.ISessionController");
            param1Int1 = getRatingType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 36:
            param1Parcel1.enforceInterface("android.media.session.ISessionController");
            bundle2 = getExtras();
            param1Parcel2.writeNoException();
            if (bundle2 != null) {
              param1Parcel2.writeInt(1);
              bundle2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 35:
            bundle2.enforceInterface("android.media.session.ISessionController");
            charSequence = getQueueTitle();
            param1Parcel2.writeNoException();
            if (charSequence != null) {
              param1Parcel2.writeInt(1);
              TextUtils.writeToParcel(charSequence, param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 34:
            charSequence.enforceInterface("android.media.session.ISessionController");
            parceledListSlice = getQueue();
            param1Parcel2.writeNoException();
            if (parceledListSlice != null) {
              param1Parcel2.writeInt(1);
              parceledListSlice.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 33:
            parceledListSlice.enforceInterface("android.media.session.ISessionController");
            playbackState = getPlaybackState();
            param1Parcel2.writeNoException();
            if (playbackState != null) {
              param1Parcel2.writeInt(1);
              playbackState.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 32:
            playbackState.enforceInterface("android.media.session.ISessionController");
            mediaMetadata = getMetadata();
            param1Parcel2.writeNoException();
            if (mediaMetadata != null) {
              param1Parcel2.writeInt(1);
              mediaMetadata.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 31:
            mediaMetadata.enforceInterface("android.media.session.ISessionController");
            str3 = mediaMetadata.readString();
            str4 = mediaMetadata.readString();
            if (mediaMetadata.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)mediaMetadata);
            } else {
              mediaMetadata = null;
            } 
            sendCustomAction(str3, str4, (Bundle)mediaMetadata);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            mediaMetadata.enforceInterface("android.media.session.ISessionController");
            str3 = mediaMetadata.readString();
            f = mediaMetadata.readFloat();
            setPlaybackSpeed(str3, f);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            mediaMetadata.enforceInterface("android.media.session.ISessionController");
            str3 = mediaMetadata.readString();
            if (mediaMetadata.readInt() != 0) {
              Rating rating = Rating.CREATOR.createFromParcel((Parcel)mediaMetadata);
            } else {
              mediaMetadata = null;
            } 
            rate(str3, (Rating)mediaMetadata);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            mediaMetadata.enforceInterface("android.media.session.ISessionController");
            str3 = mediaMetadata.readString();
            l = mediaMetadata.readLong();
            seekTo(str3, l);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            mediaMetadata.enforceInterface("android.media.session.ISessionController");
            str2 = mediaMetadata.readString();
            rewind(str2);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            fastForward(str2);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            previous(str2);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            next(str2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            stop(str2);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            pause(str2);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            str2.enforceInterface("android.media.session.ISessionController");
            str3 = str2.readString();
            l = str2.readLong();
            skipToQueueItem(str3, l);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str3 = null;
            } 
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            playFromUri(str4, (Uri)str3, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            str3 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            playFromSearch(str4, str3, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            str3 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            playFromMediaId(str4, str3, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            play(str2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str3 = null;
            } 
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            prepareFromUri(str4, (Uri)str3, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            str3 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            prepareFromSearch(str4, str3, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str2.enforceInterface("android.media.session.ISessionController");
            str3 = str2.readString();
            str4 = str2.readString();
            if (str2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str2);
            } else {
              str2 = null;
            } 
            prepareFromMediaId(str3, str4, (Bundle)str2);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str2.enforceInterface("android.media.session.ISessionController");
            str2 = str2.readString();
            prepare(str2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str2.enforceInterface("android.media.session.ISessionController");
            str4 = str2.readString();
            str3 = str2.readString();
            param1Int1 = str2.readInt();
            param1Int2 = str2.readInt();
            setVolumeTo(str4, str3, param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.media.session.ISessionController");
            str3 = str2.readString();
            str4 = str2.readString();
            param1Int2 = str2.readInt();
            param1Int1 = str2.readInt();
            adjustVolume(str3, str4, param1Int2, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            str2.enforceInterface("android.media.session.ISessionController");
            playbackInfo = getVolumeAttributes();
            param1Parcel2.writeNoException();
            if (playbackInfo != null) {
              param1Parcel2.writeInt(1);
              playbackInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            playbackInfo.enforceInterface("android.media.session.ISessionController");
            l = getFlags();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 8:
            playbackInfo.enforceInterface("android.media.session.ISessionController");
            pendingIntent = getLaunchPendingIntent();
            param1Parcel2.writeNoException();
            if (pendingIntent != null) {
              param1Parcel2.writeInt(1);
              pendingIntent.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 7:
            pendingIntent.enforceInterface("android.media.session.ISessionController");
            bundle1 = getSessionInfo();
            param1Parcel2.writeNoException();
            if (bundle1 != null) {
              param1Parcel2.writeInt(1);
              bundle1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            bundle1.enforceInterface("android.media.session.ISessionController");
            str1 = getTag();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("android.media.session.ISessionController");
            str1 = getPackageName();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 4:
            str1.enforceInterface("android.media.session.ISessionController");
            iSessionControllerCallback = ISessionControllerCallback.Stub.asInterface(str1.readStrongBinder());
            unregisterCallback(iSessionControllerCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iSessionControllerCallback.enforceInterface("android.media.session.ISessionController");
            str3 = iSessionControllerCallback.readString();
            iSessionControllerCallback = ISessionControllerCallback.Stub.asInterface(iSessionControllerCallback.readStrongBinder());
            registerCallback(str3, iSessionControllerCallback);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iSessionControllerCallback.enforceInterface("android.media.session.ISessionController");
            str3 = iSessionControllerCallback.readString();
            if (iSessionControllerCallback.readInt() != 0) {
              KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel((Parcel)iSessionControllerCallback);
            } else {
              iSessionControllerCallback = null;
            } 
            bool = sendMediaButton(str3, (KeyEvent)iSessionControllerCallback);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 1:
            break;
        } 
        iSessionControllerCallback.enforceInterface("android.media.session.ISessionController");
        String str4 = iSessionControllerCallback.readString();
        String str5 = iSessionControllerCallback.readString();
        if (iSessionControllerCallback.readInt() != 0) {
          Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iSessionControllerCallback);
        } else {
          str3 = null;
        } 
        if (iSessionControllerCallback.readInt() != 0) {
          ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel((Parcel)iSessionControllerCallback);
        } else {
          iSessionControllerCallback = null;
        } 
        sendCommand(str4, str5, (Bundle)str3, (ResultReceiver)iSessionControllerCallback);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.media.session.ISessionController");
      return true;
    }
    
    private static class Proxy implements ISessionController {
      public static ISessionController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISessionController";
      }
      
      public void sendCommand(String param2String1, String param2String2, Bundle param2Bundle, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ResultReceiver != null) {
            parcel1.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().sendCommand(param2String1, param2String2, param2Bundle, param2ResultReceiver);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean sendMediaButton(String param2String, KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2KeyEvent != null) {
            parcel1.writeInt(1);
            param2KeyEvent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool2 && ISessionController.Stub.getDefaultImpl() != null) {
            bool1 = ISessionController.Stub.getDefaultImpl().sendMediaButton(param2String, param2KeyEvent);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(String param2String, ISessionControllerCallback param2ISessionControllerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          if (param2ISessionControllerCallback != null) {
            iBinder = param2ISessionControllerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().registerCallback(param2String, param2ISessionControllerCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallback(ISessionControllerCallback param2ISessionControllerCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          if (param2ISessionControllerCallback != null) {
            iBinder = param2ISessionControllerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().unregisterCallback(param2ISessionControllerCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getPackageName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null)
            return ISessionController.Stub.getDefaultImpl().getPackageName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getTag() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null)
            return ISessionController.Stub.getDefaultImpl().getTag(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getSessionInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            bundle = ISessionController.Stub.getDefaultImpl().getSessionInfo();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PendingIntent getLaunchPendingIntent() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PendingIntent pendingIntent;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            pendingIntent = ISessionController.Stub.getDefaultImpl().getLaunchPendingIntent();
            return pendingIntent;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            pendingIntent = PendingIntent.CREATOR.createFromParcel(parcel2);
          } else {
            pendingIntent = null;
          } 
          return pendingIntent;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getFlags() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null)
            return ISessionController.Stub.getDefaultImpl().getFlags(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MediaController.PlaybackInfo getVolumeAttributes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          MediaController.PlaybackInfo playbackInfo;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            playbackInfo = ISessionController.Stub.getDefaultImpl().getVolumeAttributes();
            return playbackInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            playbackInfo = MediaController.PlaybackInfo.CREATOR.createFromParcel(parcel2);
          } else {
            playbackInfo = null;
          } 
          return playbackInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void adjustVolume(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().adjustVolume(param2String1, param2String2, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolumeTo(String param2String1, String param2String2, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().setVolumeTo(param2String1, param2String2, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepare(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().prepare(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareFromMediaId(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().prepareFromMediaId(param2String1, param2String2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareFromSearch(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().prepareFromSearch(param2String1, param2String2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void prepareFromUri(String param2String, Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().prepareFromUri(param2String, param2Uri, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void play(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().play(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playFromMediaId(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().playFromMediaId(param2String1, param2String2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playFromSearch(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().playFromSearch(param2String1, param2String2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void playFromUri(String param2String, Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().playFromUri(param2String, param2Uri, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void skipToQueueItem(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().skipToQueueItem(param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void pause(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().pause(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stop(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().stop(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void next(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().next(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void previous(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().previous(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void fastForward(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().fastForward(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rewind(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().rewind(param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void seekTo(String param2String, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().seekTo(param2String, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void rate(String param2String, Rating param2Rating) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          if (param2Rating != null) {
            parcel1.writeInt(1);
            param2Rating.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().rate(param2String, param2Rating);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPlaybackSpeed(String param2String, float param2Float) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String);
          parcel1.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().setPlaybackSpeed(param2String, param2Float);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendCustomAction(String param2String1, String param2String2, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            ISessionController.Stub.getDefaultImpl().sendCustomAction(param2String1, param2String2, param2Bundle);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MediaMetadata getMetadata() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          MediaMetadata mediaMetadata;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            mediaMetadata = ISessionController.Stub.getDefaultImpl().getMetadata();
            return mediaMetadata;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            mediaMetadata = MediaMetadata.CREATOR.createFromParcel(parcel2);
          } else {
            mediaMetadata = null;
          } 
          return mediaMetadata;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public PlaybackState getPlaybackState() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          PlaybackState playbackState;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            playbackState = ISessionController.Stub.getDefaultImpl().getPlaybackState();
            return playbackState;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            playbackState = PlaybackState.CREATOR.createFromParcel(parcel2);
          } else {
            playbackState = null;
          } 
          return playbackState;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParceledListSlice getQueue() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          ParceledListSlice parceledListSlice;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            parceledListSlice = ISessionController.Stub.getDefaultImpl().getQueue();
            return parceledListSlice;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            parceledListSlice = ParceledListSlice.CREATOR.createFromParcel(parcel2);
          } else {
            parceledListSlice = null;
          } 
          return parceledListSlice;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public CharSequence getQueueTitle() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          CharSequence charSequence;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            charSequence = ISessionController.Stub.getDefaultImpl().getQueueTitle();
            return charSequence;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            charSequence = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(parcel2);
          } else {
            charSequence = null;
          } 
          return charSequence;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public Bundle getExtras() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          Bundle bundle;
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null) {
            bundle = ISessionController.Stub.getDefaultImpl().getExtras();
            return bundle;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            bundle = Bundle.CREATOR.createFromParcel(parcel2);
          } else {
            bundle = null;
          } 
          return bundle;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getRatingType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.session.ISessionController");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ISessionController.Stub.getDefaultImpl() != null)
            return ISessionController.Stub.getDefaultImpl().getRatingType(); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISessionController param1ISessionController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISessionController != null) {
          Proxy.sDefaultImpl = param1ISessionController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISessionController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
