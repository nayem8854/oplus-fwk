package android.media.session;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.view.KeyEvent;

public interface IOnMediaKeyListener extends IInterface {
  void onMediaKey(KeyEvent paramKeyEvent, ResultReceiver paramResultReceiver) throws RemoteException;
  
  class Default implements IOnMediaKeyListener {
    public void onMediaKey(KeyEvent param1KeyEvent, ResultReceiver param1ResultReceiver) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnMediaKeyListener {
    private static final String DESCRIPTOR = "android.media.session.IOnMediaKeyListener";
    
    static final int TRANSACTION_onMediaKey = 1;
    
    public Stub() {
      attachInterface(this, "android.media.session.IOnMediaKeyListener");
    }
    
    public static IOnMediaKeyListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.IOnMediaKeyListener");
      if (iInterface != null && iInterface instanceof IOnMediaKeyListener)
        return (IOnMediaKeyListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onMediaKey";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.session.IOnMediaKeyListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.session.IOnMediaKeyListener");
      if (param1Parcel1.readInt() != 0) {
        KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel2 = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        ResultReceiver resultReceiver = ResultReceiver.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onMediaKey((KeyEvent)param1Parcel2, (ResultReceiver)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOnMediaKeyListener {
      public static IOnMediaKeyListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.IOnMediaKeyListener";
      }
      
      public void onMediaKey(KeyEvent param2KeyEvent, ResultReceiver param2ResultReceiver) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.IOnMediaKeyListener");
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ResultReceiver != null) {
            parcel.writeInt(1);
            param2ResultReceiver.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOnMediaKeyListener.Stub.getDefaultImpl() != null) {
            IOnMediaKeyListener.Stub.getDefaultImpl().onMediaKey(param2KeyEvent, param2ResultReceiver);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnMediaKeyListener param1IOnMediaKeyListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnMediaKeyListener != null) {
          Proxy.sDefaultImpl = param1IOnMediaKeyListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnMediaKeyListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
