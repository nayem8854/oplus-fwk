package android.media.session;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.Context;
import android.content.pm.ParceledListSlice;
import android.media.IRemoteVolumeController;
import android.media.Session2Token;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.os.ServiceManager;
import android.os.UserHandle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;

public final class MediaSessionManager {
  private final OnMediaKeyEventDispatchedListenerStub mOnMediaKeyEventDispatchedListenerStub = new OnMediaKeyEventDispatchedListenerStub();
  
  private final OnMediaKeyEventSessionChangedListenerStub mOnMediaKeyEventSessionChangedListenerStub = new OnMediaKeyEventSessionChangedListenerStub();
  
  private final Object mLock = new Object();
  
  private final ArrayMap<OnActiveSessionsChangedListener, SessionsChangedWrapper> mListeners = new ArrayMap();
  
  private final ArrayMap<OnSession2TokensChangedListener, Session2TokensChangedWrapper> mSession2TokensListeners = new ArrayMap();
  
  private final Map<OnMediaKeyEventDispatchedListener, Executor> mOnMediaKeyEventDispatchedListeners = new HashMap<>();
  
  private final Map<OnMediaKeyEventSessionChangedListener, Executor> mMediaKeyEventSessionChangedCallbacks = new HashMap<>();
  
  public static final int RESULT_MEDIA_KEY_HANDLED = 1;
  
  public static final int RESULT_MEDIA_KEY_NOT_HANDLED = 0;
  
  private static final String TAG = "SessionManager";
  
  private Context mContext;
  
  private MediaSession.Token mCurMediaKeyEventSession;
  
  private String mCurMediaKeyEventSessionPackage;
  
  private OnMediaKeyListenerImpl mOnMediaKeyListener;
  
  private OnVolumeKeyLongPressListenerImpl mOnVolumeKeyLongPressListener;
  
  private final ISessionManager mService;
  
  public MediaSessionManager(Context paramContext) {
    this.mContext = paramContext;
    IBinder iBinder = ServiceManager.getService("media_session");
    this.mService = ISessionManager.Stub.asInterface(iBinder);
  }
  
  public ISession createSession(MediaSession.CallbackStub paramCallbackStub, String paramString, Bundle paramBundle) {
    try {
      ISessionManager iSessionManager = this.mService;
      String str = this.mContext.getPackageName();
      int i = UserHandle.myUserId();
      return iSessionManager.createSession(str, paramCallbackStub, paramString, paramBundle, i);
    } catch (RemoteException remoteException) {
      throw new RuntimeException(remoteException);
    } 
  }
  
  public void notifySession2Created(Session2Token paramSession2Token) {
    if (paramSession2Token != null) {
      if (paramSession2Token.getType() == 0) {
        try {
          this.mService.notifySession2Created(paramSession2Token);
        } catch (RemoteException remoteException) {
          remoteException.rethrowFromSystemServer();
        } 
        return;
      } 
      throw new IllegalArgumentException("token's type should be TYPE_SESSION");
    } 
    throw new IllegalArgumentException("token shouldn't be null");
  }
  
  public List<MediaController> getActiveSessions(ComponentName paramComponentName) {
    return getActiveSessionsForUser(paramComponentName, UserHandle.myUserId());
  }
  
  public List<MediaController> getActiveSessionsForUser(ComponentName paramComponentName, int paramInt) {
    ArrayList<MediaController> arrayList = new ArrayList();
    try {
      List<MediaSession.Token> list = this.mService.getSessions(paramComponentName, paramInt);
      int i = list.size();
      for (paramInt = 0; paramInt < i; paramInt++) {
        MediaController mediaController = new MediaController();
        this(this.mContext, list.get(paramInt));
        arrayList.add(mediaController);
      } 
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to get active sessions: ", (Throwable)remoteException);
    } 
    return arrayList;
  }
  
  public List<Session2Token> getSession2Tokens() {
    return getSession2Tokens(UserHandle.myUserId());
  }
  
  public List<Session2Token> getSession2Tokens(int paramInt) {
    try {
      List<Session2Token> list;
      ParceledListSlice parceledListSlice = this.mService.getSession2Tokens(paramInt);
      if (parceledListSlice == null) {
        list = new ArrayList();
      } else {
        list = list.getList();
      } 
      return list;
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to get session tokens", (Throwable)remoteException);
      return new ArrayList<>();
    } 
  }
  
  public void addOnActiveSessionsChangedListener(OnActiveSessionsChangedListener paramOnActiveSessionsChangedListener, ComponentName paramComponentName) {
    addOnActiveSessionsChangedListener(paramOnActiveSessionsChangedListener, paramComponentName, null);
  }
  
  public void addOnActiveSessionsChangedListener(OnActiveSessionsChangedListener paramOnActiveSessionsChangedListener, ComponentName paramComponentName, Handler paramHandler) {
    int i = UserHandle.myUserId();
    addOnActiveSessionsChangedListener(paramOnActiveSessionsChangedListener, paramComponentName, i, paramHandler);
  }
  
  public void addOnActiveSessionsChangedListener(OnActiveSessionsChangedListener paramOnActiveSessionsChangedListener, ComponentName paramComponentName, int paramInt, Handler paramHandler) {
    if (paramOnActiveSessionsChangedListener != null) {
      if (paramHandler == null)
        paramHandler = new Handler(); 
      synchronized (this.mLock) {
        if (this.mListeners.get(paramOnActiveSessionsChangedListener) != null) {
          Log.w("SessionManager", "Attempted to add session listener twice, ignoring.");
          return;
        } 
        SessionsChangedWrapper sessionsChangedWrapper = new SessionsChangedWrapper();
        this(this.mContext, paramOnActiveSessionsChangedListener, paramHandler);
        try {
          this.mService.addSessionsListener(sessionsChangedWrapper.mStub, paramComponentName, paramInt);
          this.mListeners.put(paramOnActiveSessionsChangedListener, sessionsChangedWrapper);
        } catch (RemoteException remoteException) {
          Log.e("SessionManager", "Error in addOnActiveSessionsChangedListener.", (Throwable)remoteException);
        } 
        return;
      } 
    } 
    throw new IllegalArgumentException("listener may not be null");
  }
  
  public void removeOnActiveSessionsChangedListener(OnActiveSessionsChangedListener paramOnActiveSessionsChangedListener) {
    if (paramOnActiveSessionsChangedListener != null)
      synchronized (this.mLock) {
        SessionsChangedWrapper sessionsChangedWrapper = (SessionsChangedWrapper)this.mListeners.remove(paramOnActiveSessionsChangedListener);
        if (sessionsChangedWrapper != null)
          try {
            this.mService.removeSessionsListener(sessionsChangedWrapper.mStub);
            sessionsChangedWrapper.release();
          } catch (RemoteException remoteException) {
            Log.e("SessionManager", "Error in removeOnActiveSessionsChangedListener.", (Throwable)remoteException);
            sessionsChangedWrapper.release();
          } finally {
            Exception exception;
          }  
        return;
      }  
    throw new IllegalArgumentException("listener may not be null");
  }
  
  public void addOnSession2TokensChangedListener(OnSession2TokensChangedListener paramOnSession2TokensChangedListener) {
    addOnSession2TokensChangedListener(UserHandle.myUserId(), paramOnSession2TokensChangedListener, new Handler());
  }
  
  public void addOnSession2TokensChangedListener(OnSession2TokensChangedListener paramOnSession2TokensChangedListener, Handler paramHandler) {
    addOnSession2TokensChangedListener(UserHandle.myUserId(), paramOnSession2TokensChangedListener, paramHandler);
  }
  
  public void addOnSession2TokensChangedListener(int paramInt, OnSession2TokensChangedListener paramOnSession2TokensChangedListener, Handler paramHandler) {
    if (paramOnSession2TokensChangedListener != null)
      synchronized (this.mLock) {
        if (this.mSession2TokensListeners.get(paramOnSession2TokensChangedListener) != null) {
          Log.w("SessionManager", "Attempted to add session listener twice, ignoring.");
          return;
        } 
        Session2TokensChangedWrapper session2TokensChangedWrapper = new Session2TokensChangedWrapper();
        this(paramOnSession2TokensChangedListener, paramHandler);
        try {
          this.mService.addSession2TokensListener(session2TokensChangedWrapper.getStub(), paramInt);
          this.mSession2TokensListeners.put(paramOnSession2TokensChangedListener, session2TokensChangedWrapper);
        } catch (RemoteException remoteException) {
          Log.e("SessionManager", "Error in addSessionTokensListener.", (Throwable)remoteException);
          remoteException.rethrowFromSystemServer();
        } 
        return;
      }  
    throw new IllegalArgumentException("listener shouldn't be null");
  }
  
  public void removeOnSession2TokensChangedListener(OnSession2TokensChangedListener paramOnSession2TokensChangedListener) {
    if (paramOnSession2TokensChangedListener != null)
      synchronized (this.mLock) {
        Session2TokensChangedWrapper session2TokensChangedWrapper = (Session2TokensChangedWrapper)this.mSession2TokensListeners.remove(paramOnSession2TokensChangedListener);
        if (session2TokensChangedWrapper != null)
          try {
            this.mService.removeSession2TokensListener(session2TokensChangedWrapper.getStub());
          } catch (RemoteException remoteException) {
            Log.e("SessionManager", "Error in removeSessionTokensListener.", (Throwable)remoteException);
            remoteException.rethrowFromSystemServer();
          }  
        return;
      }  
    throw new IllegalArgumentException("listener may not be null");
  }
  
  public void registerRemoteVolumeController(IRemoteVolumeController paramIRemoteVolumeController) {
    try {
      this.mService.registerRemoteVolumeController(paramIRemoteVolumeController);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Error in registerRemoteVolumeController.", (Throwable)remoteException);
    } 
  }
  
  public void unregisterRemoteVolumeController(IRemoteVolumeController paramIRemoteVolumeController) {
    try {
      this.mService.unregisterRemoteVolumeController(paramIRemoteVolumeController);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Error in unregisterRemoteVolumeController.", (Throwable)remoteException);
    } 
  }
  
  public void dispatchMediaKeyEvent(KeyEvent paramKeyEvent) {
    dispatchMediaKeyEvent(paramKeyEvent, false);
  }
  
  public void dispatchMediaKeyEvent(KeyEvent paramKeyEvent, boolean paramBoolean) {
    dispatchMediaKeyEventInternal(false, paramKeyEvent, paramBoolean);
  }
  
  public void dispatchMediaKeyEventAsSystemService(KeyEvent paramKeyEvent) {
    dispatchMediaKeyEventInternal(true, paramKeyEvent, false);
  }
  
  private void dispatchMediaKeyEventInternal(boolean paramBoolean1, KeyEvent paramKeyEvent, boolean paramBoolean2) {
    try {
      this.mService.dispatchMediaKeyEvent(this.mContext.getPackageName(), paramBoolean1, paramKeyEvent, paramBoolean2);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to send key event.", (Throwable)remoteException);
    } 
  }
  
  public boolean dispatchMediaKeyEventAsSystemService(MediaSession.Token paramToken, KeyEvent paramKeyEvent) {
    if (paramToken != null) {
      if (paramKeyEvent != null) {
        if (!KeyEvent.isMediaSessionKey(paramKeyEvent.getKeyCode()))
          return false; 
        try {
          return this.mService.dispatchMediaKeyEventToSessionAsSystemService(this.mContext.getPackageName(), paramToken, paramKeyEvent);
        } catch (RemoteException remoteException) {
          Log.e("SessionManager", "Failed to send key event.", (Throwable)remoteException);
          return false;
        } 
      } 
      throw new IllegalArgumentException("keyEvent shouldn't be null");
    } 
    throw new IllegalArgumentException("sessionToken shouldn't be null");
  }
  
  public void dispatchVolumeKeyEvent(KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean) {
    dispatchVolumeKeyEventInternal(false, paramKeyEvent, paramInt, paramBoolean);
  }
  
  public void dispatchVolumeKeyEventAsSystemService(KeyEvent paramKeyEvent, int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaSessionManager dispatchVolumeKeyEventAsSystemService streamType=");
    stringBuilder.append(paramInt);
    Log.d("SessionManager", stringBuilder.toString());
    dispatchVolumeKeyEventInternal(true, paramKeyEvent, paramInt, false);
  }
  
  private void dispatchVolumeKeyEventInternal(boolean paramBoolean1, KeyEvent paramKeyEvent, int paramInt, boolean paramBoolean2) {
    try {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("MediaSessionManager dispatchVolumeKeyEventInternal asSystemService=");
      stringBuilder.append(paramBoolean1);
      stringBuilder.append(" stream=");
      stringBuilder.append(paramInt);
      stringBuilder.append(" musicOnly=");
      stringBuilder.append(paramBoolean2);
      Log.d("SessionManager", stringBuilder.toString());
      this.mService.dispatchVolumeKeyEvent(this.mContext.getPackageName(), this.mContext.getOpPackageName(), paramBoolean1, paramKeyEvent, paramInt, paramBoolean2);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to send volume key event.", (Throwable)remoteException);
    } 
  }
  
  public void dispatchVolumeKeyEventAsSystemService(MediaSession.Token paramToken, KeyEvent paramKeyEvent) {
    if (paramToken != null) {
      if (paramKeyEvent != null) {
        try {
          ISessionManager iSessionManager = this.mService;
          String str1 = this.mContext.getPackageName();
          Context context = this.mContext;
          String str2 = context.getOpPackageName();
          iSessionManager.dispatchVolumeKeyEventToSessionAsSystemService(str1, str2, paramToken, paramKeyEvent);
        } catch (RemoteException remoteException) {
          Log.wtf("SessionManager", "Error calling dispatchVolumeKeyEventAsSystemService", (Throwable)remoteException);
        } 
        return;
      } 
      throw new IllegalArgumentException("keyEvent shouldn't be null");
    } 
    throw new IllegalArgumentException("sessionToken shouldn't be null");
  }
  
  public void dispatchAdjustVolume(int paramInt1, int paramInt2, int paramInt3) {
    try {
      this.mService.dispatchAdjustVolume(this.mContext.getPackageName(), this.mContext.getOpPackageName(), paramInt1, paramInt2, paramInt3);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to send adjust volume.", (Throwable)remoteException);
    } 
  }
  
  public boolean isTrustedForMediaControl(RemoteUserInfo paramRemoteUserInfo) {
    if (paramRemoteUserInfo != null) {
      if (paramRemoteUserInfo.getPackageName() == null)
        return false; 
      try {
        ISessionManager iSessionManager = this.mService;
        String str = paramRemoteUserInfo.getPackageName();
        int i = paramRemoteUserInfo.getPid(), j = paramRemoteUserInfo.getUid();
        return iSessionManager.isTrusted(str, i, j);
      } catch (RemoteException remoteException) {
        Log.wtf("SessionManager", "Cannot communicate with the service.", (Throwable)remoteException);
        return false;
      } 
    } 
    throw new IllegalArgumentException("userInfo may not be null");
  }
  
  public boolean isGlobalPriorityActive() {
    try {
      return this.mService.isGlobalPriorityActive();
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to check if the global priority is active.", (Throwable)remoteException);
      return false;
    } 
  }
  
  @SystemApi
  public void setOnVolumeKeyLongPressListener(OnVolumeKeyLongPressListener paramOnVolumeKeyLongPressListener, Handler paramHandler) {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    if (paramOnVolumeKeyLongPressListener == null) {
      try {
        this.mOnVolumeKeyLongPressListener = null;
        this.mService.setOnVolumeKeyLongPressListener(null);
      } catch (RemoteException remoteException) {
        Log.e("SessionManager", "Failed to set volume key long press listener", (Throwable)remoteException);
      } finally {}
    } else {
      Handler handler = paramHandler;
      if (paramHandler == null) {
        handler = new Handler();
        this();
      } 
      OnVolumeKeyLongPressListenerImpl onVolumeKeyLongPressListenerImpl = new OnVolumeKeyLongPressListenerImpl();
      this(paramOnVolumeKeyLongPressListener, handler);
      this.mOnVolumeKeyLongPressListener = onVolumeKeyLongPressListenerImpl;
      this.mService.setOnVolumeKeyLongPressListener(onVolumeKeyLongPressListenerImpl);
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  @SystemApi
  public void setOnMediaKeyListener(OnMediaKeyListener paramOnMediaKeyListener, Handler paramHandler) {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    if (paramOnMediaKeyListener == null) {
      try {
        this.mOnMediaKeyListener = null;
        this.mService.setOnMediaKeyListener(null);
      } catch (RemoteException remoteException) {
        Log.e("SessionManager", "Failed to set media key listener", (Throwable)remoteException);
      } finally {}
    } else {
      Handler handler = paramHandler;
      if (paramHandler == null) {
        handler = new Handler();
        this();
      } 
      OnMediaKeyListenerImpl onMediaKeyListenerImpl = new OnMediaKeyListenerImpl();
      this(paramOnMediaKeyListener, handler);
      this.mOnMediaKeyListener = onMediaKeyListenerImpl;
      this.mService.setOnMediaKeyListener(onMediaKeyListenerImpl);
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  @SystemApi
  public void addOnMediaKeyEventDispatchedListener(Executor paramExecutor, OnMediaKeyEventDispatchedListener paramOnMediaKeyEventDispatchedListener) {
    if (paramExecutor != null) {
      if (paramOnMediaKeyEventDispatchedListener != null) {
        Object object = this.mLock;
        /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        try {
          this.mOnMediaKeyEventDispatchedListeners.put(paramOnMediaKeyEventDispatchedListener, paramExecutor);
          if (this.mOnMediaKeyEventDispatchedListeners.size() == 1)
            this.mService.addOnMediaKeyEventDispatchedListener(this.mOnMediaKeyEventDispatchedListenerStub); 
        } catch (RemoteException remoteException) {
          Log.e("SessionManager", "Failed to set media key listener", (Throwable)remoteException);
        } finally {}
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return;
      } 
      throw new NullPointerException("listener shouldn't be null");
    } 
    throw new NullPointerException("executor shouldn't be null");
  }
  
  @SystemApi
  public void removeOnMediaKeyEventDispatchedListener(OnMediaKeyEventDispatchedListener paramOnMediaKeyEventDispatchedListener) {
    if (paramOnMediaKeyEventDispatchedListener != null) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        this.mOnMediaKeyEventDispatchedListeners.remove(paramOnMediaKeyEventDispatchedListener);
        if (this.mOnMediaKeyEventDispatchedListeners.size() == 0)
          this.mService.removeOnMediaKeyEventDispatchedListener(this.mOnMediaKeyEventDispatchedListenerStub); 
      } catch (RemoteException remoteException) {
        Log.e("SessionManager", "Failed to set media key event dispatched listener", (Throwable)remoteException);
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return;
    } 
    throw new NullPointerException("listener shouldn't be null");
  }
  
  @SystemApi
  public void addOnMediaKeyEventSessionChangedListener(Executor paramExecutor, OnMediaKeyEventSessionChangedListener paramOnMediaKeyEventSessionChangedListener) {
    if (paramExecutor != null) {
      if (paramOnMediaKeyEventSessionChangedListener != null) {
        Object object = this.mLock;
        /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        try {
          this.mMediaKeyEventSessionChangedCallbacks.put(paramOnMediaKeyEventSessionChangedListener, paramExecutor);
          _$$Lambda$MediaSessionManager$IEuWPZ528guBgmyKPMUWhBwnMCE _$$Lambda$MediaSessionManager$IEuWPZ528guBgmyKPMUWhBwnMCE = new _$$Lambda$MediaSessionManager$IEuWPZ528guBgmyKPMUWhBwnMCE();
          this(this, paramOnMediaKeyEventSessionChangedListener);
          paramExecutor.execute(_$$Lambda$MediaSessionManager$IEuWPZ528guBgmyKPMUWhBwnMCE);
          if (this.mMediaKeyEventSessionChangedCallbacks.size() == 1)
            this.mService.addOnMediaKeyEventSessionChangedListener(this.mOnMediaKeyEventSessionChangedListenerStub); 
        } catch (RemoteException remoteException) {
          Log.e("SessionManager", "Failed to set media key listener", (Throwable)remoteException);
        } finally {}
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return;
      } 
      throw new NullPointerException("listener shouldn't be null");
    } 
    throw new NullPointerException("executor shouldn't be null");
  }
  
  @SystemApi
  public void removeOnMediaKeyEventSessionChangedListener(OnMediaKeyEventSessionChangedListener paramOnMediaKeyEventSessionChangedListener) {
    if (paramOnMediaKeyEventSessionChangedListener != null) {
      Object object = this.mLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      try {
        this.mMediaKeyEventSessionChangedCallbacks.remove(paramOnMediaKeyEventSessionChangedListener);
        if (this.mMediaKeyEventSessionChangedCallbacks.size() == 0)
          this.mService.removeOnMediaKeyEventSessionChangedListener(this.mOnMediaKeyEventSessionChangedListenerStub); 
      } catch (RemoteException remoteException) {
        Log.e("SessionManager", "Failed to set media key listener", (Throwable)remoteException);
      } finally {}
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return;
    } 
    throw new NullPointerException("listener shouldn't be null");
  }
  
  public void setCustomMediaKeyDispatcherForTesting(String paramString) {
    try {
      this.mService.setCustomMediaKeyDispatcherForTesting(paramString);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to set custom media key dispatcher name", (Throwable)remoteException);
    } 
  }
  
  public void setCustomSessionPolicyProviderForTesting(String paramString) {
    try {
      this.mService.setCustomSessionPolicyProviderForTesting(paramString);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to set custom session policy provider name", (Throwable)remoteException);
    } 
  }
  
  public int getSessionPolicies(MediaSession.Token paramToken) {
    try {
      return this.mService.getSessionPolicies(paramToken);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to get session policies", (Throwable)remoteException);
      return 0;
    } 
  }
  
  public void setSessionPolicies(MediaSession.Token paramToken, int paramInt) {
    try {
      this.mService.setSessionPolicies(paramToken, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("SessionManager", "Failed to set session policies", (Throwable)remoteException);
    } 
  }
  
  public static final class RemoteUserInfo {
    private final String mPackageName;
    
    private final int mPid;
    
    private final int mUid;
    
    public RemoteUserInfo(String param1String, int param1Int1, int param1Int2) {
      this.mPackageName = param1String;
      this.mPid = param1Int1;
      this.mUid = param1Int2;
    }
    
    public String getPackageName() {
      return this.mPackageName;
    }
    
    public int getPid() {
      return this.mPid;
    }
    
    public int getUid() {
      return this.mUid;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof RemoteUserInfo;
      boolean bool1 = false;
      if (!bool)
        return false; 
      if (this == param1Object)
        return true; 
      param1Object = param1Object;
      bool = bool1;
      if (TextUtils.equals(this.mPackageName, ((RemoteUserInfo)param1Object).mPackageName)) {
        bool = bool1;
        if (this.mPid == ((RemoteUserInfo)param1Object).mPid) {
          bool = bool1;
          if (this.mUid == ((RemoteUserInfo)param1Object).mUid)
            bool = true; 
        } 
      } 
      return bool;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mPackageName, Integer.valueOf(this.mPid), Integer.valueOf(this.mUid) });
    }
  }
  
  private static final class SessionsChangedWrapper {
    private Context mContext;
    
    private Handler mHandler;
    
    private MediaSessionManager.OnActiveSessionsChangedListener mListener;
    
    private final IActiveSessionsListener.Stub mStub;
    
    public SessionsChangedWrapper(Context param1Context, MediaSessionManager.OnActiveSessionsChangedListener param1OnActiveSessionsChangedListener, Handler param1Handler) {
      this.mStub = (IActiveSessionsListener.Stub)new Object(this);
      this.mContext = param1Context;
      this.mListener = param1OnActiveSessionsChangedListener;
      this.mHandler = param1Handler;
    }
    
    private void release() {
      this.mListener = null;
      this.mContext = null;
      this.mHandler = null;
    }
  }
  
  private static final class Session2TokensChangedWrapper {
    private final Handler mHandler;
    
    private final MediaSessionManager.OnSession2TokensChangedListener mListener;
    
    private final ISession2TokensListener.Stub mStub = (ISession2TokensListener.Stub)new Object(this);
    
    Session2TokensChangedWrapper(MediaSessionManager.OnSession2TokensChangedListener param1OnSession2TokensChangedListener, Handler param1Handler) {
      this.mListener = param1OnSession2TokensChangedListener;
      Handler handler = new Handler();
      if (param1Handler == null) {
        this();
      } else {
        this(param1Handler.getLooper());
      } 
      this.mHandler = handler;
    }
    
    public ISession2TokensListener.Stub getStub() {
      return this.mStub;
    }
  }
  
  class OnVolumeKeyLongPressListenerImpl extends IOnVolumeKeyLongPressListener.Stub {
    private Handler mHandler;
    
    private MediaSessionManager.OnVolumeKeyLongPressListener mListener;
    
    public OnVolumeKeyLongPressListenerImpl(MediaSessionManager this$0, Handler param1Handler) {
      this.mListener = (MediaSessionManager.OnVolumeKeyLongPressListener)this$0;
      this.mHandler = param1Handler;
    }
    
    public void onVolumeKeyLongPress(final KeyEvent event) {
      if (this.mListener != null) {
        Handler handler = this.mHandler;
        if (handler != null) {
          handler.post(new Runnable() {
                final MediaSessionManager.OnVolumeKeyLongPressListenerImpl this$0;
                
                final KeyEvent val$event;
                
                public void run() {
                  this.this$0.mListener.onVolumeKeyLongPress(event);
                }
              });
          return;
        } 
      } 
      Log.w("SessionManager", "Failed to call volume key long-press listener. Either mListener or mHandler is null");
    }
  }
  
  class null implements Runnable {
    final MediaSessionManager.OnVolumeKeyLongPressListenerImpl this$0;
    
    final KeyEvent val$event;
    
    public void run() {
      this.this$0.mListener.onVolumeKeyLongPress(event);
    }
  }
  
  class OnMediaKeyListenerImpl extends IOnMediaKeyListener.Stub {
    private Handler mHandler;
    
    private MediaSessionManager.OnMediaKeyListener mListener;
    
    public OnMediaKeyListenerImpl(MediaSessionManager this$0, Handler param1Handler) {
      this.mListener = (MediaSessionManager.OnMediaKeyListener)this$0;
      this.mHandler = param1Handler;
    }
    
    public void onMediaKey(final KeyEvent event, final ResultReceiver result) {
      if (this.mListener != null) {
        Handler handler = this.mHandler;
        if (handler != null) {
          handler.post(new Runnable() {
                final MediaSessionManager.OnMediaKeyListenerImpl this$0;
                
                final KeyEvent val$event;
                
                final ResultReceiver val$result;
                
                public void run() {
                  boolean bool = this.this$0.mListener.onMediaKey(event);
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("The media key listener is returned ");
                  stringBuilder.append(bool);
                  Log.d("SessionManager", stringBuilder.toString());
                  ResultReceiver resultReceiver = result;
                  if (resultReceiver != null)
                    resultReceiver.send(bool, null); 
                }
              });
          return;
        } 
      } 
      Log.w("SessionManager", "Failed to call media key listener. Either mListener or mHandler is null");
    }
  }
  
  class null implements Runnable {
    final MediaSessionManager.OnMediaKeyListenerImpl this$0;
    
    final KeyEvent val$event;
    
    final ResultReceiver val$result;
    
    public void run() {
      boolean bool = this.this$0.mListener.onMediaKey(event);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The media key listener is returned ");
      stringBuilder.append(bool);
      Log.d("SessionManager", stringBuilder.toString());
      ResultReceiver resultReceiver = result;
      if (resultReceiver != null)
        resultReceiver.send(bool, null); 
    }
  }
  
  class OnMediaKeyEventDispatchedListenerStub extends IOnMediaKeyEventDispatchedListener.Stub {
    final MediaSessionManager this$0;
    
    private OnMediaKeyEventDispatchedListenerStub() {}
    
    public void onMediaKeyEventDispatched(KeyEvent param1KeyEvent, String param1String, MediaSession.Token param1Token) {
      synchronized (MediaSessionManager.this.mLock) {
        for (Map.Entry entry : MediaSessionManager.this.mOnMediaKeyEventDispatchedListeners.entrySet()) {
          Executor executor = (Executor)entry.getValue();
          _$$Lambda$MediaSessionManager$OnMediaKeyEventDispatchedListenerStub$IAaO7JPqesHWh_9_dsZuk_lEsao _$$Lambda$MediaSessionManager$OnMediaKeyEventDispatchedListenerStub$IAaO7JPqesHWh_9_dsZuk_lEsao = new _$$Lambda$MediaSessionManager$OnMediaKeyEventDispatchedListenerStub$IAaO7JPqesHWh_9_dsZuk_lEsao();
          this(entry, param1KeyEvent, param1String, param1Token);
          executor.execute(_$$Lambda$MediaSessionManager$OnMediaKeyEventDispatchedListenerStub$IAaO7JPqesHWh_9_dsZuk_lEsao);
        } 
        return;
      } 
    }
  }
  
  class OnMediaKeyEventSessionChangedListenerStub extends IOnMediaKeyEventSessionChangedListener.Stub {
    final MediaSessionManager this$0;
    
    private OnMediaKeyEventSessionChangedListenerStub() {}
    
    public void onMediaKeyEventSessionChanged(String param1String, MediaSession.Token param1Token) {
      synchronized (MediaSessionManager.this.mLock) {
        MediaSessionManager.access$1302(MediaSessionManager.this, param1String);
        MediaSessionManager.access$1402(MediaSessionManager.this, param1Token);
        for (Map.Entry entry : MediaSessionManager.this.mMediaKeyEventSessionChangedCallbacks.entrySet()) {
          Executor executor = (Executor)entry.getValue();
          _$$Lambda$MediaSessionManager$OnMediaKeyEventSessionChangedListenerStub$4CTMsGoAeJ_D2gq0rNq8sthdL_0 _$$Lambda$MediaSessionManager$OnMediaKeyEventSessionChangedListenerStub$4CTMsGoAeJ_D2gq0rNq8sthdL_0 = new _$$Lambda$MediaSessionManager$OnMediaKeyEventSessionChangedListenerStub$4CTMsGoAeJ_D2gq0rNq8sthdL_0();
          this(entry, param1String, param1Token);
          executor.execute(_$$Lambda$MediaSessionManager$OnMediaKeyEventSessionChangedListenerStub$4CTMsGoAeJ_D2gq0rNq8sthdL_0);
        } 
        return;
      } 
    }
  }
  
  public static interface OnActiveSessionsChangedListener {
    void onActiveSessionsChanged(List<MediaController> param1List);
  }
  
  @SystemApi
  public static interface OnMediaKeyEventDispatchedListener {
    void onMediaKeyEventDispatched(KeyEvent param1KeyEvent, String param1String, MediaSession.Token param1Token);
  }
  
  @SystemApi
  public static interface OnMediaKeyEventSessionChangedListener {
    void onMediaKeyEventSessionChanged(String param1String, MediaSession.Token param1Token);
  }
  
  @SystemApi
  public static interface OnMediaKeyListener {
    boolean onMediaKey(KeyEvent param1KeyEvent);
  }
  
  public static interface OnSession2TokensChangedListener {
    void onSession2TokensChanged(List<Session2Token> param1List);
  }
  
  @SystemApi
  public static interface OnVolumeKeyLongPressListener {
    void onVolumeKeyLongPress(KeyEvent param1KeyEvent);
  }
}
