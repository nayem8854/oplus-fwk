package android.media.session;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.KeyEvent;

public interface IOnVolumeKeyLongPressListener extends IInterface {
  void onVolumeKeyLongPress(KeyEvent paramKeyEvent) throws RemoteException;
  
  class Default implements IOnVolumeKeyLongPressListener {
    public void onVolumeKeyLongPress(KeyEvent param1KeyEvent) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IOnVolumeKeyLongPressListener {
    private static final String DESCRIPTOR = "android.media.session.IOnVolumeKeyLongPressListener";
    
    static final int TRANSACTION_onVolumeKeyLongPress = 1;
    
    public Stub() {
      attachInterface(this, "android.media.session.IOnVolumeKeyLongPressListener");
    }
    
    public static IOnVolumeKeyLongPressListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.IOnVolumeKeyLongPressListener");
      if (iInterface != null && iInterface instanceof IOnVolumeKeyLongPressListener)
        return (IOnVolumeKeyLongPressListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onVolumeKeyLongPress";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.session.IOnVolumeKeyLongPressListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.session.IOnVolumeKeyLongPressListener");
      if (param1Parcel1.readInt() != 0) {
        KeyEvent keyEvent = KeyEvent.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onVolumeKeyLongPress((KeyEvent)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IOnVolumeKeyLongPressListener {
      public static IOnVolumeKeyLongPressListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.IOnVolumeKeyLongPressListener";
      }
      
      public void onVolumeKeyLongPress(KeyEvent param2KeyEvent) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.IOnVolumeKeyLongPressListener");
          if (param2KeyEvent != null) {
            parcel.writeInt(1);
            param2KeyEvent.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IOnVolumeKeyLongPressListener.Stub.getDefaultImpl() != null) {
            IOnVolumeKeyLongPressListener.Stub.getDefaultImpl().onVolumeKeyLongPress(param2KeyEvent);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IOnVolumeKeyLongPressListener param1IOnVolumeKeyLongPressListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IOnVolumeKeyLongPressListener != null) {
          Proxy.sDefaultImpl = param1IOnVolumeKeyLongPressListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IOnVolumeKeyLongPressListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
