package android.media.session;

import android.media.Session2Token;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ISession2TokensListener extends IInterface {
  void onSession2TokensChanged(List<Session2Token> paramList) throws RemoteException;
  
  class Default implements ISession2TokensListener {
    public void onSession2TokensChanged(List<Session2Token> param1List) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ISession2TokensListener {
    private static final String DESCRIPTOR = "android.media.session.ISession2TokensListener";
    
    static final int TRANSACTION_onSession2TokensChanged = 1;
    
    public Stub() {
      attachInterface(this, "android.media.session.ISession2TokensListener");
    }
    
    public static ISession2TokensListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.session.ISession2TokensListener");
      if (iInterface != null && iInterface instanceof ISession2TokensListener)
        return (ISession2TokensListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onSession2TokensChanged";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.session.ISession2TokensListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.session.ISession2TokensListener");
      ArrayList<?> arrayList = param1Parcel1.createTypedArrayList(Session2Token.CREATOR);
      onSession2TokensChanged((List)arrayList);
      return true;
    }
    
    private static class Proxy implements ISession2TokensListener {
      public static ISession2TokensListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.session.ISession2TokensListener";
      }
      
      public void onSession2TokensChanged(List<Session2Token> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.session.ISession2TokensListener");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ISession2TokensListener.Stub.getDefaultImpl() != null) {
            ISession2TokensListener.Stub.getDefaultImpl().onSession2TokensChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ISession2TokensListener param1ISession2TokensListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ISession2TokensListener != null) {
          Proxy.sDefaultImpl = param1ISession2TokensListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ISession2TokensListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
