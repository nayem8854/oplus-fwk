package android.media;

import java.util.Arrays;
import java.util.function.Predicate;
import java.util.function.ToIntFunction;
import java.util.stream.Stream;

public class AudioDevicePort extends AudioPort {
  private final String mAddress;
  
  private final int[] mEncapsulationMetadataTypes;
  
  private final int[] mEncapsulationModes;
  
  private final int mType;
  
  AudioDevicePort(AudioHandle paramAudioHandle, String paramString1, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, int[] paramArrayOfint4, AudioGain[] paramArrayOfAudioGain, int paramInt, String paramString2, int[] paramArrayOfint5, int[] paramArrayOfint6) {
    super(paramAudioHandle, b, paramString1, paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramArrayOfint4, paramArrayOfAudioGain);
    byte b;
    this.mType = paramInt;
    this.mAddress = paramString2;
    this.mEncapsulationModes = paramArrayOfint5;
    this.mEncapsulationMetadataTypes = paramArrayOfint6;
  }
  
  public int type() {
    return this.mType;
  }
  
  public String address() {
    return this.mAddress;
  }
  
  public int[] encapsulationModes() {
    int[] arrayOfInt = this.mEncapsulationModes;
    if (arrayOfInt == null)
      return new int[0]; 
    Stream<Integer> stream = Arrays.stream(arrayOfInt).boxed();
    -$.Lambda.AudioDevicePort.seMFZuMR9bUiEzD-eQz1grS86fI seMFZuMR9bUiEzD-eQz1grS86fI = _$$Lambda$AudioDevicePort$seMFZuMR9bUiEzD_eQz1grS86fI.INSTANCE;
    stream = stream.filter((Predicate<? super Integer>)seMFZuMR9bUiEzD-eQz1grS86fI);
    -$.Lambda.UV1wDVoVlbcxpr8zevj_aMFtUGw uV1wDVoVlbcxpr8zevj_aMFtUGw = _$$Lambda$UV1wDVoVlbcxpr8zevj_aMFtUGw.INSTANCE;
    return stream.mapToInt((ToIntFunction<? super Integer>)uV1wDVoVlbcxpr8zevj_aMFtUGw).toArray();
  }
  
  public int[] encapsulationMetadataTypes() {
    int[] arrayOfInt1 = this.mEncapsulationMetadataTypes;
    if (arrayOfInt1 == null)
      return new int[0]; 
    int[] arrayOfInt2 = new int[arrayOfInt1.length];
    System.arraycopy(arrayOfInt1, 0, arrayOfInt2, 0, arrayOfInt1.length);
    return arrayOfInt2;
  }
  
  public AudioDevicePortConfig buildConfig(int paramInt1, int paramInt2, int paramInt3, AudioGainConfig paramAudioGainConfig) {
    return new AudioDevicePortConfig(this, paramInt1, paramInt2, paramInt3, paramAudioGainConfig);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null || !(paramObject instanceof AudioDevicePort))
      return false; 
    AudioDevicePort audioDevicePort = (AudioDevicePort)paramObject;
    if (this.mType != audioDevicePort.type())
      return false; 
    if (this.mAddress == null && audioDevicePort.address() != null)
      return false; 
    if (!this.mAddress.equals(audioDevicePort.address()))
      return false; 
    return super.equals(paramObject);
  }
  
  public String toString() {
    String str;
    if (this.mRole == 1) {
      str = AudioSystem.getInputDeviceName(this.mType);
    } else {
      str = AudioSystem.getOutputDeviceName(this.mType);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append(super.toString());
    stringBuilder.append(", mType: ");
    stringBuilder.append(str);
    stringBuilder.append(", mAddress: ");
    stringBuilder.append(this.mAddress);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
