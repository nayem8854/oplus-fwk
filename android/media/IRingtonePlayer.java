package android.media;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.os.UserHandle;

public interface IRingtonePlayer extends IInterface {
  String getTitle(Uri paramUri) throws RemoteException;
  
  boolean isPlaying(IBinder paramIBinder) throws RemoteException;
  
  ParcelFileDescriptor openRingtone(Uri paramUri) throws RemoteException;
  
  void play(IBinder paramIBinder, Uri paramUri, AudioAttributes paramAudioAttributes, float paramFloat, boolean paramBoolean) throws RemoteException;
  
  void playAsync(Uri paramUri, UserHandle paramUserHandle, boolean paramBoolean, AudioAttributes paramAudioAttributes) throws RemoteException;
  
  void playWithVolumeShaping(IBinder paramIBinder, Uri paramUri, AudioAttributes paramAudioAttributes, float paramFloat, boolean paramBoolean, VolumeShaper.Configuration paramConfiguration) throws RemoteException;
  
  void setPlaybackProperties(IBinder paramIBinder, float paramFloat, boolean paramBoolean) throws RemoteException;
  
  void stop(IBinder paramIBinder) throws RemoteException;
  
  void stopAsync() throws RemoteException;
  
  class Default implements IRingtonePlayer {
    public void play(IBinder param1IBinder, Uri param1Uri, AudioAttributes param1AudioAttributes, float param1Float, boolean param1Boolean) throws RemoteException {}
    
    public void playWithVolumeShaping(IBinder param1IBinder, Uri param1Uri, AudioAttributes param1AudioAttributes, float param1Float, boolean param1Boolean, VolumeShaper.Configuration param1Configuration) throws RemoteException {}
    
    public void stop(IBinder param1IBinder) throws RemoteException {}
    
    public boolean isPlaying(IBinder param1IBinder) throws RemoteException {
      return false;
    }
    
    public void setPlaybackProperties(IBinder param1IBinder, float param1Float, boolean param1Boolean) throws RemoteException {}
    
    public void playAsync(Uri param1Uri, UserHandle param1UserHandle, boolean param1Boolean, AudioAttributes param1AudioAttributes) throws RemoteException {}
    
    public void stopAsync() throws RemoteException {}
    
    public String getTitle(Uri param1Uri) throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor openRingtone(Uri param1Uri) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRingtonePlayer {
    private static final String DESCRIPTOR = "android.media.IRingtonePlayer";
    
    static final int TRANSACTION_getTitle = 8;
    
    static final int TRANSACTION_isPlaying = 4;
    
    static final int TRANSACTION_openRingtone = 9;
    
    static final int TRANSACTION_play = 1;
    
    static final int TRANSACTION_playAsync = 6;
    
    static final int TRANSACTION_playWithVolumeShaping = 2;
    
    static final int TRANSACTION_setPlaybackProperties = 5;
    
    static final int TRANSACTION_stop = 3;
    
    static final int TRANSACTION_stopAsync = 7;
    
    public Stub() {
      attachInterface(this, "android.media.IRingtonePlayer");
    }
    
    public static IRingtonePlayer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRingtonePlayer");
      if (iInterface != null && iInterface instanceof IRingtonePlayer)
        return (IRingtonePlayer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "openRingtone";
        case 8:
          return "getTitle";
        case 7:
          return "stopAsync";
        case 6:
          return "playAsync";
        case 5:
          return "setPlaybackProperties";
        case 4:
          return "isPlaying";
        case 3:
          return "stop";
        case 2:
          return "playWithVolumeShaping";
        case 1:
          break;
      } 
      return "play";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1598968902) {
        boolean bool;
        ParcelFileDescriptor parcelFileDescriptor;
        String str;
        IBinder iBinder1;
        UserHandle userHandle;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.media.IRingtonePlayer");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            parcelFileDescriptor = openRingtone((Uri)param1Parcel1);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            parcelFileDescriptor.enforceInterface("android.media.IRingtonePlayer");
            if (parcelFileDescriptor.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)parcelFileDescriptor);
            } else {
              parcelFileDescriptor = null;
            } 
            str = getTitle((Uri)parcelFileDescriptor);
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str);
            return true;
          case 7:
            str.enforceInterface("android.media.IRingtonePlayer");
            stopAsync();
            return true;
          case 6:
            str.enforceInterface("android.media.IRingtonePlayer");
            if (str.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)str);
            } else {
              param1Parcel2 = null;
            } 
            if (str.readInt() != 0) {
              userHandle = UserHandle.CREATOR.createFromParcel((Parcel)str);
            } else {
              userHandle = null;
            } 
            if (str.readInt() != 0)
              bool2 = true; 
            if (str.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)str);
            } else {
              str = null;
            } 
            playAsync((Uri)param1Parcel2, userHandle, bool2, (AudioAttributes)str);
            return true;
          case 5:
            str.enforceInterface("android.media.IRingtonePlayer");
            iBinder = str.readStrongBinder();
            f = str.readFloat();
            bool2 = bool1;
            if (str.readInt() != 0)
              bool2 = true; 
            setPlaybackProperties(iBinder, f, bool2);
            return true;
          case 4:
            str.enforceInterface("android.media.IRingtonePlayer");
            iBinder1 = str.readStrongBinder();
            bool = isPlaying(iBinder1);
            iBinder.writeNoException();
            iBinder.writeInt(bool);
            return true;
          case 3:
            iBinder1.enforceInterface("android.media.IRingtonePlayer");
            iBinder1 = iBinder1.readStrongBinder();
            stop(iBinder1);
            return true;
          case 2:
            iBinder1.enforceInterface("android.media.IRingtonePlayer");
            iBinder2 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder = null;
            } 
            if (iBinder1.readInt() != 0) {
              AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              userHandle = null;
            } 
            f = iBinder1.readFloat();
            if (iBinder1.readInt() != 0) {
              bool2 = true;
            } else {
              bool2 = false;
            } 
            if (iBinder1.readInt() != 0) {
              VolumeShaper.Configuration configuration = VolumeShaper.Configuration.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder1 = null;
            } 
            playWithVolumeShaping(iBinder2, (Uri)iBinder, (AudioAttributes)userHandle, f, bool2, (VolumeShaper.Configuration)iBinder1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.media.IRingtonePlayer");
        IBinder iBinder2 = iBinder1.readStrongBinder();
        if (iBinder1.readInt() != 0) {
          Uri uri = Uri.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          iBinder = null;
        } 
        if (iBinder1.readInt() != 0) {
          AudioAttributes audioAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)iBinder1);
        } else {
          userHandle = null;
        } 
        float f = iBinder1.readFloat();
        if (iBinder1.readInt() != 0) {
          bool2 = true;
        } else {
          bool2 = false;
        } 
        play(iBinder2, (Uri)iBinder, (AudioAttributes)userHandle, f, bool2);
        return true;
      } 
      iBinder.writeString("android.media.IRingtonePlayer");
      return true;
    }
    
    private static class Proxy implements IRingtonePlayer {
      public static IRingtonePlayer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRingtonePlayer";
      }
      
      public void play(IBinder param2IBinder, Uri param2Uri, AudioAttributes param2AudioAttributes, float param2Float, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = false;
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2AudioAttributes != null) {
            parcel.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeFloat(param2Float);
          if (param2Boolean)
            bool = true; 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool1 && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            IRingtonePlayer.Stub.getDefaultImpl().play(param2IBinder, param2Uri, param2AudioAttributes, param2Float, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playWithVolumeShaping(IBinder param2IBinder, Uri param2Uri, AudioAttributes param2AudioAttributes, float param2Float, boolean param2Boolean, VolumeShaper.Configuration param2Configuration) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          try {
            parcel.writeStrongBinder(param2IBinder);
            if (param2Uri != null) {
              parcel.writeInt(1);
              param2Uri.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            if (param2AudioAttributes != null) {
              parcel.writeInt(1);
              param2AudioAttributes.writeToParcel(parcel, 0);
            } else {
              parcel.writeInt(0);
            } 
            try {
              boolean bool;
              parcel.writeFloat(param2Float);
              if (param2Boolean) {
                bool = true;
              } else {
                bool = false;
              } 
              parcel.writeInt(bool);
              if (param2Configuration != null) {
                parcel.writeInt(1);
                param2Configuration.writeToParcel(parcel, 0);
              } else {
                parcel.writeInt(0);
              } 
              try {
                boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
                if (!bool1 && IRingtonePlayer.Stub.getDefaultImpl() != null) {
                  IRingtonePlayer.Stub.getDefaultImpl().playWithVolumeShaping(param2IBinder, param2Uri, param2AudioAttributes, param2Float, param2Boolean, param2Configuration);
                  parcel.recycle();
                  return;
                } 
                parcel.recycle();
                return;
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel.recycle();
        throw param2IBinder;
      }
      
      public void stop(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            IRingtonePlayer.Stub.getDefaultImpl().stop(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public boolean isPlaying(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IRingtonePlayer");
          parcel1.writeStrongBinder(param2IBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(4, parcel1, parcel2, 0);
          if (!bool2 && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            bool1 = IRingtonePlayer.Stub.getDefaultImpl().isPlaying(param2IBinder);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setPlaybackProperties(IBinder param2IBinder, float param2Float, boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeFloat(param2Float);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool1 && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            IRingtonePlayer.Stub.getDefaultImpl().setPlaybackProperties(param2IBinder, param2Float, param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void playAsync(Uri param2Uri, UserHandle param2UserHandle, boolean param2Boolean, AudioAttributes param2AudioAttributes) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2UserHandle != null) {
            parcel.writeInt(1);
            param2UserHandle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          if (param2AudioAttributes != null) {
            parcel.writeInt(1);
            param2AudioAttributes.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool1 = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool1 && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            IRingtonePlayer.Stub.getDefaultImpl().playAsync(param2Uri, param2UserHandle, param2Boolean, param2AudioAttributes);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopAsync() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRingtonePlayer");
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IRingtonePlayer.Stub.getDefaultImpl() != null) {
            IRingtonePlayer.Stub.getDefaultImpl().stopAsync();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public String getTitle(Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IRingtonePlayer");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IRingtonePlayer.Stub.getDefaultImpl() != null)
            return IRingtonePlayer.Stub.getDefaultImpl().getTitle(param2Uri); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openRingtone(Uri param2Uri) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IRingtonePlayer");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IRingtonePlayer.Stub.getDefaultImpl() != null)
            return IRingtonePlayer.Stub.getDefaultImpl().openRingtone(param2Uri); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2Uri = null;
          } 
          return (ParcelFileDescriptor)param2Uri;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRingtonePlayer param1IRingtonePlayer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRingtonePlayer != null) {
          Proxy.sDefaultImpl = param1IRingtonePlayer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRingtonePlayer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
