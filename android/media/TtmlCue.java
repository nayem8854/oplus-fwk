package android.media;

class TtmlCue extends SubtitleTrack.Cue {
  public String mText;
  
  public String mTtmlFragment;
  
  public TtmlCue(long paramLong1, long paramLong2, String paramString1, String paramString2) {
    this.mStartTimeMs = paramLong1;
    this.mEndTimeMs = paramLong2;
    this.mText = paramString1;
    this.mTtmlFragment = paramString2;
  }
}
