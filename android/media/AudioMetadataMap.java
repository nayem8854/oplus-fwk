package android.media;

public interface AudioMetadataMap extends AudioMetadataReadMap {
  <T> T remove(AudioMetadata.Key<T> paramKey);
  
  <T> T set(AudioMetadata.Key<T> paramKey, T paramT);
}
