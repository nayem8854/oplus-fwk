package android.media;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public abstract class VolumeProvider {
  public static final int VOLUME_CONTROL_ABSOLUTE = 2;
  
  public static final int VOLUME_CONTROL_FIXED = 0;
  
  public static final int VOLUME_CONTROL_RELATIVE = 1;
  
  private Callback mCallback;
  
  private final String mControlId;
  
  private final int mControlType;
  
  private int mCurrentVolume;
  
  private final int mMaxVolume;
  
  public VolumeProvider(int paramInt1, int paramInt2, int paramInt3) {
    this(paramInt1, paramInt2, paramInt3, null);
  }
  
  public VolumeProvider(int paramInt1, int paramInt2, int paramInt3, String paramString) {
    this.mControlType = paramInt1;
    this.mMaxVolume = paramInt2;
    this.mCurrentVolume = paramInt3;
    this.mControlId = paramString;
  }
  
  public final int getVolumeControl() {
    return this.mControlType;
  }
  
  public final int getMaxVolume() {
    return this.mMaxVolume;
  }
  
  public final int getCurrentVolume() {
    return this.mCurrentVolume;
  }
  
  public final void setCurrentVolume(int paramInt) {
    this.mCurrentVolume = paramInt;
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onVolumeChanged(this); 
  }
  
  public final String getVolumeControlId() {
    return this.mControlId;
  }
  
  public void onSetVolumeTo(int paramInt) {}
  
  public void onAdjustVolume(int paramInt) {}
  
  public void setCallback(Callback paramCallback) {
    this.mCallback = paramCallback;
  }
  
  public static abstract class Callback {
    public abstract void onVolumeChanged(VolumeProvider param1VolumeProvider);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ControlType {}
}
