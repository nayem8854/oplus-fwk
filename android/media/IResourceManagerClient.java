package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IResourceManagerClient extends IInterface {
  String getName() throws RemoteException;
  
  boolean reclaimResource() throws RemoteException;
  
  class Default implements IResourceManagerClient {
    public boolean reclaimResource() throws RemoteException {
      return false;
    }
    
    public String getName() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IResourceManagerClient {
    private static final String DESCRIPTOR = "android.media.IResourceManagerClient";
    
    static final int TRANSACTION_getName = 2;
    
    static final int TRANSACTION_reclaimResource = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IResourceManagerClient");
    }
    
    public static IResourceManagerClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IResourceManagerClient");
      if (iInterface != null && iInterface instanceof IResourceManagerClient)
        return (IResourceManagerClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "getName";
      } 
      return "reclaimResource";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.IResourceManagerClient");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.IResourceManagerClient");
        str = getName();
        param1Parcel2.writeNoException();
        param1Parcel2.writeString(str);
        return true;
      } 
      str.enforceInterface("android.media.IResourceManagerClient");
      boolean bool = reclaimResource();
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements IResourceManagerClient {
      public static IResourceManagerClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IResourceManagerClient";
      }
      
      public boolean reclaimResource() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerClient");
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(1, parcel1, parcel2, 0);
          if (!bool2 && IResourceManagerClient.Stub.getDefaultImpl() != null) {
            bool1 = IResourceManagerClient.Stub.getDefaultImpl().reclaimResource();
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getName() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerClient");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IResourceManagerClient.Stub.getDefaultImpl() != null)
            return IResourceManagerClient.Stub.getDefaultImpl().getName(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IResourceManagerClient param1IResourceManagerClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IResourceManagerClient != null) {
          Proxy.sDefaultImpl = param1IResourceManagerClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IResourceManagerClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
