package android.media;

public class MediaCasException extends Exception {
  private MediaCasException(String paramString) {
    super(paramString);
  }
  
  static void throwExceptionIfNeeded(int paramInt) throws MediaCasException {
    if (paramInt == 0)
      return; 
    if (paramInt != 7) {
      if (paramInt != 8) {
        if (paramInt != 11) {
          MediaCasStateException.throwExceptionIfNeeded(paramInt);
          return;
        } 
        throw new DeniedByServerException(null);
      } 
      throw new ResourceBusyException(null);
    } 
    throw new NotProvisionedException(null);
  }
  
  class UnsupportedCasException extends MediaCasException {
    public UnsupportedCasException(MediaCasException this$0) {
      super((String)this$0);
    }
  }
  
  class NotProvisionedException extends MediaCasException {
    public NotProvisionedException(MediaCasException this$0) {
      super((String)this$0);
    }
  }
  
  class DeniedByServerException extends MediaCasException {
    public DeniedByServerException(MediaCasException this$0) {
      super((String)this$0);
    }
  }
  
  class ResourceBusyException extends MediaCasException {
    public ResourceBusyException(MediaCasException this$0) {
      super((String)this$0);
    }
  }
  
  class InsufficientResourceException extends MediaCasException {
    public InsufficientResourceException(MediaCasException this$0) {
      super((String)this$0);
    }
  }
}
