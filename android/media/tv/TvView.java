package android.media.tv;

import android.annotation.SystemApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Pair;
import android.view.InputEvent;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Queue;

public class TvView extends ViewGroup {
  private static final WeakReference<TvView> NULL_TV_VIEW = new WeakReference<>(null);
  
  private static final Object sMainTvViewLock = new Object();
  
  static {
    sMainTvView = NULL_TV_VIEW;
  }
  
  private final Handler mHandler = new Handler();
  
  private final Queue<Pair<String, Bundle>> mPendingAppPrivateCommands = new ArrayDeque<>();
  
  private final SurfaceHolder.Callback mSurfaceHolderCallback = (SurfaceHolder.Callback)new Object(this);
  
  private final TvInputManager.Session.FinishedInputEventCallback mFinishedInputEventCallback = (TvInputManager.Session.FinishedInputEventCallback)new Object(this);
  
  private static final boolean DEBUG = false;
  
  private static final String TAG = "TvView";
  
  private static final int ZORDER_MEDIA = 0;
  
  private static final int ZORDER_MEDIA_OVERLAY = 1;
  
  private static final int ZORDER_ON_TOP = 2;
  
  private static WeakReference<TvView> sMainTvView;
  
  private final AttributeSet mAttrs;
  
  private TvInputCallback mCallback;
  
  private Boolean mCaptionEnabled;
  
  private final int mDefStyleAttr;
  
  private OnUnhandledInputEventListener mOnUnhandledInputEventListener;
  
  private boolean mOverlayViewCreated;
  
  private Rect mOverlayViewFrame;
  
  private TvInputManager.Session mSession;
  
  private MySessionCallback mSessionCallback;
  
  private Float mStreamVolume;
  
  private Surface mSurface;
  
  private boolean mSurfaceChanged;
  
  private int mSurfaceFormat;
  
  private int mSurfaceHeight;
  
  private SurfaceView mSurfaceView;
  
  private int mSurfaceViewBottom;
  
  private int mSurfaceViewLeft;
  
  private int mSurfaceViewRight;
  
  private int mSurfaceViewTop;
  
  private int mSurfaceWidth;
  
  private TimeShiftPositionCallback mTimeShiftPositionCallback;
  
  private final TvInputManager mTvInputManager;
  
  private boolean mUseRequestedSurfaceLayout;
  
  private int mWindowZOrder;
  
  public TvView(Context paramContext) {
    this(paramContext, (AttributeSet)null, 0);
  }
  
  public TvView(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TvView(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    super(paramContext, paramAttributeSet, paramInt);
    this.mAttrs = paramAttributeSet;
    this.mDefStyleAttr = paramInt;
    resetSurfaceView();
    this.mTvInputManager = (TvInputManager)getContext().getSystemService("tv_input");
  }
  
  public void setCallback(TvInputCallback paramTvInputCallback) {
    this.mCallback = paramTvInputCallback;
  }
  
  @SystemApi
  public void setMain() {
    synchronized (sMainTvViewLock) {
      WeakReference<TvView> weakReference = new WeakReference();
      this((T)this);
      sMainTvView = weakReference;
      if (hasWindowFocus() && this.mSession != null)
        this.mSession.setMain(); 
      return;
    } 
  }
  
  public void setZOrderMediaOverlay(boolean paramBoolean) {
    if (paramBoolean) {
      this.mWindowZOrder = 1;
      removeSessionOverlayView();
    } else {
      this.mWindowZOrder = 0;
      createSessionOverlayView();
    } 
    SurfaceView surfaceView = this.mSurfaceView;
    if (surfaceView != null) {
      surfaceView.setZOrderOnTop(false);
      this.mSurfaceView.setZOrderMediaOverlay(paramBoolean);
    } 
  }
  
  public void setZOrderOnTop(boolean paramBoolean) {
    if (paramBoolean) {
      this.mWindowZOrder = 2;
      removeSessionOverlayView();
    } else {
      this.mWindowZOrder = 0;
      createSessionOverlayView();
    } 
    SurfaceView surfaceView = this.mSurfaceView;
    if (surfaceView != null) {
      surfaceView.setZOrderMediaOverlay(false);
      this.mSurfaceView.setZOrderOnTop(paramBoolean);
    } 
  }
  
  public void setStreamVolume(float paramFloat) {
    this.mStreamVolume = Float.valueOf(paramFloat);
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return; 
    session.setStreamVolume(paramFloat);
  }
  
  public void tune(String paramString, Uri paramUri) {
    tune(paramString, paramUri, (Bundle)null);
  }
  
  public void tune(String paramString, Uri paramUri, Bundle paramBundle) {
    if (!TextUtils.isEmpty(paramString))
      synchronized (sMainTvViewLock) {
        TvInputManager.Session session;
        if (sMainTvView.get() == null) {
          WeakReference<TvView> weakReference = new WeakReference();
          this((T)this);
          sMainTvView = weakReference;
        } 
        null = this.mSessionCallback;
        if (null != null && TextUtils.equals(((MySessionCallback)null).mInputId, paramString)) {
          session = this.mSession;
          if (session != null) {
            session.tune(paramUri, paramBundle);
          } else {
            this.mSessionCallback.mChannelUri = paramUri;
            this.mSessionCallback.mTuneParams = paramBundle;
          } 
        } else {
          resetInternal();
          MySessionCallback mySessionCallback = new MySessionCallback((String)session, paramUri, paramBundle);
          TvInputManager tvInputManager = this.mTvInputManager;
          if (tvInputManager != null)
            tvInputManager.createSession((String)session, mySessionCallback, this.mHandler); 
        } 
        return;
      }  
    throw new IllegalArgumentException("inputId cannot be null or an empty string");
  }
  
  public void reset() {
    synchronized (sMainTvViewLock) {
      if (this == sMainTvView.get())
        sMainTvView = NULL_TV_VIEW; 
      resetInternal();
      return;
    } 
  }
  
  private void resetInternal() {
    this.mSessionCallback = null;
    this.mPendingAppPrivateCommands.clear();
    if (this.mSession != null) {
      setSessionSurface((Surface)null);
      removeSessionOverlayView();
      this.mUseRequestedSurfaceLayout = false;
      this.mSession.release();
      this.mSession = null;
      resetSurfaceView();
    } 
  }
  
  public void requestUnblockContent(TvContentRating paramTvContentRating) {
    unblockContent(paramTvContentRating);
  }
  
  @SystemApi
  public void unblockContent(TvContentRating paramTvContentRating) {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.unblockContent(paramTvContentRating); 
  }
  
  public void setCaptionEnabled(boolean paramBoolean) {
    this.mCaptionEnabled = Boolean.valueOf(paramBoolean);
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.setCaptionEnabled(paramBoolean); 
  }
  
  public void selectTrack(int paramInt, String paramString) {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.selectTrack(paramInt, paramString); 
  }
  
  public List<TvTrackInfo> getTracks(int paramInt) {
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return null; 
    return session.getTracks(paramInt);
  }
  
  public String getSelectedTrack(int paramInt) {
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return null; 
    return session.getSelectedTrack(paramInt);
  }
  
  public void timeShiftPlay(String paramString, Uri paramUri) {
    if (!TextUtils.isEmpty(paramString))
      synchronized (sMainTvViewLock) {
        TvInputManager.Session session;
        if (sMainTvView.get() == null) {
          WeakReference<TvView> weakReference = new WeakReference();
          this((T)this);
          sMainTvView = weakReference;
        } 
        null = this.mSessionCallback;
        if (null != null && TextUtils.equals(((MySessionCallback)null).mInputId, paramString)) {
          session = this.mSession;
          if (session != null) {
            session.timeShiftPlay(paramUri);
          } else {
            this.mSessionCallback.mRecordedProgramUri = paramUri;
          } 
        } else {
          resetInternal();
          MySessionCallback mySessionCallback = new MySessionCallback((String)session, paramUri);
          null = this.mTvInputManager;
          if (null != null)
            null.createSession((String)session, mySessionCallback, this.mHandler); 
        } 
        return;
      }  
    throw new IllegalArgumentException("inputId cannot be null or an empty string");
  }
  
  public void timeShiftPause() {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.timeShiftPause(); 
  }
  
  public void timeShiftResume() {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.timeShiftResume(); 
  }
  
  public void timeShiftSeekTo(long paramLong) {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.timeShiftSeekTo(paramLong); 
  }
  
  public void timeShiftSetPlaybackParams(PlaybackParams paramPlaybackParams) {
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.timeShiftSetPlaybackParams(paramPlaybackParams); 
  }
  
  public void setTimeShiftPositionCallback(TimeShiftPositionCallback paramTimeShiftPositionCallback) {
    this.mTimeShiftPositionCallback = paramTimeShiftPositionCallback;
    ensurePositionTracking();
  }
  
  private void ensurePositionTracking() {
    boolean bool;
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return; 
    if (this.mTimeShiftPositionCallback != null) {
      bool = true;
    } else {
      bool = false;
    } 
    session.timeShiftEnablePositionTracking(bool);
  }
  
  public void sendAppPrivateCommand(String paramString, Bundle paramBundle) {
    if (!TextUtils.isEmpty(paramString)) {
      TvInputManager.Session session = this.mSession;
      if (session != null) {
        session.sendAppPrivateCommand(paramString, paramBundle);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sendAppPrivateCommand - session not yet created (action \"");
        stringBuilder.append(paramString);
        stringBuilder.append("\" pending)");
        Log.w("TvView", stringBuilder.toString());
        this.mPendingAppPrivateCommands.add(Pair.create(paramString, paramBundle));
      } 
      return;
    } 
    throw new IllegalArgumentException("action cannot be null or an empty string");
  }
  
  public boolean dispatchUnhandledInputEvent(InputEvent paramInputEvent) {
    OnUnhandledInputEventListener onUnhandledInputEventListener = this.mOnUnhandledInputEventListener;
    if (onUnhandledInputEventListener != null && 
      onUnhandledInputEventListener.onUnhandledInputEvent(paramInputEvent))
      return true; 
    return onUnhandledInputEvent(paramInputEvent);
  }
  
  public boolean onUnhandledInputEvent(InputEvent paramInputEvent) {
    return false;
  }
  
  public void setOnUnhandledInputEventListener(OnUnhandledInputEventListener paramOnUnhandledInputEventListener) {
    this.mOnUnhandledInputEventListener = paramOnUnhandledInputEventListener;
  }
  
  public boolean dispatchKeyEvent(KeyEvent paramKeyEvent) {
    boolean bool = super.dispatchKeyEvent(paramKeyEvent);
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mSession == null)
      return false; 
    paramKeyEvent = paramKeyEvent.copy();
    int i = this.mSession.dispatchInputEvent((InputEvent)paramKeyEvent, paramKeyEvent, this.mFinishedInputEventCallback, this.mHandler);
    if (i == 0)
      bool1 = false; 
    return bool1;
  }
  
  public boolean dispatchTouchEvent(MotionEvent paramMotionEvent) {
    boolean bool = super.dispatchTouchEvent(paramMotionEvent);
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mSession == null)
      return false; 
    paramMotionEvent = paramMotionEvent.copy();
    int i = this.mSession.dispatchInputEvent((InputEvent)paramMotionEvent, paramMotionEvent, this.mFinishedInputEventCallback, this.mHandler);
    if (i == 0)
      bool1 = false; 
    return bool1;
  }
  
  public boolean dispatchTrackballEvent(MotionEvent paramMotionEvent) {
    boolean bool = super.dispatchTrackballEvent(paramMotionEvent);
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mSession == null)
      return false; 
    paramMotionEvent = paramMotionEvent.copy();
    int i = this.mSession.dispatchInputEvent((InputEvent)paramMotionEvent, paramMotionEvent, this.mFinishedInputEventCallback, this.mHandler);
    if (i == 0)
      bool1 = false; 
    return bool1;
  }
  
  public boolean dispatchGenericMotionEvent(MotionEvent paramMotionEvent) {
    boolean bool = super.dispatchGenericMotionEvent(paramMotionEvent);
    boolean bool1 = true;
    if (bool)
      return true; 
    if (this.mSession == null)
      return false; 
    paramMotionEvent = paramMotionEvent.copy();
    int i = this.mSession.dispatchInputEvent((InputEvent)paramMotionEvent, paramMotionEvent, this.mFinishedInputEventCallback, this.mHandler);
    if (i == 0)
      bool1 = false; 
    return bool1;
  }
  
  public void dispatchWindowFocusChanged(boolean paramBoolean) {
    // Byte code:
    //   0: aload_0
    //   1: iload_1
    //   2: invokespecial dispatchWindowFocusChanged : (Z)V
    //   5: getstatic android/media/tv/TvView.sMainTvViewLock : Ljava/lang/Object;
    //   8: astore_2
    //   9: aload_2
    //   10: monitorenter
    //   11: iload_1
    //   12: ifeq -> 46
    //   15: aload_0
    //   16: getstatic android/media/tv/TvView.sMainTvView : Ljava/lang/ref/WeakReference;
    //   19: invokevirtual get : ()Ljava/lang/Object;
    //   22: if_acmpne -> 46
    //   25: aload_0
    //   26: getfield mSession : Landroid/media/tv/TvInputManager$Session;
    //   29: ifnull -> 46
    //   32: aload_0
    //   33: invokespecial checkChangeHdmiCecActiveSourcePermission : ()Z
    //   36: ifeq -> 46
    //   39: aload_0
    //   40: getfield mSession : Landroid/media/tv/TvInputManager$Session;
    //   43: invokevirtual setMain : ()V
    //   46: aload_2
    //   47: monitorexit
    //   48: return
    //   49: astore_3
    //   50: aload_2
    //   51: monitorexit
    //   52: aload_3
    //   53: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #680	-> 0
    //   #683	-> 5
    //   #684	-> 11
    //   #685	-> 32
    //   #686	-> 39
    //   #688	-> 46
    //   #689	-> 48
    //   #688	-> 49
    // Exception table:
    //   from	to	target	type
    //   15	32	49	finally
    //   32	39	49	finally
    //   39	46	49	finally
    //   46	48	49	finally
    //   50	52	49	finally
  }
  
  protected void onAttachedToWindow() {
    super.onAttachedToWindow();
    createSessionOverlayView();
  }
  
  protected void onDetachedFromWindow() {
    removeSessionOverlayView();
    super.onDetachedFromWindow();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    if (this.mUseRequestedSurfaceLayout) {
      this.mSurfaceView.layout(this.mSurfaceViewLeft, this.mSurfaceViewTop, this.mSurfaceViewRight, this.mSurfaceViewBottom);
    } else {
      this.mSurfaceView.layout(0, 0, paramInt3 - paramInt1, paramInt4 - paramInt2);
    } 
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    this.mSurfaceView.measure(paramInt1, paramInt2);
    int i = this.mSurfaceView.getMeasuredWidth();
    int j = this.mSurfaceView.getMeasuredHeight();
    int k = this.mSurfaceView.getMeasuredState();
    paramInt1 = resolveSizeAndState(i, paramInt1, k);
    paramInt2 = resolveSizeAndState(j, paramInt2, k << 16);
    setMeasuredDimension(paramInt1, paramInt2);
  }
  
  public boolean gatherTransparentRegion(Region paramRegion) {
    if (this.mWindowZOrder != 2 && 
      paramRegion != null) {
      int i = getWidth();
      int j = getHeight();
      if (i > 0 && j > 0) {
        int[] arrayOfInt = new int[2];
        getLocationInWindow(arrayOfInt);
        int k = arrayOfInt[0];
        int m = arrayOfInt[1];
        paramRegion.op(k, m, k + i, m + j, Region.Op.UNION);
      } 
    } 
    return super.gatherTransparentRegion(paramRegion);
  }
  
  public void draw(Canvas paramCanvas) {
    if (this.mWindowZOrder != 2)
      paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR); 
    super.draw(paramCanvas);
  }
  
  protected void dispatchDraw(Canvas paramCanvas) {
    if (this.mWindowZOrder != 2)
      paramCanvas.drawColor(0, PorterDuff.Mode.CLEAR); 
    super.dispatchDraw(paramCanvas);
  }
  
  protected void onVisibilityChanged(View paramView, int paramInt) {
    super.onVisibilityChanged(paramView, paramInt);
    this.mSurfaceView.setVisibility(paramInt);
    if (paramInt == 0) {
      createSessionOverlayView();
    } else {
      removeSessionOverlayView();
    } 
  }
  
  private void resetSurfaceView() {
    SurfaceView surfaceView = this.mSurfaceView;
    if (surfaceView != null) {
      surfaceView.getHolder().removeCallback(this.mSurfaceHolderCallback);
      removeView((View)this.mSurfaceView);
    } 
    this.mSurface = null;
    this.mSurfaceView = surfaceView = new SurfaceView(getContext(), this.mAttrs, this.mDefStyleAttr) {
        final TvView this$0;
        
        protected void updateSurface() {
          super.updateSurface();
          TvView.this.relayoutSessionOverlayView();
        }
      };
    surfaceView.setSecure(true);
    this.mSurfaceView.getHolder().addCallback(this.mSurfaceHolderCallback);
    int i = this.mWindowZOrder;
    if (i == 1) {
      this.mSurfaceView.setZOrderMediaOverlay(true);
    } else if (i == 2) {
      this.mSurfaceView.setZOrderOnTop(true);
    } 
    addView((View)this.mSurfaceView);
  }
  
  private void setSessionSurface(Surface paramSurface) {
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return; 
    session.setSurface(paramSurface);
  }
  
  private void dispatchSurfaceChanged(int paramInt1, int paramInt2, int paramInt3) {
    TvInputManager.Session session = this.mSession;
    if (session == null)
      return; 
    session.dispatchSurfaceChanged(paramInt1, paramInt2, paramInt3);
  }
  
  private void createSessionOverlayView() {
    if (this.mSession == null || !isAttachedToWindow() || this.mOverlayViewCreated || this.mWindowZOrder != 0)
      return; 
    Rect rect = getViewFrameOnScreen();
    this.mSession.createOverlayView((View)this, rect);
    this.mOverlayViewCreated = true;
  }
  
  private void removeSessionOverlayView() {
    TvInputManager.Session session = this.mSession;
    if (session == null || !this.mOverlayViewCreated)
      return; 
    session.removeOverlayView();
    this.mOverlayViewCreated = false;
    this.mOverlayViewFrame = null;
  }
  
  private void relayoutSessionOverlayView() {
    if (this.mSession == null || !isAttachedToWindow() || !this.mOverlayViewCreated || this.mWindowZOrder != 0)
      return; 
    Rect rect = getViewFrameOnScreen();
    if (rect.equals(this.mOverlayViewFrame))
      return; 
    this.mSession.relayoutOverlayView(rect);
    this.mOverlayViewFrame = rect;
  }
  
  private Rect getViewFrameOnScreen() {
    Rect rect = new Rect();
    getGlobalVisibleRect(rect);
    RectF rectF = new RectF(rect);
    getMatrix().mapRect(rectF);
    rectF.round(rect);
    return rect;
  }
  
  private boolean checkChangeHdmiCecActiveSourcePermission() {
    boolean bool;
    if (getContext().checkSelfPermission("android.permission.CHANGE_HDMI_CEC_ACTIVE_SOURCE") == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class TimeShiftPositionCallback {
    public void onTimeShiftStartPositionChanged(String param1String, long param1Long) {}
    
    public void onTimeShiftCurrentPositionChanged(String param1String, long param1Long) {}
  }
  
  class TvInputCallback {
    public void onConnectionFailed(String param1String) {}
    
    public void onDisconnected(String param1String) {}
    
    public void onChannelRetuned(String param1String, Uri param1Uri) {}
    
    public void onTracksChanged(String param1String, List<TvTrackInfo> param1List) {}
    
    public void onTrackSelected(String param1String1, int param1Int, String param1String2) {}
    
    public void onVideoSizeChanged(String param1String, int param1Int1, int param1Int2) {}
    
    public void onVideoAvailable(String param1String) {}
    
    public void onVideoUnavailable(String param1String, int param1Int) {}
    
    public void onContentAllowed(String param1String) {}
    
    public void onContentBlocked(String param1String, TvContentRating param1TvContentRating) {}
    
    @SystemApi
    public void onEvent(String param1String1, String param1String2, Bundle param1Bundle) {}
    
    public void onTimeShiftStatusChanged(String param1String, int param1Int) {}
  }
  
  class MySessionCallback extends TvInputManager.SessionCallback {
    Uri mChannelUri;
    
    final String mInputId;
    
    Uri mRecordedProgramUri;
    
    Bundle mTuneParams;
    
    final TvView this$0;
    
    MySessionCallback(String param1String, Uri param1Uri, Bundle param1Bundle) {
      this.mInputId = param1String;
      this.mChannelUri = param1Uri;
      this.mTuneParams = param1Bundle;
    }
    
    MySessionCallback(String param1String, Uri param1Uri) {
      this.mInputId = param1String;
      this.mRecordedProgramUri = param1Uri;
    }
    
    public void onSessionCreated(TvInputManager.Session param1Session) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onSessionCreated - session already created");
        if (param1Session != null)
          param1Session.release(); 
        return;
      } 
      TvView.access$902(TvView.this, param1Session);
      if (param1Session != null) {
        for (Pair pair : TvView.this.mPendingAppPrivateCommands)
          TvView.this.mSession.sendAppPrivateCommand((String)pair.first, (Bundle)pair.second); 
        TvView.this.mPendingAppPrivateCommands.clear();
        synchronized (TvView.sMainTvViewLock) {
          if (TvView.this.hasWindowFocus() && TvView.this == TvView.sMainTvView.get()) {
            TvView tvView = TvView.this;
            if (tvView.checkChangeHdmiCecActiveSourcePermission())
              TvView.this.mSession.setMain(); 
          } 
          if (TvView.this.mSurface != null) {
            null = TvView.this;
            null.setSessionSurface(((TvView)null).mSurface);
            if (TvView.this.mSurfaceChanged) {
              null = TvView.this;
              null.dispatchSurfaceChanged(((TvView)null).mSurfaceFormat, TvView.this.mSurfaceWidth, TvView.this.mSurfaceHeight);
            } 
          } 
          TvView.this.createSessionOverlayView();
          if (TvView.this.mStreamVolume != null)
            TvView.this.mSession.setStreamVolume(TvView.this.mStreamVolume.floatValue()); 
          if (TvView.this.mCaptionEnabled != null)
            TvView.this.mSession.setCaptionEnabled(TvView.this.mCaptionEnabled.booleanValue()); 
          if (this.mChannelUri != null) {
            TvView.this.mSession.tune(this.mChannelUri, this.mTuneParams);
          } else {
            TvView.this.mSession.timeShiftPlay(this.mRecordedProgramUri);
          } 
          TvView.this.ensurePositionTracking();
        } 
      } else {
        TvView.access$802(TvView.this, (MySessionCallback)null);
        if (TvView.this.mCallback != null)
          TvView.this.mCallback.onConnectionFailed(this.mInputId); 
      } 
    }
    
    public void onSessionReleased(TvInputManager.Session param1Session) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onSessionReleased - session not created");
        return;
      } 
      TvView.access$1902(TvView.this, false);
      TvView.access$2002(TvView.this, (Rect)null);
      TvView.access$802(TvView.this, (MySessionCallback)null);
      TvView.access$902(TvView.this, (TvInputManager.Session)null);
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onDisconnected(this.mInputId); 
    }
    
    public void onChannelRetuned(TvInputManager.Session param1Session, Uri param1Uri) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onChannelRetuned - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onChannelRetuned(this.mInputId, param1Uri); 
    }
    
    public void onTracksChanged(TvInputManager.Session param1Session, List<TvTrackInfo> param1List) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onTracksChanged - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onTracksChanged(this.mInputId, param1List); 
    }
    
    public void onTrackSelected(TvInputManager.Session param1Session, int param1Int, String param1String) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onTrackSelected - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onTrackSelected(this.mInputId, param1Int, param1String); 
    }
    
    public void onVideoSizeChanged(TvInputManager.Session param1Session, int param1Int1, int param1Int2) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onVideoSizeChanged - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onVideoSizeChanged(this.mInputId, param1Int1, param1Int2); 
    }
    
    public void onVideoAvailable(TvInputManager.Session param1Session) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onVideoAvailable - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onVideoAvailable(this.mInputId); 
    }
    
    public void onVideoUnavailable(TvInputManager.Session param1Session, int param1Int) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onVideoUnavailable - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onVideoUnavailable(this.mInputId, param1Int); 
    }
    
    public void onContentAllowed(TvInputManager.Session param1Session) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onContentAllowed - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onContentAllowed(this.mInputId); 
    }
    
    public void onContentBlocked(TvInputManager.Session param1Session, TvContentRating param1TvContentRating) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onContentBlocked - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onContentBlocked(this.mInputId, param1TvContentRating); 
    }
    
    public void onLayoutSurface(TvInputManager.Session param1Session, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onLayoutSurface - session not created");
        return;
      } 
      TvView.access$2102(TvView.this, param1Int1);
      TvView.access$2202(TvView.this, param1Int2);
      TvView.access$2302(TvView.this, param1Int3);
      TvView.access$2402(TvView.this, param1Int4);
      TvView.access$2502(TvView.this, true);
      TvView.this.requestLayout();
    }
    
    public void onSessionEvent(TvInputManager.Session param1Session, String param1String, Bundle param1Bundle) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onSessionEvent - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onEvent(this.mInputId, param1String, param1Bundle); 
    }
    
    public void onTimeShiftStatusChanged(TvInputManager.Session param1Session, int param1Int) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onTimeShiftStatusChanged - session not created");
        return;
      } 
      if (TvView.this.mCallback != null)
        TvView.this.mCallback.onTimeShiftStatusChanged(this.mInputId, param1Int); 
    }
    
    public void onTimeShiftStartPositionChanged(TvInputManager.Session param1Session, long param1Long) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onTimeShiftStartPositionChanged - session not created");
        return;
      } 
      if (TvView.this.mTimeShiftPositionCallback != null)
        TvView.this.mTimeShiftPositionCallback.onTimeShiftStartPositionChanged(this.mInputId, param1Long); 
    }
    
    public void onTimeShiftCurrentPositionChanged(TvInputManager.Session param1Session, long param1Long) {
      if (this != TvView.this.mSessionCallback) {
        Log.w("TvView", "onTimeShiftCurrentPositionChanged - session not created");
        return;
      } 
      if (TvView.this.mTimeShiftPositionCallback != null)
        TvView.this.mTimeShiftPositionCallback.onTimeShiftCurrentPositionChanged(this.mInputId, param1Long); 
    }
  }
  
  class OnUnhandledInputEventListener {
    public abstract boolean onUnhandledInputEvent(InputEvent param1InputEvent);
  }
}
