package android.media.tv;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

@SystemApi
public class TvStreamConfig implements Parcelable {
  public static final Parcelable.Creator<TvStreamConfig> CREATOR;
  
  public static final int STREAM_TYPE_BUFFER_PRODUCER = 2;
  
  public static final int STREAM_TYPE_INDEPENDENT_VIDEO_SOURCE = 1;
  
  static final String TAG = TvStreamConfig.class.getSimpleName();
  
  private int mGeneration;
  
  private int mMaxHeight;
  
  private int mMaxWidth;
  
  private int mStreamId;
  
  private int mType;
  
  static {
    CREATOR = new Parcelable.Creator<TvStreamConfig>() {
        public TvStreamConfig createFromParcel(Parcel param1Parcel) {
          try {
            TvStreamConfig.Builder builder = new TvStreamConfig.Builder();
            this();
            builder = builder.streamId(param1Parcel.readInt());
            builder = builder.type(param1Parcel.readInt());
            builder = builder.maxWidth(param1Parcel.readInt());
            builder = builder.maxHeight(param1Parcel.readInt());
            return builder.generation(param1Parcel.readInt()).build();
          } catch (Exception exception) {
            Log.e(TvStreamConfig.TAG, "Exception creating TvStreamConfig from parcel", exception);
            return null;
          } 
        }
        
        public TvStreamConfig[] newArray(int param1Int) {
          return new TvStreamConfig[param1Int];
        }
      };
  }
  
  private TvStreamConfig() {}
  
  public int getStreamId() {
    return this.mStreamId;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getMaxWidth() {
    return this.mMaxWidth;
  }
  
  public int getMaxHeight() {
    return this.mMaxHeight;
  }
  
  public int getGeneration() {
    return this.mGeneration;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TvStreamConfig {mStreamId=");
    stringBuilder.append(this.mStreamId);
    stringBuilder.append(";mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append(";mGeneration=");
    stringBuilder.append(this.mGeneration);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mStreamId);
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mMaxWidth);
    paramParcel.writeInt(this.mMaxHeight);
    paramParcel.writeInt(this.mGeneration);
  }
  
  class Builder {
    private Integer mGeneration;
    
    private Integer mMaxHeight;
    
    private Integer mMaxWidth;
    
    private Integer mStreamId;
    
    private Integer mType;
    
    public Builder streamId(int param1Int) {
      this.mStreamId = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder type(int param1Int) {
      this.mType = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder maxWidth(int param1Int) {
      this.mMaxWidth = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder maxHeight(int param1Int) {
      this.mMaxHeight = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder generation(int param1Int) {
      this.mGeneration = Integer.valueOf(param1Int);
      return this;
    }
    
    public TvStreamConfig build() {
      if (this.mStreamId != null && this.mType != null && this.mMaxWidth != null && this.mMaxHeight != null && this.mGeneration != null) {
        TvStreamConfig tvStreamConfig = new TvStreamConfig();
        TvStreamConfig.access$102(tvStreamConfig, this.mStreamId.intValue());
        TvStreamConfig.access$202(tvStreamConfig, this.mType.intValue());
        TvStreamConfig.access$302(tvStreamConfig, this.mMaxWidth.intValue());
        TvStreamConfig.access$402(tvStreamConfig, this.mMaxHeight.intValue());
        TvStreamConfig.access$502(tvStreamConfig, this.mGeneration.intValue());
        return tvStreamConfig;
      } 
      throw new UnsupportedOperationException();
    }
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = false;
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof TvStreamConfig))
      return false; 
    paramObject = paramObject;
    boolean bool2 = bool1;
    if (((TvStreamConfig)paramObject).mGeneration == this.mGeneration) {
      bool2 = bool1;
      if (((TvStreamConfig)paramObject).mStreamId == this.mStreamId) {
        bool2 = bool1;
        if (((TvStreamConfig)paramObject).mType == this.mType) {
          bool2 = bool1;
          if (((TvStreamConfig)paramObject).mMaxWidth == this.mMaxWidth) {
            bool2 = bool1;
            if (((TvStreamConfig)paramObject).mMaxHeight == this.mMaxHeight)
              bool2 = true; 
          } 
        } 
      } 
    } 
    return bool2;
  }
}
