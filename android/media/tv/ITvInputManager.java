package android.media.tv;

import android.content.Intent;
import android.graphics.Rect;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.view.Surface;
import java.util.List;

public interface ITvInputManager extends IInterface {
  ITvInputHardware acquireTvInputHardware(int paramInt1, ITvInputHardwareCallback paramITvInputHardwareCallback, TvInputInfo paramTvInputInfo, int paramInt2, String paramString, int paramInt3) throws RemoteException;
  
  void addBlockedRating(String paramString, int paramInt) throws RemoteException;
  
  void addHardwareDevice(int paramInt) throws RemoteException;
  
  boolean captureFrame(String paramString, Surface paramSurface, TvStreamConfig paramTvStreamConfig, int paramInt) throws RemoteException;
  
  void createOverlayView(IBinder paramIBinder1, IBinder paramIBinder2, Rect paramRect, int paramInt) throws RemoteException;
  
  void createSession(ITvInputClient paramITvInputClient, String paramString, boolean paramBoolean, int paramInt1, int paramInt2) throws RemoteException;
  
  void dispatchSurfaceChanged(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  List<TvStreamConfig> getAvailableTvStreamConfigList(String paramString, int paramInt) throws RemoteException;
  
  List<String> getBlockedRatings(int paramInt) throws RemoteException;
  
  int getClientPid(String paramString) throws RemoteException;
  
  List<DvbDeviceInfo> getDvbDeviceList() throws RemoteException;
  
  List<TvInputHardwareInfo> getHardwareList() throws RemoteException;
  
  List<TvContentRatingSystemInfo> getTvContentRatingSystemList(int paramInt) throws RemoteException;
  
  TvInputInfo getTvInputInfo(String paramString, int paramInt) throws RemoteException;
  
  List<TvInputInfo> getTvInputList(int paramInt) throws RemoteException;
  
  int getTvInputState(String paramString, int paramInt) throws RemoteException;
  
  boolean isParentalControlsEnabled(int paramInt) throws RemoteException;
  
  boolean isRatingBlocked(String paramString, int paramInt) throws RemoteException;
  
  boolean isSingleSessionActive(int paramInt) throws RemoteException;
  
  ParcelFileDescriptor openDvbDevice(DvbDeviceInfo paramDvbDeviceInfo, int paramInt) throws RemoteException;
  
  void registerCallback(ITvInputManagerCallback paramITvInputManagerCallback, int paramInt) throws RemoteException;
  
  void relayoutOverlayView(IBinder paramIBinder, Rect paramRect, int paramInt) throws RemoteException;
  
  void releaseSession(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void releaseTvInputHardware(int paramInt1, ITvInputHardware paramITvInputHardware, int paramInt2) throws RemoteException;
  
  void removeBlockedRating(String paramString, int paramInt) throws RemoteException;
  
  void removeHardwareDevice(int paramInt) throws RemoteException;
  
  void removeOverlayView(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void requestChannelBrowsable(Uri paramUri, int paramInt) throws RemoteException;
  
  void selectTrack(IBinder paramIBinder, int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void sendAppPrivateCommand(IBinder paramIBinder, String paramString, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void sendTvInputNotifyIntent(Intent paramIntent, int paramInt) throws RemoteException;
  
  void setCaptionEnabled(IBinder paramIBinder, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setMainSession(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void setParentalControlsEnabled(boolean paramBoolean, int paramInt) throws RemoteException;
  
  void setSurface(IBinder paramIBinder, Surface paramSurface, int paramInt) throws RemoteException;
  
  void setVolume(IBinder paramIBinder, float paramFloat, int paramInt) throws RemoteException;
  
  void startRecording(IBinder paramIBinder, Uri paramUri, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void stopRecording(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void timeShiftEnablePositionTracking(IBinder paramIBinder, boolean paramBoolean, int paramInt) throws RemoteException;
  
  void timeShiftPause(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void timeShiftPlay(IBinder paramIBinder, Uri paramUri, int paramInt) throws RemoteException;
  
  void timeShiftResume(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void timeShiftSeekTo(IBinder paramIBinder, long paramLong, int paramInt) throws RemoteException;
  
  void timeShiftSetPlaybackParams(IBinder paramIBinder, PlaybackParams paramPlaybackParams, int paramInt) throws RemoteException;
  
  void tune(IBinder paramIBinder, Uri paramUri, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void unblockContent(IBinder paramIBinder, String paramString, int paramInt) throws RemoteException;
  
  void unregisterCallback(ITvInputManagerCallback paramITvInputManagerCallback, int paramInt) throws RemoteException;
  
  void updateTvInputInfo(TvInputInfo paramTvInputInfo, int paramInt) throws RemoteException;
  
  class Default implements ITvInputManager {
    public List<TvInputInfo> getTvInputList(int param1Int) throws RemoteException {
      return null;
    }
    
    public TvInputInfo getTvInputInfo(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public void updateTvInputInfo(TvInputInfo param1TvInputInfo, int param1Int) throws RemoteException {}
    
    public int getTvInputState(String param1String, int param1Int) throws RemoteException {
      return 0;
    }
    
    public List<TvContentRatingSystemInfo> getTvContentRatingSystemList(int param1Int) throws RemoteException {
      return null;
    }
    
    public void registerCallback(ITvInputManagerCallback param1ITvInputManagerCallback, int param1Int) throws RemoteException {}
    
    public void unregisterCallback(ITvInputManagerCallback param1ITvInputManagerCallback, int param1Int) throws RemoteException {}
    
    public boolean isParentalControlsEnabled(int param1Int) throws RemoteException {
      return false;
    }
    
    public void setParentalControlsEnabled(boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public boolean isRatingBlocked(String param1String, int param1Int) throws RemoteException {
      return false;
    }
    
    public List<String> getBlockedRatings(int param1Int) throws RemoteException {
      return null;
    }
    
    public void addBlockedRating(String param1String, int param1Int) throws RemoteException {}
    
    public void removeBlockedRating(String param1String, int param1Int) throws RemoteException {}
    
    public void createSession(ITvInputClient param1ITvInputClient, String param1String, boolean param1Boolean, int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseSession(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public int getClientPid(String param1String) throws RemoteException {
      return 0;
    }
    
    public void setMainSession(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void setSurface(IBinder param1IBinder, Surface param1Surface, int param1Int) throws RemoteException {}
    
    public void dispatchSurfaceChanged(IBinder param1IBinder, int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void setVolume(IBinder param1IBinder, float param1Float, int param1Int) throws RemoteException {}
    
    public void tune(IBinder param1IBinder, Uri param1Uri, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public void setCaptionEnabled(IBinder param1IBinder, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void selectTrack(IBinder param1IBinder, int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void sendAppPrivateCommand(IBinder param1IBinder, String param1String, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public void createOverlayView(IBinder param1IBinder1, IBinder param1IBinder2, Rect param1Rect, int param1Int) throws RemoteException {}
    
    public void relayoutOverlayView(IBinder param1IBinder, Rect param1Rect, int param1Int) throws RemoteException {}
    
    public void removeOverlayView(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void unblockContent(IBinder param1IBinder, String param1String, int param1Int) throws RemoteException {}
    
    public void timeShiftPlay(IBinder param1IBinder, Uri param1Uri, int param1Int) throws RemoteException {}
    
    public void timeShiftPause(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void timeShiftResume(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void timeShiftSeekTo(IBinder param1IBinder, long param1Long, int param1Int) throws RemoteException {}
    
    public void timeShiftSetPlaybackParams(IBinder param1IBinder, PlaybackParams param1PlaybackParams, int param1Int) throws RemoteException {}
    
    public void timeShiftEnablePositionTracking(IBinder param1IBinder, boolean param1Boolean, int param1Int) throws RemoteException {}
    
    public void startRecording(IBinder param1IBinder, Uri param1Uri, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public void stopRecording(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public List<TvInputHardwareInfo> getHardwareList() throws RemoteException {
      return null;
    }
    
    public ITvInputHardware acquireTvInputHardware(int param1Int1, ITvInputHardwareCallback param1ITvInputHardwareCallback, TvInputInfo param1TvInputInfo, int param1Int2, String param1String, int param1Int3) throws RemoteException {
      return null;
    }
    
    public void releaseTvInputHardware(int param1Int1, ITvInputHardware param1ITvInputHardware, int param1Int2) throws RemoteException {}
    
    public List<TvStreamConfig> getAvailableTvStreamConfigList(String param1String, int param1Int) throws RemoteException {
      return null;
    }
    
    public boolean captureFrame(String param1String, Surface param1Surface, TvStreamConfig param1TvStreamConfig, int param1Int) throws RemoteException {
      return false;
    }
    
    public boolean isSingleSessionActive(int param1Int) throws RemoteException {
      return false;
    }
    
    public List<DvbDeviceInfo> getDvbDeviceList() throws RemoteException {
      return null;
    }
    
    public ParcelFileDescriptor openDvbDevice(DvbDeviceInfo param1DvbDeviceInfo, int param1Int) throws RemoteException {
      return null;
    }
    
    public void sendTvInputNotifyIntent(Intent param1Intent, int param1Int) throws RemoteException {}
    
    public void requestChannelBrowsable(Uri param1Uri, int param1Int) throws RemoteException {}
    
    public void addHardwareDevice(int param1Int) throws RemoteException {}
    
    public void removeHardwareDevice(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputManager {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputManager";
    
    static final int TRANSACTION_acquireTvInputHardware = 38;
    
    static final int TRANSACTION_addBlockedRating = 12;
    
    static final int TRANSACTION_addHardwareDevice = 47;
    
    static final int TRANSACTION_captureFrame = 41;
    
    static final int TRANSACTION_createOverlayView = 25;
    
    static final int TRANSACTION_createSession = 14;
    
    static final int TRANSACTION_dispatchSurfaceChanged = 19;
    
    static final int TRANSACTION_getAvailableTvStreamConfigList = 40;
    
    static final int TRANSACTION_getBlockedRatings = 11;
    
    static final int TRANSACTION_getClientPid = 16;
    
    static final int TRANSACTION_getDvbDeviceList = 43;
    
    static final int TRANSACTION_getHardwareList = 37;
    
    static final int TRANSACTION_getTvContentRatingSystemList = 5;
    
    static final int TRANSACTION_getTvInputInfo = 2;
    
    static final int TRANSACTION_getTvInputList = 1;
    
    static final int TRANSACTION_getTvInputState = 4;
    
    static final int TRANSACTION_isParentalControlsEnabled = 8;
    
    static final int TRANSACTION_isRatingBlocked = 10;
    
    static final int TRANSACTION_isSingleSessionActive = 42;
    
    static final int TRANSACTION_openDvbDevice = 44;
    
    static final int TRANSACTION_registerCallback = 6;
    
    static final int TRANSACTION_relayoutOverlayView = 26;
    
    static final int TRANSACTION_releaseSession = 15;
    
    static final int TRANSACTION_releaseTvInputHardware = 39;
    
    static final int TRANSACTION_removeBlockedRating = 13;
    
    static final int TRANSACTION_removeHardwareDevice = 48;
    
    static final int TRANSACTION_removeOverlayView = 27;
    
    static final int TRANSACTION_requestChannelBrowsable = 46;
    
    static final int TRANSACTION_selectTrack = 23;
    
    static final int TRANSACTION_sendAppPrivateCommand = 24;
    
    static final int TRANSACTION_sendTvInputNotifyIntent = 45;
    
    static final int TRANSACTION_setCaptionEnabled = 22;
    
    static final int TRANSACTION_setMainSession = 17;
    
    static final int TRANSACTION_setParentalControlsEnabled = 9;
    
    static final int TRANSACTION_setSurface = 18;
    
    static final int TRANSACTION_setVolume = 20;
    
    static final int TRANSACTION_startRecording = 35;
    
    static final int TRANSACTION_stopRecording = 36;
    
    static final int TRANSACTION_timeShiftEnablePositionTracking = 34;
    
    static final int TRANSACTION_timeShiftPause = 30;
    
    static final int TRANSACTION_timeShiftPlay = 29;
    
    static final int TRANSACTION_timeShiftResume = 31;
    
    static final int TRANSACTION_timeShiftSeekTo = 32;
    
    static final int TRANSACTION_timeShiftSetPlaybackParams = 33;
    
    static final int TRANSACTION_tune = 21;
    
    static final int TRANSACTION_unblockContent = 28;
    
    static final int TRANSACTION_unregisterCallback = 7;
    
    static final int TRANSACTION_updateTvInputInfo = 3;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputManager");
    }
    
    public static ITvInputManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputManager");
      if (iInterface != null && iInterface instanceof ITvInputManager)
        return (ITvInputManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 48:
          return "removeHardwareDevice";
        case 47:
          return "addHardwareDevice";
        case 46:
          return "requestChannelBrowsable";
        case 45:
          return "sendTvInputNotifyIntent";
        case 44:
          return "openDvbDevice";
        case 43:
          return "getDvbDeviceList";
        case 42:
          return "isSingleSessionActive";
        case 41:
          return "captureFrame";
        case 40:
          return "getAvailableTvStreamConfigList";
        case 39:
          return "releaseTvInputHardware";
        case 38:
          return "acquireTvInputHardware";
        case 37:
          return "getHardwareList";
        case 36:
          return "stopRecording";
        case 35:
          return "startRecording";
        case 34:
          return "timeShiftEnablePositionTracking";
        case 33:
          return "timeShiftSetPlaybackParams";
        case 32:
          return "timeShiftSeekTo";
        case 31:
          return "timeShiftResume";
        case 30:
          return "timeShiftPause";
        case 29:
          return "timeShiftPlay";
        case 28:
          return "unblockContent";
        case 27:
          return "removeOverlayView";
        case 26:
          return "relayoutOverlayView";
        case 25:
          return "createOverlayView";
        case 24:
          return "sendAppPrivateCommand";
        case 23:
          return "selectTrack";
        case 22:
          return "setCaptionEnabled";
        case 21:
          return "tune";
        case 20:
          return "setVolume";
        case 19:
          return "dispatchSurfaceChanged";
        case 18:
          return "setSurface";
        case 17:
          return "setMainSession";
        case 16:
          return "getClientPid";
        case 15:
          return "releaseSession";
        case 14:
          return "createSession";
        case 13:
          return "removeBlockedRating";
        case 12:
          return "addBlockedRating";
        case 11:
          return "getBlockedRatings";
        case 10:
          return "isRatingBlocked";
        case 9:
          return "setParentalControlsEnabled";
        case 8:
          return "isParentalControlsEnabled";
        case 7:
          return "unregisterCallback";
        case 6:
          return "registerCallback";
        case 5:
          return "getTvContentRatingSystemList";
        case 4:
          return "getTvInputState";
        case 3:
          return "updateTvInputInfo";
        case 2:
          return "getTvInputInfo";
        case 1:
          break;
      } 
      return "getTvInputList";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        ParcelFileDescriptor parcelFileDescriptor;
        List<DvbDeviceInfo> list3;
        ITvInputHardware iTvInputHardware1;
        List<TvInputHardwareInfo> list2;
        String str1;
        List<String> list1;
        TvInputInfo tvInputInfo;
        Uri uri;
        String str4;
        ITvInputHardware iTvInputHardware2;
        IBinder iBinder1;
        String str3;
        ITvInputManagerCallback iTvInputManagerCallback;
        String str2, str5;
        IBinder iBinder2;
        TvStreamConfig tvStreamConfig;
        ITvInputHardwareCallback iTvInputHardwareCallback;
        IBinder iBinder5;
        String str7;
        IBinder iBinder4;
        String str6;
        IBinder iBinder3;
        ITvInputClient iTvInputClient;
        int n;
        long l;
        float f;
        int i1;
        boolean bool5 = false, bool6 = false, bool7 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 48:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManager");
            param1Int1 = param1Parcel1.readInt();
            removeHardwareDevice(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 47:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManager");
            param1Int1 = param1Parcel1.readInt();
            addHardwareDevice(param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 46:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManager");
            if (param1Parcel1.readInt() != 0) {
              uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            requestChannelBrowsable(uri, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 45:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManager");
            if (param1Parcel1.readInt() != 0) {
              Intent intent = Intent.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            sendTvInputNotifyIntent((Intent)uri, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 44:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManager");
            if (param1Parcel1.readInt() != 0) {
              DvbDeviceInfo dvbDeviceInfo = DvbDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              uri = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            parcelFileDescriptor = openDvbDevice((DvbDeviceInfo)uri, param1Int1);
            param1Parcel2.writeNoException();
            if (parcelFileDescriptor != null) {
              param1Parcel2.writeInt(1);
              parcelFileDescriptor.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 43:
            parcelFileDescriptor.enforceInterface("android.media.tv.ITvInputManager");
            list3 = getDvbDeviceList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 42:
            list3.enforceInterface("android.media.tv.ITvInputManager");
            param1Int1 = list3.readInt();
            bool4 = isSingleSessionActive(param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            return true;
          case 41:
            list3.enforceInterface("android.media.tv.ITvInputManager");
            str5 = list3.readString();
            if (list3.readInt() != 0) {
              Surface surface = Surface.CREATOR.createFromParcel((Parcel)list3);
            } else {
              uri = null;
            } 
            if (list3.readInt() != 0) {
              tvStreamConfig = TvStreamConfig.CREATOR.createFromParcel((Parcel)list3);
            } else {
              tvStreamConfig = null;
            } 
            m = list3.readInt();
            bool3 = captureFrame(str5, (Surface)uri, tvStreamConfig, m);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            return true;
          case 40:
            list3.enforceInterface("android.media.tv.ITvInputManager");
            str4 = list3.readString();
            k = list3.readInt();
            list3 = (List)getAvailableTvStreamConfigList(str4, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list3);
            return true;
          case 39:
            list3.enforceInterface("android.media.tv.ITvInputManager");
            k = list3.readInt();
            iTvInputHardware2 = ITvInputHardware.Stub.asInterface(list3.readStrongBinder());
            param1Int2 = list3.readInt();
            releaseTvInputHardware(k, iTvInputHardware2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 38:
            list3.enforceInterface("android.media.tv.ITvInputManager");
            n = list3.readInt();
            iTvInputHardwareCallback = ITvInputHardwareCallback.Stub.asInterface(list3.readStrongBinder());
            if (list3.readInt() != 0) {
              TvInputInfo tvInputInfo1 = TvInputInfo.CREATOR.createFromParcel((Parcel)list3);
            } else {
              iTvInputHardware2 = null;
            } 
            param1Int2 = list3.readInt();
            str5 = list3.readString();
            k = list3.readInt();
            iTvInputHardware1 = acquireTvInputHardware(n, iTvInputHardwareCallback, (TvInputInfo)iTvInputHardware2, param1Int2, str5, k);
            param1Parcel2.writeNoException();
            if (iTvInputHardware1 != null) {
              IBinder iBinder = iTvInputHardware1.asBinder();
            } else {
              iTvInputHardware1 = null;
            } 
            param1Parcel2.writeStrongBinder((IBinder)iTvInputHardware1);
            return true;
          case 37:
            iTvInputHardware1.enforceInterface("android.media.tv.ITvInputManager");
            list2 = getHardwareList();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list2);
            return true;
          case 36:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            stopRecording(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 35:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder2 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Uri uri1 = Uri.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            if (list2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iTvInputHardwareCallback = null;
            } 
            k = list2.readInt();
            startRecording(iBinder2, (Uri)iBinder1, (Bundle)iTvInputHardwareCallback, k);
            param1Parcel2.writeNoException();
            return true;
          case 34:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            bool6 = bool7;
            if (list2.readInt() != 0)
              bool6 = true; 
            k = list2.readInt();
            timeShiftEnablePositionTracking(iBinder1, bool6, k);
            param1Parcel2.writeNoException();
            return true;
          case 33:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder5 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              PlaybackParams playbackParams = PlaybackParams.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            timeShiftSetPlaybackParams(iBinder5, (PlaybackParams)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 32:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            l = list2.readLong();
            k = list2.readInt();
            timeShiftSeekTo(iBinder1, l, k);
            param1Parcel2.writeNoException();
            return true;
          case 31:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            timeShiftResume(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            timeShiftPause(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder5 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Uri uri1 = Uri.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            timeShiftPlay(iBinder5, (Uri)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            str7 = list2.readString();
            k = list2.readInt();
            unblockContent(iBinder1, str7, k);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            removeOverlayView(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder4 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Rect rect = Rect.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            relayoutOverlayView(iBinder4, (Rect)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder4 = list2.readStrongBinder();
            iBinder2 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Rect rect = Rect.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            createOverlayView(iBinder4, iBinder2, (Rect)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder2 = list2.readStrongBinder();
            str6 = list2.readString();
            if (list2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            sendAppPrivateCommand(iBinder2, str6, (Bundle)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            str6 = list2.readString();
            param1Int2 = list2.readInt();
            selectTrack(iBinder1, k, str6, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            bool6 = bool5;
            if (list2.readInt() != 0)
              bool6 = true; 
            k = list2.readInt();
            setCaptionEnabled(iBinder1, bool6, k);
            param1Parcel2.writeNoException();
            return true;
          case 21:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder2 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Uri uri1 = Uri.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            if (list2.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)list2);
            } else {
              str6 = null;
            } 
            k = list2.readInt();
            tune(iBinder2, (Uri)iBinder1, (Bundle)str6, k);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            f = list2.readFloat();
            k = list2.readInt();
            setVolume(iBinder1, f, k);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            i1 = list2.readInt();
            n = list2.readInt();
            param1Int2 = list2.readInt();
            dispatchSurfaceChanged(iBinder1, k, i1, n, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder3 = list2.readStrongBinder();
            if (list2.readInt() != 0) {
              Surface surface = Surface.CREATOR.createFromParcel((Parcel)list2);
            } else {
              iBinder1 = null;
            } 
            k = list2.readInt();
            setSurface(iBinder3, (Surface)iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = list2.readStrongBinder();
            k = list2.readInt();
            setMainSession(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            list2.enforceInterface("android.media.tv.ITvInputManager");
            str1 = list2.readString();
            k = getClientPid(str1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(k);
            return true;
          case 15:
            str1.enforceInterface("android.media.tv.ITvInputManager");
            iBinder1 = str1.readStrongBinder();
            k = str1.readInt();
            releaseSession(iBinder1, k);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str1.enforceInterface("android.media.tv.ITvInputManager");
            iTvInputClient = ITvInputClient.Stub.asInterface(str1.readStrongBinder());
            str3 = str1.readString();
            if (str1.readInt() != 0) {
              bool6 = true;
            } else {
              bool6 = false;
            } 
            param1Int2 = str1.readInt();
            k = str1.readInt();
            createSession(iTvInputClient, str3, bool6, param1Int2, k);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str1.enforceInterface("android.media.tv.ITvInputManager");
            str3 = str1.readString();
            k = str1.readInt();
            removeBlockedRating(str3, k);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            str1.enforceInterface("android.media.tv.ITvInputManager");
            str3 = str1.readString();
            k = str1.readInt();
            addBlockedRating(str3, k);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str1.enforceInterface("android.media.tv.ITvInputManager");
            k = str1.readInt();
            list1 = getBlockedRatings(k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeStringList(list1);
            return true;
          case 10:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            str3 = list1.readString();
            k = list1.readInt();
            bool2 = isRatingBlocked(str3, k);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            return true;
          case 9:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            if (list1.readInt() != 0)
              bool6 = true; 
            j = list1.readInt();
            setParentalControlsEnabled(bool6, j);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            j = list1.readInt();
            bool1 = isParentalControlsEnabled(j);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 7:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            iTvInputManagerCallback = ITvInputManagerCallback.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            unregisterCallback(iTvInputManagerCallback, i);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            iTvInputManagerCallback = ITvInputManagerCallback.Stub.asInterface(list1.readStrongBinder());
            i = list1.readInt();
            registerCallback(iTvInputManagerCallback, i);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            i = list1.readInt();
            list1 = (List)getTvContentRatingSystemList(i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 4:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            str2 = list1.readString();
            i = list1.readInt();
            i = getTvInputState(str2, i);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(i);
            return true;
          case 3:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            if (list1.readInt() != 0) {
              TvInputInfo tvInputInfo1 = TvInputInfo.CREATOR.createFromParcel((Parcel)list1);
            } else {
              str2 = null;
            } 
            i = list1.readInt();
            updateTvInputInfo((TvInputInfo)str2, i);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            list1.enforceInterface("android.media.tv.ITvInputManager");
            str2 = list1.readString();
            i = list1.readInt();
            tvInputInfo = getTvInputInfo(str2, i);
            param1Parcel2.writeNoException();
            if (tvInputInfo != null) {
              param1Parcel2.writeInt(1);
              tvInputInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 1:
            break;
        } 
        tvInputInfo.enforceInterface("android.media.tv.ITvInputManager");
        int i = tvInputInfo.readInt();
        List<TvInputInfo> list = getTvInputList(i);
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedList(list);
        return true;
      } 
      param1Parcel2.writeString("android.media.tv.ITvInputManager");
      return true;
    }
    
    private static class Proxy implements ITvInputManager {
      public static ITvInputManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputManager";
      }
      
      public List<TvInputInfo> getTvInputList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getTvInputList(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(TvInputInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public TvInputInfo getTvInputInfo(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getTvInputInfo(param2String, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            TvInputInfo tvInputInfo = TvInputInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String = null;
          } 
          return (TvInputInfo)param2String;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateTvInputInfo(TvInputInfo param2TvInputInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2TvInputInfo != null) {
            parcel1.writeInt(1);
            param2TvInputInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().updateTvInputInfo(param2TvInputInfo, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getTvInputState(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            param2Int = ITvInputManager.Stub.getDefaultImpl().getTvInputState(param2String, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<TvContentRatingSystemInfo> getTvContentRatingSystemList(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getTvContentRatingSystemList(param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(TvContentRatingSystemInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerCallback(ITvInputManagerCallback param2ITvInputManagerCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2ITvInputManagerCallback != null) {
            iBinder = param2ITvInputManagerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().registerCallback(param2ITvInputManagerCallback, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterCallback(ITvInputManagerCallback param2ITvInputManagerCallback, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2ITvInputManagerCallback != null) {
            iBinder = param2ITvInputManagerCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().unregisterCallback(param2ITvInputManagerCallback, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isParentalControlsEnabled(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(8, parcel1, parcel2, 0);
          if (!bool2 && ITvInputManager.Stub.getDefaultImpl() != null) {
            bool1 = ITvInputManager.Stub.getDefaultImpl().isParentalControlsEnabled(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setParentalControlsEnabled(boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool1 && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().setParentalControlsEnabled(param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isRatingBlocked(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(10, parcel1, parcel2, 0);
          if (!bool2 && ITvInputManager.Stub.getDefaultImpl() != null) {
            bool1 = ITvInputManager.Stub.getDefaultImpl().isRatingBlocked(param2String, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<String> getBlockedRatings(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getBlockedRatings(param2Int); 
          parcel2.readException();
          return parcel2.createStringArrayList();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addBlockedRating(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().addBlockedRating(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeBlockedRating(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().removeBlockedRating(param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createSession(ITvInputClient param2ITvInputClient, String param2String, boolean param2Boolean, int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2ITvInputClient != null) {
            iBinder = param2ITvInputClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool1 = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool1 && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().createSession(param2ITvInputClient, param2String, param2Boolean, param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseSession(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().releaseSession(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int getClientPid(String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getClientPid(param2String); 
          parcel2.readException();
          return parcel2.readInt();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setMainSession(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().setMainSession(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSurface(IBinder param2IBinder, Surface param2Surface, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Surface != null) {
            parcel1.writeInt(1);
            param2Surface.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().setSurface(param2IBinder, param2Surface, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void dispatchSurfaceChanged(IBinder param2IBinder, int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().dispatchSurfaceChanged(param2IBinder, param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setVolume(IBinder param2IBinder, float param2Float, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeFloat(param2Float);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().setVolume(param2IBinder, param2Float, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void tune(IBinder param2IBinder, Uri param2Uri, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().tune(param2IBinder, param2Uri, param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setCaptionEnabled(IBinder param2IBinder, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool1 && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().setCaptionEnabled(param2IBinder, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void selectTrack(IBinder param2IBinder, int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().selectTrack(param2IBinder, param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendAppPrivateCommand(IBinder param2IBinder, String param2String, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().sendAppPrivateCommand(param2IBinder, param2String, param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void createOverlayView(IBinder param2IBinder1, IBinder param2IBinder2, Rect param2Rect, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder1);
          parcel1.writeStrongBinder(param2IBinder2);
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().createOverlayView(param2IBinder1, param2IBinder2, param2Rect, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void relayoutOverlayView(IBinder param2IBinder, Rect param2Rect, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Rect != null) {
            parcel1.writeInt(1);
            param2Rect.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().relayoutOverlayView(param2IBinder, param2Rect, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeOverlayView(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().removeOverlayView(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unblockContent(IBinder param2IBinder, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().unblockContent(param2IBinder, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftPlay(IBinder param2IBinder, Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftPlay(param2IBinder, param2Uri, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftPause(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftPause(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftResume(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftResume(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftSeekTo(IBinder param2IBinder, long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(32, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftSeekTo(param2IBinder, param2Long, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftSetPlaybackParams(IBinder param2IBinder, PlaybackParams param2PlaybackParams, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2PlaybackParams != null) {
            parcel1.writeInt(1);
            param2PlaybackParams.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(33, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftSetPlaybackParams(param2IBinder, param2PlaybackParams, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void timeShiftEnablePositionTracking(IBinder param2IBinder, boolean param2Boolean, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          boolean bool;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          parcel1.writeInt(param2Int);
          boolean bool1 = this.mRemote.transact(34, parcel1, parcel2, 0);
          if (!bool1 && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().timeShiftEnablePositionTracking(param2IBinder, param2Boolean, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void startRecording(IBinder param2IBinder, Uri param2Uri, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel1.writeInt(1);
            param2Bundle.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(35, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().startRecording(param2IBinder, param2Uri, param2Bundle, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void stopRecording(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(36, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().stopRecording(param2IBinder, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<TvInputHardwareInfo> getHardwareList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          boolean bool = this.mRemote.transact(37, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getHardwareList(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(TvInputHardwareInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ITvInputHardware acquireTvInputHardware(int param2Int1, ITvInputHardwareCallback param2ITvInputHardwareCallback, TvInputInfo param2TvInputInfo, int param2Int2, String param2String, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          try {
            IBinder iBinder;
            parcel1.writeInt(param2Int1);
            if (param2ITvInputHardwareCallback != null) {
              iBinder = param2ITvInputHardwareCallback.asBinder();
            } else {
              iBinder = null;
            } 
            parcel1.writeStrongBinder(iBinder);
            if (param2TvInputInfo != null) {
              parcel1.writeInt(1);
              param2TvInputInfo.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeString(param2String);
                try {
                  parcel1.writeInt(param2Int3);
                  try {
                    boolean bool = this.mRemote.transact(38, parcel1, parcel2, 0);
                    if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
                      ITvInputHardware iTvInputHardware1 = ITvInputManager.Stub.getDefaultImpl().acquireTvInputHardware(param2Int1, param2ITvInputHardwareCallback, param2TvInputInfo, param2Int2, param2String, param2Int3);
                      parcel2.recycle();
                      parcel1.recycle();
                      return iTvInputHardware1;
                    } 
                    parcel2.readException();
                    ITvInputHardware iTvInputHardware = ITvInputHardware.Stub.asInterface(parcel2.readStrongBinder());
                    parcel2.recycle();
                    parcel1.recycle();
                    return iTvInputHardware;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2ITvInputHardwareCallback;
      }
      
      public void releaseTvInputHardware(int param2Int1, ITvInputHardware param2ITvInputHardware, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int1);
          if (param2ITvInputHardware != null) {
            iBinder = param2ITvInputHardware.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(39, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().releaseTvInputHardware(param2Int1, param2ITvInputHardware, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<TvStreamConfig> getAvailableTvStreamConfigList(String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(40, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getAvailableTvStreamConfigList(param2String, param2Int); 
          parcel2.readException();
          return parcel2.createTypedArrayList(TvStreamConfig.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean captureFrame(String param2String, Surface param2Surface, TvStreamConfig param2TvStreamConfig, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeString(param2String);
          boolean bool1 = true;
          if (param2Surface != null) {
            parcel1.writeInt(1);
            param2Surface.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2TvStreamConfig != null) {
            parcel1.writeInt(1);
            param2TvStreamConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool2 = this.mRemote.transact(41, parcel1, parcel2, 0);
          if (!bool2 && ITvInputManager.Stub.getDefaultImpl() != null) {
            bool1 = ITvInputManager.Stub.getDefaultImpl().captureFrame(param2String, param2Surface, param2TvStreamConfig, param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isSingleSessionActive(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(42, parcel1, parcel2, 0);
          if (!bool2 && ITvInputManager.Stub.getDefaultImpl() != null) {
            bool1 = ITvInputManager.Stub.getDefaultImpl().isSingleSessionActive(param2Int);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<DvbDeviceInfo> getDvbDeviceList() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          boolean bool = this.mRemote.transact(43, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().getDvbDeviceList(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(DvbDeviceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public ParcelFileDescriptor openDvbDevice(DvbDeviceInfo param2DvbDeviceInfo, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2DvbDeviceInfo != null) {
            parcel1.writeInt(1);
            param2DvbDeviceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(44, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null)
            return ITvInputManager.Stub.getDefaultImpl().openDvbDevice(param2DvbDeviceInfo, param2Int); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.CREATOR.createFromParcel(parcel2);
          } else {
            param2DvbDeviceInfo = null;
          } 
          return (ParcelFileDescriptor)param2DvbDeviceInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void sendTvInputNotifyIntent(Intent param2Intent, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2Intent != null) {
            parcel1.writeInt(1);
            param2Intent.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(45, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().sendTvInputNotifyIntent(param2Intent, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestChannelBrowsable(Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          if (param2Uri != null) {
            parcel1.writeInt(1);
            param2Uri.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(46, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().requestChannelBrowsable(param2Uri, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addHardwareDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(47, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().addHardwareDevice(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeHardwareDevice(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(48, parcel1, parcel2, 0);
          if (!bool && ITvInputManager.Stub.getDefaultImpl() != null) {
            ITvInputManager.Stub.getDefaultImpl().removeHardwareDevice(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputManager param1ITvInputManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputManager != null) {
          Proxy.sDefaultImpl = param1ITvInputManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
