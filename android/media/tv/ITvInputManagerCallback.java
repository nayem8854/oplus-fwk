package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITvInputManagerCallback extends IInterface {
  void onInputAdded(String paramString) throws RemoteException;
  
  void onInputRemoved(String paramString) throws RemoteException;
  
  void onInputStateChanged(String paramString, int paramInt) throws RemoteException;
  
  void onInputUpdated(String paramString) throws RemoteException;
  
  void onTvInputInfoUpdated(TvInputInfo paramTvInputInfo) throws RemoteException;
  
  class Default implements ITvInputManagerCallback {
    public void onInputAdded(String param1String) throws RemoteException {}
    
    public void onInputRemoved(String param1String) throws RemoteException {}
    
    public void onInputUpdated(String param1String) throws RemoteException {}
    
    public void onInputStateChanged(String param1String, int param1Int) throws RemoteException {}
    
    public void onTvInputInfoUpdated(TvInputInfo param1TvInputInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputManagerCallback {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputManagerCallback";
    
    static final int TRANSACTION_onInputAdded = 1;
    
    static final int TRANSACTION_onInputRemoved = 2;
    
    static final int TRANSACTION_onInputStateChanged = 4;
    
    static final int TRANSACTION_onInputUpdated = 3;
    
    static final int TRANSACTION_onTvInputInfoUpdated = 5;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputManagerCallback");
    }
    
    public static ITvInputManagerCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputManagerCallback");
      if (iInterface != null && iInterface instanceof ITvInputManagerCallback)
        return (ITvInputManagerCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "onTvInputInfoUpdated";
            } 
            return "onInputStateChanged";
          } 
          return "onInputUpdated";
        } 
        return "onInputRemoved";
      } 
      return "onInputAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.media.tv.ITvInputManagerCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.media.tv.ITvInputManagerCallback");
              if (param1Parcel1.readInt() != 0) {
                TvInputInfo tvInputInfo = TvInputInfo.CREATOR.createFromParcel(param1Parcel1);
              } else {
                param1Parcel1 = null;
              } 
              onTvInputInfoUpdated((TvInputInfo)param1Parcel1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.media.tv.ITvInputManagerCallback");
            String str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            onInputStateChanged(str1, param1Int1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.tv.ITvInputManagerCallback");
          str = param1Parcel1.readString();
          onInputUpdated(str);
          return true;
        } 
        str.enforceInterface("android.media.tv.ITvInputManagerCallback");
        str = str.readString();
        onInputRemoved(str);
        return true;
      } 
      str.enforceInterface("android.media.tv.ITvInputManagerCallback");
      String str = str.readString();
      onInputAdded(str);
      return true;
    }
    
    private static class Proxy implements ITvInputManagerCallback {
      public static ITvInputManagerCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputManagerCallback";
      }
      
      public void onInputAdded(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputManagerCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputManagerCallback.Stub.getDefaultImpl() != null) {
            ITvInputManagerCallback.Stub.getDefaultImpl().onInputAdded(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputRemoved(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputManagerCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputManagerCallback.Stub.getDefaultImpl() != null) {
            ITvInputManagerCallback.Stub.getDefaultImpl().onInputRemoved(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputUpdated(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputManagerCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputManagerCallback.Stub.getDefaultImpl() != null) {
            ITvInputManagerCallback.Stub.getDefaultImpl().onInputUpdated(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputStateChanged(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputManagerCallback");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvInputManagerCallback.Stub.getDefaultImpl() != null) {
            ITvInputManagerCallback.Stub.getDefaultImpl().onInputStateChanged(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTvInputInfoUpdated(TvInputInfo param2TvInputInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputManagerCallback");
          if (param2TvInputInfo != null) {
            parcel.writeInt(1);
            param2TvInputInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvInputManagerCallback.Stub.getDefaultImpl() != null) {
            ITvInputManagerCallback.Stub.getDefaultImpl().onTvInputInfoUpdated(param2TvInputInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputManagerCallback param1ITvInputManagerCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputManagerCallback != null) {
          Proxy.sDefaultImpl = param1ITvInputManagerCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputManagerCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
