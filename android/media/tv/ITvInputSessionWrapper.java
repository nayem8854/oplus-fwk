package android.media.tv;

import android.content.Context;
import android.graphics.Rect;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.Surface;
import com.android.internal.os.HandlerCaller;
import com.android.internal.os.SomeArgs;

public class ITvInputSessionWrapper extends ITvInputSession.Stub implements HandlerCaller.Callback {
  private static final int DO_APP_PRIVATE_COMMAND = 9;
  
  private static final int DO_CREATE_OVERLAY_VIEW = 10;
  
  private static final int DO_DISPATCH_SURFACE_CHANGED = 4;
  
  private static final int DO_RELAYOUT_OVERLAY_VIEW = 11;
  
  private static final int DO_RELEASE = 1;
  
  private static final int DO_REMOVE_OVERLAY_VIEW = 12;
  
  private static final int DO_SELECT_TRACK = 8;
  
  private static final int DO_SET_CAPTION_ENABLED = 7;
  
  private static final int DO_SET_MAIN = 2;
  
  private static final int DO_SET_STREAM_VOLUME = 5;
  
  private static final int DO_SET_SURFACE = 3;
  
  private static final int DO_START_RECORDING = 20;
  
  private static final int DO_STOP_RECORDING = 21;
  
  private static final int DO_TIME_SHIFT_ENABLE_POSITION_TRACKING = 19;
  
  private static final int DO_TIME_SHIFT_PAUSE = 15;
  
  private static final int DO_TIME_SHIFT_PLAY = 14;
  
  private static final int DO_TIME_SHIFT_RESUME = 16;
  
  private static final int DO_TIME_SHIFT_SEEK_TO = 17;
  
  private static final int DO_TIME_SHIFT_SET_PLAYBACK_PARAMS = 18;
  
  private static final int DO_TUNE = 6;
  
  private static final int DO_UNBLOCK_CONTENT = 13;
  
  private static final int EXECUTE_MESSAGE_TIMEOUT_LONG_MILLIS = 5000;
  
  private static final int EXECUTE_MESSAGE_TIMEOUT_SHORT_MILLIS = 50;
  
  private static final int EXECUTE_MESSAGE_TUNE_TIMEOUT_MILLIS = 2000;
  
  private static final String TAG = "TvInputSessionWrapper";
  
  private final HandlerCaller mCaller;
  
  private InputChannel mChannel;
  
  private final boolean mIsRecordingSession;
  
  private TvInputEventReceiver mReceiver;
  
  private TvInputService.RecordingSession mTvInputRecordingSessionImpl;
  
  private TvInputService.Session mTvInputSessionImpl;
  
  public ITvInputSessionWrapper(Context paramContext, TvInputService.Session paramSession, InputChannel paramInputChannel) {
    this.mIsRecordingSession = false;
    this.mCaller = new HandlerCaller(paramContext, null, this, true);
    this.mTvInputSessionImpl = paramSession;
    this.mChannel = paramInputChannel;
    if (paramInputChannel != null)
      this.mReceiver = new TvInputEventReceiver(paramInputChannel, paramContext.getMainLooper()); 
  }
  
  public ITvInputSessionWrapper(Context paramContext, TvInputService.RecordingSession paramRecordingSession) {
    this.mIsRecordingSession = true;
    this.mCaller = new HandlerCaller(paramContext, null, this, true);
    this.mTvInputRecordingSessionImpl = paramRecordingSession;
  }
  
  public void executeMessage(Message paramMessage) {
    StringBuilder stringBuilder;
    SomeArgs someArgs;
    TvInputEventReceiver tvInputEventReceiver;
    InputChannel inputChannel;
    if ((this.mIsRecordingSession && this.mTvInputRecordingSessionImpl == null) || (!this.mIsRecordingSession && this.mTvInputSessionImpl == null))
      return; 
    long l = System.nanoTime();
    switch (paramMessage.what) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unhandled message code: ");
        stringBuilder.append(paramMessage.what);
        Log.w("TvInputSessionWrapper", stringBuilder.toString());
        break;
      case 21:
        this.mTvInputRecordingSessionImpl.stopRecording();
        break;
      case 20:
        someArgs = (SomeArgs)paramMessage.obj;
        this.mTvInputRecordingSessionImpl.startRecording((Uri)someArgs.arg1, (Bundle)someArgs.arg2);
        break;
      case 19:
        this.mTvInputSessionImpl.timeShiftEnablePositionTracking(((Boolean)paramMessage.obj).booleanValue());
        break;
      case 18:
        this.mTvInputSessionImpl.timeShiftSetPlaybackParams((PlaybackParams)paramMessage.obj);
        break;
      case 17:
        this.mTvInputSessionImpl.timeShiftSeekTo(((Long)paramMessage.obj).longValue());
        break;
      case 16:
        this.mTvInputSessionImpl.timeShiftResume();
        break;
      case 15:
        this.mTvInputSessionImpl.timeShiftPause();
        break;
      case 14:
        this.mTvInputSessionImpl.timeShiftPlay((Uri)paramMessage.obj);
        break;
      case 13:
        this.mTvInputSessionImpl.unblockContent((String)paramMessage.obj);
        break;
      case 12:
        this.mTvInputSessionImpl.removeOverlayView(true);
        break;
      case 11:
        this.mTvInputSessionImpl.relayoutOverlayView((Rect)paramMessage.obj);
        break;
      case 10:
        someArgs = (SomeArgs)paramMessage.obj;
        this.mTvInputSessionImpl.createOverlayView((IBinder)someArgs.arg1, (Rect)someArgs.arg2);
        someArgs.recycle();
        break;
      case 9:
        someArgs = (SomeArgs)paramMessage.obj;
        if (this.mIsRecordingSession) {
          this.mTvInputRecordingSessionImpl.appPrivateCommand((String)someArgs.arg1, (Bundle)someArgs.arg2);
        } else {
          this.mTvInputSessionImpl.appPrivateCommand((String)someArgs.arg1, (Bundle)someArgs.arg2);
        } 
        someArgs.recycle();
        break;
      case 8:
        someArgs = (SomeArgs)paramMessage.obj;
        this.mTvInputSessionImpl.selectTrack(((Integer)someArgs.arg1).intValue(), (String)someArgs.arg2);
        someArgs.recycle();
        break;
      case 7:
        this.mTvInputSessionImpl.setCaptionEnabled(((Boolean)paramMessage.obj).booleanValue());
        break;
      case 6:
        someArgs = (SomeArgs)paramMessage.obj;
        if (this.mIsRecordingSession) {
          this.mTvInputRecordingSessionImpl.tune((Uri)someArgs.arg1, (Bundle)someArgs.arg2);
        } else {
          this.mTvInputSessionImpl.tune((Uri)someArgs.arg1, (Bundle)someArgs.arg2);
        } 
        someArgs.recycle();
        break;
      case 5:
        this.mTvInputSessionImpl.setStreamVolume(((Float)paramMessage.obj).floatValue());
        break;
      case 4:
        someArgs = (SomeArgs)paramMessage.obj;
        this.mTvInputSessionImpl.dispatchSurfaceChanged(someArgs.argi1, someArgs.argi2, someArgs.argi3);
        someArgs.recycle();
        break;
      case 3:
        this.mTvInputSessionImpl.setSurface((Surface)paramMessage.obj);
        break;
      case 2:
        this.mTvInputSessionImpl.setMain(((Boolean)paramMessage.obj).booleanValue());
        break;
      case 1:
        if (this.mIsRecordingSession) {
          this.mTvInputRecordingSessionImpl.release();
          this.mTvInputRecordingSessionImpl = null;
          break;
        } 
        this.mTvInputSessionImpl.release();
        this.mTvInputSessionImpl = null;
        tvInputEventReceiver = this.mReceiver;
        if (tvInputEventReceiver != null) {
          tvInputEventReceiver.dispose();
          this.mReceiver = null;
        } 
        inputChannel = this.mChannel;
        if (inputChannel != null) {
          inputChannel.dispose();
          this.mChannel = null;
        } 
        break;
    } 
    l = (System.nanoTime() - l) / 1000000L;
    if (l > 50L) {
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Handling message (");
      stringBuilder2.append(paramMessage.what);
      stringBuilder2.append(") took too long time (duration=");
      stringBuilder2.append(l);
      stringBuilder2.append("ms)");
      Log.w("TvInputSessionWrapper", stringBuilder2.toString());
      if (paramMessage.what != 6 || l <= 2000L) {
        if (l > 5000L) {
          stringBuilder2 = new StringBuilder();
          stringBuilder2.append("Too much time to handle a request. (type=");
          stringBuilder2.append(paramMessage.what);
          stringBuilder2.append(", ");
          stringBuilder2.append(l);
          stringBuilder2.append("ms > ");
          stringBuilder2.append(5000);
          stringBuilder2.append("ms).");
          throw new RuntimeException(stringBuilder2.toString());
        } 
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Too much time to handle tune request. (");
      stringBuilder1.append(l);
      stringBuilder1.append("ms > ");
      stringBuilder1.append(2000);
      stringBuilder1.append("ms) Consider handling the tune request in a separate thread.");
      throw new RuntimeException(stringBuilder1.toString());
    } 
  }
  
  public void release() {
    if (!this.mIsRecordingSession)
      this.mTvInputSessionImpl.scheduleOverlayViewCleanup(); 
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(1));
  }
  
  public void setMain(boolean paramBoolean) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(2, Boolean.valueOf(paramBoolean)));
  }
  
  public void setSurface(Surface paramSurface) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(3, paramSurface));
  }
  
  public void dispatchSurfaceChanged(int paramInt1, int paramInt2, int paramInt3) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageIIII(4, paramInt1, paramInt2, paramInt3, 0));
  }
  
  public final void setVolume(float paramFloat) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(5, Float.valueOf(paramFloat)));
  }
  
  public void tune(Uri paramUri, Bundle paramBundle) {
    this.mCaller.removeMessages(6);
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(6, paramUri, paramBundle));
  }
  
  public void setCaptionEnabled(boolean paramBoolean) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(7, Boolean.valueOf(paramBoolean)));
  }
  
  public void selectTrack(int paramInt, String paramString) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(8, Integer.valueOf(paramInt), paramString));
  }
  
  public void appPrivateCommand(String paramString, Bundle paramBundle) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(9, paramString, paramBundle));
  }
  
  public void createOverlayView(IBinder paramIBinder, Rect paramRect) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(10, paramIBinder, paramRect));
  }
  
  public void relayoutOverlayView(Rect paramRect) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(11, paramRect));
  }
  
  public void removeOverlayView() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(12));
  }
  
  public void unblockContent(String paramString) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(13, paramString));
  }
  
  public void timeShiftPlay(Uri paramUri) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(14, paramUri));
  }
  
  public void timeShiftPause() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(15));
  }
  
  public void timeShiftResume() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(16));
  }
  
  public void timeShiftSeekTo(long paramLong) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(17, Long.valueOf(paramLong)));
  }
  
  public void timeShiftSetPlaybackParams(PlaybackParams paramPlaybackParams) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(18, paramPlaybackParams));
  }
  
  public void timeShiftEnablePositionTracking(boolean paramBoolean) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageO(19, Boolean.valueOf(paramBoolean)));
  }
  
  public void startRecording(Uri paramUri, Bundle paramBundle) {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessageOO(20, paramUri, paramBundle));
  }
  
  public void stopRecording() {
    HandlerCaller handlerCaller = this.mCaller;
    handlerCaller.executeOrSendMessage(handlerCaller.obtainMessage(21));
  }
  
  class TvInputEventReceiver extends InputEventReceiver {
    final ITvInputSessionWrapper this$0;
    
    public TvInputEventReceiver(InputChannel param1InputChannel, Looper param1Looper) {
      super(param1InputChannel, param1Looper);
    }
    
    public void onInputEvent(InputEvent param1InputEvent) {
      TvInputService.Session session = ITvInputSessionWrapper.this.mTvInputSessionImpl;
      boolean bool = false;
      if (session == null) {
        finishInputEvent(param1InputEvent, false);
        return;
      } 
      int i = ITvInputSessionWrapper.this.mTvInputSessionImpl.dispatchInputEvent(param1InputEvent, this);
      if (i != -1) {
        if (i == 1)
          bool = true; 
        finishInputEvent(param1InputEvent, bool);
      } 
    }
  }
}
