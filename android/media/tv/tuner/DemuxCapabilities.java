package android.media.tv.tuner;

import android.annotation.SystemApi;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class DemuxCapabilities {
  private final int mAudioFilterCount;
  
  private final int mDemuxCount;
  
  private final int mFilterCaps;
  
  private final int[] mLinkCaps;
  
  private final int mPcrFilterCount;
  
  private final int mPesFilterCount;
  
  private final int mPlaybackCount;
  
  private final int mRecordCount;
  
  private final int mSectionFilterCount;
  
  private final long mSectionFilterLength;
  
  private final boolean mSupportTimeFilter;
  
  private final int mTsFilterCount;
  
  private final int mVideoFilterCount;
  
  private DemuxCapabilities(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9, long paramLong, int paramInt10, int[] paramArrayOfint, boolean paramBoolean) {
    this.mDemuxCount = paramInt1;
    this.mRecordCount = paramInt2;
    this.mPlaybackCount = paramInt3;
    this.mTsFilterCount = paramInt4;
    this.mSectionFilterCount = paramInt5;
    this.mAudioFilterCount = paramInt6;
    this.mVideoFilterCount = paramInt7;
    this.mPesFilterCount = paramInt8;
    this.mPcrFilterCount = paramInt9;
    this.mSectionFilterLength = paramLong;
    this.mFilterCaps = paramInt10;
    this.mLinkCaps = paramArrayOfint;
    this.mSupportTimeFilter = paramBoolean;
  }
  
  public int getDemuxCount() {
    return this.mDemuxCount;
  }
  
  public int getRecordCount() {
    return this.mRecordCount;
  }
  
  public int getPlaybackCount() {
    return this.mPlaybackCount;
  }
  
  public int getTsFilterCount() {
    return this.mTsFilterCount;
  }
  
  public int getSectionFilterCount() {
    return this.mSectionFilterCount;
  }
  
  public int getAudioFilterCount() {
    return this.mAudioFilterCount;
  }
  
  public int getVideoFilterCount() {
    return this.mVideoFilterCount;
  }
  
  public int getPesFilterCount() {
    return this.mPesFilterCount;
  }
  
  public int getPcrFilterCount() {
    return this.mPcrFilterCount;
  }
  
  public long getSectionFilterLength() {
    return this.mSectionFilterLength;
  }
  
  public int getFilterCapabilities() {
    return this.mFilterCaps;
  }
  
  public int[] getLinkCapabilities() {
    return this.mLinkCaps;
  }
  
  public boolean isTimeFilterSupported() {
    return this.mSupportTimeFilter;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FilterCapabilities {}
}
