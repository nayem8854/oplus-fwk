package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class DvbsFrontendSettings extends FrontendSettings {
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_128APSK = 2048;
  
  public static final int MODULATION_MOD_16APSK = 256;
  
  public static final int MODULATION_MOD_16PSK = 16;
  
  public static final int MODULATION_MOD_16QAM = 8;
  
  public static final int MODULATION_MOD_256APSK = 4096;
  
  public static final int MODULATION_MOD_32APSK = 512;
  
  public static final int MODULATION_MOD_32PSK = 32;
  
  public static final int MODULATION_MOD_64APSK = 1024;
  
  public static final int MODULATION_MOD_8APSK = 128;
  
  public static final int MODULATION_MOD_8PSK = 4;
  
  public static final int MODULATION_MOD_ACM = 64;
  
  public static final int MODULATION_MOD_QPSK = 2;
  
  public static final int MODULATION_MOD_RESERVED = 8192;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  public static final int PILOT_AUTO = 3;
  
  public static final int PILOT_OFF = 2;
  
  public static final int PILOT_ON = 1;
  
  public static final int PILOT_UNDEFINED = 0;
  
  public static final int ROLLOFF_0_10 = 5;
  
  public static final int ROLLOFF_0_15 = 4;
  
  public static final int ROLLOFF_0_20 = 3;
  
  public static final int ROLLOFF_0_25 = 2;
  
  public static final int ROLLOFF_0_35 = 1;
  
  public static final int ROLLOFF_0_5 = 6;
  
  public static final int ROLLOFF_UNDEFINED = 0;
  
  public static final int STANDARD_AUTO = 1;
  
  public static final int STANDARD_S = 2;
  
  public static final int STANDARD_S2 = 4;
  
  public static final int STANDARD_S2X = 8;
  
  public static final int VCM_MODE_AUTO = 1;
  
  public static final int VCM_MODE_MANUAL = 2;
  
  public static final int VCM_MODE_UNDEFINED = 0;
  
  private final DvbsCodeRate mCodeRate;
  
  private final int mInputStreamId;
  
  private final int mModulation;
  
  private final int mPilot;
  
  private final int mRolloff;
  
  private final int mStandard;
  
  private final int mSymbolRate;
  
  private final int mVcmMode;
  
  private DvbsFrontendSettings(int paramInt1, int paramInt2, DvbsCodeRate paramDvbsCodeRate, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8) {
    super(paramInt1);
    this.mModulation = paramInt2;
    this.mCodeRate = paramDvbsCodeRate;
    this.mSymbolRate = paramInt3;
    this.mRolloff = paramInt4;
    this.mPilot = paramInt5;
    this.mInputStreamId = paramInt6;
    this.mStandard = paramInt7;
    this.mVcmMode = paramInt8;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public DvbsCodeRate getCodeRate() {
    return this.mCodeRate;
  }
  
  public int getSymbolRate() {
    return this.mSymbolRate;
  }
  
  public int getRolloff() {
    return this.mRolloff;
  }
  
  public int getPilot() {
    return this.mPilot;
  }
  
  public int getInputStreamId() {
    return this.mInputStreamId;
  }
  
  public int getStandard() {
    return this.mStandard;
  }
  
  public int getVcmMode() {
    return this.mVcmMode;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private int mFrequency = 0;
    
    private int mModulation = 0;
    
    private DvbsCodeRate mCodeRate = null;
    
    private int mSymbolRate = 0;
    
    private int mRolloff = 0;
    
    private int mPilot = 0;
    
    private int mInputStreamId = 65535;
    
    private int mStandard = 1;
    
    private int mVcmMode = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public Builder setCodeRate(DvbsCodeRate param1DvbsCodeRate) {
      this.mCodeRate = param1DvbsCodeRate;
      return this;
    }
    
    public Builder setSymbolRate(int param1Int) {
      this.mSymbolRate = param1Int;
      return this;
    }
    
    public Builder setRolloff(int param1Int) {
      this.mRolloff = param1Int;
      return this;
    }
    
    public Builder setPilot(int param1Int) {
      this.mPilot = param1Int;
      return this;
    }
    
    public Builder setInputStreamId(int param1Int) {
      this.mInputStreamId = param1Int;
      return this;
    }
    
    public Builder setStandard(int param1Int) {
      this.mStandard = param1Int;
      return this;
    }
    
    public Builder setVcmMode(int param1Int) {
      this.mVcmMode = param1Int;
      return this;
    }
    
    public DvbsFrontendSettings build() {
      return new DvbsFrontendSettings(this.mFrequency, this.mModulation, this.mCodeRate, this.mSymbolRate, this.mRolloff, this.mPilot, this.mInputStreamId, this.mStandard, this.mVcmMode);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 5;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Pilot implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Rolloff implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Standard implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class VcmMode implements Annotation {}
}
