package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public interface ScanCallback {
  void onAnalogSifStandardReported(int paramInt);
  
  void onAtsc3PlpInfosReported(Atsc3PlpInfo[] paramArrayOfAtsc3PlpInfo);
  
  void onDvbsStandardReported(int paramInt);
  
  void onDvbtStandardReported(int paramInt);
  
  void onFrequenciesReported(int[] paramArrayOfint);
  
  void onGroupIdsReported(int[] paramArrayOfint);
  
  void onHierarchyReported(int paramInt);
  
  void onInputStreamIdsReported(int[] paramArrayOfint);
  
  void onLocked();
  
  void onPlpIdsReported(int[] paramArrayOfint);
  
  void onProgress(int paramInt);
  
  void onScanStopped();
  
  void onSignalTypeReported(int paramInt);
  
  void onSymbolRatesReported(int[] paramArrayOfint);
}
