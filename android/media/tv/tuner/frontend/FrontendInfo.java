package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import android.util.Range;

@SystemApi
public class FrontendInfo {
  private final int mAcquireRange;
  
  private final int mExclusiveGroupId;
  
  private final Range<Integer> mFrequencyRange;
  
  private final FrontendCapabilities mFrontendCap;
  
  private final int mId;
  
  private final int[] mStatusCaps;
  
  private final Range<Integer> mSymbolRateRange;
  
  private final int mType;
  
  private FrontendInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int[] paramArrayOfint, FrontendCapabilities paramFrontendCapabilities) {
    this.mId = paramInt1;
    this.mType = paramInt2;
    this.mFrequencyRange = new Range(Integer.valueOf(paramInt3), Integer.valueOf(paramInt4));
    this.mSymbolRateRange = new Range(Integer.valueOf(paramInt5), Integer.valueOf(paramInt6));
    this.mAcquireRange = paramInt7;
    this.mExclusiveGroupId = paramInt8;
    this.mStatusCaps = paramArrayOfint;
    this.mFrontendCap = paramFrontendCapabilities;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public Range<Integer> getFrequencyRange() {
    return this.mFrequencyRange;
  }
  
  public Range<Integer> getSymbolRateRange() {
    return this.mSymbolRateRange;
  }
  
  public int getAcquireRange() {
    return this.mAcquireRange;
  }
  
  public int getExclusiveGroupId() {
    return this.mExclusiveGroupId;
  }
  
  public int[] getStatusCapabilities() {
    return this.mStatusCaps;
  }
  
  public FrontendCapabilities getFrontendCapabilities() {
    return this.mFrontendCap;
  }
}
