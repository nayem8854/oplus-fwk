package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class Atsc3PlpSettings {
  private final int mCodeRate;
  
  private final int mFec;
  
  private final int mInterleaveMode;
  
  private final int mModulation;
  
  private final int mPlpId;
  
  private Atsc3PlpSettings(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mPlpId = paramInt1;
    this.mModulation = paramInt2;
    this.mInterleaveMode = paramInt3;
    this.mCodeRate = paramInt4;
    this.mFec = paramInt5;
  }
  
  public int getPlpId() {
    return this.mPlpId;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public int getInterleaveMode() {
    return this.mInterleaveMode;
  }
  
  public int getCodeRate() {
    return this.mCodeRate;
  }
  
  public int getFec() {
    return this.mFec;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  public static class Builder {
    private int mCodeRate;
    
    private int mFec;
    
    private int mInterleaveMode;
    
    private int mModulation;
    
    private int mPlpId;
    
    private Builder() {}
    
    public Builder setPlpId(int param1Int) {
      this.mPlpId = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public Builder setInterleaveMode(int param1Int) {
      this.mInterleaveMode = param1Int;
      return this;
    }
    
    public Builder setCodeRate(int param1Int) {
      this.mCodeRate = param1Int;
      return this;
    }
    
    public Builder setFec(int param1Int) {
      this.mFec = param1Int;
      return this;
    }
    
    public Atsc3PlpSettings build() {
      return new Atsc3PlpSettings(this.mPlpId, this.mModulation, this.mInterleaveMode, this.mCodeRate, this.mFec);
    }
  }
}
