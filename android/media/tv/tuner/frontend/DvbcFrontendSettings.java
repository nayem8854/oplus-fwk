package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class DvbcFrontendSettings extends FrontendSettings {
  public static final int ANNEX_A = 1;
  
  public static final int ANNEX_B = 2;
  
  public static final int ANNEX_C = 4;
  
  public static final int ANNEX_UNDEFINED = 0;
  
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_128QAM = 16;
  
  public static final int MODULATION_MOD_16QAM = 2;
  
  public static final int MODULATION_MOD_256QAM = 32;
  
  public static final int MODULATION_MOD_32QAM = 4;
  
  public static final int MODULATION_MOD_64QAM = 8;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  public static final int OUTER_FEC_OUTER_FEC_NONE = 1;
  
  public static final int OUTER_FEC_OUTER_FEC_RS = 2;
  
  public static final int OUTER_FEC_UNDEFINED = 0;
  
  public static final int SPECTRAL_INVERSION_INVERTED = 2;
  
  public static final int SPECTRAL_INVERSION_NORMAL = 1;
  
  public static final int SPECTRAL_INVERSION_UNDEFINED = 0;
  
  private final int mAnnex;
  
  private final long mInnerFec;
  
  private final int mModulation;
  
  private final int mOuterFec;
  
  private final int mSpectralInversion;
  
  private final int mSymbolRate;
  
  private DvbcFrontendSettings(int paramInt1, int paramInt2, long paramLong, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    super(paramInt1);
    this.mModulation = paramInt2;
    this.mInnerFec = paramLong;
    this.mSymbolRate = paramInt3;
    this.mOuterFec = paramInt4;
    this.mAnnex = paramInt5;
    this.mSpectralInversion = paramInt6;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public long getInnerFec() {
    return this.mInnerFec;
  }
  
  public int getSymbolRate() {
    return this.mSymbolRate;
  }
  
  public int getOuterFec() {
    return this.mOuterFec;
  }
  
  public int getAnnex() {
    return this.mAnnex;
  }
  
  public int getSpectralInversion() {
    return this.mSpectralInversion;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Annex implements Annotation {}
  
  class Builder {
    private int mFrequency = 0;
    
    private int mModulation = 0;
    
    private long mInnerFec = 0L;
    
    private int mSymbolRate = 0;
    
    private int mOuterFec = 0;
    
    private int mAnnex = 0;
    
    private int mSpectralInversion = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public Builder setInnerFec(long param1Long) {
      this.mInnerFec = param1Long;
      return this;
    }
    
    public Builder setSymbolRate(int param1Int) {
      this.mSymbolRate = param1Int;
      return this;
    }
    
    public Builder setOuterFec(int param1Int) {
      this.mOuterFec = param1Int;
      return this;
    }
    
    public Builder setAnnex(int param1Int) {
      this.mAnnex = param1Int;
      return this;
    }
    
    public Builder setSpectralInversion(int param1Int) {
      this.mSpectralInversion = param1Int;
      return this;
    }
    
    public DvbcFrontendSettings build() {
      return new DvbcFrontendSettings(this.mFrequency, this.mModulation, this.mInnerFec, this.mSymbolRate, this.mOuterFec, this.mAnnex, this.mSpectralInversion);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 4;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class OuterFec implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SpectralInversion implements Annotation {}
}
