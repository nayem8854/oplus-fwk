package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class DvbcFrontendCapabilities extends FrontendCapabilities {
  private final int mAnnexCap;
  
  private final int mFecCap;
  
  private final int mModulationCap;
  
  private DvbcFrontendCapabilities(int paramInt1, int paramInt2, int paramInt3) {
    this.mModulationCap = paramInt1;
    this.mFecCap = paramInt2;
    this.mAnnexCap = paramInt3;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public int getFecCapability() {
    return this.mFecCap;
  }
  
  public int getAnnexCapability() {
    return this.mAnnexCap;
  }
}
