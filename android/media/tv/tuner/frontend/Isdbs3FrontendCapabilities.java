package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class Isdbs3FrontendCapabilities extends FrontendCapabilities {
  private final int mCodeRateCap;
  
  private final int mModulationCap;
  
  private Isdbs3FrontendCapabilities(int paramInt1, int paramInt2) {
    this.mModulationCap = paramInt1;
    this.mCodeRateCap = paramInt2;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public int getCodeRateCapability() {
    return this.mCodeRateCap;
  }
}
