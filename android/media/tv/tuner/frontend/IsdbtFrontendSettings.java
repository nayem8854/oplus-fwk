package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class IsdbtFrontendSettings extends FrontendSettings {
  public static final int BANDWIDTH_6MHZ = 8;
  
  public static final int BANDWIDTH_7MHZ = 4;
  
  public static final int BANDWIDTH_8MHZ = 2;
  
  public static final int BANDWIDTH_AUTO = 1;
  
  public static final int BANDWIDTH_UNDEFINED = 0;
  
  public static final int MODE_1 = 2;
  
  public static final int MODE_2 = 4;
  
  public static final int MODE_3 = 8;
  
  public static final int MODE_AUTO = 1;
  
  public static final int MODE_UNDEFINED = 0;
  
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_16QAM = 8;
  
  public static final int MODULATION_MOD_64QAM = 16;
  
  public static final int MODULATION_MOD_DQPSK = 2;
  
  public static final int MODULATION_MOD_QPSK = 4;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  private final int mBandwidth;
  
  private final int mCodeRate;
  
  private final int mGuardInterval;
  
  private final int mMode;
  
  private final int mModulation;
  
  private final int mServiceAreaId;
  
  private IsdbtFrontendSettings(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    super(paramInt1);
    this.mModulation = paramInt2;
    this.mBandwidth = paramInt3;
    this.mMode = paramInt4;
    this.mCodeRate = paramInt5;
    this.mGuardInterval = paramInt6;
    this.mServiceAreaId = paramInt7;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public int getBandwidth() {
    return this.mBandwidth;
  }
  
  public int getMode() {
    return this.mMode;
  }
  
  public int getCodeRate() {
    return this.mCodeRate;
  }
  
  public int getGuardInterval() {
    return this.mGuardInterval;
  }
  
  public int getServiceAreaId() {
    return this.mServiceAreaId;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Bandwidth implements Annotation {}
  
  class Builder {
    private int mFrequency = 0;
    
    private int mModulation = 0;
    
    private int mBandwidth = 0;
    
    private int mMode = 0;
    
    private int mCodeRate = 0;
    
    private int mGuardInterval = 0;
    
    private int mServiceAreaId = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public Builder setBandwidth(int param1Int) {
      this.mBandwidth = param1Int;
      return this;
    }
    
    public Builder setMode(int param1Int) {
      this.mMode = param1Int;
      return this;
    }
    
    public Builder setCodeRate(int param1Int) {
      this.mCodeRate = param1Int;
      return this;
    }
    
    public Builder setGuardInterval(int param1Int) {
      this.mGuardInterval = param1Int;
      return this;
    }
    
    public Builder setServiceAreaId(int param1Int) {
      this.mServiceAreaId = param1Int;
      return this;
    }
    
    public IsdbtFrontendSettings build() {
      return new IsdbtFrontendSettings(this.mFrequency, this.mModulation, this.mBandwidth, this.mMode, this.mCodeRate, this.mGuardInterval, this.mServiceAreaId);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 9;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Mode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
}
