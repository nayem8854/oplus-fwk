package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class IsdbsFrontendCapabilities extends FrontendCapabilities {
  private final int mCodeRateCap;
  
  private final int mModulationCap;
  
  private IsdbsFrontendCapabilities(int paramInt1, int paramInt2) {
    this.mModulationCap = paramInt1;
    this.mCodeRateCap = paramInt2;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public int getCodeRateCapability() {
    return this.mCodeRateCap;
  }
}
