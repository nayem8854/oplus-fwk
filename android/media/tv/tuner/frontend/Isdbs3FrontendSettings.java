package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class Isdbs3FrontendSettings extends FrontendSettings {
  public static final int CODERATE_1_2 = 8;
  
  public static final int CODERATE_1_3 = 2;
  
  public static final int CODERATE_2_3 = 32;
  
  public static final int CODERATE_2_5 = 4;
  
  public static final int CODERATE_3_4 = 64;
  
  public static final int CODERATE_3_5 = 16;
  
  public static final int CODERATE_4_5 = 256;
  
  public static final int CODERATE_5_6 = 512;
  
  public static final int CODERATE_7_8 = 1024;
  
  public static final int CODERATE_7_9 = 128;
  
  public static final int CODERATE_9_10 = 2048;
  
  public static final int CODERATE_AUTO = 1;
  
  public static final int CODERATE_UNDEFINED = 0;
  
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_16APSK = 16;
  
  public static final int MODULATION_MOD_32APSK = 32;
  
  public static final int MODULATION_MOD_8PSK = 8;
  
  public static final int MODULATION_MOD_BPSK = 2;
  
  public static final int MODULATION_MOD_QPSK = 4;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  public static final int ROLLOFF_0_03 = 1;
  
  public static final int ROLLOFF_UNDEFINED = 0;
  
  private final int mCodeRate;
  
  private final int mModulation;
  
  private final int mRolloff;
  
  private final int mStreamId;
  
  private final int mStreamIdType;
  
  private final int mSymbolRate;
  
  private Isdbs3FrontendSettings(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) {
    super(paramInt1);
    this.mStreamId = paramInt2;
    this.mStreamIdType = paramInt3;
    this.mModulation = paramInt4;
    this.mCodeRate = paramInt5;
    this.mSymbolRate = paramInt6;
    this.mRolloff = paramInt7;
  }
  
  public int getStreamId() {
    return this.mStreamId;
  }
  
  public int getStreamIdType() {
    return this.mStreamIdType;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public int getCodeRate() {
    return this.mCodeRate;
  }
  
  public int getSymbolRate() {
    return this.mSymbolRate;
  }
  
  public int getRolloff() {
    return this.mRolloff;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private int mFrequency = 0;
    
    private int mStreamId = 65535;
    
    private int mStreamIdType = 0;
    
    private int mModulation = 0;
    
    private int mCodeRate = 0;
    
    private int mSymbolRate = 0;
    
    private int mRolloff = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setStreamId(int param1Int) {
      this.mStreamId = param1Int;
      return this;
    }
    
    public Builder setStreamIdType(int param1Int) {
      this.mStreamIdType = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public Builder setCodeRate(int param1Int) {
      this.mCodeRate = param1Int;
      return this;
    }
    
    public Builder setSymbolRate(int param1Int) {
      this.mSymbolRate = param1Int;
      return this;
    }
    
    public Builder setRolloff(int param1Int) {
      this.mRolloff = param1Int;
      return this;
    }
    
    public Isdbs3FrontendSettings build() {
      return new Isdbs3FrontendSettings(this.mFrequency, this.mStreamId, this.mStreamIdType, this.mModulation, this.mCodeRate, this.mSymbolRate, this.mRolloff);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 8;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CodeRate implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Rolloff implements Annotation {}
}
