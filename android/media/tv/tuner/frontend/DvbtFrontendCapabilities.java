package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class DvbtFrontendCapabilities extends FrontendCapabilities {
  private final int mBandwidthCap;
  
  private final int mCodeRateCap;
  
  private final int mConstellationCap;
  
  private final int mGuardIntervalCap;
  
  private final int mHierarchyCap;
  
  private final boolean mIsMisoSupported;
  
  private final boolean mIsT2Supported;
  
  private final int mTransmissionModeCap;
  
  private DvbtFrontendCapabilities(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, boolean paramBoolean1, boolean paramBoolean2) {
    this.mTransmissionModeCap = paramInt1;
    this.mBandwidthCap = paramInt2;
    this.mConstellationCap = paramInt3;
    this.mCodeRateCap = paramInt4;
    this.mHierarchyCap = paramInt5;
    this.mGuardIntervalCap = paramInt6;
    this.mIsT2Supported = paramBoolean1;
    this.mIsMisoSupported = paramBoolean2;
  }
  
  public int getTransmissionModeCapability() {
    return this.mTransmissionModeCap;
  }
  
  public int getBandwidthCapability() {
    return this.mBandwidthCap;
  }
  
  public int getConstellationCapability() {
    return this.mConstellationCap;
  }
  
  public int getCodeRateCapability() {
    return this.mCodeRateCap;
  }
  
  public int getHierarchyCapability() {
    return this.mHierarchyCap;
  }
  
  public int getGuardIntervalCapability() {
    return this.mGuardIntervalCap;
  }
  
  public boolean isT2Supported() {
    return this.mIsT2Supported;
  }
  
  public boolean isMisoSupported() {
    return this.mIsMisoSupported;
  }
}
