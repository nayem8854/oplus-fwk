package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class Atsc3PlpInfo {
  private final boolean mLlsFlag;
  
  private final int mPlpId;
  
  private Atsc3PlpInfo(int paramInt, boolean paramBoolean) {
    this.mPlpId = paramInt;
    this.mLlsFlag = paramBoolean;
  }
  
  public int getPlpId() {
    return this.mPlpId;
  }
  
  public boolean getLlsFlag() {
    return this.mLlsFlag;
  }
}
