package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class FrontendStatus {
  public static final int FRONTEND_STATUS_TYPE_AGC = 14;
  
  public static final int FRONTEND_STATUS_TYPE_ATSC3_PLP_INFO = 21;
  
  public static final int FRONTEND_STATUS_TYPE_BER = 2;
  
  public static final int FRONTEND_STATUS_TYPE_DEMOD_LOCK = 0;
  
  public static final int FRONTEND_STATUS_TYPE_EWBS = 13;
  
  public static final int FRONTEND_STATUS_TYPE_FEC = 8;
  
  public static final int FRONTEND_STATUS_TYPE_FREQ_OFFSET = 18;
  
  public static final int FRONTEND_STATUS_TYPE_HIERARCHY = 19;
  
  public static final int FRONTEND_STATUS_TYPE_LAYER_ERROR = 16;
  
  public static final int FRONTEND_STATUS_TYPE_LNA = 15;
  
  public static final int FRONTEND_STATUS_TYPE_LNB_VOLTAGE = 11;
  
  public static final int FRONTEND_STATUS_TYPE_MER = 17;
  
  public static final int FRONTEND_STATUS_TYPE_MODULATION = 9;
  
  public static final int FRONTEND_STATUS_TYPE_PER = 3;
  
  public static final int FRONTEND_STATUS_TYPE_PLP_ID = 12;
  
  public static final int FRONTEND_STATUS_TYPE_PRE_BER = 4;
  
  public static final int FRONTEND_STATUS_TYPE_RF_LOCK = 20;
  
  public static final int FRONTEND_STATUS_TYPE_SIGNAL_QUALITY = 5;
  
  public static final int FRONTEND_STATUS_TYPE_SIGNAL_STRENGTH = 6;
  
  public static final int FRONTEND_STATUS_TYPE_SNR = 1;
  
  public static final int FRONTEND_STATUS_TYPE_SPECTRAL = 10;
  
  public static final int FRONTEND_STATUS_TYPE_SYMBOL_RATE = 7;
  
  private Integer mAgc;
  
  private Integer mBer;
  
  private Integer mFreqOffset;
  
  private Integer mHierarchy;
  
  private Long mInnerFec;
  
  private Integer mInversion;
  
  private Boolean mIsDemodLocked;
  
  private Boolean mIsEwbs;
  
  private boolean[] mIsLayerErrors;
  
  private Boolean mIsLnaOn;
  
  private Boolean mIsRfLocked;
  
  private Integer mLnbVoltage;
  
  private Integer mMer;
  
  private Integer mModulation;
  
  private Integer mPer;
  
  private Integer mPerBer;
  
  private Integer mPlpId;
  
  private Atsc3PlpTuningInfo[] mPlpInfo;
  
  private Integer mSignalQuality;
  
  private Integer mSignalStrength;
  
  private Integer mSnr;
  
  private Integer mSymbolRate;
  
  public boolean isDemodLocked() {
    Boolean bool = this.mIsDemodLocked;
    if (bool != null)
      return bool.booleanValue(); 
    throw new IllegalStateException();
  }
  
  public int getSnr() {
    Integer integer = this.mSnr;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getBer() {
    Integer integer = this.mBer;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getPer() {
    Integer integer = this.mPer;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getPerBer() {
    Integer integer = this.mPerBer;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getSignalQuality() {
    Integer integer = this.mSignalQuality;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getSignalStrength() {
    Integer integer = this.mSignalStrength;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getSymbolRate() {
    Integer integer = this.mSymbolRate;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public long getInnerFec() {
    Long long_ = this.mInnerFec;
    if (long_ != null)
      return long_.longValue(); 
    throw new IllegalStateException();
  }
  
  public int getModulation() {
    Integer integer = this.mModulation;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getSpectralInversion() {
    Integer integer = this.mInversion;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getLnbVoltage() {
    Integer integer = this.mLnbVoltage;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getPlpId() {
    Integer integer = this.mPlpId;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public boolean isEwbs() {
    Boolean bool = this.mIsEwbs;
    if (bool != null)
      return bool.booleanValue(); 
    throw new IllegalStateException();
  }
  
  public int getAgc() {
    Integer integer = this.mAgc;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public boolean isLnaOn() {
    Boolean bool = this.mIsLnaOn;
    if (bool != null)
      return bool.booleanValue(); 
    throw new IllegalStateException();
  }
  
  public boolean[] getLayerErrors() {
    boolean[] arrayOfBoolean = this.mIsLayerErrors;
    if (arrayOfBoolean != null)
      return arrayOfBoolean; 
    throw new IllegalStateException();
  }
  
  public int getMer() {
    Integer integer = this.mMer;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getFreqOffset() {
    Integer integer = this.mFreqOffset;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public int getHierarchy() {
    Integer integer = this.mHierarchy;
    if (integer != null)
      return integer.intValue(); 
    throw new IllegalStateException();
  }
  
  public boolean isRfLocked() {
    Boolean bool = this.mIsRfLocked;
    if (bool != null)
      return bool.booleanValue(); 
    throw new IllegalStateException();
  }
  
  public Atsc3PlpTuningInfo[] getAtsc3PlpTuningInfo() {
    Atsc3PlpTuningInfo[] arrayOfAtsc3PlpTuningInfo = this.mPlpInfo;
    if (arrayOfAtsc3PlpTuningInfo != null)
      return arrayOfAtsc3PlpTuningInfo; 
    throw new IllegalStateException();
  }
  
  public static class Atsc3PlpTuningInfo {
    private final boolean mIsLocked;
    
    private final int mPlpId;
    
    private final int mUec;
    
    private Atsc3PlpTuningInfo(int param1Int1, boolean param1Boolean, int param1Int2) {
      this.mPlpId = param1Int1;
      this.mIsLocked = param1Boolean;
      this.mUec = param1Int2;
    }
    
    public int getPlpId() {
      return this.mPlpId;
    }
    
    public boolean isLocked() {
      return this.mIsLocked;
    }
    
    public int getUec() {
      return this.mUec;
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FrontendModulation {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface FrontendStatusType {}
}
