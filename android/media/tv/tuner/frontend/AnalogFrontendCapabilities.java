package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class AnalogFrontendCapabilities extends FrontendCapabilities {
  private final int mSifStandardCap;
  
  private final int mTypeCap;
  
  private AnalogFrontendCapabilities(int paramInt1, int paramInt2) {
    this.mTypeCap = paramInt1;
    this.mSifStandardCap = paramInt2;
  }
  
  public int getSignalTypeCapability() {
    return this.mTypeCap;
  }
  
  public int getSifStandardCapability() {
    return this.mSifStandardCap;
  }
}
