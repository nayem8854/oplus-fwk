package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class Atsc3FrontendCapabilities extends FrontendCapabilities {
  private final int mBandwidthCap;
  
  private final int mCodeRateCap;
  
  private final int mDemodOutputFormatCap;
  
  private final int mFecCap;
  
  private final int mModulationCap;
  
  private final int mTimeInterleaveModeCap;
  
  private Atsc3FrontendCapabilities(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
    this.mBandwidthCap = paramInt1;
    this.mModulationCap = paramInt2;
    this.mTimeInterleaveModeCap = paramInt3;
    this.mCodeRateCap = paramInt4;
    this.mFecCap = paramInt5;
    this.mDemodOutputFormatCap = paramInt6;
  }
  
  public int getBandwidthCapability() {
    return this.mBandwidthCap;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public int getTimeInterleaveModeCapability() {
    return this.mTimeInterleaveModeCap;
  }
  
  public int getPlpCodeRateCapability() {
    return this.mCodeRateCap;
  }
  
  public int getFecCapability() {
    return this.mFecCap;
  }
  
  public int getDemodOutputFormatCapability() {
    return this.mDemodOutputFormatCap;
  }
}
