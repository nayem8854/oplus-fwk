package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class AnalogFrontendSettings extends FrontendSettings {
  public static final int SIF_AUTO = 1;
  
  public static final int SIF_BG = 2;
  
  public static final int SIF_BG_A2 = 4;
  
  public static final int SIF_BG_NICAM = 8;
  
  public static final int SIF_DK = 32;
  
  public static final int SIF_DK1_A2 = 64;
  
  public static final int SIF_DK2_A2 = 128;
  
  public static final int SIF_DK3_A2 = 256;
  
  public static final int SIF_DK_NICAM = 512;
  
  public static final int SIF_I = 16;
  
  public static final int SIF_I_NICAM = 32768;
  
  public static final int SIF_L = 1024;
  
  public static final int SIF_L_NICAM = 65536;
  
  public static final int SIF_L_PRIME = 131072;
  
  public static final int SIF_M = 2048;
  
  public static final int SIF_M_A2 = 8192;
  
  public static final int SIF_M_BTSC = 4096;
  
  public static final int SIF_M_EIAJ = 16384;
  
  public static final int SIF_UNDEFINED = 0;
  
  public static final int SIGNAL_TYPE_AUTO = 1;
  
  public static final int SIGNAL_TYPE_NTSC = 32;
  
  public static final int SIGNAL_TYPE_NTSC_443 = 64;
  
  public static final int SIGNAL_TYPE_PAL = 2;
  
  public static final int SIGNAL_TYPE_PAL_60 = 16;
  
  public static final int SIGNAL_TYPE_PAL_M = 4;
  
  public static final int SIGNAL_TYPE_PAL_N = 8;
  
  public static final int SIGNAL_TYPE_SECAM = 128;
  
  public static final int SIGNAL_TYPE_UNDEFINED = 0;
  
  private final int mSifStandard;
  
  private final int mSignalType;
  
  public int getType() {
    return 1;
  }
  
  public int getSignalType() {
    return this.mSignalType;
  }
  
  public int getSifStandard() {
    return this.mSifStandard;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  private AnalogFrontendSettings(int paramInt1, int paramInt2, int paramInt3) {
    super(paramInt1);
    this.mSignalType = paramInt2;
    this.mSifStandard = paramInt3;
  }
  
  class Builder {
    private int mFrequency = 0;
    
    private int mSignalType = 0;
    
    private int mSifStandard = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setSignalType(int param1Int) {
      this.mSignalType = param1Int;
      return this;
    }
    
    public Builder setSifStandard(int param1Int) {
      this.mSifStandard = param1Int;
      return this;
    }
    
    public AnalogFrontendSettings build() {
      return new AnalogFrontendSettings(this.mFrequency, this.mSignalType, this.mSifStandard);
    }
    
    private Builder() {}
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class SifStandard implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class SignalType implements Annotation {}
}
