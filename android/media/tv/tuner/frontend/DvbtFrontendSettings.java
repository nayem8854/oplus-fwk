package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class DvbtFrontendSettings extends FrontendSettings {
  public static final int BANDWIDTH_10MHZ = 64;
  
  public static final int BANDWIDTH_1_7MHZ = 32;
  
  public static final int BANDWIDTH_5MHZ = 16;
  
  public static final int BANDWIDTH_6MHZ = 8;
  
  public static final int BANDWIDTH_7MHZ = 4;
  
  public static final int BANDWIDTH_8MHZ = 2;
  
  public static final int BANDWIDTH_AUTO = 1;
  
  public static final int BANDWIDTH_UNDEFINED = 0;
  
  public static final int CODERATE_1_2 = 2;
  
  public static final int CODERATE_2_3 = 4;
  
  public static final int CODERATE_3_4 = 8;
  
  public static final int CODERATE_3_5 = 64;
  
  public static final int CODERATE_4_5 = 128;
  
  public static final int CODERATE_5_6 = 16;
  
  public static final int CODERATE_6_7 = 256;
  
  public static final int CODERATE_7_8 = 32;
  
  public static final int CODERATE_8_9 = 512;
  
  public static final int CODERATE_AUTO = 1;
  
  public static final int CODERATE_UNDEFINED = 0;
  
  public static final int CONSTELLATION_16QAM = 4;
  
  public static final int CONSTELLATION_256QAM = 16;
  
  public static final int CONSTELLATION_64QAM = 8;
  
  public static final int CONSTELLATION_AUTO = 1;
  
  public static final int CONSTELLATION_QPSK = 2;
  
  public static final int CONSTELLATION_UNDEFINED = 0;
  
  public static final int GUARD_INTERVAL_19_128 = 64;
  
  public static final int GUARD_INTERVAL_19_256 = 128;
  
  public static final int GUARD_INTERVAL_1_128 = 32;
  
  public static final int GUARD_INTERVAL_1_16 = 4;
  
  public static final int GUARD_INTERVAL_1_32 = 2;
  
  public static final int GUARD_INTERVAL_1_4 = 16;
  
  public static final int GUARD_INTERVAL_1_8 = 8;
  
  public static final int GUARD_INTERVAL_AUTO = 1;
  
  public static final int GUARD_INTERVAL_UNDEFINED = 0;
  
  public static final int HIERARCHY_1_INDEPTH = 64;
  
  public static final int HIERARCHY_1_NATIVE = 4;
  
  public static final int HIERARCHY_2_INDEPTH = 128;
  
  public static final int HIERARCHY_2_NATIVE = 8;
  
  public static final int HIERARCHY_4_INDEPTH = 256;
  
  public static final int HIERARCHY_4_NATIVE = 16;
  
  public static final int HIERARCHY_AUTO = 1;
  
  public static final int HIERARCHY_NON_INDEPTH = 32;
  
  public static final int HIERARCHY_NON_NATIVE = 2;
  
  public static final int HIERARCHY_UNDEFINED = 0;
  
  public static final int PLP_MODE_AUTO = 1;
  
  public static final int PLP_MODE_MANUAL = 2;
  
  public static final int PLP_MODE_UNDEFINED = 0;
  
  public static final int STANDARD_AUTO = 1;
  
  public static final int STANDARD_T = 2;
  
  public static final int STANDARD_T2 = 4;
  
  public static final int TRANSMISSION_MODE_16K = 32;
  
  public static final int TRANSMISSION_MODE_1K = 16;
  
  public static final int TRANSMISSION_MODE_2K = 2;
  
  public static final int TRANSMISSION_MODE_32K = 64;
  
  public static final int TRANSMISSION_MODE_4K = 8;
  
  public static final int TRANSMISSION_MODE_8K = 4;
  
  public static final int TRANSMISSION_MODE_AUTO = 1;
  
  public static final int TRANSMISSION_MODE_UNDEFINED = 0;
  
  private final int mBandwidth;
  
  private final int mConstellation;
  
  private final int mGuardInterval;
  
  private final int mHierarchy;
  
  private final int mHpCodeRate;
  
  private final boolean mIsHighPriority;
  
  private final boolean mIsMiso;
  
  private final int mLpCodeRate;
  
  private final int mPlpGroupId;
  
  private final int mPlpId;
  
  private final int mPlpMode;
  
  private final int mStandard;
  
  private final int mTransmissionMode;
  
  private DvbtFrontendSettings(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, boolean paramBoolean1, int paramInt9, boolean paramBoolean2, int paramInt10, int paramInt11, int paramInt12) {
    super(paramInt1);
    this.mTransmissionMode = paramInt2;
    this.mBandwidth = paramInt3;
    this.mConstellation = paramInt4;
    this.mHierarchy = paramInt5;
    this.mHpCodeRate = paramInt6;
    this.mLpCodeRate = paramInt7;
    this.mGuardInterval = paramInt8;
    this.mIsHighPriority = paramBoolean1;
    this.mStandard = paramInt9;
    this.mIsMiso = paramBoolean2;
    this.mPlpMode = paramInt10;
    this.mPlpId = paramInt11;
    this.mPlpGroupId = paramInt12;
  }
  
  public int getTransmissionMode() {
    return this.mTransmissionMode;
  }
  
  public int getBandwidth() {
    return this.mBandwidth;
  }
  
  public int getConstellation() {
    return this.mConstellation;
  }
  
  public int getHierarchy() {
    return this.mHierarchy;
  }
  
  public int getHighPriorityCodeRate() {
    return this.mHpCodeRate;
  }
  
  public int getLowPriorityCodeRate() {
    return this.mLpCodeRate;
  }
  
  public int getGuardInterval() {
    return this.mGuardInterval;
  }
  
  public boolean isHighPriority() {
    return this.mIsHighPriority;
  }
  
  public int getStandard() {
    return this.mStandard;
  }
  
  public boolean isMiso() {
    return this.mIsMiso;
  }
  
  public int getPlpMode() {
    return this.mPlpMode;
  }
  
  public int getPlpId() {
    return this.mPlpId;
  }
  
  public int getPlpGroupId() {
    return this.mPlpGroupId;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Bandwidth implements Annotation {}
  
  class Builder {
    private int mFrequency = 0;
    
    private int mTransmissionMode = 0;
    
    private int mBandwidth = 0;
    
    private int mConstellation = 0;
    
    private int mHierarchy = 0;
    
    private int mHpCodeRate = 0;
    
    private int mLpCodeRate = 0;
    
    private int mGuardInterval = 0;
    
    private boolean mIsHighPriority = false;
    
    private int mStandard = 1;
    
    private boolean mIsMiso = false;
    
    private int mPlpMode = 0;
    
    private int mPlpId = 0;
    
    private int mPlpGroupId = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setTransmissionMode(int param1Int) {
      this.mTransmissionMode = param1Int;
      return this;
    }
    
    public Builder setBandwidth(int param1Int) {
      this.mBandwidth = param1Int;
      return this;
    }
    
    public Builder setConstellation(int param1Int) {
      this.mConstellation = param1Int;
      return this;
    }
    
    public Builder setHierarchy(int param1Int) {
      this.mHierarchy = param1Int;
      return this;
    }
    
    public Builder setHighPriorityCodeRate(int param1Int) {
      this.mHpCodeRate = param1Int;
      return this;
    }
    
    public Builder setLowPriorityCodeRate(int param1Int) {
      this.mLpCodeRate = param1Int;
      return this;
    }
    
    public Builder setGuardInterval(int param1Int) {
      this.mGuardInterval = param1Int;
      return this;
    }
    
    public Builder setHighPriority(boolean param1Boolean) {
      this.mIsHighPriority = param1Boolean;
      return this;
    }
    
    public Builder setStandard(int param1Int) {
      this.mStandard = param1Int;
      return this;
    }
    
    public Builder setMiso(boolean param1Boolean) {
      this.mIsMiso = param1Boolean;
      return this;
    }
    
    public Builder setPlpMode(int param1Int) {
      this.mPlpMode = param1Int;
      return this;
    }
    
    public Builder setPlpId(int param1Int) {
      this.mPlpId = param1Int;
      return this;
    }
    
    public Builder setPlpGroupId(int param1Int) {
      this.mPlpGroupId = param1Int;
      return this;
    }
    
    public DvbtFrontendSettings build() {
      return new DvbtFrontendSettings(this.mFrequency, this.mTransmissionMode, this.mBandwidth, this.mConstellation, this.mHierarchy, this.mHpCodeRate, this.mLpCodeRate, this.mGuardInterval, this.mIsHighPriority, this.mStandard, this.mIsMiso, this.mPlpMode, this.mPlpId, this.mPlpGroupId);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 6;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CodeRate implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Constellation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class GuardInterval implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Hierarchy implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class PlpMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Standard implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TransmissionMode implements Annotation {}
}
