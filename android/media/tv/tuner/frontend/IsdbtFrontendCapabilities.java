package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class IsdbtFrontendCapabilities extends FrontendCapabilities {
  private final int mBandwidthCap;
  
  private final int mCodeRateCap;
  
  private final int mGuardIntervalCap;
  
  private final int mModeCap;
  
  private final int mModulationCap;
  
  private IsdbtFrontendCapabilities(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mModeCap = paramInt1;
    this.mBandwidthCap = paramInt2;
    this.mModulationCap = paramInt3;
    this.mCodeRateCap = paramInt4;
    this.mGuardIntervalCap = paramInt5;
  }
  
  public int getModeCapability() {
    return this.mModeCap;
  }
  
  public int getBandwidthCapability() {
    return this.mBandwidthCap;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public int getCodeRateCapability() {
    return this.mCodeRateCap;
  }
  
  public int getGuardIntervalCapability() {
    return this.mGuardIntervalCap;
  }
}
