package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class DvbsCodeRate {
  private final int mBitsPer1000Symbol;
  
  private final long mInnerFec;
  
  private final boolean mIsLinear;
  
  private final boolean mIsShortFrames;
  
  private DvbsCodeRate(long paramLong, boolean paramBoolean1, boolean paramBoolean2, int paramInt) {
    this.mInnerFec = paramLong;
    this.mIsLinear = paramBoolean1;
    this.mIsShortFrames = paramBoolean2;
    this.mBitsPer1000Symbol = paramInt;
  }
  
  public long getInnerFec() {
    return this.mInnerFec;
  }
  
  public boolean isLinear() {
    return this.mIsLinear;
  }
  
  public boolean isShortFrameEnabled() {
    return this.mIsShortFrames;
  }
  
  public int getBitsPer1000Symbol() {
    return this.mBitsPer1000Symbol;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  public static class Builder {
    private int mBitsPer1000Symbol;
    
    private long mFec;
    
    private boolean mIsLinear;
    
    private boolean mIsShortFrames;
    
    private Builder() {}
    
    public Builder setInnerFec(long param1Long) {
      this.mFec = param1Long;
      return this;
    }
    
    public Builder setLinear(boolean param1Boolean) {
      this.mIsLinear = param1Boolean;
      return this;
    }
    
    public Builder setShortFrameEnabled(boolean param1Boolean) {
      this.mIsShortFrames = param1Boolean;
      return this;
    }
    
    public Builder setBitsPer1000Symbol(int param1Int) {
      this.mBitsPer1000Symbol = param1Int;
      return this;
    }
    
    public DvbsCodeRate build() {
      return new DvbsCodeRate(this.mFec, this.mIsLinear, this.mIsShortFrames, this.mBitsPer1000Symbol);
    }
  }
}
