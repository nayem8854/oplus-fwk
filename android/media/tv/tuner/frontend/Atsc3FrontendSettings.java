package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class Atsc3FrontendSettings extends FrontendSettings {
  public static final int BANDWIDTH_AUTO = 1;
  
  public static final int BANDWIDTH_BANDWIDTH_6MHZ = 2;
  
  public static final int BANDWIDTH_BANDWIDTH_7MHZ = 4;
  
  public static final int BANDWIDTH_BANDWIDTH_8MHZ = 8;
  
  public static final int BANDWIDTH_UNDEFINED = 0;
  
  public static final int CODERATE_10_15 = 512;
  
  public static final int CODERATE_11_15 = 1024;
  
  public static final int CODERATE_12_15 = 2048;
  
  public static final int CODERATE_13_15 = 4096;
  
  public static final int CODERATE_2_15 = 2;
  
  public static final int CODERATE_3_15 = 4;
  
  public static final int CODERATE_4_15 = 8;
  
  public static final int CODERATE_5_15 = 16;
  
  public static final int CODERATE_6_15 = 32;
  
  public static final int CODERATE_7_15 = 64;
  
  public static final int CODERATE_8_15 = 128;
  
  public static final int CODERATE_9_15 = 256;
  
  public static final int CODERATE_AUTO = 1;
  
  public static final int CODERATE_UNDEFINED = 0;
  
  public static final int DEMOD_OUTPUT_FORMAT_ATSC3_LINKLAYER_PACKET = 1;
  
  public static final int DEMOD_OUTPUT_FORMAT_BASEBAND_PACKET = 2;
  
  public static final int DEMOD_OUTPUT_FORMAT_UNDEFINED = 0;
  
  public static final int FEC_AUTO = 1;
  
  public static final int FEC_BCH_LDPC_16K = 2;
  
  public static final int FEC_BCH_LDPC_64K = 4;
  
  public static final int FEC_CRC_LDPC_16K = 8;
  
  public static final int FEC_CRC_LDPC_64K = 16;
  
  public static final int FEC_LDPC_16K = 32;
  
  public static final int FEC_LDPC_64K = 64;
  
  public static final int FEC_UNDEFINED = 0;
  
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_1024QAM = 32;
  
  public static final int MODULATION_MOD_16QAM = 4;
  
  public static final int MODULATION_MOD_256QAM = 16;
  
  public static final int MODULATION_MOD_4096QAM = 64;
  
  public static final int MODULATION_MOD_64QAM = 8;
  
  public static final int MODULATION_MOD_QPSK = 2;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  public static final int TIME_INTERLEAVE_MODE_AUTO = 1;
  
  public static final int TIME_INTERLEAVE_MODE_CTI = 2;
  
  public static final int TIME_INTERLEAVE_MODE_HTI = 4;
  
  public static final int TIME_INTERLEAVE_MODE_UNDEFINED = 0;
  
  private final int mBandwidth;
  
  private final int mDemodOutputFormat;
  
  private final Atsc3PlpSettings[] mPlpSettings;
  
  private Atsc3FrontendSettings(int paramInt1, int paramInt2, int paramInt3, Atsc3PlpSettings[] paramArrayOfAtsc3PlpSettings) {
    super(paramInt1);
    this.mBandwidth = paramInt2;
    this.mDemodOutputFormat = paramInt3;
    this.mPlpSettings = paramArrayOfAtsc3PlpSettings;
  }
  
  public int getBandwidth() {
    return this.mBandwidth;
  }
  
  public int getDemodOutputFormat() {
    return this.mDemodOutputFormat;
  }
  
  public Atsc3PlpSettings[] getPlpSettings() {
    return this.mPlpSettings;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Bandwidth implements Annotation {}
  
  class Builder {
    private int mFrequency = 0;
    
    private int mBandwidth = 0;
    
    private int mDemodOutputFormat = 0;
    
    private Atsc3PlpSettings[] mPlpSettings = new Atsc3PlpSettings[0];
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setBandwidth(int param1Int) {
      this.mBandwidth = param1Int;
      return this;
    }
    
    public Builder setDemodOutputFormat(int param1Int) {
      this.mDemodOutputFormat = param1Int;
      return this;
    }
    
    public Builder setPlpSettings(Atsc3PlpSettings[] param1ArrayOfAtsc3PlpSettings) {
      this.mPlpSettings = param1ArrayOfAtsc3PlpSettings;
      return this;
    }
    
    public Atsc3FrontendSettings build() {
      return new Atsc3FrontendSettings(this.mFrequency, this.mBandwidth, this.mDemodOutputFormat, this.mPlpSettings);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 3;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CodeRate implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class DemodOutputFormat implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Fec implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TimeInterleaveMode implements Annotation {}
}
