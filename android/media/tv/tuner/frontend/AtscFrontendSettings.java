package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class AtscFrontendSettings extends FrontendSettings {
  public static final int MODULATION_AUTO = 1;
  
  public static final int MODULATION_MOD_16VSB = 8;
  
  public static final int MODULATION_MOD_8VSB = 4;
  
  public static final int MODULATION_UNDEFINED = 0;
  
  private final int mModulation;
  
  private AtscFrontendSettings(int paramInt1, int paramInt2) {
    super(paramInt1);
    this.mModulation = paramInt2;
  }
  
  public int getModulation() {
    return this.mModulation;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private int mFrequency = 0;
    
    private int mModulation = 0;
    
    public Builder setFrequency(int param1Int) {
      this.mFrequency = param1Int;
      return this;
    }
    
    public Builder setModulation(int param1Int) {
      this.mModulation = param1Int;
      return this;
    }
    
    public AtscFrontendSettings build() {
      return new AtscFrontendSettings(this.mFrequency, this.mModulation);
    }
    
    private Builder() {}
  }
  
  public int getType() {
    return 2;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Modulation implements Annotation {}
}
