package android.media.tv.tuner.frontend;

import android.annotation.SystemApi;

@SystemApi
public class DvbsFrontendCapabilities extends FrontendCapabilities {
  private final long mInnerFecCap;
  
  private final int mModulationCap;
  
  private final int mStandard;
  
  private DvbsFrontendCapabilities(int paramInt1, long paramLong, int paramInt2) {
    this.mModulationCap = paramInt1;
    this.mInnerFecCap = paramLong;
    this.mStandard = paramInt2;
  }
  
  public int getModulationCapability() {
    return this.mModulationCap;
  }
  
  public long getInnerFecCapability() {
    return this.mInnerFecCap;
  }
  
  public int getStandardCapability() {
    return this.mStandard;
  }
}
