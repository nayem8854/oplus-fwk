package android.media.tv.tuner.dvr;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.media.tv.tuner.TunerUtils;
import android.media.tv.tuner.filter.Filter;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.android.internal.util.FrameworkStatsLog;
import java.util.concurrent.Executor;

@SystemApi
public class DvrRecorder implements AutoCloseable {
  private static final String TAG = "TvTunerRecord";
  
  private static int sInstantId = 0;
  
  private Executor mExecutor;
  
  private OnRecordStatusChangedListener mListener;
  
  private long mNativeContext;
  
  private int mOverflow;
  
  private int mSegmentId = 0;
  
  private int mUserId;
  
  private DvrRecorder() {
    this.mUserId = ActivityManager.getCurrentUser();
    int i = sInstantId;
    this.mSegmentId = (0xFFFF & i) << 16;
    sInstantId = i + 1;
  }
  
  public void setListener(Executor paramExecutor, OnRecordStatusChangedListener paramOnRecordStatusChangedListener) {
    this.mExecutor = paramExecutor;
    this.mListener = paramOnRecordStatusChangedListener;
  }
  
  private void onRecordStatusChanged(int paramInt) {
    if (paramInt == 8)
      this.mOverflow++; 
    Executor executor = this.mExecutor;
    if (executor != null && this.mListener != null)
      executor.execute(new _$$Lambda$DvrRecorder$52umHtUOwClDPwLK_2S_qZCPG6k(this, paramInt)); 
  }
  
  public int attachFilter(Filter paramFilter) {
    return nativeAttachFilter(paramFilter);
  }
  
  public int detachFilter(Filter paramFilter) {
    return nativeDetachFilter(paramFilter);
  }
  
  public int configure(DvrSettings paramDvrSettings) {
    return nativeConfigureDvr(paramDvrSettings);
  }
  
  public int start() {
    int i = this.mSegmentId;
    this.mSegmentId = (i & 0xFFFF) + 1 & 0xFFFF | 0xFFFF0000 & i;
    this.mOverflow = 0;
    Log.d("TvTunerRecord", "Write Stats Log for Record.");
    i = this.mUserId;
    int j = this.mSegmentId;
    FrameworkStatsLog.write(279, i, 2, 1, j, 0);
    return nativeStartDvr();
  }
  
  public int stop() {
    Log.d("TvTunerRecord", "Write Stats Log for Playback.");
    int i = this.mUserId, j = this.mSegmentId, k = this.mOverflow;
    FrameworkStatsLog.write(279, i, 2, 2, j, k);
    return nativeStopDvr();
  }
  
  public int flush() {
    return nativeFlushDvr();
  }
  
  public void close() {
    int i = nativeClose();
    if (i != 0)
      TunerUtils.throwExceptionForResult(i, "failed to close DVR recorder"); 
  }
  
  public void setFileDescriptor(ParcelFileDescriptor paramParcelFileDescriptor) {
    nativeSetFileDescriptor(paramParcelFileDescriptor.getFd());
  }
  
  public long write(long paramLong) {
    return nativeWrite(paramLong);
  }
  
  public long write(byte[] paramArrayOfbyte, long paramLong1, long paramLong2) {
    if (paramLong2 + paramLong1 <= paramArrayOfbyte.length)
      return nativeWrite(paramArrayOfbyte, paramLong1, paramLong2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Array length=");
    stringBuilder.append(paramArrayOfbyte.length);
    stringBuilder.append(", offset=");
    stringBuilder.append(paramLong1);
    stringBuilder.append(", size=");
    stringBuilder.append(paramLong2);
    throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private native int nativeAttachFilter(Filter paramFilter);
  
  private native int nativeClose();
  
  private native int nativeConfigureDvr(DvrSettings paramDvrSettings);
  
  private native int nativeDetachFilter(Filter paramFilter);
  
  private native int nativeFlushDvr();
  
  private native void nativeSetFileDescriptor(int paramInt);
  
  private native int nativeStartDvr();
  
  private native int nativeStopDvr();
  
  private native long nativeWrite(long paramLong);
  
  private native long nativeWrite(byte[] paramArrayOfbyte, long paramLong1, long paramLong2);
}
