package android.media.tv.tuner.dvr;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.media.tv.tuner.TunerUtils;
import android.media.tv.tuner.filter.Filter;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import com.android.internal.util.FrameworkStatsLog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

@SystemApi
public class DvrPlayback implements AutoCloseable {
  public static final int PLAYBACK_STATUS_ALMOST_EMPTY = 2;
  
  public static final int PLAYBACK_STATUS_ALMOST_FULL = 4;
  
  public static final int PLAYBACK_STATUS_EMPTY = 1;
  
  public static final int PLAYBACK_STATUS_FULL = 8;
  
  private static final String TAG = "TvTunerPlayback";
  
  private static int sInstantId = 0;
  
  private Executor mExecutor;
  
  private OnPlaybackStatusChangedListener mListener;
  
  private long mNativeContext;
  
  private int mSegmentId = 0;
  
  private int mUnderflow;
  
  private int mUserId;
  
  private DvrPlayback() {
    this.mUserId = ActivityManager.getCurrentUser();
    int i = sInstantId;
    this.mSegmentId = (0xFFFF & i) << 16;
    sInstantId = i + 1;
  }
  
  public void setListener(Executor paramExecutor, OnPlaybackStatusChangedListener paramOnPlaybackStatusChangedListener) {
    this.mExecutor = paramExecutor;
    this.mListener = paramOnPlaybackStatusChangedListener;
  }
  
  private void onPlaybackStatusChanged(int paramInt) {
    if (paramInt == 1)
      this.mUnderflow++; 
    Executor executor = this.mExecutor;
    if (executor != null && this.mListener != null)
      executor.execute(new _$$Lambda$DvrPlayback$sKzn5fYfElwunzAUVfbswXnopYc(this, paramInt)); 
  }
  
  public int attachFilter(Filter paramFilter) {
    return 1;
  }
  
  public int detachFilter(Filter paramFilter) {
    return 1;
  }
  
  public int configure(DvrSettings paramDvrSettings) {
    return nativeConfigureDvr(paramDvrSettings);
  }
  
  public int start() {
    int i = this.mSegmentId;
    this.mSegmentId = (i & 0xFFFF) + 1 & 0xFFFF | 0xFFFF0000 & i;
    this.mUnderflow = 0;
    Log.d("TvTunerPlayback", "Write Stats Log for Playback.");
    int j = this.mUserId;
    i = this.mSegmentId;
    FrameworkStatsLog.write(279, j, 1, 1, i, 0);
    return nativeStartDvr();
  }
  
  public int stop() {
    Log.d("TvTunerPlayback", "Write Stats Log for Playback.");
    int i = this.mUserId, j = this.mSegmentId, k = this.mUnderflow;
    FrameworkStatsLog.write(279, i, 1, 2, j, k);
    return nativeStopDvr();
  }
  
  public int flush() {
    return nativeFlushDvr();
  }
  
  public void close() {
    int i = nativeClose();
    if (i != 0)
      TunerUtils.throwExceptionForResult(i, "failed to close DVR playback"); 
  }
  
  public void setFileDescriptor(ParcelFileDescriptor paramParcelFileDescriptor) {
    nativeSetFileDescriptor(paramParcelFileDescriptor.getFd());
  }
  
  public long read(long paramLong) {
    return nativeRead(paramLong);
  }
  
  public long read(byte[] paramArrayOfbyte, long paramLong1, long paramLong2) {
    if (paramLong2 + paramLong1 <= paramArrayOfbyte.length)
      return nativeRead(paramArrayOfbyte, paramLong1, paramLong2); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Array length=");
    stringBuilder.append(paramArrayOfbyte.length);
    stringBuilder.append(", offset=");
    stringBuilder.append(paramLong1);
    stringBuilder.append(", size=");
    stringBuilder.append(paramLong2);
    throw new ArrayIndexOutOfBoundsException(stringBuilder.toString());
  }
  
  private native int nativeAttachFilter(Filter paramFilter);
  
  private native int nativeClose();
  
  private native int nativeConfigureDvr(DvrSettings paramDvrSettings);
  
  private native int nativeDetachFilter(Filter paramFilter);
  
  private native int nativeFlushDvr();
  
  private native long nativeRead(long paramLong);
  
  private native long nativeRead(byte[] paramArrayOfbyte, long paramLong1, long paramLong2);
  
  private native void nativeSetFileDescriptor(int paramInt);
  
  private native int nativeStartDvr();
  
  private native int nativeStopDvr();
  
  @Retention(RetentionPolicy.SOURCE)
  static @interface PlaybackStatus {}
}
