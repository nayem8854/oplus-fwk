package android.media.tv.tuner.dvr;

import android.annotation.SystemApi;

@SystemApi
public interface OnRecordStatusChangedListener {
  void onRecordStatusChanged(int paramInt);
}
