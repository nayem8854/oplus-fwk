package android.media.tv.tuner.dvr;

import android.annotation.SystemApi;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public class DvrSettings {
  public static final int DATA_FORMAT_ES = 2;
  
  public static final int DATA_FORMAT_PES = 1;
  
  public static final int DATA_FORMAT_SHV_TLV = 3;
  
  public static final int DATA_FORMAT_TS = 0;
  
  private final int mDataFormat;
  
  private final long mHighThreshold;
  
  private final long mLowThreshold;
  
  private final long mPacketSize;
  
  private final int mStatusMask;
  
  private DvrSettings(int paramInt1, long paramLong1, long paramLong2, long paramLong3, int paramInt2) {
    this.mStatusMask = paramInt1;
    this.mLowThreshold = paramLong1;
    this.mHighThreshold = paramLong2;
    this.mPacketSize = paramLong3;
    this.mDataFormat = paramInt2;
  }
  
  public int getStatusMask() {
    return this.mStatusMask;
  }
  
  public long getLowThreshold() {
    return this.mLowThreshold;
  }
  
  public long getHighThreshold() {
    return this.mHighThreshold;
  }
  
  public long getPacketSize() {
    return this.mPacketSize;
  }
  
  public int getDataFormat() {
    return this.mDataFormat;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  public static final class Builder {
    private int mDataFormat;
    
    private long mHighThreshold;
    
    private long mLowThreshold;
    
    private long mPacketSize;
    
    private int mStatusMask;
    
    public Builder setStatusMask(int param1Int) {
      this.mStatusMask = param1Int;
      return this;
    }
    
    public Builder setLowThreshold(long param1Long) {
      this.mLowThreshold = param1Long;
      return this;
    }
    
    public Builder setHighThreshold(long param1Long) {
      this.mHighThreshold = param1Long;
      return this;
    }
    
    public Builder setPacketSize(long param1Long) {
      this.mPacketSize = param1Long;
      return this;
    }
    
    public Builder setDataFormat(int param1Int) {
      this.mDataFormat = param1Int;
      return this;
    }
    
    public DvrSettings build() {
      return new DvrSettings(this.mStatusMask, this.mLowThreshold, this.mHighThreshold, this.mPacketSize, this.mDataFormat);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DataFormat {}
}
