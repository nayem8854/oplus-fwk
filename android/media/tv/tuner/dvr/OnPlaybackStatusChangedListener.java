package android.media.tv.tuner.dvr;

import android.annotation.SystemApi;

@SystemApi
public interface OnPlaybackStatusChangedListener {
  void onPlaybackStatusChanged(int paramInt);
}
