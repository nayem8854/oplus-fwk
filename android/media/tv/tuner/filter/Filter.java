package android.media.tv.tuner.filter;

import android.annotation.SystemApi;
import android.media.tv.tuner.TunerUtils;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

@SystemApi
public class Filter implements AutoCloseable {
  public static final int STATUS_DATA_READY = 1;
  
  public static final int STATUS_HIGH_WATER = 4;
  
  public static final int STATUS_LOW_WATER = 2;
  
  public static final int STATUS_OVERFLOW = 8;
  
  public static final int SUBTYPE_AUDIO = 3;
  
  public static final int SUBTYPE_DOWNLOAD = 5;
  
  public static final int SUBTYPE_IP = 13;
  
  public static final int SUBTYPE_IP_PAYLOAD = 12;
  
  public static final int SUBTYPE_MMTP = 10;
  
  public static final int SUBTYPE_NTP = 11;
  
  public static final int SUBTYPE_PAYLOAD_THROUGH = 14;
  
  public static final int SUBTYPE_PCR = 8;
  
  public static final int SUBTYPE_PES = 2;
  
  public static final int SUBTYPE_PTP = 16;
  
  public static final int SUBTYPE_RECORD = 6;
  
  public static final int SUBTYPE_SECTION = 1;
  
  public static final int SUBTYPE_TEMI = 9;
  
  public static final int SUBTYPE_TLV = 15;
  
  public static final int SUBTYPE_TS = 7;
  
  public static final int SUBTYPE_UNDEFINED = 0;
  
  public static final int SUBTYPE_VIDEO = 4;
  
  private static final String TAG = "Filter";
  
  public static final int TYPE_ALP = 16;
  
  public static final int TYPE_IP = 4;
  
  public static final int TYPE_MMTP = 2;
  
  public static final int TYPE_TLV = 8;
  
  public static final int TYPE_TS = 1;
  
  public static final int TYPE_UNDEFINED = 0;
  
  private FilterCallback mCallback;
  
  private Executor mExecutor;
  
  private final int mId;
  
  private boolean mIsClosed = false;
  
  private final Object mLock = new Object();
  
  private int mMainType;
  
  private long mNativeContext;
  
  private Filter mSource;
  
  private boolean mStarted;
  
  private int mSubtype;
  
  private Filter(int paramInt) {
    this.mId = paramInt;
  }
  
  private void onFilterStatus(int paramInt) {
    if (this.mCallback != null) {
      Executor executor = this.mExecutor;
      if (executor != null)
        executor.execute(new _$$Lambda$Filter$tekVOX4O5B3jAt2zQSijUdSjqNo(this, paramInt)); 
    } 
  }
  
  private void onFilterEvent(FilterEvent[] paramArrayOfFilterEvent) {
    if (this.mCallback != null) {
      Executor executor = this.mExecutor;
      if (executor != null)
        executor.execute(new _$$Lambda$Filter$zXQWPbykiN1ARGXVsCOpJkUisdU(this, paramArrayOfFilterEvent)); 
    } 
  }
  
  public void setType(int paramInt1, int paramInt2) {
    this.mMainType = paramInt1;
    this.mSubtype = TunerUtils.getFilterSubtype(paramInt1, paramInt2);
  }
  
  public void setCallback(FilterCallback paramFilterCallback, Executor paramExecutor) {
    this.mCallback = paramFilterCallback;
    this.mExecutor = paramExecutor;
  }
  
  public FilterCallback getCallback() {
    return this.mCallback;
  }
  
  public int configure(FilterConfiguration paramFilterConfiguration) {
    synchronized (this.mLock) {
      int i;
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      Settings settings = paramFilterConfiguration.getSettings();
      if (settings == null) {
        i = this.mSubtype;
      } else {
        i = settings.getType();
      } 
      if (this.mMainType == paramFilterConfiguration.getType() && this.mSubtype == i) {
        i = nativeConfigureFilter(paramFilterConfiguration.getType(), i, paramFilterConfiguration);
        return i;
      } 
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Invalid filter config. filter main type=");
      stringBuilder.append(this.mMainType);
      stringBuilder.append(", filter subtype=");
      stringBuilder.append(this.mSubtype);
      stringBuilder.append(". config main type=");
      stringBuilder.append(paramFilterConfiguration.getType());
      stringBuilder.append(", config subtype=");
      stringBuilder.append(i);
      this(stringBuilder.toString());
      throw illegalArgumentException;
    } 
  }
  
  public int getId() {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      return nativeGetId();
    } 
  }
  
  public int setDataSource(Filter paramFilter) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      if (this.mSource == null) {
        int i = nativeSetDataSource(paramFilter);
        if (i == 0)
          this.mSource = paramFilter; 
        return i;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("Data source is existing");
      throw illegalStateException;
    } 
  }
  
  public int start() {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      return nativeStartFilter();
    } 
  }
  
  public int stop() {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      return nativeStopFilter();
    } 
  }
  
  public int flush() {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      return nativeFlushFilter();
    } 
  }
  
  public int read(byte[] paramArrayOfbyte, long paramLong1, long paramLong2) {
    Object object = this.mLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      TunerUtils.checkResourceState("Filter", this.mIsClosed);
      paramLong2 = Math.min(paramLong2, paramArrayOfbyte.length - paramLong1);
      try {
        int i = nativeRead(paramArrayOfbyte, paramLong1, paramLong2);
        /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        return i;
      } finally {}
    } finally {}
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    throw paramArrayOfbyte;
  }
  
  public void close() {
    synchronized (this.mLock) {
      if (this.mIsClosed)
        return; 
      int i = nativeClose();
      if (i != 0) {
        TunerUtils.throwExceptionForResult(i, "Failed to close filter.");
      } else {
        this.mIsClosed = true;
      } 
      return;
    } 
  }
  
  private native int nativeClose();
  
  private native int nativeConfigureFilter(int paramInt1, int paramInt2, FilterConfiguration paramFilterConfiguration);
  
  private native int nativeFlushFilter();
  
  private native int nativeGetId();
  
  private native int nativeRead(byte[] paramArrayOfbyte, long paramLong1, long paramLong2);
  
  private native int nativeSetDataSource(Filter paramFilter);
  
  private native int nativeStartFilter();
  
  private native int nativeStopFilter();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Status {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Subtype {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Type {}
}
