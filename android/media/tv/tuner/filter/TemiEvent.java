package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class TemiEvent extends FilterEvent {
  private final byte[] mDescrData;
  
  private final byte mDescrTag;
  
  private final long mPts;
  
  private TemiEvent(long paramLong, byte paramByte, byte[] paramArrayOfbyte) {
    this.mPts = paramLong;
    this.mDescrTag = paramByte;
    this.mDescrData = paramArrayOfbyte;
  }
  
  public long getPts() {
    return this.mPts;
  }
  
  public byte getDescriptorTag() {
    return this.mDescrTag;
  }
  
  public byte[] getDescriptorData() {
    return this.mDescrData;
  }
}
