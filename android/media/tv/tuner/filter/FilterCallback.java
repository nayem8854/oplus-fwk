package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public interface FilterCallback {
  void onFilterEvent(Filter paramFilter, FilterEvent[] paramArrayOfFilterEvent);
  
  void onFilterStatusChanged(Filter paramFilter, int paramInt);
}
