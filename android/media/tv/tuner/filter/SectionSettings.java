package android.media.tv.tuner.filter;

import android.annotation.SystemApi;
import android.media.tv.tuner.TunerUtils;

@SystemApi
public abstract class SectionSettings extends Settings {
  final boolean mCrcEnabled;
  
  final boolean mIsRaw;
  
  final boolean mIsRepeat;
  
  SectionSettings(int paramInt, boolean paramBoolean1, boolean paramBoolean2, boolean paramBoolean3) {
    super(TunerUtils.getFilterSubtype(paramInt, 1));
    this.mCrcEnabled = paramBoolean1;
    this.mIsRepeat = paramBoolean2;
    this.mIsRaw = paramBoolean3;
  }
  
  public boolean isCrcEnabled() {
    return this.mCrcEnabled;
  }
  
  public boolean isRepeat() {
    return this.mIsRepeat;
  }
  
  public boolean isRaw() {
    return this.mIsRaw;
  }
  
  class Builder<T extends Builder<T>> {
    boolean mCrcEnabled;
    
    boolean mIsRaw;
    
    boolean mIsRepeat;
    
    final int mMainType;
    
    Builder(SectionSettings this$0) {
      this.mMainType = this$0;
    }
    
    public T setCrcEnabled(boolean param1Boolean) {
      this.mCrcEnabled = param1Boolean;
      return self();
    }
    
    public T setRepeat(boolean param1Boolean) {
      this.mIsRepeat = param1Boolean;
      return self();
    }
    
    public T setRaw(boolean param1Boolean) {
      this.mIsRaw = param1Boolean;
      return self();
    }
    
    abstract T self();
  }
}
