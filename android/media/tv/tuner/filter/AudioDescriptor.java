package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class AudioDescriptor {
  private final byte mAdFade;
  
  private final byte mAdGainCenter;
  
  private final byte mAdGainFront;
  
  private final byte mAdGainSurround;
  
  private final byte mAdPan;
  
  private final char mVersionTextTag;
  
  private AudioDescriptor(byte paramByte1, byte paramByte2, char paramChar, byte paramByte3, byte paramByte4, byte paramByte5) {
    this.mAdFade = paramByte1;
    this.mAdPan = paramByte2;
    this.mVersionTextTag = paramChar;
    this.mAdGainCenter = paramByte3;
    this.mAdGainFront = paramByte4;
    this.mAdGainSurround = paramByte5;
  }
  
  public byte getAdFade() {
    return this.mAdFade;
  }
  
  public byte getAdPan() {
    return this.mAdPan;
  }
  
  public char getAdVersionTextTag() {
    return this.mVersionTextTag;
  }
  
  public byte getAdGainCenter() {
    return this.mAdGainCenter;
  }
  
  public byte getAdGainFront() {
    return this.mAdGainFront;
  }
  
  public byte getAdGainSurround() {
    return this.mAdGainSurround;
  }
}
