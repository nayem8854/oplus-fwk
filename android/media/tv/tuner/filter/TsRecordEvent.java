package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class TsRecordEvent extends FilterEvent {
  private final long mDataLength;
  
  private final int mPid;
  
  private final int mScIndexMask;
  
  private final int mTsIndexMask;
  
  private TsRecordEvent(int paramInt1, int paramInt2, int paramInt3, long paramLong) {
    this.mPid = paramInt1;
    this.mTsIndexMask = paramInt2;
    this.mScIndexMask = paramInt3;
    this.mDataLength = paramLong;
  }
  
  public int getPacketId() {
    return this.mPid;
  }
  
  public int getTsIndexMask() {
    return this.mTsIndexMask;
  }
  
  public int getScIndexMask() {
    return this.mScIndexMask;
  }
  
  public long getDataLength() {
    return this.mDataLength;
  }
}
