package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class SectionEvent extends FilterEvent {
  private final int mDataLength;
  
  private final int mSectionNum;
  
  private final int mTableId;
  
  private final int mVersion;
  
  private SectionEvent(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    this.mTableId = paramInt1;
    this.mVersion = paramInt2;
    this.mSectionNum = paramInt3;
    this.mDataLength = paramInt4;
  }
  
  public int getTableId() {
    return this.mTableId;
  }
  
  public int getVersion() {
    return this.mVersion;
  }
  
  public int getSectionNumber() {
    return this.mSectionNum;
  }
  
  public int getDataLength() {
    return this.mDataLength;
  }
}
