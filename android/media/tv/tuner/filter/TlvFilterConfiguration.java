package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public final class TlvFilterConfiguration extends FilterConfiguration {
  public static final int PACKET_TYPE_COMPRESSED = 3;
  
  public static final int PACKET_TYPE_IPV4 = 1;
  
  public static final int PACKET_TYPE_IPV6 = 2;
  
  public static final int PACKET_TYPE_NULL = 255;
  
  public static final int PACKET_TYPE_SIGNALING = 254;
  
  private final boolean mIsCompressedIpPacket;
  
  private final int mPacketType;
  
  private final boolean mPassthrough;
  
  private TlvFilterConfiguration(Settings paramSettings, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    super(paramSettings);
    this.mPacketType = paramInt;
    this.mIsCompressedIpPacket = paramBoolean1;
    this.mPassthrough = paramBoolean2;
  }
  
  public int getType() {
    return 8;
  }
  
  public int getPacketType() {
    return this.mPacketType;
  }
  
  public boolean isCompressedIpPacket() {
    return this.mIsCompressedIpPacket;
  }
  
  public boolean isPassthrough() {
    return this.mPassthrough;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private int mPacketType = 255;
    
    private boolean mIsCompressedIpPacket = false;
    
    private boolean mPassthrough = false;
    
    private Settings mSettings;
    
    public Builder setPacketType(int param1Int) {
      this.mPacketType = param1Int;
      return this;
    }
    
    public Builder setCompressedIpPacket(boolean param1Boolean) {
      this.mIsCompressedIpPacket = param1Boolean;
      return this;
    }
    
    public Builder setPassthrough(boolean param1Boolean) {
      this.mPassthrough = param1Boolean;
      return this;
    }
    
    public Builder setSettings(Settings param1Settings) {
      this.mSettings = param1Settings;
      return this;
    }
    
    public TlvFilterConfiguration build() {
      return new TlvFilterConfiguration(this.mSettings, this.mPacketType, this.mIsCompressedIpPacket, this.mPassthrough);
    }
    
    private Builder() {}
  }
}
