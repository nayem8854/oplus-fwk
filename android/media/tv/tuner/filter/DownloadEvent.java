package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class DownloadEvent extends FilterEvent {
  private final int mDataLength;
  
  private final int mItemFragmentIndex;
  
  private final int mItemId;
  
  private final int mLastItemFragmentIndex;
  
  private final int mMpuSequenceNumber;
  
  private DownloadEvent(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    this.mItemId = paramInt1;
    this.mMpuSequenceNumber = paramInt2;
    this.mItemFragmentIndex = paramInt3;
    this.mLastItemFragmentIndex = paramInt4;
    this.mDataLength = paramInt5;
  }
  
  public int getItemId() {
    return this.mItemId;
  }
  
  public int getMpuSequenceNumber() {
    return this.mMpuSequenceNumber;
  }
  
  public int getItemFragmentIndex() {
    return this.mItemFragmentIndex;
  }
  
  public int getLastItemFragmentIndex() {
    return this.mLastItemFragmentIndex;
  }
  
  public int getDataLength() {
    return this.mDataLength;
  }
}
