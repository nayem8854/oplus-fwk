package android.media.tv.tuner.filter;

import android.annotation.SystemApi;
import android.media.tv.tuner.TunerUtils;

@SystemApi
public class TimeFilter implements AutoCloseable {
  private boolean mEnable = false;
  
  private long mNativeContext;
  
  public int setCurrentTimestamp(long paramLong) {
    int i = nativeSetTimestamp(paramLong);
    if (i == 0)
      this.mEnable = true; 
    return i;
  }
  
  public int clearTimestamp() {
    int i = nativeClearTimestamp();
    if (i == 0)
      this.mEnable = false; 
    return i;
  }
  
  public long getTimeStamp() {
    if (!this.mEnable)
      return -1L; 
    return nativeGetTimestamp().longValue();
  }
  
  public long getSourceTime() {
    if (!this.mEnable)
      return -1L; 
    return nativeGetSourceTime().longValue();
  }
  
  public void close() {
    int i = nativeClose();
    if (i != 0)
      TunerUtils.throwExceptionForResult(i, "Failed to close time filter."); 
  }
  
  private native int nativeClearTimestamp();
  
  private native int nativeClose();
  
  private native Long nativeGetSourceTime();
  
  private native Long nativeGetTimestamp();
  
  private native int nativeSetTimestamp(long paramLong);
}
