package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public abstract class FilterConfiguration {
  final Settings mSettings;
  
  FilterConfiguration(Settings paramSettings) {
    this.mSettings = paramSettings;
  }
  
  public Settings getSettings() {
    return this.mSettings;
  }
  
  public abstract int getType();
}
