package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public final class MmtpFilterConfiguration extends FilterConfiguration {
  private final int mMmtpPid;
  
  private MmtpFilterConfiguration(Settings paramSettings, int paramInt) {
    super(paramSettings);
    this.mMmtpPid = paramInt;
  }
  
  public int getType() {
    return 2;
  }
  
  public int getMmtpPacketId() {
    return this.mMmtpPid;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private int mMmtpPid = 65535;
    
    private Settings mSettings;
    
    public Builder setMmtpPacketId(int param1Int) {
      this.mMmtpPid = param1Int;
      return this;
    }
    
    public Builder setSettings(Settings param1Settings) {
      this.mSettings = param1Settings;
      return this;
    }
    
    public MmtpFilterConfiguration build() {
      return new MmtpFilterConfiguration(this.mSettings, this.mMmtpPid);
    }
    
    private Builder() {}
  }
}
