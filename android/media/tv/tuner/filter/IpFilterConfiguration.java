package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public final class IpFilterConfiguration extends FilterConfiguration {
  private final byte[] mDstIpAddress;
  
  private final int mDstPort;
  
  private final boolean mPassthrough;
  
  private final byte[] mSrcIpAddress;
  
  private final int mSrcPort;
  
  private IpFilterConfiguration(Settings paramSettings, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, int paramInt1, int paramInt2, boolean paramBoolean) {
    super(paramSettings);
    this.mSrcIpAddress = paramArrayOfbyte1;
    this.mDstIpAddress = paramArrayOfbyte2;
    this.mSrcPort = paramInt1;
    this.mDstPort = paramInt2;
    this.mPassthrough = paramBoolean;
  }
  
  public int getType() {
    return 4;
  }
  
  public byte[] getSrcIpAddress() {
    return this.mSrcIpAddress;
  }
  
  public byte[] getDstIpAddress() {
    return this.mDstIpAddress;
  }
  
  public int getSrcPort() {
    return this.mSrcPort;
  }
  
  public int getDstPort() {
    return this.mDstPort;
  }
  
  public boolean isPassthrough() {
    return this.mPassthrough;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private byte[] mSrcIpAddress = new byte[] { 0, 0, 0, 0 };
    
    private byte[] mDstIpAddress = new byte[] { 0, 0, 0, 0 };
    
    private int mSrcPort = 0;
    
    private int mDstPort = 0;
    
    private boolean mPassthrough = false;
    
    private Settings mSettings;
    
    public Builder setSrcIpAddress(byte[] param1ArrayOfbyte) {
      this.mSrcIpAddress = param1ArrayOfbyte;
      return this;
    }
    
    public Builder setDstIpAddress(byte[] param1ArrayOfbyte) {
      this.mDstIpAddress = param1ArrayOfbyte;
      return this;
    }
    
    public Builder setSrcPort(int param1Int) {
      this.mSrcPort = param1Int;
      return this;
    }
    
    public Builder setDstPort(int param1Int) {
      this.mDstPort = param1Int;
      return this;
    }
    
    public Builder setPassthrough(boolean param1Boolean) {
      this.mPassthrough = param1Boolean;
      return this;
    }
    
    public Builder setSettings(Settings param1Settings) {
      this.mSettings = param1Settings;
      return this;
    }
    
    public IpFilterConfiguration build() {
      int i = this.mSrcIpAddress.length;
      if (i == this.mDstIpAddress.length && (i == 4 || i == 16))
        return new IpFilterConfiguration(this.mSettings, this.mSrcIpAddress, this.mDstIpAddress, this.mSrcPort, this.mDstPort, this.mPassthrough); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("The lengths of src and dst IP address must be 4 or 16 and must be the same.srcLength=");
      stringBuilder.append(i);
      stringBuilder.append(", dstLength=");
      stringBuilder.append(this.mDstIpAddress.length);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    private Builder() {}
  }
}
