package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public final class TsFilterConfiguration extends FilterConfiguration {
  private final int mTpid;
  
  private TsFilterConfiguration(Settings paramSettings, int paramInt) {
    super(paramSettings);
    this.mTpid = paramInt;
  }
  
  public int getType() {
    return 1;
  }
  
  public int getTpid() {
    return this.mTpid;
  }
  
  public static Builder builder() {
    return new Builder();
  }
  
  class Builder {
    private Settings mSettings;
    
    private int mTpid = 0;
    
    public Builder setTpid(int param1Int) {
      this.mTpid = param1Int;
      return this;
    }
    
    public Builder setSettings(Settings param1Settings) {
      this.mSettings = param1Settings;
      return this;
    }
    
    public TsFilterConfiguration build() {
      return new TsFilterConfiguration(this.mSettings, this.mTpid);
    }
    
    private Builder() {}
  }
}
