package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class MmtpRecordEvent extends FilterEvent {
  private final long mDataLength;
  
  private final int mScHevcIndexMask;
  
  private MmtpRecordEvent(int paramInt, long paramLong) {
    this.mScHevcIndexMask = paramInt;
    this.mDataLength = paramLong;
  }
  
  public int getScHevcIndexMask() {
    return this.mScHevcIndexMask;
  }
  
  public long getDataLength() {
    return this.mDataLength;
  }
}
