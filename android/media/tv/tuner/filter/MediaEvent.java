package android.media.tv.tuner.filter;

import android.annotation.SystemApi;
import android.media.MediaCodec;

@SystemApi
public class MediaEvent extends FilterEvent {
  private final int mStreamId;
  
  private boolean mReleased = false;
  
  private final long mPts;
  
  private final long mOffset;
  
  private long mNativeContext;
  
  private final int mMpuSequenceNumber;
  
  private final Object mLock = new Object();
  
  private MediaCodec.LinearBlock mLinearBlock;
  
  private final boolean mIsSecureMemory;
  
  private final boolean mIsPtsPresent;
  
  private final boolean mIsPrivateData;
  
  private final AudioDescriptor mExtraMetaData;
  
  private final long mDataLength;
  
  private final long mDataId;
  
  private MediaEvent(int paramInt1, boolean paramBoolean1, long paramLong1, long paramLong2, long paramLong3, MediaCodec.LinearBlock paramLinearBlock, boolean paramBoolean2, long paramLong4, int paramInt2, boolean paramBoolean3, AudioDescriptor paramAudioDescriptor) {
    this.mStreamId = paramInt1;
    this.mIsPtsPresent = paramBoolean1;
    this.mPts = paramLong1;
    this.mDataLength = paramLong2;
    this.mOffset = paramLong3;
    this.mLinearBlock = paramLinearBlock;
    this.mIsSecureMemory = paramBoolean2;
    this.mDataId = paramLong4;
    this.mMpuSequenceNumber = paramInt2;
    this.mIsPrivateData = paramBoolean3;
    this.mExtraMetaData = paramAudioDescriptor;
  }
  
  public int getStreamId() {
    return this.mStreamId;
  }
  
  public boolean isPtsPresent() {
    return this.mIsPtsPresent;
  }
  
  public long getPts() {
    return this.mPts;
  }
  
  public long getDataLength() {
    return this.mDataLength;
  }
  
  public long getOffset() {
    return this.mOffset;
  }
  
  public MediaCodec.LinearBlock getLinearBlock() {
    synchronized (this.mLock) {
      if (this.mLinearBlock == null)
        this.mLinearBlock = nativeGetLinearBlock(); 
      return this.mLinearBlock;
    } 
  }
  
  public boolean isSecureMemory() {
    return this.mIsSecureMemory;
  }
  
  public long getAvDataId() {
    return this.mDataId;
  }
  
  public long getAudioHandle() {
    nativeGetAudioHandle();
    return this.mDataId;
  }
  
  public int getMpuSequenceNumber() {
    return this.mMpuSequenceNumber;
  }
  
  public boolean isPrivateData() {
    return this.mIsPrivateData;
  }
  
  public AudioDescriptor getExtraMetaData() {
    return this.mExtraMetaData;
  }
  
  protected void finalize() {
    release();
  }
  
  public void release() {
    synchronized (this.mLock) {
      if (this.mReleased)
        return; 
      nativeFinalize();
      this.mNativeContext = 0L;
      this.mReleased = true;
      return;
    } 
  }
  
  private native void nativeFinalize();
  
  private native Long nativeGetAudioHandle();
  
  private native MediaCodec.LinearBlock nativeGetLinearBlock();
}
