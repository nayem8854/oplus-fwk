package android.media.tv.tuner.filter;

import android.annotation.SystemApi;

@SystemApi
public class PesEvent extends FilterEvent {
  private final int mDataLength;
  
  private final int mMpuSequenceNumber;
  
  private final int mStreamId;
  
  private PesEvent(int paramInt1, int paramInt2, int paramInt3) {
    this.mStreamId = paramInt1;
    this.mDataLength = paramInt2;
    this.mMpuSequenceNumber = paramInt3;
  }
  
  public int getStreamId() {
    return this.mStreamId;
  }
  
  public int getDataLength() {
    return this.mDataLength;
  }
  
  public int getMpuSequenceNumber() {
    return this.mMpuSequenceNumber;
  }
}
