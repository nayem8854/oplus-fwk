package android.media.tv.tuner;

public final class TunerUtils {
  public static int getFilterSubtype(int paramInt1, int paramInt2) {
    if (paramInt1 == 1) {
      StringBuilder stringBuilder;
      switch (paramInt2) {
        default:
          stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid filter types. Main type=");
          stringBuilder.append(paramInt1);
          stringBuilder.append(", subtype=");
          stringBuilder.append(paramInt2);
          throw new IllegalArgumentException(stringBuilder.toString());
        case 9:
          return 8;
        case 8:
          return 6;
        case 7:
          return 3;
        case 6:
          return 7;
        case 4:
          return 5;
        case 3:
          return 4;
        case 2:
          return 2;
        case 1:
          return 1;
        case 0:
          break;
      } 
      return 0;
    } 
    if (paramInt1 == 2) {
      if (paramInt2 != 10) {
        switch (paramInt2) {
          default:
          
          case 6:
            return 6;
          case 5:
            return 7;
          case 4:
            return 5;
          case 3:
            return 4;
          case 2:
            return 2;
          case 1:
            return 1;
          case 0:
            break;
        } 
        return 0;
      } 
      return 3;
    } 
    if (paramInt1 == 4) {
      if (paramInt2 != 0) {
        if (paramInt2 != 1) {
          switch (paramInt2) {
            default:
            
            case 14:
              return 5;
            case 13:
              return 4;
            case 12:
              return 3;
            case 11:
              break;
          } 
          return 2;
        } 
        return 1;
      } 
      return 0;
    } 
    if (paramInt1 == 8) {
      if (paramInt2 != 0) {
        if (paramInt2 != 1) {
          if (paramInt2 != 14)
            if (paramInt2 == 15)
              return 2;  
          return 3;
        } 
        return 1;
      } 
      return 0;
    } 
    if (paramInt1 == 16) {
      if (paramInt2 != 0) {
        if (paramInt2 != 1) {
          if (paramInt2 != 14)
            if (paramInt2 == 16)
              return 2;  
          return 3;
        } 
        return 1;
      } 
      return 0;
    } 
  }
  
  public static void throwExceptionForResult(int paramInt, String paramString) {
    StringBuilder stringBuilder;
    String str = paramString;
    if (paramString == null)
      str = ""; 
    switch (paramInt) {
      default:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unexpected result ");
        stringBuilder.append(paramInt);
        stringBuilder.append(".  ");
        stringBuilder.append(str);
        throw new RuntimeException(stringBuilder.toString());
      case 6:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown error");
        stringBuilder.append(str);
        throw new RuntimeException(stringBuilder.toString());
      case 5:
        throw new OutOfMemoryError(str);
      case 4:
        throw new IllegalArgumentException(str);
      case 3:
        throw new IllegalStateException(str);
      case 2:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid state: not initialized. ");
        stringBuilder.append(str);
        throw new IllegalStateException(stringBuilder.toString());
      case 1:
        stringBuilder = new StringBuilder();
        stringBuilder.append("Invalid state: resource unavailable. ");
        stringBuilder.append(str);
        throw new IllegalStateException(stringBuilder.toString());
      case 0:
        break;
    } 
  }
  
  public static void checkResourceState(String paramString, boolean paramBoolean) {
    if (!paramBoolean)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString);
    stringBuilder.append(" has been closed");
    throw new IllegalStateException(stringBuilder.toString());
  }
}
