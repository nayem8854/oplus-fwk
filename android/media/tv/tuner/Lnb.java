package android.media.tv.tuner;

import android.annotation.SystemApi;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

@SystemApi
public class Lnb implements AutoCloseable {
  public static final int EVENT_TYPE_DISEQC_RX_OVERFLOW = 0;
  
  public static final int EVENT_TYPE_DISEQC_RX_PARITY_ERROR = 2;
  
  public static final int EVENT_TYPE_DISEQC_RX_TIMEOUT = 1;
  
  public static final int EVENT_TYPE_LNB_OVERLOAD = 3;
  
  public static final int POSITION_A = 1;
  
  public static final int POSITION_B = 2;
  
  public static final int POSITION_UNDEFINED = 0;
  
  private static final String TAG = "Lnb";
  
  public static final int TONE_CONTINUOUS = 1;
  
  public static final int TONE_NONE = 0;
  
  public static final int VOLTAGE_11V = 2;
  
  public static final int VOLTAGE_12V = 3;
  
  public static final int VOLTAGE_13V = 4;
  
  public static final int VOLTAGE_14V = 5;
  
  public static final int VOLTAGE_15V = 6;
  
  public static final int VOLTAGE_18V = 7;
  
  public static final int VOLTAGE_19V = 8;
  
  public static final int VOLTAGE_5V = 1;
  
  public static final int VOLTAGE_NONE = 0;
  
  LnbCallback mCallback;
  
  Executor mExecutor;
  
  int mId;
  
  private Boolean mIsClosed = Boolean.valueOf(false);
  
  private final Object mLock = new Object();
  
  private long mNativeContext;
  
  Tuner mTuner;
  
  private Lnb(int paramInt) {
    this.mId = paramInt;
  }
  
  void setCallback(Executor paramExecutor, LnbCallback paramLnbCallback, Tuner paramTuner) {
    this.mCallback = paramLnbCallback;
    this.mExecutor = paramExecutor;
    this.mTuner = paramTuner;
  }
  
  private void onEvent(int paramInt) {
    Executor executor = this.mExecutor;
    if (executor != null && this.mCallback != null)
      executor.execute(new _$$Lambda$Lnb$IV6NQ1_DZcILU_MY88njae06xhs(this, paramInt)); 
  }
  
  private void onDiseqcMessage(byte[] paramArrayOfbyte) {
    Executor executor = this.mExecutor;
    if (executor != null && this.mCallback != null)
      executor.execute(new _$$Lambda$Lnb$FxxuwmgmY3zg5Qj8CbdUkPw_Lnk(this, paramArrayOfbyte)); 
  }
  
  boolean isClosed() {
    synchronized (this.mLock) {
      return this.mIsClosed.booleanValue();
    } 
  }
  
  public int setVoltage(int paramInt) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Lnb", this.mIsClosed.booleanValue());
      paramInt = nativeSetVoltage(paramInt);
      return paramInt;
    } 
  }
  
  public int setTone(int paramInt) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Lnb", this.mIsClosed.booleanValue());
      paramInt = nativeSetTone(paramInt);
      return paramInt;
    } 
  }
  
  public int setSatellitePosition(int paramInt) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Lnb", this.mIsClosed.booleanValue());
      paramInt = nativeSetSatellitePosition(paramInt);
      return paramInt;
    } 
  }
  
  public int sendDiseqcMessage(byte[] paramArrayOfbyte) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Lnb", this.mIsClosed.booleanValue());
      return nativeSendDiseqcMessage(paramArrayOfbyte);
    } 
  }
  
  public void close() {
    synchronized (this.mLock) {
      if (this.mIsClosed.booleanValue())
        return; 
      int i = nativeClose();
      if (i != 0) {
        TunerUtils.throwExceptionForResult(i, "Failed to close LNB");
      } else {
        this.mIsClosed = Boolean.valueOf(true);
        this.mTuner.releaseLnb();
      } 
      return;
    } 
  }
  
  private native int nativeClose();
  
  private native int nativeSendDiseqcMessage(byte[] paramArrayOfbyte);
  
  private native int nativeSetSatellitePosition(int paramInt);
  
  private native int nativeSetTone(int paramInt);
  
  private native int nativeSetVoltage(int paramInt);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface EventType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Position {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Tone {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Voltage {}
}
