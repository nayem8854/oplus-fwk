package android.media.tv.tuner;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.content.Context;
import android.media.tv.tuner.dvr.DvrPlayback;
import android.media.tv.tuner.dvr.DvrRecorder;
import android.media.tv.tuner.dvr.OnPlaybackStatusChangedListener;
import android.media.tv.tuner.dvr.OnRecordStatusChangedListener;
import android.media.tv.tuner.filter.Filter;
import android.media.tv.tuner.filter.FilterCallback;
import android.media.tv.tuner.filter.TimeFilter;
import android.media.tv.tuner.frontend.Atsc3PlpInfo;
import android.media.tv.tuner.frontend.FrontendInfo;
import android.media.tv.tuner.frontend.FrontendSettings;
import android.media.tv.tuner.frontend.FrontendStatus;
import android.media.tv.tuner.frontend.OnTuneEventListener;
import android.media.tv.tuner.frontend.ScanCallback;
import android.media.tv.tunerresourcemanager.ResourceClientProfile;
import android.media.tv.tunerresourcemanager.TunerDemuxRequest;
import android.media.tv.tunerresourcemanager.TunerDescramblerRequest;
import android.media.tv.tunerresourcemanager.TunerFrontendInfo;
import android.media.tv.tunerresourcemanager.TunerFrontendRequest;
import android.media.tv.tunerresourcemanager.TunerLnbRequest;
import android.media.tv.tunerresourcemanager.TunerResourceManager;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import com.android.internal.util.FrameworkStatsLog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;

@SystemApi
public class Tuner implements AutoCloseable {
  static {
    try {
      System.loadLibrary("media_tv_tuner");
      nativeInit();
    } catch (UnsatisfiedLinkError unsatisfiedLinkError) {
      Log.d("MediaTvTuner", "tuner JNI library not found!");
    } 
  }
  
  private int mFrontendType = 0;
  
  private Map<Integer, Descrambler> mDescramblers = new HashMap<>();
  
  private List<Filter> mFilters = new ArrayList<>();
  
  private final TunerResourceManager.ResourcesReclaimListener mResourceListener = (TunerResourceManager.ResourcesReclaimListener)new Object(this);
  
  private static final boolean DEBUG = false;
  
  public static final int DVR_TYPE_PLAYBACK = 1;
  
  public static final int DVR_TYPE_RECORD = 0;
  
  public static final int INVALID_AV_SYNC_ID = -1;
  
  public static final int INVALID_FILTER_ID = -1;
  
  public static final int INVALID_STREAM_ID = 65535;
  
  public static final long INVALID_TIMESTAMP = -1L;
  
  public static final int INVALID_TS_PID = 65535;
  
  private static final int MSG_ON_FILTER_EVENT = 2;
  
  private static final int MSG_ON_FILTER_STATUS = 3;
  
  private static final int MSG_ON_LNB_EVENT = 4;
  
  private static final int MSG_RESOURCE_LOST = 1;
  
  public static final int RESULT_INVALID_ARGUMENT = 4;
  
  public static final int RESULT_INVALID_STATE = 3;
  
  public static final int RESULT_NOT_INITIALIZED = 2;
  
  public static final int RESULT_OUT_OF_MEMORY = 5;
  
  public static final int RESULT_SUCCESS = 0;
  
  public static final int RESULT_UNAVAILABLE = 1;
  
  public static final int RESULT_UNKNOWN_ERROR = 6;
  
  public static final int SCAN_TYPE_AUTO = 1;
  
  public static final int SCAN_TYPE_BLIND = 2;
  
  public static final int SCAN_TYPE_UNDEFINED = 0;
  
  private static final String TAG = "MediaTvTuner";
  
  private final int mClientId;
  
  private final Context mContext;
  
  private Integer mDemuxHandle;
  
  private Frontend mFrontend;
  
  private Integer mFrontendHandle;
  
  private FrontendInfo mFrontendInfo;
  
  private EventHandler mHandler;
  
  private Lnb mLnb;
  
  private Integer mLnbHandle;
  
  private long mNativeContext;
  
  private OnResourceLostListener mOnResourceLostListener;
  
  private Executor mOnResourceLostListenerExecutor;
  
  private OnTuneEventListener mOnTuneEventListener;
  
  private Executor mOnTunerEventExecutor;
  
  private ScanCallback mScanCallback;
  
  private Executor mScanCallbackExecutor;
  
  private final TunerResourceManager mTunerResourceManager;
  
  private int mUserId;
  
  public Tuner(Context paramContext, String paramString, int paramInt) {
    nativeSetup();
    this.mContext = paramContext;
    this.mTunerResourceManager = (TunerResourceManager)paramContext.getSystemService("tv_tuner_resource_mgr");
    if (this.mHandler == null)
      this.mHandler = createEventHandler(); 
    this.mHandler = createEventHandler();
    int[] arrayOfInt = new int[1];
    ResourceClientProfile resourceClientProfile = new ResourceClientProfile(paramString, paramInt);
    this.mTunerResourceManager.registerClientProfile(resourceClientProfile, new HandlerExecutor(this.mHandler), this.mResourceListener, arrayOfInt);
    this.mClientId = arrayOfInt[0];
    this.mUserId = ActivityManager.getCurrentUser();
    setFrontendInfoList();
    setLnbIds();
  }
  
  private void setFrontendInfoList() {
    List<Integer> list = getFrontendIds();
    if (list == null)
      return; 
    TunerFrontendInfo[] arrayOfTunerFrontendInfo = new TunerFrontendInfo[list.size()];
    for (byte b = 0; b < list.size(); b++) {
      int i = ((Integer)list.get(b)).intValue();
      FrontendInfo frontendInfo = getFrontendInfoById(i);
      if (frontendInfo != null) {
        TunerFrontendInfo tunerFrontendInfo = new TunerFrontendInfo(i, frontendInfo.getType(), frontendInfo.getExclusiveGroupId());
        arrayOfTunerFrontendInfo[b] = tunerFrontendInfo;
      } 
    } 
    this.mTunerResourceManager.setFrontendInfoList(arrayOfTunerFrontendInfo);
  }
  
  public List<Integer> getFrontendIds() {
    return nativeGetFrontendIds();
  }
  
  private void setLnbIds() {
    int[] arrayOfInt = nativeGetLnbIds();
    if (arrayOfInt == null)
      return; 
    this.mTunerResourceManager.setLnbInfoList(arrayOfInt);
  }
  
  public void setResourceLostListener(Executor paramExecutor, OnResourceLostListener paramOnResourceLostListener) {
    Objects.requireNonNull(paramExecutor, "OnResourceLostListener must not be null");
    Objects.requireNonNull(paramOnResourceLostListener, "executor must not be null");
    this.mOnResourceLostListener = paramOnResourceLostListener;
    this.mOnResourceLostListenerExecutor = paramExecutor;
  }
  
  public void clearResourceLostListener() {
    this.mOnResourceLostListener = null;
    this.mOnResourceLostListenerExecutor = null;
  }
  
  public void shareFrontendFromTuner(Tuner paramTuner) {
    this.mTunerResourceManager.shareFrontend(this.mClientId, paramTuner.mClientId);
    Integer integer = paramTuner.mFrontendHandle;
    this.mFrontend = nativeOpenFrontendByHandle(integer.intValue());
  }
  
  public void updateResourcePriority(int paramInt1, int paramInt2) {
    this.mTunerResourceManager.updateClientPriority(this.mClientId, paramInt1, paramInt2);
  }
  
  public void close() {
    Integer integer2 = this.mFrontendHandle;
    if (integer2 != null) {
      int i = nativeCloseFrontend(integer2.intValue());
      if (i != 0)
        TunerUtils.throwExceptionForResult(i, "failed to close frontend"); 
      this.mTunerResourceManager.releaseFrontend(this.mFrontendHandle.intValue(), this.mClientId);
      i = this.mUserId;
      FrameworkStatsLog.write(276, i, 0);
      this.mFrontendHandle = null;
      this.mFrontend = null;
    } 
    Lnb lnb = this.mLnb;
    if (lnb != null)
      lnb.close(); 
    if (!this.mDescramblers.isEmpty()) {
      for (Map.Entry<Integer, Descrambler> entry : this.mDescramblers.entrySet()) {
        ((Descrambler)entry.getValue()).close();
        this.mTunerResourceManager.releaseDescrambler(((Integer)entry.getKey()).intValue(), this.mClientId);
      } 
      this.mDescramblers.clear();
    } 
    if (!this.mFilters.isEmpty()) {
      for (Filter filter : this.mFilters)
        filter.close(); 
      this.mFilters.clear();
    } 
    Integer integer1 = this.mDemuxHandle;
    if (integer1 != null) {
      int i = nativeCloseDemux(integer1.intValue());
      if (i != 0)
        TunerUtils.throwExceptionForResult(i, "failed to close demux"); 
      this.mTunerResourceManager.releaseDemux(this.mDemuxHandle.intValue(), this.mClientId);
      this.mFrontendHandle = null;
    } 
    TunerUtils.throwExceptionForResult(nativeClose(), "failed to close tuner");
  }
  
  private EventHandler createEventHandler() {
    Looper looper = Looper.myLooper();
    if (looper != null)
      return new EventHandler(looper); 
    looper = Looper.getMainLooper();
    if (looper != null)
      return new EventHandler(looper); 
    return null;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DvrType {}
  
  class EventHandler extends Handler {
    final Tuner this$0;
    
    private EventHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i == 3) {
          Filter filter = (Filter)param1Message.obj;
          if (filter.getCallback() != null)
            filter.getCallback().onFilterStatusChanged(filter, param1Message.arg1); 
        } 
      } else if (Tuner.this.mOnResourceLostListener != null) {
        Tuner tuner = Tuner.this;
        if (tuner.mOnResourceLostListenerExecutor != null)
          Tuner.this.mOnResourceLostListenerExecutor.execute(new _$$Lambda$Tuner$EventHandler$O98cW0HuxUDT27bFMy40ViHTsqc(this)); 
      } 
    }
  }
  
  private class Frontend {
    private int mId;
    
    final Tuner this$0;
    
    private Frontend(int param1Int) {
      this.mId = param1Int;
    }
  }
  
  public void setOnTuneEventListener(Executor paramExecutor, OnTuneEventListener paramOnTuneEventListener) {
    this.mOnTuneEventListener = paramOnTuneEventListener;
    this.mOnTunerEventExecutor = paramExecutor;
  }
  
  public void clearOnTuneEventListener() {
    this.mOnTuneEventListener = null;
    this.mOnTunerEventExecutor = null;
  }
  
  public int tune(FrontendSettings paramFrontendSettings) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Tune to ");
    stringBuilder.append(paramFrontendSettings.getFrequency());
    Log.d("MediaTvTuner", stringBuilder.toString());
    this.mFrontendType = paramFrontendSettings.getType();
    if (checkResource(0)) {
      this.mFrontendInfo = null;
      Log.d("MediaTvTuner", "Write Stats Log for tuning.");
      int i = this.mUserId;
      FrameworkStatsLog.write(276, i, 1);
      return nativeTune(paramFrontendSettings.getType(), paramFrontendSettings);
    } 
    return 1;
  }
  
  public int cancelTuning() {
    return nativeStopTune();
  }
  
  public int scan(FrontendSettings paramFrontendSettings, int paramInt, Executor paramExecutor, ScanCallback paramScanCallback) {
    if (this.mScanCallback == null && this.mScanCallbackExecutor == null) {
      this.mFrontendType = paramFrontendSettings.getType();
      if (checkResource(0)) {
        this.mScanCallback = paramScanCallback;
        this.mScanCallbackExecutor = paramExecutor;
        this.mFrontendInfo = null;
        int i = this.mUserId;
        FrameworkStatsLog.write(276, i, 5);
        return nativeScan(paramFrontendSettings.getType(), paramFrontendSettings, paramInt);
      } 
      return 1;
    } 
    throw new IllegalStateException("Scan already in progress.  stopScan must be called before a new scan can be started.");
  }
  
  public int cancelScanning() {
    int i = this.mUserId;
    FrameworkStatsLog.write(276, i, 6);
    i = nativeStopScan();
    this.mScanCallback = null;
    this.mScanCallbackExecutor = null;
    return i;
  }
  
  private boolean requestFrontend() {
    int[] arrayOfInt = new int[1];
    TunerFrontendRequest tunerFrontendRequest = new TunerFrontendRequest(this.mClientId, this.mFrontendType);
    boolean bool = this.mTunerResourceManager.requestFrontend(tunerFrontendRequest, arrayOfInt);
    if (bool) {
      Integer integer = Integer.valueOf(arrayOfInt[0]);
      this.mFrontend = nativeOpenFrontendByHandle(integer.intValue());
    } 
    return bool;
  }
  
  private int setLnb(Lnb paramLnb) {
    return nativeSetLnb(paramLnb.mId);
  }
  
  public int setLnaEnabled(boolean paramBoolean) {
    return nativeSetLna(paramBoolean);
  }
  
  public FrontendStatus getFrontendStatus(int[] paramArrayOfint) {
    if (this.mFrontend != null)
      return nativeGetFrontendStatus(paramArrayOfint); 
    throw new IllegalStateException("frontend is not initialized");
  }
  
  public int getAvSyncHwId(Filter paramFilter) {
    boolean bool = checkResource(1);
    int i = -1;
    if (!bool)
      return -1; 
    Integer integer = nativeGetAvSyncHwId(paramFilter);
    if (integer != null)
      i = integer.intValue(); 
    return i;
  }
  
  public long getAvSyncTime(int paramInt) {
    boolean bool = checkResource(1);
    long l = -1L;
    if (!bool)
      return -1L; 
    Long long_ = nativeGetAvSyncTime(paramInt);
    if (long_ != null)
      l = long_.longValue(); 
    return l;
  }
  
  public int connectCiCam(int paramInt) {
    if (checkResource(1))
      return nativeConnectCiCam(paramInt); 
    return 1;
  }
  
  public int disconnectCiCam() {
    if (checkResource(1))
      return nativeDisconnectCiCam(); 
    return 1;
  }
  
  public FrontendInfo getFrontendInfo() {
    if (!checkResource(0))
      return null; 
    Frontend frontend = this.mFrontend;
    if (frontend != null) {
      if (this.mFrontendInfo == null)
        this.mFrontendInfo = getFrontendInfoById(frontend.mId); 
      return this.mFrontendInfo;
    } 
    throw new IllegalStateException("frontend is not initialized");
  }
  
  public FrontendInfo getFrontendInfoById(int paramInt) {
    return nativeGetFrontendInfo(paramInt);
  }
  
  public DemuxCapabilities getDemuxCapabilities() {
    return nativeGetDemuxCapabilities();
  }
  
  private void onFrontendEvent(int paramInt) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Got event from tuning. Event type: ");
    stringBuilder.append(paramInt);
    Log.d("MediaTvTuner", stringBuilder.toString());
    Executor executor = this.mOnTunerEventExecutor;
    if (executor != null && this.mOnTuneEventListener != null)
      executor.execute(new _$$Lambda$Tuner$W0RSFCXzyS_Nm5JhUoyHO4_gtHc(this, paramInt)); 
    Log.d("MediaTvTuner", "Wrote Stats Log for the events from tuning.");
    if (paramInt == 0) {
      paramInt = this.mUserId;
      FrameworkStatsLog.write(276, paramInt, 2);
    } else if (paramInt == 1) {
      paramInt = this.mUserId;
      FrameworkStatsLog.write(276, paramInt, 3);
    } else if (paramInt == 2) {
      paramInt = this.mUserId;
      FrameworkStatsLog.write(276, paramInt, 4);
    } 
  }
  
  private void onLocked() {
    Log.d("MediaTvTuner", "Wrote Stats Log for locked event from scanning.");
    int i = this.mUserId;
    FrameworkStatsLog.write(276, i, 2);
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$7LyXqscGIcs4FBunPKfBAgBWBPc(this)); 
  }
  
  private void onScanStopped() {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$iNLvb_AT8Ni5vDrCZXOvhTYUE1U(this)); 
  }
  
  private void onProgress(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$EpB_abkPzm4k9y4f_ckGHE7hk_Y(this, paramInt)); 
  }
  
  private void onFrequenciesReport(int[] paramArrayOfint) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$cO3VZA6A_FG0wouIFcyFrkppVKQ(this, paramArrayOfint)); 
  }
  
  private void onSymbolRates(int[] paramArrayOfint) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$qLBwtqwgHR4JTsJuxl0FSPNFPuA(this, paramArrayOfint)); 
  }
  
  private void onHierarchy(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$kZB5DmQFvVQLdFFRG1ICjIXzxIg(this, paramInt)); 
  }
  
  private void onSignalType(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$bvxMLDowJ_umoo__NEeXgXWz0Gw(this, paramInt)); 
  }
  
  private void onPlpIds(int[] paramArrayOfint) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$NJZQL7FtM6GTMGzFfc6hPmY8a_A(this, paramArrayOfint)); 
  }
  
  private void onGroupIds(int[] paramArrayOfint) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$Br0QGgsEJkvWiWGajrBIp9UhcI4(this, paramArrayOfint)); 
  }
  
  private void onInputStreamIds(int[] paramArrayOfint) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$97hY_kdBewG88N1_oDJVJ_0tzGU(this, paramArrayOfint)); 
  }
  
  private void onDvbsStandard(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$6JzdHyaum_wXts_YXYS69zQbClY(this, paramInt)); 
  }
  
  private void onDvbtStandard(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$0UUd7kSDwJbNOtYJHcznNHjL2vI(this, paramInt)); 
  }
  
  private void onAnalogSifStandard(int paramInt) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$H2MeyMgstu2_FkYFyyhEuqcpaOM(this, paramInt)); 
  }
  
  private void onAtsc3PlpInfos(Atsc3PlpInfo[] paramArrayOfAtsc3PlpInfo) {
    Executor executor = this.mScanCallbackExecutor;
    if (executor != null && this.mScanCallback != null)
      executor.execute(new _$$Lambda$Tuner$vA8r09mS8B2Xp9N33dOsBZ4Y_0g(this, paramArrayOfAtsc3PlpInfo)); 
  }
  
  public Filter openFilter(int paramInt1, int paramInt2, long paramLong, Executor paramExecutor, FilterCallback paramFilterCallback) {
    if (!checkResource(1))
      return null; 
    int i = TunerUtils.getFilterSubtype(paramInt1, paramInt2);
    Filter filter = nativeOpenFilter(paramInt1, i, paramLong);
    if (filter != null) {
      filter.setType(paramInt1, paramInt2);
      filter.setCallback(paramFilterCallback, paramExecutor);
      if (this.mHandler == null)
        this.mHandler = createEventHandler(); 
      this.mFilters.add(filter);
    } 
    return filter;
  }
  
  public Lnb openLnb(Executor paramExecutor, LnbCallback paramLnbCallback) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramLnbCallback, "LnbCallback must not be null");
    Lnb lnb = this.mLnb;
    if (lnb != null) {
      lnb.setCallback(paramExecutor, paramLnbCallback, this);
      return this.mLnb;
    } 
    if (checkResource(3)) {
      lnb = this.mLnb;
      if (lnb != null) {
        lnb.setCallback(paramExecutor, paramLnbCallback, this);
        setLnb(this.mLnb);
        return this.mLnb;
      } 
    } 
    return null;
  }
  
  public Lnb openLnbByName(String paramString, Executor paramExecutor, LnbCallback paramLnbCallback) {
    Objects.requireNonNull(paramString, "LNB name must not be null");
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramLnbCallback, "LnbCallback must not be null");
    Lnb lnb = nativeOpenLnbByName(paramString);
    if (lnb != null) {
      Lnb lnb1 = this.mLnb;
      if (lnb1 != null) {
        lnb1.close();
        this.mLnbHandle = null;
      } 
      this.mLnb = lnb;
      lnb.setCallback(paramExecutor, paramLnbCallback, this);
      setLnb(this.mLnb);
    } 
    return this.mLnb;
  }
  
  private boolean requestLnb() {
    int[] arrayOfInt = new int[1];
    TunerLnbRequest tunerLnbRequest = new TunerLnbRequest(this.mClientId);
    boolean bool = this.mTunerResourceManager.requestLnb(tunerLnbRequest, arrayOfInt);
    if (bool) {
      Integer integer = Integer.valueOf(arrayOfInt[0]);
      this.mLnb = nativeOpenLnbByHandle(integer.intValue());
    } 
    return bool;
  }
  
  public TimeFilter openTimeFilter() {
    if (!checkResource(1))
      return null; 
    return nativeOpenTimeFilter();
  }
  
  public Descrambler openDescrambler() {
    if (!checkResource(1))
      return null; 
    return requestDescrambler();
  }
  
  public DvrRecorder openDvrRecorder(long paramLong, Executor paramExecutor, OnRecordStatusChangedListener paramOnRecordStatusChangedListener) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramOnRecordStatusChangedListener, "OnRecordStatusChangedListener must not be null");
    if (!checkResource(1))
      return null; 
    DvrRecorder dvrRecorder = nativeOpenDvrRecorder(paramLong);
    dvrRecorder.setListener(paramExecutor, paramOnRecordStatusChangedListener);
    return dvrRecorder;
  }
  
  public DvrPlayback openDvrPlayback(long paramLong, Executor paramExecutor, OnPlaybackStatusChangedListener paramOnPlaybackStatusChangedListener) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramOnPlaybackStatusChangedListener, "OnPlaybackStatusChangedListener must not be null");
    if (!checkResource(1))
      return null; 
    DvrPlayback dvrPlayback = nativeOpenDvrPlayback(paramLong);
    dvrPlayback.setListener(paramExecutor, paramOnPlaybackStatusChangedListener);
    return dvrPlayback;
  }
  
  private boolean requestDemux() {
    int[] arrayOfInt = new int[1];
    TunerDemuxRequest tunerDemuxRequest = new TunerDemuxRequest(this.mClientId);
    boolean bool = this.mTunerResourceManager.requestDemux(tunerDemuxRequest, arrayOfInt);
    if (bool) {
      Integer integer = Integer.valueOf(arrayOfInt[0]);
      nativeOpenDemuxByhandle(integer.intValue());
    } 
    return bool;
  }
  
  private Descrambler requestDescrambler() {
    int[] arrayOfInt = new int[1];
    TunerDescramblerRequest tunerDescramblerRequest = new TunerDescramblerRequest(this.mClientId);
    boolean bool = this.mTunerResourceManager.requestDescrambler(tunerDescramblerRequest, arrayOfInt);
    if (!bool)
      return null; 
    int i = arrayOfInt[0];
    Descrambler descrambler = nativeOpenDescramblerByHandle(i);
    if (descrambler != null) {
      this.mDescramblers.put(Integer.valueOf(i), descrambler);
    } else {
      this.mTunerResourceManager.releaseDescrambler(i, this.mClientId);
    } 
    return descrambler;
  }
  
  private boolean checkResource(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 3)
          return false; 
        if (this.mLnb == null && !requestLnb())
          return false; 
      } else if (this.mDemuxHandle == null && !requestDemux()) {
        return false;
      } 
    } else if (this.mFrontendHandle == null && !requestFrontend()) {
      return false;
    } 
    return true;
  }
  
  void releaseLnb() {
    Integer integer = this.mLnbHandle;
    if (integer != null) {
      this.mTunerResourceManager.releaseLnb(integer.intValue(), this.mClientId);
      this.mLnbHandle = null;
    } 
    this.mLnb = null;
  }
  
  private native int nativeClose();
  
  private native int nativeCloseDemux(int paramInt);
  
  private native int nativeCloseFrontend(int paramInt);
  
  private native int nativeCloseFrontendByHandle(int paramInt);
  
  private native int nativeConnectCiCam(int paramInt);
  
  private native int nativeDisconnectCiCam();
  
  private native Integer nativeGetAvSyncHwId(Filter paramFilter);
  
  private native Long nativeGetAvSyncTime(int paramInt);
  
  private native DemuxCapabilities nativeGetDemuxCapabilities();
  
  private native List<Integer> nativeGetFrontendIds();
  
  private native FrontendInfo nativeGetFrontendInfo(int paramInt);
  
  private native FrontendStatus nativeGetFrontendStatus(int[] paramArrayOfint);
  
  private native int[] nativeGetLnbIds();
  
  private static native void nativeInit();
  
  private native int nativeOpenDemuxByhandle(int paramInt);
  
  private native Descrambler nativeOpenDescramblerByHandle(int paramInt);
  
  private native DvrPlayback nativeOpenDvrPlayback(long paramLong);
  
  private native DvrRecorder nativeOpenDvrRecorder(long paramLong);
  
  private native Filter nativeOpenFilter(int paramInt1, int paramInt2, long paramLong);
  
  private native Frontend nativeOpenFrontendByHandle(int paramInt);
  
  private native Lnb nativeOpenLnbByHandle(int paramInt);
  
  private native Lnb nativeOpenLnbByName(String paramString);
  
  private native TimeFilter nativeOpenTimeFilter();
  
  private native int nativeScan(int paramInt1, FrontendSettings paramFrontendSettings, int paramInt2);
  
  private native int nativeSetLna(boolean paramBoolean);
  
  private native int nativeSetLnb(int paramInt);
  
  private native void nativeSetup();
  
  private native int nativeStopScan();
  
  private native int nativeStopTune();
  
  private native int nativeTune(int paramInt, FrontendSettings paramFrontendSettings);
  
  public static interface OnResourceLostListener {
    void onResourceLost(Tuner param1Tuner);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Result {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScanType {}
}
