package android.media.tv.tuner;

import android.annotation.SystemApi;
import android.media.tv.tuner.filter.Filter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public class Descrambler implements AutoCloseable {
  public static final int PID_TYPE_MMTP = 2;
  
  public static final int PID_TYPE_T = 1;
  
  private static final String TAG = "Descrambler";
  
  private boolean mIsClosed = false;
  
  private final Object mLock = new Object();
  
  private long mNativeContext;
  
  public int addPid(int paramInt1, int paramInt2, Filter paramFilter) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Descrambler", this.mIsClosed);
      paramInt1 = nativeAddPid(paramInt1, paramInt2, paramFilter);
      return paramInt1;
    } 
  }
  
  public int removePid(int paramInt1, int paramInt2, Filter paramFilter) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Descrambler", this.mIsClosed);
      paramInt1 = nativeRemovePid(paramInt1, paramInt2, paramFilter);
      return paramInt1;
    } 
  }
  
  public int setKeyToken(byte[] paramArrayOfbyte) {
    synchronized (this.mLock) {
      TunerUtils.checkResourceState("Descrambler", this.mIsClosed);
      Objects.requireNonNull(paramArrayOfbyte, "key token must not be null");
      return nativeSetKeyToken(paramArrayOfbyte);
    } 
  }
  
  public void close() {
    synchronized (this.mLock) {
      if (this.mIsClosed)
        return; 
      int i = nativeClose();
      if (i != 0) {
        TunerUtils.throwExceptionForResult(i, "Failed to close descrambler");
      } else {
        this.mIsClosed = true;
      } 
      return;
    } 
  }
  
  private native int nativeAddPid(int paramInt1, int paramInt2, Filter paramFilter);
  
  private native int nativeClose();
  
  private native int nativeRemovePid(int paramInt1, int paramInt2, Filter paramFilter);
  
  private native int nativeSetKeyToken(byte[] paramArrayOfbyte);
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PidType {}
}
