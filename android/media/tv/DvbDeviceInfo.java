package android.media.tv;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

@SystemApi
public final class DvbDeviceInfo implements Parcelable {
  public static final Parcelable.Creator<DvbDeviceInfo> CREATOR = new Parcelable.Creator<DvbDeviceInfo>() {
      public DvbDeviceInfo createFromParcel(Parcel param1Parcel) {
        try {
          return new DvbDeviceInfo(param1Parcel);
        } catch (Exception exception) {
          Log.e("DvbDeviceInfo", "Exception creating DvbDeviceInfo from parcel", exception);
          return null;
        } 
      }
      
      public DvbDeviceInfo[] newArray(int param1Int) {
        return new DvbDeviceInfo[param1Int];
      }
    };
  
  static final String TAG = "DvbDeviceInfo";
  
  private final int mAdapterId;
  
  private final int mDeviceId;
  
  private DvbDeviceInfo(Parcel paramParcel) {
    this.mAdapterId = paramParcel.readInt();
    this.mDeviceId = paramParcel.readInt();
  }
  
  public DvbDeviceInfo(int paramInt1, int paramInt2) {
    this.mAdapterId = paramInt1;
    this.mDeviceId = paramInt2;
  }
  
  public int getAdapterId() {
    return this.mAdapterId;
  }
  
  public int getDeviceId() {
    return this.mDeviceId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mAdapterId);
    paramParcel.writeInt(this.mDeviceId);
  }
}
