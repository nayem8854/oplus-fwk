package android.media.tv;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ITvInputSessionCallback extends IInterface {
  void onChannelRetuned(Uri paramUri) throws RemoteException;
  
  void onContentAllowed() throws RemoteException;
  
  void onContentBlocked(String paramString) throws RemoteException;
  
  void onError(int paramInt) throws RemoteException;
  
  void onLayoutSurface(int paramInt1, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void onRecordingStopped(Uri paramUri) throws RemoteException;
  
  void onSessionCreated(ITvInputSession paramITvInputSession, IBinder paramIBinder) throws RemoteException;
  
  void onSessionEvent(String paramString, Bundle paramBundle) throws RemoteException;
  
  void onTimeShiftCurrentPositionChanged(long paramLong) throws RemoteException;
  
  void onTimeShiftStartPositionChanged(long paramLong) throws RemoteException;
  
  void onTimeShiftStatusChanged(int paramInt) throws RemoteException;
  
  void onTrackSelected(int paramInt, String paramString) throws RemoteException;
  
  void onTracksChanged(List<TvTrackInfo> paramList) throws RemoteException;
  
  void onTuned(Uri paramUri) throws RemoteException;
  
  void onVideoAvailable() throws RemoteException;
  
  void onVideoUnavailable(int paramInt) throws RemoteException;
  
  class Default implements ITvInputSessionCallback {
    public void onSessionCreated(ITvInputSession param1ITvInputSession, IBinder param1IBinder) throws RemoteException {}
    
    public void onSessionEvent(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void onChannelRetuned(Uri param1Uri) throws RemoteException {}
    
    public void onTracksChanged(List<TvTrackInfo> param1List) throws RemoteException {}
    
    public void onTrackSelected(int param1Int, String param1String) throws RemoteException {}
    
    public void onVideoAvailable() throws RemoteException {}
    
    public void onVideoUnavailable(int param1Int) throws RemoteException {}
    
    public void onContentAllowed() throws RemoteException {}
    
    public void onContentBlocked(String param1String) throws RemoteException {}
    
    public void onLayoutSurface(int param1Int1, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public void onTimeShiftStatusChanged(int param1Int) throws RemoteException {}
    
    public void onTimeShiftStartPositionChanged(long param1Long) throws RemoteException {}
    
    public void onTimeShiftCurrentPositionChanged(long param1Long) throws RemoteException {}
    
    public void onTuned(Uri param1Uri) throws RemoteException {}
    
    public void onRecordingStopped(Uri param1Uri) throws RemoteException {}
    
    public void onError(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputSessionCallback {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputSessionCallback";
    
    static final int TRANSACTION_onChannelRetuned = 3;
    
    static final int TRANSACTION_onContentAllowed = 8;
    
    static final int TRANSACTION_onContentBlocked = 9;
    
    static final int TRANSACTION_onError = 16;
    
    static final int TRANSACTION_onLayoutSurface = 10;
    
    static final int TRANSACTION_onRecordingStopped = 15;
    
    static final int TRANSACTION_onSessionCreated = 1;
    
    static final int TRANSACTION_onSessionEvent = 2;
    
    static final int TRANSACTION_onTimeShiftCurrentPositionChanged = 13;
    
    static final int TRANSACTION_onTimeShiftStartPositionChanged = 12;
    
    static final int TRANSACTION_onTimeShiftStatusChanged = 11;
    
    static final int TRANSACTION_onTrackSelected = 5;
    
    static final int TRANSACTION_onTracksChanged = 4;
    
    static final int TRANSACTION_onTuned = 14;
    
    static final int TRANSACTION_onVideoAvailable = 6;
    
    static final int TRANSACTION_onVideoUnavailable = 7;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputSessionCallback");
    }
    
    public static ITvInputSessionCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputSessionCallback");
      if (iInterface != null && iInterface instanceof ITvInputSessionCallback)
        return (ITvInputSessionCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 16:
          return "onError";
        case 15:
          return "onRecordingStopped";
        case 14:
          return "onTuned";
        case 13:
          return "onTimeShiftCurrentPositionChanged";
        case 12:
          return "onTimeShiftStartPositionChanged";
        case 11:
          return "onTimeShiftStatusChanged";
        case 10:
          return "onLayoutSurface";
        case 9:
          return "onContentBlocked";
        case 8:
          return "onContentAllowed";
        case 7:
          return "onVideoUnavailable";
        case 6:
          return "onVideoAvailable";
        case 5:
          return "onTrackSelected";
        case 4:
          return "onTracksChanged";
        case 3:
          return "onChannelRetuned";
        case 2:
          return "onSessionEvent";
        case 1:
          break;
      } 
      return "onSessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ITvInputSession iTvInputSession;
      if (param1Int1 != 1598968902) {
        String str1;
        ArrayList<TvTrackInfo> arrayList;
        String str2;
        long l;
        int i, j;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 16:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            param1Int1 = param1Parcel1.readInt();
            onError(param1Int1);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onRecordingStopped((Uri)param1Parcel1);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onTuned((Uri)param1Parcel1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            l = param1Parcel1.readLong();
            onTimeShiftCurrentPositionChanged(l);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            l = param1Parcel1.readLong();
            onTimeShiftStartPositionChanged(l);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            param1Int1 = param1Parcel1.readInt();
            onTimeShiftStatusChanged(param1Int1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            param1Int1 = param1Parcel1.readInt();
            i = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onLayoutSurface(param1Int1, i, j, param1Int2);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            str1 = param1Parcel1.readString();
            onContentBlocked(str1);
            return true;
          case 8:
            str1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            onContentAllowed();
            return true;
          case 7:
            str1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            param1Int1 = str1.readInt();
            onVideoUnavailable(param1Int1);
            return true;
          case 6:
            str1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            onVideoAvailable();
            return true;
          case 5:
            str1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            onTrackSelected(param1Int1, str1);
            return true;
          case 4:
            str1.enforceInterface("android.media.tv.ITvInputSessionCallback");
            arrayList = str1.createTypedArrayList(TvTrackInfo.CREATOR);
            onTracksChanged(arrayList);
            return true;
          case 3:
            arrayList.enforceInterface("android.media.tv.ITvInputSessionCallback");
            if (arrayList.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            onChannelRetuned((Uri)arrayList);
            return true;
          case 2:
            arrayList.enforceInterface("android.media.tv.ITvInputSessionCallback");
            str2 = arrayList.readString();
            if (arrayList.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            onSessionEvent(str2, (Bundle)arrayList);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.media.tv.ITvInputSessionCallback");
        iTvInputSession = ITvInputSession.Stub.asInterface(arrayList.readStrongBinder());
        IBinder iBinder = arrayList.readStrongBinder();
        onSessionCreated(iTvInputSession, iBinder);
        return true;
      } 
      iTvInputSession.writeString("android.media.tv.ITvInputSessionCallback");
      return true;
    }
    
    private static class Proxy implements ITvInputSessionCallback {
      public static ITvInputSessionCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputSessionCallback";
      }
      
      public void onSessionCreated(ITvInputSession param2ITvInputSession, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          if (param2ITvInputSession != null) {
            iBinder = param2ITvInputSession.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onSessionCreated(param2ITvInputSession, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionEvent(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onSessionEvent(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onChannelRetuned(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onChannelRetuned(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTracksChanged(List<TvTrackInfo> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTracksChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTrackSelected(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTrackSelected(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVideoAvailable() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onVideoAvailable();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVideoUnavailable(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onVideoUnavailable(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onContentAllowed() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onContentAllowed();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onContentBlocked(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onContentBlocked(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLayoutSurface(int param2Int1, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onLayoutSurface(param2Int1, param2Int2, param2Int3, param2Int4);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftStatusChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTimeShiftStatusChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftStartPositionChanged(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTimeShiftStartPositionChanged(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftCurrentPositionChanged(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTimeShiftCurrentPositionChanged(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTuned(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onTuned(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecordingStopped(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onRecordingStopped(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSessionCallback");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSessionCallback.Stub.getDefaultImpl() != null) {
            ITvInputSessionCallback.Stub.getDefaultImpl().onError(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputSessionCallback param1ITvInputSessionCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputSessionCallback != null) {
          Proxy.sDefaultImpl = param1ITvInputSessionCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputSessionCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
