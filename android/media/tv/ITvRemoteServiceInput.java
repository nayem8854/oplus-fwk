package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITvRemoteServiceInput extends IInterface {
  void clearInputBridge(IBinder paramIBinder) throws RemoteException;
  
  void closeInputBridge(IBinder paramIBinder) throws RemoteException;
  
  void openGamepadBridge(IBinder paramIBinder, String paramString) throws RemoteException;
  
  void openInputBridge(IBinder paramIBinder, String paramString, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void sendGamepadAxisValue(IBinder paramIBinder, int paramInt, float paramFloat) throws RemoteException;
  
  void sendGamepadKeyDown(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void sendGamepadKeyUp(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void sendKeyDown(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void sendKeyUp(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void sendPointerDown(IBinder paramIBinder, int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void sendPointerSync(IBinder paramIBinder) throws RemoteException;
  
  void sendPointerUp(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void sendTimestamp(IBinder paramIBinder, long paramLong) throws RemoteException;
  
  class Default implements ITvRemoteServiceInput {
    public void openInputBridge(IBinder param1IBinder, String param1String, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void closeInputBridge(IBinder param1IBinder) throws RemoteException {}
    
    public void clearInputBridge(IBinder param1IBinder) throws RemoteException {}
    
    public void sendTimestamp(IBinder param1IBinder, long param1Long) throws RemoteException {}
    
    public void sendKeyDown(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void sendKeyUp(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void sendPointerDown(IBinder param1IBinder, int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void sendPointerUp(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void sendPointerSync(IBinder param1IBinder) throws RemoteException {}
    
    public void openGamepadBridge(IBinder param1IBinder, String param1String) throws RemoteException {}
    
    public void sendGamepadKeyDown(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void sendGamepadKeyUp(IBinder param1IBinder, int param1Int) throws RemoteException {}
    
    public void sendGamepadAxisValue(IBinder param1IBinder, int param1Int, float param1Float) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvRemoteServiceInput {
    private static final String DESCRIPTOR = "android.media.tv.ITvRemoteServiceInput";
    
    static final int TRANSACTION_clearInputBridge = 3;
    
    static final int TRANSACTION_closeInputBridge = 2;
    
    static final int TRANSACTION_openGamepadBridge = 10;
    
    static final int TRANSACTION_openInputBridge = 1;
    
    static final int TRANSACTION_sendGamepadAxisValue = 13;
    
    static final int TRANSACTION_sendGamepadKeyDown = 11;
    
    static final int TRANSACTION_sendGamepadKeyUp = 12;
    
    static final int TRANSACTION_sendKeyDown = 5;
    
    static final int TRANSACTION_sendKeyUp = 6;
    
    static final int TRANSACTION_sendPointerDown = 7;
    
    static final int TRANSACTION_sendPointerSync = 9;
    
    static final int TRANSACTION_sendPointerUp = 8;
    
    static final int TRANSACTION_sendTimestamp = 4;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvRemoteServiceInput");
    }
    
    public static ITvRemoteServiceInput asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvRemoteServiceInput");
      if (iInterface != null && iInterface instanceof ITvRemoteServiceInput)
        return (ITvRemoteServiceInput)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 13:
          return "sendGamepadAxisValue";
        case 12:
          return "sendGamepadKeyUp";
        case 11:
          return "sendGamepadKeyDown";
        case 10:
          return "openGamepadBridge";
        case 9:
          return "sendPointerSync";
        case 8:
          return "sendPointerUp";
        case 7:
          return "sendPointerDown";
        case 6:
          return "sendKeyUp";
        case 5:
          return "sendKeyDown";
        case 4:
          return "sendTimestamp";
        case 3:
          return "clearInputBridge";
        case 2:
          return "closeInputBridge";
        case 1:
          break;
      } 
      return "openInputBridge";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        IBinder iBinder1, iBinder2;
        float f;
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 13:
            param1Parcel1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            f = param1Parcel1.readFloat();
            sendGamepadAxisValue(iBinder2, param1Int1, f);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            sendGamepadKeyUp(iBinder2, param1Int1);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = param1Parcel1.readStrongBinder();
            param1Int1 = param1Parcel1.readInt();
            sendGamepadKeyDown(iBinder2, param1Int1);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = param1Parcel1.readStrongBinder();
            str1 = param1Parcel1.readString();
            openGamepadBridge(iBinder2, str1);
            return true;
          case 9:
            str1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder1 = str1.readStrongBinder();
            sendPointerSync(iBinder1);
            return true;
          case 8:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int1 = iBinder1.readInt();
            sendPointerUp(iBinder2, param1Int1);
            return true;
          case 7:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int2 = iBinder1.readInt();
            param1Int1 = iBinder1.readInt();
            i = iBinder1.readInt();
            sendPointerDown(iBinder2, param1Int2, param1Int1, i);
            return true;
          case 6:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int1 = iBinder1.readInt();
            sendKeyUp(iBinder2, param1Int1);
            return true;
          case 5:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int1 = iBinder1.readInt();
            sendKeyDown(iBinder2, param1Int1);
            return true;
          case 4:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder2 = iBinder1.readStrongBinder();
            l = iBinder1.readLong();
            sendTimestamp(iBinder2, l);
            return true;
          case 3:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder1 = iBinder1.readStrongBinder();
            clearInputBridge(iBinder1);
            return true;
          case 2:
            iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
            iBinder1 = iBinder1.readStrongBinder();
            closeInputBridge(iBinder1);
            return true;
          case 1:
            break;
        } 
        iBinder1.enforceInterface("android.media.tv.ITvRemoteServiceInput");
        IBinder iBinder3 = iBinder1.readStrongBinder();
        str = iBinder1.readString();
        int i = iBinder1.readInt();
        param1Int1 = iBinder1.readInt();
        param1Int2 = iBinder1.readInt();
        openInputBridge(iBinder3, str, i, param1Int1, param1Int2);
        return true;
      } 
      str.writeString("android.media.tv.ITvRemoteServiceInput");
      return true;
    }
    
    private static class Proxy implements ITvRemoteServiceInput {
      public static ITvRemoteServiceInput sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvRemoteServiceInput";
      }
      
      public void openInputBridge(IBinder param2IBinder, String param2String, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().openInputBridge(param2IBinder, param2String, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void closeInputBridge(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().closeInputBridge(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void clearInputBridge(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().clearInputBridge(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendTimestamp(IBinder param2IBinder, long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendTimestamp(param2IBinder, param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendKeyDown(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendKeyDown(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendKeyUp(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendKeyUp(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendPointerDown(IBinder param2IBinder, int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendPointerDown(param2IBinder, param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendPointerUp(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendPointerUp(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendPointerSync(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendPointerSync(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void openGamepadBridge(IBinder param2IBinder, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().openGamepadBridge(param2IBinder, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendGamepadKeyDown(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendGamepadKeyDown(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendGamepadKeyUp(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendGamepadKeyUp(param2IBinder, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void sendGamepadAxisValue(IBinder param2IBinder, int param2Int, float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteServiceInput");
          parcel.writeStrongBinder(param2IBinder);
          parcel.writeInt(param2Int);
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteServiceInput.Stub.getDefaultImpl() != null) {
            ITvRemoteServiceInput.Stub.getDefaultImpl().sendGamepadAxisValue(param2IBinder, param2Int, param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvRemoteServiceInput param1ITvRemoteServiceInput) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvRemoteServiceInput != null) {
          Proxy.sDefaultImpl = param1ITvRemoteServiceInput;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvRemoteServiceInput getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
