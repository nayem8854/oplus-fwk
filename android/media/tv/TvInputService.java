package android.media.tv;

import android.annotation.SystemApi;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.hardware.hdmi.HdmiDeviceInfo;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Process;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.util.Log;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventReceiver;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import com.android.internal.os.SomeArgs;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public abstract class TvInputService extends Service {
  private TvInputManager mTvInputManager;
  
  private final Handler mServiceHandler = new ServiceHandler();
  
  private final RemoteCallbackList<ITvInputServiceCallback> mCallbacks = new RemoteCallbackList<>();
  
  private static final String TAG = "TvInputService";
  
  public static final String SERVICE_META_DATA = "android.media.tv.input";
  
  public static final String SERVICE_INTERFACE = "android.media.tv.TvInputService";
  
  public static final int PRIORITY_HINT_USE_CASE_TYPE_SCAN = 200;
  
  public static final int PRIORITY_HINT_USE_CASE_TYPE_RECORD = 500;
  
  public static final int PRIORITY_HINT_USE_CASE_TYPE_PLAYBACK = 300;
  
  public static final int PRIORITY_HINT_USE_CASE_TYPE_LIVE = 400;
  
  public static final int PRIORITY_HINT_USE_CASE_TYPE_BACKGROUND = 100;
  
  private static final int DETACH_OVERLAY_VIEW_TIMEOUT_MS = 5000;
  
  private static final boolean DEBUG = false;
  
  public final IBinder onBind(Intent paramIntent) {
    return new ITvInputService.Stub() {
        final TvInputService this$0;
        
        public void registerCallback(ITvInputServiceCallback param1ITvInputServiceCallback) {
          if (param1ITvInputServiceCallback != null)
            TvInputService.this.mCallbacks.register(param1ITvInputServiceCallback); 
        }
        
        public void unregisterCallback(ITvInputServiceCallback param1ITvInputServiceCallback) {
          if (param1ITvInputServiceCallback != null)
            TvInputService.this.mCallbacks.unregister(param1ITvInputServiceCallback); 
        }
        
        public void createSession(InputChannel param1InputChannel, ITvInputSessionCallback param1ITvInputSessionCallback, String param1String1, String param1String2) {
          if (param1InputChannel == null)
            Log.w("TvInputService", "Creating session without input channel"); 
          if (param1ITvInputSessionCallback == null)
            return; 
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1InputChannel;
          someArgs.arg2 = param1ITvInputSessionCallback;
          someArgs.arg3 = param1String1;
          someArgs.arg4 = param1String2;
          TvInputService.this.mServiceHandler.obtainMessage(1, someArgs).sendToTarget();
        }
        
        public void createRecordingSession(ITvInputSessionCallback param1ITvInputSessionCallback, String param1String1, String param1String2) {
          if (param1ITvInputSessionCallback == null)
            return; 
          SomeArgs someArgs = SomeArgs.obtain();
          someArgs.arg1 = param1ITvInputSessionCallback;
          someArgs.arg2 = param1String1;
          someArgs.arg3 = param1String2;
          Message message = TvInputService.this.mServiceHandler.obtainMessage(3, someArgs);
          message.sendToTarget();
        }
        
        public void notifyHardwareAdded(TvInputHardwareInfo param1TvInputHardwareInfo) {
          Message message = TvInputService.this.mServiceHandler.obtainMessage(4, param1TvInputHardwareInfo);
          message.sendToTarget();
        }
        
        public void notifyHardwareRemoved(TvInputHardwareInfo param1TvInputHardwareInfo) {
          Message message = TvInputService.this.mServiceHandler.obtainMessage(5, param1TvInputHardwareInfo);
          message.sendToTarget();
        }
        
        public void notifyHdmiDeviceAdded(HdmiDeviceInfo param1HdmiDeviceInfo) {
          Message message = TvInputService.this.mServiceHandler.obtainMessage(6, param1HdmiDeviceInfo);
          message.sendToTarget();
        }
        
        public void notifyHdmiDeviceRemoved(HdmiDeviceInfo param1HdmiDeviceInfo) {
          Message message = TvInputService.this.mServiceHandler.obtainMessage(7, param1HdmiDeviceInfo);
          message.sendToTarget();
        }
        
        public void notifyHdmiDeviceUpdated(HdmiDeviceInfo param1HdmiDeviceInfo) {
          Message message = TvInputService.this.mServiceHandler.obtainMessage(8, param1HdmiDeviceInfo);
          message.sendToTarget();
        }
      };
  }
  
  public RecordingSession onCreateRecordingSession(String paramString) {
    return null;
  }
  
  public Session onCreateSession(String paramString1, String paramString2) {
    return onCreateSession(paramString1);
  }
  
  public RecordingSession onCreateRecordingSession(String paramString1, String paramString2) {
    return onCreateRecordingSession(paramString1);
  }
  
  @SystemApi
  public TvInputInfo onHardwareAdded(TvInputHardwareInfo paramTvInputHardwareInfo) {
    return null;
  }
  
  @SystemApi
  public String onHardwareRemoved(TvInputHardwareInfo paramTvInputHardwareInfo) {
    return null;
  }
  
  @SystemApi
  public TvInputInfo onHdmiDeviceAdded(HdmiDeviceInfo paramHdmiDeviceInfo) {
    return null;
  }
  
  @SystemApi
  public String onHdmiDeviceRemoved(HdmiDeviceInfo paramHdmiDeviceInfo) {
    return null;
  }
  
  @SystemApi
  public void onHdmiDeviceUpdated(HdmiDeviceInfo paramHdmiDeviceInfo) {}
  
  private boolean isPassthroughInput(String paramString) {
    boolean bool;
    if (this.mTvInputManager == null)
      this.mTvInputManager = (TvInputManager)getSystemService("tv_input"); 
    TvInputInfo tvInputInfo = this.mTvInputManager.getTvInputInfo(paramString);
    if (tvInputInfo != null && tvInputInfo.isPassthroughInput()) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  class Session implements KeyEvent.Callback {
    private final KeyEvent.DispatcherState mDispatcherState = new KeyEvent.DispatcherState();
    
    private long mStartPositionMs = Long.MIN_VALUE;
    
    private long mCurrentPositionMs = Long.MIN_VALUE;
    
    private final TimeShiftPositionTrackingRunnable mTimeShiftPositionTrackingRunnable = new TimeShiftPositionTrackingRunnable();
    
    private final Object mLock = new Object();
    
    private final List<Runnable> mPendingActions = new ArrayList<>();
    
    private static final int POSITION_UPDATE_INTERVAL_MS = 1000;
    
    private final Context mContext;
    
    final Handler mHandler;
    
    private Rect mOverlayFrame;
    
    private View mOverlayView;
    
    private TvInputService.OverlayViewCleanUpTask mOverlayViewCleanUpTask;
    
    private FrameLayout mOverlayViewContainer;
    
    private boolean mOverlayViewEnabled;
    
    private ITvInputSessionCallback mSessionCallback;
    
    private Surface mSurface;
    
    private final WindowManager mWindowManager;
    
    private WindowManager.LayoutParams mWindowParams;
    
    private IBinder mWindowToken;
    
    public Session(TvInputService this$0) {
      this.mContext = (Context)this$0;
      this.mWindowManager = (WindowManager)this$0.getSystemService("window");
      this.mHandler = new Handler(this$0.getMainLooper());
    }
    
    public void setOverlayViewEnabled(boolean param1Boolean) {
      this.mHandler.post((Runnable)new Object(this, param1Boolean));
    }
    
    @SystemApi
    public void notifySessionEvent(String param1String, Bundle param1Bundle) {
      Preconditions.checkNotNull(param1String);
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1String, param1Bundle));
    }
    
    public void notifyChannelRetuned(Uri param1Uri) {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Uri));
    }
    
    public void notifyTracksChanged(List<TvTrackInfo> param1List) {
      param1List = new ArrayList<>(param1List);
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1List));
    }
    
    public void notifyTrackSelected(int param1Int, String param1String) {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Int, param1String));
    }
    
    public void notifyVideoAvailable() {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this));
    }
    
    public void notifyVideoUnavailable(int param1Int) {
      if (param1Int < 0 || param1Int > 18) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("notifyVideoUnavailable - unknown reason: ");
        stringBuilder.append(param1Int);
        Log.e("TvInputService", stringBuilder.toString());
      } 
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Int));
    }
    
    public void notifyContentAllowed() {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this));
    }
    
    public void notifyContentBlocked(TvContentRating param1TvContentRating) {
      Preconditions.checkNotNull(param1TvContentRating);
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1TvContentRating));
    }
    
    public void notifyTimeShiftStatusChanged(int param1Int) {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Int));
    }
    
    private void notifyTimeShiftStartPositionChanged(long param1Long) {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Long));
    }
    
    private void notifyTimeShiftCurrentPositionChanged(long param1Long) {
      executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Long));
    }
    
    public void layoutSurface(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      if (param1Int1 <= param1Int3 && param1Int2 <= param1Int4) {
        executeOrPostRunnableOnMainThread((Runnable)new Object(this, param1Int1, param1Int2, param1Int3, param1Int4));
        return;
      } 
      throw new IllegalArgumentException("Invalid parameter");
    }
    
    @SystemApi
    public void onSetMain(boolean param1Boolean) {}
    
    public void onSurfaceChanged(int param1Int1, int param1Int2, int param1Int3) {}
    
    public void onOverlayViewSizeChanged(int param1Int1, int param1Int2) {}
    
    public boolean onTune(Uri param1Uri, Bundle param1Bundle) {
      return onTune(param1Uri);
    }
    
    public void onUnblockContent(TvContentRating param1TvContentRating) {}
    
    public boolean onSelectTrack(int param1Int, String param1String) {
      return false;
    }
    
    public void onAppPrivateCommand(String param1String, Bundle param1Bundle) {}
    
    public View onCreateOverlayView() {
      return null;
    }
    
    public void onTimeShiftPlay(Uri param1Uri) {}
    
    public void onTimeShiftPause() {}
    
    public void onTimeShiftResume() {}
    
    public void onTimeShiftSeekTo(long param1Long) {}
    
    public void onTimeShiftSetPlaybackParams(PlaybackParams param1PlaybackParams) {}
    
    public long onTimeShiftGetStartPosition() {
      return Long.MIN_VALUE;
    }
    
    public long onTimeShiftGetCurrentPosition() {
      return Long.MIN_VALUE;
    }
    
    public boolean onKeyDown(int param1Int, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onKeyLongPress(int param1Int, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onKeyMultiple(int param1Int1, int param1Int2, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onKeyUp(int param1Int, KeyEvent param1KeyEvent) {
      return false;
    }
    
    public boolean onTouchEvent(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onTrackballEvent(MotionEvent param1MotionEvent) {
      return false;
    }
    
    public boolean onGenericMotionEvent(MotionEvent param1MotionEvent) {
      return false;
    }
    
    void release() {
      onRelease();
      Surface surface = this.mSurface;
      if (surface != null) {
        surface.release();
        this.mSurface = null;
      } 
      synchronized (this.mLock) {
        this.mSessionCallback = null;
        this.mPendingActions.clear();
        removeOverlayView(true);
        this.mHandler.removeCallbacks(this.mTimeShiftPositionTrackingRunnable);
        return;
      } 
    }
    
    void setMain(boolean param1Boolean) {
      onSetMain(param1Boolean);
    }
    
    void setSurface(Surface param1Surface) {
      onSetSurface(param1Surface);
      Surface surface = this.mSurface;
      if (surface != null)
        surface.release(); 
      this.mSurface = param1Surface;
    }
    
    void dispatchSurfaceChanged(int param1Int1, int param1Int2, int param1Int3) {
      onSurfaceChanged(param1Int1, param1Int2, param1Int3);
    }
    
    void setStreamVolume(float param1Float) {
      onSetStreamVolume(param1Float);
    }
    
    void tune(Uri param1Uri, Bundle param1Bundle) {
      this.mCurrentPositionMs = Long.MIN_VALUE;
      onTune(param1Uri, param1Bundle);
    }
    
    void setCaptionEnabled(boolean param1Boolean) {
      onSetCaptionEnabled(param1Boolean);
    }
    
    void selectTrack(int param1Int, String param1String) {
      onSelectTrack(param1Int, param1String);
    }
    
    void unblockContent(String param1String) {
      onUnblockContent(TvContentRating.unflattenFromString(param1String));
    }
    
    void appPrivateCommand(String param1String, Bundle param1Bundle) {
      onAppPrivateCommand(param1String, param1Bundle);
    }
    
    void createOverlayView(IBinder param1IBinder, Rect param1Rect) {
      if (this.mOverlayViewContainer != null)
        removeOverlayView(false); 
      this.mWindowToken = param1IBinder;
      this.mOverlayFrame = param1Rect;
      onOverlayViewSizeChanged(param1Rect.right - param1Rect.left, param1Rect.bottom - param1Rect.top);
      if (!this.mOverlayViewEnabled)
        return; 
      View view = onCreateOverlayView();
      if (view == null)
        return; 
      TvInputService.OverlayViewCleanUpTask overlayViewCleanUpTask = this.mOverlayViewCleanUpTask;
      if (overlayViewCleanUpTask != null) {
        overlayViewCleanUpTask.cancel(true);
        this.mOverlayViewCleanUpTask = null;
      } 
      FrameLayout frameLayout = new FrameLayout(this.mContext.getApplicationContext());
      frameLayout.addView(this.mOverlayView);
      int i = 536;
      if (ActivityManager.isHighEndGfx())
        i = 0x218 | 0x1000000; 
      WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams(param1Rect.right - param1Rect.left, param1Rect.bottom - param1Rect.top, param1Rect.left, param1Rect.top, 1004, i, -2);
      layoutParams.privateFlags |= 0x40;
      this.mWindowParams.gravity = 8388659;
      this.mWindowParams.token = param1IBinder;
      this.mWindowManager.addView((View)this.mOverlayViewContainer, (ViewGroup.LayoutParams)this.mWindowParams);
    }
    
    void relayoutOverlayView(Rect param1Rect) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mOverlayFrame : Landroid/graphics/Rect;
      //   4: astore_2
      //   5: aload_2
      //   6: ifnull -> 36
      //   9: aload_2
      //   10: invokevirtual width : ()I
      //   13: aload_1
      //   14: invokevirtual width : ()I
      //   17: if_icmpne -> 36
      //   20: aload_0
      //   21: getfield mOverlayFrame : Landroid/graphics/Rect;
      //   24: astore_2
      //   25: aload_2
      //   26: invokevirtual height : ()I
      //   29: aload_1
      //   30: invokevirtual height : ()I
      //   33: if_icmpeq -> 58
      //   36: aload_0
      //   37: aload_1
      //   38: getfield right : I
      //   41: aload_1
      //   42: getfield left : I
      //   45: isub
      //   46: aload_1
      //   47: getfield bottom : I
      //   50: aload_1
      //   51: getfield top : I
      //   54: isub
      //   55: invokevirtual onOverlayViewSizeChanged : (II)V
      //   58: aload_0
      //   59: aload_1
      //   60: putfield mOverlayFrame : Landroid/graphics/Rect;
      //   63: aload_0
      //   64: getfield mOverlayViewEnabled : Z
      //   67: ifeq -> 152
      //   70: aload_0
      //   71: getfield mOverlayViewContainer : Landroid/widget/FrameLayout;
      //   74: ifnonnull -> 80
      //   77: goto -> 152
      //   80: aload_0
      //   81: getfield mWindowParams : Landroid/view/WindowManager$LayoutParams;
      //   84: aload_1
      //   85: getfield left : I
      //   88: putfield x : I
      //   91: aload_0
      //   92: getfield mWindowParams : Landroid/view/WindowManager$LayoutParams;
      //   95: aload_1
      //   96: getfield top : I
      //   99: putfield y : I
      //   102: aload_0
      //   103: getfield mWindowParams : Landroid/view/WindowManager$LayoutParams;
      //   106: aload_1
      //   107: getfield right : I
      //   110: aload_1
      //   111: getfield left : I
      //   114: isub
      //   115: putfield width : I
      //   118: aload_0
      //   119: getfield mWindowParams : Landroid/view/WindowManager$LayoutParams;
      //   122: aload_1
      //   123: getfield bottom : I
      //   126: aload_1
      //   127: getfield top : I
      //   130: isub
      //   131: putfield height : I
      //   134: aload_0
      //   135: getfield mWindowManager : Landroid/view/WindowManager;
      //   138: aload_0
      //   139: getfield mOverlayViewContainer : Landroid/widget/FrameLayout;
      //   142: aload_0
      //   143: getfield mWindowParams : Landroid/view/WindowManager$LayoutParams;
      //   146: invokeinterface updateViewLayout : (Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
      //   151: return
      //   152: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1385	-> 0
      //   #1386	-> 25
      //   #1389	-> 36
      //   #1391	-> 58
      //   #1392	-> 63
      //   #1395	-> 80
      //   #1396	-> 91
      //   #1397	-> 102
      //   #1398	-> 118
      //   #1399	-> 134
      //   #1400	-> 151
      //   #1393	-> 152
    }
    
    void removeOverlayView(boolean param1Boolean) {
      if (param1Boolean) {
        this.mWindowToken = null;
        this.mOverlayFrame = null;
      } 
      FrameLayout frameLayout = this.mOverlayViewContainer;
      if (frameLayout != null) {
        frameLayout.removeView(this.mOverlayView);
        this.mOverlayView = null;
        this.mWindowManager.removeView((View)this.mOverlayViewContainer);
        this.mOverlayViewContainer = null;
        this.mWindowParams = null;
      } 
    }
    
    void timeShiftPlay(Uri param1Uri) {
      this.mCurrentPositionMs = 0L;
      onTimeShiftPlay(param1Uri);
    }
    
    void timeShiftPause() {
      onTimeShiftPause();
    }
    
    void timeShiftResume() {
      onTimeShiftResume();
    }
    
    void timeShiftSeekTo(long param1Long) {
      onTimeShiftSeekTo(param1Long);
    }
    
    void timeShiftSetPlaybackParams(PlaybackParams param1PlaybackParams) {
      onTimeShiftSetPlaybackParams(param1PlaybackParams);
    }
    
    void timeShiftEnablePositionTracking(boolean param1Boolean) {
      if (param1Boolean) {
        this.mHandler.post(this.mTimeShiftPositionTrackingRunnable);
      } else {
        this.mHandler.removeCallbacks(this.mTimeShiftPositionTrackingRunnable);
        this.mStartPositionMs = Long.MIN_VALUE;
        this.mCurrentPositionMs = Long.MIN_VALUE;
      } 
    }
    
    void scheduleOverlayViewCleanup() {
      FrameLayout frameLayout = this.mOverlayViewContainer;
      if (frameLayout != null) {
        TvInputService.OverlayViewCleanUpTask overlayViewCleanUpTask = new TvInputService.OverlayViewCleanUpTask();
        overlayViewCleanUpTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new View[] { (View)frameLayout });
      } 
    }
    
    int dispatchInputEvent(InputEvent param1InputEvent, InputEventReceiver param1InputEventReceiver) {
      boolean bool1 = false;
      boolean bool = false;
      boolean bool2 = false;
      int i = 0;
      if (param1InputEvent instanceof KeyEvent) {
        KeyEvent keyEvent = (KeyEvent)param1InputEvent;
        if (keyEvent.dispatch(this, this.mDispatcherState, this))
          return 1; 
        bool = TvInputService.isNavigationKey(keyEvent.getKeyCode());
        if (KeyEvent.isMediaSessionKey(keyEvent.getKeyCode()) || 
          keyEvent.getKeyCode() == 222) {
          i = 1;
        } else {
          i = 0;
        } 
      } else if (param1InputEvent instanceof MotionEvent) {
        MotionEvent motionEvent = (MotionEvent)param1InputEvent;
        i = motionEvent.getSource();
        if (motionEvent.isTouchEvent()) {
          bool = bool1;
          i = bool2;
          if (onTouchEvent(motionEvent))
            return 1; 
        } else if ((i & 0x4) != 0) {
          bool = bool1;
          i = bool2;
          if (onTrackballEvent(motionEvent))
            return 1; 
        } else {
          bool = bool1;
          i = bool2;
          if (onGenericMotionEvent(motionEvent))
            return 1; 
        } 
      } 
      FrameLayout frameLayout = this.mOverlayViewContainer;
      if (frameLayout == null || !frameLayout.isAttachedToWindow() || i != 0)
        return 0; 
      if (!this.mOverlayViewContainer.hasWindowFocus())
        this.mOverlayViewContainer.getViewRootImpl().windowFocusChanged(true, true); 
      if (bool && this.mOverlayViewContainer.hasFocusable()) {
        this.mOverlayViewContainer.getViewRootImpl().dispatchInputEvent(param1InputEvent);
        return 1;
      } 
      this.mOverlayViewContainer.getViewRootImpl().dispatchInputEvent(param1InputEvent, param1InputEventReceiver);
      return -1;
    }
    
    private void initialize(ITvInputSessionCallback param1ITvInputSessionCallback) {
      synchronized (this.mLock) {
        this.mSessionCallback = param1ITvInputSessionCallback;
        for (Runnable runnable : this.mPendingActions)
          runnable.run(); 
        this.mPendingActions.clear();
        return;
      } 
    }
    
    private void executeOrPostRunnableOnMainThread(Runnable param1Runnable) {
      synchronized (this.mLock) {
        if (this.mSessionCallback == null) {
          this.mPendingActions.add(param1Runnable);
        } else if (this.mHandler.getLooper().isCurrentThread()) {
          param1Runnable.run();
        } else {
          this.mHandler.post(param1Runnable);
        } 
        return;
      } 
    }
    
    public abstract void onRelease();
    
    public abstract void onSetCaptionEnabled(boolean param1Boolean);
    
    public abstract void onSetStreamVolume(float param1Float);
    
    public abstract boolean onSetSurface(Surface param1Surface);
    
    public abstract boolean onTune(Uri param1Uri);
    
    class TimeShiftPositionTrackingRunnable implements Runnable {
      final TvInputService.Session this$0;
      
      private TimeShiftPositionTrackingRunnable() {}
      
      public void run() {
        // Byte code:
        //   0: aload_0
        //   1: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   4: invokevirtual onTimeShiftGetStartPosition : ()J
        //   7: lstore_1
        //   8: aload_0
        //   9: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   12: invokestatic access$900 : (Landroid/media/tv/TvInputService$Session;)J
        //   15: ldc2_w -9223372036854775808
        //   18: lcmp
        //   19: ifeq -> 36
        //   22: aload_0
        //   23: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   26: astore_3
        //   27: aload_3
        //   28: invokestatic access$900 : (Landroid/media/tv/TvInputService$Session;)J
        //   31: lload_1
        //   32: lcmp
        //   33: ifeq -> 53
        //   36: aload_0
        //   37: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   40: lload_1
        //   41: invokestatic access$902 : (Landroid/media/tv/TvInputService$Session;J)J
        //   44: pop2
        //   45: aload_0
        //   46: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   49: lload_1
        //   50: invokestatic access$1000 : (Landroid/media/tv/TvInputService$Session;J)V
        //   53: aload_0
        //   54: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   57: invokevirtual onTimeShiftGetCurrentPosition : ()J
        //   60: lstore #4
        //   62: lload #4
        //   64: lstore_1
        //   65: lload #4
        //   67: aload_0
        //   68: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   71: invokestatic access$900 : (Landroid/media/tv/TvInputService$Session;)J
        //   74: lcmp
        //   75: ifge -> 155
        //   78: new java/lang/StringBuilder
        //   81: dup
        //   82: invokespecial <init> : ()V
        //   85: astore #6
        //   87: aload #6
        //   89: ldc 'Current position ('
        //   91: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   94: pop
        //   95: aload #6
        //   97: lload #4
        //   99: invokevirtual append : (J)Ljava/lang/StringBuilder;
        //   102: pop
        //   103: aload #6
        //   105: ldc ') cannot be earlier than start position ('
        //   107: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   110: pop
        //   111: aload_0
        //   112: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   115: astore_3
        //   116: aload #6
        //   118: aload_3
        //   119: invokestatic access$900 : (Landroid/media/tv/TvInputService$Session;)J
        //   122: invokevirtual append : (J)Ljava/lang/StringBuilder;
        //   125: pop
        //   126: aload #6
        //   128: ldc '). Reset to the start position.'
        //   130: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
        //   133: pop
        //   134: aload #6
        //   136: invokevirtual toString : ()Ljava/lang/String;
        //   139: astore_3
        //   140: ldc 'TvInputService'
        //   142: aload_3
        //   143: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
        //   146: pop
        //   147: aload_0
        //   148: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   151: invokestatic access$900 : (Landroid/media/tv/TvInputService$Session;)J
        //   154: lstore_1
        //   155: aload_0
        //   156: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   159: invokestatic access$1100 : (Landroid/media/tv/TvInputService$Session;)J
        //   162: ldc2_w -9223372036854775808
        //   165: lcmp
        //   166: ifeq -> 183
        //   169: aload_0
        //   170: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   173: astore_3
        //   174: aload_3
        //   175: invokestatic access$1100 : (Landroid/media/tv/TvInputService$Session;)J
        //   178: lload_1
        //   179: lcmp
        //   180: ifeq -> 200
        //   183: aload_0
        //   184: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   187: lload_1
        //   188: invokestatic access$1102 : (Landroid/media/tv/TvInputService$Session;J)J
        //   191: pop2
        //   192: aload_0
        //   193: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   196: lload_1
        //   197: invokestatic access$1200 : (Landroid/media/tv/TvInputService$Session;J)V
        //   200: aload_0
        //   201: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   204: getfield mHandler : Landroid/os/Handler;
        //   207: aload_0
        //   208: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   211: invokestatic access$1300 : (Landroid/media/tv/TvInputService$Session;)Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;
        //   214: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)V
        //   217: aload_0
        //   218: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   221: getfield mHandler : Landroid/os/Handler;
        //   224: aload_0
        //   225: getfield this$0 : Landroid/media/tv/TvInputService$Session;
        //   228: invokestatic access$1300 : (Landroid/media/tv/TvInputService$Session;)Landroid/media/tv/TvInputService$Session$TimeShiftPositionTrackingRunnable;
        //   231: ldc2_w 1000
        //   234: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
        //   237: pop
        //   238: return
        // Line number table:
        //   Java source line number -> byte code offset
        //   #1573	-> 0
        //   #1574	-> 8
        //   #1575	-> 27
        //   #1576	-> 36
        //   #1577	-> 45
        //   #1579	-> 53
        //   #1580	-> 62
        //   #1581	-> 78
        //   #1582	-> 116
        //   #1581	-> 140
        //   #1584	-> 147
        //   #1586	-> 155
        //   #1587	-> 174
        //   #1588	-> 183
        //   #1589	-> 192
        //   #1591	-> 200
        //   #1592	-> 217
        //   #1594	-> 238
      }
    }
  }
  
  class OverlayViewCleanUpTask extends AsyncTask<View, Void, Void> {
    private OverlayViewCleanUpTask() {}
    
    protected Void doInBackground(View... param1VarArgs) {
      View view = param1VarArgs[0];
      try {
        Thread.sleep(5000L);
        if (isCancelled())
          return null; 
        if (view.isAttachedToWindow()) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Time out on releasing overlay view. Killing ");
          stringBuilder.append(view.getContext().getPackageName());
          String str = stringBuilder.toString();
          Log.e("TvInputService", str);
          Process.killProcess(Process.myPid());
        } 
        return null;
      } catch (InterruptedException interruptedException) {
        return null;
      } 
    }
  }
  
  class RecordingSession {
    final Handler mHandler;
    
    private final Object mLock = new Object();
    
    private final List<Runnable> mPendingActions = new ArrayList<>();
    
    private ITvInputSessionCallback mSessionCallback;
    
    public RecordingSession(TvInputService this$0) {
      this.mHandler = new Handler(this$0.getMainLooper());
    }
    
    public void notifyTuned(final Uri channelUri) {
      executeOrPostRunnableOnMainThread(new Runnable() {
            final TvInputService.RecordingSession this$0;
            
            final Uri val$channelUri;
            
            public void run() {
              try {
                if (TvInputService.RecordingSession.this.mSessionCallback != null)
                  TvInputService.RecordingSession.this.mSessionCallback.onTuned(channelUri); 
              } catch (RemoteException remoteException) {
                Log.w("TvInputService", "error in notifyTuned", (Throwable)remoteException);
              } 
            }
          });
    }
    
    public void notifyRecordingStopped(final Uri recordedProgramUri) {
      executeOrPostRunnableOnMainThread(new Runnable() {
            final TvInputService.RecordingSession this$0;
            
            final Uri val$recordedProgramUri;
            
            public void run() {
              try {
                if (TvInputService.RecordingSession.this.mSessionCallback != null)
                  TvInputService.RecordingSession.this.mSessionCallback.onRecordingStopped(recordedProgramUri); 
              } catch (RemoteException remoteException) {
                Log.w("TvInputService", "error in notifyRecordingStopped", (Throwable)remoteException);
              } 
            }
          });
    }
    
    public void notifyError(int param1Int) {
      // Byte code:
      //   0: iload_1
      //   1: iflt -> 11
      //   4: iload_1
      //   5: istore_2
      //   6: iload_1
      //   7: iconst_2
      //   8: if_icmple -> 51
      //   11: new java/lang/StringBuilder
      //   14: dup
      //   15: invokespecial <init> : ()V
      //   18: astore_3
      //   19: aload_3
      //   20: ldc 'notifyError - invalid error code ('
      //   22: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   25: pop
      //   26: aload_3
      //   27: iload_1
      //   28: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   31: pop
      //   32: aload_3
      //   33: ldc ') is changed to RECORDING_ERROR_UNKNOWN.'
      //   35: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   38: pop
      //   39: ldc 'TvInputService'
      //   41: aload_3
      //   42: invokevirtual toString : ()Ljava/lang/String;
      //   45: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   48: pop
      //   49: iconst_0
      //   50: istore_2
      //   51: aload_0
      //   52: new android/media/tv/TvInputService$RecordingSession$3
      //   55: dup
      //   56: aload_0
      //   57: iload_2
      //   58: invokespecial <init> : (Landroid/media/tv/TvInputService$RecordingSession;I)V
      //   61: invokespecial executeOrPostRunnableOnMainThread : (Ljava/lang/Runnable;)V
      //   64: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1714	-> 0
      //   #1716	-> 11
      //   #1718	-> 49
      //   #1720	-> 51
      //   #1721	-> 51
      //   #1735	-> 64
    }
    
    @SystemApi
    public void notifySessionEvent(final String eventType, final Bundle eventArgs) {
      Preconditions.checkNotNull(eventType);
      executeOrPostRunnableOnMainThread(new Runnable() {
            final TvInputService.RecordingSession this$0;
            
            final Bundle val$eventArgs;
            
            final String val$eventType;
            
            public void run() {
              try {
                if (TvInputService.RecordingSession.this.mSessionCallback != null)
                  TvInputService.RecordingSession.this.mSessionCallback.onSessionEvent(eventType, eventArgs); 
              } catch (RemoteException remoteException) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("error in sending event (event=");
                stringBuilder.append(eventType);
                stringBuilder.append(")");
                Log.w("TvInputService", stringBuilder.toString(), (Throwable)remoteException);
              } 
            }
          });
    }
    
    public void onTune(Uri param1Uri, Bundle param1Bundle) {
      onTune(param1Uri);
    }
    
    public void onStartRecording(Uri param1Uri, Bundle param1Bundle) {
      onStartRecording(param1Uri);
    }
    
    public void onAppPrivateCommand(String param1String, Bundle param1Bundle) {}
    
    void tune(Uri param1Uri, Bundle param1Bundle) {
      onTune(param1Uri, param1Bundle);
    }
    
    void release() {
      onRelease();
    }
    
    void startRecording(Uri param1Uri, Bundle param1Bundle) {
      onStartRecording(param1Uri, param1Bundle);
    }
    
    void stopRecording() {
      onStopRecording();
    }
    
    void appPrivateCommand(String param1String, Bundle param1Bundle) {
      onAppPrivateCommand(param1String, param1Bundle);
    }
    
    private void initialize(ITvInputSessionCallback param1ITvInputSessionCallback) {
      synchronized (this.mLock) {
        this.mSessionCallback = param1ITvInputSessionCallback;
        for (Runnable runnable : this.mPendingActions)
          runnable.run(); 
        this.mPendingActions.clear();
        return;
      } 
    }
    
    private void executeOrPostRunnableOnMainThread(Runnable param1Runnable) {
      synchronized (this.mLock) {
        if (this.mSessionCallback == null) {
          this.mPendingActions.add(param1Runnable);
        } else if (this.mHandler.getLooper().isCurrentThread()) {
          param1Runnable.run();
        } else {
          this.mHandler.post(param1Runnable);
        } 
        return;
      } 
    }
    
    public abstract void onRelease();
    
    public abstract void onStartRecording(Uri param1Uri);
    
    public abstract void onStopRecording();
    
    public abstract void onTune(Uri param1Uri);
  }
  
  class HardwareSession extends Session {
    private TvInputManager.Session mHardwareSession;
    
    private final TvInputManager.SessionCallback mHardwareSessionCallback;
    
    private ITvInputSession mProxySession;
    
    private ITvInputSessionCallback mProxySessionCallback;
    
    private Handler mServiceHandler;
    
    public HardwareSession(TvInputService this$0) {
      super((Context)this$0);
      this.mHardwareSessionCallback = (TvInputManager.SessionCallback)new Object(this);
    }
    
    public final boolean onSetSurface(Surface param1Surface) {
      Log.e("TvInputService", "onSetSurface() should not be called in HardwareProxySession.");
      return false;
    }
    
    public void onHardwareVideoAvailable() {}
    
    public void onHardwareVideoUnavailable(int param1Int) {}
    
    void release() {
      TvInputManager.Session session = this.mHardwareSession;
      if (session != null) {
        session.release();
        this.mHardwareSession = null;
      } 
      super.release();
    }
    
    public abstract String getHardwareInputId();
  }
  
  public static boolean isNavigationKey(int paramInt) {
    if (paramInt != 61 && paramInt != 62 && paramInt != 66 && paramInt != 92 && paramInt != 93 && paramInt != 122 && paramInt != 123)
      switch (paramInt) {
        default:
          return false;
        case 19:
        case 20:
        case 21:
        case 22:
        case 23:
          break;
      }  
    return true;
  }
  
  public abstract Session onCreateSession(String paramString);
  
  class ServiceHandler extends Handler {
    private static final int DO_ADD_HARDWARE_INPUT = 4;
    
    private static final int DO_ADD_HDMI_INPUT = 6;
    
    private static final int DO_CREATE_RECORDING_SESSION = 3;
    
    private static final int DO_CREATE_SESSION = 1;
    
    private static final int DO_NOTIFY_SESSION_CREATED = 2;
    
    private static final int DO_REMOVE_HARDWARE_INPUT = 5;
    
    private static final int DO_REMOVE_HDMI_INPUT = 7;
    
    private static final int DO_UPDATE_HDMI_INPUT = 8;
    
    final TvInputService this$0;
    
    private ServiceHandler() {}
    
    private void broadcastAddHardwareInput(int param1Int, TvInputInfo param1TvInputInfo) {
      int i = TvInputService.this.mCallbacks.beginBroadcast();
      for (byte b = 0; b < i; b++) {
        try {
          ((ITvInputServiceCallback)TvInputService.this.mCallbacks.getBroadcastItem(b)).addHardwareInput(param1Int, param1TvInputInfo);
        } catch (RemoteException remoteException) {
          Log.e("TvInputService", "error in broadcastAddHardwareInput", (Throwable)remoteException);
        } 
      } 
      TvInputService.this.mCallbacks.finishBroadcast();
    }
    
    private void broadcastAddHdmiInput(int param1Int, TvInputInfo param1TvInputInfo) {
      int i = TvInputService.this.mCallbacks.beginBroadcast();
      for (byte b = 0; b < i; b++) {
        try {
          ((ITvInputServiceCallback)TvInputService.this.mCallbacks.getBroadcastItem(b)).addHdmiInput(param1Int, param1TvInputInfo);
        } catch (RemoteException remoteException) {
          Log.e("TvInputService", "error in broadcastAddHdmiInput", (Throwable)remoteException);
        } 
      } 
      TvInputService.this.mCallbacks.finishBroadcast();
    }
    
    private void broadcastRemoveHardwareInput(String param1String) {
      int i = TvInputService.this.mCallbacks.beginBroadcast();
      for (byte b = 0; b < i; b++) {
        try {
          ((ITvInputServiceCallback)TvInputService.this.mCallbacks.getBroadcastItem(b)).removeHardwareInput(param1String);
        } catch (RemoteException remoteException) {
          Log.e("TvInputService", "error in broadcastRemoveHardwareInput", (Throwable)remoteException);
        } 
      } 
      TvInputService.this.mCallbacks.finishBroadcast();
    }
    
    public final void handleMessage(Message param1Message) {
      // Byte code:
      //   0: aload_1
      //   1: getfield what : I
      //   4: tableswitch default -> 52, 1 -> 420, 2 -> 340, 3 -> 220, 4 -> 189, 5 -> 162, 6 -> 131, 7 -> 104, 8 -> 87
      //   52: new java/lang/StringBuilder
      //   55: dup
      //   56: invokespecial <init> : ()V
      //   59: astore_2
      //   60: aload_2
      //   61: ldc 'Unhandled message code: '
      //   63: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   66: pop
      //   67: aload_2
      //   68: aload_1
      //   69: getfield what : I
      //   72: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   75: pop
      //   76: ldc 'TvInputService'
      //   78: aload_2
      //   79: invokevirtual toString : ()Ljava/lang/String;
      //   82: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   85: pop
      //   86: return
      //   87: aload_1
      //   88: getfield obj : Ljava/lang/Object;
      //   91: checkcast android/hardware/hdmi/HdmiDeviceInfo
      //   94: astore_1
      //   95: aload_0
      //   96: getfield this$0 : Landroid/media/tv/TvInputService;
      //   99: aload_1
      //   100: invokevirtual onHdmiDeviceUpdated : (Landroid/hardware/hdmi/HdmiDeviceInfo;)V
      //   103: return
      //   104: aload_1
      //   105: getfield obj : Ljava/lang/Object;
      //   108: checkcast android/hardware/hdmi/HdmiDeviceInfo
      //   111: astore_1
      //   112: aload_0
      //   113: getfield this$0 : Landroid/media/tv/TvInputService;
      //   116: aload_1
      //   117: invokevirtual onHdmiDeviceRemoved : (Landroid/hardware/hdmi/HdmiDeviceInfo;)Ljava/lang/String;
      //   120: astore_1
      //   121: aload_1
      //   122: ifnull -> 130
      //   125: aload_0
      //   126: aload_1
      //   127: invokespecial broadcastRemoveHardwareInput : (Ljava/lang/String;)V
      //   130: return
      //   131: aload_1
      //   132: getfield obj : Ljava/lang/Object;
      //   135: checkcast android/hardware/hdmi/HdmiDeviceInfo
      //   138: astore_1
      //   139: aload_0
      //   140: getfield this$0 : Landroid/media/tv/TvInputService;
      //   143: aload_1
      //   144: invokevirtual onHdmiDeviceAdded : (Landroid/hardware/hdmi/HdmiDeviceInfo;)Landroid/media/tv/TvInputInfo;
      //   147: astore_2
      //   148: aload_2
      //   149: ifnull -> 161
      //   152: aload_0
      //   153: aload_1
      //   154: invokevirtual getId : ()I
      //   157: aload_2
      //   158: invokespecial broadcastAddHdmiInput : (ILandroid/media/tv/TvInputInfo;)V
      //   161: return
      //   162: aload_1
      //   163: getfield obj : Ljava/lang/Object;
      //   166: checkcast android/media/tv/TvInputHardwareInfo
      //   169: astore_1
      //   170: aload_0
      //   171: getfield this$0 : Landroid/media/tv/TvInputService;
      //   174: aload_1
      //   175: invokevirtual onHardwareRemoved : (Landroid/media/tv/TvInputHardwareInfo;)Ljava/lang/String;
      //   178: astore_1
      //   179: aload_1
      //   180: ifnull -> 188
      //   183: aload_0
      //   184: aload_1
      //   185: invokespecial broadcastRemoveHardwareInput : (Ljava/lang/String;)V
      //   188: return
      //   189: aload_1
      //   190: getfield obj : Ljava/lang/Object;
      //   193: checkcast android/media/tv/TvInputHardwareInfo
      //   196: astore_1
      //   197: aload_0
      //   198: getfield this$0 : Landroid/media/tv/TvInputService;
      //   201: aload_1
      //   202: invokevirtual onHardwareAdded : (Landroid/media/tv/TvInputHardwareInfo;)Landroid/media/tv/TvInputInfo;
      //   205: astore_2
      //   206: aload_2
      //   207: ifnull -> 219
      //   210: aload_0
      //   211: aload_1
      //   212: invokevirtual getDeviceId : ()I
      //   215: aload_2
      //   216: invokespecial broadcastAddHardwareInput : (ILandroid/media/tv/TvInputInfo;)V
      //   219: return
      //   220: aload_1
      //   221: getfield obj : Ljava/lang/Object;
      //   224: checkcast com/android/internal/os/SomeArgs
      //   227: astore_3
      //   228: aload_3
      //   229: getfield arg1 : Ljava/lang/Object;
      //   232: checkcast android/media/tv/ITvInputSessionCallback
      //   235: astore_1
      //   236: aload_3
      //   237: getfield arg2 : Ljava/lang/Object;
      //   240: checkcast java/lang/String
      //   243: astore #4
      //   245: aload_3
      //   246: getfield arg3 : Ljava/lang/Object;
      //   249: checkcast java/lang/String
      //   252: astore_2
      //   253: aload_3
      //   254: invokevirtual recycle : ()V
      //   257: aload_0
      //   258: getfield this$0 : Landroid/media/tv/TvInputService;
      //   261: astore_3
      //   262: aload_3
      //   263: aload #4
      //   265: aload_2
      //   266: invokevirtual onCreateRecordingSession : (Ljava/lang/String;Ljava/lang/String;)Landroid/media/tv/TvInputService$RecordingSession;
      //   269: astore_2
      //   270: aload_2
      //   271: ifnonnull -> 296
      //   274: aload_1
      //   275: aconst_null
      //   276: aconst_null
      //   277: invokeinterface onSessionCreated : (Landroid/media/tv/ITvInputSession;Landroid/os/IBinder;)V
      //   282: goto -> 295
      //   285: astore_1
      //   286: ldc 'TvInputService'
      //   288: ldc 'error in onSessionCreated'
      //   290: aload_1
      //   291: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   294: pop
      //   295: return
      //   296: new android/media/tv/ITvInputSessionWrapper
      //   299: dup
      //   300: aload_0
      //   301: getfield this$0 : Landroid/media/tv/TvInputService;
      //   304: aload_2
      //   305: invokespecial <init> : (Landroid/content/Context;Landroid/media/tv/TvInputService$RecordingSession;)V
      //   308: astore #4
      //   310: aload_1
      //   311: aload #4
      //   313: aconst_null
      //   314: invokeinterface onSessionCreated : (Landroid/media/tv/ITvInputSession;Landroid/os/IBinder;)V
      //   319: goto -> 334
      //   322: astore #4
      //   324: ldc 'TvInputService'
      //   326: ldc 'error in onSessionCreated'
      //   328: aload #4
      //   330: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   333: pop
      //   334: aload_2
      //   335: aload_1
      //   336: invokestatic access$2200 : (Landroid/media/tv/TvInputService$RecordingSession;Landroid/media/tv/ITvInputSessionCallback;)V
      //   339: return
      //   340: aload_1
      //   341: getfield obj : Ljava/lang/Object;
      //   344: checkcast com/android/internal/os/SomeArgs
      //   347: astore_1
      //   348: aload_1
      //   349: getfield arg1 : Ljava/lang/Object;
      //   352: checkcast android/media/tv/TvInputService$Session
      //   355: astore #4
      //   357: aload_1
      //   358: getfield arg2 : Ljava/lang/Object;
      //   361: checkcast android/media/tv/ITvInputSession
      //   364: astore #5
      //   366: aload_1
      //   367: getfield arg3 : Ljava/lang/Object;
      //   370: checkcast android/media/tv/ITvInputSessionCallback
      //   373: astore_2
      //   374: aload_1
      //   375: getfield arg4 : Ljava/lang/Object;
      //   378: checkcast android/os/IBinder
      //   381: astore_3
      //   382: aload_2
      //   383: aload #5
      //   385: aload_3
      //   386: invokeinterface onSessionCreated : (Landroid/media/tv/ITvInputSession;Landroid/os/IBinder;)V
      //   391: goto -> 404
      //   394: astore_3
      //   395: ldc 'TvInputService'
      //   397: ldc 'error in onSessionCreated'
      //   399: aload_3
      //   400: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   403: pop
      //   404: aload #4
      //   406: ifnull -> 415
      //   409: aload #4
      //   411: aload_2
      //   412: invokestatic access$2100 : (Landroid/media/tv/TvInputService$Session;Landroid/media/tv/ITvInputSessionCallback;)V
      //   415: aload_1
      //   416: invokevirtual recycle : ()V
      //   419: return
      //   420: aload_1
      //   421: getfield obj : Ljava/lang/Object;
      //   424: checkcast com/android/internal/os/SomeArgs
      //   427: astore #5
      //   429: aload #5
      //   431: getfield arg1 : Ljava/lang/Object;
      //   434: checkcast android/view/InputChannel
      //   437: astore_2
      //   438: aload #5
      //   440: getfield arg2 : Ljava/lang/Object;
      //   443: checkcast android/media/tv/ITvInputSessionCallback
      //   446: astore_1
      //   447: aload #5
      //   449: getfield arg3 : Ljava/lang/Object;
      //   452: checkcast java/lang/String
      //   455: astore_3
      //   456: aload #5
      //   458: getfield arg4 : Ljava/lang/Object;
      //   461: checkcast java/lang/String
      //   464: astore #4
      //   466: aload #5
      //   468: invokevirtual recycle : ()V
      //   471: aload_0
      //   472: getfield this$0 : Landroid/media/tv/TvInputService;
      //   475: aload_3
      //   476: aload #4
      //   478: invokevirtual onCreateSession : (Ljava/lang/String;Ljava/lang/String;)Landroid/media/tv/TvInputService$Session;
      //   481: astore #4
      //   483: aload #4
      //   485: ifnonnull -> 510
      //   488: aload_1
      //   489: aconst_null
      //   490: aconst_null
      //   491: invokeinterface onSessionCreated : (Landroid/media/tv/ITvInputSession;Landroid/os/IBinder;)V
      //   496: goto -> 509
      //   499: astore_1
      //   500: ldc 'TvInputService'
      //   502: ldc 'error in onSessionCreated'
      //   504: aload_1
      //   505: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   508: pop
      //   509: return
      //   510: new android/media/tv/ITvInputSessionWrapper
      //   513: dup
      //   514: aload_0
      //   515: getfield this$0 : Landroid/media/tv/TvInputService;
      //   518: aload #4
      //   520: aload_2
      //   521: invokespecial <init> : (Landroid/content/Context;Landroid/media/tv/TvInputService$Session;Landroid/view/InputChannel;)V
      //   524: astore #5
      //   526: aload #4
      //   528: instanceof android/media/tv/TvInputService$HardwareSession
      //   531: ifeq -> 711
      //   534: aload #4
      //   536: checkcast android/media/tv/TvInputService$HardwareSession
      //   539: astore_3
      //   540: aload_3
      //   541: invokevirtual getHardwareInputId : ()Ljava/lang/String;
      //   544: astore_2
      //   545: aload_2
      //   546: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
      //   549: ifne -> 633
      //   552: aload_0
      //   553: getfield this$0 : Landroid/media/tv/TvInputService;
      //   556: astore #6
      //   558: aload #6
      //   560: aload_2
      //   561: invokestatic access$1900 : (Landroid/media/tv/TvInputService;Ljava/lang/String;)Z
      //   564: ifne -> 570
      //   567: goto -> 633
      //   570: aload_3
      //   571: aload #5
      //   573: invokestatic access$1602 : (Landroid/media/tv/TvInputService$HardwareSession;Landroid/media/tv/ITvInputSession;)Landroid/media/tv/ITvInputSession;
      //   576: pop
      //   577: aload_3
      //   578: aload_1
      //   579: invokestatic access$1702 : (Landroid/media/tv/TvInputService$HardwareSession;Landroid/media/tv/ITvInputSessionCallback;)Landroid/media/tv/ITvInputSessionCallback;
      //   582: pop
      //   583: aload_3
      //   584: aload_0
      //   585: getfield this$0 : Landroid/media/tv/TvInputService;
      //   588: invokestatic access$200 : (Landroid/media/tv/TvInputService;)Landroid/os/Handler;
      //   591: invokestatic access$1802 : (Landroid/media/tv/TvInputService$HardwareSession;Landroid/os/Handler;)Landroid/os/Handler;
      //   594: pop
      //   595: aload_0
      //   596: getfield this$0 : Landroid/media/tv/TvInputService;
      //   599: ldc 'tv_input'
      //   601: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
      //   604: checkcast android/media/tv/TvInputManager
      //   607: astore_1
      //   608: aload_3
      //   609: invokestatic access$2000 : (Landroid/media/tv/TvInputService$HardwareSession;)Landroid/media/tv/TvInputManager$SessionCallback;
      //   612: astore_3
      //   613: aload_0
      //   614: getfield this$0 : Landroid/media/tv/TvInputService;
      //   617: invokestatic access$200 : (Landroid/media/tv/TvInputService;)Landroid/os/Handler;
      //   620: astore #4
      //   622: aload_1
      //   623: aload_2
      //   624: aload_3
      //   625: aload #4
      //   627: invokevirtual createSession : (Ljava/lang/String;Landroid/media/tv/TvInputManager$SessionCallback;Landroid/os/Handler;)V
      //   630: goto -> 754
      //   633: aload_2
      //   634: invokestatic isEmpty : (Ljava/lang/CharSequence;)Z
      //   637: ifeq -> 652
      //   640: ldc 'TvInputService'
      //   642: ldc_w 'Hardware input id is not setup yet.'
      //   645: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   648: pop
      //   649: goto -> 684
      //   652: new java/lang/StringBuilder
      //   655: dup
      //   656: invokespecial <init> : ()V
      //   659: astore_3
      //   660: aload_3
      //   661: ldc_w 'Invalid hardware input id : '
      //   664: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   667: pop
      //   668: aload_3
      //   669: aload_2
      //   670: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   673: pop
      //   674: ldc 'TvInputService'
      //   676: aload_3
      //   677: invokevirtual toString : ()Ljava/lang/String;
      //   680: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   683: pop
      //   684: aload #4
      //   686: invokevirtual onRelease : ()V
      //   689: aload_1
      //   690: aconst_null
      //   691: aconst_null
      //   692: invokeinterface onSessionCreated : (Landroid/media/tv/ITvInputSession;Landroid/os/IBinder;)V
      //   697: goto -> 710
      //   700: astore_1
      //   701: ldc 'TvInputService'
      //   703: ldc 'error in onSessionCreated'
      //   705: aload_1
      //   706: invokestatic e : (Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   709: pop
      //   710: return
      //   711: invokestatic obtain : ()Lcom/android/internal/os/SomeArgs;
      //   714: astore_2
      //   715: aload_2
      //   716: aload #4
      //   718: putfield arg1 : Ljava/lang/Object;
      //   721: aload_2
      //   722: aload #5
      //   724: putfield arg2 : Ljava/lang/Object;
      //   727: aload_2
      //   728: aload_1
      //   729: putfield arg3 : Ljava/lang/Object;
      //   732: aload_2
      //   733: aconst_null
      //   734: putfield arg4 : Ljava/lang/Object;
      //   737: aload_0
      //   738: getfield this$0 : Landroid/media/tv/TvInputService;
      //   741: invokestatic access$200 : (Landroid/media/tv/TvInputService;)Landroid/os/Handler;
      //   744: iconst_2
      //   745: aload_2
      //   746: invokevirtual obtainMessage : (ILjava/lang/Object;)Landroid/os/Message;
      //   749: astore_1
      //   750: aload_1
      //   751: invokevirtual sendToTarget : ()V
      //   754: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2132	-> 0
      //   #2271	-> 52
      //   #2272	-> 86
      //   #2266	-> 87
      //   #2267	-> 95
      //   #2268	-> 103
      //   #2258	-> 104
      //   #2259	-> 112
      //   #2260	-> 121
      //   #2261	-> 125
      //   #2263	-> 130
      //   #2250	-> 131
      //   #2251	-> 139
      //   #2252	-> 148
      //   #2253	-> 152
      //   #2255	-> 161
      //   #2242	-> 162
      //   #2243	-> 170
      //   #2244	-> 179
      //   #2245	-> 183
      //   #2247	-> 188
      //   #2234	-> 189
      //   #2235	-> 197
      //   #2236	-> 206
      //   #2237	-> 210
      //   #2239	-> 219
      //   #2207	-> 220
      //   #2208	-> 228
      //   #2209	-> 236
      //   #2210	-> 245
      //   #2211	-> 253
      //   #2212	-> 257
      //   #2213	-> 262
      //   #2214	-> 270
      //   #2217	-> 274
      //   #2220	-> 282
      //   #2218	-> 285
      //   #2219	-> 286
      //   #2221	-> 295
      //   #2223	-> 296
      //   #2226	-> 310
      //   #2229	-> 319
      //   #2227	-> 322
      //   #2228	-> 324
      //   #2230	-> 334
      //   #2231	-> 339
      //   #2190	-> 340
      //   #2191	-> 348
      //   #2192	-> 357
      //   #2193	-> 366
      //   #2194	-> 374
      //   #2196	-> 382
      //   #2199	-> 391
      //   #2197	-> 394
      //   #2198	-> 395
      //   #2200	-> 404
      //   #2201	-> 409
      //   #2203	-> 415
      //   #2204	-> 419
      //   #2134	-> 420
      //   #2135	-> 429
      //   #2136	-> 438
      //   #2137	-> 447
      //   #2138	-> 456
      //   #2139	-> 466
      //   #2140	-> 471
      //   #2141	-> 483
      //   #2144	-> 488
      //   #2147	-> 496
      //   #2145	-> 499
      //   #2146	-> 500
      //   #2148	-> 509
      //   #2150	-> 510
      //   #2152	-> 526
      //   #2153	-> 534
      //   #2155	-> 540
      //   #2156	-> 545
      //   #2157	-> 558
      //   #2171	-> 570
      //   #2172	-> 577
      //   #2173	-> 583
      //   #2174	-> 595
      //   #2176	-> 608
      //   #2177	-> 608
      //   #2176	-> 622
      //   #2178	-> 630
      //   #2158	-> 633
      //   #2159	-> 640
      //   #2161	-> 652
      //   #2163	-> 684
      //   #2165	-> 689
      //   #2168	-> 697
      //   #2166	-> 700
      //   #2167	-> 701
      //   #2169	-> 710
      //   #2179	-> 711
      //   #2180	-> 715
      //   #2181	-> 721
      //   #2182	-> 727
      //   #2183	-> 732
      //   #2184	-> 737
      //   #2185	-> 750
      //   #2187	-> 754
      // Exception table:
      //   from	to	target	type
      //   274	282	285	android/os/RemoteException
      //   310	319	322	android/os/RemoteException
      //   382	391	394	android/os/RemoteException
      //   488	496	499	android/os/RemoteException
      //   689	697	700	android/os/RemoteException
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PriorityHintUseCaseType implements Annotation {}
}
