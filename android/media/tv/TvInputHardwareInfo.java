package android.media.tv;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@SystemApi
public final class TvInputHardwareInfo implements Parcelable {
  public static final int CABLE_CONNECTION_STATUS_CONNECTED = 1;
  
  public static final int CABLE_CONNECTION_STATUS_DISCONNECTED = 2;
  
  public static final int CABLE_CONNECTION_STATUS_UNKNOWN = 0;
  
  public static final Parcelable.Creator<TvInputHardwareInfo> CREATOR = new Parcelable.Creator<TvInputHardwareInfo>() {
      public TvInputHardwareInfo createFromParcel(Parcel param1Parcel) {
        try {
          TvInputHardwareInfo tvInputHardwareInfo = new TvInputHardwareInfo();
          this();
          tvInputHardwareInfo.readFromParcel(param1Parcel);
          return tvInputHardwareInfo;
        } catch (Exception exception) {
          Log.e("TvInputHardwareInfo", "Exception creating TvInputHardwareInfo from parcel", exception);
          return null;
        } 
      }
      
      public TvInputHardwareInfo[] newArray(int param1Int) {
        return new TvInputHardwareInfo[param1Int];
      }
    };
  
  static final String TAG = "TvInputHardwareInfo";
  
  public static final int TV_INPUT_TYPE_COMPONENT = 6;
  
  public static final int TV_INPUT_TYPE_COMPOSITE = 3;
  
  public static final int TV_INPUT_TYPE_DISPLAY_PORT = 10;
  
  public static final int TV_INPUT_TYPE_DVI = 8;
  
  public static final int TV_INPUT_TYPE_HDMI = 9;
  
  public static final int TV_INPUT_TYPE_OTHER_HARDWARE = 1;
  
  public static final int TV_INPUT_TYPE_SCART = 5;
  
  public static final int TV_INPUT_TYPE_SVIDEO = 4;
  
  public static final int TV_INPUT_TYPE_TUNER = 2;
  
  public static final int TV_INPUT_TYPE_VGA = 7;
  
  private String mAudioAddress;
  
  private int mAudioType;
  
  private int mCableConnectionStatus;
  
  private int mDeviceId;
  
  private int mHdmiPortId;
  
  private int mType;
  
  private TvInputHardwareInfo() {}
  
  public int getDeviceId() {
    return this.mDeviceId;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getAudioType() {
    return this.mAudioType;
  }
  
  public String getAudioAddress() {
    return this.mAudioAddress;
  }
  
  public int getHdmiPortId() {
    if (this.mType == 9)
      return this.mHdmiPortId; 
    throw new IllegalStateException();
  }
  
  public int getCableConnectionStatus() {
    return this.mCableConnectionStatus;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TvInputHardwareInfo {id=");
    stringBuilder.append(this.mDeviceId);
    stringBuilder.append(", type=");
    stringBuilder.append(this.mType);
    stringBuilder.append(", audio_type=");
    stringBuilder.append(this.mAudioType);
    stringBuilder.append(", audio_addr=");
    stringBuilder.append(this.mAudioAddress);
    if (this.mType == 9) {
      stringBuilder.append(", hdmi_port=");
      stringBuilder.append(this.mHdmiPortId);
    } 
    stringBuilder.append(", cable_connection_status=");
    stringBuilder.append(this.mCableConnectionStatus);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mDeviceId);
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mAudioType);
    paramParcel.writeString(this.mAudioAddress);
    if (this.mType == 9)
      paramParcel.writeInt(this.mHdmiPortId); 
    paramParcel.writeInt(this.mCableConnectionStatus);
  }
  
  public void readFromParcel(Parcel paramParcel) {
    this.mDeviceId = paramParcel.readInt();
    this.mType = paramParcel.readInt();
    this.mAudioType = paramParcel.readInt();
    this.mAudioAddress = paramParcel.readString();
    if (this.mType == 9)
      this.mHdmiPortId = paramParcel.readInt(); 
    this.mCableConnectionStatus = paramParcel.readInt();
  }
  
  class Builder {
    private Integer mDeviceId = null;
    
    private Integer mType = null;
    
    private int mAudioType = 0;
    
    private String mAudioAddress = "";
    
    private Integer mHdmiPortId = null;
    
    private Integer mCableConnectionStatus = Integer.valueOf(0);
    
    public Builder deviceId(int param1Int) {
      this.mDeviceId = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder type(int param1Int) {
      this.mType = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder audioType(int param1Int) {
      this.mAudioType = param1Int;
      return this;
    }
    
    public Builder audioAddress(String param1String) {
      this.mAudioAddress = param1String;
      return this;
    }
    
    public Builder hdmiPortId(int param1Int) {
      this.mHdmiPortId = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder cableConnectionStatus(int param1Int) {
      this.mCableConnectionStatus = Integer.valueOf(param1Int);
      return this;
    }
    
    public TvInputHardwareInfo build() {
      if (this.mDeviceId != null) {
        Integer integer = this.mType;
        if (integer != null) {
          if (integer.intValue() != 9 || this.mHdmiPortId != null) {
            integer = this.mType;
            if (integer.intValue() == 9 || this.mHdmiPortId == null) {
              TvInputHardwareInfo tvInputHardwareInfo = new TvInputHardwareInfo();
              TvInputHardwareInfo.access$102(tvInputHardwareInfo, this.mDeviceId.intValue());
              TvInputHardwareInfo.access$202(tvInputHardwareInfo, this.mType.intValue());
              TvInputHardwareInfo.access$302(tvInputHardwareInfo, this.mAudioType);
              if (tvInputHardwareInfo.mAudioType != 0)
                TvInputHardwareInfo.access$402(tvInputHardwareInfo, this.mAudioAddress); 
              Integer integer1 = this.mHdmiPortId;
              if (integer1 != null)
                TvInputHardwareInfo.access$502(tvInputHardwareInfo, integer1.intValue()); 
              TvInputHardwareInfo.access$602(tvInputHardwareInfo, this.mCableConnectionStatus.intValue());
              return tvInputHardwareInfo;
            } 
          } 
          throw new UnsupportedOperationException();
        } 
      } 
      throw new UnsupportedOperationException();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class CableConnectionStatus implements Annotation {}
}
