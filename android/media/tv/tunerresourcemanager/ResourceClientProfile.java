package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class ResourceClientProfile implements Parcelable {
  public static final Parcelable.Creator<ResourceClientProfile> CREATOR = new Parcelable.Creator<ResourceClientProfile>() {
      public ResourceClientProfile createFromParcel(Parcel param1Parcel) {
        try {
          return new ResourceClientProfile(param1Parcel);
        } catch (Exception exception) {
          Log.e("ResourceClientProfile", "Exception creating ResourceClientProfile from parcel", exception);
          return null;
        } 
      }
      
      public ResourceClientProfile[] newArray(int param1Int) {
        return new ResourceClientProfile[param1Int];
      }
    };
  
  static final String TAG = "ResourceClientProfile";
  
  private final String mTvInputSessionId;
  
  private final int mUseCase;
  
  private ResourceClientProfile(Parcel paramParcel) {
    this.mTvInputSessionId = paramParcel.readString();
    this.mUseCase = paramParcel.readInt();
  }
  
  public ResourceClientProfile(String paramString, int paramInt) {
    this.mTvInputSessionId = paramString;
    this.mUseCase = paramInt;
  }
  
  public String getTvInputSessionId() {
    return this.mTvInputSessionId;
  }
  
  public int getUseCase() {
    return this.mUseCase;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("ResourceClientProfile {tvInputSessionId=");
    stringBuilder.append(this.mTvInputSessionId);
    stringBuilder.append(", useCase=");
    stringBuilder.append(this.mUseCase);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mTvInputSessionId);
    paramParcel.writeInt(this.mUseCase);
  }
}
