package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class CasSessionRequest implements Parcelable {
  public static final Parcelable.Creator<CasSessionRequest> CREATOR = new Parcelable.Creator<CasSessionRequest>() {
      public CasSessionRequest createFromParcel(Parcel param1Parcel) {
        try {
          return new CasSessionRequest(param1Parcel);
        } catch (Exception exception) {
          Log.e("CasSessionRequest", "Exception creating CasSessionRequest from parcel", exception);
          return null;
        } 
      }
      
      public CasSessionRequest[] newArray(int param1Int) {
        return new CasSessionRequest[param1Int];
      }
    };
  
  static final String TAG = "CasSessionRequest";
  
  private final int mCasSystemId;
  
  private final int mClientId;
  
  private CasSessionRequest(Parcel paramParcel) {
    this.mClientId = paramParcel.readInt();
    this.mCasSystemId = paramParcel.readInt();
  }
  
  public CasSessionRequest(int paramInt1, int paramInt2) {
    this.mClientId = paramInt1;
    this.mCasSystemId = paramInt2;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public int getCasSystemId() {
    return this.mCasSystemId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("CasSessionRequest {clientId=");
    stringBuilder.append(this.mClientId);
    stringBuilder.append(", casSystemId=");
    stringBuilder.append(this.mCasSystemId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientId);
    paramParcel.writeInt(this.mCasSystemId);
  }
}
