package android.media.tv.tunerresourcemanager;

import android.os.RemoteException;
import android.util.Log;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.Executor;

public class TunerResourceManager {
  private static final boolean DEBUG = Log.isLoggable("TunerResourceManager", 3);
  
  public static final int INVALID_OWNER_ID = -1;
  
  public static final int INVALID_RESOURCE_HANDLE = -1;
  
  private static final String TAG = "TunerResourceManager";
  
  public static final int TUNER_RESOURCE_TYPE_CAS_SESSION = 4;
  
  public static final int TUNER_RESOURCE_TYPE_DEMUX = 1;
  
  public static final int TUNER_RESOURCE_TYPE_DESCRAMBLER = 2;
  
  public static final int TUNER_RESOURCE_TYPE_FRONTEND = 0;
  
  public static final int TUNER_RESOURCE_TYPE_LNB = 3;
  
  public static final int TUNER_RESOURCE_TYPE_MAX = 5;
  
  private final ITunerResourceManager mService;
  
  private final int mUserId;
  
  public TunerResourceManager(ITunerResourceManager paramITunerResourceManager, int paramInt) {
    this.mService = paramITunerResourceManager;
    this.mUserId = paramInt;
  }
  
  public void registerClientProfile(ResourceClientProfile paramResourceClientProfile, Executor paramExecutor, ResourcesReclaimListener paramResourcesReclaimListener, int[] paramArrayOfint) {
    try {
      ITunerResourceManager iTunerResourceManager = this.mService;
      Object object = new Object();
      super(this, paramExecutor, paramResourcesReclaimListener);
      iTunerResourceManager.registerClientProfile(paramResourceClientProfile, (IResourcesReclaimListener)object, paramArrayOfint);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterClientProfile(int paramInt) {
    try {
      this.mService.unregisterClientProfile(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean updateClientPriority(int paramInt1, int paramInt2, int paramInt3) {
    try {
      return this.mService.updateClientPriority(paramInt1, paramInt2, paramInt3);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setFrontendInfoList(TunerFrontendInfo[] paramArrayOfTunerFrontendInfo) {
    try {
      this.mService.setFrontendInfoList(paramArrayOfTunerFrontendInfo);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateCasInfo(int paramInt1, int paramInt2) {
    try {
      this.mService.updateCasInfo(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void setLnbInfoList(int[] paramArrayOfint) {
    try {
      this.mService.setLnbInfoList(paramArrayOfint);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestFrontend(TunerFrontendRequest paramTunerFrontendRequest, int[] paramArrayOfint) {
    try {
      return this.mService.requestFrontend(paramTunerFrontendRequest, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void shareFrontend(int paramInt1, int paramInt2) {
    try {
      this.mService.shareFrontend(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestDemux(TunerDemuxRequest paramTunerDemuxRequest, int[] paramArrayOfint) {
    try {
      return this.mService.requestDemux(paramTunerDemuxRequest, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestDescrambler(TunerDescramblerRequest paramTunerDescramblerRequest, int[] paramArrayOfint) {
    try {
      return this.mService.requestDescrambler(paramTunerDescramblerRequest, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestCasSession(CasSessionRequest paramCasSessionRequest, int[] paramArrayOfint) {
    try {
      return this.mService.requestCasSession(paramCasSessionRequest, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean requestLnb(TunerLnbRequest paramTunerLnbRequest, int[] paramArrayOfint) {
    try {
      return this.mService.requestLnb(paramTunerLnbRequest, paramArrayOfint);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releaseFrontend(int paramInt1, int paramInt2) {
    try {
      this.mService.releaseFrontend(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releaseDemux(int paramInt1, int paramInt2) {
    try {
      this.mService.releaseDemux(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releaseDescrambler(int paramInt1, int paramInt2) {
    try {
      this.mService.releaseDescrambler(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releaseCasSession(int paramInt1, int paramInt2) {
    try {
      this.mService.releaseCasSession(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void releaseLnb(int paramInt1, int paramInt2) {
    try {
      this.mService.releaseLnb(paramInt1, paramInt2);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isHigherPriority(ResourceClientProfile paramResourceClientProfile1, ResourceClientProfile paramResourceClientProfile2) {
    try {
      return this.mService.isHigherPriority(paramResourceClientProfile1, paramResourceClientProfile2);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public static abstract class ResourcesReclaimListener {
    public abstract void onReclaimResources();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface TunerResourceType {}
}
