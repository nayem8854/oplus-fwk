package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class TunerFrontendInfo implements Parcelable {
  public static final Parcelable.Creator<TunerFrontendInfo> CREATOR = new Parcelable.Creator<TunerFrontendInfo>() {
      public TunerFrontendInfo createFromParcel(Parcel param1Parcel) {
        try {
          return new TunerFrontendInfo(param1Parcel);
        } catch (Exception exception) {
          Log.e("TunerFrontendInfo", "Exception creating TunerFrontendInfo from parcel", exception);
          return null;
        } 
      }
      
      public TunerFrontendInfo[] newArray(int param1Int) {
        return new TunerFrontendInfo[param1Int];
      }
    };
  
  static final String TAG = "TunerFrontendInfo";
  
  private final int mExclusiveGroupId;
  
  private final int mFrontendType;
  
  private final int mId;
  
  private TunerFrontendInfo(Parcel paramParcel) {
    this.mId = paramParcel.readInt();
    this.mFrontendType = paramParcel.readInt();
    this.mExclusiveGroupId = paramParcel.readInt();
  }
  
  public TunerFrontendInfo(int paramInt1, int paramInt2, int paramInt3) {
    this.mId = paramInt1;
    this.mFrontendType = paramInt2;
    this.mExclusiveGroupId = paramInt3;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int getFrontendType() {
    return this.mFrontendType;
  }
  
  public int getExclusiveGroupId() {
    return this.mExclusiveGroupId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TunerFrontendInfo {id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", frontendType=");
    stringBuilder.append(this.mFrontendType);
    stringBuilder.append(", exclusiveGroupId=");
    stringBuilder.append(this.mExclusiveGroupId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mFrontendType);
    paramParcel.writeInt(this.mExclusiveGroupId);
  }
}
