package android.media.tv.tunerresourcemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IResourcesReclaimListener extends IInterface {
  void onReclaimResources() throws RemoteException;
  
  class Default implements IResourcesReclaimListener {
    public void onReclaimResources() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IResourcesReclaimListener {
    private static final String DESCRIPTOR = "android.media.tv.tunerresourcemanager.IResourcesReclaimListener";
    
    static final int TRANSACTION_onReclaimResources = 1;
    
    public Stub() {
      attachInterface(this, "android.media.tv.tunerresourcemanager.IResourcesReclaimListener");
    }
    
    public static IResourcesReclaimListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.tunerresourcemanager.IResourcesReclaimListener");
      if (iInterface != null && iInterface instanceof IResourcesReclaimListener)
        return (IResourcesReclaimListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onReclaimResources";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.tv.tunerresourcemanager.IResourcesReclaimListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.IResourcesReclaimListener");
      onReclaimResources();
      return true;
    }
    
    private static class Proxy implements IResourcesReclaimListener {
      public static IResourcesReclaimListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.tunerresourcemanager.IResourcesReclaimListener";
      }
      
      public void onReclaimResources() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.tunerresourcemanager.IResourcesReclaimListener");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IResourcesReclaimListener.Stub.getDefaultImpl() != null) {
            IResourcesReclaimListener.Stub.getDefaultImpl().onReclaimResources();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IResourcesReclaimListener param1IResourcesReclaimListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IResourcesReclaimListener != null) {
          Proxy.sDefaultImpl = param1IResourcesReclaimListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IResourcesReclaimListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
