package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class TunerDescramblerRequest implements Parcelable {
  public static final Parcelable.Creator<TunerDescramblerRequest> CREATOR = new Parcelable.Creator<TunerDescramblerRequest>() {
      public TunerDescramblerRequest createFromParcel(Parcel param1Parcel) {
        try {
          return new TunerDescramblerRequest(param1Parcel);
        } catch (Exception exception) {
          Log.e("TunerDescramblerRequest", "Exception creating TunerDescramblerRequest from parcel", exception);
          return null;
        } 
      }
      
      public TunerDescramblerRequest[] newArray(int param1Int) {
        return new TunerDescramblerRequest[param1Int];
      }
    };
  
  static final String TAG = "TunerDescramblerRequest";
  
  private final int mClientId;
  
  private TunerDescramblerRequest(Parcel paramParcel) {
    this.mClientId = paramParcel.readInt();
  }
  
  public TunerDescramblerRequest(int paramInt) {
    this.mClientId = paramInt;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TunerDescramblerRequest {clientId=");
    stringBuilder.append(this.mClientId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientId);
  }
}
