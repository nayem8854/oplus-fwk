package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class TunerLnbRequest implements Parcelable {
  public static final Parcelable.Creator<TunerLnbRequest> CREATOR = new Parcelable.Creator<TunerLnbRequest>() {
      public TunerLnbRequest createFromParcel(Parcel param1Parcel) {
        try {
          return new TunerLnbRequest(param1Parcel);
        } catch (Exception exception) {
          Log.e("TunerLnbRequest", "Exception creating TunerLnbRequest from parcel", exception);
          return null;
        } 
      }
      
      public TunerLnbRequest[] newArray(int param1Int) {
        return new TunerLnbRequest[param1Int];
      }
    };
  
  static final String TAG = "TunerLnbRequest";
  
  private final int mClientId;
  
  private TunerLnbRequest(Parcel paramParcel) {
    this.mClientId = paramParcel.readInt();
  }
  
  public TunerLnbRequest(int paramInt) {
    this.mClientId = paramInt;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TunerLnbRequest {clientId=");
    stringBuilder.append(this.mClientId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientId);
  }
}
