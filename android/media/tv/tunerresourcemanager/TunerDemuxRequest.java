package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class TunerDemuxRequest implements Parcelable {
  public static final Parcelable.Creator<TunerDemuxRequest> CREATOR = new Parcelable.Creator<TunerDemuxRequest>() {
      public TunerDemuxRequest createFromParcel(Parcel param1Parcel) {
        try {
          return new TunerDemuxRequest(param1Parcel);
        } catch (Exception exception) {
          Log.e("TunerDemuxRequest", "Exception creating TunerDemuxRequest from parcel", exception);
          return null;
        } 
      }
      
      public TunerDemuxRequest[] newArray(int param1Int) {
        return new TunerDemuxRequest[param1Int];
      }
    };
  
  static final String TAG = "TunerDemuxRequest";
  
  private final int mClientId;
  
  private TunerDemuxRequest(Parcel paramParcel) {
    this.mClientId = paramParcel.readInt();
  }
  
  public TunerDemuxRequest(int paramInt) {
    this.mClientId = paramInt;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TunerDemuxRequest {clientId=");
    stringBuilder.append(this.mClientId);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientId);
  }
}
