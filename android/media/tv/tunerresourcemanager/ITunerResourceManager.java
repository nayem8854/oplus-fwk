package android.media.tv.tunerresourcemanager;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITunerResourceManager extends IInterface {
  boolean isHigherPriority(ResourceClientProfile paramResourceClientProfile1, ResourceClientProfile paramResourceClientProfile2) throws RemoteException;
  
  void registerClientProfile(ResourceClientProfile paramResourceClientProfile, IResourcesReclaimListener paramIResourcesReclaimListener, int[] paramArrayOfint) throws RemoteException;
  
  void releaseCasSession(int paramInt1, int paramInt2) throws RemoteException;
  
  void releaseDemux(int paramInt1, int paramInt2) throws RemoteException;
  
  void releaseDescrambler(int paramInt1, int paramInt2) throws RemoteException;
  
  void releaseFrontend(int paramInt1, int paramInt2) throws RemoteException;
  
  void releaseLnb(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean requestCasSession(CasSessionRequest paramCasSessionRequest, int[] paramArrayOfint) throws RemoteException;
  
  boolean requestDemux(TunerDemuxRequest paramTunerDemuxRequest, int[] paramArrayOfint) throws RemoteException;
  
  boolean requestDescrambler(TunerDescramblerRequest paramTunerDescramblerRequest, int[] paramArrayOfint) throws RemoteException;
  
  boolean requestFrontend(TunerFrontendRequest paramTunerFrontendRequest, int[] paramArrayOfint) throws RemoteException;
  
  boolean requestLnb(TunerLnbRequest paramTunerLnbRequest, int[] paramArrayOfint) throws RemoteException;
  
  void setFrontendInfoList(TunerFrontendInfo[] paramArrayOfTunerFrontendInfo) throws RemoteException;
  
  void setLnbInfoList(int[] paramArrayOfint) throws RemoteException;
  
  void shareFrontend(int paramInt1, int paramInt2) throws RemoteException;
  
  void unregisterClientProfile(int paramInt) throws RemoteException;
  
  void updateCasInfo(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean updateClientPriority(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  class Default implements ITunerResourceManager {
    public void registerClientProfile(ResourceClientProfile param1ResourceClientProfile, IResourcesReclaimListener param1IResourcesReclaimListener, int[] param1ArrayOfint) throws RemoteException {}
    
    public void unregisterClientProfile(int param1Int) throws RemoteException {}
    
    public boolean updateClientPriority(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {
      return false;
    }
    
    public void setFrontendInfoList(TunerFrontendInfo[] param1ArrayOfTunerFrontendInfo) throws RemoteException {}
    
    public void updateCasInfo(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void setLnbInfoList(int[] param1ArrayOfint) throws RemoteException {}
    
    public boolean requestFrontend(TunerFrontendRequest param1TunerFrontendRequest, int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public void shareFrontend(int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean requestDemux(TunerDemuxRequest param1TunerDemuxRequest, int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public boolean requestDescrambler(TunerDescramblerRequest param1TunerDescramblerRequest, int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public boolean requestCasSession(CasSessionRequest param1CasSessionRequest, int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public boolean requestLnb(TunerLnbRequest param1TunerLnbRequest, int[] param1ArrayOfint) throws RemoteException {
      return false;
    }
    
    public void releaseFrontend(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseDemux(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseDescrambler(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseCasSession(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void releaseLnb(int param1Int1, int param1Int2) throws RemoteException {}
    
    public boolean isHigherPriority(ResourceClientProfile param1ResourceClientProfile1, ResourceClientProfile param1ResourceClientProfile2) throws RemoteException {
      return false;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITunerResourceManager {
    private static final String DESCRIPTOR = "android.media.tv.tunerresourcemanager.ITunerResourceManager";
    
    static final int TRANSACTION_isHigherPriority = 18;
    
    static final int TRANSACTION_registerClientProfile = 1;
    
    static final int TRANSACTION_releaseCasSession = 16;
    
    static final int TRANSACTION_releaseDemux = 14;
    
    static final int TRANSACTION_releaseDescrambler = 15;
    
    static final int TRANSACTION_releaseFrontend = 13;
    
    static final int TRANSACTION_releaseLnb = 17;
    
    static final int TRANSACTION_requestCasSession = 11;
    
    static final int TRANSACTION_requestDemux = 9;
    
    static final int TRANSACTION_requestDescrambler = 10;
    
    static final int TRANSACTION_requestFrontend = 7;
    
    static final int TRANSACTION_requestLnb = 12;
    
    static final int TRANSACTION_setFrontendInfoList = 4;
    
    static final int TRANSACTION_setLnbInfoList = 6;
    
    static final int TRANSACTION_shareFrontend = 8;
    
    static final int TRANSACTION_unregisterClientProfile = 2;
    
    static final int TRANSACTION_updateCasInfo = 5;
    
    static final int TRANSACTION_updateClientPriority = 3;
    
    public Stub() {
      attachInterface(this, "android.media.tv.tunerresourcemanager.ITunerResourceManager");
    }
    
    public static ITunerResourceManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
      if (iInterface != null && iInterface instanceof ITunerResourceManager)
        return (ITunerResourceManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 18:
          return "isHigherPriority";
        case 17:
          return "releaseLnb";
        case 16:
          return "releaseCasSession";
        case 15:
          return "releaseDescrambler";
        case 14:
          return "releaseDemux";
        case 13:
          return "releaseFrontend";
        case 12:
          return "requestLnb";
        case 11:
          return "requestCasSession";
        case 10:
          return "requestDescrambler";
        case 9:
          return "requestDemux";
        case 8:
          return "shareFrontend";
        case 7:
          return "requestFrontend";
        case 6:
          return "setLnbInfoList";
        case 5:
          return "updateCasInfo";
        case 4:
          return "setFrontendInfoList";
        case 3:
          return "updateClientPriority";
        case 2:
          return "unregisterClientProfile";
        case 1:
          break;
      } 
      return "registerClientProfile";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool7;
        int i2;
        boolean bool6;
        int i1;
        boolean bool5;
        int n;
        boolean bool4;
        int m;
        boolean bool3;
        int k;
        boolean bool2;
        int j;
        boolean bool1;
        int[] arrayOfInt2;
        TunerFrontendInfo[] arrayOfTunerFrontendInfo;
        int[] arrayOfInt1;
        ResourceClientProfile resourceClientProfile;
        int i3;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 18:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (param1Parcel1.readInt() != 0) {
              resourceClientProfile = ResourceClientProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              resourceClientProfile = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              ResourceClientProfile resourceClientProfile1 = ResourceClientProfile.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            bool7 = isHigherPriority(resourceClientProfile, (ResourceClientProfile)param1Parcel1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool7);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            i2 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            releaseLnb(i2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            i2 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            releaseCasSession(i2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            param1Int2 = param1Parcel1.readInt();
            i2 = param1Parcel1.readInt();
            releaseDescrambler(param1Int2, i2);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            i2 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            releaseDemux(i2, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            param1Int2 = param1Parcel1.readInt();
            i2 = param1Parcel1.readInt();
            releaseFrontend(param1Int2, i2);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (param1Parcel1.readInt() != 0) {
              TunerLnbRequest tunerLnbRequest = TunerLnbRequest.CREATOR.createFromParcel(param1Parcel1);
            } else {
              resourceClientProfile = null;
            } 
            i2 = param1Parcel1.readInt();
            if (i2 < 0) {
              param1Parcel1 = null;
            } else {
              arrayOfInt2 = new int[i2];
            } 
            bool6 = requestLnb((TunerLnbRequest)resourceClientProfile, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool6);
            param1Parcel2.writeIntArray(arrayOfInt2);
            return true;
          case 11:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (arrayOfInt2.readInt() != 0) {
              CasSessionRequest casSessionRequest = CasSessionRequest.CREATOR.createFromParcel((Parcel)arrayOfInt2);
            } else {
              resourceClientProfile = null;
            } 
            i1 = arrayOfInt2.readInt();
            if (i1 < 0) {
              arrayOfInt2 = null;
            } else {
              arrayOfInt2 = new int[i1];
            } 
            bool5 = requestCasSession((CasSessionRequest)resourceClientProfile, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool5);
            param1Parcel2.writeIntArray(arrayOfInt2);
            return true;
          case 10:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (arrayOfInt2.readInt() != 0) {
              TunerDescramblerRequest tunerDescramblerRequest = TunerDescramblerRequest.CREATOR.createFromParcel((Parcel)arrayOfInt2);
            } else {
              resourceClientProfile = null;
            } 
            n = arrayOfInt2.readInt();
            if (n < 0) {
              arrayOfInt2 = null;
            } else {
              arrayOfInt2 = new int[n];
            } 
            bool4 = requestDescrambler((TunerDescramblerRequest)resourceClientProfile, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool4);
            param1Parcel2.writeIntArray(arrayOfInt2);
            return true;
          case 9:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (arrayOfInt2.readInt() != 0) {
              TunerDemuxRequest tunerDemuxRequest = TunerDemuxRequest.CREATOR.createFromParcel((Parcel)arrayOfInt2);
            } else {
              resourceClientProfile = null;
            } 
            m = arrayOfInt2.readInt();
            if (m < 0) {
              arrayOfInt2 = null;
            } else {
              arrayOfInt2 = new int[m];
            } 
            bool3 = requestDemux((TunerDemuxRequest)resourceClientProfile, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool3);
            param1Parcel2.writeIntArray(arrayOfInt2);
            return true;
          case 8:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            k = arrayOfInt2.readInt();
            param1Int2 = arrayOfInt2.readInt();
            shareFrontend(k, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            if (arrayOfInt2.readInt() != 0) {
              TunerFrontendRequest tunerFrontendRequest = TunerFrontendRequest.CREATOR.createFromParcel((Parcel)arrayOfInt2);
            } else {
              resourceClientProfile = null;
            } 
            k = arrayOfInt2.readInt();
            if (k < 0) {
              arrayOfInt2 = null;
            } else {
              arrayOfInt2 = new int[k];
            } 
            bool2 = requestFrontend((TunerFrontendRequest)resourceClientProfile, arrayOfInt2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool2);
            param1Parcel2.writeIntArray(arrayOfInt2);
            return true;
          case 6:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            arrayOfInt2 = arrayOfInt2.createIntArray();
            setLnbInfoList(arrayOfInt2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            param1Int2 = arrayOfInt2.readInt();
            j = arrayOfInt2.readInt();
            updateCasInfo(param1Int2, j);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            arrayOfInt2.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            arrayOfTunerFrontendInfo = arrayOfInt2.<TunerFrontendInfo>createTypedArray(TunerFrontendInfo.CREATOR);
            setFrontendInfoList(arrayOfTunerFrontendInfo);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfTunerFrontendInfo.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            param1Int2 = arrayOfTunerFrontendInfo.readInt();
            j = arrayOfTunerFrontendInfo.readInt();
            i3 = arrayOfTunerFrontendInfo.readInt();
            bool1 = updateClientPriority(param1Int2, j, i3);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool1);
            return true;
          case 2:
            arrayOfTunerFrontendInfo.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
            i = arrayOfTunerFrontendInfo.readInt();
            unregisterClientProfile(i);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfTunerFrontendInfo.enforceInterface("android.media.tv.tunerresourcemanager.ITunerResourceManager");
        if (arrayOfTunerFrontendInfo.readInt() != 0) {
          resourceClientProfile = ResourceClientProfile.CREATOR.createFromParcel((Parcel)arrayOfTunerFrontendInfo);
        } else {
          resourceClientProfile = null;
        } 
        IResourcesReclaimListener iResourcesReclaimListener = IResourcesReclaimListener.Stub.asInterface(arrayOfTunerFrontendInfo.readStrongBinder());
        int i = arrayOfTunerFrontendInfo.readInt();
        if (i < 0) {
          arrayOfTunerFrontendInfo = null;
        } else {
          arrayOfInt1 = new int[i];
        } 
        registerClientProfile(resourceClientProfile, iResourcesReclaimListener, arrayOfInt1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeIntArray(arrayOfInt1);
        return true;
      } 
      param1Parcel2.writeString("android.media.tv.tunerresourcemanager.ITunerResourceManager");
      return true;
    }
    
    private static class Proxy implements ITunerResourceManager {
      public static ITunerResourceManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.tunerresourcemanager.ITunerResourceManager";
      }
      
      public void registerClientProfile(ResourceClientProfile param2ResourceClientProfile, IResourcesReclaimListener param2IResourcesReclaimListener, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          if (param2ResourceClientProfile != null) {
            parcel1.writeInt(1);
            param2ResourceClientProfile.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IResourcesReclaimListener != null) {
            iBinder = param2IResourcesReclaimListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().registerClientProfile(param2ResourceClientProfile, param2IResourcesReclaimListener, param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          parcel2.readIntArray(param2ArrayOfint);
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterClientProfile(int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().unregisterClientProfile(param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean updateClientPriority(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(3, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().updateClientPriority(param2Int1, param2Int2, param2Int3);
            return bool1;
          } 
          parcel2.readException();
          param2Int1 = parcel2.readInt();
          if (param2Int1 != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setFrontendInfoList(TunerFrontendInfo[] param2ArrayOfTunerFrontendInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeTypedArray(param2ArrayOfTunerFrontendInfo, 0);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().setFrontendInfoList(param2ArrayOfTunerFrontendInfo);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void updateCasInfo(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().updateCasInfo(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setLnbInfoList(int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeIntArray(param2ArrayOfint);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().setLnbInfoList(param2ArrayOfint);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestFrontend(TunerFrontendRequest param2TunerFrontendRequest, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2TunerFrontendRequest != null) {
            parcel1.writeInt(1);
            param2TunerFrontendRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool2 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().requestFrontend(param2TunerFrontendRequest, param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          parcel2.readIntArray(param2ArrayOfint);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void shareFrontend(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().shareFrontend(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestDemux(TunerDemuxRequest param2TunerDemuxRequest, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2TunerDemuxRequest != null) {
            parcel1.writeInt(1);
            param2TunerDemuxRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool2 = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().requestDemux(param2TunerDemuxRequest, param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          parcel2.readIntArray(param2ArrayOfint);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestDescrambler(TunerDescramblerRequest param2TunerDescramblerRequest, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2TunerDescramblerRequest != null) {
            parcel1.writeInt(1);
            param2TunerDescramblerRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool2 = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().requestDescrambler(param2TunerDescramblerRequest, param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          parcel2.readIntArray(param2ArrayOfint);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestCasSession(CasSessionRequest param2CasSessionRequest, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2CasSessionRequest != null) {
            parcel1.writeInt(1);
            param2CasSessionRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool2 = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().requestCasSession(param2CasSessionRequest, param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          parcel2.readIntArray(param2ArrayOfint);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean requestLnb(TunerLnbRequest param2TunerLnbRequest, int[] param2ArrayOfint) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2TunerLnbRequest != null) {
            parcel1.writeInt(1);
            param2TunerLnbRequest.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ArrayOfint == null) {
            parcel1.writeInt(-1);
          } else {
            parcel1.writeInt(param2ArrayOfint.length);
          } 
          boolean bool2 = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().requestLnb(param2TunerLnbRequest, param2ArrayOfint);
            return bool1;
          } 
          parcel2.readException();
          if (parcel2.readInt() == 0)
            bool1 = false; 
          parcel2.readIntArray(param2ArrayOfint);
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseFrontend(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().releaseFrontend(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseDemux(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().releaseDemux(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseDescrambler(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().releaseDescrambler(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseCasSession(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().releaseCasSession(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseLnb(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            ITunerResourceManager.Stub.getDefaultImpl().releaseLnb(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isHigherPriority(ResourceClientProfile param2ResourceClientProfile1, ResourceClientProfile param2ResourceClientProfile2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.tunerresourcemanager.ITunerResourceManager");
          boolean bool1 = true;
          if (param2ResourceClientProfile1 != null) {
            parcel1.writeInt(1);
            param2ResourceClientProfile1.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2ResourceClientProfile2 != null) {
            parcel1.writeInt(1);
            param2ResourceClientProfile2.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool2 && ITunerResourceManager.Stub.getDefaultImpl() != null) {
            bool1 = ITunerResourceManager.Stub.getDefaultImpl().isHigherPriority(param2ResourceClientProfile1, param2ResourceClientProfile2);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITunerResourceManager param1ITunerResourceManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITunerResourceManager != null) {
          Proxy.sDefaultImpl = param1ITunerResourceManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITunerResourceManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
