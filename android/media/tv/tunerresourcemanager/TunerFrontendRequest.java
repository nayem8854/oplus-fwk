package android.media.tv.tunerresourcemanager;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class TunerFrontendRequest implements Parcelable {
  public static final Parcelable.Creator<TunerFrontendRequest> CREATOR = new Parcelable.Creator<TunerFrontendRequest>() {
      public TunerFrontendRequest createFromParcel(Parcel param1Parcel) {
        try {
          return new TunerFrontendRequest(param1Parcel);
        } catch (Exception exception) {
          Log.e("TunerFrontendRequest", "Exception creating TunerFrontendRequest from parcel", exception);
          return null;
        } 
      }
      
      public TunerFrontendRequest[] newArray(int param1Int) {
        return new TunerFrontendRequest[param1Int];
      }
    };
  
  static final String TAG = "TunerFrontendRequest";
  
  private final int mClientId;
  
  private final int mFrontendType;
  
  private TunerFrontendRequest(Parcel paramParcel) {
    this.mClientId = paramParcel.readInt();
    this.mFrontendType = paramParcel.readInt();
  }
  
  public TunerFrontendRequest(int paramInt1, int paramInt2) {
    this.mClientId = paramInt1;
    this.mFrontendType = paramInt2;
  }
  
  public int getClientId() {
    return this.mClientId;
  }
  
  public int getFrontendType() {
    return this.mFrontendType;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder(128);
    stringBuilder.append("TunerFrontendRequest {clientId=");
    stringBuilder.append(this.mClientId);
    stringBuilder.append(", frontendType=");
    stringBuilder.append(this.mFrontendType);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mClientId);
    paramParcel.writeInt(this.mFrontendType);
  }
}
