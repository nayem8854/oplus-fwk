package android.media.tv;

import android.annotation.SystemApi;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

@SystemApi
public final class TvContentRatingSystemInfo implements Parcelable {
  public static final TvContentRatingSystemInfo createTvContentRatingSystemInfo(int paramInt, ApplicationInfo paramApplicationInfo) {
    Uri.Builder builder = new Uri.Builder();
    builder = builder.scheme("android.resource");
    String str = paramApplicationInfo.packageName;
    builder = builder.authority(str);
    builder = builder.appendPath(String.valueOf(paramInt));
    Uri uri = builder.build();
    return new TvContentRatingSystemInfo(uri, paramApplicationInfo);
  }
  
  private TvContentRatingSystemInfo(Uri paramUri, ApplicationInfo paramApplicationInfo) {
    this.mXmlUri = paramUri;
    this.mApplicationInfo = paramApplicationInfo;
  }
  
  public final boolean isSystemDefined() {
    int i = this.mApplicationInfo.flags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public final Uri getXmlUri() {
    return this.mXmlUri;
  }
  
  public static final Parcelable.Creator<TvContentRatingSystemInfo> CREATOR = new Parcelable.Creator<TvContentRatingSystemInfo>() {
      public TvContentRatingSystemInfo createFromParcel(Parcel param1Parcel) {
        return new TvContentRatingSystemInfo(param1Parcel);
      }
      
      public TvContentRatingSystemInfo[] newArray(int param1Int) {
        return new TvContentRatingSystemInfo[param1Int];
      }
    };
  
  private final ApplicationInfo mApplicationInfo;
  
  private final Uri mXmlUri;
  
  private TvContentRatingSystemInfo(Parcel paramParcel) {
    this.mXmlUri = paramParcel.<Uri>readParcelable(null);
    this.mApplicationInfo = paramParcel.<ApplicationInfo>readParcelable(null);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mXmlUri, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mApplicationInfo, paramInt);
  }
  
  public int describeContents() {
    return 0;
  }
}
