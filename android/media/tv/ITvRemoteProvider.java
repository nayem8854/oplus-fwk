package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITvRemoteProvider extends IInterface {
  void onInputBridgeConnected(IBinder paramIBinder) throws RemoteException;
  
  void setRemoteServiceInputSink(ITvRemoteServiceInput paramITvRemoteServiceInput) throws RemoteException;
  
  class Default implements ITvRemoteProvider {
    public void setRemoteServiceInputSink(ITvRemoteServiceInput param1ITvRemoteServiceInput) throws RemoteException {}
    
    public void onInputBridgeConnected(IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvRemoteProvider {
    private static final String DESCRIPTOR = "android.media.tv.ITvRemoteProvider";
    
    static final int TRANSACTION_onInputBridgeConnected = 2;
    
    static final int TRANSACTION_setRemoteServiceInputSink = 1;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvRemoteProvider");
    }
    
    public static ITvRemoteProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvRemoteProvider");
      if (iInterface != null && iInterface instanceof ITvRemoteProvider)
        return (ITvRemoteProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onInputBridgeConnected";
      } 
      return "setRemoteServiceInputSink";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      IBinder iBinder;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.tv.ITvRemoteProvider");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.tv.ITvRemoteProvider");
        iBinder = param1Parcel1.readStrongBinder();
        onInputBridgeConnected(iBinder);
        return true;
      } 
      iBinder.enforceInterface("android.media.tv.ITvRemoteProvider");
      ITvRemoteServiceInput iTvRemoteServiceInput = ITvRemoteServiceInput.Stub.asInterface(iBinder.readStrongBinder());
      setRemoteServiceInputSink(iTvRemoteServiceInput);
      return true;
    }
    
    private static class Proxy implements ITvRemoteProvider {
      public static ITvRemoteProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvRemoteProvider";
      }
      
      public void setRemoteServiceInputSink(ITvRemoteServiceInput param2ITvRemoteServiceInput) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteProvider");
          if (param2ITvRemoteServiceInput != null) {
            iBinder = param2ITvRemoteServiceInput.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteProvider.Stub.getDefaultImpl() != null) {
            ITvRemoteProvider.Stub.getDefaultImpl().setRemoteServiceInputSink(param2ITvRemoteServiceInput);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onInputBridgeConnected(IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvRemoteProvider");
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvRemoteProvider.Stub.getDefaultImpl() != null) {
            ITvRemoteProvider.Stub.getDefaultImpl().onInputBridgeConnected(param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvRemoteProvider param1ITvRemoteProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvRemoteProvider != null) {
          Proxy.sDefaultImpl = param1ITvRemoteProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvRemoteProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
