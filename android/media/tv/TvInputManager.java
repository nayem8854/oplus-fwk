package android.media.tv;

import android.annotation.SystemApi;
import android.content.Intent;
import android.graphics.Rect;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pools;
import android.util.SparseArray;
import android.view.InputChannel;
import android.view.InputEvent;
import android.view.InputEventSender;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.View;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;

public final class TvInputManager {
  private final Object mLock = new Object();
  
  private final List<TvInputCallbackRecord> mCallbackRecords = new LinkedList<>();
  
  private final Map<String, Integer> mStateMap = (Map<String, Integer>)new ArrayMap();
  
  private final SparseArray<SessionCallbackRecord> mSessionCallbackRecordMap = new SparseArray();
  
  public static final String ACTION_BLOCKED_RATINGS_CHANGED = "android.media.tv.action.BLOCKED_RATINGS_CHANGED";
  
  public static final String ACTION_PARENTAL_CONTROLS_ENABLED_CHANGED = "android.media.tv.action.PARENTAL_CONTROLS_ENABLED_CHANGED";
  
  public static final String ACTION_QUERY_CONTENT_RATING_SYSTEMS = "android.media.tv.action.QUERY_CONTENT_RATING_SYSTEMS";
  
  public static final String ACTION_SETUP_INPUTS = "android.media.tv.action.SETUP_INPUTS";
  
  public static final String ACTION_VIEW_RECORDING_SCHEDULES = "android.media.tv.action.VIEW_RECORDING_SCHEDULES";
  
  public static final int DVB_DEVICE_DEMUX = 0;
  
  public static final int DVB_DEVICE_DVR = 1;
  
  static final int DVB_DEVICE_END = 2;
  
  public static final int DVB_DEVICE_FRONTEND = 2;
  
  static final int DVB_DEVICE_START = 0;
  
  public static final int INPUT_STATE_CONNECTED = 0;
  
  public static final int INPUT_STATE_CONNECTED_STANDBY = 1;
  
  public static final int INPUT_STATE_DISCONNECTED = 2;
  
  public static final String META_DATA_CONTENT_RATING_SYSTEMS = "android.media.tv.metadata.CONTENT_RATING_SYSTEMS";
  
  static final int RECORDING_ERROR_END = 2;
  
  public static final int RECORDING_ERROR_INSUFFICIENT_SPACE = 1;
  
  public static final int RECORDING_ERROR_RESOURCE_BUSY = 2;
  
  static final int RECORDING_ERROR_START = 0;
  
  public static final int RECORDING_ERROR_UNKNOWN = 0;
  
  private static final String TAG = "TvInputManager";
  
  public static final long TIME_SHIFT_INVALID_TIME = -9223372036854775808L;
  
  public static final int TIME_SHIFT_STATUS_AVAILABLE = 3;
  
  public static final int TIME_SHIFT_STATUS_UNAVAILABLE = 2;
  
  public static final int TIME_SHIFT_STATUS_UNKNOWN = 0;
  
  public static final int TIME_SHIFT_STATUS_UNSUPPORTED = 1;
  
  public static final int UNKNOWN_CLIENT_PID = -1;
  
  public static final int VIDEO_UNAVAILABLE_REASON_AUDIO_ONLY = 4;
  
  public static final int VIDEO_UNAVAILABLE_REASON_BUFFERING = 3;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_BLACKOUT = 16;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_CARD_INVALID = 15;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_CARD_MUTE = 14;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_INSUFFICIENT_OUTPUT_PROTECTION = 7;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_LICENSE_EXPIRED = 10;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_NEED_ACTIVATION = 11;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_NEED_PAIRING = 12;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_NO_CARD = 13;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_NO_LICENSE = 9;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_PVR_RECORDING_NOT_ALLOWED = 8;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_REBOOTING = 17;
  
  public static final int VIDEO_UNAVAILABLE_REASON_CAS_UNKNOWN = 18;
  
  static final int VIDEO_UNAVAILABLE_REASON_END = 18;
  
  public static final int VIDEO_UNAVAILABLE_REASON_INSUFFICIENT_RESOURCE = 6;
  
  public static final int VIDEO_UNAVAILABLE_REASON_NOT_CONNECTED = 5;
  
  static final int VIDEO_UNAVAILABLE_REASON_START = 0;
  
  public static final int VIDEO_UNAVAILABLE_REASON_TUNING = 1;
  
  public static final int VIDEO_UNAVAILABLE_REASON_UNKNOWN = 0;
  
  public static final int VIDEO_UNAVAILABLE_REASON_WEAK_SIGNAL = 2;
  
  private final ITvInputClient mClient;
  
  private int mNextSeq;
  
  private final ITvInputManager mService;
  
  private final int mUserId;
  
  class SessionCallback {
    public void onSessionCreated(TvInputManager.Session param1Session) {}
    
    public void onSessionReleased(TvInputManager.Session param1Session) {}
    
    public void onChannelRetuned(TvInputManager.Session param1Session, Uri param1Uri) {}
    
    public void onTracksChanged(TvInputManager.Session param1Session, List<TvTrackInfo> param1List) {}
    
    public void onTrackSelected(TvInputManager.Session param1Session, int param1Int, String param1String) {}
    
    public void onVideoSizeChanged(TvInputManager.Session param1Session, int param1Int1, int param1Int2) {}
    
    public void onVideoAvailable(TvInputManager.Session param1Session) {}
    
    public void onVideoUnavailable(TvInputManager.Session param1Session, int param1Int) {}
    
    public void onContentAllowed(TvInputManager.Session param1Session) {}
    
    public void onContentBlocked(TvInputManager.Session param1Session, TvContentRating param1TvContentRating) {}
    
    public void onLayoutSurface(TvInputManager.Session param1Session, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {}
    
    public void onSessionEvent(TvInputManager.Session param1Session, String param1String, Bundle param1Bundle) {}
    
    public void onTimeShiftStatusChanged(TvInputManager.Session param1Session, int param1Int) {}
    
    public void onTimeShiftStartPositionChanged(TvInputManager.Session param1Session, long param1Long) {}
    
    public void onTimeShiftCurrentPositionChanged(TvInputManager.Session param1Session, long param1Long) {}
    
    void onTuned(TvInputManager.Session param1Session, Uri param1Uri) {}
    
    void onRecordingStopped(TvInputManager.Session param1Session, Uri param1Uri) {}
    
    void onError(TvInputManager.Session param1Session, int param1Int) {}
  }
  
  class SessionCallbackRecord {
    private final Handler mHandler;
    
    private TvInputManager.Session mSession;
    
    private final TvInputManager.SessionCallback mSessionCallback;
    
    SessionCallbackRecord(TvInputManager this$0, Handler param1Handler) {
      this.mSessionCallback = (TvInputManager.SessionCallback)this$0;
      this.mHandler = param1Handler;
    }
    
    void postSessionCreated(final TvInputManager.Session session) {
      this.mSession = session;
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final TvInputManager.Session val$session;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onSessionCreated(session);
            }
          });
    }
    
    void postSessionReleased() {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onSessionReleased(TvInputManager.SessionCallbackRecord.this.mSession);
            }
          });
    }
    
    void postChannelRetuned(final Uri channelUri) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final Uri val$channelUri;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onChannelRetuned(TvInputManager.SessionCallbackRecord.this.mSession, channelUri);
            }
          });
    }
    
    void postTracksChanged(final List<TvTrackInfo> tracks) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final List val$tracks;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTracksChanged(TvInputManager.SessionCallbackRecord.this.mSession, tracks);
            }
          });
    }
    
    void postTrackSelected(final int type, final String trackId) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final String val$trackId;
            
            final int val$type;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTrackSelected(TvInputManager.SessionCallbackRecord.this.mSession, type, trackId);
            }
          });
    }
    
    void postVideoSizeChanged(final int width, final int height) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final int val$height;
            
            final int val$width;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onVideoSizeChanged(TvInputManager.SessionCallbackRecord.this.mSession, width, height);
            }
          });
    }
    
    void postVideoAvailable() {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onVideoAvailable(TvInputManager.SessionCallbackRecord.this.mSession);
            }
          });
    }
    
    void postVideoUnavailable(final int reason) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final int val$reason;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onVideoUnavailable(TvInputManager.SessionCallbackRecord.this.mSession, reason);
            }
          });
    }
    
    void postContentAllowed() {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onContentAllowed(TvInputManager.SessionCallbackRecord.this.mSession);
            }
          });
    }
    
    void postContentBlocked(final TvContentRating rating) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final TvContentRating val$rating;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onContentBlocked(TvInputManager.SessionCallbackRecord.this.mSession, rating);
            }
          });
    }
    
    void postLayoutSurface(final int left, final int top, final int right, final int bottom) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final int val$bottom;
            
            final int val$left;
            
            final int val$right;
            
            final int val$top;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onLayoutSurface(TvInputManager.SessionCallbackRecord.this.mSession, left, top, right, bottom);
            }
          });
    }
    
    void postSessionEvent(final String eventType, final Bundle eventArgs) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final Bundle val$eventArgs;
            
            final String val$eventType;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onSessionEvent(TvInputManager.SessionCallbackRecord.this.mSession, eventType, eventArgs);
            }
          });
    }
    
    void postTimeShiftStatusChanged(final int status) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final int val$status;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTimeShiftStatusChanged(TvInputManager.SessionCallbackRecord.this.mSession, status);
            }
          });
    }
    
    void postTimeShiftStartPositionChanged(final long timeMs) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final long val$timeMs;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTimeShiftStartPositionChanged(TvInputManager.SessionCallbackRecord.this.mSession, timeMs);
            }
          });
    }
    
    void postTimeShiftCurrentPositionChanged(final long timeMs) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final long val$timeMs;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTimeShiftCurrentPositionChanged(TvInputManager.SessionCallbackRecord.this.mSession, timeMs);
            }
          });
    }
    
    void postTuned(final Uri channelUri) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final Uri val$channelUri;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onTuned(TvInputManager.SessionCallbackRecord.this.mSession, channelUri);
            }
          });
    }
    
    void postRecordingStopped(final Uri recordedProgramUri) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final Uri val$recordedProgramUri;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onRecordingStopped(TvInputManager.SessionCallbackRecord.this.mSession, recordedProgramUri);
            }
          });
    }
    
    void postError(final int error) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.SessionCallbackRecord this$0;
            
            final int val$error;
            
            public void run() {
              TvInputManager.SessionCallbackRecord.this.mSessionCallback.onError(TvInputManager.SessionCallbackRecord.this.mSession, error);
            }
          });
    }
  }
  
  class TvInputCallback {
    public void onInputStateChanged(String param1String, int param1Int) {}
    
    public void onInputAdded(String param1String) {}
    
    public void onInputRemoved(String param1String) {}
    
    public void onInputUpdated(String param1String) {}
    
    public void onTvInputInfoUpdated(TvInputInfo param1TvInputInfo) {}
  }
  
  private static final class TvInputCallbackRecord {
    private final TvInputManager.TvInputCallback mCallback;
    
    private final Handler mHandler;
    
    public TvInputCallbackRecord(TvInputManager.TvInputCallback param1TvInputCallback, Handler param1Handler) {
      this.mCallback = param1TvInputCallback;
      this.mHandler = param1Handler;
    }
    
    public TvInputManager.TvInputCallback getCallback() {
      return this.mCallback;
    }
    
    public void postInputAdded(String param1String) {
      this.mHandler.post((Runnable)new Object(this, param1String));
    }
    
    public void postInputRemoved(String param1String) {
      this.mHandler.post((Runnable)new Object(this, param1String));
    }
    
    public void postInputUpdated(String param1String) {
      this.mHandler.post((Runnable)new Object(this, param1String));
    }
    
    public void postInputStateChanged(String param1String, int param1Int) {
      this.mHandler.post((Runnable)new Object(this, param1String, param1Int));
    }
    
    public void postTvInputInfoUpdated(final TvInputInfo inputInfo) {
      this.mHandler.post(new Runnable() {
            final TvInputManager.TvInputCallbackRecord this$0;
            
            final TvInputInfo val$inputInfo;
            
            public void run() {
              TvInputManager.TvInputCallbackRecord.this.mCallback.onTvInputInfoUpdated(inputInfo);
            }
          });
    }
  }
  
  class null implements Runnable {
    final TvInputManager.TvInputCallbackRecord this$0;
    
    final TvInputInfo val$inputInfo;
    
    public void run() {
      this.this$0.mCallback.onTvInputInfoUpdated(inputInfo);
    }
  }
  
  @SystemApi
  class HardwareCallback {
    public abstract void onReleased();
    
    public abstract void onStreamConfigChanged(TvStreamConfig[] param1ArrayOfTvStreamConfig);
  }
  
  public TvInputManager(ITvInputManager paramITvInputManager, int paramInt) {
    this.mService = paramITvInputManager;
    this.mUserId = paramInt;
    this.mClient = (ITvInputClient)new Object(this);
    Object object = new Object(this);
    try {
      if (this.mService != null) {
        this.mService.registerCallback((ITvInputManagerCallback)object, this.mUserId);
        null = this.mService.getTvInputList(this.mUserId);
        synchronized (this.mLock) {
          for (TvInputInfo tvInputInfo : null) {
            String str = tvInputInfo.getId();
            this.mStateMap.put(str, Integer.valueOf(this.mService.getTvInputState(str, this.mUserId)));
          } 
        } 
      } 
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<TvInputInfo> getTvInputList() {
    try {
      return this.mService.getTvInputList(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public TvInputInfo getTvInputInfo(String paramString) {
    Preconditions.checkNotNull(paramString);
    try {
      return this.mService.getTvInputInfo(paramString, this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void updateTvInputInfo(TvInputInfo paramTvInputInfo) {
    Preconditions.checkNotNull(paramTvInputInfo);
    try {
      this.mService.updateTvInputInfo(paramTvInputInfo, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public int getInputState(String paramString) {
    Preconditions.checkNotNull(paramString);
    synchronized (this.mLock) {
      StringBuilder stringBuilder;
      Integer integer = this.mStateMap.get(paramString);
      if (integer == null) {
        stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Unrecognized input ID: ");
        stringBuilder.append(paramString);
        Log.w("TvInputManager", stringBuilder.toString());
        return 2;
      } 
      return stringBuilder.intValue();
    } 
  }
  
  public void registerCallback(TvInputCallback paramTvInputCallback, Handler paramHandler) {
    Preconditions.checkNotNull(paramTvInputCallback);
    Preconditions.checkNotNull(paramHandler);
    synchronized (this.mLock) {
      List<TvInputCallbackRecord> list = this.mCallbackRecords;
      TvInputCallbackRecord tvInputCallbackRecord = new TvInputCallbackRecord();
      this(paramTvInputCallback, paramHandler);
      list.add(tvInputCallbackRecord);
      return;
    } 
  }
  
  public void unregisterCallback(TvInputCallback paramTvInputCallback) {
    Preconditions.checkNotNull(paramTvInputCallback);
    synchronized (this.mLock) {
      Iterator<TvInputCallbackRecord> iterator = this.mCallbackRecords.iterator();
      while (iterator.hasNext()) {
        TvInputCallbackRecord tvInputCallbackRecord = iterator.next();
        if (tvInputCallbackRecord.getCallback() == paramTvInputCallback) {
          iterator.remove();
          break;
        } 
      } 
      return;
    } 
  }
  
  public boolean isParentalControlsEnabled() {
    try {
      return this.mService.isParentalControlsEnabled(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void setParentalControlsEnabled(boolean paramBoolean) {
    try {
      this.mService.setParentalControlsEnabled(paramBoolean, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public boolean isRatingBlocked(TvContentRating paramTvContentRating) {
    Preconditions.checkNotNull(paramTvContentRating);
    try {
      return this.mService.isRatingBlocked(paramTvContentRating.flattenToString(), this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public List<TvContentRating> getBlockedRatings() {
    try {
      ArrayList<TvContentRating> arrayList = new ArrayList();
      this();
      for (String str : this.mService.getBlockedRatings(this.mUserId))
        arrayList.add(TvContentRating.unflattenFromString(str)); 
      return arrayList;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void addBlockedRating(TvContentRating paramTvContentRating) {
    Preconditions.checkNotNull(paramTvContentRating);
    try {
      this.mService.addBlockedRating(paramTvContentRating.flattenToString(), this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void removeBlockedRating(TvContentRating paramTvContentRating) {
    Preconditions.checkNotNull(paramTvContentRating);
    try {
      this.mService.removeBlockedRating(paramTvContentRating.flattenToString(), this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<TvContentRatingSystemInfo> getTvContentRatingSystemList() {
    try {
      return this.mService.getTvContentRatingSystemList(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void notifyPreviewProgramBrowsableDisabled(String paramString, long paramLong) {
    Intent intent = new Intent();
    intent.setAction("android.media.tv.action.PREVIEW_PROGRAM_BROWSABLE_DISABLED");
    intent.putExtra("android.media.tv.extra.PREVIEW_PROGRAM_ID", paramLong);
    intent.setPackage(paramString);
    try {
      this.mService.sendTvInputNotifyIntent(intent, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void notifyWatchNextProgramBrowsableDisabled(String paramString, long paramLong) {
    Intent intent = new Intent();
    intent.setAction("android.media.tv.action.WATCH_NEXT_PROGRAM_BROWSABLE_DISABLED");
    intent.putExtra("android.media.tv.extra.WATCH_NEXT_PROGRAM_ID", paramLong);
    intent.setPackage(paramString);
    try {
      this.mService.sendTvInputNotifyIntent(intent, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void notifyPreviewProgramAddedToWatchNext(String paramString, long paramLong1, long paramLong2) {
    Intent intent = new Intent();
    intent.setAction("android.media.tv.action.PREVIEW_PROGRAM_ADDED_TO_WATCH_NEXT");
    intent.putExtra("android.media.tv.extra.PREVIEW_PROGRAM_ID", paramLong1);
    intent.putExtra("android.media.tv.extra.WATCH_NEXT_PROGRAM_ID", paramLong2);
    intent.setPackage(paramString);
    try {
      this.mService.sendTvInputNotifyIntent(intent, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void createSession(String paramString, SessionCallback paramSessionCallback, Handler paramHandler) {
    createSessionInternal(paramString, false, paramSessionCallback, paramHandler);
  }
  
  public int getClientPid(String paramString) {
    return getClientPidInternal(paramString);
  }
  
  public void createRecordingSession(String paramString, SessionCallback paramSessionCallback, Handler paramHandler) {
    createSessionInternal(paramString, true, paramSessionCallback, paramHandler);
  }
  
  private void createSessionInternal(String paramString, boolean paramBoolean, SessionCallback paramSessionCallback, Handler paramHandler) {
    Preconditions.checkNotNull(paramString);
    Preconditions.checkNotNull(paramSessionCallback);
    Preconditions.checkNotNull(paramHandler);
    SessionCallbackRecord sessionCallbackRecord = new SessionCallbackRecord(paramSessionCallback, paramHandler);
    synchronized (this.mSessionCallbackRecordMap) {
      int i = this.mNextSeq;
      this.mNextSeq = i + 1;
      this.mSessionCallbackRecordMap.put(i, sessionCallbackRecord);
      try {
        this.mService.createSession(this.mClient, paramString, paramBoolean, i, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  private int getClientPidInternal(String paramString) {
    Preconditions.checkNotNull(paramString);
    try {
      return this.mService.getClientPid(paramString);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<TvStreamConfig> getAvailableTvStreamConfigList(String paramString) {
    try {
      return this.mService.getAvailableTvStreamConfigList(paramString, this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean captureFrame(String paramString, Surface paramSurface, TvStreamConfig paramTvStreamConfig) {
    try {
      return this.mService.captureFrame(paramString, paramSurface, paramTvStreamConfig, this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public boolean isSingleSessionActive() {
    try {
      return this.mService.isSingleSessionActive(this.mUserId);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<TvInputHardwareInfo> getHardwareList() {
    try {
      return this.mService.getHardwareList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public Hardware acquireTvInputHardware(int paramInt, HardwareCallback paramHardwareCallback, TvInputInfo paramTvInputInfo) {
    return acquireTvInputHardware(paramInt, paramTvInputInfo, paramHardwareCallback);
  }
  
  @SystemApi
  public Hardware acquireTvInputHardware(int paramInt, TvInputInfo paramTvInputInfo, HardwareCallback paramHardwareCallback) {
    Preconditions.checkNotNull(paramTvInputInfo);
    Preconditions.checkNotNull(paramHardwareCallback);
    return acquireTvInputHardwareInternal(paramInt, paramTvInputInfo, null, 400, (Executor)new Object(this), paramHardwareCallback);
  }
  
  @SystemApi
  public Hardware acquireTvInputHardware(int paramInt1, TvInputInfo paramTvInputInfo, String paramString, int paramInt2, Executor paramExecutor, HardwareCallback paramHardwareCallback) {
    Preconditions.checkNotNull(paramTvInputInfo);
    Preconditions.checkNotNull(paramHardwareCallback);
    return acquireTvInputHardwareInternal(paramInt1, paramTvInputInfo, paramString, paramInt2, paramExecutor, paramHardwareCallback);
  }
  
  public void addHardwareDevice(int paramInt) {
    try {
      this.mService.addHardwareDevice(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void removeHardwareDevice(int paramInt) {
    try {
      this.mService.removeHardwareDevice(paramInt);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private Hardware acquireTvInputHardwareInternal(int paramInt1, TvInputInfo paramTvInputInfo, String paramString, int paramInt2, Executor paramExecutor, HardwareCallback paramHardwareCallback) {
    try {
      ITvInputManager iTvInputManager = this.mService;
      Object object = new Object();
      super(this, paramExecutor, paramHardwareCallback);
      int i = this.mUserId;
      ITvInputHardware iTvInputHardware = iTvInputManager.acquireTvInputHardware(paramInt1, (ITvInputHardwareCallback)object, paramTvInputInfo, i, paramString, paramInt2);
      if (iTvInputHardware == null)
        return null; 
      return new Hardware();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public void releaseTvInputHardware(int paramInt, Hardware paramHardware) {
    try {
      this.mService.releaseTvInputHardware(paramInt, paramHardware.getInterface(), this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public List<DvbDeviceInfo> getDvbDeviceList() {
    try {
      return this.mService.getDvbDeviceList();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  @SystemApi
  public ParcelFileDescriptor openDvbDevice(DvbDeviceInfo paramDvbDeviceInfo, int paramInt) {
    if (paramInt >= 0 && 2 >= paramInt)
      try {
        return this.mService.openDvbDevice(paramDvbDeviceInfo, paramInt);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
    IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
    StringBuilder stringBuilder = new StringBuilder();
    this();
    stringBuilder.append("Invalid DVB device: ");
    stringBuilder.append(paramInt);
    this(stringBuilder.toString());
    throw illegalArgumentException;
  }
  
  public void requestChannelBrowsable(Uri paramUri) {
    try {
      this.mService.requestChannelBrowsable(paramUri, this.mUserId);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class Session {
    private final InputEventHandler mHandler = new InputEventHandler(Looper.getMainLooper());
    
    private final Pools.Pool<PendingEvent> mPendingEventPool = (Pools.Pool<PendingEvent>)new Pools.SimplePool(20);
    
    private final SparseArray<PendingEvent> mPendingEvents = new SparseArray(20);
    
    private final Object mMetadataLock = new Object();
    
    private final List<TvTrackInfo> mAudioTracks = new ArrayList<>();
    
    private final List<TvTrackInfo> mVideoTracks = new ArrayList<>();
    
    private final List<TvTrackInfo> mSubtitleTracks = new ArrayList<>();
    
    static final int DISPATCH_HANDLED = 1;
    
    static final int DISPATCH_IN_PROGRESS = -1;
    
    static final int DISPATCH_NOT_HANDLED = 0;
    
    private static final long INPUT_SESSION_NOT_RESPONDING_TIMEOUT = 2500L;
    
    private InputChannel mChannel;
    
    private String mSelectedAudioTrackId;
    
    private String mSelectedSubtitleTrackId;
    
    private String mSelectedVideoTrackId;
    
    private TvInputEventSender mSender;
    
    private final int mSeq;
    
    private final ITvInputManager mService;
    
    private final SparseArray<TvInputManager.SessionCallbackRecord> mSessionCallbackRecordMap;
    
    private IBinder mToken;
    
    private final int mUserId;
    
    private int mVideoHeight;
    
    private int mVideoWidth;
    
    private Session(TvInputManager this$0, InputChannel param1InputChannel, ITvInputManager param1ITvInputManager, int param1Int1, int param1Int2, SparseArray<TvInputManager.SessionCallbackRecord> param1SparseArray) {
      this.mToken = (IBinder)this$0;
      this.mChannel = param1InputChannel;
      this.mService = param1ITvInputManager;
      this.mUserId = param1Int1;
      this.mSeq = param1Int2;
      this.mSessionCallbackRecordMap = param1SparseArray;
    }
    
    public void release() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.releaseSession(iBinder, this.mUserId);
        releaseInternal();
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void setMain() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.setMainSession(iBinder, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void setSurface(Surface param1Surface) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.setSurface(iBinder, param1Surface, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void dispatchSurfaceChanged(int param1Int1, int param1Int2, int param1Int3) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.dispatchSurfaceChanged(iBinder, param1Int1, param1Int2, param1Int3, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void setStreamVolume(float param1Float) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      if (param1Float >= 0.0F && param1Float <= 1.0F)
        try {
          this.mService.setVolume(iBinder, param1Float, this.mUserId);
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        }  
      IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
      this("volume should be between 0.0f and 1.0f");
      throw illegalArgumentException;
    }
    
    public void tune(Uri param1Uri) {
      tune(param1Uri, null);
    }
    
    public void tune(Uri param1Uri, Bundle param1Bundle) {
      Preconditions.checkNotNull(param1Uri);
      if (this.mToken == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      synchronized (this.mMetadataLock) {
        this.mAudioTracks.clear();
        this.mVideoTracks.clear();
        this.mSubtitleTracks.clear();
        this.mSelectedAudioTrackId = null;
        this.mSelectedVideoTrackId = null;
        this.mSelectedSubtitleTrackId = null;
        this.mVideoWidth = 0;
        this.mVideoHeight = 0;
        try {
          this.mService.tune(this.mToken, param1Uri, param1Bundle, this.mUserId);
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
    }
    
    public void setCaptionEnabled(boolean param1Boolean) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.setCaptionEnabled(iBinder, param1Boolean, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void selectTrack(int param1Int, String param1String) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mMetadataLock : Ljava/lang/Object;
      //   4: astore_3
      //   5: aload_3
      //   6: monitorenter
      //   7: iload_1
      //   8: ifne -> 68
      //   11: aload_2
      //   12: ifnull -> 192
      //   15: aload_0
      //   16: aload_0
      //   17: getfield mAudioTracks : Ljava/util/List;
      //   20: aload_2
      //   21: invokespecial containsTrack : (Ljava/util/List;Ljava/lang/String;)Z
      //   24: ifne -> 192
      //   27: new java/lang/StringBuilder
      //   30: astore #4
      //   32: aload #4
      //   34: invokespecial <init> : ()V
      //   37: aload #4
      //   39: ldc_w 'Invalid audio trackId: '
      //   42: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   45: pop
      //   46: aload #4
      //   48: aload_2
      //   49: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   52: pop
      //   53: ldc_w 'TvInputManager'
      //   56: aload #4
      //   58: invokevirtual toString : ()Ljava/lang/String;
      //   61: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   64: pop
      //   65: aload_3
      //   66: monitorexit
      //   67: return
      //   68: iload_1
      //   69: iconst_1
      //   70: if_icmpne -> 130
      //   73: aload_2
      //   74: ifnull -> 192
      //   77: aload_0
      //   78: aload_0
      //   79: getfield mVideoTracks : Ljava/util/List;
      //   82: aload_2
      //   83: invokespecial containsTrack : (Ljava/util/List;Ljava/lang/String;)Z
      //   86: ifne -> 192
      //   89: new java/lang/StringBuilder
      //   92: astore #4
      //   94: aload #4
      //   96: invokespecial <init> : ()V
      //   99: aload #4
      //   101: ldc_w 'Invalid video trackId: '
      //   104: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   107: pop
      //   108: aload #4
      //   110: aload_2
      //   111: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   114: pop
      //   115: ldc_w 'TvInputManager'
      //   118: aload #4
      //   120: invokevirtual toString : ()Ljava/lang/String;
      //   123: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   126: pop
      //   127: aload_3
      //   128: monitorexit
      //   129: return
      //   130: iload_1
      //   131: iconst_2
      //   132: if_icmpne -> 241
      //   135: aload_2
      //   136: ifnull -> 192
      //   139: aload_0
      //   140: aload_0
      //   141: getfield mSubtitleTracks : Ljava/util/List;
      //   144: aload_2
      //   145: invokespecial containsTrack : (Ljava/util/List;Ljava/lang/String;)Z
      //   148: ifne -> 192
      //   151: new java/lang/StringBuilder
      //   154: astore #4
      //   156: aload #4
      //   158: invokespecial <init> : ()V
      //   161: aload #4
      //   163: ldc_w 'Invalid subtitle trackId: '
      //   166: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   169: pop
      //   170: aload #4
      //   172: aload_2
      //   173: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   176: pop
      //   177: ldc_w 'TvInputManager'
      //   180: aload #4
      //   182: invokevirtual toString : ()Ljava/lang/String;
      //   185: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   188: pop
      //   189: aload_3
      //   190: monitorexit
      //   191: return
      //   192: aload_3
      //   193: monitorexit
      //   194: aload_0
      //   195: getfield mToken : Landroid/os/IBinder;
      //   198: astore_3
      //   199: aload_3
      //   200: ifnonnull -> 214
      //   203: ldc_w 'TvInputManager'
      //   206: ldc_w 'The session has been already released'
      //   209: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   212: pop
      //   213: return
      //   214: aload_0
      //   215: getfield mService : Landroid/media/tv/ITvInputManager;
      //   218: aload_3
      //   219: iload_1
      //   220: aload_2
      //   221: aload_0
      //   222: getfield mUserId : I
      //   225: invokeinterface selectTrack : (Landroid/os/IBinder;ILjava/lang/String;I)V
      //   230: return
      //   231: astore_2
      //   232: aload_2
      //   233: invokevirtual rethrowFromSystemServer : ()Ljava/lang/RuntimeException;
      //   236: athrow
      //   237: astore_2
      //   238: goto -> 282
      //   241: new java/lang/IllegalArgumentException
      //   244: astore_2
      //   245: new java/lang/StringBuilder
      //   248: astore #4
      //   250: aload #4
      //   252: invokespecial <init> : ()V
      //   255: aload #4
      //   257: ldc_w 'invalid type: '
      //   260: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   263: pop
      //   264: aload #4
      //   266: iload_1
      //   267: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   270: pop
      //   271: aload_2
      //   272: aload #4
      //   274: invokevirtual toString : ()Ljava/lang/String;
      //   277: invokespecial <init> : (Ljava/lang/String;)V
      //   280: aload_2
      //   281: athrow
      //   282: aload_3
      //   283: monitorexit
      //   284: aload_2
      //   285: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2170	-> 0
      //   #2171	-> 7
      //   #2172	-> 11
      //   #2173	-> 27
      //   #2174	-> 65
      //   #2176	-> 68
      //   #2177	-> 73
      //   #2178	-> 89
      //   #2179	-> 127
      //   #2181	-> 130
      //   #2182	-> 135
      //   #2183	-> 151
      //   #2184	-> 189
      //   #2189	-> 192
      //   #2190	-> 194
      //   #2191	-> 203
      //   #2192	-> 213
      //   #2195	-> 214
      //   #2198	-> 230
      //   #2199	-> 230
      //   #2196	-> 231
      //   #2197	-> 232
      //   #2189	-> 237
      //   #2187	-> 241
      //   #2189	-> 282
      // Exception table:
      //   from	to	target	type
      //   15	27	237	finally
      //   27	65	237	finally
      //   65	67	237	finally
      //   77	89	237	finally
      //   89	127	237	finally
      //   127	129	237	finally
      //   139	151	237	finally
      //   151	189	237	finally
      //   189	191	237	finally
      //   192	194	237	finally
      //   214	230	231	android/os/RemoteException
      //   241	282	237	finally
      //   282	284	237	finally
    }
    
    private boolean containsTrack(List<TvTrackInfo> param1List, String param1String) {
      for (TvTrackInfo tvTrackInfo : param1List) {
        if (tvTrackInfo.getId().equals(param1String))
          return true; 
      } 
      return false;
    }
    
    public List<TvTrackInfo> getTracks(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mMetadataLock : Ljava/lang/Object;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: iload_1
      //   8: ifne -> 42
      //   11: aload_0
      //   12: getfield mAudioTracks : Ljava/util/List;
      //   15: ifnonnull -> 22
      //   18: aload_2
      //   19: monitorexit
      //   20: aconst_null
      //   21: areturn
      //   22: new java/util/ArrayList
      //   25: astore_3
      //   26: aload_3
      //   27: aload_0
      //   28: getfield mAudioTracks : Ljava/util/List;
      //   31: invokespecial <init> : (Ljava/util/Collection;)V
      //   34: aload_2
      //   35: monitorexit
      //   36: aload_3
      //   37: areturn
      //   38: astore_3
      //   39: goto -> 142
      //   42: iload_1
      //   43: iconst_1
      //   44: if_icmpne -> 74
      //   47: aload_0
      //   48: getfield mVideoTracks : Ljava/util/List;
      //   51: ifnonnull -> 58
      //   54: aload_2
      //   55: monitorexit
      //   56: aconst_null
      //   57: areturn
      //   58: new java/util/ArrayList
      //   61: astore_3
      //   62: aload_3
      //   63: aload_0
      //   64: getfield mVideoTracks : Ljava/util/List;
      //   67: invokespecial <init> : (Ljava/util/Collection;)V
      //   70: aload_2
      //   71: monitorexit
      //   72: aload_3
      //   73: areturn
      //   74: iload_1
      //   75: iconst_2
      //   76: if_icmpne -> 106
      //   79: aload_0
      //   80: getfield mSubtitleTracks : Ljava/util/List;
      //   83: ifnonnull -> 90
      //   86: aload_2
      //   87: monitorexit
      //   88: aconst_null
      //   89: areturn
      //   90: new java/util/ArrayList
      //   93: astore_3
      //   94: aload_3
      //   95: aload_0
      //   96: getfield mSubtitleTracks : Ljava/util/List;
      //   99: invokespecial <init> : (Ljava/util/Collection;)V
      //   102: aload_2
      //   103: monitorexit
      //   104: aload_3
      //   105: areturn
      //   106: aload_2
      //   107: monitorexit
      //   108: new java/lang/StringBuilder
      //   111: dup
      //   112: invokespecial <init> : ()V
      //   115: astore_2
      //   116: aload_2
      //   117: ldc_w 'invalid type: '
      //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   123: pop
      //   124: aload_2
      //   125: iload_1
      //   126: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   129: pop
      //   130: new java/lang/IllegalArgumentException
      //   133: dup
      //   134: aload_2
      //   135: invokevirtual toString : ()Ljava/lang/String;
      //   138: invokespecial <init> : (Ljava/lang/String;)V
      //   141: athrow
      //   142: aload_2
      //   143: monitorexit
      //   144: aload_3
      //   145: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2220	-> 0
      //   #2221	-> 7
      //   #2222	-> 11
      //   #2223	-> 18
      //   #2225	-> 22
      //   #2237	-> 38
      //   #2226	-> 42
      //   #2227	-> 47
      //   #2228	-> 54
      //   #2230	-> 58
      //   #2231	-> 74
      //   #2232	-> 79
      //   #2233	-> 86
      //   #2235	-> 90
      //   #2237	-> 106
      //   #2238	-> 108
      //   #2237	-> 142
      // Exception table:
      //   from	to	target	type
      //   11	18	38	finally
      //   18	20	38	finally
      //   22	36	38	finally
      //   47	54	38	finally
      //   54	56	38	finally
      //   58	72	38	finally
      //   79	86	38	finally
      //   86	88	38	finally
      //   90	104	38	finally
      //   106	108	38	finally
      //   142	144	38	finally
    }
    
    public String getSelectedTrack(int param1Int) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mMetadataLock : Ljava/lang/Object;
      //   4: astore_2
      //   5: aload_2
      //   6: monitorenter
      //   7: iload_1
      //   8: ifne -> 24
      //   11: aload_0
      //   12: getfield mSelectedAudioTrackId : Ljava/lang/String;
      //   15: astore_3
      //   16: aload_2
      //   17: monitorexit
      //   18: aload_3
      //   19: areturn
      //   20: astore_3
      //   21: goto -> 88
      //   24: iload_1
      //   25: iconst_1
      //   26: if_icmpne -> 38
      //   29: aload_0
      //   30: getfield mSelectedVideoTrackId : Ljava/lang/String;
      //   33: astore_3
      //   34: aload_2
      //   35: monitorexit
      //   36: aload_3
      //   37: areturn
      //   38: iload_1
      //   39: iconst_2
      //   40: if_icmpne -> 52
      //   43: aload_0
      //   44: getfield mSelectedSubtitleTrackId : Ljava/lang/String;
      //   47: astore_3
      //   48: aload_2
      //   49: monitorexit
      //   50: aload_3
      //   51: areturn
      //   52: aload_2
      //   53: monitorexit
      //   54: new java/lang/StringBuilder
      //   57: dup
      //   58: invokespecial <init> : ()V
      //   61: astore_2
      //   62: aload_2
      //   63: ldc_w 'invalid type: '
      //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   69: pop
      //   70: aload_2
      //   71: iload_1
      //   72: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   75: pop
      //   76: new java/lang/IllegalArgumentException
      //   79: dup
      //   80: aload_2
      //   81: invokevirtual toString : ()Ljava/lang/String;
      //   84: invokespecial <init> : (Ljava/lang/String;)V
      //   87: athrow
      //   88: aload_2
      //   89: monitorexit
      //   90: aload_3
      //   91: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2250	-> 0
      //   #2251	-> 7
      //   #2252	-> 11
      //   #2258	-> 20
      //   #2253	-> 24
      //   #2254	-> 29
      //   #2255	-> 38
      //   #2256	-> 43
      //   #2258	-> 52
      //   #2259	-> 54
      //   #2258	-> 88
      // Exception table:
      //   from	to	target	type
      //   11	18	20	finally
      //   29	36	20	finally
      //   43	50	20	finally
      //   52	54	20	finally
      //   88	90	20	finally
    }
    
    boolean updateTracks(List<TvTrackInfo> param1List) {
      synchronized (this.mMetadataLock) {
        boolean bool;
        this.mAudioTracks.clear();
        this.mVideoTracks.clear();
        this.mSubtitleTracks.clear();
        Iterator<TvTrackInfo> iterator = param1List.iterator();
        while (true) {
          boolean bool1 = iterator.hasNext();
          bool = true;
          if (bool1) {
            TvTrackInfo tvTrackInfo = iterator.next();
            if (tvTrackInfo.getType() == 0) {
              this.mAudioTracks.add(tvTrackInfo);
              continue;
            } 
            if (tvTrackInfo.getType() == 1) {
              this.mVideoTracks.add(tvTrackInfo);
              continue;
            } 
            if (tvTrackInfo.getType() == 2)
              this.mSubtitleTracks.add(tvTrackInfo); 
            continue;
          } 
          break;
        } 
        if (this.mAudioTracks.isEmpty() && this.mVideoTracks.isEmpty()) {
          param1List = this.mSubtitleTracks;
          if (param1List.isEmpty())
            bool = false; 
        } 
        return bool;
      } 
    }
    
    boolean updateTrackSelection(int param1Int, String param1String) {
      Object object = this.mMetadataLock;
      /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      if (param1Int == 0)
        try {
          String str = this.mSelectedAudioTrackId;
          if (!TextUtils.equals(param1String, str)) {
            this.mSelectedAudioTrackId = param1String;
            /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
            return true;
          } 
        } finally {} 
      if (param1Int == 1) {
        String str = this.mSelectedVideoTrackId;
        if (!TextUtils.equals(param1String, str)) {
          this.mSelectedVideoTrackId = param1String;
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return true;
        } 
      } 
      if (param1Int == 2) {
        String str = this.mSelectedSubtitleTrackId;
        if (!TextUtils.equals(param1String, str)) {
          this.mSelectedSubtitleTrackId = param1String;
          /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
          return true;
        } 
      } 
      /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
      return false;
    }
    
    TvTrackInfo getVideoTrackToNotify() {
      synchronized (this.mMetadataLock) {
        if (!this.mVideoTracks.isEmpty() && this.mSelectedVideoTrackId != null)
          for (TvTrackInfo tvTrackInfo : this.mVideoTracks) {
            if (tvTrackInfo.getId().equals(this.mSelectedVideoTrackId)) {
              int i = tvTrackInfo.getVideoWidth();
              int j = tvTrackInfo.getVideoHeight();
              if (this.mVideoWidth != i || this.mVideoHeight != j) {
                this.mVideoWidth = i;
                this.mVideoHeight = j;
                return tvTrackInfo;
              } 
            } 
          }  
        return null;
      } 
    }
    
    void timeShiftPlay(Uri param1Uri) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftPlay(iBinder, param1Uri, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void timeShiftPause() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftPause(iBinder, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void timeShiftResume() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftResume(iBinder, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void timeShiftSeekTo(long param1Long) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftSeekTo(iBinder, param1Long, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void timeShiftSetPlaybackParams(PlaybackParams param1PlaybackParams) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftSetPlaybackParams(iBinder, param1PlaybackParams, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void timeShiftEnablePositionTracking(boolean param1Boolean) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.timeShiftEnablePositionTracking(iBinder, param1Boolean, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void startRecording(Uri param1Uri) {
      startRecording(param1Uri, null);
    }
    
    void startRecording(Uri param1Uri, Bundle param1Bundle) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.startRecording(iBinder, param1Uri, param1Bundle, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void stopRecording() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.stopRecording(iBinder, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public void sendAppPrivateCommand(String param1String, Bundle param1Bundle) {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.sendAppPrivateCommand(iBinder, param1String, param1Bundle, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void createOverlayView(View param1View, Rect param1Rect) {
      Preconditions.checkNotNull(param1View);
      Preconditions.checkNotNull(param1Rect);
      if (param1View.getWindowToken() != null) {
        IBinder iBinder = this.mToken;
        if (iBinder == null) {
          Log.w("TvInputManager", "The session has been already released");
          return;
        } 
        try {
          this.mService.createOverlayView(iBinder, param1View.getWindowToken(), param1Rect, this.mUserId);
          return;
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
      throw new IllegalStateException("view must be attached to a window");
    }
    
    void relayoutOverlayView(Rect param1Rect) {
      Preconditions.checkNotNull(param1Rect);
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.relayoutOverlayView(iBinder, param1Rect, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void removeOverlayView() {
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.removeOverlayView(iBinder, this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    void unblockContent(TvContentRating param1TvContentRating) {
      Preconditions.checkNotNull(param1TvContentRating);
      IBinder iBinder = this.mToken;
      if (iBinder == null) {
        Log.w("TvInputManager", "The session has been already released");
        return;
      } 
      try {
        this.mService.unblockContent(iBinder, param1TvContentRating.flattenToString(), this.mUserId);
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public int dispatchInputEvent(InputEvent param1InputEvent, Object param1Object, FinishedInputEventCallback param1FinishedInputEventCallback, Handler param1Handler) {
      Preconditions.checkNotNull(param1InputEvent);
      Preconditions.checkNotNull(param1FinishedInputEventCallback);
      Preconditions.checkNotNull(param1Handler);
      synchronized (this.mHandler) {
        if (this.mChannel == null)
          return 0; 
        PendingEvent pendingEvent = obtainPendingEventLocked(param1InputEvent, param1Object, param1FinishedInputEventCallback, param1Handler);
        if (Looper.myLooper() == Looper.getMainLooper())
          return sendInputEventOnMainLooperLocked(pendingEvent); 
        Message message = this.mHandler.obtainMessage(1, pendingEvent);
        message.setAsynchronous(true);
        this.mHandler.sendMessage(message);
        return -1;
      } 
    }
    
    private void sendInputEventAndReportResultOnMainLooper(PendingEvent param1PendingEvent) {
      synchronized (this.mHandler) {
        int i = sendInputEventOnMainLooperLocked(param1PendingEvent);
        if (i == -1)
          return; 
        invokeFinishedInputEventCallback(param1PendingEvent, false);
        return;
      } 
    }
    
    private int sendInputEventOnMainLooperLocked(PendingEvent param1PendingEvent) {
      InputChannel inputChannel = this.mChannel;
      if (inputChannel != null) {
        if (this.mSender == null)
          this.mSender = new TvInputEventSender(inputChannel, this.mHandler.getLooper()); 
        InputEvent inputEvent = param1PendingEvent.mEvent;
        int i = inputEvent.getSequenceNumber();
        if (this.mSender.sendInputEvent(i, inputEvent)) {
          this.mPendingEvents.put(i, param1PendingEvent);
          Message message = this.mHandler.obtainMessage(2, param1PendingEvent);
          message.setAsynchronous(true);
          this.mHandler.sendMessageDelayed(message, 2500L);
          return -1;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unable to send input event to session: ");
        stringBuilder.append(this.mToken);
        stringBuilder.append(" dropping:");
        stringBuilder.append(inputEvent);
        Log.w("TvInputManager", stringBuilder.toString());
      } 
      return 0;
    }
    
    void finishedInputEvent(int param1Int, boolean param1Boolean1, boolean param1Boolean2) {
      synchronized (this.mHandler) {
        param1Int = this.mPendingEvents.indexOfKey(param1Int);
        if (param1Int < 0)
          return; 
        PendingEvent pendingEvent = (PendingEvent)this.mPendingEvents.valueAt(param1Int);
        this.mPendingEvents.removeAt(param1Int);
        if (param1Boolean2) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Timeout waiting for session to handle input event after 2500 ms: ");
          stringBuilder.append(this.mToken);
          Log.w("TvInputManager", stringBuilder.toString());
        } else {
          this.mHandler.removeMessages(2, pendingEvent);
        } 
        invokeFinishedInputEventCallback(pendingEvent, param1Boolean1);
        return;
      } 
    }
    
    void invokeFinishedInputEventCallback(PendingEvent param1PendingEvent, boolean param1Boolean) {
      param1PendingEvent.mHandled = param1Boolean;
      if (param1PendingEvent.mEventHandler.getLooper().isCurrentThread()) {
        param1PendingEvent.run();
      } else {
        Message message = Message.obtain(param1PendingEvent.mEventHandler, param1PendingEvent);
        message.setAsynchronous(true);
        message.sendToTarget();
      } 
    }
    
    private void flushPendingEventsLocked() {
      this.mHandler.removeMessages(3);
      int i = this.mPendingEvents.size();
      for (byte b = 0; b < i; b++) {
        int j = this.mPendingEvents.keyAt(b);
        Message message = this.mHandler.obtainMessage(3, j, 0);
        message.setAsynchronous(true);
        message.sendToTarget();
      } 
    }
    
    private PendingEvent obtainPendingEventLocked(InputEvent param1InputEvent, Object param1Object, FinishedInputEventCallback param1FinishedInputEventCallback, Handler param1Handler) {
      PendingEvent pendingEvent1 = (PendingEvent)this.mPendingEventPool.acquire();
      PendingEvent pendingEvent2 = pendingEvent1;
      if (pendingEvent1 == null)
        pendingEvent2 = new PendingEvent(); 
      pendingEvent2.mEvent = param1InputEvent;
      pendingEvent2.mEventToken = param1Object;
      pendingEvent2.mCallback = param1FinishedInputEventCallback;
      pendingEvent2.mEventHandler = param1Handler;
      return pendingEvent2;
    }
    
    private void recyclePendingEventLocked(PendingEvent param1PendingEvent) {
      param1PendingEvent.recycle();
      this.mPendingEventPool.release(param1PendingEvent);
    }
    
    IBinder getToken() {
      return this.mToken;
    }
    
    private void releaseInternal() {
      this.mToken = null;
      synchronized (this.mHandler) {
        if (this.mChannel != null) {
          if (this.mSender != null) {
            flushPendingEventsLocked();
            this.mSender.dispose();
            this.mSender = null;
          } 
          this.mChannel.dispose();
          this.mChannel = null;
        } 
        synchronized (this.mSessionCallbackRecordMap) {
          this.mSessionCallbackRecordMap.delete(this.mSeq);
          return;
        } 
      } 
    }
    
    class InputEventHandler extends Handler {
      public static final int MSG_FLUSH_INPUT_EVENT = 3;
      
      public static final int MSG_SEND_INPUT_EVENT = 1;
      
      public static final int MSG_TIMEOUT_INPUT_EVENT = 2;
      
      final TvInputManager.Session this$0;
      
      InputEventHandler(Looper param2Looper) {
        super(param2Looper, null, true);
      }
      
      public void handleMessage(Message param2Message) {
        int i = param2Message.what;
        if (i != 1) {
          if (i != 2) {
            if (i != 3)
              return; 
            TvInputManager.Session.this.finishedInputEvent(param2Message.arg1, false, false);
            return;
          } 
          TvInputManager.Session.this.finishedInputEvent(param2Message.arg1, false, true);
          return;
        } 
        TvInputManager.Session.this.sendInputEventAndReportResultOnMainLooper((TvInputManager.Session.PendingEvent)param2Message.obj);
      }
    }
    
    class TvInputEventSender extends InputEventSender {
      final TvInputManager.Session this$0;
      
      public TvInputEventSender(InputChannel param2InputChannel, Looper param2Looper) {
        super(param2InputChannel, param2Looper);
      }
      
      public void onInputEventFinished(int param2Int, boolean param2Boolean) {
        TvInputManager.Session.this.finishedInputEvent(param2Int, param2Boolean, false);
      }
    }
    
    private final class PendingEvent implements Runnable {
      public TvInputManager.Session.FinishedInputEventCallback mCallback;
      
      public InputEvent mEvent;
      
      public Handler mEventHandler;
      
      public Object mEventToken;
      
      public boolean mHandled;
      
      final TvInputManager.Session this$0;
      
      private PendingEvent() {}
      
      public void recycle() {
        this.mEvent = null;
        this.mEventToken = null;
        this.mCallback = null;
        this.mEventHandler = null;
        this.mHandled = false;
      }
      
      public void run() {
        this.mCallback.onFinishedInputEvent(this.mEventToken, this.mHandled);
        synchronized (this.mEventHandler) {
          TvInputManager.Session.this.recyclePendingEventLocked(this);
          return;
        } 
      }
    }
    
    public static interface FinishedInputEventCallback {
      void onFinishedInputEvent(Object param2Object, boolean param2Boolean);
    }
  }
  
  @SystemApi
  class Hardware {
    private final ITvInputHardware mInterface;
    
    private Hardware(TvInputManager this$0) {
      this.mInterface = (ITvInputHardware)this$0;
    }
    
    private ITvInputHardware getInterface() {
      return this.mInterface;
    }
    
    public boolean setSurface(Surface param1Surface, TvStreamConfig param1TvStreamConfig) {
      try {
        return this.mInterface.setSurface(param1Surface, param1TvStreamConfig);
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      } 
    }
    
    public void setStreamVolume(float param1Float) {
      try {
        this.mInterface.setStreamVolume(param1Float);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      } 
    }
    
    @SystemApi
    public boolean dispatchKeyEventToHdmi(KeyEvent param1KeyEvent) {
      return false;
    }
    
    public void overrideAudioSink(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4) {
      try {
        this.mInterface.overrideAudioSink(param1Int1, param1String, param1Int2, param1Int3, param1Int4);
        return;
      } catch (RemoteException remoteException) {
        throw new RuntimeException(remoteException);
      } 
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DvbDeviceType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class InputState implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class RecordingError implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TimeShiftStatus implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VideoUnavailableReason {}
}
