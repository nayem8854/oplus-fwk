package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.Surface;

public interface ITvInputHardware extends IInterface {
  void overrideAudioSink(int paramInt1, String paramString, int paramInt2, int paramInt3, int paramInt4) throws RemoteException;
  
  void setStreamVolume(float paramFloat) throws RemoteException;
  
  boolean setSurface(Surface paramSurface, TvStreamConfig paramTvStreamConfig) throws RemoteException;
  
  class Default implements ITvInputHardware {
    public boolean setSurface(Surface param1Surface, TvStreamConfig param1TvStreamConfig) throws RemoteException {
      return false;
    }
    
    public void setStreamVolume(float param1Float) throws RemoteException {}
    
    public void overrideAudioSink(int param1Int1, String param1String, int param1Int2, int param1Int3, int param1Int4) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputHardware {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputHardware";
    
    static final int TRANSACTION_overrideAudioSink = 3;
    
    static final int TRANSACTION_setStreamVolume = 2;
    
    static final int TRANSACTION_setSurface = 1;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputHardware");
    }
    
    public static ITvInputHardware asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputHardware");
      if (iInterface != null && iInterface instanceof ITvInputHardware)
        return (ITvInputHardware)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "overrideAudioSink";
        } 
        return "setStreamVolume";
      } 
      return "setSurface";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      Surface surface;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.media.tv.ITvInputHardware");
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.tv.ITvInputHardware");
          int i = param1Parcel1.readInt();
          surface = (Surface)param1Parcel1.readString();
          param1Int2 = param1Parcel1.readInt();
          param1Int1 = param1Parcel1.readInt();
          int j = param1Parcel1.readInt();
          overrideAudioSink(i, (String)surface, param1Int2, param1Int1, j);
          param1Parcel2.writeNoException();
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.tv.ITvInputHardware");
        float f = param1Parcel1.readFloat();
        setStreamVolume(f);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.tv.ITvInputHardware");
      if (param1Parcel1.readInt() != 0) {
        surface = Surface.CREATOR.createFromParcel(param1Parcel1);
      } else {
        surface = null;
      } 
      if (param1Parcel1.readInt() != 0) {
        TvStreamConfig tvStreamConfig = TvStreamConfig.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      boolean bool = setSurface(surface, (TvStreamConfig)param1Parcel1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeInt(bool);
      return true;
    }
    
    private static class Proxy implements ITvInputHardware {
      public static ITvInputHardware sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputHardware";
      }
      
      public boolean setSurface(Surface param2Surface, TvStreamConfig param2TvStreamConfig) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputHardware");
          boolean bool1 = true;
          if (param2Surface != null) {
            parcel1.writeInt(1);
            param2Surface.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2TvStreamConfig != null) {
            parcel1.writeInt(1);
            param2TvStreamConfig.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool2 = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool2 && ITvInputHardware.Stub.getDefaultImpl() != null) {
            bool1 = ITvInputHardware.Stub.getDefaultImpl().setSurface(param2Surface, param2TvStreamConfig);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i == 0)
            bool1 = false; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setStreamVolume(float param2Float) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputHardware");
          parcel1.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && ITvInputHardware.Stub.getDefaultImpl() != null) {
            ITvInputHardware.Stub.getDefaultImpl().setStreamVolume(param2Float);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overrideAudioSink(int param2Int1, String param2String, int param2Int2, int param2Int3, int param2Int4) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.tv.ITvInputHardware");
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          parcel1.writeInt(param2Int3);
          parcel1.writeInt(param2Int4);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && ITvInputHardware.Stub.getDefaultImpl() != null) {
            ITvInputHardware.Stub.getDefaultImpl().overrideAudioSink(param2Int1, param2String, param2Int2, param2Int3, param2Int4);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputHardware param1ITvInputHardware) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputHardware != null) {
          Proxy.sDefaultImpl = param1ITvInputHardware;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputHardware getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
