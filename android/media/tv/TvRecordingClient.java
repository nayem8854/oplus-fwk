package android.media.tv;

import android.annotation.SystemApi;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import java.util.ArrayDeque;
import java.util.Queue;

public class TvRecordingClient {
  private static final boolean DEBUG = false;
  
  private static final String TAG = "TvRecordingClient";
  
  private final RecordingCallback mCallback;
  
  private final Handler mHandler;
  
  private boolean mIsRecordingStarted;
  
  private boolean mIsTuned;
  
  private final Queue<Pair<String, Bundle>> mPendingAppPrivateCommands = new ArrayDeque<>();
  
  private TvInputManager.Session mSession;
  
  private MySessionCallback mSessionCallback;
  
  private final TvInputManager mTvInputManager;
  
  public TvRecordingClient(Context paramContext, String paramString, RecordingCallback paramRecordingCallback, Handler paramHandler) {
    this.mCallback = paramRecordingCallback;
    if (paramHandler == null)
      paramHandler = new Handler(Looper.getMainLooper()); 
    this.mHandler = paramHandler;
    this.mTvInputManager = (TvInputManager)paramContext.getSystemService("tv_input");
  }
  
  public void tune(String paramString, Uri paramUri) {
    tune(paramString, paramUri, null);
  }
  
  public void tune(String paramString, Uri paramUri, Bundle paramBundle) {
    if (!TextUtils.isEmpty(paramString)) {
      if (!this.mIsRecordingStarted) {
        TvInputManager.Session session;
        MySessionCallback mySessionCallback = this.mSessionCallback;
        if (mySessionCallback != null && TextUtils.equals(mySessionCallback.mInputId, paramString)) {
          session = this.mSession;
          if (session != null) {
            session.tune(paramUri, paramBundle);
          } else {
            this.mSessionCallback.mChannelUri = paramUri;
            this.mSessionCallback.mConnectionParams = paramBundle;
          } 
        } else {
          resetInternal();
          MySessionCallback mySessionCallback1 = new MySessionCallback((String)session, paramUri, paramBundle);
          TvInputManager tvInputManager = this.mTvInputManager;
          if (tvInputManager != null)
            tvInputManager.createRecordingSession((String)session, mySessionCallback1, this.mHandler); 
        } 
        return;
      } 
      throw new IllegalStateException("tune failed - recording already started");
    } 
    throw new IllegalArgumentException("inputId cannot be null or an empty string");
  }
  
  public void release() {
    resetInternal();
  }
  
  private void resetInternal() {
    this.mSessionCallback = null;
    this.mPendingAppPrivateCommands.clear();
    TvInputManager.Session session = this.mSession;
    if (session != null) {
      session.release();
      this.mIsTuned = false;
      this.mIsRecordingStarted = false;
      this.mSession = null;
    } 
  }
  
  public void startRecording(Uri paramUri) {
    startRecording(paramUri, Bundle.EMPTY);
  }
  
  public void startRecording(Uri paramUri, Bundle paramBundle) {
    if (this.mIsTuned) {
      TvInputManager.Session session = this.mSession;
      if (session != null) {
        session.startRecording(paramUri, paramBundle);
        this.mIsRecordingStarted = true;
      } 
      return;
    } 
    throw new IllegalStateException("startRecording failed - not yet tuned");
  }
  
  public void stopRecording() {
    if (!this.mIsRecordingStarted)
      Log.w("TvRecordingClient", "stopRecording failed - recording not yet started"); 
    TvInputManager.Session session = this.mSession;
    if (session != null)
      session.stopRecording(); 
  }
  
  public void sendAppPrivateCommand(String paramString, Bundle paramBundle) {
    if (!TextUtils.isEmpty(paramString)) {
      TvInputManager.Session session = this.mSession;
      if (session != null) {
        session.sendAppPrivateCommand(paramString, paramBundle);
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sendAppPrivateCommand - session not yet created (action \"");
        stringBuilder.append(paramString);
        stringBuilder.append("\" pending)");
        Log.w("TvRecordingClient", stringBuilder.toString());
        this.mPendingAppPrivateCommands.add(Pair.create(paramString, paramBundle));
      } 
      return;
    } 
    throw new IllegalArgumentException("action cannot be null or an empty string");
  }
  
  public static abstract class RecordingCallback {
    public void onConnectionFailed(String param1String) {}
    
    public void onDisconnected(String param1String) {}
    
    public void onTuned(Uri param1Uri) {}
    
    public void onRecordingStopped(Uri param1Uri) {}
    
    public void onError(int param1Int) {}
    
    @SystemApi
    public void onEvent(String param1String1, String param1String2, Bundle param1Bundle) {}
  }
  
  class MySessionCallback extends TvInputManager.SessionCallback {
    Uri mChannelUri;
    
    Bundle mConnectionParams;
    
    final String mInputId;
    
    final TvRecordingClient this$0;
    
    MySessionCallback(String param1String, Uri param1Uri, Bundle param1Bundle) {
      this.mInputId = param1String;
      this.mChannelUri = param1Uri;
      this.mConnectionParams = param1Bundle;
    }
    
    public void onSessionCreated(TvInputManager.Session param1Session) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onSessionCreated - session already created");
        if (param1Session != null)
          param1Session.release(); 
        return;
      } 
      TvRecordingClient.access$102(TvRecordingClient.this, param1Session);
      if (param1Session != null) {
        for (Pair pair : TvRecordingClient.this.mPendingAppPrivateCommands)
          TvRecordingClient.this.mSession.sendAppPrivateCommand((String)pair.first, (Bundle)pair.second); 
        TvRecordingClient.this.mPendingAppPrivateCommands.clear();
        TvRecordingClient.this.mSession.tune(this.mChannelUri, this.mConnectionParams);
      } else {
        TvRecordingClient.access$002(TvRecordingClient.this, null);
        if (TvRecordingClient.this.mCallback != null)
          TvRecordingClient.this.mCallback.onConnectionFailed(this.mInputId); 
      } 
    }
    
    void onTuned(TvInputManager.Session param1Session, Uri param1Uri) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onTuned - session not created");
        return;
      } 
      TvRecordingClient.access$402(TvRecordingClient.this, true);
      TvRecordingClient.this.mCallback.onTuned(param1Uri);
    }
    
    public void onSessionReleased(TvInputManager.Session param1Session) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onSessionReleased - session not created");
        return;
      } 
      TvRecordingClient.access$402(TvRecordingClient.this, false);
      TvRecordingClient.access$502(TvRecordingClient.this, false);
      TvRecordingClient.access$002(TvRecordingClient.this, null);
      TvRecordingClient.access$102(TvRecordingClient.this, null);
      if (TvRecordingClient.this.mCallback != null)
        TvRecordingClient.this.mCallback.onDisconnected(this.mInputId); 
    }
    
    public void onRecordingStopped(TvInputManager.Session param1Session, Uri param1Uri) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onRecordingStopped - session not created");
        return;
      } 
      TvRecordingClient.access$502(TvRecordingClient.this, false);
      TvRecordingClient.this.mCallback.onRecordingStopped(param1Uri);
    }
    
    public void onError(TvInputManager.Session param1Session, int param1Int) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onError - session not created");
        return;
      } 
      TvRecordingClient.this.mCallback.onError(param1Int);
    }
    
    public void onSessionEvent(TvInputManager.Session param1Session, String param1String, Bundle param1Bundle) {
      if (this != TvRecordingClient.this.mSessionCallback) {
        Log.w("TvRecordingClient", "onSessionEvent - session not created");
        return;
      } 
      if (TvRecordingClient.this.mCallback != null)
        TvRecordingClient.this.mCallback.onEvent(this.mInputId, param1String, param1Bundle); 
    }
  }
}
