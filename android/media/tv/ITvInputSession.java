package android.media.tv;

import android.graphics.Rect;
import android.media.PlaybackParams;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.Surface;

public interface ITvInputSession extends IInterface {
  void appPrivateCommand(String paramString, Bundle paramBundle) throws RemoteException;
  
  void createOverlayView(IBinder paramIBinder, Rect paramRect) throws RemoteException;
  
  void dispatchSurfaceChanged(int paramInt1, int paramInt2, int paramInt3) throws RemoteException;
  
  void relayoutOverlayView(Rect paramRect) throws RemoteException;
  
  void release() throws RemoteException;
  
  void removeOverlayView() throws RemoteException;
  
  void selectTrack(int paramInt, String paramString) throws RemoteException;
  
  void setCaptionEnabled(boolean paramBoolean) throws RemoteException;
  
  void setMain(boolean paramBoolean) throws RemoteException;
  
  void setSurface(Surface paramSurface) throws RemoteException;
  
  void setVolume(float paramFloat) throws RemoteException;
  
  void startRecording(Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void stopRecording() throws RemoteException;
  
  void timeShiftEnablePositionTracking(boolean paramBoolean) throws RemoteException;
  
  void timeShiftPause() throws RemoteException;
  
  void timeShiftPlay(Uri paramUri) throws RemoteException;
  
  void timeShiftResume() throws RemoteException;
  
  void timeShiftSeekTo(long paramLong) throws RemoteException;
  
  void timeShiftSetPlaybackParams(PlaybackParams paramPlaybackParams) throws RemoteException;
  
  void tune(Uri paramUri, Bundle paramBundle) throws RemoteException;
  
  void unblockContent(String paramString) throws RemoteException;
  
  class Default implements ITvInputSession {
    public void release() throws RemoteException {}
    
    public void setMain(boolean param1Boolean) throws RemoteException {}
    
    public void setSurface(Surface param1Surface) throws RemoteException {}
    
    public void dispatchSurfaceChanged(int param1Int1, int param1Int2, int param1Int3) throws RemoteException {}
    
    public void setVolume(float param1Float) throws RemoteException {}
    
    public void tune(Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void setCaptionEnabled(boolean param1Boolean) throws RemoteException {}
    
    public void selectTrack(int param1Int, String param1String) throws RemoteException {}
    
    public void appPrivateCommand(String param1String, Bundle param1Bundle) throws RemoteException {}
    
    public void createOverlayView(IBinder param1IBinder, Rect param1Rect) throws RemoteException {}
    
    public void relayoutOverlayView(Rect param1Rect) throws RemoteException {}
    
    public void removeOverlayView() throws RemoteException {}
    
    public void unblockContent(String param1String) throws RemoteException {}
    
    public void timeShiftPlay(Uri param1Uri) throws RemoteException {}
    
    public void timeShiftPause() throws RemoteException {}
    
    public void timeShiftResume() throws RemoteException {}
    
    public void timeShiftSeekTo(long param1Long) throws RemoteException {}
    
    public void timeShiftSetPlaybackParams(PlaybackParams param1PlaybackParams) throws RemoteException {}
    
    public void timeShiftEnablePositionTracking(boolean param1Boolean) throws RemoteException {}
    
    public void startRecording(Uri param1Uri, Bundle param1Bundle) throws RemoteException {}
    
    public void stopRecording() throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputSession {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputSession";
    
    static final int TRANSACTION_appPrivateCommand = 9;
    
    static final int TRANSACTION_createOverlayView = 10;
    
    static final int TRANSACTION_dispatchSurfaceChanged = 4;
    
    static final int TRANSACTION_relayoutOverlayView = 11;
    
    static final int TRANSACTION_release = 1;
    
    static final int TRANSACTION_removeOverlayView = 12;
    
    static final int TRANSACTION_selectTrack = 8;
    
    static final int TRANSACTION_setCaptionEnabled = 7;
    
    static final int TRANSACTION_setMain = 2;
    
    static final int TRANSACTION_setSurface = 3;
    
    static final int TRANSACTION_setVolume = 5;
    
    static final int TRANSACTION_startRecording = 20;
    
    static final int TRANSACTION_stopRecording = 21;
    
    static final int TRANSACTION_timeShiftEnablePositionTracking = 19;
    
    static final int TRANSACTION_timeShiftPause = 15;
    
    static final int TRANSACTION_timeShiftPlay = 14;
    
    static final int TRANSACTION_timeShiftResume = 16;
    
    static final int TRANSACTION_timeShiftSeekTo = 17;
    
    static final int TRANSACTION_timeShiftSetPlaybackParams = 18;
    
    static final int TRANSACTION_tune = 6;
    
    static final int TRANSACTION_unblockContent = 13;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputSession");
    }
    
    public static ITvInputSession asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputSession");
      if (iInterface != null && iInterface instanceof ITvInputSession)
        return (ITvInputSession)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 21:
          return "stopRecording";
        case 20:
          return "startRecording";
        case 19:
          return "timeShiftEnablePositionTracking";
        case 18:
          return "timeShiftSetPlaybackParams";
        case 17:
          return "timeShiftSeekTo";
        case 16:
          return "timeShiftResume";
        case 15:
          return "timeShiftPause";
        case 14:
          return "timeShiftPlay";
        case 13:
          return "unblockContent";
        case 12:
          return "removeOverlayView";
        case 11:
          return "relayoutOverlayView";
        case 10:
          return "createOverlayView";
        case 9:
          return "appPrivateCommand";
        case 8:
          return "selectTrack";
        case 7:
          return "setCaptionEnabled";
        case 6:
          return "tune";
        case 5:
          return "setVolume";
        case 4:
          return "dispatchSurfaceChanged";
        case 3:
          return "setSurface";
        case 2:
          return "setMain";
        case 1:
          break;
      } 
      return "release";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        IBinder iBinder;
        long l;
        float f;
        int i;
        boolean bool1 = false, bool2 = false, bool3 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 21:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            stopRecording();
            return true;
          case 20:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            startRecording((Uri)param1Parcel2, (Bundle)param1Parcel1);
            return true;
          case 19:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            bool2 = bool3;
            if (param1Parcel1.readInt() != 0)
              bool2 = true; 
            timeShiftEnablePositionTracking(bool2);
            return true;
          case 18:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            if (param1Parcel1.readInt() != 0) {
              PlaybackParams playbackParams = PlaybackParams.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            timeShiftSetPlaybackParams((PlaybackParams)param1Parcel1);
            return true;
          case 17:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            l = param1Parcel1.readLong();
            timeShiftSeekTo(l);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            timeShiftResume();
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            timeShiftPause();
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            timeShiftPlay((Uri)param1Parcel1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputSession");
            str1 = param1Parcel1.readString();
            unblockContent(str1);
            return true;
          case 12:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            removeOverlayView();
            return true;
          case 11:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            if (str1.readInt() != 0) {
              Rect rect = Rect.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            relayoutOverlayView((Rect)str1);
            return true;
          case 10:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            iBinder = str1.readStrongBinder();
            if (str1.readInt() != 0) {
              Rect rect = Rect.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            createOverlayView(iBinder, (Rect)str1);
            return true;
          case 9:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            str = str1.readString();
            if (str1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            appPrivateCommand(str, (Bundle)str1);
            return true;
          case 8:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            param1Int1 = str1.readInt();
            str1 = str1.readString();
            selectTrack(param1Int1, str1);
            return true;
          case 7:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            bool2 = bool1;
            if (str1.readInt() != 0)
              bool2 = true; 
            setCaptionEnabled(bool2);
            return true;
          case 6:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            if (str1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            if (str1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            tune((Uri)str, (Bundle)str1);
            return true;
          case 5:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            f = str1.readFloat();
            setVolume(f);
            return true;
          case 4:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            param1Int1 = str1.readInt();
            param1Int2 = str1.readInt();
            i = str1.readInt();
            dispatchSurfaceChanged(param1Int1, param1Int2, i);
            return true;
          case 3:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            if (str1.readInt() != 0) {
              Surface surface = Surface.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str1 = null;
            } 
            setSurface((Surface)str1);
            return true;
          case 2:
            str1.enforceInterface("android.media.tv.ITvInputSession");
            if (str1.readInt() != 0)
              bool2 = true; 
            setMain(bool2);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.media.tv.ITvInputSession");
        release();
        return true;
      } 
      str.writeString("android.media.tv.ITvInputSession");
      return true;
    }
    
    private static class Proxy implements ITvInputSession {
      public static ITvInputSession sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputSession";
      }
      
      public void release() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().release();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setMain(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool1 && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().setMain(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setSurface(Surface param2Surface) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Surface != null) {
            parcel.writeInt(1);
            param2Surface.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().setSurface(param2Surface);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchSurfaceChanged(int param2Int1, int param2Int2, int param2Int3) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().dispatchSurfaceChanged(param2Int1, param2Int2, param2Int3);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVolume(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().setVolume(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void tune(Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().tune(param2Uri, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setCaptionEnabled(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool1 && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().setCaptionEnabled(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void selectTrack(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().selectTrack(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void appPrivateCommand(String param2String, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().appPrivateCommand(param2String, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createOverlayView(IBinder param2IBinder, Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeStrongBinder(param2IBinder);
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().createOverlayView(param2IBinder, param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void relayoutOverlayView(Rect param2Rect) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Rect != null) {
            parcel.writeInt(1);
            param2Rect.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().relayoutOverlayView(param2Rect);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeOverlayView() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().removeOverlayView();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unblockContent(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().unblockContent(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftPlay(Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftPlay(param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftPause() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftPause();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftResume() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftResume();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftSeekTo(long param2Long) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          parcel.writeLong(param2Long);
          boolean bool = this.mRemote.transact(17, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftSeekTo(param2Long);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftSetPlaybackParams(PlaybackParams param2PlaybackParams) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2PlaybackParams != null) {
            parcel.writeInt(1);
            param2PlaybackParams.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftSetPlaybackParams(param2PlaybackParams);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void timeShiftEnablePositionTracking(boolean param2Boolean) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          boolean bool;
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel.writeInt(bool);
          boolean bool1 = this.mRemote.transact(19, parcel, (Parcel)null, 1);
          if (!bool1 && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().timeShiftEnablePositionTracking(param2Boolean);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void startRecording(Uri param2Uri, Bundle param2Bundle) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(20, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().startRecording(param2Uri, param2Bundle);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stopRecording() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputSession");
          boolean bool = this.mRemote.transact(21, parcel, (Parcel)null, 1);
          if (!bool && ITvInputSession.Stub.getDefaultImpl() != null) {
            ITvInputSession.Stub.getDefaultImpl().stopRecording();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputSession param1ITvInputSession) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputSession != null) {
          Proxy.sDefaultImpl = param1ITvInputSession;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputSession getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
