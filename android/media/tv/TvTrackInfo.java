package android.media.tv;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

public final class TvTrackInfo implements Parcelable {
  private TvTrackInfo(int paramInt1, String paramString1, String paramString2, CharSequence paramCharSequence, String paramString3, boolean paramBoolean1, int paramInt2, int paramInt3, boolean paramBoolean2, boolean paramBoolean3, boolean paramBoolean4, int paramInt4, int paramInt5, float paramFloat1, float paramFloat2, byte paramByte, Bundle paramBundle) {
    this.mType = paramInt1;
    this.mId = paramString1;
    this.mLanguage = paramString2;
    this.mDescription = paramCharSequence;
    this.mEncoding = paramString3;
    this.mEncrypted = paramBoolean1;
    this.mAudioChannelCount = paramInt2;
    this.mAudioSampleRate = paramInt3;
    this.mAudioDescription = paramBoolean2;
    this.mHardOfHearing = paramBoolean3;
    this.mSpokenSubtitle = paramBoolean4;
    this.mVideoWidth = paramInt4;
    this.mVideoHeight = paramInt5;
    this.mVideoFrameRate = paramFloat1;
    this.mVideoPixelAspectRatio = paramFloat2;
    this.mVideoActiveFormatDescription = paramByte;
    this.mExtra = paramBundle;
  }
  
  private TvTrackInfo(Parcel paramParcel) {
    boolean bool2;
    this.mType = paramParcel.readInt();
    this.mId = paramParcel.readString();
    this.mLanguage = paramParcel.readString();
    this.mDescription = paramParcel.readString();
    this.mEncoding = paramParcel.readString();
    int i = paramParcel.readInt();
    boolean bool1 = true;
    if (i != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mEncrypted = bool2;
    this.mAudioChannelCount = paramParcel.readInt();
    this.mAudioSampleRate = paramParcel.readInt();
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mAudioDescription = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mHardOfHearing = bool2;
    if (paramParcel.readInt() != 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    this.mSpokenSubtitle = bool2;
    this.mVideoWidth = paramParcel.readInt();
    this.mVideoHeight = paramParcel.readInt();
    this.mVideoFrameRate = paramParcel.readFloat();
    this.mVideoPixelAspectRatio = paramParcel.readFloat();
    this.mVideoActiveFormatDescription = paramParcel.readByte();
    this.mExtra = paramParcel.readBundle();
  }
  
  public final int getType() {
    return this.mType;
  }
  
  public final String getId() {
    return this.mId;
  }
  
  public final String getLanguage() {
    return this.mLanguage;
  }
  
  public final CharSequence getDescription() {
    return this.mDescription;
  }
  
  public String getEncoding() {
    return this.mEncoding;
  }
  
  public boolean isEncrypted() {
    return this.mEncrypted;
  }
  
  public final int getAudioChannelCount() {
    if (this.mType == 0)
      return this.mAudioChannelCount; 
    throw new IllegalStateException("Not an audio track");
  }
  
  public final int getAudioSampleRate() {
    if (this.mType == 0)
      return this.mAudioSampleRate; 
    throw new IllegalStateException("Not an audio track");
  }
  
  public boolean isAudioDescription() {
    if (this.mType == 0)
      return this.mAudioDescription; 
    throw new IllegalStateException("Not an audio track");
  }
  
  public boolean isHardOfHearing() {
    int i = this.mType;
    if (i == 0 || i == 2)
      return this.mHardOfHearing; 
    throw new IllegalStateException("Not an audio or a subtitle track");
  }
  
  public boolean isSpokenSubtitle() {
    if (this.mType == 0)
      return this.mSpokenSubtitle; 
    throw new IllegalStateException("Not an audio track");
  }
  
  public final int getVideoWidth() {
    if (this.mType == 1)
      return this.mVideoWidth; 
    throw new IllegalStateException("Not a video track");
  }
  
  public final int getVideoHeight() {
    if (this.mType == 1)
      return this.mVideoHeight; 
    throw new IllegalStateException("Not a video track");
  }
  
  public final float getVideoFrameRate() {
    if (this.mType == 1)
      return this.mVideoFrameRate; 
    throw new IllegalStateException("Not a video track");
  }
  
  public final float getVideoPixelAspectRatio() {
    if (this.mType == 1)
      return this.mVideoPixelAspectRatio; 
    throw new IllegalStateException("Not a video track");
  }
  
  public final byte getVideoActiveFormatDescription() {
    if (this.mType == 1)
      return this.mVideoActiveFormatDescription; 
    throw new IllegalStateException("Not a video track");
  }
  
  public final Bundle getExtra() {
    return this.mExtra;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    Preconditions.checkNotNull(paramParcel);
    paramParcel.writeInt(this.mType);
    paramParcel.writeString(this.mId);
    paramParcel.writeString(this.mLanguage);
    CharSequence charSequence = this.mDescription;
    if (charSequence != null) {
      charSequence = charSequence.toString();
    } else {
      charSequence = null;
    } 
    paramParcel.writeString((String)charSequence);
    paramParcel.writeString(this.mEncoding);
    paramParcel.writeInt(this.mEncrypted);
    paramParcel.writeInt(this.mAudioChannelCount);
    paramParcel.writeInt(this.mAudioSampleRate);
    paramParcel.writeInt(this.mAudioDescription);
    paramParcel.writeInt(this.mHardOfHearing);
    paramParcel.writeInt(this.mSpokenSubtitle);
    paramParcel.writeInt(this.mVideoWidth);
    paramParcel.writeInt(this.mVideoHeight);
    paramParcel.writeFloat(this.mVideoFrameRate);
    paramParcel.writeFloat(this.mVideoPixelAspectRatio);
    paramParcel.writeByte(this.mVideoActiveFormatDescription);
    paramParcel.writeBundle(this.mExtra);
  }
  
  public boolean equals(Object paramObject) {
    boolean bool1 = true, bool2 = true, bool3 = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof TvTrackInfo))
      return false; 
    paramObject = paramObject;
    if (TextUtils.equals(this.mId, ((TvTrackInfo)paramObject).mId) && this.mType == ((TvTrackInfo)paramObject).mType) {
      String str1 = this.mLanguage, str2 = ((TvTrackInfo)paramObject).mLanguage;
      if (TextUtils.equals(str1, str2)) {
        CharSequence charSequence2 = this.mDescription, charSequence1 = ((TvTrackInfo)paramObject).mDescription;
        if (TextUtils.equals(charSequence2, charSequence1)) {
          charSequence2 = this.mEncoding;
          charSequence1 = ((TvTrackInfo)paramObject).mEncoding;
          if (TextUtils.equals(charSequence2, charSequence1) && this.mEncrypted == ((TvTrackInfo)paramObject).mEncrypted) {
            int i = this.mType;
            if (i != 0) {
              if (i != 1) {
                if (i != 2)
                  return true; 
                if (this.mHardOfHearing == ((TvTrackInfo)paramObject).mHardOfHearing) {
                  bool2 = bool3;
                } else {
                  bool2 = false;
                } 
                return bool2;
              } 
              if (this.mVideoWidth == ((TvTrackInfo)paramObject).mVideoWidth && this.mVideoHeight == ((TvTrackInfo)paramObject).mVideoHeight && this.mVideoFrameRate == ((TvTrackInfo)paramObject).mVideoFrameRate && this.mVideoPixelAspectRatio == ((TvTrackInfo)paramObject).mVideoPixelAspectRatio && this.mVideoActiveFormatDescription == ((TvTrackInfo)paramObject).mVideoActiveFormatDescription) {
                bool2 = bool1;
              } else {
                bool2 = false;
              } 
              return bool2;
            } 
            if (this.mAudioChannelCount != ((TvTrackInfo)paramObject).mAudioChannelCount || this.mAudioSampleRate != ((TvTrackInfo)paramObject).mAudioSampleRate || this.mAudioDescription != ((TvTrackInfo)paramObject).mAudioDescription || this.mHardOfHearing != ((TvTrackInfo)paramObject).mHardOfHearing || this.mSpokenSubtitle != ((TvTrackInfo)paramObject).mSpokenSubtitle)
              bool2 = false; 
            return bool2;
          } 
        } 
      } 
    } 
    return false;
  }
  
  public int hashCode() {
    int k, i = Objects.hash(new Object[] { this.mId, Integer.valueOf(this.mType), this.mLanguage, this.mDescription });
    int j = this.mType;
    if (j == 0) {
      k = Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(this.mAudioChannelCount), Integer.valueOf(this.mAudioSampleRate) });
    } else {
      k = i;
      if (j == 1) {
        j = this.mVideoWidth;
        k = this.mVideoHeight;
        float f1 = this.mVideoFrameRate, f2 = this.mVideoPixelAspectRatio;
        k = Objects.hash(new Object[] { Integer.valueOf(i), Integer.valueOf(j), Integer.valueOf(k), Float.valueOf(f1), Float.valueOf(f2) });
      } 
    } 
    return k;
  }
  
  public static final Parcelable.Creator<TvTrackInfo> CREATOR = new Parcelable.Creator<TvTrackInfo>() {
      public TvTrackInfo createFromParcel(Parcel param1Parcel) {
        return new TvTrackInfo(param1Parcel);
      }
      
      public TvTrackInfo[] newArray(int param1Int) {
        return new TvTrackInfo[param1Int];
      }
    };
  
  public static final int TYPE_AUDIO = 0;
  
  public static final int TYPE_SUBTITLE = 2;
  
  public static final int TYPE_VIDEO = 1;
  
  private final int mAudioChannelCount;
  
  private final boolean mAudioDescription;
  
  private final int mAudioSampleRate;
  
  private final CharSequence mDescription;
  
  private final String mEncoding;
  
  private final boolean mEncrypted;
  
  private final Bundle mExtra;
  
  private final boolean mHardOfHearing;
  
  private final String mId;
  
  private final String mLanguage;
  
  private final boolean mSpokenSubtitle;
  
  private final int mType;
  
  private final byte mVideoActiveFormatDescription;
  
  private final float mVideoFrameRate;
  
  private final int mVideoHeight;
  
  private final float mVideoPixelAspectRatio;
  
  private final int mVideoWidth;
  
  class Builder {
    private int mAudioChannelCount;
    
    private boolean mAudioDescription;
    
    private int mAudioSampleRate;
    
    private CharSequence mDescription;
    
    private String mEncoding;
    
    private boolean mEncrypted;
    
    private Bundle mExtra;
    
    private boolean mHardOfHearing;
    
    private final String mId;
    
    private String mLanguage;
    
    private boolean mSpokenSubtitle;
    
    private final int mType;
    
    private byte mVideoActiveFormatDescription;
    
    private float mVideoFrameRate;
    
    private int mVideoHeight;
    
    private float mVideoPixelAspectRatio = 1.0F;
    
    private int mVideoWidth;
    
    public Builder(TvTrackInfo this$0, String param1String) {
      if (this$0 == null || this$0 == true || this$0 == 2) {
        Preconditions.checkNotNull(param1String);
        this.mType = this$0;
        this.mId = param1String;
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown type: ");
      stringBuilder.append(this$0);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setLanguage(String param1String) {
      Preconditions.checkNotNull(param1String);
      this.mLanguage = param1String;
      return this;
    }
    
    public Builder setDescription(CharSequence param1CharSequence) {
      Preconditions.checkNotNull(param1CharSequence);
      this.mDescription = param1CharSequence;
      return this;
    }
    
    public Builder setEncoding(String param1String) {
      this.mEncoding = param1String;
      return this;
    }
    
    public Builder setEncrypted(boolean param1Boolean) {
      this.mEncrypted = param1Boolean;
      return this;
    }
    
    public Builder setAudioChannelCount(int param1Int) {
      if (this.mType == 0) {
        this.mAudioChannelCount = param1Int;
        return this;
      } 
      throw new IllegalStateException("Not an audio track");
    }
    
    public Builder setAudioSampleRate(int param1Int) {
      if (this.mType == 0) {
        this.mAudioSampleRate = param1Int;
        return this;
      } 
      throw new IllegalStateException("Not an audio track");
    }
    
    public Builder setAudioDescription(boolean param1Boolean) {
      if (this.mType == 0) {
        this.mAudioDescription = param1Boolean;
        return this;
      } 
      throw new IllegalStateException("Not an audio track");
    }
    
    public Builder setHardOfHearing(boolean param1Boolean) {
      int i = this.mType;
      if (i == 0 || i == 2) {
        this.mHardOfHearing = param1Boolean;
        return this;
      } 
      throw new IllegalStateException("Not an audio track or a subtitle track");
    }
    
    public Builder setSpokenSubtitle(boolean param1Boolean) {
      if (this.mType == 0) {
        this.mSpokenSubtitle = param1Boolean;
        return this;
      } 
      throw new IllegalStateException("Not an audio track");
    }
    
    public Builder setVideoWidth(int param1Int) {
      if (this.mType == 1) {
        this.mVideoWidth = param1Int;
        return this;
      } 
      throw new IllegalStateException("Not a video track");
    }
    
    public Builder setVideoHeight(int param1Int) {
      if (this.mType == 1) {
        this.mVideoHeight = param1Int;
        return this;
      } 
      throw new IllegalStateException("Not a video track");
    }
    
    public Builder setVideoFrameRate(float param1Float) {
      if (this.mType == 1) {
        this.mVideoFrameRate = param1Float;
        return this;
      } 
      throw new IllegalStateException("Not a video track");
    }
    
    public Builder setVideoPixelAspectRatio(float param1Float) {
      if (this.mType == 1) {
        this.mVideoPixelAspectRatio = param1Float;
        return this;
      } 
      throw new IllegalStateException("Not a video track");
    }
    
    public Builder setVideoActiveFormatDescription(byte param1Byte) {
      if (this.mType == 1) {
        this.mVideoActiveFormatDescription = param1Byte;
        return this;
      } 
      throw new IllegalStateException("Not a video track");
    }
    
    public Builder setExtra(Bundle param1Bundle) {
      Preconditions.checkNotNull(param1Bundle);
      this.mExtra = new Bundle(param1Bundle);
      return this;
    }
    
    public TvTrackInfo build() {
      return new TvTrackInfo(this.mType, this.mId, this.mLanguage, this.mDescription, this.mEncoding, this.mEncrypted, this.mAudioChannelCount, this.mAudioSampleRate, this.mAudioDescription, this.mHardOfHearing, this.mSpokenSubtitle, this.mVideoWidth, this.mVideoHeight, this.mVideoFrameRate, this.mVideoPixelAspectRatio, this.mVideoActiveFormatDescription, this.mExtra);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
