package android.media.tv;

import android.annotation.SystemApi;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.hardware.hdmi.HdmiControlManager;
import android.hardware.hdmi.HdmiDeviceInfo;
import android.hardware.hdmi.HdmiUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.UserHandle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.util.Xml;
import com.android.internal.R;
import java.io.IOException;
import java.io.InputStream;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public final class TvInputInfo implements Parcelable {
  @SystemApi
  @Deprecated
  public static TvInputInfo createTvInputInfo(Context paramContext, ResolveInfo paramResolveInfo, HdmiDeviceInfo paramHdmiDeviceInfo, String paramString1, String paramString2, Uri paramUri) throws XmlPullParserException, IOException {
    Builder builder = new Builder(paramContext, paramResolveInfo);
    builder = builder.setHdmiDeviceInfo(paramHdmiDeviceInfo);
    builder = builder.setParentId(paramString1);
    builder = builder.setLabel(paramString2);
    TvInputInfo tvInputInfo = builder.build();
    tvInputInfo.mIconUri = paramUri;
    return tvInputInfo;
  }
  
  @SystemApi
  @Deprecated
  public static TvInputInfo createTvInputInfo(Context paramContext, ResolveInfo paramResolveInfo, HdmiDeviceInfo paramHdmiDeviceInfo, String paramString, int paramInt, Icon paramIcon) throws XmlPullParserException, IOException {
    Builder builder = new Builder(paramContext, paramResolveInfo);
    builder = builder.setHdmiDeviceInfo(paramHdmiDeviceInfo);
    builder = builder.setParentId(paramString);
    builder = builder.setLabel(paramInt);
    builder = builder.setIcon(paramIcon);
    return builder.build();
  }
  
  @SystemApi
  @Deprecated
  public static TvInputInfo createTvInputInfo(Context paramContext, ResolveInfo paramResolveInfo, TvInputHardwareInfo paramTvInputHardwareInfo, String paramString, Uri paramUri) throws XmlPullParserException, IOException {
    Builder builder = new Builder(paramContext, paramResolveInfo);
    builder = builder.setTvInputHardwareInfo(paramTvInputHardwareInfo);
    builder = builder.setLabel(paramString);
    TvInputInfo tvInputInfo = builder.build();
    tvInputInfo.mIconUri = paramUri;
    return tvInputInfo;
  }
  
  @SystemApi
  @Deprecated
  public static TvInputInfo createTvInputInfo(Context paramContext, ResolveInfo paramResolveInfo, TvInputHardwareInfo paramTvInputHardwareInfo, int paramInt, Icon paramIcon) throws XmlPullParserException, IOException {
    Builder builder = new Builder(paramContext, paramResolveInfo);
    builder = builder.setTvInputHardwareInfo(paramTvInputHardwareInfo);
    builder = builder.setLabel(paramInt);
    builder = builder.setIcon(paramIcon);
    return builder.build();
  }
  
  private TvInputInfo(ResolveInfo paramResolveInfo, String paramString1, int paramInt1, boolean paramBoolean1, CharSequence paramCharSequence, int paramInt2, Icon paramIcon1, Icon paramIcon2, Icon paramIcon3, String paramString2, boolean paramBoolean2, int paramInt3, HdmiDeviceInfo paramHdmiDeviceInfo, boolean paramBoolean3, int paramInt4, String paramString3, Bundle paramBundle) {
    this.mService = paramResolveInfo;
    this.mId = paramString1;
    this.mType = paramInt1;
    this.mIsHardwareInput = paramBoolean1;
    this.mLabel = paramCharSequence;
    this.mLabelResId = paramInt2;
    this.mIcon = paramIcon1;
    this.mIconStandby = paramIcon2;
    this.mIconDisconnected = paramIcon3;
    this.mSetupActivity = paramString2;
    this.mCanRecord = paramBoolean2;
    this.mTunerCount = paramInt3;
    this.mHdmiDeviceInfo = paramHdmiDeviceInfo;
    this.mIsConnectedToHdmiSwitch = paramBoolean3;
    this.mHdmiConnectionRelativePosition = paramInt4;
    this.mParentId = paramString3;
    this.mExtras = paramBundle;
  }
  
  public String getId() {
    return this.mId;
  }
  
  public String getParentId() {
    return this.mParentId;
  }
  
  public ServiceInfo getServiceInfo() {
    return this.mService.serviceInfo;
  }
  
  public ComponentName getComponent() {
    return new ComponentName(this.mService.serviceInfo.packageName, this.mService.serviceInfo.name);
  }
  
  public Intent createSetupIntent() {
    if (!TextUtils.isEmpty(this.mSetupActivity)) {
      Intent intent = new Intent("android.intent.action.MAIN");
      intent.setClassName(this.mService.serviceInfo.packageName, this.mSetupActivity);
      intent.putExtra("android.media.tv.extra.INPUT_ID", getId());
      return intent;
    } 
    return null;
  }
  
  @Deprecated
  public Intent createSettingsIntent() {
    return null;
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getTunerCount() {
    return this.mTunerCount;
  }
  
  public boolean canRecord() {
    return this.mCanRecord;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  @SystemApi
  public HdmiDeviceInfo getHdmiDeviceInfo() {
    if (this.mType == 1007)
      return this.mHdmiDeviceInfo; 
    return null;
  }
  
  public boolean isPassthroughInput() {
    boolean bool;
    if (this.mType != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  @SystemApi
  public boolean isHardwareInput() {
    return this.mIsHardwareInput;
  }
  
  @SystemApi
  public boolean isConnectedToHdmiSwitch() {
    return this.mIsConnectedToHdmiSwitch;
  }
  
  public int getHdmiConnectionRelativePosition() {
    return this.mHdmiConnectionRelativePosition;
  }
  
  public boolean isHidden(Context paramContext) {
    return TvInputSettings.isHidden(paramContext, this.mId, UserHandle.myUserId());
  }
  
  public CharSequence loadLabel(Context paramContext) {
    if (this.mLabelResId != 0)
      return paramContext.getPackageManager().getText(this.mService.serviceInfo.packageName, this.mLabelResId, null); 
    if (!TextUtils.isEmpty(this.mLabel))
      return this.mLabel; 
    return this.mService.loadLabel(paramContext.getPackageManager());
  }
  
  public CharSequence loadCustomLabel(Context paramContext) {
    return TvInputSettings.getCustomLabel(paramContext, this.mId, UserHandle.myUserId());
  }
  
  public Drawable loadIcon(Context paramContext) {
    Icon icon = this.mIcon;
    if (icon != null)
      return icon.loadDrawable(paramContext); 
    if (this.mIconUri != null)
      try {
        InputStream inputStream = paramContext.getContentResolver().openInputStream(this.mIconUri);
        try {
          Drawable drawable = Drawable.createFromStream(inputStream, null);
          if (drawable != null)
            return drawable; 
        } finally {
          if (inputStream != null)
            try {
              inputStream.close();
            } finally {
              inputStream = null;
            }  
        } 
      } catch (IOException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Loading the default icon due to a failure on loading ");
        stringBuilder.append(this.mIconUri);
        Log.w("TvInputInfo", stringBuilder.toString(), iOException);
      }  
    return loadServiceIcon(paramContext);
  }
  
  @SystemApi
  public Drawable loadIcon(Context paramContext, int paramInt) {
    if (paramInt == 0)
      return loadIcon(paramContext); 
    if (paramInt == 1) {
      Icon icon = this.mIconStandby;
      if (icon != null)
        return icon.loadDrawable(paramContext); 
    } else {
      if (paramInt == 2) {
        Icon icon = this.mIconDisconnected;
        if (icon != null)
          return icon.loadDrawable(paramContext); 
        return null;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unknown state: ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return null;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public int hashCode() {
    return this.mId.hashCode();
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (paramObject == this)
      return true; 
    if (!(paramObject instanceof TvInputInfo))
      return false; 
    paramObject = paramObject;
    if (Objects.equals(this.mService, ((TvInputInfo)paramObject).mService)) {
      String str1 = this.mId, str2 = ((TvInputInfo)paramObject).mId;
      if (TextUtils.equals(str1, str2) && this.mType == ((TvInputInfo)paramObject).mType && this.mIsHardwareInput == ((TvInputInfo)paramObject).mIsHardwareInput) {
        CharSequence charSequence2 = this.mLabel, charSequence1 = ((TvInputInfo)paramObject).mLabel;
        if (TextUtils.equals(charSequence2, charSequence1)) {
          Uri uri2 = this.mIconUri, uri1 = ((TvInputInfo)paramObject).mIconUri;
          if (Objects.equals(uri2, uri1) && this.mLabelResId == ((TvInputInfo)paramObject).mLabelResId) {
            Icon icon1 = this.mIcon, icon2 = ((TvInputInfo)paramObject).mIcon;
            if (Objects.equals(icon1, icon2)) {
              icon2 = this.mIconStandby;
              icon1 = ((TvInputInfo)paramObject).mIconStandby;
              if (Objects.equals(icon2, icon1)) {
                icon1 = this.mIconDisconnected;
                icon2 = ((TvInputInfo)paramObject).mIconDisconnected;
                if (Objects.equals(icon1, icon2)) {
                  String str3 = this.mSetupActivity, str4 = ((TvInputInfo)paramObject).mSetupActivity;
                  if (TextUtils.equals(str3, str4) && this.mCanRecord == ((TvInputInfo)paramObject).mCanRecord && this.mTunerCount == ((TvInputInfo)paramObject).mTunerCount) {
                    HdmiDeviceInfo hdmiDeviceInfo1 = this.mHdmiDeviceInfo, hdmiDeviceInfo2 = ((TvInputInfo)paramObject).mHdmiDeviceInfo;
                    if (Objects.equals(hdmiDeviceInfo1, hdmiDeviceInfo2) && this.mIsConnectedToHdmiSwitch == ((TvInputInfo)paramObject).mIsConnectedToHdmiSwitch && this.mHdmiConnectionRelativePosition == ((TvInputInfo)paramObject).mHdmiConnectionRelativePosition) {
                      String str5 = this.mParentId, str6 = ((TvInputInfo)paramObject).mParentId;
                      if (TextUtils.equals(str5, str6)) {
                        Bundle bundle = this.mExtras;
                        paramObject = ((TvInputInfo)paramObject).mExtras;
                        if (Objects.equals(bundle, paramObject))
                          return null; 
                      } 
                    } 
                  } 
                } 
              } 
            } 
          } 
        } 
      } 
    } 
    return false;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("TvInputInfo{id=");
    stringBuilder.append(this.mId);
    stringBuilder.append(", pkg=");
    stringBuilder.append(this.mService.serviceInfo.packageName);
    stringBuilder.append(", service=");
    stringBuilder.append(this.mService.serviceInfo.name);
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    this.mService.writeToParcel(paramParcel, paramInt);
    paramParcel.writeString(this.mId);
    paramParcel.writeInt(this.mType);
    paramParcel.writeByte(this.mIsHardwareInput);
    TextUtils.writeToParcel(this.mLabel, paramParcel, paramInt);
    paramParcel.writeParcelable(this.mIconUri, paramInt);
    paramParcel.writeInt(this.mLabelResId);
    paramParcel.writeParcelable((Parcelable)this.mIcon, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mIconStandby, paramInt);
    paramParcel.writeParcelable((Parcelable)this.mIconDisconnected, paramInt);
    paramParcel.writeString(this.mSetupActivity);
    paramParcel.writeByte(this.mCanRecord);
    paramParcel.writeInt(this.mTunerCount);
    paramParcel.writeParcelable((Parcelable)this.mHdmiDeviceInfo, paramInt);
    paramParcel.writeByte(this.mIsConnectedToHdmiSwitch);
    paramParcel.writeInt(this.mHdmiConnectionRelativePosition);
    paramParcel.writeString(this.mParentId);
    paramParcel.writeBundle(this.mExtras);
  }
  
  private Drawable loadServiceIcon(Context paramContext) {
    if (this.mService.serviceInfo.icon == 0 && this.mService.serviceInfo.applicationInfo.icon == 0)
      return null; 
    return this.mService.serviceInfo.loadIcon(paramContext.getPackageManager());
  }
  
  public static final Parcelable.Creator<TvInputInfo> CREATOR = new Parcelable.Creator<TvInputInfo>() {
      public TvInputInfo createFromParcel(Parcel param1Parcel) {
        return new TvInputInfo(param1Parcel);
      }
      
      public TvInputInfo[] newArray(int param1Int) {
        return new TvInputInfo[param1Int];
      }
    };
  
  private static final boolean DEBUG = false;
  
  public static final String EXTRA_INPUT_ID = "android.media.tv.extra.INPUT_ID";
  
  private static final String TAG = "TvInputInfo";
  
  public static final int TYPE_COMPONENT = 1004;
  
  public static final int TYPE_COMPOSITE = 1001;
  
  public static final int TYPE_DISPLAY_PORT = 1008;
  
  public static final int TYPE_DVI = 1006;
  
  public static final int TYPE_HDMI = 1007;
  
  public static final int TYPE_OTHER = 1000;
  
  public static final int TYPE_SCART = 1003;
  
  public static final int TYPE_SVIDEO = 1002;
  
  public static final int TYPE_TUNER = 0;
  
  public static final int TYPE_VGA = 1005;
  
  private final boolean mCanRecord;
  
  private final Bundle mExtras;
  
  private final int mHdmiConnectionRelativePosition;
  
  private final HdmiDeviceInfo mHdmiDeviceInfo;
  
  private final Icon mIcon;
  
  private final Icon mIconDisconnected;
  
  private final Icon mIconStandby;
  
  private Uri mIconUri;
  
  private final String mId;
  
  private final boolean mIsConnectedToHdmiSwitch;
  
  private final boolean mIsHardwareInput;
  
  private final CharSequence mLabel;
  
  private final int mLabelResId;
  
  private final String mParentId;
  
  private final ResolveInfo mService;
  
  private final String mSetupActivity;
  
  private final int mTunerCount;
  
  private final int mType;
  
  private TvInputInfo(Parcel paramParcel) {
    this.mService = ResolveInfo.CREATOR.createFromParcel(paramParcel);
    this.mId = paramParcel.readString();
    this.mType = paramParcel.readInt();
    byte b = paramParcel.readByte();
    boolean bool1 = false;
    if (b == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mIsHardwareInput = bool2;
    this.mLabel = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mIconUri = paramParcel.<Uri>readParcelable(null);
    this.mLabelResId = paramParcel.readInt();
    this.mIcon = paramParcel.<Icon>readParcelable(null);
    this.mIconStandby = paramParcel.<Icon>readParcelable(null);
    this.mIconDisconnected = paramParcel.<Icon>readParcelable(null);
    this.mSetupActivity = paramParcel.readString();
    if (paramParcel.readByte() == 1) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    this.mCanRecord = bool2;
    this.mTunerCount = paramParcel.readInt();
    this.mHdmiDeviceInfo = paramParcel.<HdmiDeviceInfo>readParcelable(null);
    boolean bool2 = bool1;
    if (paramParcel.readByte() == 1)
      bool2 = true; 
    this.mIsConnectedToHdmiSwitch = bool2;
    this.mHdmiConnectionRelativePosition = paramParcel.readInt();
    this.mParentId = paramParcel.readString();
    this.mExtras = paramParcel.readBundle();
  }
  
  class Builder {
    private static final String DELIMITER_INFO_IN_ID = "/";
    
    private static final int LENGTH_HDMI_DEVICE_ID = 2;
    
    private static final int LENGTH_HDMI_PHYSICAL_ADDRESS = 4;
    
    private static final String PREFIX_HARDWARE_DEVICE = "HW";
    
    private static final String PREFIX_HDMI_DEVICE = "HDMI";
    
    private static final String XML_START_TAG_NAME = "tv-input";
    
    private static final SparseIntArray sHardwareTypeToTvInputType;
    
    private Boolean mCanRecord;
    
    private final Context mContext;
    
    private Bundle mExtras;
    
    private HdmiDeviceInfo mHdmiDeviceInfo;
    
    private Icon mIcon;
    
    private Icon mIconDisconnected;
    
    private Icon mIconStandby;
    
    private CharSequence mLabel;
    
    private int mLabelResId;
    
    private String mParentId;
    
    private final ResolveInfo mResolveInfo;
    
    private String mSetupActivity;
    
    private Integer mTunerCount;
    
    private TvInputHardwareInfo mTvInputHardwareInfo;
    
    static {
      SparseIntArray sparseIntArray = new SparseIntArray();
      sparseIntArray.put(1, 1000);
      sHardwareTypeToTvInputType.put(2, 0);
      sHardwareTypeToTvInputType.put(3, 1001);
      sHardwareTypeToTvInputType.put(4, 1002);
      sHardwareTypeToTvInputType.put(5, 1003);
      sHardwareTypeToTvInputType.put(6, 1004);
      sHardwareTypeToTvInputType.put(7, 1005);
      sHardwareTypeToTvInputType.put(8, 1006);
      sHardwareTypeToTvInputType.put(9, 1007);
      sHardwareTypeToTvInputType.put(10, 1008);
    }
    
    public Builder(TvInputInfo this$0, ComponentName param1ComponentName) {
      if (this$0 != null) {
        Intent intent = (new Intent("android.media.tv.TvInputService")).setComponent(param1ComponentName);
        ResolveInfo resolveInfo = this$0.getPackageManager().resolveService(intent, 132);
        if (resolveInfo != null) {
          this.mContext = (Context)this$0;
          return;
        } 
        throw new IllegalArgumentException("Invalid component. Can't find the service.");
      } 
      throw new IllegalArgumentException("context cannot be null.");
    }
    
    public Builder(TvInputInfo this$0, ResolveInfo param1ResolveInfo) {
      if (this$0 != null) {
        if (param1ResolveInfo != null) {
          this.mContext = (Context)this$0;
          this.mResolveInfo = param1ResolveInfo;
          return;
        } 
        throw new IllegalArgumentException("resolveInfo cannot be null");
      } 
      throw new IllegalArgumentException("context cannot be null");
    }
    
    @SystemApi
    public Builder setIcon(Icon param1Icon) {
      this.mIcon = param1Icon;
      return this;
    }
    
    @SystemApi
    public Builder setIcon(Icon param1Icon, int param1Int) {
      if (param1Int == 0) {
        this.mIcon = param1Icon;
      } else if (param1Int == 1) {
        this.mIconStandby = param1Icon;
      } else {
        if (param1Int == 2) {
          this.mIconDisconnected = param1Icon;
          return this;
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown state: ");
        stringBuilder.append(param1Int);
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      return this;
    }
    
    @SystemApi
    public Builder setLabel(CharSequence param1CharSequence) {
      if (this.mLabelResId == 0) {
        this.mLabel = param1CharSequence;
        return this;
      } 
      throw new IllegalStateException("Resource ID for label is already set.");
    }
    
    @SystemApi
    public Builder setLabel(int param1Int) {
      if (this.mLabel == null) {
        this.mLabelResId = param1Int;
        return this;
      } 
      throw new IllegalStateException("Label text is already set.");
    }
    
    @SystemApi
    public Builder setHdmiDeviceInfo(HdmiDeviceInfo param1HdmiDeviceInfo) {
      if (this.mTvInputHardwareInfo != null) {
        Log.w("TvInputInfo", "TvInputHardwareInfo will not be used to build this TvInputInfo");
        this.mTvInputHardwareInfo = null;
      } 
      this.mHdmiDeviceInfo = param1HdmiDeviceInfo;
      return this;
    }
    
    @SystemApi
    public Builder setParentId(String param1String) {
      this.mParentId = param1String;
      return this;
    }
    
    @SystemApi
    public Builder setTvInputHardwareInfo(TvInputHardwareInfo param1TvInputHardwareInfo) {
      if (this.mHdmiDeviceInfo != null) {
        Log.w("TvInputInfo", "mHdmiDeviceInfo will not be used to build this TvInputInfo");
        this.mHdmiDeviceInfo = null;
      } 
      this.mTvInputHardwareInfo = param1TvInputHardwareInfo;
      return this;
    }
    
    public Builder setTunerCount(int param1Int) {
      this.mTunerCount = Integer.valueOf(param1Int);
      return this;
    }
    
    public Builder setCanRecord(boolean param1Boolean) {
      this.mCanRecord = Boolean.valueOf(param1Boolean);
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public TvInputInfo build() {
      String str1;
      boolean bool3;
      boolean bool;
      ComponentName componentName = new ComponentName(this.mResolveInfo.serviceInfo.packageName, this.mResolveInfo.serviceInfo.name);
      boolean bool1 = false;
      boolean bool2 = false;
      int i = 0;
      HdmiDeviceInfo hdmiDeviceInfo = this.mHdmiDeviceInfo;
      int j = 0;
      if (hdmiDeviceInfo != null) {
        str1 = generateInputId(componentName, hdmiDeviceInfo);
        bool3 = true;
        bool = true;
        i = getRelativePosition(this.mContext, this.mHdmiDeviceInfo);
        bool1 = true;
        if (i == 1)
          bool1 = false; 
        bool2 = bool1;
        bool1 = bool;
      } else {
        TvInputHardwareInfo tvInputHardwareInfo = this.mTvInputHardwareInfo;
        if (tvInputHardwareInfo != null) {
          str1 = generateInputId((ComponentName)str1, tvInputHardwareInfo);
          bool3 = sHardwareTypeToTvInputType.get(this.mTvInputHardwareInfo.getType(), 0);
          bool1 = true;
        } else {
          str1 = generateInputId((ComponentName)str1);
          bool3 = false;
        } 
      } 
      parseServiceMetadata(bool3);
      ResolveInfo resolveInfo = this.mResolveInfo;
      CharSequence charSequence = this.mLabel;
      int k = this.mLabelResId;
      Icon icon1 = this.mIcon, icon2 = this.mIconStandby, icon3 = this.mIconDisconnected;
      String str2 = this.mSetupActivity;
      Boolean bool4 = this.mCanRecord;
      if (bool4 == null) {
        bool = false;
      } else {
        bool = bool4.booleanValue();
      } 
      Integer integer = this.mTunerCount;
      if (integer != null)
        j = integer.intValue(); 
      return new TvInputInfo(resolveInfo, str1, bool3, bool1, charSequence, k, icon1, icon2, icon3, str2, bool, j, this.mHdmiDeviceInfo, bool2, i, this.mParentId, this.mExtras);
    }
    
    private static String generateInputId(ComponentName param1ComponentName) {
      return param1ComponentName.flattenToShortString();
    }
    
    private static String generateInputId(ComponentName param1ComponentName, HdmiDeviceInfo param1HdmiDeviceInfo) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1ComponentName.flattenToShortString());
      Locale locale = Locale.ENGLISH;
      int i = param1HdmiDeviceInfo.getPhysicalAddress(), j = param1HdmiDeviceInfo.getId();
      stringBuilder.append(String.format(locale, "/HDMI%04X%02X", new Object[] { Integer.valueOf(i), Integer.valueOf(j) }));
      return stringBuilder.toString();
    }
    
    private static String generateInputId(ComponentName param1ComponentName, TvInputHardwareInfo param1TvInputHardwareInfo) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1ComponentName.flattenToShortString());
      stringBuilder.append("/");
      stringBuilder.append("HW");
      stringBuilder.append(param1TvInputHardwareInfo.getDeviceId());
      return stringBuilder.toString();
    }
    
    private static int getRelativePosition(Context param1Context, HdmiDeviceInfo param1HdmiDeviceInfo) {
      HdmiControlManager hdmiControlManager = (HdmiControlManager)param1Context.getSystemService("hdmi_control");
      if (hdmiControlManager == null)
        return 0; 
      int i = param1HdmiDeviceInfo.getPhysicalAddress(), j = hdmiControlManager.getPhysicalAddress();
      return HdmiUtils.getHdmiAddressRelativePosition(i, j);
    }
    
    private void parseServiceMetadata(int param1Int) {
      ServiceInfo serviceInfo = this.mResolveInfo.serviceInfo;
      PackageManager packageManager = this.mContext.getPackageManager();
      try {
        XmlResourceParser xmlResourceParser = serviceInfo.loadXmlMetaData(packageManager, "android.media.tv.input");
        if (xmlResourceParser != null)
          try {
            Resources resources = packageManager.getResourcesForApplication(serviceInfo.applicationInfo);
            AttributeSet attributeSet = Xml.asAttributeSet((XmlPullParser)xmlResourceParser);
            while (true) {
              int i = xmlResourceParser.next();
              if (i != 1 && i != 2)
                continue; 
              break;
            } 
            String str = xmlResourceParser.getName();
            if ("tv-input".equals(str)) {
              TypedArray typedArray = resources.obtainAttributes(attributeSet, R.styleable.TvInputService);
              this.mSetupActivity = typedArray.getString(1);
              if (this.mCanRecord == null)
                this.mCanRecord = Boolean.valueOf(typedArray.getBoolean(2, false)); 
              if (this.mTunerCount == null && param1Int == 0)
                this.mTunerCount = Integer.valueOf(typedArray.getInt(3, 1)); 
              typedArray.recycle();
              return;
            } 
            IllegalStateException illegalStateException1 = new IllegalStateException();
            StringBuilder stringBuilder1 = new StringBuilder();
            this();
            stringBuilder1.append("Meta-data does not start with tv-input tag for ");
            stringBuilder1.append(serviceInfo.name);
            this(stringBuilder1.toString());
            throw illegalStateException1;
          } finally {
            if (xmlResourceParser != null)
              try {
                xmlResourceParser.close();
              } finally {
                xmlResourceParser = null;
              }  
          }  
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("No android.media.tv.input meta-data found for ");
        stringBuilder.append(serviceInfo.name);
        this(stringBuilder.toString());
        throw illegalStateException;
      } catch (IOException|XmlPullParserException iOException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Failed reading meta-data for ");
        stringBuilder.append(serviceInfo.packageName);
        throw new IllegalStateException(stringBuilder.toString(), iOException);
      } catch (android.content.pm.PackageManager.NameNotFoundException nameNotFoundException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("No resources found for ");
        stringBuilder.append(serviceInfo.packageName);
        throw new IllegalStateException(stringBuilder.toString(), nameNotFoundException);
      } 
    }
  }
  
  @SystemApi
  class TvInputSettings {
    private static final String CUSTOM_NAME_SEPARATOR = ",";
    
    private static final String TV_INPUT_SEPARATOR = ":";
    
    private static boolean isHidden(Context param1Context, String param1String, int param1Int) {
      return getHiddenTvInputIds(param1Context, param1Int).contains(param1String);
    }
    
    private static String getCustomLabel(Context param1Context, String param1String, int param1Int) {
      return getCustomLabels(param1Context, param1Int).get(param1String);
    }
    
    @SystemApi
    public static Set<String> getHiddenTvInputIds(Context param1Context, int param1Int) {
      ContentResolver contentResolver = param1Context.getContentResolver();
      String str = Settings.Secure.getStringForUser(contentResolver, "tv_input_hidden_inputs", param1Int);
      HashSet<String> hashSet = new HashSet();
      if (TextUtils.isEmpty(str))
        return hashSet; 
      String[] arrayOfString = str.split(":");
      for (int i = arrayOfString.length; param1Int < i; ) {
        str = arrayOfString[param1Int];
        hashSet.add(Uri.decode(str));
        param1Int++;
      } 
      return hashSet;
    }
    
    @SystemApi
    public static Map<String, String> getCustomLabels(Context param1Context, int param1Int) {
      ContentResolver contentResolver = param1Context.getContentResolver();
      String str = Settings.Secure.getStringForUser(contentResolver, "tv_input_custom_labels", param1Int);
      HashMap<Object, Object> hashMap = new HashMap<>();
      if (TextUtils.isEmpty(str))
        return (Map)hashMap; 
      String[] arrayOfString = str.split(":");
      for (int i = arrayOfString.length; param1Int < i; ) {
        String str1 = arrayOfString[param1Int];
        String[] arrayOfString1 = str1.split(",");
        hashMap.put(Uri.decode(arrayOfString1[0]), Uri.decode(arrayOfString1[1]));
        param1Int++;
      } 
      return (Map)hashMap;
    }
    
    @SystemApi
    public static void putHiddenTvInputs(Context param1Context, Set<String> param1Set, int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      boolean bool = true;
      for (String str1 : param1Set) {
        ensureValidField(str1);
        if (bool) {
          bool = false;
        } else {
          stringBuilder.append(":");
        } 
        stringBuilder.append(Uri.encode(str1));
      } 
      ContentResolver contentResolver = param1Context.getContentResolver();
      str = stringBuilder.toString();
      Settings.Secure.putStringForUser(contentResolver, "tv_input_hidden_inputs", str, param1Int);
      TvInputManager tvInputManager = (TvInputManager)param1Context.getSystemService("tv_input");
      for (String str : param1Set) {
        TvInputInfo tvInputInfo = tvInputManager.getTvInputInfo(str);
        if (tvInputInfo != null)
          tvInputManager.updateTvInputInfo(tvInputInfo); 
      } 
    }
    
    @SystemApi
    public static void putCustomLabels(Context param1Context, Map<String, String> param1Map, int param1Int) {
      StringBuilder stringBuilder = new StringBuilder();
      boolean bool = true;
      for (Map.Entry<String, String> entry : param1Map.entrySet()) {
        ensureValidField((String)entry.getKey());
        ensureValidField((String)entry.getValue());
        if (bool) {
          bool = false;
        } else {
          stringBuilder.append(":");
        } 
        stringBuilder.append(Uri.encode((String)entry.getKey()));
        stringBuilder.append(",");
        stringBuilder.append(Uri.encode((String)entry.getValue()));
      } 
      ContentResolver contentResolver = param1Context.getContentResolver();
      str = stringBuilder.toString();
      Settings.Secure.putStringForUser(contentResolver, "tv_input_custom_labels", str, param1Int);
      TvInputManager tvInputManager = (TvInputManager)param1Context.getSystemService("tv_input");
      for (String str : param1Map.keySet()) {
        TvInputInfo tvInputInfo = tvInputManager.getTvInputInfo(str);
        if (tvInputInfo != null)
          tvInputManager.updateTvInputInfo(tvInputInfo); 
      } 
    }
    
    private static void ensureValidField(String param1String) {
      if (!TextUtils.isEmpty(param1String))
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append(" should not empty ");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Type implements Annotation {}
}
