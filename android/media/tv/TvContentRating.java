package android.media.tv;

import android.text.TextUtils;
import com.android.internal.util.Preconditions;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public final class TvContentRating {
  private static final String DELIMITER = "/";
  
  public static final TvContentRating UNRATED = new TvContentRating("null", "null", "null", null);
  
  private final String mDomain;
  
  private final int mHashCode;
  
  private final String mRating;
  
  private final String mRatingSystem;
  
  private final String[] mSubRatings;
  
  public static TvContentRating createRating(String paramString1, String paramString2, String paramString3, String... paramVarArgs) {
    if (!TextUtils.isEmpty(paramString1)) {
      if (!TextUtils.isEmpty(paramString2)) {
        if (!TextUtils.isEmpty(paramString3))
          return new TvContentRating(paramString1, paramString2, paramString3, paramVarArgs); 
        throw new IllegalArgumentException("rating cannot be empty");
      } 
      throw new IllegalArgumentException("ratingSystem cannot be empty");
    } 
    throw new IllegalArgumentException("domain cannot be empty");
  }
  
  public static TvContentRating unflattenFromString(String paramString) {
    if (!TextUtils.isEmpty(paramString)) {
      String[] arrayOfString1, arrayOfString2 = paramString.split("/");
      if (arrayOfString2.length >= 3) {
        if (arrayOfString2.length > 3) {
          arrayOfString1 = new String[arrayOfString2.length - 3];
          System.arraycopy(arrayOfString2, 3, arrayOfString1, 0, arrayOfString1.length);
          return new TvContentRating(arrayOfString2[0], arrayOfString2[1], arrayOfString2[2], arrayOfString1);
        } 
        return new TvContentRating(arrayOfString2[0], arrayOfString2[1], arrayOfString2[2], null);
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid rating string: ");
      stringBuilder.append((String)arrayOfString1);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("ratingString cannot be empty");
  }
  
  private TvContentRating(String paramString1, String paramString2, String paramString3, String[] paramArrayOfString) {
    this.mDomain = paramString1;
    this.mRatingSystem = paramString2;
    this.mRating = paramString3;
    if (paramArrayOfString == null || paramArrayOfString.length == 0) {
      this.mSubRatings = null;
    } else {
      Arrays.sort((Object[])paramArrayOfString);
      this.mSubRatings = paramArrayOfString;
    } 
    this.mHashCode = Objects.hash(new Object[] { this.mDomain, this.mRating }) * 31 + Arrays.hashCode((Object[])this.mSubRatings);
  }
  
  public String getDomain() {
    return this.mDomain;
  }
  
  public String getRatingSystem() {
    return this.mRatingSystem;
  }
  
  public String getMainRating() {
    return this.mRating;
  }
  
  public List<String> getSubRatings() {
    String[] arrayOfString = this.mSubRatings;
    if (arrayOfString == null)
      return null; 
    return Collections.unmodifiableList(Arrays.asList(arrayOfString));
  }
  
  public String flattenToString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mDomain);
    stringBuilder.append("/");
    stringBuilder.append(this.mRatingSystem);
    stringBuilder.append("/");
    stringBuilder.append(this.mRating);
    String[] arrayOfString = this.mSubRatings;
    if (arrayOfString != null) {
      int i;
      byte b;
      for (i = arrayOfString.length, b = 0; b < i; ) {
        String str = arrayOfString[b];
        stringBuilder.append("/");
        stringBuilder.append(str);
        b++;
      } 
    } 
    return stringBuilder.toString();
  }
  
  public final boolean contains(TvContentRating paramTvContentRating) {
    Preconditions.checkNotNull(paramTvContentRating);
    if (!paramTvContentRating.getMainRating().equals(this.mRating))
      return false; 
    if (!paramTvContentRating.getDomain().equals(this.mDomain) || 
      !paramTvContentRating.getRatingSystem().equals(this.mRatingSystem) || 
      !paramTvContentRating.getMainRating().equals(this.mRating))
      return false; 
    List<String> list2 = getSubRatings();
    List<String> list1 = paramTvContentRating.getSubRatings();
    if (list2 == null && list1 == null)
      return true; 
    if (list2 == null && list1 != null)
      return false; 
    if (list2 != null && list1 == null)
      return true; 
    return list2.containsAll(list1);
  }
  
  public boolean equals(Object paramObject) {
    if (!(paramObject instanceof TvContentRating))
      return false; 
    paramObject = paramObject;
    if (this.mHashCode != ((TvContentRating)paramObject).mHashCode)
      return false; 
    if (!TextUtils.equals(this.mDomain, ((TvContentRating)paramObject).mDomain))
      return false; 
    if (!TextUtils.equals(this.mRatingSystem, ((TvContentRating)paramObject).mRatingSystem))
      return false; 
    if (!TextUtils.equals(this.mRating, ((TvContentRating)paramObject).mRating))
      return false; 
    return Arrays.equals((Object[])this.mSubRatings, (Object[])((TvContentRating)paramObject).mSubRatings);
  }
  
  public int hashCode() {
    return this.mHashCode;
  }
}
