package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITvInputServiceCallback extends IInterface {
  void addHardwareInput(int paramInt, TvInputInfo paramTvInputInfo) throws RemoteException;
  
  void addHdmiInput(int paramInt, TvInputInfo paramTvInputInfo) throws RemoteException;
  
  void removeHardwareInput(String paramString) throws RemoteException;
  
  class Default implements ITvInputServiceCallback {
    public void addHardwareInput(int param1Int, TvInputInfo param1TvInputInfo) throws RemoteException {}
    
    public void addHdmiInput(int param1Int, TvInputInfo param1TvInputInfo) throws RemoteException {}
    
    public void removeHardwareInput(String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputServiceCallback {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputServiceCallback";
    
    static final int TRANSACTION_addHardwareInput = 1;
    
    static final int TRANSACTION_addHdmiInput = 2;
    
    static final int TRANSACTION_removeHardwareInput = 3;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputServiceCallback");
    }
    
    public static ITvInputServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputServiceCallback");
      if (iInterface != null && iInterface instanceof ITvInputServiceCallback)
        return (ITvInputServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "removeHardwareInput";
        } 
        return "addHdmiInput";
      } 
      return "addHardwareInput";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.media.tv.ITvInputServiceCallback");
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.tv.ITvInputServiceCallback");
          str = param1Parcel1.readString();
          removeHardwareInput(str);
          return true;
        } 
        str.enforceInterface("android.media.tv.ITvInputServiceCallback");
        param1Int1 = str.readInt();
        if (str.readInt() != 0) {
          TvInputInfo tvInputInfo = TvInputInfo.CREATOR.createFromParcel((Parcel)str);
        } else {
          str = null;
        } 
        addHdmiInput(param1Int1, (TvInputInfo)str);
        return true;
      } 
      str.enforceInterface("android.media.tv.ITvInputServiceCallback");
      param1Int1 = str.readInt();
      if (str.readInt() != 0) {
        TvInputInfo tvInputInfo = TvInputInfo.CREATOR.createFromParcel((Parcel)str);
      } else {
        str = null;
      } 
      addHardwareInput(param1Int1, (TvInputInfo)str);
      return true;
    }
    
    private static class Proxy implements ITvInputServiceCallback {
      public static ITvInputServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputServiceCallback";
      }
      
      public void addHardwareInput(int param2Int, TvInputInfo param2TvInputInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputServiceCallback");
          parcel.writeInt(param2Int);
          if (param2TvInputInfo != null) {
            parcel.writeInt(1);
            param2TvInputInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputServiceCallback.Stub.getDefaultImpl() != null) {
            ITvInputServiceCallback.Stub.getDefaultImpl().addHardwareInput(param2Int, param2TvInputInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void addHdmiInput(int param2Int, TvInputInfo param2TvInputInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputServiceCallback");
          parcel.writeInt(param2Int);
          if (param2TvInputInfo != null) {
            parcel.writeInt(1);
            param2TvInputInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputServiceCallback.Stub.getDefaultImpl() != null) {
            ITvInputServiceCallback.Stub.getDefaultImpl().addHdmiInput(param2Int, param2TvInputInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void removeHardwareInput(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputServiceCallback");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputServiceCallback.Stub.getDefaultImpl() != null) {
            ITvInputServiceCallback.Stub.getDefaultImpl().removeHardwareInput(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputServiceCallback param1ITvInputServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputServiceCallback != null) {
          Proxy.sDefaultImpl = param1ITvInputServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
