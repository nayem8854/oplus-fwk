package android.media.tv;

import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.InputChannel;
import java.util.ArrayList;
import java.util.List;

public interface ITvInputClient extends IInterface {
  void onChannelRetuned(Uri paramUri, int paramInt) throws RemoteException;
  
  void onContentAllowed(int paramInt) throws RemoteException;
  
  void onContentBlocked(String paramString, int paramInt) throws RemoteException;
  
  void onError(int paramInt1, int paramInt2) throws RemoteException;
  
  void onLayoutSurface(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws RemoteException;
  
  void onRecordingStopped(Uri paramUri, int paramInt) throws RemoteException;
  
  void onSessionCreated(String paramString, IBinder paramIBinder, InputChannel paramInputChannel, int paramInt) throws RemoteException;
  
  void onSessionEvent(String paramString, Bundle paramBundle, int paramInt) throws RemoteException;
  
  void onSessionReleased(int paramInt) throws RemoteException;
  
  void onTimeShiftCurrentPositionChanged(long paramLong, int paramInt) throws RemoteException;
  
  void onTimeShiftStartPositionChanged(long paramLong, int paramInt) throws RemoteException;
  
  void onTimeShiftStatusChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  void onTrackSelected(int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void onTracksChanged(List<TvTrackInfo> paramList, int paramInt) throws RemoteException;
  
  void onTuned(int paramInt, Uri paramUri) throws RemoteException;
  
  void onVideoAvailable(int paramInt) throws RemoteException;
  
  void onVideoUnavailable(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements ITvInputClient {
    public void onSessionCreated(String param1String, IBinder param1IBinder, InputChannel param1InputChannel, int param1Int) throws RemoteException {}
    
    public void onSessionReleased(int param1Int) throws RemoteException {}
    
    public void onSessionEvent(String param1String, Bundle param1Bundle, int param1Int) throws RemoteException {}
    
    public void onChannelRetuned(Uri param1Uri, int param1Int) throws RemoteException {}
    
    public void onTracksChanged(List<TvTrackInfo> param1List, int param1Int) throws RemoteException {}
    
    public void onTrackSelected(int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void onVideoAvailable(int param1Int) throws RemoteException {}
    
    public void onVideoUnavailable(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onContentAllowed(int param1Int) throws RemoteException {}
    
    public void onContentBlocked(String param1String, int param1Int) throws RemoteException {}
    
    public void onLayoutSurface(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5) throws RemoteException {}
    
    public void onTimeShiftStatusChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void onTimeShiftStartPositionChanged(long param1Long, int param1Int) throws RemoteException {}
    
    public void onTimeShiftCurrentPositionChanged(long param1Long, int param1Int) throws RemoteException {}
    
    public void onTuned(int param1Int, Uri param1Uri) throws RemoteException {}
    
    public void onRecordingStopped(Uri param1Uri, int param1Int) throws RemoteException {}
    
    public void onError(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputClient {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputClient";
    
    static final int TRANSACTION_onChannelRetuned = 4;
    
    static final int TRANSACTION_onContentAllowed = 9;
    
    static final int TRANSACTION_onContentBlocked = 10;
    
    static final int TRANSACTION_onError = 17;
    
    static final int TRANSACTION_onLayoutSurface = 11;
    
    static final int TRANSACTION_onRecordingStopped = 16;
    
    static final int TRANSACTION_onSessionCreated = 1;
    
    static final int TRANSACTION_onSessionEvent = 3;
    
    static final int TRANSACTION_onSessionReleased = 2;
    
    static final int TRANSACTION_onTimeShiftCurrentPositionChanged = 14;
    
    static final int TRANSACTION_onTimeShiftStartPositionChanged = 13;
    
    static final int TRANSACTION_onTimeShiftStatusChanged = 12;
    
    static final int TRANSACTION_onTrackSelected = 6;
    
    static final int TRANSACTION_onTracksChanged = 5;
    
    static final int TRANSACTION_onTuned = 15;
    
    static final int TRANSACTION_onVideoAvailable = 7;
    
    static final int TRANSACTION_onVideoUnavailable = 8;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputClient");
    }
    
    public static ITvInputClient asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputClient");
      if (iInterface != null && iInterface instanceof ITvInputClient)
        return (ITvInputClient)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 17:
          return "onError";
        case 16:
          return "onRecordingStopped";
        case 15:
          return "onTuned";
        case 14:
          return "onTimeShiftCurrentPositionChanged";
        case 13:
          return "onTimeShiftStartPositionChanged";
        case 12:
          return "onTimeShiftStatusChanged";
        case 11:
          return "onLayoutSurface";
        case 10:
          return "onContentBlocked";
        case 9:
          return "onContentAllowed";
        case 8:
          return "onVideoUnavailable";
        case 7:
          return "onVideoAvailable";
        case 6:
          return "onTrackSelected";
        case 5:
          return "onTracksChanged";
        case 4:
          return "onChannelRetuned";
        case 3:
          return "onSessionEvent";
        case 2:
          return "onSessionReleased";
        case 1:
          break;
      } 
      return "onSessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      ArrayList<TvTrackInfo> arrayList;
      if (param1Int1 != 1598968902) {
        String str1;
        long l;
        int i, j, k;
        String str2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 17:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onError(param1Int2, param1Int1);
            return true;
          case 16:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onRecordingStopped((Uri)param1Parcel2, param1Int1);
            return true;
          case 15:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            onTuned(param1Int1, (Uri)param1Parcel1);
            return true;
          case 14:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            onTimeShiftCurrentPositionChanged(l, param1Int1);
            return true;
          case 13:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            l = param1Parcel1.readLong();
            param1Int1 = param1Parcel1.readInt();
            onTimeShiftStartPositionChanged(l, param1Int1);
            return true;
          case 12:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            onTimeShiftStatusChanged(param1Int1, param1Int2);
            return true;
          case 11:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            i = param1Parcel1.readInt();
            j = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            k = param1Parcel1.readInt();
            onLayoutSurface(i, j, param1Int1, param1Int2, k);
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            str1 = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            onContentBlocked(str1, param1Int1);
            return true;
          case 9:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            onContentAllowed(param1Int1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            onVideoUnavailable(param1Int2, param1Int1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            onVideoAvailable(param1Int1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            str1 = param1Parcel1.readString();
            param1Int2 = param1Parcel1.readInt();
            onTrackSelected(param1Int1, str1, param1Int2);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            arrayList = param1Parcel1.createTypedArrayList(TvTrackInfo.CREATOR);
            param1Int1 = param1Parcel1.readInt();
            onTracksChanged(arrayList, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            if (param1Parcel1.readInt() != 0) {
              Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
            } else {
              arrayList = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onChannelRetuned((Uri)arrayList, param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            str2 = param1Parcel1.readString();
            if (param1Parcel1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel(param1Parcel1);
            } else {
              arrayList = null;
            } 
            param1Int1 = param1Parcel1.readInt();
            onSessionEvent(str2, (Bundle)arrayList, param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
            param1Int1 = param1Parcel1.readInt();
            onSessionReleased(param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.tv.ITvInputClient");
        String str3 = param1Parcel1.readString();
        IBinder iBinder = param1Parcel1.readStrongBinder();
        if (param1Parcel1.readInt() != 0) {
          InputChannel inputChannel = InputChannel.CREATOR.createFromParcel(param1Parcel1);
        } else {
          arrayList = null;
        } 
        param1Int1 = param1Parcel1.readInt();
        onSessionCreated(str3, iBinder, (InputChannel)arrayList, param1Int1);
        return true;
      } 
      arrayList.writeString("android.media.tv.ITvInputClient");
      return true;
    }
    
    private static class Proxy implements ITvInputClient {
      public static ITvInputClient sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputClient";
      }
      
      public void onSessionCreated(String param2String, IBinder param2IBinder, InputChannel param2InputChannel, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeString(param2String);
          parcel.writeStrongBinder(param2IBinder);
          if (param2InputChannel != null) {
            parcel.writeInt(1);
            param2InputChannel.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onSessionCreated(param2String, param2IBinder, param2InputChannel, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionReleased(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onSessionReleased(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onSessionEvent(String param2String, Bundle param2Bundle, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeString(param2String);
          if (param2Bundle != null) {
            parcel.writeInt(1);
            param2Bundle.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onSessionEvent(param2String, param2Bundle, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onChannelRetuned(Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onChannelRetuned(param2Uri, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTracksChanged(List<TvTrackInfo> param2List, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeTypedList(param2List);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTracksChanged(param2List, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTrackSelected(int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int1);
          parcel.writeString(param2String);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTrackSelected(param2Int1, param2String, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVideoAvailable(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onVideoAvailable(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onVideoUnavailable(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onVideoUnavailable(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onContentAllowed(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onContentAllowed(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onContentBlocked(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(10, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onContentBlocked(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onLayoutSurface(int param2Int1, int param2Int2, int param2Int3, int param2Int4, int param2Int5) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          parcel.writeInt(param2Int3);
          parcel.writeInt(param2Int4);
          parcel.writeInt(param2Int5);
          boolean bool = this.mRemote.transact(11, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onLayoutSurface(param2Int1, param2Int2, param2Int3, param2Int4, param2Int5);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftStatusChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(12, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTimeShiftStatusChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftStartPositionChanged(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(13, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTimeShiftStartPositionChanged(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTimeShiftCurrentPositionChanged(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(14, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTimeShiftCurrentPositionChanged(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onTuned(int param2Int, Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(15, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onTuned(param2Int, param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onRecordingStopped(Uri param2Uri, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(16, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onRecordingStopped(param2Uri, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onError(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputClient");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(17, parcel, (Parcel)null, 1);
          if (!bool && ITvInputClient.Stub.getDefaultImpl() != null) {
            ITvInputClient.Stub.getDefaultImpl().onError(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputClient param1ITvInputClient) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputClient != null) {
          Proxy.sDefaultImpl = param1ITvInputClient;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputClient getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
