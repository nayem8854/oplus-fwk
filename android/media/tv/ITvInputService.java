package android.media.tv;

import android.hardware.hdmi.HdmiDeviceInfo;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.view.InputChannel;

public interface ITvInputService extends IInterface {
  void createRecordingSession(ITvInputSessionCallback paramITvInputSessionCallback, String paramString1, String paramString2) throws RemoteException;
  
  void createSession(InputChannel paramInputChannel, ITvInputSessionCallback paramITvInputSessionCallback, String paramString1, String paramString2) throws RemoteException;
  
  void notifyHardwareAdded(TvInputHardwareInfo paramTvInputHardwareInfo) throws RemoteException;
  
  void notifyHardwareRemoved(TvInputHardwareInfo paramTvInputHardwareInfo) throws RemoteException;
  
  void notifyHdmiDeviceAdded(HdmiDeviceInfo paramHdmiDeviceInfo) throws RemoteException;
  
  void notifyHdmiDeviceRemoved(HdmiDeviceInfo paramHdmiDeviceInfo) throws RemoteException;
  
  void notifyHdmiDeviceUpdated(HdmiDeviceInfo paramHdmiDeviceInfo) throws RemoteException;
  
  void registerCallback(ITvInputServiceCallback paramITvInputServiceCallback) throws RemoteException;
  
  void unregisterCallback(ITvInputServiceCallback paramITvInputServiceCallback) throws RemoteException;
  
  class Default implements ITvInputService {
    public void registerCallback(ITvInputServiceCallback param1ITvInputServiceCallback) throws RemoteException {}
    
    public void unregisterCallback(ITvInputServiceCallback param1ITvInputServiceCallback) throws RemoteException {}
    
    public void createSession(InputChannel param1InputChannel, ITvInputSessionCallback param1ITvInputSessionCallback, String param1String1, String param1String2) throws RemoteException {}
    
    public void createRecordingSession(ITvInputSessionCallback param1ITvInputSessionCallback, String param1String1, String param1String2) throws RemoteException {}
    
    public void notifyHardwareAdded(TvInputHardwareInfo param1TvInputHardwareInfo) throws RemoteException {}
    
    public void notifyHardwareRemoved(TvInputHardwareInfo param1TvInputHardwareInfo) throws RemoteException {}
    
    public void notifyHdmiDeviceAdded(HdmiDeviceInfo param1HdmiDeviceInfo) throws RemoteException {}
    
    public void notifyHdmiDeviceRemoved(HdmiDeviceInfo param1HdmiDeviceInfo) throws RemoteException {}
    
    public void notifyHdmiDeviceUpdated(HdmiDeviceInfo param1HdmiDeviceInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputService {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputService";
    
    static final int TRANSACTION_createRecordingSession = 4;
    
    static final int TRANSACTION_createSession = 3;
    
    static final int TRANSACTION_notifyHardwareAdded = 5;
    
    static final int TRANSACTION_notifyHardwareRemoved = 6;
    
    static final int TRANSACTION_notifyHdmiDeviceAdded = 7;
    
    static final int TRANSACTION_notifyHdmiDeviceRemoved = 8;
    
    static final int TRANSACTION_notifyHdmiDeviceUpdated = 9;
    
    static final int TRANSACTION_registerCallback = 1;
    
    static final int TRANSACTION_unregisterCallback = 2;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputService");
    }
    
    public static ITvInputService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputService");
      if (iInterface != null && iInterface instanceof ITvInputService)
        return (ITvInputService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 9:
          return "notifyHdmiDeviceUpdated";
        case 8:
          return "notifyHdmiDeviceRemoved";
        case 7:
          return "notifyHdmiDeviceAdded";
        case 6:
          return "notifyHardwareRemoved";
        case 5:
          return "notifyHardwareAdded";
        case 4:
          return "createRecordingSession";
        case 3:
          return "createSession";
        case 2:
          return "unregisterCallback";
        case 1:
          break;
      } 
      return "registerCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        ITvInputSessionCallback iTvInputSessionCallback1;
        String str2;
        ITvInputSessionCallback iTvInputSessionCallback2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 9:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            if (param1Parcel1.readInt() != 0) {
              HdmiDeviceInfo hdmiDeviceInfo = HdmiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyHdmiDeviceUpdated((HdmiDeviceInfo)param1Parcel1);
            return true;
          case 8:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            if (param1Parcel1.readInt() != 0) {
              HdmiDeviceInfo hdmiDeviceInfo = HdmiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyHdmiDeviceRemoved((HdmiDeviceInfo)param1Parcel1);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            if (param1Parcel1.readInt() != 0) {
              HdmiDeviceInfo hdmiDeviceInfo = HdmiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyHdmiDeviceAdded((HdmiDeviceInfo)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            if (param1Parcel1.readInt() != 0) {
              TvInputHardwareInfo tvInputHardwareInfo = TvInputHardwareInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyHardwareRemoved((TvInputHardwareInfo)param1Parcel1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            if (param1Parcel1.readInt() != 0) {
              TvInputHardwareInfo tvInputHardwareInfo = TvInputHardwareInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifyHardwareAdded((TvInputHardwareInfo)param1Parcel1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.tv.ITvInputService");
            iTvInputSessionCallback1 = ITvInputSessionCallback.Stub.asInterface(param1Parcel1.readStrongBinder());
            str = param1Parcel1.readString();
            str1 = param1Parcel1.readString();
            createRecordingSession(iTvInputSessionCallback1, str, str1);
            return true;
          case 3:
            str1.enforceInterface("android.media.tv.ITvInputService");
            if (str1.readInt() != 0) {
              InputChannel inputChannel = InputChannel.CREATOR.createFromParcel((Parcel)str1);
            } else {
              str = null;
            } 
            iTvInputSessionCallback2 = ITvInputSessionCallback.Stub.asInterface(str1.readStrongBinder());
            str2 = str1.readString();
            str1 = str1.readString();
            createSession((InputChannel)str, iTvInputSessionCallback2, str2, str1);
            return true;
          case 2:
            str1.enforceInterface("android.media.tv.ITvInputService");
            iTvInputServiceCallback = ITvInputServiceCallback.Stub.asInterface(str1.readStrongBinder());
            unregisterCallback(iTvInputServiceCallback);
            return true;
          case 1:
            break;
        } 
        iTvInputServiceCallback.enforceInterface("android.media.tv.ITvInputService");
        ITvInputServiceCallback iTvInputServiceCallback = ITvInputServiceCallback.Stub.asInterface(iTvInputServiceCallback.readStrongBinder());
        registerCallback(iTvInputServiceCallback);
        return true;
      } 
      str.writeString("android.media.tv.ITvInputService");
      return true;
    }
    
    private static class Proxy implements ITvInputService {
      public static ITvInputService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputService";
      }
      
      public void registerCallback(ITvInputServiceCallback param2ITvInputServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2ITvInputServiceCallback != null) {
            iBinder = param2ITvInputServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().registerCallback(param2ITvInputServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void unregisterCallback(ITvInputServiceCallback param2ITvInputServiceCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2ITvInputServiceCallback != null) {
            iBinder = param2ITvInputServiceCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().unregisterCallback(param2ITvInputServiceCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createSession(InputChannel param2InputChannel, ITvInputSessionCallback param2ITvInputSessionCallback, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2InputChannel != null) {
            parcel.writeInt(1);
            param2InputChannel.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2ITvInputSessionCallback != null) {
            iBinder = param2ITvInputSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().createSession(param2InputChannel, param2ITvInputSessionCallback, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void createRecordingSession(ITvInputSessionCallback param2ITvInputSessionCallback, String param2String1, String param2String2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2ITvInputSessionCallback != null) {
            iBinder = param2ITvInputSessionCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeString(param2String1);
          parcel.writeString(param2String2);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().createRecordingSession(param2ITvInputSessionCallback, param2String1, param2String2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyHardwareAdded(TvInputHardwareInfo param2TvInputHardwareInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2TvInputHardwareInfo != null) {
            parcel.writeInt(1);
            param2TvInputHardwareInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().notifyHardwareAdded(param2TvInputHardwareInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyHardwareRemoved(TvInputHardwareInfo param2TvInputHardwareInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2TvInputHardwareInfo != null) {
            parcel.writeInt(1);
            param2TvInputHardwareInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().notifyHardwareRemoved(param2TvInputHardwareInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyHdmiDeviceAdded(HdmiDeviceInfo param2HdmiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2HdmiDeviceInfo != null) {
            parcel.writeInt(1);
            param2HdmiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().notifyHdmiDeviceAdded(param2HdmiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyHdmiDeviceRemoved(HdmiDeviceInfo param2HdmiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2HdmiDeviceInfo != null) {
            parcel.writeInt(1);
            param2HdmiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().notifyHdmiDeviceRemoved(param2HdmiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyHdmiDeviceUpdated(HdmiDeviceInfo param2HdmiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputService");
          if (param2HdmiDeviceInfo != null) {
            parcel.writeInt(1);
            param2HdmiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(9, parcel, (Parcel)null, 1);
          if (!bool && ITvInputService.Stub.getDefaultImpl() != null) {
            ITvInputService.Stub.getDefaultImpl().notifyHdmiDeviceUpdated(param2HdmiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputService param1ITvInputService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputService != null) {
          Proxy.sDefaultImpl = param1ITvInputService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
