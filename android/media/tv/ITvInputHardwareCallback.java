package android.media.tv;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface ITvInputHardwareCallback extends IInterface {
  void onReleased() throws RemoteException;
  
  void onStreamConfigChanged(TvStreamConfig[] paramArrayOfTvStreamConfig) throws RemoteException;
  
  class Default implements ITvInputHardwareCallback {
    public void onReleased() throws RemoteException {}
    
    public void onStreamConfigChanged(TvStreamConfig[] param1ArrayOfTvStreamConfig) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements ITvInputHardwareCallback {
    private static final String DESCRIPTOR = "android.media.tv.ITvInputHardwareCallback";
    
    static final int TRANSACTION_onReleased = 1;
    
    static final int TRANSACTION_onStreamConfigChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.media.tv.ITvInputHardwareCallback");
    }
    
    public static ITvInputHardwareCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.tv.ITvInputHardwareCallback");
      if (iInterface != null && iInterface instanceof ITvInputHardwareCallback)
        return (ITvInputHardwareCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "onStreamConfigChanged";
      } 
      return "onReleased";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      TvStreamConfig[] arrayOfTvStreamConfig;
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.tv.ITvInputHardwareCallback");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.tv.ITvInputHardwareCallback");
        arrayOfTvStreamConfig = param1Parcel1.<TvStreamConfig>createTypedArray(TvStreamConfig.CREATOR);
        onStreamConfigChanged(arrayOfTvStreamConfig);
        return true;
      } 
      arrayOfTvStreamConfig.enforceInterface("android.media.tv.ITvInputHardwareCallback");
      onReleased();
      return true;
    }
    
    private static class Proxy implements ITvInputHardwareCallback {
      public static ITvInputHardwareCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.tv.ITvInputHardwareCallback";
      }
      
      public void onReleased() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputHardwareCallback");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && ITvInputHardwareCallback.Stub.getDefaultImpl() != null) {
            ITvInputHardwareCallback.Stub.getDefaultImpl().onReleased();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onStreamConfigChanged(TvStreamConfig[] param2ArrayOfTvStreamConfig) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.tv.ITvInputHardwareCallback");
          parcel.writeTypedArray(param2ArrayOfTvStreamConfig, 0);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && ITvInputHardwareCallback.Stub.getDefaultImpl() != null) {
            ITvInputHardwareCallback.Stub.getDefaultImpl().onStreamConfigChanged(param2ArrayOfTvStreamConfig);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(ITvInputHardwareCallback param1ITvInputHardwareCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1ITvInputHardwareCallback != null) {
          Proxy.sDefaultImpl = param1ITvInputHardwareCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static ITvInputHardwareCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
