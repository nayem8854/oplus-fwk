package android.media;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import com.android.internal.app.IAppOpsCallback;
import com.android.internal.app.IAppOpsService;
import java.lang.ref.WeakReference;
import java.util.Objects;

public abstract class PlayerBase {
  protected float mLeftVolume = 1.0F;
  
  protected float mRightVolume = 1.0F;
  
  protected float mAuxEffectSendLevel = 0.0F;
  
  private final Object mLock = new Object();
  
  private boolean mHasAppOpsPlayAudio = true;
  
  private int mPlayerIId = -1;
  
  private int mStartDelayMs = 0;
  
  private float mPanMultiplierL = 1.0F;
  
  private float mPanMultiplierR = 1.0F;
  
  private float mVolMultiplier = 1.0F;
  
  private static final boolean DEBUG = false;
  
  private static final boolean DEBUG_APP_OPS = false;
  
  private static final String TAG = "PlayerBase";
  
  private static final boolean USE_AUDIOFLINGER_MUTING_FOR_OP = true;
  
  private static IAudioService sService;
  
  private IAppOpsService mAppOps;
  
  private IAppOpsCallback mAppOpsCallback;
  
  protected AudioAttributes mAttributes;
  
  private final int mImplType;
  
  private int mState;
  
  PlayerBase(AudioAttributes paramAudioAttributes, int paramInt) {
    if (paramAudioAttributes != null) {
      this.mAttributes = paramAudioAttributes;
      this.mImplType = paramInt;
      this.mState = 1;
      return;
    } 
    throw new IllegalArgumentException("Illegal null AudioAttributes");
  }
  
  protected void baseRegisterPlayer() {
    try {
      IAudioService iAudioService = getService();
      PlayerIdCard playerIdCard = new PlayerIdCard();
      int i = this.mImplType;
      AudioAttributes audioAttributes = this.mAttributes;
      IPlayerWrapper iPlayerWrapper = new IPlayerWrapper();
      this(this);
      this(i, audioAttributes, iPlayerWrapper);
      this.mPlayerIId = iAudioService.trackPlayer(playerIdCard);
    } catch (RemoteException remoteException) {
      Log.e("PlayerBase", "Error talking to audio service, player will not be tracked", (Throwable)remoteException);
    } 
  }
  
  void baseUpdateAudioAttributes(AudioAttributes paramAudioAttributes) {
    if (paramAudioAttributes != null) {
      try {
        getService().playerAttributes(this.mPlayerIId, paramAudioAttributes);
      } catch (RemoteException remoteException) {
        Log.e("PlayerBase", "Error talking to audio service, STARTED state will not be tracked", (Throwable)remoteException);
      } 
      synchronized (this.mLock) {
        boolean bool;
        if (this.mAttributes != paramAudioAttributes) {
          bool = true;
        } else {
          bool = false;
        } 
        this.mAttributes = paramAudioAttributes;
        updateAppOpsPlayAudio_sync(bool);
        return;
      } 
    } 
    throw new IllegalArgumentException("Illegal null AudioAttributes");
  }
  
  private void updateState(int paramInt) {
    synchronized (this.mLock) {
      this.mState = paramInt;
      int i = this.mPlayerIId;
      try {
        getService().playerEvent(i, paramInt);
      } catch (RemoteException null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error talking to audio service, ");
        stringBuilder.append(AudioPlaybackConfiguration.toLogFriendlyPlayerState(paramInt));
        stringBuilder.append(" state will not be tracked for piid=");
        stringBuilder.append(i);
        String str = stringBuilder.toString();
        Log.e("PlayerBase", str, (Throwable)null);
      } 
      return;
    } 
  }
  
  void baseStart() {
    updateState(2);
    synchronized (this.mLock) {
      if (isRestricted_sync())
        playerSetVolume(true, 0.0F, 0.0F); 
      return;
    } 
  }
  
  void baseSetStartDelayMs(int paramInt) {
    synchronized (this.mLock) {
      this.mStartDelayMs = Math.max(paramInt, 0);
      return;
    } 
  }
  
  protected int getStartDelayMs() {
    synchronized (this.mLock) {
      return this.mStartDelayMs;
    } 
  }
  
  void basePause() {
    updateState(3);
  }
  
  void baseStop() {
    updateState(4);
  }
  
  void baseSetPan(float paramFloat) {
    // Byte code:
    //   0: ldc -1.0
    //   2: fload_1
    //   3: invokestatic max : (FF)F
    //   6: fconst_1
    //   7: invokestatic min : (FF)F
    //   10: fstore_1
    //   11: aload_0
    //   12: getfield mLock : Ljava/lang/Object;
    //   15: astore_2
    //   16: aload_2
    //   17: monitorenter
    //   18: fload_1
    //   19: fconst_0
    //   20: fcmpl
    //   21: iflt -> 39
    //   24: aload_0
    //   25: fconst_1
    //   26: fload_1
    //   27: fsub
    //   28: putfield mPanMultiplierL : F
    //   31: aload_0
    //   32: fconst_1
    //   33: putfield mPanMultiplierR : F
    //   36: goto -> 51
    //   39: aload_0
    //   40: fconst_1
    //   41: putfield mPanMultiplierL : F
    //   44: aload_0
    //   45: fconst_1
    //   46: fload_1
    //   47: fadd
    //   48: putfield mPanMultiplierR : F
    //   51: aload_2
    //   52: monitorexit
    //   53: aload_0
    //   54: invokespecial updatePlayerVolume : ()V
    //   57: return
    //   58: astore_3
    //   59: aload_2
    //   60: monitorexit
    //   61: aload_3
    //   62: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #202	-> 0
    //   #203	-> 11
    //   #204	-> 18
    //   #205	-> 24
    //   #206	-> 31
    //   #208	-> 39
    //   #209	-> 44
    //   #211	-> 51
    //   #212	-> 53
    //   #213	-> 57
    //   #211	-> 58
    // Exception table:
    //   from	to	target	type
    //   24	31	58	finally
    //   31	36	58	finally
    //   39	44	58	finally
    //   44	51	58	finally
    //   51	53	58	finally
    //   59	61	58	finally
  }
  
  private void updatePlayerVolume() {
    synchronized (this.mLock) {
      float f1 = this.mVolMultiplier, f2 = this.mLeftVolume, f3 = this.mPanMultiplierL;
      float f4 = this.mVolMultiplier, f5 = this.mRightVolume, f6 = this.mPanMultiplierR;
      boolean bool = isRestricted_sync();
      playerSetVolume(bool, f1 * f2 * f3, f4 * f5 * f6);
      return;
    } 
  }
  
  void setVolumeMultiplier(float paramFloat) {
    synchronized (this.mLock) {
      this.mVolMultiplier = paramFloat;
      updatePlayerVolume();
      return;
    } 
  }
  
  void baseSetVolume(float paramFloat1, float paramFloat2) {
    synchronized (this.mLock) {
      this.mLeftVolume = paramFloat1;
      this.mRightVolume = paramFloat2;
      updatePlayerVolume();
      return;
    } 
  }
  
  int baseSetAuxEffectSendLevel(float paramFloat) {
    synchronized (this.mLock) {
      this.mAuxEffectSendLevel = paramFloat;
      if (isRestricted_sync())
        return 0; 
      return playerSetAuxEffectSendLevel(false, paramFloat);
    } 
  }
  
  void baseRelease() {
    boolean bool = false;
    synchronized (this.mLock) {
      if (this.mState != 0) {
        bool = true;
        this.mState = 0;
      } 
      if (bool)
        try {
          getService().releasePlayer(this.mPlayerIId);
        } catch (RemoteException remoteException) {
          Log.e("PlayerBase", "Error talking to audio service, the player will still be tracked", (Throwable)remoteException);
        }  
      try {
        if (this.mAppOps != null)
          this.mAppOps.stopWatchingMode(this.mAppOpsCallback); 
      } catch (Exception null) {}
      return;
    } 
  }
  
  private void updateAppOpsPlayAudio() {
    synchronized (this.mLock) {
      updateAppOpsPlayAudio_sync(false);
      return;
    } 
  }
  
  void updateAppOpsPlayAudio_sync(boolean paramBoolean) {}
  
  boolean isRestricted_sync() {
    return false;
  }
  
  private static IAudioService getService() {
    IAudioService iAudioService2 = sService;
    if (iAudioService2 != null)
      return iAudioService2; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService1 = IAudioService.Stub.asInterface(iBinder);
    return iAudioService1;
  }
  
  public void setStartDelayMs(int paramInt) {
    baseSetStartDelayMs(paramInt);
  }
  
  class IAppOpsCallbackWrapper extends IAppOpsCallback.Stub {
    private final WeakReference<PlayerBase> mWeakPB;
    
    public IAppOpsCallbackWrapper(PlayerBase this$0) {
      this.mWeakPB = new WeakReference<>(this$0);
    }
    
    public void opChanged(int param1Int1, int param1Int2, String param1String) {
      if (param1Int1 == 28) {
        PlayerBase playerBase = this.mWeakPB.get();
        if (playerBase != null)
          playerBase.updateAppOpsPlayAudio(); 
      } 
    }
  }
  
  class IPlayerWrapper extends IPlayer.Stub {
    private final WeakReference<PlayerBase> mWeakPB;
    
    public IPlayerWrapper(PlayerBase this$0) {
      this.mWeakPB = new WeakReference<>(this$0);
    }
    
    public void start() {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.playerStart(); 
    }
    
    public void pause() {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.playerPause(); 
    }
    
    public void stop() {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.playerStop(); 
    }
    
    public void setVolume(float param1Float) {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.setVolumeMultiplier(param1Float); 
    }
    
    public void setPan(float param1Float) {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.baseSetPan(param1Float); 
    }
    
    public void setStartDelayMs(int param1Int) {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.baseSetStartDelayMs(param1Int); 
    }
    
    public void applyVolumeShaper(VolumeShaper.Configuration param1Configuration, VolumeShaper.Operation param1Operation) {
      PlayerBase playerBase = this.mWeakPB.get();
      if (playerBase != null)
        playerBase.playerApplyVolumeShaper(param1Configuration, param1Operation); 
    }
  }
  
  class PlayerIdCard implements Parcelable {
    public static final int AUDIO_ATTRIBUTES_DEFINED = 1;
    
    public static final int AUDIO_ATTRIBUTES_NONE = 0;
    
    PlayerIdCard(PlayerBase this$0, AudioAttributes param1AudioAttributes, IPlayer param1IPlayer) {
      this.mPlayerType = this$0;
      this.mAttributes = param1AudioAttributes;
      this.mIPlayer = param1IPlayer;
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { Integer.valueOf(this.mPlayerType) });
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      IBinder iBinder;
      param1Parcel.writeInt(this.mPlayerType);
      this.mAttributes.writeToParcel(param1Parcel, 0);
      IPlayer iPlayer = this.mIPlayer;
      if (iPlayer == null) {
        iPlayer = null;
      } else {
        iBinder = iPlayer.asBinder();
      } 
      param1Parcel.writeStrongBinder(iBinder);
    }
    
    public static final Parcelable.Creator<PlayerIdCard> CREATOR = new Parcelable.Creator<PlayerIdCard>() {
        public PlayerBase.PlayerIdCard createFromParcel(Parcel param2Parcel) {
          return new PlayerBase.PlayerIdCard();
        }
        
        public PlayerBase.PlayerIdCard[] newArray(int param2Int) {
          return new PlayerBase.PlayerIdCard[param2Int];
        }
      };
    
    public final AudioAttributes mAttributes;
    
    public final IPlayer mIPlayer;
    
    public final int mPlayerType;
    
    private PlayerIdCard(PlayerBase this$0) {
      IPlayer iPlayer;
      this.mPlayerType = this$0.readInt();
      this.mAttributes = AudioAttributes.CREATOR.createFromParcel((Parcel)this$0);
      IBinder iBinder = this$0.readStrongBinder();
      if (iBinder == null) {
        iBinder = null;
      } else {
        iPlayer = IPlayer.Stub.asInterface(iBinder);
      } 
      this.mIPlayer = iPlayer;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || !(param1Object instanceof PlayerIdCard))
        return false; 
      param1Object = param1Object;
      if (this.mPlayerType != ((PlayerIdCard)param1Object).mPlayerType || !this.mAttributes.equals(((PlayerIdCard)param1Object).mAttributes))
        bool = false; 
      return bool;
    }
  }
  
  public static void deprecateStreamTypeForPlayback(int paramInt, String paramString1, String paramString2) throws IllegalArgumentException {
    if (paramInt != 10) {
      Log.w(paramString1, "Use of stream types is deprecated for operations other than volume control");
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("See the documentation of ");
      stringBuilder.append(paramString2);
      stringBuilder.append(" for what to use instead with android.media.AudioAttributes to qualify your playback use case");
      Log.w(paramString1, stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Use of STREAM_ACCESSIBILITY is reserved for volume control");
  }
  
  abstract int playerApplyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation);
  
  abstract VolumeShaper.State playerGetVolumeShaperState(int paramInt);
  
  abstract void playerPause();
  
  abstract int playerSetAuxEffectSendLevel(boolean paramBoolean, float paramFloat);
  
  abstract void playerSetVolume(boolean paramBoolean, float paramFloat1, float paramFloat2);
  
  abstract void playerStart();
  
  abstract void playerStop();
}
