package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;

public final class RemoteDisplayState implements Parcelable {
  public RemoteDisplayState() {
    this.displays = new ArrayList<>();
  }
  
  RemoteDisplayState(Parcel paramParcel) {
    this.displays = paramParcel.createTypedArrayList(RemoteDisplayInfo.CREATOR);
  }
  
  public boolean isValid() {
    ArrayList<RemoteDisplayInfo> arrayList = this.displays;
    if (arrayList == null)
      return false; 
    int i = arrayList.size();
    for (byte b = 0; b < i; b++) {
      if (!((RemoteDisplayInfo)this.displays.get(b)).isValid())
        return false; 
    } 
    return true;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.displays);
  }
  
  public static final Parcelable.Creator<RemoteDisplayState> CREATOR = new Parcelable.Creator<RemoteDisplayState>() {
      public RemoteDisplayState createFromParcel(Parcel param1Parcel) {
        return new RemoteDisplayState(param1Parcel);
      }
      
      public RemoteDisplayState[] newArray(int param1Int) {
        return new RemoteDisplayState[param1Int];
      }
    };
  
  public static final int DISCOVERY_MODE_ACTIVE = 2;
  
  public static final int DISCOVERY_MODE_NONE = 0;
  
  public static final int DISCOVERY_MODE_PASSIVE = 1;
  
  public static final String SERVICE_INTERFACE = "com.android.media.remotedisplay.RemoteDisplayProvider";
  
  public final ArrayList<RemoteDisplayInfo> displays;
  
  public static final class RemoteDisplayInfo implements Parcelable {
    public RemoteDisplayInfo(String param1String) {
      this.id = param1String;
      this.status = 0;
      this.volumeHandling = 0;
      this.presentationDisplayId = -1;
    }
    
    public RemoteDisplayInfo(RemoteDisplayInfo param1RemoteDisplayInfo) {
      this.id = param1RemoteDisplayInfo.id;
      this.name = param1RemoteDisplayInfo.name;
      this.description = param1RemoteDisplayInfo.description;
      this.status = param1RemoteDisplayInfo.status;
      this.volume = param1RemoteDisplayInfo.volume;
      this.volumeMax = param1RemoteDisplayInfo.volumeMax;
      this.volumeHandling = param1RemoteDisplayInfo.volumeHandling;
      this.presentationDisplayId = param1RemoteDisplayInfo.presentationDisplayId;
    }
    
    RemoteDisplayInfo(Parcel param1Parcel) {
      this.id = param1Parcel.readString();
      this.name = param1Parcel.readString();
      this.description = param1Parcel.readString();
      this.status = param1Parcel.readInt();
      this.volume = param1Parcel.readInt();
      this.volumeMax = param1Parcel.readInt();
      this.volumeHandling = param1Parcel.readInt();
      this.presentationDisplayId = param1Parcel.readInt();
    }
    
    public boolean isValid() {
      boolean bool;
      if (!TextUtils.isEmpty(this.id) && !TextUtils.isEmpty(this.name)) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.id);
      param1Parcel.writeString(this.name);
      param1Parcel.writeString(this.description);
      param1Parcel.writeInt(this.status);
      param1Parcel.writeInt(this.volume);
      param1Parcel.writeInt(this.volumeMax);
      param1Parcel.writeInt(this.volumeHandling);
      param1Parcel.writeInt(this.presentationDisplayId);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RemoteDisplayInfo{ id=");
      stringBuilder.append(this.id);
      stringBuilder.append(", name=");
      stringBuilder.append(this.name);
      stringBuilder.append(", description=");
      stringBuilder.append(this.description);
      stringBuilder.append(", status=");
      stringBuilder.append(this.status);
      stringBuilder.append(", volume=");
      stringBuilder.append(this.volume);
      stringBuilder.append(", volumeMax=");
      stringBuilder.append(this.volumeMax);
      stringBuilder.append(", volumeHandling=");
      stringBuilder.append(this.volumeHandling);
      stringBuilder.append(", presentationDisplayId=");
      stringBuilder.append(this.presentationDisplayId);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<RemoteDisplayInfo> CREATOR = new Parcelable.Creator<RemoteDisplayInfo>() {
        public RemoteDisplayState.RemoteDisplayInfo createFromParcel(Parcel param2Parcel) {
          return new RemoteDisplayState.RemoteDisplayInfo(param2Parcel);
        }
        
        public RemoteDisplayState.RemoteDisplayInfo[] newArray(int param2Int) {
          return new RemoteDisplayState.RemoteDisplayInfo[param2Int];
        }
      };
    
    public static final int PLAYBACK_VOLUME_FIXED = 0;
    
    public static final int PLAYBACK_VOLUME_VARIABLE = 1;
    
    public static final int STATUS_AVAILABLE = 2;
    
    public static final int STATUS_CONNECTED = 4;
    
    public static final int STATUS_CONNECTING = 3;
    
    public static final int STATUS_IN_USE = 1;
    
    public static final int STATUS_NOT_AVAILABLE = 0;
    
    public String description;
    
    public String id;
    
    public String name;
    
    public int presentationDisplayId;
    
    public int status;
    
    public int volume;
    
    public int volumeHandling;
    
    public int volumeMax;
  }
  
  class null implements Parcelable.Creator<RemoteDisplayInfo> {
    public RemoteDisplayState.RemoteDisplayInfo createFromParcel(Parcel param1Parcel) {
      return new RemoteDisplayState.RemoteDisplayInfo(param1Parcel);
    }
    
    public RemoteDisplayState.RemoteDisplayInfo[] newArray(int param1Int) {
      return new RemoteDisplayState.RemoteDisplayInfo[param1Int];
    }
  }
}
