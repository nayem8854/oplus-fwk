package android.media;

import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.List;

public interface IMediaRouterService extends IInterface {
  void deselectRouteWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void deselectRouteWithRouter2(IMediaRouter2 paramIMediaRouter2, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  List<RoutingSessionInfo> getActiveSessions(IMediaRouter2Manager paramIMediaRouter2Manager) throws RemoteException;
  
  MediaRouterClientState getState(IMediaRouterClient paramIMediaRouterClient) throws RemoteException;
  
  List<MediaRoute2Info> getSystemRoutes() throws RemoteException;
  
  RoutingSessionInfo getSystemSessionInfo() throws RemoteException;
  
  boolean isPlaybackActive(IMediaRouterClient paramIMediaRouterClient) throws RemoteException;
  
  void registerClientAsUser(IMediaRouterClient paramIMediaRouterClient, String paramString, int paramInt) throws RemoteException;
  
  void registerClientGroupId(IMediaRouterClient paramIMediaRouterClient, String paramString) throws RemoteException;
  
  void registerManager(IMediaRouter2Manager paramIMediaRouter2Manager, String paramString) throws RemoteException;
  
  void registerRouter2(IMediaRouter2 paramIMediaRouter2, String paramString) throws RemoteException;
  
  void releaseSessionWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt, String paramString) throws RemoteException;
  
  void releaseSessionWithRouter2(IMediaRouter2 paramIMediaRouter2, String paramString) throws RemoteException;
  
  void requestCreateSessionWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt, RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void requestCreateSessionWithRouter2(IMediaRouter2 paramIMediaRouter2, int paramInt, long paramLong, RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info, Bundle paramBundle) throws RemoteException;
  
  void requestSetVolume(IMediaRouterClient paramIMediaRouterClient, String paramString, int paramInt) throws RemoteException;
  
  void requestUpdateVolume(IMediaRouterClient paramIMediaRouterClient, String paramString, int paramInt) throws RemoteException;
  
  void selectRouteWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void selectRouteWithRouter2(IMediaRouter2 paramIMediaRouter2, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void setDiscoveryRequest(IMediaRouterClient paramIMediaRouterClient, int paramInt, boolean paramBoolean) throws RemoteException;
  
  void setDiscoveryRequestWithRouter2(IMediaRouter2 paramIMediaRouter2, RouteDiscoveryPreference paramRouteDiscoveryPreference) throws RemoteException;
  
  void setRouteVolumeWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt1, MediaRoute2Info paramMediaRoute2Info, int paramInt2) throws RemoteException;
  
  void setRouteVolumeWithRouter2(IMediaRouter2 paramIMediaRouter2, MediaRoute2Info paramMediaRoute2Info, int paramInt) throws RemoteException;
  
  void setSelectedRoute(IMediaRouterClient paramIMediaRouterClient, String paramString, boolean paramBoolean) throws RemoteException;
  
  void setSessionVolumeWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt1, String paramString, int paramInt2) throws RemoteException;
  
  void setSessionVolumeWithRouter2(IMediaRouter2 paramIMediaRouter2, String paramString, int paramInt) throws RemoteException;
  
  void transferToRouteWithManager(IMediaRouter2Manager paramIMediaRouter2Manager, int paramInt, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void transferToRouteWithRouter2(IMediaRouter2 paramIMediaRouter2, String paramString, MediaRoute2Info paramMediaRoute2Info) throws RemoteException;
  
  void unregisterClient(IMediaRouterClient paramIMediaRouterClient) throws RemoteException;
  
  void unregisterManager(IMediaRouter2Manager paramIMediaRouter2Manager) throws RemoteException;
  
  void unregisterRouter2(IMediaRouter2 paramIMediaRouter2) throws RemoteException;
  
  class Default implements IMediaRouterService {
    public void registerClientAsUser(IMediaRouterClient param1IMediaRouterClient, String param1String, int param1Int) throws RemoteException {}
    
    public void unregisterClient(IMediaRouterClient param1IMediaRouterClient) throws RemoteException {}
    
    public void registerClientGroupId(IMediaRouterClient param1IMediaRouterClient, String param1String) throws RemoteException {}
    
    public MediaRouterClientState getState(IMediaRouterClient param1IMediaRouterClient) throws RemoteException {
      return null;
    }
    
    public boolean isPlaybackActive(IMediaRouterClient param1IMediaRouterClient) throws RemoteException {
      return false;
    }
    
    public void setDiscoveryRequest(IMediaRouterClient param1IMediaRouterClient, int param1Int, boolean param1Boolean) throws RemoteException {}
    
    public void setSelectedRoute(IMediaRouterClient param1IMediaRouterClient, String param1String, boolean param1Boolean) throws RemoteException {}
    
    public void requestSetVolume(IMediaRouterClient param1IMediaRouterClient, String param1String, int param1Int) throws RemoteException {}
    
    public void requestUpdateVolume(IMediaRouterClient param1IMediaRouterClient, String param1String, int param1Int) throws RemoteException {}
    
    public List<MediaRoute2Info> getSystemRoutes() throws RemoteException {
      return null;
    }
    
    public RoutingSessionInfo getSystemSessionInfo() throws RemoteException {
      return null;
    }
    
    public void registerRouter2(IMediaRouter2 param1IMediaRouter2, String param1String) throws RemoteException {}
    
    public void unregisterRouter2(IMediaRouter2 param1IMediaRouter2) throws RemoteException {}
    
    public void setDiscoveryRequestWithRouter2(IMediaRouter2 param1IMediaRouter2, RouteDiscoveryPreference param1RouteDiscoveryPreference) throws RemoteException {}
    
    public void setRouteVolumeWithRouter2(IMediaRouter2 param1IMediaRouter2, MediaRoute2Info param1MediaRoute2Info, int param1Int) throws RemoteException {}
    
    public void requestCreateSessionWithRouter2(IMediaRouter2 param1IMediaRouter2, int param1Int, long param1Long, RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info, Bundle param1Bundle) throws RemoteException {}
    
    public void selectRouteWithRouter2(IMediaRouter2 param1IMediaRouter2, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void deselectRouteWithRouter2(IMediaRouter2 param1IMediaRouter2, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void transferToRouteWithRouter2(IMediaRouter2 param1IMediaRouter2, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void setSessionVolumeWithRouter2(IMediaRouter2 param1IMediaRouter2, String param1String, int param1Int) throws RemoteException {}
    
    public void releaseSessionWithRouter2(IMediaRouter2 param1IMediaRouter2, String param1String) throws RemoteException {}
    
    public List<RoutingSessionInfo> getActiveSessions(IMediaRouter2Manager param1IMediaRouter2Manager) throws RemoteException {
      return null;
    }
    
    public void registerManager(IMediaRouter2Manager param1IMediaRouter2Manager, String param1String) throws RemoteException {}
    
    public void unregisterManager(IMediaRouter2Manager param1IMediaRouter2Manager) throws RemoteException {}
    
    public void setRouteVolumeWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int1, MediaRoute2Info param1MediaRoute2Info, int param1Int2) throws RemoteException {}
    
    public void requestCreateSessionWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int, RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void selectRouteWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void deselectRouteWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void transferToRouteWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int, String param1String, MediaRoute2Info param1MediaRoute2Info) throws RemoteException {}
    
    public void setSessionVolumeWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int1, String param1String, int param1Int2) throws RemoteException {}
    
    public void releaseSessionWithManager(IMediaRouter2Manager param1IMediaRouter2Manager, int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRouterService {
    private static final String DESCRIPTOR = "android.media.IMediaRouterService";
    
    static final int TRANSACTION_deselectRouteWithManager = 28;
    
    static final int TRANSACTION_deselectRouteWithRouter2 = 18;
    
    static final int TRANSACTION_getActiveSessions = 22;
    
    static final int TRANSACTION_getState = 4;
    
    static final int TRANSACTION_getSystemRoutes = 10;
    
    static final int TRANSACTION_getSystemSessionInfo = 11;
    
    static final int TRANSACTION_isPlaybackActive = 5;
    
    static final int TRANSACTION_registerClientAsUser = 1;
    
    static final int TRANSACTION_registerClientGroupId = 3;
    
    static final int TRANSACTION_registerManager = 23;
    
    static final int TRANSACTION_registerRouter2 = 12;
    
    static final int TRANSACTION_releaseSessionWithManager = 31;
    
    static final int TRANSACTION_releaseSessionWithRouter2 = 21;
    
    static final int TRANSACTION_requestCreateSessionWithManager = 26;
    
    static final int TRANSACTION_requestCreateSessionWithRouter2 = 16;
    
    static final int TRANSACTION_requestSetVolume = 8;
    
    static final int TRANSACTION_requestUpdateVolume = 9;
    
    static final int TRANSACTION_selectRouteWithManager = 27;
    
    static final int TRANSACTION_selectRouteWithRouter2 = 17;
    
    static final int TRANSACTION_setDiscoveryRequest = 6;
    
    static final int TRANSACTION_setDiscoveryRequestWithRouter2 = 14;
    
    static final int TRANSACTION_setRouteVolumeWithManager = 25;
    
    static final int TRANSACTION_setRouteVolumeWithRouter2 = 15;
    
    static final int TRANSACTION_setSelectedRoute = 7;
    
    static final int TRANSACTION_setSessionVolumeWithManager = 30;
    
    static final int TRANSACTION_setSessionVolumeWithRouter2 = 20;
    
    static final int TRANSACTION_transferToRouteWithManager = 29;
    
    static final int TRANSACTION_transferToRouteWithRouter2 = 19;
    
    static final int TRANSACTION_unregisterClient = 2;
    
    static final int TRANSACTION_unregisterManager = 24;
    
    static final int TRANSACTION_unregisterRouter2 = 13;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRouterService");
    }
    
    public static IMediaRouterService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRouterService");
      if (iInterface != null && iInterface instanceof IMediaRouterService)
        return (IMediaRouterService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 31:
          return "releaseSessionWithManager";
        case 30:
          return "setSessionVolumeWithManager";
        case 29:
          return "transferToRouteWithManager";
        case 28:
          return "deselectRouteWithManager";
        case 27:
          return "selectRouteWithManager";
        case 26:
          return "requestCreateSessionWithManager";
        case 25:
          return "setRouteVolumeWithManager";
        case 24:
          return "unregisterManager";
        case 23:
          return "registerManager";
        case 22:
          return "getActiveSessions";
        case 21:
          return "releaseSessionWithRouter2";
        case 20:
          return "setSessionVolumeWithRouter2";
        case 19:
          return "transferToRouteWithRouter2";
        case 18:
          return "deselectRouteWithRouter2";
        case 17:
          return "selectRouteWithRouter2";
        case 16:
          return "requestCreateSessionWithRouter2";
        case 15:
          return "setRouteVolumeWithRouter2";
        case 14:
          return "setDiscoveryRequestWithRouter2";
        case 13:
          return "unregisterRouter2";
        case 12:
          return "registerRouter2";
        case 11:
          return "getSystemSessionInfo";
        case 10:
          return "getSystemRoutes";
        case 9:
          return "requestUpdateVolume";
        case 8:
          return "requestSetVolume";
        case 7:
          return "setSelectedRoute";
        case 6:
          return "setDiscoveryRequest";
        case 5:
          return "isPlaybackActive";
        case 4:
          return "getState";
        case 3:
          return "registerClientGroupId";
        case 2:
          return "unregisterClient";
        case 1:
          break;
      } 
      return "registerClientAsUser";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        String str5;
        IMediaRouter2Manager iMediaRouter2Manager2;
        String str4;
        IMediaRouter2Manager iMediaRouter2Manager1;
        List<RoutingSessionInfo> list1;
        String str3;
        IMediaRouter2 iMediaRouter21;
        String str2;
        RoutingSessionInfo routingSessionInfo;
        List<MediaRoute2Info> list;
        IMediaRouterClient iMediaRouterClient2;
        MediaRouterClientState mediaRouterClientState;
        String str1;
        IMediaRouterClient iMediaRouterClient1;
        IMediaRouter2Manager iMediaRouter2Manager4;
        String str10;
        IMediaRouter2Manager iMediaRouter2Manager3;
        IMediaRouter2 iMediaRouter23;
        String str9;
        IMediaRouter2 iMediaRouter22;
        String str8;
        IMediaRouterClient iMediaRouterClient4;
        String str7;
        IMediaRouterClient iMediaRouterClient3;
        String str14;
        IMediaRouter2Manager iMediaRouter2Manager6;
        String str13;
        IMediaRouter2Manager iMediaRouter2Manager5;
        IMediaRouter2 iMediaRouter25;
        String str12;
        IMediaRouter2 iMediaRouter24;
        IMediaRouterClient iMediaRouterClient6;
        String str11;
        IMediaRouter2 iMediaRouter26;
        long l;
        boolean bool1 = false, bool2 = false;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 31:
            param1Parcel1.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager4 = IMediaRouter2Manager.Stub.asInterface(param1Parcel1.readStrongBinder());
            param1Int1 = param1Parcel1.readInt();
            str5 = param1Parcel1.readString();
            releaseSessionWithManager(iMediaRouter2Manager4, param1Int1, str5);
            param1Parcel2.writeNoException();
            return true;
          case 30:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager4 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int2 = str5.readInt();
            str14 = str5.readString();
            param1Int1 = str5.readInt();
            setSessionVolumeWithManager(iMediaRouter2Manager4, param1Int2, str14, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 29:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager6 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int1 = str5.readInt();
            str10 = str5.readString();
            if (str5.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            transferToRouteWithManager(iMediaRouter2Manager6, param1Int1, str10, (MediaRoute2Info)str5);
            param1Parcel2.writeNoException();
            return true;
          case 28:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager3 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int1 = str5.readInt();
            str13 = str5.readString();
            if (str5.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            deselectRouteWithManager(iMediaRouter2Manager3, param1Int1, str13, (MediaRoute2Info)str5);
            param1Parcel2.writeNoException();
            return true;
          case 27:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager3 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int1 = str5.readInt();
            str13 = str5.readString();
            if (str5.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            selectRouteWithManager(iMediaRouter2Manager3, param1Int1, str13, (MediaRoute2Info)str5);
            param1Parcel2.writeNoException();
            return true;
          case 26:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager5 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int1 = str5.readInt();
            if (str5.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo1 = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)str5);
            } else {
              iMediaRouter2Manager3 = null;
            } 
            if (str5.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str5);
            } else {
              str5 = null;
            } 
            requestCreateSessionWithManager(iMediaRouter2Manager5, param1Int1, (RoutingSessionInfo)iMediaRouter2Manager3, (MediaRoute2Info)str5);
            param1Parcel2.writeNoException();
            return true;
          case 25:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager5 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            param1Int2 = str5.readInt();
            if (str5.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str5);
            } else {
              iMediaRouter2Manager3 = null;
            } 
            param1Int1 = str5.readInt();
            setRouteVolumeWithManager(iMediaRouter2Manager5, param1Int2, (MediaRoute2Info)iMediaRouter2Manager3, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 24:
            str5.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager2 = IMediaRouter2Manager.Stub.asInterface(str5.readStrongBinder());
            unregisterManager(iMediaRouter2Manager2);
            param1Parcel2.writeNoException();
            return true;
          case 23:
            iMediaRouter2Manager2.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager3 = IMediaRouter2Manager.Stub.asInterface(iMediaRouter2Manager2.readStrongBinder());
            str4 = iMediaRouter2Manager2.readString();
            registerManager(iMediaRouter2Manager3, str4);
            param1Parcel2.writeNoException();
            return true;
          case 22:
            str4.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter2Manager1 = IMediaRouter2Manager.Stub.asInterface(str4.readStrongBinder());
            list1 = getActiveSessions(iMediaRouter2Manager1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list1);
            return true;
          case 21:
            list1.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter23 = IMediaRouter2.Stub.asInterface(list1.readStrongBinder());
            str3 = list1.readString();
            releaseSessionWithRouter2(iMediaRouter23, str3);
            param1Parcel2.writeNoException();
            return true;
          case 20:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter25 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            str9 = str3.readString();
            param1Int1 = str3.readInt();
            setSessionVolumeWithRouter2(iMediaRouter25, str9, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 19:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter25 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            str9 = str3.readString();
            if (str3.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            transferToRouteWithRouter2(iMediaRouter25, str9, (MediaRoute2Info)str3);
            param1Parcel2.writeNoException();
            return true;
          case 18:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter25 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            str9 = str3.readString();
            if (str3.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            deselectRouteWithRouter2(iMediaRouter25, str9, (MediaRoute2Info)str3);
            param1Parcel2.writeNoException();
            return true;
          case 17:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter22 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            str12 = str3.readString();
            if (str3.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            selectRouteWithRouter2(iMediaRouter22, str12, (MediaRoute2Info)str3);
            param1Parcel2.writeNoException();
            return true;
          case 16:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter26 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            param1Int1 = str3.readInt();
            l = str3.readLong();
            if (str3.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo1 = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)str3);
            } else {
              iMediaRouter22 = null;
            } 
            if (str3.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str12 = null;
            } 
            if (str3.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            requestCreateSessionWithRouter2(iMediaRouter26, param1Int1, l, (RoutingSessionInfo)iMediaRouter22, (MediaRoute2Info)str12, (Bundle)str3);
            param1Parcel2.writeNoException();
            return true;
          case 15:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter24 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            if (str3.readInt() != 0) {
              MediaRoute2Info mediaRoute2Info = MediaRoute2Info.CREATOR.createFromParcel((Parcel)str3);
            } else {
              iMediaRouter22 = null;
            } 
            param1Int1 = str3.readInt();
            setRouteVolumeWithRouter2(iMediaRouter24, (MediaRoute2Info)iMediaRouter22, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 14:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter22 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            if (str3.readInt() != 0) {
              RouteDiscoveryPreference routeDiscoveryPreference = RouteDiscoveryPreference.CREATOR.createFromParcel((Parcel)str3);
            } else {
              str3 = null;
            } 
            setDiscoveryRequestWithRouter2(iMediaRouter22, (RouteDiscoveryPreference)str3);
            param1Parcel2.writeNoException();
            return true;
          case 13:
            str3.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter21 = IMediaRouter2.Stub.asInterface(str3.readStrongBinder());
            unregisterRouter2(iMediaRouter21);
            param1Parcel2.writeNoException();
            return true;
          case 12:
            iMediaRouter21.enforceInterface("android.media.IMediaRouterService");
            iMediaRouter22 = IMediaRouter2.Stub.asInterface(iMediaRouter21.readStrongBinder());
            str2 = iMediaRouter21.readString();
            registerRouter2(iMediaRouter22, str2);
            param1Parcel2.writeNoException();
            return true;
          case 11:
            str2.enforceInterface("android.media.IMediaRouterService");
            routingSessionInfo = getSystemSessionInfo();
            param1Parcel2.writeNoException();
            if (routingSessionInfo != null) {
              param1Parcel2.writeInt(1);
              routingSessionInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 10:
            routingSessionInfo.enforceInterface("android.media.IMediaRouterService");
            list = getSystemRoutes();
            param1Parcel2.writeNoException();
            param1Parcel2.writeTypedList(list);
            return true;
          case 9:
            list.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient6 = IMediaRouterClient.Stub.asInterface(list.readStrongBinder());
            str8 = list.readString();
            param1Int1 = list.readInt();
            requestUpdateVolume(iMediaRouterClient6, str8, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 8:
            list.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient4 = IMediaRouterClient.Stub.asInterface(list.readStrongBinder());
            str11 = list.readString();
            param1Int1 = list.readInt();
            requestSetVolume(iMediaRouterClient4, str11, param1Int1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            list.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient5 = IMediaRouterClient.Stub.asInterface(list.readStrongBinder());
            str7 = list.readString();
            bool1 = bool2;
            if (list.readInt() != 0)
              bool1 = true; 
            setSelectedRoute(iMediaRouterClient5, str7, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            list.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient3 = IMediaRouterClient.Stub.asInterface(list.readStrongBinder());
            param1Int1 = list.readInt();
            if (list.readInt() != 0)
              bool1 = true; 
            setDiscoveryRequest(iMediaRouterClient3, param1Int1, bool1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            list.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient2 = IMediaRouterClient.Stub.asInterface(list.readStrongBinder());
            bool = isPlaybackActive(iMediaRouterClient2);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            iMediaRouterClient2.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient2 = IMediaRouterClient.Stub.asInterface(iMediaRouterClient2.readStrongBinder());
            mediaRouterClientState = getState(iMediaRouterClient2);
            param1Parcel2.writeNoException();
            if (mediaRouterClientState != null) {
              param1Parcel2.writeInt(1);
              mediaRouterClientState.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 3:
            mediaRouterClientState.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient3 = IMediaRouterClient.Stub.asInterface(mediaRouterClientState.readStrongBinder());
            str1 = mediaRouterClientState.readString();
            registerClientGroupId(iMediaRouterClient3, str1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            str1.enforceInterface("android.media.IMediaRouterService");
            iMediaRouterClient1 = IMediaRouterClient.Stub.asInterface(str1.readStrongBinder());
            unregisterClient(iMediaRouterClient1);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iMediaRouterClient1.enforceInterface("android.media.IMediaRouterService");
        IMediaRouterClient iMediaRouterClient5 = IMediaRouterClient.Stub.asInterface(iMediaRouterClient1.readStrongBinder());
        String str6 = iMediaRouterClient1.readString();
        int i = iMediaRouterClient1.readInt();
        registerClientAsUser(iMediaRouterClient5, str6, i);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.media.IMediaRouterService");
      return true;
    }
    
    private static class Proxy implements IMediaRouterService {
      public static IMediaRouterService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRouterService";
      }
      
      public void registerClientAsUser(IMediaRouterClient param2IMediaRouterClient, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().registerClientAsUser(param2IMediaRouterClient, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterClient(IMediaRouterClient param2IMediaRouterClient) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().unregisterClient(param2IMediaRouterClient);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerClientGroupId(IMediaRouterClient param2IMediaRouterClient, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().registerClientGroupId(param2IMediaRouterClient, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MediaRouterClientState getState(IMediaRouterClient param2IMediaRouterClient) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null)
            return IMediaRouterService.Stub.getDefaultImpl().getState(param2IMediaRouterClient); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            MediaRouterClientState mediaRouterClientState = MediaRouterClientState.CREATOR.createFromParcel(parcel2);
          } else {
            param2IMediaRouterClient = null;
          } 
          return (MediaRouterClientState)param2IMediaRouterClient;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean isPlaybackActive(IMediaRouterClient param2IMediaRouterClient) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          IBinder iBinder = this.mRemote;
          boolean bool1 = false, bool2 = iBinder.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IMediaRouterService.Stub.getDefaultImpl() != null) {
            bool1 = IMediaRouterService.Stub.getDefaultImpl().isPlaybackActive(param2IMediaRouterClient);
            return bool1;
          } 
          parcel2.readException();
          int i = parcel2.readInt();
          if (i != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDiscoveryRequest(IMediaRouterClient param2IMediaRouterClient, int param2Int, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool1 && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setDiscoveryRequest(param2IMediaRouterClient, param2Int, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSelectedRoute(IMediaRouterClient param2IMediaRouterClient, String param2String, boolean param2Boolean) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          boolean bool;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2Boolean) {
            bool = true;
          } else {
            bool = false;
          } 
          parcel1.writeInt(bool);
          boolean bool1 = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool1 && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setSelectedRoute(param2IMediaRouterClient, param2String, param2Boolean);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestSetVolume(IMediaRouterClient param2IMediaRouterClient, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().requestSetVolume(param2IMediaRouterClient, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestUpdateVolume(IMediaRouterClient param2IMediaRouterClient, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouterClient != null) {
            iBinder = param2IMediaRouterClient.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().requestUpdateVolume(param2IMediaRouterClient, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<MediaRoute2Info> getSystemRoutes() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null)
            return IMediaRouterService.Stub.getDefaultImpl().getSystemRoutes(); 
          parcel2.readException();
          return parcel2.createTypedArrayList(MediaRoute2Info.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public RoutingSessionInfo getSystemSessionInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          RoutingSessionInfo routingSessionInfo;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            routingSessionInfo = IMediaRouterService.Stub.getDefaultImpl().getSystemSessionInfo();
            return routingSessionInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(parcel2);
          } else {
            routingSessionInfo = null;
          } 
          return routingSessionInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerRouter2(IMediaRouter2 param2IMediaRouter2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(12, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().registerRouter2(param2IMediaRouter2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterRouter2(IMediaRouter2 param2IMediaRouter2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(13, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().unregisterRouter2(param2IMediaRouter2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDiscoveryRequestWithRouter2(IMediaRouter2 param2IMediaRouter2, RouteDiscoveryPreference param2RouteDiscoveryPreference) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2RouteDiscoveryPreference != null) {
            parcel1.writeInt(1);
            param2RouteDiscoveryPreference.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(14, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setDiscoveryRequestWithRouter2(param2IMediaRouter2, param2RouteDiscoveryPreference);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRouteVolumeWithRouter2(IMediaRouter2 param2IMediaRouter2, MediaRoute2Info param2MediaRoute2Info, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(15, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setRouteVolumeWithRouter2(param2IMediaRouter2, param2MediaRoute2Info, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestCreateSessionWithRouter2(IMediaRouter2 param2IMediaRouter2, int param2Int, long param2Long, RoutingSessionInfo param2RoutingSessionInfo, MediaRoute2Info param2MediaRoute2Info, Bundle param2Bundle) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeInt(param2Int);
            parcel1.writeLong(param2Long);
            if (param2RoutingSessionInfo != null) {
              parcel1.writeInt(1);
              param2RoutingSessionInfo.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2MediaRoute2Info != null) {
              parcel1.writeInt(1);
              param2MediaRoute2Info.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            if (param2Bundle != null) {
              parcel1.writeInt(1);
              param2Bundle.writeToParcel(parcel1, 0);
            } else {
              parcel1.writeInt(0);
            } 
            try {
              boolean bool = this.mRemote.transact(16, parcel1, parcel2, 0);
              if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
                IMediaRouterService.Stub.getDefaultImpl().requestCreateSessionWithRouter2(param2IMediaRouter2, param2Int, param2Long, param2RoutingSessionInfo, param2MediaRoute2Info, param2Bundle);
                parcel2.recycle();
                parcel1.recycle();
                return;
              } 
              parcel2.readException();
              parcel2.recycle();
              parcel1.recycle();
              return;
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IMediaRouter2;
      }
      
      public void selectRouteWithRouter2(IMediaRouter2 param2IMediaRouter2, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(17, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().selectRouteWithRouter2(param2IMediaRouter2, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deselectRouteWithRouter2(IMediaRouter2 param2IMediaRouter2, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(18, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().deselectRouteWithRouter2(param2IMediaRouter2, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void transferToRouteWithRouter2(IMediaRouter2 param2IMediaRouter2, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(19, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().transferToRouteWithRouter2(param2IMediaRouter2, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSessionVolumeWithRouter2(IMediaRouter2 param2IMediaRouter2, String param2String, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(20, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setSessionVolumeWithRouter2(param2IMediaRouter2, param2String, param2Int);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseSessionWithRouter2(IMediaRouter2 param2IMediaRouter2, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2 != null) {
            iBinder = param2IMediaRouter2.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(21, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().releaseSessionWithRouter2(param2IMediaRouter2, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public List<RoutingSessionInfo> getActiveSessions(IMediaRouter2Manager param2IMediaRouter2Manager) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(22, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null)
            return IMediaRouterService.Stub.getDefaultImpl().getActiveSessions(param2IMediaRouter2Manager); 
          parcel2.readException();
          return parcel2.createTypedArrayList(RoutingSessionInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerManager(IMediaRouter2Manager param2IMediaRouter2Manager, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(23, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().registerManager(param2IMediaRouter2Manager, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterManager(IMediaRouter2Manager param2IMediaRouter2Manager) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(24, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().unregisterManager(param2IMediaRouter2Manager);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setRouteVolumeWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int1, MediaRoute2Info param2MediaRoute2Info, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(25, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setRouteVolumeWithManager(param2IMediaRouter2Manager, param2Int1, param2MediaRoute2Info, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void requestCreateSessionWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int, RoutingSessionInfo param2RoutingSessionInfo, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          if (param2RoutingSessionInfo != null) {
            parcel1.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(26, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().requestCreateSessionWithManager(param2IMediaRouter2Manager, param2Int, param2RoutingSessionInfo, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void selectRouteWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(27, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().selectRouteWithManager(param2IMediaRouter2Manager, param2Int, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void deselectRouteWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(28, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().deselectRouteWithManager(param2IMediaRouter2Manager, param2Int, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void transferToRouteWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int, String param2String, MediaRoute2Info param2MediaRoute2Info) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          if (param2MediaRoute2Info != null) {
            parcel1.writeInt(1);
            param2MediaRoute2Info.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(29, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().transferToRouteWithManager(param2IMediaRouter2Manager, param2Int, param2String, param2MediaRoute2Info);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setSessionVolumeWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int1, String param2String, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int1);
          parcel1.writeString(param2String);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(30, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().setSessionVolumeWithManager(param2IMediaRouter2Manager, param2Int1, param2String, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void releaseSessionWithManager(IMediaRouter2Manager param2IMediaRouter2Manager, int param2Int, String param2String) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.IMediaRouterService");
          if (param2IMediaRouter2Manager != null) {
            iBinder = param2IMediaRouter2Manager.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          parcel1.writeInt(param2Int);
          parcel1.writeString(param2String);
          boolean bool = this.mRemote.transact(31, parcel1, parcel2, 0);
          if (!bool && IMediaRouterService.Stub.getDefaultImpl() != null) {
            IMediaRouterService.Stub.getDefaultImpl().releaseSessionWithManager(param2IMediaRouter2Manager, param2Int, param2String);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRouterService param1IMediaRouterService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRouterService != null) {
          Proxy.sDefaultImpl = param1IMediaRouterService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRouterService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
