package android.media.midi;

import java.io.IOException;

public abstract class MidiReceiver {
  private final int mMaxMessageSize;
  
  public MidiReceiver() {
    this.mMaxMessageSize = Integer.MAX_VALUE;
  }
  
  public MidiReceiver(int paramInt) {
    this.mMaxMessageSize = paramInt;
  }
  
  public void flush() throws IOException {
    onFlush();
  }
  
  public void onFlush() throws IOException {}
  
  public final int getMaxMessageSize() {
    return this.mMaxMessageSize;
  }
  
  public void send(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws IOException {
    send(paramArrayOfbyte, paramInt1, paramInt2, 0L);
  }
  
  public void send(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) throws IOException {
    int i = getMaxMessageSize();
    while (paramInt2 > 0) {
      int j;
      if (paramInt2 > i) {
        j = i;
      } else {
        j = paramInt2;
      } 
      onSend(paramArrayOfbyte, paramInt1, j, paramLong);
      paramInt1 += j;
      paramInt2 -= j;
    } 
  }
  
  public abstract void onSend(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) throws IOException;
}
