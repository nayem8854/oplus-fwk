package android.media.midi;

import android.bluetooth.BluetoothDevice;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import java.util.concurrent.ConcurrentHashMap;

public final class MidiManager {
  private final IBinder mToken = new Binder();
  
  private final IMidiManager mService;
  
  private ConcurrentHashMap<DeviceCallback, DeviceListener> mDeviceListeners = new ConcurrentHashMap<>();
  
  private static final String TAG = "MidiManager";
  
  public static final String BLUETOOTH_MIDI_SERVICE_PACKAGE = "com.android.bluetoothmidiservice";
  
  public static final String BLUETOOTH_MIDI_SERVICE_INTENT = "android.media.midi.BluetoothMidiService";
  
  public static final String BLUETOOTH_MIDI_SERVICE_CLASS = "com.android.bluetoothmidiservice.BluetoothMidiService";
  
  class DeviceListener extends IMidiDeviceListener.Stub {
    private final MidiManager.DeviceCallback mCallback;
    
    private final Handler mHandler;
    
    final MidiManager this$0;
    
    public DeviceListener(MidiManager.DeviceCallback param1DeviceCallback, Handler param1Handler) {
      this.mCallback = param1DeviceCallback;
      this.mHandler = param1Handler;
    }
    
    public void onDeviceAdded(final MidiDeviceInfo deviceF) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new Runnable() {
              final MidiManager.DeviceListener this$1;
              
              final MidiDeviceInfo val$deviceF;
              
              public void run() {
                this.this$1.mCallback.onDeviceAdded(deviceF);
              }
            });
      } else {
        this.mCallback.onDeviceAdded(deviceF);
      } 
    }
    
    public void onDeviceRemoved(final MidiDeviceInfo deviceF) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new Runnable() {
              final MidiManager.DeviceListener this$1;
              
              final MidiDeviceInfo val$deviceF;
              
              public void run() {
                this.this$1.mCallback.onDeviceRemoved(deviceF);
              }
            });
      } else {
        this.mCallback.onDeviceRemoved(deviceF);
      } 
    }
    
    public void onDeviceStatusChanged(final MidiDeviceStatus statusF) {
      Handler handler = this.mHandler;
      if (handler != null) {
        handler.post(new Runnable() {
              final MidiManager.DeviceListener this$1;
              
              final MidiDeviceStatus val$statusF;
              
              public void run() {
                this.this$1.mCallback.onDeviceStatusChanged(statusF);
              }
            });
      } else {
        this.mCallback.onDeviceStatusChanged(statusF);
      } 
    }
  }
  
  class null implements Runnable {
    final MidiManager.DeviceListener this$1;
    
    final MidiDeviceInfo val$deviceF;
    
    public void run() {
      this.this$1.mCallback.onDeviceAdded(deviceF);
    }
  }
  
  class null implements Runnable {
    final MidiManager.DeviceListener this$1;
    
    final MidiDeviceInfo val$deviceF;
    
    public void run() {
      this.this$1.mCallback.onDeviceRemoved(deviceF);
    }
  }
  
  class null implements Runnable {
    final MidiManager.DeviceListener this$1;
    
    final MidiDeviceStatus val$statusF;
    
    public void run() {
      this.this$1.mCallback.onDeviceStatusChanged(statusF);
    }
  }
  
  public static class DeviceCallback {
    public void onDeviceAdded(MidiDeviceInfo param1MidiDeviceInfo) {}
    
    public void onDeviceRemoved(MidiDeviceInfo param1MidiDeviceInfo) {}
    
    public void onDeviceStatusChanged(MidiDeviceStatus param1MidiDeviceStatus) {}
  }
  
  public MidiManager(IMidiManager paramIMidiManager) {
    this.mService = paramIMidiManager;
  }
  
  public void registerDeviceCallback(DeviceCallback paramDeviceCallback, Handler paramHandler) {
    DeviceListener deviceListener = new DeviceListener(paramDeviceCallback, paramHandler);
    try {
      this.mService.registerListener(this.mToken, deviceListener);
      this.mDeviceListeners.put(paramDeviceCallback, deviceListener);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void unregisterDeviceCallback(DeviceCallback paramDeviceCallback) {
    DeviceListener deviceListener = this.mDeviceListeners.remove(paramDeviceCallback);
    if (deviceListener != null)
      try {
        this.mService.unregisterListener(this.mToken, deviceListener);
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      }  
  }
  
  public MidiDeviceInfo[] getDevices() {
    try {
      return this.mService.getDevices();
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  private void sendOpenDeviceResponse(final MidiDevice device, final OnDeviceOpenedListener listener, Handler paramHandler) {
    if (paramHandler != null) {
      paramHandler.post(new Runnable() {
            final MidiManager this$0;
            
            final MidiDevice val$device;
            
            final MidiManager.OnDeviceOpenedListener val$listener;
            
            public void run() {
              listener.onDeviceOpened(device);
            }
          });
    } else {
      listener.onDeviceOpened(device);
    } 
  }
  
  public void openDevice(MidiDeviceInfo paramMidiDeviceInfo, OnDeviceOpenedListener paramOnDeviceOpenedListener, Handler paramHandler) {
    Object object = new Object(this, paramMidiDeviceInfo, paramOnDeviceOpenedListener, paramHandler);
    try {
      this.mService.openDevice(this.mToken, paramMidiDeviceInfo, (IMidiDeviceOpenCallback)object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public void openBluetoothDevice(BluetoothDevice paramBluetoothDevice, OnDeviceOpenedListener paramOnDeviceOpenedListener, Handler paramHandler) {
    Object object = new Object(this, paramOnDeviceOpenedListener, paramHandler);
    try {
      this.mService.openBluetoothDevice(this.mToken, paramBluetoothDevice, (IMidiDeviceOpenCallback)object);
      return;
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  public MidiDeviceServer createDeviceServer(MidiReceiver[] paramArrayOfMidiReceiver, int paramInt1, String[] paramArrayOfString1, String[] paramArrayOfString2, Bundle paramBundle, int paramInt2, MidiDeviceServer.Callback paramCallback) {
    try {
      MidiDeviceServer midiDeviceServer = new MidiDeviceServer();
      IMidiManager iMidiManager = this.mService;
      try {
        this(iMidiManager, paramArrayOfMidiReceiver, paramInt1, paramCallback);
        MidiDeviceInfo midiDeviceInfo = this.mService.registerDeviceServer(midiDeviceServer.getBinderInterface(), paramArrayOfMidiReceiver.length, paramInt1, paramArrayOfString1, paramArrayOfString2, paramBundle, paramInt2);
        if (midiDeviceInfo == null) {
          Log.e("MidiManager", "registerVirtualDevice failed");
          return null;
        } 
        return midiDeviceServer;
      } catch (RemoteException null) {}
    } catch (RemoteException remoteException) {}
    throw remoteException.rethrowFromSystemServer();
  }
  
  public static interface OnDeviceOpenedListener {
    void onDeviceOpened(MidiDevice param1MidiDevice);
  }
}
