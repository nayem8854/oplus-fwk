package android.media.midi;

import android.os.IBinder;
import android.os.ParcelFileDescriptor;
import android.os.RemoteException;
import android.util.Log;
import com.android.internal.midi.MidiDispatcher;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.IOException;

public final class MidiOutputPort extends MidiSender implements Closeable {
  private static final String TAG = "MidiOutputPort";
  
  private IMidiDeviceServer mDeviceServer;
  
  private final MidiDispatcher mDispatcher = new MidiDispatcher();
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private final FileInputStream mInputStream;
  
  private boolean mIsClosed;
  
  private final int mPortNumber;
  
  private final Thread mThread = (Thread)new Object(this);
  
  private final IBinder mToken;
  
  MidiOutputPort(IMidiDeviceServer paramIMidiDeviceServer, IBinder paramIBinder, FileDescriptor paramFileDescriptor, int paramInt) {
    this.mDeviceServer = paramIMidiDeviceServer;
    this.mToken = paramIBinder;
    this.mPortNumber = paramInt;
    this.mInputStream = new ParcelFileDescriptor.AutoCloseInputStream(new ParcelFileDescriptor(paramFileDescriptor));
    this.mThread.start();
    this.mGuard.open("close");
  }
  
  MidiOutputPort(FileDescriptor paramFileDescriptor, int paramInt) {
    this(null, null, paramFileDescriptor, paramInt);
  }
  
  public final int getPortNumber() {
    return this.mPortNumber;
  }
  
  public void onConnect(MidiReceiver paramMidiReceiver) {
    this.mDispatcher.getSender().connect(paramMidiReceiver);
  }
  
  public void onDisconnect(MidiReceiver paramMidiReceiver) {
    this.mDispatcher.getSender().disconnect(paramMidiReceiver);
  }
  
  public void close() throws IOException {
    synchronized (this.mGuard) {
      if (this.mIsClosed)
        return; 
      this.mGuard.close();
      this.mInputStream.close();
      IMidiDeviceServer iMidiDeviceServer = this.mDeviceServer;
      if (iMidiDeviceServer != null)
        try {
          this.mDeviceServer.closePort(this.mToken);
        } catch (RemoteException remoteException) {
          Log.e("MidiOutputPort", "RemoteException in MidiOutputPort.close()");
        }  
      this.mIsClosed = true;
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null)
        this.mGuard.warnIfOpen(); 
      this.mDeviceServer = null;
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
}
