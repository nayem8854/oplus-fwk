package android.media.midi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMidiDeviceListener extends IInterface {
  void onDeviceAdded(MidiDeviceInfo paramMidiDeviceInfo) throws RemoteException;
  
  void onDeviceRemoved(MidiDeviceInfo paramMidiDeviceInfo) throws RemoteException;
  
  void onDeviceStatusChanged(MidiDeviceStatus paramMidiDeviceStatus) throws RemoteException;
  
  class Default implements IMidiDeviceListener {
    public void onDeviceAdded(MidiDeviceInfo param1MidiDeviceInfo) throws RemoteException {}
    
    public void onDeviceRemoved(MidiDeviceInfo param1MidiDeviceInfo) throws RemoteException {}
    
    public void onDeviceStatusChanged(MidiDeviceStatus param1MidiDeviceStatus) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMidiDeviceListener {
    private static final String DESCRIPTOR = "android.media.midi.IMidiDeviceListener";
    
    static final int TRANSACTION_onDeviceAdded = 1;
    
    static final int TRANSACTION_onDeviceRemoved = 2;
    
    static final int TRANSACTION_onDeviceStatusChanged = 3;
    
    public Stub() {
      attachInterface(this, "android.media.midi.IMidiDeviceListener");
    }
    
    public static IMidiDeviceListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.midi.IMidiDeviceListener");
      if (iInterface != null && iInterface instanceof IMidiDeviceListener)
        return (IMidiDeviceListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3)
            return null; 
          return "onDeviceStatusChanged";
        } 
        return "onDeviceRemoved";
      } 
      return "onDeviceAdded";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 1598968902)
              return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
            param1Parcel2.writeString("android.media.midi.IMidiDeviceListener");
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceListener");
          if (param1Parcel1.readInt() != 0) {
            MidiDeviceStatus midiDeviceStatus = MidiDeviceStatus.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          onDeviceStatusChanged((MidiDeviceStatus)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceListener");
        if (param1Parcel1.readInt() != 0) {
          MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        onDeviceRemoved((MidiDeviceInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceListener");
      if (param1Parcel1.readInt() != 0) {
        MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      onDeviceAdded((MidiDeviceInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMidiDeviceListener {
      public static IMidiDeviceListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.midi.IMidiDeviceListener";
      }
      
      public void onDeviceAdded(MidiDeviceInfo param2MidiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceListener");
          if (param2MidiDeviceInfo != null) {
            parcel.writeInt(1);
            param2MidiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceListener.Stub.getDefaultImpl() != null) {
            IMidiDeviceListener.Stub.getDefaultImpl().onDeviceAdded(param2MidiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDeviceRemoved(MidiDeviceInfo param2MidiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceListener");
          if (param2MidiDeviceInfo != null) {
            parcel.writeInt(1);
            param2MidiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceListener.Stub.getDefaultImpl() != null) {
            IMidiDeviceListener.Stub.getDefaultImpl().onDeviceRemoved(param2MidiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void onDeviceStatusChanged(MidiDeviceStatus param2MidiDeviceStatus) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceListener");
          if (param2MidiDeviceStatus != null) {
            parcel.writeInt(1);
            param2MidiDeviceStatus.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceListener.Stub.getDefaultImpl() != null) {
            IMidiDeviceListener.Stub.getDefaultImpl().onDeviceStatusChanged(param2MidiDeviceStatus);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMidiDeviceListener param1IMidiDeviceListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMidiDeviceListener != null) {
          Proxy.sDefaultImpl = param1IMidiDeviceListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMidiDeviceListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
