package android.media.midi;

import android.bluetooth.BluetoothDevice;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMidiManager extends IInterface {
  void closeDevice(IBinder paramIBinder1, IBinder paramIBinder2) throws RemoteException;
  
  MidiDeviceStatus getDeviceStatus(MidiDeviceInfo paramMidiDeviceInfo) throws RemoteException;
  
  MidiDeviceInfo[] getDevices() throws RemoteException;
  
  MidiDeviceInfo getServiceDeviceInfo(String paramString1, String paramString2) throws RemoteException;
  
  void openBluetoothDevice(IBinder paramIBinder, BluetoothDevice paramBluetoothDevice, IMidiDeviceOpenCallback paramIMidiDeviceOpenCallback) throws RemoteException;
  
  void openDevice(IBinder paramIBinder, MidiDeviceInfo paramMidiDeviceInfo, IMidiDeviceOpenCallback paramIMidiDeviceOpenCallback) throws RemoteException;
  
  MidiDeviceInfo registerDeviceServer(IMidiDeviceServer paramIMidiDeviceServer, int paramInt1, int paramInt2, String[] paramArrayOfString1, String[] paramArrayOfString2, Bundle paramBundle, int paramInt3) throws RemoteException;
  
  void registerListener(IBinder paramIBinder, IMidiDeviceListener paramIMidiDeviceListener) throws RemoteException;
  
  void setDeviceStatus(IMidiDeviceServer paramIMidiDeviceServer, MidiDeviceStatus paramMidiDeviceStatus) throws RemoteException;
  
  void unregisterDeviceServer(IMidiDeviceServer paramIMidiDeviceServer) throws RemoteException;
  
  void unregisterListener(IBinder paramIBinder, IMidiDeviceListener paramIMidiDeviceListener) throws RemoteException;
  
  class Default implements IMidiManager {
    public MidiDeviceInfo[] getDevices() throws RemoteException {
      return null;
    }
    
    public void registerListener(IBinder param1IBinder, IMidiDeviceListener param1IMidiDeviceListener) throws RemoteException {}
    
    public void unregisterListener(IBinder param1IBinder, IMidiDeviceListener param1IMidiDeviceListener) throws RemoteException {}
    
    public void openDevice(IBinder param1IBinder, MidiDeviceInfo param1MidiDeviceInfo, IMidiDeviceOpenCallback param1IMidiDeviceOpenCallback) throws RemoteException {}
    
    public void openBluetoothDevice(IBinder param1IBinder, BluetoothDevice param1BluetoothDevice, IMidiDeviceOpenCallback param1IMidiDeviceOpenCallback) throws RemoteException {}
    
    public void closeDevice(IBinder param1IBinder1, IBinder param1IBinder2) throws RemoteException {}
    
    public MidiDeviceInfo registerDeviceServer(IMidiDeviceServer param1IMidiDeviceServer, int param1Int1, int param1Int2, String[] param1ArrayOfString1, String[] param1ArrayOfString2, Bundle param1Bundle, int param1Int3) throws RemoteException {
      return null;
    }
    
    public void unregisterDeviceServer(IMidiDeviceServer param1IMidiDeviceServer) throws RemoteException {}
    
    public MidiDeviceInfo getServiceDeviceInfo(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public MidiDeviceStatus getDeviceStatus(MidiDeviceInfo param1MidiDeviceInfo) throws RemoteException {
      return null;
    }
    
    public void setDeviceStatus(IMidiDeviceServer param1IMidiDeviceServer, MidiDeviceStatus param1MidiDeviceStatus) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMidiManager {
    private static final String DESCRIPTOR = "android.media.midi.IMidiManager";
    
    static final int TRANSACTION_closeDevice = 6;
    
    static final int TRANSACTION_getDeviceStatus = 10;
    
    static final int TRANSACTION_getDevices = 1;
    
    static final int TRANSACTION_getServiceDeviceInfo = 9;
    
    static final int TRANSACTION_openBluetoothDevice = 5;
    
    static final int TRANSACTION_openDevice = 4;
    
    static final int TRANSACTION_registerDeviceServer = 7;
    
    static final int TRANSACTION_registerListener = 2;
    
    static final int TRANSACTION_setDeviceStatus = 11;
    
    static final int TRANSACTION_unregisterDeviceServer = 8;
    
    static final int TRANSACTION_unregisterListener = 3;
    
    public Stub() {
      attachInterface(this, "android.media.midi.IMidiManager");
    }
    
    public static IMidiManager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.midi.IMidiManager");
      if (iInterface != null && iInterface instanceof IMidiManager)
        return (IMidiManager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 11:
          return "setDeviceStatus";
        case 10:
          return "getDeviceStatus";
        case 9:
          return "getServiceDeviceInfo";
        case 8:
          return "unregisterDeviceServer";
        case 7:
          return "registerDeviceServer";
        case 6:
          return "closeDevice";
        case 5:
          return "openBluetoothDevice";
        case 4:
          return "openDevice";
        case 3:
          return "unregisterListener";
        case 2:
          return "registerListener";
        case 1:
          break;
      } 
      return "getDevices";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        MidiDeviceStatus midiDeviceStatus;
        String str1;
        MidiDeviceInfo midiDeviceInfo2;
        IMidiDeviceServer iMidiDeviceServer1;
        MidiDeviceInfo midiDeviceInfo1;
        IBinder iBinder1;
        IMidiDeviceOpenCallback iMidiDeviceOpenCallback;
        IMidiDeviceListener iMidiDeviceListener;
        IMidiDeviceServer iMidiDeviceServer2;
        String str2;
        IBinder iBinder2;
        IMidiDeviceServer iMidiDeviceServer3;
        String[] arrayOfString1, arrayOfString2;
        IBinder iBinder3;
        int i;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 11:
            param1Parcel1.enforceInterface("android.media.midi.IMidiManager");
            iMidiDeviceServer2 = IMidiDeviceServer.Stub.asInterface(param1Parcel1.readStrongBinder());
            if (param1Parcel1.readInt() != 0) {
              MidiDeviceStatus midiDeviceStatus1 = MidiDeviceStatus.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDeviceStatus(iMidiDeviceServer2, (MidiDeviceStatus)param1Parcel1);
            param1Parcel2.writeNoException();
            return true;
          case 10:
            param1Parcel1.enforceInterface("android.media.midi.IMidiManager");
            if (param1Parcel1.readInt() != 0) {
              MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            midiDeviceStatus = getDeviceStatus((MidiDeviceInfo)param1Parcel1);
            param1Parcel2.writeNoException();
            if (midiDeviceStatus != null) {
              param1Parcel2.writeInt(1);
              midiDeviceStatus.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 9:
            midiDeviceStatus.enforceInterface("android.media.midi.IMidiManager");
            str2 = midiDeviceStatus.readString();
            str1 = midiDeviceStatus.readString();
            midiDeviceInfo2 = getServiceDeviceInfo(str2, str1);
            param1Parcel2.writeNoException();
            if (midiDeviceInfo2 != null) {
              param1Parcel2.writeInt(1);
              midiDeviceInfo2.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 8:
            midiDeviceInfo2.enforceInterface("android.media.midi.IMidiManager");
            iMidiDeviceServer1 = IMidiDeviceServer.Stub.asInterface(midiDeviceInfo2.readStrongBinder());
            unregisterDeviceServer(iMidiDeviceServer1);
            param1Parcel2.writeNoException();
            return true;
          case 7:
            iMidiDeviceServer1.enforceInterface("android.media.midi.IMidiManager");
            iMidiDeviceServer3 = IMidiDeviceServer.Stub.asInterface(iMidiDeviceServer1.readStrongBinder());
            param1Int2 = iMidiDeviceServer1.readInt();
            param1Int1 = iMidiDeviceServer1.readInt();
            arrayOfString1 = iMidiDeviceServer1.createStringArray();
            arrayOfString2 = iMidiDeviceServer1.createStringArray();
            if (iMidiDeviceServer1.readInt() != 0) {
              Bundle bundle = Bundle.CREATOR.createFromParcel((Parcel)iMidiDeviceServer1);
            } else {
              str2 = null;
            } 
            i = iMidiDeviceServer1.readInt();
            midiDeviceInfo1 = registerDeviceServer(iMidiDeviceServer3, param1Int2, param1Int1, arrayOfString1, arrayOfString2, (Bundle)str2, i);
            param1Parcel2.writeNoException();
            if (midiDeviceInfo1 != null) {
              param1Parcel2.writeInt(1);
              midiDeviceInfo1.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 6:
            midiDeviceInfo1.enforceInterface("android.media.midi.IMidiManager");
            iBinder2 = midiDeviceInfo1.readStrongBinder();
            iBinder1 = midiDeviceInfo1.readStrongBinder();
            closeDevice(iBinder2, iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            iBinder1.enforceInterface("android.media.midi.IMidiManager");
            iBinder3 = iBinder1.readStrongBinder();
            if (iBinder1.readInt() != 0) {
              BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel((Parcel)iBinder1);
            } else {
              iBinder2 = null;
            } 
            iMidiDeviceOpenCallback = IMidiDeviceOpenCallback.Stub.asInterface(iBinder1.readStrongBinder());
            openBluetoothDevice(iBinder3, (BluetoothDevice)iBinder2, iMidiDeviceOpenCallback);
            param1Parcel2.writeNoException();
            return true;
          case 4:
            iMidiDeviceOpenCallback.enforceInterface("android.media.midi.IMidiManager");
            iBinder3 = iMidiDeviceOpenCallback.readStrongBinder();
            if (iMidiDeviceOpenCallback.readInt() != 0) {
              MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel((Parcel)iMidiDeviceOpenCallback);
            } else {
              iBinder2 = null;
            } 
            iMidiDeviceOpenCallback = IMidiDeviceOpenCallback.Stub.asInterface(iMidiDeviceOpenCallback.readStrongBinder());
            openDevice(iBinder3, (MidiDeviceInfo)iBinder2, iMidiDeviceOpenCallback);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            iMidiDeviceOpenCallback.enforceInterface("android.media.midi.IMidiManager");
            iBinder2 = iMidiDeviceOpenCallback.readStrongBinder();
            iMidiDeviceListener = IMidiDeviceListener.Stub.asInterface(iMidiDeviceOpenCallback.readStrongBinder());
            unregisterListener(iBinder2, iMidiDeviceListener);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iMidiDeviceListener.enforceInterface("android.media.midi.IMidiManager");
            iBinder2 = iMidiDeviceListener.readStrongBinder();
            iMidiDeviceListener = IMidiDeviceListener.Stub.asInterface(iMidiDeviceListener.readStrongBinder());
            registerListener(iBinder2, iMidiDeviceListener);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        iMidiDeviceListener.enforceInterface("android.media.midi.IMidiManager");
        MidiDeviceInfo[] arrayOfMidiDeviceInfo = getDevices();
        param1Parcel2.writeNoException();
        param1Parcel2.writeTypedArray(arrayOfMidiDeviceInfo, 1);
        return true;
      } 
      param1Parcel2.writeString("android.media.midi.IMidiManager");
      return true;
    }
    
    private static class Proxy implements IMidiManager {
      public static IMidiManager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.midi.IMidiManager";
      }
      
      public MidiDeviceInfo[] getDevices() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null)
            return IMidiManager.Stub.getDefaultImpl().getDevices(); 
          parcel2.readException();
          return parcel2.<MidiDeviceInfo>createTypedArray(MidiDeviceInfo.CREATOR);
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void registerListener(IBinder param2IBinder, IMidiDeviceListener param2IMidiDeviceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IMidiDeviceListener != null) {
            iBinder = param2IMidiDeviceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().registerListener(param2IBinder, param2IMidiDeviceListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void unregisterListener(IBinder param2IBinder, IMidiDeviceListener param2IMidiDeviceListener) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2IMidiDeviceListener != null) {
            iBinder = param2IMidiDeviceListener.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().unregisterListener(param2IBinder, param2IMidiDeviceListener);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void openDevice(IBinder param2IBinder, MidiDeviceInfo param2MidiDeviceInfo, IMidiDeviceOpenCallback param2IMidiDeviceOpenCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2MidiDeviceInfo != null) {
            parcel1.writeInt(1);
            param2MidiDeviceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IMidiDeviceOpenCallback != null) {
            iBinder = param2IMidiDeviceOpenCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().openDevice(param2IBinder, param2MidiDeviceInfo, param2IMidiDeviceOpenCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void openBluetoothDevice(IBinder param2IBinder, BluetoothDevice param2BluetoothDevice, IMidiDeviceOpenCallback param2IMidiDeviceOpenCallback) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeStrongBinder(param2IBinder);
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          if (param2IMidiDeviceOpenCallback != null) {
            iBinder = param2IMidiDeviceOpenCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().openBluetoothDevice(param2IBinder, param2BluetoothDevice, param2IMidiDeviceOpenCallback);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeDevice(IBinder param2IBinder1, IBinder param2IBinder2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeStrongBinder(param2IBinder1);
          parcel1.writeStrongBinder(param2IBinder2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().closeDevice(param2IBinder1, param2IBinder2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MidiDeviceInfo registerDeviceServer(IMidiDeviceServer param2IMidiDeviceServer, int param2Int1, int param2Int2, String[] param2ArrayOfString1, String[] param2ArrayOfString2, Bundle param2Bundle, int param2Int3) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          if (param2IMidiDeviceServer != null) {
            iBinder = param2IMidiDeviceServer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                parcel1.writeStringArray(param2ArrayOfString1);
                try {
                  parcel1.writeStringArray(param2ArrayOfString2);
                  if (param2Bundle != null) {
                    parcel1.writeInt(1);
                    param2Bundle.writeToParcel(parcel1, 0);
                  } else {
                    parcel1.writeInt(0);
                  } 
                  parcel1.writeInt(param2Int3);
                  boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
                  if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
                    MidiDeviceInfo midiDeviceInfo = IMidiManager.Stub.getDefaultImpl().registerDeviceServer(param2IMidiDeviceServer, param2Int1, param2Int2, param2ArrayOfString1, param2ArrayOfString2, param2Bundle, param2Int3);
                    parcel2.recycle();
                    parcel1.recycle();
                    return midiDeviceInfo;
                  } 
                  parcel2.readException();
                  if (parcel2.readInt() != 0) {
                    MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(parcel2);
                  } else {
                    param2IMidiDeviceServer = null;
                  } 
                  parcel2.recycle();
                  parcel1.recycle();
                  return (MidiDeviceInfo)param2IMidiDeviceServer;
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IMidiDeviceServer;
      }
      
      public void unregisterDeviceServer(IMidiDeviceServer param2IMidiDeviceServer) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          if (param2IMidiDeviceServer != null) {
            iBinder = param2IMidiDeviceServer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(8, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().unregisterDeviceServer(param2IMidiDeviceServer);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MidiDeviceInfo getServiceDeviceInfo(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(9, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null)
            return IMidiManager.Stub.getDefaultImpl().getServiceDeviceInfo(param2String1, param2String2); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            MidiDeviceInfo midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            param2String1 = null;
          } 
          return (MidiDeviceInfo)param2String1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MidiDeviceStatus getDeviceStatus(MidiDeviceInfo param2MidiDeviceInfo) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          if (param2MidiDeviceInfo != null) {
            parcel1.writeInt(1);
            param2MidiDeviceInfo.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(10, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null)
            return IMidiManager.Stub.getDefaultImpl().getDeviceStatus(param2MidiDeviceInfo); 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            MidiDeviceStatus midiDeviceStatus = MidiDeviceStatus.CREATOR.createFromParcel(parcel2);
          } else {
            param2MidiDeviceInfo = null;
          } 
          return (MidiDeviceStatus)param2MidiDeviceInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDeviceStatus(IMidiDeviceServer param2IMidiDeviceServer, MidiDeviceStatus param2MidiDeviceStatus) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel1.writeInterfaceToken("android.media.midi.IMidiManager");
          if (param2IMidiDeviceServer != null) {
            iBinder = param2IMidiDeviceServer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel1.writeStrongBinder(iBinder);
          if (param2MidiDeviceStatus != null) {
            parcel1.writeInt(1);
            param2MidiDeviceStatus.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(11, parcel1, parcel2, 0);
          if (!bool && IMidiManager.Stub.getDefaultImpl() != null) {
            IMidiManager.Stub.getDefaultImpl().setDeviceStatus(param2IMidiDeviceServer, param2MidiDeviceStatus);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMidiManager param1IMidiManager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMidiManager != null) {
          Proxy.sDefaultImpl = param1IMidiManager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMidiManager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
