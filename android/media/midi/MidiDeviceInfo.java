package android.media.midi;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

public final class MidiDeviceInfo implements Parcelable {
  class PortInfo {
    public static final int TYPE_INPUT = 1;
    
    public static final int TYPE_OUTPUT = 2;
    
    private final String mName;
    
    private final int mPortNumber;
    
    private final int mPortType;
    
    PortInfo(MidiDeviceInfo this$0, int param1Int1, String param1String) {
      this.mPortType = this$0;
      this.mPortNumber = param1Int1;
      if (param1String == null)
        param1String = ""; 
      this.mName = param1String;
    }
    
    public int getType() {
      return this.mPortType;
    }
    
    public int getPortNumber() {
      return this.mPortNumber;
    }
    
    public String getName() {
      return this.mName;
    }
  }
  
  public MidiDeviceInfo(int paramInt1, int paramInt2, int paramInt3, int paramInt4, String[] paramArrayOfString1, String[] paramArrayOfString2, Bundle paramBundle, boolean paramBoolean) {
    if (paramInt3 >= 0 && paramInt3 <= 256) {
      if (paramInt4 >= 0 && paramInt4 <= 256) {
        this.mType = paramInt1;
        this.mId = paramInt2;
        this.mInputPortCount = paramInt3;
        this.mOutputPortCount = paramInt4;
        if (paramArrayOfString1 == null) {
          this.mInputPortNames = new String[paramInt3];
        } else {
          this.mInputPortNames = paramArrayOfString1;
        } 
        if (paramArrayOfString2 == null) {
          this.mOutputPortNames = new String[paramInt4];
        } else {
          this.mOutputPortNames = paramArrayOfString2;
        } 
        this.mProperties = paramBundle;
        this.mIsPrivate = paramBoolean;
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("numOutputPorts out of range = ");
      stringBuilder1.append(paramInt4);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("numInputPorts out of range = ");
    stringBuilder.append(paramInt3);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getId() {
    return this.mId;
  }
  
  public int getInputPortCount() {
    return this.mInputPortCount;
  }
  
  public int getOutputPortCount() {
    return this.mOutputPortCount;
  }
  
  public PortInfo[] getPorts() {
    PortInfo[] arrayOfPortInfo = new PortInfo[this.mInputPortCount + this.mOutputPortCount];
    byte b1 = 0;
    byte b2;
    for (b2 = 0; b2 < this.mInputPortCount; b2++, b1++)
      arrayOfPortInfo[b1] = new PortInfo(1, b2, this.mInputPortNames[b2]); 
    for (b2 = 0; b2 < this.mOutputPortCount; b2++, b1++)
      arrayOfPortInfo[b1] = new PortInfo(2, b2, this.mOutputPortNames[b2]); 
    return arrayOfPortInfo;
  }
  
  public Bundle getProperties() {
    return this.mProperties;
  }
  
  public boolean isPrivate() {
    return this.mIsPrivate;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof MidiDeviceInfo;
    boolean bool1 = false;
    if (bool) {
      if (((MidiDeviceInfo)paramObject).mId == this.mId)
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public int hashCode() {
    return this.mId;
  }
  
  public String toString() {
    this.mProperties.getString("name");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MidiDeviceInfo[mType=");
    stringBuilder.append(this.mType);
    stringBuilder.append(",mInputPortCount=");
    stringBuilder.append(this.mInputPortCount);
    stringBuilder.append(",mOutputPortCount=");
    stringBuilder.append(this.mOutputPortCount);
    stringBuilder.append(",mProperties=");
    stringBuilder.append(this.mProperties);
    stringBuilder.append(",mIsPrivate=");
    stringBuilder.append(this.mIsPrivate);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<MidiDeviceInfo> CREATOR = new Parcelable.Creator<MidiDeviceInfo>() {
      public MidiDeviceInfo createFromParcel(Parcel param1Parcel) {
        boolean bool;
        int i = param1Parcel.readInt();
        int j = param1Parcel.readInt();
        int k = param1Parcel.readInt();
        int m = param1Parcel.readInt();
        String[] arrayOfString1 = param1Parcel.createStringArray();
        String[] arrayOfString2 = param1Parcel.createStringArray();
        if (param1Parcel.readInt() == 1) {
          bool = true;
        } else {
          bool = false;
        } 
        param1Parcel.readBundle();
        Bundle bundle = param1Parcel.readBundle();
        return new MidiDeviceInfo(i, j, k, m, arrayOfString1, arrayOfString2, bundle, bool);
      }
      
      public MidiDeviceInfo[] newArray(int param1Int) {
        return new MidiDeviceInfo[param1Int];
      }
    };
  
  public static final String PROPERTY_ALSA_CARD = "alsa_card";
  
  public static final String PROPERTY_ALSA_DEVICE = "alsa_device";
  
  public static final String PROPERTY_BLUETOOTH_DEVICE = "bluetooth_device";
  
  public static final String PROPERTY_MANUFACTURER = "manufacturer";
  
  public static final String PROPERTY_NAME = "name";
  
  public static final String PROPERTY_PRODUCT = "product";
  
  public static final String PROPERTY_SERIAL_NUMBER = "serial_number";
  
  public static final String PROPERTY_SERVICE_INFO = "service_info";
  
  public static final String PROPERTY_USB_DEVICE = "usb_device";
  
  public static final String PROPERTY_VERSION = "version";
  
  private static final String TAG = "MidiDeviceInfo";
  
  public static final int TYPE_BLUETOOTH = 3;
  
  public static final int TYPE_USB = 1;
  
  public static final int TYPE_VIRTUAL = 2;
  
  private final int mId;
  
  private final int mInputPortCount;
  
  private final String[] mInputPortNames;
  
  private final boolean mIsPrivate;
  
  private final int mOutputPortCount;
  
  private final String[] mOutputPortNames;
  
  private final Bundle mProperties;
  
  private final int mType;
  
  public int describeContents() {
    return 0;
  }
  
  private Bundle getBasicProperties(String[] paramArrayOfString) {
    Bundle bundle = new Bundle();
    int i;
    byte b;
    for (i = paramArrayOfString.length, b = 0; b < i; ) {
      String str = paramArrayOfString[b];
      Object object = this.mProperties.get(str);
      if (object != null)
        if (object instanceof String) {
          bundle.putString(str, (String)object);
        } else if (object instanceof Integer) {
          bundle.putInt(str, ((Integer)object).intValue());
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unsupported property type: ");
          stringBuilder.append(object.getClass().getName());
          Log.w("MidiDeviceInfo", stringBuilder.toString());
        }  
      b++;
    } 
    return bundle;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mType);
    paramParcel.writeInt(this.mId);
    paramParcel.writeInt(this.mInputPortCount);
    paramParcel.writeInt(this.mOutputPortCount);
    paramParcel.writeStringArray(this.mInputPortNames);
    paramParcel.writeStringArray(this.mOutputPortNames);
    paramParcel.writeInt(this.mIsPrivate);
    paramParcel.writeBundle(getBasicProperties(new String[] { "name", "manufacturer", "product", "version", "serial_number", "alsa_card", "alsa_device" }));
    paramParcel.writeBundle(this.mProperties);
  }
}
