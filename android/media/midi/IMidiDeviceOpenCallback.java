package android.media.midi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMidiDeviceOpenCallback extends IInterface {
  void onDeviceOpened(IMidiDeviceServer paramIMidiDeviceServer, IBinder paramIBinder) throws RemoteException;
  
  class Default implements IMidiDeviceOpenCallback {
    public void onDeviceOpened(IMidiDeviceServer param1IMidiDeviceServer, IBinder param1IBinder) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMidiDeviceOpenCallback {
    private static final String DESCRIPTOR = "android.media.midi.IMidiDeviceOpenCallback";
    
    static final int TRANSACTION_onDeviceOpened = 1;
    
    public Stub() {
      attachInterface(this, "android.media.midi.IMidiDeviceOpenCallback");
    }
    
    public static IMidiDeviceOpenCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.midi.IMidiDeviceOpenCallback");
      if (iInterface != null && iInterface instanceof IMidiDeviceOpenCallback)
        return (IMidiDeviceOpenCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "onDeviceOpened";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.midi.IMidiDeviceOpenCallback");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceOpenCallback");
      IMidiDeviceServer iMidiDeviceServer = IMidiDeviceServer.Stub.asInterface(param1Parcel1.readStrongBinder());
      IBinder iBinder = param1Parcel1.readStrongBinder();
      onDeviceOpened(iMidiDeviceServer, iBinder);
      return true;
    }
    
    private static class Proxy implements IMidiDeviceOpenCallback {
      public static IMidiDeviceOpenCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.midi.IMidiDeviceOpenCallback";
      }
      
      public void onDeviceOpened(IMidiDeviceServer param2IMidiDeviceServer, IBinder param2IBinder) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceOpenCallback");
          if (param2IMidiDeviceServer != null) {
            iBinder = param2IMidiDeviceServer.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          parcel.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceOpenCallback.Stub.getDefaultImpl() != null) {
            IMidiDeviceOpenCallback.Stub.getDefaultImpl().onDeviceOpened(param2IMidiDeviceServer, param2IBinder);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMidiDeviceOpenCallback param1IMidiDeviceOpenCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMidiDeviceOpenCallback != null) {
          Proxy.sDefaultImpl = param1IMidiDeviceOpenCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMidiDeviceOpenCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
