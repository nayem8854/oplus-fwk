package android.media.midi;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;

public abstract class MidiDeviceService extends Service {
  public static final String SERVICE_INTERFACE = "android.media.midi.MidiDeviceService";
  
  private static final String TAG = "MidiDeviceService";
  
  private final MidiDeviceServer.Callback mCallback = (MidiDeviceServer.Callback)new Object(this);
  
  private MidiDeviceInfo mDeviceInfo;
  
  private IMidiManager mMidiManager;
  
  private MidiDeviceServer mServer;
  
  public void onCreate() {
    IBinder iBinder = ServiceManager.getService("midi");
    IMidiManager iMidiManager = IMidiManager.Stub.asInterface(iBinder);
    try {
      String str2 = getPackageName();
      String str1 = getClass().getName();
      MidiDeviceInfo midiDeviceInfo = iMidiManager.getServiceDeviceInfo(str2, str1);
      if (midiDeviceInfo == null) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Could not find MidiDeviceInfo for MidiDeviceService ");
        stringBuilder.append(this);
        Log.e("MidiDeviceService", stringBuilder.toString());
        return;
      } 
      this.mDeviceInfo = midiDeviceInfo;
      MidiReceiver[] arrayOfMidiReceiver2 = onGetInputPortReceivers();
      MidiReceiver[] arrayOfMidiReceiver1 = arrayOfMidiReceiver2;
      if (arrayOfMidiReceiver2 == null)
        arrayOfMidiReceiver1 = new MidiReceiver[0]; 
      MidiDeviceServer midiDeviceServer = new MidiDeviceServer(this.mMidiManager, arrayOfMidiReceiver1, midiDeviceInfo, this.mCallback);
    } catch (RemoteException remoteException) {
      Log.e("MidiDeviceService", "RemoteException in IMidiManager.getServiceDeviceInfo");
      remoteException = null;
    } 
    this.mServer = (MidiDeviceServer)remoteException;
  }
  
  public final MidiReceiver[] getOutputPortReceivers() {
    MidiDeviceServer midiDeviceServer = this.mServer;
    if (midiDeviceServer == null)
      return null; 
    return midiDeviceServer.getOutputPortReceivers();
  }
  
  public final MidiDeviceInfo getDeviceInfo() {
    return this.mDeviceInfo;
  }
  
  public void onDeviceStatusChanged(MidiDeviceStatus paramMidiDeviceStatus) {}
  
  public void onClose() {}
  
  public IBinder onBind(Intent paramIntent) {
    if ("android.media.midi.MidiDeviceService".equals(paramIntent.getAction())) {
      MidiDeviceServer midiDeviceServer = this.mServer;
      if (midiDeviceServer != null)
        return midiDeviceServer.getBinderInterface().asBinder(); 
    } 
    return null;
  }
  
  public abstract MidiReceiver[] onGetInputPortReceivers();
}
