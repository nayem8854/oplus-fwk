package android.media.midi;

import android.bluetooth.BluetoothDevice;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IBluetoothMidiService extends IInterface {
  IBinder addBluetoothDevice(BluetoothDevice paramBluetoothDevice) throws RemoteException;
  
  class Default implements IBluetoothMidiService {
    public IBinder addBluetoothDevice(BluetoothDevice param1BluetoothDevice) throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IBluetoothMidiService {
    private static final String DESCRIPTOR = "android.media.midi.IBluetoothMidiService";
    
    static final int TRANSACTION_addBluetoothDevice = 1;
    
    public Stub() {
      attachInterface(this, "android.media.midi.IBluetoothMidiService");
    }
    
    public static IBluetoothMidiService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.midi.IBluetoothMidiService");
      if (iInterface != null && iInterface instanceof IBluetoothMidiService)
        return (IBluetoothMidiService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "addBluetoothDevice";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.midi.IBluetoothMidiService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.midi.IBluetoothMidiService");
      if (param1Parcel1.readInt() != 0) {
        BluetoothDevice bluetoothDevice = BluetoothDevice.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      IBinder iBinder = addBluetoothDevice((BluetoothDevice)param1Parcel1);
      param1Parcel2.writeNoException();
      param1Parcel2.writeStrongBinder(iBinder);
      return true;
    }
    
    private static class Proxy implements IBluetoothMidiService {
      public static IBluetoothMidiService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.midi.IBluetoothMidiService";
      }
      
      public IBinder addBluetoothDevice(BluetoothDevice param2BluetoothDevice) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IBluetoothMidiService");
          if (param2BluetoothDevice != null) {
            parcel1.writeInt(1);
            param2BluetoothDevice.writeToParcel(parcel1, 0);
          } else {
            parcel1.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IBluetoothMidiService.Stub.getDefaultImpl() != null)
            return IBluetoothMidiService.Stub.getDefaultImpl().addBluetoothDevice(param2BluetoothDevice); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IBluetoothMidiService param1IBluetoothMidiService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IBluetoothMidiService != null) {
          Proxy.sDefaultImpl = param1IBluetoothMidiService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IBluetoothMidiService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
