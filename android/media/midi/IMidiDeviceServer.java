package android.media.midi;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.io.FileDescriptor;

public interface IMidiDeviceServer extends IInterface {
  void closeDevice() throws RemoteException;
  
  void closePort(IBinder paramIBinder) throws RemoteException;
  
  int connectPorts(IBinder paramIBinder, FileDescriptor paramFileDescriptor, int paramInt) throws RemoteException;
  
  MidiDeviceInfo getDeviceInfo() throws RemoteException;
  
  FileDescriptor openInputPort(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  FileDescriptor openOutputPort(IBinder paramIBinder, int paramInt) throws RemoteException;
  
  void setDeviceInfo(MidiDeviceInfo paramMidiDeviceInfo) throws RemoteException;
  
  class Default implements IMidiDeviceServer {
    public FileDescriptor openInputPort(IBinder param1IBinder, int param1Int) throws RemoteException {
      return null;
    }
    
    public FileDescriptor openOutputPort(IBinder param1IBinder, int param1Int) throws RemoteException {
      return null;
    }
    
    public void closePort(IBinder param1IBinder) throws RemoteException {}
    
    public void closeDevice() throws RemoteException {}
    
    public int connectPorts(IBinder param1IBinder, FileDescriptor param1FileDescriptor, int param1Int) throws RemoteException {
      return 0;
    }
    
    public MidiDeviceInfo getDeviceInfo() throws RemoteException {
      return null;
    }
    
    public void setDeviceInfo(MidiDeviceInfo param1MidiDeviceInfo) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMidiDeviceServer {
    private static final String DESCRIPTOR = "android.media.midi.IMidiDeviceServer";
    
    static final int TRANSACTION_closeDevice = 4;
    
    static final int TRANSACTION_closePort = 3;
    
    static final int TRANSACTION_connectPorts = 5;
    
    static final int TRANSACTION_getDeviceInfo = 6;
    
    static final int TRANSACTION_openInputPort = 1;
    
    static final int TRANSACTION_openOutputPort = 2;
    
    static final int TRANSACTION_setDeviceInfo = 7;
    
    public Stub() {
      attachInterface(this, "android.media.midi.IMidiDeviceServer");
    }
    
    public static IMidiDeviceServer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.midi.IMidiDeviceServer");
      if (iInterface != null && iInterface instanceof IMidiDeviceServer)
        return (IMidiDeviceServer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "setDeviceInfo";
        case 6:
          return "getDeviceInfo";
        case 5:
          return "connectPorts";
        case 4:
          return "closeDevice";
        case 3:
          return "closePort";
        case 2:
          return "openOutputPort";
        case 1:
          break;
      } 
      return "openInputPort";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        MidiDeviceInfo midiDeviceInfo;
        IBinder iBinder1;
        FileDescriptor fileDescriptor2;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceServer");
            if (param1Parcel1.readInt() != 0) {
              MidiDeviceInfo midiDeviceInfo1 = MidiDeviceInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            setDeviceInfo((MidiDeviceInfo)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.midi.IMidiDeviceServer");
            midiDeviceInfo = getDeviceInfo();
            param1Parcel2.writeNoException();
            if (midiDeviceInfo != null) {
              param1Parcel2.writeInt(1);
              midiDeviceInfo.writeToParcel(param1Parcel2, 1);
            } else {
              param1Parcel2.writeInt(0);
            } 
            return true;
          case 5:
            midiDeviceInfo.enforceInterface("android.media.midi.IMidiDeviceServer");
            iBinder2 = midiDeviceInfo.readStrongBinder();
            fileDescriptor2 = midiDeviceInfo.readRawFileDescriptor();
            param1Int1 = midiDeviceInfo.readInt();
            param1Int1 = connectPorts(iBinder2, fileDescriptor2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 4:
            midiDeviceInfo.enforceInterface("android.media.midi.IMidiDeviceServer");
            closeDevice();
            return true;
          case 3:
            midiDeviceInfo.enforceInterface("android.media.midi.IMidiDeviceServer");
            iBinder1 = midiDeviceInfo.readStrongBinder();
            closePort(iBinder1);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            iBinder1.enforceInterface("android.media.midi.IMidiDeviceServer");
            iBinder2 = iBinder1.readStrongBinder();
            param1Int1 = iBinder1.readInt();
            fileDescriptor1 = openOutputPort(iBinder2, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeRawFileDescriptor(fileDescriptor1);
            return true;
          case 1:
            break;
        } 
        fileDescriptor1.enforceInterface("android.media.midi.IMidiDeviceServer");
        IBinder iBinder2 = fileDescriptor1.readStrongBinder();
        param1Int1 = fileDescriptor1.readInt();
        FileDescriptor fileDescriptor1 = openInputPort(iBinder2, param1Int1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeRawFileDescriptor(fileDescriptor1);
        return true;
      } 
      param1Parcel2.writeString("android.media.midi.IMidiDeviceServer");
      return true;
    }
    
    private static class Proxy implements IMidiDeviceServer {
      public static IMidiDeviceServer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.midi.IMidiDeviceServer";
      }
      
      public FileDescriptor openInputPort(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null)
            return IMidiDeviceServer.Stub.getDefaultImpl().openInputPort(param2IBinder, param2Int); 
          parcel2.readException();
          return parcel2.readRawFileDescriptor();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public FileDescriptor openOutputPort(IBinder param2IBinder, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null)
            return IMidiDeviceServer.Stub.getDefaultImpl().openOutputPort(param2IBinder, param2Int); 
          parcel2.readException();
          return parcel2.readRawFileDescriptor();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closePort(IBinder param2IBinder) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          parcel1.writeStrongBinder(param2IBinder);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null) {
            IMidiDeviceServer.Stub.getDefaultImpl().closePort(param2IBinder);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void closeDevice() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null) {
            IMidiDeviceServer.Stub.getDefaultImpl().closeDevice();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public int connectPorts(IBinder param2IBinder, FileDescriptor param2FileDescriptor, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          parcel1.writeStrongBinder(param2IBinder);
          parcel1.writeRawFileDescriptor(param2FileDescriptor);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null) {
            param2Int = IMidiDeviceServer.Stub.getDefaultImpl().connectPorts(param2IBinder, param2FileDescriptor, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public MidiDeviceInfo getDeviceInfo() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          MidiDeviceInfo midiDeviceInfo;
          parcel1.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null) {
            midiDeviceInfo = IMidiDeviceServer.Stub.getDefaultImpl().getDeviceInfo();
            return midiDeviceInfo;
          } 
          parcel2.readException();
          if (parcel2.readInt() != 0) {
            midiDeviceInfo = MidiDeviceInfo.CREATOR.createFromParcel(parcel2);
          } else {
            midiDeviceInfo = null;
          } 
          return midiDeviceInfo;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void setDeviceInfo(MidiDeviceInfo param2MidiDeviceInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.midi.IMidiDeviceServer");
          if (param2MidiDeviceInfo != null) {
            parcel.writeInt(1);
            param2MidiDeviceInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IMidiDeviceServer.Stub.getDefaultImpl() != null) {
            IMidiDeviceServer.Stub.getDefaultImpl().setDeviceInfo(param2MidiDeviceInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMidiDeviceServer param1IMidiDeviceServer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMidiDeviceServer != null) {
          Proxy.sDefaultImpl = param1IMidiDeviceServer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMidiDeviceServer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
