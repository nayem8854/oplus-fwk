package android.media.midi;

import android.os.Parcel;
import android.os.Parcelable;

public final class MidiDeviceStatus implements Parcelable {
  public MidiDeviceStatus(MidiDeviceInfo paramMidiDeviceInfo, boolean[] paramArrayOfboolean, int[] paramArrayOfint) {
    this.mDeviceInfo = paramMidiDeviceInfo;
    boolean[] arrayOfBoolean = new boolean[paramArrayOfboolean.length];
    System.arraycopy(paramArrayOfboolean, 0, arrayOfBoolean, 0, paramArrayOfboolean.length);
    int[] arrayOfInt = new int[paramArrayOfint.length];
    System.arraycopy(paramArrayOfint, 0, arrayOfInt, 0, paramArrayOfint.length);
  }
  
  public MidiDeviceStatus(MidiDeviceInfo paramMidiDeviceInfo) {
    this.mDeviceInfo = paramMidiDeviceInfo;
    this.mInputPortOpen = new boolean[paramMidiDeviceInfo.getInputPortCount()];
    this.mOutputPortOpenCount = new int[paramMidiDeviceInfo.getOutputPortCount()];
  }
  
  public MidiDeviceInfo getDeviceInfo() {
    return this.mDeviceInfo;
  }
  
  public boolean isInputPortOpen(int paramInt) {
    return this.mInputPortOpen[paramInt];
  }
  
  public int getOutputPortOpenCount(int paramInt) {
    return this.mOutputPortOpenCount[paramInt];
  }
  
  public String toString() {
    int i = this.mDeviceInfo.getInputPortCount();
    int j = this.mDeviceInfo.getOutputPortCount();
    StringBuilder stringBuilder = new StringBuilder("mInputPortOpen=[");
    byte b;
    for (b = 0; b < i; b++) {
      stringBuilder.append(this.mInputPortOpen[b]);
      if (b < i - 1)
        stringBuilder.append(","); 
    } 
    stringBuilder.append("] mOutputPortOpenCount=[");
    for (b = 0; b < j; b++) {
      stringBuilder.append(this.mOutputPortOpenCount[b]);
      if (b < j - 1)
        stringBuilder.append(","); 
    } 
    stringBuilder.append("]");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<MidiDeviceStatus> CREATOR = new Parcelable.Creator<MidiDeviceStatus>() {
      public MidiDeviceStatus createFromParcel(Parcel param1Parcel) {
        ClassLoader classLoader = MidiDeviceInfo.class.getClassLoader();
        MidiDeviceInfo midiDeviceInfo = param1Parcel.<MidiDeviceInfo>readParcelable(classLoader);
        boolean[] arrayOfBoolean = param1Parcel.createBooleanArray();
        int[] arrayOfInt = param1Parcel.createIntArray();
        return new MidiDeviceStatus(midiDeviceInfo, arrayOfBoolean, arrayOfInt);
      }
      
      public MidiDeviceStatus[] newArray(int param1Int) {
        return new MidiDeviceStatus[param1Int];
      }
    };
  
  private static final String TAG = "MidiDeviceStatus";
  
  private final MidiDeviceInfo mDeviceInfo;
  
  private final boolean[] mInputPortOpen;
  
  private final int[] mOutputPortOpenCount;
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeParcelable(this.mDeviceInfo, paramInt);
    paramParcel.writeBooleanArray(this.mInputPortOpen);
    paramParcel.writeIntArray(this.mOutputPortOpenCount);
  }
}
