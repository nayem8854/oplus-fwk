package android.media.midi;

import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;
import libcore.io.IoUtils;

public final class MidiDevice implements Closeable {
  private static final String TAG = "MidiDevice";
  
  private final IBinder mClientToken;
  
  private final MidiDeviceInfo mDeviceInfo;
  
  private final IMidiDeviceServer mDeviceServer;
  
  private final IBinder mDeviceServerBinder;
  
  private final IBinder mDeviceToken;
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private boolean mIsDeviceClosed;
  
  private final IMidiManager mMidiManager;
  
  private long mNativeHandle;
  
  public class MidiConnection implements Closeable {
    private final CloseGuard mGuard = CloseGuard.get();
    
    private final IMidiDeviceServer mInputPortDeviceServer;
    
    private final IBinder mInputPortToken;
    
    private boolean mIsClosed;
    
    private final IBinder mOutputPortToken;
    
    final MidiDevice this$0;
    
    MidiConnection(IBinder param1IBinder, MidiInputPort param1MidiInputPort) {
      this.mInputPortDeviceServer = param1MidiInputPort.getDeviceServer();
      this.mInputPortToken = param1MidiInputPort.getToken();
      this.mOutputPortToken = param1IBinder;
      this.mGuard.open("close");
    }
    
    public void close() throws IOException {
      synchronized (this.mGuard) {
        if (this.mIsClosed)
          return; 
        this.mGuard.close();
        try {
          this.mInputPortDeviceServer.closePort(this.mInputPortToken);
          MidiDevice.this.mDeviceServer.closePort(this.mOutputPortToken);
        } catch (RemoteException remoteException) {
          Log.e("MidiDevice", "RemoteException in MidiConnection.close");
        } 
        this.mIsClosed = true;
        return;
      } 
    }
    
    protected void finalize() throws Throwable {
      try {
        if (this.mGuard != null)
          this.mGuard.warnIfOpen(); 
        close();
        return;
      } finally {
        super.finalize();
      } 
    }
  }
  
  MidiDevice(MidiDeviceInfo paramMidiDeviceInfo, IMidiDeviceServer paramIMidiDeviceServer, IMidiManager paramIMidiManager, IBinder paramIBinder1, IBinder paramIBinder2) {
    this.mDeviceInfo = paramMidiDeviceInfo;
    this.mDeviceServer = paramIMidiDeviceServer;
    this.mDeviceServerBinder = paramIMidiDeviceServer.asBinder();
    this.mMidiManager = paramIMidiManager;
    this.mClientToken = paramIBinder1;
    this.mDeviceToken = paramIBinder2;
    this.mGuard.open("close");
  }
  
  public MidiDeviceInfo getInfo() {
    return this.mDeviceInfo;
  }
  
  public MidiInputPort openInputPort(int paramInt) {
    if (this.mIsDeviceClosed)
      return null; 
    try {
      Binder binder = new Binder();
      this();
      FileDescriptor fileDescriptor = this.mDeviceServer.openInputPort(binder, paramInt);
      if (fileDescriptor == null)
        return null; 
      return new MidiInputPort(this.mDeviceServer, binder, fileDescriptor, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("MidiDevice", "RemoteException in openInputPort");
      return null;
    } 
  }
  
  public MidiOutputPort openOutputPort(int paramInt) {
    if (this.mIsDeviceClosed)
      return null; 
    try {
      Binder binder = new Binder();
      this();
      FileDescriptor fileDescriptor = this.mDeviceServer.openOutputPort(binder, paramInt);
      if (fileDescriptor == null)
        return null; 
      return new MidiOutputPort(this.mDeviceServer, binder, fileDescriptor, paramInt);
    } catch (RemoteException remoteException) {
      Log.e("MidiDevice", "RemoteException in openOutputPort");
      return null;
    } 
  }
  
  public MidiConnection connectPorts(MidiInputPort paramMidiInputPort, int paramInt) {
    if (paramInt >= 0 && paramInt < this.mDeviceInfo.getOutputPortCount()) {
      if (this.mIsDeviceClosed)
        return null; 
      FileDescriptor fileDescriptor = paramMidiInputPort.claimFileDescriptor();
      if (fileDescriptor == null)
        return null; 
      try {
        Binder binder = new Binder();
        this();
        paramInt = this.mDeviceServer.connectPorts(binder, fileDescriptor, paramInt);
        if (paramInt != Process.myPid())
          IoUtils.closeQuietly(fileDescriptor); 
        return new MidiConnection(binder, paramMidiInputPort);
      } catch (RemoteException remoteException) {
        Log.e("MidiDevice", "RemoteException in connectPorts");
        return null;
      } 
    } 
    throw new IllegalArgumentException("outputPortNumber out of range");
  }
  
  public void close() throws IOException {
    synchronized (this.mGuard) {
      if (this.mNativeHandle != 0L) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("MidiDevice#close() called while there is an outstanding native client 0x");
        long l = this.mNativeHandle;
        stringBuilder.append(Long.toHexString(l));
        String str = stringBuilder.toString();
        Log.w("MidiDevice", str);
      } 
      if (!this.mIsDeviceClosed && this.mNativeHandle == 0L) {
        this.mGuard.close();
        this.mIsDeviceClosed = true;
        try {
          this.mMidiManager.closeDevice(this.mClientToken, this.mDeviceToken);
        } catch (RemoteException remoteException) {
          Log.e("MidiDevice", "RemoteException in closeDevice");
        } 
      } 
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null)
        this.mGuard.warnIfOpen(); 
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MidiDevice: ");
    stringBuilder.append(this.mDeviceInfo.toString());
    return stringBuilder.toString();
  }
}
