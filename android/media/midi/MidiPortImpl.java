package android.media.midi;

class MidiPortImpl {
  private static final int DATA_PACKET_OVERHEAD = 9;
  
  public static final int MAX_PACKET_DATA_SIZE = 1015;
  
  public static final int MAX_PACKET_SIZE = 1024;
  
  public static final int PACKET_TYPE_DATA = 1;
  
  public static final int PACKET_TYPE_FLUSH = 2;
  
  private static final String TAG = "MidiPort";
  
  private static final int TIMESTAMP_SIZE = 8;
  
  public static int packData(byte[] paramArrayOfbyte1, int paramInt1, int paramInt2, long paramLong, byte[] paramArrayOfbyte2) {
    int i = paramInt2;
    if (paramInt2 > 1015)
      i = 1015; 
    paramInt2 = 0 + 1;
    paramArrayOfbyte2[0] = 1;
    System.arraycopy(paramArrayOfbyte1, paramInt1, paramArrayOfbyte2, paramInt2, i);
    paramInt1 = paramInt2 + i;
    for (paramInt2 = 0; paramInt2 < 8; paramInt2++, paramInt1++) {
      paramArrayOfbyte2[paramInt1] = (byte)(int)paramLong;
      paramLong >>= 8L;
    } 
    return paramInt1;
  }
  
  public static int packFlush(byte[] paramArrayOfbyte) {
    paramArrayOfbyte[0] = 2;
    return 1;
  }
  
  public static int getPacketType(byte[] paramArrayOfbyte, int paramInt) {
    return paramArrayOfbyte[0];
  }
  
  public static int getDataOffset(byte[] paramArrayOfbyte, int paramInt) {
    return 1;
  }
  
  public static int getDataSize(byte[] paramArrayOfbyte, int paramInt) {
    return paramInt - 9;
  }
  
  public static long getPacketTimestamp(byte[] paramArrayOfbyte, int paramInt) {
    int i = paramInt;
    long l = 0L;
    for (paramInt = 0; paramInt < 8; paramInt++) {
      byte b = paramArrayOfbyte[--i];
      l = l << 8L | (b & 0xFF);
    } 
    return l;
  }
}
