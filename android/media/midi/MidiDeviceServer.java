package android.media.midi;

import android.os.Binder;
import android.os.IBinder;
import android.os.RemoteException;
import android.system.ErrnoException;
import android.system.Os;
import android.system.OsConstants;
import android.util.Log;
import com.android.internal.midi.MidiDispatcher;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import libcore.io.IoUtils;

public final class MidiDeviceServer implements Closeable {
  private final CopyOnWriteArrayList<MidiInputPort> mInputPorts = new CopyOnWriteArrayList<>();
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private final HashMap<IBinder, PortClient> mPortClients = new HashMap<>();
  
  private final HashMap<MidiInputPort, PortClient> mInputPortClients = new HashMap<>();
  
  class PortClient implements IBinder.DeathRecipient {
    final IBinder mToken;
    
    final MidiDeviceServer this$0;
    
    PortClient(IBinder param1IBinder) {
      this.mToken = param1IBinder;
      try {
        param1IBinder.linkToDeath(this, 0);
      } catch (RemoteException remoteException) {
        close();
      } 
    }
    
    MidiInputPort getInputPort() {
      return null;
    }
    
    public void binderDied() {
      close();
    }
    
    abstract void close();
  }
  
  class InputPortClient extends PortClient {
    private final MidiOutputPort mOutputPort;
    
    final MidiDeviceServer this$0;
    
    InputPortClient(IBinder param1IBinder, MidiOutputPort param1MidiOutputPort) {
      super(param1IBinder);
      this.mOutputPort = param1MidiOutputPort;
    }
    
    void close() {
      this.mToken.unlinkToDeath(this, 0);
      synchronized (MidiDeviceServer.this.mInputPortOutputPorts) {
        int i = this.mOutputPort.getPortNumber();
        MidiDeviceServer.this.mInputPortOutputPorts[i] = null;
        MidiDeviceServer.this.mInputPortOpen[i] = false;
        MidiDeviceServer.this.updateDeviceStatus();
        IoUtils.closeQuietly(this.mOutputPort);
        return;
      } 
    }
  }
  
  class OutputPortClient extends PortClient {
    private final MidiInputPort mInputPort;
    
    final MidiDeviceServer this$0;
    
    OutputPortClient(IBinder param1IBinder, MidiInputPort param1MidiInputPort) {
      super(param1IBinder);
      this.mInputPort = param1MidiInputPort;
    }
    
    void close() {
      this.mToken.unlinkToDeath(this, 0);
      int i = this.mInputPort.getPortNumber();
      synchronized (MidiDeviceServer.this.mOutputPortDispatchers[i]) {
        null.getSender().disconnect(this.mInputPort);
        int j = null.getReceiverCount();
        MidiDeviceServer.this.mOutputPortOpenCount[i] = j;
        MidiDeviceServer.this.updateDeviceStatus();
        MidiDeviceServer.this.mInputPorts.remove(this.mInputPort);
        IoUtils.closeQuietly(this.mInputPort);
        return;
      } 
    }
    
    MidiInputPort getInputPort() {
      return this.mInputPort;
    }
  }
  
  private static FileDescriptor[] createSeqPacketSocketPair() throws IOException {
    try {
      FileDescriptor fileDescriptor1 = new FileDescriptor();
      this();
      FileDescriptor fileDescriptor2 = new FileDescriptor();
      this();
      Os.socketpair(OsConstants.AF_UNIX, OsConstants.SOCK_SEQPACKET, 0, fileDescriptor1, fileDescriptor2);
      return new FileDescriptor[] { fileDescriptor1, fileDescriptor2 };
    } catch (ErrnoException errnoException) {
      throw errnoException.rethrowAsIOException();
    } 
  }
  
  private final IMidiDeviceServer mServer = (IMidiDeviceServer)new Object(this);
  
  private static final String TAG = "MidiDeviceServer";
  
  private final Callback mCallback;
  
  private MidiDeviceInfo mDeviceInfo;
  
  private final int mInputPortCount;
  
  private final MidiDispatcher.MidiReceiverFailureHandler mInputPortFailureHandler;
  
  private final boolean[] mInputPortOpen;
  
  private final MidiOutputPort[] mInputPortOutputPorts;
  
  private final MidiReceiver[] mInputPortReceivers;
  
  private boolean mIsClosed;
  
  private final IMidiManager mMidiManager;
  
  private final int mOutputPortCount;
  
  private MidiDispatcher[] mOutputPortDispatchers;
  
  private final int[] mOutputPortOpenCount;
  
  MidiDeviceServer(IMidiManager paramIMidiManager, MidiReceiver[] paramArrayOfMidiReceiver, int paramInt, Callback paramCallback) {
    this.mInputPortFailureHandler = (MidiDispatcher.MidiReceiverFailureHandler)new Object(this);
    this.mMidiManager = paramIMidiManager;
    this.mInputPortReceivers = paramArrayOfMidiReceiver;
    int i = paramArrayOfMidiReceiver.length;
    this.mOutputPortCount = paramInt;
    this.mCallback = paramCallback;
    this.mInputPortOutputPorts = new MidiOutputPort[i];
    this.mOutputPortDispatchers = new MidiDispatcher[paramInt];
    for (i = 0; i < paramInt; i++)
      this.mOutputPortDispatchers[i] = new MidiDispatcher(this.mInputPortFailureHandler); 
    this.mInputPortOpen = new boolean[this.mInputPortCount];
    this.mOutputPortOpenCount = new int[paramInt];
    this.mGuard.open("close");
  }
  
  MidiDeviceServer(IMidiManager paramIMidiManager, MidiReceiver[] paramArrayOfMidiReceiver, MidiDeviceInfo paramMidiDeviceInfo, Callback paramCallback) {
    this(paramIMidiManager, paramArrayOfMidiReceiver, paramMidiDeviceInfo.getOutputPortCount(), paramCallback);
    this.mDeviceInfo = paramMidiDeviceInfo;
  }
  
  IMidiDeviceServer getBinderInterface() {
    return this.mServer;
  }
  
  public IBinder asBinder() {
    return this.mServer.asBinder();
  }
  
  private void updateDeviceStatus() {
    long l = Binder.clearCallingIdentity();
    MidiDeviceStatus midiDeviceStatus = new MidiDeviceStatus(this.mDeviceInfo, this.mInputPortOpen, this.mOutputPortOpenCount);
    Callback callback = this.mCallback;
    if (callback != null)
      callback.onDeviceStatusChanged(this, midiDeviceStatus); 
    try {
      this.mMidiManager.setDeviceStatus(this.mServer, midiDeviceStatus);
      Binder.restoreCallingIdentity(l);
    } catch (RemoteException remoteException) {
      Log.e("MidiDeviceServer", "RemoteException in updateDeviceStatus");
      Binder.restoreCallingIdentity(l);
    } finally {}
  }
  
  public void close() throws IOException {
    synchronized (this.mGuard) {
      if (this.mIsClosed)
        return; 
      this.mGuard.close();
      for (byte b = 0; b < this.mInputPortCount; b++) {
        MidiOutputPort midiOutputPort = this.mInputPortOutputPorts[b];
        if (midiOutputPort != null) {
          IoUtils.closeQuietly(midiOutputPort);
          this.mInputPortOutputPorts[b] = null;
        } 
      } 
      for (MidiInputPort midiInputPort : this.mInputPorts)
        IoUtils.closeQuietly(midiInputPort); 
      this.mInputPorts.clear();
      try {
        this.mMidiManager.unregisterDeviceServer(this.mServer);
      } catch (RemoteException remoteException) {
        Log.e("MidiDeviceServer", "RemoteException in unregisterDeviceServer");
      } 
      this.mIsClosed = true;
      return;
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null)
        this.mGuard.warnIfOpen(); 
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public MidiReceiver[] getOutputPortReceivers() {
    int i = this.mOutputPortCount;
    MidiReceiver[] arrayOfMidiReceiver = new MidiReceiver[i];
    System.arraycopy(this.mOutputPortDispatchers, 0, arrayOfMidiReceiver, 0, i);
    return arrayOfMidiReceiver;
  }
  
  public static interface Callback {
    void onClose();
    
    void onDeviceStatusChanged(MidiDeviceServer param1MidiDeviceServer, MidiDeviceStatus param1MidiDeviceStatus);
  }
}
