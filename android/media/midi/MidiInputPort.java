package android.media.midi;

import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.io.Closeable;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import libcore.io.IoUtils;

public final class MidiInputPort extends MidiReceiver implements Closeable {
  private final IBinder mToken;
  
  private final int mPortNumber;
  
  private FileOutputStream mOutputStream;
  
  private boolean mIsClosed;
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private FileDescriptor mFileDescriptor;
  
  private IMidiDeviceServer mDeviceServer;
  
  private final byte[] mBuffer = new byte[1024];
  
  private static final String TAG = "MidiInputPort";
  
  MidiInputPort(IMidiDeviceServer paramIMidiDeviceServer, IBinder paramIBinder, FileDescriptor paramFileDescriptor, int paramInt) {
    super(1015);
    this.mDeviceServer = paramIMidiDeviceServer;
    this.mToken = paramIBinder;
    this.mFileDescriptor = paramFileDescriptor;
    this.mPortNumber = paramInt;
    this.mOutputStream = new FileOutputStream(paramFileDescriptor);
    this.mGuard.open("close");
  }
  
  MidiInputPort(FileDescriptor paramFileDescriptor, int paramInt) {
    this(null, null, paramFileDescriptor, paramInt);
  }
  
  public final int getPortNumber() {
    return this.mPortNumber;
  }
  
  public void onSend(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, long paramLong) throws IOException {
    if (paramInt1 >= 0 && paramInt2 >= 0 && paramInt1 + paramInt2 <= paramArrayOfbyte.length) {
      if (paramInt2 <= 1015)
        synchronized (this.mBuffer) {
          if (this.mOutputStream != null) {
            paramInt1 = MidiPortImpl.packData(paramArrayOfbyte, paramInt1, paramInt2, paramLong, this.mBuffer);
            this.mOutputStream.write(this.mBuffer, 0, paramInt1);
            return;
          } 
          IOException iOException = new IOException();
          this("MidiInputPort is closed");
          throw iOException;
        }  
      throw new IllegalArgumentException("count exceeds max message size");
    } 
    throw new IllegalArgumentException("offset or count out of range");
  }
  
  public void onFlush() throws IOException {
    synchronized (this.mBuffer) {
      if (this.mOutputStream != null) {
        int i = MidiPortImpl.packFlush(this.mBuffer);
        this.mOutputStream.write(this.mBuffer, 0, i);
        return;
      } 
      IOException iOException = new IOException();
      this("MidiInputPort is closed");
      throw iOException;
    } 
  }
  
  FileDescriptor claimFileDescriptor() {
    synchronized (this.mGuard) {
      synchronized (this.mBuffer) {
        FileDescriptor fileDescriptor = this.mFileDescriptor;
        if (fileDescriptor == null)
          return null; 
        IoUtils.closeQuietly(this.mOutputStream);
        this.mFileDescriptor = null;
        this.mOutputStream = null;
        this.mIsClosed = true;
        return fileDescriptor;
      } 
    } 
  }
  
  IBinder getToken() {
    return this.mToken;
  }
  
  IMidiDeviceServer getDeviceServer() {
    return this.mDeviceServer;
  }
  
  public void close() throws IOException {
    synchronized (this.mGuard) {
      byte[] arrayOfByte;
      if (this.mIsClosed)
        return; 
      this.mGuard.close();
      synchronized (this.mBuffer) {
        if (this.mFileDescriptor != null) {
          IoUtils.closeQuietly(this.mFileDescriptor);
          this.mFileDescriptor = null;
        } 
        if (this.mOutputStream != null) {
          this.mOutputStream.close();
          this.mOutputStream = null;
        } 
        IMidiDeviceServer iMidiDeviceServer = this.mDeviceServer;
        if (iMidiDeviceServer != null)
          try {
            this.mDeviceServer.closePort(this.mToken);
          } catch (RemoteException remoteException) {
            Log.e("MidiInputPort", "RemoteException in MidiInputPort.close()");
          }  
        this.mIsClosed = true;
        return;
      } 
    } 
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mGuard != null)
        this.mGuard.warnIfOpen(); 
      this.mDeviceServer = null;
      close();
      return;
    } finally {
      super.finalize();
    } 
  }
}
