package android.media;

import android.content.ContentProviderClient;
import android.content.ContentValues;
import android.net.Uri;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MediaInserter {
  private final HashMap<Uri, List<ContentValues>> mRowMap = new HashMap<>();
  
  private final ContentProviderClient mProvider;
  
  private final HashMap<Uri, List<ContentValues>> mPriorityRowMap = new HashMap<>();
  
  private final int mBufferSizePerUri;
  
  public MediaInserter(ContentProviderClient paramContentProviderClient, int paramInt) {
    this.mProvider = paramContentProviderClient;
    this.mBufferSizePerUri = paramInt;
  }
  
  public void insert(Uri paramUri, ContentValues paramContentValues) throws RemoteException {
    insert(paramUri, paramContentValues, false);
  }
  
  public void insertwithPriority(Uri paramUri, ContentValues paramContentValues) throws RemoteException {
    insert(paramUri, paramContentValues, true);
  }
  
  private void insert(Uri paramUri, ContentValues paramContentValues, boolean paramBoolean) throws RemoteException {
    HashMap<Uri, List<ContentValues>> hashMap;
    if (paramBoolean) {
      hashMap = this.mPriorityRowMap;
    } else {
      hashMap = this.mRowMap;
    } 
    List<ContentValues> list1 = hashMap.get(paramUri);
    List<ContentValues> list2 = list1;
    if (list1 == null) {
      list2 = new ArrayList();
      hashMap.put(paramUri, list2);
    } 
    list2.add(new ContentValues(paramContentValues));
    if (list2.size() >= this.mBufferSizePerUri) {
      flushAllPriority();
      flush(paramUri, list2);
    } 
  }
  
  public void flushAll() throws RemoteException {
    flushAllPriority();
    for (Uri uri : this.mRowMap.keySet()) {
      List<ContentValues> list = this.mRowMap.get(uri);
      flush(uri, list);
    } 
    this.mRowMap.clear();
  }
  
  private void flushAllPriority() throws RemoteException {
    for (Uri uri : this.mPriorityRowMap.keySet()) {
      List<ContentValues> list = this.mPriorityRowMap.get(uri);
      flush(uri, list);
    } 
    this.mPriorityRowMap.clear();
  }
  
  private void flush(Uri paramUri, List<ContentValues> paramList) throws RemoteException {
    if (!paramList.isEmpty()) {
      ContentValues[] arrayOfContentValues = new ContentValues[paramList.size()];
      arrayOfContentValues = paramList.<ContentValues>toArray(arrayOfContentValues);
      this.mProvider.bulkInsert(paramUri, arrayOfContentValues);
      paramList.clear();
    } 
  }
}
