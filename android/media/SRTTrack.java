package android.media;

import android.os.Handler;
import android.os.Message;
import android.os.Parcel;
import android.util.Log;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Vector;

class SRTTrack extends WebVttTrack {
  private static final int KEY_LOCAL_SETTING = 102;
  
  private static final int KEY_START_TIME = 7;
  
  private static final int KEY_STRUCT_TEXT = 16;
  
  private static final int MEDIA_TIMED_TEXT = 99;
  
  private static final String TAG = "SRTTrack";
  
  private final Handler mEventHandler;
  
  SRTTrack(WebVttRenderingWidget paramWebVttRenderingWidget, MediaFormat paramMediaFormat) {
    super(paramWebVttRenderingWidget, paramMediaFormat);
    this.mEventHandler = null;
  }
  
  SRTTrack(Handler paramHandler, MediaFormat paramMediaFormat) {
    super((WebVttRenderingWidget)null, paramMediaFormat);
    this.mEventHandler = paramHandler;
  }
  
  protected void onData(SubtitleData paramSubtitleData) {
    try {
      TextTrackCue textTrackCue = new TextTrackCue();
      this();
      textTrackCue.mStartTimeMs = paramSubtitleData.getStartTimeUs() / 1000L;
      textTrackCue.mEndTimeMs = (paramSubtitleData.getStartTimeUs() + paramSubtitleData.getDurationUs()) / 1000L;
      String str = new String();
      this(paramSubtitleData.getData(), "UTF-8");
      String[] arrayOfString = str.split("\\r?\\n");
      textTrackCue.mLines = new TextTrackCueSpan[arrayOfString.length][];
      byte b1 = 0;
      int i;
      byte b2;
      for (i = arrayOfString.length, b2 = 0; b2 < i; ) {
        String str1 = arrayOfString[b2];
        TextTrackCueSpan textTrackCueSpan = new TextTrackCueSpan();
        this(str1, -1L);
        (new TextTrackCueSpan[1])[0] = textTrackCueSpan;
        textTrackCue.mLines[b1] = new TextTrackCueSpan[1];
        b2++;
        b1++;
      } 
      addCue(textTrackCue);
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("subtitle data is not UTF-8 encoded: ");
      stringBuilder.append(unsupportedEncodingException);
      Log.w("SRTTrack", stringBuilder.toString());
    } 
  }
  
  public void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong) {
    try {
      InputStreamReader inputStreamReader = new InputStreamReader();
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream();
      this(paramArrayOfbyte);
      this(byteArrayInputStream, "UTF-8");
      BufferedReader bufferedReader = new BufferedReader();
      this(inputStreamReader);
      while (bufferedReader.readLine() != null) {
        String str = bufferedReader.readLine();
        if (str == null)
          break; 
        TextTrackCue textTrackCue = new TextTrackCue();
        this();
        String[] arrayOfString = str.split("-->");
        textTrackCue.mStartTimeMs = parseMs(arrayOfString[0]);
        textTrackCue.mEndTimeMs = parseMs(arrayOfString[1]);
        try {
          textTrackCue.mRunID = paramLong;
          ArrayList<String> arrayList = new ArrayList();
          this();
          while (true) {
            String str1 = bufferedReader.readLine();
            if (str1 != null && !str1.trim().equals("")) {
              arrayList.add(str1);
              continue;
            } 
            break;
          } 
          byte b = 0;
          textTrackCue.mLines = new TextTrackCueSpan[arrayList.size()][];
          textTrackCue.mStrings = arrayList.<String>toArray(new String[0]);
          for (String str1 : arrayList) {
            TextTrackCueSpan textTrackCueSpan = new TextTrackCueSpan();
            this(str1, -1L);
            textTrackCue.mStrings[b] = str1;
            (new TextTrackCueSpan[1])[0] = textTrackCueSpan;
            textTrackCue.mLines[b] = new TextTrackCueSpan[1];
            b++;
          } 
          try {
            addCue(textTrackCue);
            continue;
          } catch (UnsupportedEncodingException unsupportedEncodingException) {
            // Byte code: goto -> 313
          } catch (IOException iOException) {}
          // Byte code: goto -> 298
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
          // Byte code: goto -> 313
        } catch (IOException iOException) {
          // Byte code: goto -> 298
        } 
      } 
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("subtitle data is not UTF-8 encoded: ");
      stringBuilder.append(unsupportedEncodingException);
      Log.w("SRTTrack", stringBuilder.toString());
    } catch (IOException iOException) {
      Log.e("SRTTrack", iOException.getMessage(), iOException);
    } 
  }
  
  public void updateView(Vector<SubtitleTrack.Cue> paramVector) {
    if (getRenderingWidget() != null) {
      super.updateView(paramVector);
      return;
    } 
    if (this.mEventHandler == null)
      return; 
    for (SubtitleTrack.Cue cue : paramVector) {
      TextTrackCue textTrackCue = (TextTrackCue)cue;
      Parcel parcel = Parcel.obtain();
      parcel.writeInt(102);
      parcel.writeInt(7);
      parcel.writeInt((int)cue.mStartTimeMs);
      parcel.writeInt(16);
      StringBuilder stringBuilder = new StringBuilder();
      for (String str : textTrackCue.mStrings) {
        stringBuilder.append(str);
        stringBuilder.append('\n');
      } 
      byte[] arrayOfByte = stringBuilder.toString().getBytes();
      parcel.writeInt(arrayOfByte.length);
      parcel.writeByteArray(arrayOfByte);
      Message message = this.mEventHandler.obtainMessage(99, 0, 0, parcel);
      this.mEventHandler.sendMessage(message);
    } 
    paramVector.clear();
  }
  
  private static long parseMs(String paramString) {
    long l1 = Long.parseLong(paramString.split(":")[0].trim());
    long l2 = Long.parseLong(paramString.split(":")[1].trim());
    long l3 = Long.parseLong(paramString.split(":")[2].split(",")[0].trim());
    long l4 = Long.parseLong(paramString.split(":")[2].split(",")[1].trim());
    return l1 * 60L * 60L * 1000L + 60L * l2 * 1000L + 1000L * l3 + l4;
  }
}
