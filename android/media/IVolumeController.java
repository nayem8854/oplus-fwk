package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IVolumeController extends IInterface {
  void dismiss() throws RemoteException;
  
  void displaySafeVolumeWarning(int paramInt) throws RemoteException;
  
  void masterMuteChanged(int paramInt) throws RemoteException;
  
  void setA11yMode(int paramInt) throws RemoteException;
  
  void setLayoutDirection(int paramInt) throws RemoteException;
  
  void volumeChanged(int paramInt1, int paramInt2) throws RemoteException;
  
  class Default implements IVolumeController {
    public void displaySafeVolumeWarning(int param1Int) throws RemoteException {}
    
    public void volumeChanged(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void masterMuteChanged(int param1Int) throws RemoteException {}
    
    public void setLayoutDirection(int param1Int) throws RemoteException {}
    
    public void dismiss() throws RemoteException {}
    
    public void setA11yMode(int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IVolumeController {
    private static final String DESCRIPTOR = "android.media.IVolumeController";
    
    static final int TRANSACTION_dismiss = 5;
    
    static final int TRANSACTION_displaySafeVolumeWarning = 1;
    
    static final int TRANSACTION_masterMuteChanged = 3;
    
    static final int TRANSACTION_setA11yMode = 6;
    
    static final int TRANSACTION_setLayoutDirection = 4;
    
    static final int TRANSACTION_volumeChanged = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IVolumeController");
    }
    
    public static IVolumeController asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IVolumeController");
      if (iInterface != null && iInterface instanceof IVolumeController)
        return (IVolumeController)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "setA11yMode";
        case 5:
          return "dismiss";
        case 4:
          return "setLayoutDirection";
        case 3:
          return "masterMuteChanged";
        case 2:
          return "volumeChanged";
        case 1:
          break;
      } 
      return "displaySafeVolumeWarning";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.media.IVolumeController");
            param1Int1 = param1Parcel1.readInt();
            setA11yMode(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.IVolumeController");
            dismiss();
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.IVolumeController");
            param1Int1 = param1Parcel1.readInt();
            setLayoutDirection(param1Int1);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.IVolumeController");
            param1Int1 = param1Parcel1.readInt();
            masterMuteChanged(param1Int1);
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.IVolumeController");
            param1Int2 = param1Parcel1.readInt();
            param1Int1 = param1Parcel1.readInt();
            volumeChanged(param1Int2, param1Int1);
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.IVolumeController");
        param1Int1 = param1Parcel1.readInt();
        displaySafeVolumeWarning(param1Int1);
        return true;
      } 
      param1Parcel2.writeString("android.media.IVolumeController");
      return true;
    }
    
    private static class Proxy implements IVolumeController {
      public static IVolumeController sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IVolumeController";
      }
      
      public void displaySafeVolumeWarning(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().displaySafeVolumeWarning(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void volumeChanged(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().volumeChanged(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void masterMuteChanged(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().masterMuteChanged(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setLayoutDirection(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().setLayoutDirection(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dismiss() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().dismiss();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setA11yMode(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IVolumeController");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IVolumeController.Stub.getDefaultImpl() != null) {
            IVolumeController.Stub.getDefaultImpl().setA11yMode(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IVolumeController param1IVolumeController) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IVolumeController != null) {
          Proxy.sDefaultImpl = param1IVolumeController;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IVolumeController getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
