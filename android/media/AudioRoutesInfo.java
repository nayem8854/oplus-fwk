package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class AudioRoutesInfo implements Parcelable {
  public int mainType = 0;
  
  public CharSequence bluetoothName;
  
  public static final int MAIN_USB = 16;
  
  public static final int MAIN_SPEAKER = 0;
  
  public static final int MAIN_HEADSET = 1;
  
  public static final int MAIN_HEADPHONES = 2;
  
  public static final int MAIN_HDMI = 8;
  
  public static final int MAIN_DOCK_SPEAKERS = 4;
  
  public AudioRoutesInfo(AudioRoutesInfo paramAudioRoutesInfo) {
    this.bluetoothName = paramAudioRoutesInfo.bluetoothName;
    this.mainType = paramAudioRoutesInfo.mainType;
  }
  
  AudioRoutesInfo(Parcel paramParcel) {
    this.bluetoothName = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mainType = paramParcel.readInt();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(getClass().getSimpleName());
    stringBuilder.append("{ type=");
    stringBuilder.append(typeToString(this.mainType));
    if (TextUtils.isEmpty(this.bluetoothName)) {
      null = "";
    } else {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(", bluetoothName=");
      stringBuilder1.append(this.bluetoothName);
      null = stringBuilder1.toString();
    } 
    stringBuilder.append(null);
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  private static String typeToString(int paramInt) {
    if (paramInt == 0)
      return "SPEAKER"; 
    if ((paramInt & 0x1) != 0)
      return "HEADSET"; 
    if ((paramInt & 0x2) != 0)
      return "HEADPHONES"; 
    if ((paramInt & 0x4) != 0)
      return "DOCK_SPEAKERS"; 
    if ((paramInt & 0x8) != 0)
      return "HDMI"; 
    if ((paramInt & 0x10) != 0)
      return "USB"; 
    return Integer.toHexString(paramInt);
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    TextUtils.writeToParcel(this.bluetoothName, paramParcel, paramInt);
    paramParcel.writeInt(this.mainType);
  }
  
  public static final Parcelable.Creator<AudioRoutesInfo> CREATOR = new Parcelable.Creator<AudioRoutesInfo>() {
      public AudioRoutesInfo createFromParcel(Parcel param1Parcel) {
        return new AudioRoutesInfo(param1Parcel);
      }
      
      public AudioRoutesInfo[] newArray(int param1Int) {
        return new AudioRoutesInfo[param1Int];
      }
    };
  
  public AudioRoutesInfo() {}
}
