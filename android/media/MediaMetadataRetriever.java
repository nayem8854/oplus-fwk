package android.media;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.IBinder;
import android.text.TextUtils;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MediaMetadataRetriever implements AutoCloseable {
  private static final int EMBEDDED_PICTURE_TYPE_ANY = 65535;
  
  public static final int METADATA_KEY_ALBUM = 1;
  
  public static final int METADATA_KEY_ALBUMARTIST = 13;
  
  public static final int METADATA_KEY_ARTIST = 2;
  
  public static final int METADATA_KEY_AUTHOR = 3;
  
  public static final int METADATA_KEY_BITRATE = 20;
  
  public static final int METADATA_KEY_BITS_PER_SAMPLE = 39;
  
  public static final int METADATA_KEY_CAPTURE_FRAMERATE = 25;
  
  public static final int METADATA_KEY_CD_TRACK_NUMBER = 0;
  
  public static final int METADATA_KEY_COLOR_RANGE = 37;
  
  public static final int METADATA_KEY_COLOR_STANDARD = 35;
  
  public static final int METADATA_KEY_COLOR_TRANSFER = 36;
  
  public static final int METADATA_KEY_COMPILATION = 15;
  
  public static final int METADATA_KEY_COMPOSER = 4;
  
  public static final int METADATA_KEY_DATE = 5;
  
  public static final int METADATA_KEY_DISC_NUMBER = 14;
  
  public static final int METADATA_KEY_DURATION = 9;
  
  public static final int METADATA_KEY_EXIF_LENGTH = 34;
  
  public static final int METADATA_KEY_EXIF_OFFSET = 33;
  
  public static final int METADATA_KEY_GENRE = 6;
  
  public static final int METADATA_KEY_HAS_AUDIO = 16;
  
  public static final int METADATA_KEY_HAS_IMAGE = 26;
  
  public static final int METADATA_KEY_HAS_VIDEO = 17;
  
  public static final int METADATA_KEY_IMAGE_COUNT = 27;
  
  public static final int METADATA_KEY_IMAGE_HEIGHT = 30;
  
  public static final int METADATA_KEY_IMAGE_PRIMARY = 28;
  
  public static final int METADATA_KEY_IMAGE_ROTATION = 31;
  
  public static final int METADATA_KEY_IMAGE_WIDTH = 29;
  
  public static final int METADATA_KEY_IS_DRM = 22;
  
  public static final int METADATA_KEY_LOCATION = 23;
  
  public static final int METADATA_KEY_MIMETYPE = 12;
  
  public static final int METADATA_KEY_NUM_TRACKS = 10;
  
  public static final int METADATA_KEY_SAMPLERATE = 38;
  
  public static final int METADATA_KEY_TIMED_TEXT_LANGUAGES = 21;
  
  public static final int METADATA_KEY_TITLE = 7;
  
  public static final int METADATA_KEY_VIDEO_FRAME_COUNT = 32;
  
  public static final int METADATA_KEY_VIDEO_HEIGHT = 19;
  
  public static final int METADATA_KEY_VIDEO_ROTATION = 24;
  
  public static final int METADATA_KEY_VIDEO_WIDTH = 18;
  
  public static final int METADATA_KEY_WRITER = 11;
  
  public static final int METADATA_KEY_YEAR = 8;
  
  public static final int OPTION_CLOSEST = 3;
  
  public static final int OPTION_CLOSEST_SYNC = 2;
  
  public static final int OPTION_NEXT_SYNC = 1;
  
  public static final int OPTION_PREVIOUS_SYNC = 0;
  
  private static final String[] STANDARD_GENRES = new String[] { 
      "Blues", "Classic Rock", "Country", "Dance", "Disco", "Funk", "Grunge", "Hip-Hop", "Jazz", "Metal", 
      "New Age", "Oldies", "Other", "Pop", "R&B", "Rap", "Reggae", "Rock", "Techno", "Industrial", 
      "Alternative", "Ska", "Death Metal", "Pranks", "Soundtrack", "Euro-Techno", "Ambient", "Trip-Hop", "Vocal", "Jazz+Funk", 
      "Fusion", "Trance", "Classical", "Instrumental", "Acid", "House", "Game", "Sound Clip", "Gospel", "Noise", 
      "AlternRock", "Bass", "Soul", "Punk", "Space", "Meditative", "Instrumental Pop", "Instrumental Rock", "Ethnic", "Gothic", 
      "Darkwave", "Techno-Industrial", "Electronic", "Pop-Folk", "Eurodance", "Dream", "Southern Rock", "Comedy", "Cult", "Gangsta", 
      "Top 40", "Christian Rap", "Pop/Funk", "Jungle", "Native American", "Cabaret", "New Wave", "Psychadelic", "Rave", "Showtunes", 
      "Trailer", "Lo-Fi", "Tribal", "Acid Punk", "Acid Jazz", "Polka", "Retro", "Musical", "Rock & Roll", "Hard Rock", 
      "Folk", "Folk-Rock", "National Folk", "Swing", "Fast Fusion", "Bebob", "Latin", "Revival", "Celtic", "Bluegrass", 
      "Avantgarde", "Gothic Rock", "Progressive Rock", "Psychedelic Rock", "Symphonic Rock", "Slow Rock", "Big Band", "Chorus", "Easy Listening", "Acoustic", 
      "Humour", "Speech", "Chanson", "Opera", "Chamber Music", "Sonata", "Symphony", "Booty Bass", "Primus", "Porn Groove", 
      "Satire", "Slow Jam", "Club", "Tango", "Samba", "Folklore", "Ballad", "Power Ballad", "Rhythmic Soul", "Freestyle", 
      "Duet", "Punk Rock", "Drum Solo", "A capella", "Euro-House", "Dance Hall", "Goa", "Drum & Bass", "Club-House", "Hardcore", 
      "Terror", "Indie", "BritPop", "Afro-Punk", "Polsk Punk", "Beat", "Christian Gangsta Rap", "Heavy Metal", "Black Metal", "Crossover", 
      "Contemporary Christian", "Christian Rock", "Merengue", "Salsa", "Thrash Metal", "Anime", "Jpop", "Synthpop" };
  
  private long mNativeContext;
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  public MediaMetadataRetriever() {
    native_setup();
  }
  
  public void setDataSource(String paramString) throws IllegalArgumentException {
    if (paramString != null) {
      Exception exception;
      IllegalArgumentException illegalArgumentException;
      String str2;
      FileInputStream fileInputStream;
      Uri uri1 = Uri.parse(paramString);
      String str1 = uri1.getScheme();
      if ("file".equals(str1)) {
        str2 = uri1.getPath();
      } else {
        str2 = paramString;
        if (str1 != null) {
          setDataSource(paramString, new HashMap<>());
          return;
        } 
      } 
      Uri uri2 = null;
      str1 = null;
      String str3 = null;
      paramString = str3;
      uri1 = uri2;
      String str4 = str1;
      try {
        FileInputStream fileInputStream4 = new FileInputStream();
        paramString = str3;
        uri1 = uri2;
        str4 = str1;
        this(str2);
        FileInputStream fileInputStream3 = fileInputStream4;
        FileInputStream fileInputStream1 = fileInputStream3, fileInputStream2 = fileInputStream3;
        fileInputStream = fileInputStream3;
        FileDescriptor fileDescriptor = fileInputStream3.getFD();
        fileInputStream1 = fileInputStream3;
        fileInputStream2 = fileInputStream3;
        fileInputStream = fileInputStream3;
        setDataSource(fileDescriptor, 0L, 576460752303423487L);
        try {
          fileInputStream3.close();
        } catch (Exception null) {}
        return;
      } catch (FileNotFoundException fileNotFoundException) {
        FileInputStream fileInputStream1 = fileInputStream;
        illegalArgumentException = new IllegalArgumentException();
        fileInputStream1 = fileInputStream;
        StringBuilder stringBuilder = new StringBuilder();
        fileInputStream1 = fileInputStream;
        this();
        fileInputStream1 = fileInputStream;
        stringBuilder.append(str2);
        fileInputStream1 = fileInputStream;
        stringBuilder.append(" does not exist");
        fileInputStream1 = fileInputStream;
        this(stringBuilder.toString());
        fileInputStream1 = fileInputStream;
        throw illegalArgumentException;
      } catch (IOException iOException) {
        exception = illegalArgumentException;
        IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
        exception = illegalArgumentException;
        StringBuilder stringBuilder = new StringBuilder();
        exception = illegalArgumentException;
        this();
        exception = illegalArgumentException;
        stringBuilder.append("couldn't open ");
        exception = illegalArgumentException;
        stringBuilder.append(str2);
        exception = illegalArgumentException;
        this(stringBuilder.toString());
        exception = illegalArgumentException;
        throw illegalArgumentException1;
      } finally {}
      if (exception != null)
        try {
          exception.close();
        } catch (Exception exception1) {} 
      throw str1;
    } 
    throw new IllegalArgumentException("null path");
  }
  
  public void setDataSource(String paramString, Map<String, String> paramMap) throws IllegalArgumentException {
    byte b = 0;
    String[] arrayOfString1 = new String[paramMap.size()];
    String[] arrayOfString2 = new String[paramMap.size()];
    for (Map.Entry<String, String> entry : paramMap.entrySet()) {
      arrayOfString1[b] = (String)entry.getKey();
      arrayOfString2[b] = (String)entry.getValue();
      b++;
    } 
    IBinder iBinder = MediaHTTPService.createHttpServiceBinderIfNecessary(paramString);
    _setDataSource(iBinder, paramString, arrayOfString1, arrayOfString2);
  }
  
  public void setDataSource(FileDescriptor paramFileDescriptor) throws IllegalArgumentException {
    setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
  }
  
  public void setDataSource(Context paramContext, Uri paramUri) throws IllegalArgumentException, SecurityException {
    if (paramUri != null) {
      IllegalArgumentException illegalArgumentException;
      StringBuilder stringBuilder;
      String str1 = paramUri.getScheme();
      if (str1 == null || str1.equals("file")) {
        setDataSource(paramUri.getPath());
        return;
      } 
      String str2 = null;
      AssetFileDescriptor assetFileDescriptor1 = null;
      AssetFileDescriptor assetFileDescriptor2 = assetFileDescriptor1;
      str1 = str2;
      try {
        IllegalArgumentException illegalArgumentException1;
        StringBuilder stringBuilder1;
        ContentResolver contentResolver = paramContext.getContentResolver();
        assetFileDescriptor2 = assetFileDescriptor1;
        str1 = str2;
      } catch (SecurityException securityException) {
        if (illegalArgumentException != null)
          try {
            illegalArgumentException.close();
          } catch (IOException iOException) {} 
        return;
      } finally {
        if (stringBuilder != null)
          try {
            stringBuilder.close();
          } catch (IOException iOException) {} 
      } 
    } 
    throw new IllegalArgumentException("null uri");
  }
  
  public void setDataSource(MediaDataSource paramMediaDataSource) throws IllegalArgumentException {
    _setDataSource(paramMediaDataSource);
  }
  
  public String extractMetadata(int paramInt) {
    String str1 = nativeExtractMetadata(paramInt);
    String str2 = str1;
    if (paramInt == 6)
      str2 = convertGenreTag(str1); 
    return str2;
  }
  
  private String convertGenreTag(String paramString) {
    boolean bool = TextUtils.isEmpty(paramString);
    NumberFormatException numberFormatException2 = null;
    if (bool)
      return null; 
    if (Character.isDigit(paramString.charAt(0))) {
      try {
        int i = Integer.parseInt(paramString);
        if (i >= 0 && i < STANDARD_GENRES.length)
          return STANDARD_GENRES[i]; 
      } catch (NumberFormatException numberFormatException1) {}
      return null;
    } 
    StringBuilder stringBuilder = null;
    CharSequence charSequence = null;
    while (true) {
      String str2;
      StringBuilder stringBuilder1 = stringBuilder;
      if (!TextUtils.isEmpty(charSequence)) {
        stringBuilder1 = stringBuilder;
        if (stringBuilder == null)
          stringBuilder1 = new StringBuilder(); 
        if (stringBuilder1.length() != 0)
          stringBuilder1.append(", "); 
        stringBuilder1.append((String)charSequence);
      } 
      if (TextUtils.isEmpty((CharSequence)numberFormatException1)) {
        numberFormatException1 = numberFormatException2;
        if (stringBuilder1 != null)
          if (stringBuilder1.length() == 0) {
            numberFormatException1 = numberFormatException2;
          } else {
            str2 = stringBuilder1.toString();
          }  
        return str2;
      } 
      if (str2.startsWith("(RX)")) {
        charSequence = "Remix";
        str2 = str2.substring(4);
        stringBuilder = stringBuilder1;
        continue;
      } 
      if (str2.startsWith("(CR)")) {
        charSequence = "Cover";
        str2 = str2.substring(4);
        stringBuilder = stringBuilder1;
        continue;
      } 
      if (str2.startsWith("((")) {
        int i = str2.indexOf(')');
        if (i == -1) {
          charSequence = str2.substring(1);
          str2 = "";
        } else {
          charSequence = str2.substring(1, i + 1);
          str2 = str2.substring(i + 1);
        } 
        stringBuilder = stringBuilder1;
        continue;
      } 
      if (str2.startsWith("(")) {
        int i = str2.indexOf(')');
        if (i == -1)
          return null; 
        charSequence = str2.substring(1, i);
        try {
          int j = Integer.parseInt(charSequence.toString());
          if (j >= 0 && j < STANDARD_GENRES.length) {
            charSequence = STANDARD_GENRES[j];
            str2 = str2.substring(i + 1);
            stringBuilder = stringBuilder1;
            continue;
          } 
          return null;
        } catch (NumberFormatException numberFormatException3) {
          return null;
        } 
      } 
      String str3 = "";
      stringBuilder = stringBuilder1;
      NumberFormatException numberFormatException4 = numberFormatException3;
      String str1 = str3;
    } 
  }
  
  public Bitmap getFrameAtTime(long paramLong, int paramInt) {
    if (paramInt >= 0 && paramInt <= 3)
      return _getFrameAtTime(paramLong, paramInt, -1, -1, null); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported option: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Bitmap getFrameAtTime(long paramLong, int paramInt, BitmapParams paramBitmapParams) {
    if (paramInt >= 0 && paramInt <= 3)
      return _getFrameAtTime(paramLong, paramInt, -1, -1, paramBitmapParams); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported option: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Bitmap getScaledFrameAtTime(long paramLong, int paramInt1, int paramInt2, int paramInt3) {
    validate(paramInt1, paramInt2, paramInt3);
    return _getFrameAtTime(paramLong, paramInt1, paramInt2, paramInt3, null);
  }
  
  public Bitmap getScaledFrameAtTime(long paramLong, int paramInt1, int paramInt2, int paramInt3, BitmapParams paramBitmapParams) {
    validate(paramInt1, paramInt2, paramInt3);
    return _getFrameAtTime(paramLong, paramInt1, paramInt2, paramInt3, paramBitmapParams);
  }
  
  private void validate(int paramInt1, int paramInt2, int paramInt3) {
    if (paramInt1 >= 0 && paramInt1 <= 3) {
      if (paramInt2 > 0) {
        if (paramInt3 > 0)
          return; 
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Invalid height: ");
        stringBuilder2.append(paramInt3);
        throw new IllegalArgumentException(stringBuilder2.toString());
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Invalid width: ");
      stringBuilder1.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported option: ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public Bitmap getFrameAtTime(long paramLong) {
    return getFrameAtTime(paramLong, 2);
  }
  
  public Bitmap getFrameAtTime() {
    return _getFrameAtTime(-1L, 2, -1, -1, null);
  }
  
  public static final class BitmapParams {
    private Bitmap.Config inPreferredConfig = Bitmap.Config.ARGB_8888;
    
    private Bitmap.Config outActualConfig = Bitmap.Config.ARGB_8888;
    
    public void setPreferredConfig(Bitmap.Config param1Config) {
      if (param1Config != null) {
        this.inPreferredConfig = param1Config;
        return;
      } 
      throw new IllegalArgumentException("preferred config can't be null");
    }
    
    public Bitmap.Config getPreferredConfig() {
      return this.inPreferredConfig;
    }
    
    public Bitmap.Config getActualConfig() {
      return this.outActualConfig;
    }
  }
  
  public Bitmap getFrameAtIndex(int paramInt, BitmapParams paramBitmapParams) {
    List<Bitmap> list = getFramesAtIndex(paramInt, 1, paramBitmapParams);
    return list.get(0);
  }
  
  public Bitmap getFrameAtIndex(int paramInt) {
    List<Bitmap> list = getFramesAtIndex(paramInt, 1);
    return list.get(0);
  }
  
  public List<Bitmap> getFramesAtIndex(int paramInt1, int paramInt2, BitmapParams paramBitmapParams) {
    return getFramesAtIndexInternal(paramInt1, paramInt2, paramBitmapParams);
  }
  
  public List<Bitmap> getFramesAtIndex(int paramInt1, int paramInt2) {
    return getFramesAtIndexInternal(paramInt1, paramInt2, null);
  }
  
  private List<Bitmap> getFramesAtIndexInternal(int paramInt1, int paramInt2, BitmapParams paramBitmapParams) {
    if ("yes".equals(extractMetadata(17))) {
      String str = extractMetadata(32);
      int i = Integer.parseInt(str);
      if (paramInt1 >= 0 && paramInt2 >= 1 && paramInt1 < i && paramInt1 <= i - paramInt2)
        return _getFrameAtIndex(paramInt1, paramInt2, paramBitmapParams); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid frameIndex or numFrames: ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(", ");
      stringBuilder.append(paramInt2);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalStateException("Does not contail video or image sequences");
  }
  
  public Bitmap getImageAtIndex(int paramInt, BitmapParams paramBitmapParams) {
    return getImageAtIndexInternal(paramInt, paramBitmapParams);
  }
  
  public Bitmap getImageAtIndex(int paramInt) {
    return getImageAtIndexInternal(paramInt, null);
  }
  
  public Bitmap getPrimaryImage(BitmapParams paramBitmapParams) {
    return getImageAtIndexInternal(-1, paramBitmapParams);
  }
  
  public Bitmap getPrimaryImage() {
    return getImageAtIndexInternal(-1, null);
  }
  
  private Bitmap getImageAtIndexInternal(int paramInt, BitmapParams paramBitmapParams) {
    if ("yes".equals(extractMetadata(26))) {
      String str = extractMetadata(27);
      if (paramInt < Integer.parseInt(str))
        return _getImageAtIndex(paramInt, paramBitmapParams); 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid image index: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalStateException("Does not contail still images");
  }
  
  public byte[] getEmbeddedPicture() {
    return getEmbeddedPicture(65535);
  }
  
  public void close() {
    release();
  }
  
  protected void finalize() throws Throwable {
    try {
      native_finalize();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  private native List<Bitmap> _getFrameAtIndex(int paramInt1, int paramInt2, BitmapParams paramBitmapParams);
  
  private native Bitmap _getFrameAtTime(long paramLong, int paramInt1, int paramInt2, int paramInt3, BitmapParams paramBitmapParams);
  
  private native Bitmap _getImageAtIndex(int paramInt, BitmapParams paramBitmapParams);
  
  private native void _setDataSource(MediaDataSource paramMediaDataSource) throws IllegalArgumentException;
  
  private native void _setDataSource(IBinder paramIBinder, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) throws IllegalArgumentException;
  
  private native byte[] getEmbeddedPicture(int paramInt);
  
  private native String nativeExtractMetadata(int paramInt);
  
  private final native void native_finalize();
  
  private static native void native_init();
  
  private native void native_setup();
  
  public native Bitmap getThumbnailImageAtIndex(int paramInt1, BitmapParams paramBitmapParams, int paramInt2, int paramInt3);
  
  public native void release();
  
  public native void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) throws IllegalArgumentException;
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Option {}
}
