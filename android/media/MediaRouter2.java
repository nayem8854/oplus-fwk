package android.media;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import com.android.internal.util.function.QuadConsumer;
import com.android.internal.util.function.TriConsumer;
import com.android.internal.util.function.pooled.PooledLambda;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MediaRouter2 {
  private static final boolean DEBUG = Log.isLoggable("MR2", 3);
  
  private static final long MANAGER_REQUEST_ID_NONE = 0L;
  
  private static final String TAG = "MR2";
  
  private static final int TRANSFER_TIMEOUT_MS = 30000;
  
  private static MediaRouter2 sInstance;
  
  private static final Object sRouterLock = new Object();
  
  private final Context mContext;
  
  private final CopyOnWriteArrayList<ControllerCallbackRecord> mControllerCallbackRecords;
  
  private final CopyOnWriteArrayList<ControllerCreationRequest> mControllerCreationRequests;
  
  private RouteDiscoveryPreference mDiscoveryPreference;
  
  private volatile List<MediaRoute2Info> mFilteredRoutes;
  
  final Handler mHandler;
  
  private final IMediaRouterService mMediaRouterService;
  
  private final AtomicInteger mNextRequestId;
  
  private final Map<String, RoutingController> mNonSystemRoutingControllers;
  
  private volatile OnGetControllerHintsListener mOnGetControllerHintsListener;
  
  private final String mPackageName;
  
  private final CopyOnWriteArrayList<RouteCallbackRecord> mRouteCallbackRecords;
  
  final Map<String, MediaRoute2Info> mRoutes;
  
  private boolean mShouldUpdateRoutes;
  
  MediaRouter2Stub mStub;
  
  final RoutingController mSystemController;
  
  private final CopyOnWriteArrayList<TransferCallbackRecord> mTransferCallbackRecords;
  
  public static MediaRouter2 getInstance(Context paramContext) {
    Objects.requireNonNull(paramContext, "context must not be null");
    synchronized (sRouterLock) {
      if (sInstance == null) {
        MediaRouter2 mediaRouter2 = new MediaRouter2();
        this(paramContext.getApplicationContext());
        sInstance = mediaRouter2;
      } 
      return sInstance;
    } 
  }
  
  private MediaRouter2(Context paramContext) {
    RoutingSessionInfo routingSessionInfo1, routingSessionInfo2;
    List<MediaRoute2Info> list2;
    this.mRouteCallbackRecords = new CopyOnWriteArrayList<>();
    this.mTransferCallbackRecords = new CopyOnWriteArrayList<>();
    this.mControllerCallbackRecords = new CopyOnWriteArrayList<>();
    this.mControllerCreationRequests = new CopyOnWriteArrayList<>();
    this.mRoutes = (Map<String, MediaRoute2Info>)new ArrayMap();
    this.mDiscoveryPreference = RouteDiscoveryPreference.EMPTY;
    this.mNonSystemRoutingControllers = (Map<String, RoutingController>)new ArrayMap();
    this.mNextRequestId = new AtomicInteger(1);
    this.mShouldUpdateRoutes = true;
    this.mFilteredRoutes = Collections.emptyList();
    this.mContext = paramContext;
    IBinder iBinder = ServiceManager.getService("media_router");
    this.mMediaRouterService = IMediaRouterService.Stub.asInterface(iBinder);
    this.mPackageName = this.mContext.getPackageName();
    this.mHandler = new Handler(Looper.getMainLooper());
    iBinder = null;
    List<MediaRoute2Info> list1 = null;
    try {
      List<MediaRoute2Info> list4 = this.mMediaRouterService.getSystemRoutes();
      List<MediaRoute2Info> list3 = list4;
      RoutingSessionInfo routingSessionInfo4 = this.mMediaRouterService.getSystemSessionInfo();
      list1 = list4;
      RoutingSessionInfo routingSessionInfo3 = routingSessionInfo1;
    } catch (RemoteException remoteException) {
      Log.e("MR2", "Unable to get current system's routes / session info", (Throwable)remoteException);
      list2 = list1;
      routingSessionInfo2 = routingSessionInfo1;
    } 
    if (routingSessionInfo2 != null && !routingSessionInfo2.isEmpty()) {
      if (list2 != null) {
        for (MediaRoute2Info mediaRoute2Info : routingSessionInfo2)
          this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info); 
        this.mSystemController = new SystemRoutingController((RoutingSessionInfo)list2);
        return;
      } 
      throw new RuntimeException("Null currentSystemSessionInfo. Something is wrong.");
    } 
    throw new RuntimeException("Null or empty currentSystemRoutes. Something is wrong.");
  }
  
  static boolean checkRouteListContainsRouteId(List<MediaRoute2Info> paramList, String paramString) {
    for (MediaRoute2Info mediaRoute2Info : paramList) {
      if (TextUtils.equals(paramString, mediaRoute2Info.getId()))
        return true; 
    } 
    return false;
  }
  
  public void registerRouteCallback(Executor paramExecutor, RouteCallback paramRouteCallback, RouteDiscoveryPreference paramRouteDiscoveryPreference) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramRouteCallback, "callback must not be null");
    Objects.requireNonNull(paramRouteDiscoveryPreference, "preference must not be null");
    RouteCallbackRecord routeCallbackRecord = new RouteCallbackRecord(paramExecutor, paramRouteCallback, paramRouteDiscoveryPreference);
    this.mRouteCallbackRecords.remove(routeCallbackRecord);
    this.mRouteCallbackRecords.addIfAbsent(routeCallbackRecord);
    synchronized (sRouterLock) {
      if (this.mStub == null) {
        MediaRouter2Stub mediaRouter2Stub = new MediaRouter2Stub();
        this(this);
        try {
          this.mMediaRouterService.registerRouter2(mediaRouter2Stub, this.mPackageName);
          this.mStub = mediaRouter2Stub;
        } catch (RemoteException remoteException) {
          Log.e("MR2", "registerRouteCallback: Unable to register MediaRouter2.", (Throwable)remoteException);
        } 
      } 
      if (this.mStub != null) {
        boolean bool = updateDiscoveryPreferenceIfNeededLocked();
        if (bool)
          try {
            this.mMediaRouterService.setDiscoveryRequestWithRouter2(this.mStub, this.mDiscoveryPreference);
          } catch (RemoteException remoteException) {
            Log.e("MR2", "registerRouteCallback: Unable to set discovery request.", (Throwable)remoteException);
          }  
      } 
      return;
    } 
  }
  
  public void unregisterRouteCallback(RouteCallback paramRouteCallback) {
    Objects.requireNonNull(paramRouteCallback, "callback must not be null");
    if (!this.mRouteCallbackRecords.remove(new RouteCallbackRecord(null, paramRouteCallback, null))) {
      Log.w("MR2", "unregisterRouteCallback: Ignoring unknown callback");
      return;
    } 
    synchronized (sRouterLock) {
      if (this.mStub == null)
        return; 
      boolean bool = updateDiscoveryPreferenceIfNeededLocked();
      if (bool)
        try {
          this.mMediaRouterService.setDiscoveryRequestWithRouter2(this.mStub, this.mDiscoveryPreference);
        } catch (RemoteException remoteException) {
          Log.e("MR2", "unregisterRouteCallback: Unable to set discovery request.", (Throwable)remoteException);
        }  
      if (this.mRouteCallbackRecords.isEmpty()) {
        bool = this.mNonSystemRoutingControllers.isEmpty();
        if (bool) {
          try {
            this.mMediaRouterService.unregisterRouter2(this.mStub);
          } catch (RemoteException remoteException) {
            Log.e("MR2", "Unable to unregister media router.", (Throwable)remoteException);
          } 
          this.mStub = null;
        } 
      } 
      this.mShouldUpdateRoutes = true;
      return;
    } 
  }
  
  private boolean updateDiscoveryPreferenceIfNeededLocked() {
    CopyOnWriteArrayList<RouteCallbackRecord> copyOnWriteArrayList = this.mRouteCallbackRecords;
    Stream<?> stream = copyOnWriteArrayList.stream().map((Function)_$$Lambda$MediaRouter2$cGcxv69EHAfCH68BnC1wkAsMANY.INSTANCE);
    Collector<?, ?, List<?>> collector = Collectors.toList();
    RouteDiscoveryPreference.Builder builder = new RouteDiscoveryPreference.Builder((Collection<RouteDiscoveryPreference>)stream.collect(collector));
    RouteDiscoveryPreference routeDiscoveryPreference = builder.build();
    if (Objects.equals(this.mDiscoveryPreference, routeDiscoveryPreference))
      return false; 
    this.mDiscoveryPreference = routeDiscoveryPreference;
    this.mShouldUpdateRoutes = true;
    return true;
  }
  
  public List<MediaRoute2Info> getRoutes() {
    synchronized (sRouterLock) {
      if (this.mShouldUpdateRoutes) {
        this.mShouldUpdateRoutes = false;
        ArrayList<MediaRoute2Info> arrayList = new ArrayList();
        this();
        for (MediaRoute2Info mediaRoute2Info : this.mRoutes.values()) {
          if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
            arrayList.add(mediaRoute2Info); 
        } 
        this.mFilteredRoutes = Collections.unmodifiableList(arrayList);
      } 
      return this.mFilteredRoutes;
    } 
  }
  
  public void registerTransferCallback(Executor paramExecutor, TransferCallback paramTransferCallback) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramTransferCallback, "callback must not be null");
    TransferCallbackRecord transferCallbackRecord = new TransferCallbackRecord(paramExecutor, paramTransferCallback);
    if (!this.mTransferCallbackRecords.addIfAbsent(transferCallbackRecord)) {
      Log.w("MR2", "registerTransferCallback: Ignoring the same callback");
      return;
    } 
  }
  
  public void unregisterTransferCallback(TransferCallback paramTransferCallback) {
    Objects.requireNonNull(paramTransferCallback, "callback must not be null");
    if (!this.mTransferCallbackRecords.remove(new TransferCallbackRecord(null, paramTransferCallback))) {
      Log.w("MR2", "unregisterTransferCallback: Ignoring an unknown callback");
      return;
    } 
  }
  
  public void registerControllerCallback(Executor paramExecutor, ControllerCallback paramControllerCallback) {
    Objects.requireNonNull(paramExecutor, "executor must not be null");
    Objects.requireNonNull(paramControllerCallback, "callback must not be null");
    ControllerCallbackRecord controllerCallbackRecord = new ControllerCallbackRecord(paramExecutor, paramControllerCallback);
    if (!this.mControllerCallbackRecords.addIfAbsent(controllerCallbackRecord)) {
      Log.w("MR2", "registerControllerCallback: Ignoring the same callback");
      return;
    } 
  }
  
  public void unregisterControllerCallback(ControllerCallback paramControllerCallback) {
    Objects.requireNonNull(paramControllerCallback, "callback must not be null");
    if (!this.mControllerCallbackRecords.remove(new ControllerCallbackRecord(null, paramControllerCallback))) {
      Log.w("MR2", "unregisterControllerCallback: Ignoring an unknown callback");
      return;
    } 
  }
  
  public void setOnGetControllerHintsListener(OnGetControllerHintsListener paramOnGetControllerHintsListener) {
    this.mOnGetControllerHintsListener = paramOnGetControllerHintsListener;
  }
  
  public void transferTo(MediaRoute2Info paramMediaRoute2Info) {
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Transferring to route: ");
    stringBuilder.append(paramMediaRoute2Info);
    Log.v("MR2", stringBuilder.toString());
    transfer(getCurrentController(), paramMediaRoute2Info);
  }
  
  public void stop() {
    getCurrentController().release();
  }
  
  void transfer(RoutingController paramRoutingController, MediaRoute2Info paramMediaRoute2Info) {
    Objects.requireNonNull(paramRoutingController, "controller must not be null");
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    synchronized (sRouterLock) {
      boolean bool = this.mRoutes.containsKey(paramMediaRoute2Info.getId());
      if (!bool) {
        notifyTransferFailure(paramMediaRoute2Info);
        return;
      } 
      if (paramRoutingController.getRoutingSessionInfo().getTransferableRoutes().contains(paramMediaRoute2Info.getId())) {
        paramRoutingController.transferToRoute(paramMediaRoute2Info);
        return;
      } 
      requestCreateController(paramRoutingController, paramMediaRoute2Info, 0L);
      return;
    } 
  }
  
  void requestCreateController(RoutingController paramRoutingController, MediaRoute2Info paramMediaRoute2Info, long paramLong) {
    int i = this.mNextRequestId.getAndIncrement();
    ControllerCreationRequest controllerCreationRequest = new ControllerCreationRequest(i, paramLong, paramMediaRoute2Info, paramRoutingController);
    this.mControllerCreationRequests.add(controllerCreationRequest);
    OnGetControllerHintsListener onGetControllerHintsListener = this.mOnGetControllerHintsListener;
    if (onGetControllerHintsListener != null) {
      Bundle bundle = onGetControllerHintsListener.onGetControllerHints(paramMediaRoute2Info);
      if (bundle != null)
        bundle = new Bundle(bundle); 
    } else {
      onGetControllerHintsListener = null;
    } 
    synchronized (sRouterLock) {
      MediaRouter2Stub mediaRouter2Stub = this.mStub;
      if (mediaRouter2Stub != null)
        try {
          null = this.mMediaRouterService;
          RoutingSessionInfo routingSessionInfo = paramRoutingController.getRoutingSessionInfo();
          null.requestCreateSessionWithRouter2(mediaRouter2Stub, i, paramLong, routingSessionInfo, paramMediaRoute2Info, (Bundle)onGetControllerHintsListener);
        } catch (RemoteException remoteException) {
          Log.e("MR2", "createControllerForTransfer: Failed to request for creating a controller.", (Throwable)remoteException);
          this.mControllerCreationRequests.remove(controllerCreationRequest);
          if (paramLong == 0L)
            notifyTransferFailure(paramMediaRoute2Info); 
        }  
      return;
    } 
  }
  
  private RoutingController getCurrentController() {
    List<RoutingController> list = getControllers();
    return list.get(list.size() - 1);
  }
  
  public RoutingController getSystemController() {
    return this.mSystemController;
  }
  
  public List<RoutingController> getControllers() {
    null = new ArrayList();
    null.add(0, this.mSystemController);
    synchronized (sRouterLock) {
      null.addAll(this.mNonSystemRoutingControllers.values());
      return null;
    } 
  }
  
  public void setRouteVolume(MediaRoute2Info paramMediaRoute2Info, int paramInt) {
    Objects.requireNonNull(paramMediaRoute2Info, "route must not be null");
    synchronized (sRouterLock) {
      MediaRouter2Stub mediaRouter2Stub = this.mStub;
      if (mediaRouter2Stub != null)
        try {
          this.mMediaRouterService.setRouteVolumeWithRouter2(mediaRouter2Stub, paramMediaRoute2Info, paramInt);
        } catch (RemoteException remoteException) {
          Log.e("MR2", "Unable to set route volume.", (Throwable)remoteException);
        }  
      return;
    } 
  }
  
  void syncRoutesOnHandler(List<MediaRoute2Info> paramList, RoutingSessionInfo paramRoutingSessionInfo) {
    if (paramList == null || paramList.isEmpty() || paramRoutingSessionInfo == null) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("syncRoutesOnHandler: Received wrong data. currentRoutes=");
      stringBuilder.append(paramList);
      stringBuilder.append(", currentSystemSessionInfo=");
      stringBuilder.append(paramRoutingSessionInfo);
      Log.e("MR2", stringBuilder.toString());
      return;
    } 
    ArrayList<MediaRoute2Info> arrayList1 = new ArrayList();
    ArrayList<MediaRoute2Info> arrayList2 = new ArrayList();
    ArrayList<MediaRoute2Info> arrayList3 = new ArrayList();
    synchronized (sRouterLock) {
      Stream stream = paramList.stream().map((Function)_$$Lambda$Jl1VWT2dPpodkj8vkFOye7iVD0Y.INSTANCE);
      List list = (List)stream.collect(Collectors.toList());
      for (String str : this.mRoutes.keySet()) {
        if (!list.contains(str)) {
          MediaRoute2Info mediaRoute2Info = this.mRoutes.get(str);
          if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
            arrayList2.add(this.mRoutes.get(str)); 
        } 
      } 
      for (MediaRoute2Info mediaRoute2Info : paramList) {
        if (this.mRoutes.containsKey(mediaRoute2Info.getId())) {
          if (!mediaRoute2Info.equals(this.mRoutes.get(mediaRoute2Info.getId()))) {
            RouteDiscoveryPreference routeDiscoveryPreference = this.mDiscoveryPreference;
            List<String> list1 = routeDiscoveryPreference.getPreferredFeatures();
            if (mediaRoute2Info.hasAnyFeatures(list1))
              arrayList3.add(mediaRoute2Info); 
          } 
          continue;
        } 
        if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
          arrayList1.add(mediaRoute2Info); 
      } 
      this.mRoutes.clear();
      for (MediaRoute2Info mediaRoute2Info : paramList)
        this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info); 
      this.mShouldUpdateRoutes = true;
      if (!arrayList1.isEmpty())
        notifyRoutesAdded(arrayList1); 
      if (!arrayList2.isEmpty())
        notifyRoutesRemoved(arrayList2); 
      if (!arrayList3.isEmpty())
        notifyRoutesChanged(arrayList3); 
      RoutingSessionInfo routingSessionInfo = this.mSystemController.getRoutingSessionInfo();
      this.mSystemController.setRoutingSessionInfo(paramRoutingSessionInfo);
      if (!routingSessionInfo.equals(paramRoutingSessionInfo))
        notifyControllerUpdated(this.mSystemController); 
      return;
    } 
  }
  
  void addRoutesOnHandler(List<MediaRoute2Info> paramList) {
    ArrayList<MediaRoute2Info> arrayList = new ArrayList();
    synchronized (sRouterLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList) {
        this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info);
        if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
          arrayList.add(mediaRoute2Info); 
      } 
      this.mShouldUpdateRoutes = true;
      if (!arrayList.isEmpty())
        notifyRoutesAdded(arrayList); 
      return;
    } 
  }
  
  void removeRoutesOnHandler(List<MediaRoute2Info> paramList) {
    ArrayList<MediaRoute2Info> arrayList = new ArrayList();
    synchronized (sRouterLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList) {
        this.mRoutes.remove(mediaRoute2Info.getId());
        if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
          arrayList.add(mediaRoute2Info); 
      } 
      this.mShouldUpdateRoutes = true;
      if (!arrayList.isEmpty())
        notifyRoutesRemoved(arrayList); 
      return;
    } 
  }
  
  void changeRoutesOnHandler(List<MediaRoute2Info> paramList) {
    ArrayList<MediaRoute2Info> arrayList = new ArrayList();
    synchronized (sRouterLock) {
      for (MediaRoute2Info mediaRoute2Info : paramList) {
        this.mRoutes.put(mediaRoute2Info.getId(), mediaRoute2Info);
        if (mediaRoute2Info.hasAnyFeatures(this.mDiscoveryPreference.getPreferredFeatures()))
          arrayList.add(mediaRoute2Info); 
      } 
      this.mShouldUpdateRoutes = true;
      if (!arrayList.isEmpty())
        notifyRoutesChanged(arrayList); 
      return;
    } 
  }
  
  void createControllerOnHandler(int paramInt, RoutingSessionInfo paramRoutingSessionInfo) {
    String str1;
    RoutingController routingController1;
    ControllerCreationRequest controllerCreationRequest2;
    StringBuilder stringBuilder;
    ControllerCreationRequest controllerCreationRequest1 = null;
    Iterator<ControllerCreationRequest> iterator = this.mControllerCreationRequests.iterator();
    while (true) {
      controllerCreationRequest2 = controllerCreationRequest1;
      if (iterator.hasNext()) {
        controllerCreationRequest2 = iterator.next();
        if (controllerCreationRequest2.mRequestId == paramInt)
          break; 
        continue;
      } 
      break;
    } 
    if (controllerCreationRequest2 == null) {
      Log.w("MR2", "createControllerOnHandler: Ignoring an unknown request.");
      return;
    } 
    this.mControllerCreationRequests.remove(controllerCreationRequest2);
    MediaRoute2Info mediaRoute2Info = controllerCreationRequest2.mRoute;
    if (paramRoutingSessionInfo == null) {
      notifyTransferFailure(mediaRoute2Info);
      return;
    } 
    String str2 = mediaRoute2Info.getProviderId();
    String str3 = paramRoutingSessionInfo.getProviderId();
    if (!TextUtils.equals(str2, str3)) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("The session's provider ID does not match the requested route's. (requested route's providerId=");
      stringBuilder.append(mediaRoute2Info.getProviderId());
      stringBuilder.append(", actual providerId=");
      stringBuilder.append(paramRoutingSessionInfo.getProviderId());
      stringBuilder.append(")");
      str1 = stringBuilder.toString();
      Log.w("MR2", str1);
      notifyTransferFailure(mediaRoute2Info);
      return;
    } 
    RoutingController routingController2 = ((ControllerCreationRequest)stringBuilder).mOldController;
    if (!routingController2.scheduleRelease()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("createControllerOnHandler: Ignoring controller creation for released old controller. oldController=");
      stringBuilder.append(routingController2);
      Log.w("MR2", stringBuilder.toString());
      if (!str1.isSystemSession())
        (new RoutingController((RoutingSessionInfo)str1)).release(); 
      notifyTransferFailure(mediaRoute2Info);
      return;
    } 
    if (str1.isSystemSession()) {
      RoutingController routingController = getSystemController();
      routingController.setRoutingSessionInfo((RoutingSessionInfo)str1);
      routingController1 = routingController;
    } else {
      routingController1 = new RoutingController((RoutingSessionInfo)routingController1);
      synchronized (sRouterLock) {
        this.mNonSystemRoutingControllers.put(routingController1.getId(), routingController1);
        notifyTransfer(routingController2, routingController1);
        return;
      } 
    } 
    notifyTransfer(routingController2, routingController1);
  }
  
  void updateControllerOnHandler(RoutingSessionInfo paramRoutingSessionInfo) {
    if (paramRoutingSessionInfo == null) {
      Log.w("MR2", "updateControllerOnHandler: Ignoring null sessionInfo.");
      return;
    } 
    if (paramRoutingSessionInfo.isSystemSession()) {
      RoutingController routingController = getSystemController();
      routingController.setRoutingSessionInfo(paramRoutingSessionInfo);
      notifyControllerUpdated(routingController);
      return;
    } 
    synchronized (sRouterLock) {
      String str;
      StringBuilder stringBuilder;
      RoutingController routingController = this.mNonSystemRoutingControllers.get(paramRoutingSessionInfo.getId());
      if (routingController == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("updateControllerOnHandler: Matching controller not found. uniqueSessionId=");
        stringBuilder.append(paramRoutingSessionInfo.getId());
        str = stringBuilder.toString();
        Log.w("MR2", str);
        return;
      } 
      null = stringBuilder.getRoutingSessionInfo();
      if (!TextUtils.equals(null.getProviderId(), str.getProviderId())) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("updateControllerOnHandler: Provider IDs are not matched. old=");
        stringBuilder.append(null.getProviderId());
        stringBuilder.append(", new=");
        stringBuilder.append(str.getProviderId());
        str = stringBuilder.toString();
        Log.w("MR2", str);
        return;
      } 
      stringBuilder.setRoutingSessionInfo((RoutingSessionInfo)str);
      notifyControllerUpdated((RoutingController)stringBuilder);
      return;
    } 
  }
  
  void releaseControllerOnHandler(RoutingSessionInfo paramRoutingSessionInfo) {
    if (paramRoutingSessionInfo == null) {
      Log.w("MR2", "releaseControllerOnHandler: Ignoring null sessionInfo.");
      return;
    } 
    synchronized (sRouterLock) {
      String str;
      StringBuilder stringBuilder;
      RoutingController routingController = this.mNonSystemRoutingControllers.get(paramRoutingSessionInfo.getId());
      if (routingController == null) {
        if (DEBUG) {
          null = new StringBuilder();
          null.append("releaseControllerOnHandler: Matching controller not found. uniqueSessionId=");
          null.append(paramRoutingSessionInfo.getId());
          str = null.toString();
          Log.d("MR2", str);
        } 
        return;
      } 
      null = routingController.getRoutingSessionInfo();
      if (!TextUtils.equals(null.getProviderId(), str.getProviderId())) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("releaseControllerOnHandler: Provider IDs are not matched. old=");
        stringBuilder.append(null.getProviderId());
        stringBuilder.append(", new=");
        stringBuilder.append(str.getProviderId());
        str = stringBuilder.toString();
        Log.w("MR2", str);
        return;
      } 
      stringBuilder.releaseInternal(false);
      return;
    } 
  }
  
  void onRequestCreateControllerByManagerOnHandler(RoutingSessionInfo paramRoutingSessionInfo, MediaRoute2Info paramMediaRoute2Info, long paramLong) {
    RoutingController routingController;
    if (paramRoutingSessionInfo.isSystemSession()) {
      routingController = getSystemController();
    } else {
      synchronized (sRouterLock) {
        routingController = this.mNonSystemRoutingControllers.get(routingController.getId());
        if (routingController == null)
          return; 
        requestCreateController(routingController, paramMediaRoute2Info, paramLong);
        return;
      } 
    } 
    if (routingController == null)
      return; 
    requestCreateController(routingController, paramMediaRoute2Info, paramLong);
  }
  
  private List<MediaRoute2Info> filterRoutes(List<MediaRoute2Info> paramList, RouteDiscoveryPreference paramRouteDiscoveryPreference) {
    Stream<MediaRoute2Info> stream = paramList.stream();
    _$$Lambda$MediaRouter2$y_xhYB6qpwaV9__Rsqi3qIBxRfE _$$Lambda$MediaRouter2$y_xhYB6qpwaV9__Rsqi3qIBxRfE = new _$$Lambda$MediaRouter2$y_xhYB6qpwaV9__Rsqi3qIBxRfE(paramRouteDiscoveryPreference);
    stream = stream.filter(_$$Lambda$MediaRouter2$y_xhYB6qpwaV9__Rsqi3qIBxRfE);
    return stream.collect((Collector)Collectors.toList());
  }
  
  private void notifyRoutesAdded(List<MediaRoute2Info> paramList) {
    for (RouteCallbackRecord routeCallbackRecord : this.mRouteCallbackRecords) {
      List<MediaRoute2Info> list = filterRoutes(paramList, routeCallbackRecord.mPreference);
      if (!list.isEmpty())
        routeCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$n9IPRYiEy7tiocaw8mnctKBTnhw(routeCallbackRecord, list)); 
    } 
  }
  
  private void notifyRoutesRemoved(List<MediaRoute2Info> paramList) {
    for (RouteCallbackRecord routeCallbackRecord : this.mRouteCallbackRecords) {
      List<MediaRoute2Info> list = filterRoutes(paramList, routeCallbackRecord.mPreference);
      if (!list.isEmpty())
        routeCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$_IpRbaHzBEh1I7uHKDK98aRUJUU(routeCallbackRecord, list)); 
    } 
  }
  
  private void notifyRoutesChanged(List<MediaRoute2Info> paramList) {
    for (RouteCallbackRecord routeCallbackRecord : this.mRouteCallbackRecords) {
      List<MediaRoute2Info> list = filterRoutes(paramList, routeCallbackRecord.mPreference);
      if (!list.isEmpty())
        routeCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$VQ_2MhZu1QeTu4Kecl12ipaxmO0(routeCallbackRecord, list)); 
    } 
  }
  
  private void notifyTransfer(RoutingController paramRoutingController1, RoutingController paramRoutingController2) {
    for (TransferCallbackRecord transferCallbackRecord : this.mTransferCallbackRecords)
      transferCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$vs7yUmqlI_cAA10wFKeA9IrdOJc(transferCallbackRecord, paramRoutingController1, paramRoutingController2)); 
  }
  
  private void notifyTransferFailure(MediaRoute2Info paramMediaRoute2Info) {
    for (TransferCallbackRecord transferCallbackRecord : this.mTransferCallbackRecords)
      transferCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$X3ZOrCeYw3vfbvKGRXNPtHNaJ_I(transferCallbackRecord, paramMediaRoute2Info)); 
  }
  
  private void notifyStop(RoutingController paramRoutingController) {
    for (TransferCallbackRecord transferCallbackRecord : this.mTransferCallbackRecords)
      transferCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$hgifIdYD8PPhrPbfzt1coc9Af6w(transferCallbackRecord, paramRoutingController)); 
  }
  
  private void notifyControllerUpdated(RoutingController paramRoutingController) {
    for (ControllerCallbackRecord controllerCallbackRecord : this.mControllerCallbackRecords)
      controllerCallbackRecord.mExecutor.execute(new _$$Lambda$MediaRouter2$lHWSTgRtklDut3GYEB4IKwyIFeY(controllerCallbackRecord, paramRoutingController)); 
  }
  
  public static abstract class RouteCallback {
    public void onRoutesAdded(List<MediaRoute2Info> param1List) {}
    
    public void onRoutesRemoved(List<MediaRoute2Info> param1List) {}
    
    public void onRoutesChanged(List<MediaRoute2Info> param1List) {}
  }
  
  public static abstract class TransferCallback {
    public void onTransfer(MediaRouter2.RoutingController param1RoutingController1, MediaRouter2.RoutingController param1RoutingController2) {}
    
    public void onTransferFailure(MediaRoute2Info param1MediaRoute2Info) {}
    
    public void onStop(MediaRouter2.RoutingController param1RoutingController) {}
  }
  
  public static abstract class ControllerCallback {
    public void onControllerUpdated(MediaRouter2.RoutingController param1RoutingController) {}
  }
  
  public class RoutingController {
    private static final int CONTROLLER_STATE_ACTIVE = 1;
    
    private static final int CONTROLLER_STATE_RELEASED = 3;
    
    private static final int CONTROLLER_STATE_RELEASING = 2;
    
    private static final int CONTROLLER_STATE_UNKNOWN = 0;
    
    private final Object mControllerLock = new Object();
    
    private RoutingSessionInfo mSessionInfo;
    
    private int mState;
    
    final MediaRouter2 this$0;
    
    RoutingController(RoutingSessionInfo param1RoutingSessionInfo) {
      this.mSessionInfo = param1RoutingSessionInfo;
      this.mState = 1;
    }
    
    public String getId() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getId();
      } 
    }
    
    public String getOriginalId() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getOriginalId();
      } 
    }
    
    public Bundle getControlHints() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getControlHints();
      } 
    }
    
    public List<MediaRoute2Info> getSelectedRoutes() {
      synchronized (this.mControllerLock) {
        List<String> list = this.mSessionInfo.getSelectedRoutes();
        return getRoutesWithIds(list);
      } 
    }
    
    public List<MediaRoute2Info> getSelectableRoutes() {
      synchronized (this.mControllerLock) {
        List<String> list = this.mSessionInfo.getSelectableRoutes();
        return getRoutesWithIds(list);
      } 
    }
    
    public List<MediaRoute2Info> getDeselectableRoutes() {
      synchronized (this.mControllerLock) {
        List<String> list = this.mSessionInfo.getDeselectableRoutes();
        return getRoutesWithIds(list);
      } 
    }
    
    public int getVolumeHandling() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getVolumeHandling();
      } 
    }
    
    public int getVolumeMax() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getVolumeMax();
      } 
    }
    
    public int getVolume() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo.getVolume();
      } 
    }
    
    public boolean isReleased() {
      synchronized (this.mControllerLock) {
        boolean bool;
        if (this.mState == 3) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      } 
    }
    
    public void selectRoute(MediaRoute2Info param1MediaRoute2Info) {
      Objects.requireNonNull(param1MediaRoute2Info, "route must not be null");
      if (isReleased()) {
        Log.w("MR2", "selectRoute: Called on released controller. Ignoring.");
        return;
      } 
      List<MediaRoute2Info> list = getSelectedRoutes();
      if (MediaRouter2.checkRouteListContainsRouteId(list, param1MediaRoute2Info.getId())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring selecting a route that is already selected. route=");
        stringBuilder.append(param1MediaRoute2Info);
        Log.w("MR2", stringBuilder.toString());
        return;
      } 
      list = getSelectableRoutes();
      if (!MediaRouter2.checkRouteListContainsRouteId(list, param1MediaRoute2Info.getId())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring selecting a non-selectable route=");
        stringBuilder.append(param1MediaRoute2Info);
        Log.w("MR2", stringBuilder.toString());
        return;
      } 
      synchronized (MediaRouter2.sRouterLock) {
        MediaRouter2.MediaRouter2Stub mediaRouter2Stub = MediaRouter2.this.mStub;
        if (mediaRouter2Stub != null)
          try {
            MediaRouter2.this.mMediaRouterService.selectRouteWithRouter2(mediaRouter2Stub, getId(), param1MediaRoute2Info);
          } catch (RemoteException remoteException) {
            Log.e("MR2", "Unable to select route for session.", (Throwable)remoteException);
          }  
        return;
      } 
    }
    
    public void deselectRoute(MediaRoute2Info param1MediaRoute2Info) {
      Objects.requireNonNull(param1MediaRoute2Info, "route must not be null");
      if (isReleased()) {
        Log.w("MR2", "deselectRoute: called on released controller. Ignoring.");
        return;
      } 
      List<MediaRoute2Info> list = getSelectedRoutes();
      if (!MediaRouter2.checkRouteListContainsRouteId(list, param1MediaRoute2Info.getId())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring deselecting a route that is not selected. route=");
        stringBuilder.append(param1MediaRoute2Info);
        Log.w("MR2", stringBuilder.toString());
        return;
      } 
      list = getDeselectableRoutes();
      if (!MediaRouter2.checkRouteListContainsRouteId(list, param1MediaRoute2Info.getId())) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Ignoring deselecting a non-deselectable route=");
        stringBuilder.append(param1MediaRoute2Info);
        Log.w("MR2", stringBuilder.toString());
        return;
      } 
      synchronized (MediaRouter2.sRouterLock) {
        MediaRouter2.MediaRouter2Stub mediaRouter2Stub = MediaRouter2.this.mStub;
        if (mediaRouter2Stub != null)
          try {
            MediaRouter2.this.mMediaRouterService.deselectRouteWithRouter2(mediaRouter2Stub, getId(), param1MediaRoute2Info);
          } catch (RemoteException remoteException) {
            Log.e("MR2", "Unable to deselect route from session.", (Throwable)remoteException);
          }  
        return;
      } 
    }
    
    void transferToRoute(MediaRoute2Info param1MediaRoute2Info) {
      Objects.requireNonNull(param1MediaRoute2Info, "route must not be null");
      synchronized (this.mControllerLock) {
        if (isReleased()) {
          Log.w("MR2", "transferToRoute: Called on released controller. Ignoring.");
          return;
        } 
        if (!this.mSessionInfo.getTransferableRoutes().contains(param1MediaRoute2Info.getId())) {
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Ignoring transferring to a non-transferable route=");
          stringBuilder.append(param1MediaRoute2Info);
          Log.w("MR2", stringBuilder.toString());
          return;
        } 
        synchronized (MediaRouter2.sRouterLock) {
          MediaRouter2.MediaRouter2Stub mediaRouter2Stub = MediaRouter2.this.mStub;
          if (mediaRouter2Stub != null)
            try {
              MediaRouter2.this.mMediaRouterService.transferToRouteWithRouter2(mediaRouter2Stub, getId(), param1MediaRoute2Info);
            } catch (RemoteException remoteException) {
              Log.e("MR2", "Unable to transfer to route for session.", (Throwable)remoteException);
            }  
          return;
        } 
      } 
    }
    
    public void setVolume(int param1Int) {
      if (getVolumeHandling() == 0) {
        Log.w("MR2", "setVolume: The routing session has fixed volume. Ignoring.");
        return;
      } 
      if (param1Int < 0 || param1Int > getVolumeMax()) {
        Log.w("MR2", "setVolume: The target volume is out of range. Ignoring");
        return;
      } 
      if (isReleased()) {
        Log.w("MR2", "setVolume: Called on released controller. Ignoring.");
        return;
      } 
      synchronized (MediaRouter2.sRouterLock) {
        MediaRouter2.MediaRouter2Stub mediaRouter2Stub = MediaRouter2.this.mStub;
        if (mediaRouter2Stub != null)
          try {
            MediaRouter2.this.mMediaRouterService.setSessionVolumeWithRouter2(mediaRouter2Stub, getId(), param1Int);
          } catch (RemoteException null) {
            Log.e("MR2", "setVolume: Failed to deliver request.", (Throwable)null);
          }  
        return;
      } 
    }
    
    public void release() {
      releaseInternal(true);
    }
    
    boolean scheduleRelease() {
      synchronized (this.mControllerLock) {
        if (this.mState != 1)
          return false; 
        this.mState = 2;
        synchronized (MediaRouter2.sRouterLock) {
          if (!MediaRouter2.this.mNonSystemRoutingControllers.remove(getId(), this))
            return true; 
          MediaRouter2.this.mHandler.postDelayed(new _$$Lambda$zC5PIaJz8xUU_soxzzOU1rDcAD0(this), 30000L);
          return true;
        } 
      } 
    }
    
    void releaseInternal(boolean param1Boolean) {
      synchronized (this.mControllerLock) {
        if (this.mState == 3) {
          if (MediaRouter2.DEBUG)
            Log.d("MR2", "releaseInternal: Called on released controller. Ignoring."); 
          return;
        } 
        int i = this.mState;
        boolean bool = true;
        if (i != 1)
          bool = false; 
        this.mState = 3;
        synchronized (MediaRouter2.sRouterLock) {
          MediaRouter2.this.mNonSystemRoutingControllers.remove(getId(), this);
          if (param1Boolean) {
            null = MediaRouter2.this.mStub;
            if (null != null)
              try {
                MediaRouter2.this.mMediaRouterService.releaseSessionWithRouter2(MediaRouter2.this.mStub, getId());
              } catch (RemoteException remoteException) {
                Log.e("MR2", "Unable to release session", (Throwable)remoteException);
              }  
          } 
          if (bool)
            MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$MediaRouter2$RoutingController$mzVQ8I_o_lraqQmkSctESiC2aiI.INSTANCE, MediaRouter2.this, this)); 
          if (MediaRouter2.this.mRouteCallbackRecords.isEmpty() && MediaRouter2.this.mNonSystemRoutingControllers.isEmpty()) {
            null = MediaRouter2.this.mStub;
            if (null != null) {
              try {
                MediaRouter2.this.mMediaRouterService.unregisterRouter2(MediaRouter2.this.mStub);
              } catch (RemoteException remoteException) {
                Log.e("MR2", "releaseInternal: Unable to unregister media router.", (Throwable)remoteException);
              } 
              MediaRouter2.this.mStub = null;
            } 
          } 
          return;
        } 
      } 
    }
    
    public String toString() {
      Stream<MediaRoute2Info> stream1 = getSelectedRoutes().stream();
      -$.Lambda.Jl1VWT2dPpodkj8vkFOye7iVD0Y jl1VWT2dPpodkj8vkFOye7iVD0Y1 = _$$Lambda$Jl1VWT2dPpodkj8vkFOye7iVD0Y.INSTANCE;
      List list1 = (List)stream1.map((Function<? super MediaRoute2Info, ?>)jl1VWT2dPpodkj8vkFOye7iVD0Y1).collect(Collectors.toList());
      Stream<MediaRoute2Info> stream2 = getSelectableRoutes().stream();
      jl1VWT2dPpodkj8vkFOye7iVD0Y1 = _$$Lambda$Jl1VWT2dPpodkj8vkFOye7iVD0Y.INSTANCE;
      List list2 = (List)stream2.map((Function<? super MediaRoute2Info, ?>)jl1VWT2dPpodkj8vkFOye7iVD0Y1).collect(Collectors.toList());
      stream2 = getDeselectableRoutes().stream();
      -$.Lambda.Jl1VWT2dPpodkj8vkFOye7iVD0Y jl1VWT2dPpodkj8vkFOye7iVD0Y2 = _$$Lambda$Jl1VWT2dPpodkj8vkFOye7iVD0Y.INSTANCE;
      List list3 = (List)stream2.map((Function<? super MediaRoute2Info, ?>)jl1VWT2dPpodkj8vkFOye7iVD0Y2).collect(Collectors.toList());
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("RoutingController{ ");
      stringBuilder2.append("id=");
      stringBuilder2.append(getId());
      stringBuilder2.append(", selectedRoutes={");
      stringBuilder2.append(list1);
      stringBuilder2.append("}");
      stringBuilder2.append(", selectableRoutes={");
      stringBuilder2.append(list2);
      stringBuilder2.append("}");
      stringBuilder2.append(", deselectableRoutes={");
      stringBuilder2.append(list3);
      stringBuilder2.append("}");
      StringBuilder stringBuilder1 = stringBuilder2.append(" }");
      return stringBuilder1.toString();
    }
    
    RoutingSessionInfo getRoutingSessionInfo() {
      synchronized (this.mControllerLock) {
        return this.mSessionInfo;
      } 
    }
    
    void setRoutingSessionInfo(RoutingSessionInfo param1RoutingSessionInfo) {
      synchronized (this.mControllerLock) {
        this.mSessionInfo = param1RoutingSessionInfo;
        return;
      } 
    }
    
    private List<MediaRoute2Info> getRoutesWithIds(List<String> param1List) {
      synchronized (MediaRouter2.sRouterLock) {
        Stream<String> stream1 = param1List.stream();
        Map<String, MediaRoute2Info> map = MediaRouter2.this.mRoutes;
        Objects.requireNonNull(map);
        _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA = new _$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA();
        this(map);
        Stream<?> stream = stream1.map(_$$Lambda$0KjW5dKQDNOkejDzbQIfcK35kWA);
        -$.Lambda.fo3R-Przkq5mg2wxR3lAN3cgNY fo3R-Przkq5mg2wxR3lAN3cgNY = _$$Lambda$8fo3R_Przkq5mg2wxR3lAN3cgNY.INSTANCE;
        stream = stream.filter((Predicate<?>)fo3R-Przkq5mg2wxR3lAN3cgNY);
        return stream.collect((Collector)Collectors.toList());
      } 
    }
  }
  
  class SystemRoutingController extends RoutingController {
    final MediaRouter2 this$0;
    
    SystemRoutingController(RoutingSessionInfo param1RoutingSessionInfo) {
      super(param1RoutingSessionInfo);
    }
    
    public boolean isReleased() {
      return false;
    }
    
    boolean scheduleRelease() {
      return true;
    }
    
    void releaseInternal(boolean param1Boolean) {}
  }
  
  static final class RouteCallbackRecord {
    public final Executor mExecutor;
    
    public final RouteDiscoveryPreference mPreference;
    
    public final MediaRouter2.RouteCallback mRouteCallback;
    
    RouteCallbackRecord(Executor param1Executor, MediaRouter2.RouteCallback param1RouteCallback, RouteDiscoveryPreference param1RouteDiscoveryPreference) {
      this.mRouteCallback = param1RouteCallback;
      this.mExecutor = param1Executor;
      this.mPreference = param1RouteDiscoveryPreference;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof RouteCallbackRecord))
        return false; 
      if (this.mRouteCallback != ((RouteCallbackRecord)param1Object).mRouteCallback)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mRouteCallback.hashCode();
    }
  }
  
  static final class TransferCallbackRecord {
    public final Executor mExecutor;
    
    public final MediaRouter2.TransferCallback mTransferCallback;
    
    TransferCallbackRecord(Executor param1Executor, MediaRouter2.TransferCallback param1TransferCallback) {
      this.mTransferCallback = param1TransferCallback;
      this.mExecutor = param1Executor;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof TransferCallbackRecord))
        return false; 
      if (this.mTransferCallback != ((TransferCallbackRecord)param1Object).mTransferCallback)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mTransferCallback.hashCode();
    }
  }
  
  static final class ControllerCallbackRecord {
    public final MediaRouter2.ControllerCallback mCallback;
    
    public final Executor mExecutor;
    
    ControllerCallbackRecord(Executor param1Executor, MediaRouter2.ControllerCallback param1ControllerCallback) {
      this.mCallback = param1ControllerCallback;
      this.mExecutor = param1Executor;
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = true;
      if (this == param1Object)
        return true; 
      if (!(param1Object instanceof ControllerCallbackRecord))
        return false; 
      if (this.mCallback != ((ControllerCallbackRecord)param1Object).mCallback)
        bool = false; 
      return bool;
    }
    
    public int hashCode() {
      return this.mCallback.hashCode();
    }
  }
  
  static final class ControllerCreationRequest {
    public final long mManagerRequestId;
    
    public final MediaRouter2.RoutingController mOldController;
    
    public final int mRequestId;
    
    public final MediaRoute2Info mRoute;
    
    ControllerCreationRequest(int param1Int, long param1Long, MediaRoute2Info param1MediaRoute2Info, MediaRouter2.RoutingController param1RoutingController) {
      this.mRequestId = param1Int;
      this.mManagerRequestId = param1Long;
      Objects.requireNonNull(param1MediaRoute2Info, "route must not be null");
      this.mRoute = param1MediaRoute2Info;
      Objects.requireNonNull(param1RoutingController, "oldController must not be null");
      this.mOldController = param1RoutingController;
    }
  }
  
  class MediaRouter2Stub extends IMediaRouter2.Stub {
    final MediaRouter2 this$0;
    
    public void notifyRouterRegistered(List<MediaRoute2Info> param1List, RoutingSessionInfo param1RoutingSessionInfo) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((TriConsumer)_$$Lambda$5cJIARwoi3XVzgD4EVyArqlU1tk.INSTANCE, MediaRouter2.this, param1List, param1RoutingSessionInfo));
    }
    
    public void notifyRoutesAdded(List<MediaRoute2Info> param1List) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$AbDO54d92mSjDgTzfP_QPLAC0d0.INSTANCE, MediaRouter2.this, param1List));
    }
    
    public void notifyRoutesRemoved(List<MediaRoute2Info> param1List) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$eoeWbdSQfT6O05r1IQFokK3xLq4.INSTANCE, MediaRouter2.this, param1List));
    }
    
    public void notifyRoutesChanged(List<MediaRoute2Info> param1List) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$Zj0XQI3Pye_OEtgtVI___O0S_Xc.INSTANCE, MediaRouter2.this, param1List));
    }
    
    public void notifySessionCreated(int param1Int, RoutingSessionInfo param1RoutingSessionInfo) {
      Handler handler = MediaRouter2.this.mHandler;
      -$.Lambda.qEgKIH7rwGEsgQxqBbumuZ83YpU qEgKIH7rwGEsgQxqBbumuZ83YpU = _$$Lambda$qEgKIH7rwGEsgQxqBbumuZ83YpU.INSTANCE;
      MediaRouter2 mediaRouter2 = MediaRouter2.this;
      handler.sendMessage(PooledLambda.obtainMessage((TriConsumer)qEgKIH7rwGEsgQxqBbumuZ83YpU, mediaRouter2, Integer.valueOf(param1Int), param1RoutingSessionInfo));
    }
    
    public void notifySessionInfoChanged(RoutingSessionInfo param1RoutingSessionInfo) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$TMiu0kKTdfB9WFVB3w_hG3TiXxU.INSTANCE, MediaRouter2.this, param1RoutingSessionInfo));
    }
    
    public void notifySessionReleased(RoutingSessionInfo param1RoutingSessionInfo) {
      MediaRouter2.this.mHandler.sendMessage(PooledLambda.obtainMessage((BiConsumer)_$$Lambda$JoIE2UKoTQNlDgxmnd4L0xMFxDc.INSTANCE, MediaRouter2.this, param1RoutingSessionInfo));
    }
    
    public void requestCreateSessionByManager(long param1Long, RoutingSessionInfo param1RoutingSessionInfo, MediaRoute2Info param1MediaRoute2Info) {
      Handler handler = MediaRouter2.this.mHandler;
      -$.Lambda.AyyBjjVvb4GbCZOsbbK9KlXkpI ayyBjjVvb4GbCZOsbbK9KlXkpI = _$$Lambda$9AyyBjjVvb4GbCZOsbbK9KlXkpI.INSTANCE;
      MediaRouter2 mediaRouter2 = MediaRouter2.this;
      handler.sendMessage(PooledLambda.obtainMessage((QuadConsumer)ayyBjjVvb4GbCZOsbbK9KlXkpI, mediaRouter2, param1RoutingSessionInfo, param1MediaRoute2Info, Long.valueOf(param1Long)));
    }
  }
  
  public static interface OnGetControllerHintsListener {
    Bundle onGetControllerHints(MediaRoute2Info param1MediaRoute2Info);
  }
}
