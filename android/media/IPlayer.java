package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IPlayer extends IInterface {
  void applyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) throws RemoteException;
  
  void pause() throws RemoteException;
  
  void setPan(float paramFloat) throws RemoteException;
  
  void setStartDelayMs(int paramInt) throws RemoteException;
  
  void setVolume(float paramFloat) throws RemoteException;
  
  void start() throws RemoteException;
  
  void stop() throws RemoteException;
  
  class Default implements IPlayer {
    public void start() throws RemoteException {}
    
    public void pause() throws RemoteException {}
    
    public void stop() throws RemoteException {}
    
    public void setVolume(float param1Float) throws RemoteException {}
    
    public void setPan(float param1Float) throws RemoteException {}
    
    public void setStartDelayMs(int param1Int) throws RemoteException {}
    
    public void applyVolumeShaper(VolumeShaper.Configuration param1Configuration, VolumeShaper.Operation param1Operation) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IPlayer {
    private static final String DESCRIPTOR = "android.media.IPlayer";
    
    static final int TRANSACTION_applyVolumeShaper = 7;
    
    static final int TRANSACTION_pause = 2;
    
    static final int TRANSACTION_setPan = 5;
    
    static final int TRANSACTION_setStartDelayMs = 6;
    
    static final int TRANSACTION_setVolume = 4;
    
    static final int TRANSACTION_start = 1;
    
    static final int TRANSACTION_stop = 3;
    
    public Stub() {
      attachInterface(this, "android.media.IPlayer");
    }
    
    public static IPlayer asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IPlayer");
      if (iInterface != null && iInterface instanceof IPlayer)
        return (IPlayer)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "applyVolumeShaper";
        case 6:
          return "setStartDelayMs";
        case 5:
          return "setPan";
        case 4:
          return "setVolume";
        case 3:
          return "stop";
        case 2:
          return "pause";
        case 1:
          break;
      } 
      return "start";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        float f;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            if (param1Parcel1.readInt() != 0) {
              VolumeShaper.Configuration configuration = VolumeShaper.Configuration.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel2 = null;
            } 
            if (param1Parcel1.readInt() != 0) {
              VolumeShaper.Operation operation = VolumeShaper.Operation.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            applyVolumeShaper((VolumeShaper.Configuration)param1Parcel2, (VolumeShaper.Operation)param1Parcel1);
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            param1Int1 = param1Parcel1.readInt();
            setStartDelayMs(param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            f = param1Parcel1.readFloat();
            setPan(f);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            f = param1Parcel1.readFloat();
            setVolume(f);
            return true;
          case 3:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            stop();
            return true;
          case 2:
            param1Parcel1.enforceInterface("android.media.IPlayer");
            pause();
            return true;
          case 1:
            break;
        } 
        param1Parcel1.enforceInterface("android.media.IPlayer");
        start();
        return true;
      } 
      param1Parcel2.writeString("android.media.IPlayer");
      return true;
    }
    
    private static class Proxy implements IPlayer {
      public static IPlayer sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IPlayer";
      }
      
      public void start() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().start();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void pause() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().pause();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void stop() throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().stop();
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVolume(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().setVolume(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setPan(float param2Float) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          parcel.writeFloat(param2Float);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().setPan(param2Float);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setStartDelayMs(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().setStartDelayMs(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void applyVolumeShaper(VolumeShaper.Configuration param2Configuration, VolumeShaper.Operation param2Operation) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IPlayer");
          if (param2Configuration != null) {
            parcel.writeInt(1);
            param2Configuration.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          if (param2Operation != null) {
            parcel.writeInt(1);
            param2Operation.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IPlayer.Stub.getDefaultImpl() != null) {
            IPlayer.Stub.getDefaultImpl().applyVolumeShaper(param2Configuration, param2Operation);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IPlayer param1IPlayer) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IPlayer != null) {
          Proxy.sDefaultImpl = param1IPlayer;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IPlayer getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
