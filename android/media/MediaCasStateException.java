package android.media;

public class MediaCasStateException extends IllegalStateException {
  private final String mDiagnosticInfo;
  
  private final int mErrorCode;
  
  private MediaCasStateException(int paramInt, String paramString1, String paramString2) {
    super(paramString1);
    this.mErrorCode = paramInt;
    this.mDiagnosticInfo = paramString2;
  }
  
  static void throwExceptionIfNeeded(int paramInt) {
    throwExceptionIfNeeded(paramInt, null);
  }
  
  static void throwExceptionIfNeeded(int paramInt, String paramString) {
    if (paramInt == 0)
      return; 
    if (paramInt != 6) {
      switch (paramInt) {
        default:
          str = "Unknown CAS state exception";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 21:
          str = "Rebooting";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 20:
          str = "Blackout";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 19:
          str = "Card Invalid";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 18:
          str = "Card Muted";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 17:
          str = "No Card";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 16:
          str = "Need Pairing";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 15:
          str = "Need Activation";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 14:
          str = "General CAS error";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 13:
          str = "Decrypt error";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 12:
          str = "Not initialized";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 10:
          str = "Tamper detected";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 9:
          str = "Insufficient output protection";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 5:
          str = "Invalid CAS state";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 4:
          str = "Unsupported scheme or data format";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 3:
          str = "Session not opened";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 2:
          str = "License expired";
          throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
        case 1:
          break;
      } 
      String str = "No license";
      throw new MediaCasStateException(paramInt, paramString, String.format("%s (err=%d)", new Object[] { str, Integer.valueOf(paramInt) }));
    } 
    throw new IllegalArgumentException();
  }
  
  public int getErrorCode() {
    return this.mErrorCode;
  }
  
  public String getDiagnosticInfo() {
    return this.mDiagnosticInfo;
  }
}
