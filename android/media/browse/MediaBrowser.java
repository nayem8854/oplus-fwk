package android.media.browse;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ParceledListSlice;
import android.media.MediaDescription;
import android.media.session.MediaSession;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.os.ResultReceiver;
import android.service.media.IMediaBrowserService;
import android.service.media.IMediaBrowserServiceCallbacks;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public final class MediaBrowser {
  private final Handler mHandler = new Handler();
  
  private final ArrayMap<String, Subscription> mSubscriptions = new ArrayMap();
  
  private volatile int mState = 1;
  
  private static final int CONNECT_STATE_CONNECTED = 3;
  
  private static final int CONNECT_STATE_CONNECTING = 2;
  
  private static final int CONNECT_STATE_DISCONNECTED = 1;
  
  private static final int CONNECT_STATE_DISCONNECTING = 0;
  
  private static final int CONNECT_STATE_SUSPENDED = 4;
  
  private static final boolean DBG = false;
  
  public static final String EXTRA_PAGE = "android.media.browse.extra.PAGE";
  
  public static final String EXTRA_PAGE_SIZE = "android.media.browse.extra.PAGE_SIZE";
  
  private static final String TAG = "MediaBrowser";
  
  private final ConnectionCallback mCallback;
  
  private final Context mContext;
  
  private volatile Bundle mExtras;
  
  private volatile MediaSession.Token mMediaSessionToken;
  
  private final Bundle mRootHints;
  
  private volatile String mRootId;
  
  private IMediaBrowserService mServiceBinder;
  
  private IMediaBrowserServiceCallbacks mServiceCallbacks;
  
  private final ComponentName mServiceComponent;
  
  private MediaServiceConnection mServiceConnection;
  
  public MediaBrowser(Context paramContext, ComponentName paramComponentName, ConnectionCallback paramConnectionCallback, Bundle paramBundle) {
    if (paramContext != null) {
      if (paramComponentName != null) {
        if (paramConnectionCallback != null) {
          Bundle bundle;
          this.mContext = paramContext;
          this.mServiceComponent = paramComponentName;
          this.mCallback = paramConnectionCallback;
          if (paramBundle == null) {
            paramContext = null;
          } else {
            bundle = new Bundle(paramBundle);
          } 
          this.mRootHints = bundle;
          return;
        } 
        throw new IllegalArgumentException("connection callback must not be null");
      } 
      throw new IllegalArgumentException("service component must not be null");
    } 
    throw new IllegalArgumentException("context must not be null");
  }
  
  public void connect() {
    if (this.mState == 0 || this.mState == 1) {
      this.mState = 2;
      this.mHandler.post(new Runnable() {
            final MediaBrowser this$0;
            
            public void run() {
              if (MediaBrowser.this.mState == 0)
                return; 
              MediaBrowser.access$002(MediaBrowser.this, 2);
              if (MediaBrowser.this.mServiceBinder == null) {
                if (MediaBrowser.this.mServiceCallbacks == null) {
                  Intent intent = new Intent("android.media.browse.MediaBrowserService");
                  intent.setComponent(MediaBrowser.this.mServiceComponent);
                  MediaBrowser mediaBrowser2 = MediaBrowser.this;
                  MediaBrowser.access$402(mediaBrowser2, new MediaBrowser.MediaServiceConnection());
                  boolean bool = false;
                  try {
                    boolean bool1 = MediaBrowser.this.mContext.bindService(intent, MediaBrowser.this.mServiceConnection, 1);
                  } catch (Exception exception) {
                    StringBuilder stringBuilder2 = new StringBuilder();
                    stringBuilder2.append("Failed binding to service ");
                    stringBuilder2.append(MediaBrowser.this.mServiceComponent);
                    Log.e("MediaBrowser", stringBuilder2.toString());
                  } 
                  if (!bool) {
                    MediaBrowser.this.forceCloseConnection();
                    MediaBrowser.this.mCallback.onConnectionFailed();
                  } 
                  return;
                } 
                StringBuilder stringBuilder1 = new StringBuilder();
                stringBuilder1.append("mServiceCallbacks should be null. Instead it is ");
                MediaBrowser mediaBrowser1 = MediaBrowser.this;
                stringBuilder1.append(mediaBrowser1.mServiceCallbacks);
                throw new RuntimeException(stringBuilder1.toString());
              } 
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("mServiceBinder should be null. Instead it is ");
              MediaBrowser mediaBrowser = MediaBrowser.this;
              stringBuilder.append(mediaBrowser.mServiceBinder);
              throw new RuntimeException(stringBuilder.toString());
            }
          });
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("connect() called while neither disconnecting nor disconnected (state=");
    int i = this.mState;
    stringBuilder.append(getStateLabel(i));
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void disconnect() {
    this.mState = 0;
    this.mHandler.post(new Runnable() {
          final MediaBrowser this$0;
          
          public void run() {
            if (MediaBrowser.this.mServiceCallbacks != null)
              try {
                MediaBrowser.this.mServiceBinder.disconnect(MediaBrowser.this.mServiceCallbacks);
              } catch (RemoteException remoteException) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("RemoteException during connect for ");
                stringBuilder.append(MediaBrowser.this.mServiceComponent);
                Log.w("MediaBrowser", stringBuilder.toString());
              }  
            int i = MediaBrowser.this.mState;
            MediaBrowser.this.forceCloseConnection();
            if (i != 0)
              MediaBrowser.access$002(MediaBrowser.this, i); 
          }
        });
  }
  
  private void forceCloseConnection() {
    MediaServiceConnection mediaServiceConnection = this.mServiceConnection;
    if (mediaServiceConnection != null)
      try {
        this.mContext.unbindService(mediaServiceConnection);
      } catch (IllegalArgumentException illegalArgumentException) {} 
    this.mState = 1;
    this.mServiceConnection = null;
    this.mServiceBinder = null;
    this.mServiceCallbacks = null;
    this.mRootId = null;
    this.mMediaSessionToken = null;
  }
  
  public boolean isConnected() {
    boolean bool;
    if (this.mState == 3) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public ComponentName getServiceComponent() {
    if (isConnected())
      return this.mServiceComponent; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getServiceComponent() called while not connected (state=");
    stringBuilder.append(this.mState);
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public String getRoot() {
    if (isConnected())
      return this.mRootId; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getRoot() called while not connected (state=");
    int i = this.mState;
    stringBuilder.append(getStateLabel(i));
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public Bundle getExtras() {
    if (isConnected())
      return this.mExtras; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getExtras() called while not connected (state=");
    int i = this.mState;
    stringBuilder.append(getStateLabel(i));
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public MediaSession.Token getSessionToken() {
    if (isConnected())
      return this.mMediaSessionToken; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("getSessionToken() called while not connected (state=");
    stringBuilder.append(this.mState);
    stringBuilder.append(")");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  public void subscribe(String paramString, SubscriptionCallback paramSubscriptionCallback) {
    subscribeInternal(paramString, null, paramSubscriptionCallback);
  }
  
  public void subscribe(String paramString, Bundle paramBundle, SubscriptionCallback paramSubscriptionCallback) {
    if (paramBundle != null) {
      subscribeInternal(paramString, new Bundle(paramBundle), paramSubscriptionCallback);
      return;
    } 
    throw new IllegalArgumentException("options cannot be null");
  }
  
  public void unsubscribe(String paramString) {
    unsubscribeInternal(paramString, null);
  }
  
  public void unsubscribe(String paramString, SubscriptionCallback paramSubscriptionCallback) {
    if (paramSubscriptionCallback != null) {
      unsubscribeInternal(paramString, paramSubscriptionCallback);
      return;
    } 
    throw new IllegalArgumentException("callback cannot be null");
  }
  
  public void getItem(final String mediaId, final ItemCallback cb) {
    if (!TextUtils.isEmpty(mediaId)) {
      if (cb != null) {
        if (this.mState != 3) {
          Log.i("MediaBrowser", "Not connected, unable to retrieve the MediaItem.");
          this.mHandler.post(new Runnable() {
                final MediaBrowser this$0;
                
                final MediaBrowser.ItemCallback val$cb;
                
                final String val$mediaId;
                
                public void run() {
                  cb.onError(mediaId);
                }
              });
          return;
        } 
        Object object = new Object(this, this.mHandler, cb, mediaId);
        try {
          this.mServiceBinder.getMediaItem(mediaId, (ResultReceiver)object, this.mServiceCallbacks);
        } catch (RemoteException remoteException) {
          Log.i("MediaBrowser", "Remote error getting media item.");
          this.mHandler.post(new Runnable() {
                final MediaBrowser this$0;
                
                final MediaBrowser.ItemCallback val$cb;
                
                final String val$mediaId;
                
                public void run() {
                  cb.onError(mediaId);
                }
              });
        } 
        return;
      } 
      throw new IllegalArgumentException("cb cannot be null.");
    } 
    throw new IllegalArgumentException("mediaId cannot be empty.");
  }
  
  private void subscribeInternal(String paramString, Bundle paramBundle, SubscriptionCallback paramSubscriptionCallback) {
    StringBuilder stringBuilder;
    if (!TextUtils.isEmpty(paramString)) {
      if (paramSubscriptionCallback != null) {
        Subscription subscription1 = (Subscription)this.mSubscriptions.get(paramString);
        Subscription subscription2 = subscription1;
        if (subscription1 == null) {
          subscription2 = new Subscription();
          this.mSubscriptions.put(paramString, subscription2);
        } 
        subscription2.putCallback(this.mContext, paramBundle, paramSubscriptionCallback);
        if (isConnected()) {
          if (paramBundle == null) {
            try {
              this.mServiceBinder.addSubscriptionDeprecated(paramString, this.mServiceCallbacks);
              this.mServiceBinder.addSubscription(paramString, paramSubscriptionCallback.mToken, paramBundle, this.mServiceCallbacks);
            } catch (RemoteException remoteException) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("addSubscription failed with RemoteException parentId=");
              stringBuilder.append(paramString);
              Log.d("MediaBrowser", stringBuilder.toString());
            } 
            return;
          } 
        } else {
          return;
        } 
      } else {
        throw new IllegalArgumentException("callback cannot be null");
      } 
    } else {
      throw new IllegalArgumentException("parentId cannot be empty.");
    } 
    this.mServiceBinder.addSubscription(paramString, paramSubscriptionCallback.mToken, (Bundle)stringBuilder, this.mServiceCallbacks);
  }
  
  private void unsubscribeInternal(String paramString, SubscriptionCallback paramSubscriptionCallback) {
    if (!TextUtils.isEmpty(paramString)) {
      Subscription subscription = (Subscription)this.mSubscriptions.get(paramString);
      if (subscription == null)
        return; 
      if (paramSubscriptionCallback == null) {
        try {
          if (isConnected()) {
            this.mServiceBinder.removeSubscriptionDeprecated(paramString, this.mServiceCallbacks);
            this.mServiceBinder.removeSubscription(paramString, null, this.mServiceCallbacks);
          } 
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("removeSubscription failed with RemoteException parentId=");
          stringBuilder.append(paramString);
          Log.d("MediaBrowser", stringBuilder.toString());
        } 
      } else {
        List<SubscriptionCallback> list = subscription.getCallbacks();
        List<Bundle> list1 = subscription.getOptionsList();
        for (int i = list.size() - 1; i >= 0; i--) {
          if (list.get(i) == paramSubscriptionCallback) {
            if (isConnected())
              this.mServiceBinder.removeSubscription(paramString, paramSubscriptionCallback.mToken, this.mServiceCallbacks); 
            list.remove(i);
            list1.remove(i);
          } 
        } 
      } 
      if (subscription.isEmpty() || paramSubscriptionCallback == null)
        this.mSubscriptions.remove(paramString); 
      return;
    } 
    throw new IllegalArgumentException("parentId cannot be empty.");
  }
  
  private static String getStateLabel(int paramInt) {
    if (paramInt != 0) {
      if (paramInt != 1) {
        if (paramInt != 2) {
          if (paramInt != 3) {
            if (paramInt != 4) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("UNKNOWN/");
              stringBuilder.append(paramInt);
              return stringBuilder.toString();
            } 
            return "CONNECT_STATE_SUSPENDED";
          } 
          return "CONNECT_STATE_CONNECTED";
        } 
        return "CONNECT_STATE_CONNECTING";
      } 
      return "CONNECT_STATE_DISCONNECTED";
    } 
    return "CONNECT_STATE_DISCONNECTING";
  }
  
  private void onServiceConnected(final IMediaBrowserServiceCallbacks callback, final String root, final MediaSession.Token session, final Bundle extra) {
    this.mHandler.post(new Runnable() {
          final MediaBrowser this$0;
          
          final IMediaBrowserServiceCallbacks val$callback;
          
          final Bundle val$extra;
          
          final String val$root;
          
          final MediaSession.Token val$session;
          
          public void run() {
            if (!MediaBrowser.this.isCurrent(callback, "onConnect"))
              return; 
            if (MediaBrowser.this.mState != 2) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("onConnect from service while mState=");
              MediaBrowser mediaBrowser = MediaBrowser.this;
              stringBuilder.append(MediaBrowser.getStateLabel(mediaBrowser.mState));
              stringBuilder.append("... ignoring");
              String str = stringBuilder.toString();
              Log.w("MediaBrowser", str);
              return;
            } 
            MediaBrowser.access$1102(MediaBrowser.this, root);
            MediaBrowser.access$1202(MediaBrowser.this, session);
            MediaBrowser.access$1302(MediaBrowser.this, extra);
            MediaBrowser.access$002(MediaBrowser.this, 3);
            MediaBrowser.this.mCallback.onConnected();
            for (Map.Entry entry : MediaBrowser.this.mSubscriptions.entrySet()) {
              String str = (String)entry.getKey();
              MediaBrowser.Subscription subscription = (MediaBrowser.Subscription)entry.getValue();
              List<MediaBrowser.SubscriptionCallback> list = subscription.getCallbacks();
              List<Bundle> list1 = subscription.getOptionsList();
              for (byte b = 0; b < list.size(); b++) {
                try {
                  IMediaBrowserService iMediaBrowserService = MediaBrowser.this.mServiceBinder;
                  Binder binder = ((MediaBrowser.SubscriptionCallback)list.get(b)).mToken;
                  Bundle bundle = list1.get(b);
                  IMediaBrowserServiceCallbacks iMediaBrowserServiceCallbacks = MediaBrowser.this.mServiceCallbacks;
                  iMediaBrowserService.addSubscription(str, binder, bundle, iMediaBrowserServiceCallbacks);
                } catch (RemoteException remoteException) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("addSubscription failed with RemoteException parentId=");
                  stringBuilder.append(str);
                  Log.d("MediaBrowser", stringBuilder.toString());
                } 
              } 
            } 
          }
        });
  }
  
  private void onConnectionFailed(final IMediaBrowserServiceCallbacks callback) {
    this.mHandler.post(new Runnable() {
          final MediaBrowser this$0;
          
          final IMediaBrowserServiceCallbacks val$callback;
          
          public void run() {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("onConnectFailed for ");
            stringBuilder.append(MediaBrowser.this.mServiceComponent);
            Log.e("MediaBrowser", stringBuilder.toString());
            if (!MediaBrowser.this.isCurrent(callback, "onConnectFailed"))
              return; 
            if (MediaBrowser.this.mState != 2) {
              StringBuilder stringBuilder1 = new StringBuilder();
              stringBuilder1.append("onConnect from service while mState=");
              MediaBrowser mediaBrowser = MediaBrowser.this;
              stringBuilder1.append(MediaBrowser.getStateLabel(mediaBrowser.mState));
              stringBuilder1.append("... ignoring");
              String str = stringBuilder1.toString();
              Log.w("MediaBrowser", str);
              return;
            } 
            MediaBrowser.this.forceCloseConnection();
            MediaBrowser.this.mCallback.onConnectionFailed();
          }
        });
  }
  
  private void onLoadChildren(final IMediaBrowserServiceCallbacks callback, final String parentId, final ParceledListSlice list, final Bundle options) {
    this.mHandler.post(new Runnable() {
          final MediaBrowser this$0;
          
          final IMediaBrowserServiceCallbacks val$callback;
          
          final ParceledListSlice val$list;
          
          final Bundle val$options;
          
          final String val$parentId;
          
          public void run() {
            if (!MediaBrowser.this.isCurrent(callback, "onLoadChildren"))
              return; 
            MediaBrowser.Subscription subscription = (MediaBrowser.Subscription)MediaBrowser.this.mSubscriptions.get(parentId);
            if (subscription != null) {
              MediaBrowser mediaBrowser = MediaBrowser.this;
              MediaBrowser.SubscriptionCallback subscriptionCallback = subscription.getCallback(mediaBrowser.mContext, options);
              if (subscriptionCallback != null) {
                List<MediaBrowser.MediaItem> list;
                ParceledListSlice parceledListSlice = list;
                if (parceledListSlice == null) {
                  parceledListSlice = null;
                } else {
                  list = parceledListSlice.getList();
                } 
                Bundle bundle = options;
                if (bundle == null) {
                  if (list == null) {
                    subscriptionCallback.onError(parentId);
                  } else {
                    subscriptionCallback.onChildrenLoaded(parentId, list);
                  } 
                } else if (list == null) {
                  subscriptionCallback.onError(parentId, bundle);
                } else {
                  subscriptionCallback.onChildrenLoaded(parentId, list, bundle);
                } 
                return;
              } 
            } 
          }
        });
  }
  
  private boolean isCurrent(IMediaBrowserServiceCallbacks paramIMediaBrowserServiceCallbacks, String paramString) {
    if (this.mServiceCallbacks != paramIMediaBrowserServiceCallbacks || this.mState == 0 || this.mState == 1) {
      if (this.mState != 0 && this.mState != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(paramString);
        stringBuilder.append(" for ");
        stringBuilder.append(this.mServiceComponent);
        stringBuilder.append(" with mServiceConnection=");
        stringBuilder.append(this.mServiceCallbacks);
        stringBuilder.append(" this=");
        stringBuilder.append(this);
        Log.i("MediaBrowser", stringBuilder.toString());
      } 
      return false;
    } 
    return true;
  }
  
  private ServiceCallbacks getNewServiceCallbacks() {
    return new ServiceCallbacks(this);
  }
  
  void dump() {
    Log.d("MediaBrowser", "MediaBrowser...");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("  mServiceComponent=");
    stringBuilder.append(this.mServiceComponent);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mCallback=");
    stringBuilder.append(this.mCallback);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mRootHints=");
    stringBuilder.append(this.mRootHints);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mState=");
    stringBuilder.append(getStateLabel(this.mState));
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServiceConnection=");
    stringBuilder.append(this.mServiceConnection);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServiceBinder=");
    stringBuilder.append(this.mServiceBinder);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mServiceCallbacks=");
    stringBuilder.append(this.mServiceCallbacks);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mRootId=");
    stringBuilder.append(this.mRootId);
    Log.d("MediaBrowser", stringBuilder.toString());
    stringBuilder = new StringBuilder();
    stringBuilder.append("  mMediaSessionToken=");
    stringBuilder.append(this.mMediaSessionToken);
    Log.d("MediaBrowser", stringBuilder.toString());
  }
  
  class MediaItem implements Parcelable {
    public MediaItem(MediaBrowser this$0, int param1Int) {
      if (this$0 != null) {
        if (!TextUtils.isEmpty(this$0.getMediaId())) {
          this.mFlags = param1Int;
          this.mDescription = (MediaDescription)this$0;
          return;
        } 
        throw new IllegalArgumentException("description must have a non-empty media id");
      } 
      throw new IllegalArgumentException("description cannot be null");
    }
    
    private MediaItem(MediaBrowser this$0) {
      this.mFlags = this$0.readInt();
      this.mDescription = MediaDescription.CREATOR.createFromParcel((Parcel)this$0);
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeInt(this.mFlags);
      this.mDescription.writeToParcel(param1Parcel, param1Int);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder("MediaItem{");
      stringBuilder.append("mFlags=");
      stringBuilder.append(this.mFlags);
      stringBuilder.append(", mDescription=");
      stringBuilder.append(this.mDescription);
      stringBuilder.append('}');
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<MediaItem> CREATOR = new Parcelable.Creator<MediaItem>() {
        public MediaBrowser.MediaItem createFromParcel(Parcel param2Parcel) {
          return new MediaBrowser.MediaItem();
        }
        
        public MediaBrowser.MediaItem[] newArray(int param2Int) {
          return new MediaBrowser.MediaItem[param2Int];
        }
      };
    
    public static final int FLAG_BROWSABLE = 1;
    
    public static final int FLAG_PLAYABLE = 2;
    
    private final MediaDescription mDescription;
    
    private final int mFlags;
    
    public int getFlags() {
      return this.mFlags;
    }
    
    public boolean isBrowsable() {
      int i = this.mFlags;
      boolean bool = true;
      if ((i & 0x1) == 0)
        bool = false; 
      return bool;
    }
    
    public boolean isPlayable() {
      boolean bool;
      if ((this.mFlags & 0x2) != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaDescription getDescription() {
      return this.mDescription;
    }
    
    public String getMediaId() {
      return this.mDescription.getMediaId();
    }
    
    @Retention(RetentionPolicy.SOURCE)
    class Flags implements Annotation {}
  }
  
  public static class ConnectionCallback {
    public void onConnected() {}
    
    public void onConnectionSuspended() {}
    
    public void onConnectionFailed() {}
  }
  
  public static abstract class SubscriptionCallback {
    Binder mToken = new Binder();
    
    public void onChildrenLoaded(String param1String, List<MediaBrowser.MediaItem> param1List) {}
    
    public void onChildrenLoaded(String param1String, List<MediaBrowser.MediaItem> param1List, Bundle param1Bundle) {}
    
    public void onError(String param1String) {}
    
    public void onError(String param1String, Bundle param1Bundle) {}
  }
  
  public static abstract class ItemCallback {
    public void onItemLoaded(MediaBrowser.MediaItem param1MediaItem) {}
    
    public void onError(String param1String) {}
  }
  
  class MediaServiceConnection implements ServiceConnection {
    final MediaBrowser this$0;
    
    private MediaServiceConnection() {}
    
    public void onServiceConnected(final ComponentName name, final IBinder binder) {
      postOrRun(new Runnable() {
            final MediaBrowser.MediaServiceConnection this$1;
            
            final IBinder val$binder;
            
            final ComponentName val$name;
            
            public void run() {
              if (!this.this$1.isCurrent("onServiceConnected"))
                return; 
              MediaBrowser.access$102(MediaBrowser.this, IMediaBrowserService.Stub.asInterface(binder));
              MediaBrowser.access$202(MediaBrowser.this, MediaBrowser.this.getNewServiceCallbacks());
              MediaBrowser.access$002(MediaBrowser.this, 2);
              try {
                IMediaBrowserService iMediaBrowserService = MediaBrowser.this.mServiceBinder;
                String str = MediaBrowser.this.mContext.getPackageName();
                Bundle bundle = MediaBrowser.this.mRootHints;
                MediaBrowser mediaBrowser = MediaBrowser.this;
                IMediaBrowserServiceCallbacks iMediaBrowserServiceCallbacks = mediaBrowser.mServiceCallbacks;
                iMediaBrowserService.connect(str, bundle, iMediaBrowserServiceCallbacks);
              } catch (RemoteException remoteException) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("RemoteException during connect for ");
                stringBuilder.append(MediaBrowser.this.mServiceComponent);
                Log.w("MediaBrowser", stringBuilder.toString());
              } 
            }
          });
    }
    
    public void onServiceDisconnected(final ComponentName name) {
      postOrRun(new Runnable() {
            final MediaBrowser.MediaServiceConnection this$1;
            
            final ComponentName val$name;
            
            public void run() {
              if (!this.this$1.isCurrent("onServiceDisconnected"))
                return; 
              MediaBrowser.access$102(MediaBrowser.this, null);
              MediaBrowser.access$202(MediaBrowser.this, null);
              MediaBrowser.access$002(MediaBrowser.this, 4);
              MediaBrowser.this.mCallback.onConnectionSuspended();
            }
          });
    }
    
    private void postOrRun(Runnable param1Runnable) {
      if (Thread.currentThread() == MediaBrowser.this.mHandler.getLooper().getThread()) {
        param1Runnable.run();
      } else {
        MediaBrowser.this.mHandler.post(param1Runnable);
      } 
    }
    
    private boolean isCurrent(String param1String) {
      if (MediaBrowser.this.mServiceConnection == this && MediaBrowser.this.mState != 0) {
        MediaBrowser mediaBrowser = MediaBrowser.this;
        if (mediaBrowser.mState != 1)
          return true; 
      } 
      if (MediaBrowser.this.mState != 0 && MediaBrowser.this.mState != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param1String);
        stringBuilder.append(" for ");
        stringBuilder.append(MediaBrowser.this.mServiceComponent);
        stringBuilder.append(" with mServiceConnection=");
        MediaBrowser mediaBrowser = MediaBrowser.this;
        stringBuilder.append(mediaBrowser.mServiceConnection);
        stringBuilder.append(" this=");
        stringBuilder.append(this);
        String str = stringBuilder.toString();
        Log.i("MediaBrowser", str);
      } 
      return false;
    }
  }
  
  class null implements Runnable {
    final MediaBrowser.MediaServiceConnection this$1;
    
    final IBinder val$binder;
    
    final ComponentName val$name;
    
    public void run() {
      if (!this.this$1.isCurrent("onServiceConnected"))
        return; 
      MediaBrowser.access$102(MediaBrowser.this, IMediaBrowserService.Stub.asInterface(binder));
      MediaBrowser.access$202(MediaBrowser.this, MediaBrowser.this.getNewServiceCallbacks());
      MediaBrowser.access$002(MediaBrowser.this, 2);
      try {
        IMediaBrowserService iMediaBrowserService = MediaBrowser.this.mServiceBinder;
        String str = MediaBrowser.this.mContext.getPackageName();
        Bundle bundle = MediaBrowser.this.mRootHints;
        MediaBrowser mediaBrowser = MediaBrowser.this;
        IMediaBrowserServiceCallbacks iMediaBrowserServiceCallbacks = mediaBrowser.mServiceCallbacks;
        iMediaBrowserService.connect(str, bundle, iMediaBrowserServiceCallbacks);
      } catch (RemoteException remoteException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("RemoteException during connect for ");
        stringBuilder.append(MediaBrowser.this.mServiceComponent);
        Log.w("MediaBrowser", stringBuilder.toString());
      } 
    }
  }
  
  class null implements Runnable {
    final MediaBrowser.MediaServiceConnection this$1;
    
    final ComponentName val$name;
    
    public void run() {
      if (!this.this$1.isCurrent("onServiceDisconnected"))
        return; 
      MediaBrowser.access$102(MediaBrowser.this, null);
      MediaBrowser.access$202(MediaBrowser.this, null);
      MediaBrowser.access$002(MediaBrowser.this, 4);
      MediaBrowser.this.mCallback.onConnectionSuspended();
    }
  }
  
  class ServiceCallbacks extends IMediaBrowserServiceCallbacks.Stub {
    private WeakReference<MediaBrowser> mMediaBrowser;
    
    ServiceCallbacks(MediaBrowser this$0) {
      this.mMediaBrowser = new WeakReference<>(this$0);
    }
    
    public void onConnect(String param1String, MediaSession.Token param1Token, Bundle param1Bundle) {
      MediaBrowser mediaBrowser = this.mMediaBrowser.get();
      if (mediaBrowser != null)
        mediaBrowser.onServiceConnected(this, param1String, param1Token, param1Bundle); 
    }
    
    public void onConnectFailed() {
      MediaBrowser mediaBrowser = this.mMediaBrowser.get();
      if (mediaBrowser != null)
        mediaBrowser.onConnectionFailed(this); 
    }
    
    public void onLoadChildren(String param1String, ParceledListSlice param1ParceledListSlice) {
      onLoadChildrenWithOptions(param1String, param1ParceledListSlice, null);
    }
    
    public void onLoadChildrenWithOptions(String param1String, ParceledListSlice param1ParceledListSlice, Bundle param1Bundle) {
      MediaBrowser mediaBrowser = this.mMediaBrowser.get();
      if (mediaBrowser != null)
        mediaBrowser.onLoadChildren(this, param1String, param1ParceledListSlice, param1Bundle); 
    }
  }
  
  private static class Subscription {
    private final List<MediaBrowser.SubscriptionCallback> mCallbacks;
    
    private final List<Bundle> mOptionsList;
    
    Subscription() {
      this.mCallbacks = new ArrayList<>();
      this.mOptionsList = new ArrayList<>();
    }
    
    public boolean isEmpty() {
      return this.mCallbacks.isEmpty();
    }
    
    public List<Bundle> getOptionsList() {
      return this.mOptionsList;
    }
    
    public List<MediaBrowser.SubscriptionCallback> getCallbacks() {
      return this.mCallbacks;
    }
    
    public MediaBrowser.SubscriptionCallback getCallback(Context param1Context, Bundle param1Bundle) {
      if (param1Bundle != null)
        param1Bundle.setClassLoader(param1Context.getClassLoader()); 
      for (byte b = 0; b < this.mOptionsList.size(); b++) {
        if (MediaBrowserUtils.areSameOptions(this.mOptionsList.get(b), param1Bundle))
          return this.mCallbacks.get(b); 
      } 
      return null;
    }
    
    public void putCallback(Context param1Context, Bundle param1Bundle, MediaBrowser.SubscriptionCallback param1SubscriptionCallback) {
      if (param1Bundle != null)
        param1Bundle.setClassLoader(param1Context.getClassLoader()); 
      for (byte b = 0; b < this.mOptionsList.size(); b++) {
        if (MediaBrowserUtils.areSameOptions(this.mOptionsList.get(b), param1Bundle)) {
          this.mCallbacks.set(b, param1SubscriptionCallback);
          return;
        } 
      } 
      this.mCallbacks.add(param1SubscriptionCallback);
      this.mOptionsList.add(param1Bundle);
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Flags {}
}
