package android.media.browse;

import android.os.Bundle;

public class MediaBrowserUtils {
  public static boolean areSameOptions(Bundle paramBundle1, Bundle paramBundle2) {
    boolean bool1 = true, bool2 = true;
    null = true;
    if (paramBundle1 == paramBundle2)
      return true; 
    if (paramBundle1 == null) {
      if (paramBundle2.getInt("android.media.browse.extra.PAGE", -1) != -1 || 
        paramBundle2.getInt("android.media.browse.extra.PAGE_SIZE", -1) != -1)
        null = false; 
      return null;
    } 
    if (paramBundle2 == null) {
      if (paramBundle1.getInt("android.media.browse.extra.PAGE", -1) == -1 && 
        paramBundle1.getInt("android.media.browse.extra.PAGE_SIZE", -1) == -1) {
        null = bool1;
      } else {
        null = false;
      } 
      return null;
    } 
    int i = paramBundle1.getInt("android.media.browse.extra.PAGE", -1);
    if (i == paramBundle2.getInt("android.media.browse.extra.PAGE", -1)) {
      i = paramBundle1.getInt("android.media.browse.extra.PAGE_SIZE", -1);
      if (i == paramBundle2.getInt("android.media.browse.extra.PAGE_SIZE", -1))
        return bool2; 
    } 
    return false;
  }
  
  public static boolean hasDuplicatedItems(Bundle paramBundle1, Bundle paramBundle2) {
    int i;
    int j;
    int k;
    int m;
    if (paramBundle1 == null) {
      i = -1;
    } else {
      i = paramBundle1.getInt("android.media.browse.extra.PAGE", -1);
    } 
    if (paramBundle2 == null) {
      j = -1;
    } else {
      j = paramBundle2.getInt("android.media.browse.extra.PAGE", -1);
    } 
    if (paramBundle1 == null) {
      k = -1;
    } else {
      k = paramBundle1.getInt("android.media.browse.extra.PAGE_SIZE", -1);
    } 
    if (paramBundle2 == null) {
      m = -1;
    } else {
      m = paramBundle2.getInt("android.media.browse.extra.PAGE_SIZE", -1);
    } 
    if (i == -1 || k == -1) {
      k = 0;
      i = Integer.MAX_VALUE;
    } else {
      i = k * i;
      int n = i + k - 1;
      k = i;
      i = n;
    } 
    if (j == -1 || m == -1) {
      m = 0;
      j = Integer.MAX_VALUE;
    } else {
      int n = m * j;
      j = n + m - 1;
      m = n;
    } 
    if (k <= m && m <= i)
      return true; 
    if (k <= j && j <= i)
      return true; 
    return false;
  }
}
