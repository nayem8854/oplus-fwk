package android.media;

class TextTrackCueSpan {
  boolean mEnabled;
  
  String mText;
  
  long mTimestampMs;
  
  TextTrackCueSpan(String paramString, long paramLong) {
    boolean bool;
    this.mTimestampMs = paramLong;
    this.mText = paramString;
    if (paramLong < 0L) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mEnabled = bool;
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = paramObject instanceof TextTrackCueSpan;
    boolean bool1 = false;
    if (!bool)
      return false; 
    TextTrackCueSpan textTrackCueSpan = (TextTrackCueSpan)paramObject;
    if (this.mTimestampMs == textTrackCueSpan.mTimestampMs) {
      paramObject = this.mText;
      String str = textTrackCueSpan.mText;
      if (paramObject.equals(str))
        bool1 = true; 
    } 
    return bool1;
  }
}
