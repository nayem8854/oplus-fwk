package android.media;

public class AudioMixPort extends AudioPort {
  private final int mIoHandle;
  
  AudioMixPort(AudioHandle paramAudioHandle, int paramInt1, int paramInt2, String paramString, int[] paramArrayOfint1, int[] paramArrayOfint2, int[] paramArrayOfint3, int[] paramArrayOfint4, AudioGain[] paramArrayOfAudioGain) {
    super(paramAudioHandle, paramInt2, paramString, paramArrayOfint1, paramArrayOfint2, paramArrayOfint3, paramArrayOfint4, paramArrayOfAudioGain);
    this.mIoHandle = paramInt1;
  }
  
  public AudioMixPortConfig buildConfig(int paramInt1, int paramInt2, int paramInt3, AudioGainConfig paramAudioGainConfig) {
    return new AudioMixPortConfig(this, paramInt1, paramInt2, paramInt3, paramAudioGainConfig);
  }
  
  public int ioHandle() {
    return this.mIoHandle;
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null || !(paramObject instanceof AudioMixPort))
      return false; 
    AudioMixPort audioMixPort = (AudioMixPort)paramObject;
    if (this.mIoHandle != audioMixPort.ioHandle())
      return false; 
    return super.equals(paramObject);
  }
}
