package android.media;

import android.util.IntArray;

public abstract class AudioManagerInternal {
  public abstract void adjustStreamVolumeForUid(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5);
  
  public abstract void adjustSuggestedStreamVolumeForUid(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5);
  
  public abstract int getRingerModeInternal();
  
  public abstract void setAccessibilityServiceUids(IntArray paramIntArray);
  
  public abstract void setInputMethodServiceUid(int paramInt);
  
  public abstract void setRingerModeDelegate(RingerModeDelegate paramRingerModeDelegate);
  
  public abstract void setRingerModeInternal(int paramInt, String paramString);
  
  public abstract void setStreamVolumeForUid(int paramInt1, int paramInt2, int paramInt3, String paramString, int paramInt4, int paramInt5);
  
  public abstract void silenceRingerModeInternal(String paramString);
  
  public abstract void updateRingerModeAffectedStreamsInternal();
  
  public static interface RingerModeDelegate {
    boolean canVolumeDownEnterSilent();
    
    int getRingerModeAffectedStreams(int param1Int);
    
    int onSetRingerModeExternal(int param1Int1, int param1Int2, String param1String, int param1Int3, VolumePolicy param1VolumePolicy);
    
    int onSetRingerModeInternal(int param1Int1, int param1Int2, String param1String, int param1Int3, VolumePolicy param1VolumePolicy);
  }
}
