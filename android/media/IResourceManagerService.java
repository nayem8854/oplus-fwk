package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IResourceManagerService extends IInterface {
  public static final String kPolicySupportsMultipleSecureCodecs = "supports-multiple-secure-codecs";
  
  public static final String kPolicySupportsSecureWithNonSecureCodec = "supports-secure-with-non-secure-codec";
  
  void addResource(int paramInt1, int paramInt2, long paramLong, IResourceManagerClient paramIResourceManagerClient, MediaResourceParcel[] paramArrayOfMediaResourceParcel) throws RemoteException;
  
  void config(MediaResourcePolicyParcel[] paramArrayOfMediaResourcePolicyParcel) throws RemoteException;
  
  void markClientForPendingRemoval(int paramInt, long paramLong) throws RemoteException;
  
  void overridePid(int paramInt1, int paramInt2) throws RemoteException;
  
  boolean reclaimResource(int paramInt, MediaResourceParcel[] paramArrayOfMediaResourceParcel) throws RemoteException;
  
  void removeClient(int paramInt, long paramLong) throws RemoteException;
  
  void removeResource(int paramInt, long paramLong, MediaResourceParcel[] paramArrayOfMediaResourceParcel) throws RemoteException;
  
  class Default implements IResourceManagerService {
    public void config(MediaResourcePolicyParcel[] param1ArrayOfMediaResourcePolicyParcel) throws RemoteException {}
    
    public void addResource(int param1Int1, int param1Int2, long param1Long, IResourceManagerClient param1IResourceManagerClient, MediaResourceParcel[] param1ArrayOfMediaResourceParcel) throws RemoteException {}
    
    public void removeResource(int param1Int, long param1Long, MediaResourceParcel[] param1ArrayOfMediaResourceParcel) throws RemoteException {}
    
    public void removeClient(int param1Int, long param1Long) throws RemoteException {}
    
    public boolean reclaimResource(int param1Int, MediaResourceParcel[] param1ArrayOfMediaResourceParcel) throws RemoteException {
      return false;
    }
    
    public void overridePid(int param1Int1, int param1Int2) throws RemoteException {}
    
    public void markClientForPendingRemoval(int param1Int, long param1Long) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IResourceManagerService {
    private static final String DESCRIPTOR = "android.media.IResourceManagerService";
    
    static final int TRANSACTION_addResource = 2;
    
    static final int TRANSACTION_config = 1;
    
    static final int TRANSACTION_markClientForPendingRemoval = 7;
    
    static final int TRANSACTION_overridePid = 6;
    
    static final int TRANSACTION_reclaimResource = 5;
    
    static final int TRANSACTION_removeClient = 4;
    
    static final int TRANSACTION_removeResource = 3;
    
    public Stub() {
      attachInterface(this, "android.media.IResourceManagerService");
    }
    
    public static IResourceManagerService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IResourceManagerService");
      if (iInterface != null && iInterface instanceof IResourceManagerService)
        return (IResourceManagerService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 7:
          return "markClientForPendingRemoval";
        case 6:
          return "overridePid";
        case 5:
          return "reclaimResource";
        case 4:
          return "removeClient";
        case 3:
          return "removeResource";
        case 2:
          return "addResource";
        case 1:
          break;
      } 
      return "config";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        boolean bool;
        int i;
        MediaResourceParcel[] arrayOfMediaResourceParcel;
        long l;
        IResourceManagerClient iResourceManagerClient;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 7:
            param1Parcel1.enforceInterface("android.media.IResourceManagerService");
            param1Int1 = param1Parcel1.readInt();
            l = param1Parcel1.readLong();
            markClientForPendingRemoval(param1Int1, l);
            param1Parcel2.writeNoException();
            return true;
          case 6:
            param1Parcel1.enforceInterface("android.media.IResourceManagerService");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            overridePid(param1Int1, param1Int2);
            param1Parcel2.writeNoException();
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.IResourceManagerService");
            param1Int1 = param1Parcel1.readInt();
            arrayOfMediaResourceParcel = param1Parcel1.<MediaResourceParcel>createTypedArray(MediaResourceParcel.CREATOR);
            bool = reclaimResource(param1Int1, arrayOfMediaResourceParcel);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(bool);
            return true;
          case 4:
            arrayOfMediaResourceParcel.enforceInterface("android.media.IResourceManagerService");
            i = arrayOfMediaResourceParcel.readInt();
            l = arrayOfMediaResourceParcel.readLong();
            removeClient(i, l);
            param1Parcel2.writeNoException();
            return true;
          case 3:
            arrayOfMediaResourceParcel.enforceInterface("android.media.IResourceManagerService");
            i = arrayOfMediaResourceParcel.readInt();
            l = arrayOfMediaResourceParcel.readLong();
            arrayOfMediaResourceParcel = arrayOfMediaResourceParcel.<MediaResourceParcel>createTypedArray(MediaResourceParcel.CREATOR);
            removeResource(i, l, arrayOfMediaResourceParcel);
            param1Parcel2.writeNoException();
            return true;
          case 2:
            arrayOfMediaResourceParcel.enforceInterface("android.media.IResourceManagerService");
            i = arrayOfMediaResourceParcel.readInt();
            param1Int2 = arrayOfMediaResourceParcel.readInt();
            l = arrayOfMediaResourceParcel.readLong();
            iResourceManagerClient = IResourceManagerClient.Stub.asInterface(arrayOfMediaResourceParcel.readStrongBinder());
            arrayOfMediaResourceParcel = arrayOfMediaResourceParcel.<MediaResourceParcel>createTypedArray(MediaResourceParcel.CREATOR);
            addResource(i, param1Int2, l, iResourceManagerClient, arrayOfMediaResourceParcel);
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        arrayOfMediaResourceParcel.enforceInterface("android.media.IResourceManagerService");
        MediaResourcePolicyParcel[] arrayOfMediaResourcePolicyParcel = arrayOfMediaResourceParcel.<MediaResourcePolicyParcel>createTypedArray(MediaResourcePolicyParcel.CREATOR);
        config(arrayOfMediaResourcePolicyParcel);
        param1Parcel2.writeNoException();
        return true;
      } 
      param1Parcel2.writeString("android.media.IResourceManagerService");
      return true;
    }
    
    private static class Proxy implements IResourceManagerService {
      public static IResourceManagerService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IResourceManagerService";
      }
      
      public void config(MediaResourcePolicyParcel[] param2ArrayOfMediaResourcePolicyParcel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeTypedArray(param2ArrayOfMediaResourcePolicyParcel, 0);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
            IResourceManagerService.Stub.getDefaultImpl().config(param2ArrayOfMediaResourcePolicyParcel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void addResource(int param2Int1, int param2Int2, long param2Long, IResourceManagerClient param2IResourceManagerClient, MediaResourceParcel[] param2ArrayOfMediaResourceParcel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          try {
            parcel1.writeInt(param2Int1);
            try {
              parcel1.writeInt(param2Int2);
              try {
                IBinder iBinder;
                parcel1.writeLong(param2Long);
                if (param2IResourceManagerClient != null) {
                  iBinder = param2IResourceManagerClient.asBinder();
                } else {
                  iBinder = null;
                } 
                parcel1.writeStrongBinder(iBinder);
                try {
                  parcel1.writeTypedArray(param2ArrayOfMediaResourceParcel, 0);
                  try {
                    boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
                    if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
                      IResourceManagerService.Stub.getDefaultImpl().addResource(param2Int1, param2Int2, param2Long, param2IResourceManagerClient, param2ArrayOfMediaResourceParcel);
                      parcel2.recycle();
                      parcel1.recycle();
                      return;
                    } 
                    parcel2.readException();
                    parcel2.recycle();
                    parcel1.recycle();
                    return;
                  } finally {}
                } finally {}
              } finally {}
            } finally {}
          } finally {}
        } finally {}
        parcel2.recycle();
        parcel1.recycle();
        throw param2IResourceManagerClient;
      }
      
      public void removeResource(int param2Int, long param2Long, MediaResourceParcel[] param2ArrayOfMediaResourceParcel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          parcel1.writeTypedArray(param2ArrayOfMediaResourceParcel, 0);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
            IResourceManagerService.Stub.getDefaultImpl().removeResource(param2Int, param2Long, param2ArrayOfMediaResourceParcel);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void removeClient(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
            IResourceManagerService.Stub.getDefaultImpl().removeClient(param2Int, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public boolean reclaimResource(int param2Int, MediaResourceParcel[] param2ArrayOfMediaResourceParcel) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeInt(param2Int);
          boolean bool1 = false;
          parcel1.writeTypedArray(param2ArrayOfMediaResourceParcel, 0);
          boolean bool2 = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool2 && IResourceManagerService.Stub.getDefaultImpl() != null) {
            bool1 = IResourceManagerService.Stub.getDefaultImpl().reclaimResource(param2Int, param2ArrayOfMediaResourceParcel);
            return bool1;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          if (param2Int != 0)
            bool1 = true; 
          return bool1;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void overridePid(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeInt(param2Int1);
          parcel1.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
            IResourceManagerService.Stub.getDefaultImpl().overridePid(param2Int1, param2Int2);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void markClientForPendingRemoval(int param2Int, long param2Long) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IResourceManagerService");
          parcel1.writeInt(param2Int);
          parcel1.writeLong(param2Long);
          boolean bool = this.mRemote.transact(7, parcel1, parcel2, 0);
          if (!bool && IResourceManagerService.Stub.getDefaultImpl() != null) {
            IResourceManagerService.Stub.getDefaultImpl().markClientForPendingRemoval(param2Int, param2Long);
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IResourceManagerService param1IResourceManagerService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IResourceManagerService != null) {
          Proxy.sDefaultImpl = param1IResourceManagerService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IResourceManagerService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
