package android.media;

import android.graphics.Rect;
import android.hardware.HardwareBuffer;
import java.nio.ByteBuffer;

public abstract class Image implements AutoCloseable {
  private Rect mCropRect;
  
  protected boolean mIsImageValid = false;
  
  protected void throwISEIfImageIsInvalid() {
    if (this.mIsImageValid)
      return; 
    throw new IllegalStateException("Image is already closed");
  }
  
  public HardwareBuffer getHardwareBuffer() {
    throwISEIfImageIsInvalid();
    return null;
  }
  
  public void setTimestamp(long paramLong) {
    throwISEIfImageIsInvalid();
  }
  
  public Rect getCropRect() {
    throwISEIfImageIsInvalid();
    if (this.mCropRect == null)
      return new Rect(0, 0, getWidth(), getHeight()); 
    return new Rect(this.mCropRect);
  }
  
  public void setCropRect(Rect paramRect) {
    throwISEIfImageIsInvalid();
    Rect rect = paramRect;
    if (paramRect != null) {
      paramRect = new Rect(paramRect);
      rect = paramRect;
      if (!paramRect.intersect(0, 0, getWidth(), getHeight())) {
        paramRect.setEmpty();
        rect = paramRect;
      } 
    } 
    this.mCropRect = rect;
  }
  
  boolean isAttachable() {
    throwISEIfImageIsInvalid();
    return false;
  }
  
  Object getOwner() {
    throwISEIfImageIsInvalid();
    return null;
  }
  
  long getNativeContext() {
    throwISEIfImageIsInvalid();
    return 0L;
  }
  
  public abstract void close();
  
  public abstract int getFormat();
  
  public abstract int getHeight();
  
  public abstract Plane[] getPlanes();
  
  public abstract int getScalingMode();
  
  public abstract long getTimestamp();
  
  public abstract int getTransform();
  
  public abstract int getWidth();
  
  public static abstract class Plane {
    public abstract ByteBuffer getBuffer();
    
    public abstract int getPixelStride();
    
    public abstract int getRowStride();
  }
}
