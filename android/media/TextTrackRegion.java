package android.media;

class TextTrackRegion {
  String mId = "";
  
  float mWidth = 100.0F;
  
  int mLines = 3;
  
  float mAnchorPointX = 0.0F;
  
  float mViewportAnchorPointX = 0.0F;
  
  float mAnchorPointY = 100.0F;
  
  float mViewportAnchorPointY = 100.0F;
  
  int mScrollValue = 300;
  
  static final int SCROLL_VALUE_NONE = 300;
  
  static final int SCROLL_VALUE_SCROLL_UP = 301;
  
  public String toString() {
    String str;
    StringBuilder stringBuilder1 = new StringBuilder(" {id:\"");
    stringBuilder1.append(this.mId);
    stringBuilder1.append("\", width:");
    stringBuilder1.append(this.mWidth);
    stringBuilder1.append(", lines:");
    stringBuilder1.append(this.mLines);
    stringBuilder1.append(", anchorPoint:(");
    stringBuilder1.append(this.mAnchorPointX);
    stringBuilder1.append(", ");
    stringBuilder1.append(this.mAnchorPointY);
    stringBuilder1.append("), viewportAnchorPoints:");
    stringBuilder1.append(this.mViewportAnchorPointX);
    stringBuilder1.append(", ");
    stringBuilder1.append(this.mViewportAnchorPointY);
    stringBuilder1.append("), scrollValue:");
    int i = this.mScrollValue;
    if (i == 300) {
      str = "none";
    } else if (i == 301) {
      str = "scroll_up";
    } else {
      str = "INVALID";
    } 
    stringBuilder1.append(str);
    StringBuilder stringBuilder2 = stringBuilder1.append("}");
    return stringBuilder2.toString();
  }
}
