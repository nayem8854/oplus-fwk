package android.media;

import android.annotation.SystemApi;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.util.ArrayMap;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.NioUtils;
import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.Executor;

public class AudioTrack extends PlayerBase implements AudioRouting, VolumeAutomation {
  private int mState = 0;
  
  private int mPlayState = 1;
  
  private boolean mOffloadEosPending = false;
  
  private final Object mPlayStateLock = new Object();
  
  private int mNativeBufferSizeInBytes = 0;
  
  private int mNativeBufferSizeInFrames = 0;
  
  private int mChannelCount = 1;
  
  private int mChannelMask = 4;
  
  private int mStreamType = 3;
  
  private int mDataLoadMode = 1;
  
  private int mChannelConfiguration = 4;
  
  private int mChannelIndexMask = 0;
  
  private int mSessionId = 0;
  
  private ByteBuffer mAvSyncHeader = null;
  
  private int mAvSyncBytesRemaining = 0;
  
  private int mOffset = 0;
  
  private boolean mOffloaded = false;
  
  private int mOffloadDelayFrames = 0;
  
  private int mOffloadPaddingFrames = 0;
  
  private static final int AUDIO_OUTPUT_FLAG_DEEP_BUFFER = 8;
  
  private static final int AUDIO_OUTPUT_FLAG_FAST = 4;
  
  public static final int DUAL_MONO_MODE_LL = 2;
  
  public static final int DUAL_MONO_MODE_LR = 1;
  
  public static final int DUAL_MONO_MODE_OFF = 0;
  
  public static final int DUAL_MONO_MODE_RR = 3;
  
  public static final int ENCAPSULATION_METADATA_TYPE_DVB_AD_DESCRIPTOR = 2;
  
  public static final int ENCAPSULATION_METADATA_TYPE_FRAMEWORK_TUNER = 1;
  
  public static final int ENCAPSULATION_METADATA_TYPE_NONE = 0;
  
  public static final int ENCAPSULATION_MODE_ELEMENTARY_STREAM = 1;
  
  @SystemApi
  public static final int ENCAPSULATION_MODE_HANDLE = 2;
  
  public static final int ENCAPSULATION_MODE_NONE = 0;
  
  public static final int ERROR = -1;
  
  public static final int ERROR_BAD_VALUE = -2;
  
  public static final int ERROR_DEAD_OBJECT = -6;
  
  public static final int ERROR_INVALID_OPERATION = -3;
  
  private static final int ERROR_NATIVESETUP_AUDIOSYSTEM = -16;
  
  private static final int ERROR_NATIVESETUP_INVALIDCHANNELMASK = -17;
  
  private static final int ERROR_NATIVESETUP_INVALIDFORMAT = -18;
  
  private static final int ERROR_NATIVESETUP_INVALIDSTREAMTYPE = -19;
  
  private static final int ERROR_NATIVESETUP_NATIVEINITFAILED = -20;
  
  public static final int ERROR_WOULD_BLOCK = -7;
  
  private static final float GAIN_MAX = 1.0F;
  
  private static final float GAIN_MIN = 0.0F;
  
  private static final float HEADER_V2_SIZE_BYTES = 20.0F;
  
  private static final float MAX_AUDIO_DESCRIPTION_MIX_LEVEL = 48.0F;
  
  public static final int MODE_STATIC = 0;
  
  public static final int MODE_STREAM = 1;
  
  private static final int NATIVE_EVENT_CAN_WRITE_MORE_DATA = 9;
  
  private static final int NATIVE_EVENT_CODEC_FORMAT_CHANGE = 100;
  
  private static final int NATIVE_EVENT_MARKER = 3;
  
  private static final int NATIVE_EVENT_NEW_IAUDIOTRACK = 6;
  
  private static final int NATIVE_EVENT_NEW_POS = 4;
  
  private static final int NATIVE_EVENT_STREAM_END = 7;
  
  public static final int PERFORMANCE_MODE_LOW_LATENCY = 1;
  
  public static final int PERFORMANCE_MODE_NONE = 0;
  
  public static final int PERFORMANCE_MODE_POWER_SAVING = 2;
  
  public static final int PLAYSTATE_PAUSED = 2;
  
  private static final int PLAYSTATE_PAUSED_STOPPING = 5;
  
  public static final int PLAYSTATE_PLAYING = 3;
  
  public static final int PLAYSTATE_STOPPED = 1;
  
  private static final int PLAYSTATE_STOPPING = 4;
  
  public static final int STATE_INITIALIZED = 1;
  
  public static final int STATE_NO_STATIC_DATA = 2;
  
  public static final int STATE_UNINITIALIZED = 0;
  
  public static final int SUCCESS = 0;
  
  private static final int SUPPORTED_OUT_CHANNELS = 7420;
  
  private static final String TAG = "android.media.AudioTrack";
  
  public static final int WRITE_BLOCKING = 0;
  
  public static final int WRITE_NON_BLOCKING = 1;
  
  private int mAudioFormat;
  
  private final Utils.ListenerList<AudioMetadataReadMap> mCodecFormatChangedListeners;
  
  private AudioAttributes mConfiguredAudioAttributes;
  
  private NativePositionEventHandlerDelegate mEventHandlerDelegate;
  
  private final Looper mInitializationLooper;
  
  private long mJniData;
  
  protected long mNativeTrackInJavaObj;
  
  private AudioDeviceInfo mPreferredDevice;
  
  private ArrayMap<AudioRouting.OnRoutingChangedListener, NativeRoutingEventHandlerDelegate> mRoutingChangeListeners;
  
  private int mSampleRate;
  
  private LinkedList<StreamEventCbInfo> mStreamEventCbInfoList;
  
  private final Object mStreamEventCbLock;
  
  private volatile StreamEventHandler mStreamEventHandler;
  
  private HandlerThread mStreamEventHandlerThread;
  
  public AudioTrack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) throws IllegalArgumentException {
    this(paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6, 0);
  }
  
  public AudioTrack(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7) throws IllegalArgumentException {
    this(audioAttributes, audioFormat, paramInt5, paramInt6, paramInt7);
    deprecateStreamTypeForPlayback(paramInt1, "AudioTrack", "AudioTrack()");
  }
  
  public AudioTrack(AudioAttributes paramAudioAttributes, AudioFormat paramAudioFormat, int paramInt1, int paramInt2, int paramInt3) throws IllegalArgumentException {
    this(paramAudioAttributes, paramAudioFormat, paramInt1, paramInt2, paramInt3, false, 0, (TunerConfiguration)null);
  }
  
  private AudioTrack(AudioAttributes paramAudioAttributes, AudioFormat paramAudioFormat, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean, int paramInt4, TunerConfiguration paramTunerConfiguration) throws IllegalArgumentException {
    super(paramAudioAttributes, 1);
    this.mPreferredDevice = null;
    this.mRoutingChangeListeners = new ArrayMap();
    this.mCodecFormatChangedListeners = new Utils.ListenerList<>();
    this.mStreamEventCbLock = new Object();
    this.mStreamEventCbInfoList = new LinkedList<>();
    this.mConfiguredAudioAttributes = paramAudioAttributes;
    if (paramAudioFormat != null) {
      boolean bool1, bool2, bool3;
      if (shouldEnablePowerSaving(this.mAttributes, paramAudioFormat, paramInt1, paramInt2)) {
        AudioAttributes.Builder builder2 = new AudioAttributes.Builder(this.mAttributes);
        paramAudioAttributes = this.mAttributes;
        AudioAttributes.Builder builder1 = builder2.replaceFlags((paramAudioAttributes.getAllFlags() | 0x200) & 0xFFFFFEFF);
        this.mAttributes = builder1.build();
      } 
      Looper looper = Looper.myLooper();
      if (looper == null)
        looper = Looper.getMainLooper(); 
      int i = paramAudioFormat.getSampleRate();
      if (i == 0)
        i = 0; 
      if ((paramAudioFormat.getPropertySetMask() & 0x8) != 0) {
        bool1 = paramAudioFormat.getChannelIndexMask();
      } else {
        bool1 = false;
      } 
      if ((0x4 & paramAudioFormat.getPropertySetMask()) != 0) {
        bool2 = paramAudioFormat.getChannelMask();
      } else if (!bool1) {
        bool2 = true;
      } else {
        bool2 = false;
      } 
      if ((paramAudioFormat.getPropertySetMask() & 0x1) != 0) {
        bool3 = paramAudioFormat.getEncoding();
      } else {
        bool3 = true;
      } 
      audioParamCheck(i, bool2, bool1, bool3, paramInt2);
      this.mOffloaded = paramBoolean;
      this.mStreamType = -1;
      audioBuffSizeCheck(paramInt1);
      this.mInitializationLooper = looper;
      if (paramInt3 >= 0) {
        StringBuilder stringBuilder1;
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = this.mSampleRate;
        int[] arrayOfInt2 = new int[1];
        arrayOfInt2[0] = paramInt3;
        paramInt1 = native_setup(new WeakReference<>(this), this.mAttributes, arrayOfInt1, this.mChannelMask, this.mChannelIndexMask, this.mAudioFormat, this.mNativeBufferSizeInBytes, this.mDataLoadMode, arrayOfInt2, 0L, paramBoolean, paramInt4, paramTunerConfiguration);
        if (paramInt1 != 0) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("Error code ");
          stringBuilder1.append(paramInt1);
          stringBuilder1.append(" when initializing AudioTrack.");
          loge(stringBuilder1.toString());
          return;
        } 
        this.mSampleRate = stringBuilder1[0];
        this.mSessionId = arrayOfInt2[0];
        if ((this.mAttributes.getFlags() & 0x10) != 0) {
          if (AudioFormat.isEncodingLinearFrames(this.mAudioFormat)) {
            paramInt1 = this.mChannelCount * AudioFormat.getBytesPerSample(this.mAudioFormat);
          } else {
            paramInt1 = 1;
          } 
          this.mOffset = (int)Math.ceil((20.0F / paramInt1)) * paramInt1;
        } 
        if (this.mDataLoadMode == 0) {
          this.mState = 2;
        } else {
          this.mState = 1;
        } 
        baseRegisterPlayer();
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid audio session ID: ");
      stringBuilder.append(paramInt3);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    throw new IllegalArgumentException("Illegal null AudioFormat");
  }
  
  AudioTrack(long paramLong) {
    super((new AudioAttributes.Builder()).build(), 1);
    this.mPreferredDevice = null;
    this.mRoutingChangeListeners = new ArrayMap();
    this.mCodecFormatChangedListeners = new Utils.ListenerList<>();
    this.mStreamEventCbLock = new Object();
    this.mStreamEventCbInfoList = new LinkedList<>();
    this.mNativeTrackInJavaObj = 0L;
    this.mJniData = 0L;
    Looper looper1 = Looper.myLooper(), looper2 = looper1;
    if (looper1 == null)
      looper2 = Looper.getMainLooper(); 
    this.mInitializationLooper = looper2;
    if (paramLong != 0L) {
      baseRegisterPlayer();
      deferred_connect(paramLong);
    } else {
      this.mState = 0;
    } 
  }
  
  void deferred_connect(long paramLong) {
    if (this.mState != 1) {
      StringBuilder stringBuilder;
      int[] arrayOfInt = new int[1];
      arrayOfInt[0] = 0;
      int i = native_setup(new WeakReference<>(this), (Object)null, new int[] { 0 }, 0, 0, 0, 0, 0, arrayOfInt, paramLong, false, 0, (Object)null);
      if (i != 0) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Error code ");
        stringBuilder.append(i);
        stringBuilder.append(" when initializing AudioTrack.");
        loge(stringBuilder.toString());
        return;
      } 
      this.mSessionId = stringBuilder[0];
      this.mState = 1;
    } 
  }
  
  @SystemApi
  class TunerConfiguration {
    private final int mContentId;
    
    private final int mSyncId;
    
    public TunerConfiguration(AudioTrack this$0, int param1Int1) {
      if (this$0 >= true) {
        if (param1Int1 >= 1) {
          this.mContentId = this$0;
          this.mSyncId = param1Int1;
          return;
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("syncId ");
        stringBuilder1.append(param1Int1);
        stringBuilder1.append(" must be positive");
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("contentId ");
      stringBuilder.append(this$0);
      stringBuilder.append(" must be positive");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public int getContentId() {
      return this.mContentId;
    }
    
    public int getSyncId() {
      return this.mSyncId;
    }
  }
  
  class Builder {
    private int mEncapsulationMode = 0;
    
    private int mSessionId = 0;
    
    private int mMode = 1;
    
    private int mPerformanceMode = 0;
    
    private boolean mOffload = false;
    
    private AudioAttributes mAttributes;
    
    private int mBufferSizeInBytes;
    
    private AudioFormat mFormat;
    
    private AudioTrack.TunerConfiguration mTunerConfiguration;
    
    public Builder setAudioAttributes(AudioAttributes param1AudioAttributes) throws IllegalArgumentException {
      if (param1AudioAttributes != null) {
        this.mAttributes = param1AudioAttributes;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioAttributes argument");
    }
    
    public Builder setAudioFormat(AudioFormat param1AudioFormat) throws IllegalArgumentException {
      if (param1AudioFormat != null) {
        this.mFormat = param1AudioFormat;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioFormat argument");
    }
    
    public Builder setBufferSizeInBytes(int param1Int) throws IllegalArgumentException {
      if (param1Int > 0) {
        this.mBufferSizeInBytes = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid buffer size ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setEncapsulationMode(int param1Int) {
      if (param1Int == 0 || param1Int == 1 || param1Int == 2) {
        this.mEncapsulationMode = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid encapsulation mode ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setTransferMode(int param1Int) throws IllegalArgumentException {
      if (param1Int == 0 || param1Int == 1) {
        this.mMode = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid transfer mode ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setSessionId(int param1Int) throws IllegalArgumentException {
      if (param1Int == 0 || param1Int >= 1) {
        this.mSessionId = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid audio session ID ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setPerformanceMode(int param1Int) {
      if (param1Int == 0 || param1Int == 1 || param1Int == 2) {
        this.mPerformanceMode = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid performance mode ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setOffloadedPlayback(boolean param1Boolean) {
      this.mOffload = param1Boolean;
      return this;
    }
    
    @SystemApi
    public Builder setTunerConfiguration(AudioTrack.TunerConfiguration param1TunerConfiguration) {
      if (param1TunerConfiguration != null) {
        this.mTunerConfiguration = param1TunerConfiguration;
        return this;
      } 
      throw new IllegalArgumentException("tunerConfiguration is null");
    }
    
    public AudioTrack build() throws UnsupportedOperationException {
      // Byte code:
      //   0: aload_0
      //   1: getfield mAttributes : Landroid/media/AudioAttributes;
      //   4: ifnonnull -> 29
      //   7: new android/media/AudioAttributes$Builder
      //   10: dup
      //   11: invokespecial <init> : ()V
      //   14: astore_1
      //   15: aload_1
      //   16: iconst_1
      //   17: invokevirtual setUsage : (I)Landroid/media/AudioAttributes$Builder;
      //   20: astore_1
      //   21: aload_0
      //   22: aload_1
      //   23: invokevirtual build : ()Landroid/media/AudioAttributes;
      //   26: putfield mAttributes : Landroid/media/AudioAttributes;
      //   29: aload_0
      //   30: getfield mPerformanceMode : I
      //   33: istore_2
      //   34: iload_2
      //   35: ifeq -> 96
      //   38: iload_2
      //   39: iconst_1
      //   40: if_icmpeq -> 51
      //   43: iload_2
      //   44: iconst_2
      //   45: if_icmpeq -> 121
      //   48: goto -> 163
      //   51: new android/media/AudioAttributes$Builder
      //   54: dup
      //   55: aload_0
      //   56: getfield mAttributes : Landroid/media/AudioAttributes;
      //   59: invokespecial <init> : (Landroid/media/AudioAttributes;)V
      //   62: astore_3
      //   63: aload_0
      //   64: getfield mAttributes : Landroid/media/AudioAttributes;
      //   67: astore_1
      //   68: aload_3
      //   69: aload_1
      //   70: invokevirtual getAllFlags : ()I
      //   73: sipush #256
      //   76: ior
      //   77: sipush #-513
      //   80: iand
      //   81: invokevirtual replaceFlags : (I)Landroid/media/AudioAttributes$Builder;
      //   84: astore_1
      //   85: aload_0
      //   86: aload_1
      //   87: invokevirtual build : ()Landroid/media/AudioAttributes;
      //   90: putfield mAttributes : Landroid/media/AudioAttributes;
      //   93: goto -> 163
      //   96: aload_0
      //   97: getfield mAttributes : Landroid/media/AudioAttributes;
      //   100: aload_0
      //   101: getfield mFormat : Landroid/media/AudioFormat;
      //   104: aload_0
      //   105: getfield mBufferSizeInBytes : I
      //   108: aload_0
      //   109: getfield mMode : I
      //   112: invokestatic access$000 : (Landroid/media/AudioAttributes;Landroid/media/AudioFormat;II)Z
      //   115: ifne -> 121
      //   118: goto -> 163
      //   121: new android/media/AudioAttributes$Builder
      //   124: dup
      //   125: aload_0
      //   126: getfield mAttributes : Landroid/media/AudioAttributes;
      //   129: invokespecial <init> : (Landroid/media/AudioAttributes;)V
      //   132: astore_1
      //   133: aload_0
      //   134: getfield mAttributes : Landroid/media/AudioAttributes;
      //   137: astore_3
      //   138: aload_1
      //   139: aload_3
      //   140: invokevirtual getAllFlags : ()I
      //   143: sipush #512
      //   146: ior
      //   147: sipush #-257
      //   150: iand
      //   151: invokevirtual replaceFlags : (I)Landroid/media/AudioAttributes$Builder;
      //   154: astore_1
      //   155: aload_0
      //   156: aload_1
      //   157: invokevirtual build : ()Landroid/media/AudioAttributes;
      //   160: putfield mAttributes : Landroid/media/AudioAttributes;
      //   163: aload_0
      //   164: getfield mFormat : Landroid/media/AudioFormat;
      //   167: ifnonnull -> 199
      //   170: new android/media/AudioFormat$Builder
      //   173: dup
      //   174: invokespecial <init> : ()V
      //   177: astore_1
      //   178: aload_1
      //   179: bipush #12
      //   181: invokevirtual setChannelMask : (I)Landroid/media/AudioFormat$Builder;
      //   184: astore_1
      //   185: aload_1
      //   186: iconst_1
      //   187: invokevirtual setEncoding : (I)Landroid/media/AudioFormat$Builder;
      //   190: astore_1
      //   191: aload_0
      //   192: aload_1
      //   193: invokevirtual build : ()Landroid/media/AudioFormat;
      //   196: putfield mFormat : Landroid/media/AudioFormat;
      //   199: aload_0
      //   200: getfield mOffload : Z
      //   203: ifeq -> 251
      //   206: aload_0
      //   207: getfield mPerformanceMode : I
      //   210: iconst_1
      //   211: if_icmpeq -> 241
      //   214: aload_0
      //   215: getfield mFormat : Landroid/media/AudioFormat;
      //   218: aload_0
      //   219: getfield mAttributes : Landroid/media/AudioAttributes;
      //   222: invokestatic isOffloadSupported : (Landroid/media/AudioFormat;Landroid/media/AudioAttributes;)Z
      //   225: ifeq -> 231
      //   228: goto -> 251
      //   231: new java/lang/UnsupportedOperationException
      //   234: dup
      //   235: ldc 'Cannot create AudioTrack, offload format / attributes not supported'
      //   237: invokespecial <init> : (Ljava/lang/String;)V
      //   240: athrow
      //   241: new java/lang/UnsupportedOperationException
      //   244: dup
      //   245: ldc 'Offload and low latency modes are incompatible'
      //   247: invokespecial <init> : (Ljava/lang/String;)V
      //   250: athrow
      //   251: aload_0
      //   252: getfield mMode : I
      //   255: iconst_1
      //   256: if_icmpne -> 292
      //   259: aload_0
      //   260: getfield mBufferSizeInBytes : I
      //   263: ifne -> 292
      //   266: aload_0
      //   267: getfield mFormat : Landroid/media/AudioFormat;
      //   270: invokevirtual getChannelCount : ()I
      //   273: istore_2
      //   274: aload_0
      //   275: getfield mFormat : Landroid/media/AudioFormat;
      //   278: astore_1
      //   279: aload_0
      //   280: iload_2
      //   281: aload_1
      //   282: invokevirtual getEncoding : ()I
      //   285: invokestatic getBytesPerSample : (I)I
      //   288: imul
      //   289: putfield mBufferSizeInBytes : I
      //   292: new android/media/AudioTrack
      //   295: astore_1
      //   296: aload_1
      //   297: aload_0
      //   298: getfield mAttributes : Landroid/media/AudioAttributes;
      //   301: aload_0
      //   302: getfield mFormat : Landroid/media/AudioFormat;
      //   305: aload_0
      //   306: getfield mBufferSizeInBytes : I
      //   309: aload_0
      //   310: getfield mMode : I
      //   313: aload_0
      //   314: getfield mSessionId : I
      //   317: aload_0
      //   318: getfield mOffload : Z
      //   321: aload_0
      //   322: getfield mEncapsulationMode : I
      //   325: aload_0
      //   326: getfield mTunerConfiguration : Landroid/media/AudioTrack$TunerConfiguration;
      //   329: aconst_null
      //   330: invokespecial <init> : (Landroid/media/AudioAttributes;Landroid/media/AudioFormat;IIIZILandroid/media/AudioTrack$TunerConfiguration;Landroid/media/AudioTrack$1;)V
      //   333: aload_1
      //   334: invokevirtual getState : ()I
      //   337: ifeq -> 342
      //   340: aload_1
      //   341: areturn
      //   342: new java/lang/UnsupportedOperationException
      //   345: astore_1
      //   346: aload_1
      //   347: ldc 'Cannot create AudioTrack'
      //   349: invokespecial <init> : (Ljava/lang/String;)V
      //   352: aload_1
      //   353: athrow
      //   354: astore_1
      //   355: new java/lang/UnsupportedOperationException
      //   358: dup
      //   359: aload_1
      //   360: invokevirtual getMessage : ()Ljava/lang/String;
      //   363: invokespecial <init> : (Ljava/lang/String;)V
      //   366: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1223	-> 0
      //   #1224	-> 7
      //   #1225	-> 15
      //   #1226	-> 21
      //   #1228	-> 29
      //   #1230	-> 51
      //   #1231	-> 68
      //   #1234	-> 85
      //   #1235	-> 93
      //   #1237	-> 96
      //   #1238	-> 118
      //   #1242	-> 121
      //   #1243	-> 138
      //   #1246	-> 155
      //   #1250	-> 163
      //   #1251	-> 170
      //   #1252	-> 178
      //   #1254	-> 185
      //   #1255	-> 191
      //   #1258	-> 199
      //   #1259	-> 206
      //   #1263	-> 214
      //   #1264	-> 231
      //   #1260	-> 241
      //   #1275	-> 251
      //   #1276	-> 266
      //   #1277	-> 279
      //   #1279	-> 292
      //   #1282	-> 333
      //   #1286	-> 340
      //   #1284	-> 342
      //   #1287	-> 354
      //   #1288	-> 355
      // Exception table:
      //   from	to	target	type
      //   251	266	354	java/lang/IllegalArgumentException
      //   266	279	354	java/lang/IllegalArgumentException
      //   279	292	354	java/lang/IllegalArgumentException
      //   292	333	354	java/lang/IllegalArgumentException
      //   333	340	354	java/lang/IllegalArgumentException
      //   342	354	354	java/lang/IllegalArgumentException
    }
  }
  
  public void setOffloadDelayPadding(int paramInt1, int paramInt2) {
    if (paramInt2 >= 0) {
      if (paramInt1 >= 0) {
        if (this.mOffloaded) {
          if (this.mState != 0) {
            this.mOffloadDelayFrames = paramInt1;
            this.mOffloadPaddingFrames = paramInt2;
            native_set_delay_padding(paramInt1, paramInt2);
            return;
          } 
          throw new IllegalStateException("Uninitialized track");
        } 
        throw new IllegalStateException("Illegal use of delay/padding on non-offloaded track");
      } 
      throw new IllegalArgumentException("Illegal negative delay");
    } 
    throw new IllegalArgumentException("Illegal negative padding");
  }
  
  public int getOffloadDelay() {
    if (this.mOffloaded) {
      if (this.mState != 0)
        return this.mOffloadDelayFrames; 
      throw new IllegalStateException("Illegal query of delay on uninitialized track");
    } 
    throw new IllegalStateException("Illegal query of delay on non-offloaded track");
  }
  
  public int getOffloadPadding() {
    if (this.mOffloaded) {
      if (this.mState != 0)
        return this.mOffloadPaddingFrames; 
      throw new IllegalStateException("Illegal query of padding on uninitialized track");
    } 
    throw new IllegalStateException("Illegal query of padding on non-offloaded track");
  }
  
  public void setOffloadEndOfStream() {
    if (this.mOffloaded) {
      if (this.mState != 0) {
        if (this.mPlayState == 3)
          synchronized (this.mStreamEventCbLock) {
            if (this.mStreamEventCbInfoList.size() != 0)
              synchronized (this.mPlayStateLock) {
                native_stop();
                this.mOffloadEosPending = true;
                this.mPlayState = 4;
                return;
              }  
            IllegalStateException illegalStateException = new IllegalStateException();
            this("EOS not supported without StreamEventCallback");
            throw illegalStateException;
          }  
        throw new IllegalStateException("EOS not supported if not playing");
      } 
      throw new IllegalStateException("Uninitialized track");
    } 
    throw new IllegalStateException("EOS not supported on non-offloaded track");
  }
  
  public boolean isOffloadedPlayback() {
    return this.mOffloaded;
  }
  
  public static boolean isDirectPlaybackSupported(AudioFormat paramAudioFormat, AudioAttributes paramAudioAttributes) {
    if (paramAudioFormat != null) {
      if (paramAudioAttributes != null) {
        int i = paramAudioFormat.getEncoding(), j = paramAudioFormat.getSampleRate();
        int k = paramAudioFormat.getChannelMask(), m = paramAudioFormat.getChannelIndexMask();
        int n = paramAudioAttributes.getContentType(), i1 = paramAudioAttributes.getUsage(), i2 = paramAudioAttributes.getFlags();
        return native_is_direct_output_supported(i, j, k, m, n, i1, i2);
      } 
      throw new IllegalArgumentException("Illegal null AudioAttributes argument");
    } 
    throw new IllegalArgumentException("Illegal null AudioFormat argument");
  }
  
  private static boolean isValidAudioDescriptionMixLevel(float paramFloat) {
    boolean bool;
    if (!Float.isNaN(paramFloat) && paramFloat <= 48.0F) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean setAudioDescriptionMixLeveldB(float paramFloat) {
    if (isValidAudioDescriptionMixLevel(paramFloat)) {
      boolean bool;
      if (native_set_audio_description_mix_level_db(paramFloat) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("level is out of range");
    stringBuilder.append(paramFloat);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public float getAudioDescriptionMixLeveldB() {
    float[] arrayOfFloat = new float[1];
    arrayOfFloat[0] = Float.NEGATIVE_INFINITY;
    try {
      int i = native_get_audio_description_mix_level_db(arrayOfFloat);
      if (i == 0) {
        boolean bool = Float.isNaN(arrayOfFloat[0]);
        if (!bool)
          return arrayOfFloat[0]; 
      } 
      return Float.NEGATIVE_INFINITY;
    } catch (Exception exception) {
      return Float.NEGATIVE_INFINITY;
    } 
  }
  
  private static boolean isValidDualMonoMode(int paramInt) {
    if (paramInt != 0 && paramInt != 1 && paramInt != 2 && paramInt != 3)
      return false; 
    return true;
  }
  
  public boolean setDualMonoMode(int paramInt) {
    if (isValidDualMonoMode(paramInt)) {
      boolean bool;
      if (native_set_dual_mono_mode(paramInt) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid Dual Mono mode ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getDualMonoMode() {
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = 0;
    try {
      int i = native_get_dual_mono_mode(arrayOfInt);
      if (i == 0) {
        boolean bool = isValidDualMonoMode(arrayOfInt[0]);
        if (bool)
          return arrayOfInt[0]; 
      } 
      return 0;
    } catch (Exception exception) {
      return 0;
    } 
  }
  
  private static boolean shouldEnablePowerSaving(AudioAttributes paramAudioAttributes, AudioFormat paramAudioFormat, int paramInt1, int paramInt2) {
    int i = paramAudioAttributes.getAllFlags();
    if (paramAudioAttributes != null)
      if ((i & 0x318) == 0) {
        if (paramAudioAttributes.getUsage() != 1 || (paramAudioAttributes.getContentType() != 0 && paramAudioAttributes.getContentType() != 2 && paramAudioAttributes.getContentType() != 3))
          return false; 
      } else {
        return false;
      }  
    if (paramAudioFormat == null || paramAudioFormat.getSampleRate() == 0 || !AudioFormat.isEncodingLinearPcm(paramAudioFormat.getEncoding()) || !AudioFormat.isValidEncoding(paramAudioFormat.getEncoding()) || paramAudioFormat.getChannelCount() < 1)
      return false; 
    if (paramInt2 != 1)
      return false; 
    if (paramInt1 != 0) {
      long l1 = paramAudioFormat.getChannelCount();
      long l2 = AudioFormat.getBytesPerSample(paramAudioFormat.getEncoding());
      l2 = l1 * 100L * l2 * paramAudioFormat.getSampleRate() / 1000L;
      if (paramInt1 < l2)
        return false; 
    } 
    return true;
  }
  
  private void audioParamCheck(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) {
    // Byte code:
    //   0: iload_1
    //   1: sipush #4000
    //   4: if_icmplt -> 14
    //   7: iload_1
    //   8: ldc_w 192000
    //   11: if_icmple -> 18
    //   14: iload_1
    //   15: ifne -> 343
    //   18: aload_0
    //   19: iload_1
    //   20: putfield mSampleRate : I
    //   23: iload #4
    //   25: bipush #13
    //   27: if_icmpne -> 50
    //   30: iload_2
    //   31: bipush #12
    //   33: if_icmpne -> 39
    //   36: goto -> 50
    //   39: new java/lang/IllegalArgumentException
    //   42: dup
    //   43: ldc_w 'ENCODING_IEC61937 must be configured as CHANNEL_OUT_STEREO'
    //   46: invokespecial <init> : (Ljava/lang/String;)V
    //   49: athrow
    //   50: aload_0
    //   51: iload_2
    //   52: putfield mChannelConfiguration : I
    //   55: iload_2
    //   56: iconst_1
    //   57: if_icmpeq -> 145
    //   60: iload_2
    //   61: iconst_2
    //   62: if_icmpeq -> 145
    //   65: iload_2
    //   66: iconst_3
    //   67: if_icmpeq -> 131
    //   70: iload_2
    //   71: iconst_4
    //   72: if_icmpeq -> 145
    //   75: iload_2
    //   76: bipush #12
    //   78: if_icmpeq -> 131
    //   81: iload_2
    //   82: ifne -> 97
    //   85: iload_3
    //   86: ifeq -> 97
    //   89: aload_0
    //   90: iconst_0
    //   91: putfield mChannelCount : I
    //   94: goto -> 155
    //   97: iload_2
    //   98: invokestatic isMultichannelConfigSupported : (I)Z
    //   101: ifeq -> 120
    //   104: aload_0
    //   105: iload_2
    //   106: putfield mChannelMask : I
    //   109: aload_0
    //   110: iload_2
    //   111: invokestatic channelCountFromOutChannelMask : (I)I
    //   114: putfield mChannelCount : I
    //   117: goto -> 155
    //   120: new java/lang/IllegalArgumentException
    //   123: dup
    //   124: ldc_w 'Unsupported channel configuration.'
    //   127: invokespecial <init> : (Ljava/lang/String;)V
    //   130: athrow
    //   131: aload_0
    //   132: iconst_2
    //   133: putfield mChannelCount : I
    //   136: aload_0
    //   137: bipush #12
    //   139: putfield mChannelMask : I
    //   142: goto -> 155
    //   145: aload_0
    //   146: iconst_1
    //   147: putfield mChannelCount : I
    //   150: aload_0
    //   151: iconst_4
    //   152: putfield mChannelMask : I
    //   155: aload_0
    //   156: iload_3
    //   157: putfield mChannelIndexMask : I
    //   160: iload_3
    //   161: ifeq -> 259
    //   164: getstatic android/media/AudioSystem.OUT_CHANNEL_COUNT_MAX : I
    //   167: istore_1
    //   168: iconst_1
    //   169: iload_1
    //   170: ishl
    //   171: iconst_1
    //   172: isub
    //   173: iconst_m1
    //   174: ixor
    //   175: iload_3
    //   176: iand
    //   177: ifne -> 221
    //   180: iload_3
    //   181: invokestatic bitCount : (I)I
    //   184: istore_2
    //   185: aload_0
    //   186: getfield mChannelCount : I
    //   189: istore_1
    //   190: iload_1
    //   191: ifne -> 202
    //   194: aload_0
    //   195: iload_2
    //   196: putfield mChannelCount : I
    //   199: goto -> 259
    //   202: iload_1
    //   203: iload_2
    //   204: if_icmpne -> 210
    //   207: goto -> 259
    //   210: new java/lang/IllegalArgumentException
    //   213: dup
    //   214: ldc_w 'Channel count must match'
    //   217: invokespecial <init> : (Ljava/lang/String;)V
    //   220: athrow
    //   221: new java/lang/StringBuilder
    //   224: dup
    //   225: invokespecial <init> : ()V
    //   228: astore #6
    //   230: aload #6
    //   232: ldc_w 'Unsupported channel index configuration '
    //   235: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   238: pop
    //   239: aload #6
    //   241: iload_3
    //   242: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   245: pop
    //   246: new java/lang/IllegalArgumentException
    //   249: dup
    //   250: aload #6
    //   252: invokevirtual toString : ()Ljava/lang/String;
    //   255: invokespecial <init> : (Ljava/lang/String;)V
    //   258: athrow
    //   259: iload #4
    //   261: istore_1
    //   262: iload #4
    //   264: iconst_1
    //   265: if_icmpne -> 270
    //   268: iconst_2
    //   269: istore_1
    //   270: iload_1
    //   271: invokestatic isPublicEncoding : (I)Z
    //   274: ifeq -> 332
    //   277: aload_0
    //   278: iload_1
    //   279: putfield mAudioFormat : I
    //   282: iload #5
    //   284: iconst_1
    //   285: if_icmpeq -> 293
    //   288: iload #5
    //   290: ifne -> 314
    //   293: iload #5
    //   295: iconst_1
    //   296: if_icmpeq -> 325
    //   299: aload_0
    //   300: getfield mAudioFormat : I
    //   303: istore_1
    //   304: iload_1
    //   305: invokestatic isEncodingLinearPcm : (I)Z
    //   308: ifeq -> 314
    //   311: goto -> 325
    //   314: new java/lang/IllegalArgumentException
    //   317: dup
    //   318: ldc_w 'Invalid mode.'
    //   321: invokespecial <init> : (Ljava/lang/String;)V
    //   324: athrow
    //   325: aload_0
    //   326: iload #5
    //   328: putfield mDataLoadMode : I
    //   331: return
    //   332: new java/lang/IllegalArgumentException
    //   335: dup
    //   336: ldc_w 'Unsupported audio encoding.'
    //   339: invokespecial <init> : (Ljava/lang/String;)V
    //   342: athrow
    //   343: new java/lang/StringBuilder
    //   346: dup
    //   347: invokespecial <init> : ()V
    //   350: astore #6
    //   352: aload #6
    //   354: iload_1
    //   355: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   358: pop
    //   359: aload #6
    //   361: ldc_w 'Hz is not a supported sample rate.'
    //   364: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   367: pop
    //   368: new java/lang/IllegalArgumentException
    //   371: dup
    //   372: aload #6
    //   374: invokevirtual toString : ()Ljava/lang/String;
    //   377: invokespecial <init> : (Ljava/lang/String;)V
    //   380: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1648	-> 0
    //   #1654	-> 18
    //   #1659	-> 23
    //   #1661	-> 39
    //   #1667	-> 50
    //   #1669	-> 55
    //   #1682	-> 81
    //   #1683	-> 89
    //   #1684	-> 94
    //   #1686	-> 97
    //   #1690	-> 104
    //   #1691	-> 109
    //   #1688	-> 120
    //   #1678	-> 131
    //   #1679	-> 136
    //   #1680	-> 142
    //   #1673	-> 145
    //   #1674	-> 150
    //   #1675	-> 155
    //   #1694	-> 155
    //   #1695	-> 160
    //   #1697	-> 164
    //   #1698	-> 168
    //   #1702	-> 180
    //   #1703	-> 185
    //   #1704	-> 194
    //   #1705	-> 202
    //   #1706	-> 210
    //   #1699	-> 221
    //   #1712	-> 259
    //   #1713	-> 268
    //   #1716	-> 270
    //   #1719	-> 277
    //   #1723	-> 282
    //   #1724	-> 304
    //   #1725	-> 314
    //   #1727	-> 325
    //   #1728	-> 331
    //   #1717	-> 332
    //   #1651	-> 343
  }
  
  private static boolean isMultichannelConfigSupported(int paramInt) {
    if ((paramInt & 0x1CFC) != paramInt) {
      loge("Channel configuration features unsupported channels");
      return false;
    } 
    int i = AudioFormat.channelCountFromOutChannelMask(paramInt);
    if (i > AudioSystem.OUT_CHANNEL_COUNT_MAX) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Channel configuration contains too many channels ");
      stringBuilder.append(i);
      stringBuilder.append(">");
      stringBuilder.append(AudioSystem.OUT_CHANNEL_COUNT_MAX);
      loge(stringBuilder.toString());
      return false;
    } 
    if ((paramInt & 0xC) != 12) {
      loge("Front channels must be present in multichannel configurations");
      return false;
    } 
    if ((paramInt & 0xC0) != 0 && (paramInt & 0xC0) != 192) {
      loge("Rear channels can't be used independently");
      return false;
    } 
    if ((paramInt & 0x1800) != 0 && (paramInt & 0x1800) != 6144) {
      loge("Side channels can't be used independently");
      return false;
    } 
    return true;
  }
  
  private void audioBuffSizeCheck(int paramInt) {
    byte b;
    if (AudioFormat.isEncodingLinearFrames(this.mAudioFormat)) {
      b = this.mChannelCount * AudioFormat.getBytesPerSample(this.mAudioFormat);
    } else {
      b = 1;
    } 
    if (paramInt % b == 0 && paramInt >= 1) {
      this.mNativeBufferSizeInBytes = paramInt;
      this.mNativeBufferSizeInFrames = paramInt / b;
      return;
    } 
    throw new IllegalArgumentException("Invalid audio buffer size.");
  }
  
  public void release() {
    synchronized (this.mStreamEventCbLock) {
      endStreamEventHandling();
      try {
        stop();
      } catch (IllegalStateException illegalStateException) {}
      baseRelease();
      native_release();
      synchronized (this.mPlayStateLock) {
        this.mState = 0;
        this.mPlayState = 1;
        this.mPlayStateLock.notify();
        return;
      } 
    } 
  }
  
  protected void finalize() {
    baseRelease();
    native_finalize();
  }
  
  public static float getMinVolume() {
    return 0.0F;
  }
  
  public static float getMaxVolume() {
    return 1.0F;
  }
  
  public int getSampleRate() {
    return this.mSampleRate;
  }
  
  public int getPlaybackRate() {
    return native_get_playback_rate();
  }
  
  public PlaybackParams getPlaybackParams() {
    return native_get_playback_params();
  }
  
  public AudioAttributes getAudioAttributes() {
    if (this.mState != 0) {
      AudioAttributes audioAttributes = this.mConfiguredAudioAttributes;
      if (audioAttributes != null)
        return audioAttributes; 
    } 
    throw new IllegalStateException("track not initialized");
  }
  
  public int getAudioFormat() {
    return this.mAudioFormat;
  }
  
  public int getStreamType() {
    return this.mStreamType;
  }
  
  public int getChannelConfiguration() {
    return this.mChannelConfiguration;
  }
  
  public AudioFormat getFormat() {
    AudioFormat.Builder builder = new AudioFormat.Builder();
    int i = this.mSampleRate;
    builder = builder.setSampleRate(i);
    i = this.mAudioFormat;
    builder = builder.setEncoding(i);
    i = this.mChannelConfiguration;
    if (i != 0)
      builder.setChannelMask(i); 
    i = this.mChannelIndexMask;
    if (i != 0)
      builder.setChannelIndexMask(i); 
    return builder.build();
  }
  
  public int getChannelCount() {
    return this.mChannelCount;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public int getPlayState() {
    synchronized (this.mPlayStateLock) {
      int i = this.mPlayState;
      if (i != 4) {
        if (i != 5) {
          i = this.mPlayState;
          return i;
        } 
        return 2;
      } 
      return 3;
    } 
  }
  
  public int getBufferSizeInFrames() {
    return native_get_buffer_size_frames();
  }
  
  public int setBufferSizeInFrames(int paramInt) {
    if (this.mDataLoadMode == 0 || this.mState == 0)
      return -3; 
    if (paramInt < 0)
      return -2; 
    return native_set_buffer_size_frames(paramInt);
  }
  
  public int getBufferCapacityInFrames() {
    return native_get_buffer_capacity_frames();
  }
  
  @Deprecated
  protected int getNativeFrameCount() {
    return native_get_buffer_capacity_frames();
  }
  
  public int getNotificationMarkerPosition() {
    return native_get_marker_pos();
  }
  
  public int getPositionNotificationPeriod() {
    return native_get_pos_update_period();
  }
  
  public int getPlaybackHeadPosition() {
    return native_get_position();
  }
  
  public int getLatency() {
    return native_get_latency();
  }
  
  public int getUnderrunCount() {
    return native_get_underrun_count();
  }
  
  public int getPerformanceMode() {
    int i = native_get_flags();
    if ((i & 0x4) != 0)
      return 1; 
    if ((i & 0x8) != 0)
      return 2; 
    return 0;
  }
  
  public static int getNativeOutputSampleRate(int paramInt) {
    return native_get_output_sample_rate(paramInt);
  }
  
  public static int getMinBufferSize(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: iload_0
    //   1: istore_3
    //   2: iload_0
    //   3: ifne -> 121
    //   6: new java/lang/StringBuilder
    //   9: astore #4
    //   11: aload #4
    //   13: invokespecial <init> : ()V
    //   16: aload #4
    //   18: ldc_w 'get_listinfo_bypid=at-sampleRate='
    //   21: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   24: pop
    //   25: aload #4
    //   27: invokestatic getCallingPid : ()I
    //   30: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   33: pop
    //   34: aload #4
    //   36: invokevirtual toString : ()Ljava/lang/String;
    //   39: astore #4
    //   41: aconst_null
    //   42: invokestatic getInstance : (Landroid/content/Context;)Lcom/oppo/atlas/OppoAtlasManager;
    //   45: aload #4
    //   47: invokevirtual getParameters : (Ljava/lang/String;)Ljava/lang/String;
    //   50: astore #4
    //   52: iload_0
    //   53: istore_3
    //   54: aload #4
    //   56: ifnull -> 114
    //   59: aload #4
    //   61: invokestatic parseInt : (Ljava/lang/String;)I
    //   64: istore #5
    //   66: new java/lang/StringBuilder
    //   69: astore #4
    //   71: aload #4
    //   73: invokespecial <init> : ()V
    //   76: aload #4
    //   78: ldc_w 'getMinBufferSize sampleRate:'
    //   81: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   84: pop
    //   85: aload #4
    //   87: iload #5
    //   89: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   92: pop
    //   93: ldc 'android.media.AudioTrack'
    //   95: aload #4
    //   97: invokevirtual toString : ()Ljava/lang/String;
    //   100: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   103: pop
    //   104: iload_0
    //   105: istore_3
    //   106: iload #5
    //   108: ifle -> 114
    //   111: iload #5
    //   113: istore_3
    //   114: goto -> 121
    //   117: astore #4
    //   119: iload_0
    //   120: istore_3
    //   121: iload_1
    //   122: iconst_2
    //   123: if_icmpeq -> 171
    //   126: iload_1
    //   127: iconst_3
    //   128: if_icmpeq -> 166
    //   131: iload_1
    //   132: iconst_4
    //   133: if_icmpeq -> 171
    //   136: iload_1
    //   137: bipush #12
    //   139: if_icmpeq -> 166
    //   142: iload_1
    //   143: invokestatic isMultichannelConfigSupported : (I)Z
    //   146: ifne -> 158
    //   149: ldc_w 'getMinBufferSize(): Invalid channel configuration.'
    //   152: invokestatic loge : (Ljava/lang/String;)V
    //   155: bipush #-2
    //   157: ireturn
    //   158: iload_1
    //   159: invokestatic channelCountFromOutChannelMask : (I)I
    //   162: istore_0
    //   163: goto -> 173
    //   166: iconst_2
    //   167: istore_0
    //   168: goto -> 173
    //   171: iconst_1
    //   172: istore_0
    //   173: iload_2
    //   174: invokestatic isPublicEncoding : (I)Z
    //   177: ifne -> 189
    //   180: ldc_w 'getMinBufferSize(): Invalid audio format.'
    //   183: invokestatic loge : (Ljava/lang/String;)V
    //   186: bipush #-2
    //   188: ireturn
    //   189: iload_3
    //   190: sipush #4000
    //   193: if_icmplt -> 227
    //   196: iload_3
    //   197: ldc_w 192000
    //   200: if_icmple -> 206
    //   203: goto -> 227
    //   206: iload_3
    //   207: iload_0
    //   208: iload_2
    //   209: invokestatic native_get_min_buff_size : (III)I
    //   212: istore_0
    //   213: iload_0
    //   214: ifgt -> 225
    //   217: ldc_w 'getMinBufferSize(): error querying hardware'
    //   220: invokestatic loge : (Ljava/lang/String;)V
    //   223: iconst_m1
    //   224: ireturn
    //   225: iload_0
    //   226: ireturn
    //   227: new java/lang/StringBuilder
    //   230: dup
    //   231: invokespecial <init> : ()V
    //   234: astore #4
    //   236: aload #4
    //   238: ldc_w 'getMinBufferSize(): '
    //   241: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   244: pop
    //   245: aload #4
    //   247: iload_3
    //   248: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   251: pop
    //   252: aload #4
    //   254: ldc_w ' Hz is not a supported sample rate.'
    //   257: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   260: pop
    //   261: aload #4
    //   263: invokevirtual toString : ()Ljava/lang/String;
    //   266: invokestatic loge : (Ljava/lang/String;)V
    //   269: bipush #-2
    //   271: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2193	-> 0
    //   #2195	-> 6
    //   #2196	-> 25
    //   #2197	-> 41
    //   #2198	-> 52
    //   #2199	-> 59
    //   #2200	-> 66
    //   #2201	-> 104
    //   #2202	-> 111
    //   #2206	-> 114
    //   #2205	-> 117
    //   #2209	-> 121
    //   #2210	-> 121
    //   #2220	-> 142
    //   #2221	-> 149
    //   #2222	-> 155
    //   #2224	-> 158
    //   #2217	-> 166
    //   #2218	-> 168
    //   #2213	-> 171
    //   #2214	-> 173
    //   #2228	-> 173
    //   #2229	-> 180
    //   #2230	-> 186
    //   #2235	-> 189
    //   #2241	-> 206
    //   #2242	-> 213
    //   #2243	-> 217
    //   #2244	-> 223
    //   #2247	-> 225
    //   #2237	-> 227
    //   #2238	-> 269
    // Exception table:
    //   from	to	target	type
    //   6	25	117	java/lang/NumberFormatException
    //   25	41	117	java/lang/NumberFormatException
    //   41	52	117	java/lang/NumberFormatException
    //   59	66	117	java/lang/NumberFormatException
    //   66	104	117	java/lang/NumberFormatException
  }
  
  public int getAudioSessionId() {
    return this.mSessionId;
  }
  
  public boolean getTimestamp(AudioTimestamp paramAudioTimestamp) {
    if (paramAudioTimestamp != null) {
      long[] arrayOfLong = new long[2];
      int i = native_get_timestamp(arrayOfLong);
      if (i != 0)
        return false; 
      paramAudioTimestamp.framePosition = arrayOfLong[0];
      paramAudioTimestamp.nanoTime = arrayOfLong[1];
      return true;
    } 
    throw new IllegalArgumentException();
  }
  
  public int getTimestampWithStatus(AudioTimestamp paramAudioTimestamp) {
    if (paramAudioTimestamp != null) {
      long[] arrayOfLong = new long[2];
      int i = native_get_timestamp(arrayOfLong);
      paramAudioTimestamp.framePosition = arrayOfLong[0];
      paramAudioTimestamp.nanoTime = arrayOfLong[1];
      return i;
    } 
    throw new IllegalArgumentException();
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  public void setPlaybackPositionUpdateListener(OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener) {
    setPlaybackPositionUpdateListener(paramOnPlaybackPositionUpdateListener, (Handler)null);
  }
  
  public void setPlaybackPositionUpdateListener(OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener, Handler paramHandler) {
    if (paramOnPlaybackPositionUpdateListener != null) {
      this.mEventHandlerDelegate = new NativePositionEventHandlerDelegate(this, paramOnPlaybackPositionUpdateListener, paramHandler);
    } else {
      this.mEventHandlerDelegate = null;
    } 
  }
  
  private static float clampGainOrLevel(float paramFloat) {
    if (!Float.isNaN(paramFloat)) {
      float f;
      if (paramFloat < 0.0F) {
        f = 0.0F;
      } else {
        f = paramFloat;
        if (paramFloat > 1.0F)
          f = 1.0F; 
      } 
      return f;
    } 
    throw new IllegalArgumentException();
  }
  
  @Deprecated
  public int setStereoVolume(float paramFloat1, float paramFloat2) {
    if (this.mState == 0)
      return -3; 
    baseSetVolume(paramFloat1, paramFloat2);
    return 0;
  }
  
  void playerSetVolume(boolean paramBoolean, float paramFloat1, float paramFloat2) {
    float f1 = 0.0F;
    if (paramBoolean)
      paramFloat1 = 0.0F; 
    float f2 = clampGainOrLevel(paramFloat1);
    if (paramBoolean) {
      paramFloat1 = f1;
    } else {
      paramFloat1 = paramFloat2;
    } 
    paramFloat1 = clampGainOrLevel(paramFloat1);
    native_setVolume(f2, paramFloat1);
  }
  
  public int setVolume(float paramFloat) {
    return setStereoVolume(paramFloat, paramFloat);
  }
  
  int playerApplyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) {
    return native_applyVolumeShaper(paramConfiguration, paramOperation);
  }
  
  VolumeShaper.State playerGetVolumeShaperState(int paramInt) {
    return native_getVolumeShaperState(paramInt);
  }
  
  public VolumeShaper createVolumeShaper(VolumeShaper.Configuration paramConfiguration) {
    return new VolumeShaper(paramConfiguration, this);
  }
  
  public int setPlaybackRate(int paramInt) {
    if (this.mState != 1)
      return -3; 
    if (paramInt <= 0)
      return -2; 
    return native_set_playback_rate(paramInt);
  }
  
  public void setPlaybackParams(PlaybackParams paramPlaybackParams) {
    if (paramPlaybackParams != null) {
      native_set_playback_params(paramPlaybackParams);
      return;
    } 
    throw new IllegalArgumentException("params is null");
  }
  
  public int setNotificationMarkerPosition(int paramInt) {
    if (this.mState == 0)
      return -3; 
    return native_set_marker_pos(paramInt);
  }
  
  public int setPositionNotificationPeriod(int paramInt) {
    if (this.mState == 0)
      return -3; 
    return native_set_pos_update_period(paramInt);
  }
  
  public int setPlaybackHeadPosition(int paramInt) {
    if (this.mDataLoadMode == 1 || this.mState == 0 || getPlayState() == 3)
      return -3; 
    if (paramInt < 0 || paramInt > this.mNativeBufferSizeInFrames)
      return -2; 
    return native_set_position(paramInt);
  }
  
  public int setLoopPoints(int paramInt1, int paramInt2, int paramInt3) {
    if (this.mDataLoadMode == 1 || this.mState == 0 || getPlayState() == 3)
      return -3; 
    if (paramInt3 != 0) {
      if (paramInt1 >= 0) {
        int i = this.mNativeBufferSizeInFrames;
        if (paramInt1 >= i || paramInt1 >= paramInt2 || paramInt2 > i)
          return -2; 
        return native_set_loop(paramInt1, paramInt2, paramInt3);
      } 
      return -2;
    } 
    return native_set_loop(paramInt1, paramInt2, paramInt3);
  }
  
  public int setPresentation(AudioPresentation paramAudioPresentation) {
    if (paramAudioPresentation != null) {
      int i = paramAudioPresentation.getPresentationId();
      int j = paramAudioPresentation.getProgramId();
      return native_setPresentation(i, j);
    } 
    throw new IllegalArgumentException("audio presentation is null");
  }
  
  @Deprecated
  protected void setState(int paramInt) {
    this.mState = paramInt;
  }
  
  public void play() throws IllegalStateException {
    if (this.mState == 1) {
      int i = getStartDelayMs();
      if (i == 0) {
        startImpl();
      } else {
        Object object = new Object(this, i);
        object.start();
      } 
      return;
    } 
    throw new IllegalStateException("play() called on uninitialized AudioTrack.");
  }
  
  private void startImpl() {
    synchronized (this.mPlayStateLock) {
      baseStart();
      native_start();
      if (this.mPlayState == 5) {
        this.mPlayState = 4;
      } else {
        this.mPlayState = 3;
        this.mOffloadEosPending = false;
      } 
      return;
    } 
  }
  
  public void stop() throws IllegalStateException {
    if (this.mState == 1)
      synchronized (this.mPlayStateLock) {
        native_stop();
        baseStop();
        if (this.mOffloaded && this.mPlayState != 5) {
          this.mPlayState = 4;
        } else {
          this.mPlayState = 1;
          this.mOffloadEosPending = false;
          this.mAvSyncHeader = null;
          this.mAvSyncBytesRemaining = 0;
          this.mPlayStateLock.notify();
        } 
        return;
      }  
    throw new IllegalStateException("stop() called on uninitialized AudioTrack.");
  }
  
  public void pause() throws IllegalStateException {
    if (this.mState == 1)
      synchronized (this.mPlayStateLock) {
        native_pause();
        basePause();
        if (this.mPlayState == 4) {
          this.mPlayState = 5;
        } else {
          this.mPlayState = 2;
        } 
        return;
      }  
    throw new IllegalStateException("pause() called on uninitialized AudioTrack.");
  }
  
  public void flush() {
    if (this.mState == 1) {
      native_flush();
      this.mAvSyncHeader = null;
      this.mAvSyncBytesRemaining = 0;
    } 
  }
  
  public int write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return write(paramArrayOfbyte, paramInt1, paramInt2, 0);
  }
  
  public int write(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (this.mState == 0 || this.mAudioFormat == 4)
      return -3; 
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOfbyte == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOfbyte.length)
      return -2; 
    if (!blockUntilOffloadDrain(paramInt3))
      return 0; 
    int i = this.mAudioFormat;
    if (paramInt3 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramInt1 = native_write_byte(paramArrayOfbyte, paramInt1, paramInt2, i, bool);
    if (this.mDataLoadMode == 0 && this.mState == 2 && paramInt1 > 0)
      this.mState = 1; 
    return paramInt1;
  }
  
  public int write(short[] paramArrayOfshort, int paramInt1, int paramInt2) {
    return write(paramArrayOfshort, paramInt1, paramInt2, 0);
  }
  
  public int write(short[] paramArrayOfshort, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (this.mState == 0 || this.mAudioFormat == 4)
      return -3; 
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOfshort == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOfshort.length)
      return -2; 
    if (!blockUntilOffloadDrain(paramInt3))
      return 0; 
    int i = this.mAudioFormat;
    if (paramInt3 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramInt1 = native_write_short(paramArrayOfshort, paramInt1, paramInt2, i, bool);
    if (this.mDataLoadMode == 0 && this.mState == 2 && paramInt1 > 0)
      this.mState = 1; 
    return paramInt1;
  }
  
  public int write(float[] paramArrayOffloat, int paramInt1, int paramInt2, int paramInt3) {
    boolean bool;
    if (this.mState == 0) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called in invalid state STATE_UNINITIALIZED");
      return -3;
    } 
    if (this.mAudioFormat != 4) {
      Log.e("android.media.AudioTrack", "AudioTrack.write(float[] ...) requires format ENCODING_PCM_FLOAT");
      return -3;
    } 
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOffloat == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOffloat.length) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid array, offset, or size");
      return -2;
    } 
    if (!blockUntilOffloadDrain(paramInt3))
      return 0; 
    int i = this.mAudioFormat;
    if (paramInt3 == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    paramInt1 = native_write_float(paramArrayOffloat, paramInt1, paramInt2, i, bool);
    if (this.mDataLoadMode == 0 && this.mState == 2 && paramInt1 > 0)
      this.mState = 1; 
    return paramInt1;
  }
  
  public int write(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) {
    StringBuilder stringBuilder;
    if (this.mState == 0) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called in invalid state STATE_UNINITIALIZED");
      return -3;
    } 
    if (paramInt2 != 0 && paramInt2 != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid blocking mode");
      return -2;
    } 
    if (paramByteBuffer == null || paramInt1 < 0 || paramInt1 > paramByteBuffer.remaining()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("AudioTrack.write() called with invalid size (");
      stringBuilder.append(paramInt1);
      stringBuilder.append(") value");
      Log.e("android.media.AudioTrack", stringBuilder.toString());
      return -2;
    } 
    if (!blockUntilOffloadDrain(paramInt2))
      return 0; 
    if (stringBuilder.isDirect()) {
      boolean bool;
      int i = stringBuilder.position(), j = this.mAudioFormat;
      if (paramInt2 == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      paramInt1 = native_write_native_bytes((ByteBuffer)stringBuilder, i, paramInt1, j, bool);
    } else {
      boolean bool;
      byte[] arrayOfByte = NioUtils.unsafeArray((ByteBuffer)stringBuilder);
      int j = NioUtils.unsafeArrayOffset((ByteBuffer)stringBuilder), i = stringBuilder.position(), k = this.mAudioFormat;
      if (paramInt2 == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      paramInt1 = native_write_byte(arrayOfByte, i + j, paramInt1, k, bool);
    } 
    if (this.mDataLoadMode == 0 && this.mState == 2 && paramInt1 > 0)
      this.mState = 1; 
    if (paramInt1 > 0)
      stringBuilder.position(stringBuilder.position() + paramInt1); 
    return paramInt1;
  }
  
  public int write(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2, long paramLong) {
    StringBuilder stringBuilder;
    if (this.mState == 0) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called in invalid state STATE_UNINITIALIZED");
      return -3;
    } 
    if (paramInt2 != 0 && paramInt2 != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() called with invalid blocking mode");
      return -2;
    } 
    if (this.mDataLoadMode != 1) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() with timestamp called for non-streaming mode track");
      return -3;
    } 
    if ((this.mAttributes.getFlags() & 0x10) == 0) {
      Log.d("android.media.AudioTrack", "AudioTrack.write() called on a regular AudioTrack. Ignoring pts...");
      return write(paramByteBuffer, paramInt1, paramInt2);
    } 
    if (paramByteBuffer == null || paramInt1 < 0 || paramInt1 > paramByteBuffer.remaining()) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("AudioTrack.write() called with invalid size (");
      stringBuilder.append(paramInt1);
      stringBuilder.append(") value");
      Log.e("android.media.AudioTrack", stringBuilder.toString());
      return -2;
    } 
    if (!blockUntilOffloadDrain(paramInt2))
      return 0; 
    if (this.mAvSyncHeader == null) {
      ByteBuffer byteBuffer = ByteBuffer.allocate(this.mOffset);
      byteBuffer.order(ByteOrder.BIG_ENDIAN);
      this.mAvSyncHeader.putInt(1431633922);
    } 
    if (this.mAvSyncBytesRemaining == 0) {
      this.mAvSyncHeader.putInt(4, paramInt1);
      this.mAvSyncHeader.putLong(8, paramLong);
      this.mAvSyncHeader.putInt(16, this.mOffset);
      this.mAvSyncHeader.position(0);
      this.mAvSyncBytesRemaining = paramInt1;
    } 
    if (this.mAvSyncHeader.remaining() != 0) {
      ByteBuffer byteBuffer = this.mAvSyncHeader;
      int i = write(byteBuffer, byteBuffer.remaining(), paramInt2);
      if (i < 0) {
        Log.e("android.media.AudioTrack", "AudioTrack.write() could not write timestamp header!");
        this.mAvSyncHeader = null;
        this.mAvSyncBytesRemaining = 0;
        return i;
      } 
      if (this.mAvSyncHeader.remaining() > 0) {
        Log.v("android.media.AudioTrack", "AudioTrack.write() partial timestamp header written.");
        return 0;
      } 
    } 
    paramInt1 = Math.min(this.mAvSyncBytesRemaining, paramInt1);
    paramInt1 = write((ByteBuffer)stringBuilder, paramInt1, paramInt2);
    if (paramInt1 < 0) {
      Log.e("android.media.AudioTrack", "AudioTrack.write() could not write audio data!");
      this.mAvSyncHeader = null;
      this.mAvSyncBytesRemaining = 0;
      return paramInt1;
    } 
    this.mAvSyncBytesRemaining -= paramInt1;
    return paramInt1;
  }
  
  public int reloadStaticData() {
    if (this.mDataLoadMode == 1 || this.mState != 1)
      return -3; 
    return native_reload_static();
  }
  
  private boolean blockUntilOffloadDrain(int paramInt) {
    synchronized (this.mPlayStateLock) {
      while (true) {
        if (this.mPlayState == 4 || this.mPlayState == 5) {
          if (paramInt == 1)
            return false; 
          try {
            this.mPlayStateLock.wait();
          } catch (InterruptedException interruptedException) {}
          continue;
        } 
        return true;
      } 
    } 
  }
  
  public int attachAuxEffect(int paramInt) {
    if (this.mState == 0)
      return -3; 
    return native_attachAuxEffect(paramInt);
  }
  
  public int setAuxEffectSendLevel(float paramFloat) {
    if (this.mState == 0)
      return -3; 
    return baseSetAuxEffectSendLevel(paramFloat);
  }
  
  int playerSetAuxEffectSendLevel(boolean paramBoolean, float paramFloat) {
    if (paramBoolean)
      paramFloat = 0.0F; 
    paramFloat = clampGainOrLevel(paramFloat);
    int i = native_setAuxEffectSendLevel(paramFloat);
    if (i == 0) {
      i = 0;
    } else {
      i = -1;
    } 
    return i;
  }
  
  public boolean setPreferredDevice(AudioDeviceInfo paramAudioDeviceInfo) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_1
    //   3: ifnull -> 15
    //   6: aload_1
    //   7: invokevirtual isSink : ()Z
    //   10: ifne -> 15
    //   13: iconst_0
    //   14: ireturn
    //   15: aload_1
    //   16: ifnull -> 24
    //   19: aload_1
    //   20: invokevirtual getId : ()I
    //   23: istore_2
    //   24: aload_0
    //   25: iload_2
    //   26: invokespecial native_setOutputDevice : (I)Z
    //   29: istore_3
    //   30: iload_3
    //   31: iconst_1
    //   32: if_icmpne -> 52
    //   35: aload_0
    //   36: monitorenter
    //   37: aload_0
    //   38: aload_1
    //   39: putfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   42: aload_0
    //   43: monitorexit
    //   44: goto -> 52
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    //   52: iload_3
    //   53: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3466	-> 0
    //   #3467	-> 13
    //   #3469	-> 15
    //   #3470	-> 24
    //   #3471	-> 30
    //   #3472	-> 35
    //   #3473	-> 37
    //   #3474	-> 42
    //   #3476	-> 52
    // Exception table:
    //   from	to	target	type
    //   37	42	47	finally
    //   42	44	47	finally
    //   48	50	47	finally
  }
  
  public AudioDeviceInfo getPreferredDevice() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3485	-> 0
    //   #3486	-> 2
    //   #3487	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public AudioDeviceInfo getRoutedDevice() {
    int i = native_getRoutedDeviceId();
    if (i == 0)
      return null; 
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = AudioManager.getDevicesStatic(2);
    for (byte b = 0; b < arrayOfAudioDeviceInfo.length; b++) {
      if (arrayOfAudioDeviceInfo[b].getId() == i)
        return arrayOfAudioDeviceInfo[b]; 
    } 
    return null;
  }
  
  private void testEnableNativeRoutingCallbacksLocked() {
    if (this.mRoutingChangeListeners.size() == 0)
      native_enableDeviceCallback(); 
  }
  
  private void testDisableNativeRoutingCallbacksLocked() {
    if (this.mRoutingChangeListeners.size() == 0)
      native_disableDeviceCallback(); 
  }
  
  public void addOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 73
    //   11: aload_0
    //   12: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   19: ifne -> 73
    //   22: aload_0
    //   23: invokespecial testEnableNativeRoutingCallbacksLocked : ()V
    //   26: aload_0
    //   27: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   30: astore #4
    //   32: new android/media/NativeRoutingEventHandlerDelegate
    //   35: astore #5
    //   37: aload_2
    //   38: ifnull -> 44
    //   41: goto -> 56
    //   44: new android/os/Handler
    //   47: dup
    //   48: aload_0
    //   49: getfield mInitializationLooper : Landroid/os/Looper;
    //   52: invokespecial <init> : (Landroid/os/Looper;)V
    //   55: astore_2
    //   56: aload #5
    //   58: aload_0
    //   59: aload_1
    //   60: aload_2
    //   61: invokespecial <init> : (Landroid/media/AudioRouting;Landroid/media/AudioRouting$OnRoutingChangedListener;Landroid/os/Handler;)V
    //   64: aload #4
    //   66: aload_1
    //   67: aload #5
    //   69: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   72: pop
    //   73: aload_3
    //   74: monitorexit
    //   75: return
    //   76: astore_1
    //   77: aload_3
    //   78: monitorexit
    //   79: aload_1
    //   80: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3555	-> 0
    //   #3556	-> 7
    //   #3557	-> 22
    //   #3558	-> 26
    //   #3560	-> 37
    //   #3558	-> 64
    //   #3562	-> 73
    //   #3563	-> 75
    //   #3562	-> 76
    // Exception table:
    //   from	to	target	type
    //   11	22	76	finally
    //   22	26	76	finally
    //   26	37	76	finally
    //   44	56	76	finally
    //   56	64	76	finally
    //   64	73	76	finally
    //   73	75	76	finally
    //   77	79	76	finally
  }
  
  public void removeOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener) {
    synchronized (this.mRoutingChangeListeners) {
      if (this.mRoutingChangeListeners.containsKey(paramOnRoutingChangedListener))
        this.mRoutingChangeListeners.remove(paramOnRoutingChangedListener); 
      testDisableNativeRoutingCallbacksLocked();
      return;
    } 
  }
  
  @Deprecated
  public static interface OnRoutingChangedListener extends AudioRouting.OnRoutingChangedListener {
    default void onRoutingChanged(AudioRouting param1AudioRouting) {
      if (param1AudioRouting instanceof AudioTrack)
        onRoutingChanged((AudioTrack)param1AudioRouting); 
    }
    
    void onRoutingChanged(AudioTrack param1AudioTrack);
  }
  
  @Deprecated
  public void addOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    addOnRoutingChangedListener(paramOnRoutingChangedListener, paramHandler);
  }
  
  @Deprecated
  public void removeOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener) {
    removeOnRoutingChangedListener(paramOnRoutingChangedListener);
  }
  
  private void broadcastRoutingChange() {
    AudioManager.resetAudioPortGeneration();
    synchronized (this.mRoutingChangeListeners) {
      for (NativeRoutingEventHandlerDelegate nativeRoutingEventHandlerDelegate : this.mRoutingChangeListeners.values())
        nativeRoutingEventHandlerDelegate.notifyClient(); 
      return;
    } 
  }
  
  public void addOnCodecFormatChangedListener(Executor paramExecutor, OnCodecFormatChangedListener paramOnCodecFormatChangedListener) {
    this.mCodecFormatChangedListeners.add(paramOnCodecFormatChangedListener, paramExecutor, new _$$Lambda$AudioTrack$_tggs0CIzmnwn1nRK8KlBjbHnSE(this, paramOnCodecFormatChangedListener));
  }
  
  public void removeOnCodecFormatChangedListener(OnCodecFormatChangedListener paramOnCodecFormatChangedListener) {
    this.mCodecFormatChangedListeners.remove(paramOnCodecFormatChangedListener);
  }
  
  class StreamEventCallback {
    public void onTearDown(AudioTrack param1AudioTrack) {}
    
    public void onPresentationEnded(AudioTrack param1AudioTrack) {}
    
    public void onDataRequest(AudioTrack param1AudioTrack, int param1Int) {}
  }
  
  public void registerStreamEventCallback(Executor paramExecutor, StreamEventCallback paramStreamEventCallback) {
    if (paramStreamEventCallback != null) {
      if (this.mOffloaded) {
        if (paramExecutor != null)
          synchronized (this.mStreamEventCbLock) {
            IllegalArgumentException illegalArgumentException;
            for (StreamEventCbInfo streamEventCbInfo1 : this.mStreamEventCbInfoList) {
              if (streamEventCbInfo1.mStreamEventCb != paramStreamEventCallback)
                continue; 
              illegalArgumentException = new IllegalArgumentException();
              this("StreamEventCallback already registered");
              throw illegalArgumentException;
            } 
            beginStreamEventHandling();
            LinkedList<StreamEventCbInfo> linkedList = this.mStreamEventCbInfoList;
            StreamEventCbInfo streamEventCbInfo = new StreamEventCbInfo();
            this((Executor)illegalArgumentException, paramStreamEventCallback);
            linkedList.add(streamEventCbInfo);
            return;
          }  
        throw new IllegalArgumentException("Illegal null Executor for the StreamEventCallback");
      } 
      throw new IllegalStateException("Cannot register StreamEventCallback on non-offloaded AudioTrack");
    } 
    throw new IllegalArgumentException("Illegal null StreamEventCallback");
  }
  
  public void unregisterStreamEventCallback(StreamEventCallback paramStreamEventCallback) {
    if (paramStreamEventCallback != null) {
      if (this.mOffloaded)
        synchronized (this.mStreamEventCbLock) {
          for (StreamEventCbInfo streamEventCbInfo : this.mStreamEventCbInfoList) {
            if (streamEventCbInfo.mStreamEventCb == paramStreamEventCallback) {
              this.mStreamEventCbInfoList.remove(streamEventCbInfo);
              if (this.mStreamEventCbInfoList.size() == 0)
                endStreamEventHandling(); 
              return;
            } 
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          this("StreamEventCallback was not registered");
          throw illegalArgumentException;
        }  
      throw new IllegalStateException("No StreamEventCallback on non-offloaded AudioTrack");
    } 
    throw new IllegalArgumentException("Illegal null StreamEventCallback");
  }
  
  class StreamEventCbInfo {
    final AudioTrack.StreamEventCallback mStreamEventCb;
    
    final Executor mStreamEventExec;
    
    StreamEventCbInfo(AudioTrack this$0, AudioTrack.StreamEventCallback param1StreamEventCallback) {
      this.mStreamEventExec = (Executor)this$0;
      this.mStreamEventCb = param1StreamEventCallback;
    }
  }
  
  void handleStreamEventFromNative(int paramInt1, int paramInt2) {
    if (this.mStreamEventHandler == null)
      return; 
    if (paramInt1 != 6) {
      if (paramInt1 != 7) {
        if (paramInt1 == 9) {
          this.mStreamEventHandler.removeMessages(9);
          StreamEventHandler streamEventHandler1 = this.mStreamEventHandler, streamEventHandler2 = this.mStreamEventHandler;
          Message message = streamEventHandler2.obtainMessage(9, paramInt2, 0);
          streamEventHandler1.sendMessage(message);
        } 
      } else {
        StreamEventHandler streamEventHandler1 = this.mStreamEventHandler, streamEventHandler2 = this.mStreamEventHandler;
        Message message = streamEventHandler2.obtainMessage(7);
        streamEventHandler1.sendMessage(message);
      } 
    } else {
      StreamEventHandler streamEventHandler1 = this.mStreamEventHandler, streamEventHandler2 = this.mStreamEventHandler;
      Message message = streamEventHandler2.obtainMessage(6);
      streamEventHandler1.sendMessage(message);
    } 
  }
  
  private class StreamEventHandler extends Handler {
    final AudioTrack this$0;
    
    StreamEventHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (AudioTrack.this.mStreamEventCbLock) {
        if (param1Message.what == 7)
          synchronized (AudioTrack.this.mPlayStateLock) {
            if (AudioTrack.this.mPlayState == 4) {
              if (AudioTrack.this.mOffloadEosPending) {
                AudioTrack.this.native_start();
                AudioTrack.access$502(AudioTrack.this, 3);
              } else {
                AudioTrack.access$802(AudioTrack.this, (ByteBuffer)null);
                AudioTrack.access$902(AudioTrack.this, 0);
                AudioTrack.access$502(AudioTrack.this, 1);
              } 
              AudioTrack.access$602(AudioTrack.this, false);
              AudioTrack.this.mPlayStateLock.notify();
            } 
          }  
        if (AudioTrack.this.mStreamEventCbInfoList.size() == 0)
          return; 
        LinkedList linkedList = new LinkedList();
        this((Collection)AudioTrack.this.mStreamEventCbInfoList);
        long l = Binder.clearCallingIdentity();
        try {
          for (Object null : linkedList) {
            int i = param1Message.what;
            if (i != 6) {
              if (i != 7) {
                if (i != 9)
                  continue; 
                Executor executor2 = ((AudioTrack.StreamEventCbInfo)null).mStreamEventExec;
                _$$Lambda$AudioTrack$StreamEventHandler$IUDediua4qA5AgKwU3zNCXA7jQo _$$Lambda$AudioTrack$StreamEventHandler$IUDediua4qA5AgKwU3zNCXA7jQo = new _$$Lambda$AudioTrack$StreamEventHandler$IUDediua4qA5AgKwU3zNCXA7jQo();
                this(this, (AudioTrack.StreamEventCbInfo)null, param1Message);
                executor2.execute(_$$Lambda$AudioTrack$StreamEventHandler$IUDediua4qA5AgKwU3zNCXA7jQo);
                continue;
              } 
              Executor executor1 = ((AudioTrack.StreamEventCbInfo)null).mStreamEventExec;
              _$$Lambda$AudioTrack$StreamEventHandler$_3NLz6Sbq0z_YUytzGW6tVjPCao _$$Lambda$AudioTrack$StreamEventHandler$_3NLz6Sbq0z_YUytzGW6tVjPCao = new _$$Lambda$AudioTrack$StreamEventHandler$_3NLz6Sbq0z_YUytzGW6tVjPCao();
              this(this, (AudioTrack.StreamEventCbInfo)null);
              executor1.execute(_$$Lambda$AudioTrack$StreamEventHandler$_3NLz6Sbq0z_YUytzGW6tVjPCao);
              continue;
            } 
            Executor executor = ((AudioTrack.StreamEventCbInfo)null).mStreamEventExec;
            _$$Lambda$AudioTrack$StreamEventHandler$uWnWUbk1g3MhAY3NoSFc6o37wsk _$$Lambda$AudioTrack$StreamEventHandler$uWnWUbk1g3MhAY3NoSFc6o37wsk = new _$$Lambda$AudioTrack$StreamEventHandler$uWnWUbk1g3MhAY3NoSFc6o37wsk();
            this(this, (AudioTrack.StreamEventCbInfo)null);
            executor.execute(_$$Lambda$AudioTrack$StreamEventHandler$uWnWUbk1g3MhAY3NoSFc6o37wsk);
          } 
          return;
        } finally {
          Binder.restoreCallingIdentity(l);
        } 
      } 
    }
  }
  
  private void beginStreamEventHandling() {
    if (this.mStreamEventHandlerThread == null) {
      HandlerThread handlerThread = new HandlerThread("android.media.AudioTrack.StreamEvent");
      handlerThread.start();
      Looper looper = this.mStreamEventHandlerThread.getLooper();
      if (looper != null)
        this.mStreamEventHandler = new StreamEventHandler(looper); 
    } 
  }
  
  private void endStreamEventHandling() {
    HandlerThread handlerThread = this.mStreamEventHandlerThread;
    if (handlerThread != null) {
      handlerThread.quit();
      this.mStreamEventHandlerThread = null;
    } 
  }
  
  class NativePositionEventHandlerDelegate {
    private final Handler mHandler;
    
    final AudioTrack this$0;
    
    NativePositionEventHandlerDelegate(final AudioTrack track, final AudioTrack.OnPlaybackPositionUpdateListener listener, Handler param1Handler) {
      Looper looper;
      if (param1Handler != null) {
        looper = param1Handler.getLooper();
      } else {
        looper = AudioTrack.this.mInitializationLooper;
      } 
      if (looper != null) {
        this.mHandler = new Handler(looper) {
            final AudioTrack.NativePositionEventHandlerDelegate this$1;
            
            final AudioTrack.OnPlaybackPositionUpdateListener val$listener;
            
            final AudioTrack val$this$0;
            
            final AudioTrack val$track;
            
            Handler(Looper param1Looper) {
              super(param1Looper);
            }
            
            public void handleMessage(Message param1Message) {
              if (track == null)
                return; 
              int i = param1Message.what;
              if (i != 3) {
                if (i != 4) {
                  StringBuilder stringBuilder = new StringBuilder();
                  stringBuilder.append("Unknown native event type: ");
                  stringBuilder.append(param1Message.what);
                  AudioTrack.loge(stringBuilder.toString());
                } else {
                  AudioTrack.OnPlaybackPositionUpdateListener onPlaybackPositionUpdateListener = listener;
                  if (onPlaybackPositionUpdateListener != null)
                    onPlaybackPositionUpdateListener.onPeriodicNotification(track); 
                } 
              } else {
                AudioTrack.OnPlaybackPositionUpdateListener onPlaybackPositionUpdateListener = listener;
                if (onPlaybackPositionUpdateListener != null)
                  onPlaybackPositionUpdateListener.onMarkerReached(track); 
              } 
            }
          };
      } else {
        this.mHandler = null;
      } 
    }
    
    Handler getHandler() {
      return this.mHandler;
    }
  }
  
  class null extends Handler {
    final AudioTrack.NativePositionEventHandlerDelegate this$1;
    
    final AudioTrack.OnPlaybackPositionUpdateListener val$listener;
    
    final AudioTrack val$this$0;
    
    final AudioTrack val$track;
    
    null(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      if (track == null)
        return; 
      int i = param1Message.what;
      if (i != 3) {
        if (i != 4) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Unknown native event type: ");
          stringBuilder.append(param1Message.what);
          AudioTrack.loge(stringBuilder.toString());
        } else {
          AudioTrack.OnPlaybackPositionUpdateListener onPlaybackPositionUpdateListener = listener;
          if (onPlaybackPositionUpdateListener != null)
            onPlaybackPositionUpdateListener.onPeriodicNotification(track); 
        } 
      } else {
        AudioTrack.OnPlaybackPositionUpdateListener onPlaybackPositionUpdateListener = listener;
        if (onPlaybackPositionUpdateListener != null)
          onPlaybackPositionUpdateListener.onMarkerReached(track); 
      } 
    }
  }
  
  void playerStart() {
    play();
  }
  
  void playerPause() {
    pause();
  }
  
  void playerStop() {
    stop();
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<AudioTrack>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    if (paramInt1 == 1000) {
      paramObject1.broadcastRoutingChange();
      return;
    } 
    if (paramInt1 == 100) {
      paramObject2 = paramObject2;
      paramObject2.order(ByteOrder.nativeOrder());
      paramObject2.rewind();
      paramObject2 = AudioMetadata.fromByteBuffer((ByteBuffer)paramObject2);
      if (paramObject2 == null) {
        Log.e("android.media.AudioTrack", "Unable to get audio metadata from byte buffer");
        return;
      } 
      ((AudioTrack)paramObject1).mCodecFormatChangedListeners.notify(0, paramObject2);
      return;
    } 
    if (paramInt1 == 9 || paramInt1 == 6 || paramInt1 == 7) {
      paramObject1.handleStreamEventFromNative(paramInt1, paramInt2);
      return;
    } 
    paramObject1 = ((AudioTrack)paramObject1).mEventHandlerDelegate;
    if (paramObject1 != null) {
      paramObject1 = paramObject1.getHandler();
      if (paramObject1 != null) {
        paramObject2 = paramObject1.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
        paramObject1.sendMessage((Message)paramObject2);
      } 
    } 
  }
  
  private static void logd(String paramString) {
    Log.d("android.media.AudioTrack", paramString);
  }
  
  private static void loge(String paramString) {
    Log.e("android.media.AudioTrack", paramString);
  }
  
  private native int native_applyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation);
  
  private final native int native_attachAuxEffect(int paramInt);
  
  private final native void native_disableDeviceCallback();
  
  private final native void native_enableDeviceCallback();
  
  private final native void native_finalize();
  
  private final native void native_flush();
  
  private native PersistableBundle native_getMetrics();
  
  private native int native_getPortId();
  
  private final native int native_getRoutedDeviceId();
  
  private native VolumeShaper.State native_getVolumeShaperState(int paramInt);
  
  private native int native_get_audio_description_mix_level_db(float[] paramArrayOffloat);
  
  private final native int native_get_buffer_capacity_frames();
  
  private final native int native_get_buffer_size_frames();
  
  private native int native_get_dual_mono_mode(int[] paramArrayOfint);
  
  private final native int native_get_flags();
  
  private final native int native_get_latency();
  
  private final native int native_get_marker_pos();
  
  private static final native int native_get_min_buff_size(int paramInt1, int paramInt2, int paramInt3);
  
  private static final native int native_get_output_sample_rate(int paramInt);
  
  private final native PlaybackParams native_get_playback_params();
  
  private final native int native_get_playback_rate();
  
  private final native int native_get_pos_update_period();
  
  private final native int native_get_position();
  
  private final native int native_get_timestamp(long[] paramArrayOflong);
  
  private final native int native_get_underrun_count();
  
  private static native boolean native_is_direct_output_supported(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7);
  
  private final native void native_pause();
  
  private final native int native_reload_static();
  
  private final native int native_setAuxEffectSendLevel(float paramFloat);
  
  private final native boolean native_setOutputDevice(int paramInt);
  
  private final native int native_setPresentation(int paramInt1, int paramInt2);
  
  private final native void native_setVolume(float paramFloat1, float paramFloat2);
  
  private native int native_set_audio_description_mix_level_db(float paramFloat);
  
  private final native int native_set_buffer_size_frames(int paramInt);
  
  private native void native_set_delay_padding(int paramInt1, int paramInt2);
  
  private native int native_set_dual_mono_mode(int paramInt);
  
  private final native int native_set_loop(int paramInt1, int paramInt2, int paramInt3);
  
  private final native int native_set_marker_pos(int paramInt);
  
  private final native void native_set_playback_params(PlaybackParams paramPlaybackParams);
  
  private final native int native_set_playback_rate(int paramInt);
  
  private final native int native_set_pos_update_period(int paramInt);
  
  private final native int native_set_position(int paramInt);
  
  private final native int native_setup(Object paramObject1, Object paramObject2, int[] paramArrayOfint1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int[] paramArrayOfint2, long paramLong, boolean paramBoolean, int paramInt6, Object paramObject3);
  
  private final native void native_start();
  
  private final native void native_stop();
  
  private final native int native_write_byte(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean);
  
  private final native int native_write_float(float[] paramArrayOffloat, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean);
  
  private final native int native_write_native_bytes(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean);
  
  private final native int native_write_short(short[] paramArrayOfshort, int paramInt1, int paramInt2, int paramInt3, boolean paramBoolean);
  
  public final native void native_release();
  
  class MetricsConstants {
    public static final String ATTRIBUTES = "android.media.audiotrack.attributes";
    
    @Deprecated
    public static final String CHANNELMASK = "android.media.audiorecord.channelmask";
    
    public static final String CHANNEL_MASK = "android.media.audiotrack.channelMask";
    
    public static final String CONTENTTYPE = "android.media.audiotrack.type";
    
    public static final String ENCODING = "android.media.audiotrack.encoding";
    
    public static final String FRAME_COUNT = "android.media.audiotrack.frameCount";
    
    private static final String MM_PREFIX = "android.media.audiotrack.";
    
    public static final String PORT_ID = "android.media.audiotrack.portId";
    
    @Deprecated
    public static final String SAMPLERATE = "android.media.audiorecord.samplerate";
    
    public static final String SAMPLE_RATE = "android.media.audiotrack.sampleRate";
    
    public static final String STREAMTYPE = "android.media.audiotrack.streamtype";
    
    public static final String USAGE = "android.media.audiotrack.usage";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class DualMonoMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EncapsulationMetadataType implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class EncapsulationMode implements Annotation {}
  
  class OnCodecFormatChangedListener {
    public abstract void onCodecFormatChanged(AudioTrack param1AudioTrack, AudioMetadataReadMap param1AudioMetadataReadMap);
  }
  
  class OnPlaybackPositionUpdateListener {
    public abstract void onMarkerReached(AudioTrack param1AudioTrack);
    
    public abstract void onPeriodicNotification(AudioTrack param1AudioTrack);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class PerformanceMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class TransferMode implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  class WriteMode implements Annotation {}
}
