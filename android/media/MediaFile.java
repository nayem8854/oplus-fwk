package android.media;

import java.util.HashMap;
import java.util.Locale;
import libcore.content.type.MimeMap;

public class MediaFile {
  @Deprecated
  private static final int FIRST_AUDIO_FILE_TYPE = 1;
  
  @Deprecated
  private static final int LAST_AUDIO_FILE_TYPE = 10;
  
  @Deprecated
  public static class MediaFileType {
    public final int fileType;
    
    public final String mimeType;
    
    MediaFileType(int param1Int, String param1String) {
      this.fileType = param1Int;
      this.mimeType = param1String;
    }
  }
  
  @Deprecated
  private static final HashMap<String, MediaFileType> sFileTypeMap = new HashMap<>();
  
  @Deprecated
  private static final HashMap<String, Integer> sFileTypeToFormatMap = new HashMap<>();
  
  private static final HashMap<Integer, String> sFormatToMimeTypeMap;
  
  private static final HashMap<String, Integer> sMimeTypeToFormatMap = new HashMap<>();
  
  static {
    sFormatToMimeTypeMap = new HashMap<>();
    addFileType(12297, "audio/mpeg");
    addFileType(12296, "audio/x-wav");
    addFileType(47361, "audio/x-ms-wma");
    addFileType(47362, "audio/ogg");
    addFileType(47363, "audio/aac");
    addFileType(47366, "audio/flac");
    addFileType(12295, "audio/x-aiff");
    addFileType(47491, "audio/mpeg");
    addFileType(12299, "video/mpeg");
    addFileType(47490, "video/mp4");
    addFileType(47492, "video/3gpp");
    addFileType(47492, "video/3gpp2");
    addFileType(12298, "video/avi");
    addFileType(47489, "video/x-ms-wmv");
    addFileType(12300, "video/x-ms-asf");
    addFileType(14337, "image/jpeg");
    addFileType(14343, "image/gif");
    addFileType(14347, "image/png");
    addFileType(14340, "image/x-ms-bmp");
    addFileType(14354, "image/heif");
    addFileType(14353, "image/x-adobe-dng");
    addFileType(14349, "image/tiff");
    addFileType(14349, "image/x-canon-cr2");
    addFileType(14349, "image/x-nikon-nrw");
    addFileType(14349, "image/x-sony-arw");
    addFileType(14349, "image/x-panasonic-rw2");
    addFileType(14349, "image/x-olympus-orf");
    addFileType(14349, "image/x-pentax-pef");
    addFileType(14349, "image/x-samsung-srw");
    addFileType(14338, "image/tiff");
    addFileType(14338, "image/x-nikon-nef");
    addFileType(14351, "image/jp2");
    addFileType(14352, "image/jpx");
    addFileType(47633, "audio/x-mpegurl");
    addFileType(47636, "audio/x-scpls");
    addFileType(47632, "application/vnd.ms-wpl");
    addFileType(47635, "video/x-ms-asf");
    addFileType(12292, "text/plain");
    addFileType(12293, "text/html");
    addFileType(47746, "text/xml");
    addFileType(47747, "application/msword");
    addFileType(47747, "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    addFileType(47749, "application/vnd.ms-excel");
    addFileType(47749, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    addFileType(47750, "application/vnd.ms-powerpoint");
    addFileType(47750, "application/vnd.openxmlformats-officedocument.presentationml.presentation");
  }
  
  @Deprecated
  static void addFileType(String paramString1, int paramInt, String paramString2) {}
  
  private static void addFileType(int paramInt, String paramString) {
    if (!sMimeTypeToFormatMap.containsKey(paramString))
      sMimeTypeToFormatMap.put(paramString, Integer.valueOf(paramInt)); 
    if (!sFormatToMimeTypeMap.containsKey(Integer.valueOf(paramInt)))
      sFormatToMimeTypeMap.put(Integer.valueOf(paramInt), paramString); 
  }
  
  @Deprecated
  public static boolean isAudioFileType(int paramInt) {
    return false;
  }
  
  @Deprecated
  public static boolean isVideoFileType(int paramInt) {
    return false;
  }
  
  @Deprecated
  public static boolean isImageFileType(int paramInt) {
    return false;
  }
  
  @Deprecated
  public static boolean isPlayListFileType(int paramInt) {
    return false;
  }
  
  @Deprecated
  public static boolean isDrmFileType(int paramInt) {
    return false;
  }
  
  @Deprecated
  public static MediaFileType getFileType(String paramString) {
    return null;
  }
  
  public static boolean isDocumentMimeType(String paramString) {
    if (paramString == null)
      return false; 
    paramString = normalizeMimeType(paramString);
    if (paramString.startsWith("text/"))
      return true; 
    switch (paramString.toLowerCase(Locale.ROOT)) {
      default:
        return false;
      case "application/epub+zip":
      case "application/msword":
      case "application/pdf":
      case "application/rtf":
      case "application/vnd.ms-excel":
      case "application/vnd.ms-excel.addin.macroenabled.12":
      case "application/vnd.ms-excel.sheet.binary.macroenabled.12":
      case "application/vnd.ms-excel.sheet.macroenabled.12":
      case "application/vnd.ms-excel.template.macroenabled.12":
      case "application/vnd.ms-powerpoint":
      case "application/vnd.ms-powerpoint.addin.macroenabled.12":
      case "application/vnd.ms-powerpoint.presentation.macroenabled.12":
      case "application/vnd.ms-powerpoint.slideshow.macroenabled.12":
      case "application/vnd.ms-powerpoint.template.macroenabled.12":
      case "application/vnd.ms-word.document.macroenabled.12":
      case "application/vnd.ms-word.template.macroenabled.12":
      case "application/vnd.oasis.opendocument.chart":
      case "application/vnd.oasis.opendocument.database":
      case "application/vnd.oasis.opendocument.formula":
      case "application/vnd.oasis.opendocument.graphics":
      case "application/vnd.oasis.opendocument.graphics-template":
      case "application/vnd.oasis.opendocument.presentation":
      case "application/vnd.oasis.opendocument.presentation-template":
      case "application/vnd.oasis.opendocument.spreadsheet":
      case "application/vnd.oasis.opendocument.spreadsheet-template":
      case "application/vnd.oasis.opendocument.text":
      case "application/vnd.oasis.opendocument.text-master":
      case "application/vnd.oasis.opendocument.text-template":
      case "application/vnd.oasis.opendocument.text-web":
      case "application/vnd.openxmlformats-officedocument.presentationml.presentation":
      case "application/vnd.openxmlformats-officedocument.presentationml.slideshow":
      case "application/vnd.openxmlformats-officedocument.presentationml.template":
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet":
      case "application/vnd.openxmlformats-officedocument.spreadsheetml.template":
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.document":
      case "application/vnd.openxmlformats-officedocument.wordprocessingml.template":
      case "application/vnd.stardivision.calc":
      case "application/vnd.stardivision.chart":
      case "application/vnd.stardivision.draw":
      case "application/vnd.stardivision.impress":
      case "application/vnd.stardivision.impress-packed":
      case "application/vnd.stardivision.mail":
      case "application/vnd.stardivision.math":
      case "application/vnd.stardivision.writer":
      case "application/vnd.stardivision.writer-global":
      case "application/vnd.sun.xml.calc":
      case "application/vnd.sun.xml.calc.template":
      case "application/vnd.sun.xml.draw":
      case "application/vnd.sun.xml.draw.template":
      case "application/vnd.sun.xml.impress":
      case "application/vnd.sun.xml.impress.template":
      case "application/vnd.sun.xml.math":
      case "application/vnd.sun.xml.writer":
      case "application/vnd.sun.xml.writer.global":
      case "application/vnd.sun.xml.writer.template":
      case "application/x-mspublisher":
        break;
    } 
    return true;
  }
  
  public static boolean isExifMimeType(String paramString) {
    return isImageMimeType(paramString);
  }
  
  public static boolean isAudioMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("audio/");
  }
  
  public static boolean isVideoMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("video/");
  }
  
  public static boolean isImageMimeType(String paramString) {
    return normalizeMimeType(paramString).startsWith("image/");
  }
  
  public static boolean isPlayListMimeType(String paramString) {
    byte b;
    paramString = normalizeMimeType(paramString);
    switch (paramString.hashCode()) {
      default:
        b = -1;
        break;
      case 1872259501:
        if (paramString.equals("application/vnd.ms-wpl")) {
          b = 0;
          break;
        } 
      case 264230524:
        if (paramString.equals("audio/x-mpegurl")) {
          b = 1;
          break;
        } 
      case -432766831:
        if (paramString.equals("audio/mpegurl")) {
          b = 2;
          break;
        } 
      case -622808459:
        if (paramString.equals("application/vnd.apple.mpegurl")) {
          b = 4;
          break;
        } 
      case -979095690:
        if (paramString.equals("application/x-mpegurl")) {
          b = 3;
          break;
        } 
      case -1165508903:
        if (paramString.equals("audio/x-scpls")) {
          b = 5;
          break;
        } 
    } 
    if (b != 0 && b != 1 && b != 2 && b != 3 && b != 4 && b != 5)
      return false; 
    return true;
  }
  
  public static boolean isDrmMimeType(String paramString) {
    return normalizeMimeType(paramString).equals("application/x-android-drm-fl");
  }
  
  public static String getFileTitle(String paramString) {
    int i = paramString.lastIndexOf('/');
    String str = paramString;
    if (i >= 0) {
      i++;
      str = paramString;
      if (i < paramString.length())
        str = paramString.substring(i); 
    } 
    i = str.lastIndexOf('.');
    paramString = str;
    if (i > 0)
      paramString = str.substring(0, i); 
    return paramString;
  }
  
  public static String getFileExtension(String paramString) {
    if (paramString == null)
      return null; 
    int i = paramString.lastIndexOf('.');
    if (i >= 0)
      return paramString.substring(i + 1); 
    return null;
  }
  
  @Deprecated
  public static int getFileTypeForMimeType(String paramString) {
    return 0;
  }
  
  public static String getMimeType(String paramString, int paramInt) {
    paramString = getMimeTypeForFile(paramString);
    if (!"application/octet-stream".equals(paramString))
      return paramString; 
    return getMimeTypeForFormatCode(paramInt);
  }
  
  public static String getMimeTypeForFile(String paramString) {
    paramString = getFileExtension(paramString);
    paramString = MimeMap.getDefault().guessMimeTypeFromExtension(paramString);
    if (paramString == null)
      paramString = "application/octet-stream"; 
    return paramString;
  }
  
  public static String getMimeTypeForFormatCode(int paramInt) {
    String str = sFormatToMimeTypeMap.get(Integer.valueOf(paramInt));
    if (str == null)
      str = "application/octet-stream"; 
    return str;
  }
  
  public static int getFormatCode(String paramString1, String paramString2) {
    int i = getFormatCodeForMimeType(paramString2);
    if (i != 12288)
      return i; 
    return getFormatCodeForFile(paramString1);
  }
  
  public static int getFormatCodeForFile(String paramString) {
    return getFormatCodeForMimeType(getMimeTypeForFile(paramString));
  }
  
  public static int getFormatCodeForMimeType(String paramString) {
    if (paramString == null)
      return 12288; 
    Integer integer2 = sMimeTypeToFormatMap.get(paramString);
    if (integer2 != null)
      return integer2.intValue(); 
    String str = normalizeMimeType(paramString);
    Integer integer1 = sMimeTypeToFormatMap.get(str);
    if (integer1 != null)
      return integer1.intValue(); 
    if (str.startsWith("audio/"))
      return 47360; 
    if (str.startsWith("video/"))
      return 47488; 
    if (str.startsWith("image/"))
      return 14336; 
    return 12288;
  }
  
  private static String normalizeMimeType(String paramString) {
    MimeMap mimeMap = MimeMap.getDefault();
    String str = mimeMap.guessExtensionFromMimeType(paramString);
    if (str != null) {
      String str1 = mimeMap.guessMimeTypeFromExtension(str);
      if (str1 != null)
        return str1; 
    } 
    if (paramString == null)
      paramString = "application/octet-stream"; 
    return paramString;
  }
}
