package android.media;

import android.util.Log;
import android.util.Pair;
import java.lang.reflect.ParameterizedType;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class AudioMetadata {
  private static final Charset AUDIO_METADATA_CHARSET;
  
  public static AudioMetadataMap createMap() {
    return new BaseMap();
  }
  
  public static class Format {
    public static final AudioMetadata.Key<Boolean> KEY_ATMOS_PRESENT;
    
    public static final AudioMetadata.Key<Integer> KEY_AUDIO_ENCODING;
    
    public static final AudioMetadata.Key<Integer> KEY_BIT_RATE = AudioMetadata.createKey("bitrate", Integer.class);
    
    public static final AudioMetadata.Key<Integer> KEY_BIT_WIDTH = AudioMetadata.createKey("bit-width", Integer.class);
    
    public static final AudioMetadata.Key<Integer> KEY_CHANNEL_MASK = AudioMetadata.createKey("channel-mask", Integer.class);
    
    public static final AudioMetadata.Key<Integer> KEY_HAS_ATMOS;
    
    public static final AudioMetadata.Key<String> KEY_MIME = AudioMetadata.createKey("mime", String.class);
    
    public static final AudioMetadata.Key<Integer> KEY_SAMPLE_RATE = AudioMetadata.createKey("sample-rate", Integer.class);
    
    static {
      KEY_ATMOS_PRESENT = AudioMetadata.createKey("atmos-present", Boolean.class);
      KEY_HAS_ATMOS = AudioMetadata.createKey("has-atmos", Integer.class);
      KEY_AUDIO_ENCODING = AudioMetadata.createKey("audio-encoding", Integer.class);
    }
  }
  
  public static <T> Key<T> createKey(String paramString, Class<T> paramClass) {
    return (Key<T>)new Object(paramString, paramClass);
  }
  
  class BaseMap implements AudioMetadataMap {
    public <T> boolean containsKey(AudioMetadata.Key<T> param1Key) {
      boolean bool;
      Pair pair = this.mHashMap.get(pairFromKey(param1Key));
      if (pair != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public AudioMetadataMap dup() {
      BaseMap baseMap = new BaseMap();
      baseMap.mHashMap.putAll(this.mHashMap);
      return baseMap;
    }
    
    public <T> T get(AudioMetadata.Key<T> param1Key) {
      Pair<AudioMetadata.Key<?>, Object> pair = this.mHashMap.get(pairFromKey(param1Key));
      return (T)getValueFromValuePair(pair);
    }
    
    public Set<AudioMetadata.Key<?>> keySet() {
      HashSet<AudioMetadata.Key> hashSet = new HashSet();
      for (Pair<AudioMetadata.Key<?>, Object> pair : this.mHashMap.values())
        hashSet.add((AudioMetadata.Key)pair.first); 
      return (Set)hashSet;
    }
    
    public <T> T remove(AudioMetadata.Key<T> param1Key) {
      Pair<AudioMetadata.Key<?>, Object> pair = this.mHashMap.remove(pairFromKey(param1Key));
      return (T)getValueFromValuePair(pair);
    }
    
    public <T> T set(AudioMetadata.Key<T> param1Key, T param1T) {
      Objects.requireNonNull(param1T);
      HashMap<Pair<String, Class<?>>, Pair<AudioMetadata.Key<?>, Object>> hashMap = this.mHashMap;
      Pair<AudioMetadata.Key<?>, Object> pair = hashMap.put(pairFromKey(param1Key), new Pair(param1Key, param1T));
      return (T)getValueFromValuePair(pair);
    }
    
    public int size() {
      return this.mHashMap.size();
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object == this)
        return true; 
      if (!(param1Object instanceof BaseMap))
        return false; 
      param1Object = param1Object;
      return this.mHashMap.equals(((BaseMap)param1Object).mHashMap);
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.mHashMap });
    }
    
    private static <T> Pair<String, Class<?>> pairFromKey(AudioMetadata.Key<T> param1Key) {
      Objects.requireNonNull(param1Key);
      return new Pair(param1Key.getName(), param1Key.getValueClass());
    }
    
    private static Object getValueFromValuePair(Pair<AudioMetadata.Key<?>, Object> param1Pair) {
      if (param1Pair == null)
        return null; 
      return param1Pair.second;
    }
    
    private final HashMap<Pair<String, Class<?>>, Pair<AudioMetadata.Key<?>, Object>> mHashMap = new HashMap<>();
  }
  
  private static final HashMap<Class, Integer> AUDIO_METADATA_OBJ_TYPES = new HashMap<Class, Integer>() {
    
    };
  
  private static final int AUDIO_METADATA_OBJ_TYPE_BASEMAP = 6;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_DOUBLE = 4;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_FLOAT = 3;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_INT = 1;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_LONG = 2;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_NONE = 0;
  
  private static final int AUDIO_METADATA_OBJ_TYPE_STRING = 5;
  
  private static final HashMap<Integer, DataPackage<?>> DATA_PACKAGES;
  
  private static final ObjectPackage OBJECT_PACKAGE;
  
  private static final String TAG = "AudioMetadata";
  
  static {
    AUDIO_METADATA_CHARSET = StandardCharsets.UTF_8;
    DATA_PACKAGES = new HashMap<Integer, DataPackage<?>>() {
      
      };
    OBJECT_PACKAGE = new ObjectPackage();
  }
  
  private static class AutoGrowByteBuffer {
    private static final int DOUBLE_BYTE_COUNT = 8;
    
    private static final int FLOAT_BYTE_COUNT = 4;
    
    private static final int INTEGER_BYTE_COUNT = 4;
    
    private static final int LONG_BYTE_COUNT = 8;
    
    private ByteBuffer mBuffer;
    
    AutoGrowByteBuffer() {
      this(1024);
    }
    
    AutoGrowByteBuffer(int param1Int) {
      this.mBuffer = ByteBuffer.allocateDirect(param1Int);
    }
    
    public ByteBuffer getRawByteBuffer() {
      int i = this.mBuffer.limit();
      int j = this.mBuffer.position();
      this.mBuffer.limit(j);
      this.mBuffer.position(0);
      ByteBuffer byteBuffer = this.mBuffer.slice();
      this.mBuffer.limit(i);
      this.mBuffer.position(j);
      return byteBuffer;
    }
    
    public ByteOrder order() {
      return this.mBuffer.order();
    }
    
    public int position() {
      return this.mBuffer.position();
    }
    
    public AutoGrowByteBuffer position(int param1Int) {
      this.mBuffer.position(param1Int);
      return this;
    }
    
    public AutoGrowByteBuffer order(ByteOrder param1ByteOrder) {
      this.mBuffer.order(param1ByteOrder);
      return this;
    }
    
    public AutoGrowByteBuffer putInt(int param1Int) {
      ensureCapacity(4);
      this.mBuffer.putInt(param1Int);
      return this;
    }
    
    public AutoGrowByteBuffer putLong(long param1Long) {
      ensureCapacity(8);
      this.mBuffer.putLong(param1Long);
      return this;
    }
    
    public AutoGrowByteBuffer putFloat(float param1Float) {
      ensureCapacity(4);
      this.mBuffer.putFloat(param1Float);
      return this;
    }
    
    public AutoGrowByteBuffer putDouble(double param1Double) {
      ensureCapacity(8);
      this.mBuffer.putDouble(param1Double);
      return this;
    }
    
    public AutoGrowByteBuffer put(byte[] param1ArrayOfbyte) {
      ensureCapacity(param1ArrayOfbyte.length);
      this.mBuffer.put(param1ArrayOfbyte);
      return this;
    }
    
    private void ensureCapacity(int param1Int) {
      if (this.mBuffer.remaining() < param1Int) {
        param1Int = this.mBuffer.position() + param1Int;
        if (param1Int <= 1073741823) {
          ByteBuffer byteBuffer = ByteBuffer.allocateDirect(param1Int << 1);
          byteBuffer.order(this.mBuffer.order());
          this.mBuffer.flip();
          byteBuffer.put(this.mBuffer);
          this.mBuffer = byteBuffer;
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Item memory requirements too large: ");
          stringBuilder.append(param1Int);
          throw new IllegalStateException(stringBuilder.toString());
        } 
      } 
    }
  }
  
  private static interface DataPackage<T> {
    default Class getMyType() {
      ParameterizedType parameterizedType = (ParameterizedType)getClass().getGenericInterfaces()[0];
      return (Class)parameterizedType.getActualTypeArguments()[0];
    }
    
    boolean pack(AudioMetadata.AutoGrowByteBuffer param1AutoGrowByteBuffer, T param1T);
    
    T unpack(ByteBuffer param1ByteBuffer);
  }
  
  class ObjectPackage implements DataPackage<Pair<Class, Object>> {
    private ObjectPackage() {}
    
    public Pair<Class, Object> unpack(ByteBuffer param1ByteBuffer) {
      StringBuilder stringBuilder;
      int i = param1ByteBuffer.getInt();
      AudioMetadata.DataPackage<Object> dataPackage = (AudioMetadata.DataPackage)AudioMetadata.DATA_PACKAGES.get(Integer.valueOf(i));
      if (dataPackage == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot find DataPackage for type:");
        stringBuilder.append(i);
        Log.e("AudioMetadata", stringBuilder.toString());
        return null;
      } 
      i = stringBuilder.getInt();
      int j = stringBuilder.position();
      Object object = dataPackage.unpack((ByteBuffer)stringBuilder);
      if (stringBuilder.position() - j != i) {
        Log.e("AudioMetadata", "Broken data package");
        return null;
      } 
      return new Pair(dataPackage.getMyType(), object);
    }
    
    public boolean pack(AudioMetadata.AutoGrowByteBuffer param1AutoGrowByteBuffer, Pair<Class, Object> param1Pair) {
      StringBuilder stringBuilder;
      Integer integer = (Integer)AudioMetadata.AUDIO_METADATA_OBJ_TYPES.get(param1Pair.first);
      if (integer == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot find data type for ");
        stringBuilder.append(param1Pair.first);
        Log.e("AudioMetadata", stringBuilder.toString());
        return false;
      } 
      AudioMetadata.DataPackage<Object> dataPackage = (AudioMetadata.DataPackage)AudioMetadata.DATA_PACKAGES.get(integer);
      if (dataPackage == null) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Cannot find DataPackage for type:");
        stringBuilder.append(integer);
        Log.e("AudioMetadata", stringBuilder.toString());
        return false;
      } 
      stringBuilder.putInt(integer.intValue());
      int i = stringBuilder.position();
      stringBuilder.putInt(0);
      int j = stringBuilder.position();
      if (!dataPackage.pack((AudioMetadata.AutoGrowByteBuffer)stringBuilder, param1Pair.second)) {
        stringBuilder = new StringBuilder();
        stringBuilder.append("Failed to pack object: ");
        stringBuilder.append(param1Pair.second);
        Log.i("AudioMetadata", stringBuilder.toString());
        return false;
      } 
      int k = stringBuilder.position();
      stringBuilder.position(i);
      stringBuilder.putInt(k - j);
      stringBuilder.position(k);
      return true;
    }
  }
  
  class BaseMapPackage implements DataPackage<BaseMap> {
    private BaseMapPackage() {}
    
    public AudioMetadata.BaseMap unpack(ByteBuffer param1ByteBuffer) {
      AudioMetadata.BaseMap baseMap = new AudioMetadata.BaseMap();
      int i = param1ByteBuffer.getInt();
      AudioMetadata.DataPackage<String> dataPackage = (AudioMetadata.DataPackage)AudioMetadata.DATA_PACKAGES.get(Integer.valueOf(5));
      if (dataPackage == null) {
        Log.e("AudioMetadata", "Cannot find DataPackage for String");
        return null;
      } 
      for (byte b = 0; b < i; b++) {
        Integer integer;
        String str = dataPackage.unpack(param1ByteBuffer);
        if (str == null) {
          Log.e("AudioMetadata", "Failed to unpack key for map");
          return null;
        } 
        Pair<Class, Object> pair = AudioMetadata.OBJECT_PACKAGE.unpack(param1ByteBuffer);
        if (pair == null) {
          Log.e("AudioMetadata", "Failed to unpack value for map");
          return null;
        } 
        if (str.equals(AudioMetadata.Format.KEY_HAS_ATMOS.getName())) {
          Object<Boolean> object = (Object<Boolean>)pair.first;
          AudioMetadata.Key<Integer> key = AudioMetadata.Format.KEY_HAS_ATMOS;
          if (object == key.getValueClass()) {
            boolean bool;
            object = (Object<Boolean>)AudioMetadata.Format.KEY_ATMOS_PRESENT;
            integer = (Integer)pair.second;
            if (integer.intValue() != 0) {
              bool = true;
            } else {
              bool = false;
            } 
            baseMap.set((AudioMetadata.Key<Boolean>)object, Boolean.valueOf(bool));
            continue;
          } 
        } 
        baseMap.set(AudioMetadata.createKey(str, (Class)((Pair)integer).first), ((Class)((Pair)integer).first).cast(((Pair)integer).second));
        continue;
      } 
      return baseMap;
    }
    
    public boolean pack(AudioMetadata.AutoGrowByteBuffer param1AutoGrowByteBuffer, AudioMetadata.BaseMap param1BaseMap) {
      param1AutoGrowByteBuffer.putInt(param1BaseMap.size());
      AudioMetadata.DataPackage<String> dataPackage = (AudioMetadata.DataPackage)AudioMetadata.DATA_PACKAGES.get(Integer.valueOf(5));
      if (dataPackage == null) {
        Log.e("AudioMetadata", "Cannot find DataPackage for String");
        return false;
      } 
      for (AudioMetadata.Key<Object> key1 : param1BaseMap.keySet()) {
        StringBuilder stringBuilder;
        AudioMetadata.Key<Integer> key;
        Integer integer1 = (Integer)param1BaseMap.get(key1);
        AudioMetadata.Key<Object> key2 = key1;
        Integer integer2 = integer1;
        if (key1 == AudioMetadata.Format.KEY_ATMOS_PRESENT) {
          key = AudioMetadata.Format.KEY_HAS_ATMOS;
          integer2 = Integer.valueOf(((Boolean)integer1).booleanValue());
        } 
        if (!dataPackage.pack(param1AutoGrowByteBuffer, key.getName())) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to pack key: ");
          stringBuilder.append(key.getName());
          Log.i("AudioMetadata", stringBuilder.toString());
          return false;
        } 
        if (!AudioMetadata.OBJECT_PACKAGE.pack((AudioMetadata.AutoGrowByteBuffer)stringBuilder, new Pair(key.getValueClass(), integer2))) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Failed to pack value: ");
          stringBuilder.append(param1BaseMap.get(key));
          Log.i("AudioMetadata", stringBuilder.toString());
          return false;
        } 
      } 
      return true;
    }
  }
  
  public static BaseMap fromByteBuffer(ByteBuffer paramByteBuffer) {
    DataPackage<BaseMap> dataPackage = (DataPackage)DATA_PACKAGES.get(Integer.valueOf(6));
    if (dataPackage == null) {
      Log.e("AudioMetadata", "Cannot find DataPackage for BaseMap");
      return null;
    } 
    try {
      return dataPackage.unpack(paramByteBuffer);
    } catch (BufferUnderflowException bufferUnderflowException) {
      Log.e("AudioMetadata", "No enough data to unpack");
      return null;
    } 
  }
  
  public static ByteBuffer toByteBuffer(BaseMap paramBaseMap, ByteOrder paramByteOrder) {
    DataPackage<BaseMap> dataPackage = (DataPackage)DATA_PACKAGES.get(Integer.valueOf(6));
    if (dataPackage == null) {
      Log.e("AudioMetadata", "Cannot find DataPackage for BaseMap");
      return null;
    } 
    AutoGrowByteBuffer autoGrowByteBuffer = new AutoGrowByteBuffer();
    autoGrowByteBuffer.order(paramByteOrder);
    if (dataPackage.pack(autoGrowByteBuffer, paramBaseMap))
      return autoGrowByteBuffer.getRawByteBuffer(); 
    return null;
  }
  
  public static interface Key<T> {
    String getName();
    
    Class<T> getValueClass();
  }
}
