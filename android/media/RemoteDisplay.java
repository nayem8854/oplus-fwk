package android.media;

import android.os.Handler;
import android.view.Surface;
import dalvik.system.CloseGuard;

public final class RemoteDisplay {
  public static final int DISPLAY_ERROR_CONNECTION_DROPPED = 2;
  
  public static final int DISPLAY_ERROR_UNKOWN = 1;
  
  public static final int DISPLAY_FLAG_SECURE = 1;
  
  private final CloseGuard mGuard = CloseGuard.get();
  
  private final Handler mHandler;
  
  private final Listener mListener;
  
  private final String mOpPackageName;
  
  private long mPtr;
  
  private RemoteDisplay(Listener paramListener, Handler paramHandler, String paramString) {
    this.mListener = paramListener;
    this.mHandler = paramHandler;
    this.mOpPackageName = paramString;
  }
  
  protected void finalize() throws Throwable {
    try {
      dispose(true);
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public static RemoteDisplay listen(String paramString1, Listener paramListener, Handler paramHandler, String paramString2) {
    if (paramString1 != null) {
      if (paramListener != null) {
        if (paramHandler != null) {
          RemoteDisplay remoteDisplay = new RemoteDisplay(paramListener, paramHandler, paramString2);
          remoteDisplay.startListening(paramString1);
          return remoteDisplay;
        } 
        throw new IllegalArgumentException("handler must not be null");
      } 
      throw new IllegalArgumentException("listener must not be null");
    } 
    throw new IllegalArgumentException("iface must not be null");
  }
  
  public void dispose() {
    dispose(false);
  }
  
  public void pause() {
    nativePause(this.mPtr);
  }
  
  public void resume() {
    nativeResume(this.mPtr);
  }
  
  private void dispose(boolean paramBoolean) {
    if (this.mPtr != 0L) {
      CloseGuard closeGuard = this.mGuard;
      if (closeGuard != null)
        if (paramBoolean) {
          closeGuard.warnIfOpen();
        } else {
          closeGuard.close();
        }  
      nativeDispose(this.mPtr);
      this.mPtr = 0L;
    } 
  }
  
  private void startListening(String paramString) {
    long l = nativeListen(paramString, this.mOpPackageName);
    if (l != 0L) {
      this.mGuard.open("dispose");
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Could not start listening for remote display connection on \"");
    stringBuilder.append(paramString);
    stringBuilder.append("\"");
    throw new IllegalStateException(stringBuilder.toString());
  }
  
  private void notifyDisplayConnected(final Surface surface, final int width, final int height, final int flags, final int session) {
    this.mHandler.post(new Runnable() {
          final RemoteDisplay this$0;
          
          final int val$flags;
          
          final int val$height;
          
          final int val$session;
          
          final Surface val$surface;
          
          final int val$width;
          
          public void run() {
            RemoteDisplay.this.mListener.onDisplayConnected(surface, width, height, flags, session);
          }
        });
  }
  
  private void notifyDisplayDisconnected() {
    this.mHandler.post(new Runnable() {
          final RemoteDisplay this$0;
          
          public void run() {
            RemoteDisplay.this.mListener.onDisplayDisconnected();
          }
        });
  }
  
  private void notifyDisplayError(final int error) {
    this.mHandler.post(new Runnable() {
          final RemoteDisplay this$0;
          
          final int val$error;
          
          public void run() {
            RemoteDisplay.this.mListener.onDisplayError(error);
          }
        });
  }
  
  private native void nativeDispose(long paramLong);
  
  private native long nativeListen(String paramString1, String paramString2);
  
  private native void nativePause(long paramLong);
  
  private native void nativeResume(long paramLong);
  
  public static interface Listener {
    void onDisplayConnected(Surface param1Surface, int param1Int1, int param1Int2, int param1Int3, int param1Int4);
    
    void onDisplayDisconnected();
    
    void onDisplayError(int param1Int);
  }
}
