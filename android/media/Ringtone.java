package android.media;

import android.content.Context;
import android.net.Uri;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import java.util.ArrayList;

public class Ringtone {
  private static final boolean LOGD = true;
  
  private static final String[] MEDIA_COLUMNS = new String[] { "_id", "title" };
  
  private static final String MEDIA_SELECTION = "mime_type LIKE 'audio/%' OR mime_type IN ('application/ogg', 'application/x-flac')";
  
  private static final String TAG = "Ringtone";
  
  private static final ArrayList<Ringtone> sActiveRingtones = new ArrayList<>();
  
  private final boolean mAllowRemote;
  
  private AudioAttributes mAudioAttributes;
  
  private final AudioManager mAudioManager;
  
  private final MyOnCompletionListener mCompletionListener;
  
  private final Context mContext;
  
  private boolean mIsLooping;
  
  private MediaPlayer mLocalPlayer;
  
  private final MyOnErrorListener mOnErrorListener;
  
  private final Object mPlaybackSettingsLock;
  
  private final IRingtonePlayer mRemotePlayer;
  
  private final Binder mRemoteToken;
  
  private String mTitle;
  
  private Uri mUri;
  
  private float mVolume;
  
  private VolumeShaper mVolumeShaper;
  
  private VolumeShaper.Configuration mVolumeShaperConfig;
  
  public Ringtone(Context paramContext, boolean paramBoolean) {
    Binder binder;
    this.mOnErrorListener = new MyOnErrorListener();
    this.mCompletionListener = new MyOnCompletionListener();
    AudioAttributes.Builder builder2 = new AudioAttributes.Builder();
    builder2 = builder2.setUsage(6);
    builder2 = builder2.setContentType(4);
    this.mAudioAttributes = builder2.build();
    this.mIsLooping = false;
    this.mVolume = 1.0F;
    this.mPlaybackSettingsLock = new Object();
    this.mContext = paramContext;
    AudioManager audioManager = (AudioManager)paramContext.getSystemService("audio");
    this.mAllowRemote = paramBoolean;
    builder2 = null;
    if (paramBoolean) {
      IRingtonePlayer iRingtonePlayer = audioManager.getRingtonePlayer();
    } else {
      audioManager = null;
    } 
    this.mRemotePlayer = (IRingtonePlayer)audioManager;
    AudioAttributes.Builder builder1 = builder2;
    if (paramBoolean)
      binder = new Binder(); 
    this.mRemoteToken = binder;
  }
  
  @Deprecated
  public void setStreamType(int paramInt) {
    PlayerBase.deprecateStreamTypeForPlayback(paramInt, "Ringtone", "setStreamType()");
    AudioAttributes.Builder builder = new AudioAttributes.Builder();
    builder = builder.setInternalLegacyStreamType(paramInt);
    AudioAttributes audioAttributes = builder.build();
    setAudioAttributes(audioAttributes);
  }
  
  @Deprecated
  public int getStreamType() {
    return AudioAttributes.toLegacyStreamType(this.mAudioAttributes);
  }
  
  public void setAudioAttributes(AudioAttributes paramAudioAttributes) throws IllegalArgumentException {
    if (paramAudioAttributes != null) {
      this.mAudioAttributes = paramAudioAttributes;
      setUri(this.mUri, this.mVolumeShaperConfig);
      return;
    } 
    throw new IllegalArgumentException("Invalid null AudioAttributes for Ringtone");
  }
  
  public AudioAttributes getAudioAttributes() {
    return this.mAudioAttributes;
  }
  
  public void setLooping(boolean paramBoolean) {
    synchronized (this.mPlaybackSettingsLock) {
      this.mIsLooping = paramBoolean;
      applyPlaybackProperties_sync();
      return;
    } 
  }
  
  public boolean isLooping() {
    synchronized (this.mPlaybackSettingsLock) {
      return this.mIsLooping;
    } 
  }
  
  public void setVolume(float paramFloat) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mPlaybackSettingsLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: fload_1
    //   8: fstore_3
    //   9: fload_1
    //   10: fconst_0
    //   11: fcmpg
    //   12: ifge -> 17
    //   15: fconst_0
    //   16: fstore_3
    //   17: fload_3
    //   18: fstore_1
    //   19: fload_3
    //   20: fconst_1
    //   21: fcmpl
    //   22: ifle -> 27
    //   25: fconst_1
    //   26: fstore_1
    //   27: aload_0
    //   28: fload_1
    //   29: putfield mVolume : F
    //   32: aload_0
    //   33: invokespecial applyPlaybackProperties_sync : ()V
    //   36: aload_2
    //   37: monitorexit
    //   38: return
    //   39: astore #4
    //   41: aload_2
    //   42: monitorexit
    //   43: aload #4
    //   45: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #188	-> 0
    //   #189	-> 7
    //   #190	-> 17
    //   #191	-> 27
    //   #192	-> 32
    //   #193	-> 36
    //   #194	-> 38
    //   #193	-> 39
    // Exception table:
    //   from	to	target	type
    //   27	32	39	finally
    //   32	36	39	finally
    //   36	38	39	finally
    //   41	43	39	finally
  }
  
  public float getVolume() {
    synchronized (this.mPlaybackSettingsLock) {
      return this.mVolume;
    } 
  }
  
  private void applyPlaybackProperties_sync() {
    MediaPlayer mediaPlayer = this.mLocalPlayer;
    if (mediaPlayer != null) {
      mediaPlayer.setVolume(this.mVolume);
      this.mLocalPlayer.setLooping(this.mIsLooping);
    } else {
      if (this.mAllowRemote) {
        IRingtonePlayer iRingtonePlayer = this.mRemotePlayer;
        if (iRingtonePlayer != null) {
          try {
            iRingtonePlayer.setPlaybackProperties(this.mRemoteToken, this.mVolume, this.mIsLooping);
          } catch (RemoteException remoteException) {
            Log.w("Ringtone", "Problem setting playback properties: ", (Throwable)remoteException);
          } 
          return;
        } 
      } 
      Log.w("Ringtone", "Neither local nor remote player available when applying playback properties");
    } 
  }
  
  public String getTitle(Context paramContext) {
    String str2 = this.mTitle;
    if (str2 != null)
      return str2; 
    String str1 = getTitle(paramContext, this.mUri, true, this.mAllowRemote);
    return str1;
  }
  
  public static String getTitle(Context paramContext, Uri paramUri, boolean paramBoolean1, boolean paramBoolean2) {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   4: astore #4
    //   6: aconst_null
    //   7: astore #5
    //   9: aconst_null
    //   10: astore #6
    //   12: aconst_null
    //   13: astore #7
    //   15: aconst_null
    //   16: astore #8
    //   18: aload_1
    //   19: ifnull -> 376
    //   22: aload_1
    //   23: invokevirtual getAuthority : ()Ljava/lang/String;
    //   26: invokestatic getAuthorityWithoutUserId : (Ljava/lang/String;)Ljava/lang/String;
    //   29: astore #9
    //   31: ldc 'settings'
    //   33: aload #9
    //   35: invokevirtual equals : (Ljava/lang/Object;)Z
    //   38: ifeq -> 85
    //   41: iload_2
    //   42: ifeq -> 373
    //   45: aload_1
    //   46: invokestatic getDefaultType : (Landroid/net/Uri;)I
    //   49: istore #10
    //   51: aload_0
    //   52: iload #10
    //   54: invokestatic getActualDefaultRingtoneUri : (Landroid/content/Context;I)Landroid/net/Uri;
    //   57: astore_1
    //   58: aload_0
    //   59: aload_1
    //   60: iconst_0
    //   61: iload_3
    //   62: invokestatic getTitle : (Landroid/content/Context;Landroid/net/Uri;ZZ)Ljava/lang/String;
    //   65: astore_1
    //   66: aload_0
    //   67: ldc 17041202
    //   69: iconst_1
    //   70: anewarray java/lang/Object
    //   73: dup
    //   74: iconst_0
    //   75: aload_1
    //   76: aastore
    //   77: invokevirtual getString : (I[Ljava/lang/Object;)Ljava/lang/String;
    //   80: astore #7
    //   82: goto -> 373
    //   85: aconst_null
    //   86: astore #11
    //   88: aconst_null
    //   89: astore #12
    //   91: aconst_null
    //   92: astore #13
    //   94: aload #11
    //   96: astore #14
    //   98: aload #12
    //   100: astore #7
    //   102: ldc 'media'
    //   104: aload #9
    //   106: invokevirtual equals : (Ljava/lang/Object;)Z
    //   109: ifeq -> 230
    //   112: iload_3
    //   113: ifeq -> 122
    //   116: aconst_null
    //   117: astore #13
    //   119: goto -> 126
    //   122: ldc 'mime_type LIKE 'audio/%' OR mime_type IN ('application/ogg', 'application/x-flac')'
    //   124: astore #13
    //   126: aload #11
    //   128: astore #14
    //   130: aload #12
    //   132: astore #7
    //   134: aload #4
    //   136: aload_1
    //   137: getstatic android/media/Ringtone.MEDIA_COLUMNS : [Ljava/lang/String;
    //   140: aload #13
    //   142: aconst_null
    //   143: aconst_null
    //   144: invokevirtual query : (Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    //   147: astore #12
    //   149: aload #12
    //   151: astore #13
    //   153: aload #12
    //   155: ifnull -> 230
    //   158: aload #12
    //   160: astore #13
    //   162: aload #12
    //   164: astore #14
    //   166: aload #12
    //   168: astore #7
    //   170: aload #12
    //   172: invokeinterface getCount : ()I
    //   177: iconst_1
    //   178: if_icmpne -> 230
    //   181: aload #12
    //   183: astore #14
    //   185: aload #12
    //   187: astore #7
    //   189: aload #12
    //   191: invokeinterface moveToFirst : ()Z
    //   196: pop
    //   197: aload #12
    //   199: astore #14
    //   201: aload #12
    //   203: astore #7
    //   205: aload #12
    //   207: iconst_1
    //   208: invokeinterface getString : (I)Ljava/lang/String;
    //   213: astore #13
    //   215: aload #12
    //   217: ifnull -> 227
    //   220: aload #12
    //   222: invokeinterface close : ()V
    //   227: aload #13
    //   229: areturn
    //   230: aload #5
    //   232: astore #14
    //   234: aload #13
    //   236: ifnull -> 254
    //   239: aload #13
    //   241: astore #7
    //   243: aload #8
    //   245: astore #14
    //   247: aload #7
    //   249: invokeinterface close : ()V
    //   254: goto -> 358
    //   257: astore_0
    //   258: goto -> 322
    //   261: astore #14
    //   263: aconst_null
    //   264: astore #12
    //   266: iload_3
    //   267: ifeq -> 296
    //   270: aload #7
    //   272: astore #14
    //   274: aload_0
    //   275: ldc 'audio'
    //   277: invokevirtual getSystemService : (Ljava/lang/String;)Ljava/lang/Object;
    //   280: checkcast android/media/AudioManager
    //   283: astore #13
    //   285: aload #7
    //   287: astore #14
    //   289: aload #13
    //   291: invokevirtual getRingtonePlayer : ()Landroid/media/IRingtonePlayer;
    //   294: astore #12
    //   296: aload #6
    //   298: astore #13
    //   300: aload #12
    //   302: ifnull -> 342
    //   305: aload #7
    //   307: astore #14
    //   309: aload #12
    //   311: aload_1
    //   312: invokeinterface getTitle : (Landroid/net/Uri;)Ljava/lang/String;
    //   317: astore #13
    //   319: goto -> 342
    //   322: aload #14
    //   324: ifnull -> 334
    //   327: aload #14
    //   329: invokeinterface close : ()V
    //   334: aload_0
    //   335: athrow
    //   336: astore #14
    //   338: aload #6
    //   340: astore #13
    //   342: aload #13
    //   344: astore #14
    //   346: aload #7
    //   348: ifnull -> 254
    //   351: aload #13
    //   353: astore #14
    //   355: goto -> 247
    //   358: aload #14
    //   360: astore #7
    //   362: aload #14
    //   364: ifnonnull -> 373
    //   367: aload_1
    //   368: invokevirtual getLastPathSegment : ()Ljava/lang/String;
    //   371: astore #7
    //   373: goto -> 385
    //   376: aload_0
    //   377: ldc_w 17041206
    //   380: invokevirtual getString : (I)Ljava/lang/String;
    //   383: astore #7
    //   385: aload #7
    //   387: astore_1
    //   388: aload #7
    //   390: ifnonnull -> 411
    //   393: aload_0
    //   394: ldc_w 17041207
    //   397: invokevirtual getString : (I)Ljava/lang/String;
    //   400: astore_0
    //   401: aload_0
    //   402: astore_1
    //   403: aload_0
    //   404: ifnonnull -> 411
    //   407: ldc_w ''
    //   410: astore_1
    //   411: aload_1
    //   412: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #241	-> 0
    //   #243	-> 6
    //   #245	-> 18
    //   #246	-> 22
    //   #248	-> 31
    //   #249	-> 41
    //   #250	-> 45
    //   #251	-> 45
    //   #250	-> 51
    //   #252	-> 58
    //   #254	-> 66
    //   #255	-> 66
    //   #257	-> 82
    //   #259	-> 85
    //   #261	-> 94
    //   #262	-> 112
    //   #263	-> 126
    //   #264	-> 149
    //   #265	-> 181
    //   #266	-> 197
    //   #284	-> 215
    //   #285	-> 220
    //   #287	-> 227
    //   #266	-> 227
    //   #284	-> 230
    //   #285	-> 247
    //   #287	-> 254
    //   #288	-> 254
    //   #284	-> 257
    //   #270	-> 261
    //   #271	-> 263
    //   #272	-> 266
    //   #273	-> 270
    //   #274	-> 270
    //   #275	-> 285
    //   #277	-> 296
    //   #279	-> 305
    //   #281	-> 319
    //   #284	-> 322
    //   #285	-> 327
    //   #287	-> 334
    //   #288	-> 334
    //   #280	-> 336
    //   #284	-> 342
    //   #285	-> 351
    //   #289	-> 358
    //   #290	-> 367
    //   #293	-> 373
    //   #294	-> 376
    //   #297	-> 385
    //   #298	-> 393
    //   #299	-> 401
    //   #300	-> 407
    //   #304	-> 411
    // Exception table:
    //   from	to	target	type
    //   102	112	261	java/lang/SecurityException
    //   102	112	257	finally
    //   134	149	261	java/lang/SecurityException
    //   134	149	257	finally
    //   170	181	261	java/lang/SecurityException
    //   170	181	257	finally
    //   189	197	261	java/lang/SecurityException
    //   189	197	257	finally
    //   205	215	261	java/lang/SecurityException
    //   205	215	257	finally
    //   274	285	257	finally
    //   289	296	257	finally
    //   309	319	336	android/os/RemoteException
    //   309	319	257	finally
  }
  
  public void setUri(Uri paramUri) {
    setUri(paramUri, null);
  }
  
  public void setUri(Uri paramUri, VolumeShaper.Configuration paramConfiguration) {
    this.mVolumeShaperConfig = paramConfiguration;
    destroyLocalPlayer();
    this.mUri = paramUri;
    if (paramUri == null)
      return; 
    MediaPlayer mediaPlayer = new MediaPlayer();
    try {
      mediaPlayer.setDataSource(this.mContext, this.mUri);
      this.mLocalPlayer.setAudioAttributes(this.mAudioAttributes);
      synchronized (this.mPlaybackSettingsLock) {
        applyPlaybackProperties_sync();
        if (this.mVolumeShaperConfig != null)
          this.mVolumeShaper = this.mLocalPlayer.createVolumeShaper(this.mVolumeShaperConfig); 
        this.mLocalPlayer.prepare();
      } 
    } catch (SecurityException|java.io.IOException securityException) {
      destroyLocalPlayer();
      if (!this.mAllowRemote) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Remote playback not allowed: ");
        stringBuilder.append(securityException);
        Log.w("Ringtone", stringBuilder.toString());
      } 
    } 
    if (this.mLocalPlayer != null) {
      Log.d("Ringtone", "Successfully created local player");
    } else {
      Log.d("Ringtone", "Problem opening; delegating to remote player");
    } 
  }
  
  public Uri getUri() {
    return this.mUri;
  }
  
  public void play() {
    if (this.mLocalPlayer != null) {
      AudioManager audioManager = this.mAudioManager;
      AudioAttributes audioAttributes = this.mAudioAttributes;
      int i = AudioAttributes.toLegacyStreamType(audioAttributes);
      if (audioManager.getStreamVolume(i) != 0)
        startLocalPlayer(); 
    } else if (this.mAllowRemote && this.mRemotePlayer != null) {
      null = this.mUri.getCanonicalUri();
      synchronized (this.mPlaybackSettingsLock) {
        boolean bool = this.mIsLooping;
        float f = this.mVolume;
        try {
          this.mRemotePlayer.playWithVolumeShaping(this.mRemoteToken, null, this.mAudioAttributes, f, bool, this.mVolumeShaperConfig);
        } catch (RemoteException remoteException) {
          if (!playFallbackRingtone()) {
            null = new StringBuilder();
            null.append("Problem playing ringtone: ");
            null.append(remoteException);
            Log.w("Ringtone", null.toString());
          } 
        } 
      } 
    } else if (!playFallbackRingtone()) {
      Log.w("Ringtone", "Neither local nor remote playback available");
    } 
  }
  
  public void stop() {
    if (this.mLocalPlayer != null) {
      destroyLocalPlayer();
    } else if (this.mAllowRemote) {
      IRingtonePlayer iRingtonePlayer = this.mRemotePlayer;
      if (iRingtonePlayer != null)
        try {
          iRingtonePlayer.stop(this.mRemoteToken);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Problem stopping ringtone: ");
          stringBuilder.append(remoteException);
          Log.w("Ringtone", stringBuilder.toString());
        }  
    } 
  }
  
  private void destroyLocalPlayer() {
    MediaPlayer mediaPlayer = this.mLocalPlayer;
    if (mediaPlayer != null) {
      mediaPlayer.setOnCompletionListener(null);
      this.mLocalPlayer.reset();
      this.mLocalPlayer.release();
      this.mLocalPlayer = null;
      this.mVolumeShaper = null;
      synchronized (sActiveRingtones) {
        sActiveRingtones.remove(this);
      } 
    } 
  }
  
  private void startLocalPlayer() {
    ArrayList<Ringtone> arrayList;
    VolumeShaper volumeShaper;
    if (this.mLocalPlayer == null)
      return; 
    synchronized (sActiveRingtones) {
      sActiveRingtones.add(this);
      this.mLocalPlayer.setOnCompletionListener(this.mCompletionListener);
      this.mLocalPlayer.start();
      volumeShaper = this.mVolumeShaper;
      if (volumeShaper != null)
        volumeShaper.apply(VolumeShaper.Operation.PLAY); 
      return;
    } 
  }
  
  public boolean isPlaying() {
    MediaPlayer mediaPlayer = this.mLocalPlayer;
    if (mediaPlayer != null)
      return mediaPlayer.isPlaying(); 
    if (this.mAllowRemote) {
      IRingtonePlayer iRingtonePlayer = this.mRemotePlayer;
      if (iRingtonePlayer != null)
        try {
          return iRingtonePlayer.isPlaying(this.mRemoteToken);
        } catch (RemoteException remoteException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Problem checking ringtone: ");
          stringBuilder.append(remoteException);
          Log.w("Ringtone", stringBuilder.toString());
          return false;
        }  
    } 
    Log.w("Ringtone", "Neither local nor remote playback available");
    return false;
  }
  
  private boolean playFallbackRingtone() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mAudioManager : Landroid/media/AudioManager;
    //   4: aload_0
    //   5: getfield mAudioAttributes : Landroid/media/AudioAttributes;
    //   8: invokestatic toLegacyStreamType : (Landroid/media/AudioAttributes;)I
    //   11: invokevirtual getStreamVolume : (I)I
    //   14: ifeq -> 278
    //   17: aload_0
    //   18: getfield mUri : Landroid/net/Uri;
    //   21: invokestatic getDefaultType : (Landroid/net/Uri;)I
    //   24: istore_1
    //   25: iload_1
    //   26: iconst_m1
    //   27: if_icmpeq -> 84
    //   30: aload_0
    //   31: getfield mContext : Landroid/content/Context;
    //   34: astore_2
    //   35: aload_2
    //   36: iload_1
    //   37: invokestatic getActualDefaultRingtoneUri : (Landroid/content/Context;I)Landroid/net/Uri;
    //   40: ifnull -> 46
    //   43: goto -> 84
    //   46: new java/lang/StringBuilder
    //   49: dup
    //   50: invokespecial <init> : ()V
    //   53: astore_2
    //   54: aload_2
    //   55: ldc_w 'not playing fallback for '
    //   58: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload_2
    //   63: aload_0
    //   64: getfield mUri : Landroid/net/Uri;
    //   67: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   70: pop
    //   71: ldc 'Ringtone'
    //   73: aload_2
    //   74: invokevirtual toString : ()Ljava/lang/String;
    //   77: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   80: pop
    //   81: goto -> 278
    //   84: aload_0
    //   85: getfield mContext : Landroid/content/Context;
    //   88: invokevirtual getResources : ()Landroid/content/res/Resources;
    //   91: ldc_w 17825797
    //   94: invokevirtual openRawResourceFd : (I)Landroid/content/res/AssetFileDescriptor;
    //   97: astore_2
    //   98: aload_2
    //   99: ifnull -> 239
    //   102: new android/media/MediaPlayer
    //   105: astore_3
    //   106: aload_3
    //   107: invokespecial <init> : ()V
    //   110: aload_0
    //   111: aload_3
    //   112: putfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   115: aload_2
    //   116: invokevirtual getDeclaredLength : ()J
    //   119: lconst_0
    //   120: lcmp
    //   121: ifge -> 138
    //   124: aload_0
    //   125: getfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   128: aload_2
    //   129: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   132: invokevirtual setDataSource : (Ljava/io/FileDescriptor;)V
    //   135: goto -> 171
    //   138: aload_0
    //   139: getfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   142: astore_3
    //   143: aload_2
    //   144: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   147: astore #4
    //   149: aload_2
    //   150: invokevirtual getStartOffset : ()J
    //   153: lstore #5
    //   155: aload_2
    //   156: invokevirtual getDeclaredLength : ()J
    //   159: lstore #7
    //   161: aload_3
    //   162: aload #4
    //   164: lload #5
    //   166: lload #7
    //   168: invokevirtual setDataSource : (Ljava/io/FileDescriptor;JJ)V
    //   171: aload_0
    //   172: getfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   175: aload_0
    //   176: getfield mAudioAttributes : Landroid/media/AudioAttributes;
    //   179: invokevirtual setAudioAttributes : (Landroid/media/AudioAttributes;)V
    //   182: aload_0
    //   183: getfield mPlaybackSettingsLock : Ljava/lang/Object;
    //   186: astore_3
    //   187: aload_3
    //   188: monitorenter
    //   189: aload_0
    //   190: invokespecial applyPlaybackProperties_sync : ()V
    //   193: aload_3
    //   194: monitorexit
    //   195: aload_0
    //   196: getfield mVolumeShaperConfig : Landroid/media/VolumeShaper$Configuration;
    //   199: ifnull -> 217
    //   202: aload_0
    //   203: aload_0
    //   204: getfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   207: aload_0
    //   208: getfield mVolumeShaperConfig : Landroid/media/VolumeShaper$Configuration;
    //   211: invokevirtual createVolumeShaper : (Landroid/media/VolumeShaper$Configuration;)Landroid/media/VolumeShaper;
    //   214: putfield mVolumeShaper : Landroid/media/VolumeShaper;
    //   217: aload_0
    //   218: getfield mLocalPlayer : Landroid/media/MediaPlayer;
    //   221: invokevirtual prepare : ()V
    //   224: aload_0
    //   225: invokespecial startLocalPlayer : ()V
    //   228: aload_2
    //   229: invokevirtual close : ()V
    //   232: iconst_1
    //   233: ireturn
    //   234: astore_2
    //   235: aload_3
    //   236: monitorexit
    //   237: aload_2
    //   238: athrow
    //   239: ldc 'Ringtone'
    //   241: ldc_w 'Could not load fallback ringtone'
    //   244: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   247: pop
    //   248: goto -> 278
    //   251: astore_2
    //   252: ldc 'Ringtone'
    //   254: ldc_w 'Fallback ringtone does not exist'
    //   257: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   260: pop
    //   261: goto -> 278
    //   264: astore_2
    //   265: aload_0
    //   266: invokespecial destroyLocalPlayer : ()V
    //   269: ldc 'Ringtone'
    //   271: ldc_w 'Failed to open fallback ringtone'
    //   274: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   277: pop
    //   278: iconst_0
    //   279: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #470	-> 0
    //   #472	-> 17
    //   #473	-> 25
    //   #474	-> 35
    //   #509	-> 46
    //   #477	-> 84
    //   #479	-> 98
    //   #480	-> 102
    //   #481	-> 115
    //   #482	-> 124
    //   #484	-> 138
    //   #485	-> 149
    //   #486	-> 155
    //   #484	-> 161
    //   #488	-> 171
    //   #489	-> 182
    //   #490	-> 189
    //   #491	-> 193
    //   #492	-> 195
    //   #493	-> 202
    //   #495	-> 217
    //   #496	-> 224
    //   #497	-> 228
    //   #498	-> 232
    //   #491	-> 234
    //   #500	-> 239
    //   #507	-> 248
    //   #505	-> 251
    //   #506	-> 252
    //   #502	-> 264
    //   #503	-> 265
    //   #504	-> 269
    //   #512	-> 278
    // Exception table:
    //   from	to	target	type
    //   84	98	264	java/io/IOException
    //   84	98	251	android/content/res/Resources$NotFoundException
    //   102	115	264	java/io/IOException
    //   102	115	251	android/content/res/Resources$NotFoundException
    //   115	124	264	java/io/IOException
    //   115	124	251	android/content/res/Resources$NotFoundException
    //   124	135	264	java/io/IOException
    //   124	135	251	android/content/res/Resources$NotFoundException
    //   138	149	264	java/io/IOException
    //   138	149	251	android/content/res/Resources$NotFoundException
    //   149	155	264	java/io/IOException
    //   149	155	251	android/content/res/Resources$NotFoundException
    //   155	161	264	java/io/IOException
    //   155	161	251	android/content/res/Resources$NotFoundException
    //   161	171	264	java/io/IOException
    //   161	171	251	android/content/res/Resources$NotFoundException
    //   171	182	264	java/io/IOException
    //   171	182	251	android/content/res/Resources$NotFoundException
    //   182	189	264	java/io/IOException
    //   182	189	251	android/content/res/Resources$NotFoundException
    //   189	193	234	finally
    //   193	195	234	finally
    //   195	202	264	java/io/IOException
    //   195	202	251	android/content/res/Resources$NotFoundException
    //   202	217	264	java/io/IOException
    //   202	217	251	android/content/res/Resources$NotFoundException
    //   217	224	264	java/io/IOException
    //   217	224	251	android/content/res/Resources$NotFoundException
    //   224	228	264	java/io/IOException
    //   224	228	251	android/content/res/Resources$NotFoundException
    //   228	232	264	java/io/IOException
    //   228	232	251	android/content/res/Resources$NotFoundException
    //   235	237	234	finally
    //   237	239	264	java/io/IOException
    //   237	239	251	android/content/res/Resources$NotFoundException
    //   239	248	264	java/io/IOException
    //   239	248	251	android/content/res/Resources$NotFoundException
  }
  
  void setTitle(String paramString) {
    this.mTitle = paramString;
  }
  
  protected void finalize() {
    MediaPlayer mediaPlayer = this.mLocalPlayer;
    if (mediaPlayer != null)
      mediaPlayer.release(); 
  }
  
  class MyOnCompletionListener implements MediaPlayer.OnCompletionListener {
    final Ringtone this$0;
    
    public void onCompletion(MediaPlayer param1MediaPlayer) {
      synchronized (Ringtone.sActiveRingtones) {
        Ringtone.sActiveRingtones.remove(Ringtone.this);
        param1MediaPlayer.setOnCompletionListener(null);
        param1MediaPlayer.setOnErrorListener(null);
        return;
      } 
    }
  }
  
  class MyOnErrorListener implements MediaPlayer.OnErrorListener {
    final Ringtone this$0;
    
    public boolean onError(MediaPlayer param1MediaPlayer, int param1Int1, int param1Int2) {
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("mMediaPlayer OnError what:");
      stringBuilder2.append(param1Int1);
      stringBuilder2.append(" extra:");
      stringBuilder2.append(param1Int2);
      stringBuilder2.append(" mp:");
      stringBuilder2.append(param1MediaPlayer);
      Log.d("Ringtone", stringBuilder2.toString());
      param1MediaPlayer.setOnCompletionListener(null);
      param1MediaPlayer.setOnErrorListener(null);
      Ringtone.this.destroyLocalPlayer();
      param1Int1 = Binder.getCallingUid();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("onError calling uid:");
      stringBuilder1.append(param1Int1);
      stringBuilder1.append(" pid:");
      stringBuilder1.append(Binder.getCallingPid());
      Log.d("Ringtone", stringBuilder1.toString());
      if (param1Int1 >= 10000) {
        Log.w("Ringtone", "onError remote player disabled for 3rd apps");
        return true;
      } 
      if (Ringtone.this.mAllowRemote && Ringtone.this.mRemotePlayer != null) {
        null = Ringtone.this.mUri.getCanonicalUri();
        synchronized (Ringtone.this.mPlaybackSettingsLock) {
          boolean bool = Ringtone.this.mIsLooping;
          float f = Ringtone.this.mVolume;
          try {
            Ringtone.this.mRemotePlayer.play(Ringtone.this.mRemoteToken, null, Ringtone.this.mAudioAttributes, f, bool);
          } catch (RemoteException remoteException) {
            if (!Ringtone.this.playFallbackRingtone()) {
              null = new StringBuilder();
              null.append("Problem playing ringtone:");
              null.append(remoteException);
              Log.w("Ringtone", null.toString());
            } 
          } 
        } 
      } 
      return true;
    }
  }
}
