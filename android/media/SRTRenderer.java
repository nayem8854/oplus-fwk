package android.media;

import android.content.Context;
import android.os.Handler;

public class SRTRenderer extends SubtitleController.Renderer {
  private final Context mContext;
  
  private final Handler mEventHandler;
  
  private final boolean mRender;
  
  private WebVttRenderingWidget mRenderingWidget;
  
  public SRTRenderer(Context paramContext) {
    this(paramContext, null);
  }
  
  SRTRenderer(Context paramContext, Handler paramHandler) {
    boolean bool;
    this.mContext = paramContext;
    if (paramHandler == null) {
      bool = true;
    } else {
      bool = false;
    } 
    this.mRender = bool;
    this.mEventHandler = paramHandler;
  }
  
  public boolean supports(MediaFormat paramMediaFormat) {
    boolean bool = paramMediaFormat.containsKey("mime");
    boolean bool1 = false;
    if (bool) {
      String str = paramMediaFormat.getString("mime");
      if (!str.equals("application/x-subrip"))
        return false; 
      boolean bool2 = this.mRender;
      if (paramMediaFormat.getInteger("is-timed-text", 0) == 0) {
        bool = true;
      } else {
        bool = false;
      } 
      if (bool2 == bool)
        bool1 = true; 
      return bool1;
    } 
    return false;
  }
  
  public SubtitleTrack createTrack(MediaFormat paramMediaFormat) {
    if (this.mRender && this.mRenderingWidget == null)
      this.mRenderingWidget = new WebVttRenderingWidget(this.mContext); 
    if (this.mRender)
      return new SRTTrack(this.mRenderingWidget, paramMediaFormat); 
    return new SRTTrack(this.mEventHandler, paramMediaFormat);
  }
}
