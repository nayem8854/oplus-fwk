package android.media;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.hardware.Camera;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import android.view.Surface;
import com.android.internal.util.Preconditions;
import com.oplus.orms.OplusResourceManager;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

public class MediaRecorder implements AudioRouting, AudioRecordingMonitor, AudioRecordingMonitorClient, MicrophoneDirection {
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private static OplusResourceManager mOrmsManager = null;
  
  public MediaRecorder() {
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler = new EventHandler(this, looper);
      } else {
        this.mEventHandler = null;
      } 
    } 
    this.mChannelCount = 1;
    String str1 = ActivityThread.currentPackageName();
    WeakReference<MediaRecorder> weakReference = new WeakReference<>(this);
    String str2 = ActivityThread.currentOpPackageName();
    native_setup(weakReference, str1, str2);
  }
  
  public void setInputSurface(Surface paramSurface) {
    if (paramSurface instanceof MediaCodec.PersistentSurface) {
      native_setInputSurface(paramSurface);
      return;
    } 
    throw new IllegalArgumentException("not a PersistentSurface");
  }
  
  public void setPreviewDisplay(Surface paramSurface) {
    this.mSurface = paramSurface;
  }
  
  class AudioSource {
    public static final int AUDIO_SOURCE_INVALID = -1;
    
    public static final int CAMCORDER = 5;
    
    public static final int DEFAULT = 0;
    
    @SystemApi
    public static final int ECHO_REFERENCE = 1997;
    
    @SystemApi
    public static final int HOTWORD = 1999;
    
    public static final int MIC = 1;
    
    @SystemApi
    public static final int RADIO_TUNER = 1998;
    
    public static final int REMOTE_SUBMIX = 8;
    
    public static final int UNPROCESSED = 9;
    
    public static final int VOICE_CALL = 4;
    
    public static final int VOICE_COMMUNICATION = 7;
    
    public static final int VOICE_DOWNLINK = 3;
    
    public static final int VOICE_PERFORMANCE = 10;
    
    public static final int VOICE_RECOGNITION = 6;
    
    public static final int VOICE_UPLINK = 2;
    
    final MediaRecorder this$0;
  }
  
  public static boolean isSystemOnlyAudioSource(int paramInt) {
    switch (paramInt) {
      default:
        return true;
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 9:
      case 10:
        break;
    } 
    return false;
  }
  
  public static final String toLogFriendlyAudioSource(int paramInt) {
    StringBuilder stringBuilder;
    switch (paramInt) {
      default:
        switch (paramInt) {
          default:
            stringBuilder = new StringBuilder();
            stringBuilder.append("unknown source ");
            stringBuilder.append(paramInt);
            return stringBuilder.toString();
          case 1999:
            return "HOTWORD";
          case 1998:
            return "RADIO_TUNER";
          case 1997:
            break;
        } 
        return "ECHO_REFERENCE";
      case 10:
        return "VOICE_PERFORMANCE";
      case 9:
        return "UNPROCESSED";
      case 8:
        return "REMOTE_SUBMIX";
      case 7:
        return "VOICE_COMMUNICATION";
      case 6:
        return "VOICE_RECOGNITION";
      case 5:
        return "CAMCORDER";
      case 4:
        return "VOICE_CALL";
      case 3:
        return "VOICE_DOWNLINK";
      case 2:
        return "VOICE_UPLINK";
      case 1:
        return "MIC";
      case 0:
        return "DEFAULT";
      case -1:
        break;
    } 
    return "AUDIO_SOURCE_INVALID";
  }
  
  class VideoSource {
    public static final int CAMERA = 1;
    
    public static final int DEFAULT = 0;
    
    public static final int SURFACE = 2;
    
    final MediaRecorder this$0;
  }
  
  class OutputFormat {
    public static final int AAC_ADIF = 5;
    
    public static final int AAC_ADTS = 6;
    
    public static final int AMR_NB = 3;
    
    public static final int AMR_WB = 4;
    
    public static final int DEFAULT = 0;
    
    public static final int HEIF = 10;
    
    public static final int MPEG_2_TS = 8;
    
    public static final int MPEG_4 = 2;
    
    public static final int OGG = 11;
    
    public static final int OUTPUT_FORMAT_RTP_AVP = 7;
    
    public static final int QCP = 20;
    
    public static final int RAW_AMR = 3;
    
    public static final int THREE_GPP = 1;
    
    public static final int WAVE = 21;
    
    public static final int WEBM = 9;
    
    final MediaRecorder this$0;
  }
  
  class AudioEncoder {
    public static final int AAC = 3;
    
    public static final int AAC_ELD = 5;
    
    public static final int AMR_NB = 1;
    
    public static final int AMR_WB = 2;
    
    public static final int DEFAULT = 0;
    
    public static final int EVRC = 10;
    
    public static final int HE_AAC = 4;
    
    public static final int LPCM = 12;
    
    public static final int MPEGH = 13;
    
    public static final int OPUS = 7;
    
    public static final int QCELP = 11;
    
    public static final int VORBIS = 6;
    
    final MediaRecorder this$0;
  }
  
  class VideoEncoder {
    public static final int DEFAULT = 0;
    
    public static final int H263 = 1;
    
    public static final int H264 = 2;
    
    public static final int HEVC = 5;
    
    public static final int MPEG_4_SP = 3;
    
    public static final int VP8 = 4;
    
    final MediaRecorder this$0;
  }
  
  public static final int getAudioSourceMax() {
    return 10;
  }
  
  public void setProfile(CamcorderProfile paramCamcorderProfile) {
    setOutputFormat(paramCamcorderProfile.fileFormat);
    setVideoFrameRate(paramCamcorderProfile.videoFrameRate);
    setVideoSize(paramCamcorderProfile.videoFrameWidth, paramCamcorderProfile.videoFrameHeight);
    setVideoEncodingBitRate(paramCamcorderProfile.videoBitRate);
    setVideoEncoder(paramCamcorderProfile.videoCodec);
    if (paramCamcorderProfile.quality < 1000 || paramCamcorderProfile.quality > 1007)
      if (paramCamcorderProfile.audioCodec >= 0) {
        setAudioEncodingBitRate(paramCamcorderProfile.audioBitRate);
        setAudioChannels(paramCamcorderProfile.audioChannels);
        setAudioSamplingRate(paramCamcorderProfile.audioSampleRate);
        setAudioEncoder(paramCamcorderProfile.audioCodec);
      }  
  }
  
  public void setCaptureRate(double paramDouble) {
    setParameter("time-lapse-enable=1");
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("time-lapse-fps=");
    stringBuilder.append(paramDouble);
    setParameter(stringBuilder.toString());
  }
  
  public void setOrientationHint(int paramInt) {
    if (paramInt == 0 || paramInt == 90 || paramInt == 180 || paramInt == 270) {
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("video-param-rotation-angle-degrees=");
      stringBuilder1.append(paramInt);
      setParameter(stringBuilder1.toString());
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unsupported angle: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setLocation(float paramFloat1, float paramFloat2) {
    int i = (int)((paramFloat1 * 10000.0F) + 0.5D);
    int j = (int)((10000.0F * paramFloat2) + 0.5D);
    if (i <= 900000 && i >= -900000) {
      if (j <= 1800000 && j >= -1800000) {
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("param-geotag-latitude=");
        stringBuilder2.append(i);
        setParameter(stringBuilder2.toString());
        stringBuilder2 = new StringBuilder();
        stringBuilder2.append("param-geotag-longitude=");
        stringBuilder2.append(j);
        setParameter(stringBuilder2.toString());
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("Longitude: ");
      stringBuilder1.append(paramFloat2);
      stringBuilder1.append(" out of range");
      String str1 = stringBuilder1.toString();
      throw new IllegalArgumentException(str1);
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Latitude: ");
    stringBuilder.append(paramFloat1);
    stringBuilder.append(" out of range.");
    String str = stringBuilder.toString();
    throw new IllegalArgumentException(str);
  }
  
  public void setAudioSamplingRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-sampling-rate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Audio sampling rate is not positive");
  }
  
  public void setAudioChannels(int paramInt) {
    if (paramInt > 0) {
      this.mChannelCount = paramInt;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-number-of-channels=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Number of channels is not positive");
  }
  
  public void setAudioEncodingBitRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("audio-param-encoding-bitrate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Audio encoding bit rate is not positive");
  }
  
  public void setVideoEncodingBitRate(int paramInt) {
    if (paramInt > 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("video-param-encoding-bitrate=");
      stringBuilder.append(paramInt);
      setParameter(stringBuilder.toString());
      return;
    } 
    throw new IllegalArgumentException("Video encoding bit rate is not positive");
  }
  
  public void setVideoEncodingProfileLevel(int paramInt1, int paramInt2) {
    if (paramInt1 > 0) {
      if (paramInt2 > 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("video-param-encoder-profile=");
        stringBuilder.append(paramInt1);
        setParameter(stringBuilder.toString());
        stringBuilder = new StringBuilder();
        stringBuilder.append("video-param-encoder-level=");
        stringBuilder.append(paramInt2);
        setParameter(stringBuilder.toString());
        return;
      } 
      throw new IllegalArgumentException("Video encoding level is not positive");
    } 
    throw new IllegalArgumentException("Video encoding profile is not positive");
  }
  
  public void setAuxiliaryOutputFile(FileDescriptor paramFileDescriptor) {
    Log.w("MediaRecorder", "setAuxiliaryOutputFile(FileDescriptor) is no longer supported.");
  }
  
  public void setAuxiliaryOutputFile(String paramString) {
    Log.w("MediaRecorder", "setAuxiliaryOutputFile(String) is no longer supported.");
  }
  
  public void setOutputFile(FileDescriptor paramFileDescriptor) throws IllegalStateException {
    this.mPath = null;
    this.mFile = null;
    this.mFd = paramFileDescriptor;
  }
  
  public void setOutputFile(File paramFile) {
    this.mPath = null;
    this.mFd = null;
    this.mFile = paramFile;
  }
  
  public void setNextOutputFile(FileDescriptor paramFileDescriptor) throws IOException {
    _setNextOutputFile(paramFileDescriptor);
  }
  
  public void setOutputFile(String paramString) throws IllegalStateException {
    this.mFd = null;
    this.mFile = null;
    this.mPath = paramString;
  }
  
  public void setNextOutputFile(File paramFile) throws IOException {
    RandomAccessFile randomAccessFile = new RandomAccessFile(paramFile, "rw");
    try {
      _setNextOutputFile(randomAccessFile.getFD());
      return;
    } finally {
      randomAccessFile.close();
    } 
  }
  
  public void prepare() throws IllegalStateException, IOException {
    // Byte code:
    //   0: ldc_w 'off'
    //   3: ldc_w 'oppo.camera.hypnus.contrl'
    //   6: ldc_w 'on'
    //   9: invokestatic get : (Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    //   12: invokevirtual equals : (Ljava/lang/Object;)Z
    //   15: ifeq -> 26
    //   18: aload_0
    //   19: iconst_0
    //   20: putfield mHypnusCtrl : Z
    //   23: goto -> 31
    //   26: aload_0
    //   27: iconst_1
    //   28: putfield mHypnusCtrl : Z
    //   31: new java/lang/StringBuilder
    //   34: dup
    //   35: invokespecial <init> : ()V
    //   38: astore_1
    //   39: aload_1
    //   40: ldc_w 'mHypnusCtrl is '
    //   43: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   46: pop
    //   47: aload_1
    //   48: aload_0
    //   49: getfield mHypnusCtrl : Z
    //   52: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   55: pop
    //   56: ldc 'MediaRecorder'
    //   58: aload_1
    //   59: invokevirtual toString : ()Ljava/lang/String;
    //   62: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   65: pop
    //   66: getstatic android/media/MediaRecorder.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   69: ifnonnull -> 86
    //   72: ldc android/media/MediaRecorder
    //   74: invokestatic getInstance : (Ljava/lang/Class;)Lcom/oplus/orms/OplusResourceManager;
    //   77: astore_1
    //   78: aload_1
    //   79: putstatic android/media/MediaRecorder.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   82: aload_1
    //   83: ifnull -> 109
    //   86: getstatic android/media/MediaRecorder.mOrmsManager : Lcom/oplus/orms/OplusResourceManager;
    //   89: new com/oplus/orms/info/OrmsSaParam
    //   92: dup
    //   93: ldc_w 'ORMS_SYSTEM_SCENE_MEDIA'
    //   96: ldc_w 'ORMS_ACTION_RECORD'
    //   99: sipush #1000
    //   102: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;I)V
    //   105: invokevirtual ormsSetSceneAction : (Lcom/oplus/orms/info/OrmsSaParam;)J
    //   108: pop2
    //   109: aload_0
    //   110: getfield mPath : Ljava/lang/String;
    //   113: ifnull -> 153
    //   116: new java/io/RandomAccessFile
    //   119: dup
    //   120: aload_0
    //   121: getfield mPath : Ljava/lang/String;
    //   124: ldc_w 'rw'
    //   127: invokespecial <init> : (Ljava/lang/String;Ljava/lang/String;)V
    //   130: astore_1
    //   131: aload_0
    //   132: aload_1
    //   133: invokevirtual getFD : ()Ljava/io/FileDescriptor;
    //   136: invokespecial _setOutputFile : (Ljava/io/FileDescriptor;)V
    //   139: aload_1
    //   140: invokevirtual close : ()V
    //   143: goto -> 204
    //   146: astore_2
    //   147: aload_1
    //   148: invokevirtual close : ()V
    //   151: aload_2
    //   152: athrow
    //   153: aload_0
    //   154: getfield mFd : Ljava/io/FileDescriptor;
    //   157: astore_1
    //   158: aload_1
    //   159: ifnull -> 170
    //   162: aload_0
    //   163: aload_1
    //   164: invokespecial _setOutputFile : (Ljava/io/FileDescriptor;)V
    //   167: goto -> 204
    //   170: aload_0
    //   171: getfield mFile : Ljava/io/File;
    //   174: ifnull -> 216
    //   177: new java/io/RandomAccessFile
    //   180: dup
    //   181: aload_0
    //   182: getfield mFile : Ljava/io/File;
    //   185: ldc_w 'rw'
    //   188: invokespecial <init> : (Ljava/io/File;Ljava/lang/String;)V
    //   191: astore_1
    //   192: aload_0
    //   193: aload_1
    //   194: invokevirtual getFD : ()Ljava/io/FileDescriptor;
    //   197: invokespecial _setOutputFile : (Ljava/io/FileDescriptor;)V
    //   200: aload_1
    //   201: invokevirtual close : ()V
    //   204: aload_0
    //   205: invokespecial _prepare : ()V
    //   208: return
    //   209: astore_2
    //   210: aload_1
    //   211: invokevirtual close : ()V
    //   214: aload_2
    //   215: athrow
    //   216: new java/io/IOException
    //   219: dup
    //   220: ldc_w 'No valid output file'
    //   223: invokespecial <init> : (Ljava/lang/String;)V
    //   226: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1141	-> 0
    //   #1142	-> 18
    //   #1144	-> 26
    //   #1146	-> 31
    //   #1150	-> 66
    //   #1151	-> 86
    //   #1156	-> 109
    //   #1157	-> 116
    //   #1159	-> 131
    //   #1161	-> 139
    //   #1162	-> 143
    //   #1163	-> 143
    //   #1161	-> 146
    //   #1162	-> 151
    //   #1163	-> 153
    //   #1164	-> 162
    //   #1165	-> 170
    //   #1166	-> 177
    //   #1168	-> 192
    //   #1170	-> 200
    //   #1171	-> 204
    //   #1172	-> 204
    //   #1176	-> 204
    //   #1177	-> 208
    //   #1170	-> 209
    //   #1171	-> 214
    //   #1173	-> 216
    // Exception table:
    //   from	to	target	type
    //   131	139	146	finally
    //   192	200	209	finally
  }
  
  public void reset() {
    native_reset();
    this.mEventHandler.removeCallbacksAndMessages(null);
  }
  
  public void setOnErrorListener(OnErrorListener paramOnErrorListener) {
    this.mOnErrorListener = paramOnErrorListener;
  }
  
  public void setOnInfoListener(OnInfoListener paramOnInfoListener) {
    this.mOnInfoListener = paramOnInfoListener;
  }
  
  private class EventHandler extends Handler {
    private static final int MEDIA_RECORDER_AUDIO_ROUTING_CHANGED = 10000;
    
    private static final int MEDIA_RECORDER_EVENT_ERROR = 1;
    
    private static final int MEDIA_RECORDER_EVENT_INFO = 2;
    
    private static final int MEDIA_RECORDER_EVENT_LIST_END = 99;
    
    private static final int MEDIA_RECORDER_EVENT_LIST_START = 1;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_ERROR = 100;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_INFO = 101;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_END = 1000;
    
    private static final int MEDIA_RECORDER_TRACK_EVENT_LIST_START = 100;
    
    private MediaRecorder mMediaRecorder;
    
    final MediaRecorder this$0;
    
    public EventHandler(MediaRecorder param1MediaRecorder1, Looper param1Looper) {
      super(param1Looper);
      this.mMediaRecorder = param1MediaRecorder1;
    }
    
    public void handleMessage(Message param1Message) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mMediaRecorder : Landroid/media/MediaRecorder;
      //   4: invokestatic access$000 : (Landroid/media/MediaRecorder;)J
      //   7: lconst_0
      //   8: lcmp
      //   9: ifne -> 21
      //   12: ldc 'MediaRecorder'
      //   14: ldc 'mediarecorder went away with unhandled events'
      //   16: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   19: pop
      //   20: return
      //   21: aload_1
      //   22: getfield what : I
      //   25: istore_2
      //   26: iload_2
      //   27: iconst_1
      //   28: if_icmpeq -> 195
      //   31: iload_2
      //   32: iconst_2
      //   33: if_icmpeq -> 156
      //   36: iload_2
      //   37: bipush #100
      //   39: if_icmpeq -> 195
      //   42: iload_2
      //   43: bipush #101
      //   45: if_icmpeq -> 156
      //   48: iload_2
      //   49: sipush #10000
      //   52: if_icmpeq -> 90
      //   55: new java/lang/StringBuilder
      //   58: dup
      //   59: invokespecial <init> : ()V
      //   62: astore_3
      //   63: aload_3
      //   64: ldc 'Unknown message type '
      //   66: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   69: pop
      //   70: aload_3
      //   71: aload_1
      //   72: getfield what : I
      //   75: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   78: pop
      //   79: ldc 'MediaRecorder'
      //   81: aload_3
      //   82: invokevirtual toString : ()Ljava/lang/String;
      //   85: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   88: pop
      //   89: return
      //   90: invokestatic resetAudioPortGeneration : ()I
      //   93: pop
      //   94: aload_0
      //   95: getfield this$0 : Landroid/media/MediaRecorder;
      //   98: invokestatic access$300 : (Landroid/media/MediaRecorder;)Landroid/util/ArrayMap;
      //   101: astore_1
      //   102: aload_1
      //   103: monitorenter
      //   104: aload_0
      //   105: getfield this$0 : Landroid/media/MediaRecorder;
      //   108: invokestatic access$300 : (Landroid/media/MediaRecorder;)Landroid/util/ArrayMap;
      //   111: invokevirtual values : ()Ljava/util/Collection;
      //   114: invokeinterface iterator : ()Ljava/util/Iterator;
      //   119: astore_3
      //   120: aload_3
      //   121: invokeinterface hasNext : ()Z
      //   126: ifeq -> 148
      //   129: aload_3
      //   130: invokeinterface next : ()Ljava/lang/Object;
      //   135: checkcast android/media/NativeRoutingEventHandlerDelegate
      //   138: astore #4
      //   140: aload #4
      //   142: invokevirtual notifyClient : ()V
      //   145: goto -> 120
      //   148: aload_1
      //   149: monitorexit
      //   150: return
      //   151: astore_3
      //   152: aload_1
      //   153: monitorexit
      //   154: aload_3
      //   155: athrow
      //   156: aload_0
      //   157: getfield this$0 : Landroid/media/MediaRecorder;
      //   160: invokestatic access$200 : (Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnInfoListener;
      //   163: ifnull -> 190
      //   166: aload_0
      //   167: getfield this$0 : Landroid/media/MediaRecorder;
      //   170: invokestatic access$200 : (Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnInfoListener;
      //   173: aload_0
      //   174: getfield mMediaRecorder : Landroid/media/MediaRecorder;
      //   177: aload_1
      //   178: getfield arg1 : I
      //   181: aload_1
      //   182: getfield arg2 : I
      //   185: invokeinterface onInfo : (Landroid/media/MediaRecorder;II)V
      //   190: goto -> 194
      //   193: astore_1
      //   194: return
      //   195: aload_0
      //   196: getfield this$0 : Landroid/media/MediaRecorder;
      //   199: invokestatic access$100 : (Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnErrorListener;
      //   202: ifnull -> 229
      //   205: aload_0
      //   206: getfield this$0 : Landroid/media/MediaRecorder;
      //   209: invokestatic access$100 : (Landroid/media/MediaRecorder;)Landroid/media/MediaRecorder$OnErrorListener;
      //   212: aload_0
      //   213: getfield mMediaRecorder : Landroid/media/MediaRecorder;
      //   216: aload_1
      //   217: getfield arg1 : I
      //   220: aload_1
      //   221: getfield arg2 : I
      //   224: invokeinterface onError : (Landroid/media/MediaRecorder;II)V
      //   229: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1463	-> 0
      //   #1464	-> 12
      //   #1465	-> 20
      //   #1467	-> 21
      //   #1505	-> 55
      //   #1506	-> 89
      //   #1495	-> 90
      //   #1496	-> 94
      //   #1498	-> 104
      //   #1499	-> 140
      //   #1500	-> 145
      //   #1501	-> 148
      //   #1502	-> 150
      //   #1501	-> 151
      //   #1486	-> 156
      //   #1487	-> 166
      //   #1489	-> 190
      //   #1488	-> 193
      //   #1492	-> 194
      //   #1470	-> 195
      //   #1471	-> 205
      //   #1473	-> 229
      // Exception table:
      //   from	to	target	type
      //   104	120	151	finally
      //   120	140	151	finally
      //   140	145	151	finally
      //   148	150	151	finally
      //   152	154	151	finally
      //   156	166	193	java/lang/NullPointerException
      //   166	190	193	java/lang/NullPointerException
    }
  }
  
  private AudioDeviceInfo mPreferredDevice = null;
  
  public boolean setPreferredDevice(AudioDeviceInfo paramAudioDeviceInfo) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_1
    //   3: ifnull -> 15
    //   6: aload_1
    //   7: invokevirtual isSource : ()Z
    //   10: ifne -> 15
    //   13: iconst_0
    //   14: ireturn
    //   15: aload_1
    //   16: ifnull -> 24
    //   19: aload_1
    //   20: invokevirtual getId : ()I
    //   23: istore_2
    //   24: aload_0
    //   25: iload_2
    //   26: invokespecial native_setInputDevice : (I)Z
    //   29: istore_3
    //   30: iload_3
    //   31: iconst_1
    //   32: if_icmpne -> 52
    //   35: aload_0
    //   36: monitorenter
    //   37: aload_0
    //   38: aload_1
    //   39: putfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   42: aload_0
    //   43: monitorexit
    //   44: goto -> 52
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    //   52: iload_3
    //   53: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1526	-> 0
    //   #1527	-> 13
    //   #1529	-> 15
    //   #1530	-> 24
    //   #1531	-> 30
    //   #1532	-> 35
    //   #1533	-> 37
    //   #1534	-> 42
    //   #1536	-> 52
    // Exception table:
    //   from	to	target	type
    //   37	42	47	finally
    //   42	44	47	finally
    //   48	50	47	finally
  }
  
  public AudioDeviceInfo getPreferredDevice() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1545	-> 0
    //   #1546	-> 2
    //   #1547	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public AudioDeviceInfo getRoutedDevice() {
    int i = native_getRoutedDeviceId();
    if (i == 0)
      return null; 
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = AudioManager.getDevicesStatic(1);
    for (byte b = 0; b < arrayOfAudioDeviceInfo.length; b++) {
      if (arrayOfAudioDeviceInfo[b].getId() == i)
        return arrayOfAudioDeviceInfo[b]; 
    } 
    return null;
  }
  
  private void enableNativeRoutingCallbacksLocked(boolean paramBoolean) {
    if (this.mRoutingChangeListeners.size() == 0)
      native_enableDeviceCallback(paramBoolean); 
  }
  
  private ArrayMap<AudioRouting.OnRoutingChangedListener, NativeRoutingEventHandlerDelegate> mRoutingChangeListeners = new ArrayMap();
  
  public void addOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 67
    //   11: aload_0
    //   12: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   19: ifne -> 67
    //   22: aload_0
    //   23: iconst_1
    //   24: invokespecial enableNativeRoutingCallbacksLocked : (Z)V
    //   27: aload_0
    //   28: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   31: astore #4
    //   33: new android/media/NativeRoutingEventHandlerDelegate
    //   36: astore #5
    //   38: aload_2
    //   39: ifnull -> 45
    //   42: goto -> 50
    //   45: aload_0
    //   46: getfield mEventHandler : Landroid/media/MediaRecorder$EventHandler;
    //   49: astore_2
    //   50: aload #5
    //   52: aload_0
    //   53: aload_1
    //   54: aload_2
    //   55: invokespecial <init> : (Landroid/media/AudioRouting;Landroid/media/AudioRouting$OnRoutingChangedListener;Landroid/os/Handler;)V
    //   58: aload #4
    //   60: aload_1
    //   61: aload #5
    //   63: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   66: pop
    //   67: aload_3
    //   68: monitorexit
    //   69: return
    //   70: astore_1
    //   71: aload_3
    //   72: monitorexit
    //   73: aload_1
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1602	-> 0
    //   #1603	-> 7
    //   #1604	-> 22
    //   #1605	-> 27
    //   #1607	-> 38
    //   #1605	-> 58
    //   #1609	-> 67
    //   #1610	-> 69
    //   #1609	-> 70
    // Exception table:
    //   from	to	target	type
    //   11	22	70	finally
    //   22	27	70	finally
    //   27	38	70	finally
    //   45	50	70	finally
    //   50	58	70	finally
    //   58	67	70	finally
    //   67	69	70	finally
    //   71	73	70	finally
  }
  
  public void removeOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener) {
    synchronized (this.mRoutingChangeListeners) {
      if (this.mRoutingChangeListeners.containsKey(paramOnRoutingChangedListener)) {
        this.mRoutingChangeListeners.remove(paramOnRoutingChangedListener);
        enableNativeRoutingCallbacksLocked(false);
      } 
      return;
    } 
  }
  
  public List<MicrophoneInfo> getActiveMicrophones() throws IOException {
    ArrayList<MicrophoneInfo> arrayList = new ArrayList();
    int i = native_getActiveMicrophones(arrayList);
    if (i != 0) {
      if (i != -3) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getActiveMicrophones failed:");
        stringBuilder.append(i);
        Log.e("MediaRecorder", stringBuilder.toString());
      } 
      Log.i("MediaRecorder", "getActiveMicrophones failed, fallback on routed device info");
    } 
    AudioManager.setPortIdForMicrophones(arrayList);
    if (arrayList.size() == 0) {
      AudioDeviceInfo audioDeviceInfo = getRoutedDevice();
      if (audioDeviceInfo != null) {
        MicrophoneInfo microphoneInfo = AudioManager.microphoneInfoFromAudioDeviceInfo(audioDeviceInfo);
        ArrayList<Pair> arrayList1 = new ArrayList();
        for (i = 0; i < this.mChannelCount; i++)
          arrayList1.add(new Pair(Integer.valueOf(i), Integer.valueOf(1))); 
        microphoneInfo.setChannelMapping((List)arrayList1);
        arrayList.add(microphoneInfo);
      } 
    } 
    return arrayList;
  }
  
  public boolean setPreferredMicrophoneDirection(int paramInt) {
    boolean bool;
    if (native_setPreferredMicrophoneDirection(paramInt) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean setPreferredMicrophoneFieldDimension(float paramFloat) {
    boolean bool2, bool1 = true;
    if (paramFloat >= -1.0F && paramFloat <= 1.0F) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "Argument must fall between -1 & 1 (inclusive)");
    if (native_setPreferredMicrophoneFieldDimension(paramFloat) == 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  AudioRecordingMonitorImpl mRecordingInfoImpl = new AudioRecordingMonitorImpl(this);
  
  private static final int CAMERA_TIME_HYPNUS_1000 = 1000;
  
  public static final int MEDIA_ERROR_SERVER_DIED = 100;
  
  public static final int MEDIA_RECORDER_ERROR_UNKNOWN = 1;
  
  public static final int MEDIA_RECORDER_INFO_MAX_DURATION_REACHED = 800;
  
  public static final int MEDIA_RECORDER_INFO_MAX_FILESIZE_APPROACHING = 802;
  
  public static final int MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED = 801;
  
  public static final int MEDIA_RECORDER_INFO_NEXT_OUTPUT_FILE_STARTED = 803;
  
  public static final int MEDIA_RECORDER_INFO_UNKNOWN = 1;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_COMPLETION_STATUS = 1000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_DATA_KBYTES = 1009;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_DURATION_MS = 1003;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_ENCODED_FRAMES = 1005;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_INITIAL_DELAY_MS = 1007;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_LIST_END = 2000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_LIST_START = 1000;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_MAX_CHUNK_DUR_MS = 1004;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_PROGRESS_IN_TIME = 1001;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_START_OFFSET_MS = 1008;
  
  public static final int MEDIA_RECORDER_TRACK_INFO_TYPE = 1002;
  
  public static final int MEDIA_RECORDER_TRACK_INTER_CHUNK_TIME_MS = 1006;
  
  private static final String TAG = "MediaRecorder";
  
  private int mChannelCount;
  
  private EventHandler mEventHandler;
  
  private FileDescriptor mFd;
  
  private File mFile;
  
  private boolean mHypnusCtrl;
  
  private long mNativeContext;
  
  private OnErrorListener mOnErrorListener;
  
  private OnInfoListener mOnInfoListener;
  
  private String mPath;
  
  private Surface mSurface;
  
  public void registerAudioRecordingCallback(Executor paramExecutor, AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    this.mRecordingInfoImpl.registerAudioRecordingCallback(paramExecutor, paramAudioRecordingCallback);
  }
  
  public void unregisterAudioRecordingCallback(AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    this.mRecordingInfoImpl.unregisterAudioRecordingCallback(paramAudioRecordingCallback);
  }
  
  public AudioRecordingConfiguration getActiveRecordingConfiguration() {
    return this.mRecordingInfoImpl.getActiveRecordingConfiguration();
  }
  
  public int getPortId() {
    if (this.mNativeContext == 0L)
      return 0; 
    return native_getPortId();
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<MediaRecorder>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    EventHandler eventHandler = ((MediaRecorder)paramObject1).mEventHandler;
    if (eventHandler != null) {
      paramObject2 = eventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
      ((MediaRecorder)paramObject1).mEventHandler.sendMessage((Message)paramObject2);
    } 
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  private native void _prepare() throws IllegalStateException, IOException;
  
  private native void _setNextOutputFile(FileDescriptor paramFileDescriptor) throws IllegalStateException, IOException;
  
  private native void _setOutputFile(FileDescriptor paramFileDescriptor) throws IllegalStateException, IOException;
  
  private final native void native_enableDeviceCallback(boolean paramBoolean);
  
  private final native void native_finalize();
  
  private final native int native_getActiveMicrophones(ArrayList<MicrophoneInfo> paramArrayList);
  
  private native PersistableBundle native_getMetrics();
  
  private native int native_getPortId();
  
  private final native int native_getRoutedDeviceId();
  
  private static final native void native_init();
  
  private native void native_reset();
  
  private final native boolean native_setInputDevice(int paramInt);
  
  private final native void native_setInputSurface(Surface paramSurface);
  
  private native int native_setPreferredMicrophoneDirection(int paramInt);
  
  private native int native_setPreferredMicrophoneFieldDimension(float paramFloat);
  
  private final native void native_setup(Object paramObject, String paramString1, String paramString2) throws IllegalStateException;
  
  private native void setParameter(String paramString);
  
  public native int getMaxAmplitude() throws IllegalStateException;
  
  public native Surface getSurface();
  
  public native boolean isPrivacySensitive();
  
  public native void pause() throws IllegalStateException;
  
  public native void release();
  
  public native void resume() throws IllegalStateException;
  
  public native void setAudioEncoder(int paramInt) throws IllegalStateException;
  
  public native void setAudioSource(int paramInt) throws IllegalStateException;
  
  @Deprecated
  public native void setCamera(Camera paramCamera);
  
  public native void setMaxDuration(int paramInt) throws IllegalArgumentException;
  
  public native void setMaxFileSize(long paramLong) throws IllegalArgumentException;
  
  public native void setOutputFormat(int paramInt) throws IllegalStateException;
  
  public native void setPrivacySensitive(boolean paramBoolean);
  
  public native void setVideoEncoder(int paramInt) throws IllegalStateException;
  
  public native void setVideoFrameRate(int paramInt) throws IllegalStateException;
  
  public native void setVideoSize(int paramInt1, int paramInt2) throws IllegalStateException;
  
  public native void setVideoSource(int paramInt) throws IllegalStateException;
  
  public native void start() throws IllegalStateException;
  
  public native void stop() throws IllegalStateException;
  
  class MetricsConstants {
    public static final String AUDIO_BITRATE = "android.media.mediarecorder.audio-bitrate";
    
    public static final String AUDIO_CHANNELS = "android.media.mediarecorder.audio-channels";
    
    public static final String AUDIO_SAMPLERATE = "android.media.mediarecorder.audio-samplerate";
    
    public static final String AUDIO_TIMESCALE = "android.media.mediarecorder.audio-timescale";
    
    public static final String CAPTURE_FPS = "android.media.mediarecorder.capture-fps";
    
    public static final String CAPTURE_FPS_ENABLE = "android.media.mediarecorder.capture-fpsenable";
    
    public static final String FRAMERATE = "android.media.mediarecorder.frame-rate";
    
    public static final String HEIGHT = "android.media.mediarecorder.height";
    
    public static final String MOVIE_TIMESCALE = "android.media.mediarecorder.movie-timescale";
    
    public static final String ROTATION = "android.media.mediarecorder.rotation";
    
    public static final String VIDEO_BITRATE = "android.media.mediarecorder.video-bitrate";
    
    public static final String VIDEO_IFRAME_INTERVAL = "android.media.mediarecorder.video-iframe-interval";
    
    public static final String VIDEO_LEVEL = "android.media.mediarecorder.video-encoder-level";
    
    public static final String VIDEO_PROFILE = "android.media.mediarecorder.video-encoder-profile";
    
    public static final String VIDEO_TIMESCALE = "android.media.mediarecorder.video-timescale";
    
    public static final String WIDTH = "android.media.mediarecorder.width";
  }
  
  class OnErrorListener {
    public abstract void onError(MediaRecorder param1MediaRecorder, int param1Int1, int param1Int2);
  }
  
  class OnInfoListener {
    public abstract void onInfo(MediaRecorder param1MediaRecorder, int param1Int1, int param1Int2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class Source implements Annotation {}
}
