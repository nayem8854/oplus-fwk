package android.media;

import android.os.Parcel;

public final class SubtitleData {
  private static final String TAG = "SubtitleData";
  
  private byte[] mData;
  
  private long mDurationUs;
  
  private long mStartTimeUs;
  
  private int mTrackIndex;
  
  public SubtitleData(Parcel paramParcel) {
    if (parseParcel(paramParcel))
      return; 
    throw new IllegalArgumentException("parseParcel() fails");
  }
  
  public SubtitleData(int paramInt, long paramLong1, long paramLong2, byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte != null) {
      this.mTrackIndex = paramInt;
      this.mStartTimeUs = paramLong1;
      this.mDurationUs = paramLong2;
      this.mData = paramArrayOfbyte;
      return;
    } 
    throw new IllegalArgumentException("null data is not allowed");
  }
  
  public int getTrackIndex() {
    return this.mTrackIndex;
  }
  
  public long getStartTimeUs() {
    return this.mStartTimeUs;
  }
  
  public long getDurationUs() {
    return this.mDurationUs;
  }
  
  public byte[] getData() {
    return this.mData;
  }
  
  private boolean parseParcel(Parcel paramParcel) {
    paramParcel.setDataPosition(0);
    if (paramParcel.dataAvail() == 0)
      return false; 
    this.mTrackIndex = paramParcel.readInt();
    this.mStartTimeUs = paramParcel.readLong();
    this.mDurationUs = paramParcel.readLong();
    byte[] arrayOfByte = new byte[paramParcel.readInt()];
    paramParcel.readByteArray(arrayOfByte);
    return true;
  }
}
