package android.media;

public class AudioGainConfig {
  private final int mChannelMask;
  
  AudioGain mGain;
  
  private final int mIndex;
  
  private final int mMode;
  
  private final int mRampDurationMs;
  
  private final int[] mValues;
  
  AudioGainConfig(int paramInt1, AudioGain paramAudioGain, int paramInt2, int paramInt3, int[] paramArrayOfint, int paramInt4) {
    this.mIndex = paramInt1;
    this.mGain = paramAudioGain;
    this.mMode = paramInt2;
    this.mChannelMask = paramInt3;
    this.mValues = paramArrayOfint;
    this.mRampDurationMs = paramInt4;
  }
  
  int index() {
    return this.mIndex;
  }
  
  public int mode() {
    return this.mMode;
  }
  
  public int channelMask() {
    return this.mChannelMask;
  }
  
  public int[] values() {
    return this.mValues;
  }
  
  public int rampDurationMs() {
    return this.mRampDurationMs;
  }
}
