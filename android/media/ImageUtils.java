package android.media;

import android.util.Size;
import java.nio.ByteBuffer;
import libcore.io.Memory;

class ImageUtils {
  public static int getNumPlanesForFormat(int paramInt) {
    if (paramInt != 1 && paramInt != 2 && paramInt != 3 && paramInt != 4) {
      if (paramInt != 16)
        if (paramInt != 17) {
          if (paramInt != 256 && paramInt != 257)
            switch (paramInt) {
              default:
                switch (paramInt) {
                  default:
                    throw new UnsupportedOperationException(String.format("Invalid format specified %d", new Object[] { Integer.valueOf(paramInt) }));
                  case 34:
                    return 0;
                  case 35:
                    return 3;
                  case 36:
                  case 37:
                  case 38:
                    break;
                } 
                break;
              case 842094169:
              
              case 20:
              case 32:
              case 4098:
              case 538982489:
              case 540422489:
              case 1144402265:
              case 1212500294:
              case 1768253795:
                break;
            }  
          return 1;
        }  
      return 2;
    } 
    return 1;
  }
  
  public static void imageCopy(Image paramImage1, Image paramImage2) {
    if (paramImage1 != null && paramImage2 != null) {
      if (paramImage1.getFormat() == paramImage2.getFormat()) {
        if (paramImage1.getFormat() != 34 && 
          paramImage2.getFormat() != 34) {
          if (paramImage1.getFormat() != 36) {
            if (paramImage1.getFormat() != 4098) {
              if (paramImage2.getOwner() instanceof ImageWriter) {
                Image.Plane plane;
                Size size1 = new Size(paramImage1.getWidth(), paramImage1.getHeight());
                Size size2 = new Size(paramImage2.getWidth(), paramImage2.getHeight());
                if (size1.equals(size2)) {
                  Image.Plane[] arrayOfPlane2 = paramImage1.getPlanes();
                  Image.Plane[] arrayOfPlane1 = paramImage2.getPlanes();
                  for (byte b = 0; b < arrayOfPlane2.length; ) {
                    int i = arrayOfPlane2[b].getRowStride();
                    int j = arrayOfPlane1[b].getRowStride();
                    ByteBuffer byteBuffer1 = arrayOfPlane2[b].getBuffer();
                    ByteBuffer byteBuffer2 = arrayOfPlane1[b].getBuffer();
                    if (byteBuffer1.isDirect() && byteBuffer2.isDirect()) {
                      if (arrayOfPlane2[b].getPixelStride() == arrayOfPlane1[b].getPixelStride()) {
                        int k = byteBuffer1.position();
                        byteBuffer1.rewind();
                        byteBuffer2.rewind();
                        if (i == j) {
                          byteBuffer2.put(byteBuffer1);
                        } else {
                          int m = byteBuffer1.position();
                          int n = byteBuffer2.position();
                          size1 = getEffectivePlaneSizeForImage(paramImage1, b);
                          int i1 = size1.getWidth() * arrayOfPlane2[b].getPixelStride();
                          for (byte b1 = 0; b1 < size1.getHeight(); b1++, i1 = i2) {
                            int i2 = i1;
                            if (b1 == size1.getHeight() - 1) {
                              int i3 = byteBuffer1.remaining() - m;
                              i2 = i1;
                              if (i1 > i3)
                                i2 = i3; 
                            } 
                            directByteBufferCopy(byteBuffer1, m, byteBuffer2, n, i2);
                            m += i;
                            n += j;
                          } 
                        } 
                        byteBuffer1.position(k);
                        byteBuffer2.rewind();
                        b++;
                      } 
                      StringBuilder stringBuilder1 = new StringBuilder();
                      stringBuilder1.append("Source plane image pixel stride ");
                      plane = arrayOfPlane2[b];
                      stringBuilder1.append(plane.getPixelStride());
                      stringBuilder1.append(" must be same as destination image pixel stride ");
                      Image.Plane plane1 = arrayOfPlane1[b];
                      stringBuilder1.append(plane1.getPixelStride());
                      throw new IllegalArgumentException(stringBuilder1.toString());
                    } 
                    throw new IllegalArgumentException("Source and destination ByteBuffers must be direct byteBuffer!");
                  } 
                  return;
                } 
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("source image size ");
                stringBuilder.append(size1);
                stringBuilder.append(" is different with destination image size ");
                stringBuilder.append(plane);
                throw new IllegalArgumentException(stringBuilder.toString());
              } 
              throw new IllegalArgumentException("Destination image is not from ImageWriter. Only the images from ImageWriter are writable");
            } 
            throw new IllegalArgumentException("Copy of RAW_DEPTH format has not been implemented");
          } 
          throw new IllegalArgumentException("Copy of RAW_OPAQUE format has not been implemented");
        } 
        throw new IllegalArgumentException("PRIVATE format images are not copyable");
      } 
      throw new IllegalArgumentException("Src and dst images should have the same format");
    } 
    throw new IllegalArgumentException("Images should be non-null");
  }
  
  public static int getEstimatedNativeAllocBytes(int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    double d;
    if (paramInt3 != 1 && paramInt3 != 2) {
      if (paramInt3 != 3) {
        if (paramInt3 != 4 && paramInt3 != 16)
          if (paramInt3 != 17) {
            if (paramInt3 != 256 && paramInt3 != 257) {
              double d1;
              switch (paramInt3) {
                default:
                  switch (paramInt3) {
                    default:
                      throw new UnsupportedOperationException(String.format("Invalid format specified %d", new Object[] { Integer.valueOf(paramInt3) }));
                    case 37:
                      d1 = 1.25D;
                      return (int)((paramInt1 * paramInt2) * d1 * paramInt4);
                    case 34:
                    case 35:
                    case 38:
                      d1 = 1.5D;
                      return (int)((paramInt1 * paramInt2) * d1 * paramInt4);
                    case 36:
                      break;
                  } 
                  break;
                case 538982489:
                  d1 = 1.0D;
                  return (int)((paramInt1 * paramInt2) * d1 * paramInt4);
                case 1212500294:
                case 1768253795:
                  d1 = 0.3D;
                  return (int)((paramInt1 * paramInt2) * d1 * paramInt4);
                case 842094169:
                
                case 20:
                case 32:
                case 4098:
                case 540422489:
                case 1144402265:
                  break;
              } 
            } else {
            
            } 
          } else {
          
          }  
        d = 2.0D;
      } else {
        d = 3.0D;
      } 
    } else {
      d = 4.0D;
    } 
    return (int)((paramInt1 * paramInt2) * d * paramInt4);
  }
  
  private static Size getEffectivePlaneSizeForImage(Image paramImage, int paramInt) {
    int i = paramImage.getFormat();
    if (i != 1 && i != 2 && i != 3 && i != 4) {
      if (i != 16)
        if (i != 17) {
          if (i != 34)
            if (i != 35) {
              if (i != 37 && i != 38)
                switch (i) {
                  default:
                    throw new UnsupportedOperationException(String.format("Invalid image format %d", new Object[] { Integer.valueOf(paramImage.getFormat()) }));
                  case 842094169:
                    if (paramInt == 0)
                      return new Size(paramImage.getWidth(), paramImage.getHeight()); 
                    return new Size(paramImage.getWidth() / 2, paramImage.getHeight() / 2);
                  case 20:
                  case 32:
                  case 256:
                  case 4098:
                  case 538982489:
                  case 540422489:
                  case 1212500294:
                    break;
                }  
              return new Size(paramImage.getWidth(), paramImage.getHeight());
            }  
          return new Size(0, 0);
        }  
      if (paramInt == 0)
        return new Size(paramImage.getWidth(), paramImage.getHeight()); 
      return new Size(paramImage.getWidth(), paramImage.getHeight() / 2);
    } 
    return new Size(paramImage.getWidth(), paramImage.getHeight());
  }
  
  private static void directByteBufferCopy(ByteBuffer paramByteBuffer1, int paramInt1, ByteBuffer paramByteBuffer2, int paramInt2, int paramInt3) {
    Memory.memmove(paramByteBuffer2, paramInt2, paramByteBuffer1, paramInt1, paramInt3);
  }
}
