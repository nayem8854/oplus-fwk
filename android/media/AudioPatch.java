package android.media;

public class AudioPatch {
  private final AudioHandle mHandle;
  
  private final AudioPortConfig[] mSinks;
  
  private final AudioPortConfig[] mSources;
  
  AudioPatch(AudioHandle paramAudioHandle, AudioPortConfig[] paramArrayOfAudioPortConfig1, AudioPortConfig[] paramArrayOfAudioPortConfig2) {
    this.mHandle = paramAudioHandle;
    this.mSources = paramArrayOfAudioPortConfig1;
    this.mSinks = paramArrayOfAudioPortConfig2;
  }
  
  public AudioPortConfig[] sources() {
    return this.mSources;
  }
  
  public AudioPortConfig[] sinks() {
    return this.mSinks;
  }
  
  public int id() {
    return this.mHandle.id();
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("mHandle: ");
    stringBuilder.append(this.mHandle.toString());
    stringBuilder.append(" mSources: {");
    AudioPortConfig[] arrayOfAudioPortConfig;
    int i;
    boolean bool;
    byte b;
    for (arrayOfAudioPortConfig = this.mSources, i = arrayOfAudioPortConfig.length, bool = false, b = 0; b < i; ) {
      AudioPortConfig audioPortConfig = arrayOfAudioPortConfig[b];
      stringBuilder.append(audioPortConfig.toString());
      stringBuilder.append(", ");
      b++;
    } 
    stringBuilder.append("} mSinks: {");
    for (arrayOfAudioPortConfig = this.mSinks, i = arrayOfAudioPortConfig.length, b = bool; b < i; ) {
      AudioPortConfig audioPortConfig = arrayOfAudioPortConfig[b];
      stringBuilder.append(audioPortConfig.toString());
      stringBuilder.append(", ");
      b++;
    } 
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
}
