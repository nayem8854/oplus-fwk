package android.media;

import android.content.Context;

public class TtmlRenderer extends SubtitleController.Renderer {
  private static final String MEDIA_MIMETYPE_TEXT_TTML = "application/ttml+xml";
  
  private final Context mContext;
  
  private TtmlRenderingWidget mRenderingWidget;
  
  public TtmlRenderer(Context paramContext) {
    this.mContext = paramContext;
  }
  
  public boolean supports(MediaFormat paramMediaFormat) {
    if (paramMediaFormat.containsKey("mime"))
      return paramMediaFormat.getString("mime").equals("application/ttml+xml"); 
    return false;
  }
  
  public SubtitleTrack createTrack(MediaFormat paramMediaFormat) {
    if (this.mRenderingWidget == null)
      this.mRenderingWidget = new TtmlRenderingWidget(this.mContext); 
    return new TtmlTrack(this.mRenderingWidget, paramMediaFormat);
  }
}
