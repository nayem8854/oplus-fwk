package android.media;

import java.util.Vector;

class Cea608CaptionTrack extends SubtitleTrack {
  private final Cea608CCParser mCCParser;
  
  private final Cea608CCWidget mRenderingWidget;
  
  Cea608CaptionTrack(Cea608CCWidget paramCea608CCWidget, MediaFormat paramMediaFormat) {
    super(paramMediaFormat);
    this.mRenderingWidget = paramCea608CCWidget;
    this.mCCParser = new Cea608CCParser(this.mRenderingWidget);
  }
  
  public void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong) {
    this.mCCParser.parse(paramArrayOfbyte);
  }
  
  public SubtitleTrack.RenderingWidget getRenderingWidget() {
    return this.mRenderingWidget;
  }
  
  public void updateView(Vector<SubtitleTrack.Cue> paramVector) {}
}
