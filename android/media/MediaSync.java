package android.media;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.Surface;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class MediaSync {
  public static abstract class Callback {
    public abstract void onAudioBufferConsumed(MediaSync param1MediaSync, ByteBuffer param1ByteBuffer, int param1Int);
  }
  
  private static class AudioBuffer {
    public int mBufferIndex;
    
    public ByteBuffer mByteBuffer;
    
    long mPresentationTimeUs;
    
    public AudioBuffer(ByteBuffer param1ByteBuffer, int param1Int, long param1Long) {
      this.mByteBuffer = param1ByteBuffer;
      this.mBufferIndex = param1Int;
      this.mPresentationTimeUs = param1Long;
    }
  }
  
  private final Object mCallbackLock = new Object();
  
  private Handler mCallbackHandler = null;
  
  private Callback mCallback = null;
  
  private final Object mOnErrorListenerLock = new Object();
  
  private Handler mOnErrorListenerHandler = null;
  
  private OnErrorListener mOnErrorListener = null;
  
  private Thread mAudioThread = null;
  
  private Handler mAudioHandler = null;
  
  private Looper mAudioLooper = null;
  
  private final Object mAudioLock = new Object();
  
  private AudioTrack mAudioTrack = null;
  
  private List<AudioBuffer> mAudioBuffers = new LinkedList<>();
  
  private float mPlaybackRate = 0.0F;
  
  private static final int CB_RETURN_AUDIO_BUFFER = 1;
  
  private static final int EVENT_CALLBACK = 1;
  
  private static final int EVENT_SET_CALLBACK = 2;
  
  public static final int MEDIASYNC_ERROR_AUDIOTRACK_FAIL = 1;
  
  public static final int MEDIASYNC_ERROR_SURFACE_FAIL = 2;
  
  private static final String TAG = "MediaSync";
  
  private long mNativeContext;
  
  public MediaSync() {
    native_setup();
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  public final void release() {
    returnAudioBuffers();
    if (this.mAudioThread != null) {
      Looper looper = this.mAudioLooper;
      if (looper != null)
        looper.quit(); 
    } 
    setCallback(null, null);
    native_release();
  }
  
  public void setCallback(Callback paramCallback, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCallbackLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_2
    //   8: ifnull -> 19
    //   11: aload_0
    //   12: aload_2
    //   13: putfield mCallbackHandler : Landroid/os/Handler;
    //   16: goto -> 65
    //   19: invokestatic myLooper : ()Landroid/os/Looper;
    //   22: astore #4
    //   24: aload #4
    //   26: astore_2
    //   27: aload #4
    //   29: ifnonnull -> 36
    //   32: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   35: astore_2
    //   36: aload_2
    //   37: ifnonnull -> 48
    //   40: aload_0
    //   41: aconst_null
    //   42: putfield mCallbackHandler : Landroid/os/Handler;
    //   45: goto -> 65
    //   48: new android/os/Handler
    //   51: astore #4
    //   53: aload #4
    //   55: aload_2
    //   56: invokespecial <init> : (Landroid/os/Looper;)V
    //   59: aload_0
    //   60: aload #4
    //   62: putfield mCallbackHandler : Landroid/os/Handler;
    //   65: aload_0
    //   66: aload_1
    //   67: putfield mCallback : Landroid/media/MediaSync$Callback;
    //   70: aload_3
    //   71: monitorexit
    //   72: return
    //   73: astore_1
    //   74: aload_3
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #254	-> 0
    //   #255	-> 7
    //   #256	-> 11
    //   #259	-> 19
    //   #260	-> 32
    //   #262	-> 36
    //   #263	-> 40
    //   #265	-> 48
    //   #269	-> 65
    //   #270	-> 70
    //   #271	-> 72
    //   #270	-> 73
    // Exception table:
    //   from	to	target	type
    //   11	16	73	finally
    //   19	24	73	finally
    //   32	36	73	finally
    //   40	45	73	finally
    //   48	65	73	finally
    //   65	70	73	finally
    //   70	72	73	finally
    //   74	76	73	finally
  }
  
  public void setOnErrorListener(OnErrorListener paramOnErrorListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mOnErrorListenerLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_2
    //   8: ifnull -> 19
    //   11: aload_0
    //   12: aload_2
    //   13: putfield mOnErrorListenerHandler : Landroid/os/Handler;
    //   16: goto -> 65
    //   19: invokestatic myLooper : ()Landroid/os/Looper;
    //   22: astore #4
    //   24: aload #4
    //   26: astore_2
    //   27: aload #4
    //   29: ifnonnull -> 36
    //   32: invokestatic getMainLooper : ()Landroid/os/Looper;
    //   35: astore_2
    //   36: aload_2
    //   37: ifnonnull -> 48
    //   40: aload_0
    //   41: aconst_null
    //   42: putfield mOnErrorListenerHandler : Landroid/os/Handler;
    //   45: goto -> 65
    //   48: new android/os/Handler
    //   51: astore #4
    //   53: aload #4
    //   55: aload_2
    //   56: invokespecial <init> : (Landroid/os/Looper;)V
    //   59: aload_0
    //   60: aload #4
    //   62: putfield mOnErrorListenerHandler : Landroid/os/Handler;
    //   65: aload_0
    //   66: aload_1
    //   67: putfield mOnErrorListener : Landroid/media/MediaSync$OnErrorListener;
    //   70: aload_3
    //   71: monitorexit
    //   72: return
    //   73: astore_1
    //   74: aload_3
    //   75: monitorexit
    //   76: aload_1
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #287	-> 0
    //   #288	-> 7
    //   #289	-> 11
    //   #292	-> 19
    //   #293	-> 32
    //   #295	-> 36
    //   #296	-> 40
    //   #298	-> 48
    //   #302	-> 65
    //   #303	-> 70
    //   #304	-> 72
    //   #303	-> 73
    // Exception table:
    //   from	to	target	type
    //   11	16	73	finally
    //   19	24	73	finally
    //   32	36	73	finally
    //   40	45	73	finally
    //   48	65	73	finally
    //   65	70	73	finally
    //   70	72	73	finally
    //   74	76	73	finally
  }
  
  public void setSurface(Surface paramSurface) {
    native_setSurface(paramSurface);
  }
  
  public void setAudioTrack(AudioTrack paramAudioTrack) {
    native_setAudioTrack(paramAudioTrack);
    this.mAudioTrack = paramAudioTrack;
    if (paramAudioTrack != null && this.mAudioThread == null)
      createAudioThread(); 
  }
  
  public void setPlaybackParams(PlaybackParams paramPlaybackParams) {
    synchronized (this.mAudioLock) {
      float f = native_setPlaybackParams(paramPlaybackParams);
      if (f != 0.0D && this.mAudioThread != null)
        postRenderAudio(0L); 
      return;
    } 
  }
  
  public void setSyncParams(SyncParams paramSyncParams) {
    synchronized (this.mAudioLock) {
      float f = native_setSyncParams(paramSyncParams);
      if (f != 0.0D && this.mAudioThread != null)
        postRenderAudio(0L); 
      return;
    } 
  }
  
  public void flush() {
    synchronized (this.mAudioLock) {
      this.mAudioBuffers.clear();
      this.mCallbackHandler.removeCallbacksAndMessages(null);
      AudioTrack audioTrack = this.mAudioTrack;
      if (audioTrack != null) {
        audioTrack.pause();
        this.mAudioTrack.flush();
        this.mAudioTrack.stop();
      } 
      native_flush();
      return;
    } 
  }
  
  public MediaTimestamp getTimestamp() {
    try {
      MediaTimestamp mediaTimestamp = new MediaTimestamp();
      this();
      boolean bool = native_getTimestamp(mediaTimestamp);
      if (bool)
        return mediaTimestamp; 
      return null;
    } catch (IllegalStateException illegalStateException) {
      return null;
    } 
  }
  
  public void queueAudio(ByteBuffer paramByteBuffer, int paramInt, long paramLong) {
    if (this.mAudioTrack != null && this.mAudioThread != null)
      synchronized (this.mAudioLock) {
        List<AudioBuffer> list = this.mAudioBuffers;
        AudioBuffer audioBuffer = new AudioBuffer();
        this(paramByteBuffer, paramInt, paramLong);
        list.add(audioBuffer);
        if (this.mPlaybackRate != 0.0D)
          postRenderAudio(0L); 
        return;
      }  
    throw new IllegalStateException("AudioTrack is NOT set or audio thread is not created");
  }
  
  private void postRenderAudio(long paramLong) {
    this.mAudioHandler.postDelayed(new Runnable() {
          final MediaSync this$0;
          
          public void run() {
            synchronized (MediaSync.this.mAudioLock) {
              if (MediaSync.this.mPlaybackRate == 0.0D)
                return; 
              if (MediaSync.this.mAudioBuffers.isEmpty())
                return; 
              MediaSync.AudioBuffer audioBuffer = MediaSync.this.mAudioBuffers.get(0);
              int i = audioBuffer.mByteBuffer.remaining();
              if (i > 0) {
                int k = MediaSync.this.mAudioTrack.getPlayState();
                if (k != 3)
                  try {
                    MediaSync.this.mAudioTrack.play();
                  } catch (IllegalStateException illegalStateException) {
                    Log.w("MediaSync", "could not start audio track");
                  }  
              } 
              int j = MediaSync.this.mAudioTrack.write(audioBuffer.mByteBuffer, i, 1);
              if (j > 0) {
                if (audioBuffer.mPresentationTimeUs != -1L) {
                  MediaSync.this.native_updateQueuedAudioData(i, audioBuffer.mPresentationTimeUs);
                  audioBuffer.mPresentationTimeUs = -1L;
                } 
                if (j == i) {
                  MediaSync.this.postReturnByteBuffer(audioBuffer);
                  MediaSync.this.mAudioBuffers.remove(0);
                  if (!MediaSync.this.mAudioBuffers.isEmpty())
                    MediaSync.this.postRenderAudio(0L); 
                  return;
                } 
              } 
              TimeUnit timeUnit = TimeUnit.MICROSECONDS;
              MediaSync mediaSync = MediaSync.this;
              long l = mediaSync.native_getPlayTimeForPendingAudioFrames();
              l = timeUnit.toMillis(l);
              MediaSync.this.postRenderAudio(l / 2L);
              return;
            } 
          }
        }paramLong);
  }
  
  private final void postReturnByteBuffer(AudioBuffer paramAudioBuffer) {
    synchronized (this.mCallbackLock) {
      if (this.mCallbackHandler != null) {
        Handler handler = this.mCallbackHandler;
        Runnable runnable = new Runnable() {
            final MediaSync this$0;
            
            final MediaSync.AudioBuffer val$audioBuffer;
            
            final MediaSync val$sync;
            
            public void run() {
              synchronized (MediaSync.this.mCallbackLock) {
                MediaSync.Callback callback = MediaSync.this.mCallback;
                if (MediaSync.this.mCallbackHandler != null) {
                  MediaSync mediaSync = MediaSync.this;
                  Thread thread = mediaSync.mCallbackHandler.getLooper().getThread();
                  if (thread == Thread.currentThread()) {
                    if (callback != null)
                      callback.onAudioBufferConsumed(sync, audioBuffer.mByteBuffer, audioBuffer.mBufferIndex); 
                    return;
                  } 
                } 
                return;
              } 
            }
          };
        super(this, this, paramAudioBuffer);
        handler.post(runnable);
      } 
      return;
    } 
  }
  
  private final void returnAudioBuffers() {
    synchronized (this.mAudioLock) {
      for (AudioBuffer audioBuffer : this.mAudioBuffers)
        postReturnByteBuffer(audioBuffer); 
      this.mAudioBuffers.clear();
      return;
    } 
  }
  
  private void createAudioThread() {
    Thread thread = new Thread() {
        final MediaSync this$0;
        
        public void run() {
          Looper.prepare();
          synchronized (MediaSync.this.mAudioLock) {
            MediaSync.access$1102(MediaSync.this, Looper.myLooper());
            MediaSync mediaSync = MediaSync.this;
            Handler handler = new Handler();
            this();
            MediaSync.access$1202(mediaSync, handler);
            MediaSync.this.mAudioLock.notify();
            Looper.loop();
            return;
          } 
        }
      };
    thread.start();
    Object object = this.mAudioLock;
    /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
    try {
      this.mAudioLock.wait();
    } catch (InterruptedException interruptedException) {
    
    } finally {
      Exception exception;
    } 
    /* monitor exit ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private final native void native_finalize();
  
  private final native void native_flush();
  
  private final native long native_getPlayTimeForPendingAudioFrames();
  
  private final native boolean native_getTimestamp(MediaTimestamp paramMediaTimestamp);
  
  private static final native void native_init();
  
  private final native void native_release();
  
  private final native void native_setAudioTrack(AudioTrack paramAudioTrack);
  
  private native float native_setPlaybackParams(PlaybackParams paramPlaybackParams);
  
  private final native void native_setSurface(Surface paramSurface);
  
  private native float native_setSyncParams(SyncParams paramSyncParams);
  
  private final native void native_setup();
  
  private final native void native_updateQueuedAudioData(int paramInt, long paramLong);
  
  public final native Surface createInputSurface();
  
  public native PlaybackParams getPlaybackParams();
  
  public native SyncParams getSyncParams();
  
  public static interface OnErrorListener {
    void onError(MediaSync param1MediaSync, int param1Int1, int param1Int2);
  }
}
