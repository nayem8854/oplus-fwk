package android.media;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;

abstract class ClosedCaptionWidget extends ViewGroup implements SubtitleTrack.RenderingWidget {
  private static final CaptioningManager.CaptionStyle DEFAULT_CAPTION_STYLE = CaptioningManager.CaptionStyle.DEFAULT;
  
  protected CaptioningManager.CaptionStyle mCaptionStyle;
  
  private final CaptioningManager.CaptioningChangeListener mCaptioningListener;
  
  protected ClosedCaptionLayout mClosedCaptionLayout;
  
  private boolean mHasChangeListener;
  
  protected SubtitleTrack.RenderingWidget.OnChangedListener mListener;
  
  private final CaptioningManager mManager;
  
  public ClosedCaptionWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public ClosedCaptionWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public ClosedCaptionWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public ClosedCaptionWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mCaptioningListener = (CaptioningManager.CaptioningChangeListener)new Object(this);
    setLayerType(1, null);
    CaptioningManager captioningManager = (CaptioningManager)paramContext.getSystemService("captioning");
    this.mCaptionStyle = DEFAULT_CAPTION_STYLE.applyStyle(captioningManager.getUserStyle());
    ClosedCaptionLayout closedCaptionLayout = createCaptionLayout(paramContext);
    closedCaptionLayout.setCaptionStyle(this.mCaptionStyle);
    this.mClosedCaptionLayout.setFontScale(this.mManager.getFontScale());
    addView((View)this.mClosedCaptionLayout, -1, -1);
    requestLayout();
  }
  
  public void setOnChangedListener(SubtitleTrack.RenderingWidget.OnChangedListener paramOnChangedListener) {
    this.mListener = paramOnChangedListener;
  }
  
  public void setSize(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    int j = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    measure(i, j);
    layout(0, 0, paramInt1, paramInt2);
  }
  
  public void setVisible(boolean paramBoolean) {
    if (paramBoolean) {
      setVisibility(0);
    } else {
      setVisibility(8);
    } 
    manageChangeListener();
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
    manageChangeListener();
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
    manageChangeListener();
  }
  
  protected void onMeasure(int paramInt1, int paramInt2) {
    super.onMeasure(paramInt1, paramInt2);
    ((ViewGroup)this.mClosedCaptionLayout).measure(paramInt1, paramInt2);
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4) {
    ((ViewGroup)this.mClosedCaptionLayout).layout(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  private void manageChangeListener() {
    boolean bool;
    if (isAttachedToWindow() && getVisibility() == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    if (this.mHasChangeListener != bool) {
      this.mHasChangeListener = bool;
      if (bool) {
        this.mManager.addCaptioningChangeListener(this.mCaptioningListener);
      } else {
        this.mManager.removeCaptioningChangeListener(this.mCaptioningListener);
      } 
    } 
  }
  
  public abstract ClosedCaptionLayout createCaptionLayout(Context paramContext);
  
  class ClosedCaptionLayout {
    public abstract void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle);
    
    public abstract void setFontScale(float param1Float);
  }
}
