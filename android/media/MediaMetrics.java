package android.media;

import android.os.Bundle;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class MediaMetrics {
  public static class Name {
    public static final String AUDIO = "audio";
    
    public static final String AUDIO_BLUETOOTH = "audio.bluetooth";
    
    public static final String AUDIO_DEVICE = "audio.device";
    
    public static final String AUDIO_FOCUS = "audio.focus";
    
    public static final String AUDIO_FORCE_USE = "audio.forceUse";
    
    public static final String AUDIO_MIC = "audio.mic";
    
    public static final String AUDIO_MODE = "audio.mode";
    
    public static final String AUDIO_SERVICE = "audio.service";
    
    public static final String AUDIO_VOLUME = "audio.volume";
    
    public static final String AUDIO_VOLUME_EVENT = "audio.volume.event";
  }
  
  public static class Value {
    public static final String CONNECT = "connect";
    
    public static final String CONNECTED = "connected";
    
    public static final String DISCONNECT = "disconnect";
    
    public static final String DISCONNECTED = "disconnected";
    
    public static final String DOWN = "down";
    
    public static final String MUTE = "mute";
    
    public static final String NO = "no";
    
    public static final String OFF = "off";
    
    public static final String ON = "on";
    
    public static final String UNMUTE = "unmute";
    
    public static final String UP = "up";
    
    public static final String YES = "yes";
  }
  
  public static class Property {
    public static final MediaMetrics.Key<String> ADDRESS = MediaMetrics.createKey("address", String.class);
    
    public static final MediaMetrics.Key<String> ATTRIBUTES = MediaMetrics.createKey("attributes", String.class);
    
    public static final MediaMetrics.Key<String> CALLING_PACKAGE = MediaMetrics.createKey("callingPackage", String.class);
    
    public static final MediaMetrics.Key<String> CLIENT_NAME = MediaMetrics.createKey("clientName", String.class);
    
    public static final MediaMetrics.Key<Integer> DELAY_MS = MediaMetrics.createKey("delayMs", Integer.class);
    
    public static final MediaMetrics.Key<String> DEVICE = MediaMetrics.createKey("device", String.class);
    
    public static final MediaMetrics.Key<String> DIRECTION = MediaMetrics.createKey("direction", String.class);
    
    public static final MediaMetrics.Key<String> EARLY_RETURN = MediaMetrics.createKey("earlyReturn", String.class);
    
    public static final MediaMetrics.Key<String> ENCODING = MediaMetrics.createKey("encoding", String.class);
    
    public static final MediaMetrics.Key<String> EVENT = MediaMetrics.createKey("event#", String.class);
    
    public static final MediaMetrics.Key<String> EXTERNAL = MediaMetrics.createKey("external", String.class);
    
    public static final MediaMetrics.Key<Integer> FLAGS = MediaMetrics.createKey("flags", Integer.class);
    
    public static final MediaMetrics.Key<String> FOCUS_CHANGE_HINT = MediaMetrics.createKey("focusChangeHint", String.class);
    
    public static final MediaMetrics.Key<String> FORCE_USE_DUE_TO = MediaMetrics.createKey("forceUseDueTo", String.class);
    
    public static final MediaMetrics.Key<String> FORCE_USE_MODE = MediaMetrics.createKey("forceUseMode", String.class);
    
    public static final MediaMetrics.Key<Double> GAIN_DB = MediaMetrics.createKey("gainDb", Double.class);
    
    public static final MediaMetrics.Key<String> GROUP = MediaMetrics.createKey("group", String.class);
    
    public static final MediaMetrics.Key<Integer> INDEX = MediaMetrics.createKey("index", Integer.class);
    
    public static final MediaMetrics.Key<Integer> MAX_INDEX = MediaMetrics.createKey("maxIndex", Integer.class);
    
    public static final MediaMetrics.Key<Integer> MIN_INDEX = MediaMetrics.createKey("minIndex", Integer.class);
    
    public static final MediaMetrics.Key<String> MODE = MediaMetrics.createKey("mode", String.class);
    
    public static final MediaMetrics.Key<String> MUTE = MediaMetrics.createKey("mute", String.class);
    
    public static final MediaMetrics.Key<String> NAME = MediaMetrics.createKey("name", String.class);
    
    public static final MediaMetrics.Key<Integer> OBSERVERS = MediaMetrics.createKey("observers", Integer.class);
    
    public static final MediaMetrics.Key<String> REQUEST = MediaMetrics.createKey("request", String.class);
    
    public static final MediaMetrics.Key<String> REQUESTED_MODE = MediaMetrics.createKey("requestedMode", String.class);
    
    public static final MediaMetrics.Key<String> SCO_AUDIO_MODE = MediaMetrics.createKey("scoAudioMode", String.class);
    
    public static final MediaMetrics.Key<Integer> SDK = MediaMetrics.createKey("sdk", Integer.class);
    
    public static final MediaMetrics.Key<String> STATE = MediaMetrics.createKey("state", String.class);
    
    public static final MediaMetrics.Key<Integer> STATUS = MediaMetrics.createKey("status", Integer.class);
    
    public static final MediaMetrics.Key<String> STREAM_TYPE = MediaMetrics.createKey("streamType", String.class);
  }
  
  private static final Charset MEDIAMETRICS_CHARSET = StandardCharsets.UTF_8;
  
  public static final String SEPARATOR = ".";
  
  public static final String TAG = "MediaMetrics";
  
  private static final int TYPE_CSTRING = 4;
  
  private static final int TYPE_DOUBLE = 3;
  
  private static final int TYPE_INT32 = 1;
  
  private static final int TYPE_INT64 = 2;
  
  private static final int TYPE_NONE = 0;
  
  private static final int TYPE_RATE = 5;
  
  public static <T> Key<T> createKey(String paramString, Class<T> paramClass) {
    return (Key<T>)new Object(paramString, paramClass);
  }
  
  private static native int native_submit_bytebuffer(ByteBuffer paramByteBuffer, int paramInt);
  
  public static class Item {
    public static final String BUNDLE_HEADER_SIZE = "_headerSize";
    
    public static final String BUNDLE_KEY = "_key";
    
    public static final String BUNDLE_KEY_SIZE = "_keySize";
    
    public static final String BUNDLE_PID = "_pid";
    
    public static final String BUNDLE_PROPERTY_COUNT = "_propertyCount";
    
    public static final String BUNDLE_TIMESTAMP = "_timestamp";
    
    public static final String BUNDLE_TOTAL_SIZE = "_totalSize";
    
    public static final String BUNDLE_UID = "_uid";
    
    public static final String BUNDLE_VERSION = "_version";
    
    private static final int FORMAT_VERSION = 0;
    
    private static final int HEADER_SIZE_OFFSET = 4;
    
    private static final int MINIMUM_PAYLOAD_SIZE = 4;
    
    private static final int TOTAL_SIZE_OFFSET = 0;
    
    private ByteBuffer mBuffer;
    
    private final int mHeaderSize;
    
    private final String mKey;
    
    private final int mPidOffset;
    
    public Item(String param1String) {
      this(param1String, -1, -1, 0L, 2048);
    }
    
    public Item(String param1String, int param1Int1, int param1Int2, long param1Long, int param1Int3) {
      byte[] arrayOfByte = param1String.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      int i = arrayOfByte.length;
      if (i <= 65534) {
        int j = i + 12 + 1 + 4 + 4 + 8;
        this.mPidOffset = j - 16;
        this.mUidOffset = j - 12;
        this.mTimeNsOffset = j - 8;
        this.mPropertyCountOffset = j;
        this.mPropertyStartOffset = j + 4;
        this.mKey = param1String;
        param1Int3 = Math.max(param1Int3, j + 4);
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(param1Int3);
        byteBuffer = byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer = byteBuffer.putInt(0);
        param1Int3 = this.mHeaderSize;
        byteBuffer = byteBuffer.putInt(param1Int3);
        byteBuffer = byteBuffer.putChar(false);
        char c = (char)(i + 1);
        byteBuffer = byteBuffer.putChar(c);
        byteBuffer = byteBuffer.put(arrayOfByte).put((byte)0);
        byteBuffer = byteBuffer.putInt(param1Int1);
        byteBuffer = byteBuffer.putInt(param1Int2);
        byteBuffer.putLong(param1Long);
        if (this.mHeaderSize == this.mBuffer.position()) {
          this.mBuffer.putInt(0);
          return;
        } 
        throw new IllegalStateException("Mismatched sizing");
      } 
      throw new IllegalArgumentException("Key length too large");
    }
    
    public <T> Item set(MediaMetrics.Key<T> param1Key, T param1T) {
      if (param1T instanceof Integer) {
        putInt(param1Key.getName(), ((Integer)param1T).intValue());
      } else if (param1T instanceof Long) {
        putLong(param1Key.getName(), ((Long)param1T).longValue());
      } else if (param1T instanceof Double) {
        putDouble(param1Key.getName(), ((Double)param1T).doubleValue());
      } else if (param1T instanceof String) {
        putString(param1Key.getName(), (String)param1T);
      } 
      return this;
    }
    
    public Item putInt(String param1String, int param1Int) {
      byte[] arrayOfByte = param1String.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      char c = (char)reserveProperty(arrayOfByte, 4);
      int i = this.mBuffer.position() + c;
      ByteBuffer byteBuffer2 = this.mBuffer.putChar(c);
      byteBuffer2 = byteBuffer2.put((byte)1);
      ByteBuffer byteBuffer1 = byteBuffer2.put(arrayOfByte).put((byte)0);
      byteBuffer1.putInt(param1Int);
      this.mPropertyCount++;
      if (this.mBuffer.position() == i)
        return this; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Final position ");
      stringBuilder.append(this.mBuffer.position());
      stringBuilder.append(" != estimatedFinalPosition ");
      stringBuilder.append(i);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Item putLong(String param1String, long param1Long) {
      byte[] arrayOfByte = param1String.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      char c = (char)reserveProperty(arrayOfByte, 8);
      int i = this.mBuffer.position() + c;
      ByteBuffer byteBuffer2 = this.mBuffer.putChar(c);
      byteBuffer2 = byteBuffer2.put((byte)2);
      ByteBuffer byteBuffer1 = byteBuffer2.put(arrayOfByte).put((byte)0);
      byteBuffer1.putLong(param1Long);
      this.mPropertyCount++;
      if (this.mBuffer.position() == i)
        return this; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Final position ");
      stringBuilder.append(this.mBuffer.position());
      stringBuilder.append(" != estimatedFinalPosition ");
      stringBuilder.append(i);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Item putDouble(String param1String, double param1Double) {
      byte[] arrayOfByte = param1String.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      char c = (char)reserveProperty(arrayOfByte, 8);
      int i = this.mBuffer.position() + c;
      ByteBuffer byteBuffer2 = this.mBuffer.putChar(c);
      byteBuffer2 = byteBuffer2.put((byte)3);
      ByteBuffer byteBuffer1 = byteBuffer2.put(arrayOfByte).put((byte)0);
      byteBuffer1.putDouble(param1Double);
      this.mPropertyCount++;
      if (this.mBuffer.position() == i)
        return this; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Final position ");
      stringBuilder.append(this.mBuffer.position());
      stringBuilder.append(" != estimatedFinalPosition ");
      stringBuilder.append(i);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Item putString(String param1String1, String param1String2) {
      byte[] arrayOfByte2 = param1String1.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      byte[] arrayOfByte1 = param1String2.getBytes(MediaMetrics.MEDIAMETRICS_CHARSET);
      char c = (char)reserveProperty(arrayOfByte2, arrayOfByte1.length + 1);
      int i = this.mBuffer.position() + c;
      ByteBuffer byteBuffer = this.mBuffer.putChar(c);
      byteBuffer = byteBuffer.put((byte)4);
      byteBuffer = byteBuffer.put(arrayOfByte2).put((byte)0);
      byteBuffer.put(arrayOfByte1).put((byte)0);
      this.mPropertyCount++;
      if (this.mBuffer.position() == i)
        return this; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Final position ");
      stringBuilder.append(this.mBuffer.position());
      stringBuilder.append(" != estimatedFinalPosition ");
      stringBuilder.append(i);
      throw new IllegalStateException(stringBuilder.toString());
    }
    
    public Item setPid(int param1Int) {
      this.mBuffer.putInt(this.mPidOffset, param1Int);
      return this;
    }
    
    public Item setUid(int param1Int) {
      this.mBuffer.putInt(this.mUidOffset, param1Int);
      return this;
    }
    
    public Item setTimestamp(long param1Long) {
      this.mBuffer.putLong(this.mTimeNsOffset, param1Long);
      return this;
    }
    
    public Item clear() {
      this.mBuffer.position(this.mPropertyStartOffset);
      ByteBuffer byteBuffer = this.mBuffer;
      byteBuffer.limit(byteBuffer.capacity());
      this.mBuffer.putLong(this.mTimeNsOffset, 0L);
      this.mPropertyCount = 0;
      return this;
    }
    
    public boolean record() {
      boolean bool;
      updateHeader();
      ByteBuffer byteBuffer = this.mBuffer;
      if (MediaMetrics.native_submit_bytebuffer(byteBuffer, byteBuffer.limit()) >= 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public Bundle toBundle() {
      updateHeader();
      ByteBuffer byteBuffer1 = this.mBuffer.duplicate();
      ByteBuffer byteBuffer2 = byteBuffer1.order(ByteOrder.nativeOrder());
      byteBuffer2.flip();
      return toBundle(byteBuffer1);
    }
    
    public static Bundle toBundle(ByteBuffer param1ByteBuffer) {
      Bundle bundle = new Bundle();
      int i = param1ByteBuffer.getInt();
      int j = param1ByteBuffer.getInt();
      char c1 = param1ByteBuffer.getChar();
      char c2 = param1ByteBuffer.getChar();
      if (i >= 0 && j >= 0) {
        if (c2 > '\000') {
          StringBuilder stringBuilder;
          String str = getStringFromBuffer(param1ByteBuffer, c2);
          int k = param1ByteBuffer.getInt();
          int m = param1ByteBuffer.getInt();
          long l = param1ByteBuffer.getLong();
          int n = param1ByteBuffer.position();
          if (c1 == '\000') {
            if (n != j) {
              stringBuilder = new StringBuilder();
              stringBuilder.append("Item key:");
              stringBuilder.append(str);
              stringBuilder.append(" headerRead:");
              stringBuilder.append(n);
              stringBuilder.append(" != headerSize:");
              stringBuilder.append(j);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
          } else if (n <= j) {
            if (n < j)
              stringBuilder.position(j); 
          } else {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Item key:");
            stringBuilder.append(str);
            stringBuilder.append(" headerRead:");
            stringBuilder.append(n);
            stringBuilder.append(" > headerSize:");
            stringBuilder.append(j);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          int i1 = stringBuilder.getInt();
          if (i1 >= 0) {
            bundle.putInt("_totalSize", i);
            bundle.putInt("_headerSize", j);
            bundle.putChar("_version", c1);
            bundle.putChar("_keySize", c2);
            bundle.putString("_key", str);
            bundle.putInt("_pid", k);
            bundle.putInt("_uid", m);
            bundle.putLong("_timestamp", l);
            bundle.putInt("_propertyCount", i1);
            char c;
            for (j = 0, c = c2, n = c1; j < i1; ) {
              int i2 = stringBuilder.position();
              c1 = stringBuilder.getChar();
              byte b = stringBuilder.get();
              str = getStringFromBuffer((ByteBuffer)stringBuilder);
              if (b != 0)
                if (b != 1) {
                  if (b != 2) {
                    if (b != 3) {
                      if (b != 4) {
                        if (b != 5) {
                          if (n != 0) {
                            stringBuilder.position(i2 + c1);
                          } else {
                            stringBuilder = new StringBuilder();
                            stringBuilder.append("Property ");
                            stringBuilder.append(str);
                            stringBuilder.append(" has unsupported type ");
                            stringBuilder.append(b);
                            throw new IllegalArgumentException(stringBuilder.toString());
                          } 
                        } else {
                          stringBuilder.getLong();
                          stringBuilder.getLong();
                        } 
                      } else {
                        bundle.putString(str, getStringFromBuffer((ByteBuffer)stringBuilder));
                      } 
                    } else {
                      bundle.putDouble(str, stringBuilder.getDouble());
                    } 
                  } else {
                    bundle.putLong(str, stringBuilder.getLong());
                  } 
                } else {
                  bundle.putInt(str, stringBuilder.getInt());
                }  
              int i3 = stringBuilder.position() - i2;
              if (i3 == c1) {
                j++;
                continue;
              } 
              stringBuilder = new StringBuilder();
              stringBuilder.append("propSize:");
              stringBuilder.append(c1);
              stringBuilder.append(" != deltaPosition:");
              stringBuilder.append(i3);
              throw new IllegalArgumentException(stringBuilder.toString());
            } 
            m = stringBuilder.position();
            if (m == i)
              return bundle; 
            stringBuilder = new StringBuilder();
            stringBuilder.append("totalSize:");
            stringBuilder.append(i);
            stringBuilder.append(" != finalPosition:");
            stringBuilder.append(m);
            throw new IllegalArgumentException(stringBuilder.toString());
          } 
          throw new IllegalArgumentException("Cannot have more than 2147483647 properties");
        } 
        throw new IllegalArgumentException("Illegal null key");
      } 
      throw new IllegalArgumentException("Item size cannot be > 2147483647");
    }
    
    private int mPropertyCount = 0;
    
    private final int mPropertyCountOffset;
    
    private final int mPropertyStartOffset;
    
    private final int mTimeNsOffset;
    
    private final int mUidOffset;
    
    private int reserveProperty(byte[] param1ArrayOfbyte, int param1Int) {
      StringBuilder stringBuilder1;
      int i = param1ArrayOfbyte.length;
      if (i <= 65535) {
        if (param1Int <= 65535) {
          StringBuilder stringBuilder3;
          i = i + 3 + 1 + param1Int;
          if (i <= 65535) {
            if (this.mBuffer.remaining() < i) {
              param1Int = this.mBuffer.position() + i;
              if (param1Int <= 1073741823) {
                ByteBuffer byteBuffer = ByteBuffer.allocateDirect(param1Int << 1);
                byteBuffer.order(ByteOrder.nativeOrder());
                this.mBuffer.flip();
                byteBuffer.put(this.mBuffer);
                this.mBuffer = byteBuffer;
              } else {
                stringBuilder3 = new StringBuilder();
                stringBuilder3.append("Item memory requirements too large: ");
                stringBuilder3.append(param1Int);
                throw new IllegalStateException(stringBuilder3.toString());
              } 
            } 
            return i;
          } 
          StringBuilder stringBuilder4 = new StringBuilder();
          stringBuilder4.append("Item property ");
          stringBuilder4.append(new String((byte[])stringBuilder3, MediaMetrics.MEDIAMETRICS_CHARSET));
          stringBuilder4.append(" is too large to send");
          throw new IllegalStateException(stringBuilder4.toString());
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("payload too large ");
        stringBuilder1.append(param1Int);
        throw new IllegalStateException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("property key too long ");
      stringBuilder2.append(new String((byte[])stringBuilder1, MediaMetrics.MEDIAMETRICS_CHARSET));
      throw new IllegalStateException(stringBuilder2.toString());
    }
    
    private static String getStringFromBuffer(ByteBuffer param1ByteBuffer) {
      return getStringFromBuffer(param1ByteBuffer, 2147483647);
    }
    
    private static String getStringFromBuffer(ByteBuffer param1ByteBuffer, int param1Int) {
      String str;
      int i = param1ByteBuffer.position();
      int j = param1ByteBuffer.limit();
      int k = i, m = j;
      if (param1Int < Integer.MAX_VALUE - i) {
        k = i;
        m = j;
        if (i + param1Int < j) {
          m = i + param1Int;
          k = i;
        } 
      } 
      for (; k < m; k++) {
        if (param1ByteBuffer.get(k) == 0) {
          m = k + 1;
          if (param1Int == Integer.MAX_VALUE || m - param1ByteBuffer.position() == param1Int) {
            if (param1ByteBuffer.hasArray()) {
              byte[] arrayOfByte = param1ByteBuffer.array();
              param1Int = param1ByteBuffer.position();
              i = param1ByteBuffer.arrayOffset();
              String str1 = new String(arrayOfByte, param1Int + i, k - param1ByteBuffer.position(), MediaMetrics.MEDIAMETRICS_CHARSET);
              param1ByteBuffer.position(m);
              str = str1;
            } else {
              byte[] arrayOfByte = new byte[k - str.position()];
              str.get(arrayOfByte);
              String str1 = new String(arrayOfByte, MediaMetrics.MEDIAMETRICS_CHARSET);
              str.get();
              str = str1;
            } 
            return str;
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("chars consumed at ");
          stringBuilder1.append(k);
          stringBuilder1.append(": ");
          stringBuilder1.append(m - str.position());
          stringBuilder1.append(" != size: ");
          stringBuilder1.append(param1Int);
          throw new IllegalArgumentException(stringBuilder1.toString());
        } 
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("No zero termination found in string position: ");
      stringBuilder.append(str.position());
      stringBuilder.append(" end: ");
      stringBuilder.append(k);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    private void updateHeader() {
      ByteBuffer byteBuffer = this.mBuffer;
      byteBuffer = byteBuffer.putInt(0, byteBuffer.position());
      int i = this.mPropertyCountOffset;
      char c = (char)this.mPropertyCount;
      byteBuffer.putInt(i, c);
    }
  }
  
  public static interface Key<T> {
    String getName();
    
    Class<T> getValueClass();
  }
}
