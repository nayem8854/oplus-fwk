package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaHTTPConnection extends IInterface {
  IBinder connect(String paramString1, String paramString2) throws RemoteException;
  
  void disconnect() throws RemoteException;
  
  String getMIMEType() throws RemoteException;
  
  long getSize() throws RemoteException;
  
  String getUri() throws RemoteException;
  
  int readAt(long paramLong, int paramInt) throws RemoteException;
  
  class Default implements IMediaHTTPConnection {
    public IBinder connect(String param1String1, String param1String2) throws RemoteException {
      return null;
    }
    
    public void disconnect() throws RemoteException {}
    
    public int readAt(long param1Long, int param1Int) throws RemoteException {
      return 0;
    }
    
    public long getSize() throws RemoteException {
      return 0L;
    }
    
    public String getMIMEType() throws RemoteException {
      return null;
    }
    
    public String getUri() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaHTTPConnection {
    private static final String DESCRIPTOR = "android.media.IMediaHTTPConnection";
    
    static final int TRANSACTION_connect = 1;
    
    static final int TRANSACTION_disconnect = 2;
    
    static final int TRANSACTION_getMIMEType = 5;
    
    static final int TRANSACTION_getSize = 4;
    
    static final int TRANSACTION_getUri = 6;
    
    static final int TRANSACTION_readAt = 3;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaHTTPConnection");
    }
    
    public static IMediaHTTPConnection asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaHTTPConnection");
      if (iInterface != null && iInterface instanceof IMediaHTTPConnection)
        return (IMediaHTTPConnection)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "getUri";
        case 5:
          return "getMIMEType";
        case 4:
          return "getSize";
        case 3:
          return "readAt";
        case 2:
          return "disconnect";
        case 1:
          break;
      } 
      return "connect";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1598968902) {
        long l;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.media.IMediaHTTPConnection");
            str1 = getUri();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 5:
            str1.enforceInterface("android.media.IMediaHTTPConnection");
            str1 = getMIMEType();
            param1Parcel2.writeNoException();
            param1Parcel2.writeString(str1);
            return true;
          case 4:
            str1.enforceInterface("android.media.IMediaHTTPConnection");
            l = getSize();
            param1Parcel2.writeNoException();
            param1Parcel2.writeLong(l);
            return true;
          case 3:
            str1.enforceInterface("android.media.IMediaHTTPConnection");
            l = str1.readLong();
            param1Int1 = str1.readInt();
            param1Int1 = readAt(l, param1Int1);
            param1Parcel2.writeNoException();
            param1Parcel2.writeInt(param1Int1);
            return true;
          case 2:
            str1.enforceInterface("android.media.IMediaHTTPConnection");
            disconnect();
            param1Parcel2.writeNoException();
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.media.IMediaHTTPConnection");
        String str2 = str1.readString();
        String str1 = str1.readString();
        IBinder iBinder = connect(str2, str1);
        param1Parcel2.writeNoException();
        param1Parcel2.writeStrongBinder(iBinder);
        return true;
      } 
      param1Parcel2.writeString("android.media.IMediaHTTPConnection");
      return true;
    }
    
    private static class Proxy implements IMediaHTTPConnection {
      public static IMediaHTTPConnection sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaHTTPConnection";
      }
      
      public IBinder connect(String param2String1, String param2String2) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          parcel1.writeString(param2String1);
          parcel1.writeString(param2String2);
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null)
            return IMediaHTTPConnection.Stub.getDefaultImpl().connect(param2String1, param2String2); 
          parcel2.readException();
          return parcel2.readStrongBinder();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public void disconnect() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          boolean bool = this.mRemote.transact(2, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null) {
            IMediaHTTPConnection.Stub.getDefaultImpl().disconnect();
            return;
          } 
          parcel2.readException();
          return;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public int readAt(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          parcel1.writeLong(param2Long);
          parcel1.writeInt(param2Int);
          boolean bool = this.mRemote.transact(3, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null) {
            param2Int = IMediaHTTPConnection.Stub.getDefaultImpl().readAt(param2Long, param2Int);
            return param2Int;
          } 
          parcel2.readException();
          param2Int = parcel2.readInt();
          return param2Int;
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public long getSize() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          boolean bool = this.mRemote.transact(4, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null)
            return IMediaHTTPConnection.Stub.getDefaultImpl().getSize(); 
          parcel2.readException();
          return parcel2.readLong();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getMIMEType() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          boolean bool = this.mRemote.transact(5, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null)
            return IMediaHTTPConnection.Stub.getDefaultImpl().getMIMEType(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
      
      public String getUri() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPConnection");
          boolean bool = this.mRemote.transact(6, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPConnection.Stub.getDefaultImpl() != null)
            return IMediaHTTPConnection.Stub.getDefaultImpl().getUri(); 
          parcel2.readException();
          return parcel2.readString();
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaHTTPConnection param1IMediaHTTPConnection) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaHTTPConnection != null) {
          Proxy.sDefaultImpl = param1IMediaHTTPConnection;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaHTTPConnection getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
