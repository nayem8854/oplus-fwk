package android.media;

import android.net.NetworkUtils;
import android.os.IBinder;
import android.util.Log;
import com.oplus.media.OplusMediaHTTPConnection;
import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class MediaHTTPConnection extends IMediaHTTPConnection.Stub {
  public long mCurrentOffset = -1L;
  
  private URL mURL = null;
  
  private Map<String, String> mHeaders = null;
  
  private volatile HttpURLConnection mConnection = null;
  
  private long mTotalSize = -1L;
  
  public InputStream mInputStream = null;
  
  private boolean mAllowCrossDomainRedirect = true;
  
  private boolean mAllowCrossProtocolRedirect = true;
  
  public OplusMediaHTTPConnection mOplusMediaHTTPConnection = new OplusMediaHTTPConnection(this);
  
  private final AtomicInteger mNumDisconnectingThreads = new AtomicInteger(0);
  
  private static final int CONNECT_TIMEOUT_MS = 30000;
  
  private static final int HTTP_TEMP_REDIRECT = 307;
  
  private static final int MAX_REDIRECTS = 20;
  
  private static final String TAG = "MediaHTTPConnection";
  
  private static final boolean VERBOSE = false;
  
  private long mNativeContext;
  
  public MediaHTTPConnection() {
    CookieHandler cookieHandler = CookieHandler.getDefault();
    if (cookieHandler == null)
      Log.w("MediaHTTPConnection", "MediaHTTPConnection: Unexpected. No CookieHandler found."); 
    native_setup();
  }
  
  public IBinder connect(String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokevirtual disconnect : ()V
    //   6: aload_0
    //   7: iconst_1
    //   8: putfield mAllowCrossDomainRedirect : Z
    //   11: new java/net/URL
    //   14: astore_3
    //   15: aload_3
    //   16: aload_1
    //   17: invokespecial <init> : (Ljava/lang/String;)V
    //   20: aload_0
    //   21: aload_3
    //   22: putfield mURL : Ljava/net/URL;
    //   25: aload_0
    //   26: aload_0
    //   27: aload_2
    //   28: invokespecial convertHeaderStringToMap : (Ljava/lang/String;)Ljava/util/Map;
    //   31: putfield mHeaders : Ljava/util/Map;
    //   34: aload_0
    //   35: invokespecial native_getIMemory : ()Landroid/os/IBinder;
    //   38: astore_1
    //   39: aload_0
    //   40: monitorexit
    //   41: aload_1
    //   42: areturn
    //   43: astore_1
    //   44: goto -> 53
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    //   52: astore_1
    //   53: aload_0
    //   54: monitorexit
    //   55: aconst_null
    //   56: areturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #131	-> 2
    //   #132	-> 6
    //   #133	-> 11
    //   #134	-> 25
    //   #137	-> 34
    //   #139	-> 34
    //   #135	-> 43
    //   #130	-> 47
    //   #135	-> 52
    //   #136	-> 53
    // Exception table:
    //   from	to	target	type
    //   2	6	52	java/net/MalformedURLException
    //   2	6	47	finally
    //   6	11	43	java/net/MalformedURLException
    //   6	11	47	finally
    //   11	25	43	java/net/MalformedURLException
    //   11	25	47	finally
    //   25	34	43	java/net/MalformedURLException
    //   25	34	47	finally
    //   34	39	47	finally
  }
  
  private static boolean parseBoolean(String paramString) {
    boolean bool1 = true, bool2 = true;
    try {
      long l = Long.parseLong(paramString);
      if (l != 0L) {
        bool1 = bool2;
      } else {
        bool1 = false;
      } 
      return bool1;
    } catch (NumberFormatException numberFormatException) {
      if (!"true".equalsIgnoreCase(paramString) && 
        !"yes".equalsIgnoreCase(paramString))
        bool1 = false; 
      return bool1;
    } 
  }
  
  private boolean filterOutInternalHeaders(String paramString1, String paramString2) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc 'android-allow-cross-domain-redirect'
    //   4: aload_1
    //   5: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
    //   8: ifeq -> 30
    //   11: aload_2
    //   12: invokestatic parseBoolean : (Ljava/lang/String;)Z
    //   15: istore_3
    //   16: aload_0
    //   17: iload_3
    //   18: putfield mAllowCrossDomainRedirect : Z
    //   21: aload_0
    //   22: iload_3
    //   23: putfield mAllowCrossProtocolRedirect : Z
    //   26: aload_0
    //   27: monitorexit
    //   28: iconst_1
    //   29: ireturn
    //   30: aload_0
    //   31: monitorexit
    //   32: iconst_0
    //   33: ireturn
    //   34: astore_1
    //   35: aload_0
    //   36: monitorexit
    //   37: aload_1
    //   38: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #153	-> 2
    //   #154	-> 11
    //   #156	-> 21
    //   #160	-> 26
    //   #158	-> 30
    //   #152	-> 34
    // Exception table:
    //   from	to	target	type
    //   2	11	34	finally
    //   11	21	34	finally
    //   21	26	34	finally
  }
  
  private Map<String, String> convertHeaderStringToMap(String paramString) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: new java/util/HashMap
    //   5: astore_2
    //   6: aload_2
    //   7: invokespecial <init> : ()V
    //   10: aload_1
    //   11: ldc '\\r\\n'
    //   13: invokevirtual split : (Ljava/lang/String;)[Ljava/lang/String;
    //   16: astore_3
    //   17: aload_3
    //   18: arraylength
    //   19: istore #4
    //   21: iconst_0
    //   22: istore #5
    //   24: iload #5
    //   26: iload #4
    //   28: if_icmpge -> 95
    //   31: aload_3
    //   32: iload #5
    //   34: aaload
    //   35: astore #6
    //   37: aload #6
    //   39: ldc ':'
    //   41: invokevirtual indexOf : (Ljava/lang/String;)I
    //   44: istore #7
    //   46: iload #7
    //   48: iflt -> 89
    //   51: aload #6
    //   53: iconst_0
    //   54: iload #7
    //   56: invokevirtual substring : (II)Ljava/lang/String;
    //   59: astore_1
    //   60: aload #6
    //   62: iload #7
    //   64: iconst_1
    //   65: iadd
    //   66: invokevirtual substring : (I)Ljava/lang/String;
    //   69: astore #6
    //   71: aload_0
    //   72: aload_1
    //   73: aload #6
    //   75: invokespecial filterOutInternalHeaders : (Ljava/lang/String;Ljava/lang/String;)Z
    //   78: ifne -> 89
    //   81: aload_2
    //   82: aload_1
    //   83: aload #6
    //   85: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   88: pop
    //   89: iinc #5, 1
    //   92: goto -> 24
    //   95: aload_0
    //   96: monitorexit
    //   97: aload_2
    //   98: areturn
    //   99: astore_1
    //   100: aload_0
    //   101: monitorexit
    //   102: aload_1
    //   103: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #164	-> 2
    //   #166	-> 10
    //   #167	-> 17
    //   #168	-> 37
    //   #169	-> 46
    //   #170	-> 51
    //   #171	-> 60
    //   #173	-> 71
    //   #174	-> 81
    //   #167	-> 89
    //   #179	-> 95
    //   #163	-> 99
    // Exception table:
    //   from	to	target	type
    //   2	10	99	finally
    //   10	17	99	finally
    //   17	21	99	finally
    //   37	46	99	finally
    //   51	60	99	finally
    //   60	71	99	finally
    //   71	81	99	finally
    //   81	89	99	finally
  }
  
  public void disconnect() {
    // Byte code:
    //   0: aload_0
    //   1: getfield mNumDisconnectingThreads : Ljava/util/concurrent/atomic/AtomicInteger;
    //   4: invokevirtual incrementAndGet : ()I
    //   7: pop
    //   8: aload_0
    //   9: getfield mConnection : Ljava/net/HttpURLConnection;
    //   12: astore_1
    //   13: aload_1
    //   14: ifnull -> 21
    //   17: aload_1
    //   18: invokevirtual disconnect : ()V
    //   21: aload_0
    //   22: monitorenter
    //   23: aload_0
    //   24: invokespecial teardownConnection : ()V
    //   27: aload_0
    //   28: aconst_null
    //   29: putfield mHeaders : Ljava/util/Map;
    //   32: aload_0
    //   33: aconst_null
    //   34: putfield mURL : Ljava/net/URL;
    //   37: aload_0
    //   38: monitorexit
    //   39: aload_0
    //   40: getfield mNumDisconnectingThreads : Ljava/util/concurrent/atomic/AtomicInteger;
    //   43: invokevirtual decrementAndGet : ()I
    //   46: pop
    //   47: return
    //   48: astore_1
    //   49: aload_0
    //   50: monitorexit
    //   51: aload_1
    //   52: athrow
    //   53: astore_1
    //   54: aload_0
    //   55: getfield mNumDisconnectingThreads : Ljava/util/concurrent/atomic/AtomicInteger;
    //   58: invokevirtual decrementAndGet : ()I
    //   61: pop
    //   62: aload_1
    //   63: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #185	-> 0
    //   #187	-> 8
    //   #190	-> 13
    //   #191	-> 17
    //   #193	-> 21
    //   #197	-> 23
    //   #198	-> 27
    //   #199	-> 32
    //   #200	-> 37
    //   #202	-> 39
    //   #203	-> 47
    //   #204	-> 47
    //   #200	-> 48
    //   #202	-> 53
    //   #203	-> 62
    // Exception table:
    //   from	to	target	type
    //   8	13	53	finally
    //   17	21	53	finally
    //   21	23	53	finally
    //   23	27	48	finally
    //   27	32	48	finally
    //   32	37	48	finally
    //   37	39	48	finally
    //   49	51	48	finally
    //   51	53	53	finally
  }
  
  private void teardownConnection() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mConnection : Ljava/net/HttpURLConnection;
    //   6: ifnull -> 89
    //   9: aload_0
    //   10: getfield mInputStream : Ljava/io/InputStream;
    //   13: astore_1
    //   14: aload_1
    //   15: ifnull -> 70
    //   18: aload_0
    //   19: getfield mInputStream : Ljava/io/InputStream;
    //   22: invokevirtual close : ()V
    //   25: goto -> 65
    //   28: astore_2
    //   29: new java/lang/StringBuilder
    //   32: astore_1
    //   33: aload_1
    //   34: invokespecial <init> : ()V
    //   37: aload_1
    //   38: ldc_w 'teardown connection unknown exception '
    //   41: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   44: pop
    //   45: aload_1
    //   46: aload_2
    //   47: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: ldc 'MediaHTTPConnection'
    //   53: aload_1
    //   54: invokevirtual toString : ()Ljava/lang/String;
    //   57: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   60: pop
    //   61: goto -> 65
    //   64: astore_1
    //   65: aload_0
    //   66: aconst_null
    //   67: putfield mInputStream : Ljava/io/InputStream;
    //   70: aload_0
    //   71: getfield mConnection : Ljava/net/HttpURLConnection;
    //   74: invokevirtual disconnect : ()V
    //   77: aload_0
    //   78: aconst_null
    //   79: putfield mConnection : Ljava/net/HttpURLConnection;
    //   82: aload_0
    //   83: ldc2_w -1
    //   86: putfield mCurrentOffset : J
    //   89: aload_0
    //   90: monitorexit
    //   91: return
    //   92: astore_1
    //   93: aload_0
    //   94: monitorexit
    //   95: aload_1
    //   96: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #207	-> 2
    //   #208	-> 9
    //   #210	-> 18
    //   #218	-> 25
    //   #216	-> 28
    //   #217	-> 29
    //   #211	-> 64
    //   #218	-> 65
    //   #220	-> 65
    //   #223	-> 70
    //   #224	-> 77
    //   #226	-> 82
    //   #228	-> 89
    //   #206	-> 92
    // Exception table:
    //   from	to	target	type
    //   2	9	92	finally
    //   9	14	92	finally
    //   18	25	64	java/io/IOException
    //   18	25	28	java/lang/AssertionError
    //   18	25	92	finally
    //   29	61	92	finally
    //   65	70	92	finally
    //   70	77	92	finally
    //   77	82	92	finally
    //   82	89	92	finally
  }
  
  private static final boolean isLocalHost(URL paramURL) {
    if (paramURL == null)
      return false; 
    String str = paramURL.getHost();
    if (str == null)
      return false; 
    try {
      if (str.equalsIgnoreCase("localhost"))
        return true; 
      boolean bool = NetworkUtils.numericToInetAddress(str).isLoopbackAddress();
      if (bool)
        return true; 
    } catch (IllegalArgumentException illegalArgumentException) {}
    return false;
  }
  
  public void seekTo(long paramLong) throws IOException {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: invokespecial teardownConnection : ()V
    //   6: aload_0
    //   7: getfield mURL : Ljava/net/URL;
    //   10: astore_3
    //   11: aload_3
    //   12: invokestatic isLocalHost : (Ljava/net/URL;)Z
    //   15: istore #4
    //   17: iconst_0
    //   18: istore #5
    //   20: aload_0
    //   21: getfield mNumDisconnectingThreads : Ljava/util/concurrent/atomic/AtomicInteger;
    //   24: invokevirtual get : ()I
    //   27: ifgt -> 765
    //   30: iload #4
    //   32: ifeq -> 52
    //   35: aload_0
    //   36: aload_3
    //   37: getstatic java/net/Proxy.NO_PROXY : Ljava/net/Proxy;
    //   40: invokevirtual openConnection : (Ljava/net/Proxy;)Ljava/net/URLConnection;
    //   43: checkcast java/net/HttpURLConnection
    //   46: putfield mConnection : Ljava/net/HttpURLConnection;
    //   49: goto -> 63
    //   52: aload_0
    //   53: aload_3
    //   54: invokevirtual openConnection : ()Ljava/net/URLConnection;
    //   57: checkcast java/net/HttpURLConnection
    //   60: putfield mConnection : Ljava/net/HttpURLConnection;
    //   63: aload_0
    //   64: getfield mNumDisconnectingThreads : Ljava/util/concurrent/atomic/AtomicInteger;
    //   67: invokevirtual get : ()I
    //   70: ifgt -> 752
    //   73: aload_0
    //   74: getfield mConnection : Ljava/net/HttpURLConnection;
    //   77: sipush #30000
    //   80: invokevirtual setConnectTimeout : (I)V
    //   83: aload_0
    //   84: getfield mConnection : Ljava/net/HttpURLConnection;
    //   87: aload_0
    //   88: getfield mAllowCrossDomainRedirect : Z
    //   91: invokevirtual setInstanceFollowRedirects : (Z)V
    //   94: aload_0
    //   95: getfield mHeaders : Ljava/util/Map;
    //   98: ifnull -> 178
    //   101: aload_0
    //   102: getfield mHeaders : Ljava/util/Map;
    //   105: invokeinterface entrySet : ()Ljava/util/Set;
    //   110: invokeinterface iterator : ()Ljava/util/Iterator;
    //   115: astore_3
    //   116: aload_3
    //   117: invokeinterface hasNext : ()Z
    //   122: ifeq -> 178
    //   125: aload_3
    //   126: invokeinterface next : ()Ljava/lang/Object;
    //   131: checkcast java/util/Map$Entry
    //   134: astore #6
    //   136: aload_0
    //   137: getfield mConnection : Ljava/net/HttpURLConnection;
    //   140: astore #7
    //   142: aload #6
    //   144: invokeinterface getKey : ()Ljava/lang/Object;
    //   149: checkcast java/lang/String
    //   152: astore #8
    //   154: aload #6
    //   156: invokeinterface getValue : ()Ljava/lang/Object;
    //   161: checkcast java/lang/String
    //   164: astore #6
    //   166: aload #7
    //   168: aload #8
    //   170: aload #6
    //   172: invokevirtual setRequestProperty : (Ljava/lang/String;Ljava/lang/String;)V
    //   175: goto -> 116
    //   178: lload_1
    //   179: lconst_0
    //   180: lcmp
    //   181: ifle -> 232
    //   184: aload_0
    //   185: getfield mConnection : Ljava/net/HttpURLConnection;
    //   188: astore #7
    //   190: new java/lang/StringBuilder
    //   193: astore_3
    //   194: aload_3
    //   195: invokespecial <init> : ()V
    //   198: aload_3
    //   199: ldc_w 'bytes='
    //   202: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: aload_3
    //   207: lload_1
    //   208: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   211: pop
    //   212: aload_3
    //   213: ldc_w '-'
    //   216: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   219: pop
    //   220: aload #7
    //   222: ldc_w 'Range'
    //   225: aload_3
    //   226: invokevirtual toString : ()Ljava/lang/String;
    //   229: invokevirtual setRequestProperty : (Ljava/lang/String;Ljava/lang/String;)V
    //   232: aload_0
    //   233: getfield mConnection : Ljava/net/HttpURLConnection;
    //   236: invokevirtual getResponseCode : ()I
    //   239: istore #9
    //   241: iload #9
    //   243: sipush #300
    //   246: if_icmpeq -> 458
    //   249: iload #9
    //   251: sipush #301
    //   254: if_icmpeq -> 458
    //   257: iload #9
    //   259: sipush #302
    //   262: if_icmpeq -> 458
    //   265: iload #9
    //   267: sipush #303
    //   270: if_icmpeq -> 458
    //   273: iload #9
    //   275: sipush #307
    //   278: if_icmpeq -> 458
    //   281: aload_0
    //   282: getfield mAllowCrossDomainRedirect : Z
    //   285: ifeq -> 299
    //   288: aload_0
    //   289: aload_0
    //   290: getfield mConnection : Ljava/net/HttpURLConnection;
    //   293: invokevirtual getURL : ()Ljava/net/URL;
    //   296: putfield mURL : Ljava/net/URL;
    //   299: iload #9
    //   301: sipush #206
    //   304: if_icmpne -> 368
    //   307: aload_0
    //   308: getfield mConnection : Ljava/net/HttpURLConnection;
    //   311: astore_3
    //   312: aload_3
    //   313: ldc_w 'Content-Range'
    //   316: invokevirtual getHeaderField : (Ljava/lang/String;)Ljava/lang/String;
    //   319: astore_3
    //   320: aload_0
    //   321: ldc2_w -1
    //   324: putfield mTotalSize : J
    //   327: aload_3
    //   328: ifnull -> 365
    //   331: aload_3
    //   332: bipush #47
    //   334: invokevirtual lastIndexOf : (I)I
    //   337: istore #5
    //   339: iload #5
    //   341: iflt -> 365
    //   344: aload_3
    //   345: iload #5
    //   347: iconst_1
    //   348: iadd
    //   349: invokevirtual substring : (I)Ljava/lang/String;
    //   352: astore_3
    //   353: aload_0
    //   354: aload_3
    //   355: invokestatic parseLong : (Ljava/lang/String;)J
    //   358: putfield mTotalSize : J
    //   361: goto -> 365
    //   364: astore_3
    //   365: goto -> 388
    //   368: iload #9
    //   370: sipush #200
    //   373: if_icmpne -> 448
    //   376: aload_0
    //   377: aload_0
    //   378: getfield mConnection : Ljava/net/HttpURLConnection;
    //   381: invokevirtual getContentLength : ()I
    //   384: i2l
    //   385: putfield mTotalSize : J
    //   388: lload_1
    //   389: lconst_0
    //   390: lcmp
    //   391: ifle -> 415
    //   394: iload #9
    //   396: sipush #206
    //   399: if_icmpne -> 405
    //   402: goto -> 415
    //   405: new java/net/ProtocolException
    //   408: astore_3
    //   409: aload_3
    //   410: invokespecial <init> : ()V
    //   413: aload_3
    //   414: athrow
    //   415: new java/io/BufferedInputStream
    //   418: astore #7
    //   420: aload_0
    //   421: getfield mConnection : Ljava/net/HttpURLConnection;
    //   424: astore_3
    //   425: aload #7
    //   427: aload_3
    //   428: invokevirtual getInputStream : ()Ljava/io/InputStream;
    //   431: invokespecial <init> : (Ljava/io/InputStream;)V
    //   434: aload_0
    //   435: aload #7
    //   437: putfield mInputStream : Ljava/io/InputStream;
    //   440: aload_0
    //   441: lload_1
    //   442: putfield mCurrentOffset : J
    //   445: aload_0
    //   446: monitorexit
    //   447: return
    //   448: new java/io/IOException
    //   451: astore_3
    //   452: aload_3
    //   453: invokespecial <init> : ()V
    //   456: aload_3
    //   457: athrow
    //   458: iinc #5, 1
    //   461: iload #5
    //   463: bipush #20
    //   465: if_icmpgt -> 710
    //   468: aload_0
    //   469: getfield mConnection : Ljava/net/HttpURLConnection;
    //   472: invokevirtual getRequestMethod : ()Ljava/lang/String;
    //   475: astore_3
    //   476: iload #9
    //   478: sipush #307
    //   481: if_icmpne -> 520
    //   484: aload_3
    //   485: ldc_w 'GET'
    //   488: invokevirtual equals : (Ljava/lang/Object;)Z
    //   491: ifne -> 520
    //   494: aload_3
    //   495: ldc_w 'HEAD'
    //   498: invokevirtual equals : (Ljava/lang/Object;)Z
    //   501: ifeq -> 507
    //   504: goto -> 520
    //   507: new java/net/NoRouteToHostException
    //   510: astore_3
    //   511: aload_3
    //   512: ldc_w 'Invalid redirect'
    //   515: invokespecial <init> : (Ljava/lang/String;)V
    //   518: aload_3
    //   519: athrow
    //   520: aload_0
    //   521: getfield mConnection : Ljava/net/HttpURLConnection;
    //   524: ldc_w 'Location'
    //   527: invokevirtual getHeaderField : (Ljava/lang/String;)Ljava/lang/String;
    //   530: astore #7
    //   532: aload #7
    //   534: ifnull -> 697
    //   537: new java/net/URL
    //   540: astore_3
    //   541: aload_3
    //   542: aload_0
    //   543: getfield mURL : Ljava/net/URL;
    //   546: aload #7
    //   548: invokespecial <init> : (Ljava/net/URL;Ljava/lang/String;)V
    //   551: aload_3
    //   552: invokevirtual getProtocol : ()Ljava/lang/String;
    //   555: ldc_w 'https'
    //   558: invokevirtual equals : (Ljava/lang/Object;)Z
    //   561: ifne -> 593
    //   564: aload_3
    //   565: invokevirtual getProtocol : ()Ljava/lang/String;
    //   568: ldc_w 'http'
    //   571: invokevirtual equals : (Ljava/lang/Object;)Z
    //   574: ifeq -> 580
    //   577: goto -> 593
    //   580: new java/net/NoRouteToHostException
    //   583: astore_3
    //   584: aload_3
    //   585: ldc_w 'Unsupported protocol redirect'
    //   588: invokespecial <init> : (Ljava/lang/String;)V
    //   591: aload_3
    //   592: athrow
    //   593: aload_0
    //   594: getfield mURL : Ljava/net/URL;
    //   597: invokevirtual getProtocol : ()Ljava/lang/String;
    //   600: aload_3
    //   601: invokevirtual getProtocol : ()Ljava/lang/String;
    //   604: invokevirtual equals : (Ljava/lang/Object;)Z
    //   607: istore #10
    //   609: aload_0
    //   610: getfield mAllowCrossProtocolRedirect : Z
    //   613: ifne -> 637
    //   616: iload #10
    //   618: ifeq -> 624
    //   621: goto -> 637
    //   624: new java/net/NoRouteToHostException
    //   627: astore_3
    //   628: aload_3
    //   629: ldc_w 'Cross-protocol redirects are disallowed'
    //   632: invokespecial <init> : (Ljava/lang/String;)V
    //   635: aload_3
    //   636: athrow
    //   637: aload_0
    //   638: getfield mURL : Ljava/net/URL;
    //   641: invokevirtual getHost : ()Ljava/lang/String;
    //   644: aload_3
    //   645: invokevirtual getHost : ()Ljava/lang/String;
    //   648: invokevirtual equals : (Ljava/lang/Object;)Z
    //   651: istore #10
    //   653: aload_0
    //   654: getfield mAllowCrossDomainRedirect : Z
    //   657: ifne -> 681
    //   660: iload #10
    //   662: ifeq -> 668
    //   665: goto -> 681
    //   668: new java/net/NoRouteToHostException
    //   671: astore_3
    //   672: aload_3
    //   673: ldc_w 'Cross-domain redirects are disallowed'
    //   676: invokespecial <init> : (Ljava/lang/String;)V
    //   679: aload_3
    //   680: athrow
    //   681: iload #9
    //   683: sipush #307
    //   686: if_icmpeq -> 694
    //   689: aload_0
    //   690: aload_3
    //   691: putfield mURL : Ljava/net/URL;
    //   694: goto -> 20
    //   697: new java/net/NoRouteToHostException
    //   700: astore_3
    //   701: aload_3
    //   702: ldc_w 'Invalid redirect'
    //   705: invokespecial <init> : (Ljava/lang/String;)V
    //   708: aload_3
    //   709: athrow
    //   710: new java/net/NoRouteToHostException
    //   713: astore_3
    //   714: new java/lang/StringBuilder
    //   717: astore #7
    //   719: aload #7
    //   721: invokespecial <init> : ()V
    //   724: aload #7
    //   726: ldc_w 'Too many redirects: '
    //   729: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   732: pop
    //   733: aload #7
    //   735: iload #5
    //   737: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   740: pop
    //   741: aload_3
    //   742: aload #7
    //   744: invokevirtual toString : ()Ljava/lang/String;
    //   747: invokespecial <init> : (Ljava/lang/String;)V
    //   750: aload_3
    //   751: athrow
    //   752: new java/io/IOException
    //   755: astore_3
    //   756: aload_3
    //   757: ldc_w 'concurrently disconnecting'
    //   760: invokespecial <init> : (Ljava/lang/String;)V
    //   763: aload_3
    //   764: athrow
    //   765: new java/io/IOException
    //   768: astore_3
    //   769: aload_3
    //   770: ldc_w 'concurrently disconnecting'
    //   773: invokespecial <init> : (Ljava/lang/String;)V
    //   776: aload_3
    //   777: athrow
    //   778: astore_3
    //   779: aload_0
    //   780: ldc2_w -1
    //   783: putfield mTotalSize : J
    //   786: aload_0
    //   787: invokespecial teardownConnection : ()V
    //   790: aload_0
    //   791: ldc2_w -1
    //   794: putfield mCurrentOffset : J
    //   797: aload_3
    //   798: athrow
    //   799: astore_3
    //   800: aload_0
    //   801: monitorexit
    //   802: aload_3
    //   803: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #259	-> 2
    //   #263	-> 6
    //   #265	-> 6
    //   #268	-> 11
    //   #281	-> 20
    //   #284	-> 30
    //   #285	-> 35
    //   #287	-> 52
    //   #294	-> 63
    //   #301	-> 73
    //   #304	-> 83
    //   #306	-> 94
    //   #307	-> 101
    //   #308	-> 136
    //   #309	-> 142
    //   #308	-> 166
    //   #310	-> 175
    //   #313	-> 178
    //   #314	-> 184
    //   #318	-> 232
    //   #319	-> 241
    //   #325	-> 281
    //   #364	-> 281
    //   #367	-> 288
    //   #370	-> 299
    //   #375	-> 307
    //   #376	-> 312
    //   #378	-> 320
    //   #379	-> 327
    //   #384	-> 331
    //   #385	-> 339
    //   #386	-> 344
    //   #387	-> 344
    //   #390	-> 353
    //   #392	-> 361
    //   #391	-> 364
    //   #395	-> 365
    //   #398	-> 376
    //   #401	-> 388
    //   #404	-> 405
    //   #407	-> 415
    //   #408	-> 425
    //   #410	-> 440
    //   #417	-> 445
    //   #418	-> 445
    //   #396	-> 448
    //   #328	-> 458
    //   #332	-> 468
    //   #333	-> 476
    //   #334	-> 484
    //   #338	-> 507
    //   #340	-> 520
    //   #341	-> 532
    //   #344	-> 537
    //   #345	-> 551
    //   #346	-> 564
    //   #347	-> 580
    //   #349	-> 593
    //   #350	-> 609
    //   #351	-> 624
    //   #353	-> 637
    //   #354	-> 653
    //   #355	-> 668
    //   #358	-> 681
    //   #360	-> 689
    //   #362	-> 694
    //   #342	-> 697
    //   #329	-> 710
    //   #295	-> 752
    //   #282	-> 765
    //   #411	-> 778
    //   #412	-> 779
    //   #413	-> 786
    //   #414	-> 790
    //   #416	-> 797
    //   #258	-> 799
    // Exception table:
    //   from	to	target	type
    //   2	6	799	finally
    //   6	11	778	java/io/IOException
    //   6	11	799	finally
    //   11	17	778	java/io/IOException
    //   11	17	799	finally
    //   20	30	778	java/io/IOException
    //   20	30	799	finally
    //   35	49	778	java/io/IOException
    //   35	49	799	finally
    //   52	63	778	java/io/IOException
    //   52	63	799	finally
    //   63	73	778	java/io/IOException
    //   63	73	799	finally
    //   73	83	778	java/io/IOException
    //   73	83	799	finally
    //   83	94	778	java/io/IOException
    //   83	94	799	finally
    //   94	101	778	java/io/IOException
    //   94	101	799	finally
    //   101	116	778	java/io/IOException
    //   101	116	799	finally
    //   116	136	778	java/io/IOException
    //   116	136	799	finally
    //   136	142	778	java/io/IOException
    //   136	142	799	finally
    //   142	166	778	java/io/IOException
    //   142	166	799	finally
    //   166	175	778	java/io/IOException
    //   166	175	799	finally
    //   184	232	778	java/io/IOException
    //   184	232	799	finally
    //   232	241	778	java/io/IOException
    //   232	241	799	finally
    //   281	288	778	java/io/IOException
    //   281	288	799	finally
    //   288	299	778	java/io/IOException
    //   288	299	799	finally
    //   307	312	778	java/io/IOException
    //   307	312	799	finally
    //   312	320	778	java/io/IOException
    //   312	320	799	finally
    //   320	327	778	java/io/IOException
    //   320	327	799	finally
    //   331	339	778	java/io/IOException
    //   331	339	799	finally
    //   344	353	778	java/io/IOException
    //   344	353	799	finally
    //   353	361	364	java/lang/NumberFormatException
    //   353	361	778	java/io/IOException
    //   353	361	799	finally
    //   376	388	778	java/io/IOException
    //   376	388	799	finally
    //   405	415	778	java/io/IOException
    //   405	415	799	finally
    //   415	425	778	java/io/IOException
    //   415	425	799	finally
    //   425	440	778	java/io/IOException
    //   425	440	799	finally
    //   440	445	778	java/io/IOException
    //   440	445	799	finally
    //   448	458	778	java/io/IOException
    //   448	458	799	finally
    //   468	476	778	java/io/IOException
    //   468	476	799	finally
    //   484	504	778	java/io/IOException
    //   484	504	799	finally
    //   507	520	778	java/io/IOException
    //   507	520	799	finally
    //   520	532	778	java/io/IOException
    //   520	532	799	finally
    //   537	551	778	java/io/IOException
    //   537	551	799	finally
    //   551	564	778	java/io/IOException
    //   551	564	799	finally
    //   564	577	778	java/io/IOException
    //   564	577	799	finally
    //   580	593	778	java/io/IOException
    //   580	593	799	finally
    //   593	609	778	java/io/IOException
    //   593	609	799	finally
    //   609	616	778	java/io/IOException
    //   609	616	799	finally
    //   624	637	778	java/io/IOException
    //   624	637	799	finally
    //   637	653	778	java/io/IOException
    //   637	653	799	finally
    //   653	660	778	java/io/IOException
    //   653	660	799	finally
    //   668	681	778	java/io/IOException
    //   668	681	799	finally
    //   689	694	778	java/io/IOException
    //   689	694	799	finally
    //   697	710	778	java/io/IOException
    //   697	710	799	finally
    //   710	752	778	java/io/IOException
    //   710	752	799	finally
    //   752	765	778	java/io/IOException
    //   752	765	799	finally
    //   765	778	778	java/io/IOException
    //   765	778	799	finally
    //   779	786	799	finally
    //   786	790	799	finally
    //   790	797	799	finally
    //   797	799	799	finally
  }
  
  public int readAt(long paramLong, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: iload_3
    //   5: invokespecial native_readAt : (JI)I
    //   8: istore_3
    //   9: aload_0
    //   10: monitorexit
    //   11: iload_3
    //   12: ireturn
    //   13: astore #4
    //   15: aload_0
    //   16: monitorexit
    //   17: aload #4
    //   19: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #423	-> 2
    //   #423	-> 13
    // Exception table:
    //   from	to	target	type
    //   2	9	13	finally
  }
  
  private int readAt(long paramLong, byte[] paramArrayOfbyte, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: ldc 'debug.oplus.mediahttp'
    //   4: iconst_0
    //   5: invokestatic getBoolean : (Ljava/lang/String;Z)Z
    //   8: ifeq -> 29
    //   11: aload_0
    //   12: getfield mOplusMediaHTTPConnection : Lcom/oplus/media/OplusMediaHTTPConnection;
    //   15: lload_1
    //   16: aload_3
    //   17: iload #4
    //   19: invokevirtual readAt : (J[BI)I
    //   22: istore #4
    //   24: aload_0
    //   25: monitorexit
    //   26: iload #4
    //   28: ireturn
    //   29: new android/os/StrictMode$ThreadPolicy$Builder
    //   32: astore #5
    //   34: aload #5
    //   36: invokespecial <init> : ()V
    //   39: aload #5
    //   41: invokevirtual permitAll : ()Landroid/os/StrictMode$ThreadPolicy$Builder;
    //   44: invokevirtual build : ()Landroid/os/StrictMode$ThreadPolicy;
    //   47: astore #5
    //   49: aload #5
    //   51: invokestatic setThreadPolicy : (Landroid/os/StrictMode$ThreadPolicy;)V
    //   54: aload_0
    //   55: getfield mCurrentOffset : J
    //   58: lstore #6
    //   60: lload_1
    //   61: lload #6
    //   63: lcmp
    //   64: ifeq -> 72
    //   67: aload_0
    //   68: lload_1
    //   69: invokevirtual seekTo : (J)V
    //   72: aload_0
    //   73: getfield mInputStream : Ljava/io/InputStream;
    //   76: aload_3
    //   77: iconst_0
    //   78: iload #4
    //   80: invokevirtual read : ([BII)I
    //   83: istore #8
    //   85: iload #8
    //   87: istore #9
    //   89: iload #8
    //   91: iconst_m1
    //   92: if_icmpne -> 98
    //   95: iconst_0
    //   96: istore #9
    //   98: aload_0
    //   99: aload_0
    //   100: getfield mCurrentOffset : J
    //   103: iload #9
    //   105: i2l
    //   106: ladd
    //   107: putfield mCurrentOffset : J
    //   110: aload_0
    //   111: monitorexit
    //   112: iload #9
    //   114: ireturn
    //   115: astore_3
    //   116: goto -> 129
    //   119: astore_3
    //   120: goto -> 134
    //   123: astore_3
    //   124: aload_0
    //   125: monitorexit
    //   126: iconst_m1
    //   127: ireturn
    //   128: astore_3
    //   129: aload_0
    //   130: monitorexit
    //   131: iconst_m1
    //   132: ireturn
    //   133: astore_3
    //   134: new java/lang/StringBuilder
    //   137: astore #5
    //   139: aload #5
    //   141: invokespecial <init> : ()V
    //   144: aload #5
    //   146: ldc 'readAt '
    //   148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   151: pop
    //   152: aload #5
    //   154: lload_1
    //   155: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   158: pop
    //   159: aload #5
    //   161: ldc ' / '
    //   163: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   166: pop
    //   167: aload #5
    //   169: iload #4
    //   171: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   174: pop
    //   175: aload #5
    //   177: ldc_w ' => '
    //   180: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload #5
    //   186: aload_3
    //   187: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   190: pop
    //   191: ldc 'MediaHTTPConnection'
    //   193: aload #5
    //   195: invokevirtual toString : ()Ljava/lang/String;
    //   198: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   201: pop
    //   202: aload_0
    //   203: monitorexit
    //   204: sipush #-1010
    //   207: ireturn
    //   208: astore_3
    //   209: new java/lang/StringBuilder
    //   212: astore #5
    //   214: aload #5
    //   216: invokespecial <init> : ()V
    //   219: aload #5
    //   221: ldc 'readAt '
    //   223: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload #5
    //   229: lload_1
    //   230: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload #5
    //   236: ldc ' / '
    //   238: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   241: pop
    //   242: aload #5
    //   244: iload #4
    //   246: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   249: pop
    //   250: aload #5
    //   252: ldc_w ' => '
    //   255: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   258: pop
    //   259: aload #5
    //   261: aload_3
    //   262: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   265: pop
    //   266: ldc 'MediaHTTPConnection'
    //   268: aload #5
    //   270: invokevirtual toString : ()Ljava/lang/String;
    //   273: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   276: pop
    //   277: aload_0
    //   278: monitorexit
    //   279: sipush #-1010
    //   282: ireturn
    //   283: astore #5
    //   285: new java/lang/StringBuilder
    //   288: astore_3
    //   289: aload_3
    //   290: invokespecial <init> : ()V
    //   293: aload_3
    //   294: ldc 'readAt '
    //   296: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   299: pop
    //   300: aload_3
    //   301: lload_1
    //   302: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   305: pop
    //   306: aload_3
    //   307: ldc ' / '
    //   309: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   312: pop
    //   313: aload_3
    //   314: iload #4
    //   316: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   319: pop
    //   320: aload_3
    //   321: ldc_w ' => '
    //   324: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   327: pop
    //   328: aload_3
    //   329: aload #5
    //   331: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   334: pop
    //   335: ldc 'MediaHTTPConnection'
    //   337: aload_3
    //   338: invokevirtual toString : ()Ljava/lang/String;
    //   341: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   344: pop
    //   345: aload_0
    //   346: monitorexit
    //   347: sipush #-1010
    //   350: ireturn
    //   351: astore_3
    //   352: aload_0
    //   353: monitorexit
    //   354: aload_3
    //   355: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #430	-> 2
    //   #431	-> 11
    //   #434	-> 29
    //   #435	-> 39
    //   #437	-> 49
    //   #440	-> 54
    //   #441	-> 67
    //   #444	-> 72
    //   #446	-> 85
    //   #449	-> 95
    //   #452	-> 98
    //   #458	-> 110
    //   #468	-> 115
    //   #465	-> 119
    //   #473	-> 123
    //   #478	-> 124
    //   #468	-> 128
    //   #472	-> 129
    //   #465	-> 133
    //   #466	-> 134
    //   #467	-> 202
    //   #462	-> 208
    //   #463	-> 209
    //   #464	-> 277
    //   #459	-> 283
    //   #460	-> 285
    //   #461	-> 345
    //   #429	-> 351
    // Exception table:
    //   from	to	target	type
    //   2	11	351	finally
    //   11	24	351	finally
    //   29	39	351	finally
    //   39	49	351	finally
    //   49	54	351	finally
    //   54	60	283	java/net/ProtocolException
    //   54	60	208	java/net/NoRouteToHostException
    //   54	60	133	java/net/UnknownServiceException
    //   54	60	128	java/io/IOException
    //   54	60	123	java/lang/Exception
    //   54	60	351	finally
    //   67	72	283	java/net/ProtocolException
    //   67	72	208	java/net/NoRouteToHostException
    //   67	72	119	java/net/UnknownServiceException
    //   67	72	115	java/io/IOException
    //   67	72	123	java/lang/Exception
    //   67	72	351	finally
    //   72	85	283	java/net/ProtocolException
    //   72	85	208	java/net/NoRouteToHostException
    //   72	85	119	java/net/UnknownServiceException
    //   72	85	115	java/io/IOException
    //   72	85	123	java/lang/Exception
    //   72	85	351	finally
    //   98	110	283	java/net/ProtocolException
    //   98	110	208	java/net/NoRouteToHostException
    //   98	110	119	java/net/UnknownServiceException
    //   98	110	115	java/io/IOException
    //   98	110	123	java/lang/Exception
    //   98	110	351	finally
    //   134	202	351	finally
    //   209	277	351	finally
    //   285	345	351	finally
  }
  
  public long getSize() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mConnection : Ljava/net/HttpURLConnection;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnonnull -> 26
    //   11: aload_0
    //   12: lconst_0
    //   13: invokevirtual seekTo : (J)V
    //   16: goto -> 26
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: ldc2_w -1
    //   25: lreturn
    //   26: aload_0
    //   27: getfield mTotalSize : J
    //   30: lstore_2
    //   31: aload_0
    //   32: monitorexit
    //   33: lload_2
    //   34: lreturn
    //   35: astore_1
    //   36: aload_0
    //   37: monitorexit
    //   38: aload_1
    //   39: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #484	-> 2
    //   #486	-> 11
    //   #489	-> 16
    //   #487	-> 19
    //   #488	-> 20
    //   #492	-> 26
    //   #483	-> 35
    // Exception table:
    //   from	to	target	type
    //   2	7	35	finally
    //   11	16	19	java/io/IOException
    //   11	16	35	finally
    //   26	31	35	finally
  }
  
  public String getMIMEType() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mConnection : Ljava/net/HttpURLConnection;
    //   6: astore_1
    //   7: aload_1
    //   8: ifnonnull -> 26
    //   11: aload_0
    //   12: lconst_0
    //   13: invokevirtual seekTo : (J)V
    //   16: goto -> 26
    //   19: astore_1
    //   20: aload_0
    //   21: monitorexit
    //   22: ldc_w 'application/octet-stream'
    //   25: areturn
    //   26: aload_0
    //   27: getfield mConnection : Ljava/net/HttpURLConnection;
    //   30: invokevirtual getContentType : ()Ljava/lang/String;
    //   33: astore_1
    //   34: aload_0
    //   35: monitorexit
    //   36: aload_1
    //   37: areturn
    //   38: astore_1
    //   39: aload_0
    //   40: monitorexit
    //   41: aload_1
    //   42: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #498	-> 2
    //   #500	-> 11
    //   #503	-> 16
    //   #501	-> 19
    //   #502	-> 20
    //   #506	-> 26
    //   #497	-> 38
    // Exception table:
    //   from	to	target	type
    //   2	7	38	finally
    //   11	16	19	java/io/IOException
    //   11	16	38	finally
    //   26	34	38	finally
  }
  
  public String getUri() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mURL : Ljava/net/URL;
    //   6: invokevirtual toString : ()Ljava/lang/String;
    //   9: astore_1
    //   10: aload_0
    //   11: monitorexit
    //   12: aload_1
    //   13: areturn
    //   14: astore_1
    //   15: aload_0
    //   16: monitorexit
    //   17: aload_1
    //   18: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #512	-> 2
    //   #512	-> 14
    // Exception table:
    //   from	to	target	type
    //   2	10	14	finally
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private final native void native_finalize();
  
  private final native IBinder native_getIMemory();
  
  private static final native void native_init();
  
  private final native int native_readAt(long paramLong, int paramInt);
  
  private final native void native_setup();
}
