package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaRoute2ProviderServiceCallback extends IInterface {
  void notifyRequestFailed(long paramLong, int paramInt) throws RemoteException;
  
  void notifySessionCreated(long paramLong, RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionReleased(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionUpdated(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void updateState(MediaRoute2ProviderInfo paramMediaRoute2ProviderInfo) throws RemoteException;
  
  class Default implements IMediaRoute2ProviderServiceCallback {
    public void updateState(MediaRoute2ProviderInfo param1MediaRoute2ProviderInfo) throws RemoteException {}
    
    public void notifySessionCreated(long param1Long, RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionUpdated(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionReleased(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifyRequestFailed(long param1Long, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRoute2ProviderServiceCallback {
    private static final String DESCRIPTOR = "android.media.IMediaRoute2ProviderServiceCallback";
    
    static final int TRANSACTION_notifyRequestFailed = 5;
    
    static final int TRANSACTION_notifySessionCreated = 2;
    
    static final int TRANSACTION_notifySessionReleased = 4;
    
    static final int TRANSACTION_notifySessionUpdated = 3;
    
    static final int TRANSACTION_updateState = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRoute2ProviderServiceCallback");
    }
    
    public static IMediaRoute2ProviderServiceCallback asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRoute2ProviderServiceCallback");
      if (iInterface != null && iInterface instanceof IMediaRoute2ProviderServiceCallback)
        return (IMediaRoute2ProviderServiceCallback)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2) {
          if (param1Int != 3) {
            if (param1Int != 4) {
              if (param1Int != 5)
                return null; 
              return "notifyRequestFailed";
            } 
            return "notifySessionReleased";
          } 
          return "notifySessionUpdated";
        } 
        return "notifySessionCreated";
      } 
      return "updateState";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            if (param1Int1 != 4) {
              if (param1Int1 != 5) {
                if (param1Int1 != 1598968902)
                  return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
                param1Parcel2.writeString("android.media.IMediaRoute2ProviderServiceCallback");
                return true;
              } 
              param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderServiceCallback");
              long l1 = param1Parcel1.readLong();
              param1Int1 = param1Parcel1.readInt();
              notifyRequestFailed(l1, param1Int1);
              return true;
            } 
            param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderServiceCallback");
            if (param1Parcel1.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
            } else {
              param1Parcel1 = null;
            } 
            notifySessionReleased((RoutingSessionInfo)param1Parcel1);
            return true;
          } 
          param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderServiceCallback");
          if (param1Parcel1.readInt() != 0) {
            RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
          } else {
            param1Parcel1 = null;
          } 
          notifySessionUpdated((RoutingSessionInfo)param1Parcel1);
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderServiceCallback");
        long l = param1Parcel1.readLong();
        if (param1Parcel1.readInt() != 0) {
          RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel(param1Parcel1);
        } else {
          param1Parcel1 = null;
        } 
        notifySessionCreated(l, (RoutingSessionInfo)param1Parcel1);
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IMediaRoute2ProviderServiceCallback");
      if (param1Parcel1.readInt() != 0) {
        MediaRoute2ProviderInfo mediaRoute2ProviderInfo = MediaRoute2ProviderInfo.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      updateState((MediaRoute2ProviderInfo)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMediaRoute2ProviderServiceCallback {
      public static IMediaRoute2ProviderServiceCallback sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRoute2ProviderServiceCallback";
      }
      
      public void updateState(MediaRoute2ProviderInfo param2MediaRoute2ProviderInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderServiceCallback");
          if (param2MediaRoute2ProviderInfo != null) {
            parcel.writeInt(1);
            param2MediaRoute2ProviderInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl().updateState(param2MediaRoute2ProviderInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionCreated(long param2Long, RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderServiceCallback");
          parcel.writeLong(param2Long);
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl().notifySessionCreated(param2Long, param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionUpdated(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderServiceCallback");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl().notifySessionUpdated(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionReleased(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderServiceCallback");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl().notifySessionReleased(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRequestFailed(long param2Long, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRoute2ProviderServiceCallback");
          parcel.writeLong(param2Long);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl() != null) {
            IMediaRoute2ProviderServiceCallback.Stub.getDefaultImpl().notifyRequestFailed(param2Long, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRoute2ProviderServiceCallback param1IMediaRoute2ProviderServiceCallback) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRoute2ProviderServiceCallback != null) {
          Proxy.sDefaultImpl = param1IMediaRoute2ProviderServiceCallback;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRoute2ProviderServiceCallback getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
