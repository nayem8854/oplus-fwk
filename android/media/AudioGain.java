package android.media;

public class AudioGain {
  public static final int MODE_CHANNELS = 2;
  
  public static final int MODE_JOINT = 1;
  
  public static final int MODE_RAMP = 4;
  
  private final int mChannelMask;
  
  private final int mDefaultValue;
  
  private final int mIndex;
  
  private final int mMaxValue;
  
  private final int mMinValue;
  
  private final int mMode;
  
  private final int mRampDurationMaxMs;
  
  private final int mRampDurationMinMs;
  
  private final int mStepValue;
  
  AudioGain(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6, int paramInt7, int paramInt8, int paramInt9) {
    this.mIndex = paramInt1;
    this.mMode = paramInt2;
    this.mChannelMask = paramInt3;
    this.mMinValue = paramInt4;
    this.mMaxValue = paramInt5;
    this.mDefaultValue = paramInt6;
    this.mStepValue = paramInt7;
    this.mRampDurationMinMs = paramInt8;
    this.mRampDurationMaxMs = paramInt9;
  }
  
  public int mode() {
    return this.mMode;
  }
  
  public int channelMask() {
    return this.mChannelMask;
  }
  
  public int minValue() {
    return this.mMinValue;
  }
  
  public int maxValue() {
    return this.mMaxValue;
  }
  
  public int defaultValue() {
    return this.mDefaultValue;
  }
  
  public int stepValue() {
    return this.mStepValue;
  }
  
  public int rampDurationMinMs() {
    return this.mRampDurationMinMs;
  }
  
  public int rampDurationMaxMs() {
    return this.mRampDurationMaxMs;
  }
  
  public AudioGainConfig buildConfig(int paramInt1, int paramInt2, int[] paramArrayOfint, int paramInt3) {
    return new AudioGainConfig(this.mIndex, this, paramInt1, paramInt2, paramArrayOfint, paramInt3);
  }
}
