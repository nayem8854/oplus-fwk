package android.media;

public interface VolumeAutomation {
  VolumeShaper createVolumeShaper(VolumeShaper.Configuration paramConfiguration);
}
