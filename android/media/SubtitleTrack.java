package android.media;

import android.graphics.Canvas;
import android.os.Handler;
import android.util.Log;
import android.util.LongSparseArray;
import android.util.Pair;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.Vector;

public abstract class SubtitleTrack implements MediaTimeProvider.OnMediaTimeListener {
  protected final LongSparseArray<Run> mRunsByEndTime = new LongSparseArray();
  
  protected final LongSparseArray<Run> mRunsByID = new LongSparseArray();
  
  protected final Vector<Cue> mActiveCues = new Vector<>();
  
  public boolean DEBUG = false;
  
  protected Handler mHandler = new Handler();
  
  private static final String TAG = "SubtitleTrack";
  
  protected CueList mCues;
  
  private MediaFormat mFormat;
  
  private long mLastTimeMs;
  
  private long mLastUpdateTimeMs;
  
  private long mNextScheduledTimeMs;
  
  private Runnable mRunnable;
  
  protected MediaTimeProvider mTimeProvider;
  
  protected boolean mVisible;
  
  public final MediaFormat getFormat() {
    return this.mFormat;
  }
  
  public SubtitleTrack(MediaFormat paramMediaFormat) {
    this.mNextScheduledTimeMs = -1L;
    this.mFormat = paramMediaFormat;
    this.mCues = new CueList();
    clearActiveCues();
    this.mLastTimeMs = -1L;
  }
  
  protected void onData(SubtitleData paramSubtitleData) {
    long l1 = paramSubtitleData.getStartTimeUs() + 1L;
    onData(paramSubtitleData.getData(), true, l1);
    long l2 = (paramSubtitleData.getStartTimeUs() + paramSubtitleData.getDurationUs()) / 1000L;
    setRunDiscardTimeMs(l1, l2);
  }
  
  protected void updateActiveCues(boolean paramBoolean, long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: iload_1
    //   3: ifne -> 15
    //   6: aload_0
    //   7: getfield mLastUpdateTimeMs : J
    //   10: lload_2
    //   11: lcmp
    //   12: ifle -> 19
    //   15: aload_0
    //   16: invokevirtual clearActiveCues : ()V
    //   19: aload_0
    //   20: getfield mCues : Landroid/media/SubtitleTrack$CueList;
    //   23: astore #4
    //   25: aload_0
    //   26: getfield mLastUpdateTimeMs : J
    //   29: lstore #5
    //   31: aload #4
    //   33: lload #5
    //   35: lload_2
    //   36: invokevirtual entriesBetween : (JJ)Ljava/lang/Iterable;
    //   39: invokeinterface iterator : ()Ljava/util/Iterator;
    //   44: astore #7
    //   46: aload #7
    //   48: invokeinterface hasNext : ()Z
    //   53: ifeq -> 282
    //   56: aload #7
    //   58: invokeinterface next : ()Ljava/lang/Object;
    //   63: checkcast android/util/Pair
    //   66: astore #8
    //   68: aload #8
    //   70: getfield second : Ljava/lang/Object;
    //   73: checkcast android/media/SubtitleTrack$Cue
    //   76: astore #4
    //   78: aload #4
    //   80: getfield mEndTimeMs : J
    //   83: aload #8
    //   85: getfield first : Ljava/lang/Object;
    //   88: checkcast java/lang/Long
    //   91: invokevirtual longValue : ()J
    //   94: lcmp
    //   95: ifne -> 173
    //   98: aload_0
    //   99: getfield DEBUG : Z
    //   102: ifeq -> 143
    //   105: new java/lang/StringBuilder
    //   108: astore #8
    //   110: aload #8
    //   112: invokespecial <init> : ()V
    //   115: aload #8
    //   117: ldc_w 'Removing '
    //   120: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload #8
    //   126: aload #4
    //   128: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   131: pop
    //   132: ldc 'SubtitleTrack'
    //   134: aload #8
    //   136: invokevirtual toString : ()Ljava/lang/String;
    //   139: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   142: pop
    //   143: aload_0
    //   144: getfield mActiveCues : Ljava/util/Vector;
    //   147: aload #4
    //   149: invokevirtual remove : (Ljava/lang/Object;)Z
    //   152: pop
    //   153: aload #4
    //   155: getfield mRunID : J
    //   158: lconst_0
    //   159: lcmp
    //   160: ifne -> 279
    //   163: aload #7
    //   165: invokeinterface remove : ()V
    //   170: goto -> 279
    //   173: aload #4
    //   175: getfield mStartTimeMs : J
    //   178: aload #8
    //   180: getfield first : Ljava/lang/Object;
    //   183: checkcast java/lang/Long
    //   186: invokevirtual longValue : ()J
    //   189: lcmp
    //   190: ifne -> 265
    //   193: aload_0
    //   194: getfield DEBUG : Z
    //   197: ifeq -> 238
    //   200: new java/lang/StringBuilder
    //   203: astore #8
    //   205: aload #8
    //   207: invokespecial <init> : ()V
    //   210: aload #8
    //   212: ldc_w 'Adding '
    //   215: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   218: pop
    //   219: aload #8
    //   221: aload #4
    //   223: invokevirtual append : (Ljava/lang/Object;)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: ldc 'SubtitleTrack'
    //   229: aload #8
    //   231: invokevirtual toString : ()Ljava/lang/String;
    //   234: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   237: pop
    //   238: aload #4
    //   240: getfield mInnerTimesMs : [J
    //   243: ifnull -> 252
    //   246: aload #4
    //   248: lload_2
    //   249: invokevirtual onTime : (J)V
    //   252: aload_0
    //   253: getfield mActiveCues : Ljava/util/Vector;
    //   256: aload #4
    //   258: invokevirtual add : (Ljava/lang/Object;)Z
    //   261: pop
    //   262: goto -> 279
    //   265: aload #4
    //   267: getfield mInnerTimesMs : [J
    //   270: ifnull -> 279
    //   273: aload #4
    //   275: lload_2
    //   276: invokevirtual onTime : (J)V
    //   279: goto -> 46
    //   282: aload_0
    //   283: getfield mRunsByEndTime : Landroid/util/LongSparseArray;
    //   286: invokevirtual size : ()I
    //   289: ifle -> 317
    //   292: aload_0
    //   293: getfield mRunsByEndTime : Landroid/util/LongSparseArray;
    //   296: astore #4
    //   298: aload #4
    //   300: iconst_0
    //   301: invokevirtual keyAt : (I)J
    //   304: lload_2
    //   305: lcmp
    //   306: ifgt -> 317
    //   309: aload_0
    //   310: iconst_0
    //   311: invokespecial removeRunsByEndTimeIndex : (I)V
    //   314: goto -> 282
    //   317: aload_0
    //   318: lload_2
    //   319: putfield mLastUpdateTimeMs : J
    //   322: aload_0
    //   323: monitorexit
    //   324: return
    //   325: astore #4
    //   327: aload_0
    //   328: monitorexit
    //   329: aload #4
    //   331: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #132	-> 2
    //   #133	-> 15
    //   #136	-> 19
    //   #137	-> 31
    //   #138	-> 56
    //   #139	-> 68
    //   #141	-> 78
    //   #143	-> 98
    //   #144	-> 143
    //   #145	-> 153
    //   #146	-> 163
    //   #148	-> 173
    //   #151	-> 193
    //   #152	-> 238
    //   #153	-> 246
    //   #155	-> 252
    //   #156	-> 265
    //   #158	-> 273
    //   #160	-> 279
    //   #163	-> 282
    //   #164	-> 298
    //   #165	-> 309
    //   #167	-> 317
    //   #168	-> 322
    //   #131	-> 325
    // Exception table:
    //   from	to	target	type
    //   6	15	325	finally
    //   15	19	325	finally
    //   19	31	325	finally
    //   31	46	325	finally
    //   46	56	325	finally
    //   56	68	325	finally
    //   68	78	325	finally
    //   78	98	325	finally
    //   98	143	325	finally
    //   143	153	325	finally
    //   153	163	325	finally
    //   163	170	325	finally
    //   173	193	325	finally
    //   193	238	325	finally
    //   238	246	325	finally
    //   246	252	325	finally
    //   252	262	325	finally
    //   265	273	325	finally
    //   273	279	325	finally
    //   282	298	325	finally
    //   298	309	325	finally
    //   309	314	325	finally
    //   317	322	325	finally
  }
  
  private void removeRunsByEndTimeIndex(int paramInt) {
    Run run = (Run)this.mRunsByEndTime.valueAt(paramInt);
    while (run != null) {
      Cue cue = run.mFirstCue;
      while (cue != null) {
        this.mCues.remove(cue);
        Cue cue1 = cue.mNextInRun;
        cue.mNextInRun = null;
        cue = cue1;
      } 
      this.mRunsByID.remove(run.mRunID);
      Run run1 = run.mNextRunAtEndTimeMs;
      run.mPrevRunAtEndTimeMs = null;
      run.mNextRunAtEndTimeMs = null;
      run = run1;
    } 
    this.mRunsByEndTime.removeAt(paramInt);
  }
  
  protected void finalize() throws Throwable {
    int i = this.mRunsByEndTime.size();
    for (; --i >= 0; i--)
      removeRunsByEndTimeIndex(i); 
    super.finalize();
  }
  
  private void takeTime(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: lload_1
    //   4: putfield mLastTimeMs : J
    //   7: aload_0
    //   8: monitorexit
    //   9: return
    //   10: astore_3
    //   11: aload_0
    //   12: monitorexit
    //   13: aload_3
    //   14: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #201	-> 2
    //   #202	-> 7
    //   #200	-> 10
    // Exception table:
    //   from	to	target	type
    //   2	7	10	finally
  }
  
  protected void clearActiveCues() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield DEBUG : Z
    //   6: ifeq -> 53
    //   9: new java/lang/StringBuilder
    //   12: astore_1
    //   13: aload_1
    //   14: invokespecial <init> : ()V
    //   17: aload_1
    //   18: ldc 'Clearing '
    //   20: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   23: pop
    //   24: aload_1
    //   25: aload_0
    //   26: getfield mActiveCues : Ljava/util/Vector;
    //   29: invokevirtual size : ()I
    //   32: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   35: pop
    //   36: aload_1
    //   37: ldc ' active cues'
    //   39: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: ldc 'SubtitleTrack'
    //   45: aload_1
    //   46: invokevirtual toString : ()Ljava/lang/String;
    //   49: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   52: pop
    //   53: aload_0
    //   54: getfield mActiveCues : Ljava/util/Vector;
    //   57: invokevirtual clear : ()V
    //   60: aload_0
    //   61: ldc2_w -1
    //   64: putfield mLastUpdateTimeMs : J
    //   67: aload_0
    //   68: monitorexit
    //   69: return
    //   70: astore_1
    //   71: aload_0
    //   72: monitorexit
    //   73: aload_1
    //   74: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #206	-> 2
    //   #207	-> 53
    //   #208	-> 60
    //   #209	-> 67
    //   #205	-> 70
    // Exception table:
    //   from	to	target	type
    //   2	53	70	finally
    //   53	60	70	finally
    //   60	67	70	finally
  }
  
  protected void scheduleTimedEvents() {
    if (this.mTimeProvider != null) {
      this.mNextScheduledTimeMs = this.mCues.nextTimeAfter(this.mLastTimeMs);
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("sched @");
        stringBuilder.append(this.mNextScheduledTimeMs);
        stringBuilder.append(" after ");
        stringBuilder.append(this.mLastTimeMs);
        Log.d("SubtitleTrack", stringBuilder.toString());
      } 
      MediaTimeProvider mediaTimeProvider = this.mTimeProvider;
      long l = this.mNextScheduledTimeMs;
      if (l >= 0L) {
        l *= 1000L;
      } else {
        l = -1L;
      } 
      mediaTimeProvider.notifyAt(l, this);
    } 
  }
  
  public void onTimedEvent(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: getfield DEBUG : Z
    //   4: ifeq -> 39
    //   7: new java/lang/StringBuilder
    //   10: dup
    //   11: invokespecial <init> : ()V
    //   14: astore_3
    //   15: aload_3
    //   16: ldc_w 'onTimedEvent '
    //   19: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: pop
    //   23: aload_3
    //   24: lload_1
    //   25: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: ldc 'SubtitleTrack'
    //   31: aload_3
    //   32: invokevirtual toString : ()Ljava/lang/String;
    //   35: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   38: pop
    //   39: aload_0
    //   40: monitorenter
    //   41: lload_1
    //   42: ldc2_w 1000
    //   45: ldiv
    //   46: lstore_1
    //   47: aload_0
    //   48: iconst_0
    //   49: lload_1
    //   50: invokevirtual updateActiveCues : (ZJ)V
    //   53: aload_0
    //   54: lload_1
    //   55: invokespecial takeTime : (J)V
    //   58: aload_0
    //   59: monitorexit
    //   60: aload_0
    //   61: aload_0
    //   62: getfield mActiveCues : Ljava/util/Vector;
    //   65: invokevirtual updateView : (Ljava/util/Vector;)V
    //   68: aload_0
    //   69: invokevirtual scheduleTimedEvents : ()V
    //   72: return
    //   73: astore_3
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_3
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #229	-> 0
    //   #230	-> 39
    //   #231	-> 41
    //   #232	-> 47
    //   #233	-> 53
    //   #234	-> 58
    //   #235	-> 60
    //   #236	-> 68
    //   #237	-> 72
    //   #234	-> 73
    // Exception table:
    //   from	to	target	type
    //   41	47	73	finally
    //   47	53	73	finally
    //   53	58	73	finally
    //   58	60	73	finally
    //   74	76	73	finally
  }
  
  public void onSeek(long paramLong) {
    // Byte code:
    //   0: aload_0
    //   1: getfield DEBUG : Z
    //   4: ifeq -> 39
    //   7: new java/lang/StringBuilder
    //   10: dup
    //   11: invokespecial <init> : ()V
    //   14: astore_3
    //   15: aload_3
    //   16: ldc_w 'onSeek '
    //   19: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: pop
    //   23: aload_3
    //   24: lload_1
    //   25: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   28: pop
    //   29: ldc 'SubtitleTrack'
    //   31: aload_3
    //   32: invokevirtual toString : ()Ljava/lang/String;
    //   35: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   38: pop
    //   39: aload_0
    //   40: monitorenter
    //   41: lload_1
    //   42: ldc2_w 1000
    //   45: ldiv
    //   46: lstore_1
    //   47: aload_0
    //   48: iconst_1
    //   49: lload_1
    //   50: invokevirtual updateActiveCues : (ZJ)V
    //   53: aload_0
    //   54: lload_1
    //   55: invokespecial takeTime : (J)V
    //   58: aload_0
    //   59: monitorexit
    //   60: aload_0
    //   61: aload_0
    //   62: getfield mActiveCues : Ljava/util/Vector;
    //   65: invokevirtual updateView : (Ljava/util/Vector;)V
    //   68: aload_0
    //   69: invokevirtual scheduleTimedEvents : ()V
    //   72: return
    //   73: astore_3
    //   74: aload_0
    //   75: monitorexit
    //   76: aload_3
    //   77: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #244	-> 0
    //   #245	-> 39
    //   #246	-> 41
    //   #247	-> 47
    //   #248	-> 53
    //   #249	-> 58
    //   #250	-> 60
    //   #251	-> 68
    //   #252	-> 72
    //   #249	-> 73
    // Exception table:
    //   from	to	target	type
    //   41	47	73	finally
    //   47	53	73	finally
    //   53	58	73	finally
    //   58	60	73	finally
    //   74	76	73	finally
  }
  
  public void onStop() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield DEBUG : Z
    //   6: ifeq -> 18
    //   9: ldc 'SubtitleTrack'
    //   11: ldc_w 'onStop'
    //   14: invokestatic d : (Ljava/lang/String;Ljava/lang/String;)I
    //   17: pop
    //   18: aload_0
    //   19: invokevirtual clearActiveCues : ()V
    //   22: aload_0
    //   23: ldc2_w -1
    //   26: putfield mLastTimeMs : J
    //   29: aload_0
    //   30: monitorexit
    //   31: aload_0
    //   32: aload_0
    //   33: getfield mActiveCues : Ljava/util/Vector;
    //   36: invokevirtual updateView : (Ljava/util/Vector;)V
    //   39: aload_0
    //   40: ldc2_w -1
    //   43: putfield mNextScheduledTimeMs : J
    //   46: aload_0
    //   47: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   50: astore_1
    //   51: aload_1
    //   52: ifnull -> 65
    //   55: aload_1
    //   56: ldc2_w -1
    //   59: aload_0
    //   60: invokeinterface notifyAt : (JLandroid/media/MediaTimeProvider$OnMediaTimeListener;)V
    //   65: return
    //   66: astore_1
    //   67: aload_0
    //   68: monitorexit
    //   69: aload_1
    //   70: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #259	-> 0
    //   #260	-> 2
    //   #261	-> 18
    //   #262	-> 22
    //   #263	-> 29
    //   #264	-> 31
    //   #265	-> 39
    //   #266	-> 46
    //   #267	-> 55
    //   #269	-> 65
    //   #263	-> 66
    // Exception table:
    //   from	to	target	type
    //   2	18	66	finally
    //   18	22	66	finally
    //   22	29	66	finally
    //   29	31	66	finally
    //   67	69	66	finally
  }
  
  public void show() {
    if (this.mVisible)
      return; 
    this.mVisible = true;
    RenderingWidget renderingWidget = getRenderingWidget();
    if (renderingWidget != null)
      renderingWidget.setVisible(true); 
    MediaTimeProvider mediaTimeProvider = this.mTimeProvider;
    if (mediaTimeProvider != null)
      mediaTimeProvider.scheduleUpdate(this); 
  }
  
  public void hide() {
    if (!this.mVisible)
      return; 
    MediaTimeProvider mediaTimeProvider = this.mTimeProvider;
    if (mediaTimeProvider != null)
      mediaTimeProvider.cancelNotifications(this); 
    RenderingWidget renderingWidget = getRenderingWidget();
    if (renderingWidget != null)
      renderingWidget.setVisible(false); 
    this.mVisible = false;
  }
  
  protected boolean addCue(Cue paramCue) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mCues : Landroid/media/SubtitleTrack$CueList;
    //   6: aload_1
    //   7: invokevirtual add : (Landroid/media/SubtitleTrack$Cue;)V
    //   10: aload_1
    //   11: getfield mRunID : J
    //   14: lconst_0
    //   15: lcmp
    //   16: ifeq -> 107
    //   19: aload_0
    //   20: getfield mRunsByID : Landroid/util/LongSparseArray;
    //   23: aload_1
    //   24: getfield mRunID : J
    //   27: invokevirtual get : (J)Ljava/lang/Object;
    //   30: checkcast android/media/SubtitleTrack$Run
    //   33: astore_2
    //   34: aload_2
    //   35: ifnonnull -> 70
    //   38: new android/media/SubtitleTrack$Run
    //   41: astore_3
    //   42: aload_3
    //   43: aconst_null
    //   44: invokespecial <init> : (Landroid/media/SubtitleTrack$1;)V
    //   47: aload_0
    //   48: getfield mRunsByID : Landroid/util/LongSparseArray;
    //   51: aload_1
    //   52: getfield mRunID : J
    //   55: aload_3
    //   56: invokevirtual put : (JLjava/lang/Object;)V
    //   59: aload_3
    //   60: aload_1
    //   61: getfield mEndTimeMs : J
    //   64: putfield mEndTimeMs : J
    //   67: goto -> 94
    //   70: aload_2
    //   71: astore_3
    //   72: aload_2
    //   73: getfield mEndTimeMs : J
    //   76: aload_1
    //   77: getfield mEndTimeMs : J
    //   80: lcmp
    //   81: ifge -> 94
    //   84: aload_2
    //   85: aload_1
    //   86: getfield mEndTimeMs : J
    //   89: putfield mEndTimeMs : J
    //   92: aload_2
    //   93: astore_3
    //   94: aload_1
    //   95: aload_3
    //   96: getfield mFirstCue : Landroid/media/SubtitleTrack$Cue;
    //   99: putfield mNextInRun : Landroid/media/SubtitleTrack$Cue;
    //   102: aload_3
    //   103: aload_1
    //   104: putfield mFirstCue : Landroid/media/SubtitleTrack$Cue;
    //   107: ldc2_w -1
    //   110: lstore #4
    //   112: aload_0
    //   113: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   116: astore_3
    //   117: lload #4
    //   119: lstore #6
    //   121: aload_3
    //   122: ifnull -> 150
    //   125: aload_0
    //   126: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   129: iconst_0
    //   130: iconst_1
    //   131: invokeinterface getCurrentTimeUs : (ZZ)J
    //   136: ldc2_w 1000
    //   139: ldiv
    //   140: lstore #6
    //   142: goto -> 150
    //   145: astore_3
    //   146: lload #4
    //   148: lstore #6
    //   150: aload_0
    //   151: getfield DEBUG : Z
    //   154: ifeq -> 253
    //   157: new java/lang/StringBuilder
    //   160: astore_3
    //   161: aload_3
    //   162: invokespecial <init> : ()V
    //   165: aload_3
    //   166: ldc 'mVisible='
    //   168: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   171: pop
    //   172: aload_3
    //   173: aload_0
    //   174: getfield mVisible : Z
    //   177: invokevirtual append : (Z)Ljava/lang/StringBuilder;
    //   180: pop
    //   181: aload_3
    //   182: ldc ', '
    //   184: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   187: pop
    //   188: aload_3
    //   189: aload_1
    //   190: getfield mStartTimeMs : J
    //   193: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   196: pop
    //   197: aload_3
    //   198: ldc ' <= '
    //   200: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   203: pop
    //   204: aload_3
    //   205: lload #6
    //   207: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   210: pop
    //   211: aload_3
    //   212: ldc ', '
    //   214: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   217: pop
    //   218: aload_3
    //   219: aload_1
    //   220: getfield mEndTimeMs : J
    //   223: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   226: pop
    //   227: aload_3
    //   228: ldc ' >= '
    //   230: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   233: pop
    //   234: aload_3
    //   235: aload_0
    //   236: getfield mLastTimeMs : J
    //   239: invokevirtual append : (J)Ljava/lang/StringBuilder;
    //   242: pop
    //   243: ldc 'SubtitleTrack'
    //   245: aload_3
    //   246: invokevirtual toString : ()Ljava/lang/String;
    //   249: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   252: pop
    //   253: aload_0
    //   254: getfield mVisible : Z
    //   257: ifeq -> 368
    //   260: aload_1
    //   261: getfield mStartTimeMs : J
    //   264: lload #6
    //   266: lcmp
    //   267: ifgt -> 368
    //   270: aload_1
    //   271: getfield mEndTimeMs : J
    //   274: aload_0
    //   275: getfield mLastTimeMs : J
    //   278: lcmp
    //   279: iflt -> 368
    //   282: aload_0
    //   283: getfield mRunnable : Ljava/lang/Runnable;
    //   286: ifnull -> 300
    //   289: aload_0
    //   290: getfield mHandler : Landroid/os/Handler;
    //   293: aload_0
    //   294: getfield mRunnable : Ljava/lang/Runnable;
    //   297: invokevirtual removeCallbacks : (Ljava/lang/Runnable;)V
    //   300: new android/media/SubtitleTrack$1
    //   303: astore_1
    //   304: aload_1
    //   305: aload_0
    //   306: aload_0
    //   307: lload #6
    //   309: invokespecial <init> : (Landroid/media/SubtitleTrack;Landroid/media/SubtitleTrack;J)V
    //   312: aload_0
    //   313: aload_1
    //   314: putfield mRunnable : Ljava/lang/Runnable;
    //   317: aload_0
    //   318: getfield mHandler : Landroid/os/Handler;
    //   321: aload_1
    //   322: ldc2_w 10
    //   325: invokevirtual postDelayed : (Ljava/lang/Runnable;J)Z
    //   328: ifeq -> 349
    //   331: aload_0
    //   332: getfield DEBUG : Z
    //   335: ifeq -> 364
    //   338: ldc 'SubtitleTrack'
    //   340: ldc 'scheduling update'
    //   342: invokestatic v : (Ljava/lang/String;Ljava/lang/String;)I
    //   345: pop
    //   346: goto -> 364
    //   349: aload_0
    //   350: getfield DEBUG : Z
    //   353: ifeq -> 364
    //   356: ldc 'SubtitleTrack'
    //   358: ldc 'failed to schedule subtitle view update'
    //   360: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
    //   363: pop
    //   364: aload_0
    //   365: monitorexit
    //   366: iconst_1
    //   367: ireturn
    //   368: aload_0
    //   369: getfield mVisible : Z
    //   372: ifeq -> 412
    //   375: aload_1
    //   376: getfield mEndTimeMs : J
    //   379: aload_0
    //   380: getfield mLastTimeMs : J
    //   383: lcmp
    //   384: iflt -> 412
    //   387: aload_1
    //   388: getfield mStartTimeMs : J
    //   391: aload_0
    //   392: getfield mNextScheduledTimeMs : J
    //   395: lcmp
    //   396: iflt -> 408
    //   399: aload_0
    //   400: getfield mNextScheduledTimeMs : J
    //   403: lconst_0
    //   404: lcmp
    //   405: ifge -> 412
    //   408: aload_0
    //   409: invokevirtual scheduleTimedEvents : ()V
    //   412: aload_0
    //   413: monitorexit
    //   414: iconst_0
    //   415: ireturn
    //   416: astore_1
    //   417: aload_0
    //   418: monitorexit
    //   419: aload_1
    //   420: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #308	-> 2
    //   #310	-> 10
    //   #311	-> 19
    //   #312	-> 34
    //   #313	-> 38
    //   #314	-> 47
    //   #315	-> 59
    //   #316	-> 70
    //   #317	-> 84
    //   #321	-> 94
    //   #322	-> 102
    //   #326	-> 107
    //   #327	-> 112
    //   #329	-> 125
    //   #333	-> 142
    //   #331	-> 145
    //   #336	-> 150
    //   #340	-> 253
    //   #344	-> 282
    //   #345	-> 289
    //   #347	-> 300
    //   #348	-> 300
    //   #349	-> 300
    //   #363	-> 317
    //   #364	-> 331
    //   #366	-> 349
    //   #368	-> 364
    //   #371	-> 368
    //   #375	-> 408
    //   #378	-> 412
    //   #307	-> 416
    // Exception table:
    //   from	to	target	type
    //   2	10	416	finally
    //   10	19	416	finally
    //   19	34	416	finally
    //   38	47	416	finally
    //   47	59	416	finally
    //   59	67	416	finally
    //   72	84	416	finally
    //   84	92	416	finally
    //   94	102	416	finally
    //   102	107	416	finally
    //   112	117	416	finally
    //   125	142	145	java/lang/IllegalStateException
    //   125	142	416	finally
    //   150	253	416	finally
    //   253	282	416	finally
    //   282	289	416	finally
    //   289	300	416	finally
    //   300	317	416	finally
    //   317	331	416	finally
    //   331	346	416	finally
    //   349	364	416	finally
    //   368	408	416	finally
    //   408	412	416	finally
  }
  
  public void setTimeProvider(MediaTimeProvider paramMediaTimeProvider) {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   6: astore_2
    //   7: aload_2
    //   8: aload_1
    //   9: if_acmpne -> 15
    //   12: aload_0
    //   13: monitorexit
    //   14: return
    //   15: aload_0
    //   16: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   19: ifnull -> 32
    //   22: aload_0
    //   23: getfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   26: aload_0
    //   27: invokeinterface cancelNotifications : (Landroid/media/MediaTimeProvider$OnMediaTimeListener;)V
    //   32: aload_0
    //   33: aload_1
    //   34: putfield mTimeProvider : Landroid/media/MediaTimeProvider;
    //   37: aload_1
    //   38: ifnull -> 48
    //   41: aload_1
    //   42: aload_0
    //   43: invokeinterface scheduleUpdate : (Landroid/media/MediaTimeProvider$OnMediaTimeListener;)V
    //   48: aload_0
    //   49: monitorexit
    //   50: return
    //   51: astore_1
    //   52: aload_0
    //   53: monitorexit
    //   54: aload_1
    //   55: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #383	-> 2
    //   #384	-> 12
    //   #386	-> 15
    //   #387	-> 22
    //   #389	-> 32
    //   #390	-> 37
    //   #391	-> 41
    //   #393	-> 48
    //   #382	-> 51
    // Exception table:
    //   from	to	target	type
    //   2	7	51	finally
    //   15	22	51	finally
    //   22	32	51	finally
    //   32	37	51	finally
    //   41	48	51	finally
  }
  
  class CueList {
    private static final String TAG = "CueList";
    
    public boolean DEBUG = false;
    
    private SortedMap<Long, Vector<SubtitleTrack.Cue>> mCues;
    
    private boolean addEvent(SubtitleTrack.Cue param1Cue, long param1Long) {
      Vector<SubtitleTrack.Cue> vector2, vector1 = this.mCues.get(Long.valueOf(param1Long));
      if (vector1 == null) {
        vector2 = new Vector(2);
        this.mCues.put(Long.valueOf(param1Long), vector2);
      } else {
        vector2 = vector1;
        if (vector1.contains(param1Cue))
          return false; 
      } 
      vector2.add(param1Cue);
      return true;
    }
    
    private void removeEvent(SubtitleTrack.Cue param1Cue, long param1Long) {
      Vector vector = this.mCues.get(Long.valueOf(param1Long));
      if (vector != null) {
        vector.remove(param1Cue);
        if (vector.size() == 0)
          this.mCues.remove(Long.valueOf(param1Long)); 
      } 
    }
    
    public void add(SubtitleTrack.Cue param1Cue) {
      if (param1Cue.mStartTimeMs >= param1Cue.mEndTimeMs)
        return; 
      if (!addEvent(param1Cue, param1Cue.mStartTimeMs))
        return; 
      long l = param1Cue.mStartTimeMs;
      if (param1Cue.mInnerTimesMs != null) {
        long[] arrayOfLong;
        int i;
        byte b;
        for (arrayOfLong = param1Cue.mInnerTimesMs, i = arrayOfLong.length, b = 0; b < i; ) {
          long l1 = arrayOfLong[b];
          long l2 = l;
          if (l1 > l) {
            l2 = l;
            if (l1 < param1Cue.mEndTimeMs) {
              addEvent(param1Cue, l1);
              l2 = l1;
            } 
          } 
          b++;
          l = l2;
        } 
      } 
      addEvent(param1Cue, param1Cue.mEndTimeMs);
    }
    
    public void remove(SubtitleTrack.Cue param1Cue) {
      removeEvent(param1Cue, param1Cue.mStartTimeMs);
      if (param1Cue.mInnerTimesMs != null)
        for (long l : param1Cue.mInnerTimesMs)
          removeEvent(param1Cue, l);  
      removeEvent(param1Cue, param1Cue.mEndTimeMs);
    }
    
    public Iterable<Pair<Long, SubtitleTrack.Cue>> entriesBetween(final long lastTimeMs, final long timeMs) {
      return new Iterable<Pair<Long, SubtitleTrack.Cue>>() {
          final SubtitleTrack.CueList this$0;
          
          final long val$lastTimeMs;
          
          final long val$timeMs;
          
          public Iterator<Pair<Long, SubtitleTrack.Cue>> iterator() {
            if (SubtitleTrack.CueList.this.DEBUG) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append("slice (");
              stringBuilder.append(lastTimeMs);
              stringBuilder.append(", ");
              stringBuilder.append(timeMs);
              stringBuilder.append("]=");
              Log.d("CueList", stringBuilder.toString());
            } 
            try {
              SubtitleTrack.CueList cueList1 = SubtitleTrack.CueList.this, cueList2 = SubtitleTrack.CueList.this;
              return 
                new SubtitleTrack.CueList.EntryIterator(cueList2.mCues.subMap(Long.valueOf(lastTimeMs + 1L), Long.valueOf(timeMs + 1L)));
            } catch (IllegalArgumentException illegalArgumentException) {
              return new SubtitleTrack.CueList.EntryIterator(null);
            } 
          }
        };
    }
    
    public long nextTimeAfter(long param1Long) {
      try {
        SortedMap<Long, Vector<SubtitleTrack.Cue>> sortedMap = this.mCues.tailMap(Long.valueOf(1L + param1Long));
        if (sortedMap != null)
          return ((Long)sortedMap.firstKey()).longValue(); 
        return -1L;
      } catch (IllegalArgumentException illegalArgumentException) {
        return -1L;
      } catch (NoSuchElementException noSuchElementException) {
        return -1L;
      } 
    }
    
    class EntryIterator implements Iterator<Pair<Long, SubtitleTrack.Cue>> {
      private long mCurrentTimeMs;
      
      private boolean mDone;
      
      private Pair<Long, SubtitleTrack.Cue> mLastEntry;
      
      private Iterator<SubtitleTrack.Cue> mLastListIterator;
      
      private Iterator<SubtitleTrack.Cue> mListIterator;
      
      private SortedMap<Long, Vector<SubtitleTrack.Cue>> mRemainingCues;
      
      final SubtitleTrack.CueList this$0;
      
      public boolean hasNext() {
        return this.mDone ^ true;
      }
      
      public Pair<Long, SubtitleTrack.Cue> next() {
        if (!this.mDone) {
          long l = this.mCurrentTimeMs;
          this.mLastEntry = new Pair(Long.valueOf(l), this.mListIterator.next());
          Iterator<SubtitleTrack.Cue> iterator = this.mListIterator;
          if (!iterator.hasNext())
            nextKey(); 
          return this.mLastEntry;
        } 
        throw new NoSuchElementException("");
      }
      
      public void remove() {
        if (this.mLastListIterator != null) {
          l = ((SubtitleTrack.Cue)this.mLastEntry.second).mEndTimeMs;
          Long long_ = (Long)this.mLastEntry.first;
          if (l == long_.longValue()) {
            this.mLastListIterator.remove();
            this.mLastListIterator = null;
            if (((Vector)SubtitleTrack.CueList.this.mCues.get(this.mLastEntry.first)).size() == 0)
              SubtitleTrack.CueList.this.mCues.remove(this.mLastEntry.first); 
            SubtitleTrack.Cue cue = (SubtitleTrack.Cue)this.mLastEntry.second;
            SubtitleTrack.CueList.this.removeEvent(cue, cue.mStartTimeMs);
            if (cue.mInnerTimesMs != null)
              for (long l : cue.mInnerTimesMs)
                SubtitleTrack.CueList.this.removeEvent(cue, l);  
            return;
          } 
        } 
        throw new IllegalStateException("");
      }
      
      public EntryIterator(SortedMap<Long, Vector<SubtitleTrack.Cue>> param2SortedMap) {
        if (SubtitleTrack.CueList.this.DEBUG) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(param2SortedMap);
          stringBuilder.append("");
          Log.v("CueList", stringBuilder.toString());
        } 
        this.mRemainingCues = param2SortedMap;
        this.mLastListIterator = null;
        nextKey();
      }
      
      private void nextKey() {
        try {
          while (this.mRemainingCues != null) {
            long l = ((Long)this.mRemainingCues.firstKey()).longValue();
            SortedMap<Long, Vector<SubtitleTrack.Cue>> sortedMap = this.mRemainingCues;
            this.mListIterator = ((Vector<SubtitleTrack.Cue>)sortedMap.get(Long.valueOf(l))).iterator();
            try {
              sortedMap = this.mRemainingCues;
              l = this.mCurrentTimeMs;
              this.mRemainingCues = sortedMap.tailMap(Long.valueOf(l + 1L));
            } catch (IllegalArgumentException illegalArgumentException) {
              this.mRemainingCues = null;
            } 
            this.mDone = false;
            if (this.mListIterator.hasNext())
              return; 
          } 
          NoSuchElementException noSuchElementException = new NoSuchElementException();
          this("");
          throw noSuchElementException;
        } catch (NoSuchElementException noSuchElementException) {
          this.mDone = true;
          this.mRemainingCues = null;
          this.mListIterator = null;
          return;
        } 
      }
    }
    
    CueList() {
      this.mCues = new TreeMap<>();
    }
  }
  
  class Cue {
    public long mEndTimeMs;
    
    public long[] mInnerTimesMs;
    
    public Cue mNextInRun;
    
    public long mRunID;
    
    public long mStartTimeMs;
    
    public void onTime(long param1Long) {}
  }
  
  protected void finishedRun(long paramLong) {
    if (paramLong != 0L && paramLong != -1L) {
      Run run = (Run)this.mRunsByID.get(paramLong);
      if (run != null)
        run.storeByEndTimeMs(this.mRunsByEndTime); 
    } 
  }
  
  public void setRunDiscardTimeMs(long paramLong1, long paramLong2) {
    if (paramLong1 != 0L && paramLong1 != -1L) {
      Run run = (Run)this.mRunsByID.get(paramLong1);
      if (run != null) {
        run.mEndTimeMs = paramLong2;
        run.storeByEndTimeMs(this.mRunsByEndTime);
      } 
    } 
  }
  
  public int getTrackType() {
    byte b;
    if (getRenderingWidget() == null) {
      b = 3;
    } else {
      b = 4;
    } 
    return b;
  }
  
  public abstract RenderingWidget getRenderingWidget();
  
  public abstract void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong);
  
  public abstract void updateView(Vector<Cue> paramVector);
  
  class Run {
    static final boolean $assertionsDisabled = false;
    
    public long mEndTimeMs = -1L;
    
    public SubtitleTrack.Cue mFirstCue;
    
    public Run mNextRunAtEndTimeMs;
    
    public Run mPrevRunAtEndTimeMs;
    
    public long mRunID = 0L;
    
    private long mStoredEndTimeMs = -1L;
    
    public void storeByEndTimeMs(LongSparseArray<Run> param1LongSparseArray) {
      int i = param1LongSparseArray.indexOfKey(this.mStoredEndTimeMs);
      if (i >= 0) {
        if (this.mPrevRunAtEndTimeMs == null) {
          Run run = this.mNextRunAtEndTimeMs;
          if (run == null) {
            param1LongSparseArray.removeAt(i);
          } else {
            param1LongSparseArray.setValueAt(i, run);
          } 
        } 
        removeAtEndTimeMs();
      } 
      long l = this.mEndTimeMs;
      if (l >= 0L) {
        this.mPrevRunAtEndTimeMs = null;
        Run run = (Run)param1LongSparseArray.get(l);
        if (run != null)
          run.mPrevRunAtEndTimeMs = this; 
        param1LongSparseArray.put(this.mEndTimeMs, this);
        this.mStoredEndTimeMs = this.mEndTimeMs;
      } 
    }
    
    public void removeAtEndTimeMs() {
      Run run1 = this.mPrevRunAtEndTimeMs;
      Run run2 = this.mPrevRunAtEndTimeMs;
      if (run2 != null) {
        run2.mNextRunAtEndTimeMs = this.mNextRunAtEndTimeMs;
        this.mPrevRunAtEndTimeMs = null;
      } 
      run2 = this.mNextRunAtEndTimeMs;
      if (run2 != null) {
        run2.mPrevRunAtEndTimeMs = run1;
        this.mNextRunAtEndTimeMs = null;
      } 
    }
    
    private Run() {}
  }
  
  class RenderingWidget {
    public abstract void draw(Canvas param1Canvas);
    
    public abstract void onAttachedToWindow();
    
    public abstract void onDetachedFromWindow();
    
    public abstract void setOnChangedListener(OnChangedListener param1OnChangedListener);
    
    public abstract void setSize(int param1Int1, int param1Int2);
    
    public abstract void setVisible(boolean param1Boolean);
    
    public static interface OnChangedListener {
      void onChanged(SubtitleTrack.RenderingWidget param2RenderingWidget);
    }
  }
}
