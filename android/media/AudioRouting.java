package android.media;

import android.os.Handler;

public interface AudioRouting {
  void addOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler);
  
  AudioDeviceInfo getPreferredDevice();
  
  AudioDeviceInfo getRoutedDevice();
  
  void removeOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener);
  
  boolean setPreferredDevice(AudioDeviceInfo paramAudioDeviceInfo);
  
  public static interface OnRoutingChangedListener {
    void onRoutingChanged(AudioRouting param1AudioRouting);
  }
}
