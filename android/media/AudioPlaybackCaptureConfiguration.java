package android.media;

import android.media.audiopolicy.AudioMix;
import android.media.audiopolicy.AudioMixingRule;
import android.media.projection.MediaProjection;
import android.os.RemoteException;
import com.android.internal.util.Preconditions;
import java.util.function.ToIntFunction;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public final class AudioPlaybackCaptureConfiguration {
  private final AudioMixingRule mAudioMixingRule;
  
  private final MediaProjection mProjection;
  
  private AudioPlaybackCaptureConfiguration(AudioMixingRule paramAudioMixingRule, MediaProjection paramMediaProjection) {
    this.mAudioMixingRule = paramAudioMixingRule;
    this.mProjection = paramMediaProjection;
  }
  
  public MediaProjection getMediaProjection() {
    return this.mProjection;
  }
  
  public int[] getMatchingUsages() {
    return getIntPredicates(1, (ToIntFunction<AudioMixingRule.AudioMixMatchCriterion>)_$$Lambda$AudioPlaybackCaptureConfiguration$JTl6zvocylA2c1D_zvOeuHFBuXc.INSTANCE);
  }
  
  public int[] getMatchingUids() {
    return getIntPredicates(4, (ToIntFunction<AudioMixingRule.AudioMixMatchCriterion>)_$$Lambda$AudioPlaybackCaptureConfiguration$lExv8IaPEEDrexk0ZpgJOYug6js.INSTANCE);
  }
  
  public int[] getExcludeUsages() {
    return getIntPredicates(32769, (ToIntFunction<AudioMixingRule.AudioMixMatchCriterion>)_$$Lambda$AudioPlaybackCaptureConfiguration$XNNDt4F3y6GuPLTW14ozD51SjGw.INSTANCE);
  }
  
  public int[] getExcludeUids() {
    return getIntPredicates(32772, (ToIntFunction<AudioMixingRule.AudioMixMatchCriterion>)_$$Lambda$AudioPlaybackCaptureConfiguration$OOmSH4uNi7bw_cxkUNQt_briVbM.INSTANCE);
  }
  
  private int[] getIntPredicates(int paramInt, ToIntFunction<AudioMixingRule.AudioMixMatchCriterion> paramToIntFunction) {
    Stream<AudioMixingRule.AudioMixMatchCriterion> stream1 = this.mAudioMixingRule.getCriteria().stream();
    _$$Lambda$AudioPlaybackCaptureConfiguration$CbJtSEmgD3swIYuOWlTCDMPxK1s _$$Lambda$AudioPlaybackCaptureConfiguration$CbJtSEmgD3swIYuOWlTCDMPxK1s = new _$$Lambda$AudioPlaybackCaptureConfiguration$CbJtSEmgD3swIYuOWlTCDMPxK1s(paramInt);
    Stream<AudioMixingRule.AudioMixMatchCriterion> stream2 = stream1.filter(_$$Lambda$AudioPlaybackCaptureConfiguration$CbJtSEmgD3swIYuOWlTCDMPxK1s);
    IntStream intStream = stream2.mapToInt(paramToIntFunction);
    return intStream.toArray();
  }
  
  AudioMix createAudioMix(AudioFormat paramAudioFormat) {
    AudioMix.Builder builder2 = new AudioMix.Builder(this.mAudioMixingRule);
    AudioMix.Builder builder1 = builder2.setFormat(paramAudioFormat);
    builder1 = builder1.setRouteFlags(3);
    return builder1.build();
  }
  
  public static final class Builder {
    private int mUsageMatchType = 0;
    
    private int mUidMatchType = 0;
    
    private final MediaProjection mProjection;
    
    private final AudioMixingRule.Builder mAudioMixingRuleBuilder;
    
    private static final int MATCH_TYPE_UNSPECIFIED = 0;
    
    private static final int MATCH_TYPE_INCLUSIVE = 1;
    
    private static final int MATCH_TYPE_EXCLUSIVE = 2;
    
    private static final String ERROR_MESSAGE_START_ACTIVITY_FAILED = "startActivityForResult failed";
    
    private static final String ERROR_MESSAGE_NON_AUDIO_PROJECTION = "MediaProjection can not project audio";
    
    private static final String ERROR_MESSAGE_MISMATCHED_RULES = "Inclusive and exclusive usage rules cannot be combined";
    
    public Builder(MediaProjection param1MediaProjection) {
      Preconditions.checkNotNull(param1MediaProjection);
      try {
        Preconditions.checkArgument(param1MediaProjection.getProjection().canProjectAudio(), "MediaProjection can not project audio");
        this.mProjection = param1MediaProjection;
        this.mAudioMixingRuleBuilder = new AudioMixingRule.Builder();
        return;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    }
    
    public Builder addMatchingUsage(int param1Int) {
      boolean bool;
      if (this.mUsageMatchType != 2) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Inclusive and exclusive usage rules cannot be combined");
      this.mAudioMixingRuleBuilder.addRule((new AudioAttributes.Builder()).setUsage(param1Int).build(), 1);
      this.mUsageMatchType = 1;
      return this;
    }
    
    public Builder addMatchingUid(int param1Int) {
      boolean bool;
      if (this.mUidMatchType != 2) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Inclusive and exclusive usage rules cannot be combined");
      this.mAudioMixingRuleBuilder.addMixRule(4, Integer.valueOf(param1Int));
      this.mUidMatchType = 1;
      return this;
    }
    
    public Builder excludeUsage(int param1Int) {
      boolean bool;
      if (this.mUsageMatchType != 1) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Inclusive and exclusive usage rules cannot be combined");
      AudioMixingRule.Builder builder = this.mAudioMixingRuleBuilder;
      AudioAttributes.Builder builder1 = new AudioAttributes.Builder();
      builder1 = builder1.setUsage(param1Int);
      AudioAttributes audioAttributes = builder1.build();
      builder.excludeRule(audioAttributes, 1);
      this.mUsageMatchType = 2;
      return this;
    }
    
    public Builder excludeUid(int param1Int) {
      int i = this.mUidMatchType;
      boolean bool = true;
      if (i == 1)
        bool = false; 
      Preconditions.checkState(bool, "Inclusive and exclusive usage rules cannot be combined");
      this.mAudioMixingRuleBuilder.excludeMixRule(4, Integer.valueOf(param1Int));
      this.mUidMatchType = 2;
      return this;
    }
    
    public AudioPlaybackCaptureConfiguration build() {
      return new AudioPlaybackCaptureConfiguration(this.mAudioMixingRuleBuilder.build(), this.mProjection);
    }
  }
}
