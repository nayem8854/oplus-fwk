package android.media;

public interface MediaScannerClient {
  void handleStringTag(String paramString1, String paramString2);
  
  void scanFile(String paramString, long paramLong1, long paramLong2, boolean paramBoolean1, boolean paramBoolean2);
  
  void setMimeType(String paramString);
}
