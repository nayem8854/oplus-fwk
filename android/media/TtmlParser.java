package android.media;

import android.text.TextUtils;
import android.util.Log;
import java.io.IOException;
import java.io.StringReader;
import java.util.LinkedList;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

class TtmlParser {
  private static final int DEFAULT_FRAMERATE = 30;
  
  private static final int DEFAULT_SUBFRAMERATE = 1;
  
  private static final int DEFAULT_TICKRATE = 1;
  
  static final String TAG = "TtmlParser";
  
  private long mCurrentRunId;
  
  private final TtmlNodeListener mListener;
  
  private XmlPullParser mParser;
  
  public TtmlParser(TtmlNodeListener paramTtmlNodeListener) {
    this.mListener = paramTtmlNodeListener;
  }
  
  public void parse(String paramString, long paramLong) throws XmlPullParserException, IOException {
    this.mParser = null;
    this.mCurrentRunId = paramLong;
    loadParser(paramString);
    parseTtml();
  }
  
  private void loadParser(String paramString) throws XmlPullParserException {
    XmlPullParserFactory xmlPullParserFactory = XmlPullParserFactory.newInstance();
    xmlPullParserFactory.setNamespaceAware(false);
    this.mParser = xmlPullParserFactory.newPullParser();
    StringReader stringReader = new StringReader(paramString);
    this.mParser.setInput(stringReader);
  }
  
  private void extractAttribute(XmlPullParser paramXmlPullParser, int paramInt, StringBuilder paramStringBuilder) {
    paramStringBuilder.append(" ");
    paramStringBuilder.append(paramXmlPullParser.getAttributeName(paramInt));
    paramStringBuilder.append("=\"");
    paramStringBuilder.append(paramXmlPullParser.getAttributeValue(paramInt));
    paramStringBuilder.append("\"");
  }
  
  private void parseTtml() throws XmlPullParserException, IOException {
    LinkedList<TtmlNode> linkedList = new LinkedList();
    byte b = 0;
    boolean bool = true;
    while (!isEndOfDoc()) {
      int i = this.mParser.getEventType();
      TtmlNode ttmlNode = linkedList.peekLast();
      if (bool) {
        StringBuilder stringBuilder;
        if (i == 2) {
          if (!isSupportedTag(this.mParser.getName())) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported tag ");
            stringBuilder.append(this.mParser.getName());
            stringBuilder.append(" is ignored.");
            Log.w("TtmlParser", stringBuilder.toString());
            b++;
            bool = false;
          } else {
            TtmlNode ttmlNode1 = parseNode((TtmlNode)stringBuilder);
            linkedList.addLast(ttmlNode1);
            if (stringBuilder != null)
              ((TtmlNode)stringBuilder).mChildren.add(ttmlNode1); 
          } 
        } else if (i == 4) {
          String str = TtmlUtils.applyDefaultSpacePolicy(this.mParser.getText());
          if (!TextUtils.isEmpty(str))
            ((TtmlNode)stringBuilder).mChildren.add(new TtmlNode("#pcdata", "", str, 0L, Long.MAX_VALUE, (TtmlNode)stringBuilder, this.mCurrentRunId)); 
        } else if (i == 3) {
          if (this.mParser.getName().equals("p")) {
            this.mListener.onTtmlNodeParsed(linkedList.getLast());
          } else if (this.mParser.getName().equals("tt")) {
            this.mListener.onRootNodeParsed(linkedList.getLast());
          } 
          linkedList.removeLast();
        } 
      } else if (i == 2) {
        b++;
      } else {
        b--;
        if (i == 3 && b == 0)
          bool = true; 
      } 
      this.mParser.next();
    } 
  }
  
  private TtmlNode parseNode(TtmlNode paramTtmlNode) throws XmlPullParserException, IOException {
    int i = this.mParser.getEventType();
    if (i != 2)
      return null; 
    StringBuilder stringBuilder = new StringBuilder();
    long l1 = 0L;
    long l2 = Long.MAX_VALUE;
    long l3;
    for (i = 0, l3 = 0L; i < this.mParser.getAttributeCount(); i++) {
      String str1 = this.mParser.getAttributeName(i);
      String str2 = this.mParser.getAttributeValue(i);
      str1 = str1.replaceFirst("^.*:", "");
      if (str1.equals("begin")) {
        l1 = TtmlUtils.parseTimeExpression(str2, 30, 1, 1);
      } else if (str1.equals("end")) {
        l2 = TtmlUtils.parseTimeExpression(str2, 30, 1, 1);
      } else if (str1.equals("dur")) {
        l3 = TtmlUtils.parseTimeExpression(str2, 30, 1, 1);
      } else {
        extractAttribute(this.mParser, i, stringBuilder);
      } 
    } 
    if (paramTtmlNode != null) {
      l1 += paramTtmlNode.mStartTimeMs;
      if (l2 != Long.MAX_VALUE)
        l2 += paramTtmlNode.mStartTimeMs; 
    } 
    long l4 = l2;
    if (l3 > 0L) {
      if (l2 != Long.MAX_VALUE)
        Log.e("TtmlParser", "'dur' and 'end' attributes are defined at the same time.'end' value is ignored."); 
      l4 = l1 + l3;
    } 
    if (paramTtmlNode != null)
      if (l4 == Long.MAX_VALUE && paramTtmlNode.mEndTimeMs != Long.MAX_VALUE && l4 > paramTtmlNode.mEndTimeMs)
        l4 = paramTtmlNode.mEndTimeMs;  
    paramTtmlNode = new TtmlNode(this.mParser.getName(), stringBuilder.toString(), null, l1, l4, paramTtmlNode, this.mCurrentRunId);
    return paramTtmlNode;
  }
  
  private boolean isEndOfDoc() throws XmlPullParserException {
    int i = this.mParser.getEventType();
    boolean bool = true;
    if (i != 1)
      bool = false; 
    return bool;
  }
  
  private static boolean isSupportedTag(String paramString) {
    if (paramString.equals("tt") || paramString.equals("head") || 
      paramString.equals("body") || paramString.equals("div") || 
      paramString.equals("p") || paramString.equals("span") || 
      paramString.equals("br") || paramString.equals("style") || 
      paramString.equals("styling") || paramString.equals("layout") || 
      paramString.equals("region") || paramString.equals("metadata") || 
      paramString.equals("smpte:image") || paramString.equals("smpte:data") || 
      paramString.equals("smpte:information"))
      return true; 
    return false;
  }
}
