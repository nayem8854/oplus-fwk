package android.media;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.media.audiopolicy.AudioMix;
import android.media.audiopolicy.AudioPolicy;
import android.media.projection.MediaProjection;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.ISecurityPermissionService;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.os.Process;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.os.SystemProperties;
import android.util.ArrayMap;
import android.util.Log;
import android.util.Pair;
import android.util.SeempLog;
import com.android.internal.util.Preconditions;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Executor;

public class AudioRecord implements AudioRouting, MicrophoneDirection, AudioRecordingMonitor, AudioRecordingMonitorClient {
  private int mState = 0;
  
  private int mRecordingState = 1;
  
  private final Object mRecordingStateLock = new Object();
  
  private OnRecordPositionUpdateListener mPositionListener = null;
  
  private final Object mPositionListenerLock = new Object();
  
  private NativeEventHandler mEventHandler = null;
  
  private Looper mInitializationLooper = null;
  
  private int mNativeBufferSizeInBytes = 0;
  
  private int mSessionId = 0;
  
  private boolean mIsSubmixFullVolume = false;
  
  private static final int AUDIORECORD_ERROR_SETUP_INVALIDCHANNELMASK = -17;
  
  private static final int AUDIORECORD_ERROR_SETUP_INVALIDFORMAT = -18;
  
  private static final int AUDIORECORD_ERROR_SETUP_INVALIDSOURCE = -19;
  
  private static final int AUDIORECORD_ERROR_SETUP_NATIVEINITFAILED = -20;
  
  private static final int AUDIORECORD_ERROR_SETUP_ZEROFRAMECOUNT = -16;
  
  public static final int ERROR = -1;
  
  public static final int ERROR_BAD_VALUE = -2;
  
  public static final int ERROR_DEAD_OBJECT = -6;
  
  public static final int ERROR_INVALID_OPERATION = -3;
  
  private static final int NATIVE_EVENT_MARKER = 2;
  
  private static final int NATIVE_EVENT_NEW_POS = 3;
  
  public static final int READ_BLOCKING = 0;
  
  public static final int READ_NON_BLOCKING = 1;
  
  public static final int RECORDSTATE_RECORDING = 3;
  
  public static final int RECORDSTATE_STOPPED = 1;
  
  public static final int STATE_INITIALIZED = 1;
  
  public static final int STATE_UNINITIALIZED = 0;
  
  public static final String SUBMIX_FIXED_VOLUME = "fixedVolume";
  
  public static final int SUCCESS = 0;
  
  private static final String TAG = "android.media.AudioRecord";
  
  private AudioAttributes mAudioAttributes;
  
  private AudioPolicy mAudioCapturePolicy;
  
  private int mAudioFormat;
  
  private int mChannelCount;
  
  private int mChannelIndexMask;
  
  private int mChannelMask;
  
  private final IBinder mICallBack;
  
  private long mNativeCallbackCookie;
  
  private long mNativeDeviceCallback;
  
  private long mNativeRecorderInJavaObj;
  
  private AudioDeviceInfo mPreferredDevice;
  
  private int mRecordSource;
  
  AudioRecordingMonitorImpl mRecordingInfoImpl;
  
  private ArrayMap<AudioRouting.OnRoutingChangedListener, NativeRoutingEventHandlerDelegate> mRoutingChangeListeners;
  
  private int mSampleRate;
  
  public AudioRecord(int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5) throws IllegalArgumentException {
    this(audioAttributes, audioFormat, paramInt5, 0);
  }
  
  private String getCurrentOpPackageName() {
    String str = ActivityThread.currentOpPackageName();
    if (str != null)
      return str; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("uid:");
    stringBuilder.append(Binder.getCallingUid());
    return stringBuilder.toString();
  }
  
  private void unregisterAudioPolicyOnRelease(AudioPolicy paramAudioPolicy) {
    this.mAudioCapturePolicy = paramAudioPolicy;
  }
  
  void deferred_connect(long paramLong) {
    if (this.mState != 1) {
      int[] arrayOfInt = new int[1];
      arrayOfInt[0] = 0;
      WeakReference<AudioRecord> weakReference = new WeakReference<>(this);
      String str = ActivityThread.currentOpPackageName();
      int i = native_setup(weakReference, null, new int[] { 0 }, 0, 0, 0, 0, arrayOfInt, str, paramLong);
      if (i != 0) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Error code ");
        stringBuilder.append(i);
        stringBuilder.append(" when initializing native AudioRecord object.");
        loge(stringBuilder.toString());
        return;
      } 
      this.mSessionId = arrayOfInt[0];
      this.mState = 1;
    } 
  }
  
  class Builder {
    private int mSessionId = 0;
    
    private int mPrivacySensitive = -1;
    
    private AudioFormat mFormat;
    
    private int mBufferSizeInBytes;
    
    private AudioPlaybackCaptureConfiguration mAudioPlaybackCaptureConfiguration;
    
    private AudioAttributes mAttributes;
    
    private static final int PRIVACY_SENSITIVE_ENABLED = 1;
    
    private static final int PRIVACY_SENSITIVE_DISABLED = 0;
    
    private static final int PRIVACY_SENSITIVE_DEFAULT = -1;
    
    private static final String ERROR_MESSAGE_SOURCE_MISMATCH = "Cannot both set audio source and set playback capture config";
    
    public Builder setAudioSource(int param1Int) throws IllegalArgumentException {
      boolean bool;
      if (this.mAudioPlaybackCaptureConfiguration == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Cannot both set audio source and set playback capture config");
      if (param1Int >= 0 && 
        param1Int <= MediaRecorder.getAudioSourceMax()) {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        builder = builder.setInternalCapturePreset(param1Int);
        this.mAttributes = builder.build();
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid audio source ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    @SystemApi
    public Builder setAudioAttributes(AudioAttributes param1AudioAttributes) throws IllegalArgumentException {
      if (param1AudioAttributes != null) {
        if (param1AudioAttributes.getCapturePreset() != -1) {
          this.mAttributes = param1AudioAttributes;
          return this;
        } 
        throw new IllegalArgumentException("No valid capture preset in AudioAttributes argument");
      } 
      throw new IllegalArgumentException("Illegal null AudioAttributes argument");
    }
    
    public Builder setAudioFormat(AudioFormat param1AudioFormat) throws IllegalArgumentException {
      if (param1AudioFormat != null) {
        this.mFormat = param1AudioFormat;
        return this;
      } 
      throw new IllegalArgumentException("Illegal null AudioFormat argument");
    }
    
    public Builder setBufferSizeInBytes(int param1Int) throws IllegalArgumentException {
      if (param1Int > 0) {
        this.mBufferSizeInBytes = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid buffer size ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Builder setAudioPlaybackCaptureConfig(AudioPlaybackCaptureConfiguration param1AudioPlaybackCaptureConfiguration) {
      boolean bool;
      Preconditions.checkNotNull(param1AudioPlaybackCaptureConfiguration, "Illegal null AudioPlaybackCaptureConfiguration argument");
      if (this.mAttributes == null) {
        bool = true;
      } else {
        bool = false;
      } 
      Preconditions.checkState(bool, "Cannot both set audio source and set playback capture config");
      this.mAudioPlaybackCaptureConfiguration = param1AudioPlaybackCaptureConfiguration;
      return this;
    }
    
    public Builder setPrivacySensitive(boolean param1Boolean) {
      this.mPrivacySensitive = param1Boolean;
      return this;
    }
    
    @SystemApi
    public Builder setSessionId(int param1Int) throws IllegalArgumentException {
      if (param1Int >= 0) {
        this.mSessionId = param1Int;
        return this;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid session ID ");
      stringBuilder.append(param1Int);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    private AudioRecord buildAudioPlaybackCaptureRecord() {
      AudioMix audioMix = this.mAudioPlaybackCaptureConfiguration.createAudioMix(this.mFormat);
      MediaProjection mediaProjection = this.mAudioPlaybackCaptureConfiguration.getMediaProjection();
      AudioPolicy.Builder builder = new AudioPolicy.Builder(null);
      builder = builder.setMediaProjection(mediaProjection);
      AudioPolicy audioPolicy = builder.addMix(audioMix).build();
      int i = AudioManager.registerAudioPolicyStatic(audioPolicy);
      if (i == 0) {
        AudioRecord audioRecord = audioPolicy.createAudioRecordSink(audioMix);
        if (audioRecord != null) {
          audioRecord.unregisterAudioPolicyOnRelease(audioPolicy);
          return audioRecord;
        } 
        throw new UnsupportedOperationException("Cannot create AudioRecord");
      } 
      throw new UnsupportedOperationException("Error: could not register audio policy");
    }
    
    public AudioRecord build() throws UnsupportedOperationException {
      AudioFormat.Builder builder;
      if (this.mAudioPlaybackCaptureConfiguration != null)
        return buildAudioPlaybackCaptureRecord(); 
      AudioFormat audioFormat = this.mFormat;
      if (audioFormat == null) {
        builder = new AudioFormat.Builder();
        builder = builder.setEncoding(2);
        builder = builder.setChannelMask(16);
        this.mFormat = builder.build();
      } else {
        if (builder.getEncoding() == 0) {
          builder = new AudioFormat.Builder(this.mFormat);
          builder = builder.setEncoding(2);
          this.mFormat = builder.build();
        } 
        if (this.mFormat.getChannelMask() == 0) {
          AudioFormat audioFormat1 = this.mFormat;
          if (audioFormat1.getChannelIndexMask() == 0) {
            AudioFormat.Builder builder1 = new AudioFormat.Builder(this.mFormat);
            builder1 = builder1.setChannelMask(16);
            this.mFormat = builder1.build();
          } 
        } 
      } 
      AudioAttributes audioAttributes = this.mAttributes;
      boolean bool = false;
      if (audioAttributes == null) {
        AudioAttributes.Builder builder1 = new AudioAttributes.Builder();
        builder1 = builder1.setInternalCapturePreset(0);
        this.mAttributes = builder1.build();
      } 
      if (this.mPrivacySensitive != -1) {
        int i = this.mAttributes.getCapturePreset();
        if (i != 8 && i != 1998 && i != 3 && i != 2 && i != 4 && i != 1997) {
          AudioAttributes.Builder builder1 = new AudioAttributes.Builder(this.mAttributes);
          builder1 = builder1.setInternalCapturePreset(i);
          if (this.mPrivacySensitive == 1)
            bool = true; 
          builder1 = builder1.setPrivacySensitive(bool);
          this.mAttributes = builder1.build();
        } else {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot request private capture with source: ");
          stringBuilder.append(i);
          throw new UnsupportedOperationException(stringBuilder.toString());
        } 
      } 
      try {
        if (this.mBufferSizeInBytes == 0) {
          int i = this.mFormat.getChannelCount();
          AudioFormat audioFormat1 = this.mFormat;
          this.mBufferSizeInBytes = i * AudioFormat.getBytesPerSample(audioFormat1.getEncoding());
        } 
        AudioRecord audioRecord = new AudioRecord();
        this(this.mAttributes, this.mFormat, this.mBufferSizeInBytes, this.mSessionId);
        if (audioRecord.getState() != 0)
          return audioRecord; 
        UnsupportedOperationException unsupportedOperationException = new UnsupportedOperationException();
        this("Cannot create AudioRecord");
        throw unsupportedOperationException;
      } catch (IllegalArgumentException illegalArgumentException) {
        throw new UnsupportedOperationException(illegalArgumentException.getMessage());
      } 
    }
  }
  
  private static int getChannelMaskFromLegacyConfig(int paramInt, boolean paramBoolean) {
    // Byte code:
    //   0: iload_0
    //   1: iconst_1
    //   2: if_icmpeq -> 78
    //   5: iload_0
    //   6: iconst_2
    //   7: if_icmpeq -> 78
    //   10: iload_0
    //   11: iconst_3
    //   12: if_icmpeq -> 72
    //   15: iload_0
    //   16: bipush #12
    //   18: if_icmpeq -> 72
    //   21: iload_0
    //   22: bipush #16
    //   24: if_icmpeq -> 78
    //   27: iload_0
    //   28: bipush #28
    //   30: if_icmpeq -> 67
    //   33: iload_0
    //   34: bipush #48
    //   36: if_icmpeq -> 62
    //   39: iload_0
    //   40: ldc_w 6291468
    //   43: if_icmpne -> 51
    //   46: iload_0
    //   47: istore_2
    //   48: goto -> 81
    //   51: new java/lang/IllegalArgumentException
    //   54: dup
    //   55: ldc_w 'Unsupported channel configuration.'
    //   58: invokespecial <init> : (Ljava/lang/String;)V
    //   61: athrow
    //   62: iload_0
    //   63: istore_2
    //   64: goto -> 81
    //   67: iload_0
    //   68: istore_2
    //   69: goto -> 81
    //   72: bipush #12
    //   74: istore_2
    //   75: goto -> 81
    //   78: bipush #16
    //   80: istore_2
    //   81: iload_1
    //   82: ifne -> 109
    //   85: iload_0
    //   86: iconst_2
    //   87: if_icmpeq -> 98
    //   90: iload_0
    //   91: iconst_3
    //   92: if_icmpeq -> 98
    //   95: goto -> 109
    //   98: new java/lang/IllegalArgumentException
    //   101: dup
    //   102: ldc_w 'Unsupported deprecated configuration.'
    //   105: invokespecial <init> : (Ljava/lang/String;)V
    //   108: athrow
    //   109: iload_2
    //   110: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #804	-> 0
    //   #824	-> 46
    //   #825	-> 48
    //   #828	-> 51
    //   #815	-> 62
    //   #816	-> 64
    //   #820	-> 67
    //   #821	-> 69
    //   #812	-> 72
    //   #813	-> 75
    //   #808	-> 78
    //   #809	-> 81
    //   #831	-> 81
    //   #834	-> 98
    //   #837	-> 109
  }
  
  private void audioParamCheck(int paramInt1, int paramInt2, int paramInt3) throws IllegalArgumentException {
    if (paramInt1 >= 0 && (
      paramInt1 <= MediaRecorder.getAudioSourceMax() || paramInt1 == 1998 || paramInt1 == 1997 || paramInt1 == 1999)) {
      this.mRecordSource = paramInt1;
      if ((paramInt2 >= 4000 && paramInt2 <= 192000) || paramInt2 == 0) {
        this.mSampleRate = paramInt2;
        if (paramInt3 != 1) {
          if (paramInt3 != 2 && paramInt3 != 3 && paramInt3 != 4) {
            StringBuilder stringBuilder2;
            switch (paramInt3) {
              default:
                stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Unsupported sample encoding ");
                stringBuilder2.append(paramInt3);
                stringBuilder2.append(". Should be ENCODING_PCM_8BIT, ENCODING_PCM_16BIT, or ENCODING_PCM_FLOAT.");
                throw new IllegalArgumentException(stringBuilder2.toString());
              case 100:
              case 101:
              case 102:
              case 103:
              case 104:
              case 105:
                break;
            } 
          } 
          this.mAudioFormat = paramInt3;
        } else {
          this.mAudioFormat = 2;
        } 
        return;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append(paramInt2);
      stringBuilder1.append("Hz is not a supported sample rate.");
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid audio source ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void audioBuffSizeCheck(int paramInt) throws IllegalArgumentException {
    int i = this.mChannelCount, j = this.mAudioFormat;
    j = i * AudioFormat.getBytesPerSample(j);
    if (paramInt % j == 0 && paramInt >= 1) {
      this.mNativeBufferSizeInBytes = paramInt;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid audio buffer size ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" (frame size ");
    stringBuilder.append(j);
    stringBuilder.append(")");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void release() {
    try {
      stop();
    } catch (IllegalStateException illegalStateException) {}
    AudioPolicy audioPolicy = this.mAudioCapturePolicy;
    if (audioPolicy != null) {
      AudioManager.unregisterAudioPolicyAsyncStatic(audioPolicy);
      this.mAudioCapturePolicy = null;
    } 
    native_release();
    this.mState = 0;
  }
  
  protected void finalize() {
    release();
  }
  
  public int getSampleRate() {
    return this.mSampleRate;
  }
  
  public int getAudioSource() {
    return this.mRecordSource;
  }
  
  public int getAudioFormat() {
    return this.mAudioFormat;
  }
  
  public int getChannelConfiguration() {
    return this.mChannelMask;
  }
  
  public AudioFormat getFormat() {
    AudioFormat.Builder builder = new AudioFormat.Builder();
    int i = this.mSampleRate;
    builder = builder.setSampleRate(i);
    i = this.mAudioFormat;
    builder = builder.setEncoding(i);
    i = this.mChannelMask;
    if (i != 0)
      builder.setChannelMask(i); 
    i = this.mChannelIndexMask;
    if (i != 0)
      builder.setChannelIndexMask(i); 
    return builder.build();
  }
  
  public int getChannelCount() {
    return this.mChannelCount;
  }
  
  public int getState() {
    return this.mState;
  }
  
  public int getRecordingState() {
    synchronized (this.mRecordingStateLock) {
      return this.mRecordingState;
    } 
  }
  
  public int getBufferSizeInFrames() {
    return native_get_buffer_size_in_frames();
  }
  
  public int getNotificationMarkerPosition() {
    return native_get_marker_pos();
  }
  
  public int getPositionNotificationPeriod() {
    return native_get_pos_update_period();
  }
  
  public int getTimestamp(AudioTimestamp paramAudioTimestamp, int paramInt) {
    if (paramAudioTimestamp != null && (paramInt == 1 || paramInt == 0))
      return native_get_timestamp(paramAudioTimestamp, paramInt); 
    throw new IllegalArgumentException();
  }
  
  public static int getMinBufferSize(int paramInt1, int paramInt2, int paramInt3) {
    // Byte code:
    //   0: iload_1
    //   1: iconst_1
    //   2: if_icmpeq -> 83
    //   5: iload_1
    //   6: iconst_2
    //   7: if_icmpeq -> 83
    //   10: iload_1
    //   11: iconst_3
    //   12: if_icmpeq -> 78
    //   15: iload_1
    //   16: bipush #12
    //   18: if_icmpeq -> 78
    //   21: iload_1
    //   22: bipush #16
    //   24: if_icmpeq -> 83
    //   27: iload_1
    //   28: bipush #28
    //   30: if_icmpeq -> 73
    //   33: iload_1
    //   34: bipush #48
    //   36: if_icmpeq -> 78
    //   39: iload_1
    //   40: sipush #252
    //   43: if_icmpeq -> 67
    //   46: iload_1
    //   47: ldc_w 6291468
    //   50: if_icmpeq -> 62
    //   53: ldc_w 'getMinBufferSize(): Invalid channel configuration.'
    //   56: invokestatic loge : (Ljava/lang/String;)V
    //   59: bipush #-2
    //   61: ireturn
    //   62: iconst_4
    //   63: istore_3
    //   64: goto -> 85
    //   67: bipush #6
    //   69: istore_3
    //   70: goto -> 85
    //   73: iconst_3
    //   74: istore_3
    //   75: goto -> 85
    //   78: iconst_2
    //   79: istore_3
    //   80: goto -> 85
    //   83: iconst_1
    //   84: istore_3
    //   85: iload_0
    //   86: iload_3
    //   87: iload_2
    //   88: invokestatic native_get_min_buff_size : (III)I
    //   91: istore_3
    //   92: iload_3
    //   93: ifgt -> 215
    //   96: new java/lang/StringBuilder
    //   99: dup
    //   100: invokespecial <init> : ()V
    //   103: astore #4
    //   105: aload #4
    //   107: ldc_w 'EventID,5,SampleRateInHz,'
    //   110: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload #4
    //   116: iload_0
    //   117: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   120: pop
    //   121: aload #4
    //   123: ldc_w ',Channel,'
    //   126: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   129: pop
    //   130: aload #4
    //   132: iload_1
    //   133: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   136: pop
    //   137: aload #4
    //   139: ldc_w ',Format,'
    //   142: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   145: pop
    //   146: aload #4
    //   148: iload_2
    //   149: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   152: pop
    //   153: aload #4
    //   155: ldc_w ',ClientPid,'
    //   158: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   161: pop
    //   162: aload #4
    //   164: invokestatic getCallingPid : ()I
    //   167: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   170: pop
    //   171: aload #4
    //   173: ldc_w ',Size,'
    //   176: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   179: pop
    //   180: aload #4
    //   182: iload_3
    //   183: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   186: pop
    //   187: aload #4
    //   189: ldc_w ',ReportLevel,1'
    //   192: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   195: pop
    //   196: aload #4
    //   198: invokevirtual toString : ()Ljava/lang/String;
    //   201: astore #4
    //   203: aconst_null
    //   204: invokestatic getInstance : (Landroid/content/Context;)Lcom/oppo/atlas/OppoAtlasManager;
    //   207: sipush #257
    //   210: aload #4
    //   212: invokevirtual setEvent : (ILjava/lang/String;)V
    //   215: iload_3
    //   216: ifne -> 222
    //   219: bipush #-2
    //   221: ireturn
    //   222: iload_3
    //   223: iconst_m1
    //   224: if_icmpne -> 229
    //   227: iconst_m1
    //   228: ireturn
    //   229: iload_3
    //   230: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1112	-> 0
    //   #1113	-> 0
    //   #1141	-> 53
    //   #1142	-> 59
    //   #1136	-> 62
    //   #1137	-> 64
    //   #1131	-> 67
    //   #1132	-> 70
    //   #1127	-> 73
    //   #1128	-> 75
    //   #1122	-> 78
    //   #1123	-> 80
    //   #1117	-> 83
    //   #1118	-> 85
    //   #1145	-> 85
    //   #1149	-> 92
    //   #1150	-> 96
    //   #1151	-> 162
    //   #1152	-> 203
    //   #1157	-> 215
    //   #1158	-> 219
    //   #1160	-> 222
    //   #1161	-> 227
    //   #1164	-> 229
  }
  
  public int getAudioSessionId() {
    return this.mSessionId;
  }
  
  public boolean isPrivacySensitive() {
    boolean bool;
    if ((this.mAudioAttributes.getAllFlags() & 0x2000) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public void startRecording() throws IllegalStateException {
    SeempLog.record(70);
    if (SystemProperties.getBoolean("persist.sys.permission.enable", false))
      try {
        IBinder iBinder = ServiceManager.getService("security_permission");
        ISecurityPermissionService iSecurityPermissionService = ISecurityPermissionService.Stub.asInterface(iBinder);
        if (!iSecurityPermissionService.checkOplusPermission("android.permission.RECORD_AUDIO", Process.myPid(), Process.myUid())) {
          Log.d("android.media.AudioRecord", "permission denied : RECORD_AUDIO");
          IllegalStateException illegalStateException = new IllegalStateException();
          this("permission denied");
          throw illegalStateException;
        } 
      } catch (RemoteException remoteException) {
        Log.e("android.media.AudioRecord", "check error.");
      }  
    if (this.mState == 1)
      synchronized (this.mRecordingStateLock) {
        if (native_start(0, 0) == 0) {
          handleFullVolumeRec(true);
          this.mRecordingState = 3;
        } 
        return;
      }  
    throw new IllegalStateException("startRecording() called on an uninitialized AudioRecord.");
  }
  
  public void startRecording(MediaSyncEvent paramMediaSyncEvent) throws IllegalStateException {
    SeempLog.record(70);
    if (this.mState == 1) {
      if (SystemProperties.getBoolean("persist.sys.permission.enable", false))
        try {
          IBinder iBinder = ServiceManager.getService("security_permission");
          ISecurityPermissionService iSecurityPermissionService = ISecurityPermissionService.Stub.asInterface(iBinder);
          if (!iSecurityPermissionService.checkOplusPermission("android.permission.RECORD_AUDIO", Process.myPid(), Process.myUid())) {
            Log.d("android.media.AudioRecord", "permission denied : RECORD_AUDIO");
            IllegalStateException illegalStateException = new IllegalStateException();
            this("permission denied");
            throw illegalStateException;
          } 
        } catch (RemoteException remoteException) {
          Log.e("android.media.AudioRecord", "check error.");
        }  
      synchronized (this.mRecordingStateLock) {
        if (native_start(paramMediaSyncEvent.getType(), paramMediaSyncEvent.getAudioSessionId()) == 0) {
          handleFullVolumeRec(true);
          this.mRecordingState = 3;
        } 
        return;
      } 
    } 
    throw new IllegalStateException("startRecording() called on an uninitialized AudioRecord.");
  }
  
  public void stop() throws IllegalStateException {
    if (this.mState == 1)
      synchronized (this.mRecordingStateLock) {
        handleFullVolumeRec(false);
        native_stop();
        this.mRecordingState = 1;
        return;
      }  
    throw new IllegalStateException("stop() called on an uninitialized AudioRecord.");
  }
  
  AudioRecord(long paramLong) {
    this.mICallBack = new Binder();
    this.mRoutingChangeListeners = new ArrayMap();
    this.mPreferredDevice = null;
    this.mRecordingInfoImpl = new AudioRecordingMonitorImpl(this);
    this.mNativeRecorderInJavaObj = 0L;
    this.mNativeCallbackCookie = 0L;
    this.mNativeDeviceCallback = 0L;
    if (paramLong != 0L) {
      deferred_connect(paramLong);
    } else {
      this.mState = 0;
    } 
  }
  
  @SystemApi
  public AudioRecord(AudioAttributes paramAudioAttributes, AudioFormat paramAudioFormat, int paramInt1, int paramInt2) throws IllegalArgumentException {
    this.mICallBack = new Binder();
    this.mRoutingChangeListeners = new ArrayMap();
    this.mPreferredDevice = null;
    this.mRecordingInfoImpl = new AudioRecordingMonitorImpl(this);
    this.mRecordingState = 1;
    if (paramAudioAttributes != null) {
      if (paramAudioFormat != null) {
        Looper looper = Looper.myLooper();
        if (looper == null)
          this.mInitializationLooper = Looper.getMainLooper(); 
        if (paramAudioAttributes.getCapturePreset() == 8) {
          AudioAttributes.Builder builder = new AudioAttributes.Builder();
          Iterator<String> iterator = paramAudioAttributes.getTags().iterator();
          while (iterator.hasNext()) {
            String str1 = iterator.next();
            if (str1.equalsIgnoreCase("fixedVolume")) {
              this.mIsSubmixFullVolume = true;
              Log.v("android.media.AudioRecord", "Will record from REMOTE_SUBMIX at full fixed volume");
              continue;
            } 
            builder.addTag(str1);
          } 
          builder.setInternalCapturePreset(paramAudioAttributes.getCapturePreset());
          this.mAudioAttributes = builder.build();
        } else {
          this.mAudioAttributes = paramAudioAttributes;
        } 
        int i = paramAudioFormat.getSampleRate();
        if (i == 0)
          i = 0; 
        if ((paramAudioFormat.getPropertySetMask() & 0x1) != 0) {
          j = paramAudioFormat.getEncoding();
        } else {
          j = 1;
        } 
        audioParamCheck(paramAudioAttributes.getCapturePreset(), i, j);
        if ((paramAudioFormat.getPropertySetMask() & 0x8) != 0) {
          this.mChannelIndexMask = paramAudioFormat.getChannelIndexMask();
          this.mChannelCount = paramAudioFormat.getChannelCount();
        } 
        if ((paramAudioFormat.getPropertySetMask() & 0x4) != 0) {
          this.mChannelMask = getChannelMaskFromLegacyConfig(paramAudioFormat.getChannelMask(), false);
          this.mChannelCount = paramAudioFormat.getChannelCount();
        } else if (this.mChannelIndexMask == 0) {
          this.mChannelMask = i = getChannelMaskFromLegacyConfig(1, false);
          this.mChannelCount = AudioFormat.channelCountFromInChannelMask(i);
        } 
        audioBuffSizeCheck(paramInt1);
        int[] arrayOfInt2 = new int[1];
        arrayOfInt2[0] = this.mSampleRate;
        int[] arrayOfInt1 = new int[1];
        arrayOfInt1[0] = paramInt2;
        WeakReference<AudioRecord> weakReference = new WeakReference<>(this);
        paramAudioAttributes = this.mAudioAttributes;
        paramInt1 = this.mChannelMask;
        paramInt2 = this.mChannelIndexMask;
        i = this.mAudioFormat;
        int j = this.mNativeBufferSizeInBytes;
        String str = getCurrentOpPackageName();
        paramInt1 = native_setup(weakReference, paramAudioAttributes, arrayOfInt2, paramInt1, paramInt2, i, j, arrayOfInt1, str, 0L);
        if (paramInt1 != 0) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("Error code ");
          stringBuilder.append(paramInt1);
          stringBuilder.append(" when initializing native AudioRecord object.");
          loge(stringBuilder.toString());
          return;
        } 
        this.mSampleRate = arrayOfInt2[0];
        this.mSessionId = arrayOfInt1[0];
        this.mState = 1;
        return;
      } 
      throw new IllegalArgumentException("Illegal null AudioFormat");
    } 
    throw new IllegalArgumentException("Illegal null AudioAttributes");
  }
  
  private void handleFullVolumeRec(boolean paramBoolean) {
    if (!this.mIsSubmixFullVolume)
      return; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService = IAudioService.Stub.asInterface(iBinder);
    try {
      iAudioService.forceRemoteSubmixFullVolume(paramBoolean, this.mICallBack);
    } catch (RemoteException remoteException) {
      Log.e("android.media.AudioRecord", "Error talking to AudioService when handling full submix volume", (Throwable)remoteException);
    } 
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    return read(paramArrayOfbyte, paramInt1, paramInt2, 0);
  }
  
  public int read(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, int paramInt3) {
    int i = this.mState;
    boolean bool = true;
    if (i != 1 || this.mAudioFormat == 4)
      return -3; 
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioRecord", "AudioRecord.read() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOfbyte == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOfbyte.length)
      return -2; 
    if (paramInt3 != 0)
      bool = false; 
    return native_read_in_byte_array(paramArrayOfbyte, paramInt1, paramInt2, bool);
  }
  
  public int read(short[] paramArrayOfshort, int paramInt1, int paramInt2) {
    return read(paramArrayOfshort, paramInt1, paramInt2, 0);
  }
  
  public int read(short[] paramArrayOfshort, int paramInt1, int paramInt2, int paramInt3) {
    int i = this.mState;
    boolean bool = true;
    if (i != 1 || this.mAudioFormat == 4)
      return -3; 
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioRecord", "AudioRecord.read() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOfshort == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOfshort.length)
      return -2; 
    if (paramInt3 != 0)
      bool = false; 
    return native_read_in_short_array(paramArrayOfshort, paramInt1, paramInt2, bool);
  }
  
  public int read(float[] paramArrayOffloat, int paramInt1, int paramInt2, int paramInt3) {
    if (this.mState == 0) {
      Log.e("android.media.AudioRecord", "AudioRecord.read() called in invalid state STATE_UNINITIALIZED");
      return -3;
    } 
    if (this.mAudioFormat != 4) {
      Log.e("android.media.AudioRecord", "AudioRecord.read(float[] ...) requires format ENCODING_PCM_FLOAT");
      return -3;
    } 
    boolean bool = true;
    if (paramInt3 != 0 && paramInt3 != 1) {
      Log.e("android.media.AudioRecord", "AudioRecord.read() called with invalid blocking mode");
      return -2;
    } 
    if (paramArrayOffloat == null || paramInt1 < 0 || paramInt2 < 0 || paramInt1 + paramInt2 < 0 || paramInt1 + paramInt2 > paramArrayOffloat.length)
      return -2; 
    if (paramInt3 != 0)
      bool = false; 
    return native_read_in_float_array(paramArrayOffloat, paramInt1, paramInt2, bool);
  }
  
  public int read(ByteBuffer paramByteBuffer, int paramInt) {
    return read(paramByteBuffer, paramInt, 0);
  }
  
  public int read(ByteBuffer paramByteBuffer, int paramInt1, int paramInt2) {
    int i = this.mState;
    boolean bool = true;
    if (i != 1)
      return -3; 
    if (paramInt2 != 0 && paramInt2 != 1) {
      Log.e("android.media.AudioRecord", "AudioRecord.read() called with invalid blocking mode");
      return -2;
    } 
    if (paramByteBuffer == null || paramInt1 < 0)
      return -2; 
    if (paramInt2 != 0)
      bool = false; 
    return native_read_in_direct_buffer(paramByteBuffer, paramInt1, bool);
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener paramOnRecordPositionUpdateListener) {
    setRecordPositionUpdateListener(paramOnRecordPositionUpdateListener, null);
  }
  
  public void setRecordPositionUpdateListener(OnRecordPositionUpdateListener paramOnRecordPositionUpdateListener, Handler paramHandler) {
    synchronized (this.mPositionListenerLock) {
      this.mPositionListener = paramOnRecordPositionUpdateListener;
      if (paramOnRecordPositionUpdateListener != null) {
        if (paramHandler != null) {
          NativeEventHandler nativeEventHandler = new NativeEventHandler();
          this(this, this, paramHandler.getLooper());
          this.mEventHandler = nativeEventHandler;
        } else {
          NativeEventHandler nativeEventHandler = new NativeEventHandler();
          this(this, this, this.mInitializationLooper);
          this.mEventHandler = nativeEventHandler;
        } 
      } else {
        this.mEventHandler = null;
      } 
      return;
    } 
  }
  
  public int setNotificationMarkerPosition(int paramInt) {
    if (this.mState == 0)
      return -3; 
    return native_set_marker_pos(paramInt);
  }
  
  public AudioDeviceInfo getRoutedDevice() {
    int i = native_getRoutedDeviceId();
    if (i == 0)
      return null; 
    AudioDeviceInfo[] arrayOfAudioDeviceInfo = AudioManager.getDevicesStatic(1);
    for (byte b = 0; b < arrayOfAudioDeviceInfo.length; b++) {
      if (arrayOfAudioDeviceInfo[b].getId() == i)
        return arrayOfAudioDeviceInfo[b]; 
    } 
    return null;
  }
  
  private void testEnableNativeRoutingCallbacksLocked() {
    if (this.mRoutingChangeListeners.size() == 0)
      native_enableDeviceCallback(); 
  }
  
  private void testDisableNativeRoutingCallbacksLocked() {
    if (this.mRoutingChangeListeners.size() == 0)
      native_disableDeviceCallback(); 
  }
  
  public void addOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 73
    //   11: aload_0
    //   12: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   15: aload_1
    //   16: invokevirtual containsKey : (Ljava/lang/Object;)Z
    //   19: ifne -> 73
    //   22: aload_0
    //   23: invokespecial testEnableNativeRoutingCallbacksLocked : ()V
    //   26: aload_0
    //   27: getfield mRoutingChangeListeners : Landroid/util/ArrayMap;
    //   30: astore #4
    //   32: new android/media/NativeRoutingEventHandlerDelegate
    //   35: astore #5
    //   37: aload_2
    //   38: ifnull -> 44
    //   41: goto -> 56
    //   44: new android/os/Handler
    //   47: dup
    //   48: aload_0
    //   49: getfield mInitializationLooper : Landroid/os/Looper;
    //   52: invokespecial <init> : (Landroid/os/Looper;)V
    //   55: astore_2
    //   56: aload #5
    //   58: aload_0
    //   59: aload_1
    //   60: aload_2
    //   61: invokespecial <init> : (Landroid/media/AudioRouting;Landroid/media/AudioRouting$OnRoutingChangedListener;Landroid/os/Handler;)V
    //   64: aload #4
    //   66: aload_1
    //   67: aload #5
    //   69: invokevirtual put : (Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    //   72: pop
    //   73: aload_3
    //   74: monitorexit
    //   75: return
    //   76: astore_1
    //   77: aload_3
    //   78: monitorexit
    //   79: aload_1
    //   80: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1710	-> 0
    //   #1711	-> 7
    //   #1712	-> 22
    //   #1713	-> 26
    //   #1715	-> 37
    //   #1713	-> 64
    //   #1717	-> 73
    //   #1718	-> 75
    //   #1717	-> 76
    // Exception table:
    //   from	to	target	type
    //   11	22	76	finally
    //   22	26	76	finally
    //   26	37	76	finally
    //   44	56	76	finally
    //   56	64	76	finally
    //   64	73	76	finally
    //   73	75	76	finally
    //   77	79	76	finally
  }
  
  public void removeOnRoutingChangedListener(AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener) {
    synchronized (this.mRoutingChangeListeners) {
      if (this.mRoutingChangeListeners.containsKey(paramOnRoutingChangedListener)) {
        this.mRoutingChangeListeners.remove(paramOnRoutingChangedListener);
        testDisableNativeRoutingCallbacksLocked();
      } 
      return;
    } 
  }
  
  @Deprecated
  public static interface OnRoutingChangedListener extends AudioRouting.OnRoutingChangedListener {
    void onRoutingChanged(AudioRecord param1AudioRecord);
    
    default void onRoutingChanged(AudioRouting param1AudioRouting) {
      if (param1AudioRouting instanceof AudioRecord)
        onRoutingChanged((AudioRecord)param1AudioRouting); 
    }
  }
  
  @Deprecated
  public void addOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    addOnRoutingChangedListener(paramOnRoutingChangedListener, paramHandler);
  }
  
  @Deprecated
  public void removeOnRoutingChangedListener(OnRoutingChangedListener paramOnRoutingChangedListener) {
    removeOnRoutingChangedListener(paramOnRoutingChangedListener);
  }
  
  private void broadcastRoutingChange() {
    AudioManager.resetAudioPortGeneration();
    synchronized (this.mRoutingChangeListeners) {
      for (NativeRoutingEventHandlerDelegate nativeRoutingEventHandlerDelegate : this.mRoutingChangeListeners.values())
        nativeRoutingEventHandlerDelegate.notifyClient(); 
      return;
    } 
  }
  
  public int setPositionNotificationPeriod(int paramInt) {
    if (this.mState == 0)
      return -3; 
    return native_set_pos_update_period(paramInt);
  }
  
  public boolean setPreferredDevice(AudioDeviceInfo paramAudioDeviceInfo) {
    // Byte code:
    //   0: iconst_0
    //   1: istore_2
    //   2: aload_1
    //   3: ifnull -> 15
    //   6: aload_1
    //   7: invokevirtual isSource : ()Z
    //   10: ifne -> 15
    //   13: iconst_0
    //   14: ireturn
    //   15: aload_1
    //   16: ifnull -> 24
    //   19: aload_1
    //   20: invokevirtual getId : ()I
    //   23: istore_2
    //   24: aload_0
    //   25: iload_2
    //   26: invokespecial native_setInputDevice : (I)Z
    //   29: istore_3
    //   30: iload_3
    //   31: iconst_1
    //   32: if_icmpne -> 52
    //   35: aload_0
    //   36: monitorenter
    //   37: aload_0
    //   38: aload_1
    //   39: putfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   42: aload_0
    //   43: monitorexit
    //   44: goto -> 52
    //   47: astore_1
    //   48: aload_0
    //   49: monitorexit
    //   50: aload_1
    //   51: athrow
    //   52: iload_3
    //   53: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1835	-> 0
    //   #1836	-> 13
    //   #1839	-> 15
    //   #1840	-> 24
    //   #1841	-> 30
    //   #1842	-> 35
    //   #1843	-> 37
    //   #1844	-> 42
    //   #1846	-> 52
    // Exception table:
    //   from	to	target	type
    //   37	42	47	finally
    //   42	44	47	finally
    //   48	50	47	finally
  }
  
  public AudioDeviceInfo getPreferredDevice() {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield mPreferredDevice : Landroid/media/AudioDeviceInfo;
    //   6: astore_1
    //   7: aload_0
    //   8: monitorexit
    //   9: aload_1
    //   10: areturn
    //   11: astore_1
    //   12: aload_0
    //   13: monitorexit
    //   14: aload_1
    //   15: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #1855	-> 0
    //   #1856	-> 2
    //   #1857	-> 11
    // Exception table:
    //   from	to	target	type
    //   2	9	11	finally
    //   12	14	11	finally
  }
  
  public List<MicrophoneInfo> getActiveMicrophones() throws IOException {
    ArrayList<MicrophoneInfo> arrayList = new ArrayList();
    int i = native_get_active_microphones(arrayList);
    if (i != 0) {
      if (i != -3) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("getActiveMicrophones failed:");
        stringBuilder.append(i);
        Log.e("android.media.AudioRecord", stringBuilder.toString());
      } 
      Log.i("android.media.AudioRecord", "getActiveMicrophones failed, fallback on routed device info");
    } 
    AudioManager.setPortIdForMicrophones(arrayList);
    if (arrayList.size() == 0) {
      AudioDeviceInfo audioDeviceInfo = getRoutedDevice();
      if (audioDeviceInfo != null) {
        MicrophoneInfo microphoneInfo = AudioManager.microphoneInfoFromAudioDeviceInfo(audioDeviceInfo);
        ArrayList<Pair> arrayList1 = new ArrayList();
        for (i = 0; i < this.mChannelCount; i++)
          arrayList1.add(new Pair(Integer.valueOf(i), Integer.valueOf(1))); 
        microphoneInfo.setChannelMapping((List)arrayList1);
        arrayList.add(microphoneInfo);
      } 
    } 
    return arrayList;
  }
  
  public void registerAudioRecordingCallback(Executor paramExecutor, AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    this.mRecordingInfoImpl.registerAudioRecordingCallback(paramExecutor, paramAudioRecordingCallback);
  }
  
  public void unregisterAudioRecordingCallback(AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    this.mRecordingInfoImpl.unregisterAudioRecordingCallback(paramAudioRecordingCallback);
  }
  
  public AudioRecordingConfiguration getActiveRecordingConfiguration() {
    return this.mRecordingInfoImpl.getActiveRecordingConfiguration();
  }
  
  public int getPortId() {
    if (this.mNativeRecorderInJavaObj == 0L)
      return 0; 
    return native_getPortId();
  }
  
  public boolean setPreferredMicrophoneDirection(int paramInt) {
    boolean bool;
    if (native_set_preferred_microphone_direction(paramInt) == 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public boolean setPreferredMicrophoneFieldDimension(float paramFloat) {
    boolean bool2, bool1 = true;
    if (paramFloat >= -1.0F && paramFloat <= 1.0F) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Preconditions.checkArgument(bool2, "Argument must fall between -1 & 1 (inclusive)");
    if (native_set_preferred_microphone_field_dimension(paramFloat) == 0) {
      bool2 = bool1;
    } else {
      bool2 = false;
    } 
    return bool2;
  }
  
  private class NativeEventHandler extends Handler {
    private final AudioRecord mAudioRecord;
    
    final AudioRecord this$0;
    
    NativeEventHandler(AudioRecord param1AudioRecord1, Looper param1Looper) {
      super(param1Looper);
      this.mAudioRecord = param1AudioRecord1;
    }
    
    public void handleMessage(Message param1Message) {
      synchronized (AudioRecord.this.mPositionListenerLock) {
        AudioRecord.OnRecordPositionUpdateListener onRecordPositionUpdateListener = this.mAudioRecord.mPositionListener;
        int i = param1Message.what;
        if (i != 2) {
          if (i != 3) {
            null = new StringBuilder();
            null.append("Unknown native event type: ");
            null.append(param1Message.what);
            AudioRecord.loge(null.toString());
          } else if (onRecordPositionUpdateListener != null) {
            onRecordPositionUpdateListener.onPeriodicNotification(this.mAudioRecord);
          } 
        } else if (onRecordPositionUpdateListener != null) {
          onRecordPositionUpdateListener.onMarkerReached(this.mAudioRecord);
        } 
        return;
      } 
    }
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<AudioRecord>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    if (paramInt1 == 1000) {
      paramObject1.broadcastRoutingChange();
      return;
    } 
    NativeEventHandler nativeEventHandler = ((AudioRecord)paramObject1).mEventHandler;
    if (nativeEventHandler != null) {
      paramObject2 = nativeEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
      ((AudioRecord)paramObject1).mEventHandler.sendMessage((Message)paramObject2);
    } 
  }
  
  private static void logd(String paramString) {
    Log.d("android.media.AudioRecord", paramString);
  }
  
  private static void loge(String paramString) {
    Log.e("android.media.AudioRecord", paramString);
  }
  
  private final native void native_disableDeviceCallback();
  
  private final native void native_enableDeviceCallback();
  
  private final native void native_finalize();
  
  private native PersistableBundle native_getMetrics();
  
  private native int native_getPortId();
  
  private final native int native_getRoutedDeviceId();
  
  private final native int native_get_active_microphones(ArrayList<MicrophoneInfo> paramArrayList);
  
  private final native int native_get_buffer_size_in_frames();
  
  private final native int native_get_marker_pos();
  
  private static final native int native_get_min_buff_size(int paramInt1, int paramInt2, int paramInt3);
  
  private final native int native_get_pos_update_period();
  
  private final native int native_get_timestamp(AudioTimestamp paramAudioTimestamp, int paramInt);
  
  private final native int native_read_in_byte_array(byte[] paramArrayOfbyte, int paramInt1, int paramInt2, boolean paramBoolean);
  
  private final native int native_read_in_direct_buffer(Object paramObject, int paramInt, boolean paramBoolean);
  
  private final native int native_read_in_float_array(float[] paramArrayOffloat, int paramInt1, int paramInt2, boolean paramBoolean);
  
  private final native int native_read_in_short_array(short[] paramArrayOfshort, int paramInt1, int paramInt2, boolean paramBoolean);
  
  private final native boolean native_setInputDevice(int paramInt);
  
  private final native int native_set_marker_pos(int paramInt);
  
  private final native int native_set_pos_update_period(int paramInt);
  
  private native int native_set_preferred_microphone_direction(int paramInt);
  
  private native int native_set_preferred_microphone_field_dimension(float paramFloat);
  
  private final native int native_setup(Object paramObject1, Object paramObject2, int[] paramArrayOfint1, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int[] paramArrayOfint2, String paramString, long paramLong);
  
  private final native int native_start(int paramInt1, int paramInt2);
  
  private final native void native_stop();
  
  public final native void native_release();
  
  class MetricsConstants {
    public static final String ATTRIBUTES = "android.media.audiorecord.attributes";
    
    public static final String CHANNELS = "android.media.audiorecord.channels";
    
    public static final String CHANNEL_MASK = "android.media.audiorecord.channelMask";
    
    public static final String DURATION_MS = "android.media.audiorecord.durationMs";
    
    public static final String ENCODING = "android.media.audiorecord.encoding";
    
    public static final String FRAME_COUNT = "android.media.audiorecord.frameCount";
    
    @Deprecated
    public static final String LATENCY = "android.media.audiorecord.latency";
    
    private static final String MM_PREFIX = "android.media.audiorecord.";
    
    public static final String PORT_ID = "android.media.audiorecord.portId";
    
    public static final String SAMPLERATE = "android.media.audiorecord.samplerate";
    
    public static final String SOURCE = "android.media.audiorecord.source";
    
    public static final String START_COUNT = "android.media.audiorecord.startCount";
  }
  
  class OnRecordPositionUpdateListener {
    public abstract void onMarkerReached(AudioRecord param1AudioRecord);
    
    public abstract void onPeriodicNotification(AudioRecord param1AudioRecord);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ReadMode implements Annotation {}
}
