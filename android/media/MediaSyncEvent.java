package android.media;

public class MediaSyncEvent {
  public static final int SYNC_EVENT_NONE = 0;
  
  public static final int SYNC_EVENT_PRESENTATION_COMPLETE = 1;
  
  public static MediaSyncEvent createEvent(int paramInt) throws IllegalArgumentException {
    if (isValidType(paramInt))
      return new MediaSyncEvent(paramInt); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append("is not a valid MediaSyncEvent type.");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private int mAudioSession = 0;
  
  private final int mType;
  
  private MediaSyncEvent(int paramInt) {
    this.mType = paramInt;
  }
  
  public MediaSyncEvent setAudioSessionId(int paramInt) throws IllegalArgumentException {
    if (paramInt > 0) {
      this.mAudioSession = paramInt;
      return this;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramInt);
    stringBuilder.append(" is not a valid session ID.");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public int getType() {
    return this.mType;
  }
  
  public int getAudioSessionId() {
    return this.mAudioSession;
  }
  
  private static boolean isValidType(int paramInt) {
    if (paramInt != 0 && paramInt != 1)
      return false; 
    return true;
  }
}
