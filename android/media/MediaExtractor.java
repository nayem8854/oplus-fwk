package android.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import android.os.IBinder;
import android.os.IHwBinder;
import android.os.PersistableBundle;
import com.android.internal.util.Preconditions;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MediaExtractor {
  public static final int SAMPLE_FLAG_ENCRYPTED = 2;
  
  public static final int SAMPLE_FLAG_PARTIAL_FRAME = 4;
  
  public static final int SAMPLE_FLAG_SYNC = 1;
  
  public static final int SEEK_TO_CLOSEST_SYNC = 2;
  
  public static final int SEEK_TO_NEXT_SYNC = 1;
  
  public static final int SEEK_TO_PREVIOUS_SYNC = 0;
  
  private MediaCas mMediaCas;
  
  private long mNativeContext;
  
  public MediaExtractor() {
    native_setup();
  }
  
  public final void setDataSource(Context paramContext, Uri paramUri, Map<String, String> paramMap) throws IOException {
    // Byte code:
    //   0: aload_2
    //   1: invokevirtual getScheme : ()Ljava/lang/String;
    //   4: astore #4
    //   6: aload #4
    //   8: ifnull -> 246
    //   11: aload #4
    //   13: ldc_w 'file'
    //   16: invokevirtual equals : (Ljava/lang/Object;)Z
    //   19: ifeq -> 25
    //   22: goto -> 246
    //   25: aconst_null
    //   26: astore #5
    //   28: aconst_null
    //   29: astore #6
    //   31: aconst_null
    //   32: astore #7
    //   34: aload #7
    //   36: astore #8
    //   38: aload #5
    //   40: astore #9
    //   42: aload #6
    //   44: astore #4
    //   46: aload_1
    //   47: invokevirtual getContentResolver : ()Landroid/content/ContentResolver;
    //   50: astore_1
    //   51: aload #7
    //   53: astore #8
    //   55: aload #5
    //   57: astore #9
    //   59: aload #6
    //   61: astore #4
    //   63: aload_1
    //   64: aload_2
    //   65: ldc_w 'r'
    //   68: invokevirtual openAssetFileDescriptor : (Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    //   71: astore_1
    //   72: aload_1
    //   73: ifnonnull -> 85
    //   76: aload_1
    //   77: ifnull -> 84
    //   80: aload_1
    //   81: invokevirtual close : ()V
    //   84: return
    //   85: aload_1
    //   86: astore #8
    //   88: aload_1
    //   89: astore #9
    //   91: aload_1
    //   92: astore #4
    //   94: aload_1
    //   95: invokevirtual getDeclaredLength : ()J
    //   98: lconst_0
    //   99: lcmp
    //   100: ifge -> 123
    //   103: aload_1
    //   104: astore #8
    //   106: aload_1
    //   107: astore #9
    //   109: aload_1
    //   110: astore #4
    //   112: aload_0
    //   113: aload_1
    //   114: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   117: invokevirtual setDataSource : (Ljava/io/FileDescriptor;)V
    //   120: goto -> 187
    //   123: aload_1
    //   124: astore #8
    //   126: aload_1
    //   127: astore #9
    //   129: aload_1
    //   130: astore #4
    //   132: aload_1
    //   133: invokevirtual getFileDescriptor : ()Ljava/io/FileDescriptor;
    //   136: astore #7
    //   138: aload_1
    //   139: astore #8
    //   141: aload_1
    //   142: astore #9
    //   144: aload_1
    //   145: astore #4
    //   147: aload_1
    //   148: invokevirtual getStartOffset : ()J
    //   151: lstore #10
    //   153: aload_1
    //   154: astore #8
    //   156: aload_1
    //   157: astore #9
    //   159: aload_1
    //   160: astore #4
    //   162: aload_1
    //   163: invokevirtual getDeclaredLength : ()J
    //   166: lstore #12
    //   168: aload_1
    //   169: astore #8
    //   171: aload_1
    //   172: astore #9
    //   174: aload_1
    //   175: astore #4
    //   177: aload_0
    //   178: aload #7
    //   180: lload #10
    //   182: lload #12
    //   184: invokevirtual setDataSource : (Ljava/io/FileDescriptor;JJ)V
    //   187: aload_1
    //   188: ifnull -> 195
    //   191: aload_1
    //   192: invokevirtual close : ()V
    //   195: return
    //   196: astore_1
    //   197: aload #8
    //   199: ifnull -> 207
    //   202: aload #8
    //   204: invokevirtual close : ()V
    //   207: aload_1
    //   208: athrow
    //   209: astore_1
    //   210: aload #9
    //   212: ifnull -> 236
    //   215: aload #9
    //   217: astore #4
    //   219: aload #4
    //   221: invokevirtual close : ()V
    //   224: goto -> 236
    //   227: astore_1
    //   228: aload #4
    //   230: ifnull -> 236
    //   233: goto -> 219
    //   236: aload_0
    //   237: aload_2
    //   238: invokevirtual toString : ()Ljava/lang/String;
    //   241: aload_3
    //   242: invokevirtual setDataSource : (Ljava/lang/String;Ljava/util/Map;)V
    //   245: return
    //   246: aload_0
    //   247: aload_2
    //   248: invokevirtual getPath : ()Ljava/lang/String;
    //   251: invokevirtual setDataSource : (Ljava/lang/String;)V
    //   254: return
    // Line number table:
    //   Java source line number -> byte code offset
    //   #111	-> 0
    //   #112	-> 6
    //   #117	-> 25
    //   #119	-> 34
    //   #120	-> 51
    //   #121	-> 72
    //   #139	-> 76
    //   #140	-> 80
    //   #122	-> 84
    //   #127	-> 85
    //   #128	-> 103
    //   #130	-> 123
    //   #131	-> 123
    //   #132	-> 138
    //   #133	-> 153
    //   #130	-> 168
    //   #139	-> 187
    //   #140	-> 191
    //   #135	-> 195
    //   #139	-> 196
    //   #140	-> 202
    //   #142	-> 207
    //   #137	-> 209
    //   #139	-> 210
    //   #140	-> 219
    //   #136	-> 227
    //   #139	-> 228
    //   #140	-> 233
    //   #144	-> 236
    //   #145	-> 245
    //   #113	-> 246
    //   #114	-> 254
    // Exception table:
    //   from	to	target	type
    //   46	51	227	java/lang/SecurityException
    //   46	51	209	java/io/IOException
    //   46	51	196	finally
    //   63	72	227	java/lang/SecurityException
    //   63	72	209	java/io/IOException
    //   63	72	196	finally
    //   94	103	227	java/lang/SecurityException
    //   94	103	209	java/io/IOException
    //   94	103	196	finally
    //   112	120	227	java/lang/SecurityException
    //   112	120	209	java/io/IOException
    //   112	120	196	finally
    //   132	138	227	java/lang/SecurityException
    //   132	138	209	java/io/IOException
    //   132	138	196	finally
    //   147	153	227	java/lang/SecurityException
    //   147	153	209	java/io/IOException
    //   147	153	196	finally
    //   162	168	227	java/lang/SecurityException
    //   162	168	209	java/io/IOException
    //   162	168	196	finally
    //   177	187	227	java/lang/SecurityException
    //   177	187	209	java/io/IOException
    //   177	187	196	finally
  }
  
  public final void setDataSource(String paramString, Map<String, String> paramMap) throws IOException {
    Map.Entry entry;
    String[] arrayOfString1 = null;
    String[] arrayOfString2 = null;
    if (paramMap != null) {
      String[] arrayOfString3 = new String[paramMap.size()];
      String[] arrayOfString4 = new String[paramMap.size()];
      byte b = 0;
      Iterator<Map.Entry> iterator = paramMap.entrySet().iterator();
      while (true) {
        arrayOfString1 = arrayOfString3;
        arrayOfString2 = arrayOfString4;
        if (iterator.hasNext()) {
          entry = iterator.next();
          arrayOfString3[b] = (String)entry.getKey();
          arrayOfString4[b] = (String)entry.getValue();
          b++;
          continue;
        } 
        break;
      } 
    } 
    IBinder iBinder = MediaHTTPService.createHttpServiceBinderIfNecessary(paramString);
    nativeSetDataSource(iBinder, paramString, (String[])entry, arrayOfString2);
  }
  
  public final void setDataSource(String paramString) throws IOException {
    IBinder iBinder = MediaHTTPService.createHttpServiceBinderIfNecessary(paramString);
    nativeSetDataSource(iBinder, paramString, null, null);
  }
  
  public final void setDataSource(AssetFileDescriptor paramAssetFileDescriptor) throws IOException, IllegalArgumentException, IllegalStateException {
    Preconditions.checkNotNull(paramAssetFileDescriptor);
    if (paramAssetFileDescriptor.getDeclaredLength() < 0L) {
      setDataSource(paramAssetFileDescriptor.getFileDescriptor());
    } else {
      setDataSource(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), paramAssetFileDescriptor.getDeclaredLength());
    } 
  }
  
  public final void setDataSource(FileDescriptor paramFileDescriptor) throws IOException {
    setDataSource(paramFileDescriptor, 0L, 576460752303423487L);
  }
  
  public final void setMediaCas(MediaCas paramMediaCas) {
    this.mMediaCas = paramMediaCas;
    nativeSetMediaCas(paramMediaCas.getBinder());
  }
  
  public static final class CasInfo {
    private final byte[] mPrivateData;
    
    private final MediaCas.Session mSession;
    
    private final int mSystemId;
    
    CasInfo(int param1Int, MediaCas.Session param1Session, byte[] param1ArrayOfbyte) {
      this.mSystemId = param1Int;
      this.mSession = param1Session;
      this.mPrivateData = param1ArrayOfbyte;
    }
    
    public int getSystemId() {
      return this.mSystemId;
    }
    
    public byte[] getPrivateData() {
      return this.mPrivateData;
    }
    
    public MediaCas.Session getSession() {
      return this.mSession;
    }
  }
  
  private ArrayList<Byte> toByteArray(byte[] paramArrayOfbyte) {
    ArrayList<Byte> arrayList = new ArrayList(paramArrayOfbyte.length);
    for (byte b = 0; b < paramArrayOfbyte.length; b++)
      arrayList.add(b, Byte.valueOf(paramArrayOfbyte[b])); 
    return arrayList;
  }
  
  public CasInfo getCasInfo(int paramInt) {
    Map<String, Object> map = getTrackFormatNative(paramInt);
    if (map.containsKey("ca-system-id")) {
      MediaCas.Session session;
      paramInt = ((Integer)map.get("ca-system-id")).intValue();
      ByteBuffer byteBuffer1 = null;
      byte[] arrayOfByte = null;
      if (map.containsKey("ca-private-data")) {
        ByteBuffer byteBuffer = (ByteBuffer)map.get("ca-private-data");
        byteBuffer.rewind();
        arrayOfByte = new byte[byteBuffer.remaining()];
        byteBuffer.get(arrayOfByte);
      } 
      ByteBuffer byteBuffer2 = byteBuffer1;
      if (this.mMediaCas != null) {
        byteBuffer2 = byteBuffer1;
        if (map.containsKey("ca-session-id")) {
          byteBuffer2 = (ByteBuffer)map.get("ca-session-id");
          byteBuffer2.rewind();
          byte[] arrayOfByte1 = new byte[byteBuffer2.remaining()];
          byteBuffer2.get(arrayOfByte1);
          session = this.mMediaCas.createFromSessionId(toByteArray(arrayOfByte1));
        } 
      } 
      return new CasInfo(paramInt, session, arrayOfByte);
    } 
    return null;
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  public DrmInitData getDrmInitData() {
    Map<String, Object> map = getFileFormatNative();
    if (map == null)
      return null; 
    if (map.containsKey("pssh")) {
      map = (Map)getPsshInfo();
      Stream stream = map.entrySet().stream().map((Function)_$$Lambda$MediaExtractor$AQWHHMUeHgJTkS0k0b0xoZxPezM.INSTANCE);
      -$.Lambda.MediaExtractor.XTtCu2qZqPfrj1LW7KOxdVVSgoY xTtCu2qZqPfrj1LW7KOxdVVSgoY = _$$Lambda$MediaExtractor$XTtCu2qZqPfrj1LW7KOxdVVSgoY.INSTANCE;
      DrmInitData.SchemeInitData[] arrayOfSchemeInitData = (DrmInitData.SchemeInitData[])stream.toArray((IntFunction)xTtCu2qZqPfrj1LW7KOxdVVSgoY);
      Stream<DrmInitData.SchemeInitData> stream1 = Arrays.stream(arrayOfSchemeInitData);
      -$.Lambda.MediaExtractor.R3OLaOzRieGuaOtoydwuYEiQATg r3OLaOzRieGuaOtoydwuYEiQATg = _$$Lambda$MediaExtractor$R3OLaOzRieGuaOtoydwuYEiQATg.INSTANCE;
      -$.Lambda.MediaExtractor.j4boqJIEvcYyVawgtNNWskNKVz8 j4boqJIEvcYyVawgtNNWskNKVz8 = _$$Lambda$MediaExtractor$j4boqJIEvcYyVawgtNNWskNKVz8.INSTANCE;
      Collector<?, ?, Map<?, ?>> collector = Collectors.toMap((Function<?, ?>)r3OLaOzRieGuaOtoydwuYEiQATg, (Function<?, ?>)j4boqJIEvcYyVawgtNNWskNKVz8);
      Map map1 = stream1.collect((Collector)collector);
      return (DrmInitData)new Object(this, map1, arrayOfSchemeInitData);
    } 
    int i = getTrackCount();
    for (byte b = 0; b < i; ) {
      map = getTrackFormatNative(b);
      if (!map.containsKey("crypto-key")) {
        b++;
        continue;
      } 
      ByteBuffer byteBuffer = (ByteBuffer)map.get("crypto-key");
      byteBuffer.rewind();
      byte[] arrayOfByte = new byte[byteBuffer.remaining()];
      byteBuffer.get(arrayOfByte);
      DrmInitData.SchemeInitData schemeInitData = new DrmInitData.SchemeInitData(DrmInitData.SchemeInitData.UUID_NIL, "webm", arrayOfByte);
      return (DrmInitData)new Object(this, schemeInitData);
    } 
    return null;
  }
  
  public List<AudioPresentation> getAudioPresentations(int paramInt) {
    return native_getAudioPresentations(paramInt);
  }
  
  public Map<UUID, byte[]> getPsshInfo() {
    UUID uUID;
    HashMap<Object, Object> hashMap1 = null;
    Map<String, Object> map = getFileFormatNative();
    HashMap<Object, Object> hashMap2 = hashMap1;
    if (map != null) {
      hashMap2 = hashMap1;
      if (map.containsKey("pssh")) {
        ByteBuffer byteBuffer = (ByteBuffer)map.get("pssh");
        byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer.rewind();
        map.remove("pssh");
        hashMap1 = new HashMap<>();
        while (true) {
          hashMap2 = hashMap1;
          if (byteBuffer.remaining() > 0) {
            byteBuffer.order(ByteOrder.BIG_ENDIAN);
            long l1 = byteBuffer.getLong();
            long l2 = byteBuffer.getLong();
            uUID = new UUID(l1, l2);
            byteBuffer.order(ByteOrder.nativeOrder());
            int i = byteBuffer.getInt();
            byte[] arrayOfByte = new byte[i];
            byteBuffer.get(arrayOfByte);
            hashMap1.put(uUID, arrayOfByte);
            continue;
          } 
          break;
        } 
      } 
    } 
    return (Map<UUID, byte[]>)uUID;
  }
  
  public MediaFormat getTrackFormat(int paramInt) {
    return new MediaFormat(getTrackFormatNative(paramInt));
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private native Map<String, Object> getFileFormatNative();
  
  private native Map<String, Object> getTrackFormatNative(int paramInt);
  
  private final native void nativeSetDataSource(IBinder paramIBinder, String paramString, String[] paramArrayOfString1, String[] paramArrayOfString2) throws IOException;
  
  private final native void nativeSetMediaCas(IHwBinder paramIHwBinder);
  
  private final native void native_finalize();
  
  private native List<AudioPresentation> native_getAudioPresentations(int paramInt);
  
  private native PersistableBundle native_getMetrics();
  
  private static final native void native_init();
  
  private final native void native_setup();
  
  public native boolean advance();
  
  public native long getCachedDuration();
  
  public native boolean getSampleCryptoInfo(MediaCodec.CryptoInfo paramCryptoInfo);
  
  public native int getSampleFlags();
  
  public native long getSampleSize();
  
  public native long getSampleTime();
  
  public native int getSampleTrackIndex();
  
  public final native int getTrackCount();
  
  public native boolean hasCacheReachedEndOfStream();
  
  public native int readSampleData(ByteBuffer paramByteBuffer, int paramInt);
  
  public final native void release();
  
  public native void seekTo(long paramLong, int paramInt);
  
  public native void selectTrack(int paramInt);
  
  public final native void setDataSource(MediaDataSource paramMediaDataSource) throws IOException;
  
  public final native void setDataSource(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2) throws IOException;
  
  public native void unselectTrack(int paramInt);
  
  public static final class MetricsConstants {
    public static final String FORMAT = "android.media.mediaextractor.fmt";
    
    public static final String MIME_TYPE = "android.media.mediaextractor.mime";
    
    public static final String TRACKS = "android.media.mediaextractor.ntrk";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SampleFlag {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SeekMode {}
}
