package android.media;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import com.android.internal.util.Preconditions;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executor;
import java.util.concurrent.locks.ReentrantLock;

public final class MediaTranscodeManager {
  private static final int EVENT_JOB_FINISHED = 3;
  
  private static final int EVENT_JOB_PROGRESSED = 2;
  
  private static final int EVENT_JOB_STARTED = 1;
  
  private static final long ID_INVALID = -1L;
  
  private static final String TAG = "MediaTranscodeManager";
  
  private static MediaTranscodeManager sMediaTranscodeManager;
  
  private final Context mContext;
  
  private final ConcurrentMap<Long, TranscodingJob> mPendingTranscodingJobs = new ConcurrentHashMap<>();
  
  public static final class TranscodingRequest {
    private MediaFormat mDstFormat;
    
    private Uri mDstUri;
    
    private Uri mSrcUri;
    
    private TranscodingRequest(Builder param1Builder) {
      this.mSrcUri = param1Builder.mSrcUri;
      this.mDstUri = param1Builder.mDstUri;
      this.mDstFormat = param1Builder.mDstFormat;
    }
    
    public static class Builder {
      private MediaFormat mDstFormat;
      
      private Uri mDstUri;
      
      private Uri mSrcUri;
      
      public Builder setSourceUri(Uri param2Uri) {
        this.mSrcUri = param2Uri;
        return this;
      }
      
      public Builder setDestinationUri(Uri param2Uri) {
        this.mDstUri = param2Uri;
        return this;
      }
      
      public Builder setDestinationFormat(MediaFormat param2MediaFormat) {
        this.mDstFormat = param2MediaFormat;
        return this;
      }
      
      public MediaTranscodeManager.TranscodingRequest build() {
        return new MediaTranscodeManager.TranscodingRequest(this);
      }
    }
  }
  
  public static class Builder {
    private MediaFormat mDstFormat;
    
    private Uri mDstUri;
    
    private Uri mSrcUri;
    
    public Builder setSourceUri(Uri param1Uri) {
      this.mSrcUri = param1Uri;
      return this;
    }
    
    public Builder setDestinationUri(Uri param1Uri) {
      this.mDstUri = param1Uri;
      return this;
    }
    
    public Builder setDestinationFormat(MediaFormat param1MediaFormat) {
      this.mDstFormat = param1MediaFormat;
      return this;
    }
    
    public MediaTranscodeManager.TranscodingRequest build() {
      return new MediaTranscodeManager.TranscodingRequest(this);
    }
  }
  
  public static final class TranscodingJob {
    private final ReentrantLock mStatusChangeLock = new ReentrantLock();
    
    private float mProgress = 0.0F;
    
    private int mStatus = 1;
    
    private int mResult = 1;
    
    public static final int RESULT_CANCELED = 4;
    
    public static final int RESULT_ERROR = 3;
    
    public static final int RESULT_NONE = 1;
    
    public static final int RESULT_SUCCESS = 2;
    
    public static final int STATUS_FINISHED = 3;
    
    public static final int STATUS_PENDING = 1;
    
    public static final int STATUS_RUNNING = 2;
    
    private final Executor mExecutor;
    
    private long mID;
    
    private final MediaTranscodeManager.OnTranscodingFinishedListener mListener;
    
    private Executor mProgressChangedExecutor;
    
    private OnProgressChangedListener mProgressChangedListener;
    
    private TranscodingJob(long param1Long, Executor param1Executor, MediaTranscodeManager.OnTranscodingFinishedListener param1OnTranscodingFinishedListener) {
      this.mID = param1Long;
      this.mExecutor = param1Executor;
      this.mListener = param1OnTranscodingFinishedListener;
    }
    
    public void setOnProgressChangedListener(Executor param1Executor, OnProgressChangedListener param1OnProgressChangedListener) {
      this.mProgressChangedExecutor = param1Executor;
      this.mProgressChangedListener = param1OnProgressChangedListener;
    }
    
    public void cancel() {
      setJobFinished(4);
      MediaTranscodeManager.sMediaTranscodeManager.native_cancelTranscodingRequest(this.mID);
    }
    
    public float getProgress() {
      return this.mProgress;
    }
    
    public int getStatus() {
      return this.mStatus;
    }
    
    public int getResult() {
      return this.mResult;
    }
    
    private void setJobStarted() {
      this.mStatus = 2;
    }
    
    private void setJobProgress(float param1Float) {
      this.mProgress = param1Float;
      OnProgressChangedListener onProgressChangedListener = this.mProgressChangedListener;
      if (onProgressChangedListener != null)
        this.mProgressChangedExecutor.execute(new _$$Lambda$MediaTranscodeManager$TranscodingJob$zBKUqcscKK9kg5Ya3kpdPZqyUQw(this, onProgressChangedListener)); 
    }
    
    private void setJobFinished(int param1Int) {
      boolean bool = false;
      try {
        this.mStatusChangeLock.lock();
        if (this.mStatus != 3) {
          this.mStatus = 3;
          this.mResult = param1Int;
          bool = true;
        } 
        this.mStatusChangeLock.unlock();
        return;
      } finally {
        this.mStatusChangeLock.unlock();
      } 
    }
    
    private void processJobEvent(int param1Int1, int param1Int2) {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 3) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Unsupported event: ");
            stringBuilder.append(param1Int1);
            Log.e("MediaTranscodeManager", stringBuilder.toString());
          } else {
            setJobFinished(param1Int2);
          } 
        } else {
          setJobProgress(param1Int2 / 100.0F);
        } 
      } else {
        setJobStarted();
      } 
    }
    
    @FunctionalInterface
    public static interface OnProgressChangedListener {
      void onProgressChanged(float param2Float);
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Result {}
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Status {}
  }
  
  private MediaTranscodeManager(Context paramContext) {
    this.mContext = paramContext;
  }
  
  private void postEventFromNative(int paramInt1, long paramLong, int paramInt2) {
    StringBuilder stringBuilder;
    Log.d("MediaTranscodeManager", String.format("postEventFromNative. Event %d, ID %d, arg %d", new Object[] { Integer.valueOf(paramInt1), Long.valueOf(paramLong), Integer.valueOf(paramInt2) }));
    TranscodingJob transcodingJob = this.mPendingTranscodingJobs.get(Long.valueOf(paramLong));
    if (transcodingJob == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("No matching transcode job found for id ");
      stringBuilder.append(paramLong);
      Log.e("MediaTranscodeManager", stringBuilder.toString());
      return;
    } 
    stringBuilder.processJobEvent(paramInt1, paramInt2);
  }
  
  public static MediaTranscodeManager getInstance(Context paramContext) {
    // Byte code:
    //   0: aload_0
    //   1: invokestatic checkNotNull : (Ljava/lang/Object;)Ljava/lang/Object;
    //   4: pop
    //   5: ldc android/media/MediaTranscodeManager
    //   7: monitorenter
    //   8: getstatic android/media/MediaTranscodeManager.sMediaTranscodeManager : Landroid/media/MediaTranscodeManager;
    //   11: ifnonnull -> 30
    //   14: new android/media/MediaTranscodeManager
    //   17: astore_1
    //   18: aload_1
    //   19: aload_0
    //   20: invokevirtual getApplicationContext : ()Landroid/content/Context;
    //   23: invokespecial <init> : (Landroid/content/Context;)V
    //   26: aload_1
    //   27: putstatic android/media/MediaTranscodeManager.sMediaTranscodeManager : Landroid/media/MediaTranscodeManager;
    //   30: getstatic android/media/MediaTranscodeManager.sMediaTranscodeManager : Landroid/media/MediaTranscodeManager;
    //   33: astore_0
    //   34: ldc android/media/MediaTranscodeManager
    //   36: monitorexit
    //   37: aload_0
    //   38: areturn
    //   39: astore_0
    //   40: ldc android/media/MediaTranscodeManager
    //   42: monitorexit
    //   43: aload_0
    //   44: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #354	-> 0
    //   #355	-> 5
    //   #356	-> 8
    //   #357	-> 14
    //   #359	-> 30
    //   #360	-> 39
    // Exception table:
    //   from	to	target	type
    //   8	14	39	finally
    //   14	30	39	finally
    //   30	37	39	finally
    //   40	43	39	finally
  }
  
  public TranscodingJob enqueueTranscodingRequest(TranscodingRequest paramTranscodingRequest, Executor paramExecutor, OnTranscodingFinishedListener paramOnTranscodingFinishedListener) {
    Log.i("MediaTranscodeManager", "enqueueTranscodingRequest called.");
    Preconditions.checkNotNull(paramTranscodingRequest);
    Preconditions.checkNotNull(paramExecutor);
    Preconditions.checkNotNull(paramOnTranscodingFinishedListener);
    long l = native_requestUniqueJobID();
    if (l == -1L)
      return null; 
    TranscodingJob transcodingJob = new TranscodingJob(l, paramExecutor, paramOnTranscodingFinishedListener);
    this.mPendingTranscodingJobs.put(Long.valueOf(l), transcodingJob);
    boolean bool = native_enqueueTranscodingRequest(l, paramTranscodingRequest, this.mContext);
    if (!bool) {
      this.mPendingTranscodingJobs.remove(Long.valueOf(l));
      return null;
    } 
    return transcodingJob;
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private native void native_cancelTranscodingRequest(long paramLong);
  
  private native boolean native_enqueueTranscodingRequest(long paramLong, TranscodingRequest paramTranscodingRequest, Context paramContext);
  
  private static native void native_init();
  
  private native long native_requestUniqueJobID();
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Event {}
  
  @FunctionalInterface
  public static interface OnTranscodingFinishedListener {
    void onTranscodingFinished(MediaTranscodeManager.TranscodingJob param1TranscodingJob);
  }
  
  @FunctionalInterface
  public static interface OnProgressChangedListener {
    void onProgressChanged(float param1Float);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Result {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Status {}
}
