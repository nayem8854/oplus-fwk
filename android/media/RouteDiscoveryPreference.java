package android.media;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class RouteDiscoveryPreference implements Parcelable {
  public static final Parcelable.Creator<RouteDiscoveryPreference> CREATOR = new Parcelable.Creator<RouteDiscoveryPreference>() {
      public RouteDiscoveryPreference createFromParcel(Parcel param1Parcel) {
        return new RouteDiscoveryPreference(param1Parcel);
      }
      
      public RouteDiscoveryPreference[] newArray(int param1Int) {
        return new RouteDiscoveryPreference[param1Int];
      }
    };
  
  public static final RouteDiscoveryPreference EMPTY = (new Builder(Collections.emptyList(), false)).build();
  
  private final Bundle mExtras;
  
  private final List<String> mPreferredFeatures;
  
  private final boolean mShouldPerformActiveScan;
  
  RouteDiscoveryPreference(Builder paramBuilder) {
    this.mPreferredFeatures = paramBuilder.mPreferredFeatures;
    this.mShouldPerformActiveScan = paramBuilder.mActiveScan;
    this.mExtras = paramBuilder.mExtras;
  }
  
  RouteDiscoveryPreference(Parcel paramParcel) {
    this.mPreferredFeatures = paramParcel.createStringArrayList();
    this.mShouldPerformActiveScan = paramParcel.readBoolean();
    this.mExtras = paramParcel.readBundle();
  }
  
  public List<String> getPreferredFeatures() {
    return this.mPreferredFeatures;
  }
  
  public boolean shouldPerformActiveScan() {
    return this.mShouldPerformActiveScan;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeStringList(this.mPreferredFeatures);
    paramParcel.writeBoolean(this.mShouldPerformActiveScan);
    paramParcel.writeBundle(this.mExtras);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("RouteDiscoveryRequest{ ");
    stringBuilder.append("preferredFeatures={");
    List<String> list = this.mPreferredFeatures;
    stringBuilder.append(String.join(", ", (Iterable)list));
    stringBuilder.append("}");
    stringBuilder.append(", activeScan=");
    boolean bool = this.mShouldPerformActiveScan;
    stringBuilder.append(bool);
    stringBuilder = stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public boolean equals(Object paramObject) {
    boolean bool = true;
    if (this == paramObject)
      return true; 
    if (!(paramObject instanceof RouteDiscoveryPreference))
      return false; 
    paramObject = paramObject;
    if (!Objects.equals(this.mPreferredFeatures, ((RouteDiscoveryPreference)paramObject).mPreferredFeatures) || this.mShouldPerformActiveScan != ((RouteDiscoveryPreference)paramObject).mShouldPerformActiveScan)
      bool = false; 
    return bool;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { this.mPreferredFeatures, Boolean.valueOf(this.mShouldPerformActiveScan) });
  }
  
  class Builder {
    boolean mActiveScan;
    
    Bundle mExtras;
    
    List<String> mPreferredFeatures;
    
    public Builder(RouteDiscoveryPreference this$0, boolean param1Boolean) {
      Objects.requireNonNull(this$0, "preferredFeatures must not be null");
      Stream stream = this$0.stream().filter((Predicate)_$$Lambda$RouteDiscoveryPreference$Builder$RAMVaK9RAZ5Ai0qMO3YINliBf1o.INSTANCE);
      this.mPreferredFeatures = (List<String>)stream.collect(Collectors.toList());
      this.mActiveScan = param1Boolean;
    }
    
    public Builder(RouteDiscoveryPreference this$0) {
      Objects.requireNonNull(this$0, "preference must not be null");
      this.mPreferredFeatures = this$0.getPreferredFeatures();
      this.mActiveScan = this$0.shouldPerformActiveScan();
      this.mExtras = this$0.getExtras();
    }
    
    public Builder(RouteDiscoveryPreference this$0) {
      Objects.requireNonNull(this$0, "preferences must not be null");
      HashSet<? extends String> hashSet = new HashSet();
      this.mActiveScan = false;
      for (RouteDiscoveryPreference routeDiscoveryPreference : this$0) {
        hashSet.addAll(routeDiscoveryPreference.mPreferredFeatures);
        this.mActiveScan |= routeDiscoveryPreference.mShouldPerformActiveScan;
      } 
      this.mPreferredFeatures = new ArrayList<>(hashSet);
    }
    
    public Builder setPreferredFeatures(List<String> param1List) {
      Objects.requireNonNull(param1List, "preferredFeatures must not be null");
      Stream stream = param1List.stream().filter((Predicate)_$$Lambda$RouteDiscoveryPreference$Builder$Y04J69jgqbK9_Wo7TZhFFWS0ztk.INSTANCE);
      this.mPreferredFeatures = (List<String>)stream.collect(Collectors.toList());
      return this;
    }
    
    public Builder setShouldPerformActiveScan(boolean param1Boolean) {
      this.mActiveScan = param1Boolean;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public RouteDiscoveryPreference build() {
      return new RouteDiscoveryPreference(this);
    }
  }
}
