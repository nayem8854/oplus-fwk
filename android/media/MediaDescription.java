package android.media;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

public class MediaDescription implements Parcelable {
  public static final long BT_FOLDER_TYPE_ALBUMS = 2L;
  
  public static final long BT_FOLDER_TYPE_ARTISTS = 3L;
  
  public static final long BT_FOLDER_TYPE_GENRES = 4L;
  
  public static final long BT_FOLDER_TYPE_MIXED = 0L;
  
  public static final long BT_FOLDER_TYPE_PLAYLISTS = 5L;
  
  public static final long BT_FOLDER_TYPE_TITLES = 1L;
  
  public static final long BT_FOLDER_TYPE_YEARS = 6L;
  
  private MediaDescription(String paramString, CharSequence paramCharSequence1, CharSequence paramCharSequence2, CharSequence paramCharSequence3, Bitmap paramBitmap, Uri paramUri1, Bundle paramBundle, Uri paramUri2) {
    this.mMediaId = paramString;
    this.mTitle = paramCharSequence1;
    this.mSubtitle = paramCharSequence2;
    this.mDescription = paramCharSequence3;
    this.mIcon = paramBitmap;
    this.mIconUri = paramUri1;
    this.mExtras = paramBundle;
    this.mMediaUri = paramUri2;
  }
  
  private MediaDescription(Parcel paramParcel) {
    this.mMediaId = paramParcel.readString();
    this.mTitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mSubtitle = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mDescription = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(paramParcel);
    this.mIcon = paramParcel.<Bitmap>readParcelable(null);
    this.mIconUri = paramParcel.<Uri>readParcelable(null);
    this.mExtras = paramParcel.readBundle();
    this.mMediaUri = paramParcel.<Uri>readParcelable(null);
  }
  
  public String getMediaId() {
    return this.mMediaId;
  }
  
  public CharSequence getTitle() {
    return this.mTitle;
  }
  
  public CharSequence getSubtitle() {
    return this.mSubtitle;
  }
  
  public CharSequence getDescription() {
    return this.mDescription;
  }
  
  public Bitmap getIconBitmap() {
    return this.mIcon;
  }
  
  public Uri getIconUri() {
    return this.mIconUri;
  }
  
  public Bundle getExtras() {
    return this.mExtras;
  }
  
  public Uri getMediaUri() {
    return this.mMediaUri;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mMediaId);
    TextUtils.writeToParcel(this.mTitle, paramParcel, 0);
    TextUtils.writeToParcel(this.mSubtitle, paramParcel, 0);
    TextUtils.writeToParcel(this.mDescription, paramParcel, 0);
    paramParcel.writeParcelable((Parcelable)this.mIcon, paramInt);
    paramParcel.writeParcelable(this.mIconUri, paramInt);
    paramParcel.writeBundle(this.mExtras);
    paramParcel.writeParcelable(this.mMediaUri, paramInt);
  }
  
  public boolean equals(Object paramObject) {
    if (paramObject == null)
      return false; 
    if (!(paramObject instanceof MediaDescription))
      return false; 
    paramObject = paramObject;
    if (!String.valueOf(this.mTitle).equals(String.valueOf(((MediaDescription)paramObject).mTitle)))
      return false; 
    if (!String.valueOf(this.mSubtitle).equals(String.valueOf(((MediaDescription)paramObject).mSubtitle)))
      return false; 
    if (!String.valueOf(this.mDescription).equals(String.valueOf(((MediaDescription)paramObject).mDescription)))
      return false; 
    return true;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(this.mTitle);
    stringBuilder.append(", ");
    stringBuilder.append(this.mSubtitle);
    stringBuilder.append(", ");
    stringBuilder.append(this.mDescription);
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<MediaDescription> CREATOR = new Parcelable.Creator<MediaDescription>() {
      public MediaDescription createFromParcel(Parcel param1Parcel) {
        return new MediaDescription(param1Parcel);
      }
      
      public MediaDescription[] newArray(int param1Int) {
        return new MediaDescription[param1Int];
      }
    };
  
  public static final String EXTRA_BT_FOLDER_TYPE = "android.media.extra.BT_FOLDER_TYPE";
  
  private final CharSequence mDescription;
  
  private final Bundle mExtras;
  
  private final Bitmap mIcon;
  
  private final Uri mIconUri;
  
  private final String mMediaId;
  
  private final Uri mMediaUri;
  
  private final CharSequence mSubtitle;
  
  private final CharSequence mTitle;
  
  class Builder {
    private CharSequence mDescription;
    
    private Bundle mExtras;
    
    private Bitmap mIcon;
    
    private Uri mIconUri;
    
    private String mMediaId;
    
    private Uri mMediaUri;
    
    private CharSequence mSubtitle;
    
    private CharSequence mTitle;
    
    public Builder setMediaId(String param1String) {
      this.mMediaId = param1String;
      return this;
    }
    
    public Builder setTitle(CharSequence param1CharSequence) {
      this.mTitle = param1CharSequence;
      return this;
    }
    
    public Builder setSubtitle(CharSequence param1CharSequence) {
      this.mSubtitle = param1CharSequence;
      return this;
    }
    
    public Builder setDescription(CharSequence param1CharSequence) {
      this.mDescription = param1CharSequence;
      return this;
    }
    
    public Builder setIconBitmap(Bitmap param1Bitmap) {
      this.mIcon = param1Bitmap;
      return this;
    }
    
    public Builder setIconUri(Uri param1Uri) {
      this.mIconUri = param1Uri;
      return this;
    }
    
    public Builder setExtras(Bundle param1Bundle) {
      this.mExtras = param1Bundle;
      return this;
    }
    
    public Builder setMediaUri(Uri param1Uri) {
      this.mMediaUri = param1Uri;
      return this;
    }
    
    public MediaDescription build() {
      return new MediaDescription(this.mMediaId, this.mTitle, this.mSubtitle, this.mDescription, this.mIcon, this.mIconUri, this.mExtras, this.mMediaUri);
    }
  }
}
