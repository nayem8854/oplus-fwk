package android.media;

import android.app.ActivityManager;
import android.content.Context;
import android.hardware.cas.V1_0.HidlCasPluginDescriptor;
import android.hardware.cas.V1_0.ICas;
import android.hardware.cas.V1_0.ICasListener;
import android.hardware.cas.V1_0.IMediaCasService;
import android.hardware.cas.V1_1.ICas;
import android.hardware.cas.V1_1.ICasListener;
import android.hardware.cas.V1_1.IMediaCasService;
import android.hardware.cas.V1_2.ICas;
import android.hardware.cas.V1_2.ICasListener;
import android.hardware.cas.V1_2.IMediaCasService;
import android.media.tv.tunerresourcemanager.CasSessionRequest;
import android.media.tv.tunerresourcemanager.ResourceClientProfile;
import android.media.tv.tunerresourcemanager.TunerResourceManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IHwBinder;
import android.os.IHwInterface;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.util.Singleton;
import com.android.internal.util.FrameworkStatsLog;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Executor;

public final class MediaCas implements AutoCloseable {
  private TunerResourceManager mTunerResourceManager = null;
  
  private final Map<Session, Integer> mSessionMap = new HashMap<>();
  
  private static final Singleton<IMediaCasService> sService = (Singleton<IMediaCasService>)new Object();
  
  static IMediaCasService getService() {
    return (IMediaCasService)sService.get();
  }
  
  private void validateInternalStates() {
    if (this.mICas != null)
      return; 
    throw new IllegalStateException();
  }
  
  private void cleanupAndRethrowIllegalState() {
    this.mICas = null;
    this.mICasV11 = null;
    this.mICasV12 = null;
    throw new IllegalStateException();
  }
  
  class EventHandler extends Handler {
    private static final String DATA_KEY = "data";
    
    private static final int MSG_CAS_EVENT = 0;
    
    private static final int MSG_CAS_RESOURCE_LOST = 3;
    
    private static final int MSG_CAS_SESSION_EVENT = 1;
    
    private static final int MSG_CAS_STATUS_EVENT = 2;
    
    private static final String SESSION_KEY = "sessionId";
    
    final MediaCas this$0;
    
    public EventHandler(Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      byte[] arrayOfByte;
      if (param1Message.what == 0) {
        MediaCas.EventListener eventListener = MediaCas.this.mListener;
        MediaCas mediaCas1 = MediaCas.this;
        int i = param1Message.arg1, j = param1Message.arg2;
        MediaCas mediaCas2 = MediaCas.this;
        ArrayList arrayList = (ArrayList)param1Message.obj;
        arrayOfByte = mediaCas2.toBytes(arrayList);
        eventListener.onEvent(mediaCas1, i, j, arrayOfByte);
      } else if (((Message)arrayOfByte).what == 1) {
        Bundle bundle = arrayOfByte.getData();
        ArrayList<Byte> arrayList = MediaCas.this.toByteArray(bundle.getByteArray("sessionId"));
        MediaCas.EventListener eventListener = MediaCas.this.mListener;
        MediaCas mediaCas = MediaCas.this;
        MediaCas.Session session = mediaCas.createFromSessionId(arrayList);
        int i = ((Message)arrayOfByte).arg1, j = ((Message)arrayOfByte).arg2;
        arrayOfByte = bundle.getByteArray("data");
        eventListener.onSessionEvent(mediaCas, session, i, j, arrayOfByte);
      } else if (((Message)arrayOfByte).what == 2) {
        if (((Message)arrayOfByte).arg1 == 1) {
          MediaCas mediaCas = MediaCas.this;
          if (mediaCas.mTunerResourceManager != null)
            MediaCas.this.mTunerResourceManager.updateCasInfo(MediaCas.this.mCasSystemId, ((Message)arrayOfByte).arg2); 
        } 
        MediaCas.this.mListener.onPluginStatusUpdate(MediaCas.this, ((Message)arrayOfByte).arg1, ((Message)arrayOfByte).arg2);
      } else if (((Message)arrayOfByte).what == 3) {
        MediaCas.this.mListener.onResourceLost(MediaCas.this);
      } 
    }
  }
  
  private final ICasListener.Stub mBinder = (ICasListener.Stub)new Object(this);
  
  private final TunerResourceManager.ResourcesReclaimListener mResourceListener = (TunerResourceManager.ResourcesReclaimListener)new Object(this);
  
  public static final int PLUGIN_STATUS_PHYSICAL_MODULE_CHANGED = 0;
  
  public static final int PLUGIN_STATUS_SESSION_NUMBER_CHANGED = 1;
  
  public static final int SCRAMBLING_MODE_AES128 = 9;
  
  public static final int SCRAMBLING_MODE_AES_ECB = 10;
  
  public static final int SCRAMBLING_MODE_AES_SCTE52 = 11;
  
  public static final int SCRAMBLING_MODE_DVB_CISSA_V1 = 6;
  
  public static final int SCRAMBLING_MODE_DVB_CSA1 = 1;
  
  public static final int SCRAMBLING_MODE_DVB_CSA2 = 2;
  
  public static final int SCRAMBLING_MODE_DVB_CSA3_ENHANCE = 5;
  
  public static final int SCRAMBLING_MODE_DVB_CSA3_MINIMAL = 4;
  
  public static final int SCRAMBLING_MODE_DVB_CSA3_STANDARD = 3;
  
  public static final int SCRAMBLING_MODE_DVB_IDSA = 7;
  
  public static final int SCRAMBLING_MODE_MULTI2 = 8;
  
  public static final int SCRAMBLING_MODE_RESERVED = 0;
  
  public static final int SCRAMBLING_MODE_TDES_ECB = 12;
  
  public static final int SCRAMBLING_MODE_TDES_SCTE52 = 13;
  
  public static final int SESSION_USAGE_LIVE = 0;
  
  public static final int SESSION_USAGE_PLAYBACK = 1;
  
  public static final int SESSION_USAGE_RECORD = 2;
  
  public static final int SESSION_USAGE_TIMESHIFT = 3;
  
  private static final String TAG = "MediaCas";
  
  private int mCasSystemId;
  
  private int mClientId;
  
  private EventHandler mEventHandler;
  
  private HandlerThread mHandlerThread;
  
  private ICas mICas;
  
  private ICas mICasV11;
  
  private ICas mICasV12;
  
  private EventListener mListener;
  
  private int mPriorityHint;
  
  private String mTvInputServiceSessionId;
  
  private int mUserId;
  
  public static class PluginDescriptor {
    private final int mCASystemId;
    
    private final String mName;
    
    private PluginDescriptor() {
      this.mCASystemId = 65535;
      this.mName = null;
    }
    
    PluginDescriptor(HidlCasPluginDescriptor param1HidlCasPluginDescriptor) {
      this.mCASystemId = param1HidlCasPluginDescriptor.caSystemId;
      this.mName = param1HidlCasPluginDescriptor.name;
    }
    
    public int getSystemId() {
      return this.mCASystemId;
    }
    
    public String getName() {
      return this.mName;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PluginDescriptor {");
      stringBuilder.append(this.mCASystemId);
      stringBuilder.append(", ");
      stringBuilder.append(this.mName);
      stringBuilder.append("}");
      return stringBuilder.toString();
    }
  }
  
  private ArrayList<Byte> toByteArray(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) {
    ArrayList<Byte> arrayList = new ArrayList(paramInt2);
    for (byte b = 0; b < paramInt2; b++)
      arrayList.add(Byte.valueOf(paramArrayOfbyte[paramInt1 + b])); 
    return arrayList;
  }
  
  private ArrayList<Byte> toByteArray(byte[] paramArrayOfbyte) {
    if (paramArrayOfbyte == null)
      return new ArrayList<>(); 
    return toByteArray(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  private byte[] toBytes(ArrayList<Byte> paramArrayList) {
    byte[] arrayOfByte = null;
    if (paramArrayList != null) {
      byte[] arrayOfByte1 = new byte[paramArrayList.size()];
      byte b = 0;
      while (true) {
        arrayOfByte = arrayOfByte1;
        if (b < arrayOfByte1.length) {
          arrayOfByte1[b] = ((Byte)paramArrayList.get(b)).byteValue();
          b++;
          continue;
        } 
        break;
      } 
    } 
    return arrayOfByte;
  }
  
  public final class Session implements AutoCloseable {
    boolean mIsClosed = false;
    
    final ArrayList<Byte> mSessionId;
    
    final MediaCas this$0;
    
    Session(ArrayList<Byte> param1ArrayList) {
      this.mSessionId = new ArrayList<>(param1ArrayList);
    }
    
    private void validateSessionInternalStates() {
      if (MediaCas.this.mICas != null) {
        if (this.mIsClosed)
          MediaCasStateException.throwExceptionIfNeeded(3); 
        return;
      } 
      throw new IllegalStateException();
    }
    
    public boolean equals(Object param1Object) {
      if (param1Object instanceof Session)
        return this.mSessionId.equals(((Session)param1Object).mSessionId); 
      return false;
    }
    
    public void setPrivateData(byte[] param1ArrayOfbyte) throws MediaCasException {
      validateSessionInternalStates();
      try {
        MediaCas mediaCas = MediaCas.this;
        int i = mediaCas.mICas.setSessionPrivateData(this.mSessionId, MediaCas.this.toByteArray(param1ArrayOfbyte, 0, param1ArrayOfbyte.length));
        MediaCasException.throwExceptionIfNeeded(i);
      } catch (RemoteException remoteException) {
        MediaCas.this.cleanupAndRethrowIllegalState();
      } 
    }
    
    public void processEcm(byte[] param1ArrayOfbyte, int param1Int1, int param1Int2) throws MediaCasException {
      validateSessionInternalStates();
      try {
        MediaCas mediaCas = MediaCas.this;
        param1Int1 = mediaCas.mICas.processEcm(this.mSessionId, MediaCas.this.toByteArray(param1ArrayOfbyte, param1Int1, param1Int2));
        MediaCasException.throwExceptionIfNeeded(param1Int1);
      } catch (RemoteException remoteException) {
        MediaCas.this.cleanupAndRethrowIllegalState();
      } 
    }
    
    public void processEcm(byte[] param1ArrayOfbyte) throws MediaCasException {
      processEcm(param1ArrayOfbyte, 0, param1ArrayOfbyte.length);
    }
    
    public void sendSessionEvent(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) throws MediaCasException {
      validateSessionInternalStates();
      if (MediaCas.this.mICasV11 != null) {
        try {
          MediaCas mediaCas = MediaCas.this;
          param1Int1 = mediaCas.mICasV11.sendSessionEvent(this.mSessionId, param1Int1, param1Int2, MediaCas.this.toByteArray(param1ArrayOfbyte));
          MediaCasException.throwExceptionIfNeeded(param1Int1);
        } catch (RemoteException remoteException) {
          MediaCas.this.cleanupAndRethrowIllegalState();
        } 
        return;
      } 
      Log.d("MediaCas", "Send Session Event isn't supported by cas@1.0 interface");
      throw new MediaCasException.UnsupportedCasException("Send Session Event is not supported");
    }
    
    public byte[] getSessionId() {
      validateSessionInternalStates();
      return MediaCas.this.toBytes(this.mSessionId);
    }
    
    public void close() {
      validateSessionInternalStates();
      try {
        MediaCas mediaCas = MediaCas.this;
        int i = mediaCas.mICas.closeSession(this.mSessionId);
        MediaCasStateException.throwExceptionIfNeeded(i);
        this.mIsClosed = true;
        MediaCas.this.removeSessionFromResourceMap(this);
      } catch (RemoteException remoteException) {
        MediaCas.this.cleanupAndRethrowIllegalState();
      } 
    }
  }
  
  Session createFromSessionId(ArrayList<Byte> paramArrayList) {
    if (paramArrayList == null || paramArrayList.size() == 0)
      return null; 
    return new Session(paramArrayList);
  }
  
  public static boolean isSystemIdSupported(int paramInt) {
    IMediaCasService iMediaCasService = getService();
    if (iMediaCasService != null)
      try {
        return iMediaCasService.isSystemIdSupported(paramInt);
      } catch (RemoteException remoteException) {} 
    return false;
  }
  
  public static PluginDescriptor[] enumeratePlugins() {
    IMediaCasService iMediaCasService = getService();
    if (iMediaCasService != null)
      try {
        ArrayList<HidlCasPluginDescriptor> arrayList = iMediaCasService.enumeratePlugins();
        if (arrayList.size() == 0)
          return null; 
        PluginDescriptor[] arrayOfPluginDescriptor = new PluginDescriptor[arrayList.size()];
        for (byte b = 0; b < arrayOfPluginDescriptor.length; b++)
          arrayOfPluginDescriptor[b] = new PluginDescriptor(arrayList.get(b)); 
        return arrayOfPluginDescriptor;
      } catch (RemoteException remoteException) {} 
    return null;
  }
  
  public MediaCas(int paramInt) throws MediaCasException.UnsupportedCasException {
    try {
      IMediaCasService iMediaCasService1;
      this.mCasSystemId = paramInt;
      this.mUserId = ActivityManager.getCurrentUser();
      IMediaCasService iMediaCasService = getService();
      IMediaCasService iMediaCasService2 = IMediaCasService.castFrom((IHwInterface)iMediaCasService);
      if (iMediaCasService2 == null) {
        iMediaCasService1 = IMediaCasService.castFrom((IHwInterface)iMediaCasService);
        if (iMediaCasService1 == null) {
          Log.d("MediaCas", "Used cas@1_0 interface to create plugin");
          this.mICas = iMediaCasService.createPlugin(paramInt, (ICasListener)this.mBinder);
        } else {
          Log.d("MediaCas", "Used cas@1.1 interface to create plugin");
          ICas iCas = iMediaCasService1.createPluginExt(paramInt, (ICasListener)this.mBinder);
          this.mICas = (ICas)iCas;
        } 
      } else {
        Log.d("MediaCas", "Used cas@1.2 interface to create plugin");
        ICasListener.Stub stub = this.mBinder;
        ICas iCas = ICas.castFrom((IHwInterface)iMediaCasService1.createPluginExt(paramInt, (ICasListener)stub));
        this.mICasV11 = (ICas)iCas;
        this.mICas = (ICas)iCas;
      } 
      if (this.mICas == null) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported CA_system_id ");
        stringBuilder.append(paramInt);
        throw new MediaCasException.UnsupportedCasException(stringBuilder.toString());
      } 
    } catch (Exception exception) {
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("Failed to create plugin: ");
      stringBuilder.append(exception);
      Log.e("MediaCas", stringBuilder.toString());
      this.mICas = null;
      if (!false) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("Unsupported CA_system_id ");
        stringBuilder1.append(paramInt);
        throw new MediaCasException.UnsupportedCasException(stringBuilder1.toString());
      } 
    } finally {
      Exception exception;
    } 
  }
  
  public MediaCas(Context paramContext, int paramInt1, String paramString, int paramInt2) throws MediaCasException.UnsupportedCasException {
    this(paramInt1);
    Objects.requireNonNull(paramContext, "context must not be null");
    TunerResourceManager tunerResourceManager = (TunerResourceManager)paramContext.getSystemService("tv_tuner_resource_mgr");
    if (tunerResourceManager != null) {
      int[] arrayOfInt = new int[1];
      ResourceClientProfile resourceClientProfile = new ResourceClientProfile(paramString, paramInt2);
      TunerResourceManager tunerResourceManager1 = this.mTunerResourceManager;
      Executor executor = paramContext.getMainExecutor();
      TunerResourceManager.ResourcesReclaimListener resourcesReclaimListener = this.mResourceListener;
      tunerResourceManager1.registerClientProfile(resourceClientProfile, executor, resourcesReclaimListener, arrayOfInt);
      this.mClientId = arrayOfInt[0];
    } 
  }
  
  IHwBinder getBinder() {
    validateInternalStates();
    return this.mICas.asBinder();
  }
  
  public static interface EventListener {
    default void onSessionEvent(MediaCas param1MediaCas, MediaCas.Session param1Session, int param1Int1, int param1Int2, byte[] param1ArrayOfbyte) {
      Log.d("MediaCas", "Received MediaCas Session event");
    }
    
    default void onPluginStatusUpdate(MediaCas param1MediaCas, int param1Int1, int param1Int2) {
      Log.d("MediaCas", "Received MediaCas Plugin Status event");
    }
    
    default void onResourceLost(MediaCas param1MediaCas) {
      Log.d("MediaCas", "Received MediaCas Resource Reclaim event");
    }
    
    void onEvent(MediaCas param1MediaCas, int param1Int1, int param1Int2, byte[] param1ArrayOfbyte);
  }
  
  public void setEventListener(EventListener paramEventListener, Handler paramHandler) {
    this.mListener = paramEventListener;
    Looper looper2 = null;
    if (paramEventListener == null) {
      this.mEventHandler = null;
      return;
    } 
    if (paramHandler != null)
      looper2 = paramHandler.getLooper(); 
    Looper looper1 = looper2;
    if (looper2 == null) {
      Looper looper = Looper.myLooper();
      if (looper == null) {
        looper1 = looper = Looper.getMainLooper();
        if (looper == null) {
          HandlerThread handlerThread = this.mHandlerThread;
          if (handlerThread == null || !handlerThread.isAlive()) {
            this.mHandlerThread = handlerThread = new HandlerThread("MediaCasEventThread", -2);
            handlerThread.start();
          } 
          looper1 = this.mHandlerThread.getLooper();
        } 
      } 
    } 
    this.mEventHandler = new EventHandler(looper1);
  }
  
  public void setPrivateData(byte[] paramArrayOfbyte) throws MediaCasException {
    validateInternalStates();
    try {
      ICas iCas = this.mICas;
      int i = paramArrayOfbyte.length;
      i = iCas.setPrivateData(toByteArray(paramArrayOfbyte, 0, i));
      MediaCasException.throwExceptionIfNeeded(i);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  class OpenSessionCallback implements ICas.openSessionCallback {
    public MediaCas.Session mSession;
    
    public int mStatus;
    
    final MediaCas this$0;
    
    private OpenSessionCallback() {}
    
    public void onValues(int param1Int, ArrayList<Byte> param1ArrayList) {
      this.mStatus = param1Int;
      this.mSession = MediaCas.this.createFromSessionId(param1ArrayList);
    }
  }
  
  class OpenSession_1_2_Callback implements ICas.openSession_1_2Callback {
    public MediaCas.Session mSession;
    
    public int mStatus;
    
    final MediaCas this$0;
    
    private OpenSession_1_2_Callback() {}
    
    public void onValues(int param1Int, ArrayList<Byte> param1ArrayList) {
      this.mStatus = param1Int;
      this.mSession = MediaCas.this.createFromSessionId(param1ArrayList);
    }
  }
  
  private int getSessionResourceHandle() throws MediaCasException {
    validateInternalStates();
    int[] arrayOfInt = new int[1];
    arrayOfInt[0] = -1;
    if (this.mTunerResourceManager != null) {
      CasSessionRequest casSessionRequest = new CasSessionRequest(this.mClientId, this.mCasSystemId);
      TunerResourceManager tunerResourceManager = this.mTunerResourceManager;
      if (!tunerResourceManager.requestCasSession(casSessionRequest, arrayOfInt))
        throw new MediaCasException.InsufficientResourceException("insufficient resource to Open Session"); 
    } 
    return arrayOfInt[0];
  }
  
  private void addSessionToResourceMap(Session paramSession, int paramInt) {
    if (paramInt != -1)
      synchronized (this.mSessionMap) {
        this.mSessionMap.put(paramSession, Integer.valueOf(paramInt));
      }  
  }
  
  private void removeSessionFromResourceMap(Session paramSession) {
    synchronized (this.mSessionMap) {
      if (this.mSessionMap.get(paramSession) != null) {
        this.mTunerResourceManager.releaseCasSession(((Integer)this.mSessionMap.get(paramSession)).intValue(), this.mClientId);
        this.mSessionMap.remove(paramSession);
      } 
      return;
    } 
  }
  
  public Session openSession() throws MediaCasException {
    int i = getSessionResourceHandle();
    try {
      OpenSessionCallback openSessionCallback = new OpenSessionCallback();
      this(this);
      this.mICas.openSession(openSessionCallback);
      MediaCasException.throwExceptionIfNeeded(openSessionCallback.mStatus);
      addSessionToResourceMap(openSessionCallback.mSession, i);
      Log.d("MediaCas", "Write Stats Log for succeed to Open Session.");
      int j = this.mUserId;
      i = this.mCasSystemId;
      FrameworkStatsLog.write(280, j, i, 1);
      return openSessionCallback.mSession;
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
      Log.d("MediaCas", "Write Stats Log for fail to Open Session.");
      i = this.mUserId;
      int j = this.mCasSystemId;
      FrameworkStatsLog.write(280, i, j, 2);
      return null;
    } 
  }
  
  public Session openSession(int paramInt1, int paramInt2) throws MediaCasException {
    int i = getSessionResourceHandle();
    if (this.mICasV12 != null)
      try {
        OpenSession_1_2_Callback openSession_1_2_Callback = new OpenSession_1_2_Callback();
        this(this);
        this.mICasV12.openSession_1_2(paramInt1, paramInt2, openSession_1_2_Callback);
        MediaCasException.throwExceptionIfNeeded(openSession_1_2_Callback.mStatus);
        addSessionToResourceMap(openSession_1_2_Callback.mSession, i);
        Log.d("MediaCas", "Write Stats Log for succeed to Open Session.");
        paramInt2 = this.mUserId;
        paramInt1 = this.mCasSystemId;
        FrameworkStatsLog.write(280, paramInt2, paramInt1, 1);
        return openSession_1_2_Callback.mSession;
      } catch (RemoteException remoteException) {
        cleanupAndRethrowIllegalState();
        Log.d("MediaCas", "Write Stats Log for fail to Open Session.");
        paramInt2 = this.mUserId;
        paramInt1 = this.mCasSystemId;
        FrameworkStatsLog.write(280, paramInt2, paramInt1, 2);
        return null;
      }  
    Log.d("MediaCas", "Open Session with scrambling mode is only supported by cas@1.2+ interface");
    throw new MediaCasException.UnsupportedCasException("Open Session with scrambling mode is not supported");
  }
  
  public void processEmm(byte[] paramArrayOfbyte, int paramInt1, int paramInt2) throws MediaCasException {
    validateInternalStates();
    try {
      ICas iCas = this.mICas;
      paramInt1 = iCas.processEmm(toByteArray(paramArrayOfbyte, paramInt1, paramInt2));
      MediaCasException.throwExceptionIfNeeded(paramInt1);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  public void processEmm(byte[] paramArrayOfbyte) throws MediaCasException {
    processEmm(paramArrayOfbyte, 0, paramArrayOfbyte.length);
  }
  
  public void sendEvent(int paramInt1, int paramInt2, byte[] paramArrayOfbyte) throws MediaCasException {
    validateInternalStates();
    try {
      ICas iCas = this.mICas;
      paramInt1 = iCas.sendEvent(paramInt1, paramInt2, toByteArray(paramArrayOfbyte));
      MediaCasException.throwExceptionIfNeeded(paramInt1);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  public void provision(String paramString) throws MediaCasException {
    validateInternalStates();
    try {
      ICas iCas = this.mICas;
      int i = iCas.provision(paramString);
      MediaCasException.throwExceptionIfNeeded(i);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  public void refreshEntitlements(int paramInt, byte[] paramArrayOfbyte) throws MediaCasException {
    validateInternalStates();
    try {
      ICas iCas = this.mICas;
      paramInt = iCas.refreshEntitlements(paramInt, toByteArray(paramArrayOfbyte));
      MediaCasException.throwExceptionIfNeeded(paramInt);
    } catch (RemoteException remoteException) {
      cleanupAndRethrowIllegalState();
    } 
  }
  
  public void forceResourceLost() {
    TunerResourceManager.ResourcesReclaimListener resourcesReclaimListener = this.mResourceListener;
    if (resourcesReclaimListener != null)
      resourcesReclaimListener.onReclaimResources(); 
  }
  
  public void close() {
    ICas iCas = this.mICas;
    if (iCas != null)
      try {
        iCas.release();
      } catch (RemoteException remoteException) {
      
      } finally {
        this.mICas = null;
      }  
    TunerResourceManager tunerResourceManager = this.mTunerResourceManager;
    if (tunerResourceManager != null) {
      tunerResourceManager.unregisterClientProfile(this.mClientId);
      this.mTunerResourceManager = null;
    } 
    HandlerThread handlerThread = this.mHandlerThread;
    if (handlerThread != null) {
      handlerThread.quit();
      this.mHandlerThread = null;
    } 
  }
  
  protected void finalize() {
    close();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface PluginStatus {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ScramblingMode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SessionUsage {}
}
