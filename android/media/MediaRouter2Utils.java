package android.media;

import android.text.TextUtils;
import android.util.Log;

public class MediaRouter2Utils {
  static final String SEPARATOR = ":";
  
  static final String TAG = "MR2Utils";
  
  public static String toUniqueId(String paramString1, String paramString2) {
    if (TextUtils.isEmpty(paramString1)) {
      Log.w("MR2Utils", "toUniqueId: providerId shouldn't be empty");
      return null;
    } 
    if (TextUtils.isEmpty(paramString2)) {
      Log.w("MR2Utils", "toUniqueId: id shouldn't be null");
      return null;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(paramString1);
    stringBuilder.append(":");
    stringBuilder.append(paramString2);
    return stringBuilder.toString();
  }
  
  public static String getProviderId(String paramString) {
    if (TextUtils.isEmpty(paramString)) {
      Log.w("MR2Utils", "getProviderId: uniqueId shouldn't be empty");
      return null;
    } 
    int i = paramString.indexOf(":");
    if (i == -1)
      return null; 
    paramString = paramString.substring(0, i);
    if (TextUtils.isEmpty(paramString))
      return null; 
    return paramString;
  }
  
  public static String getOriginalId(String paramString) {
    if (TextUtils.isEmpty(paramString)) {
      Log.w("MR2Utils", "getOriginalId: uniqueId shouldn't be empty");
      return null;
    } 
    int i = paramString.indexOf(":");
    if (i == -1 || i + 1 >= paramString.length())
      return null; 
    paramString = paramString.substring(i + 1);
    if (TextUtils.isEmpty(paramString))
      return null; 
    return paramString;
  }
}
