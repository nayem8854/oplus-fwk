package android.media;

import android.os.Parcel;
import android.util.Log;
import android.util.MathUtils;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.TimeZone;

@Deprecated
public class Metadata {
  public static final int ALBUM = 8;
  
  public static final int ALBUM_ART = 18;
  
  public static final int ANY = 0;
  
  public static final int ARTIST = 9;
  
  public static final int AUDIO_BIT_RATE = 21;
  
  public static final int AUDIO_CODEC = 26;
  
  public static final int AUDIO_SAMPLE_RATE = 23;
  
  public static final int AUTHOR = 10;
  
  public static final int BIT_RATE = 20;
  
  public static final int BOOLEAN_VAL = 3;
  
  public static final int BYTE_ARRAY_VAL = 7;
  
  public static final int CD_TRACK_MAX = 16;
  
  public static final int CD_TRACK_NUM = 15;
  
  public static final int COMMENT = 6;
  
  public static final int COMPOSER = 11;
  
  public static final int COPYRIGHT = 7;
  
  public static final int DATE = 13;
  
  public static final int DATE_VAL = 6;
  
  public static final int DOUBLE_VAL = 5;
  
  public static final int DRM_CRIPPLED = 31;
  
  public static final int DURATION = 14;
  
  private static final int FIRST_CUSTOM = 8192;
  
  public static final int GENRE = 12;
  
  public static final int INTEGER_VAL = 2;
  
  private static final int LAST_SYSTEM = 31;
  
  private static final int LAST_TYPE = 7;
  
  public static final int LONG_VAL = 4;
  
  public static final Set<Integer> MATCH_ALL;
  
  public static final Set<Integer> MATCH_NONE = Collections.EMPTY_SET;
  
  public static final int MIME_TYPE = 25;
  
  public static final int NUM_TRACKS = 30;
  
  public static final int PAUSE_AVAILABLE = 1;
  
  public static final int RATING = 17;
  
  public static final int SEEK_AVAILABLE = 4;
  
  public static final int SEEK_BACKWARD_AVAILABLE = 2;
  
  public static final int SEEK_FORWARD_AVAILABLE = 3;
  
  public static final int STRING_VAL = 1;
  
  private static final String TAG = "media.Metadata";
  
  public static final int TITLE = 5;
  
  public static final int VIDEO_BIT_RATE = 22;
  
  public static final int VIDEO_CODEC = 27;
  
  public static final int VIDEO_FRAME = 19;
  
  public static final int VIDEO_FRAME_RATE = 24;
  
  public static final int VIDEO_HEIGHT = 28;
  
  public static final int VIDEO_WIDTH = 29;
  
  private static final int kInt32Size = 4;
  
  private static final int kMetaHeaderSize = 8;
  
  private static final int kMetaMarker = 1296389185;
  
  private static final int kRecordHeaderSize = 12;
  
  static {
    MATCH_ALL = Collections.singleton(Integer.valueOf(0));
  }
  
  private final HashMap<Integer, Integer> mKeyToPosMap = new HashMap<>();
  
  private Parcel mParcel;
  
  private boolean scanAllRecords(Parcel paramParcel, int paramInt) {
    byte b = 0;
    boolean bool = false;
    this.mKeyToPosMap.clear();
    int i = paramInt;
    while (true) {
      paramInt = bool;
      if (i > 12) {
        StringBuilder stringBuilder;
        int j = paramParcel.dataPosition();
        paramInt = paramParcel.readInt();
        if (paramInt <= 12) {
          Log.e("media.Metadata", "Record is too short");
          paramInt = 1;
          break;
        } 
        int k = paramParcel.readInt();
        if (!checkMetadataId(k)) {
          paramInt = 1;
          break;
        } 
        if (this.mKeyToPosMap.containsKey(Integer.valueOf(k))) {
          Log.e("media.Metadata", "Duplicate metadata ID found");
          paramInt = 1;
          break;
        } 
        this.mKeyToPosMap.put(Integer.valueOf(k), Integer.valueOf(paramParcel.dataPosition()));
        k = paramParcel.readInt();
        if (k <= 0 || k > 7) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid metadata type ");
          stringBuilder.append(k);
          Log.e("media.Metadata", stringBuilder.toString());
          paramInt = 1;
          break;
        } 
        try {
          stringBuilder.setDataPosition(MathUtils.addOrThrow(j, paramInt));
          i -= paramInt;
          b++;
          continue;
        } catch (IllegalArgumentException illegalArgumentException) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Invalid size: ");
          stringBuilder.append(illegalArgumentException.getMessage());
          Log.e("media.Metadata", stringBuilder.toString());
          paramInt = 1;
          break;
        } 
      } 
      break;
    } 
    if (i != 0 || paramInt != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Ran out of data or error on record ");
      stringBuilder.append(b);
      Log.e("media.Metadata", stringBuilder.toString());
      this.mKeyToPosMap.clear();
      return false;
    } 
    return true;
  }
  
  public boolean parse(Parcel paramParcel) {
    if (paramParcel.dataAvail() < 8) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Not enough data ");
      stringBuilder.append(paramParcel.dataAvail());
      Log.e("media.Metadata", stringBuilder.toString());
      return false;
    } 
    int i = paramParcel.dataPosition();
    int j = paramParcel.readInt();
    if (paramParcel.dataAvail() + 4 < j || j < 8) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Bad size ");
      stringBuilder.append(j);
      stringBuilder.append(" avail ");
      stringBuilder.append(paramParcel.dataAvail());
      stringBuilder.append(" position ");
      stringBuilder.append(i);
      Log.e("media.Metadata", stringBuilder.toString());
      paramParcel.setDataPosition(i);
      return false;
    } 
    int k = paramParcel.readInt();
    if (k != 1296389185) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Marker missing ");
      stringBuilder.append(Integer.toHexString(k));
      Log.e("media.Metadata", stringBuilder.toString());
      paramParcel.setDataPosition(i);
      return false;
    } 
    if (!scanAllRecords(paramParcel, j - 8)) {
      paramParcel.setDataPosition(i);
      return false;
    } 
    this.mParcel = paramParcel;
    return true;
  }
  
  public Set<Integer> keySet() {
    return this.mKeyToPosMap.keySet();
  }
  
  public boolean has(int paramInt) {
    if (checkMetadataId(paramInt))
      return this.mKeyToPosMap.containsKey(Integer.valueOf(paramInt)); 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid key: ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public String getString(int paramInt) {
    checkType(paramInt, 1);
    return this.mParcel.readString();
  }
  
  public int getInt(int paramInt) {
    checkType(paramInt, 2);
    return this.mParcel.readInt();
  }
  
  public boolean getBoolean(int paramInt) {
    checkType(paramInt, 3);
    paramInt = this.mParcel.readInt();
    boolean bool = true;
    if (paramInt != 1)
      bool = false; 
    return bool;
  }
  
  public long getLong(int paramInt) {
    checkType(paramInt, 4);
    return this.mParcel.readLong();
  }
  
  public double getDouble(int paramInt) {
    checkType(paramInt, 5);
    return this.mParcel.readDouble();
  }
  
  public byte[] getByteArray(int paramInt) {
    checkType(paramInt, 7);
    return this.mParcel.createByteArray();
  }
  
  public Date getDate(int paramInt) {
    checkType(paramInt, 6);
    long l = this.mParcel.readLong();
    String str = this.mParcel.readString();
    if (str.length() == 0)
      return new Date(l); 
    TimeZone timeZone = TimeZone.getTimeZone(str);
    Calendar calendar = Calendar.getInstance(timeZone);
    calendar.setTimeInMillis(l);
    return calendar.getTime();
  }
  
  public static int lastSytemId() {
    return 31;
  }
  
  public static int firstCustomId() {
    return 8192;
  }
  
  public static int lastType() {
    return 7;
  }
  
  private boolean checkMetadataId(int paramInt) {
    if (paramInt <= 0 || (31 < paramInt && paramInt < 8192)) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Invalid metadata ID ");
      stringBuilder.append(paramInt);
      Log.e("media.Metadata", stringBuilder.toString());
      return false;
    } 
    return true;
  }
  
  private void checkType(int paramInt1, int paramInt2) {
    paramInt1 = ((Integer)this.mKeyToPosMap.get(Integer.valueOf(paramInt1))).intValue();
    this.mParcel.setDataPosition(paramInt1);
    paramInt1 = this.mParcel.readInt();
    if (paramInt1 == paramInt2)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Wrong type ");
    stringBuilder.append(paramInt2);
    stringBuilder.append(" but got ");
    stringBuilder.append(paramInt1);
    throw new IllegalStateException(stringBuilder.toString());
  }
}
