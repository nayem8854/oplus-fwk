package android.media;

import android.util.Log;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public final class MediaCodecList {
  public static final int ALL_CODECS = 1;
  
  public static final int REGULAR_CODECS = 0;
  
  private static final String TAG = "MediaCodecList";
  
  private static MediaCodecInfo[] sAllCodecInfos;
  
  private static Map<String, Object> sGlobalSettings;
  
  public static final int getCodecCount() {
    initCodecList();
    return sRegularCodecInfos.length;
  }
  
  public static final MediaCodecInfo getCodecInfoAt(int paramInt) {
    initCodecList();
    if (paramInt >= 0) {
      MediaCodecInfo[] arrayOfMediaCodecInfo = sRegularCodecInfos;
      if (paramInt <= arrayOfMediaCodecInfo.length)
        return arrayOfMediaCodecInfo[paramInt]; 
    } 
    throw new IllegalArgumentException();
  }
  
  static final Map<String, Object> getGlobalSettings() {
    synchronized (sInitLock) {
      if (sGlobalSettings == null)
        sGlobalSettings = native_getGlobalSettings(); 
      return sGlobalSettings;
    } 
  }
  
  private static Object sInitLock = new Object();
  
  private static MediaCodecInfo[] sRegularCodecInfos;
  
  private MediaCodecInfo[] mCodecInfos;
  
  private static final void initCodecList() {
    synchronized (sInitLock) {
      if (sRegularCodecInfos == null) {
        int i = native_getCodecCount();
        ArrayList<MediaCodecInfo> arrayList1 = new ArrayList();
        this();
        ArrayList<MediaCodecInfo> arrayList2 = new ArrayList();
        this();
        for (byte b = 0; b < i; b++) {
          try {
            MediaCodecInfo mediaCodecInfo = getNewCodecInfoAt(b);
            arrayList2.add(mediaCodecInfo);
            mediaCodecInfo = mediaCodecInfo.makeRegular();
            if (mediaCodecInfo != null)
              arrayList1.add(mediaCodecInfo); 
          } catch (Exception exception) {
            Log.e("MediaCodecList", "Could not get codec capabilities", exception);
          } 
        } 
        sRegularCodecInfos = arrayList1.<MediaCodecInfo>toArray(new MediaCodecInfo[arrayList1.size()]);
        sAllCodecInfos = arrayList2.<MediaCodecInfo>toArray(new MediaCodecInfo[arrayList2.size()]);
      } 
      return;
    } 
  }
  
  private static MediaCodecInfo getNewCodecInfoAt(int paramInt) {
    String[] arrayOfString = getSupportedTypes(paramInt);
    MediaCodecInfo.CodecCapabilities[] arrayOfCodecCapabilities = new MediaCodecInfo.CodecCapabilities[arrayOfString.length];
    byte b1 = 0;
    int i;
    byte b2;
    for (i = arrayOfString.length, b2 = 0; b2 < i; ) {
      String str = arrayOfString[b2];
      arrayOfCodecCapabilities[b1] = getCodecCapabilities(paramInt, str);
      b2++;
      b1++;
    } 
    return 
      new MediaCodecInfo(getCodecName(paramInt), getCanonicalName(paramInt), getAttributes(paramInt), arrayOfCodecCapabilities);
  }
  
  public static MediaCodecInfo getInfoFor(String paramString) {
    initCodecList();
    return sAllCodecInfos[findCodecByName(paramString)];
  }
  
  private MediaCodecList() {
    this(0);
  }
  
  public MediaCodecList(int paramInt) {
    initCodecList();
    if (paramInt == 0) {
      this.mCodecInfos = sRegularCodecInfos;
    } else {
      this.mCodecInfos = sAllCodecInfos;
    } 
  }
  
  public final MediaCodecInfo[] getCodecInfos() {
    MediaCodecInfo[] arrayOfMediaCodecInfo = this.mCodecInfos;
    return Arrays.<MediaCodecInfo>copyOf(arrayOfMediaCodecInfo, arrayOfMediaCodecInfo.length);
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  public final String findDecoderForFormat(MediaFormat paramMediaFormat) {
    return findCodecForFormat(false, paramMediaFormat);
  }
  
  public final String findEncoderForFormat(MediaFormat paramMediaFormat) {
    return findCodecForFormat(true, paramMediaFormat);
  }
  
  private String findCodecForFormat(boolean paramBoolean, MediaFormat paramMediaFormat) {
    String str = paramMediaFormat.getString("mime");
    for (MediaCodecInfo mediaCodecInfo : this.mCodecInfos) {
      if (mediaCodecInfo.isEncoder() == paramBoolean)
        try {
          MediaCodecInfo.CodecCapabilities codecCapabilities = mediaCodecInfo.getCapabilitiesForType(str);
          if (codecCapabilities != null && codecCapabilities.isFormatSupported(paramMediaFormat))
            return mediaCodecInfo.getName(); 
        } catch (IllegalArgumentException illegalArgumentException) {} 
    } 
    return null;
  }
  
  static final native int findCodecByName(String paramString);
  
  static final native int getAttributes(int paramInt);
  
  static final native String getCanonicalName(int paramInt);
  
  static final native MediaCodecInfo.CodecCapabilities getCodecCapabilities(int paramInt, String paramString);
  
  static final native String getCodecName(int paramInt);
  
  static final native String[] getSupportedTypes(int paramInt);
  
  private static final native int native_getCodecCount();
  
  static final native Map<String, Object> native_getGlobalSettings();
  
  private static final native void native_init();
}
