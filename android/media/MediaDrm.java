package android.media;

import android.app.ActivityThread;
import android.os.Handler;
import android.os.HandlerExecutor;
import android.os.Looper;
import android.os.Parcel;
import android.os.PersistableBundle;
import android.util.Log;
import dalvik.system.CloseGuard;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;
import java.util.function.Function;

public final class MediaDrm implements AutoCloseable {
  private long mNativeContext;
  
  private final Map<Integer, ListenerWithExecutor> mListenerMap;
  
  private final AtomicBoolean mClosed = new AtomicBoolean();
  
  private final CloseGuard mCloseGuard = CloseGuard.get();
  
  private static final String TAG = "MediaDrm";
  
  private static final int SESSION_LOST_STATE = 203;
  
  public static final int SECURITY_LEVEL_UNKNOWN = 0;
  
  public static final int SECURITY_LEVEL_SW_SECURE_DECODE = 2;
  
  public static final int SECURITY_LEVEL_SW_SECURE_CRYPTO = 1;
  
  public static final int SECURITY_LEVEL_MAX = 6;
  
  public static final int SECURITY_LEVEL_HW_SECURE_DECODE = 4;
  
  public static final int SECURITY_LEVEL_HW_SECURE_CRYPTO = 3;
  
  public static final int SECURITY_LEVEL_HW_SECURE_ALL = 5;
  
  public static final String PROPERTY_VERSION = "version";
  
  public static final String PROPERTY_VENDOR = "vendor";
  
  public static final String PROPERTY_DEVICE_UNIQUE_ID = "deviceUniqueId";
  
  public static final String PROPERTY_DESCRIPTION = "description";
  
  public static final String PROPERTY_ALGORITHMS = "algorithms";
  
  private static final String PERMISSION = "android.permission.ACCESS_DRM_CERTIFICATES";
  
  public static final int OFFLINE_LICENSE_STATE_USABLE = 1;
  
  public static final int OFFLINE_LICENSE_STATE_UNKNOWN = 0;
  
  public static final int OFFLINE_LICENSE_STATE_RELEASED = 2;
  
  public static final int KEY_TYPE_STREAMING = 1;
  
  public static final int KEY_TYPE_RELEASE = 3;
  
  public static final int KEY_TYPE_OFFLINE = 2;
  
  private static final int KEY_STATUS_CHANGE = 202;
  
  public static final int HDCP_V2_3 = 6;
  
  public static final int HDCP_V2_2 = 5;
  
  public static final int HDCP_V2_1 = 4;
  
  public static final int HDCP_V2 = 3;
  
  public static final int HDCP_V1 = 2;
  
  public static final int HDCP_NO_DIGITAL_OUTPUT = 2147483647;
  
  public static final int HDCP_NONE = 1;
  
  public static final int HDCP_LEVEL_UNKNOWN = 0;
  
  private static final int EXPIRATION_UPDATE = 201;
  
  public static final int EVENT_VENDOR_DEFINED = 4;
  
  public static final int EVENT_SESSION_RECLAIMED = 5;
  
  public static final int EVENT_PROVISION_REQUIRED = 1;
  
  public static final int EVENT_KEY_REQUIRED = 2;
  
  public static final int EVENT_KEY_EXPIRED = 3;
  
  private static final int DRM_EVENT = 200;
  
  public static final int CERTIFICATE_TYPE_X509 = 1;
  
  public static final int CERTIFICATE_TYPE_NONE = 0;
  
  public static final boolean isCryptoSchemeSupported(UUID paramUUID) {
    return isCryptoSchemeSupportedNative(getByteArrayFromUUID(paramUUID), null, 0);
  }
  
  public static final boolean isCryptoSchemeSupported(UUID paramUUID, String paramString) {
    return isCryptoSchemeSupportedNative(getByteArrayFromUUID(paramUUID), paramString, 0);
  }
  
  public static final boolean isCryptoSchemeSupported(UUID paramUUID, String paramString, int paramInt) {
    return isCryptoSchemeSupportedNative(getByteArrayFromUUID(paramUUID), paramString, paramInt);
  }
  
  public static final List<UUID> getSupportedCryptoSchemes() {
    byte[] arrayOfByte = getSupportedCryptoSchemesNative();
    return getUUIDsFromByteArray(arrayOfByte);
  }
  
  private static final byte[] getByteArrayFromUUID(UUID paramUUID) {
    long l1 = paramUUID.getMostSignificantBits();
    long l2 = paramUUID.getLeastSignificantBits();
    byte[] arrayOfByte = new byte[16];
    for (byte b = 0; b < 8; b++) {
      arrayOfByte[b] = (byte)(int)(l1 >>> (7 - b) * 8);
      arrayOfByte[b + 8] = (byte)(int)(l2 >>> (7 - b) * 8);
    } 
    return arrayOfByte;
  }
  
  private static final UUID getUUIDFromByteArray(byte[] paramArrayOfbyte, int paramInt) {
    long l1 = 0L;
    long l2 = 0L;
    for (byte b = 0; b < 8; b++) {
      l1 = l1 << 8L | paramArrayOfbyte[paramInt + b] & 0xFFL;
      l2 = l2 << 8L | paramArrayOfbyte[paramInt + b + 8] & 0xFFL;
    } 
    return new UUID(l1, l2);
  }
  
  private static final List<UUID> getUUIDsFromByteArray(byte[] paramArrayOfbyte) {
    LinkedHashSet<UUID> linkedHashSet = new LinkedHashSet();
    for (byte b = 0; b < paramArrayOfbyte.length; b += 16)
      linkedHashSet.add(getUUIDFromByteArray(paramArrayOfbyte, b)); 
    return new ArrayList<>(linkedHashSet);
  }
  
  private Handler createHandler() {
    Looper looper = Looper.myLooper();
    if (looper != null) {
      Handler handler = new Handler(looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        Handler handler = new Handler(looper);
      } else {
        looper = null;
      } 
    } 
    return (Handler)looper;
  }
  
  public static final class MediaDrmStateException extends IllegalStateException {
    private final String mDiagnosticInfo;
    
    private final int mErrorCode;
    
    public MediaDrmStateException(int param1Int, String param1String) {
      super(param1String);
      this.mErrorCode = param1Int;
      if (param1Int < 0) {
        param1String = "neg_";
      } else {
        param1String = "";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("android.media.MediaDrm.error_");
      stringBuilder.append(param1String);
      stringBuilder.append(Math.abs(param1Int));
      this.mDiagnosticInfo = stringBuilder.toString();
    }
    
    public int getErrorCode() {
      return this.mErrorCode;
    }
    
    public String getDiagnosticInfo() {
      return this.mDiagnosticInfo;
    }
  }
  
  public static final class SessionException extends RuntimeException {
    public static final int ERROR_RESOURCE_CONTENTION = 1;
    
    public static final int ERROR_UNKNOWN = 0;
    
    private final int mErrorCode;
    
    public SessionException(int param1Int, String param1String) {
      super(param1String);
      this.mErrorCode = param1Int;
    }
    
    public int getErrorCode() {
      return this.mErrorCode;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface SessionErrorCode {}
  }
  
  public void setOnExpirationUpdateListener(OnExpirationUpdateListener paramOnExpirationUpdateListener, Handler paramHandler) {
    setListenerWithHandler(201, paramHandler, paramOnExpirationUpdateListener, new _$$Lambda$MediaDrm$dloezJ1eKxYxi1Oq_oYrMXoRpPM(this));
  }
  
  public void setOnExpirationUpdateListener(Executor paramExecutor, OnExpirationUpdateListener paramOnExpirationUpdateListener) {
    setListenerWithExecutor(201, paramExecutor, paramOnExpirationUpdateListener, new _$$Lambda$MediaDrm$dloezJ1eKxYxi1Oq_oYrMXoRpPM(this));
  }
  
  public void clearOnExpirationUpdateListener() {
    clearGenericListener(201);
  }
  
  public void setOnKeyStatusChangeListener(OnKeyStatusChangeListener paramOnKeyStatusChangeListener, Handler paramHandler) {
    setListenerWithHandler(202, paramHandler, paramOnKeyStatusChangeListener, new _$$Lambda$MediaDrm$V4Xmxq2t4qcaWIsuRLRluTj6MT0(this));
  }
  
  public void setOnKeyStatusChangeListener(Executor paramExecutor, OnKeyStatusChangeListener paramOnKeyStatusChangeListener) {
    setListenerWithExecutor(202, paramExecutor, paramOnKeyStatusChangeListener, new _$$Lambda$MediaDrm$V4Xmxq2t4qcaWIsuRLRluTj6MT0(this));
  }
  
  public void clearOnKeyStatusChangeListener() {
    clearGenericListener(202);
  }
  
  public void setOnSessionLostStateListener(OnSessionLostStateListener paramOnSessionLostStateListener, Handler paramHandler) {
    setListenerWithHandler(203, paramHandler, paramOnSessionLostStateListener, new _$$Lambda$MediaDrm$o5lC7TOBZhvtA31JYaLa_MogSw4(this));
  }
  
  public void setOnSessionLostStateListener(Executor paramExecutor, OnSessionLostStateListener paramOnSessionLostStateListener) {
    setListenerWithExecutor(203, paramExecutor, paramOnSessionLostStateListener, new _$$Lambda$MediaDrm$o5lC7TOBZhvtA31JYaLa_MogSw4(this));
  }
  
  public void clearOnSessionLostStateListener() {
    clearGenericListener(203);
  }
  
  public static final class KeyStatus {
    public static final int STATUS_EXPIRED = 1;
    
    public static final int STATUS_INTERNAL_ERROR = 4;
    
    public static final int STATUS_OUTPUT_NOT_ALLOWED = 2;
    
    public static final int STATUS_PENDING = 3;
    
    public static final int STATUS_USABLE = 0;
    
    public static final int STATUS_USABLE_IN_FUTURE = 5;
    
    private final byte[] mKeyId;
    
    private final int mStatusCode;
    
    KeyStatus(byte[] param1ArrayOfbyte, int param1Int) {
      this.mKeyId = param1ArrayOfbyte;
      this.mStatusCode = param1Int;
    }
    
    public int getStatusCode() {
      return this.mStatusCode;
    }
    
    public byte[] getKeyId() {
      return this.mKeyId;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface KeyStatusCode {}
  }
  
  public void setOnEventListener(OnEventListener paramOnEventListener) {
    setOnEventListener(paramOnEventListener, (Handler)null);
  }
  
  public void setOnEventListener(OnEventListener paramOnEventListener, Handler paramHandler) {
    setListenerWithHandler(200, paramHandler, paramOnEventListener, new _$$Lambda$MediaDrm$IvEWhXQgSYABwC6_1bdnhTJ4V2I(this));
  }
  
  public void setOnEventListener(Executor paramExecutor, OnEventListener paramOnEventListener) {
    setListenerWithExecutor(200, paramExecutor, paramOnEventListener, new _$$Lambda$MediaDrm$IvEWhXQgSYABwC6_1bdnhTJ4V2I(this));
  }
  
  public void clearOnEventListener() {
    clearGenericListener(200);
  }
  
  public MediaDrm(UUID paramUUID) throws UnsupportedSchemeException {
    this.mListenerMap = new ConcurrentHashMap<>();
    WeakReference<MediaDrm> weakReference = new WeakReference<>(this);
    byte[] arrayOfByte = getByteArrayFromUUID(paramUUID);
    String str = ActivityThread.currentOpPackageName();
    native_setup(weakReference, arrayOfByte, str);
    this.mCloseGuard.open("release");
  }
  
  private <T> void setListenerWithHandler(int paramInt, Handler paramHandler, T paramT, Function<T, Consumer<ListenerArgs>> paramFunction) {
    if (paramT == null) {
      clearGenericListener(paramInt);
    } else {
      if (paramHandler == null)
        paramHandler = createHandler(); 
      HandlerExecutor handlerExecutor = new HandlerExecutor(paramHandler);
      setGenericListener(paramInt, handlerExecutor, paramT, paramFunction);
    } 
  }
  
  private <T> void setListenerWithExecutor(int paramInt, Executor paramExecutor, T paramT, Function<T, Consumer<ListenerArgs>> paramFunction) {
    if (paramExecutor != null && paramT != null) {
      setGenericListener(paramInt, paramExecutor, paramT, paramFunction);
      return;
    } 
    String str = String.format("executor %s listener %s", new Object[] { paramExecutor, paramT });
    throw new IllegalArgumentException(str);
  }
  
  private <T> void setGenericListener(int paramInt, Executor paramExecutor, T paramT, Function<T, Consumer<ListenerArgs>> paramFunction) {
    this.mListenerMap.put(Integer.valueOf(paramInt), new ListenerWithExecutor(paramExecutor, paramFunction.apply(paramT)));
  }
  
  private void clearGenericListener(int paramInt) {
    this.mListenerMap.remove(Integer.valueOf(paramInt));
  }
  
  private Consumer<ListenerArgs> createOnEventListener(OnEventListener paramOnEventListener) {
    return new _$$Lambda$MediaDrm$8rRollK1F3eENvuaBGoS8u__heQ(this, paramOnEventListener);
  }
  
  private Consumer<ListenerArgs> createOnKeyStatusChangeListener(OnKeyStatusChangeListener paramOnKeyStatusChangeListener) {
    return new _$$Lambda$MediaDrm$_FHBF1q3qSxz22Mhv8jmgjN4xt0(this, paramOnKeyStatusChangeListener);
  }
  
  private Consumer<ListenerArgs> createOnExpirationUpdateListener(OnExpirationUpdateListener paramOnExpirationUpdateListener) {
    return new _$$Lambda$MediaDrm$btxNighXxrJ0k5ooHZIA_tMesRA(this, paramOnExpirationUpdateListener);
  }
  
  private Consumer<ListenerArgs> createOnSessionLostStateListener(OnSessionLostStateListener paramOnSessionLostStateListener) {
    return new _$$Lambda$MediaDrm$4XHJHM_muz_p2PFHVhlVJb_7ccc(this, paramOnSessionLostStateListener);
  }
  
  private static class ListenerArgs {
    private final int arg1;
    
    private final int arg2;
    
    private final byte[] data;
    
    private final long expirationTime;
    
    private final boolean hasNewUsableKey;
    
    private final List<MediaDrm.KeyStatus> keyStatusList;
    
    private final byte[] sessionId;
    
    public ListenerArgs(int param1Int1, int param1Int2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, long param1Long, List<MediaDrm.KeyStatus> param1List, boolean param1Boolean) {
      this.arg1 = param1Int1;
      this.arg2 = param1Int2;
      this.sessionId = param1ArrayOfbyte1;
      this.data = param1ArrayOfbyte2;
      this.expirationTime = param1Long;
      this.keyStatusList = param1List;
      this.hasNewUsableKey = param1Boolean;
    }
  }
  
  private static class ListenerWithExecutor {
    private final Consumer<MediaDrm.ListenerArgs> mConsumer;
    
    private final Executor mExecutor;
    
    public ListenerWithExecutor(Executor param1Executor, Consumer<MediaDrm.ListenerArgs> param1Consumer) {
      this.mExecutor = param1Executor;
      this.mConsumer = param1Consumer;
    }
  }
  
  private List<KeyStatus> keyStatusListFromParcel(Parcel paramParcel) {
    int i = paramParcel.readInt();
    ArrayList<KeyStatus> arrayList = new ArrayList(i);
    while (i > 0) {
      byte[] arrayOfByte = paramParcel.createByteArray();
      int j = paramParcel.readInt();
      arrayList.add(new KeyStatus(arrayOfByte, j));
      i--;
    } 
    return arrayList;
  }
  
  private static void postEventFromNative(Object paramObject, int paramInt1, int paramInt2, int paramInt3, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, long paramLong, List<KeyStatus> paramList, boolean paramBoolean) {
    MediaDrm mediaDrm = ((WeakReference<MediaDrm>)paramObject).get();
    if (mediaDrm == null)
      return; 
    switch (paramInt1) {
      default:
        paramObject = new StringBuilder();
        paramObject.append("Unknown message type ");
        paramObject.append(paramInt1);
        Log.e("MediaDrm", paramObject.toString());
        return;
      case 200:
      case 201:
      case 202:
      case 203:
        break;
    } 
    paramObject = mediaDrm.mListenerMap.get(Integer.valueOf(paramInt1));
    if (paramObject != null) {
      _$$Lambda$MediaDrm$UPVWCanGo24eu9_1S_t6PvJ1Zno _$$Lambda$MediaDrm$UPVWCanGo24eu9_1S_t6PvJ1Zno = new _$$Lambda$MediaDrm$UPVWCanGo24eu9_1S_t6PvJ1Zno(mediaDrm, paramInt2, paramInt3, paramArrayOfbyte1, paramArrayOfbyte2, paramLong, paramList, paramBoolean, (ListenerWithExecutor)paramObject);
      ((ListenerWithExecutor)paramObject).mExecutor.execute(_$$Lambda$MediaDrm$UPVWCanGo24eu9_1S_t6PvJ1Zno);
    } 
  }
  
  public byte[] openSession() throws NotProvisionedException, ResourceBusyException {
    return openSession(getMaxSecurityLevel());
  }
  
  public static final class KeyRequest {
    public static final int REQUEST_TYPE_INITIAL = 0;
    
    public static final int REQUEST_TYPE_NONE = 3;
    
    public static final int REQUEST_TYPE_RELEASE = 2;
    
    public static final int REQUEST_TYPE_RENEWAL = 1;
    
    public static final int REQUEST_TYPE_UPDATE = 4;
    
    private byte[] mData;
    
    private String mDefaultUrl;
    
    private int mRequestType;
    
    public byte[] getData() {
      byte[] arrayOfByte = this.mData;
      if (arrayOfByte != null)
        return arrayOfByte; 
      throw new RuntimeException("KeyRequest is not initialized");
    }
    
    public String getDefaultUrl() {
      String str = this.mDefaultUrl;
      if (str != null)
        return str; 
      throw new RuntimeException("KeyRequest is not initialized");
    }
    
    public int getRequestType() {
      return this.mRequestType;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface RequestType {}
  }
  
  public static final class ProvisionRequest {
    private byte[] mData;
    
    private String mDefaultUrl;
    
    public byte[] getData() {
      byte[] arrayOfByte = this.mData;
      if (arrayOfByte != null)
        return arrayOfByte; 
      throw new RuntimeException("ProvisionRequest is not initialized");
    }
    
    public String getDefaultUrl() {
      String str = this.mDefaultUrl;
      if (str != null)
        return str; 
      throw new RuntimeException("ProvisionRequest is not initialized");
    }
  }
  
  public ProvisionRequest getProvisionRequest() {
    return getProvisionRequestNative(0, "");
  }
  
  public void provideProvisionResponse(byte[] paramArrayOfbyte) throws DeniedByServerException {
    provideProvisionResponseNative(paramArrayOfbyte);
  }
  
  public void releaseAllSecureStops() {
    removeAllSecureStops();
  }
  
  public static final int getMaxSecurityLevel() {
    return 6;
  }
  
  public PersistableBundle getMetrics() {
    return getMetricsNative();
  }
  
  public final class CryptoSession {
    private byte[] mSessionId;
    
    final MediaDrm this$0;
    
    CryptoSession(byte[] param1ArrayOfbyte, String param1String1, String param1String2) {
      this.mSessionId = param1ArrayOfbyte;
      MediaDrm.setCipherAlgorithmNative(MediaDrm.this, param1ArrayOfbyte, param1String1);
      MediaDrm.setMacAlgorithmNative(MediaDrm.this, param1ArrayOfbyte, param1String2);
    }
    
    public byte[] encrypt(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) {
      return MediaDrm.encryptNative(MediaDrm.this, this.mSessionId, param1ArrayOfbyte1, param1ArrayOfbyte2, param1ArrayOfbyte3);
    }
    
    public byte[] decrypt(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) {
      return MediaDrm.decryptNative(MediaDrm.this, this.mSessionId, param1ArrayOfbyte1, param1ArrayOfbyte2, param1ArrayOfbyte3);
    }
    
    public byte[] sign(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      return MediaDrm.signNative(MediaDrm.this, this.mSessionId, param1ArrayOfbyte1, param1ArrayOfbyte2);
    }
    
    public boolean verify(byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, byte[] param1ArrayOfbyte3) {
      return MediaDrm.verifyNative(MediaDrm.this, this.mSessionId, param1ArrayOfbyte1, param1ArrayOfbyte2, param1ArrayOfbyte3);
    }
  }
  
  public CryptoSession getCryptoSession(byte[] paramArrayOfbyte, String paramString1, String paramString2) {
    return new CryptoSession(paramArrayOfbyte, paramString1, paramString2);
  }
  
  public static final class CertificateRequest {
    private byte[] mData;
    
    private String mDefaultUrl;
    
    CertificateRequest(byte[] param1ArrayOfbyte, String param1String) {
      this.mData = param1ArrayOfbyte;
      this.mDefaultUrl = param1String;
    }
    
    public byte[] getData() {
      return this.mData;
    }
    
    public String getDefaultUrl() {
      return this.mDefaultUrl;
    }
  }
  
  public CertificateRequest getCertificateRequest(int paramInt, String paramString) {
    ProvisionRequest provisionRequest = getProvisionRequestNative(paramInt, paramString);
    byte[] arrayOfByte = provisionRequest.getData();
    return 
      new CertificateRequest(arrayOfByte, provisionRequest.getDefaultUrl());
  }
  
  public static final class Certificate {
    private byte[] mCertificateData;
    
    private byte[] mWrappedKey;
    
    public byte[] getWrappedPrivateKey() {
      byte[] arrayOfByte = this.mWrappedKey;
      if (arrayOfByte != null)
        return arrayOfByte; 
      throw new RuntimeException("Certificate is not initialized");
    }
    
    public byte[] getContent() {
      byte[] arrayOfByte = this.mCertificateData;
      if (arrayOfByte != null)
        return arrayOfByte; 
      throw new RuntimeException("Certificate is not initialized");
    }
  }
  
  public Certificate provideCertificateResponse(byte[] paramArrayOfbyte) throws DeniedByServerException {
    return provideProvisionResponseNative(paramArrayOfbyte);
  }
  
  public byte[] signRSA(byte[] paramArrayOfbyte1, String paramString, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3) {
    return signRSANative(this, paramArrayOfbyte1, paramString, paramArrayOfbyte2, paramArrayOfbyte3);
  }
  
  protected void finalize() throws Throwable {
    try {
      if (this.mCloseGuard != null)
        this.mCloseGuard.warnIfOpen(); 
      release();
      return;
    } finally {
      super.finalize();
    } 
  }
  
  public void close() {
    release();
  }
  
  @Deprecated
  public void release() {
    this.mCloseGuard.close();
    if (this.mClosed.compareAndSet(false, true))
      native_release(); 
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private static final native byte[] decryptNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4);
  
  private static final native byte[] encryptNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4);
  
  private native PersistableBundle getMetricsNative();
  
  private native ProvisionRequest getProvisionRequestNative(int paramInt, String paramString);
  
  private static final native byte[] getSupportedCryptoSchemesNative();
  
  private static final native boolean isCryptoSchemeSupportedNative(byte[] paramArrayOfbyte, String paramString, int paramInt);
  
  private static final native void native_init();
  
  private final native void native_setup(Object paramObject, byte[] paramArrayOfbyte, String paramString);
  
  private native Certificate provideProvisionResponseNative(byte[] paramArrayOfbyte) throws DeniedByServerException;
  
  private static final native void setCipherAlgorithmNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte, String paramString);
  
  private static final native void setMacAlgorithmNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte, String paramString);
  
  private static final native byte[] signNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3);
  
  private static final native byte[] signRSANative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte1, String paramString, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3);
  
  private static final native boolean verifyNative(MediaDrm paramMediaDrm, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, byte[] paramArrayOfbyte3, byte[] paramArrayOfbyte4);
  
  public native void closeSession(byte[] paramArrayOfbyte);
  
  public native int getConnectedHdcpLevel();
  
  public native KeyRequest getKeyRequest(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2, String paramString, int paramInt, HashMap<String, String> paramHashMap) throws NotProvisionedException;
  
  public native int getMaxHdcpLevel();
  
  public native int getMaxSessionCount();
  
  public native List<byte[]> getOfflineLicenseKeySetIds();
  
  public native int getOfflineLicenseState(byte[] paramArrayOfbyte);
  
  public native int getOpenSessionCount();
  
  public native byte[] getPropertyByteArray(String paramString);
  
  public native String getPropertyString(String paramString);
  
  public native byte[] getSecureStop(byte[] paramArrayOfbyte);
  
  public native List<byte[]> getSecureStopIds();
  
  public native List<byte[]> getSecureStops();
  
  public native int getSecurityLevel(byte[] paramArrayOfbyte);
  
  public final native void native_release();
  
  public native byte[] openSession(int paramInt) throws NotProvisionedException, ResourceBusyException;
  
  public native byte[] provideKeyResponse(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws NotProvisionedException, DeniedByServerException;
  
  public native HashMap<String, String> queryKeyStatus(byte[] paramArrayOfbyte);
  
  public native void releaseSecureStops(byte[] paramArrayOfbyte);
  
  public native void removeAllSecureStops();
  
  public native void removeKeys(byte[] paramArrayOfbyte);
  
  public native void removeOfflineLicense(byte[] paramArrayOfbyte);
  
  public native void removeSecureStop(byte[] paramArrayOfbyte);
  
  public native void restoreKeys(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2);
  
  public native void setPropertyByteArray(String paramString, byte[] paramArrayOfbyte);
  
  public native void setPropertyString(String paramString1, String paramString2);
  
  public static final class MetricsConstants {
    public static final String CLOSE_SESSION_ERROR_COUNT = "drm.mediadrm.close_session.error.count";
    
    public static final String CLOSE_SESSION_ERROR_LIST = "drm.mediadrm.close_session.error.list";
    
    public static final String CLOSE_SESSION_OK_COUNT = "drm.mediadrm.close_session.ok.count";
    
    public static final String EVENT_KEY_EXPIRED_COUNT = "drm.mediadrm.event.KEY_EXPIRED.count";
    
    public static final String EVENT_KEY_NEEDED_COUNT = "drm.mediadrm.event.KEY_NEEDED.count";
    
    public static final String EVENT_PROVISION_REQUIRED_COUNT = "drm.mediadrm.event.PROVISION_REQUIRED.count";
    
    public static final String EVENT_SESSION_RECLAIMED_COUNT = "drm.mediadrm.event.SESSION_RECLAIMED.count";
    
    public static final String EVENT_VENDOR_DEFINED_COUNT = "drm.mediadrm.event.VENDOR_DEFINED.count";
    
    public static final String GET_DEVICE_UNIQUE_ID_ERROR_COUNT = "drm.mediadrm.get_device_unique_id.error.count";
    
    public static final String GET_DEVICE_UNIQUE_ID_ERROR_LIST = "drm.mediadrm.get_device_unique_id.error.list";
    
    public static final String GET_DEVICE_UNIQUE_ID_OK_COUNT = "drm.mediadrm.get_device_unique_id.ok.count";
    
    public static final String GET_KEY_REQUEST_ERROR_COUNT = "drm.mediadrm.get_key_request.error.count";
    
    public static final String GET_KEY_REQUEST_ERROR_LIST = "drm.mediadrm.get_key_request.error.list";
    
    public static final String GET_KEY_REQUEST_OK_COUNT = "drm.mediadrm.get_key_request.ok.count";
    
    public static final String GET_KEY_REQUEST_OK_TIME_MICROS = "drm.mediadrm.get_key_request.ok.average_time_micros";
    
    public static final String GET_PROVISION_REQUEST_ERROR_COUNT = "drm.mediadrm.get_provision_request.error.count";
    
    public static final String GET_PROVISION_REQUEST_ERROR_LIST = "drm.mediadrm.get_provision_request.error.list";
    
    public static final String GET_PROVISION_REQUEST_OK_COUNT = "drm.mediadrm.get_provision_request.ok.count";
    
    public static final String KEY_STATUS_EXPIRED_COUNT = "drm.mediadrm.key_status.EXPIRED.count";
    
    public static final String KEY_STATUS_INTERNAL_ERROR_COUNT = "drm.mediadrm.key_status.INTERNAL_ERROR.count";
    
    public static final String KEY_STATUS_OUTPUT_NOT_ALLOWED_COUNT = "drm.mediadrm.key_status_change.OUTPUT_NOT_ALLOWED.count";
    
    public static final String KEY_STATUS_PENDING_COUNT = "drm.mediadrm.key_status_change.PENDING.count";
    
    public static final String KEY_STATUS_USABLE_COUNT = "drm.mediadrm.key_status_change.USABLE.count";
    
    public static final String OPEN_SESSION_ERROR_COUNT = "drm.mediadrm.open_session.error.count";
    
    public static final String OPEN_SESSION_ERROR_LIST = "drm.mediadrm.open_session.error.list";
    
    public static final String OPEN_SESSION_OK_COUNT = "drm.mediadrm.open_session.ok.count";
    
    public static final String PROVIDE_KEY_RESPONSE_ERROR_COUNT = "drm.mediadrm.provide_key_response.error.count";
    
    public static final String PROVIDE_KEY_RESPONSE_ERROR_LIST = "drm.mediadrm.provide_key_response.error.list";
    
    public static final String PROVIDE_KEY_RESPONSE_OK_COUNT = "drm.mediadrm.provide_key_response.ok.count";
    
    public static final String PROVIDE_KEY_RESPONSE_OK_TIME_MICROS = "drm.mediadrm.provide_key_response.ok.average_time_micros";
    
    public static final String PROVIDE_PROVISION_RESPONSE_ERROR_COUNT = "drm.mediadrm.provide_provision_response.error.count";
    
    public static final String PROVIDE_PROVISION_RESPONSE_ERROR_LIST = "drm.mediadrm.provide_provision_response.error.list";
    
    public static final String PROVIDE_PROVISION_RESPONSE_OK_COUNT = "drm.mediadrm.provide_provision_response.ok.count";
    
    public static final String SESSION_END_TIMES_MS = "drm.mediadrm.session_end_times_ms";
    
    public static final String SESSION_START_TIMES_MS = "drm.mediadrm.session_start_times_ms";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ArrayProperty {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CertificateType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface DrmEvent {}
  
  @Deprecated
  @Retention(RetentionPolicy.SOURCE)
  public static @interface HdcpLevel {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface RequestType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeyStatusCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface KeyType {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OfflineLicenseState {}
  
  public static interface OnEventListener {
    void onEvent(MediaDrm param1MediaDrm, byte[] param1ArrayOfbyte1, int param1Int1, int param1Int2, byte[] param1ArrayOfbyte2);
  }
  
  public static interface OnExpirationUpdateListener {
    void onExpirationUpdate(MediaDrm param1MediaDrm, byte[] param1ArrayOfbyte, long param1Long);
  }
  
  public static interface OnKeyStatusChangeListener {
    void onKeyStatusChange(MediaDrm param1MediaDrm, byte[] param1ArrayOfbyte, List<MediaDrm.KeyStatus> param1List, boolean param1Boolean);
  }
  
  public static interface OnSessionLostStateListener {
    void onSessionLostState(MediaDrm param1MediaDrm, byte[] param1ArrayOfbyte);
  }
  
  @Deprecated
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SecurityLevel {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface SessionErrorCode {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface StringProperty {}
}
