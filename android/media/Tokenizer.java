package android.media;

import android.util.Log;

class Tokenizer {
  private static final String TAG = "Tokenizer";
  
  private TokenizerPhase mDataTokenizer;
  
  private int mHandledLen;
  
  private String mLine;
  
  private OnTokenListener mListener;
  
  private TokenizerPhase mPhase;
  
  private TokenizerPhase mTagTokenizer;
  
  class DataTokenizer implements TokenizerPhase {
    private StringBuilder mData;
    
    final Tokenizer this$0;
    
    public Tokenizer.TokenizerPhase start() {
      this.mData = new StringBuilder();
      return this;
    }
    
    private boolean replaceEscape(String param1String1, String param1String2, int param1Int) {
      if (Tokenizer.this.mLine.startsWith(param1String1, param1Int)) {
        this.mData.append(Tokenizer.this.mLine.substring(Tokenizer.this.mHandledLen, param1Int));
        this.mData.append(param1String2);
        Tokenizer.access$102(Tokenizer.this, param1String1.length() + param1Int);
        Tokenizer.this.mHandledLen;
        return true;
      } 
      return false;
    }
    
    public void tokenize() {
      int k, i = Tokenizer.this.mLine.length();
      int j = Tokenizer.this.mHandledLen;
      while (true) {
        k = i;
        if (j < Tokenizer.this.mLine.length()) {
          if (Tokenizer.this.mLine.charAt(j) == '&') {
            if (replaceEscape("&amp;", "&", j) || 
              replaceEscape("&lt;", "<", j) || 
              replaceEscape("&gt;", ">", j) || 
              replaceEscape("&lrm;", "‎", j) || 
              replaceEscape("&rlm;", "‏", j) || 
              replaceEscape("&nbsp;", " ", j));
          } else if (Tokenizer.this.mLine.charAt(j) == '<') {
            Tokenizer tokenizer = Tokenizer.this;
            Tokenizer.access$202(tokenizer, tokenizer.mTagTokenizer.start());
            k = j;
            break;
          } 
          j++;
          continue;
        } 
        break;
      } 
      this.mData.append(Tokenizer.this.mLine.substring(Tokenizer.this.mHandledLen, k));
      Tokenizer.this.mListener.onData(this.mData.toString());
      StringBuilder stringBuilder = this.mData;
      stringBuilder.delete(0, stringBuilder.length());
      Tokenizer.access$102(Tokenizer.this, k);
    }
  }
  
  static interface OnTokenListener {
    void onData(String param1String);
    
    void onEnd(String param1String);
    
    void onLineEnd();
    
    void onStart(String param1String1, String[] param1ArrayOfString, String param1String2);
    
    void onTimeStamp(long param1Long);
  }
  
  class TagTokenizer implements TokenizerPhase {
    private String mAnnotation;
    
    private boolean mAtAnnotation;
    
    private String mName;
    
    final Tokenizer this$0;
    
    public Tokenizer.TokenizerPhase start() {
      this.mAnnotation = "";
      this.mName = "";
      this.mAtAnnotation = false;
      return this;
    }
    
    public void tokenize() {
      if (!this.mAtAnnotation)
        Tokenizer.access$108(Tokenizer.this); 
      if (Tokenizer.this.mHandledLen < Tokenizer.this.mLine.length()) {
        String[] arrayOfString;
        if (this.mAtAnnotation || Tokenizer.this.mLine.charAt(Tokenizer.this.mHandledLen) == '/') {
          arrayOfString = Tokenizer.this.mLine.substring(Tokenizer.this.mHandledLen).split(">");
        } else {
          arrayOfString = Tokenizer.this.mLine.substring(Tokenizer.this.mHandledLen).split("[\t\f >]");
        } 
        String str = Tokenizer.this.mLine;
        Tokenizer tokenizer = Tokenizer.this;
        int i = tokenizer.mHandledLen, j = Tokenizer.this.mHandledLen, k = arrayOfString[0].length();
        str = str.substring(i, j + k);
        Tokenizer.access$112(Tokenizer.this, arrayOfString[0].length());
        if (this.mAtAnnotation) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append(this.mAnnotation);
          stringBuilder.append(" ");
          stringBuilder.append(str);
          this.mAnnotation = stringBuilder.toString();
        } else {
          this.mName = str;
        } 
      } 
      this.mAtAnnotation = true;
      if (Tokenizer.this.mHandledLen < Tokenizer.this.mLine.length() && Tokenizer.this.mLine.charAt(Tokenizer.this.mHandledLen) == '>') {
        yield_tag();
        Tokenizer tokenizer = Tokenizer.this;
        Tokenizer.access$202(tokenizer, tokenizer.mDataTokenizer.start());
        Tokenizer.access$108(Tokenizer.this);
      } 
    }
    
    private void yield_tag() {
      if (this.mName.startsWith("/")) {
        Tokenizer.this.mListener.onEnd(this.mName.substring(1));
      } else if (this.mName.length() > 0 && Character.isDigit(this.mName.charAt(0))) {
        try {
          long l = WebVttParser.parseTimestampMs(this.mName);
          Tokenizer.this.mListener.onTimeStamp(l);
        } catch (NumberFormatException numberFormatException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("invalid timestamp tag: <");
          stringBuilder.append(this.mName);
          stringBuilder.append(">");
          Log.d("Tokenizer", stringBuilder.toString());
        } 
      } else {
        String arrayOfString[], str = this.mAnnotation.replaceAll("\\s+", " ");
        if (str.startsWith(" "))
          this.mAnnotation = this.mAnnotation.substring(1); 
        if (this.mAnnotation.endsWith(" ")) {
          str = this.mAnnotation;
          this.mAnnotation = str.substring(0, str.length() - 1);
        } 
        str = null;
        int i = this.mName.indexOf('.');
        if (i >= 0) {
          arrayOfString = this.mName.substring(i + 1).split("\\.");
          this.mName = this.mName.substring(0, i);
        } 
        Tokenizer.this.mListener.onStart(this.mName, arrayOfString, this.mAnnotation);
      } 
    }
  }
  
  Tokenizer(OnTokenListener paramOnTokenListener) {
    this.mDataTokenizer = new DataTokenizer();
    this.mTagTokenizer = new TagTokenizer();
    reset();
    this.mListener = paramOnTokenListener;
  }
  
  void reset() {
    this.mPhase = this.mDataTokenizer.start();
  }
  
  void tokenize(String paramString) {
    this.mHandledLen = 0;
    this.mLine = paramString;
    while (this.mHandledLen < this.mLine.length())
      this.mPhase.tokenize(); 
    if (!(this.mPhase instanceof TagTokenizer))
      this.mListener.onLineEnd(); 
  }
  
  static interface TokenizerPhase {
    TokenizerPhase start();
    
    void tokenize();
  }
}
