package android.media;

import android.graphics.Color;
import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

class Cea708CCParser {
  public static final int CAPTION_EMIT_TYPE_BUFFER = 1;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_CLW = 4;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_CWX = 3;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_DFX = 16;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_DLC = 10;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_DLW = 8;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_DLY = 9;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_DSW = 5;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_HDW = 6;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_RST = 11;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_SPA = 12;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_SPC = 13;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_SPL = 14;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_SWA = 15;
  
  public static final int CAPTION_EMIT_TYPE_COMMAND_TGW = 7;
  
  public static final int CAPTION_EMIT_TYPE_CONTROL = 2;
  
  private static final boolean DEBUG = false;
  
  private static final String MUSIC_NOTE_CHAR;
  
  private static final String TAG = "Cea708CCParser";
  
  static {
    Charset charset = StandardCharsets.UTF_8;
    MUSIC_NOTE_CHAR = new String("♫".getBytes(charset), StandardCharsets.UTF_8);
  }
  
  private final StringBuffer mBuffer = new StringBuffer();
  
  private int mCommand = 0;
  
  private DisplayListener mListener = (DisplayListener)new Object(this);
  
  Cea708CCParser(DisplayListener paramDisplayListener) {
    if (paramDisplayListener != null)
      this.mListener = paramDisplayListener; 
  }
  
  private void emitCaptionEvent(CaptionEvent paramCaptionEvent) {
    emitCaptionBuffer();
    this.mListener.emitEvent(paramCaptionEvent);
  }
  
  private void emitCaptionBuffer() {
    if (this.mBuffer.length() > 0) {
      this.mListener.emitEvent(new CaptionEvent(1, this.mBuffer.toString()));
      this.mBuffer.setLength(0);
    } 
  }
  
  public void parse(byte[] paramArrayOfbyte) {
    int i = 0;
    while (i < paramArrayOfbyte.length)
      i = parseServiceBlockData(paramArrayOfbyte, i); 
    emitCaptionBuffer();
  }
  
  private int parseServiceBlockData(byte[] paramArrayOfbyte, int paramInt) {
    int i = paramArrayOfbyte[paramInt] & 0xFF;
    int j = paramInt + 1;
    if (i == 16) {
      paramInt = parseExt1(paramArrayOfbyte, j);
    } else if (i >= 0 && i <= 31) {
      paramInt = parseC0(paramArrayOfbyte, j);
    } else {
      paramInt = this.mCommand;
      if (paramInt >= 128 && paramInt <= 159) {
        paramInt = parseC1(paramArrayOfbyte, j);
      } else {
        paramInt = this.mCommand;
        if (paramInt >= 32 && paramInt <= 127) {
          paramInt = parseG0(paramArrayOfbyte, j);
        } else {
          i = this.mCommand;
          paramInt = j;
          if (i >= 160) {
            paramInt = j;
            if (i <= 255)
              paramInt = parseG1(paramArrayOfbyte, j); 
          } 
        } 
      } 
    } 
    return paramInt;
  }
  
  private int parseC0(byte[] paramArrayOfbyte, int paramInt) {
    int i = this.mCommand;
    if (i >= 24 && i <= 31) {
      if (i == 24)
        if (paramArrayOfbyte[paramInt] == 0) {
          try {
            this.mBuffer.append((char)paramArrayOfbyte[paramInt + 1]);
          } catch (UnsupportedEncodingException unsupportedEncodingException) {
            Log.e("Cea708CCParser", "P16 Code - Could not find supported encoding", unsupportedEncodingException);
          } 
        } else {
          String str = new String();
          this(Arrays.copyOfRange((byte[])unsupportedEncodingException, paramInt, paramInt + 2), "EUC-KR");
          this.mBuffer.append(str);
        }  
      paramInt += 2;
    } 
    i = this.mCommand;
    if (i >= 16 && i <= 23)
      paramInt++; 
    i = this.mCommand;
    if (i != 3) {
      if (i != 8) {
        switch (i) {
          default:
            return paramInt;
          case 14:
            emitCaptionEvent(new CaptionEvent(2, Character.valueOf((char)i)));
          case 13:
            this.mBuffer.append('\n');
          case 12:
            break;
        } 
        emitCaptionEvent(new CaptionEvent(2, Character.valueOf((char)i)));
      } 
      emitCaptionEvent(new CaptionEvent(2, Character.valueOf((char)i)));
    } 
    emitCaptionEvent(new CaptionEvent(2, Character.valueOf((char)i)));
  }
  
  private int parseC1(byte[] paramArrayOfbyte, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mCommand : I
    //   4: istore_3
    //   5: iload_3
    //   6: tableswitch default -> 148, 128 -> 1241, 129 -> 1241, 130 -> 1241, 131 -> 1241, 132 -> 1241, 133 -> 1241, 134 -> 1241, 135 -> 1241, 136 -> 1209, 137 -> 1177, 138 -> 1144, 139 -> 1111, 140 -> 1078, 141 -> 1045, 142 -> 1028, 143 -> 1011, 144 -> 880, 145 -> 679, 146 -> 631, 147 -> 148, 148 -> 148, 149 -> 148, 150 -> 148, 151 -> 365, 152 -> 151, 153 -> 151, 154 -> 151, 155 -> 151, 156 -> 151, 157 -> 151, 158 -> 151, 159 -> 151
    //   148: goto -> 1261
    //   151: aload_1
    //   152: iload_2
    //   153: baload
    //   154: bipush #32
    //   156: iand
    //   157: ifeq -> 166
    //   160: iconst_1
    //   161: istore #4
    //   163: goto -> 169
    //   166: iconst_0
    //   167: istore #4
    //   169: aload_1
    //   170: iload_2
    //   171: baload
    //   172: bipush #16
    //   174: iand
    //   175: ifeq -> 184
    //   178: iconst_1
    //   179: istore #5
    //   181: goto -> 187
    //   184: iconst_0
    //   185: istore #5
    //   187: aload_1
    //   188: iload_2
    //   189: baload
    //   190: bipush #8
    //   192: iand
    //   193: ifeq -> 202
    //   196: iconst_1
    //   197: istore #6
    //   199: goto -> 205
    //   202: iconst_0
    //   203: istore #6
    //   205: aload_1
    //   206: iload_2
    //   207: baload
    //   208: istore #7
    //   210: aload_1
    //   211: iload_2
    //   212: iconst_1
    //   213: iadd
    //   214: baload
    //   215: sipush #128
    //   218: iand
    //   219: ifeq -> 228
    //   222: iconst_1
    //   223: istore #8
    //   225: goto -> 231
    //   228: iconst_0
    //   229: istore #8
    //   231: aload_1
    //   232: iload_2
    //   233: iconst_1
    //   234: iadd
    //   235: baload
    //   236: istore #9
    //   238: aload_1
    //   239: iload_2
    //   240: iconst_2
    //   241: iadd
    //   242: baload
    //   243: istore #10
    //   245: aload_1
    //   246: iload_2
    //   247: iconst_3
    //   248: iadd
    //   249: baload
    //   250: istore #11
    //   252: aload_1
    //   253: iload_2
    //   254: iconst_3
    //   255: iadd
    //   256: baload
    //   257: istore #12
    //   259: aload_1
    //   260: iload_2
    //   261: iconst_4
    //   262: iadd
    //   263: baload
    //   264: istore #13
    //   266: aload_1
    //   267: iload_2
    //   268: iconst_5
    //   269: iadd
    //   270: baload
    //   271: istore #14
    //   273: aload_1
    //   274: iload_2
    //   275: iconst_5
    //   276: iadd
    //   277: baload
    //   278: istore #15
    //   280: aload_0
    //   281: new android/media/Cea708CCParser$CaptionEvent
    //   284: dup
    //   285: bipush #16
    //   287: new android/media/Cea708CCParser$CaptionWindow
    //   290: dup
    //   291: iload_3
    //   292: sipush #152
    //   295: isub
    //   296: iload #4
    //   298: iload #5
    //   300: iload #6
    //   302: iload #7
    //   304: bipush #7
    //   306: iand
    //   307: iload #8
    //   309: iload #9
    //   311: bipush #127
    //   313: iand
    //   314: iload #10
    //   316: sipush #255
    //   319: iand
    //   320: iload #11
    //   322: sipush #240
    //   325: iand
    //   326: iconst_4
    //   327: ishr
    //   328: bipush #15
    //   330: iload #12
    //   332: iand
    //   333: iload #13
    //   335: bipush #63
    //   337: iand
    //   338: bipush #7
    //   340: iload #15
    //   342: iand
    //   343: iload #14
    //   345: bipush #56
    //   347: iand
    //   348: iconst_3
    //   349: ishr
    //   350: invokespecial <init> : (IZZZIZIIIIIII)V
    //   353: invokespecial <init> : (ILjava/lang/Object;)V
    //   356: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   359: iinc #2, 6
    //   362: goto -> 1261
    //   365: aload_1
    //   366: iload_2
    //   367: baload
    //   368: istore #10
    //   370: aload_1
    //   371: iload_2
    //   372: baload
    //   373: istore #14
    //   375: aload_1
    //   376: iload_2
    //   377: baload
    //   378: istore #7
    //   380: aload_1
    //   381: iload_2
    //   382: baload
    //   383: istore #15
    //   385: new android/media/Cea708CCParser$CaptionColor
    //   388: dup
    //   389: iload #10
    //   391: sipush #192
    //   394: iand
    //   395: bipush #6
    //   397: ishr
    //   398: iload #14
    //   400: bipush #48
    //   402: iand
    //   403: iconst_4
    //   404: ishr
    //   405: iload #7
    //   407: bipush #12
    //   409: iand
    //   410: iconst_2
    //   411: ishr
    //   412: iload #15
    //   414: iconst_3
    //   415: iand
    //   416: invokespecial <init> : (IIII)V
    //   419: astore #16
    //   421: aload_1
    //   422: iload_2
    //   423: iconst_1
    //   424: iadd
    //   425: baload
    //   426: istore #14
    //   428: aload_1
    //   429: iload_2
    //   430: iconst_2
    //   431: iadd
    //   432: baload
    //   433: istore #7
    //   435: aload_1
    //   436: iload_2
    //   437: iconst_1
    //   438: iadd
    //   439: baload
    //   440: istore #13
    //   442: aload_1
    //   443: iload_2
    //   444: iconst_1
    //   445: iadd
    //   446: baload
    //   447: istore #10
    //   449: aload_1
    //   450: iload_2
    //   451: iconst_1
    //   452: iadd
    //   453: baload
    //   454: istore #15
    //   456: new android/media/Cea708CCParser$CaptionColor
    //   459: dup
    //   460: iconst_0
    //   461: iload #13
    //   463: bipush #48
    //   465: iand
    //   466: iconst_4
    //   467: ishr
    //   468: iload #10
    //   470: bipush #12
    //   472: iand
    //   473: iconst_2
    //   474: ishr
    //   475: iload #15
    //   477: iconst_3
    //   478: iand
    //   479: invokespecial <init> : (IIII)V
    //   482: astore #17
    //   484: aload_1
    //   485: iload_2
    //   486: iconst_2
    //   487: iadd
    //   488: baload
    //   489: bipush #64
    //   491: iand
    //   492: ifeq -> 501
    //   495: iconst_1
    //   496: istore #4
    //   498: goto -> 504
    //   501: iconst_0
    //   502: istore #4
    //   504: aload_1
    //   505: iload_2
    //   506: iconst_2
    //   507: iadd
    //   508: baload
    //   509: istore #10
    //   511: aload_1
    //   512: iload_2
    //   513: iconst_2
    //   514: iadd
    //   515: baload
    //   516: istore_3
    //   517: aload_1
    //   518: iload_2
    //   519: iconst_2
    //   520: iadd
    //   521: baload
    //   522: istore #15
    //   524: aload_1
    //   525: iload_2
    //   526: iconst_3
    //   527: iadd
    //   528: baload
    //   529: istore #9
    //   531: aload_1
    //   532: iload_2
    //   533: iconst_3
    //   534: iadd
    //   535: baload
    //   536: istore #11
    //   538: aload_1
    //   539: iload_2
    //   540: iconst_3
    //   541: iadd
    //   542: baload
    //   543: istore #13
    //   545: aload_0
    //   546: new android/media/Cea708CCParser$CaptionEvent
    //   549: dup
    //   550: bipush #15
    //   552: new android/media/Cea708CCParser$CaptionWindowAttr
    //   555: dup
    //   556: aload #16
    //   558: aload #17
    //   560: iload #7
    //   562: sipush #128
    //   565: iand
    //   566: iconst_5
    //   567: ishr
    //   568: iload #14
    //   570: sipush #192
    //   573: iand
    //   574: bipush #6
    //   576: ishr
    //   577: ior
    //   578: iload #4
    //   580: iload #10
    //   582: bipush #48
    //   584: iand
    //   585: iconst_4
    //   586: ishr
    //   587: iload_3
    //   588: bipush #12
    //   590: iand
    //   591: iconst_2
    //   592: ishr
    //   593: iload #15
    //   595: iconst_3
    //   596: iand
    //   597: bipush #12
    //   599: iload #11
    //   601: iand
    //   602: iconst_2
    //   603: ishr
    //   604: iload #9
    //   606: sipush #240
    //   609: iand
    //   610: iconst_4
    //   611: ishr
    //   612: iconst_3
    //   613: iload #13
    //   615: iand
    //   616: invokespecial <init> : (Landroid/media/Cea708CCParser$CaptionColor;Landroid/media/Cea708CCParser$CaptionColor;IZIIIIII)V
    //   619: invokespecial <init> : (ILjava/lang/Object;)V
    //   622: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   625: iinc #2, 4
    //   628: goto -> 1261
    //   631: aload_1
    //   632: iload_2
    //   633: baload
    //   634: istore #14
    //   636: aload_1
    //   637: iload_2
    //   638: iconst_1
    //   639: iadd
    //   640: baload
    //   641: istore #7
    //   643: aload_0
    //   644: new android/media/Cea708CCParser$CaptionEvent
    //   647: dup
    //   648: bipush #14
    //   650: new android/media/Cea708CCParser$CaptionPenLocation
    //   653: dup
    //   654: iload #14
    //   656: bipush #15
    //   658: iand
    //   659: iload #7
    //   661: bipush #63
    //   663: iand
    //   664: invokespecial <init> : (II)V
    //   667: invokespecial <init> : (ILjava/lang/Object;)V
    //   670: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   673: iinc #2, 2
    //   676: goto -> 1261
    //   679: aload_1
    //   680: iload_2
    //   681: baload
    //   682: istore #14
    //   684: aload_1
    //   685: iload_2
    //   686: baload
    //   687: istore #7
    //   689: aload_1
    //   690: iload_2
    //   691: baload
    //   692: istore #15
    //   694: aload_1
    //   695: iload_2
    //   696: baload
    //   697: istore #10
    //   699: new android/media/Cea708CCParser$CaptionColor
    //   702: dup
    //   703: iload #14
    //   705: sipush #192
    //   708: iand
    //   709: bipush #6
    //   711: ishr
    //   712: iload #7
    //   714: bipush #48
    //   716: iand
    //   717: iconst_4
    //   718: ishr
    //   719: iload #15
    //   721: bipush #12
    //   723: iand
    //   724: iconst_2
    //   725: ishr
    //   726: iload #10
    //   728: iconst_3
    //   729: iand
    //   730: invokespecial <init> : (IIII)V
    //   733: astore #17
    //   735: iload_2
    //   736: iconst_1
    //   737: iadd
    //   738: istore #7
    //   740: aload_1
    //   741: iload #7
    //   743: baload
    //   744: istore #14
    //   746: aload_1
    //   747: iload #7
    //   749: baload
    //   750: istore #10
    //   752: aload_1
    //   753: iload #7
    //   755: baload
    //   756: istore_2
    //   757: aload_1
    //   758: iload #7
    //   760: baload
    //   761: istore #15
    //   763: new android/media/Cea708CCParser$CaptionColor
    //   766: dup
    //   767: iload #14
    //   769: sipush #192
    //   772: iand
    //   773: bipush #6
    //   775: ishr
    //   776: iload #10
    //   778: bipush #48
    //   780: iand
    //   781: iconst_4
    //   782: ishr
    //   783: iload_2
    //   784: bipush #12
    //   786: iand
    //   787: iconst_2
    //   788: ishr
    //   789: iload #15
    //   791: iconst_3
    //   792: iand
    //   793: invokespecial <init> : (IIII)V
    //   796: astore #16
    //   798: iload #7
    //   800: iconst_1
    //   801: iadd
    //   802: istore #14
    //   804: aload_1
    //   805: iload #14
    //   807: baload
    //   808: istore #7
    //   810: aload_1
    //   811: iload #14
    //   813: baload
    //   814: istore #15
    //   816: aload_1
    //   817: iload #14
    //   819: baload
    //   820: istore_2
    //   821: new android/media/Cea708CCParser$CaptionColor
    //   824: dup
    //   825: iconst_0
    //   826: iload #7
    //   828: bipush #48
    //   830: iand
    //   831: iconst_4
    //   832: ishr
    //   833: bipush #12
    //   835: iload #15
    //   837: iand
    //   838: iconst_2
    //   839: ishr
    //   840: iload_2
    //   841: iconst_3
    //   842: iand
    //   843: invokespecial <init> : (IIII)V
    //   846: astore_1
    //   847: aload_0
    //   848: new android/media/Cea708CCParser$CaptionEvent
    //   851: dup
    //   852: bipush #13
    //   854: new android/media/Cea708CCParser$CaptionPenColor
    //   857: dup
    //   858: aload #17
    //   860: aload #16
    //   862: aload_1
    //   863: invokespecial <init> : (Landroid/media/Cea708CCParser$CaptionColor;Landroid/media/Cea708CCParser$CaptionColor;Landroid/media/Cea708CCParser$CaptionColor;)V
    //   866: invokespecial <init> : (ILjava/lang/Object;)V
    //   869: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   872: iload #14
    //   874: iconst_1
    //   875: iadd
    //   876: istore_2
    //   877: goto -> 1261
    //   880: aload_1
    //   881: iload_2
    //   882: baload
    //   883: istore #15
    //   885: aload_1
    //   886: iload_2
    //   887: baload
    //   888: istore #7
    //   890: aload_1
    //   891: iload_2
    //   892: baload
    //   893: istore #14
    //   895: aload_1
    //   896: iload_2
    //   897: iconst_1
    //   898: iadd
    //   899: baload
    //   900: sipush #128
    //   903: iand
    //   904: ifeq -> 913
    //   907: iconst_1
    //   908: istore #4
    //   910: goto -> 916
    //   913: iconst_0
    //   914: istore #4
    //   916: aload_1
    //   917: iload_2
    //   918: iconst_1
    //   919: iadd
    //   920: baload
    //   921: bipush #64
    //   923: iand
    //   924: ifeq -> 933
    //   927: iconst_1
    //   928: istore #5
    //   930: goto -> 936
    //   933: iconst_0
    //   934: istore #5
    //   936: aload_1
    //   937: iload_2
    //   938: iconst_1
    //   939: iadd
    //   940: baload
    //   941: istore #10
    //   943: aload_1
    //   944: iload_2
    //   945: iconst_1
    //   946: iadd
    //   947: baload
    //   948: istore #13
    //   950: aload_0
    //   951: new android/media/Cea708CCParser$CaptionEvent
    //   954: dup
    //   955: bipush #12
    //   957: new android/media/Cea708CCParser$CaptionPenAttr
    //   960: dup
    //   961: iload #7
    //   963: iconst_3
    //   964: iand
    //   965: iload #14
    //   967: bipush #12
    //   969: iand
    //   970: iconst_2
    //   971: ishr
    //   972: iload #15
    //   974: sipush #240
    //   977: iand
    //   978: iconst_4
    //   979: ishr
    //   980: bipush #7
    //   982: iload #13
    //   984: iand
    //   985: iload #10
    //   987: bipush #56
    //   989: iand
    //   990: iconst_3
    //   991: ishr
    //   992: iload #5
    //   994: iload #4
    //   996: invokespecial <init> : (IIIIIZZ)V
    //   999: invokespecial <init> : (ILjava/lang/Object;)V
    //   1002: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1005: iinc #2, 2
    //   1008: goto -> 1261
    //   1011: aload_0
    //   1012: new android/media/Cea708CCParser$CaptionEvent
    //   1015: dup
    //   1016: bipush #11
    //   1018: aconst_null
    //   1019: invokespecial <init> : (ILjava/lang/Object;)V
    //   1022: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1025: goto -> 1261
    //   1028: aload_0
    //   1029: new android/media/Cea708CCParser$CaptionEvent
    //   1032: dup
    //   1033: bipush #10
    //   1035: aconst_null
    //   1036: invokespecial <init> : (ILjava/lang/Object;)V
    //   1039: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1042: goto -> 1261
    //   1045: aload_1
    //   1046: iload_2
    //   1047: baload
    //   1048: istore #14
    //   1050: iinc #2, 1
    //   1053: aload_0
    //   1054: new android/media/Cea708CCParser$CaptionEvent
    //   1057: dup
    //   1058: bipush #9
    //   1060: iload #14
    //   1062: sipush #255
    //   1065: iand
    //   1066: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1069: invokespecial <init> : (ILjava/lang/Object;)V
    //   1072: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1075: goto -> 1261
    //   1078: aload_1
    //   1079: iload_2
    //   1080: baload
    //   1081: istore #14
    //   1083: iinc #2, 1
    //   1086: aload_0
    //   1087: new android/media/Cea708CCParser$CaptionEvent
    //   1090: dup
    //   1091: bipush #8
    //   1093: iload #14
    //   1095: sipush #255
    //   1098: iand
    //   1099: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1102: invokespecial <init> : (ILjava/lang/Object;)V
    //   1105: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1108: goto -> 1261
    //   1111: aload_1
    //   1112: iload_2
    //   1113: baload
    //   1114: istore #14
    //   1116: iinc #2, 1
    //   1119: aload_0
    //   1120: new android/media/Cea708CCParser$CaptionEvent
    //   1123: dup
    //   1124: bipush #7
    //   1126: iload #14
    //   1128: sipush #255
    //   1131: iand
    //   1132: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1135: invokespecial <init> : (ILjava/lang/Object;)V
    //   1138: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1141: goto -> 1261
    //   1144: aload_1
    //   1145: iload_2
    //   1146: baload
    //   1147: istore #14
    //   1149: iinc #2, 1
    //   1152: aload_0
    //   1153: new android/media/Cea708CCParser$CaptionEvent
    //   1156: dup
    //   1157: bipush #6
    //   1159: iload #14
    //   1161: sipush #255
    //   1164: iand
    //   1165: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1168: invokespecial <init> : (ILjava/lang/Object;)V
    //   1171: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1174: goto -> 1261
    //   1177: aload_1
    //   1178: iload_2
    //   1179: baload
    //   1180: istore #14
    //   1182: iinc #2, 1
    //   1185: aload_0
    //   1186: new android/media/Cea708CCParser$CaptionEvent
    //   1189: dup
    //   1190: iconst_5
    //   1191: iload #14
    //   1193: sipush #255
    //   1196: iand
    //   1197: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1200: invokespecial <init> : (ILjava/lang/Object;)V
    //   1203: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1206: goto -> 1261
    //   1209: aload_1
    //   1210: iload_2
    //   1211: baload
    //   1212: istore #14
    //   1214: iinc #2, 1
    //   1217: aload_0
    //   1218: new android/media/Cea708CCParser$CaptionEvent
    //   1221: dup
    //   1222: iconst_4
    //   1223: iload #14
    //   1225: sipush #255
    //   1228: iand
    //   1229: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1232: invokespecial <init> : (ILjava/lang/Object;)V
    //   1235: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1238: goto -> 1261
    //   1241: aload_0
    //   1242: new android/media/Cea708CCParser$CaptionEvent
    //   1245: dup
    //   1246: iconst_3
    //   1247: iload_3
    //   1248: sipush #128
    //   1251: isub
    //   1252: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   1255: invokespecial <init> : (ILjava/lang/Object;)V
    //   1258: invokespecial emitCaptionEvent : (Landroid/media/Cea708CCParser$CaptionEvent;)V
    //   1261: iload_2
    //   1262: ireturn
    // Line number table:
    //   Java source line number -> byte code offset
    //   #371	-> 0
    //   #586	-> 151
    //   #587	-> 151
    //   #588	-> 169
    //   #589	-> 187
    //   #590	-> 205
    //   #591	-> 210
    //   #592	-> 231
    //   #593	-> 238
    //   #594	-> 245
    //   #595	-> 252
    //   #596	-> 259
    //   #597	-> 266
    //   #598	-> 273
    //   #599	-> 280
    //   #600	-> 280
    //   #615	-> 359
    //   #542	-> 365
    //   #543	-> 370
    //   #544	-> 375
    //   #545	-> 380
    //   #546	-> 385
    //   #547	-> 421
    //   #548	-> 435
    //   #549	-> 442
    //   #550	-> 449
    //   #551	-> 456
    //   #553	-> 484
    //   #554	-> 504
    //   #555	-> 511
    //   #556	-> 517
    //   #557	-> 524
    //   #558	-> 531
    //   #559	-> 538
    //   #560	-> 545
    //   #561	-> 545
    //   #574	-> 625
    //   #528	-> 631
    //   #529	-> 636
    //   #530	-> 643
    //   #531	-> 643
    //   #537	-> 673
    //   #497	-> 679
    //   #498	-> 684
    //   #499	-> 689
    //   #500	-> 694
    //   #501	-> 699
    //   #502	-> 735
    //   #503	-> 740
    //   #504	-> 746
    //   #505	-> 752
    //   #506	-> 757
    //   #507	-> 763
    //   #508	-> 798
    //   #509	-> 804
    //   #510	-> 810
    //   #511	-> 816
    //   #512	-> 821
    //   #514	-> 847
    //   #515	-> 847
    //   #522	-> 872
    //   #475	-> 880
    //   #476	-> 885
    //   #477	-> 890
    //   #478	-> 895
    //   #479	-> 916
    //   #480	-> 936
    //   #481	-> 943
    //   #482	-> 950
    //   #483	-> 950
    //   #492	-> 1005
    //   #466	-> 1011
    //   #470	-> 1025
    //   #457	-> 1028
    //   #461	-> 1042
    //   #446	-> 1045
    //   #447	-> 1050
    //   #448	-> 1053
    //   #453	-> 1075
    //   #435	-> 1078
    //   #436	-> 1083
    //   #437	-> 1086
    //   #441	-> 1108
    //   #424	-> 1111
    //   #425	-> 1116
    //   #426	-> 1119
    //   #430	-> 1141
    //   #413	-> 1144
    //   #414	-> 1149
    //   #415	-> 1152
    //   #419	-> 1174
    //   #402	-> 1177
    //   #403	-> 1182
    //   #404	-> 1185
    //   #408	-> 1206
    //   #391	-> 1209
    //   #392	-> 1214
    //   #393	-> 1217
    //   #397	-> 1238
    //   #381	-> 1241
    //   #382	-> 1241
    //   #386	-> 1261
    //   #621	-> 1261
  }
  
  private int parseG0(byte[] paramArrayOfbyte, int paramInt) {
    int i = this.mCommand;
    if (i == 127) {
      this.mBuffer.append(MUSIC_NOTE_CHAR);
    } else {
      this.mBuffer.append((char)i);
    } 
    return paramInt;
  }
  
  private int parseG1(byte[] paramArrayOfbyte, int paramInt) {
    this.mBuffer.append((char)this.mCommand);
    return paramInt;
  }
  
  private int parseExt1(byte[] paramArrayOfbyte, int paramInt) {
    int i = paramArrayOfbyte[paramInt] & 0xFF;
    int j = paramInt + 1;
    if (i >= 0 && i <= 31) {
      paramInt = parseC2(paramArrayOfbyte, j);
    } else {
      paramInt = this.mCommand;
      if (paramInt >= 128 && paramInt <= 159) {
        paramInt = parseC3(paramArrayOfbyte, j);
      } else {
        paramInt = this.mCommand;
        if (paramInt >= 32 && paramInt <= 127) {
          paramInt = parseG2(paramArrayOfbyte, j);
        } else {
          i = this.mCommand;
          paramInt = j;
          if (i >= 160) {
            paramInt = j;
            if (i <= 255)
              paramInt = parseG3(paramArrayOfbyte, j); 
          } 
        } 
      } 
    } 
    return paramInt;
  }
  
  private int parseC2(byte[] paramArrayOfbyte, int paramInt) {
    int i = this.mCommand;
    if (i >= 0 && i <= 7) {
      i = paramInt;
    } else {
      i = this.mCommand;
      if (i >= 8 && i <= 15) {
        i = paramInt + 1;
      } else {
        i = this.mCommand;
        if (i >= 16 && i <= 23) {
          i = paramInt + 2;
        } else {
          int j = this.mCommand;
          i = paramInt;
          if (j >= 24) {
            i = paramInt;
            if (j <= 31)
              i = paramInt + 3; 
          } 
        } 
      } 
    } 
    return i;
  }
  
  private int parseC3(byte[] paramArrayOfbyte, int paramInt) {
    int i = this.mCommand;
    if (i >= 128 && i <= 135) {
      i = paramInt + 4;
    } else {
      int j = this.mCommand;
      i = paramInt;
      if (j >= 136) {
        i = paramInt;
        if (j <= 143)
          i = paramInt + 5; 
      } 
    } 
    return i;
  }
  
  private int parseG2(byte[] paramArrayOfbyte, int paramInt) {
    return paramInt;
  }
  
  private int parseG3(byte[] paramArrayOfbyte, int paramInt) {
    return paramInt;
  }
  
  private static class Const {
    public static final int CODE_C0_BS = 8;
    
    public static final int CODE_C0_CR = 13;
    
    public static final int CODE_C0_ETX = 3;
    
    public static final int CODE_C0_EXT1 = 16;
    
    public static final int CODE_C0_FF = 12;
    
    public static final int CODE_C0_HCR = 14;
    
    public static final int CODE_C0_NUL = 0;
    
    public static final int CODE_C0_P16 = 24;
    
    public static final int CODE_C0_RANGE_END = 31;
    
    public static final int CODE_C0_RANGE_START = 0;
    
    public static final int CODE_C0_SKIP1_RANGE_END = 23;
    
    public static final int CODE_C0_SKIP1_RANGE_START = 16;
    
    public static final int CODE_C0_SKIP2_RANGE_END = 31;
    
    public static final int CODE_C0_SKIP2_RANGE_START = 24;
    
    public static final int CODE_C1_CLW = 136;
    
    public static final int CODE_C1_CW0 = 128;
    
    public static final int CODE_C1_CW1 = 129;
    
    public static final int CODE_C1_CW2 = 130;
    
    public static final int CODE_C1_CW3 = 131;
    
    public static final int CODE_C1_CW4 = 132;
    
    public static final int CODE_C1_CW5 = 133;
    
    public static final int CODE_C1_CW6 = 134;
    
    public static final int CODE_C1_CW7 = 135;
    
    public static final int CODE_C1_DF0 = 152;
    
    public static final int CODE_C1_DF1 = 153;
    
    public static final int CODE_C1_DF2 = 154;
    
    public static final int CODE_C1_DF3 = 155;
    
    public static final int CODE_C1_DF4 = 156;
    
    public static final int CODE_C1_DF5 = 157;
    
    public static final int CODE_C1_DF6 = 158;
    
    public static final int CODE_C1_DF7 = 159;
    
    public static final int CODE_C1_DLC = 142;
    
    public static final int CODE_C1_DLW = 140;
    
    public static final int CODE_C1_DLY = 141;
    
    public static final int CODE_C1_DSW = 137;
    
    public static final int CODE_C1_HDW = 138;
    
    public static final int CODE_C1_RANGE_END = 159;
    
    public static final int CODE_C1_RANGE_START = 128;
    
    public static final int CODE_C1_RST = 143;
    
    public static final int CODE_C1_SPA = 144;
    
    public static final int CODE_C1_SPC = 145;
    
    public static final int CODE_C1_SPL = 146;
    
    public static final int CODE_C1_SWA = 151;
    
    public static final int CODE_C1_TGW = 139;
    
    public static final int CODE_C2_RANGE_END = 31;
    
    public static final int CODE_C2_RANGE_START = 0;
    
    public static final int CODE_C2_SKIP0_RANGE_END = 7;
    
    public static final int CODE_C2_SKIP0_RANGE_START = 0;
    
    public static final int CODE_C2_SKIP1_RANGE_END = 15;
    
    public static final int CODE_C2_SKIP1_RANGE_START = 8;
    
    public static final int CODE_C2_SKIP2_RANGE_END = 23;
    
    public static final int CODE_C2_SKIP2_RANGE_START = 16;
    
    public static final int CODE_C2_SKIP3_RANGE_END = 31;
    
    public static final int CODE_C2_SKIP3_RANGE_START = 24;
    
    public static final int CODE_C3_RANGE_END = 159;
    
    public static final int CODE_C3_RANGE_START = 128;
    
    public static final int CODE_C3_SKIP4_RANGE_END = 135;
    
    public static final int CODE_C3_SKIP4_RANGE_START = 128;
    
    public static final int CODE_C3_SKIP5_RANGE_END = 143;
    
    public static final int CODE_C3_SKIP5_RANGE_START = 136;
    
    public static final int CODE_G0_MUSICNOTE = 127;
    
    public static final int CODE_G0_RANGE_END = 127;
    
    public static final int CODE_G0_RANGE_START = 32;
    
    public static final int CODE_G1_RANGE_END = 255;
    
    public static final int CODE_G1_RANGE_START = 160;
    
    public static final int CODE_G2_BLK = 48;
    
    public static final int CODE_G2_NBTSP = 33;
    
    public static final int CODE_G2_RANGE_END = 127;
    
    public static final int CODE_G2_RANGE_START = 32;
    
    public static final int CODE_G2_TSP = 32;
    
    public static final int CODE_G3_CC = 160;
    
    public static final int CODE_G3_RANGE_END = 255;
    
    public static final int CODE_G3_RANGE_START = 160;
  }
  
  public static class CaptionColor {
    private static final int[] COLOR_MAP = new int[] { 0, 15, 240, 255 };
    
    public static final int OPACITY_FLASH = 1;
    
    private static final int[] OPACITY_MAP = new int[] { 255, 254, 128, 0 };
    
    public static final int OPACITY_SOLID = 0;
    
    public static final int OPACITY_TRANSLUCENT = 2;
    
    public static final int OPACITY_TRANSPARENT = 3;
    
    public final int blue;
    
    public final int green;
    
    public final int opacity;
    
    public final int red;
    
    public CaptionColor(int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      this.opacity = param1Int1;
      this.red = param1Int2;
      this.green = param1Int3;
      this.blue = param1Int4;
    }
    
    public int getArgbValue() {
      int i = OPACITY_MAP[this.opacity], arrayOfInt[] = COLOR_MAP;
      return Color.argb(i, arrayOfInt[this.red], arrayOfInt[this.green], arrayOfInt[this.blue]);
    }
  }
  
  public static class CaptionEvent {
    public final Object obj;
    
    public final int type;
    
    public CaptionEvent(int param1Int, Object param1Object) {
      this.type = param1Int;
      this.obj = param1Object;
    }
  }
  
  public static class CaptionPenAttr {
    public static final int OFFSET_NORMAL = 1;
    
    public static final int OFFSET_SUBSCRIPT = 0;
    
    public static final int OFFSET_SUPERSCRIPT = 2;
    
    public static final int PEN_SIZE_LARGE = 2;
    
    public static final int PEN_SIZE_SMALL = 0;
    
    public static final int PEN_SIZE_STANDARD = 1;
    
    public final int edgeType;
    
    public final int fontTag;
    
    public final boolean italic;
    
    public final int penOffset;
    
    public final int penSize;
    
    public final int textTag;
    
    public final boolean underline;
    
    public CaptionPenAttr(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, boolean param1Boolean1, boolean param1Boolean2) {
      this.penSize = param1Int1;
      this.penOffset = param1Int2;
      this.textTag = param1Int3;
      this.fontTag = param1Int4;
      this.edgeType = param1Int5;
      this.underline = param1Boolean1;
      this.italic = param1Boolean2;
    }
  }
  
  public static class CaptionPenColor {
    public final Cea708CCParser.CaptionColor backgroundColor;
    
    public final Cea708CCParser.CaptionColor edgeColor;
    
    public final Cea708CCParser.CaptionColor foregroundColor;
    
    public CaptionPenColor(Cea708CCParser.CaptionColor param1CaptionColor1, Cea708CCParser.CaptionColor param1CaptionColor2, Cea708CCParser.CaptionColor param1CaptionColor3) {
      this.foregroundColor = param1CaptionColor1;
      this.backgroundColor = param1CaptionColor2;
      this.edgeColor = param1CaptionColor3;
    }
  }
  
  public static class CaptionPenLocation {
    public final int column;
    
    public final int row;
    
    public CaptionPenLocation(int param1Int1, int param1Int2) {
      this.row = param1Int1;
      this.column = param1Int2;
    }
  }
  
  public static class CaptionWindowAttr {
    public final Cea708CCParser.CaptionColor borderColor;
    
    public final int borderType;
    
    public final int displayEffect;
    
    public final int effectDirection;
    
    public final int effectSpeed;
    
    public final Cea708CCParser.CaptionColor fillColor;
    
    public final int justify;
    
    public final int printDirection;
    
    public final int scrollDirection;
    
    public final boolean wordWrap;
    
    public CaptionWindowAttr(Cea708CCParser.CaptionColor param1CaptionColor1, Cea708CCParser.CaptionColor param1CaptionColor2, int param1Int1, boolean param1Boolean, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7) {
      this.fillColor = param1CaptionColor1;
      this.borderColor = param1CaptionColor2;
      this.borderType = param1Int1;
      this.wordWrap = param1Boolean;
      this.printDirection = param1Int2;
      this.scrollDirection = param1Int3;
      this.justify = param1Int4;
      this.effectDirection = param1Int5;
      this.effectSpeed = param1Int6;
      this.displayEffect = param1Int7;
    }
  }
  
  public static class CaptionWindow {
    public final int anchorHorizontal;
    
    public final int anchorId;
    
    public final int anchorVertical;
    
    public final int columnCount;
    
    public final boolean columnLock;
    
    public final int id;
    
    public final int penStyle;
    
    public final int priority;
    
    public final boolean relativePositioning;
    
    public final int rowCount;
    
    public final boolean rowLock;
    
    public final boolean visible;
    
    public final int windowStyle;
    
    public CaptionWindow(int param1Int1, boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3, int param1Int2, boolean param1Boolean4, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8, int param1Int9) {
      this.id = param1Int1;
      this.visible = param1Boolean1;
      this.rowLock = param1Boolean2;
      this.columnLock = param1Boolean3;
      this.priority = param1Int2;
      this.relativePositioning = param1Boolean4;
      this.anchorVertical = param1Int3;
      this.anchorHorizontal = param1Int4;
      this.anchorId = param1Int5;
      this.rowCount = param1Int6;
      this.columnCount = param1Int7;
      this.penStyle = param1Int8;
      this.windowStyle = param1Int9;
    }
  }
  
  static interface DisplayListener {
    void emitEvent(Cea708CCParser.CaptionEvent param1CaptionEvent);
  }
}
