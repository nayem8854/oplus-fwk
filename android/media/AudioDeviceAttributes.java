package android.media;

import android.annotation.SystemApi;
import android.os.Parcel;
import android.os.Parcelable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Objects;

@SystemApi
public final class AudioDeviceAttributes implements Parcelable {
  @SystemApi
  public AudioDeviceAttributes(AudioDeviceInfo paramAudioDeviceInfo) {
    boolean bool;
    Objects.requireNonNull(paramAudioDeviceInfo);
    if (paramAudioDeviceInfo.isSink()) {
      bool = true;
    } else {
      bool = true;
    } 
    this.mRole = bool;
    this.mType = paramAudioDeviceInfo.getType();
    this.mAddress = paramAudioDeviceInfo.getAddress();
  }
  
  @SystemApi
  public AudioDeviceAttributes(int paramInt1, int paramInt2, String paramString) {
    Objects.requireNonNull(paramString);
    if (paramInt1 == 2 || paramInt1 == 1) {
      if (paramInt1 == 2)
        AudioDeviceInfo.enforceValidAudioDeviceTypeOut(paramInt2); 
      if (paramInt1 == 1)
        AudioDeviceInfo.enforceValidAudioDeviceTypeIn(paramInt2); 
      this.mRole = paramInt1;
      this.mType = paramInt2;
      this.mAddress = paramString;
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Invalid role ");
    stringBuilder.append(paramInt1);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  AudioDeviceAttributes(int paramInt, String paramString) {
    byte b;
    if ((Integer.MIN_VALUE & paramInt) != 0) {
      b = 1;
    } else {
      b = 2;
    } 
    this.mRole = b;
    this.mType = AudioDeviceInfo.convertInternalDeviceToDeviceType(paramInt);
    this.mAddress = paramString;
  }
  
  @SystemApi
  public int getRole() {
    return this.mRole;
  }
  
  @SystemApi
  public int getType() {
    return this.mType;
  }
  
  @SystemApi
  public String getAddress() {
    return this.mAddress;
  }
  
  public int hashCode() {
    return Objects.hash(new Object[] { Integer.valueOf(this.mRole), Integer.valueOf(this.mType), this.mAddress });
  }
  
  public boolean equals(Object paramObject) {
    null = true;
    if (this == paramObject)
      return true; 
    if (paramObject == null || getClass() != paramObject.getClass())
      return false; 
    AudioDeviceAttributes audioDeviceAttributes = (AudioDeviceAttributes)paramObject;
    if (this.mRole == audioDeviceAttributes.mRole && this.mType == audioDeviceAttributes.mType) {
      paramObject = this.mAddress;
      String str = audioDeviceAttributes.mAddress;
      if (paramObject.equals(str))
        return null; 
    } 
    return false;
  }
  
  public static String roleToString(int paramInt) {
    String str;
    if (paramInt == 2) {
      str = "output";
    } else {
      str = "input";
    } 
    return str;
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("AudioDeviceAttributes: role:");
    int i = this.mRole;
    stringBuilder.append(roleToString(i));
    stringBuilder.append(" type:");
    if (this.mRole == 2) {
      i = this.mType;
      i = AudioDeviceInfo.convertDeviceTypeToInternalDevice(i);
      null = AudioSystem.getOutputDeviceName(i);
    } else {
      i = this.mType;
      i = AudioDeviceInfo.convertDeviceTypeToInternalDevice(i);
      null = AudioSystem.getInputDeviceName(i);
    } 
    stringBuilder.append(null);
    stringBuilder.append(" addr:");
    stringBuilder.append(this.mAddress);
    return new String(stringBuilder.toString());
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeInt(this.mRole);
    paramParcel.writeInt(this.mType);
    paramParcel.writeString(this.mAddress);
  }
  
  private AudioDeviceAttributes(Parcel paramParcel) {
    this.mRole = paramParcel.readInt();
    this.mType = paramParcel.readInt();
    this.mAddress = paramParcel.readString();
  }
  
  public static final Parcelable.Creator<AudioDeviceAttributes> CREATOR = new Parcelable.Creator<AudioDeviceAttributes>() {
      public AudioDeviceAttributes createFromParcel(Parcel param1Parcel) {
        return new AudioDeviceAttributes(param1Parcel);
      }
      
      public AudioDeviceAttributes[] newArray(int param1Int) {
        return new AudioDeviceAttributes[param1Int];
      }
    };
  
  public static final int ROLE_INPUT = 1;
  
  public static final int ROLE_OUTPUT = 2;
  
  private final String mAddress;
  
  private final int mRole;
  
  private final int mType;
  
  @Retention(RetentionPolicy.SOURCE)
  class Role implements Annotation {}
}
