package android.media;

import android.os.SystemProperties;
import android.util.Log;
import android.util.Pair;
import android.util.Range;
import android.util.Rational;
import android.util.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

public final class MediaCodecInfo {
  private static final Range<Integer> BITRATE_RANGE;
  
  private static final int DEFAULT_MAX_SUPPORTED_INSTANCES = 32;
  
  private static final int ERROR_NONE_SUPPORTED = 4;
  
  private static final int ERROR_UNRECOGNIZED = 1;
  
  private static final int ERROR_UNSUPPORTED = 2;
  
  private static final int FLAG_IS_ENCODER = 1;
  
  private static final int FLAG_IS_HARDWARE_ACCELERATED = 8;
  
  private static final int FLAG_IS_SOFTWARE_ONLY = 4;
  
  private static final int FLAG_IS_VENDOR = 2;
  
  private static final Range<Integer> FRAME_RATE_RANGE;
  
  private static final int MAX_SUPPORTED_INSTANCES_LIMIT = 256;
  
  private static final Range<Integer> POSITIVE_INTEGERS;
  
  MediaCodecInfo(String paramString1, String paramString2, int paramInt, CodecCapabilities[] paramArrayOfCodecCapabilities) {
    this.mName = paramString1;
    this.mCanonicalName = paramString2;
    this.mFlags = paramInt;
    this.mCaps = new HashMap<>();
    for (int i = paramArrayOfCodecCapabilities.length; paramInt < i; ) {
      CodecCapabilities codecCapabilities = paramArrayOfCodecCapabilities[paramInt];
      this.mCaps.put(codecCapabilities.getMimeType(), codecCapabilities);
      paramInt++;
    } 
  }
  
  public final String getName() {
    return this.mName;
  }
  
  public final String getCanonicalName() {
    return this.mCanonicalName;
  }
  
  public final boolean isAlias() {
    return this.mName.equals(this.mCanonicalName) ^ true;
  }
  
  public final boolean isEncoder() {
    int i = this.mFlags;
    boolean bool = true;
    if ((i & 0x1) == 0)
      bool = false; 
    return bool;
  }
  
  public final boolean isVendor() {
    boolean bool;
    if ((this.mFlags & 0x2) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean isSoftwareOnly() {
    boolean bool;
    if ((this.mFlags & 0x4) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final boolean isHardwareAccelerated() {
    boolean bool;
    if ((this.mFlags & 0x8) != 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public final String[] getSupportedTypes() {
    Set<String> set = this.mCaps.keySet();
    String[] arrayOfString = set.<String>toArray(new String[set.size()]);
    Arrays.sort((Object[])arrayOfString);
    return arrayOfString;
  }
  
  private static int checkPowerOfTwo(int paramInt, String paramString) {
    if ((paramInt - 1 & paramInt) == 0)
      return paramInt; 
    throw new IllegalArgumentException(paramString);
  }
  
  private static class Feature {
    public boolean mDefault;
    
    public String mName;
    
    public int mValue;
    
    public Feature(String param1String, int param1Int, boolean param1Boolean) {
      this.mName = param1String;
      this.mValue = param1Int;
      this.mDefault = param1Boolean;
    }
  }
  
  static {
    Integer integer1 = Integer.valueOf(1);
    POSITIVE_INTEGERS = Range.create(integer1, Integer.valueOf(2147483647));
  }
  
  private static final Range<Long> POSITIVE_LONGS = Range.create(Long.valueOf(1L), Long.valueOf(Long.MAX_VALUE));
  
  private static final Range<Rational> POSITIVE_RATIONALS;
  
  private static final Range<Integer> SIZE_RANGE;
  
  private static final String TAG = "MediaCodecInfo";
  
  private String mCanonicalName;
  
  private Map<String, CodecCapabilities> mCaps;
  
  private int mFlags;
  
  private String mName;
  
  static {
    Rational rational1 = new Rational(1, 2147483647), rational2 = new Rational(2147483647, 1);
    POSITIVE_RATIONALS = Range.create((Comparable)rational1, (Comparable)rational2);
    SIZE_RANGE = Range.create(integer1, Integer.valueOf(32768));
    Integer integer2 = Integer.valueOf(0);
    FRAME_RATE_RANGE = Range.create(integer2, Integer.valueOf(960));
    BITRATE_RANGE = Range.create(integer2, Integer.valueOf(500000000));
  }
  
  public static final class CodecCapabilities {
    public static final int COLOR_Format12bitRGB444 = 3;
    
    public static final int COLOR_Format16bitARGB1555 = 5;
    
    public static final int COLOR_Format16bitARGB4444 = 4;
    
    public static final int COLOR_Format16bitBGR565 = 7;
    
    public static final int COLOR_Format16bitRGB565 = 6;
    
    public static final int COLOR_Format18BitBGR666 = 41;
    
    public static final int COLOR_Format18bitARGB1665 = 9;
    
    public static final int COLOR_Format18bitRGB666 = 8;
    
    public static final int COLOR_Format19bitARGB1666 = 10;
    
    public static final int COLOR_Format24BitABGR6666 = 43;
    
    public static final int COLOR_Format24BitARGB6666 = 42;
    
    public static final int COLOR_Format24bitARGB1887 = 13;
    
    public static final int COLOR_Format24bitBGR888 = 12;
    
    public static final int COLOR_Format24bitRGB888 = 11;
    
    public static final int COLOR_Format25bitARGB1888 = 14;
    
    public static final int COLOR_Format32bitABGR8888 = 2130747392;
    
    public static final int COLOR_Format32bitARGB8888 = 16;
    
    public static final int COLOR_Format32bitBGRA8888 = 15;
    
    public static final int COLOR_Format8bitRGB332 = 2;
    
    public static final int COLOR_FormatCbYCrY = 27;
    
    public static final int COLOR_FormatCrYCbY = 28;
    
    public static final int COLOR_FormatL16 = 36;
    
    public static final int COLOR_FormatL2 = 33;
    
    public static final int COLOR_FormatL24 = 37;
    
    public static final int COLOR_FormatL32 = 38;
    
    public static final int COLOR_FormatL4 = 34;
    
    public static final int COLOR_FormatL8 = 35;
    
    public static final int COLOR_FormatMonochrome = 1;
    
    public static final int COLOR_FormatRGBAFlexible = 2134288520;
    
    public static final int COLOR_FormatRGBFlexible = 2134292616;
    
    public static final int COLOR_FormatRawBayer10bit = 31;
    
    public static final int COLOR_FormatRawBayer8bit = 30;
    
    public static final int COLOR_FormatRawBayer8bitcompressed = 32;
    
    public static final int COLOR_FormatSurface = 2130708361;
    
    public static final int COLOR_FormatYCbYCr = 25;
    
    public static final int COLOR_FormatYCrYCb = 26;
    
    public static final int COLOR_FormatYUV411PackedPlanar = 18;
    
    public static final int COLOR_FormatYUV411Planar = 17;
    
    public static final int COLOR_FormatYUV420Flexible = 2135033992;
    
    public static final int COLOR_FormatYUV420PackedPlanar = 20;
    
    public static final int COLOR_FormatYUV420PackedSemiPlanar = 39;
    
    public static final int COLOR_FormatYUV420Planar = 19;
    
    public static final int COLOR_FormatYUV420SemiPlanar = 21;
    
    public static final int COLOR_FormatYUV422Flexible = 2135042184;
    
    public static final int COLOR_FormatYUV422PackedPlanar = 23;
    
    public static final int COLOR_FormatYUV422PackedSemiPlanar = 40;
    
    public static final int COLOR_FormatYUV422Planar = 22;
    
    public static final int COLOR_FormatYUV422SemiPlanar = 24;
    
    public static final int COLOR_FormatYUV444Flexible = 2135181448;
    
    public static final int COLOR_FormatYUV444Interleaved = 29;
    
    public static final int COLOR_QCOM_FormatYUV420SemiPlanar = 2141391872;
    
    public static final int COLOR_TI_FormatYUV420PackedSemiPlanar = 2130706688;
    
    public static final String FEATURE_AdaptivePlayback = "adaptive-playback";
    
    public static final String FEATURE_DynamicTimestamp = "dynamic-timestamp";
    
    public static final String FEATURE_FrameParsing = "frame-parsing";
    
    public static final String FEATURE_IntraRefresh = "intra-refresh";
    
    public static final String FEATURE_LowLatency = "low-latency";
    
    public static final String FEATURE_MultipleFrames = "multiple-frames";
    
    public static final String FEATURE_PartialFrame = "partial-frame";
    
    public static final String FEATURE_SecurePlayback = "secure-playback";
    
    public static final String FEATURE_TunneledPlayback = "tunneled-playback";
    
    private static final String TAG = "CodecCapabilities";
    
    public CodecCapabilities() {}
    
    public final boolean isFeatureSupported(String param1String) {
      return checkFeature(param1String, this.mFlagsSupported);
    }
    
    public final boolean isFeatureRequired(String param1String) {
      return checkFeature(param1String, this.mFlagsRequired);
    }
    
    private static final MediaCodecInfo.Feature[] decoderFeatures = new MediaCodecInfo.Feature[] { new MediaCodecInfo.Feature("adaptive-playback", 1, true), new MediaCodecInfo.Feature("secure-playback", 2, false), new MediaCodecInfo.Feature("tunneled-playback", 4, false), new MediaCodecInfo.Feature("partial-frame", 8, false), new MediaCodecInfo.Feature("frame-parsing", 16, false), new MediaCodecInfo.Feature("multiple-frames", 32, false), new MediaCodecInfo.Feature("dynamic-timestamp", 64, false), new MediaCodecInfo.Feature("low-latency", 128, true) };
    
    private static final MediaCodecInfo.Feature[] encoderFeatures = new MediaCodecInfo.Feature[] { new MediaCodecInfo.Feature("intra-refresh", 1, false), new MediaCodecInfo.Feature("multiple-frames", 2, false), new MediaCodecInfo.Feature("dynamic-timestamp", 4, false) };
    
    public int[] colorFormats;
    
    private MediaCodecInfo.AudioCapabilities mAudioCaps;
    
    private MediaFormat mCapabilitiesInfo;
    
    private MediaFormat mDefaultFormat;
    
    private MediaCodecInfo.EncoderCapabilities mEncoderCaps;
    
    int mError;
    
    private int mFlagsRequired;
    
    private int mFlagsSupported;
    
    private int mFlagsVerified;
    
    private int mMaxSupportedInstances;
    
    private String mMime;
    
    private MediaCodecInfo.VideoCapabilities mVideoCaps;
    
    public MediaCodecInfo.CodecProfileLevel[] profileLevels;
    
    public String[] validFeatures() {
      MediaCodecInfo.Feature[] arrayOfFeature = getValidFeatures();
      String[] arrayOfString = new String[arrayOfFeature.length];
      for (byte b = 0; b < arrayOfString.length; b++)
        arrayOfString[b] = (arrayOfFeature[b]).mName; 
      return arrayOfString;
    }
    
    private MediaCodecInfo.Feature[] getValidFeatures() {
      if (!isEncoder())
        return decoderFeatures; 
      return encoderFeatures;
    }
    
    private boolean checkFeature(String param1String, int param1Int) {
      MediaCodecInfo.Feature[] arrayOfFeature;
      int i;
      boolean bool;
      byte b;
      for (arrayOfFeature = getValidFeatures(), i = arrayOfFeature.length, bool = false, b = 0; b < i; ) {
        MediaCodecInfo.Feature feature = arrayOfFeature[b];
        if (feature.mName.equals(param1String)) {
          if ((feature.mValue & param1Int) != 0)
            bool = true; 
          return bool;
        } 
        b++;
      } 
      return false;
    }
    
    public boolean isRegular() {
      for (MediaCodecInfo.Feature feature : getValidFeatures()) {
        if (!feature.mDefault && isFeatureRequired(feature.mName))
          return false; 
      } 
      return true;
    }
    
    public final boolean isFormatSupported(MediaFormat param1MediaFormat) {
      // Byte code:
      //   0: aload_1
      //   1: invokevirtual getMap : ()Ljava/util/Map;
      //   4: astore_2
      //   5: aload_2
      //   6: ldc 'mime'
      //   8: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   13: checkcast java/lang/String
      //   16: astore_3
      //   17: aload_3
      //   18: ifnull -> 34
      //   21: aload_0
      //   22: getfield mMime : Ljava/lang/String;
      //   25: aload_3
      //   26: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   29: ifne -> 34
      //   32: iconst_0
      //   33: ireturn
      //   34: aload_0
      //   35: invokespecial getValidFeatures : ()[Landroid/media/MediaCodecInfo$Feature;
      //   38: astore_3
      //   39: aload_3
      //   40: arraylength
      //   41: istore #4
      //   43: iconst_0
      //   44: istore #5
      //   46: iload #5
      //   48: iload #4
      //   50: if_icmpge -> 161
      //   53: aload_3
      //   54: iload #5
      //   56: aaload
      //   57: astore #6
      //   59: new java/lang/StringBuilder
      //   62: dup
      //   63: invokespecial <init> : ()V
      //   66: astore #7
      //   68: aload #7
      //   70: ldc_w 'feature-'
      //   73: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   76: pop
      //   77: aload #7
      //   79: aload #6
      //   81: getfield mName : Ljava/lang/String;
      //   84: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   87: pop
      //   88: aload_2
      //   89: aload #7
      //   91: invokevirtual toString : ()Ljava/lang/String;
      //   94: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   99: checkcast java/lang/Integer
      //   102: astore #7
      //   104: aload #7
      //   106: ifnonnull -> 112
      //   109: goto -> 155
      //   112: aload #7
      //   114: invokevirtual intValue : ()I
      //   117: iconst_1
      //   118: if_icmpne -> 133
      //   121: aload_0
      //   122: aload #6
      //   124: getfield mName : Ljava/lang/String;
      //   127: invokevirtual isFeatureSupported : (Ljava/lang/String;)Z
      //   130: ifeq -> 153
      //   133: aload #7
      //   135: invokevirtual intValue : ()I
      //   138: ifne -> 155
      //   141: aload_0
      //   142: aload #6
      //   144: getfield mName : Ljava/lang/String;
      //   147: invokevirtual isFeatureRequired : (Ljava/lang/String;)Z
      //   150: ifeq -> 155
      //   153: iconst_0
      //   154: ireturn
      //   155: iinc #5, 1
      //   158: goto -> 46
      //   161: aload_2
      //   162: ldc_w 'profile'
      //   165: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   170: checkcast java/lang/Integer
      //   173: astore_3
      //   174: aload_2
      //   175: ldc_w 'level'
      //   178: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   183: checkcast java/lang/Integer
      //   186: astore #6
      //   188: aload_3
      //   189: ifnull -> 374
      //   192: aload_0
      //   193: aload_3
      //   194: invokevirtual intValue : ()I
      //   197: aload #6
      //   199: invokespecial supportsProfileLevel : (ILjava/lang/Integer;)Z
      //   202: ifne -> 207
      //   205: iconst_0
      //   206: ireturn
      //   207: iconst_0
      //   208: istore #4
      //   210: aload_0
      //   211: getfield profileLevels : [Landroid/media/MediaCodecInfo$CodecProfileLevel;
      //   214: astore #6
      //   216: aload #6
      //   218: arraylength
      //   219: istore #8
      //   221: iconst_0
      //   222: istore #5
      //   224: iload #5
      //   226: iload #8
      //   228: if_icmpge -> 318
      //   231: aload #6
      //   233: iload #5
      //   235: aaload
      //   236: astore #7
      //   238: iload #4
      //   240: istore #9
      //   242: aload #7
      //   244: getfield profile : I
      //   247: aload_3
      //   248: invokevirtual intValue : ()I
      //   251: if_icmpne -> 308
      //   254: iload #4
      //   256: istore #9
      //   258: aload #7
      //   260: getfield level : I
      //   263: iload #4
      //   265: if_icmple -> 308
      //   268: aload_0
      //   269: getfield mMime : Ljava/lang/String;
      //   272: ldc_w 'video/3gpp'
      //   275: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   278: ifeq -> 301
      //   281: aload #7
      //   283: getfield level : I
      //   286: bipush #16
      //   288: if_icmpne -> 301
      //   291: iload #4
      //   293: istore #9
      //   295: iload #4
      //   297: iconst_1
      //   298: if_icmpne -> 308
      //   301: aload #7
      //   303: getfield level : I
      //   306: istore #9
      //   308: iinc #5, 1
      //   311: iload #9
      //   313: istore #4
      //   315: goto -> 224
      //   318: aload_0
      //   319: getfield mMime : Ljava/lang/String;
      //   322: aload_3
      //   323: invokevirtual intValue : ()I
      //   326: iload #4
      //   328: invokestatic createFromProfileLevel : (Ljava/lang/String;II)Landroid/media/MediaCodecInfo$CodecCapabilities;
      //   331: astore_3
      //   332: new java/util/HashMap
      //   335: dup
      //   336: aload_2
      //   337: invokespecial <init> : (Ljava/util/Map;)V
      //   340: astore_2
      //   341: aload_2
      //   342: ldc_w 'profile'
      //   345: invokeinterface remove : (Ljava/lang/Object;)Ljava/lang/Object;
      //   350: pop
      //   351: new android/media/MediaFormat
      //   354: dup
      //   355: aload_2
      //   356: invokespecial <init> : (Ljava/util/Map;)V
      //   359: astore_2
      //   360: aload_3
      //   361: ifnull -> 374
      //   364: aload_3
      //   365: aload_2
      //   366: invokevirtual isFormatSupported : (Landroid/media/MediaFormat;)Z
      //   369: ifne -> 374
      //   372: iconst_0
      //   373: ireturn
      //   374: aload_0
      //   375: getfield mAudioCaps : Landroid/media/MediaCodecInfo$AudioCapabilities;
      //   378: astore_2
      //   379: aload_2
      //   380: ifnull -> 393
      //   383: aload_2
      //   384: aload_1
      //   385: invokevirtual supportsFormat : (Landroid/media/MediaFormat;)Z
      //   388: ifne -> 393
      //   391: iconst_0
      //   392: ireturn
      //   393: aload_0
      //   394: getfield mVideoCaps : Landroid/media/MediaCodecInfo$VideoCapabilities;
      //   397: astore_2
      //   398: aload_2
      //   399: ifnull -> 412
      //   402: aload_2
      //   403: aload_1
      //   404: invokevirtual supportsFormat : (Landroid/media/MediaFormat;)Z
      //   407: ifne -> 412
      //   410: iconst_0
      //   411: ireturn
      //   412: aload_0
      //   413: getfield mEncoderCaps : Landroid/media/MediaCodecInfo$EncoderCapabilities;
      //   416: astore_2
      //   417: aload_2
      //   418: ifnull -> 431
      //   421: aload_2
      //   422: aload_1
      //   423: invokevirtual supportsFormat : (Landroid/media/MediaFormat;)Z
      //   426: ifne -> 431
      //   429: iconst_0
      //   430: ireturn
      //   431: iconst_1
      //   432: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #751	-> 0
      //   #752	-> 5
      //   #755	-> 17
      //   #756	-> 32
      //   #760	-> 34
      //   #761	-> 59
      //   #762	-> 104
      //   #763	-> 109
      //   #765	-> 112
      //   #766	-> 133
      //   #767	-> 153
      //   #760	-> 155
      //   #771	-> 161
      //   #772	-> 174
      //   #774	-> 188
      //   #775	-> 192
      //   #776	-> 205
      //   #786	-> 207
      //   #787	-> 207
      //   #788	-> 210
      //   #789	-> 238
      //   #792	-> 268
      //   #795	-> 301
      //   #788	-> 308
      //   #799	-> 318
      //   #802	-> 332
      //   #803	-> 341
      //   #804	-> 351
      //   #805	-> 360
      //   #806	-> 372
      //   #809	-> 374
      //   #810	-> 391
      //   #812	-> 393
      //   #813	-> 410
      //   #815	-> 412
      //   #816	-> 429
      //   #818	-> 431
    }
    
    private static boolean supportsBitrate(Range<Integer> param1Range, MediaFormat param1MediaFormat) {
      Integer integer1;
      Map<String, Object> map = param1MediaFormat.getMap();
      Integer integer2 = (Integer)map.get("max-bitrate");
      Integer integer3 = (Integer)map.get("bitrate");
      if (integer3 == null) {
        integer1 = integer2;
      } else {
        integer1 = integer3;
        if (integer2 != null)
          integer1 = Integer.valueOf(Math.max(integer3.intValue(), integer2.intValue())); 
      } 
      if (integer1 != null && integer1.intValue() > 0)
        return param1Range.contains(integer1); 
      return true;
    }
    
    private boolean supportsProfileLevel(int param1Int, Integer param1Integer) {
      MediaCodecInfo.CodecProfileLevel[] arrayOfCodecProfileLevel;
      int i;
      boolean bool;
      byte b;
      for (arrayOfCodecProfileLevel = this.profileLevels, i = arrayOfCodecProfileLevel.length, bool = false, b = 0; b < i; ) {
        MediaCodecInfo.CodecProfileLevel codecProfileLevel = arrayOfCodecProfileLevel[b];
        if (codecProfileLevel.profile != param1Int)
          continue; 
        if (param1Integer == null || this.mMime.equalsIgnoreCase("audio/mp4a-latm"))
          return true; 
        if (this.mMime.equalsIgnoreCase("video/3gpp") && 
          codecProfileLevel.level != param1Integer.intValue() && codecProfileLevel.level == 16 && 
          param1Integer.intValue() > 1)
          continue; 
        if (this.mMime.equalsIgnoreCase("video/mp4v-es") && 
          codecProfileLevel.level != param1Integer.intValue() && codecProfileLevel.level == 4 && 
          param1Integer.intValue() > 1)
          continue; 
        if (this.mMime.equalsIgnoreCase("video/hevc")) {
          boolean bool1;
          boolean bool2;
          if ((codecProfileLevel.level & 0x2AAAAAA) != 0) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          if ((0x2AAAAAA & param1Integer.intValue()) != 0) {
            bool2 = true;
          } else {
            bool2 = false;
          } 
          if (bool2 && !bool1)
            continue; 
        } 
        if (codecProfileLevel.level >= param1Integer.intValue()) {
          if (createFromProfileLevel(this.mMime, param1Int, codecProfileLevel.level) != null) {
            if (createFromProfileLevel(this.mMime, param1Int, param1Integer.intValue()) != null)
              bool = true; 
            return bool;
          } 
          return true;
        } 
        continue;
        b++;
      } 
      return false;
    }
    
    public MediaFormat getDefaultFormat() {
      return this.mDefaultFormat;
    }
    
    public String getMimeType() {
      return this.mMime;
    }
    
    public int getMaxSupportedInstances() {
      return this.mMaxSupportedInstances;
    }
    
    private boolean isAudio() {
      boolean bool;
      if (this.mAudioCaps != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaCodecInfo.AudioCapabilities getAudioCapabilities() {
      return this.mAudioCaps;
    }
    
    private boolean isEncoder() {
      boolean bool;
      if (this.mEncoderCaps != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaCodecInfo.EncoderCapabilities getEncoderCapabilities() {
      return this.mEncoderCaps;
    }
    
    private boolean isVideo() {
      boolean bool;
      if (this.mVideoCaps != null) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public MediaCodecInfo.VideoCapabilities getVideoCapabilities() {
      return this.mVideoCaps;
    }
    
    public CodecCapabilities dup() {
      CodecCapabilities codecCapabilities = new CodecCapabilities();
      MediaCodecInfo.CodecProfileLevel[] arrayOfCodecProfileLevel = this.profileLevels;
      codecCapabilities.profileLevels = Arrays.<MediaCodecInfo.CodecProfileLevel>copyOf(arrayOfCodecProfileLevel, arrayOfCodecProfileLevel.length);
      int[] arrayOfInt = this.colorFormats;
      codecCapabilities.colorFormats = Arrays.copyOf(arrayOfInt, arrayOfInt.length);
      codecCapabilities.mMime = this.mMime;
      codecCapabilities.mMaxSupportedInstances = this.mMaxSupportedInstances;
      codecCapabilities.mFlagsRequired = this.mFlagsRequired;
      codecCapabilities.mFlagsSupported = this.mFlagsSupported;
      codecCapabilities.mFlagsVerified = this.mFlagsVerified;
      codecCapabilities.mAudioCaps = this.mAudioCaps;
      codecCapabilities.mVideoCaps = this.mVideoCaps;
      codecCapabilities.mEncoderCaps = this.mEncoderCaps;
      codecCapabilities.mDefaultFormat = this.mDefaultFormat;
      codecCapabilities.mCapabilitiesInfo = this.mCapabilitiesInfo;
      return codecCapabilities;
    }
    
    public static CodecCapabilities createFromProfileLevel(String param1String, int param1Int1, int param1Int2) {
      MediaCodecInfo.CodecProfileLevel codecProfileLevel = new MediaCodecInfo.CodecProfileLevel();
      codecProfileLevel.profile = param1Int1;
      codecProfileLevel.level = param1Int2;
      MediaFormat mediaFormat2 = new MediaFormat();
      mediaFormat2.setString("mime", param1String);
      MediaFormat mediaFormat1 = new MediaFormat();
      CodecCapabilities codecCapabilities = new CodecCapabilities(new MediaCodecInfo.CodecProfileLevel[] { codecProfileLevel }, new int[0], true, mediaFormat2, mediaFormat1);
      if (codecCapabilities.mError != 0)
        return null; 
      return codecCapabilities;
    }
    
    CodecCapabilities(MediaCodecInfo.CodecProfileLevel[] param1ArrayOfCodecProfileLevel, int[] param1ArrayOfint, boolean param1Boolean, Map<String, Object> param1Map1, Map<String, Object> param1Map2) {
      this(param1ArrayOfCodecProfileLevel, param1ArrayOfint, param1Boolean, new MediaFormat(param1Map1), new MediaFormat(param1Map2));
    }
    
    CodecCapabilities(MediaCodecInfo.CodecProfileLevel[] param1ArrayOfCodecProfileLevel, int[] param1ArrayOfint, boolean param1Boolean, MediaFormat param1MediaFormat1, MediaFormat param1MediaFormat2) {
      // Byte code:
      //   0: aload_0
      //   1: invokespecial <init> : ()V
      //   4: aload #5
      //   6: invokevirtual getMap : ()Ljava/util/Map;
      //   9: astore #6
      //   11: aload_0
      //   12: aload_2
      //   13: putfield colorFormats : [I
      //   16: iconst_0
      //   17: istore #7
      //   19: aload_0
      //   20: iconst_0
      //   21: putfield mFlagsVerified : I
      //   24: aload_0
      //   25: aload #4
      //   27: putfield mDefaultFormat : Landroid/media/MediaFormat;
      //   30: aload_0
      //   31: aload #5
      //   33: putfield mCapabilitiesInfo : Landroid/media/MediaFormat;
      //   36: aload #4
      //   38: ldc 'mime'
      //   40: invokevirtual getString : (Ljava/lang/String;)Ljava/lang/String;
      //   43: astore #4
      //   45: aload_0
      //   46: aload #4
      //   48: putfield mMime : Ljava/lang/String;
      //   51: aload_1
      //   52: arraylength
      //   53: istore #8
      //   55: iconst_1
      //   56: istore #9
      //   58: aload_1
      //   59: astore_2
      //   60: iload #8
      //   62: ifne -> 108
      //   65: aload_1
      //   66: astore_2
      //   67: aload #4
      //   69: ldc 'video/x-vnd.on2.vp9'
      //   71: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   74: ifeq -> 108
      //   77: new android/media/MediaCodecInfo$CodecProfileLevel
      //   80: dup
      //   81: invokespecial <init> : ()V
      //   84: astore_1
      //   85: aload_1
      //   86: iconst_1
      //   87: putfield profile : I
      //   90: aload_1
      //   91: aload #5
      //   93: invokestatic equivalentVP9Level : (Landroid/media/MediaFormat;)I
      //   96: putfield level : I
      //   99: iconst_1
      //   100: anewarray android/media/MediaCodecInfo$CodecProfileLevel
      //   103: dup
      //   104: iconst_0
      //   105: aload_1
      //   106: aastore
      //   107: astore_2
      //   108: aload_0
      //   109: aload_2
      //   110: putfield profileLevels : [Landroid/media/MediaCodecInfo$CodecProfileLevel;
      //   113: aload_0
      //   114: getfield mMime : Ljava/lang/String;
      //   117: invokevirtual toLowerCase : ()Ljava/lang/String;
      //   120: ldc 'audio/'
      //   122: invokevirtual startsWith : (Ljava/lang/String;)Z
      //   125: ifeq -> 151
      //   128: aload #5
      //   130: aload_0
      //   131: invokestatic create : (Landroid/media/MediaFormat;Landroid/media/MediaCodecInfo$CodecCapabilities;)Landroid/media/MediaCodecInfo$AudioCapabilities;
      //   134: astore_1
      //   135: aload_0
      //   136: aload_1
      //   137: putfield mAudioCaps : Landroid/media/MediaCodecInfo$AudioCapabilities;
      //   140: aload_1
      //   141: aload_0
      //   142: getfield mDefaultFormat : Landroid/media/MediaFormat;
      //   145: invokevirtual getDefaultFormat : (Landroid/media/MediaFormat;)V
      //   148: goto -> 190
      //   151: aload_0
      //   152: getfield mMime : Ljava/lang/String;
      //   155: invokevirtual toLowerCase : ()Ljava/lang/String;
      //   158: ldc 'video/'
      //   160: invokevirtual startsWith : (Ljava/lang/String;)Z
      //   163: ifne -> 180
      //   166: aload_0
      //   167: getfield mMime : Ljava/lang/String;
      //   170: astore_1
      //   171: aload_1
      //   172: ldc 'image/vnd.android.heic'
      //   174: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   177: ifeq -> 190
      //   180: aload_0
      //   181: aload #5
      //   183: aload_0
      //   184: invokestatic create : (Landroid/media/MediaFormat;Landroid/media/MediaCodecInfo$CodecCapabilities;)Landroid/media/MediaCodecInfo$VideoCapabilities;
      //   187: putfield mVideoCaps : Landroid/media/MediaCodecInfo$VideoCapabilities;
      //   190: iload_3
      //   191: ifeq -> 214
      //   194: aload #5
      //   196: aload_0
      //   197: invokestatic create : (Landroid/media/MediaFormat;Landroid/media/MediaCodecInfo$CodecCapabilities;)Landroid/media/MediaCodecInfo$EncoderCapabilities;
      //   200: astore_1
      //   201: aload_0
      //   202: aload_1
      //   203: putfield mEncoderCaps : Landroid/media/MediaCodecInfo$EncoderCapabilities;
      //   206: aload_1
      //   207: aload_0
      //   208: getfield mDefaultFormat : Landroid/media/MediaFormat;
      //   211: invokevirtual getDefaultFormat : (Landroid/media/MediaFormat;)V
      //   214: invokestatic getGlobalSettings : ()Ljava/util/Map;
      //   217: astore_1
      //   218: aload_1
      //   219: ldc_w 'max-concurrent-instances'
      //   222: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   227: astore_1
      //   228: aload_0
      //   229: aload_1
      //   230: bipush #32
      //   232: invokestatic parseIntSafely : (Ljava/lang/Object;I)I
      //   235: putfield mMaxSupportedInstances : I
      //   238: aload #6
      //   240: ldc_w 'max-concurrent-instances'
      //   243: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   248: astore_1
      //   249: aload_0
      //   250: getfield mMaxSupportedInstances : I
      //   253: istore #8
      //   255: aload_1
      //   256: iload #8
      //   258: invokestatic parseIntSafely : (Ljava/lang/Object;I)I
      //   261: istore #8
      //   263: aload_0
      //   264: iconst_1
      //   265: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   268: sipush #256
      //   271: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   274: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   277: iload #8
      //   279: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   282: invokevirtual clamp : (Ljava/lang/Comparable;)Ljava/lang/Comparable;
      //   285: checkcast java/lang/Integer
      //   288: invokevirtual intValue : ()I
      //   291: putfield mMaxSupportedInstances : I
      //   294: aload_0
      //   295: invokespecial getValidFeatures : ()[Landroid/media/MediaCodecInfo$Feature;
      //   298: astore_1
      //   299: aload_1
      //   300: arraylength
      //   301: istore #8
      //   303: iload #9
      //   305: istore_3
      //   306: iload #7
      //   308: iload #8
      //   310: if_icmpge -> 432
      //   313: aload_1
      //   314: iload #7
      //   316: aaload
      //   317: astore #4
      //   319: new java/lang/StringBuilder
      //   322: dup
      //   323: invokespecial <init> : ()V
      //   326: astore_2
      //   327: aload_2
      //   328: ldc_w 'feature-'
      //   331: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   334: pop
      //   335: aload_2
      //   336: aload #4
      //   338: getfield mName : Ljava/lang/String;
      //   341: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   344: pop
      //   345: aload_2
      //   346: invokevirtual toString : ()Ljava/lang/String;
      //   349: astore_2
      //   350: aload #6
      //   352: aload_2
      //   353: invokeinterface get : (Ljava/lang/Object;)Ljava/lang/Object;
      //   358: checkcast java/lang/Integer
      //   361: astore #5
      //   363: aload #5
      //   365: ifnonnull -> 371
      //   368: goto -> 426
      //   371: aload #5
      //   373: invokevirtual intValue : ()I
      //   376: ifle -> 397
      //   379: aload_0
      //   380: getfield mFlagsRequired : I
      //   383: istore #10
      //   385: aload_0
      //   386: aload #4
      //   388: getfield mValue : I
      //   391: iload #10
      //   393: ior
      //   394: putfield mFlagsRequired : I
      //   397: aload_0
      //   398: aload_0
      //   399: getfield mFlagsSupported : I
      //   402: aload #4
      //   404: getfield mValue : I
      //   407: ior
      //   408: putfield mFlagsSupported : I
      //   411: aload_0
      //   412: getfield mDefaultFormat : Landroid/media/MediaFormat;
      //   415: astore #4
      //   417: iconst_1
      //   418: istore_3
      //   419: aload #4
      //   421: aload_2
      //   422: iconst_1
      //   423: invokevirtual setInteger : (Ljava/lang/String;I)V
      //   426: iinc #7, 1
      //   429: goto -> 306
      //   432: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1025	-> 0
      //   #1026	-> 4
      //   #1027	-> 11
      //   #1028	-> 16
      //   #1029	-> 24
      //   #1030	-> 30
      //   #1031	-> 36
      //   #1035	-> 51
      //   #1036	-> 77
      //   #1037	-> 85
      //   #1038	-> 90
      //   #1039	-> 99
      //   #1041	-> 108
      //   #1043	-> 113
      //   #1044	-> 128
      //   #1045	-> 140
      //   #1046	-> 151
      //   #1047	-> 171
      //   #1048	-> 180
      //   #1050	-> 190
      //   #1051	-> 194
      //   #1052	-> 206
      //   #1055	-> 214
      //   #1056	-> 218
      //   #1057	-> 218
      //   #1056	-> 228
      //   #1059	-> 238
      //   #1060	-> 238
      //   #1059	-> 255
      //   #1061	-> 263
      //   #1062	-> 263
      //   #1064	-> 294
      //   #1065	-> 319
      //   #1066	-> 350
      //   #1067	-> 363
      //   #1068	-> 368
      //   #1070	-> 371
      //   #1071	-> 379
      //   #1073	-> 397
      //   #1074	-> 411
      //   #1064	-> 426
      //   #1077	-> 432
    }
  }
  
  public static final class AudioCapabilities {
    private static final int MAX_INPUT_CHANNEL_COUNT = 30;
    
    private static final String TAG = "AudioCapabilities";
    
    private Range<Integer> mBitrateRange;
    
    private int mMaxInputChannelCount;
    
    private MediaCodecInfo.CodecCapabilities mParent;
    
    private Range<Integer>[] mSampleRateRanges;
    
    private int[] mSampleRates;
    
    public Range<Integer> getBitrateRange() {
      return this.mBitrateRange;
    }
    
    public int[] getSupportedSampleRates() {
      int[] arrayOfInt = this.mSampleRates;
      if (arrayOfInt != null) {
        arrayOfInt = Arrays.copyOf(arrayOfInt, arrayOfInt.length);
      } else {
        arrayOfInt = null;
      } 
      return arrayOfInt;
    }
    
    public Range<Integer>[] getSupportedSampleRateRanges() {
      Range<Integer>[] arrayOfRange = this.mSampleRateRanges;
      return Arrays.<Range<Integer>>copyOf(arrayOfRange, arrayOfRange.length);
    }
    
    public int getMaxInputChannelCount() {
      return this.mMaxInputChannelCount;
    }
    
    public static AudioCapabilities create(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      AudioCapabilities audioCapabilities = new AudioCapabilities();
      audioCapabilities.init(param1MediaFormat, param1CodecCapabilities);
      return audioCapabilities;
    }
    
    private void init(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      this.mParent = param1CodecCapabilities;
      initWithPlatformLimits();
      applyLevelLimits();
      parseFromInfo(param1MediaFormat);
    }
    
    private void initWithPlatformLimits() {
      this.mBitrateRange = Range.create(Integer.valueOf(0), Integer.valueOf(2147483647));
      this.mMaxInputChannelCount = 30;
      int i = SystemProperties.getInt("ro.mediacodec.min_sample_rate", 7350);
      int j = SystemProperties.getInt("ro.mediacodec.max_sample_rate", 192000);
      this.mSampleRateRanges = (Range<Integer>[])new Range[] { Range.create(Integer.valueOf(i), Integer.valueOf(j)) };
      this.mSampleRates = null;
    }
    
    private boolean supports(Integer param1Integer1, Integer param1Integer2) {
      if (param1Integer2 != null && (
        param1Integer2.intValue() < 1 || param1Integer2.intValue() > this.mMaxInputChannelCount))
        return false; 
      if (param1Integer1 != null) {
        int i = Utils.binarySearchDistinctRanges(this.mSampleRateRanges, param1Integer1);
        if (i < 0)
          return false; 
      } 
      return true;
    }
    
    public boolean isSampleRateSupported(int param1Int) {
      return supports(Integer.valueOf(param1Int), null);
    }
    
    private void limitSampleRates(int[] param1ArrayOfint) {
      Arrays.sort(param1ArrayOfint);
      ArrayList<Range> arrayList = new ArrayList();
      int i;
      byte b;
      for (i = param1ArrayOfint.length, b = 0; b < i; ) {
        int j = param1ArrayOfint[b];
        if (supports(Integer.valueOf(j), null))
          arrayList.add(Range.create(Integer.valueOf(j), Integer.valueOf(j))); 
        b++;
      } 
      this.mSampleRateRanges = arrayList.<Range<Integer>>toArray((Range<Integer>[])new Range[arrayList.size()]);
      createDiscreteSampleRates();
    }
    
    private void createDiscreteSampleRates() {
      this.mSampleRates = new int[this.mSampleRateRanges.length];
      byte b = 0;
      while (true) {
        Range<Integer>[] arrayOfRange = this.mSampleRateRanges;
        if (b < arrayOfRange.length) {
          this.mSampleRates[b] = ((Integer)arrayOfRange[b].getLower()).intValue();
          b++;
          continue;
        } 
        break;
      } 
    }
    
    private void limitSampleRates(Range<Integer>[] param1ArrayOfRange) {
      Utils.sortDistinctRanges(param1ArrayOfRange);
      Range[] arrayOfRange = (Range[])Utils.intersectSortedDistinctRanges(this.mSampleRateRanges, param1ArrayOfRange);
      int i;
      byte b;
      for (i = arrayOfRange.length, b = 0; b < i; ) {
        Range range = arrayOfRange[b];
        if (!((Integer)range.getLower()).equals(range.getUpper())) {
          this.mSampleRates = null;
          return;
        } 
        b++;
      } 
      createDiscreteSampleRates();
    }
    
    private void applyLevelLimits() {
      int[] arrayOfInt = null;
      Range range = null;
      Range<Integer> range1 = null;
      int i = 30;
      String str = this.mParent.getMimeType();
      boolean bool = str.equalsIgnoreCase("audio/mpeg");
      Integer integer1 = Integer.valueOf(8000);
      Integer integer2 = Integer.valueOf(1);
      if (bool) {
        arrayOfInt = new int[] { 8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000 };
        range1 = Range.create(integer1, Integer.valueOf(320000));
        i = 2;
      } else if (str.equalsIgnoreCase("audio/3gpp")) {
        arrayOfInt = new int[] { 8000 };
        range1 = Range.create(Integer.valueOf(4750), Integer.valueOf(12200));
        i = 1;
      } else if (str.equalsIgnoreCase("audio/amr-wb")) {
        arrayOfInt = new int[] { 16000 };
        range1 = Range.create(Integer.valueOf(6600), Integer.valueOf(23850));
        i = 1;
      } else if (str.equalsIgnoreCase("audio/mp4a-latm")) {
        arrayOfInt = new int[] { 
            7350, 8000, 11025, 12000, 16000, 22050, 24000, 32000, 44100, 48000, 
            64000, 88200, 96000 };
        range1 = Range.create(integer1, Integer.valueOf(510000));
        i = 48;
      } else if (str.equalsIgnoreCase("audio/vorbis")) {
        range1 = Range.create(Integer.valueOf(32000), Integer.valueOf(500000));
        range = Range.create(integer1, Integer.valueOf(192000));
        i = 255;
      } else if (str.equalsIgnoreCase("audio/opus")) {
        range1 = Range.create(Integer.valueOf(6000), Integer.valueOf(510000));
        arrayOfInt = new int[] { 8000, 12000, 16000, 24000, 48000 };
        i = 255;
      } else if (str.equalsIgnoreCase("audio/raw")) {
        range = Range.create(integer2, Integer.valueOf(96000));
        range1 = Range.create(integer2, Integer.valueOf(10000000));
        i = AudioSystem.OUT_CHANNEL_COUNT_MAX;
      } else if (str.equalsIgnoreCase("audio/flac")) {
        range = Range.create(integer2, Integer.valueOf(655350));
        i = 255;
      } else if (str.equalsIgnoreCase("audio/g711-alaw") || 
        str.equalsIgnoreCase("audio/g711-mlaw")) {
        arrayOfInt = new int[] { 8000 };
        range1 = Range.create(Integer.valueOf(64000), Integer.valueOf(64000));
      } else if (str.equalsIgnoreCase("audio/gsm")) {
        arrayOfInt = new int[] { 8000 };
        range1 = Range.create(Integer.valueOf(13000), Integer.valueOf(13000));
        i = 1;
      } else if (str.equalsIgnoreCase("audio/ac3")) {
        i = 6;
      } else if (str.equalsIgnoreCase("audio/eac3")) {
        i = 16;
      } else if (str.equalsIgnoreCase("audio/eac3-joc")) {
        arrayOfInt = new int[] { 48000 };
        range1 = Range.create(Integer.valueOf(32000), Integer.valueOf(6144000));
        i = 16;
      } else if (str.equalsIgnoreCase("audio/ac4")) {
        arrayOfInt = new int[] { 44100, 48000, 96000, 192000 };
        range1 = Range.create(Integer.valueOf(16000), Integer.valueOf(2688000));
        i = 24;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unsupported mime ");
        stringBuilder.append(str);
        Log.w("AudioCapabilities", stringBuilder.toString());
        MediaCodecInfo.CodecCapabilities codecCapabilities = this.mParent;
        codecCapabilities.mError |= 0x2;
      } 
      if (arrayOfInt != null) {
        limitSampleRates(arrayOfInt);
      } else if (range != null) {
        limitSampleRates((Range<Integer>[])new Range[] { range });
      } 
      applyLimits(i, range1);
    }
    
    private void applyLimits(int param1Int, Range<Integer> param1Range) {
      Range range = Range.create(Integer.valueOf(1), Integer.valueOf(this.mMaxInputChannelCount));
      this.mMaxInputChannelCount = ((Integer)range.clamp(Integer.valueOf(param1Int))).intValue();
      if (param1Range != null)
        this.mBitrateRange = this.mBitrateRange.intersect(param1Range); 
    }
    
    private void parseFromInfo(MediaFormat param1MediaFormat) {
      byte b2, b1 = 30;
      Range<Integer> range1 = MediaCodecInfo.POSITIVE_INTEGERS;
      if (param1MediaFormat.containsKey("sample-rate-ranges")) {
        String[] arrayOfString = param1MediaFormat.getString("sample-rate-ranges").split(",");
        Range[] arrayOfRange = new Range[arrayOfString.length];
        for (b2 = 0; b2 < arrayOfString.length; b2++)
          arrayOfRange[b2] = Utils.parseIntRange(arrayOfString[b2], null); 
        limitSampleRates((Range<Integer>[])arrayOfRange);
      } 
      if (param1MediaFormat.containsKey("max-channel-count")) {
        String str = param1MediaFormat.getString("max-channel-count");
        b2 = Utils.parseIntSafely(str, 30);
      } else {
        b2 = b1;
        if ((this.mParent.mError & 0x2) != 0)
          b2 = 0; 
      } 
      Range<Integer> range2 = range1;
      if (param1MediaFormat.containsKey("bitrate-range")) {
        Range<Integer> range = Utils.parseIntRange(param1MediaFormat.getString("bitrate-range"), range1);
        range2 = range1.intersect(range);
      } 
      applyLimits(b2, range2);
    }
    
    public void getDefaultFormat(MediaFormat param1MediaFormat) {
      if (((Integer)this.mBitrateRange.getLower()).equals(this.mBitrateRange.getUpper()))
        param1MediaFormat.setInteger("bitrate", ((Integer)this.mBitrateRange.getLower()).intValue()); 
      if (this.mMaxInputChannelCount == 1)
        param1MediaFormat.setInteger("channel-count", 1); 
      int[] arrayOfInt = this.mSampleRates;
      if (arrayOfInt != null && arrayOfInt.length == 1)
        param1MediaFormat.setInteger("sample-rate", arrayOfInt[0]); 
    }
    
    public boolean supportsFormat(MediaFormat param1MediaFormat) {
      Map<String, Object> map = param1MediaFormat.getMap();
      Integer integer2 = (Integer)map.get("sample-rate");
      Integer integer1 = (Integer)map.get("channel-count");
      if (!supports(integer2, integer1))
        return false; 
      if (!MediaCodecInfo.CodecCapabilities.supportsBitrate(this.mBitrateRange, param1MediaFormat))
        return false; 
      return true;
    }
  }
  
  public static final class VideoCapabilities {
    private static final String TAG = "VideoCapabilities";
    
    private boolean mAllowMbOverride;
    
    private Range<Rational> mAspectRatioRange;
    
    private Range<Integer> mBitrateRange;
    
    private Range<Rational> mBlockAspectRatioRange;
    
    private Range<Integer> mBlockCountRange;
    
    private int mBlockHeight;
    
    private int mBlockWidth;
    
    private Range<Long> mBlocksPerSecondRange;
    
    private Range<Integer> mFrameRateRange;
    
    private int mHeightAlignment;
    
    private Range<Integer> mHeightRange;
    
    private Range<Integer> mHorizontalBlockRange;
    
    private Map<Size, Range<Long>> mMeasuredFrameRates;
    
    private MediaCodecInfo.CodecCapabilities mParent;
    
    private List<PerformancePoint> mPerformancePoints;
    
    private int mSmallerDimensionUpperLimit;
    
    private Range<Integer> mVerticalBlockRange;
    
    private int mWidthAlignment;
    
    private Range<Integer> mWidthRange;
    
    public Range<Integer> getBitrateRange() {
      return this.mBitrateRange;
    }
    
    public Range<Integer> getSupportedWidths() {
      return this.mWidthRange;
    }
    
    public Range<Integer> getSupportedHeights() {
      return this.mHeightRange;
    }
    
    public int getWidthAlignment() {
      return this.mWidthAlignment;
    }
    
    public int getHeightAlignment() {
      return this.mHeightAlignment;
    }
    
    public int getSmallerDimensionUpperLimit() {
      return this.mSmallerDimensionUpperLimit;
    }
    
    public Range<Integer> getSupportedFrameRates() {
      return this.mFrameRateRange;
    }
    
    public Range<Integer> getSupportedWidthsFor(int param1Int) {
      try {
        Range<Integer> range = this.mWidthRange;
        if (this.mHeightRange.contains(Integer.valueOf(param1Int)) && param1Int % this.mHeightAlignment == 0) {
          int i = Utils.divUp(param1Int, this.mBlockHeight);
          Range<Integer> range5 = this.mBlockCountRange;
          int j = Utils.divUp(((Integer)range5.getLower()).intValue(), i);
          Range<Rational> range4 = this.mBlockAspectRatioRange;
          int k = (int)Math.ceil(((Rational)range4.getLower()).doubleValue() * i);
          k = Math.max(j, k);
          Range<Integer> range3 = this.mBlockCountRange;
          j = ((Integer)range3.getUpper()).intValue() / i;
          Range<Rational> range2 = this.mBlockAspectRatioRange;
          i = (int)(((Rational)range2.getUpper()).doubleValue() * i);
          j = Math.min(j, i);
          int m = this.mBlockWidth, n = this.mWidthAlignment;
          i = this.mBlockWidth;
          range2 = range.intersect(Integer.valueOf((k - 1) * m + n), Integer.valueOf(i * j));
          Range<Rational> range1 = range2;
          if (param1Int > this.mSmallerDimensionUpperLimit)
            range1 = range2.intersect(Integer.valueOf(1), Integer.valueOf(this.mSmallerDimensionUpperLimit)); 
          range2 = this.mAspectRatioRange;
          k = (int)Math.ceil(((Rational)range2.getLower()).doubleValue() * param1Int);
          range2 = this.mAspectRatioRange;
          i = (int)(((Rational)range2.getUpper()).doubleValue() * param1Int);
          range1 = range1.intersect(Integer.valueOf(k), Integer.valueOf(i));
          return (Range)range1;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("unsupported height");
        throw illegalArgumentException;
      } catch (IllegalArgumentException illegalArgumentException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("could not get supported widths for ");
        stringBuilder.append(param1Int);
        Log.v("VideoCapabilities", stringBuilder.toString());
        throw new IllegalArgumentException("unsupported height");
      } 
    }
    
    public Range<Integer> getSupportedHeightsFor(int param1Int) {
      try {
        Range<Integer> range = this.mHeightRange;
        if (this.mWidthRange.contains(Integer.valueOf(param1Int)) && param1Int % this.mWidthAlignment == 0) {
          int i = Utils.divUp(param1Int, this.mBlockWidth);
          Range<Integer> range5 = this.mBlockCountRange;
          int j = Utils.divUp(((Integer)range5.getLower()).intValue(), i);
          double d = i;
          Range<Rational> range4 = this.mBlockAspectRatioRange;
          d /= ((Rational)range4.getUpper()).doubleValue();
          int k = (int)Math.ceil(d);
          j = Math.max(j, k);
          Range<Integer> range3 = this.mBlockCountRange;
          k = ((Integer)range3.getUpper()).intValue() / i;
          d = i;
          Range<Rational> range2 = this.mBlockAspectRatioRange;
          i = (int)(d / ((Rational)range2.getLower()).doubleValue());
          int m = Math.min(k, i);
          i = this.mBlockHeight;
          int n = this.mHeightAlignment;
          k = this.mBlockHeight;
          range2 = range.intersect(Integer.valueOf((j - 1) * i + n), Integer.valueOf(k * m));
          Range<Rational> range1 = range2;
          if (param1Int > this.mSmallerDimensionUpperLimit)
            range1 = range2.intersect(Integer.valueOf(1), Integer.valueOf(this.mSmallerDimensionUpperLimit)); 
          d = param1Int;
          range2 = this.mAspectRatioRange;
          d /= ((Rational)range2.getUpper()).doubleValue();
          j = (int)Math.ceil(d);
          d = param1Int;
          range2 = this.mAspectRatioRange;
          i = (int)(d / ((Rational)range2.getLower()).doubleValue());
          range1 = range1.intersect(Integer.valueOf(j), Integer.valueOf(i));
          return (Range)range1;
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("unsupported width");
        throw illegalArgumentException;
      } catch (IllegalArgumentException illegalArgumentException) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("could not get supported heights for ");
        stringBuilder.append(param1Int);
        Log.v("VideoCapabilities", stringBuilder.toString());
        throw new IllegalArgumentException("unsupported width");
      } 
    }
    
    public Range<Double> getSupportedFrameRatesFor(int param1Int1, int param1Int2) {
      Range<Integer> range = this.mHeightRange;
      if (supports(Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), null)) {
        int i = this.mBlockWidth;
        param1Int1 = Utils.divUp(param1Int1, i) * Utils.divUp(param1Int2, this.mBlockHeight);
        Range<Long> range4 = this.mBlocksPerSecondRange;
        double d1 = ((Long)range4.getLower()).longValue() / param1Int1;
        Range<Integer> range3 = this.mFrameRateRange;
        double d2 = ((Integer)range3.getLower()).intValue();
        d2 = Math.max(d1, d2);
        Range<Long> range2 = this.mBlocksPerSecondRange;
        double d3 = ((Long)range2.getUpper()).longValue() / param1Int1;
        Range<Integer> range1 = this.mFrameRateRange;
        d1 = ((Integer)range1.getUpper()).intValue();
        d1 = Math.min(d3, d1);
        return Range.create(Double.valueOf(d2), Double.valueOf(d1));
      } 
      throw new IllegalArgumentException("unsupported size");
    }
    
    private int getBlockCount(int param1Int1, int param1Int2) {
      return Utils.divUp(param1Int1, this.mBlockWidth) * Utils.divUp(param1Int2, this.mBlockHeight);
    }
    
    private Size findClosestSize(int param1Int1, int param1Int2) {
      int i = getBlockCount(param1Int1, param1Int2);
      Size size = null;
      param1Int1 = Integer.MAX_VALUE;
      for (Size size1 : this.mMeasuredFrameRates.keySet()) {
        param1Int2 = getBlockCount(size1.getWidth(), size1.getHeight());
        int j = Math.abs(i - param1Int2);
        param1Int2 = param1Int1;
        if (j < param1Int1) {
          param1Int2 = j;
          size = size1;
        } 
        param1Int1 = param1Int2;
      } 
      return size;
    }
    
    private Range<Double> estimateFrameRatesFor(int param1Int1, int param1Int2) {
      Size size = findClosestSize(param1Int1, param1Int2);
      Range range = this.mMeasuredFrameRates.get(size);
      double d = getBlockCount(size.getWidth(), size.getHeight());
      d /= Math.max(getBlockCount(param1Int1, param1Int2), 1);
      Double double_ = Double.valueOf(d);
      return Range.create(Double.valueOf(((Long)range.getLower()).longValue() * double_.doubleValue()), Double.valueOf(((Long)range.getUpper()).longValue() * double_.doubleValue()));
    }
    
    public Range<Double> getAchievableFrameRatesFor(int param1Int1, int param1Int2) {
      if (supports(Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), null)) {
        Map<Size, Range<Long>> map = this.mMeasuredFrameRates;
        if (map == null || map.size() <= 0) {
          Log.w("VideoCapabilities", "Codec did not publish any measurement data.");
          return null;
        } 
        return estimateFrameRatesFor(param1Int1, param1Int2);
      } 
      throw new IllegalArgumentException("unsupported size");
    }
    
    public static final class PerformancePoint {
      public static final PerformancePoint FHD_100;
      
      public static final PerformancePoint FHD_120;
      
      public static final PerformancePoint FHD_200;
      
      public static final PerformancePoint FHD_24;
      
      public static final PerformancePoint FHD_240;
      
      public static final PerformancePoint FHD_25;
      
      public static final PerformancePoint FHD_30;
      
      public static final PerformancePoint FHD_50;
      
      public static final PerformancePoint FHD_60;
      
      public static final PerformancePoint HD_100 = new PerformancePoint(1280, 720, 100);
      
      public static final PerformancePoint HD_120 = new PerformancePoint(1280, 720, 120);
      
      public static final PerformancePoint HD_200 = new PerformancePoint(1280, 720, 200);
      
      public static final PerformancePoint HD_24 = new PerformancePoint(1280, 720, 24);
      
      public static final PerformancePoint HD_240 = new PerformancePoint(1280, 720, 240);
      
      public static final PerformancePoint HD_25 = new PerformancePoint(1280, 720, 25);
      
      public static final PerformancePoint HD_30 = new PerformancePoint(1280, 720, 30);
      
      public static final PerformancePoint HD_50 = new PerformancePoint(1280, 720, 50);
      
      public static final PerformancePoint HD_60 = new PerformancePoint(1280, 720, 60);
      
      public int getMaxMacroBlocks() {
        return saturateLongToInt(this.mWidth * this.mHeight);
      }
      
      public int getMaxFrameRate() {
        return this.mMaxFrameRate;
      }
      
      public long getMaxMacroBlockRate() {
        return this.mMaxMacroBlockRate;
      }
      
      public String toString() {
        int i = this.mBlockSize.getWidth() * 16;
        int j = this.mBlockSize.getHeight() * 16;
        int k = (int)Utils.divUp(this.mMaxMacroBlockRate, getMaxMacroBlocks());
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append(this.mWidth * 16);
        stringBuilder2.append("x");
        stringBuilder2.append(this.mHeight * 16);
        stringBuilder2.append("@");
        stringBuilder2.append(k);
        String str3 = stringBuilder2.toString();
        String str1 = str3;
        if (k < this.mMaxFrameRate) {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str3);
          stringBuilder1.append(", max ");
          stringBuilder1.append(this.mMaxFrameRate);
          stringBuilder1.append("fps");
          str1 = stringBuilder1.toString();
        } 
        if (i <= 16) {
          String str;
          str3 = str1;
          if (j > 16) {
            StringBuilder stringBuilder5 = new StringBuilder();
            stringBuilder5.append(str1);
            stringBuilder5.append(", ");
            stringBuilder5.append(i);
            stringBuilder5.append("x");
            stringBuilder5.append(j);
            stringBuilder5.append(" blocks");
            str = stringBuilder5.toString();
            StringBuilder stringBuilder4 = new StringBuilder();
            stringBuilder4.append("PerformancePoint(");
            stringBuilder4.append(str);
            stringBuilder4.append(")");
            return stringBuilder4.toString();
          } 
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append("PerformancePoint(");
          stringBuilder1.append(str);
          stringBuilder1.append(")");
          return stringBuilder1.toString();
        } 
        StringBuilder stringBuilder3 = new StringBuilder();
        stringBuilder3.append((String)stringBuilder1);
        stringBuilder3.append(", ");
        stringBuilder3.append(i);
        stringBuilder3.append("x");
        stringBuilder3.append(j);
        stringBuilder3.append(" blocks");
        String str2 = stringBuilder3.toString();
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("PerformancePoint(");
        stringBuilder1.append(str2);
        stringBuilder1.append(")");
        return stringBuilder1.toString();
      }
      
      public int hashCode() {
        return this.mMaxFrameRate;
      }
      
      public PerformancePoint(int param2Int1, int param2Int2, int param2Int3, int param2Int4, Size param2Size) {
        MediaCodecInfo.checkPowerOfTwo(param2Size.getWidth(), "block width");
        MediaCodecInfo.checkPowerOfTwo(param2Size.getHeight(), "block height");
        int i = Utils.divUp(param2Size.getWidth(), 16);
        this.mBlockSize = new Size(i, Utils.divUp(param2Size.getHeight(), 16));
        long l1 = Math.max(1L, param2Int1);
        long l2 = Math.max(param2Size.getWidth(), 16);
        l2 = Utils.divUp(l1, l2);
        Size size = this.mBlockSize;
        this.mWidth = (int)(l2 * size.getWidth());
        l1 = Math.max(1L, param2Int2);
        l2 = Math.max(param2Size.getHeight(), 16);
        l2 = Utils.divUp(l1, l2);
        param2Size = this.mBlockSize;
        this.mHeight = (int)(l2 * param2Size.getHeight());
        this.mMaxFrameRate = Math.max(1, Math.max(param2Int3, param2Int4));
        this.mMaxMacroBlockRate = (Math.max(1, param2Int3) * getMaxMacroBlocks());
      }
      
      public PerformancePoint(PerformancePoint param2PerformancePoint, Size param2Size) {
        this(i * 16, j * 16, k, m, size);
      }
      
      public PerformancePoint(int param2Int1, int param2Int2, int param2Int3) {
        this(param2Int1, param2Int2, param2Int3, param2Int3, new Size(16, 16));
      }
      
      private int saturateLongToInt(long param2Long) {
        if (param2Long < -2147483648L)
          return Integer.MIN_VALUE; 
        if (param2Long > 2147483647L)
          return Integer.MAX_VALUE; 
        return (int)param2Long;
      }
      
      private int align(int param2Int1, int param2Int2) {
        return Utils.divUp(param2Int1, param2Int2) * param2Int2;
      }
      
      private void checkPowerOfTwo2(int param2Int, String param2String) {
        if (param2Int != 0 && (param2Int - 1 & param2Int) == 0)
          return; 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(param2String);
        stringBuilder.append(" (");
        stringBuilder.append(param2Int);
        stringBuilder.append(") must be a power of 2");
        throw new IllegalArgumentException(stringBuilder.toString());
      }
      
      public boolean covers(MediaFormat param2MediaFormat) {
        int i = param2MediaFormat.getInteger("width", 0);
        int j = param2MediaFormat.getInteger("height", 0);
        Number number = param2MediaFormat.getNumber("frame-rate", Integer.valueOf(0));
        double d = number.doubleValue();
        float f = (float)Math.ceil(d);
        PerformancePoint performancePoint = new PerformancePoint(i, j, Math.round(f));
        return covers(performancePoint);
      }
      
      public boolean covers(PerformancePoint param2PerformancePoint) {
        boolean bool;
        Size size = getCommonBlockSize(param2PerformancePoint);
        PerformancePoint performancePoint = new PerformancePoint(this, size);
        param2PerformancePoint = new PerformancePoint(param2PerformancePoint, size);
        if (performancePoint.getMaxMacroBlocks() >= param2PerformancePoint.getMaxMacroBlocks() && performancePoint.mMaxFrameRate >= param2PerformancePoint.mMaxFrameRate && performancePoint.mMaxMacroBlockRate >= param2PerformancePoint.mMaxMacroBlockRate) {
          bool = true;
        } else {
          bool = false;
        } 
        return bool;
      }
      
      private Size getCommonBlockSize(PerformancePoint param2PerformancePoint) {
        Size size = this.mBlockSize;
        int i = Math.max(size.getWidth(), param2PerformancePoint.mBlockSize.getWidth());
        size = this.mBlockSize;
        return new Size(i * 16, Math.max(size.getHeight(), param2PerformancePoint.mBlockSize.getHeight()) * 16);
      }
      
      public boolean equals(Object param2Object) {
        boolean bool = param2Object instanceof PerformancePoint;
        boolean bool1 = false;
        if (bool) {
          PerformancePoint performancePoint1 = (PerformancePoint)param2Object;
          Size size = getCommonBlockSize(performancePoint1);
          param2Object = new PerformancePoint(this, size);
          PerformancePoint performancePoint2 = new PerformancePoint(performancePoint1, size);
          bool = bool1;
          if (param2Object.getMaxMacroBlocks() == performancePoint2.getMaxMacroBlocks()) {
            bool = bool1;
            if (((PerformancePoint)param2Object).mMaxFrameRate == performancePoint2.mMaxFrameRate) {
              bool = bool1;
              if (((PerformancePoint)param2Object).mMaxMacroBlockRate == performancePoint2.mMaxMacroBlockRate)
                bool = true; 
            } 
          } 
          return bool;
        } 
        return false;
      }
      
      public static final PerformancePoint SD_24 = new PerformancePoint(720, 480, 24);
      
      public static final PerformancePoint SD_25 = new PerformancePoint(720, 576, 25);
      
      public static final PerformancePoint SD_30 = new PerformancePoint(720, 480, 30);
      
      public static final PerformancePoint SD_48 = new PerformancePoint(720, 480, 48);
      
      public static final PerformancePoint SD_50 = new PerformancePoint(720, 576, 50);
      
      public static final PerformancePoint SD_60 = new PerformancePoint(720, 480, 60);
      
      public static final PerformancePoint UHD_100;
      
      public static final PerformancePoint UHD_120;
      
      public static final PerformancePoint UHD_200;
      
      public static final PerformancePoint UHD_24;
      
      public static final PerformancePoint UHD_240;
      
      public static final PerformancePoint UHD_25;
      
      public static final PerformancePoint UHD_30;
      
      public static final PerformancePoint UHD_50;
      
      public static final PerformancePoint UHD_60;
      
      private Size mBlockSize;
      
      private int mHeight;
      
      private int mMaxFrameRate;
      
      private long mMaxMacroBlockRate;
      
      private int mWidth;
      
      static {
        FHD_24 = new PerformancePoint(1920, 1080, 24);
        FHD_25 = new PerformancePoint(1920, 1080, 25);
        FHD_30 = new PerformancePoint(1920, 1080, 30);
        FHD_50 = new PerformancePoint(1920, 1080, 50);
        FHD_60 = new PerformancePoint(1920, 1080, 60);
        FHD_100 = new PerformancePoint(1920, 1080, 100);
        FHD_120 = new PerformancePoint(1920, 1080, 120);
        FHD_200 = new PerformancePoint(1920, 1080, 200);
        FHD_240 = new PerformancePoint(1920, 1080, 240);
        UHD_24 = new PerformancePoint(3840, 2160, 24);
        UHD_25 = new PerformancePoint(3840, 2160, 25);
        UHD_30 = new PerformancePoint(3840, 2160, 30);
        UHD_50 = new PerformancePoint(3840, 2160, 50);
        UHD_60 = new PerformancePoint(3840, 2160, 60);
        UHD_100 = new PerformancePoint(3840, 2160, 100);
        UHD_120 = new PerformancePoint(3840, 2160, 120);
        UHD_200 = new PerformancePoint(3840, 2160, 200);
        UHD_240 = new PerformancePoint(3840, 2160, 240);
      }
    }
    
    public List<PerformancePoint> getSupportedPerformancePoints() {
      return this.mPerformancePoints;
    }
    
    public boolean areSizeAndRateSupported(int param1Int1, int param1Int2, double param1Double) {
      return supports(Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), Double.valueOf(param1Double));
    }
    
    public boolean isSizeSupported(int param1Int1, int param1Int2) {
      return supports(Integer.valueOf(param1Int1), Integer.valueOf(param1Int2), null);
    }
    
    private boolean supports(Integer param1Integer1, Integer param1Integer2, Number param1Number) {
      // Byte code:
      //   0: iconst_1
      //   1: istore #4
      //   3: iconst_1
      //   4: istore #5
      //   6: iload #4
      //   8: istore #6
      //   10: iconst_1
      //   11: ifeq -> 54
      //   14: iload #4
      //   16: istore #6
      //   18: aload_1
      //   19: ifnull -> 54
      //   22: aload_0
      //   23: getfield mWidthRange : Landroid/util/Range;
      //   26: aload_1
      //   27: invokevirtual contains : (Ljava/lang/Comparable;)Z
      //   30: ifeq -> 51
      //   33: aload_1
      //   34: invokevirtual intValue : ()I
      //   37: aload_0
      //   38: getfield mWidthAlignment : I
      //   41: irem
      //   42: ifne -> 51
      //   45: iconst_1
      //   46: istore #6
      //   48: goto -> 54
      //   51: iconst_0
      //   52: istore #6
      //   54: iload #6
      //   56: istore #4
      //   58: iload #6
      //   60: ifeq -> 107
      //   63: iload #6
      //   65: istore #4
      //   67: aload_2
      //   68: ifnull -> 107
      //   71: aload_0
      //   72: getfield mHeightRange : Landroid/util/Range;
      //   75: aload_2
      //   76: invokevirtual contains : (Ljava/lang/Comparable;)Z
      //   79: ifeq -> 100
      //   82: aload_2
      //   83: invokevirtual intValue : ()I
      //   86: aload_0
      //   87: getfield mHeightAlignment : I
      //   90: irem
      //   91: ifne -> 100
      //   94: iconst_1
      //   95: istore #6
      //   97: goto -> 103
      //   100: iconst_0
      //   101: istore #6
      //   103: iload #6
      //   105: istore #4
      //   107: iload #4
      //   109: istore #6
      //   111: iload #4
      //   113: ifeq -> 140
      //   116: iload #4
      //   118: istore #6
      //   120: aload_3
      //   121: ifnull -> 140
      //   124: aload_0
      //   125: getfield mFrameRateRange : Landroid/util/Range;
      //   128: aload_3
      //   129: invokevirtual doubleValue : ()D
      //   132: invokestatic intRangeFor : (D)Landroid/util/Range;
      //   135: invokevirtual contains : (Landroid/util/Range;)Z
      //   138: istore #6
      //   140: iload #6
      //   142: istore #4
      //   144: iload #6
      //   146: ifeq -> 362
      //   149: iload #6
      //   151: istore #4
      //   153: aload_2
      //   154: ifnull -> 362
      //   157: iload #6
      //   159: istore #4
      //   161: aload_1
      //   162: ifnull -> 362
      //   165: aload_2
      //   166: invokevirtual intValue : ()I
      //   169: aload_1
      //   170: invokevirtual intValue : ()I
      //   173: invokestatic min : (II)I
      //   176: aload_0
      //   177: getfield mSmallerDimensionUpperLimit : I
      //   180: if_icmpgt -> 189
      //   183: iconst_1
      //   184: istore #7
      //   186: goto -> 192
      //   189: iconst_0
      //   190: istore #7
      //   192: aload_1
      //   193: invokevirtual intValue : ()I
      //   196: aload_0
      //   197: getfield mBlockWidth : I
      //   200: invokestatic divUp : (II)I
      //   203: istore #8
      //   205: aload_2
      //   206: invokevirtual intValue : ()I
      //   209: aload_0
      //   210: getfield mBlockHeight : I
      //   213: invokestatic divUp : (II)I
      //   216: istore #9
      //   218: iload #8
      //   220: iload #9
      //   222: imul
      //   223: istore #10
      //   225: iload #7
      //   227: ifeq -> 310
      //   230: aload_0
      //   231: getfield mBlockCountRange : Landroid/util/Range;
      //   234: iload #10
      //   236: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   239: invokevirtual contains : (Ljava/lang/Comparable;)Z
      //   242: ifeq -> 310
      //   245: aload_0
      //   246: getfield mBlockAspectRatioRange : Landroid/util/Range;
      //   249: astore #11
      //   251: new android/util/Rational
      //   254: dup
      //   255: iload #8
      //   257: iload #9
      //   259: invokespecial <init> : (II)V
      //   262: astore #12
      //   264: aload #11
      //   266: aload #12
      //   268: invokevirtual contains : (Ljava/lang/Comparable;)Z
      //   271: ifeq -> 310
      //   274: aload_0
      //   275: getfield mAspectRatioRange : Landroid/util/Range;
      //   278: astore #11
      //   280: aload #11
      //   282: new android/util/Rational
      //   285: dup
      //   286: aload_1
      //   287: invokevirtual intValue : ()I
      //   290: aload_2
      //   291: invokevirtual intValue : ()I
      //   294: invokespecial <init> : (II)V
      //   297: invokevirtual contains : (Ljava/lang/Comparable;)Z
      //   300: ifeq -> 310
      //   303: iload #5
      //   305: istore #6
      //   307: goto -> 313
      //   310: iconst_0
      //   311: istore #6
      //   313: iload #6
      //   315: istore #4
      //   317: iload #6
      //   319: ifeq -> 362
      //   322: iload #6
      //   324: istore #4
      //   326: aload_3
      //   327: ifnull -> 362
      //   330: iload #10
      //   332: i2d
      //   333: dstore #13
      //   335: aload_3
      //   336: invokevirtual doubleValue : ()D
      //   339: dstore #15
      //   341: aload_0
      //   342: getfield mBlocksPerSecondRange : Landroid/util/Range;
      //   345: astore_1
      //   346: dload #13
      //   348: dload #15
      //   350: dmul
      //   351: invokestatic longRangeFor : (D)Landroid/util/Range;
      //   354: astore_2
      //   355: aload_1
      //   356: aload_2
      //   357: invokevirtual contains : (Landroid/util/Range;)Z
      //   360: istore #4
      //   362: iload #4
      //   364: ireturn
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2024	-> 0
      //   #2026	-> 3
      //   #2027	-> 22
      //   #2028	-> 33
      //   #2030	-> 54
      //   #2031	-> 71
      //   #2032	-> 82
      //   #2034	-> 107
      //   #2035	-> 124
      //   #2037	-> 140
      //   #2038	-> 165
      //   #2040	-> 192
      //   #2041	-> 205
      //   #2042	-> 218
      //   #2043	-> 225
      //   #2044	-> 264
      //   #2046	-> 280
      //   #2047	-> 313
      //   #2048	-> 330
      //   #2049	-> 341
      //   #2050	-> 346
      //   #2049	-> 355
      //   #2053	-> 362
    }
    
    public boolean supportsFormat(MediaFormat param1MediaFormat) {
      Map<String, Object> map = param1MediaFormat.getMap();
      Integer integer1 = (Integer)map.get("width");
      Integer integer2 = (Integer)map.get("height");
      Number number = (Number)map.get("frame-rate");
      if (!supports(integer1, integer2, number))
        return false; 
      if (!MediaCodecInfo.CodecCapabilities.supportsBitrate(this.mBitrateRange, param1MediaFormat))
        return false; 
      return true;
    }
    
    public static VideoCapabilities create(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      VideoCapabilities videoCapabilities = new VideoCapabilities();
      videoCapabilities.init(param1MediaFormat, param1CodecCapabilities);
      return videoCapabilities;
    }
    
    private void init(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      this.mParent = param1CodecCapabilities;
      initWithPlatformLimits();
      applyLevelLimits();
      parseFromInfo(param1MediaFormat);
      updateLimits();
    }
    
    public Size getBlockSize() {
      return new Size(this.mBlockWidth, this.mBlockHeight);
    }
    
    public Range<Integer> getBlockCountRange() {
      return this.mBlockCountRange;
    }
    
    public Range<Long> getBlocksPerSecondRange() {
      return this.mBlocksPerSecondRange;
    }
    
    public Range<Rational> getAspectRatioRange(boolean param1Boolean) {
      Range<Rational> range;
      if (param1Boolean) {
        range = this.mBlockAspectRatioRange;
      } else {
        range = this.mAspectRatioRange;
      } 
      return range;
    }
    
    private void initWithPlatformLimits() {
      this.mBitrateRange = MediaCodecInfo.BITRATE_RANGE;
      this.mWidthRange = MediaCodecInfo.SIZE_RANGE;
      this.mHeightRange = MediaCodecInfo.SIZE_RANGE;
      this.mFrameRateRange = MediaCodecInfo.FRAME_RATE_RANGE;
      this.mHorizontalBlockRange = MediaCodecInfo.SIZE_RANGE;
      this.mVerticalBlockRange = MediaCodecInfo.SIZE_RANGE;
      this.mBlockCountRange = MediaCodecInfo.POSITIVE_INTEGERS;
      this.mBlocksPerSecondRange = MediaCodecInfo.POSITIVE_LONGS;
      this.mBlockAspectRatioRange = MediaCodecInfo.POSITIVE_RATIONALS;
      this.mAspectRatioRange = MediaCodecInfo.POSITIVE_RATIONALS;
      this.mWidthAlignment = 2;
      this.mHeightAlignment = 2;
      this.mBlockWidth = 2;
      this.mBlockHeight = 2;
      this.mSmallerDimensionUpperLimit = ((Integer)MediaCodecInfo.SIZE_RANGE.getUpper()).intValue();
    }
    
    private List<PerformancePoint> getPerformancePoints(Map<String, Object> param1Map) {
      Vector<? extends PerformancePoint> vector = new Vector();
      String str = "performance-point-";
      Set<String> set = param1Map.keySet();
      for (String str1 : set) {
        if (!str1.startsWith("performance-point-"))
          continue; 
        String str3 = str1.substring("performance-point-".length());
        if (str3.equals("none") && vector.size() == 0)
          return Collections.unmodifiableList(vector); 
        String[] arrayOfString = str1.split("-");
        if (arrayOfString.length != 4)
          continue; 
        String str2 = arrayOfString[2];
        Size size = Utils.parseSize(str2, null);
        if (size == null || size.getWidth() * size.getHeight() <= 0)
          continue; 
        Range<Long> range = Utils.parseLongRange(param1Map.get(str1), null);
        if (range == null || ((Long)range.getLower()).longValue() < 0L || ((Long)range.getUpper()).longValue() < 0L)
          continue; 
        int i = size.getWidth(), j = size.getHeight(), k = ((Long)range.getLower()).intValue();
        PerformancePoint performancePoint1 = new PerformancePoint(i, j, k, ((Long)range.getUpper()).intValue(), new Size(this.mBlockWidth, this.mBlockHeight));
        j = size.getHeight();
        i = size.getWidth();
        k = ((Long)range.getLower()).intValue();
        PerformancePoint performancePoint2 = new PerformancePoint(j, i, k, ((Long)range.getUpper()).intValue(), new Size(this.mBlockWidth, this.mBlockHeight));
        vector.add(performancePoint1);
        if (!performancePoint1.covers(performancePoint2))
          vector.add(performancePoint2); 
      } 
      if (vector.size() == 0)
        return null; 
      vector.sort((Comparator<? super PerformancePoint>)_$$Lambda$MediaCodecInfo$VideoCapabilities$DpgwEn_gVFZT9EtP3qcxpiA2G0M.INSTANCE);
      return Collections.unmodifiableList(vector);
    }
    
    private Map<Size, Range<Long>> getMeasuredFrameRates(Map<String, Object> param1Map) {
      HashMap<Object, Object> hashMap = new HashMap<>();
      Set<String> set = param1Map.keySet();
      for (String str1 : set) {
        if (!str1.startsWith("measured-frame-rate-"))
          continue; 
        str1.substring("measured-frame-rate-".length());
        String[] arrayOfString = str1.split("-");
        if (arrayOfString.length != 5)
          continue; 
        String str2 = arrayOfString[3];
        Size size = Utils.parseSize(str2, null);
        if (size == null || size.getWidth() * size.getHeight() <= 0)
          continue; 
        Range<Long> range = Utils.parseLongRange(param1Map.get(str1), null);
        if (range == null || ((Long)range.getLower()).longValue() < 0L || ((Long)range.getUpper()).longValue() < 0L)
          continue; 
        hashMap.put(size, range);
      } 
      return (Map)hashMap;
    }
    
    private static Pair<Range<Integer>, Range<Integer>> parseWidthHeightRanges(Object param1Object) {
      Pair<Size, Size> pair = Utils.parseSizeRange(param1Object);
      if (pair != null)
        try {
          Size size1 = (Size)pair.first;
          Range range2 = Range.create(Integer.valueOf(size1.getWidth()), Integer.valueOf(((Size)pair.second).getWidth()));
          Size size2 = (Size)pair.first;
          Range range1 = Range.create(Integer.valueOf(size2.getHeight()), Integer.valueOf(((Size)pair.second).getHeight()));
          return Pair.create(range2, range1);
        } catch (IllegalArgumentException illegalArgumentException) {
          StringBuilder stringBuilder = new StringBuilder();
          stringBuilder.append("could not parse size range '");
          stringBuilder.append(param1Object);
          stringBuilder.append("'");
          Log.w("VideoCapabilities", stringBuilder.toString());
        }  
      return null;
    }
    
    public static int equivalentVP9Level(MediaFormat param1MediaFormat) {
      int k;
      long l;
      Map<String, Object> map = param1MediaFormat.getMap();
      Size size = Utils.parseSize(map.get("block-size"), new Size(8, 8));
      int i = size.getWidth() * size.getHeight();
      Range<Integer> range2 = Utils.parseIntRange(map.get("block-count-range"), null);
      int j = 0;
      if (range2 == null) {
        k = 0;
      } else {
        k = ((Integer)range2.getUpper()).intValue() * i;
      } 
      range2 = (Range)Utils.parseLongRange(map.get("blocks-per-second-range"), null);
      if (range2 == null) {
        l = 0L;
      } else {
        l = i * ((Long)range2.getUpper()).longValue();
      } 
      Pair<Range<Integer>, Range<Integer>> pair = parseWidthHeightRanges(map.get("size-range"));
      if (pair == null) {
        i = 0;
      } else {
        Range range = (Range)pair.first;
        i = ((Integer)range.getUpper()).intValue();
        int m = ((Integer)((Range)pair.second).getUpper()).intValue();
        i = Math.max(i, m);
      } 
      Range<Integer> range1 = Utils.parseIntRange(map.get("bitrate-range"), null);
      if (range1 != null)
        j = Utils.divUp(((Integer)range1.getUpper()).intValue(), 1000); 
      if (l <= 829440L && k <= 36864 && j <= 200 && i <= 512)
        return 1; 
      if (l <= 2764800L && k <= 73728 && j <= 800 && i <= 768)
        return 2; 
      if (l <= 4608000L && k <= 122880 && j <= 1800 && i <= 960)
        return 4; 
      if (l <= 9216000L && k <= 245760 && j <= 3600 && i <= 1344)
        return 8; 
      if (l <= 20736000L && k <= 552960 && j <= 7200 && i <= 2048)
        return 16; 
      if (l <= 36864000L && k <= 983040 && j <= 12000 && i <= 2752)
        return 32; 
      if (l <= 83558400L && k <= 2228224 && j <= 18000 && i <= 4160)
        return 64; 
      if (l <= 160432128L && k <= 2228224 && j <= 30000 && i <= 4160)
        return 128; 
      if (l <= 311951360L && k <= 8912896 && j <= 60000 && i <= 8384)
        return 256; 
      if (l <= 588251136L && k <= 8912896 && j <= 120000 && i <= 8384)
        return 512; 
      if (l <= 1176502272L && k <= 8912896 && j <= 180000 && i <= 8384)
        return 1024; 
      if (l <= 1176502272L && k <= 35651584 && j <= 180000 && i <= 16832)
        return 2048; 
      if (l <= 2353004544L && k <= 35651584 && j <= 240000 && i <= 16832)
        return 4096; 
      if (l <= 4706009088L && k <= 35651584 && j <= 480000 && i <= 16832)
        return 8192; 
      return 8192;
    }
    
    private void parseFromInfo(MediaFormat param1MediaFormat) {
      Range<Integer> range1;
      String str1, str4;
      Map<String, Object> map = param1MediaFormat.getMap();
      Size size1 = new Size(this.mBlockWidth, this.mBlockHeight);
      Size size2 = new Size(this.mWidthAlignment, this.mHeightAlignment);
      Range<Integer> range2 = null;
      param1MediaFormat = null;
      Size size3 = Utils.parseSize(map.get("block-size"), size1);
      Size size4 = Utils.parseSize(map.get("alignment"), size2);
      Range<Integer> range3 = Utils.parseIntRange(map.get("block-count-range"), null);
      Range<Long> range = Utils.parseLongRange(map.get("blocks-per-second-range"), null);
      this.mMeasuredFrameRates = getMeasuredFrameRates(map);
      this.mPerformancePoints = getPerformancePoints(map);
      Pair<Range<Integer>, Range<Integer>> pair = parseWidthHeightRanges(map.get("size-range"));
      if (pair != null) {
        range2 = (Range)pair.first;
        range1 = (Range)pair.second;
      } 
      if (map.containsKey("feature-can-swap-width-height")) {
        Range<Integer> range4, range5;
        if (range2 != null) {
          this.mSmallerDimensionUpperLimit = Math.min(((Integer)range2.getUpper()).intValue(), ((Integer)range1.getUpper()).intValue());
          range4 = range5 = range2.extend(range1);
        } else {
          Log.w("VideoCapabilities", "feature can-swap-width-height is best used with size-range");
          range5 = this.mWidthRange;
          this.mSmallerDimensionUpperLimit = Math.min(((Integer)range5.getUpper()).intValue(), ((Integer)this.mHeightRange.getUpper()).intValue());
          this.mHeightRange = range5 = this.mWidthRange.extend(this.mHeightRange);
          this.mWidthRange = range5;
          range4 = range2;
          range5 = range1;
        } 
        range1 = (Range<Integer>)map.get("block-aspect-ratio-range");
        Range<Rational> range6 = Utils.parseRationalRange(range1, null);
        range1 = (Range<Integer>)map.get("pixel-aspect-ratio-range");
        Range<Rational> range7 = Utils.parseRationalRange(range1, null);
        range1 = Utils.parseIntRange(map.get("frame-rate-range"), null);
        if (range1 != null)
          try {
            range1 = range2 = range1.intersect(MediaCodecInfo.FRAME_RATE_RANGE);
          } catch (IllegalArgumentException illegalArgumentException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("frame rate range (");
            stringBuilder.append(range1);
            stringBuilder.append(") is out of limits: ");
            stringBuilder.append(MediaCodecInfo.FRAME_RATE_RANGE);
            str1 = stringBuilder.toString();
            Log.w("VideoCapabilities", str1);
            str1 = null;
          }  
        range2 = Utils.parseIntRange(map.get("bitrate-range"), null);
        if (range2 != null)
          try {
            Range<Integer> range8 = range2.intersect(MediaCodecInfo.BITRATE_RANGE);
            range2 = range8;
          } catch (IllegalArgumentException illegalArgumentException) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("bitrate range (");
            stringBuilder.append(range2);
            stringBuilder.append(") is out of limits: ");
            stringBuilder.append(MediaCodecInfo.BITRATE_RANGE);
            str4 = stringBuilder.toString();
            Log.w("VideoCapabilities", str4);
            str4 = null;
          }  
        int i = size3.getWidth();
        MediaCodecInfo.checkPowerOfTwo(i, "block-size width must be power of two");
        i = size3.getHeight();
        MediaCodecInfo.checkPowerOfTwo(i, "block-size height must be power of two");
        i = size4.getWidth();
        MediaCodecInfo.checkPowerOfTwo(i, "alignment width must be power of two");
        i = size4.getHeight();
        MediaCodecInfo.checkPowerOfTwo(i, "alignment height must be power of two");
        i = size3.getWidth();
        int j = size3.getHeight();
        int k = size4.getWidth(), m = size4.getHeight();
        applyMacroBlockLimits(2147483647, 2147483647, 2147483647, Long.MAX_VALUE, i, j, k, m);
        if ((this.mParent.mError & 0x2) != 0 || this.mAllowMbOverride) {
          if (range4 != null)
            this.mWidthRange = MediaCodecInfo.SIZE_RANGE.intersect(range4); 
          if (range5 != null)
            this.mHeightRange = MediaCodecInfo.SIZE_RANGE.intersect(range5); 
          if (range3 != null) {
            range5 = MediaCodecInfo.POSITIVE_INTEGERS;
            k = this.mBlockWidth;
            i = this.mBlockHeight;
            i = k * i / size3.getWidth() / size3.getHeight();
            range4 = Utils.factorRange(range3, i);
            this.mBlockCountRange = range5.intersect(range4);
          } 
          if (range != null) {
            range5 = (Range)MediaCodecInfo.POSITIVE_LONGS;
            k = this.mBlockWidth;
            i = this.mBlockHeight;
            long l = (k * i / size3.getWidth() / size3.getHeight());
            range4 = (Range)Utils.factorRange(range, l);
            this.mBlocksPerSecondRange = range5.intersect(range4);
          } 
          if (range7 != null) {
            range5 = (Range)MediaCodecInfo.POSITIVE_RATIONALS;
            i = this.mBlockHeight;
            i /= size3.getHeight();
            k = this.mBlockWidth;
            k /= size3.getWidth();
            range4 = (Range)Utils.scaleRange(range7, i, k);
            this.mBlockAspectRatioRange = range5.intersect(range4);
          } 
          if (range6 != null)
            this.mAspectRatioRange = MediaCodecInfo.POSITIVE_RATIONALS.intersect(range6); 
          if (str1 != null)
            this.mFrameRateRange = MediaCodecInfo.FRAME_RATE_RANGE.intersect((Range)str1); 
          if (str4 != null)
            if ((this.mParent.mError & 0x2) != 0) {
              this.mBitrateRange = MediaCodecInfo.BITRATE_RANGE.intersect((Range)str4);
            } else {
              this.mBitrateRange = this.mBitrateRange.intersect((Range)str4);
            }  
        } else {
          if (range4 != null)
            this.mWidthRange = this.mWidthRange.intersect(range4); 
          if (range5 != null)
            this.mHeightRange = this.mHeightRange.intersect(range5); 
          if (range3 != null) {
            range5 = this.mBlockCountRange;
            k = this.mBlockWidth;
            i = this.mBlockHeight;
            i = k * i / size3.getWidth() / size3.getHeight();
            range4 = Utils.factorRange(range3, i);
            this.mBlockCountRange = range5.intersect(range4);
          } 
          if (range != null) {
            Range<Long> range8 = this.mBlocksPerSecondRange;
            i = this.mBlockWidth;
            k = this.mBlockHeight;
            long l = (i * k / size3.getWidth() / size3.getHeight());
            range4 = (Range)Utils.factorRange(range, l);
            this.mBlocksPerSecondRange = range8.intersect(range4);
          } 
          if (range7 != null) {
            Range<Rational> range8 = this.mBlockAspectRatioRange;
            i = this.mBlockHeight;
            i /= size3.getHeight();
            k = this.mBlockWidth;
            k /= size3.getWidth();
            range4 = (Range)Utils.scaleRange(range7, i, k);
            this.mBlockAspectRatioRange = range8.intersect(range4);
          } 
          if (range6 != null)
            this.mAspectRatioRange = this.mAspectRatioRange.intersect(range6); 
          if (str1 != null)
            this.mFrameRateRange = this.mFrameRateRange.intersect((Range)str1); 
          if (str4 != null)
            this.mBitrateRange = this.mBitrateRange.intersect((Range)str4); 
        } 
        updateLimits();
        return;
      } 
      String str2 = str4, str3 = str1;
    }
    
    private void applyBlockLimits(int param1Int1, int param1Int2, Range<Integer> param1Range, Range<Long> param1Range1, Range<Rational> param1Range2) {
      MediaCodecInfo.checkPowerOfTwo(param1Int1, "blockWidth must be a power of two");
      MediaCodecInfo.checkPowerOfTwo(param1Int2, "blockHeight must be a power of two");
      int i = Math.max(param1Int1, this.mBlockWidth);
      int j = Math.max(param1Int2, this.mBlockHeight);
      int k = i * j / this.mBlockWidth / this.mBlockHeight;
      if (k != 1) {
        this.mBlockCountRange = Utils.factorRange(this.mBlockCountRange, k);
        this.mBlocksPerSecondRange = Utils.factorRange(this.mBlocksPerSecondRange, k);
        this.mBlockAspectRatioRange = Utils.scaleRange(this.mBlockAspectRatioRange, j / this.mBlockHeight, i / this.mBlockWidth);
        this.mHorizontalBlockRange = Utils.factorRange(this.mHorizontalBlockRange, i / this.mBlockWidth);
        this.mVerticalBlockRange = Utils.factorRange(this.mVerticalBlockRange, j / this.mBlockHeight);
      } 
      k = i * j / param1Int1 / param1Int2;
      Range<Integer> range = param1Range;
      Range<Long> range1 = param1Range1;
      Range<Rational> range2 = param1Range2;
      if (k != 1) {
        range = Utils.factorRange(param1Range, k);
        range1 = Utils.factorRange(param1Range1, k);
        range2 = Utils.scaleRange(param1Range2, j / param1Int2, i / param1Int1);
      } 
      this.mBlockCountRange = this.mBlockCountRange.intersect(range);
      this.mBlocksPerSecondRange = this.mBlocksPerSecondRange.intersect(range1);
      this.mBlockAspectRatioRange = this.mBlockAspectRatioRange.intersect(range2);
      this.mBlockWidth = i;
      this.mBlockHeight = j;
    }
    
    private void applyAlignment(int param1Int1, int param1Int2) {
      MediaCodecInfo.checkPowerOfTwo(param1Int1, "widthAlignment must be a power of two");
      MediaCodecInfo.checkPowerOfTwo(param1Int2, "heightAlignment must be a power of two");
      if (param1Int1 > this.mBlockWidth || param1Int2 > this.mBlockHeight) {
        int i = this.mBlockWidth;
        i = Math.max(param1Int1, i);
        int j = this.mBlockHeight;
        j = Math.max(param1Int2, j);
        Range<Integer> range = MediaCodecInfo.POSITIVE_INTEGERS;
        Range<Long> range1 = MediaCodecInfo.POSITIVE_LONGS;
        Range<Rational> range2 = MediaCodecInfo.POSITIVE_RATIONALS;
        applyBlockLimits(i, j, range, range1, range2);
      } 
      this.mWidthAlignment = Math.max(param1Int1, this.mWidthAlignment);
      this.mHeightAlignment = Math.max(param1Int2, this.mHeightAlignment);
      this.mWidthRange = Utils.alignRange(this.mWidthRange, this.mWidthAlignment);
      this.mHeightRange = Utils.alignRange(this.mHeightRange, this.mHeightAlignment);
    }
    
    private void updateLimits() {
      Range<Integer> range7 = this.mHorizontalBlockRange, range12 = this.mWidthRange;
      int i = this.mBlockWidth;
      range12 = Utils.factorRange(range12, i);
      this.mHorizontalBlockRange = range7 = range7.intersect(range12);
      range12 = this.mBlockCountRange;
      int j = ((Integer)range12.getLower()).intValue() / ((Integer)this.mVerticalBlockRange.getUpper()).intValue();
      range12 = this.mBlockCountRange;
      i = ((Integer)range12.getUpper()).intValue() / ((Integer)this.mVerticalBlockRange.getLower()).intValue();
      range12 = Range.create(Integer.valueOf(j), Integer.valueOf(i));
      this.mHorizontalBlockRange = range7.intersect(range12);
      range7 = this.mVerticalBlockRange;
      range12 = this.mHeightRange;
      i = this.mBlockHeight;
      range12 = Utils.factorRange(range12, i);
      this.mVerticalBlockRange = range7 = range7.intersect(range12);
      range12 = this.mBlockCountRange;
      j = ((Integer)range12.getLower()).intValue() / ((Integer)this.mHorizontalBlockRange.getUpper()).intValue();
      range12 = this.mBlockCountRange;
      i = ((Integer)range12.getUpper()).intValue() / ((Integer)this.mHorizontalBlockRange.getLower()).intValue();
      range12 = Range.create(Integer.valueOf(j), Integer.valueOf(i));
      this.mVerticalBlockRange = range7.intersect(range12);
      range7 = this.mBlockCountRange;
      range12 = this.mHorizontalBlockRange;
      int k = ((Integer)range12.getLower()).intValue();
      range12 = this.mVerticalBlockRange;
      i = ((Integer)range12.getLower()).intValue();
      range12 = this.mHorizontalBlockRange;
      j = ((Integer)range12.getUpper()).intValue();
      range12 = this.mVerticalBlockRange;
      int m = ((Integer)range12.getUpper()).intValue();
      range12 = Range.create(Integer.valueOf(k * i), Integer.valueOf(j * m));
      this.mBlockCountRange = range7.intersect(range12);
      Range<Rational> range6 = this.mBlockAspectRatioRange;
      range12 = this.mHorizontalBlockRange;
      Rational rational2 = new Rational(((Integer)range12.getLower()).intValue(), ((Integer)this.mVerticalBlockRange.getUpper()).intValue());
      Range<Integer> range14 = this.mHorizontalBlockRange;
      Rational rational4 = new Rational(((Integer)range14.getUpper()).intValue(), ((Integer)this.mVerticalBlockRange.getLower()).intValue());
      this.mBlockAspectRatioRange = range6.intersect((Comparable)rational2, (Comparable)rational4);
      Range<Integer> range5 = this.mWidthRange, range11 = this.mHorizontalBlockRange;
      j = ((Integer)range11.getLower()).intValue();
      m = this.mBlockWidth;
      k = this.mWidthAlignment;
      range11 = this.mHorizontalBlockRange;
      int n = ((Integer)range11.getUpper()).intValue();
      i = this.mBlockWidth;
      this.mWidthRange = range5.intersect(Integer.valueOf((j - 1) * m + k), Integer.valueOf(n * i));
      range5 = this.mHeightRange;
      range11 = this.mVerticalBlockRange;
      m = ((Integer)range11.getLower()).intValue();
      k = this.mBlockHeight;
      n = this.mHeightAlignment;
      range11 = this.mVerticalBlockRange;
      j = ((Integer)range11.getUpper()).intValue();
      i = this.mBlockHeight;
      this.mHeightRange = range5.intersect(Integer.valueOf((m - 1) * k + n), Integer.valueOf(j * i));
      Range<Rational> range4 = this.mAspectRatioRange;
      range11 = this.mWidthRange;
      Rational rational1 = new Rational(((Integer)range11.getLower()).intValue(), ((Integer)this.mHeightRange.getUpper()).intValue());
      Range<Integer> range13 = this.mWidthRange;
      Rational rational3 = new Rational(((Integer)range13.getUpper()).intValue(), ((Integer)this.mHeightRange.getLower()).intValue());
      this.mAspectRatioRange = range4.intersect((Comparable)rational1, (Comparable)rational3);
      i = this.mSmallerDimensionUpperLimit;
      Range<Integer> range3 = this.mWidthRange;
      j = Math.min(((Integer)range3.getUpper()).intValue(), ((Integer)this.mHeightRange.getUpper()).intValue());
      this.mSmallerDimensionUpperLimit = Math.min(i, j);
      Range<Long> range2 = this.mBlocksPerSecondRange;
      Range<Integer> range10 = this.mBlockCountRange;
      long l1 = ((Integer)range10.getLower()).intValue(), l2 = ((Integer)this.mFrameRateRange.getLower()).intValue();
      range10 = this.mBlockCountRange;
      long l3 = ((Integer)range10.getUpper()).intValue(), l4 = ((Integer)this.mFrameRateRange.getUpper()).intValue();
      this.mBlocksPerSecondRange = (Range)(range10 = range2.intersect(Long.valueOf(l1 * l2), Long.valueOf(l3 * l4)));
      Range<Integer> range1 = this.mFrameRateRange;
      l2 = ((Long)range10.getLower()).longValue();
      range10 = this.mBlockCountRange;
      i = (int)(l2 / ((Integer)range10.getUpper()).intValue());
      Range<Long> range9 = this.mBlocksPerSecondRange;
      double d = ((Long)range9.getUpper()).longValue();
      Range<Integer> range8 = this.mBlockCountRange;
      j = (int)(d / ((Integer)range8.getLower()).intValue());
      this.mFrameRateRange = range1.intersect(Integer.valueOf(i), Integer.valueOf(j));
    }
    
    private void applyMacroBlockLimits(int param1Int1, int param1Int2, int param1Int3, long param1Long, int param1Int4, int param1Int5, int param1Int6, int param1Int7) {
      applyMacroBlockLimits(1, 1, param1Int1, param1Int2, param1Int3, param1Long, param1Int4, param1Int5, param1Int6, param1Int7);
    }
    
    private void applyMacroBlockLimits(int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, long param1Long, int param1Int6, int param1Int7, int param1Int8, int param1Int9) {
      applyAlignment(param1Int8, param1Int9);
      Range<Integer> range = Range.create(Integer.valueOf(1), Integer.valueOf(param1Int5));
      Range<Long> range1 = Range.create(Long.valueOf(1L), Long.valueOf(param1Long));
      Rational rational1 = new Rational(1, param1Int4), rational2 = new Rational(param1Int3, 1);
      Range<Rational> range2 = Range.create((Comparable)rational1, (Comparable)rational2);
      applyBlockLimits(param1Int6, param1Int7, range, range1, range2);
      range = this.mHorizontalBlockRange;
      param1Int5 = this.mBlockWidth / param1Int6;
      param1Int1 = Utils.divUp(param1Int1, param1Int5);
      param1Int3 /= this.mBlockWidth / param1Int6;
      this.mHorizontalBlockRange = range.intersect(Integer.valueOf(param1Int1), Integer.valueOf(param1Int3));
      range = this.mVerticalBlockRange;
      param1Int1 = this.mBlockHeight / param1Int7;
      param1Int1 = Utils.divUp(param1Int2, param1Int1);
      param1Int2 = param1Int4 / this.mBlockHeight / param1Int7;
      this.mVerticalBlockRange = range.intersect(Integer.valueOf(param1Int1), Integer.valueOf(param1Int2));
    }
    
    private void applyLevelLimits() {
      // Byte code:
      //   0: aload_0
      //   1: getfield mParent : Landroid/media/MediaCodecInfo$CodecCapabilities;
      //   4: getfield profileLevels : [Landroid/media/MediaCodecInfo$CodecProfileLevel;
      //   7: astore_1
      //   8: aload_0
      //   9: getfield mParent : Landroid/media/MediaCodecInfo$CodecCapabilities;
      //   12: invokevirtual getMimeType : ()Ljava/lang/String;
      //   15: astore_2
      //   16: aload_2
      //   17: ldc 'video/avc'
      //   19: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   22: istore_3
      //   23: ldc 'Unsupported profile '
      //   25: astore #4
      //   27: ldc 'Unrecognized level '
      //   29: astore #5
      //   31: ldc 'Unrecognized profile '
      //   33: astore #6
      //   35: ldc ' for '
      //   37: astore #7
      //   39: ldc 'VideoCapabilities'
      //   41: astore #8
      //   43: iconst_1
      //   44: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   47: astore #9
      //   49: iload_3
      //   50: ifeq -> 1145
      //   53: aload_1
      //   54: arraylength
      //   55: istore #10
      //   57: bipush #99
      //   59: istore #11
      //   61: ldc 64000
      //   63: istore #12
      //   65: iconst_4
      //   66: istore #13
      //   68: sipush #396
      //   71: istore #14
      //   73: ldc2_w 1485
      //   76: lstore #15
      //   78: iconst_0
      //   79: istore #17
      //   81: aload #5
      //   83: astore #8
      //   85: iload #17
      //   87: iload #10
      //   89: if_icmpge -> 1101
      //   92: aload_1
      //   93: iload #17
      //   95: aaload
      //   96: astore #6
      //   98: iconst_1
      //   99: istore #18
      //   101: iconst_1
      //   102: istore #19
      //   104: aload #6
      //   106: getfield level : I
      //   109: istore #20
      //   111: iload #20
      //   113: iconst_1
      //   114: if_icmpeq -> 757
      //   117: iload #20
      //   119: iconst_2
      //   120: if_icmpeq -> 735
      //   123: iload #20
      //   125: lookupswitch default -> 280, 4 -> 712, 8 -> 689, 16 -> 666, 32 -> 643, 64 -> 620, 128 -> 597, 256 -> 575, 512 -> 553, 1024 -> 531, 2048 -> 510, 4096 -> 490, 8192 -> 470, 16384 -> 450, 32768 -> 431, 65536 -> 412, 131072 -> 393, 262144 -> 374, 524288 -> 355
      //   280: new java/lang/StringBuilder
      //   283: dup
      //   284: invokespecial <init> : ()V
      //   287: astore #5
      //   289: aload #5
      //   291: aload #8
      //   293: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   296: pop
      //   297: aload #5
      //   299: aload #6
      //   301: getfield level : I
      //   304: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   307: pop
      //   308: aload #5
      //   310: ldc ' for '
      //   312: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   315: pop
      //   316: aload #5
      //   318: aload_2
      //   319: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   322: pop
      //   323: ldc 'VideoCapabilities'
      //   325: aload #5
      //   327: invokevirtual toString : ()Ljava/lang/String;
      //   330: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   333: pop
      //   334: iload #13
      //   336: iconst_1
      //   337: ior
      //   338: istore #13
      //   340: iconst_0
      //   341: istore #21
      //   343: iconst_0
      //   344: istore #22
      //   346: iconst_0
      //   347: istore #20
      //   349: iconst_0
      //   350: istore #23
      //   352: goto -> 775
      //   355: ldc 16711680
      //   357: istore #21
      //   359: ldc 139264
      //   361: istore #22
      //   363: ldc 800000
      //   365: istore #20
      //   367: ldc 696320
      //   369: istore #23
      //   371: goto -> 775
      //   374: ldc 8355840
      //   376: istore #21
      //   378: ldc 139264
      //   380: istore #22
      //   382: ldc 480000
      //   384: istore #20
      //   386: ldc 696320
      //   388: istore #23
      //   390: goto -> 775
      //   393: ldc 4177920
      //   395: istore #21
      //   397: ldc 139264
      //   399: istore #22
      //   401: ldc 240000
      //   403: istore #20
      //   405: ldc 696320
      //   407: istore #23
      //   409: goto -> 775
      //   412: ldc 2073600
      //   414: istore #21
      //   416: ldc 36864
      //   418: istore #22
      //   420: ldc 240000
      //   422: istore #20
      //   424: ldc 184320
      //   426: istore #23
      //   428: goto -> 775
      //   431: ldc 983040
      //   433: istore #21
      //   435: ldc 36864
      //   437: istore #22
      //   439: ldc 240000
      //   441: istore #20
      //   443: ldc 184320
      //   445: istore #23
      //   447: goto -> 775
      //   450: ldc 589824
      //   452: istore #21
      //   454: sipush #22080
      //   457: istore #22
      //   459: ldc 135000
      //   461: istore #20
      //   463: ldc 110400
      //   465: istore #23
      //   467: goto -> 775
      //   470: ldc 522240
      //   472: istore #21
      //   474: sipush #8704
      //   477: istore #22
      //   479: ldc 50000
      //   481: istore #20
      //   483: ldc 34816
      //   485: istore #23
      //   487: goto -> 775
      //   490: ldc 245760
      //   492: istore #21
      //   494: sipush #8192
      //   497: istore #22
      //   499: ldc 50000
      //   501: istore #20
      //   503: ldc 32768
      //   505: istore #23
      //   507: goto -> 775
      //   510: ldc 245760
      //   512: istore #21
      //   514: sipush #8192
      //   517: istore #22
      //   519: sipush #20000
      //   522: istore #20
      //   524: ldc 32768
      //   526: istore #23
      //   528: goto -> 775
      //   531: ldc 216000
      //   533: istore #21
      //   535: sipush #5120
      //   538: istore #22
      //   540: sipush #20000
      //   543: istore #20
      //   545: sipush #20480
      //   548: istore #23
      //   550: goto -> 775
      //   553: ldc 108000
      //   555: istore #21
      //   557: sipush #3600
      //   560: istore #22
      //   562: sipush #14000
      //   565: istore #20
      //   567: sipush #18000
      //   570: istore #23
      //   572: goto -> 775
      //   575: ldc 40500
      //   577: istore #21
      //   579: sipush #1620
      //   582: istore #22
      //   584: sipush #10000
      //   587: istore #20
      //   589: sipush #8100
      //   592: istore #23
      //   594: goto -> 775
      //   597: sipush #20250
      //   600: istore #21
      //   602: sipush #1620
      //   605: istore #22
      //   607: sipush #4000
      //   610: istore #20
      //   612: sipush #8100
      //   615: istore #23
      //   617: goto -> 775
      //   620: sipush #19800
      //   623: istore #21
      //   625: sipush #792
      //   628: istore #22
      //   630: sipush #4000
      //   633: istore #20
      //   635: sipush #4752
      //   638: istore #23
      //   640: goto -> 775
      //   643: sipush #11880
      //   646: istore #21
      //   648: sipush #396
      //   651: istore #22
      //   653: sipush #2000
      //   656: istore #20
      //   658: sipush #2376
      //   661: istore #23
      //   663: goto -> 775
      //   666: sipush #11880
      //   669: istore #21
      //   671: sipush #396
      //   674: istore #22
      //   676: sipush #768
      //   679: istore #20
      //   681: sipush #2376
      //   684: istore #23
      //   686: goto -> 775
      //   689: sipush #6000
      //   692: istore #21
      //   694: sipush #396
      //   697: istore #22
      //   699: sipush #384
      //   702: istore #20
      //   704: sipush #2376
      //   707: istore #23
      //   709: goto -> 775
      //   712: sipush #3000
      //   715: istore #21
      //   717: sipush #396
      //   720: istore #22
      //   722: sipush #192
      //   725: istore #20
      //   727: sipush #900
      //   730: istore #23
      //   732: goto -> 775
      //   735: sipush #1485
      //   738: istore #21
      //   740: bipush #99
      //   742: istore #22
      //   744: sipush #128
      //   747: istore #20
      //   749: sipush #396
      //   752: istore #23
      //   754: goto -> 775
      //   757: sipush #1485
      //   760: istore #21
      //   762: bipush #99
      //   764: istore #22
      //   766: bipush #64
      //   768: istore #20
      //   770: sipush #396
      //   773: istore #23
      //   775: aload #6
      //   777: getfield profile : I
      //   780: istore #24
      //   782: iload #13
      //   784: istore #25
      //   786: iload #19
      //   788: istore #26
      //   790: iload #24
      //   792: iconst_1
      //   793: if_icmpeq -> 1022
      //   796: iload #13
      //   798: istore #25
      //   800: iload #19
      //   802: istore #26
      //   804: iload #24
      //   806: iconst_2
      //   807: if_icmpeq -> 1022
      //   810: iload #24
      //   812: iconst_4
      //   813: if_icmpeq -> 959
      //   816: iload #24
      //   818: bipush #8
      //   820: if_icmpeq -> 948
      //   823: iload #24
      //   825: bipush #16
      //   827: if_icmpeq -> 937
      //   830: iload #24
      //   832: bipush #32
      //   834: if_icmpeq -> 959
      //   837: iload #24
      //   839: bipush #64
      //   841: if_icmpeq -> 959
      //   844: iload #13
      //   846: istore #25
      //   848: iload #19
      //   850: istore #26
      //   852: iload #24
      //   854: ldc 65536
      //   856: if_icmpeq -> 1022
      //   859: iload #24
      //   861: ldc 524288
      //   863: if_icmpeq -> 948
      //   866: new java/lang/StringBuilder
      //   869: dup
      //   870: invokespecial <init> : ()V
      //   873: astore #5
      //   875: aload #5
      //   877: ldc 'Unrecognized profile '
      //   879: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   882: pop
      //   883: aload #5
      //   885: aload #6
      //   887: getfield profile : I
      //   890: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   893: pop
      //   894: aload #5
      //   896: ldc ' for '
      //   898: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   901: pop
      //   902: aload #5
      //   904: aload_2
      //   905: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   908: pop
      //   909: ldc 'VideoCapabilities'
      //   911: aload #5
      //   913: invokevirtual toString : ()Ljava/lang/String;
      //   916: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   919: pop
      //   920: iload #13
      //   922: iconst_1
      //   923: ior
      //   924: istore #13
      //   926: iload #20
      //   928: sipush #1000
      //   931: imul
      //   932: istore #20
      //   934: goto -> 1038
      //   937: iload #20
      //   939: sipush #3000
      //   942: imul
      //   943: istore #20
      //   945: goto -> 1038
      //   948: iload #20
      //   950: sipush #1250
      //   953: imul
      //   954: istore #20
      //   956: goto -> 1038
      //   959: new java/lang/StringBuilder
      //   962: dup
      //   963: invokespecial <init> : ()V
      //   966: astore #5
      //   968: aload #5
      //   970: ldc 'Unsupported profile '
      //   972: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   975: pop
      //   976: aload #5
      //   978: aload #6
      //   980: getfield profile : I
      //   983: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   986: pop
      //   987: aload #5
      //   989: ldc ' for '
      //   991: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   994: pop
      //   995: aload #5
      //   997: aload_2
      //   998: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1001: pop
      //   1002: ldc 'VideoCapabilities'
      //   1004: aload #5
      //   1006: invokevirtual toString : ()Ljava/lang/String;
      //   1009: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1012: pop
      //   1013: iload #13
      //   1015: iconst_2
      //   1016: ior
      //   1017: istore #25
      //   1019: iconst_0
      //   1020: istore #26
      //   1022: iload #20
      //   1024: sipush #1000
      //   1027: imul
      //   1028: istore #20
      //   1030: iload #26
      //   1032: istore #18
      //   1034: iload #25
      //   1036: istore #13
      //   1038: iload #13
      //   1040: istore #26
      //   1042: iload #18
      //   1044: ifeq -> 1054
      //   1047: iload #13
      //   1049: bipush #-5
      //   1051: iand
      //   1052: istore #26
      //   1054: iload #21
      //   1056: i2l
      //   1057: lload #15
      //   1059: invokestatic max : (JJ)J
      //   1062: lstore #15
      //   1064: iload #22
      //   1066: iload #11
      //   1068: invokestatic max : (II)I
      //   1071: istore #11
      //   1073: iload #20
      //   1075: iload #12
      //   1077: invokestatic max : (II)I
      //   1080: istore #12
      //   1082: iload #14
      //   1084: iload #23
      //   1086: invokestatic max : (II)I
      //   1089: istore #14
      //   1091: iinc #17, 1
      //   1094: iload #26
      //   1096: istore #13
      //   1098: goto -> 85
      //   1101: iload #11
      //   1103: bipush #8
      //   1105: imul
      //   1106: i2d
      //   1107: invokestatic sqrt : (D)D
      //   1110: d2i
      //   1111: istore #20
      //   1113: aload_0
      //   1114: iload #20
      //   1116: iload #20
      //   1118: iload #11
      //   1120: lload #15
      //   1122: bipush #16
      //   1124: bipush #16
      //   1126: iconst_1
      //   1127: iconst_1
      //   1128: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   1131: iload #12
      //   1133: istore #20
      //   1135: aload #9
      //   1137: astore_1
      //   1138: iload #13
      //   1140: istore #21
      //   1142: goto -> 7114
      //   1145: ldc 'Unrecognized level '
      //   1147: astore #5
      //   1149: aload_2
      //   1150: ldc 'video/mpeg2'
      //   1152: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   1155: istore_3
      //   1156: ldc '/'
      //   1158: astore #27
      //   1160: ldc 'Unrecognized profile/level '
      //   1162: astore #28
      //   1164: iload_3
      //   1165: ifeq -> 1986
      //   1168: aload_1
      //   1169: arraylength
      //   1170: istore #18
      //   1172: iconst_0
      //   1173: istore #14
      //   1175: iconst_4
      //   1176: istore #26
      //   1178: bipush #99
      //   1180: istore #19
      //   1182: bipush #9
      //   1184: istore #17
      //   1186: ldc 64000
      //   1188: istore #25
      //   1190: bipush #11
      //   1192: istore #24
      //   1194: bipush #15
      //   1196: istore #11
      //   1198: ldc2_w 1485
      //   1201: lstore #15
      //   1203: aload #28
      //   1205: astore #5
      //   1207: aload #4
      //   1209: astore #7
      //   1211: iload #14
      //   1213: iload #18
      //   1215: if_icmpge -> 1933
      //   1218: aload_1
      //   1219: iload #14
      //   1221: aaload
      //   1222: astore #4
      //   1224: iconst_1
      //   1225: istore #29
      //   1227: aload #4
      //   1229: getfield profile : I
      //   1232: istore #20
      //   1234: iload #20
      //   1236: ifeq -> 1713
      //   1239: iload #20
      //   1241: iconst_1
      //   1242: if_icmpeq -> 1434
      //   1245: iload #20
      //   1247: iconst_2
      //   1248: if_icmpeq -> 1350
      //   1251: iload #20
      //   1253: iconst_3
      //   1254: if_icmpeq -> 1350
      //   1257: iload #20
      //   1259: iconst_4
      //   1260: if_icmpeq -> 1350
      //   1263: iload #20
      //   1265: iconst_5
      //   1266: if_icmpeq -> 1350
      //   1269: new java/lang/StringBuilder
      //   1272: dup
      //   1273: invokespecial <init> : ()V
      //   1276: astore #28
      //   1278: aload #28
      //   1280: aload #6
      //   1282: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1285: pop
      //   1286: aload #28
      //   1288: aload #4
      //   1290: getfield profile : I
      //   1293: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1296: pop
      //   1297: aload #28
      //   1299: ldc ' for '
      //   1301: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1304: pop
      //   1305: aload #28
      //   1307: aload_2
      //   1308: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1311: pop
      //   1312: aload #8
      //   1314: aload #28
      //   1316: invokevirtual toString : ()Ljava/lang/String;
      //   1319: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1322: pop
      //   1323: iload #26
      //   1325: iconst_1
      //   1326: ior
      //   1327: istore #26
      //   1329: iconst_0
      //   1330: istore #21
      //   1332: iconst_0
      //   1333: istore #12
      //   1335: iconst_0
      //   1336: istore #20
      //   1338: iconst_0
      //   1339: istore #22
      //   1341: iconst_0
      //   1342: istore #23
      //   1344: iconst_0
      //   1345: istore #13
      //   1347: goto -> 1848
      //   1350: new java/lang/StringBuilder
      //   1353: dup
      //   1354: invokespecial <init> : ()V
      //   1357: astore #28
      //   1359: aload #28
      //   1361: aload #7
      //   1363: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1366: pop
      //   1367: aload #28
      //   1369: aload #4
      //   1371: getfield profile : I
      //   1374: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1377: pop
      //   1378: aload #28
      //   1380: ldc ' for '
      //   1382: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1385: pop
      //   1386: aload #28
      //   1388: aload_2
      //   1389: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1392: pop
      //   1393: aload #8
      //   1395: aload #28
      //   1397: invokevirtual toString : ()Ljava/lang/String;
      //   1400: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   1403: pop
      //   1404: iload #26
      //   1406: iconst_2
      //   1407: ior
      //   1408: istore #26
      //   1410: iconst_0
      //   1411: istore #29
      //   1413: iconst_0
      //   1414: istore #21
      //   1416: iconst_0
      //   1417: istore #12
      //   1419: iconst_0
      //   1420: istore #20
      //   1422: iconst_0
      //   1423: istore #22
      //   1425: iconst_0
      //   1426: istore #23
      //   1428: iconst_0
      //   1429: istore #13
      //   1431: goto -> 1848
      //   1434: aload #4
      //   1436: getfield level : I
      //   1439: istore #20
      //   1441: iload #20
      //   1443: ifeq -> 1683
      //   1446: iload #20
      //   1448: iconst_1
      //   1449: if_icmpeq -> 1654
      //   1452: iload #20
      //   1454: iconst_2
      //   1455: if_icmpeq -> 1626
      //   1458: iload #20
      //   1460: iconst_3
      //   1461: if_icmpeq -> 1598
      //   1464: iload #20
      //   1466: iconst_4
      //   1467: if_icmpeq -> 1570
      //   1470: new java/lang/StringBuilder
      //   1473: dup
      //   1474: invokespecial <init> : ()V
      //   1477: astore #28
      //   1479: aload #28
      //   1481: aload #5
      //   1483: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1486: pop
      //   1487: aload #28
      //   1489: aload #4
      //   1491: getfield profile : I
      //   1494: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1497: pop
      //   1498: aload #28
      //   1500: aload #27
      //   1502: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1505: pop
      //   1506: aload #28
      //   1508: aload #4
      //   1510: getfield level : I
      //   1513: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1516: pop
      //   1517: aload #28
      //   1519: ldc ' for '
      //   1521: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1524: pop
      //   1525: aload #28
      //   1527: aload_2
      //   1528: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1531: pop
      //   1532: aload #8
      //   1534: aload #28
      //   1536: invokevirtual toString : ()Ljava/lang/String;
      //   1539: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1542: pop
      //   1543: iload #26
      //   1545: iconst_1
      //   1546: ior
      //   1547: istore #26
      //   1549: iconst_0
      //   1550: istore #21
      //   1552: iconst_0
      //   1553: istore #12
      //   1555: iconst_0
      //   1556: istore #20
      //   1558: iconst_0
      //   1559: istore #22
      //   1561: iconst_0
      //   1562: istore #23
      //   1564: iconst_0
      //   1565: istore #13
      //   1567: goto -> 1848
      //   1570: ldc 489600
      //   1572: istore #21
      //   1574: sipush #8160
      //   1577: istore #12
      //   1579: ldc 80000
      //   1581: istore #20
      //   1583: bipush #60
      //   1585: istore #22
      //   1587: bipush #120
      //   1589: istore #23
      //   1591: bipush #68
      //   1593: istore #13
      //   1595: goto -> 1848
      //   1598: ldc 244800
      //   1600: istore #21
      //   1602: sipush #8160
      //   1605: istore #12
      //   1607: ldc 80000
      //   1609: istore #20
      //   1611: bipush #60
      //   1613: istore #22
      //   1615: bipush #120
      //   1617: istore #23
      //   1619: bipush #68
      //   1621: istore #13
      //   1623: goto -> 1848
      //   1626: ldc 183600
      //   1628: istore #21
      //   1630: sipush #6120
      //   1633: istore #12
      //   1635: ldc 60000
      //   1637: istore #20
      //   1639: bipush #60
      //   1641: istore #22
      //   1643: bipush #90
      //   1645: istore #23
      //   1647: bipush #68
      //   1649: istore #13
      //   1651: goto -> 1848
      //   1654: ldc 40500
      //   1656: istore #21
      //   1658: sipush #1620
      //   1661: istore #12
      //   1663: sipush #15000
      //   1666: istore #20
      //   1668: bipush #30
      //   1670: istore #22
      //   1672: bipush #45
      //   1674: istore #23
      //   1676: bipush #36
      //   1678: istore #13
      //   1680: goto -> 1848
      //   1683: sipush #11880
      //   1686: istore #21
      //   1688: sipush #396
      //   1691: istore #12
      //   1693: sipush #4000
      //   1696: istore #20
      //   1698: bipush #30
      //   1700: istore #22
      //   1702: bipush #22
      //   1704: istore #23
      //   1706: bipush #18
      //   1708: istore #13
      //   1710: goto -> 1848
      //   1713: aload #4
      //   1715: getfield level : I
      //   1718: iconst_1
      //   1719: if_icmpeq -> 1822
      //   1722: new java/lang/StringBuilder
      //   1725: dup
      //   1726: invokespecial <init> : ()V
      //   1729: astore #28
      //   1731: aload #28
      //   1733: aload #5
      //   1735: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1738: pop
      //   1739: aload #28
      //   1741: aload #4
      //   1743: getfield profile : I
      //   1746: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1749: pop
      //   1750: aload #28
      //   1752: aload #27
      //   1754: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1757: pop
      //   1758: aload #28
      //   1760: aload #4
      //   1762: getfield level : I
      //   1765: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   1768: pop
      //   1769: aload #28
      //   1771: ldc ' for '
      //   1773: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1776: pop
      //   1777: aload #28
      //   1779: aload_2
      //   1780: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   1783: pop
      //   1784: aload #8
      //   1786: aload #28
      //   1788: invokevirtual toString : ()Ljava/lang/String;
      //   1791: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   1794: pop
      //   1795: iload #26
      //   1797: iconst_1
      //   1798: ior
      //   1799: istore #26
      //   1801: iconst_0
      //   1802: istore #21
      //   1804: iconst_0
      //   1805: istore #12
      //   1807: iconst_0
      //   1808: istore #20
      //   1810: iconst_0
      //   1811: istore #22
      //   1813: iconst_0
      //   1814: istore #23
      //   1816: iconst_0
      //   1817: istore #13
      //   1819: goto -> 1848
      //   1822: ldc 40500
      //   1824: istore #21
      //   1826: sipush #1620
      //   1829: istore #12
      //   1831: sipush #15000
      //   1834: istore #20
      //   1836: bipush #30
      //   1838: istore #22
      //   1840: bipush #45
      //   1842: istore #23
      //   1844: bipush #36
      //   1846: istore #13
      //   1848: iload #26
      //   1850: istore #10
      //   1852: iload #29
      //   1854: ifeq -> 1864
      //   1857: iload #26
      //   1859: bipush #-5
      //   1861: iand
      //   1862: istore #10
      //   1864: iload #21
      //   1866: i2l
      //   1867: lload #15
      //   1869: invokestatic max : (JJ)J
      //   1872: lstore #15
      //   1874: iload #12
      //   1876: iload #19
      //   1878: invokestatic max : (II)I
      //   1881: istore #19
      //   1883: iload #20
      //   1885: sipush #1000
      //   1888: imul
      //   1889: iload #25
      //   1891: invokestatic max : (II)I
      //   1894: istore #25
      //   1896: iload #23
      //   1898: iload #24
      //   1900: invokestatic max : (II)I
      //   1903: istore #24
      //   1905: iload #13
      //   1907: iload #17
      //   1909: invokestatic max : (II)I
      //   1912: istore #17
      //   1914: iload #22
      //   1916: iload #11
      //   1918: invokestatic max : (II)I
      //   1921: istore #11
      //   1923: iinc #14, 1
      //   1926: iload #10
      //   1928: istore #26
      //   1930: goto -> 1211
      //   1933: aload_0
      //   1934: iload #24
      //   1936: iload #17
      //   1938: iload #19
      //   1940: lload #15
      //   1942: bipush #16
      //   1944: bipush #16
      //   1946: iconst_1
      //   1947: iconst_1
      //   1948: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   1951: aload_0
      //   1952: aload_0
      //   1953: getfield mFrameRateRange : Landroid/util/Range;
      //   1956: bipush #12
      //   1958: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   1961: iload #11
      //   1963: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   1966: invokevirtual intersect : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   1969: putfield mFrameRateRange : Landroid/util/Range;
      //   1972: iload #25
      //   1974: istore #20
      //   1976: aload #9
      //   1978: astore_1
      //   1979: iload #26
      //   1981: istore #21
      //   1983: goto -> 7114
      //   1986: ldc '/'
      //   1988: astore #4
      //   1990: ldc 'Unrecognized profile/level '
      //   1992: astore #28
      //   1994: ldc 'VideoCapabilities'
      //   1996: astore #6
      //   1998: ldc 'Unrecognized profile '
      //   2000: astore #8
      //   2002: aload_2
      //   2003: ldc 'video/mp4v-es'
      //   2005: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   2008: ifeq -> 3316
      //   2011: aload_1
      //   2012: astore #27
      //   2014: aload #27
      //   2016: arraylength
      //   2017: istore #14
      //   2019: ldc2_w 1485
      //   2022: lstore #15
      //   2024: iconst_0
      //   2025: istore #17
      //   2027: iconst_4
      //   2028: istore #26
      //   2030: bipush #11
      //   2032: istore #24
      //   2034: bipush #9
      //   2036: istore #19
      //   2038: ldc 64000
      //   2040: istore #25
      //   2042: bipush #15
      //   2044: istore #18
      //   2046: bipush #99
      //   2048: istore #29
      //   2050: aload #6
      //   2052: astore_1
      //   2053: aload #28
      //   2055: astore #6
      //   2057: aload #4
      //   2059: astore #5
      //   2061: iload #17
      //   2063: iload #14
      //   2065: if_icmpge -> 3263
      //   2068: aload #27
      //   2070: iload #17
      //   2072: aaload
      //   2073: astore #4
      //   2075: iconst_0
      //   2076: istore #11
      //   2078: iconst_1
      //   2079: istore #30
      //   2081: aload #4
      //   2083: getfield profile : I
      //   2086: istore #20
      //   2088: iload #20
      //   2090: iconst_1
      //   2091: if_icmpeq -> 2719
      //   2094: iload #20
      //   2096: iconst_2
      //   2097: if_icmpeq -> 2636
      //   2100: iload #20
      //   2102: lookupswitch default -> 2224, 4 -> 2636, 8 -> 2636, 16 -> 2636, 32 -> 2636, 64 -> 2636, 128 -> 2636, 256 -> 2636, 512 -> 2636, 1024 -> 2636, 2048 -> 2636, 4096 -> 2636, 8192 -> 2636, 16384 -> 2636, 32768 -> 2304
      //   2224: new java/lang/StringBuilder
      //   2227: dup
      //   2228: invokespecial <init> : ()V
      //   2231: astore #28
      //   2233: aload #28
      //   2235: aload #8
      //   2237: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2240: pop
      //   2241: aload #28
      //   2243: aload #4
      //   2245: getfield profile : I
      //   2248: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2251: pop
      //   2252: aload #28
      //   2254: aload #7
      //   2256: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2259: pop
      //   2260: aload #28
      //   2262: aload_2
      //   2263: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2266: pop
      //   2267: aload_1
      //   2268: aload #28
      //   2270: invokevirtual toString : ()Ljava/lang/String;
      //   2273: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   2276: pop
      //   2277: iload #26
      //   2279: iconst_1
      //   2280: ior
      //   2281: istore #26
      //   2283: iconst_0
      //   2284: istore #13
      //   2286: iconst_0
      //   2287: istore #21
      //   2289: iconst_0
      //   2290: istore #22
      //   2292: iconst_0
      //   2293: istore #12
      //   2295: iconst_0
      //   2296: istore #20
      //   2298: iconst_0
      //   2299: istore #23
      //   2301: goto -> 3115
      //   2304: aload #4
      //   2306: getfield level : I
      //   2309: istore #20
      //   2311: iload #20
      //   2313: iconst_1
      //   2314: if_icmpeq -> 2607
      //   2317: iload #20
      //   2319: iconst_4
      //   2320: if_icmpeq -> 2607
      //   2323: iload #20
      //   2325: bipush #8
      //   2327: if_icmpeq -> 2577
      //   2330: iload #20
      //   2332: bipush #16
      //   2334: if_icmpeq -> 2547
      //   2337: iload #20
      //   2339: bipush #24
      //   2341: if_icmpeq -> 2517
      //   2344: iload #20
      //   2346: bipush #32
      //   2348: if_icmpeq -> 2487
      //   2351: iload #20
      //   2353: sipush #128
      //   2356: if_icmpeq -> 2458
      //   2359: new java/lang/StringBuilder
      //   2362: dup
      //   2363: invokespecial <init> : ()V
      //   2366: astore #28
      //   2368: aload #28
      //   2370: aload #6
      //   2372: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2375: pop
      //   2376: aload #28
      //   2378: aload #4
      //   2380: getfield profile : I
      //   2383: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2386: pop
      //   2387: aload #28
      //   2389: aload #5
      //   2391: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2394: pop
      //   2395: aload #28
      //   2397: aload #4
      //   2399: getfield level : I
      //   2402: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2405: pop
      //   2406: aload #28
      //   2408: aload #7
      //   2410: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2413: pop
      //   2414: aload #28
      //   2416: aload_2
      //   2417: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2420: pop
      //   2421: aload_1
      //   2422: aload #28
      //   2424: invokevirtual toString : ()Ljava/lang/String;
      //   2427: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   2430: pop
      //   2431: iload #26
      //   2433: iconst_1
      //   2434: ior
      //   2435: istore #26
      //   2437: iconst_0
      //   2438: istore #12
      //   2440: iconst_0
      //   2441: istore #13
      //   2443: iconst_0
      //   2444: istore #21
      //   2446: iconst_0
      //   2447: istore #20
      //   2449: iconst_0
      //   2450: istore #22
      //   2452: iconst_0
      //   2453: istore #23
      //   2455: goto -> 3115
      //   2458: ldc 48600
      //   2460: istore #21
      //   2462: sipush #1620
      //   2465: istore #12
      //   2467: bipush #30
      //   2469: istore #13
      //   2471: sipush #8000
      //   2474: istore #20
      //   2476: bipush #36
      //   2478: istore #22
      //   2480: bipush #45
      //   2482: istore #23
      //   2484: goto -> 3115
      //   2487: sipush #23760
      //   2490: istore #21
      //   2492: sipush #792
      //   2495: istore #12
      //   2497: bipush #30
      //   2499: istore #13
      //   2501: sipush #3000
      //   2504: istore #20
      //   2506: bipush #36
      //   2508: istore #22
      //   2510: bipush #44
      //   2512: istore #23
      //   2514: goto -> 3115
      //   2517: sipush #11880
      //   2520: istore #21
      //   2522: sipush #396
      //   2525: istore #12
      //   2527: bipush #30
      //   2529: istore #13
      //   2531: sipush #1500
      //   2534: istore #20
      //   2536: bipush #18
      //   2538: istore #22
      //   2540: bipush #22
      //   2542: istore #23
      //   2544: goto -> 3115
      //   2547: sipush #11880
      //   2550: istore #21
      //   2552: sipush #396
      //   2555: istore #12
      //   2557: bipush #30
      //   2559: istore #13
      //   2561: sipush #768
      //   2564: istore #20
      //   2566: bipush #18
      //   2568: istore #22
      //   2570: bipush #22
      //   2572: istore #23
      //   2574: goto -> 3115
      //   2577: sipush #5940
      //   2580: istore #21
      //   2582: sipush #396
      //   2585: istore #12
      //   2587: bipush #30
      //   2589: istore #13
      //   2591: sipush #384
      //   2594: istore #20
      //   2596: bipush #18
      //   2598: istore #22
      //   2600: bipush #22
      //   2602: istore #23
      //   2604: goto -> 3115
      //   2607: sipush #2970
      //   2610: istore #21
      //   2612: bipush #99
      //   2614: istore #12
      //   2616: bipush #30
      //   2618: istore #13
      //   2620: sipush #128
      //   2623: istore #20
      //   2625: bipush #9
      //   2627: istore #22
      //   2629: bipush #11
      //   2631: istore #23
      //   2633: goto -> 3115
      //   2636: new java/lang/StringBuilder
      //   2639: dup
      //   2640: invokespecial <init> : ()V
      //   2643: astore #28
      //   2645: aload #28
      //   2647: ldc 'Unsupported profile '
      //   2649: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2652: pop
      //   2653: aload #28
      //   2655: aload #4
      //   2657: getfield profile : I
      //   2660: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2663: pop
      //   2664: aload #28
      //   2666: aload #7
      //   2668: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2671: pop
      //   2672: aload #28
      //   2674: aload_2
      //   2675: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2678: pop
      //   2679: aload_1
      //   2680: aload #28
      //   2682: invokevirtual toString : ()Ljava/lang/String;
      //   2685: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   2688: pop
      //   2689: iload #26
      //   2691: iconst_2
      //   2692: ior
      //   2693: istore #26
      //   2695: iconst_0
      //   2696: istore #30
      //   2698: iconst_0
      //   2699: istore #12
      //   2701: iconst_0
      //   2702: istore #13
      //   2704: iconst_0
      //   2705: istore #21
      //   2707: iconst_0
      //   2708: istore #20
      //   2710: iconst_0
      //   2711: istore #22
      //   2713: iconst_0
      //   2714: istore #23
      //   2716: goto -> 3115
      //   2719: aload #4
      //   2721: getfield level : I
      //   2724: istore #20
      //   2726: iload #20
      //   2728: iconst_1
      //   2729: if_icmpeq -> 3087
      //   2732: iload #20
      //   2734: iconst_2
      //   2735: if_icmpeq -> 3055
      //   2738: iload #20
      //   2740: iconst_4
      //   2741: if_icmpeq -> 3027
      //   2744: iload #20
      //   2746: bipush #8
      //   2748: if_icmpeq -> 2997
      //   2751: iload #20
      //   2753: bipush #16
      //   2755: if_icmpeq -> 2967
      //   2758: iload #20
      //   2760: bipush #64
      //   2762: if_icmpeq -> 2938
      //   2765: iload #20
      //   2767: sipush #128
      //   2770: if_icmpeq -> 2909
      //   2773: iload #20
      //   2775: sipush #256
      //   2778: if_icmpeq -> 2880
      //   2781: new java/lang/StringBuilder
      //   2784: dup
      //   2785: invokespecial <init> : ()V
      //   2788: astore #28
      //   2790: aload #28
      //   2792: aload #6
      //   2794: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2797: pop
      //   2798: aload #28
      //   2800: aload #4
      //   2802: getfield profile : I
      //   2805: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2808: pop
      //   2809: aload #28
      //   2811: aload #5
      //   2813: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2816: pop
      //   2817: aload #28
      //   2819: aload #4
      //   2821: getfield level : I
      //   2824: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   2827: pop
      //   2828: aload #28
      //   2830: aload #7
      //   2832: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2835: pop
      //   2836: aload #28
      //   2838: aload_2
      //   2839: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   2842: pop
      //   2843: aload_1
      //   2844: aload #28
      //   2846: invokevirtual toString : ()Ljava/lang/String;
      //   2849: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   2852: pop
      //   2853: iload #26
      //   2855: iconst_1
      //   2856: ior
      //   2857: istore #26
      //   2859: iconst_0
      //   2860: istore #12
      //   2862: iconst_0
      //   2863: istore #13
      //   2865: iconst_0
      //   2866: istore #21
      //   2868: iconst_0
      //   2869: istore #20
      //   2871: iconst_0
      //   2872: istore #22
      //   2874: iconst_0
      //   2875: istore #23
      //   2877: goto -> 3115
      //   2880: ldc 108000
      //   2882: istore #21
      //   2884: sipush #3600
      //   2887: istore #12
      //   2889: bipush #30
      //   2891: istore #13
      //   2893: sipush #12000
      //   2896: istore #20
      //   2898: bipush #45
      //   2900: istore #22
      //   2902: bipush #80
      //   2904: istore #23
      //   2906: goto -> 3115
      //   2909: ldc 40500
      //   2911: istore #21
      //   2913: sipush #1620
      //   2916: istore #12
      //   2918: bipush #30
      //   2920: istore #13
      //   2922: sipush #8000
      //   2925: istore #20
      //   2927: bipush #36
      //   2929: istore #22
      //   2931: bipush #45
      //   2933: istore #23
      //   2935: goto -> 3115
      //   2938: ldc 36000
      //   2940: istore #21
      //   2942: sipush #1200
      //   2945: istore #12
      //   2947: bipush #30
      //   2949: istore #13
      //   2951: sipush #4000
      //   2954: istore #20
      //   2956: bipush #30
      //   2958: istore #22
      //   2960: bipush #40
      //   2962: istore #23
      //   2964: goto -> 3115
      //   2967: sipush #11880
      //   2970: istore #21
      //   2972: sipush #396
      //   2975: istore #12
      //   2977: bipush #30
      //   2979: istore #13
      //   2981: sipush #384
      //   2984: istore #20
      //   2986: bipush #18
      //   2988: istore #22
      //   2990: bipush #22
      //   2992: istore #23
      //   2994: goto -> 3115
      //   2997: sipush #5940
      //   3000: istore #21
      //   3002: sipush #396
      //   3005: istore #12
      //   3007: bipush #30
      //   3009: istore #13
      //   3011: sipush #128
      //   3014: istore #20
      //   3016: bipush #18
      //   3018: istore #22
      //   3020: bipush #22
      //   3022: istore #23
      //   3024: goto -> 3115
      //   3027: sipush #1485
      //   3030: istore #21
      //   3032: bipush #99
      //   3034: istore #12
      //   3036: bipush #30
      //   3038: istore #13
      //   3040: bipush #64
      //   3042: istore #20
      //   3044: bipush #9
      //   3046: istore #22
      //   3048: bipush #11
      //   3050: istore #23
      //   3052: goto -> 3115
      //   3055: iconst_1
      //   3056: istore #11
      //   3058: sipush #1485
      //   3061: istore #21
      //   3063: bipush #99
      //   3065: istore #12
      //   3067: bipush #15
      //   3069: istore #13
      //   3071: sipush #128
      //   3074: istore #20
      //   3076: bipush #9
      //   3078: istore #22
      //   3080: bipush #11
      //   3082: istore #23
      //   3084: goto -> 3115
      //   3087: iconst_1
      //   3088: istore #11
      //   3090: sipush #1485
      //   3093: istore #21
      //   3095: bipush #99
      //   3097: istore #12
      //   3099: bipush #15
      //   3101: istore #13
      //   3103: bipush #64
      //   3105: istore #20
      //   3107: bipush #9
      //   3109: istore #22
      //   3111: bipush #11
      //   3113: istore #23
      //   3115: iload #26
      //   3117: istore #10
      //   3119: iload #30
      //   3121: ifeq -> 3131
      //   3124: iload #26
      //   3126: bipush #-5
      //   3128: iand
      //   3129: istore #10
      //   3131: iload #21
      //   3133: i2l
      //   3134: lload #15
      //   3136: invokestatic max : (JJ)J
      //   3139: lstore #15
      //   3141: iload #12
      //   3143: iload #29
      //   3145: invokestatic max : (II)I
      //   3148: istore #29
      //   3150: iload #20
      //   3152: sipush #1000
      //   3155: imul
      //   3156: iload #25
      //   3158: invokestatic max : (II)I
      //   3161: istore #25
      //   3163: iload #11
      //   3165: ifeq -> 3198
      //   3168: iload #23
      //   3170: iload #24
      //   3172: invokestatic max : (II)I
      //   3175: istore #21
      //   3177: iload #22
      //   3179: iload #19
      //   3181: invokestatic max : (II)I
      //   3184: istore #20
      //   3186: iload #13
      //   3188: iload #18
      //   3190: invokestatic max : (II)I
      //   3193: istore #22
      //   3195: goto -> 3241
      //   3198: iload #12
      //   3200: iconst_2
      //   3201: imul
      //   3202: i2d
      //   3203: invokestatic sqrt : (D)D
      //   3206: d2i
      //   3207: istore #20
      //   3209: iload #20
      //   3211: iload #24
      //   3213: invokestatic max : (II)I
      //   3216: istore #21
      //   3218: iload #20
      //   3220: iload #19
      //   3222: invokestatic max : (II)I
      //   3225: istore #20
      //   3227: iload #13
      //   3229: bipush #60
      //   3231: invokestatic max : (II)I
      //   3234: iload #18
      //   3236: invokestatic max : (II)I
      //   3239: istore #22
      //   3241: iinc #17, 1
      //   3244: iload #22
      //   3246: istore #18
      //   3248: iload #21
      //   3250: istore #24
      //   3252: iload #20
      //   3254: istore #19
      //   3256: iload #10
      //   3258: istore #26
      //   3260: goto -> 2061
      //   3263: aload_0
      //   3264: iload #24
      //   3266: iload #19
      //   3268: iload #29
      //   3270: lload #15
      //   3272: bipush #16
      //   3274: bipush #16
      //   3276: iconst_1
      //   3277: iconst_1
      //   3278: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   3281: aload_0
      //   3282: aload_0
      //   3283: getfield mFrameRateRange : Landroid/util/Range;
      //   3286: bipush #12
      //   3288: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   3291: iload #18
      //   3293: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   3296: invokevirtual intersect : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   3299: putfield mFrameRateRange : Landroid/util/Range;
      //   3302: iload #25
      //   3304: istore #20
      //   3306: aload #9
      //   3308: astore_1
      //   3309: iload #26
      //   3311: istore #21
      //   3313: goto -> 7114
      //   3316: ldc ' for '
      //   3318: astore #8
      //   3320: ldc 'Unrecognized profile/level '
      //   3322: astore #27
      //   3324: ldc 'Unrecognized profile '
      //   3326: astore #4
      //   3328: ldc '/'
      //   3330: astore #28
      //   3332: aload_2
      //   3333: ldc_w 'video/3gpp'
      //   3336: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   3339: ifeq -> 4241
      //   3342: aload_1
      //   3343: astore #7
      //   3345: aload #7
      //   3347: arraylength
      //   3348: istore #19
      //   3350: iconst_4
      //   3351: istore #11
      //   3353: bipush #15
      //   3355: istore #31
      //   3357: bipush #16
      //   3359: istore #10
      //   3361: bipush #11
      //   3363: istore #24
      //   3365: bipush #9
      //   3367: istore #30
      //   3369: ldc 64000
      //   3371: istore #18
      //   3373: bipush #11
      //   3375: istore #14
      //   3377: bipush #99
      //   3379: istore #32
      //   3381: bipush #9
      //   3383: istore #17
      //   3385: ldc2_w 1485
      //   3388: lstore #15
      //   3390: iconst_0
      //   3391: istore #29
      //   3393: aload #28
      //   3395: astore_1
      //   3396: aload_2
      //   3397: astore #5
      //   3399: iload #29
      //   3401: iload #19
      //   3403: if_icmpge -> 4147
      //   3406: aload #7
      //   3408: iload #29
      //   3410: aaload
      //   3411: astore_2
      //   3412: iconst_0
      //   3413: istore #13
      //   3415: iload #14
      //   3417: istore #12
      //   3419: iload #17
      //   3421: istore #26
      //   3423: iconst_0
      //   3424: istore #25
      //   3426: aload_2
      //   3427: getfield level : I
      //   3430: istore #20
      //   3432: iload #20
      //   3434: iconst_1
      //   3435: if_icmpeq -> 3863
      //   3438: iload #20
      //   3440: iconst_2
      //   3441: if_icmpeq -> 3832
      //   3444: iload #20
      //   3446: iconst_4
      //   3447: if_icmpeq -> 3800
      //   3450: iload #20
      //   3452: bipush #8
      //   3454: if_icmpeq -> 3768
      //   3457: iload #20
      //   3459: bipush #16
      //   3461: if_icmpeq -> 3694
      //   3464: iload #20
      //   3466: bipush #32
      //   3468: if_icmpeq -> 3656
      //   3471: iload #20
      //   3473: bipush #64
      //   3475: if_icmpeq -> 3617
      //   3478: iload #20
      //   3480: sipush #128
      //   3483: if_icmpeq -> 3578
      //   3486: new java/lang/StringBuilder
      //   3489: dup
      //   3490: invokespecial <init> : ()V
      //   3493: astore #28
      //   3495: aload #28
      //   3497: aload #27
      //   3499: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3502: pop
      //   3503: aload #28
      //   3505: aload_2
      //   3506: getfield profile : I
      //   3509: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3512: pop
      //   3513: aload #28
      //   3515: aload_1
      //   3516: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3519: pop
      //   3520: aload #28
      //   3522: aload_2
      //   3523: getfield level : I
      //   3526: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3529: pop
      //   3530: aload #28
      //   3532: aload #8
      //   3534: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3537: pop
      //   3538: aload #28
      //   3540: aload #5
      //   3542: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3545: pop
      //   3546: aload #6
      //   3548: aload #28
      //   3550: invokevirtual toString : ()Ljava/lang/String;
      //   3553: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   3556: pop
      //   3557: iload #11
      //   3559: iconst_1
      //   3560: ior
      //   3561: istore #11
      //   3563: iconst_0
      //   3564: istore #20
      //   3566: iconst_0
      //   3567: istore #23
      //   3569: iconst_0
      //   3570: istore #21
      //   3572: iconst_0
      //   3573: istore #22
      //   3575: goto -> 3891
      //   3578: iconst_1
      //   3579: istore #12
      //   3581: iconst_1
      //   3582: istore #26
      //   3584: sipush #256
      //   3587: istore #13
      //   3589: iconst_4
      //   3590: istore #10
      //   3592: bipush #45
      //   3594: bipush #36
      //   3596: imul
      //   3597: bipush #50
      //   3599: imul
      //   3600: istore #20
      //   3602: bipush #60
      //   3604: istore #23
      //   3606: bipush #45
      //   3608: istore #21
      //   3610: bipush #36
      //   3612: istore #22
      //   3614: goto -> 3891
      //   3617: iconst_1
      //   3618: istore #12
      //   3620: iconst_1
      //   3621: istore #26
      //   3623: sipush #128
      //   3626: istore #13
      //   3628: iconst_4
      //   3629: istore #10
      //   3631: bipush #45
      //   3633: bipush #18
      //   3635: imul
      //   3636: bipush #50
      //   3638: imul
      //   3639: istore #20
      //   3641: bipush #60
      //   3643: istore #23
      //   3645: bipush #45
      //   3647: istore #21
      //   3649: bipush #18
      //   3651: istore #22
      //   3653: goto -> 3891
      //   3656: iconst_1
      //   3657: istore #12
      //   3659: iconst_1
      //   3660: istore #26
      //   3662: bipush #64
      //   3664: istore #13
      //   3666: iconst_4
      //   3667: istore #10
      //   3669: bipush #22
      //   3671: bipush #18
      //   3673: imul
      //   3674: bipush #50
      //   3676: imul
      //   3677: istore #20
      //   3679: bipush #60
      //   3681: istore #23
      //   3683: bipush #22
      //   3685: istore #21
      //   3687: bipush #18
      //   3689: istore #22
      //   3691: goto -> 3891
      //   3694: aload_2
      //   3695: getfield profile : I
      //   3698: iconst_1
      //   3699: if_icmpeq -> 3719
      //   3702: aload_2
      //   3703: getfield profile : I
      //   3706: iconst_4
      //   3707: if_icmpne -> 3713
      //   3710: goto -> 3719
      //   3713: iconst_0
      //   3714: istore #20
      //   3716: goto -> 3722
      //   3719: iconst_1
      //   3720: istore #20
      //   3722: iload #20
      //   3724: istore #25
      //   3726: iload #25
      //   3728: ifne -> 3740
      //   3731: iconst_1
      //   3732: istore #12
      //   3734: iconst_1
      //   3735: istore #26
      //   3737: iconst_4
      //   3738: istore #10
      //   3740: iconst_2
      //   3741: istore #13
      //   3743: bipush #11
      //   3745: bipush #9
      //   3747: imul
      //   3748: bipush #15
      //   3750: imul
      //   3751: istore #20
      //   3753: bipush #15
      //   3755: istore #23
      //   3757: bipush #11
      //   3759: istore #21
      //   3761: bipush #9
      //   3763: istore #22
      //   3765: goto -> 3891
      //   3768: iconst_1
      //   3769: istore #25
      //   3771: bipush #32
      //   3773: istore #13
      //   3775: bipush #22
      //   3777: bipush #18
      //   3779: imul
      //   3780: bipush #30
      //   3782: imul
      //   3783: istore #20
      //   3785: bipush #30
      //   3787: istore #23
      //   3789: bipush #22
      //   3791: istore #21
      //   3793: bipush #18
      //   3795: istore #22
      //   3797: goto -> 3891
      //   3800: iconst_1
      //   3801: istore #25
      //   3803: bipush #6
      //   3805: istore #13
      //   3807: bipush #22
      //   3809: bipush #18
      //   3811: imul
      //   3812: bipush #30
      //   3814: imul
      //   3815: istore #20
      //   3817: bipush #30
      //   3819: istore #23
      //   3821: bipush #22
      //   3823: istore #21
      //   3825: bipush #18
      //   3827: istore #22
      //   3829: goto -> 3891
      //   3832: iconst_1
      //   3833: istore #25
      //   3835: iconst_2
      //   3836: istore #13
      //   3838: bipush #22
      //   3840: bipush #18
      //   3842: imul
      //   3843: bipush #15
      //   3845: imul
      //   3846: istore #20
      //   3848: bipush #30
      //   3850: istore #23
      //   3852: bipush #22
      //   3854: istore #21
      //   3856: bipush #18
      //   3858: istore #22
      //   3860: goto -> 3891
      //   3863: iconst_1
      //   3864: istore #25
      //   3866: iconst_1
      //   3867: istore #13
      //   3869: bipush #11
      //   3871: bipush #9
      //   3873: imul
      //   3874: bipush #15
      //   3876: imul
      //   3877: istore #20
      //   3879: bipush #15
      //   3881: istore #23
      //   3883: bipush #11
      //   3885: istore #21
      //   3887: bipush #9
      //   3889: istore #22
      //   3891: aload_2
      //   3892: getfield profile : I
      //   3895: istore #33
      //   3897: iload #33
      //   3899: iconst_1
      //   3900: if_icmpeq -> 4022
      //   3903: iload #33
      //   3905: iconst_2
      //   3906: if_icmpeq -> 4022
      //   3909: iload #33
      //   3911: iconst_4
      //   3912: if_icmpeq -> 4022
      //   3915: iload #33
      //   3917: bipush #8
      //   3919: if_icmpeq -> 4022
      //   3922: iload #33
      //   3924: bipush #16
      //   3926: if_icmpeq -> 4022
      //   3929: iload #33
      //   3931: bipush #32
      //   3933: if_icmpeq -> 4022
      //   3936: iload #33
      //   3938: bipush #64
      //   3940: if_icmpeq -> 4022
      //   3943: iload #33
      //   3945: sipush #128
      //   3948: if_icmpeq -> 4022
      //   3951: iload #33
      //   3953: sipush #256
      //   3956: if_icmpeq -> 4022
      //   3959: new java/lang/StringBuilder
      //   3962: dup
      //   3963: invokespecial <init> : ()V
      //   3966: astore #28
      //   3968: aload #28
      //   3970: aload #4
      //   3972: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3975: pop
      //   3976: aload #28
      //   3978: aload_2
      //   3979: getfield profile : I
      //   3982: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   3985: pop
      //   3986: aload #28
      //   3988: aload #8
      //   3990: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   3993: pop
      //   3994: aload #28
      //   3996: aload #5
      //   3998: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4001: pop
      //   4002: aload #6
      //   4004: aload #28
      //   4006: invokevirtual toString : ()Ljava/lang/String;
      //   4009: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   4012: pop
      //   4013: iload #11
      //   4015: iconst_1
      //   4016: ior
      //   4017: istore #11
      //   4019: goto -> 4022
      //   4022: iload #25
      //   4024: ifeq -> 4038
      //   4027: bipush #11
      //   4029: istore #26
      //   4031: bipush #9
      //   4033: istore #12
      //   4035: goto -> 4055
      //   4038: aload_0
      //   4039: iconst_1
      //   4040: putfield mAllowMbOverride : Z
      //   4043: iload #12
      //   4045: istore #25
      //   4047: iload #26
      //   4049: istore #12
      //   4051: iload #25
      //   4053: istore #26
      //   4055: iload #11
      //   4057: bipush #-5
      //   4059: iand
      //   4060: istore #11
      //   4062: iload #20
      //   4064: i2l
      //   4065: lload #15
      //   4067: invokestatic max : (JJ)J
      //   4070: lstore #15
      //   4072: iload #21
      //   4074: iload #22
      //   4076: imul
      //   4077: iload #32
      //   4079: invokestatic max : (II)I
      //   4082: istore #32
      //   4084: ldc 64000
      //   4086: iload #13
      //   4088: imul
      //   4089: iload #18
      //   4091: invokestatic max : (II)I
      //   4094: istore #18
      //   4096: iload #21
      //   4098: iload #24
      //   4100: invokestatic max : (II)I
      //   4103: istore #24
      //   4105: iload #22
      //   4107: iload #30
      //   4109: invokestatic max : (II)I
      //   4112: istore #30
      //   4114: iload #23
      //   4116: iload #31
      //   4118: invokestatic max : (II)I
      //   4121: istore #31
      //   4123: iload #26
      //   4125: iload #14
      //   4127: invokestatic min : (II)I
      //   4130: istore #14
      //   4132: iload #12
      //   4134: iload #17
      //   4136: invokestatic min : (II)I
      //   4139: istore #17
      //   4141: iinc #29, 1
      //   4144: goto -> 3399
      //   4147: aload_0
      //   4148: getfield mAllowMbOverride : Z
      //   4151: ifne -> 4189
      //   4154: new android/util/Rational
      //   4157: dup
      //   4158: bipush #11
      //   4160: bipush #9
      //   4162: invokespecial <init> : (II)V
      //   4165: astore_1
      //   4166: new android/util/Rational
      //   4169: dup
      //   4170: bipush #11
      //   4172: bipush #9
      //   4174: invokespecial <init> : (II)V
      //   4177: astore #8
      //   4179: aload_0
      //   4180: aload_1
      //   4181: aload #8
      //   4183: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   4186: putfield mBlockAspectRatioRange : Landroid/util/Range;
      //   4189: aload_0
      //   4190: iload #14
      //   4192: iload #17
      //   4194: iload #24
      //   4196: iload #30
      //   4198: iload #32
      //   4200: lload #15
      //   4202: bipush #16
      //   4204: bipush #16
      //   4206: iload #10
      //   4208: iload #10
      //   4210: invokespecial applyMacroBlockLimits : (IIIIIJIIII)V
      //   4213: aload_0
      //   4214: aload #9
      //   4216: iload #31
      //   4218: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   4221: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   4224: putfield mFrameRateRange : Landroid/util/Range;
      //   4227: iload #18
      //   4229: istore #20
      //   4231: iload #11
      //   4233: istore #21
      //   4235: aload #9
      //   4237: astore_1
      //   4238: goto -> 7114
      //   4241: ldc 'VideoCapabilities'
      //   4243: astore #8
      //   4245: aload_2
      //   4246: ldc_w 'video/x-vnd.on2.vp8'
      //   4249: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   4252: ifeq -> 4499
      //   4255: aload_1
      //   4256: arraylength
      //   4257: istore #22
      //   4259: iconst_0
      //   4260: istore #20
      //   4262: iconst_4
      //   4263: istore #21
      //   4265: aload #8
      //   4267: astore #6
      //   4269: aload #4
      //   4271: astore #8
      //   4273: iload #20
      //   4275: iload #22
      //   4277: if_icmpge -> 4466
      //   4280: aload_1
      //   4281: iload #20
      //   4283: aaload
      //   4284: astore #27
      //   4286: aload #27
      //   4288: getfield level : I
      //   4291: istore #23
      //   4293: iload #23
      //   4295: iconst_1
      //   4296: if_icmpeq -> 4381
      //   4299: iload #23
      //   4301: iconst_2
      //   4302: if_icmpeq -> 4381
      //   4305: iload #23
      //   4307: iconst_4
      //   4308: if_icmpeq -> 4381
      //   4311: iload #23
      //   4313: bipush #8
      //   4315: if_icmpeq -> 4381
      //   4318: new java/lang/StringBuilder
      //   4321: dup
      //   4322: invokespecial <init> : ()V
      //   4325: astore #7
      //   4327: aload #7
      //   4329: aload #5
      //   4331: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4334: pop
      //   4335: aload #7
      //   4337: aload #27
      //   4339: getfield level : I
      //   4342: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   4345: pop
      //   4346: aload #7
      //   4348: ldc ' for '
      //   4350: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4353: pop
      //   4354: aload #7
      //   4356: aload_2
      //   4357: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4360: pop
      //   4361: aload #6
      //   4363: aload #7
      //   4365: invokevirtual toString : ()Ljava/lang/String;
      //   4368: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   4371: pop
      //   4372: iload #21
      //   4374: iconst_1
      //   4375: ior
      //   4376: istore #21
      //   4378: goto -> 4381
      //   4381: aload #27
      //   4383: getfield profile : I
      //   4386: iconst_1
      //   4387: if_icmpeq -> 4453
      //   4390: new java/lang/StringBuilder
      //   4393: dup
      //   4394: invokespecial <init> : ()V
      //   4397: astore #7
      //   4399: aload #7
      //   4401: aload #8
      //   4403: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4406: pop
      //   4407: aload #7
      //   4409: aload #27
      //   4411: getfield profile : I
      //   4414: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   4417: pop
      //   4418: aload #7
      //   4420: ldc ' for '
      //   4422: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4425: pop
      //   4426: aload #7
      //   4428: aload_2
      //   4429: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4432: pop
      //   4433: aload #6
      //   4435: aload #7
      //   4437: invokevirtual toString : ()Ljava/lang/String;
      //   4440: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   4443: pop
      //   4444: iload #21
      //   4446: iconst_1
      //   4447: ior
      //   4448: istore #21
      //   4450: goto -> 4453
      //   4453: iload #21
      //   4455: bipush #-5
      //   4457: iand
      //   4458: istore #21
      //   4460: iinc #20, 1
      //   4463: goto -> 4273
      //   4466: aload_0
      //   4467: sipush #32767
      //   4470: sipush #32767
      //   4473: ldc_w 2147483647
      //   4476: ldc2_w 2147483647
      //   4479: bipush #16
      //   4481: bipush #16
      //   4483: iconst_1
      //   4484: iconst_1
      //   4485: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   4488: ldc_w 100000000
      //   4491: istore #20
      //   4493: aload #9
      //   4495: astore_1
      //   4496: goto -> 7114
      //   4499: ldc 'Unrecognized level '
      //   4501: astore #6
      //   4503: aload_2
      //   4504: ldc_w 'video/x-vnd.on2.vp9'
      //   4507: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   4510: ifeq -> 5311
      //   4513: ldc2_w 829440
      //   4516: lstore #34
      //   4518: ldc 36864
      //   4520: istore #25
      //   4522: aload_1
      //   4523: arraylength
      //   4524: istore #12
      //   4526: ldc_w 200000
      //   4529: istore #13
      //   4531: iconst_0
      //   4532: istore #26
      //   4534: iconst_4
      //   4535: istore #23
      //   4537: sipush #512
      //   4540: istore #10
      //   4542: iload #26
      //   4544: iload #12
      //   4546: if_icmpge -> 5251
      //   4549: aload_1
      //   4550: iload #26
      //   4552: aaload
      //   4553: astore #5
      //   4555: aload #5
      //   4557: getfield level : I
      //   4560: istore #20
      //   4562: iload #20
      //   4564: iconst_1
      //   4565: if_icmpeq -> 5053
      //   4568: iload #20
      //   4570: iconst_2
      //   4571: if_icmpeq -> 5030
      //   4574: iload #20
      //   4576: lookupswitch default -> 4684, 4 -> 5007, 8 -> 4985, 16 -> 4962, 32 -> 4940, 64 -> 4917, 128 -> 4894, 256 -> 4872, 512 -> 4849, 1024 -> 4826, 2048 -> 4803, 4096 -> 4781, 8192 -> 4759
      //   4684: new java/lang/StringBuilder
      //   4687: dup
      //   4688: invokespecial <init> : ()V
      //   4691: astore #27
      //   4693: aload #27
      //   4695: aload #6
      //   4697: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4700: pop
      //   4701: aload #27
      //   4703: aload #5
      //   4705: getfield level : I
      //   4708: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   4711: pop
      //   4712: aload #27
      //   4714: ldc ' for '
      //   4716: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4719: pop
      //   4720: aload #27
      //   4722: aload_2
      //   4723: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   4726: pop
      //   4727: aload #8
      //   4729: aload #27
      //   4731: invokevirtual toString : ()Ljava/lang/String;
      //   4734: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   4737: pop
      //   4738: iload #23
      //   4740: iconst_1
      //   4741: ior
      //   4742: istore #23
      //   4744: iconst_0
      //   4745: istore #21
      //   4747: iconst_0
      //   4748: istore #22
      //   4750: lconst_0
      //   4751: lstore #15
      //   4753: iconst_0
      //   4754: istore #20
      //   4756: goto -> 5072
      //   4759: ldc_w 35651584
      //   4762: istore #21
      //   4764: sipush #16832
      //   4767: istore #22
      //   4769: ldc2_w 4706009088
      //   4772: lstore #15
      //   4774: ldc 480000
      //   4776: istore #20
      //   4778: goto -> 5072
      //   4781: ldc_w 35651584
      //   4784: istore #21
      //   4786: sipush #16832
      //   4789: istore #22
      //   4791: ldc2_w 2353004544
      //   4794: lstore #15
      //   4796: ldc 240000
      //   4798: istore #20
      //   4800: goto -> 5072
      //   4803: ldc_w 35651584
      //   4806: istore #21
      //   4808: sipush #16832
      //   4811: istore #22
      //   4813: ldc2_w 1176502272
      //   4816: lstore #15
      //   4818: ldc_w 180000
      //   4821: istore #20
      //   4823: goto -> 5072
      //   4826: ldc_w 8912896
      //   4829: istore #21
      //   4831: sipush #8384
      //   4834: istore #22
      //   4836: ldc2_w 1176502272
      //   4839: lstore #15
      //   4841: ldc_w 180000
      //   4844: istore #20
      //   4846: goto -> 5072
      //   4849: ldc_w 8912896
      //   4852: istore #21
      //   4854: sipush #8384
      //   4857: istore #22
      //   4859: ldc2_w 588251136
      //   4862: lstore #15
      //   4864: ldc_w 120000
      //   4867: istore #20
      //   4869: goto -> 5072
      //   4872: ldc_w 8912896
      //   4875: istore #21
      //   4877: sipush #8384
      //   4880: istore #22
      //   4882: ldc2_w 311951360
      //   4885: lstore #15
      //   4887: ldc 60000
      //   4889: istore #20
      //   4891: goto -> 5072
      //   4894: ldc_w 2228224
      //   4897: istore #21
      //   4899: sipush #4160
      //   4902: istore #22
      //   4904: ldc2_w 160432128
      //   4907: lstore #15
      //   4909: sipush #30000
      //   4912: istore #20
      //   4914: goto -> 5072
      //   4917: ldc_w 2228224
      //   4920: istore #21
      //   4922: sipush #4160
      //   4925: istore #22
      //   4927: ldc2_w 83558400
      //   4930: lstore #15
      //   4932: sipush #18000
      //   4935: istore #20
      //   4937: goto -> 5072
      //   4940: ldc 983040
      //   4942: istore #21
      //   4944: sipush #2752
      //   4947: istore #22
      //   4949: ldc2_w 36864000
      //   4952: lstore #15
      //   4954: sipush #12000
      //   4957: istore #20
      //   4959: goto -> 5072
      //   4962: ldc_w 552960
      //   4965: istore #21
      //   4967: sipush #2048
      //   4970: istore #22
      //   4972: ldc2_w 20736000
      //   4975: lstore #15
      //   4977: sipush #7200
      //   4980: istore #20
      //   4982: goto -> 5072
      //   4985: ldc 245760
      //   4987: istore #21
      //   4989: sipush #1344
      //   4992: istore #22
      //   4994: ldc2_w 9216000
      //   4997: lstore #15
      //   4999: sipush #3600
      //   5002: istore #20
      //   5004: goto -> 5072
      //   5007: ldc_w 122880
      //   5010: istore #21
      //   5012: sipush #960
      //   5015: istore #22
      //   5017: ldc2_w 4608000
      //   5020: lstore #15
      //   5022: sipush #1800
      //   5025: istore #20
      //   5027: goto -> 5072
      //   5030: ldc_w 73728
      //   5033: istore #21
      //   5035: sipush #768
      //   5038: istore #22
      //   5040: ldc2_w 2764800
      //   5043: lstore #15
      //   5045: sipush #800
      //   5048: istore #20
      //   5050: goto -> 5072
      //   5053: ldc 36864
      //   5055: istore #21
      //   5057: sipush #512
      //   5060: istore #22
      //   5062: ldc2_w 829440
      //   5065: lstore #15
      //   5067: sipush #200
      //   5070: istore #20
      //   5072: aload #5
      //   5074: getfield profile : I
      //   5077: istore #11
      //   5079: iload #11
      //   5081: iconst_1
      //   5082: if_icmpeq -> 5198
      //   5085: iload #11
      //   5087: iconst_2
      //   5088: if_icmpeq -> 5198
      //   5091: iload #11
      //   5093: iconst_4
      //   5094: if_icmpeq -> 5198
      //   5097: iload #11
      //   5099: bipush #8
      //   5101: if_icmpeq -> 5198
      //   5104: iload #11
      //   5106: sipush #4096
      //   5109: if_icmpeq -> 5198
      //   5112: iload #11
      //   5114: sipush #8192
      //   5117: if_icmpeq -> 5198
      //   5120: iload #11
      //   5122: sipush #16384
      //   5125: if_icmpeq -> 5198
      //   5128: iload #11
      //   5130: ldc 32768
      //   5132: if_icmpeq -> 5198
      //   5135: new java/lang/StringBuilder
      //   5138: dup
      //   5139: invokespecial <init> : ()V
      //   5142: astore #27
      //   5144: aload #27
      //   5146: aload #4
      //   5148: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5151: pop
      //   5152: aload #27
      //   5154: aload #5
      //   5156: getfield profile : I
      //   5159: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   5162: pop
      //   5163: aload #27
      //   5165: ldc ' for '
      //   5167: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5170: pop
      //   5171: aload #27
      //   5173: aload_2
      //   5174: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5177: pop
      //   5178: aload #8
      //   5180: aload #27
      //   5182: invokevirtual toString : ()Ljava/lang/String;
      //   5185: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   5188: pop
      //   5189: iload #23
      //   5191: iconst_1
      //   5192: ior
      //   5193: istore #23
      //   5195: goto -> 5198
      //   5198: iload #23
      //   5200: bipush #-5
      //   5202: iand
      //   5203: istore #23
      //   5205: lload #15
      //   5207: lload #34
      //   5209: invokestatic max : (JJ)J
      //   5212: lstore #34
      //   5214: iload #21
      //   5216: iload #25
      //   5218: invokestatic max : (II)I
      //   5221: istore #25
      //   5223: iload #20
      //   5225: sipush #1000
      //   5228: imul
      //   5229: iload #13
      //   5231: invokestatic max : (II)I
      //   5234: istore #13
      //   5236: iload #22
      //   5238: iload #10
      //   5240: invokestatic max : (II)I
      //   5243: istore #10
      //   5245: iinc #26, 1
      //   5248: goto -> 4542
      //   5251: aload #9
      //   5253: astore_1
      //   5254: iload #10
      //   5256: bipush #8
      //   5258: invokestatic divUp : (II)I
      //   5261: istore #20
      //   5263: iload #25
      //   5265: bipush #64
      //   5267: invokestatic divUp : (II)I
      //   5270: istore #21
      //   5272: lload #34
      //   5274: ldc2_w 64
      //   5277: invokestatic divUp : (JJ)J
      //   5280: lstore #15
      //   5282: aload_0
      //   5283: iload #20
      //   5285: iload #20
      //   5287: iload #21
      //   5289: lload #15
      //   5291: bipush #8
      //   5293: bipush #8
      //   5295: iconst_1
      //   5296: iconst_1
      //   5297: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   5300: iload #13
      //   5302: istore #20
      //   5304: iload #23
      //   5306: istore #21
      //   5308: goto -> 7114
      //   5311: aload_2
      //   5312: ldc_w 'video/hevc'
      //   5315: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   5318: ifeq -> 6227
      //   5321: sipush #576
      //   5324: bipush #15
      //   5326: imul
      //   5327: i2l
      //   5328: lstore #15
      //   5330: aload_1
      //   5331: arraylength
      //   5332: istore #13
      //   5334: sipush #576
      //   5337: istore #12
      //   5339: ldc_w 128000
      //   5342: istore #23
      //   5344: iconst_0
      //   5345: istore #26
      //   5347: iconst_4
      //   5348: istore #22
      //   5350: iload #26
      //   5352: iload #13
      //   5354: if_icmpge -> 6183
      //   5357: aload_1
      //   5358: iload #26
      //   5360: aaload
      //   5361: astore #5
      //   5363: iconst_0
      //   5364: istore #20
      //   5366: iconst_0
      //   5367: istore #21
      //   5369: aload #5
      //   5371: getfield level : I
      //   5374: istore #25
      //   5376: iload #25
      //   5378: iconst_1
      //   5379: if_icmpeq -> 6008
      //   5382: iload #25
      //   5384: iconst_2
      //   5385: if_icmpeq -> 6008
      //   5388: iload #25
      //   5390: lookupswitch default -> 5592, 4 -> 5990, 8 -> 5990, 16 -> 5973, 32 -> 5973, 64 -> 5955, 128 -> 5955, 256 -> 5938, 512 -> 5938, 1024 -> 5920, 2048 -> 5902, 4096 -> 5884, 8192 -> 5867, 16384 -> 5849, 32768 -> 5831, 65536 -> 5813, 131072 -> 5795, 262144 -> 5778, 524288 -> 5761, 1048576 -> 5744, 2097152 -> 5727, 4194304 -> 5709, 8388608 -> 5692, 16777216 -> 5675, 33554432 -> 5658
      //   5592: new java/lang/StringBuilder
      //   5595: dup
      //   5596: invokespecial <init> : ()V
      //   5599: astore #27
      //   5601: aload #27
      //   5603: aload #6
      //   5605: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5608: pop
      //   5609: aload #27
      //   5611: aload #5
      //   5613: getfield level : I
      //   5616: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   5619: pop
      //   5620: aload #27
      //   5622: ldc ' for '
      //   5624: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5627: pop
      //   5628: aload #27
      //   5630: aload_2
      //   5631: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   5634: pop
      //   5635: aload #8
      //   5637: aload #27
      //   5639: invokevirtual toString : ()Ljava/lang/String;
      //   5642: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   5645: pop
      //   5646: iload #22
      //   5648: iconst_1
      //   5649: ior
      //   5650: istore #22
      //   5652: dconst_0
      //   5653: dstore #36
      //   5655: goto -> 6022
      //   5658: ldc2_w 120.0
      //   5661: dstore #36
      //   5663: ldc_w 35651584
      //   5666: istore #20
      //   5668: ldc 800000
      //   5670: istore #21
      //   5672: goto -> 6022
      //   5675: ldc2_w 120.0
      //   5678: dstore #36
      //   5680: ldc_w 35651584
      //   5683: istore #20
      //   5685: ldc 240000
      //   5687: istore #21
      //   5689: goto -> 6022
      //   5692: ldc2_w 60.0
      //   5695: dstore #36
      //   5697: ldc_w 35651584
      //   5700: istore #20
      //   5702: ldc 480000
      //   5704: istore #21
      //   5706: goto -> 6022
      //   5709: ldc2_w 60.0
      //   5712: dstore #36
      //   5714: ldc_w 35651584
      //   5717: istore #20
      //   5719: ldc_w 120000
      //   5722: istore #21
      //   5724: goto -> 6022
      //   5727: ldc2_w 30.0
      //   5730: dstore #36
      //   5732: ldc_w 35651584
      //   5735: istore #20
      //   5737: ldc 240000
      //   5739: istore #21
      //   5741: goto -> 6022
      //   5744: ldc2_w 30.0
      //   5747: dstore #36
      //   5749: ldc_w 35651584
      //   5752: istore #20
      //   5754: ldc 60000
      //   5756: istore #21
      //   5758: goto -> 6022
      //   5761: ldc2_w 120.0
      //   5764: dstore #36
      //   5766: ldc_w 8912896
      //   5769: istore #20
      //   5771: ldc 240000
      //   5773: istore #21
      //   5775: goto -> 6022
      //   5778: ldc2_w 120.0
      //   5781: dstore #36
      //   5783: ldc_w 8912896
      //   5786: istore #20
      //   5788: ldc 60000
      //   5790: istore #21
      //   5792: goto -> 6022
      //   5795: ldc2_w 60.0
      //   5798: dstore #36
      //   5800: ldc_w 8912896
      //   5803: istore #20
      //   5805: ldc_w 160000
      //   5808: istore #21
      //   5810: goto -> 6022
      //   5813: ldc2_w 60.0
      //   5816: dstore #36
      //   5818: ldc_w 8912896
      //   5821: istore #20
      //   5823: ldc_w 40000
      //   5826: istore #21
      //   5828: goto -> 6022
      //   5831: ldc2_w 30.0
      //   5834: dstore #36
      //   5836: ldc_w 8912896
      //   5839: istore #20
      //   5841: ldc_w 100000
      //   5844: istore #21
      //   5846: goto -> 6022
      //   5849: ldc2_w 30.0
      //   5852: dstore #36
      //   5854: ldc_w 8912896
      //   5857: istore #20
      //   5859: sipush #25000
      //   5862: istore #21
      //   5864: goto -> 6022
      //   5867: ldc2_w 60.0
      //   5870: dstore #36
      //   5872: ldc_w 2228224
      //   5875: istore #20
      //   5877: ldc 50000
      //   5879: istore #21
      //   5881: goto -> 6022
      //   5884: ldc2_w 60.0
      //   5887: dstore #36
      //   5889: ldc_w 2228224
      //   5892: istore #20
      //   5894: sipush #20000
      //   5897: istore #21
      //   5899: goto -> 6022
      //   5902: ldc2_w 30.0
      //   5905: dstore #36
      //   5907: ldc_w 2228224
      //   5910: istore #20
      //   5912: sipush #30000
      //   5915: istore #21
      //   5917: goto -> 6022
      //   5920: ldc2_w 30.0
      //   5923: dstore #36
      //   5925: ldc_w 2228224
      //   5928: istore #20
      //   5930: sipush #12000
      //   5933: istore #21
      //   5935: goto -> 6022
      //   5938: ldc2_w 33.75
      //   5941: dstore #36
      //   5943: ldc 983040
      //   5945: istore #20
      //   5947: sipush #10000
      //   5950: istore #21
      //   5952: goto -> 6022
      //   5955: ldc2_w 30.0
      //   5958: dstore #36
      //   5960: ldc_w 552960
      //   5963: istore #20
      //   5965: sipush #6000
      //   5968: istore #21
      //   5970: goto -> 6022
      //   5973: ldc2_w 30.0
      //   5976: dstore #36
      //   5978: ldc 245760
      //   5980: istore #20
      //   5982: sipush #3000
      //   5985: istore #21
      //   5987: goto -> 6022
      //   5990: ldc2_w 30.0
      //   5993: dstore #36
      //   5995: ldc_w 122880
      //   5998: istore #20
      //   6000: sipush #1500
      //   6003: istore #21
      //   6005: goto -> 6022
      //   6008: ldc2_w 15.0
      //   6011: dstore #36
      //   6013: ldc 36864
      //   6015: istore #20
      //   6017: sipush #128
      //   6020: istore #21
      //   6022: aload #5
      //   6024: getfield profile : I
      //   6027: istore #25
      //   6029: iload #25
      //   6031: iconst_1
      //   6032: if_icmpeq -> 6126
      //   6035: iload #25
      //   6037: iconst_2
      //   6038: if_icmpeq -> 6126
      //   6041: iload #25
      //   6043: iconst_4
      //   6044: if_icmpeq -> 6126
      //   6047: iload #25
      //   6049: sipush #4096
      //   6052: if_icmpeq -> 6126
      //   6055: iload #25
      //   6057: sipush #8192
      //   6060: if_icmpeq -> 6126
      //   6063: new java/lang/StringBuilder
      //   6066: dup
      //   6067: invokespecial <init> : ()V
      //   6070: astore #27
      //   6072: aload #27
      //   6074: aload #4
      //   6076: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6079: pop
      //   6080: aload #27
      //   6082: aload #5
      //   6084: getfield profile : I
      //   6087: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   6090: pop
      //   6091: aload #27
      //   6093: ldc ' for '
      //   6095: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6098: pop
      //   6099: aload #27
      //   6101: aload_2
      //   6102: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6105: pop
      //   6106: aload #8
      //   6108: aload #27
      //   6110: invokevirtual toString : ()Ljava/lang/String;
      //   6113: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   6116: pop
      //   6117: iload #22
      //   6119: iconst_1
      //   6120: ior
      //   6121: istore #22
      //   6123: goto -> 6126
      //   6126: iload #20
      //   6128: bipush #6
      //   6130: ishr
      //   6131: istore #20
      //   6133: iload #22
      //   6135: bipush #-5
      //   6137: iand
      //   6138: istore #22
      //   6140: iload #20
      //   6142: i2d
      //   6143: dload #36
      //   6145: dmul
      //   6146: d2i
      //   6147: i2l
      //   6148: lload #15
      //   6150: invokestatic max : (JJ)J
      //   6153: lstore #15
      //   6155: iload #20
      //   6157: iload #12
      //   6159: invokestatic max : (II)I
      //   6162: istore #12
      //   6164: iload #21
      //   6166: sipush #1000
      //   6169: imul
      //   6170: iload #23
      //   6172: invokestatic max : (II)I
      //   6175: istore #23
      //   6177: iinc #26, 1
      //   6180: goto -> 5350
      //   6183: iload #12
      //   6185: bipush #8
      //   6187: imul
      //   6188: i2d
      //   6189: invokestatic sqrt : (D)D
      //   6192: d2i
      //   6193: istore #20
      //   6195: aload_0
      //   6196: iload #20
      //   6198: iload #20
      //   6200: iload #12
      //   6202: lload #15
      //   6204: bipush #8
      //   6206: bipush #8
      //   6208: iconst_1
      //   6209: iconst_1
      //   6210: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   6213: iload #23
      //   6215: istore #20
      //   6217: iload #22
      //   6219: istore #21
      //   6221: aload #9
      //   6223: astore_1
      //   6224: goto -> 7114
      //   6227: aload #6
      //   6229: astore #5
      //   6231: aload_2
      //   6232: ldc_w 'video/av01'
      //   6235: invokevirtual equalsIgnoreCase : (Ljava/lang/String;)Z
      //   6238: ifeq -> 7070
      //   6241: ldc2_w 829440
      //   6244: lstore #34
      //   6246: ldc 36864
      //   6248: istore #10
      //   6250: aload_1
      //   6251: arraylength
      //   6252: istore #26
      //   6254: ldc_w 200000
      //   6257: istore #13
      //   6259: iconst_4
      //   6260: istore #23
      //   6262: sipush #512
      //   6265: istore #12
      //   6267: iconst_0
      //   6268: istore #25
      //   6270: aload #8
      //   6272: astore #6
      //   6274: aload #5
      //   6276: astore #8
      //   6278: iload #25
      //   6280: iload #26
      //   6282: if_icmpge -> 7010
      //   6285: aload_1
      //   6286: iload #25
      //   6288: aaload
      //   6289: astore #5
      //   6291: aload #5
      //   6293: getfield level : I
      //   6296: istore #20
      //   6298: iload #20
      //   6300: iconst_1
      //   6301: if_icmpeq -> 6839
      //   6304: iload #20
      //   6306: iconst_2
      //   6307: if_icmpeq -> 6816
      //   6310: iload #20
      //   6312: lookupswitch default -> 6468, 4 -> 6816, 8 -> 6816, 16 -> 6793, 32 -> 6770, 64 -> 6770, 128 -> 6770, 256 -> 6747, 512 -> 6724, 1024 -> 6724, 2048 -> 6724, 4096 -> 6701, 8192 -> 6678, 16384 -> 6656, 32768 -> 6634, 65536 -> 6612, 131072 -> 6589, 262144 -> 6566, 524288 -> 6543
      //   6468: new java/lang/StringBuilder
      //   6471: dup
      //   6472: invokespecial <init> : ()V
      //   6475: astore #27
      //   6477: aload #27
      //   6479: aload #8
      //   6481: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6484: pop
      //   6485: aload #27
      //   6487: aload #5
      //   6489: getfield level : I
      //   6492: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   6495: pop
      //   6496: aload #27
      //   6498: ldc ' for '
      //   6500: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6503: pop
      //   6504: aload #27
      //   6506: aload_2
      //   6507: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6510: pop
      //   6511: aload #6
      //   6513: aload #27
      //   6515: invokevirtual toString : ()Ljava/lang/String;
      //   6518: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   6521: pop
      //   6522: iload #23
      //   6524: iconst_1
      //   6525: ior
      //   6526: istore #23
      //   6528: iconst_0
      //   6529: istore #21
      //   6531: iconst_0
      //   6532: istore #20
      //   6534: iconst_0
      //   6535: istore #22
      //   6537: lconst_0
      //   6538: lstore #15
      //   6540: goto -> 6859
      //   6543: ldc_w 35651584
      //   6546: istore #21
      //   6548: sipush #16384
      //   6551: istore #20
      //   6553: ldc_w 160000
      //   6556: istore #22
      //   6558: ldc2_w 4706009088
      //   6561: lstore #15
      //   6563: goto -> 6859
      //   6566: ldc_w 35651584
      //   6569: istore #21
      //   6571: sipush #16384
      //   6574: istore #20
      //   6576: ldc_w 160000
      //   6579: istore #22
      //   6581: ldc2_w 4379443200
      //   6584: lstore #15
      //   6586: goto -> 6859
      //   6589: ldc_w 35651584
      //   6592: istore #21
      //   6594: sipush #16384
      //   6597: istore #20
      //   6599: ldc_w 100000
      //   6602: istore #22
      //   6604: ldc2_w 2189721600
      //   6607: lstore #15
      //   6609: goto -> 6859
      //   6612: ldc_w 35651584
      //   6615: istore #21
      //   6617: sipush #16384
      //   6620: istore #20
      //   6622: ldc 60000
      //   6624: istore #22
      //   6626: ldc2_w 1176502272
      //   6629: lstore #15
      //   6631: goto -> 6859
      //   6634: ldc_w 8912896
      //   6637: istore #21
      //   6639: sipush #8192
      //   6642: istore #20
      //   6644: ldc 60000
      //   6646: istore #22
      //   6648: ldc2_w 1176502272
      //   6651: lstore #15
      //   6653: goto -> 6859
      //   6656: ldc_w 8912896
      //   6659: istore #21
      //   6661: sipush #8192
      //   6664: istore #20
      //   6666: ldc 60000
      //   6668: istore #22
      //   6670: ldc2_w 1094860800
      //   6673: lstore #15
      //   6675: goto -> 6859
      //   6678: ldc_w 8912896
      //   6681: istore #21
      //   6683: sipush #8192
      //   6686: istore #20
      //   6688: ldc_w 40000
      //   6691: istore #22
      //   6693: ldc2_w 547430400
      //   6696: lstore #15
      //   6698: goto -> 6859
      //   6701: ldc_w 8912896
      //   6704: istore #21
      //   6706: sipush #8192
      //   6709: istore #20
      //   6711: sipush #30000
      //   6714: istore #22
      //   6716: ldc2_w 273715200
      //   6719: lstore #15
      //   6721: goto -> 6859
      //   6724: ldc_w 2359296
      //   6727: istore #21
      //   6729: sipush #6144
      //   6732: istore #20
      //   6734: sipush #20000
      //   6737: istore #22
      //   6739: ldc2_w 155713536
      //   6742: lstore #15
      //   6744: goto -> 6859
      //   6747: ldc_w 2359296
      //   6750: istore #21
      //   6752: sipush #6144
      //   6755: istore #20
      //   6757: sipush #12000
      //   6760: istore #22
      //   6762: ldc2_w 77856768
      //   6765: lstore #15
      //   6767: goto -> 6859
      //   6770: ldc_w 1065024
      //   6773: istore #21
      //   6775: sipush #5504
      //   6778: istore #20
      //   6780: sipush #10000
      //   6783: istore #22
      //   6785: ldc2_w 39938400
      //   6788: lstore #15
      //   6790: goto -> 6859
      //   6793: ldc_w 665856
      //   6796: istore #21
      //   6798: sipush #4352
      //   6801: istore #20
      //   6803: sipush #6000
      //   6806: istore #22
      //   6808: ldc2_w 24969600
      //   6811: lstore #15
      //   6813: goto -> 6859
      //   6816: ldc_w 278784
      //   6819: istore #21
      //   6821: sipush #2816
      //   6824: istore #20
      //   6826: sipush #3000
      //   6829: istore #22
      //   6831: ldc2_w 10454400
      //   6834: lstore #15
      //   6836: goto -> 6859
      //   6839: ldc_w 147456
      //   6842: istore #21
      //   6844: sipush #2048
      //   6847: istore #20
      //   6849: sipush #1500
      //   6852: istore #22
      //   6854: ldc2_w 5529600
      //   6857: lstore #15
      //   6859: aload #5
      //   6861: getfield profile : I
      //   6864: istore #11
      //   6866: iload #11
      //   6868: iconst_1
      //   6869: if_icmpeq -> 6957
      //   6872: iload #11
      //   6874: iconst_2
      //   6875: if_icmpeq -> 6957
      //   6878: iload #11
      //   6880: sipush #4096
      //   6883: if_icmpeq -> 6957
      //   6886: iload #11
      //   6888: sipush #8192
      //   6891: if_icmpeq -> 6957
      //   6894: new java/lang/StringBuilder
      //   6897: dup
      //   6898: invokespecial <init> : ()V
      //   6901: astore #27
      //   6903: aload #27
      //   6905: aload #4
      //   6907: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6910: pop
      //   6911: aload #27
      //   6913: aload #5
      //   6915: getfield profile : I
      //   6918: invokevirtual append : (I)Ljava/lang/StringBuilder;
      //   6921: pop
      //   6922: aload #27
      //   6924: ldc ' for '
      //   6926: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6929: pop
      //   6930: aload #27
      //   6932: aload_2
      //   6933: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   6936: pop
      //   6937: aload #6
      //   6939: aload #27
      //   6941: invokevirtual toString : ()Ljava/lang/String;
      //   6944: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   6947: pop
      //   6948: iload #23
      //   6950: iconst_1
      //   6951: ior
      //   6952: istore #23
      //   6954: goto -> 6957
      //   6957: iload #23
      //   6959: bipush #-5
      //   6961: iand
      //   6962: istore #23
      //   6964: lload #15
      //   6966: lload #34
      //   6968: invokestatic max : (JJ)J
      //   6971: lstore #34
      //   6973: iload #21
      //   6975: iload #10
      //   6977: invokestatic max : (II)I
      //   6980: istore #10
      //   6982: iload #22
      //   6984: sipush #1000
      //   6987: imul
      //   6988: iload #13
      //   6990: invokestatic max : (II)I
      //   6993: istore #13
      //   6995: iload #20
      //   6997: iload #12
      //   6999: invokestatic max : (II)I
      //   7002: istore #12
      //   7004: iinc #25, 1
      //   7007: goto -> 6278
      //   7010: iload #12
      //   7012: bipush #8
      //   7014: invokestatic divUp : (II)I
      //   7017: istore #20
      //   7019: iload #10
      //   7021: bipush #64
      //   7023: invokestatic divUp : (II)I
      //   7026: istore #21
      //   7028: lload #34
      //   7030: ldc2_w 64
      //   7033: invokestatic divUp : (JJ)J
      //   7036: lstore #15
      //   7038: aload_0
      //   7039: iload #20
      //   7041: iload #20
      //   7043: iload #21
      //   7045: lload #15
      //   7047: bipush #8
      //   7049: bipush #8
      //   7051: iconst_1
      //   7052: iconst_1
      //   7053: invokespecial applyMacroBlockLimits : (IIIJIIII)V
      //   7056: iload #13
      //   7058: istore #20
      //   7060: iload #23
      //   7062: istore #21
      //   7064: aload #9
      //   7066: astore_1
      //   7067: goto -> 7114
      //   7070: new java/lang/StringBuilder
      //   7073: dup
      //   7074: invokespecial <init> : ()V
      //   7077: astore_1
      //   7078: aload_1
      //   7079: ldc_w 'Unsupported mime '
      //   7082: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   7085: pop
      //   7086: aload_1
      //   7087: aload_2
      //   7088: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   7091: pop
      //   7092: aload #8
      //   7094: aload_1
      //   7095: invokevirtual toString : ()Ljava/lang/String;
      //   7098: invokestatic w : (Ljava/lang/String;Ljava/lang/String;)I
      //   7101: pop
      //   7102: iconst_4
      //   7103: iconst_2
      //   7104: ior
      //   7105: istore #21
      //   7107: ldc 64000
      //   7109: istore #20
      //   7111: aload #9
      //   7113: astore_1
      //   7114: aload_0
      //   7115: aload_1
      //   7116: iload #20
      //   7118: invokestatic valueOf : (I)Ljava/lang/Integer;
      //   7121: invokestatic create : (Ljava/lang/Comparable;Ljava/lang/Comparable;)Landroid/util/Range;
      //   7124: putfield mBitrateRange : Landroid/util/Range;
      //   7127: aload_0
      //   7128: getfield mParent : Landroid/media/MediaCodecInfo$CodecCapabilities;
      //   7131: astore_1
      //   7132: aload_1
      //   7133: aload_1
      //   7134: getfield mError : I
      //   7137: iload #21
      //   7139: ior
      //   7140: putfield mError : I
      //   7143: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #2598	-> 0
      //   #2599	-> 0
      //   #2600	-> 0
      //   #2601	-> 0
      //   #2603	-> 0
      //   #2604	-> 0
      //   #2605	-> 8
      //   #2607	-> 16
      //   #3285	-> 43
      //   #2607	-> 49
      //   #2608	-> 53
      //   #2609	-> 53
      //   #2610	-> 53
      //   #2611	-> 53
      //   #2612	-> 53
      //   #2613	-> 98
      //   #2614	-> 98
      //   #2615	-> 104
      //   #2657	-> 280
      //   #2659	-> 334
      //   #2655	-> 355
      //   #2653	-> 374
      //   #2651	-> 393
      //   #2649	-> 412
      //   #2647	-> 431
      //   #2645	-> 450
      //   #2643	-> 470
      //   #2641	-> 490
      //   #2639	-> 510
      //   #2637	-> 531
      //   #2635	-> 553
      //   #2633	-> 575
      //   #2631	-> 597
      //   #2629	-> 620
      //   #2627	-> 643
      //   #2625	-> 666
      //   #2623	-> 689
      //   #2621	-> 712
      //   #2619	-> 735
      //   #2617	-> 757
      //   #2661	-> 775
      //   #2680	-> 866
      //   #2682	-> 920
      //   #2683	-> 926
      //   #2666	-> 937
      //   #2664	-> 948
      //   #2670	-> 959
      //   #2672	-> 1013
      //   #2673	-> 1019
      //   #2678	-> 1022
      //   #2685	-> 1038
      //   #2686	-> 1047
      //   #2688	-> 1054
      //   #2689	-> 1064
      //   #2690	-> 1073
      //   #2691	-> 1082
      //   #2612	-> 1091
      //   #2694	-> 1101
      //   #2695	-> 1113
      //   #2700	-> 1131
      //   #2701	-> 1168
      //   #2702	-> 1168
      //   #2703	-> 1168
      //   #2704	-> 1168
      //   #2705	-> 1168
      //   #2706	-> 1224
      //   #2707	-> 1224
      //   #2708	-> 1227
      //   #2749	-> 1269
      //   #2751	-> 1323
      //   #2743	-> 1350
      //   #2745	-> 1404
      //   #2746	-> 1410
      //   #2747	-> 1413
      //   #2721	-> 1434
      //   #2733	-> 1470
      //   #2736	-> 1543
      //   #2738	-> 1549
      //   #2731	-> 1570
      //   #2729	-> 1598
      //   #2727	-> 1626
      //   #2725	-> 1654
      //   #2723	-> 1683
      //   #2710	-> 1713
      //   #2714	-> 1722
      //   #2717	-> 1795
      //   #2719	-> 1801
      //   #2712	-> 1822
      //   #2753	-> 1848
      //   #2754	-> 1857
      //   #2756	-> 1864
      //   #2757	-> 1874
      //   #2758	-> 1883
      //   #2759	-> 1896
      //   #2760	-> 1905
      //   #2761	-> 1914
      //   #2705	-> 1923
      //   #2763	-> 1933
      //   #2767	-> 1951
      //   #2768	-> 1972
      //   #2769	-> 2011
      //   #2770	-> 2011
      //   #2771	-> 2011
      //   #2772	-> 2011
      //   #2773	-> 2011
      //   #2774	-> 2075
      //   #2775	-> 2075
      //   #2776	-> 2078
      //   #2777	-> 2081
      //   #2852	-> 2224
      //   #2854	-> 2277
      //   #2806	-> 2304
      //   #2821	-> 2359
      //   #2824	-> 2431
      //   #2826	-> 2437
      //   #2819	-> 2458
      //   #2817	-> 2487
      //   #2815	-> 2517
      //   #2813	-> 2547
      //   #2811	-> 2577
      //   #2806	-> 2607
      //   #2809	-> 2607
      //   #2777	-> 2636
      //   #2846	-> 2636
      //   #2848	-> 2689
      //   #2849	-> 2695
      //   #2850	-> 2698
      //   #2779	-> 2719
      //   #2799	-> 2781
      //   #2802	-> 2853
      //   #2804	-> 2859
      //   #2797	-> 2880
      //   #2795	-> 2909
      //   #2793	-> 2938
      //   #2791	-> 2967
      //   #2789	-> 2997
      //   #2784	-> 3027
      //   #2786	-> 3055
      //   #2787	-> 3058
      //   #2781	-> 3087
      //   #2782	-> 3090
      //   #2856	-> 3115
      //   #2857	-> 3124
      //   #2859	-> 3131
      //   #2860	-> 3141
      //   #2861	-> 3150
      //   #2862	-> 3163
      //   #2863	-> 3168
      //   #2864	-> 3177
      //   #2865	-> 3186
      //   #2868	-> 3198
      //   #2869	-> 3209
      //   #2870	-> 3218
      //   #2871	-> 3227
      //   #2773	-> 3241
      //   #2874	-> 3263
      //   #2878	-> 3281
      //   #2879	-> 3302
      //   #2880	-> 3342
      //   #2881	-> 3342
      //   #2882	-> 3342
      //   #2883	-> 3342
      //   #2884	-> 3342
      //   #2885	-> 3342
      //   #2886	-> 3342
      //   #2887	-> 3412
      //   #2888	-> 3423
      //   #2889	-> 3426
      //   #2924	-> 3486
      //   #2926	-> 3557
      //   #2921	-> 3578
      //   #2922	-> 3584
      //   #2917	-> 3617
      //   #2918	-> 3623
      //   #2913	-> 3656
      //   #2914	-> 3662
      //   #2904	-> 3694
      //   #2907	-> 3726
      //   #2908	-> 3731
      //   #2910	-> 3740
      //   #2900	-> 3768
      //   #2901	-> 3771
      //   #2897	-> 3800
      //   #2898	-> 3803
      //   #2894	-> 3832
      //   #2895	-> 3835
      //   #2891	-> 3863
      //   #2892	-> 3866
      //   #2928	-> 3891
      //   #2940	-> 3959
      //   #2942	-> 4013
      //   #2938	-> 4022
      //   #2944	-> 4022
      //   #2949	-> 4027
      //   #2954	-> 4038
      //   #2956	-> 4055
      //   #2957	-> 4062
      //   #2958	-> 4072
      //   #2959	-> 4084
      //   #2960	-> 4096
      //   #2961	-> 4105
      //   #2962	-> 4114
      //   #2963	-> 4123
      //   #2964	-> 4132
      //   #2886	-> 4141
      //   #2968	-> 4147
      //   #2969	-> 4154
      //   #2970	-> 4179
      //   #2972	-> 4189
      //   #2978	-> 4213
      //   #2979	-> 4227
      //   #2980	-> 4255
      //   #2981	-> 4255
      //   #2984	-> 4255
      //   #2988	-> 4255
      //   #2989	-> 4286
      //   #2996	-> 4318
      //   #2998	-> 4372
      //   #2989	-> 4381
      //   #2994	-> 4381
      //   #3000	-> 4381
      //   #3004	-> 4390
      //   #3006	-> 4444
      //   #3002	-> 4453
      //   #3008	-> 4453
      //   #2988	-> 4460
      //   #3011	-> 4466
      //   #3012	-> 4466
      //   #3015	-> 4488
      //   #3016	-> 4513
      //   #3017	-> 4518
      //   #3018	-> 4522
      //   #3019	-> 4522
      //   #3021	-> 4522
      //   #3022	-> 4555
      //   #3023	-> 4555
      //   #3024	-> 4555
      //   #3025	-> 4555
      //   #3026	-> 4555
      //   #3056	-> 4684
      //   #3058	-> 4738
      //   #3054	-> 4759
      //   #3052	-> 4781
      //   #3050	-> 4803
      //   #3048	-> 4826
      //   #3046	-> 4849
      //   #3044	-> 4872
      //   #3042	-> 4894
      //   #3040	-> 4917
      //   #3038	-> 4940
      //   #3036	-> 4962
      //   #3034	-> 4985
      //   #3032	-> 5007
      //   #3030	-> 5030
      //   #3028	-> 5053
      //   #3060	-> 5072
      //   #3071	-> 5135
      //   #3073	-> 5189
      //   #3069	-> 5198
      //   #3075	-> 5198
      //   #3076	-> 5205
      //   #3077	-> 5214
      //   #3078	-> 5223
      //   #3079	-> 5236
      //   #3021	-> 5245
      //   #3082	-> 5251
      //   #3083	-> 5254
      //   #3084	-> 5263
      //   #3085	-> 5272
      //   #3087	-> 5282
      //   #3092	-> 5300
      //   #3094	-> 5321
      //   #3095	-> 5321
      //   #3096	-> 5330
      //   #3097	-> 5330
      //   #3098	-> 5363
      //   #3099	-> 5363
      //   #3100	-> 5366
      //   #3101	-> 5369
      //   #3154	-> 5592
      //   #3156	-> 5646
      //   #3152	-> 5658
      //   #3150	-> 5675
      //   #3148	-> 5692
      //   #3146	-> 5709
      //   #3144	-> 5727
      //   #3142	-> 5744
      //   #3140	-> 5761
      //   #3138	-> 5778
      //   #3136	-> 5795
      //   #3134	-> 5813
      //   #3132	-> 5831
      //   #3130	-> 5849
      //   #3128	-> 5867
      //   #3126	-> 5884
      //   #3124	-> 5902
      //   #3122	-> 5920
      //   #3120	-> 5938
      //   #3117	-> 5955
      //   #3114	-> 5973
      //   #3111	-> 5990
      //   #3108	-> 6008
      //   #3158	-> 6022
      //   #3166	-> 6063
      //   #3168	-> 6117
      //   #3164	-> 6126
      //   #3178	-> 6126
      //   #3179	-> 6133
      //   #3180	-> 6140
      //   #3181	-> 6155
      //   #3182	-> 6164
      //   #3097	-> 6177
      //   #3185	-> 6183
      //   #3186	-> 6195
      //   #3191	-> 6213
      //   #3192	-> 6241
      //   #3193	-> 6246
      //   #3194	-> 6250
      //   #3195	-> 6250
      //   #3201	-> 6250
      //   #3202	-> 6291
      //   #3203	-> 6291
      //   #3204	-> 6291
      //   #3205	-> 6291
      //   #3206	-> 6291
      //   #3247	-> 6468
      //   #3249	-> 6522
      //   #3244	-> 6543
      //   #3242	-> 6566
      //   #3240	-> 6589
      //   #3238	-> 6612
      //   #3235	-> 6634
      //   #3233	-> 6656
      //   #3231	-> 6678
      //   #3229	-> 6701
      //   #3226	-> 6724
      //   #3222	-> 6747
      //   #3219	-> 6770
      //   #3215	-> 6793
      //   #3206	-> 6816
      //   #3212	-> 6816
      //   #3208	-> 6839
      //   #3251	-> 6859
      //   #3258	-> 6894
      //   #3260	-> 6948
      //   #3256	-> 6957
      //   #3262	-> 6957
      //   #3263	-> 6964
      //   #3264	-> 6973
      //   #3265	-> 6982
      //   #3266	-> 6995
      //   #3201	-> 7004
      //   #3269	-> 7010
      //   #3270	-> 7010
      //   #3271	-> 7019
      //   #3272	-> 7028
      //   #3273	-> 7038
      //   #3278	-> 7056
      //   #3279	-> 7070
      //   #3282	-> 7102
      //   #3283	-> 7102
      //   #3285	-> 7114
      //   #3286	-> 7127
      //   #3287	-> 7143
    }
  }
  
  public static final class PerformancePoint {
    public static final PerformancePoint FHD_100;
    
    public static final PerformancePoint FHD_120;
    
    public static final PerformancePoint FHD_200;
    
    public static final PerformancePoint FHD_24;
    
    public static final PerformancePoint FHD_240;
    
    public static final PerformancePoint FHD_25;
    
    public static final PerformancePoint FHD_30;
    
    public static final PerformancePoint FHD_50;
    
    public static final PerformancePoint FHD_60;
    
    public static final PerformancePoint HD_100;
    
    public static final PerformancePoint HD_120;
    
    public static final PerformancePoint HD_200;
    
    public static final PerformancePoint HD_24 = new PerformancePoint(1280, 720, 24);
    
    public static final PerformancePoint HD_240;
    
    public static final PerformancePoint HD_25 = new PerformancePoint(1280, 720, 25);
    
    public static final PerformancePoint HD_30 = new PerformancePoint(1280, 720, 30);
    
    public static final PerformancePoint HD_50 = new PerformancePoint(1280, 720, 50);
    
    public static final PerformancePoint HD_60 = new PerformancePoint(1280, 720, 60);
    
    public int getMaxMacroBlocks() {
      return saturateLongToInt(this.mWidth * this.mHeight);
    }
    
    public int getMaxFrameRate() {
      return this.mMaxFrameRate;
    }
    
    public long getMaxMacroBlockRate() {
      return this.mMaxMacroBlockRate;
    }
    
    public String toString() {
      int i = this.mBlockSize.getWidth() * 16;
      int j = this.mBlockSize.getHeight() * 16;
      int k = (int)Utils.divUp(this.mMaxMacroBlockRate, getMaxMacroBlocks());
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(this.mWidth * 16);
      stringBuilder2.append("x");
      stringBuilder2.append(this.mHeight * 16);
      stringBuilder2.append("@");
      stringBuilder2.append(k);
      String str3 = stringBuilder2.toString();
      String str1 = str3;
      if (k < this.mMaxFrameRate) {
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append(str3);
        stringBuilder1.append(", max ");
        stringBuilder1.append(this.mMaxFrameRate);
        stringBuilder1.append("fps");
        str1 = stringBuilder1.toString();
      } 
      if (i <= 16) {
        String str;
        str3 = str1;
        if (j > 16) {
          StringBuilder stringBuilder5 = new StringBuilder();
          stringBuilder5.append(str1);
          stringBuilder5.append(", ");
          stringBuilder5.append(i);
          stringBuilder5.append("x");
          stringBuilder5.append(j);
          stringBuilder5.append(" blocks");
          str = stringBuilder5.toString();
          StringBuilder stringBuilder4 = new StringBuilder();
          stringBuilder4.append("PerformancePoint(");
          stringBuilder4.append(str);
          stringBuilder4.append(")");
          return stringBuilder4.toString();
        } 
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("PerformancePoint(");
        stringBuilder1.append(str);
        stringBuilder1.append(")");
        return stringBuilder1.toString();
      } 
      StringBuilder stringBuilder3 = new StringBuilder();
      stringBuilder3.append((String)stringBuilder1);
      stringBuilder3.append(", ");
      stringBuilder3.append(i);
      stringBuilder3.append("x");
      stringBuilder3.append(j);
      stringBuilder3.append(" blocks");
      String str2 = stringBuilder3.toString();
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("PerformancePoint(");
      stringBuilder1.append(str2);
      stringBuilder1.append(")");
      return stringBuilder1.toString();
    }
    
    public int hashCode() {
      return this.mMaxFrameRate;
    }
    
    public PerformancePoint(int param1Int1, int param1Int2, int param1Int3, int param1Int4, Size param1Size) {
      MediaCodecInfo.checkPowerOfTwo(param1Size.getWidth(), "block width");
      MediaCodecInfo.checkPowerOfTwo(param1Size.getHeight(), "block height");
      int i = Utils.divUp(param1Size.getWidth(), 16);
      this.mBlockSize = new Size(i, Utils.divUp(param1Size.getHeight(), 16));
      long l1 = Math.max(1L, param1Int1);
      long l2 = Math.max(param1Size.getWidth(), 16);
      l2 = Utils.divUp(l1, l2);
      Size size = this.mBlockSize;
      this.mWidth = (int)(l2 * size.getWidth());
      l1 = Math.max(1L, param1Int2);
      l2 = Math.max(param1Size.getHeight(), 16);
      l2 = Utils.divUp(l1, l2);
      param1Size = this.mBlockSize;
      this.mHeight = (int)(l2 * param1Size.getHeight());
      this.mMaxFrameRate = Math.max(1, Math.max(param1Int3, param1Int4));
      this.mMaxMacroBlockRate = (Math.max(1, param1Int3) * getMaxMacroBlocks());
    }
    
    public PerformancePoint(PerformancePoint param1PerformancePoint, Size param1Size) {
      this(i * 16, j * 16, k, m, size);
    }
    
    public PerformancePoint(int param1Int1, int param1Int2, int param1Int3) {
      this(param1Int1, param1Int2, param1Int3, param1Int3, new Size(16, 16));
    }
    
    private int saturateLongToInt(long param1Long) {
      if (param1Long < -2147483648L)
        return Integer.MIN_VALUE; 
      if (param1Long > 2147483647L)
        return Integer.MAX_VALUE; 
      return (int)param1Long;
    }
    
    private int align(int param1Int1, int param1Int2) {
      return Utils.divUp(param1Int1, param1Int2) * param1Int2;
    }
    
    private void checkPowerOfTwo2(int param1Int, String param1String) {
      if (param1Int != 0 && (param1Int - 1 & param1Int) == 0)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(param1String);
      stringBuilder.append(" (");
      stringBuilder.append(param1Int);
      stringBuilder.append(") must be a power of 2");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public boolean covers(MediaFormat param1MediaFormat) {
      int i = param1MediaFormat.getInteger("width", 0);
      int j = param1MediaFormat.getInteger("height", 0);
      Number number = param1MediaFormat.getNumber("frame-rate", Integer.valueOf(0));
      double d = number.doubleValue();
      float f = (float)Math.ceil(d);
      PerformancePoint performancePoint = new PerformancePoint(i, j, Math.round(f));
      return covers(performancePoint);
    }
    
    public boolean covers(PerformancePoint param1PerformancePoint) {
      boolean bool;
      Size size = getCommonBlockSize(param1PerformancePoint);
      PerformancePoint performancePoint = new PerformancePoint(this, size);
      param1PerformancePoint = new PerformancePoint(param1PerformancePoint, size);
      if (performancePoint.getMaxMacroBlocks() >= param1PerformancePoint.getMaxMacroBlocks() && performancePoint.mMaxFrameRate >= param1PerformancePoint.mMaxFrameRate && performancePoint.mMaxMacroBlockRate >= param1PerformancePoint.mMaxMacroBlockRate) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    private Size getCommonBlockSize(PerformancePoint param1PerformancePoint) {
      Size size = this.mBlockSize;
      int i = Math.max(size.getWidth(), param1PerformancePoint.mBlockSize.getWidth());
      size = this.mBlockSize;
      return new Size(i * 16, Math.max(size.getHeight(), param1PerformancePoint.mBlockSize.getHeight()) * 16);
    }
    
    public boolean equals(Object param1Object) {
      boolean bool = param1Object instanceof PerformancePoint;
      boolean bool1 = false;
      if (bool) {
        PerformancePoint performancePoint1 = (PerformancePoint)param1Object;
        Size size = getCommonBlockSize(performancePoint1);
        param1Object = new PerformancePoint(this, size);
        PerformancePoint performancePoint2 = new PerformancePoint(performancePoint1, size);
        bool = bool1;
        if (param1Object.getMaxMacroBlocks() == performancePoint2.getMaxMacroBlocks()) {
          bool = bool1;
          if (((PerformancePoint)param1Object).mMaxFrameRate == performancePoint2.mMaxFrameRate) {
            bool = bool1;
            if (((PerformancePoint)param1Object).mMaxMacroBlockRate == performancePoint2.mMaxMacroBlockRate)
              bool = true; 
          } 
        } 
        return bool;
      } 
      return false;
    }
    
    public static final PerformancePoint SD_24 = new PerformancePoint(720, 480, 24);
    
    public static final PerformancePoint SD_25 = new PerformancePoint(720, 576, 25);
    
    public static final PerformancePoint SD_30 = new PerformancePoint(720, 480, 30);
    
    public static final PerformancePoint SD_48 = new PerformancePoint(720, 480, 48);
    
    public static final PerformancePoint SD_50 = new PerformancePoint(720, 576, 50);
    
    public static final PerformancePoint SD_60 = new PerformancePoint(720, 480, 60);
    
    public static final PerformancePoint UHD_100;
    
    public static final PerformancePoint UHD_120;
    
    public static final PerformancePoint UHD_200;
    
    public static final PerformancePoint UHD_24;
    
    public static final PerformancePoint UHD_240;
    
    public static final PerformancePoint UHD_25;
    
    public static final PerformancePoint UHD_30;
    
    public static final PerformancePoint UHD_50;
    
    public static final PerformancePoint UHD_60;
    
    private Size mBlockSize;
    
    private int mHeight;
    
    private int mMaxFrameRate;
    
    private long mMaxMacroBlockRate;
    
    private int mWidth;
    
    static {
      HD_100 = new PerformancePoint(1280, 720, 100);
      HD_120 = new PerformancePoint(1280, 720, 120);
      HD_200 = new PerformancePoint(1280, 720, 200);
      HD_240 = new PerformancePoint(1280, 720, 240);
      FHD_24 = new PerformancePoint(1920, 1080, 24);
      FHD_25 = new PerformancePoint(1920, 1080, 25);
      FHD_30 = new PerformancePoint(1920, 1080, 30);
      FHD_50 = new PerformancePoint(1920, 1080, 50);
      FHD_60 = new PerformancePoint(1920, 1080, 60);
      FHD_100 = new PerformancePoint(1920, 1080, 100);
      FHD_120 = new PerformancePoint(1920, 1080, 120);
      FHD_200 = new PerformancePoint(1920, 1080, 200);
      FHD_240 = new PerformancePoint(1920, 1080, 240);
      UHD_24 = new PerformancePoint(3840, 2160, 24);
      UHD_25 = new PerformancePoint(3840, 2160, 25);
      UHD_30 = new PerformancePoint(3840, 2160, 30);
      UHD_50 = new PerformancePoint(3840, 2160, 50);
      UHD_60 = new PerformancePoint(3840, 2160, 60);
      UHD_100 = new PerformancePoint(3840, 2160, 100);
      UHD_120 = new PerformancePoint(3840, 2160, 120);
      UHD_200 = new PerformancePoint(3840, 2160, 200);
      UHD_240 = new PerformancePoint(3840, 2160, 240);
    }
  }
  
  public static final class EncoderCapabilities {
    public static final int BITRATE_MODE_CBR = 2;
    
    public static final int BITRATE_MODE_CQ = 0;
    
    public static final int BITRATE_MODE_VBR = 1;
    
    public Range<Integer> getQualityRange() {
      return this.mQualityRange;
    }
    
    public Range<Integer> getComplexityRange() {
      return this.mComplexityRange;
    }
    
    private static final MediaCodecInfo.Feature[] bitrates = new MediaCodecInfo.Feature[] { new MediaCodecInfo.Feature("VBR", 1, true), new MediaCodecInfo.Feature("CBR", 2, false), new MediaCodecInfo.Feature("CQ", 0, false) };
    
    private int mBitControl;
    
    private Range<Integer> mComplexityRange;
    
    private Integer mDefaultComplexity;
    
    private Integer mDefaultQuality;
    
    private MediaCodecInfo.CodecCapabilities mParent;
    
    private Range<Integer> mQualityRange;
    
    private String mQualityScale;
    
    private static int parseBitrateMode(String param1String) {
      for (MediaCodecInfo.Feature feature : bitrates) {
        if (feature.mName.equalsIgnoreCase(param1String))
          return feature.mValue; 
      } 
      return 0;
    }
    
    public boolean isBitrateModeSupported(int param1Int) {
      MediaCodecInfo.Feature[] arrayOfFeature;
      int i;
      boolean bool;
      byte b;
      for (arrayOfFeature = bitrates, i = arrayOfFeature.length, bool = false, b = 0; b < i; ) {
        MediaCodecInfo.Feature feature = arrayOfFeature[b];
        if (param1Int == feature.mValue) {
          if ((this.mBitControl & 1 << param1Int) != 0)
            bool = true; 
          return bool;
        } 
        b++;
      } 
      return false;
    }
    
    public static EncoderCapabilities create(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      EncoderCapabilities encoderCapabilities = new EncoderCapabilities();
      encoderCapabilities.init(param1MediaFormat, param1CodecCapabilities);
      return encoderCapabilities;
    }
    
    private void init(MediaFormat param1MediaFormat, MediaCodecInfo.CodecCapabilities param1CodecCapabilities) {
      this.mParent = param1CodecCapabilities;
      Integer integer = Integer.valueOf(0);
      this.mComplexityRange = Range.create(integer, integer);
      this.mQualityRange = Range.create(integer, integer);
      this.mBitControl = 2;
      applyLevelLimits();
      parseFromInfo(param1MediaFormat);
    }
    
    private void applyLevelLimits() {
      String str = this.mParent.getMimeType();
      if (str.equalsIgnoreCase("audio/flac")) {
        this.mComplexityRange = Range.create(Integer.valueOf(0), Integer.valueOf(8));
        this.mBitControl = 1;
      } else if (str.equalsIgnoreCase("audio/3gpp") || 
        str.equalsIgnoreCase("audio/amr-wb") || 
        str.equalsIgnoreCase("audio/g711-alaw") || 
        str.equalsIgnoreCase("audio/g711-mlaw") || 
        str.equalsIgnoreCase("audio/gsm")) {
        this.mBitControl = 4;
      } 
    }
    
    private void parseFromInfo(MediaFormat param1MediaFormat) {
      Map<String, Object> map = param1MediaFormat.getMap();
      if (param1MediaFormat.containsKey("complexity-range"))
        this.mComplexityRange = Utils.parseIntRange(param1MediaFormat.getString("complexity-range"), this.mComplexityRange); 
      if (param1MediaFormat.containsKey("quality-range"))
        this.mQualityRange = Utils.parseIntRange(param1MediaFormat.getString("quality-range"), this.mQualityRange); 
      if (param1MediaFormat.containsKey("feature-bitrate-modes"))
        for (String str : param1MediaFormat.getString("feature-bitrate-modes").split(","))
          this.mBitControl |= 1 << parseBitrateMode(str);  
      try {
        this.mDefaultComplexity = Integer.valueOf(Integer.parseInt((String)map.get("complexity-default")));
      } catch (NumberFormatException numberFormatException) {}
      try {
        this.mDefaultQuality = Integer.valueOf(Integer.parseInt((String)map.get("quality-default")));
      } catch (NumberFormatException numberFormatException) {}
      this.mQualityScale = (String)map.get("quality-scale");
    }
    
    private boolean supports(Integer param1Integer1, Integer param1Integer2, Integer param1Integer3) {
      boolean bool1 = true;
      boolean bool2 = bool1;
      if (true) {
        bool2 = bool1;
        if (param1Integer1 != null)
          bool2 = this.mComplexityRange.contains(param1Integer1); 
      } 
      bool1 = bool2;
      if (bool2) {
        bool1 = bool2;
        if (param1Integer2 != null)
          bool1 = this.mQualityRange.contains(param1Integer2); 
      } 
      bool2 = bool1;
      if (bool1) {
        bool2 = bool1;
        if (param1Integer3 != null) {
          MediaCodecInfo.CodecProfileLevel codecProfileLevel, arrayOfCodecProfileLevel[] = this.mParent.profileLevels;
          int i = arrayOfCodecProfileLevel.length;
          bool1 = false;
          byte b = 0;
          while (true) {
            param1Integer1 = param1Integer3;
            if (b < i) {
              codecProfileLevel = arrayOfCodecProfileLevel[b];
              if (codecProfileLevel.profile == param1Integer3.intValue()) {
                codecProfileLevel = null;
                break;
              } 
              b++;
              continue;
            } 
            break;
          } 
          if (codecProfileLevel == null)
            bool1 = true; 
          bool2 = bool1;
        } 
      } 
      return bool2;
    }
    
    public void getDefaultFormat(MediaFormat param1MediaFormat) {
      if (!((Integer)this.mQualityRange.getUpper()).equals(this.mQualityRange.getLower())) {
        Integer integer = this.mDefaultQuality;
        if (integer != null)
          param1MediaFormat.setInteger("quality", integer.intValue()); 
      } 
      if (!((Integer)this.mComplexityRange.getUpper()).equals(this.mComplexityRange.getLower())) {
        Integer integer = this.mDefaultComplexity;
        if (integer != null)
          param1MediaFormat.setInteger("complexity", integer.intValue()); 
      } 
      for (MediaCodecInfo.Feature feature : bitrates) {
        if ((this.mBitControl & 1 << feature.mValue) != 0) {
          param1MediaFormat.setInteger("bitrate-mode", feature.mValue);
          break;
        } 
      } 
    }
    
    public boolean supportsFormat(MediaFormat param1MediaFormat) {
      Map<String, Object> map = param1MediaFormat.getMap();
      String str = this.mParent.getMimeType();
      Integer integer1 = (Integer)map.get("bitrate-mode");
      if (integer1 != null && !isBitrateModeSupported(integer1.intValue()))
        return false; 
      Integer integer2 = (Integer)map.get("complexity");
      integer1 = integer2;
      if ("audio/flac".equalsIgnoreCase(str)) {
        Integer integer = (Integer)map.get("flac-compression-level");
        if (integer2 == null) {
          integer1 = integer;
        } else {
          integer1 = integer2;
          if (integer != null)
            if (integer2.equals(integer)) {
              integer1 = integer2;
            } else {
              throw new IllegalArgumentException("conflicting values for complexity and flac-compression-level");
            }  
        } 
      } 
      integer2 = (Integer)map.get("profile");
      Integer integer3 = integer2;
      if ("audio/mp4a-latm".equalsIgnoreCase(str)) {
        Integer integer = (Integer)map.get("aac-profile");
        if (integer2 == null) {
          integer3 = integer;
        } else {
          integer3 = integer2;
          if (integer != null)
            if (integer.equals(integer2)) {
              integer3 = integer2;
            } else {
              throw new IllegalArgumentException("conflicting values for profile and aac-profile");
            }  
        } 
      } 
      integer2 = (Integer)map.get("quality");
      return supports(integer1, integer2, integer3);
    }
  }
  
  public static final class CodecProfileLevel {
    public static final int AACObjectELD = 39;
    
    public static final int AACObjectERLC = 17;
    
    public static final int AACObjectERScalable = 20;
    
    public static final int AACObjectHE = 5;
    
    public static final int AACObjectHE_PS = 29;
    
    public static final int AACObjectLC = 2;
    
    public static final int AACObjectLD = 23;
    
    public static final int AACObjectLTP = 4;
    
    public static final int AACObjectMain = 1;
    
    public static final int AACObjectSSR = 3;
    
    public static final int AACObjectScalable = 6;
    
    public static final int AACObjectXHE = 42;
    
    public static final int AV1Level2 = 1;
    
    public static final int AV1Level21 = 2;
    
    public static final int AV1Level22 = 4;
    
    public static final int AV1Level23 = 8;
    
    public static final int AV1Level3 = 16;
    
    public static final int AV1Level31 = 32;
    
    public static final int AV1Level32 = 64;
    
    public static final int AV1Level33 = 128;
    
    public static final int AV1Level4 = 256;
    
    public static final int AV1Level41 = 512;
    
    public static final int AV1Level42 = 1024;
    
    public static final int AV1Level43 = 2048;
    
    public static final int AV1Level5 = 4096;
    
    public static final int AV1Level51 = 8192;
    
    public static final int AV1Level52 = 16384;
    
    public static final int AV1Level53 = 32768;
    
    public static final int AV1Level6 = 65536;
    
    public static final int AV1Level61 = 131072;
    
    public static final int AV1Level62 = 262144;
    
    public static final int AV1Level63 = 524288;
    
    public static final int AV1Level7 = 1048576;
    
    public static final int AV1Level71 = 2097152;
    
    public static final int AV1Level72 = 4194304;
    
    public static final int AV1Level73 = 8388608;
    
    public static final int AV1ProfileMain10 = 2;
    
    public static final int AV1ProfileMain10HDR10 = 4096;
    
    public static final int AV1ProfileMain10HDR10Plus = 8192;
    
    public static final int AV1ProfileMain8 = 1;
    
    public static final int AVCLevel1 = 1;
    
    public static final int AVCLevel11 = 4;
    
    public static final int AVCLevel12 = 8;
    
    public static final int AVCLevel13 = 16;
    
    public static final int AVCLevel1b = 2;
    
    public static final int AVCLevel2 = 32;
    
    public static final int AVCLevel21 = 64;
    
    public static final int AVCLevel22 = 128;
    
    public static final int AVCLevel3 = 256;
    
    public static final int AVCLevel31 = 512;
    
    public static final int AVCLevel32 = 1024;
    
    public static final int AVCLevel4 = 2048;
    
    public static final int AVCLevel41 = 4096;
    
    public static final int AVCLevel42 = 8192;
    
    public static final int AVCLevel5 = 16384;
    
    public static final int AVCLevel51 = 32768;
    
    public static final int AVCLevel52 = 65536;
    
    public static final int AVCLevel6 = 131072;
    
    public static final int AVCLevel61 = 262144;
    
    public static final int AVCLevel62 = 524288;
    
    public static final int AVCProfileBaseline = 1;
    
    public static final int AVCProfileConstrainedBaseline = 65536;
    
    public static final int AVCProfileConstrainedHigh = 524288;
    
    public static final int AVCProfileExtended = 4;
    
    public static final int AVCProfileHigh = 8;
    
    public static final int AVCProfileHigh10 = 16;
    
    public static final int AVCProfileHigh422 = 32;
    
    public static final int AVCProfileHigh444 = 64;
    
    public static final int AVCProfileMain = 2;
    
    public static final int DolbyVisionLevelFhd24 = 4;
    
    public static final int DolbyVisionLevelFhd30 = 8;
    
    public static final int DolbyVisionLevelFhd60 = 16;
    
    public static final int DolbyVisionLevelHd24 = 1;
    
    public static final int DolbyVisionLevelHd30 = 2;
    
    public static final int DolbyVisionLevelUhd24 = 32;
    
    public static final int DolbyVisionLevelUhd30 = 64;
    
    public static final int DolbyVisionLevelUhd48 = 128;
    
    public static final int DolbyVisionLevelUhd60 = 256;
    
    public static final int DolbyVisionProfileDvav110 = 1024;
    
    public static final int DolbyVisionProfileDvavPen = 2;
    
    public static final int DolbyVisionProfileDvavPer = 1;
    
    public static final int DolbyVisionProfileDvavSe = 512;
    
    public static final int DolbyVisionProfileDvheDen = 8;
    
    public static final int DolbyVisionProfileDvheDer = 4;
    
    public static final int DolbyVisionProfileDvheDtb = 128;
    
    public static final int DolbyVisionProfileDvheDth = 64;
    
    public static final int DolbyVisionProfileDvheDtr = 16;
    
    public static final int DolbyVisionProfileDvheSt = 256;
    
    public static final int DolbyVisionProfileDvheStn = 32;
    
    public static final int H263Level10 = 1;
    
    public static final int H263Level20 = 2;
    
    public static final int H263Level30 = 4;
    
    public static final int H263Level40 = 8;
    
    public static final int H263Level45 = 16;
    
    public static final int H263Level50 = 32;
    
    public static final int H263Level60 = 64;
    
    public static final int H263Level70 = 128;
    
    public static final int H263ProfileBackwardCompatible = 4;
    
    public static final int H263ProfileBaseline = 1;
    
    public static final int H263ProfileH320Coding = 2;
    
    public static final int H263ProfileHighCompression = 32;
    
    public static final int H263ProfileHighLatency = 256;
    
    public static final int H263ProfileISWV2 = 8;
    
    public static final int H263ProfileISWV3 = 16;
    
    public static final int H263ProfileInterlace = 128;
    
    public static final int H263ProfileInternet = 64;
    
    public static final int HEVCHighTierLevel1 = 2;
    
    public static final int HEVCHighTierLevel2 = 8;
    
    public static final int HEVCHighTierLevel21 = 32;
    
    public static final int HEVCHighTierLevel3 = 128;
    
    public static final int HEVCHighTierLevel31 = 512;
    
    public static final int HEVCHighTierLevel4 = 2048;
    
    public static final int HEVCHighTierLevel41 = 8192;
    
    public static final int HEVCHighTierLevel5 = 32768;
    
    public static final int HEVCHighTierLevel51 = 131072;
    
    public static final int HEVCHighTierLevel52 = 524288;
    
    public static final int HEVCHighTierLevel6 = 2097152;
    
    public static final int HEVCHighTierLevel61 = 8388608;
    
    public static final int HEVCHighTierLevel62 = 33554432;
    
    private static final int HEVCHighTierLevels = 44739242;
    
    public static final int HEVCMainTierLevel1 = 1;
    
    public static final int HEVCMainTierLevel2 = 4;
    
    public static final int HEVCMainTierLevel21 = 16;
    
    public static final int HEVCMainTierLevel3 = 64;
    
    public static final int HEVCMainTierLevel31 = 256;
    
    public static final int HEVCMainTierLevel4 = 1024;
    
    public static final int HEVCMainTierLevel41 = 4096;
    
    public static final int HEVCMainTierLevel5 = 16384;
    
    public static final int HEVCMainTierLevel51 = 65536;
    
    public static final int HEVCMainTierLevel52 = 262144;
    
    public static final int HEVCMainTierLevel6 = 1048576;
    
    public static final int HEVCMainTierLevel61 = 4194304;
    
    public static final int HEVCMainTierLevel62 = 16777216;
    
    public static final int HEVCProfileMain = 1;
    
    public static final int HEVCProfileMain10 = 2;
    
    public static final int HEVCProfileMain10HDR10 = 4096;
    
    public static final int HEVCProfileMain10HDR10Plus = 8192;
    
    public static final int HEVCProfileMainStill = 4;
    
    public static final int MPEG2LevelH14 = 2;
    
    public static final int MPEG2LevelHL = 3;
    
    public static final int MPEG2LevelHP = 4;
    
    public static final int MPEG2LevelLL = 0;
    
    public static final int MPEG2LevelML = 1;
    
    public static final int MPEG2Profile422 = 2;
    
    public static final int MPEG2ProfileHigh = 5;
    
    public static final int MPEG2ProfileMain = 1;
    
    public static final int MPEG2ProfileSNR = 3;
    
    public static final int MPEG2ProfileSimple = 0;
    
    public static final int MPEG2ProfileSpatial = 4;
    
    public static final int MPEG4Level0 = 1;
    
    public static final int MPEG4Level0b = 2;
    
    public static final int MPEG4Level1 = 4;
    
    public static final int MPEG4Level2 = 8;
    
    public static final int MPEG4Level3 = 16;
    
    public static final int MPEG4Level3b = 24;
    
    public static final int MPEG4Level4 = 32;
    
    public static final int MPEG4Level4a = 64;
    
    public static final int MPEG4Level5 = 128;
    
    public static final int MPEG4Level6 = 256;
    
    public static final int MPEG4ProfileAdvancedCoding = 4096;
    
    public static final int MPEG4ProfileAdvancedCore = 8192;
    
    public static final int MPEG4ProfileAdvancedRealTime = 1024;
    
    public static final int MPEG4ProfileAdvancedScalable = 16384;
    
    public static final int MPEG4ProfileAdvancedSimple = 32768;
    
    public static final int MPEG4ProfileBasicAnimated = 256;
    
    public static final int MPEG4ProfileCore = 4;
    
    public static final int MPEG4ProfileCoreScalable = 2048;
    
    public static final int MPEG4ProfileHybrid = 512;
    
    public static final int MPEG4ProfileMain = 8;
    
    public static final int MPEG4ProfileNbit = 16;
    
    public static final int MPEG4ProfileScalableTexture = 32;
    
    public static final int MPEG4ProfileSimple = 1;
    
    public static final int MPEG4ProfileSimpleFBA = 128;
    
    public static final int MPEG4ProfileSimpleFace = 64;
    
    public static final int MPEG4ProfileSimpleScalable = 2;
    
    public static final int VP8Level_Version0 = 1;
    
    public static final int VP8Level_Version1 = 2;
    
    public static final int VP8Level_Version2 = 4;
    
    public static final int VP8Level_Version3 = 8;
    
    public static final int VP8ProfileMain = 1;
    
    public static final int VP9Level1 = 1;
    
    public static final int VP9Level11 = 2;
    
    public static final int VP9Level2 = 4;
    
    public static final int VP9Level21 = 8;
    
    public static final int VP9Level3 = 16;
    
    public static final int VP9Level31 = 32;
    
    public static final int VP9Level4 = 64;
    
    public static final int VP9Level41 = 128;
    
    public static final int VP9Level5 = 256;
    
    public static final int VP9Level51 = 512;
    
    public static final int VP9Level52 = 1024;
    
    public static final int VP9Level6 = 2048;
    
    public static final int VP9Level61 = 4096;
    
    public static final int VP9Level62 = 8192;
    
    public static final int VP9Profile0 = 1;
    
    public static final int VP9Profile1 = 2;
    
    public static final int VP9Profile2 = 4;
    
    public static final int VP9Profile2HDR = 4096;
    
    public static final int VP9Profile2HDR10Plus = 16384;
    
    public static final int VP9Profile3 = 8;
    
    public static final int VP9Profile3HDR = 8192;
    
    public static final int VP9Profile3HDR10Plus = 32768;
    
    public int level;
    
    public int profile;
    
    public boolean equals(Object param1Object) {
      boolean bool = false;
      if (param1Object == null)
        return false; 
      if (param1Object instanceof CodecProfileLevel) {
        param1Object = param1Object;
        boolean bool1 = bool;
        if (((CodecProfileLevel)param1Object).profile == this.profile) {
          bool1 = bool;
          if (((CodecProfileLevel)param1Object).level == this.level)
            bool1 = true; 
        } 
        return bool1;
      } 
      return false;
    }
    
    public int hashCode() {
      return Long.hashCode(this.profile << 32L | this.level);
    }
  }
  
  public final CodecCapabilities getCapabilitiesForType(String paramString) {
    CodecCapabilities codecCapabilities = this.mCaps.get(paramString);
    if (codecCapabilities != null)
      return codecCapabilities.dup(); 
    throw new IllegalArgumentException("codec does not support type");
  }
  
  public MediaCodecInfo makeRegular() {
    ArrayList<CodecCapabilities> arrayList = new ArrayList();
    for (CodecCapabilities codecCapabilities : this.mCaps.values()) {
      if (codecCapabilities.isRegular())
        arrayList.add(codecCapabilities); 
    } 
    if (arrayList.size() == 0)
      return null; 
    if (arrayList.size() == this.mCaps.size())
      return this; 
    String str2 = this.mName, str1 = this.mCanonicalName;
    int i = this.mFlags;
    return 
      
      new MediaCodecInfo(str2, str1, i, arrayList.<CodecCapabilities>toArray(new CodecCapabilities[arrayList.size()]));
  }
}
