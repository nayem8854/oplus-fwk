package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface IMediaRouter2Manager extends IInterface {
  void notifyPreferredFeaturesChanged(String paramString, List<String> paramList) throws RemoteException;
  
  void notifyRequestFailed(int paramInt1, int paramInt2) throws RemoteException;
  
  void notifyRoutesAdded(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifyRoutesChanged(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifyRoutesRemoved(List<MediaRoute2Info> paramList) throws RemoteException;
  
  void notifySessionCreated(int paramInt, RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionReleased(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  void notifySessionUpdated(RoutingSessionInfo paramRoutingSessionInfo) throws RemoteException;
  
  class Default implements IMediaRouter2Manager {
    public void notifySessionCreated(int param1Int, RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionUpdated(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifySessionReleased(RoutingSessionInfo param1RoutingSessionInfo) throws RemoteException {}
    
    public void notifyPreferredFeaturesChanged(String param1String, List<String> param1List) throws RemoteException {}
    
    public void notifyRoutesAdded(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifyRoutesRemoved(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifyRoutesChanged(List<MediaRoute2Info> param1List) throws RemoteException {}
    
    public void notifyRequestFailed(int param1Int1, int param1Int2) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaRouter2Manager {
    private static final String DESCRIPTOR = "android.media.IMediaRouter2Manager";
    
    static final int TRANSACTION_notifyPreferredFeaturesChanged = 4;
    
    static final int TRANSACTION_notifyRequestFailed = 8;
    
    static final int TRANSACTION_notifyRoutesAdded = 5;
    
    static final int TRANSACTION_notifyRoutesChanged = 7;
    
    static final int TRANSACTION_notifyRoutesRemoved = 6;
    
    static final int TRANSACTION_notifySessionCreated = 1;
    
    static final int TRANSACTION_notifySessionReleased = 3;
    
    static final int TRANSACTION_notifySessionUpdated = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaRouter2Manager");
    }
    
    public static IMediaRouter2Manager asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaRouter2Manager");
      if (iInterface != null && iInterface instanceof IMediaRouter2Manager)
        return (IMediaRouter2Manager)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 8:
          return "notifyRequestFailed";
        case 7:
          return "notifyRoutesChanged";
        case 6:
          return "notifyRoutesRemoved";
        case 5:
          return "notifyRoutesAdded";
        case 4:
          return "notifyPreferredFeaturesChanged";
        case 3:
          return "notifySessionReleased";
        case 2:
          return "notifySessionUpdated";
        case 1:
          break;
      } 
      return "notifySessionCreated";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        ArrayList<MediaRoute2Info> arrayList;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 8:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2Manager");
            param1Int1 = param1Parcel1.readInt();
            param1Int2 = param1Parcel1.readInt();
            notifyRequestFailed(param1Int1, param1Int2);
            return true;
          case 7:
            param1Parcel1.enforceInterface("android.media.IMediaRouter2Manager");
            arrayList = param1Parcel1.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesChanged(arrayList);
            return true;
          case 6:
            arrayList.enforceInterface("android.media.IMediaRouter2Manager");
            arrayList = arrayList.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesRemoved(arrayList);
            return true;
          case 5:
            arrayList.enforceInterface("android.media.IMediaRouter2Manager");
            arrayList = arrayList.createTypedArrayList(MediaRoute2Info.CREATOR);
            notifyRoutesAdded(arrayList);
            return true;
          case 4:
            arrayList.enforceInterface("android.media.IMediaRouter2Manager");
            str = arrayList.readString();
            arrayList = (ArrayList)arrayList.createStringArrayList();
            notifyPreferredFeaturesChanged(str, (List)arrayList);
            return true;
          case 3:
            arrayList.enforceInterface("android.media.IMediaRouter2Manager");
            if (arrayList.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            notifySessionReleased((RoutingSessionInfo)arrayList);
            return true;
          case 2:
            arrayList.enforceInterface("android.media.IMediaRouter2Manager");
            if (arrayList.readInt() != 0) {
              RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)arrayList);
            } else {
              arrayList = null;
            } 
            notifySessionUpdated((RoutingSessionInfo)arrayList);
            return true;
          case 1:
            break;
        } 
        arrayList.enforceInterface("android.media.IMediaRouter2Manager");
        param1Int1 = arrayList.readInt();
        if (arrayList.readInt() != 0) {
          RoutingSessionInfo routingSessionInfo = RoutingSessionInfo.CREATOR.createFromParcel((Parcel)arrayList);
        } else {
          arrayList = null;
        } 
        notifySessionCreated(param1Int1, (RoutingSessionInfo)arrayList);
        return true;
      } 
      str.writeString("android.media.IMediaRouter2Manager");
      return true;
    }
    
    private static class Proxy implements IMediaRouter2Manager {
      public static IMediaRouter2Manager sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaRouter2Manager";
      }
      
      public void notifySessionCreated(int param2Int, RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeInt(param2Int);
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifySessionCreated(param2Int, param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionUpdated(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifySessionUpdated(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifySessionReleased(RoutingSessionInfo param2RoutingSessionInfo) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          if (param2RoutingSessionInfo != null) {
            parcel.writeInt(1);
            param2RoutingSessionInfo.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifySessionReleased(param2RoutingSessionInfo);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyPreferredFeaturesChanged(String param2String, List<String> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeString(param2String);
          parcel.writeStringList(param2List);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifyPreferredFeaturesChanged(param2String, param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesAdded(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifyRoutesAdded(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesRemoved(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifyRoutesRemoved(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRoutesChanged(List<MediaRoute2Info> param2List) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeTypedList(param2List);
          boolean bool = this.mRemote.transact(7, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifyRoutesChanged(param2List);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void notifyRequestFailed(int param2Int1, int param2Int2) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaRouter2Manager");
          parcel.writeInt(param2Int1);
          parcel.writeInt(param2Int2);
          boolean bool = this.mRemote.transact(8, parcel, (Parcel)null, 1);
          if (!bool && IMediaRouter2Manager.Stub.getDefaultImpl() != null) {
            IMediaRouter2Manager.Stub.getDefaultImpl().notifyRequestFailed(param2Int1, param2Int2);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaRouter2Manager param1IMediaRouter2Manager) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaRouter2Manager != null) {
          Proxy.sDefaultImpl = param1IMediaRouter2Manager;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaRouter2Manager getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
