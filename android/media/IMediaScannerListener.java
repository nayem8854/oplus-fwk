package android.media;

import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaScannerListener extends IInterface {
  void scanCompleted(String paramString, Uri paramUri) throws RemoteException;
  
  class Default implements IMediaScannerListener {
    public void scanCompleted(String param1String, Uri param1Uri) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaScannerListener {
    private static final String DESCRIPTOR = "android.media.IMediaScannerListener";
    
    static final int TRANSACTION_scanCompleted = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaScannerListener");
    }
    
    public static IMediaScannerListener asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaScannerListener");
      if (iInterface != null && iInterface instanceof IMediaScannerListener)
        return (IMediaScannerListener)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "scanCompleted";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IMediaScannerListener");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IMediaScannerListener");
      String str = param1Parcel1.readString();
      if (param1Parcel1.readInt() != 0) {
        Uri uri = Uri.CREATOR.createFromParcel(param1Parcel1);
      } else {
        param1Parcel1 = null;
      } 
      scanCompleted(str, (Uri)param1Parcel1);
      return true;
    }
    
    private static class Proxy implements IMediaScannerListener {
      public static IMediaScannerListener sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaScannerListener";
      }
      
      public void scanCompleted(String param2String, Uri param2Uri) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IMediaScannerListener");
          parcel.writeString(param2String);
          if (param2Uri != null) {
            parcel.writeInt(1);
            param2Uri.writeToParcel(parcel, 0);
          } else {
            parcel.writeInt(0);
          } 
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IMediaScannerListener.Stub.getDefaultImpl() != null) {
            IMediaScannerListener.Stub.getDefaultImpl().scanCompleted(param2String, param2Uri);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaScannerListener param1IMediaScannerListener) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaScannerListener != null) {
          Proxy.sDefaultImpl = param1IMediaScannerListener;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaScannerListener getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
