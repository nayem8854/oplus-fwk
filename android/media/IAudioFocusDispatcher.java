package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IAudioFocusDispatcher extends IInterface {
  void dispatchAudioFocusChange(int paramInt, String paramString) throws RemoteException;
  
  void dispatchFocusResultFromExtPolicy(int paramInt, String paramString) throws RemoteException;
  
  class Default implements IAudioFocusDispatcher {
    public void dispatchAudioFocusChange(int param1Int, String param1String) throws RemoteException {}
    
    public void dispatchFocusResultFromExtPolicy(int param1Int, String param1String) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IAudioFocusDispatcher {
    private static final String DESCRIPTOR = "android.media.IAudioFocusDispatcher";
    
    static final int TRANSACTION_dispatchAudioFocusChange = 1;
    
    static final int TRANSACTION_dispatchFocusResultFromExtPolicy = 2;
    
    public Stub() {
      attachInterface(this, "android.media.IAudioFocusDispatcher");
    }
    
    public static IAudioFocusDispatcher asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IAudioFocusDispatcher");
      if (iInterface != null && iInterface instanceof IAudioFocusDispatcher)
        return (IAudioFocusDispatcher)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1) {
        if (param1Int != 2)
          return null; 
        return "dispatchFocusResultFromExtPolicy";
      } 
      return "dispatchAudioFocusChange";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 2) {
          if (param1Int1 != 1598968902)
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
          param1Parcel2.writeString("android.media.IAudioFocusDispatcher");
          return true;
        } 
        param1Parcel1.enforceInterface("android.media.IAudioFocusDispatcher");
        param1Int1 = param1Parcel1.readInt();
        str = param1Parcel1.readString();
        dispatchFocusResultFromExtPolicy(param1Int1, str);
        return true;
      } 
      str.enforceInterface("android.media.IAudioFocusDispatcher");
      param1Int1 = str.readInt();
      String str = str.readString();
      dispatchAudioFocusChange(param1Int1, str);
      return true;
    }
    
    private static class Proxy implements IAudioFocusDispatcher {
      public static IAudioFocusDispatcher sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IAudioFocusDispatcher";
      }
      
      public void dispatchAudioFocusChange(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioFocusDispatcher");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IAudioFocusDispatcher.Stub.getDefaultImpl() != null) {
            IAudioFocusDispatcher.Stub.getDefaultImpl().dispatchAudioFocusChange(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void dispatchFocusResultFromExtPolicy(int param2Int, String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IAudioFocusDispatcher");
          parcel.writeInt(param2Int);
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IAudioFocusDispatcher.Stub.getDefaultImpl() != null) {
            IAudioFocusDispatcher.Stub.getDefaultImpl().dispatchFocusResultFromExtPolicy(param2Int, param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IAudioFocusDispatcher param1IAudioFocusDispatcher) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IAudioFocusDispatcher != null) {
          Proxy.sDefaultImpl = param1IAudioFocusDispatcher;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IAudioFocusDispatcher getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
