package android.media;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public final class MediaFormat {
  public static final int COLOR_RANGE_FULL = 1;
  
  public static final int COLOR_RANGE_LIMITED = 2;
  
  public static final int COLOR_STANDARD_BT2020 = 6;
  
  public static final int COLOR_STANDARD_BT601_NTSC = 4;
  
  public static final int COLOR_STANDARD_BT601_PAL = 2;
  
  public static final int COLOR_STANDARD_BT709 = 1;
  
  public static final int COLOR_TRANSFER_HLG = 7;
  
  public static final int COLOR_TRANSFER_LINEAR = 1;
  
  public static final int COLOR_TRANSFER_SDR_VIDEO = 3;
  
  public static final int COLOR_TRANSFER_ST2084 = 6;
  
  public static final String KEY_AAC_DRC_ALBUM_MODE = "aac-drc-album-mode";
  
  public static final String KEY_AAC_DRC_ATTENUATION_FACTOR = "aac-drc-cut-level";
  
  public static final String KEY_AAC_DRC_BOOST_FACTOR = "aac-drc-boost-level";
  
  public static final String KEY_AAC_DRC_EFFECT_TYPE = "aac-drc-effect-type";
  
  public static final String KEY_AAC_DRC_HEAVY_COMPRESSION = "aac-drc-heavy-compression";
  
  public static final String KEY_AAC_DRC_OUTPUT_LOUDNESS = "aac-drc-output-loudness";
  
  public static final String KEY_AAC_DRC_TARGET_REFERENCE_LEVEL = "aac-target-ref-level";
  
  public static final String KEY_AAC_ENCODED_TARGET_LEVEL = "aac-encoded-target-level";
  
  public static final String KEY_AAC_MAX_OUTPUT_CHANNEL_COUNT = "aac-max-output-channel_count";
  
  public static final String KEY_AAC_PROFILE = "aac-profile";
  
  public static final String KEY_AAC_SBR_MODE = "aac-sbr-mode";
  
  public static final String KEY_AUDIO_SESSION_ID = "audio-session-id";
  
  public static final String KEY_BITRATE_MODE = "bitrate-mode";
  
  public static final String KEY_BIT_RATE = "bitrate";
  
  public static final String KEY_CAPTION_SERVICE_NUMBER = "caption-service-number";
  
  public static final String KEY_CAPTURE_RATE = "capture-rate";
  
  public static final String KEY_CA_PRIVATE_DATA = "ca-private-data";
  
  public static final String KEY_CA_SESSION_ID = "ca-session-id";
  
  public static final String KEY_CA_SYSTEM_ID = "ca-system-id";
  
  public static final String KEY_CHANNEL_COUNT = "channel-count";
  
  public static final String KEY_CHANNEL_MASK = "channel-mask";
  
  public static final String KEY_CODECS_STRING = "codecs-string";
  
  public static final String KEY_COLOR_FORMAT = "color-format";
  
  public static final String KEY_COLOR_RANGE = "color-range";
  
  public static final String KEY_COLOR_STANDARD = "color-standard";
  
  public static final String KEY_COLOR_TRANSFER = "color-transfer";
  
  public static final String KEY_COMPLEXITY = "complexity";
  
  public static final String KEY_CREATE_INPUT_SURFACE_SUSPENDED = "create-input-buffers-suspended";
  
  public static final String KEY_DURATION = "durationUs";
  
  public static final String KEY_ENCODER_DELAY = "encoder-delay";
  
  public static final String KEY_ENCODER_PADDING = "encoder-padding";
  
  public static final String KEY_FEATURE_ = "feature-";
  
  public static final String KEY_FLAC_COMPRESSION_LEVEL = "flac-compression-level";
  
  public static final String KEY_FRAME_RATE = "frame-rate";
  
  public static final String KEY_GRID_COLUMNS = "grid-cols";
  
  public static final String KEY_GRID_ROWS = "grid-rows";
  
  public static final String KEY_HAPTIC_CHANNEL_COUNT = "haptic-channel-count";
  
  public static final String KEY_HARDWARE_AV_SYNC_ID = "hw-av-sync-id";
  
  public static final String KEY_HDR10_PLUS_INFO = "hdr10-plus-info";
  
  public static final String KEY_HDR_STATIC_INFO = "hdr-static-info";
  
  public static final String KEY_HEIGHT = "height";
  
  public static final String KEY_INTRA_REFRESH_PERIOD = "intra-refresh-period";
  
  public static final String KEY_IS_ADTS = "is-adts";
  
  public static final String KEY_IS_AUTOSELECT = "is-autoselect";
  
  public static final String KEY_IS_DEFAULT = "is-default";
  
  public static final String KEY_IS_FORCED_SUBTITLE = "is-forced-subtitle";
  
  public static final String KEY_IS_TIMED_TEXT = "is-timed-text";
  
  public static final String KEY_I_FRAME_INTERVAL = "i-frame-interval";
  
  public static final String KEY_LANGUAGE = "language";
  
  public static final String KEY_LATENCY = "latency";
  
  public static final String KEY_LEVEL = "level";
  
  public static final String KEY_LOW_LATENCY = "low-latency";
  
  public static final String KEY_MAX_BIT_RATE = "max-bitrate";
  
  public static final String KEY_MAX_B_FRAMES = "max-bframes";
  
  public static final String KEY_MAX_FPS_TO_ENCODER = "max-fps-to-encoder";
  
  public static final String KEY_MAX_HEIGHT = "max-height";
  
  public static final String KEY_MAX_INPUT_SIZE = "max-input-size";
  
  public static final String KEY_MAX_PTS_GAP_TO_ENCODER = "max-pts-gap-to-encoder";
  
  public static final String KEY_MAX_WIDTH = "max-width";
  
  public static final String KEY_MIME = "mime";
  
  public static final String KEY_OPERATING_RATE = "operating-rate";
  
  public static final String KEY_OUTPUT_REORDER_DEPTH = "output-reorder-depth";
  
  public static final String KEY_PCM_ENCODING = "pcm-encoding";
  
  public static final String KEY_PIXEL_ASPECT_RATIO_HEIGHT = "sar-height";
  
  public static final String KEY_PIXEL_ASPECT_RATIO_WIDTH = "sar-width";
  
  public static final String KEY_PREPEND_HEADER_TO_SYNC_FRAMES = "prepend-sps-pps-to-idr-frames";
  
  public static final String KEY_PRIORITY = "priority";
  
  public static final String KEY_PROFILE = "profile";
  
  public static final String KEY_PUSH_BLANK_BUFFERS_ON_STOP = "push-blank-buffers-on-shutdown";
  
  public static final String KEY_QUALITY = "quality";
  
  public static final String KEY_REPEAT_PREVIOUS_FRAME_AFTER = "repeat-previous-frame-after";
  
  public static final String KEY_ROTATION = "rotation-degrees";
  
  public static final String KEY_SAMPLE_RATE = "sample-rate";
  
  public static final String KEY_SLICE_HEIGHT = "slice-height";
  
  public static final String KEY_STRIDE = "stride";
  
  public static final String KEY_TEMPORAL_LAYERING = "ts-schema";
  
  public static final String KEY_TILE_HEIGHT = "tile-height";
  
  public static final String KEY_TILE_WIDTH = "tile-width";
  
  public static final String KEY_TRACK_ID = "track-id";
  
  public static final String KEY_WIDTH = "width";
  
  public static final String MIMETYPE_AUDIO_AAC = "audio/mp4a-latm";
  
  public static final String MIMETYPE_AUDIO_AC3 = "audio/ac3";
  
  public static final String MIMETYPE_AUDIO_AC4 = "audio/ac4";
  
  public static final String MIMETYPE_AUDIO_AMR_NB = "audio/3gpp";
  
  public static final String MIMETYPE_AUDIO_AMR_WB = "audio/amr-wb";
  
  public static final String MIMETYPE_AUDIO_EAC3 = "audio/eac3";
  
  public static final String MIMETYPE_AUDIO_EAC3_JOC = "audio/eac3-joc";
  
  public static final String MIMETYPE_AUDIO_FLAC = "audio/flac";
  
  public static final String MIMETYPE_AUDIO_G711_ALAW = "audio/g711-alaw";
  
  public static final String MIMETYPE_AUDIO_G711_MLAW = "audio/g711-mlaw";
  
  public static final String MIMETYPE_AUDIO_MPEG = "audio/mpeg";
  
  public static final String MIMETYPE_AUDIO_MSGSM = "audio/gsm";
  
  public static final String MIMETYPE_AUDIO_OPUS = "audio/opus";
  
  public static final String MIMETYPE_AUDIO_QCELP = "audio/qcelp";
  
  public static final String MIMETYPE_AUDIO_RAW = "audio/raw";
  
  public static final String MIMETYPE_AUDIO_SCRAMBLED = "audio/scrambled";
  
  public static final String MIMETYPE_AUDIO_VORBIS = "audio/vorbis";
  
  public static final String MIMETYPE_IMAGE_ANDROID_HEIC = "image/vnd.android.heic";
  
  public static final String MIMETYPE_TEXT_CEA_608 = "text/cea-608";
  
  public static final String MIMETYPE_TEXT_CEA_708 = "text/cea-708";
  
  public static final String MIMETYPE_TEXT_SUBRIP = "application/x-subrip";
  
  public static final String MIMETYPE_TEXT_VTT = "text/vtt";
  
  public static final String MIMETYPE_VIDEO_AV1 = "video/av01";
  
  public static final String MIMETYPE_VIDEO_AVC = "video/avc";
  
  public static final String MIMETYPE_VIDEO_DOLBY_VISION = "video/dolby-vision";
  
  public static final String MIMETYPE_VIDEO_H263 = "video/3gpp";
  
  public static final String MIMETYPE_VIDEO_HEVC = "video/hevc";
  
  public static final String MIMETYPE_VIDEO_MPEG2 = "video/mpeg2";
  
  public static final String MIMETYPE_VIDEO_MPEG4 = "video/mp4v-es";
  
  public static final String MIMETYPE_VIDEO_RAW = "video/raw";
  
  public static final String MIMETYPE_VIDEO_SCRAMBLED = "video/scrambled";
  
  public static final String MIMETYPE_VIDEO_VP8 = "video/x-vnd.on2.vp8";
  
  public static final String MIMETYPE_VIDEO_VP9 = "video/x-vnd.on2.vp9";
  
  public static final int TYPE_BYTE_BUFFER = 5;
  
  public static final int TYPE_FLOAT = 3;
  
  public static final int TYPE_INTEGER = 1;
  
  public static final int TYPE_LONG = 2;
  
  public static final int TYPE_NULL = 0;
  
  public static final int TYPE_STRING = 4;
  
  private Map<String, Object> mMap;
  
  MediaFormat(Map<String, Object> paramMap) {
    this.mMap = paramMap;
  }
  
  public MediaFormat() {
    this.mMap = new HashMap<>();
  }
  
  Map<String, Object> getMap() {
    return this.mMap;
  }
  
  public final boolean containsKey(String paramString) {
    return this.mMap.containsKey(paramString);
  }
  
  public final boolean containsFeature(String paramString) {
    Map<String, Object> map = this.mMap;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("feature-");
    stringBuilder.append(paramString);
    return map.containsKey(stringBuilder.toString());
  }
  
  public final int getValueTypeForKey(String paramString) {
    paramString = (String)this.mMap.get(paramString);
    if (paramString == null)
      return 0; 
    if (paramString instanceof Integer)
      return 1; 
    if (paramString instanceof Long)
      return 2; 
    if (paramString instanceof Float)
      return 3; 
    if (paramString instanceof String)
      return 4; 
    if (paramString instanceof ByteBuffer)
      return 5; 
    throw new RuntimeException("invalid value for key");
  }
  
  public final Number getNumber(String paramString) {
    return (Number)this.mMap.get(paramString);
  }
  
  public final Number getNumber(String paramString, Number paramNumber) {
    Number number = getNumber(paramString);
    if (number != null)
      paramNumber = number; 
    return paramNumber;
  }
  
  public final int getInteger(String paramString) {
    return ((Integer)this.mMap.get(paramString)).intValue();
  }
  
  public final int getInteger(String paramString, int paramInt) {
    try {
      return getInteger(paramString);
    } catch (NullPointerException nullPointerException) {
      return paramInt;
    } 
  }
  
  public final long getLong(String paramString) {
    return ((Long)this.mMap.get(paramString)).longValue();
  }
  
  public final long getLong(String paramString, long paramLong) {
    try {
      return getLong(paramString);
    } catch (NullPointerException nullPointerException) {
      return paramLong;
    } 
  }
  
  public final float getFloat(String paramString) {
    return ((Float)this.mMap.get(paramString)).floatValue();
  }
  
  public final float getFloat(String paramString, float paramFloat) {
    paramString = (String)this.mMap.get(paramString);
    if (paramString != null)
      paramFloat = ((Float)paramString).floatValue(); 
    return paramFloat;
  }
  
  public final String getString(String paramString) {
    return (String)this.mMap.get(paramString);
  }
  
  public final String getString(String paramString1, String paramString2) {
    paramString1 = getString(paramString1);
    if (paramString1 == null)
      paramString1 = paramString2; 
    return paramString1;
  }
  
  public final ByteBuffer getByteBuffer(String paramString) {
    return (ByteBuffer)this.mMap.get(paramString);
  }
  
  public final ByteBuffer getByteBuffer(String paramString, ByteBuffer paramByteBuffer) {
    ByteBuffer byteBuffer = getByteBuffer(paramString);
    if (byteBuffer == null)
      byteBuffer = paramByteBuffer; 
    return byteBuffer;
  }
  
  public boolean getFeatureEnabled(String paramString) {
    Map<String, Object> map = this.mMap;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("feature-");
    stringBuilder.append(paramString);
    Integer integer = (Integer)map.get(stringBuilder.toString());
    if (integer != null) {
      boolean bool;
      if (integer.intValue() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    } 
    throw new IllegalArgumentException("feature is not specified");
  }
  
  public final void setInteger(String paramString, int paramInt) {
    this.mMap.put(paramString, Integer.valueOf(paramInt));
  }
  
  public final void setLong(String paramString, long paramLong) {
    this.mMap.put(paramString, Long.valueOf(paramLong));
  }
  
  public final void setFloat(String paramString, float paramFloat) {
    this.mMap.put(paramString, Float.valueOf(paramFloat));
  }
  
  public final void setString(String paramString1, String paramString2) {
    this.mMap.put(paramString1, paramString2);
  }
  
  public final void setByteBuffer(String paramString, ByteBuffer paramByteBuffer) {
    this.mMap.put(paramString, paramByteBuffer);
  }
  
  public final void removeKey(String paramString) {
    if (!paramString.startsWith("feature-"))
      this.mMap.remove(paramString); 
  }
  
  public final void removeFeature(String paramString) {
    Map<String, Object> map = this.mMap;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("feature-");
    stringBuilder.append(paramString);
    map.remove(stringBuilder.toString());
  }
  
  private abstract class FilteredMappedKeySet extends AbstractSet<String> {
    private Set<String> mKeys = MediaFormat.this.mMap.keySet();
    
    final MediaFormat this$0;
    
    public boolean contains(Object param1Object) {
      boolean bool = param1Object instanceof String;
      boolean bool1 = false;
      if (bool) {
        param1Object = mapItemToKey((String)param1Object);
        bool = bool1;
        if (keepKey((String)param1Object)) {
          bool = bool1;
          if (this.mKeys.contains(param1Object))
            bool = true; 
        } 
        return bool;
      } 
      return false;
    }
    
    public boolean remove(Object param1Object) {
      if (param1Object instanceof String) {
        param1Object = mapItemToKey((String)param1Object);
        if (keepKey((String)param1Object) && this.mKeys.remove(param1Object)) {
          MediaFormat.this.mMap.remove(param1Object);
          return true;
        } 
      } 
      return false;
    }
    
    private class KeyIterator implements Iterator<String> {
      Iterator<String> mIterator = ((List<String>)MediaFormat.FilteredMappedKeySet.this.mKeys.stream().filter(new _$$Lambda$MediaFormat$FilteredMappedKeySet$KeyIterator$3C8D_OYFyxgHLBDv_csQxBIPlfc(this)).collect(Collectors.toList())).iterator();
      
      String mLast;
      
      final MediaFormat.FilteredMappedKeySet this$1;
      
      public boolean hasNext() {
        return this.mIterator.hasNext();
      }
      
      public String next() {
        String str = this.mIterator.next();
        return MediaFormat.FilteredMappedKeySet.this.mapKeyToItem(str);
      }
      
      public void remove() {
        this.mIterator.remove();
        MediaFormat.this.mMap.remove(this.mLast);
      }
    }
    
    public Iterator<String> iterator() {
      return new KeyIterator();
    }
    
    public int size() {
      return (int)this.mKeys.stream().filter(new _$$Lambda$syLgBERouIkyuU1OuxTX624zJdE(this)).count();
    }
    
    protected abstract boolean keepKey(String param1String);
    
    protected abstract String mapItemToKey(String param1String);
    
    protected abstract String mapKeyToItem(String param1String);
  }
  
  private class KeyIterator implements Iterator<String> {
    Iterator<String> mIterator = ((List<String>)((MediaFormat.FilteredMappedKeySet)MediaFormat.this).mKeys.stream().filter(new _$$Lambda$MediaFormat$FilteredMappedKeySet$KeyIterator$3C8D_OYFyxgHLBDv_csQxBIPlfc(this)).collect(Collectors.toList())).iterator();
    
    String mLast;
    
    final MediaFormat.FilteredMappedKeySet this$1;
    
    public boolean hasNext() {
      return this.mIterator.hasNext();
    }
    
    public String next() {
      String str = this.mIterator.next();
      return this.this$1.mapKeyToItem(str);
    }
    
    public void remove() {
      this.mIterator.remove();
      MediaFormat.this.mMap.remove(this.mLast);
    }
  }
  
  class UnprefixedKeySet extends FilteredMappedKeySet {
    private String mPrefix;
    
    final MediaFormat this$0;
    
    public UnprefixedKeySet(String param1String) {
      this.mPrefix = param1String;
    }
    
    protected boolean keepKey(String param1String) {
      return param1String.startsWith(this.mPrefix) ^ true;
    }
    
    protected String mapKeyToItem(String param1String) {
      return param1String;
    }
    
    protected String mapItemToKey(String param1String) {
      return param1String;
    }
  }
  
  class PrefixedKeySetWithPrefixRemoved extends FilteredMappedKeySet {
    private String mPrefix;
    
    private int mPrefixLength;
    
    final MediaFormat this$0;
    
    public PrefixedKeySetWithPrefixRemoved(String param1String) {
      this.mPrefix = param1String;
      this.mPrefixLength = param1String.length();
    }
    
    protected boolean keepKey(String param1String) {
      return param1String.startsWith(this.mPrefix);
    }
    
    protected String mapKeyToItem(String param1String) {
      return param1String.substring(this.mPrefixLength);
    }
    
    protected String mapItemToKey(String param1String) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(this.mPrefix);
      stringBuilder.append(param1String);
      return stringBuilder.toString();
    }
  }
  
  public final Set<String> getKeys() {
    return new UnprefixedKeySet("feature-");
  }
  
  public final Set<String> getFeatures() {
    return new PrefixedKeySetWithPrefixRemoved("feature-");
  }
  
  public MediaFormat(MediaFormat paramMediaFormat) {
    this();
    this.mMap.putAll(paramMediaFormat.mMap);
  }
  
  public void setFeatureEnabled(String paramString, boolean paramBoolean) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("feature-");
    stringBuilder.append(paramString);
    setInteger(stringBuilder.toString(), paramBoolean);
  }
  
  public static final MediaFormat createAudioFormat(String paramString, int paramInt1, int paramInt2) {
    MediaFormat mediaFormat = new MediaFormat();
    mediaFormat.setString("mime", paramString);
    mediaFormat.setInteger("sample-rate", paramInt1);
    mediaFormat.setInteger("channel-count", paramInt2);
    return mediaFormat;
  }
  
  public static final MediaFormat createSubtitleFormat(String paramString1, String paramString2) {
    MediaFormat mediaFormat = new MediaFormat();
    mediaFormat.setString("mime", paramString1);
    mediaFormat.setString("language", paramString2);
    return mediaFormat;
  }
  
  public static final MediaFormat createVideoFormat(String paramString, int paramInt1, int paramInt2) {
    MediaFormat mediaFormat = new MediaFormat();
    mediaFormat.setString("mime", paramString);
    mediaFormat.setInteger("width", paramInt1);
    mediaFormat.setInteger("height", paramInt2);
    return mediaFormat;
  }
  
  public String toString() {
    return this.mMap.toString();
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorRange {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorStandard {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ColorTransfer {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface Type {}
}
