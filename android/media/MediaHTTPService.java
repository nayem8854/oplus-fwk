package android.media;

import android.os.IBinder;
import android.util.Log;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.util.List;

public class MediaHTTPService extends IMediaHTTPService.Stub {
  private static final String TAG = "MediaHTTPService";
  
  private Boolean mCookieStoreInitialized = new Boolean(false);
  
  private List<HttpCookie> mCookies;
  
  public MediaHTTPService(List<HttpCookie> paramList) {
    this.mCookies = paramList;
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaHTTPService(");
    stringBuilder.append(this);
    stringBuilder.append("): Cookies: ");
    stringBuilder.append(paramList);
    Log.v("MediaHTTPService", stringBuilder.toString());
  }
  
  public IMediaHTTPConnection makeHTTPConnection() {
    synchronized (this.mCookieStoreInitialized) {
      if (!this.mCookieStoreInitialized.booleanValue()) {
        CookieHandler cookieHandler = CookieHandler.getDefault();
        if (cookieHandler == null) {
          cookieHandler = new CookieManager();
          super();
          CookieHandler.setDefault(cookieHandler);
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("makeHTTPConnection: CookieManager created: ");
          stringBuilder1.append(cookieHandler);
          Log.v("MediaHTTPService", stringBuilder1.toString());
        } else {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("makeHTTPConnection: CookieHandler (");
          stringBuilder1.append(cookieHandler);
          stringBuilder1.append(") exists.");
          Log.v("MediaHTTPService", stringBuilder1.toString());
        } 
        if (this.mCookies != null)
          if (cookieHandler instanceof CookieManager) {
            CookieManager cookieManager = (CookieManager)cookieHandler;
            CookieStore cookieStore = cookieManager.getCookieStore();
            for (HttpCookie httpCookie : this.mCookies) {
              try {
                cookieStore.add(null, httpCookie);
              } catch (Exception exception) {
                StringBuilder stringBuilder1 = new StringBuilder();
                this();
                stringBuilder1.append("makeHTTPConnection: CookieStore.add");
                stringBuilder1.append(exception);
                Log.v("MediaHTTPService", stringBuilder1.toString());
              } 
            } 
          } else {
            Log.w("MediaHTTPService", "makeHTTPConnection: The installed CookieHandler is not a CookieManager. Can’t add the provided cookies to the cookie store.");
          }  
        this.mCookieStoreInitialized = Boolean.valueOf(true);
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("makeHTTPConnection(");
        stringBuilder.append(this);
        stringBuilder.append("): cookieHandler: ");
        stringBuilder.append(cookieHandler);
        stringBuilder.append(" Cookies: ");
        stringBuilder.append(this.mCookies);
        Log.v("MediaHTTPService", stringBuilder.toString());
      } 
      return new MediaHTTPConnection();
    } 
  }
  
  static IBinder createHttpServiceBinderIfNecessary(String paramString) {
    return createHttpServiceBinderIfNecessary(paramString, null);
  }
  
  static IBinder createHttpServiceBinderIfNecessary(String paramString, List<HttpCookie> paramList) {
    if (paramString.startsWith("http://") || paramString.startsWith("https://"))
      return (new MediaHTTPService(paramList)).asBinder(); 
    if (paramString.startsWith("widevine://"))
      Log.d("MediaHTTPService", "Widevine classic is no longer supported"); 
    return null;
  }
}
