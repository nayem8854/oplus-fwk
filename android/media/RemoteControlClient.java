package android.media;

import android.app.PendingIntent;
import android.graphics.Bitmap;
import android.media.session.MediaSession;
import android.media.session.MediaSessionLegacyHelper;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;

@Deprecated
public class RemoteControlClient {
  private static final boolean DEBUG = false;
  
  public static final int DEFAULT_PLAYBACK_VOLUME = 15;
  
  public static final int DEFAULT_PLAYBACK_VOLUME_HANDLING = 1;
  
  public static final int FLAGS_KEY_MEDIA_NONE = 0;
  
  public static final int FLAG_INFORMATION_REQUEST_ALBUM_ART = 8;
  
  public static final int FLAG_INFORMATION_REQUEST_KEY_MEDIA = 2;
  
  public static final int FLAG_INFORMATION_REQUEST_METADATA = 1;
  
  public static final int FLAG_INFORMATION_REQUEST_PLAYSTATE = 4;
  
  public static final int FLAG_KEY_MEDIA_FAST_FORWARD = 64;
  
  public static final int FLAG_KEY_MEDIA_NEXT = 128;
  
  public static final int FLAG_KEY_MEDIA_PAUSE = 16;
  
  public static final int FLAG_KEY_MEDIA_PLAY = 4;
  
  public static final int FLAG_KEY_MEDIA_PLAY_PAUSE = 8;
  
  public static final int FLAG_KEY_MEDIA_POSITION_UPDATE = 256;
  
  public static final int FLAG_KEY_MEDIA_PREVIOUS = 1;
  
  public static final int FLAG_KEY_MEDIA_RATING = 512;
  
  public static final int FLAG_KEY_MEDIA_REWIND = 2;
  
  public static final int FLAG_KEY_MEDIA_STOP = 32;
  
  public RemoteControlClient(PendingIntent paramPendingIntent) {
    this.mCacheLock = new Object();
    this.mPlaybackState = 0;
    this.mPlaybackStateChangeTimeMs = 0L;
    this.mPlaybackPositionMs = -1L;
    this.mPlaybackSpeed = 1.0F;
    this.mTransportControlFlags = 0;
    this.mMetadata = new Bundle();
    this.mCurrentClientGenId = -1;
    this.mNeedsPositionSync = false;
    this.mSessionPlaybackState = null;
    this.mTransportListener = (MediaSession.Callback)new Object(this);
    this.mRcMediaIntent = paramPendingIntent;
  }
  
  public RemoteControlClient(PendingIntent paramPendingIntent, Looper paramLooper) {
    this.mCacheLock = new Object();
    this.mPlaybackState = 0;
    this.mPlaybackStateChangeTimeMs = 0L;
    this.mPlaybackPositionMs = -1L;
    this.mPlaybackSpeed = 1.0F;
    this.mTransportControlFlags = 0;
    this.mMetadata = new Bundle();
    this.mCurrentClientGenId = -1;
    this.mNeedsPositionSync = false;
    this.mSessionPlaybackState = null;
    this.mTransportListener = (MediaSession.Callback)new Object(this);
    this.mRcMediaIntent = paramPendingIntent;
  }
  
  public void registerWithSession(MediaSessionLegacyHelper paramMediaSessionLegacyHelper) {
    paramMediaSessionLegacyHelper.addRccListener(this.mRcMediaIntent, this.mTransportListener);
    this.mSession = paramMediaSessionLegacyHelper.getSession(this.mRcMediaIntent);
    setTransportControlFlags(this.mTransportControlFlags);
  }
  
  public void unregisterWithSession(MediaSessionLegacyHelper paramMediaSessionLegacyHelper) {
    paramMediaSessionLegacyHelper.removeRccListener(this.mRcMediaIntent);
    this.mSession = null;
  }
  
  public MediaSession getMediaSession() {
    return this.mSession;
  }
  
  @Deprecated
  class MetadataEditor extends MediaMetadataEditor {
    public static final int BITMAP_KEY_ARTWORK = 100;
    
    public static final int METADATA_KEY_ARTWORK = 100;
    
    final RemoteControlClient this$0;
    
    private MetadataEditor() {}
    
    public Object clone() throws CloneNotSupportedException {
      throw new CloneNotSupportedException();
    }
    
    public MetadataEditor putString(int param1Int, String param1String) throws IllegalArgumentException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: aload_2
      //   5: invokespecial putString : (ILjava/lang/String;)Landroid/media/MediaMetadataEditor;
      //   8: pop
      //   9: aload_0
      //   10: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   13: ifnull -> 35
      //   16: iload_1
      //   17: invokestatic getKeyFromMetadataEditorKey : (I)Ljava/lang/String;
      //   20: astore_3
      //   21: aload_3
      //   22: ifnull -> 35
      //   25: aload_0
      //   26: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   29: aload_3
      //   30: aload_2
      //   31: invokevirtual putText : (Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/media/MediaMetadata$Builder;
      //   34: pop
      //   35: aload_0
      //   36: monitorexit
      //   37: aload_0
      //   38: areturn
      //   39: astore_2
      //   40: aload_0
      //   41: monitorexit
      //   42: aload_2
      //   43: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #455	-> 2
      //   #456	-> 9
      //   #458	-> 16
      //   #460	-> 21
      //   #461	-> 25
      //   #465	-> 35
      //   #454	-> 39
      // Exception table:
      //   from	to	target	type
      //   2	9	39	finally
      //   9	16	39	finally
      //   16	21	39	finally
      //   25	35	39	finally
    }
    
    public MetadataEditor putLong(int param1Int, long param1Long) throws IllegalArgumentException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: lload_2
      //   5: invokespecial putLong : (IJ)Landroid/media/MediaMetadataEditor;
      //   8: pop
      //   9: aload_0
      //   10: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   13: ifnull -> 38
      //   16: iload_1
      //   17: invokestatic getKeyFromMetadataEditorKey : (I)Ljava/lang/String;
      //   20: astore #4
      //   22: aload #4
      //   24: ifnull -> 38
      //   27: aload_0
      //   28: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   31: aload #4
      //   33: lload_2
      //   34: invokevirtual putLong : (Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;
      //   37: pop
      //   38: aload_0
      //   39: monitorexit
      //   40: aload_0
      //   41: areturn
      //   42: astore #4
      //   44: aload_0
      //   45: monitorexit
      //   46: aload #4
      //   48: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #485	-> 2
      //   #486	-> 9
      //   #488	-> 16
      //   #490	-> 22
      //   #491	-> 27
      //   #494	-> 38
      //   #484	-> 42
      // Exception table:
      //   from	to	target	type
      //   2	9	42	finally
      //   9	16	42	finally
      //   16	22	42	finally
      //   27	38	42	finally
    }
    
    public MetadataEditor putBitmap(int param1Int, Bitmap param1Bitmap) throws IllegalArgumentException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: aload_2
      //   5: invokespecial putBitmap : (ILandroid/graphics/Bitmap;)Landroid/media/MediaMetadataEditor;
      //   8: pop
      //   9: aload_0
      //   10: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   13: ifnull -> 35
      //   16: iload_1
      //   17: invokestatic getKeyFromMetadataEditorKey : (I)Ljava/lang/String;
      //   20: astore_3
      //   21: aload_3
      //   22: ifnull -> 35
      //   25: aload_0
      //   26: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   29: aload_3
      //   30: aload_2
      //   31: invokevirtual putBitmap : (Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;
      //   34: pop
      //   35: aload_0
      //   36: monitorexit
      //   37: aload_0
      //   38: areturn
      //   39: astore_2
      //   40: aload_0
      //   41: monitorexit
      //   42: aload_2
      //   43: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #510	-> 2
      //   #511	-> 9
      //   #513	-> 16
      //   #515	-> 21
      //   #516	-> 25
      //   #519	-> 35
      //   #509	-> 39
      // Exception table:
      //   from	to	target	type
      //   2	9	39	finally
      //   9	16	39	finally
      //   16	21	39	finally
      //   25	35	39	finally
    }
    
    public MetadataEditor putObject(int param1Int, Object param1Object) throws IllegalArgumentException {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: iload_1
      //   4: aload_2
      //   5: invokespecial putObject : (ILjava/lang/Object;)Landroid/media/MediaMetadataEditor;
      //   8: pop
      //   9: aload_0
      //   10: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   13: ifnull -> 50
      //   16: iload_1
      //   17: ldc 268435457
      //   19: if_icmpeq -> 28
      //   22: iload_1
      //   23: bipush #101
      //   25: if_icmpne -> 50
      //   28: iload_1
      //   29: invokestatic getKeyFromMetadataEditorKey : (I)Ljava/lang/String;
      //   32: astore_3
      //   33: aload_3
      //   34: ifnull -> 50
      //   37: aload_0
      //   38: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   41: aload_3
      //   42: aload_2
      //   43: checkcast android/media/Rating
      //   46: invokevirtual putRating : (Ljava/lang/String;Landroid/media/Rating;)Landroid/media/MediaMetadata$Builder;
      //   49: pop
      //   50: aload_0
      //   51: monitorexit
      //   52: aload_0
      //   53: areturn
      //   54: astore_2
      //   55: aload_0
      //   56: monitorexit
      //   57: aload_2
      //   58: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #525	-> 2
      //   #526	-> 9
      //   #529	-> 28
      //   #530	-> 33
      //   #531	-> 37
      //   #534	-> 50
      //   #524	-> 54
      // Exception table:
      //   from	to	target	type
      //   2	9	54	finally
      //   9	16	54	finally
      //   28	33	54	finally
      //   37	50	54	finally
    }
    
    public void clear() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: invokespecial clear : ()V
      //   6: aload_0
      //   7: monitorexit
      //   8: return
      //   9: astore_1
      //   10: aload_0
      //   11: monitorexit
      //   12: aload_1
      //   13: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #545	-> 2
      //   #546	-> 6
      //   #544	-> 9
      // Exception table:
      //   from	to	target	type
      //   2	6	9	finally
    }
    
    public void apply() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mApplied : Z
      //   6: ifeq -> 20
      //   9: ldc 'RemoteControlClient'
      //   11: ldc 'Can't apply a previously applied MetadataEditor'
      //   13: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
      //   16: pop
      //   17: aload_0
      //   18: monitorexit
      //   19: return
      //   20: aload_0
      //   21: getfield this$0 : Landroid/media/RemoteControlClient;
      //   24: invokestatic access$000 : (Landroid/media/RemoteControlClient;)Ljava/lang/Object;
      //   27: astore_1
      //   28: aload_1
      //   29: monitorenter
      //   30: aload_0
      //   31: getfield this$0 : Landroid/media/RemoteControlClient;
      //   34: astore_2
      //   35: new android/os/Bundle
      //   38: astore_3
      //   39: aload_3
      //   40: aload_0
      //   41: getfield mEditorMetadata : Landroid/os/Bundle;
      //   44: invokespecial <init> : (Landroid/os/Bundle;)V
      //   47: aload_2
      //   48: aload_3
      //   49: invokestatic access$102 : (Landroid/media/RemoteControlClient;Landroid/os/Bundle;)Landroid/os/Bundle;
      //   52: pop
      //   53: aload_0
      //   54: getfield this$0 : Landroid/media/RemoteControlClient;
      //   57: invokestatic access$100 : (Landroid/media/RemoteControlClient;)Landroid/os/Bundle;
      //   60: ldc 536870911
      //   62: invokestatic valueOf : (I)Ljava/lang/String;
      //   65: aload_0
      //   66: getfield mEditableKeys : J
      //   69: invokevirtual putLong : (Ljava/lang/String;J)V
      //   72: aload_0
      //   73: getfield this$0 : Landroid/media/RemoteControlClient;
      //   76: invokestatic access$200 : (Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
      //   79: astore_3
      //   80: aload_3
      //   81: ifnull -> 111
      //   84: aload_0
      //   85: getfield this$0 : Landroid/media/RemoteControlClient;
      //   88: invokestatic access$200 : (Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
      //   91: aload_0
      //   92: getfield mEditorArtwork : Landroid/graphics/Bitmap;
      //   95: invokevirtual equals : (Ljava/lang/Object;)Z
      //   98: ifne -> 111
      //   101: aload_0
      //   102: getfield this$0 : Landroid/media/RemoteControlClient;
      //   105: invokestatic access$200 : (Landroid/media/RemoteControlClient;)Landroid/graphics/Bitmap;
      //   108: invokevirtual recycle : ()V
      //   111: aload_0
      //   112: getfield this$0 : Landroid/media/RemoteControlClient;
      //   115: aload_0
      //   116: getfield mEditorArtwork : Landroid/graphics/Bitmap;
      //   119: invokestatic access$202 : (Landroid/media/RemoteControlClient;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
      //   122: pop
      //   123: aload_0
      //   124: aconst_null
      //   125: putfield mEditorArtwork : Landroid/graphics/Bitmap;
      //   128: aload_0
      //   129: getfield this$0 : Landroid/media/RemoteControlClient;
      //   132: invokestatic access$300 : (Landroid/media/RemoteControlClient;)Landroid/media/session/MediaSession;
      //   135: astore_3
      //   136: aload_3
      //   137: ifnull -> 179
      //   140: aload_0
      //   141: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   144: ifnull -> 179
      //   147: aload_0
      //   148: getfield this$0 : Landroid/media/RemoteControlClient;
      //   151: aload_0
      //   152: getfield mMetadataBuilder : Landroid/media/MediaMetadata$Builder;
      //   155: invokevirtual build : ()Landroid/media/MediaMetadata;
      //   158: invokestatic access$402 : (Landroid/media/RemoteControlClient;Landroid/media/MediaMetadata;)Landroid/media/MediaMetadata;
      //   161: pop
      //   162: aload_0
      //   163: getfield this$0 : Landroid/media/RemoteControlClient;
      //   166: invokestatic access$300 : (Landroid/media/RemoteControlClient;)Landroid/media/session/MediaSession;
      //   169: aload_0
      //   170: getfield this$0 : Landroid/media/RemoteControlClient;
      //   173: invokestatic access$400 : (Landroid/media/RemoteControlClient;)Landroid/media/MediaMetadata;
      //   176: invokevirtual setMetadata : (Landroid/media/MediaMetadata;)V
      //   179: aload_0
      //   180: iconst_1
      //   181: putfield mApplied : Z
      //   184: aload_1
      //   185: monitorexit
      //   186: aload_0
      //   187: monitorexit
      //   188: return
      //   189: astore_3
      //   190: aload_1
      //   191: monitorexit
      //   192: aload_3
      //   193: athrow
      //   194: astore_3
      //   195: goto -> 190
      //   198: astore_3
      //   199: aload_0
      //   200: monitorexit
      //   201: aload_3
      //   202: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #555	-> 2
      //   #556	-> 9
      //   #557	-> 17
      //   #559	-> 20
      //   #563	-> 30
      //   #565	-> 53
      //   #566	-> 72
      //   #567	-> 101
      //   #569	-> 111
      //   #570	-> 123
      //   #573	-> 128
      //   #574	-> 147
      //   #575	-> 162
      //   #577	-> 179
      //   #578	-> 184
      //   #579	-> 186
      //   #578	-> 189
      //   #554	-> 198
      // Exception table:
      //   from	to	target	type
      //   2	9	198	finally
      //   9	17	198	finally
      //   20	30	198	finally
      //   30	53	189	finally
      //   53	72	189	finally
      //   72	80	189	finally
      //   84	101	194	finally
      //   101	111	194	finally
      //   111	123	189	finally
      //   123	128	189	finally
      //   128	136	189	finally
      //   140	147	194	finally
      //   147	162	194	finally
      //   162	179	194	finally
      //   179	184	189	finally
      //   184	186	189	finally
      //   190	192	194	finally
      //   192	194	198	finally
    }
  }
  
  public MetadataEditor editMetadata(boolean paramBoolean) {
    MetadataEditor metadataEditor = new MetadataEditor();
    if (paramBoolean) {
      metadataEditor.mEditorMetadata = new Bundle();
      metadataEditor.mEditorArtwork = null;
      metadataEditor.mMetadataChanged = true;
      metadataEditor.mArtworkChanged = true;
      metadataEditor.mEditableKeys = 0L;
    } else {
      metadataEditor.mEditorMetadata = new Bundle(this.mMetadata);
      metadataEditor.mEditorArtwork = this.mOriginalArtwork;
      metadataEditor.mMetadataChanged = false;
      metadataEditor.mArtworkChanged = false;
    } 
    if (!paramBoolean) {
      MediaMetadata mediaMetadata = this.mMediaMetadata;
      if (mediaMetadata == null) {
        metadataEditor.mMetadataBuilder = new MediaMetadata.Builder();
        return metadataEditor;
      } 
      metadataEditor.mMetadataBuilder = new MediaMetadata.Builder(mediaMetadata);
      return metadataEditor;
    } 
    metadataEditor.mMetadataBuilder = new MediaMetadata.Builder();
    return metadataEditor;
  }
  
  public void setPlaybackState(int paramInt) {
    setPlaybackStateInt(paramInt, -9216204211029966080L, 1.0F, false);
  }
  
  public void setPlaybackState(int paramInt, long paramLong, float paramFloat) {
    setPlaybackStateInt(paramInt, paramLong, paramFloat, true);
  }
  
  private void setPlaybackStateInt(int paramInt, long paramLong, float paramFloat, boolean paramBoolean) {
    synchronized (this.mCacheLock) {
      if (this.mPlaybackState != paramInt || this.mPlaybackPositionMs != paramLong || this.mPlaybackSpeed != paramFloat) {
        this.mPlaybackState = paramInt;
        long l = -1L;
        if (paramBoolean) {
          if (paramLong < 0L) {
            this.mPlaybackPositionMs = -1L;
          } else {
            this.mPlaybackPositionMs = paramLong;
          } 
        } else {
          this.mPlaybackPositionMs = -9216204211029966080L;
        } 
        this.mPlaybackSpeed = paramFloat;
        this.mPlaybackStateChangeTimeMs = SystemClock.elapsedRealtime();
        if (this.mSession != null) {
          paramInt = getStateFromRccState(paramInt);
          if (paramBoolean) {
            paramLong = this.mPlaybackPositionMs;
          } else {
            paramLong = l;
          } 
          PlaybackState.Builder builder = new PlaybackState.Builder();
          this(this.mSessionPlaybackState);
          builder.setState(paramInt, paramLong, paramFloat, SystemClock.elapsedRealtime());
          builder.setErrorMessage(null);
          PlaybackState playbackState = builder.build();
          this.mSession.setPlaybackState(playbackState);
        } 
      } 
      return;
    } 
  }
  
  public void setTransportControlFlags(int paramInt) {
    synchronized (this.mCacheLock) {
      this.mTransportControlFlags = paramInt;
      if (this.mSession != null) {
        PlaybackState.Builder builder = new PlaybackState.Builder();
        this(this.mSessionPlaybackState);
        builder.setActions(getActionsFromRccControlFlags(paramInt));
        PlaybackState playbackState = builder.build();
        this.mSession.setPlaybackState(playbackState);
      } 
      return;
    } 
  }
  
  public void setMetadataUpdateListener(OnMetadataUpdateListener paramOnMetadataUpdateListener) {
    synchronized (this.mCacheLock) {
      this.mMetadataUpdateListener = paramOnMetadataUpdateListener;
      return;
    } 
  }
  
  public void setPlaybackPositionUpdateListener(OnPlaybackPositionUpdateListener paramOnPlaybackPositionUpdateListener) {
    synchronized (this.mCacheLock) {
      this.mPositionUpdateListener = paramOnPlaybackPositionUpdateListener;
      return;
    } 
  }
  
  public void setOnGetPlaybackPositionListener(OnGetPlaybackPositionListener paramOnGetPlaybackPositionListener) {
    synchronized (this.mCacheLock) {
      this.mPositionProvider = paramOnGetPlaybackPositionListener;
      return;
    } 
  }
  
  public static int MEDIA_POSITION_READABLE = 1;
  
  public static int MEDIA_POSITION_WRITABLE = 2;
  
  public static final int PLAYBACKINFO_INVALID_VALUE = -2147483648;
  
  public static final int PLAYBACKINFO_PLAYBACK_TYPE = 1;
  
  public static final int PLAYBACKINFO_USES_STREAM = 5;
  
  public static final int PLAYBACKINFO_VOLUME = 2;
  
  public static final int PLAYBACKINFO_VOLUME_HANDLING = 4;
  
  public static final int PLAYBACKINFO_VOLUME_MAX = 3;
  
  public static final long PLAYBACK_POSITION_ALWAYS_UNKNOWN = -9216204211029966080L;
  
  public static final long PLAYBACK_POSITION_INVALID = -1L;
  
  public static final float PLAYBACK_SPEED_1X = 1.0F;
  
  public static final int PLAYBACK_TYPE_LOCAL = 0;
  
  private static final int PLAYBACK_TYPE_MAX = 1;
  
  private static final int PLAYBACK_TYPE_MIN = 0;
  
  public static final int PLAYBACK_TYPE_REMOTE = 1;
  
  public static final int PLAYBACK_VOLUME_FIXED = 0;
  
  public static final int PLAYBACK_VOLUME_VARIABLE = 1;
  
  public static final int PLAYSTATE_BUFFERING = 8;
  
  public static final int PLAYSTATE_ERROR = 9;
  
  public static final int PLAYSTATE_FAST_FORWARDING = 4;
  
  public static final int PLAYSTATE_NONE = 0;
  
  public static final int PLAYSTATE_PAUSED = 2;
  
  public static final int PLAYSTATE_PLAYING = 3;
  
  public static final int PLAYSTATE_REWINDING = 5;
  
  public static final int PLAYSTATE_SKIPPING_BACKWARDS = 7;
  
  public static final int PLAYSTATE_SKIPPING_FORWARDS = 6;
  
  public static final int PLAYSTATE_STOPPED = 1;
  
  private static final long POSITION_DRIFT_MAX_MS = 500L;
  
  private static final long POSITION_REFRESH_PERIOD_MIN_MS = 2000L;
  
  private static final long POSITION_REFRESH_PERIOD_PLAYING_MS = 15000L;
  
  public static final int RCSE_ID_UNREGISTERED = -1;
  
  private static final String TAG = "RemoteControlClient";
  
  private final Object mCacheLock;
  
  private int mCurrentClientGenId;
  
  private MediaMetadata mMediaMetadata;
  
  private Bundle mMetadata;
  
  private OnMetadataUpdateListener mMetadataUpdateListener;
  
  private boolean mNeedsPositionSync;
  
  private Bitmap mOriginalArtwork;
  
  private long mPlaybackPositionMs;
  
  private float mPlaybackSpeed;
  
  private int mPlaybackState;
  
  private long mPlaybackStateChangeTimeMs;
  
  private OnGetPlaybackPositionListener mPositionProvider;
  
  private OnPlaybackPositionUpdateListener mPositionUpdateListener;
  
  private final PendingIntent mRcMediaIntent;
  
  private MediaSession mSession;
  
  private PlaybackState mSessionPlaybackState;
  
  private int mTransportControlFlags;
  
  private MediaSession.Callback mTransportListener;
  
  public PendingIntent getRcMediaIntent() {
    return this.mRcMediaIntent;
  }
  
  private void onSeekTo(int paramInt, long paramLong) {
    synchronized (this.mCacheLock) {
      if (this.mCurrentClientGenId == paramInt && this.mPositionUpdateListener != null)
        this.mPositionUpdateListener.onPlaybackPositionUpdate(paramLong); 
      return;
    } 
  }
  
  private void onUpdateMetadata(int paramInt1, int paramInt2, Object paramObject) {
    synchronized (this.mCacheLock) {
      if (this.mCurrentClientGenId == paramInt1 && this.mMetadataUpdateListener != null)
        this.mMetadataUpdateListener.onMetadataUpdate(paramInt2, paramObject); 
      return;
    } 
  }
  
  static boolean playbackPositionShouldMove(int paramInt) {
    if (paramInt != 1 && paramInt != 2)
      switch (paramInt) {
        default:
          return true;
        case 6:
        case 7:
        case 8:
        case 9:
          break;
      }  
    return false;
  }
  
  private static long getCheckPeriodFromSpeed(float paramFloat) {
    if (Math.abs(paramFloat) <= 1.0F)
      return 15000L; 
    return Math.max((long)(15000.0F / Math.abs(paramFloat)), 2000L);
  }
  
  private static int getStateFromRccState(int paramInt) {
    switch (paramInt) {
      default:
        return -1;
      case 9:
        return 7;
      case 8:
        return 6;
      case 7:
        return 9;
      case 6:
        return 10;
      case 5:
        return 5;
      case 4:
        return 4;
      case 3:
        return 3;
      case 2:
        return 2;
      case 1:
        return 1;
      case 0:
        break;
    } 
    return 0;
  }
  
  static int getRccStateFromState(int paramInt) {
    switch (paramInt) {
      default:
        return -1;
      case 10:
        return 6;
      case 9:
        return 7;
      case 7:
        return 9;
      case 6:
        return 8;
      case 5:
        return 5;
      case 4:
        return 4;
      case 3:
        return 3;
      case 2:
        return 2;
      case 1:
        return 1;
      case 0:
        break;
    } 
    return 0;
  }
  
  private static long getActionsFromRccControlFlags(int paramInt) {
    long l1 = 0L;
    long l2 = 1L;
    while (l2 <= paramInt) {
      long l = l1;
      if ((paramInt & l2) != 0L)
        l = l1 | getActionForRccFlag((int)l2); 
      l2 <<= 1L;
      l1 = l;
    } 
    return l1;
  }
  
  static int getRccControlFlagsFromActions(long paramLong) {
    int i = 0;
    long l = 1L;
    while (l <= paramLong && l < 2147483647L) {
      int j = i;
      if ((l & paramLong) != 0L)
        j = i | getRccFlagForAction(l); 
      l <<= 1L;
      i = j;
    } 
    return i;
  }
  
  private static long getActionForRccFlag(int paramInt) {
    if (paramInt != 1) {
      if (paramInt != 2) {
        if (paramInt != 4) {
          if (paramInt != 8) {
            if (paramInt != 16) {
              if (paramInt != 32) {
                if (paramInt != 64) {
                  if (paramInt != 128) {
                    if (paramInt != 256) {
                      if (paramInt != 512)
                        return 0L; 
                      return 128L;
                    } 
                    return 256L;
                  } 
                  return 32L;
                } 
                return 64L;
              } 
              return 1L;
            } 
            return 2L;
          } 
          return 512L;
        } 
        return 4L;
      } 
      return 8L;
    } 
    return 16L;
  }
  
  private static int getRccFlagForAction(long paramLong) {
    boolean bool;
    if (paramLong < 2147483647L) {
      bool = (int)paramLong;
    } else {
      bool = false;
    } 
    if (bool != true) {
      if (bool != 2) {
        if (bool != 4) {
          if (bool != 8) {
            if (bool != 16) {
              if (bool != 32) {
                if (bool != 64) {
                  if (bool != '') {
                    if (bool != 'Ā') {
                      if (bool != 'Ȁ')
                        return 0; 
                      return 8;
                    } 
                    return 256;
                  } 
                  return 512;
                } 
                return 64;
              } 
              return 128;
            } 
            return 1;
          } 
          return 2;
        } 
        return 4;
      } 
      return 16;
    } 
    return 32;
  }
  
  public static interface OnGetPlaybackPositionListener {
    long onGetPlaybackPosition();
  }
  
  public static interface OnMetadataUpdateListener {
    void onMetadataUpdate(int param1Int, Object param1Object);
  }
  
  public static interface OnPlaybackPositionUpdateListener {
    void onPlaybackPositionUpdate(long param1Long);
  }
}
