package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IMediaHTTPService extends IInterface {
  IMediaHTTPConnection makeHTTPConnection() throws RemoteException;
  
  class Default implements IMediaHTTPService {
    public IMediaHTTPConnection makeHTTPConnection() throws RemoteException {
      return null;
    }
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IMediaHTTPService {
    private static final String DESCRIPTOR = "android.media.IMediaHTTPService";
    
    static final int TRANSACTION_makeHTTPConnection = 1;
    
    public Stub() {
      attachInterface(this, "android.media.IMediaHTTPService");
    }
    
    public static IMediaHTTPService asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IMediaHTTPService");
      if (iInterface != null && iInterface instanceof IMediaHTTPService)
        return (IMediaHTTPService)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      if (param1Int != 1)
        return null; 
      return "makeHTTPConnection";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      if (param1Int1 != 1) {
        if (param1Int1 != 1598968902)
          return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2); 
        param1Parcel2.writeString("android.media.IMediaHTTPService");
        return true;
      } 
      param1Parcel1.enforceInterface("android.media.IMediaHTTPService");
      IMediaHTTPConnection iMediaHTTPConnection = makeHTTPConnection();
      param1Parcel2.writeNoException();
      if (iMediaHTTPConnection != null) {
        IBinder iBinder = iMediaHTTPConnection.asBinder();
      } else {
        iMediaHTTPConnection = null;
      } 
      param1Parcel2.writeStrongBinder((IBinder)iMediaHTTPConnection);
      return true;
    }
    
    class Proxy implements IMediaHTTPService {
      public static IMediaHTTPService sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IMediaHTTPService.Stub this$0) {
        this.mRemote = this$0;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IMediaHTTPService";
      }
      
      public IMediaHTTPConnection makeHTTPConnection() throws RemoteException {
        Parcel parcel1 = Parcel.obtain();
        Parcel parcel2 = Parcel.obtain();
        try {
          parcel1.writeInterfaceToken("android.media.IMediaHTTPService");
          boolean bool = this.mRemote.transact(1, parcel1, parcel2, 0);
          if (!bool && IMediaHTTPService.Stub.getDefaultImpl() != null)
            return IMediaHTTPService.Stub.getDefaultImpl().makeHTTPConnection(); 
          parcel2.readException();
          return IMediaHTTPConnection.Stub.asInterface(parcel2.readStrongBinder());
        } finally {
          parcel2.recycle();
          parcel1.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IMediaHTTPService param1IMediaHTTPService) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IMediaHTTPService != null) {
          Proxy.sDefaultImpl = param1IMediaHTTPService;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IMediaHTTPService getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
