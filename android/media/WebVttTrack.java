package android.media;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

class WebVttTrack extends SubtitleTrack implements WebVttCueListener {
  private static final String TAG = "WebVttTrack";
  
  private Long mCurrentRunID;
  
  private final UnstyledTextExtractor mExtractor;
  
  private final WebVttParser mParser = new WebVttParser(this);
  
  private final Map<String, TextTrackRegion> mRegions;
  
  private final WebVttRenderingWidget mRenderingWidget;
  
  private final Vector<Long> mTimestamps;
  
  private final Tokenizer mTokenizer;
  
  WebVttTrack(WebVttRenderingWidget paramWebVttRenderingWidget, MediaFormat paramMediaFormat) {
    super(paramMediaFormat);
    UnstyledTextExtractor unstyledTextExtractor = new UnstyledTextExtractor();
    this.mTokenizer = new Tokenizer(unstyledTextExtractor);
    this.mTimestamps = new Vector<>();
    this.mRegions = new HashMap<>();
    this.mRenderingWidget = paramWebVttRenderingWidget;
  }
  
  public WebVttRenderingWidget getRenderingWidget() {
    return this.mRenderingWidget;
  }
  
  public void onData(byte[] paramArrayOfbyte, boolean paramBoolean, long paramLong) {
    try {
      null = new String();
      this(paramArrayOfbyte, "UTF-8");
      synchronized (this.mParser) {
        if (this.mCurrentRunID == null || paramLong == this.mCurrentRunID.longValue()) {
          this.mCurrentRunID = Long.valueOf(paramLong);
          this.mParser.parse(null);
          if (paramBoolean) {
            finishedRun(paramLong);
            this.mParser.eos();
            this.mRegions.clear();
            this.mCurrentRunID = null;
          } 
          return;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Run #");
        stringBuilder.append(this.mCurrentRunID);
        stringBuilder.append(" in progress.  Cannot process run #");
        stringBuilder.append(paramLong);
        this(stringBuilder.toString());
        throw illegalStateException;
      } 
    } catch (UnsupportedEncodingException unsupportedEncodingException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("subtitle data is not UTF-8 encoded: ");
      stringBuilder.append(unsupportedEncodingException);
      Log.w("WebVttTrack", stringBuilder.toString());
    } 
  }
  
  public void onCueParsed(TextTrackCue paramTextTrackCue) {
    synchronized (this.mParser) {
      if (paramTextTrackCue.mRegionId.length() != 0)
        paramTextTrackCue.mRegion = this.mRegions.get(paramTextTrackCue.mRegionId); 
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("adding cue ");
        stringBuilder.append(paramTextTrackCue);
        Log.v("WebVttTrack", stringBuilder.toString());
      } 
      this.mTokenizer.reset();
      for (String str : paramTextTrackCue.mStrings)
        this.mTokenizer.tokenize(str); 
      paramTextTrackCue.mLines = this.mExtractor.getText();
      if (this.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder = paramTextTrackCue.appendStringsToBuilder(stringBuilder);
        stringBuilder.append(" simplified to: ");
        stringBuilder = paramTextTrackCue.appendLinesToBuilder(stringBuilder);
        String str = stringBuilder.toString();
        Log.v("WebVttTrack", str);
      } 
      byte b;
      TextTrackCueSpan[][] arrayOfTextTrackCueSpan;
      int i;
      for (arrayOfTextTrackCueSpan = paramTextTrackCue.mLines, i = arrayOfTextTrackCueSpan.length, b = 0; b < i; ) {
        for (TextTrackCueSpan textTrackCueSpan : arrayOfTextTrackCueSpan[b]) {
          if (textTrackCueSpan.mTimestampMs > paramTextTrackCue.mStartTimeMs && textTrackCueSpan.mTimestampMs < paramTextTrackCue.mEndTimeMs) {
            Vector<Long> vector = this.mTimestamps;
            long l = textTrackCueSpan.mTimestampMs;
            if (!vector.contains(Long.valueOf(l)))
              this.mTimestamps.add(Long.valueOf(textTrackCueSpan.mTimestampMs)); 
          } 
        } 
        b++;
      } 
      if (this.mTimestamps.size() > 0) {
        paramTextTrackCue.mInnerTimesMs = new long[this.mTimestamps.size()];
        for (b = 0; b < this.mTimestamps.size(); b++)
          paramTextTrackCue.mInnerTimesMs[b] = ((Long)this.mTimestamps.get(b)).longValue(); 
        this.mTimestamps.clear();
      } else {
        paramTextTrackCue.mInnerTimesMs = null;
      } 
      paramTextTrackCue.mRunID = this.mCurrentRunID.longValue();
      addCue(paramTextTrackCue);
      return;
    } 
  }
  
  public void onRegionParsed(TextTrackRegion paramTextTrackRegion) {
    synchronized (this.mParser) {
      this.mRegions.put(paramTextTrackRegion.mId, paramTextTrackRegion);
      return;
    } 
  }
  
  public void updateView(Vector<SubtitleTrack.Cue> paramVector) {
    if (!this.mVisible)
      return; 
    if (this.DEBUG && this.mTimeProvider != null)
      try {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("at ");
        MediaTimeProvider mediaTimeProvider = this.mTimeProvider;
        stringBuilder.append(mediaTimeProvider.getCurrentTimeUs(false, true) / 1000L);
        stringBuilder.append(" ms the active cues are:");
        String str = stringBuilder.toString();
        Log.d("WebVttTrack", str);
      } catch (IllegalStateException illegalStateException) {
        Log.d("WebVttTrack", "at (illegal state) the active cues are:");
      }  
    WebVttRenderingWidget webVttRenderingWidget = this.mRenderingWidget;
    if (webVttRenderingWidget != null)
      webVttRenderingWidget.setActiveCues(paramVector); 
  }
}
