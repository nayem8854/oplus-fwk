package android.media;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.session.MediaController;
import android.media.session.MediaSessionManager;
import android.media.session.PlaybackState;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.UserHandle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import java.util.List;

@Deprecated
public final class RemoteController {
  private static final Object mInfoLock = new Object();
  
  private MediaController.Callback mSessionCb = new MediaControllerCallback();
  
  private boolean mIsRegistered = false;
  
  private int mArtworkWidth = -1;
  
  private int mArtworkHeight = -1;
  
  private boolean mEnabled = true;
  
  private static final boolean DEBUG = false;
  
  private static final int MAX_BITMAP_DIMENSION = 512;
  
  private static final int MSG_CLIENT_CHANGE = 0;
  
  private static final int MSG_NEW_MEDIA_METADATA = 2;
  
  private static final int MSG_NEW_PLAYBACK_STATE = 1;
  
  public static final int POSITION_SYNCHRONIZATION_CHECK = 1;
  
  public static final int POSITION_SYNCHRONIZATION_NONE = 0;
  
  private static final int SENDMSG_NOOP = 1;
  
  private static final int SENDMSG_QUEUE = 2;
  
  private static final int SENDMSG_REPLACE = 0;
  
  private static final String TAG = "RemoteController";
  
  private final Context mContext;
  
  private MediaController mCurrentSession;
  
  private final EventHandler mEventHandler;
  
  private PlaybackInfo mLastPlaybackInfo;
  
  private final int mMaxBitmapDimension;
  
  private MetadataEditor mMetadataEditor;
  
  private OnClientUpdateListener mOnClientUpdateListener;
  
  private MediaSessionManager.OnActiveSessionsChangedListener mSessionListener;
  
  private MediaSessionManager mSessionManager;
  
  public RemoteController(Context paramContext, OnClientUpdateListener paramOnClientUpdateListener) throws IllegalArgumentException {
    this(paramContext, paramOnClientUpdateListener, null);
  }
  
  public RemoteController(Context paramContext, OnClientUpdateListener paramOnClientUpdateListener, Looper paramLooper) throws IllegalArgumentException {
    if (paramContext != null) {
      if (paramOnClientUpdateListener != null) {
        if (paramLooper != null) {
          this.mEventHandler = new EventHandler(this, paramLooper);
        } else {
          paramLooper = Looper.myLooper();
          if (paramLooper != null) {
            this.mEventHandler = new EventHandler(this, paramLooper);
          } else {
            throw new IllegalArgumentException("Calling thread not associated with a looper");
          } 
        } 
        this.mOnClientUpdateListener = paramOnClientUpdateListener;
        this.mContext = paramContext;
        this.mSessionManager = (MediaSessionManager)paramContext.getSystemService("media_session");
        this.mSessionListener = new TopTransportSessionListener();
        if (ActivityManager.isLowRamDeviceStatic()) {
          this.mMaxBitmapDimension = 512;
        } else {
          DisplayMetrics displayMetrics = paramContext.getResources().getDisplayMetrics();
          this.mMaxBitmapDimension = Math.max(displayMetrics.widthPixels, displayMetrics.heightPixels);
        } 
        return;
      } 
      throw new IllegalArgumentException("Invalid null OnClientUpdateListener");
    } 
    throw new IllegalArgumentException("Invalid null Context");
  }
  
  public long getEstimatedMediaPosition() {
    synchronized (mInfoLock) {
      if (this.mCurrentSession != null) {
        PlaybackState playbackState = this.mCurrentSession.getPlaybackState();
        if (playbackState != null)
          return playbackState.getPosition(); 
      } 
      return -1L;
    } 
  }
  
  public boolean sendMediaKeyEvent(KeyEvent paramKeyEvent) throws IllegalArgumentException {
    if (KeyEvent.isMediaSessionKey(paramKeyEvent.getKeyCode()))
      synchronized (mInfoLock) {
        if (this.mCurrentSession != null)
          return this.mCurrentSession.dispatchMediaButtonEvent(paramKeyEvent); 
        return false;
      }  
    throw new IllegalArgumentException("not a media key event");
  }
  
  public boolean seekTo(long paramLong) throws IllegalArgumentException {
    if (!this.mEnabled) {
      Log.e("RemoteController", "Cannot use seekTo() from a disabled RemoteController");
      return false;
    } 
    if (paramLong >= 0L)
      synchronized (mInfoLock) {
        if (this.mCurrentSession != null)
          this.mCurrentSession.getTransportControls().seekTo(paramLong); 
        return true;
      }  
    throw new IllegalArgumentException("illegal negative time value");
  }
  
  public boolean setArtworkConfiguration(boolean paramBoolean, int paramInt1, int paramInt2) throws IllegalArgumentException {
    // Byte code:
    //   0: getstatic android/media/RemoteController.mInfoLock : Ljava/lang/Object;
    //   3: astore #4
    //   5: aload #4
    //   7: monitorenter
    //   8: iload_1
    //   9: ifeq -> 82
    //   12: iload_2
    //   13: ifle -> 66
    //   16: iload_3
    //   17: ifle -> 66
    //   20: iload_2
    //   21: istore #5
    //   23: iload_2
    //   24: aload_0
    //   25: getfield mMaxBitmapDimension : I
    //   28: if_icmple -> 37
    //   31: aload_0
    //   32: getfield mMaxBitmapDimension : I
    //   35: istore #5
    //   37: iload_3
    //   38: istore_2
    //   39: iload_3
    //   40: aload_0
    //   41: getfield mMaxBitmapDimension : I
    //   44: if_icmple -> 52
    //   47: aload_0
    //   48: getfield mMaxBitmapDimension : I
    //   51: istore_2
    //   52: aload_0
    //   53: iload #5
    //   55: putfield mArtworkWidth : I
    //   58: aload_0
    //   59: iload_2
    //   60: putfield mArtworkHeight : I
    //   63: goto -> 92
    //   66: new java/lang/IllegalArgumentException
    //   69: astore #6
    //   71: aload #6
    //   73: ldc_w 'Invalid dimensions'
    //   76: invokespecial <init> : (Ljava/lang/String;)V
    //   79: aload #6
    //   81: athrow
    //   82: aload_0
    //   83: iconst_m1
    //   84: putfield mArtworkWidth : I
    //   87: aload_0
    //   88: iconst_m1
    //   89: putfield mArtworkHeight : I
    //   92: aload #4
    //   94: monitorexit
    //   95: iconst_1
    //   96: ireturn
    //   97: astore #6
    //   99: aload #4
    //   101: monitorexit
    //   102: aload #6
    //   104: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #298	-> 0
    //   #299	-> 8
    //   #300	-> 12
    //   #301	-> 20
    //   #302	-> 37
    //   #303	-> 52
    //   #304	-> 58
    //   #306	-> 66
    //   #309	-> 82
    //   #310	-> 87
    //   #312	-> 92
    //   #313	-> 95
    //   #312	-> 97
    // Exception table:
    //   from	to	target	type
    //   23	37	97	finally
    //   39	52	97	finally
    //   52	58	97	finally
    //   58	63	97	finally
    //   66	82	97	finally
    //   82	87	97	finally
    //   87	92	97	finally
    //   92	95	97	finally
    //   99	102	97	finally
  }
  
  public boolean setArtworkConfiguration(int paramInt1, int paramInt2) throws IllegalArgumentException {
    return setArtworkConfiguration(true, paramInt1, paramInt2);
  }
  
  public boolean clearArtworkConfiguration() {
    return setArtworkConfiguration(false, -1, -1);
  }
  
  public boolean setSynchronizationMode(int paramInt) throws IllegalArgumentException {
    if (paramInt == 0 || paramInt == 1) {
      if (!this.mIsRegistered) {
        Log.e("RemoteController", "Cannot set synchronization mode on an unregistered RemoteController");
        return false;
      } 
      return true;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown synchronization mode ");
    stringBuilder.append(paramInt);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public MetadataEditor editMetadata() {
    MetadataEditor metadataEditor = new MetadataEditor();
    metadataEditor.mEditorMetadata = new Bundle();
    metadataEditor.mEditorArtwork = null;
    metadataEditor.mMetadataChanged = true;
    metadataEditor.mArtworkChanged = true;
    metadataEditor.mEditableKeys = 0L;
    return metadataEditor;
  }
  
  class MetadataEditor extends MediaMetadataEditor {
    final RemoteController this$0;
    
    protected MetadataEditor() {}
    
    protected MetadataEditor(Bundle param1Bundle, long param1Long) {
      this.mEditorMetadata = param1Bundle;
      this.mEditableKeys = param1Long;
      this.mEditorArtwork = param1Bundle.<Bitmap>getParcelable(String.valueOf(100));
      if (this.mEditorArtwork != null)
        cleanupBitmapFromBundle(100); 
      this.mMetadataChanged = true;
      this.mArtworkChanged = true;
      this.mApplied = false;
    }
    
    private void cleanupBitmapFromBundle(int param1Int) {
      if (METADATA_KEYS_TYPE.get(param1Int, -1) == 2)
        this.mEditorMetadata.remove(String.valueOf(param1Int)); 
    }
    
    public void apply() {
      // Byte code:
      //   0: aload_0
      //   1: monitorenter
      //   2: aload_0
      //   3: getfield mMetadataChanged : Z
      //   6: istore_1
      //   7: iload_1
      //   8: ifne -> 14
      //   11: aload_0
      //   12: monitorexit
      //   13: return
      //   14: invokestatic access$200 : ()Ljava/lang/Object;
      //   17: astore_2
      //   18: aload_2
      //   19: monitorenter
      //   20: aload_0
      //   21: getfield this$0 : Landroid/media/RemoteController;
      //   24: invokestatic access$300 : (Landroid/media/RemoteController;)Landroid/media/session/MediaController;
      //   27: astore_3
      //   28: aload_3
      //   29: ifnull -> 85
      //   32: aload_0
      //   33: getfield mEditorMetadata : Landroid/os/Bundle;
      //   36: astore_3
      //   37: aload_3
      //   38: ldc 268435457
      //   40: invokestatic valueOf : (I)Ljava/lang/String;
      //   43: invokevirtual containsKey : (Ljava/lang/String;)Z
      //   46: ifeq -> 85
      //   49: aload_0
      //   50: ldc 268435457
      //   52: aconst_null
      //   53: invokevirtual getObject : (ILjava/lang/Object;)Ljava/lang/Object;
      //   56: checkcast android/media/Rating
      //   59: astore_3
      //   60: aload_3
      //   61: ifnull -> 85
      //   64: aload_0
      //   65: getfield this$0 : Landroid/media/RemoteController;
      //   68: invokestatic access$300 : (Landroid/media/RemoteController;)Landroid/media/session/MediaController;
      //   71: invokevirtual getTransportControls : ()Landroid/media/session/MediaController$TransportControls;
      //   74: aload_3
      //   75: invokevirtual setRating : (Landroid/media/Rating;)V
      //   78: goto -> 85
      //   81: astore_3
      //   82: goto -> 96
      //   85: aload_2
      //   86: monitorexit
      //   87: aload_0
      //   88: iconst_0
      //   89: putfield mApplied : Z
      //   92: aload_0
      //   93: monitorexit
      //   94: return
      //   95: astore_3
      //   96: aload_2
      //   97: monitorexit
      //   98: aload_3
      //   99: athrow
      //   100: astore_3
      //   101: aload_0
      //   102: monitorexit
      //   103: aload_3
      //   104: athrow
      // Line number table:
      //   Java source line number -> byte code offset
      //   #434	-> 2
      //   #435	-> 11
      //   #437	-> 14
      //   #438	-> 20
      //   #439	-> 32
      //   #440	-> 37
      //   #439	-> 37
      //   #441	-> 49
      //   #443	-> 60
      //   #444	-> 64
      //   #448	-> 81
      //   #452	-> 87
      //   #453	-> 92
      //   #448	-> 95
      //   #433	-> 100
      // Exception table:
      //   from	to	target	type
      //   2	7	100	finally
      //   14	20	100	finally
      //   20	28	95	finally
      //   32	37	81	finally
      //   37	49	81	finally
      //   49	60	81	finally
      //   64	78	81	finally
      //   85	87	95	finally
      //   87	92	100	finally
      //   96	98	95	finally
      //   98	100	100	finally
    }
  }
  
  class MediaControllerCallback extends MediaController.Callback {
    final RemoteController this$0;
    
    private MediaControllerCallback() {}
    
    public void onPlaybackStateChanged(PlaybackState param1PlaybackState) {
      RemoteController.this.onNewPlaybackState(param1PlaybackState);
    }
    
    public void onMetadataChanged(MediaMetadata param1MediaMetadata) {
      RemoteController.this.onNewMediaMetadata(param1MediaMetadata);
    }
  }
  
  class TopTransportSessionListener implements MediaSessionManager.OnActiveSessionsChangedListener {
    final RemoteController this$0;
    
    private TopTransportSessionListener() {}
    
    public void onActiveSessionsChanged(List<MediaController> param1List) {
      int i = param1List.size();
      for (byte b = 0; b < i; b++) {
        MediaController mediaController = param1List.get(b);
        long l = mediaController.getFlags();
        if ((0x2L & l) != 0L) {
          RemoteController.this.updateController(mediaController);
          return;
        } 
      } 
      RemoteController.this.updateController(null);
    }
  }
  
  class EventHandler extends Handler {
    final RemoteController this$0;
    
    public EventHandler(RemoteController param1RemoteController1, Looper param1Looper) {
      super(param1Looper);
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      boolean bool = true;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("unknown event ");
            stringBuilder.append(param1Message.what);
            Log.e("RemoteController", stringBuilder.toString());
          } else {
            RemoteController.this.onNewMediaMetadata((MediaMetadata)param1Message.obj);
          } 
        } else {
          RemoteController.this.onNewPlaybackState((PlaybackState)param1Message.obj);
        } 
      } else {
        RemoteController remoteController = RemoteController.this;
        if (param1Message.arg2 != 1)
          bool = false; 
        remoteController.onClientChange(bool);
      } 
    }
  }
  
  void startListeningToSessions() {
    Handler handler;
    Context context = this.mContext;
    OnClientUpdateListener onClientUpdateListener = this.mOnClientUpdateListener;
    ComponentName componentName = new ComponentName(context, onClientUpdateListener.getClass());
    context = null;
    if (Looper.myLooper() == null)
      handler = new Handler(Looper.getMainLooper()); 
    MediaSessionManager mediaSessionManager = this.mSessionManager;
    MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener2 = this.mSessionListener;
    int i = UserHandle.myUserId();
    mediaSessionManager.addOnActiveSessionsChangedListener(onActiveSessionsChangedListener2, componentName, i, handler);
    MediaSessionManager.OnActiveSessionsChangedListener onActiveSessionsChangedListener1 = this.mSessionListener;
    mediaSessionManager = this.mSessionManager;
    List<MediaController> list = mediaSessionManager.getActiveSessions(componentName);
    onActiveSessionsChangedListener1.onActiveSessionsChanged(list);
  }
  
  void stopListeningToSessions() {
    this.mSessionManager.removeOnActiveSessionsChangedListener(this.mSessionListener);
  }
  
  private static void sendMsg(Handler paramHandler, int paramInt1, int paramInt2, int paramInt3, int paramInt4, Object paramObject, int paramInt5) {
    StringBuilder stringBuilder;
    if (paramHandler == null) {
      stringBuilder = new StringBuilder();
      stringBuilder.append("null event handler, will not deliver message ");
      stringBuilder.append(paramInt1);
      Log.e("RemoteController", stringBuilder.toString());
      return;
    } 
    if (paramInt2 == 0) {
      stringBuilder.removeMessages(paramInt1);
    } else if (paramInt2 == 1 && stringBuilder.hasMessages(paramInt1)) {
      return;
    } 
    stringBuilder.sendMessageDelayed(stringBuilder.obtainMessage(paramInt1, paramInt3, paramInt4, paramObject), paramInt5);
  }
  
  private void onClientChange(boolean paramBoolean) {
    synchronized (mInfoLock) {
      OnClientUpdateListener onClientUpdateListener = this.mOnClientUpdateListener;
      this.mMetadataEditor = null;
      if (onClientUpdateListener != null)
        onClientUpdateListener.onClientChange(paramBoolean); 
      return;
    } 
  }
  
  private void updateController(MediaController paramMediaController) {
    // Byte code:
    //   0: getstatic android/media/RemoteController.mInfoLock : Ljava/lang/Object;
    //   3: astore_2
    //   4: aload_2
    //   5: monitorenter
    //   6: aload_1
    //   7: ifnonnull -> 49
    //   10: aload_0
    //   11: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   14: ifnull -> 165
    //   17: aload_0
    //   18: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   21: aload_0
    //   22: getfield mSessionCb : Landroid/media/session/MediaController$Callback;
    //   25: invokevirtual unregisterCallback : (Landroid/media/session/MediaController$Callback;)V
    //   28: aload_0
    //   29: aconst_null
    //   30: putfield mCurrentSession : Landroid/media/session/MediaController;
    //   33: aload_0
    //   34: getfield mEventHandler : Landroid/media/RemoteController$EventHandler;
    //   37: iconst_0
    //   38: iconst_0
    //   39: iconst_0
    //   40: iconst_1
    //   41: aconst_null
    //   42: iconst_0
    //   43: invokestatic sendMsg : (Landroid/os/Handler;IIIILjava/lang/Object;I)V
    //   46: goto -> 165
    //   49: aload_0
    //   50: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   53: ifnull -> 79
    //   56: aload_1
    //   57: invokevirtual getSessionToken : ()Landroid/media/session/MediaSession$Token;
    //   60: astore_3
    //   61: aload_0
    //   62: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   65: astore #4
    //   67: aload_3
    //   68: aload #4
    //   70: invokevirtual getSessionToken : ()Landroid/media/session/MediaSession$Token;
    //   73: invokevirtual equals : (Ljava/lang/Object;)Z
    //   76: ifne -> 165
    //   79: aload_0
    //   80: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   83: ifnull -> 97
    //   86: aload_0
    //   87: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   90: aload_0
    //   91: getfield mSessionCb : Landroid/media/session/MediaController$Callback;
    //   94: invokevirtual unregisterCallback : (Landroid/media/session/MediaController$Callback;)V
    //   97: aload_0
    //   98: getfield mEventHandler : Landroid/media/RemoteController$EventHandler;
    //   101: iconst_0
    //   102: iconst_0
    //   103: iconst_0
    //   104: iconst_0
    //   105: aconst_null
    //   106: iconst_0
    //   107: invokestatic sendMsg : (Landroid/os/Handler;IIIILjava/lang/Object;I)V
    //   110: aload_0
    //   111: aload_1
    //   112: putfield mCurrentSession : Landroid/media/session/MediaController;
    //   115: aload_1
    //   116: aload_0
    //   117: getfield mSessionCb : Landroid/media/session/MediaController$Callback;
    //   120: aload_0
    //   121: getfield mEventHandler : Landroid/media/RemoteController$EventHandler;
    //   124: invokevirtual registerCallback : (Landroid/media/session/MediaController$Callback;Landroid/os/Handler;)V
    //   127: aload_1
    //   128: invokevirtual getPlaybackState : ()Landroid/media/session/PlaybackState;
    //   131: astore #4
    //   133: aload_0
    //   134: getfield mEventHandler : Landroid/media/RemoteController$EventHandler;
    //   137: iconst_1
    //   138: iconst_0
    //   139: iconst_0
    //   140: iconst_0
    //   141: aload #4
    //   143: iconst_0
    //   144: invokestatic sendMsg : (Landroid/os/Handler;IIIILjava/lang/Object;I)V
    //   147: aload_1
    //   148: invokevirtual getMetadata : ()Landroid/media/MediaMetadata;
    //   151: astore_1
    //   152: aload_0
    //   153: getfield mEventHandler : Landroid/media/RemoteController$EventHandler;
    //   156: iconst_2
    //   157: iconst_0
    //   158: iconst_0
    //   159: iconst_0
    //   160: aload_1
    //   161: iconst_0
    //   162: invokestatic sendMsg : (Landroid/os/Handler;IIIILjava/lang/Object;I)V
    //   165: aload_2
    //   166: monitorexit
    //   167: return
    //   168: astore_1
    //   169: aload_2
    //   170: monitorexit
    //   171: aload_1
    //   172: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #598	-> 0
    //   #599	-> 6
    //   #600	-> 10
    //   #601	-> 17
    //   #602	-> 28
    //   #603	-> 33
    //   #606	-> 49
    //   #607	-> 56
    //   #608	-> 67
    //   #609	-> 79
    //   #610	-> 86
    //   #612	-> 97
    //   #614	-> 110
    //   #615	-> 115
    //   #617	-> 127
    //   #618	-> 133
    //   #621	-> 147
    //   #622	-> 152
    //   #626	-> 165
    //   #627	-> 167
    //   #626	-> 168
    // Exception table:
    //   from	to	target	type
    //   10	17	168	finally
    //   17	28	168	finally
    //   28	33	168	finally
    //   33	46	168	finally
    //   49	56	168	finally
    //   56	67	168	finally
    //   67	79	168	finally
    //   79	86	168	finally
    //   86	97	168	finally
    //   97	110	168	finally
    //   110	115	168	finally
    //   115	127	168	finally
    //   127	133	168	finally
    //   133	147	168	finally
    //   147	152	168	finally
    //   152	165	168	finally
    //   165	167	168	finally
    //   169	171	168	finally
  }
  
  private void onNewPlaybackState(PlaybackState paramPlaybackState) {
    synchronized (mInfoLock) {
      OnClientUpdateListener onClientUpdateListener = this.mOnClientUpdateListener;
      if (onClientUpdateListener != null) {
        int i;
        if (paramPlaybackState == null) {
          i = 0;
        } else {
          i = RemoteControlClient.getRccStateFromState(paramPlaybackState.getState());
        } 
        if (paramPlaybackState == null || paramPlaybackState.getPosition() == -1L) {
          onClientUpdateListener.onClientPlaybackStateUpdate(i);
        } else {
          long l1 = paramPlaybackState.getLastPositionUpdateTime();
          long l2 = paramPlaybackState.getPosition();
          float f = paramPlaybackState.getPlaybackSpeed();
          onClientUpdateListener.onClientPlaybackStateUpdate(i, l1, l2, f);
        } 
        if (paramPlaybackState != null) {
          i = RemoteControlClient.getRccControlFlagsFromActions(paramPlaybackState.getActions());
          onClientUpdateListener.onClientTransportControlUpdate(i);
        } 
      } 
      return;
    } 
  }
  
  private void onNewMediaMetadata(MediaMetadata paramMediaMetadata) {
    // Byte code:
    //   0: aload_1
    //   1: ifnonnull -> 5
    //   4: return
    //   5: getstatic android/media/RemoteController.mInfoLock : Ljava/lang/Object;
    //   8: astore_2
    //   9: aload_2
    //   10: monitorenter
    //   11: aload_0
    //   12: getfield mOnClientUpdateListener : Landroid/media/RemoteController$OnClientUpdateListener;
    //   15: astore_3
    //   16: aload_0
    //   17: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   20: ifnull -> 43
    //   23: aload_0
    //   24: getfield mCurrentSession : Landroid/media/session/MediaController;
    //   27: astore #4
    //   29: aload #4
    //   31: invokevirtual getRatingType : ()I
    //   34: ifeq -> 43
    //   37: iconst_1
    //   38: istore #5
    //   40: goto -> 46
    //   43: iconst_0
    //   44: istore #5
    //   46: iload #5
    //   48: ifeq -> 59
    //   51: ldc2_w 268435457
    //   54: lstore #6
    //   56: goto -> 62
    //   59: lconst_0
    //   60: lstore #6
    //   62: aload_1
    //   63: aload_0
    //   64: getfield mArtworkWidth : I
    //   67: aload_0
    //   68: getfield mArtworkHeight : I
    //   71: invokestatic getOldMetadata : (Landroid/media/MediaMetadata;II)Landroid/os/Bundle;
    //   74: astore #4
    //   76: new android/media/RemoteController$MetadataEditor
    //   79: astore_1
    //   80: aload_1
    //   81: aload_0
    //   82: aload #4
    //   84: lload #6
    //   86: invokespecial <init> : (Landroid/media/RemoteController;Landroid/os/Bundle;J)V
    //   89: aload_0
    //   90: aload_1
    //   91: putfield mMetadataEditor : Landroid/media/RemoteController$MetadataEditor;
    //   94: aload_2
    //   95: monitorexit
    //   96: aload_3
    //   97: ifnull -> 107
    //   100: aload_3
    //   101: aload_1
    //   102: invokeinterface onClientMetadataUpdate : (Landroid/media/RemoteController$MetadataEditor;)V
    //   107: return
    //   108: astore_1
    //   109: aload_2
    //   110: monitorexit
    //   111: aload_1
    //   112: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #651	-> 0
    //   #653	-> 4
    //   #658	-> 5
    //   #659	-> 11
    //   #660	-> 16
    //   #661	-> 29
    //   #662	-> 46
    //   #663	-> 62
    //   #665	-> 76
    //   #666	-> 94
    //   #667	-> 94
    //   #668	-> 96
    //   #669	-> 100
    //   #671	-> 107
    //   #667	-> 108
    // Exception table:
    //   from	to	target	type
    //   11	16	108	finally
    //   16	29	108	finally
    //   29	37	108	finally
    //   62	76	108	finally
    //   76	94	108	finally
    //   94	96	108	finally
    //   109	111	108	finally
  }
  
  private static class PlaybackInfo {
    long mCurrentPosMs;
    
    float mSpeed;
    
    int mState;
    
    long mStateChangeTimeMs;
    
    PlaybackInfo(int param1Int, long param1Long1, long param1Long2, float param1Float) {
      this.mState = param1Int;
      this.mStateChangeTimeMs = param1Long1;
      this.mCurrentPosMs = param1Long2;
      this.mSpeed = param1Float;
    }
  }
  
  OnClientUpdateListener getUpdateListener() {
    return this.mOnClientUpdateListener;
  }
  
  public static interface OnClientUpdateListener {
    void onClientChange(boolean param1Boolean);
    
    void onClientMetadataUpdate(RemoteController.MetadataEditor param1MetadataEditor);
    
    void onClientPlaybackStateUpdate(int param1Int);
    
    void onClientPlaybackStateUpdate(int param1Int, long param1Long1, long param1Long2, float param1Float);
    
    void onClientTransportControlUpdate(int param1Int);
  }
}
