package android.media;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Environment;
import android.os.FileUtils;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.util.Range;
import android.util.Rational;
import android.util.Size;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Vector;
import java.util.concurrent.Executor;

public class Utils {
  public static <T extends Comparable<? super T>> void sortDistinctRanges(Range<T>[] paramArrayOfRange) {
    Arrays.sort(paramArrayOfRange, (Comparator)new Comparator<Range<Range<T>>>() {
          public int compare(Range<T> param1Range1, Range<T> param1Range2) {
            if (param1Range1.getUpper().compareTo(param1Range2.getLower()) < 0)
              return -1; 
            if (param1Range1.getLower().compareTo(param1Range2.getUpper()) > 0)
              return 1; 
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("sample rate ranges must be distinct (");
            stringBuilder.append(param1Range1);
            stringBuilder.append(" and ");
            stringBuilder.append(param1Range2);
            stringBuilder.append(")");
            throw new IllegalArgumentException(stringBuilder.toString());
          }
        });
  }
  
  public static <T extends Comparable<? super T>> Range<T>[] intersectSortedDistinctRanges(Range<T>[] paramArrayOfRange1, Range<T>[] paramArrayOfRange2) {
    byte b1 = 0;
    Vector<Range> vector = new Vector();
    int i;
    byte b2;
    for (i = paramArrayOfRange2.length, b2 = 0; b2 < i; ) {
      Range<T> range = paramArrayOfRange2[b2];
      byte b = b1;
      while (true) {
        b1 = b;
        if (b < paramArrayOfRange1.length) {
          Range<T> range1 = paramArrayOfRange1[b];
          b1 = b;
          if (range1.getUpper().compareTo(range.getLower()) < 0) {
            b++;
            continue;
          } 
        } 
        break;
      } 
      while (b1 < paramArrayOfRange1.length) {
        Range<T> range1 = paramArrayOfRange1[b1];
        if (range1.getUpper().compareTo(range.getUpper()) < 0) {
          vector.add(range.intersect(paramArrayOfRange1[b1]));
          b1++;
        } 
      } 
      if (b1 == paramArrayOfRange1.length)
        break; 
      if (paramArrayOfRange1[b1].getLower().compareTo(range.getUpper()) <= 0)
        vector.add(range.intersect(paramArrayOfRange1[b1])); 
      b2++;
    } 
    return vector.<Range<T>>toArray((Range<T>[])new Range[vector.size()]);
  }
  
  public static <T extends Comparable<? super T>> int binarySearchDistinctRanges(Range<T>[] paramArrayOfRange, T paramT) {
    return Arrays.binarySearch(paramArrayOfRange, Range.create((Comparable)paramT, (Comparable)paramT), (Comparator)new Comparator<Range<Range<T>>>() {
          public int compare(Range<T> param1Range1, Range<T> param1Range2) {
            if (param1Range1.getUpper().compareTo(param1Range2.getLower()) < 0)
              return -1; 
            if (param1Range1.getLower().compareTo(param1Range2.getUpper()) > 0)
              return 1; 
            return 0;
          }
        });
  }
  
  static int gcd(int paramInt1, int paramInt2) {
    if (paramInt1 == 0 && paramInt2 == 0)
      return 1; 
    int i = paramInt2;
    if (paramInt2 < 0)
      i = -paramInt2; 
    paramInt2 = paramInt1;
    int j = i;
    if (paramInt1 < 0) {
      paramInt2 = -paramInt1;
      j = i;
    } 
    while (true) {
      paramInt1 = j;
      if (paramInt2 != 0) {
        j = paramInt2;
        paramInt2 = paramInt1 % paramInt2;
        continue;
      } 
      break;
    } 
    return paramInt1;
  }
  
  static Range<Integer> factorRange(Range<Integer> paramRange, int paramInt) {
    if (paramInt == 1)
      return paramRange; 
    return Range.create(Integer.valueOf(divUp(((Integer)paramRange.getLower()).intValue(), paramInt)), Integer.valueOf(((Integer)paramRange.getUpper()).intValue() / paramInt));
  }
  
  static Range<Long> factorRange(Range<Long> paramRange, long paramLong) {
    if (paramLong == 1L)
      return paramRange; 
    return Range.create(Long.valueOf(divUp(((Long)paramRange.getLower()).longValue(), paramLong)), Long.valueOf(((Long)paramRange.getUpper()).longValue() / paramLong));
  }
  
  private static Rational scaleRatio(Rational paramRational, int paramInt1, int paramInt2) {
    int i = gcd(paramInt1, paramInt2);
    paramInt1 /= i;
    paramInt2 /= i;
    paramInt1 = (int)(paramRational.getNumerator() * paramInt1);
    return new Rational(paramInt1, (int)(paramRational.getDenominator() * paramInt2));
  }
  
  static Range<Rational> scaleRange(Range<Rational> paramRange, int paramInt1, int paramInt2) {
    if (paramInt1 == paramInt2)
      return paramRange; 
    Rational rational2 = scaleRatio((Rational)paramRange.getLower(), paramInt1, paramInt2);
    Rational rational1 = scaleRatio((Rational)paramRange.getUpper(), paramInt1, paramInt2);
    return Range.create((Comparable)rational2, (Comparable)rational1);
  }
  
  static Range<Integer> alignRange(Range<Integer> paramRange, int paramInt) {
    int i = divUp(((Integer)paramRange.getLower()).intValue(), paramInt);
    int j = ((Integer)paramRange.getUpper()).intValue() / paramInt;
    return paramRange.intersect(Integer.valueOf(i * paramInt), Integer.valueOf(j * paramInt));
  }
  
  static int divUp(int paramInt1, int paramInt2) {
    return (paramInt1 + paramInt2 - 1) / paramInt2;
  }
  
  static long divUp(long paramLong1, long paramLong2) {
    return (paramLong1 + paramLong2 - 1L) / paramLong2;
  }
  
  private static long lcm(int paramInt1, int paramInt2) {
    if (paramInt1 != 0 && paramInt2 != 0)
      return paramInt1 * paramInt2 / gcd(paramInt1, paramInt2); 
    throw new IllegalArgumentException("lce is not defined for zero arguments");
  }
  
  static Range<Integer> intRangeFor(double paramDouble) {
    return Range.create(Integer.valueOf((int)paramDouble), Integer.valueOf((int)Math.ceil(paramDouble)));
  }
  
  static Range<Long> longRangeFor(double paramDouble) {
    return Range.create(Long.valueOf((long)paramDouble), Long.valueOf((long)Math.ceil(paramDouble)));
  }
  
  static Size parseSize(Object paramObject, Size paramSize) {
    if (paramObject == null)
      return paramSize; 
    try {
      return Size.parseSize((String)paramObject);
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse size '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return paramSize;
  }
  
  static int parseIntSafely(Object paramObject, int paramInt) {
    if (paramObject == null)
      return paramInt; 
    try {
      String str = (String)paramObject;
      return Integer.parseInt(str);
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse integer '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return paramInt;
  }
  
  static Range<Integer> parseIntRange(Object paramObject, Range<Integer> paramRange) {
    if (paramObject == null)
      return paramRange; 
    try {
      String str = (String)paramObject;
      int i = str.indexOf('-');
      if (i >= 0) {
        int k = Integer.parseInt(str.substring(0, i), 10);
        i = Integer.parseInt(str.substring(i + 1), 10);
        return Range.create(Integer.valueOf(k), Integer.valueOf(i));
      } 
      int j = Integer.parseInt(str);
      return Range.create(Integer.valueOf(j), Integer.valueOf(j));
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {
    
    } catch (IllegalArgumentException illegalArgumentException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse integer range '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return paramRange;
  }
  
  static Range<Long> parseLongRange(Object paramObject, Range<Long> paramRange) {
    if (paramObject == null)
      return paramRange; 
    try {
      String str = (String)paramObject;
      int i = str.indexOf('-');
      if (i >= 0) {
        long l1 = Long.parseLong(str.substring(0, i), 10);
        long l2 = Long.parseLong(str.substring(i + 1), 10);
        return Range.create(Long.valueOf(l1), Long.valueOf(l2));
      } 
      long l = Long.parseLong(str);
      return Range.create(Long.valueOf(l), Long.valueOf(l));
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {
    
    } catch (IllegalArgumentException illegalArgumentException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse long range '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return paramRange;
  }
  
  static Range<Rational> parseRationalRange(Object paramObject, Range<Rational> paramRange) {
    if (paramObject == null)
      return paramRange; 
    try {
      Rational rational1;
      String str = (String)paramObject;
      int i = str.indexOf('-');
      if (i >= 0) {
        Rational rational = Rational.parseRational(str.substring(0, i));
        rational1 = Rational.parseRational(str.substring(i + 1));
        return Range.create((Comparable)rational, (Comparable)rational1);
      } 
      Rational rational2 = Rational.parseRational((String)rational1);
      return Range.create((Comparable)rational2, (Comparable)rational2);
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {
    
    } catch (IllegalArgumentException illegalArgumentException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse rational range '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return paramRange;
  }
  
  static Pair<Size, Size> parseSizeRange(Object paramObject) {
    if (paramObject == null)
      return null; 
    try {
      Size size1;
      String str = (String)paramObject;
      int i = str.indexOf('-');
      if (i >= 0) {
        Size size = Size.parseSize(str.substring(0, i));
        size1 = Size.parseSize(str.substring(i + 1));
        return Pair.create(size, size1);
      } 
      Size size2 = Size.parseSize((String)size1);
      return Pair.create(size2, size2);
    } catch (ClassCastException classCastException) {
    
    } catch (NumberFormatException numberFormatException) {
    
    } catch (IllegalArgumentException illegalArgumentException) {}
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("could not parse size range '");
    stringBuilder.append(paramObject);
    stringBuilder.append("'");
    Log.w("Utils", stringBuilder.toString());
    return null;
  }
  
  public static File getUniqueExternalFile(Context paramContext, String paramString1, String paramString2, String paramString3) {
    File file = Environment.getExternalStoragePublicDirectory(paramString1);
    file.mkdirs();
    try {
      file = FileUtils.buildUniqueFile(file, paramString3, paramString2);
      return file;
    } catch (FileNotFoundException fileNotFoundException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Unable to get a unique file name: ");
      stringBuilder.append(fileNotFoundException);
      Log.e("Utils", stringBuilder.toString());
      return null;
    } 
  }
  
  static String getFileDisplayNameFromUri(Context paramContext, Uri paramUri) {
    String str = paramUri.getScheme();
    if ("file".equals(str))
      return paramUri.getLastPathSegment(); 
    if ("content".equals(str)) {
      Cursor cursor = paramContext.getContentResolver().query(paramUri, new String[] { "_display_name" }, null, null, null);
      if (cursor != null)
        try {
          if (cursor.getCount() != 0) {
            cursor.moveToFirst();
            return cursor.getString(cursor.getColumnIndex("_display_name"));
          } 
        } finally {
          if (cursor != null)
            try {
              cursor.close();
            } finally {
              cursor = null;
            }  
        }  
      if (cursor != null)
        cursor.close(); 
    } 
    return paramUri.toString();
  }
  
  public static class ListenerList<V> {
    private final boolean mClearCallingIdentity;
    
    private final boolean mForceRemoveConsistency;
    
    private HashMap<Object, ListenerWithCancellation<V>> mListeners;
    
    private final boolean mRestrictSingleCallerOnEvent;
    
    public ListenerList() {
      this(true, true, false);
    }
    
    public ListenerList(boolean param1Boolean1, boolean param1Boolean2, boolean param1Boolean3) {
      this.mListeners = new HashMap<>();
      this.mRestrictSingleCallerOnEvent = param1Boolean1;
      this.mClearCallingIdentity = param1Boolean2;
      this.mForceRemoveConsistency = param1Boolean3;
    }
    
    public void add(Object param1Object, Executor param1Executor, Listener<V> param1Listener) {
      Objects.requireNonNull(param1Object);
      Objects.requireNonNull(param1Executor);
      Objects.requireNonNull(param1Listener);
      Object object = new Object(this, param1Executor, param1Listener);
      synchronized (this.mListeners) {
        this.mListeners.put(param1Object, object);
        return;
      } 
    }
    
    public void remove(Object param1Object) {
      Objects.requireNonNull(param1Object);
      synchronized (this.mListeners) {
        ListenerWithCancellation listenerWithCancellation = this.mListeners.get(param1Object);
        if (listenerWithCancellation == null)
          return; 
        this.mListeners.remove(param1Object);
        listenerWithCancellation.cancel();
        return;
      } 
    }
    
    public void notify(int param1Int, V param1V) {
      synchronized (this.mListeners) {
        if (this.mListeners.size() == 0)
          return; 
        Object[] arrayOfObject = this.mListeners.values().toArray();
        if (this.mClearCallingIdentity) {
          Long long_ = Long.valueOf(Binder.clearCallingIdentity());
        } else {
          null = null;
        } 
        try {
          int i;
          byte b;
          for (i = arrayOfObject.length, b = 0; b < i; ) {
            Object object = arrayOfObject[b];
            object = object;
            object.onEvent(param1Int, param1V);
            b++;
          } 
          return;
        } finally {
          if (null != null)
            Binder.restoreCallingIdentity(null.longValue()); 
        } 
      } 
    }
    
    public static interface Listener<V> {
      void onEvent(int param2Int, V param2V);
    }
    
    class ListenerWithCancellation<V> implements Listener<V> {
      public abstract void cancel();
    }
  }
  
  private static final String[] BLACKLIST_DIRECTORY = new String[] { "image2", "voice2", "emoji", "avatar", "sns", "openapi", "package", "video" };
  
  private static final String FILE_NO_MEDIA = "/.nomedia";
  
  private static final String[] NEW_BLACKLIST_DIRECTORY = new String[] { "wxafiles", "wxanewfiles" };
  
  private static final String PATH_MICROMSG = "/tencent/MicroMsg";
  
  private static final String TAG = "Utils";
  
  public static void filterBlacklistDirectory() {
    String str1 = "";
    String str2 = str1;
    try {
      if ("mounted".equalsIgnoreCase(Environment.getExternalStorageState()))
        str2 = Environment.getExternalStorageDirectory().toString(); 
    } catch (Exception exception) {
      str2 = str1;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("filterBlacklistDirectory: rootDir = ");
    stringBuilder.append(str2);
    Log.d("Utils", stringBuilder.toString());
    if (TextUtils.isEmpty(str2))
      return; 
    stringBuilder = new StringBuilder();
    stringBuilder.append(str2);
    stringBuilder.append("/tencent/MicroMsg");
    String str3 = stringBuilder.toString();
    File file = new File(str3);
    if (!file.exists())
      return; 
    File[] arrayOfFile = file.listFiles();
    if (arrayOfFile == null)
      return; 
    List<String> list1 = Arrays.asList(BLACKLIST_DIRECTORY);
    List<String> list2 = Arrays.asList(NEW_BLACKLIST_DIRECTORY);
    int i;
    byte b;
    for (i = arrayOfFile.length, b = 0; b < i; ) {
      File file1 = arrayOfFile[b];
      if (file1.isDirectory()) {
        String str4 = file1.getName();
        String str5 = file1.getPath();
        boolean bool = list2.contains(str4);
        String str6 = "filterBlacklistDirectory: ready to insert file in ";
        if (bool) {
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("filterBlacklistDirectory: ready to insert file in ");
          stringBuilder1.append(str4);
          Log.d("Utils", stringBuilder1.toString());
          stringBuilder1 = new StringBuilder();
          stringBuilder1.append(str5);
          stringBuilder1.append("/.nomedia");
          File file2 = new File(stringBuilder1.toString());
          if (file2.exists()) {
            Log.d("Utils", "filterBlacklistDirectory: .nomedia file exists, continue");
          } else {
            try {
              file2.createNewFile();
            } catch (IOException iOException) {
              StringBuilder stringBuilder2 = new StringBuilder();
              stringBuilder2.append("filterBlacklistDirectory: ");
              stringBuilder2.append(iOException.getMessage());
              Log.w("Utils", stringBuilder2.toString());
            } 
          } 
        } else {
          File[] arrayOfFile1 = file1.listFiles();
          if (arrayOfFile1 != null) {
            int j;
            byte b1;
            for (j = arrayOfFile1.length, b1 = 0; b1 < j; ) {
              File file2 = arrayOfFile1[b1];
              if (file2.isDirectory()) {
                String str7 = file2.getName();
                String str8 = file2.getPath();
                if (list1.contains(str7)) {
                  StringBuilder stringBuilder2 = new StringBuilder();
                  stringBuilder2.append((String)iOException);
                  stringBuilder2.append(str7);
                  Log.d("Utils", stringBuilder2.toString());
                  StringBuilder stringBuilder1 = new StringBuilder();
                  stringBuilder1.append(str8);
                  stringBuilder1.append("/.nomedia");
                  File file3 = new File(stringBuilder1.toString());
                  if (file3.exists()) {
                    Log.d("Utils", "filterBlacklistDirectory: .nomedia file exists, continue");
                  } else {
                    try {
                      file3.createNewFile();
                    } catch (IOException iOException1) {
                      stringBuilder1 = new StringBuilder();
                      stringBuilder1.append("filterBlacklistDirectory: ");
                      stringBuilder1.append(iOException1.getMessage());
                      Log.w("Utils", stringBuilder1.toString());
                    } 
                  } 
                } 
              } 
              b1++;
            } 
          } 
        } 
      } 
      b++;
    } 
  }
  
  public static interface Listener<V> {
    void onEvent(int param1Int, V param1V);
  }
  
  class ListenerWithCancellation<V> implements ListenerList.Listener<V> {
    public abstract void cancel();
  }
}
