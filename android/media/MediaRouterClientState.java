package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.ArrayList;

public final class MediaRouterClientState implements Parcelable {
  public MediaRouterClientState() {
    this.routes = new ArrayList<>();
  }
  
  MediaRouterClientState(Parcel paramParcel) {
    this.routes = paramParcel.createTypedArrayList(RouteInfo.CREATOR);
  }
  
  public RouteInfo getRoute(String paramString) {
    int i = this.routes.size();
    for (byte b = 0; b < i; b++) {
      RouteInfo routeInfo = this.routes.get(b);
      if (routeInfo.id.equals(paramString))
        return routeInfo; 
    } 
    return null;
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeTypedList(this.routes);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaRouterClientState{ routes=");
    stringBuilder.append(this.routes.toString());
    stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  public static final Parcelable.Creator<MediaRouterClientState> CREATOR = new Parcelable.Creator<MediaRouterClientState>() {
      public MediaRouterClientState createFromParcel(Parcel param1Parcel) {
        return new MediaRouterClientState(param1Parcel);
      }
      
      public MediaRouterClientState[] newArray(int param1Int) {
        return new MediaRouterClientState[param1Int];
      }
    };
  
  public final ArrayList<RouteInfo> routes;
  
  public static final class RouteInfo implements Parcelable {
    public RouteInfo(String param1String) {
      this.id = param1String;
      this.enabled = true;
      this.statusCode = 0;
      this.playbackType = 1;
      this.playbackStream = -1;
      this.volumeHandling = 0;
      this.presentationDisplayId = -1;
      this.deviceType = 0;
    }
    
    public RouteInfo(RouteInfo param1RouteInfo) {
      this.id = param1RouteInfo.id;
      this.name = param1RouteInfo.name;
      this.description = param1RouteInfo.description;
      this.supportedTypes = param1RouteInfo.supportedTypes;
      this.enabled = param1RouteInfo.enabled;
      this.statusCode = param1RouteInfo.statusCode;
      this.playbackType = param1RouteInfo.playbackType;
      this.playbackStream = param1RouteInfo.playbackStream;
      this.volume = param1RouteInfo.volume;
      this.volumeMax = param1RouteInfo.volumeMax;
      this.volumeHandling = param1RouteInfo.volumeHandling;
      this.presentationDisplayId = param1RouteInfo.presentationDisplayId;
      this.deviceType = param1RouteInfo.deviceType;
    }
    
    RouteInfo(Parcel param1Parcel) {
      boolean bool;
      this.id = param1Parcel.readString();
      this.name = param1Parcel.readString();
      this.description = param1Parcel.readString();
      this.supportedTypes = param1Parcel.readInt();
      if (param1Parcel.readInt() != 0) {
        bool = true;
      } else {
        bool = false;
      } 
      this.enabled = bool;
      this.statusCode = param1Parcel.readInt();
      this.playbackType = param1Parcel.readInt();
      this.playbackStream = param1Parcel.readInt();
      this.volume = param1Parcel.readInt();
      this.volumeMax = param1Parcel.readInt();
      this.volumeHandling = param1Parcel.readInt();
      this.presentationDisplayId = param1Parcel.readInt();
      this.deviceType = param1Parcel.readInt();
    }
    
    public int describeContents() {
      return 0;
    }
    
    public void writeToParcel(Parcel param1Parcel, int param1Int) {
      param1Parcel.writeString(this.id);
      param1Parcel.writeString(this.name);
      param1Parcel.writeString(this.description);
      param1Parcel.writeInt(this.supportedTypes);
      param1Parcel.writeInt(this.enabled);
      param1Parcel.writeInt(this.statusCode);
      param1Parcel.writeInt(this.playbackType);
      param1Parcel.writeInt(this.playbackStream);
      param1Parcel.writeInt(this.volume);
      param1Parcel.writeInt(this.volumeMax);
      param1Parcel.writeInt(this.volumeHandling);
      param1Parcel.writeInt(this.presentationDisplayId);
      param1Parcel.writeInt(this.deviceType);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("RouteInfo{ id=");
      stringBuilder.append(this.id);
      stringBuilder.append(", name=");
      stringBuilder.append(this.name);
      stringBuilder.append(", description=");
      stringBuilder.append(this.description);
      stringBuilder.append(", supportedTypes=0x");
      int i = this.supportedTypes;
      stringBuilder.append(Integer.toHexString(i));
      stringBuilder.append(", enabled=");
      stringBuilder.append(this.enabled);
      stringBuilder.append(", statusCode=");
      stringBuilder.append(this.statusCode);
      stringBuilder.append(", playbackType=");
      stringBuilder.append(this.playbackType);
      stringBuilder.append(", playbackStream=");
      stringBuilder.append(this.playbackStream);
      stringBuilder.append(", volume=");
      stringBuilder.append(this.volume);
      stringBuilder.append(", volumeMax=");
      stringBuilder.append(this.volumeMax);
      stringBuilder.append(", volumeHandling=");
      stringBuilder.append(this.volumeHandling);
      stringBuilder.append(", presentationDisplayId=");
      stringBuilder.append(this.presentationDisplayId);
      stringBuilder.append(", deviceType=");
      stringBuilder.append(this.deviceType);
      stringBuilder.append(" }");
      return stringBuilder.toString();
    }
    
    public static final Parcelable.Creator<RouteInfo> CREATOR = new Parcelable.Creator<RouteInfo>() {
        public MediaRouterClientState.RouteInfo createFromParcel(Parcel param2Parcel) {
          return new MediaRouterClientState.RouteInfo(param2Parcel);
        }
        
        public MediaRouterClientState.RouteInfo[] newArray(int param2Int) {
          return new MediaRouterClientState.RouteInfo[param2Int];
        }
      };
    
    public String description;
    
    public int deviceType;
    
    public boolean enabled;
    
    public String id;
    
    public String name;
    
    public int playbackStream;
    
    public int playbackType;
    
    public int presentationDisplayId;
    
    public int statusCode;
    
    public int supportedTypes;
    
    public int volume;
    
    public int volumeHandling;
    
    public int volumeMax;
  }
  
  class null implements Parcelable.Creator<RouteInfo> {
    public MediaRouterClientState.RouteInfo createFromParcel(Parcel param1Parcel) {
      return new MediaRouterClientState.RouteInfo(param1Parcel);
    }
    
    public MediaRouterClientState.RouteInfo[] newArray(int param1Int) {
      return new MediaRouterClientState.RouteInfo[param1Int];
    }
  }
}
