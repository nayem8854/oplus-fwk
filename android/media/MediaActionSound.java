package android.media;

import android.util.Log;

public class MediaActionSound {
  public static final int FOCUS_COMPLETE = 1;
  
  private static final int NUM_MEDIA_SOUND_STREAMS = 1;
  
  public static final int SHUTTER_CLICK = 0;
  
  private static final String[] SOUND_DIRS = new String[] { "/product/media/audio/ui/", "/system/media/audio/ui/" };
  
  private static final String[] SOUND_FILES = new String[] { "camera_click.ogg", "camera_focus.ogg", "VideoRecord.ogg", "VideoStop.ogg" };
  
  public static final int START_VIDEO_RECORDING = 2;
  
  private static final int STATE_LOADED = 3;
  
  private static final int STATE_LOADING = 1;
  
  private static final int STATE_LOADING_PLAY_REQUESTED = 2;
  
  private static final int STATE_NOT_LOADED = 0;
  
  public static final int STOP_VIDEO_RECORDING = 3;
  
  private static final String TAG = "MediaActionSound";
  
  private class SoundState {
    public int id;
    
    public final int name;
    
    public int state;
    
    final MediaActionSound this$0;
    
    public SoundState(int param1Int) {
      this.name = param1Int;
      this.id = 0;
      this.state = 0;
    }
  }
  
  public MediaActionSound() {
    SoundPool.Builder builder = new SoundPool.Builder();
    builder = builder.setMaxStreams(1);
    AudioAttributes.Builder builder1 = new AudioAttributes.Builder();
    builder1 = builder1.setUsage(13);
    builder1 = builder1.setFlags(1);
    builder1 = builder1.setContentType(4);
    AudioAttributes audioAttributes = builder1.build();
    builder = builder.setAudioAttributes(audioAttributes);
    SoundPool soundPool = builder.build();
    soundPool.setOnLoadCompleteListener(this.mLoadCompleteListener);
    this.mSounds = new SoundState[SOUND_FILES.length];
    byte b = 0;
    while (true) {
      SoundState[] arrayOfSoundState = this.mSounds;
      if (b < arrayOfSoundState.length) {
        arrayOfSoundState[b] = new SoundState(b);
        b++;
        continue;
      } 
      break;
    } 
  }
  
  private int loadSound(SoundState paramSoundState) {
    String str = SOUND_FILES[paramSoundState.name];
    for (String str1 : SOUND_DIRS) {
      SoundPool soundPool = this.mSoundPool;
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str1);
      stringBuilder.append(str);
      int i = soundPool.load(stringBuilder.toString(), 1);
      if (i > 0) {
        paramSoundState.state = 1;
        paramSoundState.id = i;
        return i;
      } 
    } 
    return 0;
  }
  
  public void load(int paramInt) {
    if (paramInt >= 0 && paramInt < SOUND_FILES.length)
      synchronized (this.mSounds[paramInt]) {
        if (null.state != 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("load() called in wrong state: ");
          stringBuilder1.append(null);
          stringBuilder1.append(" for sound: ");
          stringBuilder1.append(paramInt);
          Log.e("MediaActionSound", stringBuilder1.toString());
        } else if (loadSound(null) <= 0) {
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("load() error loading sound: ");
          stringBuilder1.append(paramInt);
          Log.e("MediaActionSound", stringBuilder1.toString());
        } 
        return;
      }  
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Unknown sound requested: ");
    stringBuilder.append(paramInt);
    throw new RuntimeException(stringBuilder.toString());
  }
  
  public void play(int paramInt) {
    // Byte code:
    //   0: iload_1
    //   1: iflt -> 185
    //   4: iload_1
    //   5: getstatic android/media/MediaActionSound.SOUND_FILES : [Ljava/lang/String;
    //   8: arraylength
    //   9: if_icmpge -> 185
    //   12: aload_0
    //   13: getfield mSounds : [Landroid/media/MediaActionSound$SoundState;
    //   16: iload_1
    //   17: aaload
    //   18: astore_2
    //   19: aload_2
    //   20: monitorenter
    //   21: aload_2
    //   22: getfield state : I
    //   25: istore_3
    //   26: iload_3
    //   27: ifeq -> 117
    //   30: iload_3
    //   31: iconst_1
    //   32: if_icmpeq -> 170
    //   35: iload_3
    //   36: iconst_3
    //   37: if_icmpeq -> 97
    //   40: new java/lang/StringBuilder
    //   43: astore #4
    //   45: aload #4
    //   47: invokespecial <init> : ()V
    //   50: aload #4
    //   52: ldc 'play() called in wrong state: '
    //   54: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload #4
    //   60: aload_2
    //   61: getfield state : I
    //   64: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   67: pop
    //   68: aload #4
    //   70: ldc ' for sound: '
    //   72: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   75: pop
    //   76: aload #4
    //   78: iload_1
    //   79: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   82: pop
    //   83: ldc 'MediaActionSound'
    //   85: aload #4
    //   87: invokevirtual toString : ()Ljava/lang/String;
    //   90: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   93: pop
    //   94: goto -> 175
    //   97: aload_0
    //   98: getfield mSoundPool : Landroid/media/SoundPool;
    //   101: aload_2
    //   102: getfield id : I
    //   105: fconst_1
    //   106: fconst_1
    //   107: iconst_0
    //   108: iconst_0
    //   109: fconst_1
    //   110: invokevirtual play : (IFFIIF)I
    //   113: pop
    //   114: goto -> 175
    //   117: aload_0
    //   118: aload_2
    //   119: invokespecial loadSound : (Landroid/media/MediaActionSound$SoundState;)I
    //   122: pop
    //   123: aload_0
    //   124: aload_2
    //   125: invokespecial loadSound : (Landroid/media/MediaActionSound$SoundState;)I
    //   128: ifgt -> 170
    //   131: new java/lang/StringBuilder
    //   134: astore #4
    //   136: aload #4
    //   138: invokespecial <init> : ()V
    //   141: aload #4
    //   143: ldc 'play() error loading sound: '
    //   145: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   148: pop
    //   149: aload #4
    //   151: iload_1
    //   152: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: ldc 'MediaActionSound'
    //   158: aload #4
    //   160: invokevirtual toString : ()Ljava/lang/String;
    //   163: invokestatic e : (Ljava/lang/String;Ljava/lang/String;)I
    //   166: pop
    //   167: goto -> 175
    //   170: aload_2
    //   171: iconst_2
    //   172: putfield state : I
    //   175: aload_2
    //   176: monitorexit
    //   177: return
    //   178: astore #4
    //   180: aload_2
    //   181: monitorexit
    //   182: aload #4
    //   184: athrow
    //   185: new java/lang/StringBuilder
    //   188: dup
    //   189: invokespecial <init> : ()V
    //   192: astore_2
    //   193: aload_2
    //   194: ldc 'Unknown sound requested: '
    //   196: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   199: pop
    //   200: aload_2
    //   201: iload_1
    //   202: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   205: pop
    //   206: new java/lang/RuntimeException
    //   209: dup
    //   210: aload_2
    //   211: invokevirtual toString : ()Ljava/lang/String;
    //   214: invokespecial <init> : (Ljava/lang/String;)V
    //   217: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #215	-> 0
    //   #218	-> 12
    //   #219	-> 19
    //   #220	-> 21
    //   #236	-> 40
    //   #233	-> 97
    //   #234	-> 114
    //   #222	-> 117
    //   #223	-> 123
    //   #224	-> 131
    //   #225	-> 167
    //   #230	-> 170
    //   #231	-> 175
    //   #239	-> 175
    //   #240	-> 177
    //   #239	-> 178
    //   #216	-> 185
    // Exception table:
    //   from	to	target	type
    //   21	26	178	finally
    //   40	94	178	finally
    //   97	114	178	finally
    //   117	123	178	finally
    //   123	131	178	finally
    //   131	167	178	finally
    //   170	175	178	finally
    //   175	177	178	finally
    //   180	182	178	finally
  }
  
  private SoundPool.OnLoadCompleteListener mLoadCompleteListener = (SoundPool.OnLoadCompleteListener)new Object(this);
  
  private SoundPool mSoundPool;
  
  private SoundState[] mSounds;
  
  public void release() {
    if (this.mSoundPool != null) {
      SoundState[] arrayOfSoundState;
      int i;
      byte b;
      for (arrayOfSoundState = this.mSounds, i = arrayOfSoundState.length, b = 0; b < i;) {
        synchronized (arrayOfSoundState[b]) {
          null.state = 0;
          null.id = 0;
          b++;
        } 
      } 
      this.mSoundPool.release();
      this.mSoundPool = null;
    } 
  }
}
