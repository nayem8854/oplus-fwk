package android.media;

import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.ServiceManager;
import android.util.Log;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executor;

public class AudioRecordingMonitorImpl implements AudioRecordingMonitor {
  private static final int MSG_RECORDING_CONFIG_CHANGE = 1;
  
  private static final String TAG = "android.media.AudioRecordingMonitor";
  
  private static IAudioService sService;
  
  private final AudioRecordingMonitorClient mClient;
  
  private LinkedList<AudioRecordingCallbackInfo> mRecordCallbackList;
  
  private final Object mRecordCallbackLock;
  
  private final IRecordingConfigDispatcher mRecordingCallback;
  
  private volatile Handler mRecordingCallbackHandler;
  
  private HandlerThread mRecordingCallbackHandlerThread;
  
  AudioRecordingMonitorImpl(AudioRecordingMonitorClient paramAudioRecordingMonitorClient) {
    this.mRecordCallbackLock = new Object();
    this.mRecordCallbackList = new LinkedList<>();
    this.mRecordingCallback = (IRecordingConfigDispatcher)new Object(this);
    this.mClient = paramAudioRecordingMonitorClient;
  }
  
  public void registerAudioRecordingCallback(Executor paramExecutor, AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    if (paramAudioRecordingCallback != null) {
      if (paramExecutor != null)
        synchronized (this.mRecordCallbackLock) {
          IllegalArgumentException illegalArgumentException;
          for (AudioRecordingCallbackInfo audioRecordingCallbackInfo1 : this.mRecordCallbackList) {
            if (audioRecordingCallbackInfo1.mCb != paramAudioRecordingCallback)
              continue; 
            illegalArgumentException = new IllegalArgumentException();
            this("AudioRecordingCallback already registered");
            throw illegalArgumentException;
          } 
          beginRecordingCallbackHandling();
          LinkedList<AudioRecordingCallbackInfo> linkedList = this.mRecordCallbackList;
          AudioRecordingCallbackInfo audioRecordingCallbackInfo = new AudioRecordingCallbackInfo();
          this((Executor)illegalArgumentException, paramAudioRecordingCallback);
          linkedList.add(audioRecordingCallbackInfo);
          return;
        }  
      throw new IllegalArgumentException("Illegal null Executor");
    } 
    throw new IllegalArgumentException("Illegal null AudioRecordingCallback");
  }
  
  public void unregisterAudioRecordingCallback(AudioManager.AudioRecordingCallback paramAudioRecordingCallback) {
    if (paramAudioRecordingCallback != null)
      synchronized (this.mRecordCallbackLock) {
        for (AudioRecordingCallbackInfo audioRecordingCallbackInfo : this.mRecordCallbackList) {
          if (audioRecordingCallbackInfo.mCb == paramAudioRecordingCallback) {
            this.mRecordCallbackList.remove(audioRecordingCallbackInfo);
            if (this.mRecordCallbackList.size() == 0)
              endRecordingCallbackHandling(); 
            return;
          } 
        } 
        IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
        this("AudioRecordingCallback was not registered");
        throw illegalArgumentException;
      }  
    throw new IllegalArgumentException("Illegal null AudioRecordingCallback argument");
  }
  
  public AudioRecordingConfiguration getActiveRecordingConfiguration() {
    IAudioService iAudioService = getService();
    try {
      List<AudioRecordingConfiguration> list = iAudioService.getActiveRecordingConfigurations();
      return getMyConfig(list);
    } catch (RemoteException remoteException) {
      throw remoteException.rethrowFromSystemServer();
    } 
  }
  
  class AudioRecordingCallbackInfo {
    final AudioManager.AudioRecordingCallback mCb;
    
    final Executor mExecutor;
    
    AudioRecordingCallbackInfo(AudioRecordingMonitorImpl this$0, AudioManager.AudioRecordingCallback param1AudioRecordingCallback) {
      this.mExecutor = (Executor)this$0;
      this.mCb = param1AudioRecordingCallback;
    }
  }
  
  private void beginRecordingCallbackHandling() {
    if (this.mRecordingCallbackHandlerThread == null) {
      HandlerThread handlerThread = new HandlerThread("android.media.AudioRecordingMonitor.RecordingCallback");
      handlerThread.start();
      Looper looper = this.mRecordingCallbackHandlerThread.getLooper();
      if (looper != null) {
        this.mRecordingCallbackHandler = new Handler(looper) {
            final AudioRecordingMonitorImpl this$0;
            
            public void handleMessage(Message param1Message) {
              if (param1Message.what != 1) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Unknown event ");
                stringBuilder.append(param1Message.what);
                Log.e("android.media.AudioRecordingMonitor", stringBuilder.toString());
              } else {
                if (param1Message.obj == null)
                  return; 
                null = new ArrayList();
                null.add((AudioRecordingConfiguration)param1Message.obj);
                synchronized (AudioRecordingMonitorImpl.this.mRecordCallbackLock) {
                  if (AudioRecordingMonitorImpl.this.mRecordCallbackList.size() == 0)
                    return; 
                  LinkedList linkedList = new LinkedList();
                  AudioRecordingMonitorImpl audioRecordingMonitorImpl = AudioRecordingMonitorImpl.this;
                  this((Collection)audioRecordingMonitorImpl.mRecordCallbackList);
                  long l = Binder.clearCallingIdentity();
                  try {
                    for (AudioRecordingMonitorImpl.AudioRecordingCallbackInfo audioRecordingCallbackInfo : linkedList) {
                      null = audioRecordingCallbackInfo.mExecutor;
                      _$$Lambda$AudioRecordingMonitorImpl$2$cn04v8rie0OYr__fiLO_SMYka7I _$$Lambda$AudioRecordingMonitorImpl$2$cn04v8rie0OYr__fiLO_SMYka7I = new _$$Lambda$AudioRecordingMonitorImpl$2$cn04v8rie0OYr__fiLO_SMYka7I();
                      this(audioRecordingCallbackInfo, null);
                      null.execute(_$$Lambda$AudioRecordingMonitorImpl$2$cn04v8rie0OYr__fiLO_SMYka7I);
                    } 
                    return;
                  } finally {
                    Binder.restoreCallingIdentity(l);
                  } 
                } 
              } 
            }
          };
        IAudioService iAudioService = getService();
        try {
          iAudioService.registerRecordingCallback(this.mRecordingCallback);
        } catch (RemoteException remoteException) {
          throw remoteException.rethrowFromSystemServer();
        } 
      } 
    } 
  }
  
  private void endRecordingCallbackHandling() {
    if (this.mRecordingCallbackHandlerThread != null) {
      IAudioService iAudioService = getService();
      try {
        iAudioService.unregisterRecordingCallback(this.mRecordingCallback);
        this.mRecordingCallbackHandlerThread.quit();
        this.mRecordingCallbackHandlerThread = null;
      } catch (RemoteException remoteException) {
        throw remoteException.rethrowFromSystemServer();
      } 
    } 
  }
  
  AudioRecordingConfiguration getMyConfig(List<AudioRecordingConfiguration> paramList) {
    int i = this.mClient.getPortId();
    for (AudioRecordingConfiguration audioRecordingConfiguration : paramList) {
      if (audioRecordingConfiguration.getClientPortId() == i)
        return audioRecordingConfiguration; 
    } 
    return null;
  }
  
  private static IAudioService getService() {
    IAudioService iAudioService2 = sService;
    if (iAudioService2 != null)
      return iAudioService2; 
    IBinder iBinder = ServiceManager.getService("audio");
    IAudioService iAudioService1 = IAudioService.Stub.asInterface(iBinder);
    return iAudioService1;
  }
}
