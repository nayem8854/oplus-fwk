package android.media;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.accessibility.CaptioningManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.Vector;

class TtmlRenderingWidget extends LinearLayout implements SubtitleTrack.RenderingWidget {
  private SubtitleTrack.RenderingWidget.OnChangedListener mListener;
  
  private final TextView mTextView;
  
  public TtmlRenderingWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public TtmlRenderingWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public TtmlRenderingWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public TtmlRenderingWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    setLayerType(1, null);
    CaptioningManager captioningManager = (CaptioningManager)paramContext.getSystemService("captioning");
    TextView textView = new TextView(paramContext);
    textView.setTextColor((captioningManager.getUserStyle()).foregroundColor);
    addView((View)this.mTextView, -1, -1);
    this.mTextView.setGravity(81);
  }
  
  public void setOnChangedListener(SubtitleTrack.RenderingWidget.OnChangedListener paramOnChangedListener) {
    this.mListener = paramOnChangedListener;
  }
  
  public void setSize(int paramInt1, int paramInt2) {
    int i = View.MeasureSpec.makeMeasureSpec(paramInt1, 1073741824);
    int j = View.MeasureSpec.makeMeasureSpec(paramInt2, 1073741824);
    measure(i, j);
    layout(0, 0, paramInt1, paramInt2);
  }
  
  public void setVisible(boolean paramBoolean) {
    if (paramBoolean) {
      setVisibility(0);
    } else {
      setVisibility(8);
    } 
  }
  
  public void onAttachedToWindow() {
    super.onAttachedToWindow();
  }
  
  public void onDetachedFromWindow() {
    super.onDetachedFromWindow();
  }
  
  public void setActiveCues(Vector<SubtitleTrack.Cue> paramVector) {
    int i = paramVector.size();
    String str = "";
    for (byte b = 0; b < i; b++) {
      TtmlCue ttmlCue = (TtmlCue)paramVector.get(b);
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(str);
      stringBuilder.append(ttmlCue.mText);
      stringBuilder.append("\n");
      str = stringBuilder.toString();
    } 
    this.mTextView.setText(str);
    SubtitleTrack.RenderingWidget.OnChangedListener onChangedListener = this.mListener;
    if (onChangedListener != null)
      onChangedListener.onChanged(this); 
  }
}
