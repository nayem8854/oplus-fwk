package android.media;

import android.graphics.Rect;
import android.hardware.HardwareBuffer;
import android.os.Bundle;
import android.os.Handler;
import android.os.IHwBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PersistableBundle;
import android.view.Surface;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.nio.NioUtils;
import java.nio.ReadOnlyBufferException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class MediaCodec {
  class BufferInfo {
    public int flags;
    
    public int offset;
    
    public long presentationTimeUs;
    
    public int size;
    
    public void set(int param1Int1, int param1Int2, long param1Long, int param1Int3) {
      this.offset = param1Int1;
      this.size = param1Int2;
      this.presentationTimeUs = param1Long;
      this.flags = param1Int3;
    }
    
    public BufferInfo dup() {
      BufferInfo bufferInfo = new BufferInfo();
      bufferInfo.set(this.offset, this.size, this.presentationTimeUs, this.flags);
      return bufferInfo;
    }
  }
  
  private final Object mListenerLock = new Object();
  
  private final Object mCodecInfoLock = new Object();
  
  class EventHandler extends Handler {
    private MediaCodec mCodec;
    
    final MediaCodec this$0;
    
    public EventHandler(MediaCodec param1MediaCodec1, Looper param1Looper) {
      super(param1Looper);
      this.mCodec = param1MediaCodec1;
    }
    
    public void handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2) {
          if (i == 3) {
            Map map = (Map)param1Message.obj;
            i = 0;
            while (true) {
              StringBuilder stringBuilder = new StringBuilder();
              stringBuilder.append(i);
              stringBuilder.append("-media-time-us");
              Object object1 = map.get(stringBuilder.toString());
              stringBuilder = new StringBuilder();
              stringBuilder.append(i);
              stringBuilder.append("-system-nano");
              Object object2 = map.get(stringBuilder.toString());
              synchronized (MediaCodec.this.mListenerLock) {
                MediaCodec.OnFrameRenderedListener onFrameRenderedListener = MediaCodec.this.mOnFrameRenderedListener;
                if (object1 == null || object2 == null || onFrameRenderedListener == null)
                  break; 
                null = this.mCodec;
                object1 = object1;
                long l1 = object1.longValue(), l2 = ((Long)object2).longValue();
                onFrameRenderedListener.onFrameRendered((MediaCodec)null, l1, l2);
                i++;
              } 
            } 
          } 
        } else {
          MediaCodec.access$002(MediaCodec.this, (MediaCodec.Callback)param1Message.obj);
        } 
      } else {
        handleCallback(param1Message);
      } 
    }
    
    private void handleCallback(Message param1Message) {
      if (MediaCodec.this.mCallback == null)
        return; 
      int i = param1Message.arg1;
      if (i != 1) {
        if (i != 2) {
          if (i != 3) {
            if (i == 4)
              MediaCodec.this.mCallback.onOutputFormatChanged(this.mCodec, new MediaFormat((Map<String, Object>)param1Message.obj)); 
          } else {
            MediaCodec.this.mCallback.onError(this.mCodec, (MediaCodec.CodecException)param1Message.obj);
          } 
        } else {
          i = param1Message.arg2;
          MediaCodec.BufferInfo bufferInfo = (MediaCodec.BufferInfo)param1Message.obj;
          synchronized (MediaCodec.this.mBufferLock) {
            MediaCodec mediaCodec;
            int j = MediaCodec.this.mBufferMode;
            if (j != 0) {
              if (j == 1) {
                while (MediaCodec.this.mOutputFrames.size() <= i)
                  MediaCodec.this.mOutputFrames.add(null); 
                MediaCodec.OutputFrame outputFrame2 = MediaCodec.this.mOutputFrames.get(i);
                MediaCodec.OutputFrame outputFrame1 = outputFrame2;
                if (outputFrame2 == null) {
                  outputFrame1 = new MediaCodec.OutputFrame();
                  this(i);
                  MediaCodec.this.mOutputFrames.set(i, outputFrame1);
                } 
                outputFrame1.setBufferInfo(bufferInfo);
                outputFrame1.setAccessible(true);
              } else {
                IllegalStateException illegalStateException = new IllegalStateException();
                StringBuilder stringBuilder = new StringBuilder();
                this();
                stringBuilder.append("Unrecognized buffer mode: ");
                mediaCodec = MediaCodec.this;
                stringBuilder.append(mediaCodec.mBufferMode);
                this(stringBuilder.toString());
                throw illegalStateException;
              } 
            } else {
              MediaCodec.this.validateOutputByteBuffer(MediaCodec.this.mCachedOutputBuffers, i, (MediaCodec.BufferInfo)mediaCodec);
            } 
            MediaCodec.this.mCallback.onOutputBufferAvailable(this.mCodec, i, (MediaCodec.BufferInfo)mediaCodec);
          } 
        } 
      } else {
        i = param1Message.arg2;
        synchronized (MediaCodec.this.mBufferLock) {
          int j = MediaCodec.this.mBufferMode;
          if (j != 0) {
            if (j == 1) {
              while (MediaCodec.this.mQueueRequests.size() <= i)
                MediaCodec.this.mQueueRequests.add(null); 
              MediaCodec.QueueRequest queueRequest2 = MediaCodec.this.mQueueRequests.get(i);
              MediaCodec.QueueRequest queueRequest1 = queueRequest2;
              if (queueRequest2 == null) {
                queueRequest1 = new MediaCodec.QueueRequest();
                this(this.mCodec, i);
                MediaCodec.this.mQueueRequests.set(i, queueRequest1);
              } 
              queueRequest1.setAccessible(true);
            } else {
              IllegalStateException illegalStateException = new IllegalStateException();
              StringBuilder stringBuilder = new StringBuilder();
              this();
              stringBuilder.append("Unrecognized buffer mode: ");
              MediaCodec mediaCodec = MediaCodec.this;
              stringBuilder.append(mediaCodec.mBufferMode);
              this(stringBuilder.toString());
              throw illegalStateException;
            } 
          } else {
            MediaCodec.this.validateInputByteBuffer(MediaCodec.this.mCachedInputBuffers, i);
          } 
          MediaCodec.this.mCallback.onInputBufferAvailable(this.mCodec, i);
          return;
        } 
      } 
    }
  }
  
  private boolean mHasSurface = false;
  
  public static final int BUFFER_FLAG_CODEC_CONFIG = 2;
  
  public static final int BUFFER_FLAG_END_OF_STREAM = 4;
  
  public static final int BUFFER_FLAG_KEY_FRAME = 1;
  
  public static final int BUFFER_FLAG_MUXER_DATA = 16;
  
  public static final int BUFFER_FLAG_PARTIAL_FRAME = 8;
  
  public static final int BUFFER_FLAG_SYNC_FRAME = 1;
  
  private static final int BUFFER_MODE_BLOCK = 1;
  
  private static final int BUFFER_MODE_INVALID = -1;
  
  private static final int BUFFER_MODE_LEGACY = 0;
  
  private static final int CB_ERROR = 3;
  
  private static final int CB_INPUT_AVAILABLE = 1;
  
  private static final int CB_OUTPUT_AVAILABLE = 2;
  
  private static final int CB_OUTPUT_FORMAT_CHANGE = 4;
  
  public static final int CONFIGURE_FLAG_ENCODE = 1;
  
  public static final int CONFIGURE_FLAG_USE_BLOCK_MODEL = 2;
  
  public static final int CRYPTO_MODE_AES_CBC = 2;
  
  public static final int CRYPTO_MODE_AES_CTR = 1;
  
  public static final int CRYPTO_MODE_UNENCRYPTED = 0;
  
  private static final int EVENT_CALLBACK = 1;
  
  private static final int EVENT_FRAME_RENDERED = 3;
  
  private static final int EVENT_SET_CALLBACK = 2;
  
  public static final int INFO_OUTPUT_BUFFERS_CHANGED = -3;
  
  public static final int INFO_OUTPUT_FORMAT_CHANGED = -2;
  
  public static final int INFO_TRY_AGAIN_LATER = -1;
  
  public static final String PARAMETER_KEY_HDR10_PLUS_INFO = "hdr10-plus-info";
  
  public static final String PARAMETER_KEY_LOW_LATENCY = "low-latency";
  
  public static final String PARAMETER_KEY_OFFSET_TIME = "time-offset-us";
  
  public static final String PARAMETER_KEY_REQUEST_SYNC_FRAME = "request-sync";
  
  public static final String PARAMETER_KEY_SUSPEND = "drop-input-frames";
  
  public static final String PARAMETER_KEY_SUSPEND_TIME = "drop-start-time-us";
  
  public static final String PARAMETER_KEY_VIDEO_BITRATE = "video-bitrate";
  
  public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT = 1;
  
  public static final int VIDEO_SCALING_MODE_SCALE_TO_FIT_WITH_CROPPING = 2;
  
  private final Object mBufferLock;
  
  private int mBufferMode;
  
  private ByteBuffer[] mCachedInputBuffers;
  
  private ByteBuffer[] mCachedOutputBuffers;
  
  private Callback mCallback;
  
  private EventHandler mCallbackHandler;
  
  private MediaCodecInfo mCodecInfo;
  
  private MediaCrypto mCrypto;
  
  private final BufferMap mDequeuedInputBuffers;
  
  private final BufferMap mDequeuedOutputBuffers;
  
  private final Map<Integer, BufferInfo> mDequeuedOutputInfos;
  
  private EventHandler mEventHandler;
  
  private String mNameAtCreation;
  
  private long mNativeContext;
  
  private final Lock mNativeContextLock;
  
  private EventHandler mOnFrameRenderedHandler;
  
  private OnFrameRenderedListener mOnFrameRenderedListener;
  
  private final ArrayList<OutputFrame> mOutputFrames;
  
  private final ArrayList<QueueRequest> mQueueRequests;
  
  public static MediaCodec createDecoderByType(String paramString) throws IOException {
    return new MediaCodec(paramString, true, false);
  }
  
  public static MediaCodec createEncoderByType(String paramString) throws IOException {
    return new MediaCodec(paramString, true, true);
  }
  
  public static MediaCodec createByCodecName(String paramString) throws IOException {
    return new MediaCodec(paramString, false, false);
  }
  
  protected void finalize() {
    native_finalize();
    this.mCrypto = null;
  }
  
  public final void reset() {
    freeAllTrackedBuffers();
    native_reset();
    this.mCrypto = null;
  }
  
  public final void release() {
    freeAllTrackedBuffers();
    native_release();
    this.mCrypto = null;
  }
  
  public class IncompatibleWithBlockModelException extends RuntimeException {
    final MediaCodec this$0;
    
    IncompatibleWithBlockModelException() {}
    
    IncompatibleWithBlockModelException(String param1String) {
      super(param1String);
    }
    
    IncompatibleWithBlockModelException(String param1String, Throwable param1Throwable) {
      super(param1String, param1Throwable);
    }
    
    IncompatibleWithBlockModelException(Throwable param1Throwable) {
      super(param1Throwable);
    }
  }
  
  public void configure(MediaFormat paramMediaFormat, Surface paramSurface, MediaCrypto paramMediaCrypto, int paramInt) {
    configure(paramMediaFormat, paramSurface, paramMediaCrypto, null, paramInt);
  }
  
  public void configure(MediaFormat paramMediaFormat, Surface paramSurface, int paramInt, MediaDescrambler paramMediaDescrambler) {
    if (paramMediaDescrambler != null) {
      IHwBinder iHwBinder = paramMediaDescrambler.getBinder();
    } else {
      paramMediaDescrambler = null;
    } 
    configure(paramMediaFormat, paramSurface, null, (IHwBinder)paramMediaDescrambler, paramInt);
  }
  
  private MediaCodec(String paramString, boolean paramBoolean1, boolean paramBoolean2) {
    this.mBufferMode = -1;
    this.mQueueRequests = new ArrayList<>();
    String str = null;
    this.mDequeuedInputBuffers = new BufferMap();
    this.mDequeuedOutputBuffers = new BufferMap();
    this.mDequeuedOutputInfos = new HashMap<>();
    this.mOutputFrames = new ArrayList<>();
    this.mNativeContext = 0L;
    this.mNativeContextLock = new ReentrantLock();
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler = new EventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler = new EventHandler(this, looper);
      } else {
        this.mEventHandler = null;
      } 
    } 
    EventHandler eventHandler = this.mEventHandler;
    this.mOnFrameRenderedHandler = eventHandler;
    this.mBufferLock = new Object();
    if (!paramBoolean1)
      str = paramString; 
    this.mNameAtCreation = str;
    native_setup(paramString, paramBoolean1, paramBoolean2);
  }
  
  private void configure(MediaFormat paramMediaFormat, Surface paramSurface, MediaCrypto paramMediaCrypto, IHwBinder paramIHwBinder, int paramInt) {
    // Byte code:
    //   0: aload_3
    //   1: ifnull -> 23
    //   4: aload #4
    //   6: ifnonnull -> 12
    //   9: goto -> 23
    //   12: new java/lang/IllegalArgumentException
    //   15: dup
    //   16: ldc_w 'Can't use crypto and descrambler together!'
    //   19: invokespecial <init> : (Ljava/lang/String;)V
    //   22: athrow
    //   23: aload_1
    //   24: ifnull -> 199
    //   27: aload_1
    //   28: invokevirtual getMap : ()Ljava/util/Map;
    //   31: astore #6
    //   33: aload #6
    //   35: invokeinterface size : ()I
    //   40: anewarray java/lang/String
    //   43: astore #7
    //   45: aload #6
    //   47: invokeinterface size : ()I
    //   52: anewarray java/lang/Object
    //   55: astore_1
    //   56: aload #6
    //   58: invokeinterface entrySet : ()Ljava/util/Set;
    //   63: invokeinterface iterator : ()Ljava/util/Iterator;
    //   68: astore #8
    //   70: iconst_0
    //   71: istore #9
    //   73: aload #8
    //   75: invokeinterface hasNext : ()Z
    //   80: ifeq -> 196
    //   83: aload #8
    //   85: invokeinterface next : ()Ljava/lang/Object;
    //   90: checkcast java/util/Map$Entry
    //   93: astore #6
    //   95: aload #6
    //   97: invokeinterface getKey : ()Ljava/lang/Object;
    //   102: checkcast java/lang/String
    //   105: ldc_w 'audio-session-id'
    //   108: invokevirtual equals : (Ljava/lang/Object;)Z
    //   111: ifeq -> 164
    //   114: aload #6
    //   116: invokeinterface getValue : ()Ljava/lang/Object;
    //   121: checkcast java/lang/Integer
    //   124: invokevirtual intValue : ()I
    //   127: istore #10
    //   129: aload #7
    //   131: iload #9
    //   133: ldc_w 'audio-hw-sync'
    //   136: aastore
    //   137: aload_1
    //   138: iload #9
    //   140: iload #10
    //   142: invokestatic getAudioHwSyncForSession : (I)I
    //   145: invokestatic valueOf : (I)Ljava/lang/Integer;
    //   148: aastore
    //   149: goto -> 190
    //   152: astore_1
    //   153: new java/lang/IllegalArgumentException
    //   156: dup
    //   157: ldc_w 'Wrong Session ID Parameter!'
    //   160: invokespecial <init> : (Ljava/lang/String;)V
    //   163: athrow
    //   164: aload #7
    //   166: iload #9
    //   168: aload #6
    //   170: invokeinterface getKey : ()Ljava/lang/Object;
    //   175: checkcast java/lang/String
    //   178: aastore
    //   179: aload_1
    //   180: iload #9
    //   182: aload #6
    //   184: invokeinterface getValue : ()Ljava/lang/Object;
    //   189: aastore
    //   190: iinc #9, 1
    //   193: goto -> 73
    //   196: goto -> 204
    //   199: aconst_null
    //   200: astore #7
    //   202: aconst_null
    //   203: astore_1
    //   204: aload_2
    //   205: ifnull -> 214
    //   208: iconst_1
    //   209: istore #11
    //   211: goto -> 217
    //   214: iconst_0
    //   215: istore #11
    //   217: aload_0
    //   218: iload #11
    //   220: putfield mHasSurface : Z
    //   223: aload_0
    //   224: aload_3
    //   225: putfield mCrypto : Landroid/media/MediaCrypto;
    //   228: aload_0
    //   229: getfield mBufferLock : Ljava/lang/Object;
    //   232: astore #6
    //   234: aload #6
    //   236: monitorenter
    //   237: iload #5
    //   239: iconst_2
    //   240: iand
    //   241: ifeq -> 252
    //   244: aload_0
    //   245: iconst_1
    //   246: putfield mBufferMode : I
    //   249: goto -> 257
    //   252: aload_0
    //   253: iconst_0
    //   254: putfield mBufferMode : I
    //   257: aload #6
    //   259: monitorexit
    //   260: aload_0
    //   261: aload #7
    //   263: aload_1
    //   264: aload_2
    //   265: aload_3
    //   266: aload #4
    //   268: iload #5
    //   270: invokespecial native_configure : ([Ljava/lang/String;[Ljava/lang/Object;Landroid/view/Surface;Landroid/media/MediaCrypto;Landroid/os/IHwBinder;I)V
    //   273: return
    //   274: astore_1
    //   275: aload #6
    //   277: monitorexit
    //   278: aload_1
    //   279: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #2084	-> 0
    //   #2085	-> 12
    //   #2088	-> 23
    //   #2089	-> 23
    //   #2091	-> 23
    //   #2092	-> 27
    //   #2093	-> 33
    //   #2094	-> 45
    //   #2096	-> 56
    //   #2097	-> 56
    //   #2098	-> 95
    //   #2099	-> 114
    //   #2101	-> 114
    //   #2105	-> 129
    //   #2106	-> 129
    //   #2107	-> 137
    //   #2108	-> 149
    //   #2103	-> 152
    //   #2104	-> 153
    //   #2109	-> 164
    //   #2110	-> 179
    //   #2112	-> 190
    //   #2113	-> 193
    //   #2097	-> 196
    //   #2091	-> 199
    //   #2116	-> 204
    //   #2117	-> 223
    //   #2118	-> 228
    //   #2119	-> 237
    //   #2120	-> 244
    //   #2122	-> 252
    //   #2124	-> 257
    //   #2126	-> 260
    //   #2127	-> 273
    //   #2124	-> 274
    // Exception table:
    //   from	to	target	type
    //   114	129	152	java/lang/Exception
    //   244	249	274	finally
    //   252	257	274	finally
    //   257	260	274	finally
    //   275	278	274	finally
  }
  
  public void setOutputSurface(Surface paramSurface) {
    if (this.mHasSurface) {
      native_setSurface(paramSurface);
      return;
    } 
    throw new IllegalStateException("codec was not configured for an output surface");
  }
  
  public static Surface createPersistentInputSurface() {
    return native_createPersistentInputSurface();
  }
  
  class PersistentSurface extends Surface {
    private long mPersistentObject;
    
    public void release() {
      MediaCodec.native_releasePersistentInputSurface(this);
      super.release();
    }
  }
  
  public void setInputSurface(Surface paramSurface) {
    if (paramSurface instanceof PersistentSurface) {
      native_setInputSurface(paramSurface);
      return;
    } 
    throw new IllegalArgumentException("not a PersistentSurface");
  }
  
  public final void start() {
    native_start();
    synchronized (this.mBufferLock) {
      cacheBuffers(true);
      cacheBuffers(false);
      return;
    } 
  }
  
  public final void stop() {
    native_stop();
    freeAllTrackedBuffers();
    synchronized (this.mListenerLock) {
      if (this.mCallbackHandler != null) {
        this.mCallbackHandler.removeMessages(2);
        this.mCallbackHandler.removeMessages(1);
      } 
      if (this.mOnFrameRenderedHandler != null)
        this.mOnFrameRenderedHandler.removeMessages(3); 
      return;
    } 
  }
  
  public final void flush() {
    synchronized (this.mBufferLock) {
      invalidateByteBuffers(this.mCachedInputBuffers);
      invalidateByteBuffers(this.mCachedOutputBuffers);
      this.mDequeuedInputBuffers.clear();
      this.mDequeuedOutputBuffers.clear();
      native_flush();
      return;
    } 
  }
  
  class CodecException extends IllegalStateException {
    private static final int ACTION_RECOVERABLE = 2;
    
    private static final int ACTION_TRANSIENT = 1;
    
    public static final int ERROR_INSUFFICIENT_RESOURCE = 1100;
    
    public static final int ERROR_RECLAIMED = 1101;
    
    private final int mActionCode;
    
    private final String mDiagnosticInfo;
    
    private final int mErrorCode;
    
    CodecException(MediaCodec this$0, int param1Int1, String param1String) {
      super(param1String);
      this.mErrorCode = this$0;
      this.mActionCode = param1Int1;
      if (this$0 < null) {
        param1String = "neg_";
      } else {
        param1String = "";
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("android.media.MediaCodec.error_");
      stringBuilder.append(param1String);
      stringBuilder.append(Math.abs(this$0));
      this.mDiagnosticInfo = stringBuilder.toString();
    }
    
    public boolean isTransient() {
      int i = this.mActionCode;
      boolean bool = true;
      if (i != 1)
        bool = false; 
      return bool;
    }
    
    public boolean isRecoverable() {
      boolean bool;
      if (this.mActionCode == 2) {
        bool = true;
      } else {
        bool = false;
      } 
      return bool;
    }
    
    public int getErrorCode() {
      return this.mErrorCode;
    }
    
    public String getDiagnosticInfo() {
      return this.mDiagnosticInfo;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface ReasonCode {}
  }
  
  public static final class CryptoException extends RuntimeException {
    public static final int ERROR_FRAME_TOO_LARGE = 8;
    
    public static final int ERROR_INSUFFICIENT_OUTPUT_PROTECTION = 4;
    
    public static final int ERROR_INSUFFICIENT_SECURITY = 7;
    
    public static final int ERROR_KEY_EXPIRED = 2;
    
    public static final int ERROR_LOST_STATE = 9;
    
    public static final int ERROR_NO_KEY = 1;
    
    public static final int ERROR_RESOURCE_BUSY = 3;
    
    public static final int ERROR_SESSION_NOT_OPENED = 5;
    
    public static final int ERROR_UNSUPPORTED_OPERATION = 6;
    
    private int mErrorCode;
    
    public CryptoException(int param1Int, String param1String) {
      super(param1String);
      this.mErrorCode = param1Int;
    }
    
    public int getErrorCode() {
      return this.mErrorCode;
    }
    
    @Retention(RetentionPolicy.SOURCE)
    public static @interface CryptoErrorCode {}
  }
  
  public final void queueInputBuffer(int paramInt1, int paramInt2, int paramInt3, long paramLong, int paramInt4) throws CryptoException {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        invalidateByteBuffer(this.mCachedInputBuffers, paramInt1);
        this.mDequeuedInputBuffers.remove(paramInt1);
        try {
          native_queueInputBuffer(paramInt1, paramInt2, paramInt3, paramLong, paramInt4);
          return;
        } catch (CryptoException|IllegalStateException null) {
          revalidateByteBuffer(this.mCachedInputBuffers, paramInt1);
          throw null;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "queueInputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use getQueueRequest() to queue buffers");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public static final class CryptoInfo {
    public byte[] iv;
    
    public byte[] key;
    
    public int mode;
    
    public int[] numBytesOfClearData;
    
    public int[] numBytesOfEncryptedData;
    
    public int numSubSamples;
    
    private Pattern pattern;
    
    public static final class Pattern {
      private int mEncryptBlocks;
      
      private int mSkipBlocks;
      
      public Pattern(int param2Int1, int param2Int2) {
        set(param2Int1, param2Int2);
      }
      
      public void set(int param2Int1, int param2Int2) {
        this.mEncryptBlocks = param2Int1;
        this.mSkipBlocks = param2Int2;
      }
      
      public int getSkipBlocks() {
        return this.mSkipBlocks;
      }
      
      public int getEncryptBlocks() {
        return this.mEncryptBlocks;
      }
    }
    
    private final Pattern zeroPattern = new Pattern(0, 0);
    
    public void set(int param1Int1, int[] param1ArrayOfint1, int[] param1ArrayOfint2, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2, int param1Int2) {
      this.numSubSamples = param1Int1;
      this.numBytesOfClearData = param1ArrayOfint1;
      this.numBytesOfEncryptedData = param1ArrayOfint2;
      this.key = param1ArrayOfbyte1;
      this.iv = param1ArrayOfbyte2;
      this.mode = param1Int2;
      this.pattern = this.zeroPattern;
    }
    
    public void setPattern(Pattern param1Pattern) {
      Pattern pattern = param1Pattern;
      if (param1Pattern == null)
        pattern = this.zeroPattern; 
      this.pattern = pattern;
    }
    
    private void setPattern(int param1Int1, int param1Int2) {
      this.pattern = new Pattern(param1Int1, param1Int2);
    }
    
    public String toString() {
      StringBuilder stringBuilder1 = new StringBuilder();
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append(this.numSubSamples);
      stringBuilder2.append(" subsamples, key [");
      stringBuilder1.append(stringBuilder2.toString());
      byte b = 0;
      while (true) {
        byte[] arrayOfByte = this.key;
        if (b < arrayOfByte.length) {
          stringBuilder1.append("0123456789abcdef".charAt((arrayOfByte[b] & 0xF0) >> 4));
          stringBuilder1.append("0123456789abcdef".charAt(this.key[b] & 0xF));
          b++;
          continue;
        } 
        break;
      } 
      stringBuilder1.append("], iv [");
      b = 0;
      while (true) {
        byte[] arrayOfByte = this.iv;
        if (b < arrayOfByte.length) {
          stringBuilder1.append("0123456789abcdef".charAt((arrayOfByte[b] & 0xF0) >> 4));
          stringBuilder1.append("0123456789abcdef".charAt(this.iv[b] & 0xF));
          b++;
          continue;
        } 
        break;
      } 
      stringBuilder1.append("], clear ");
      stringBuilder1.append(Arrays.toString(this.numBytesOfClearData));
      stringBuilder1.append(", encrypted ");
      stringBuilder1.append(Arrays.toString(this.numBytesOfEncryptedData));
      stringBuilder1.append(", pattern (encrypt: ");
      stringBuilder1.append(this.pattern.mEncryptBlocks);
      stringBuilder1.append(", skip: ");
      stringBuilder1.append(this.pattern.mSkipBlocks);
      stringBuilder1.append(")");
      return stringBuilder1.toString();
    }
  }
  
  public static final class Pattern {
    private int mEncryptBlocks;
    
    private int mSkipBlocks;
    
    public Pattern(int param1Int1, int param1Int2) {
      set(param1Int1, param1Int2);
    }
    
    public void set(int param1Int1, int param1Int2) {
      this.mEncryptBlocks = param1Int1;
      this.mSkipBlocks = param1Int2;
    }
    
    public int getSkipBlocks() {
      return this.mSkipBlocks;
    }
    
    public int getEncryptBlocks() {
      return this.mEncryptBlocks;
    }
  }
  
  public final void queueSecureInputBuffer(int paramInt1, int paramInt2, CryptoInfo paramCryptoInfo, long paramLong, int paramInt3) throws CryptoException {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        invalidateByteBuffer(this.mCachedInputBuffers, paramInt1);
        this.mDequeuedInputBuffers.remove(paramInt1);
        try {
          native_queueSecureInputBuffer(paramInt1, paramInt2, paramCryptoInfo, paramLong, paramInt3);
          return;
        } catch (CryptoException|IllegalStateException cryptoException) {
          revalidateByteBuffer(this.mCachedInputBuffers, paramInt1);
          throw cryptoException;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "queueSecureInputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use getQueueRequest() to queue buffers");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public final int dequeueInputBuffer(long paramLong) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        int i = native_dequeueInputBuffer(paramLong);
        if (i >= 0)
          synchronized (this.mBufferLock) {
            validateInputByteBuffer(this.mCachedInputBuffers, i);
          }  
        return i;
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "dequeueInputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use MediaCodec.Callback objectes to get input buffer slots.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public static final class LinearBlock {
    private LinearBlock() {
      this.mLock = new Object();
      this.mValid = false;
      this.mMappable = false;
      this.mMapped = null;
      this.mNativeContext = 0L;
    }
    
    public boolean isMappable() {
      synchronized (this.mLock) {
        if (this.mValid)
          return this.mMappable; 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("The linear block is invalid");
        throw illegalStateException;
      } 
    }
    
    public ByteBuffer map() {
      synchronized (this.mLock) {
        if (this.mValid) {
          if (this.mMappable) {
            if (this.mMapped == null)
              this.mMapped = native_map(); 
            return this.mMapped;
          } 
          IllegalStateException illegalStateException1 = new IllegalStateException();
          this("The linear block is not mappable");
          throw illegalStateException1;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("The linear block is invalid");
        throw illegalStateException;
      } 
    }
    
    public void recycle() {
      synchronized (this.mLock) {
        if (this.mValid) {
          if (this.mMapped != null) {
            this.mMapped.setAccessible(false);
            this.mMapped = null;
          } 
          native_recycle();
          this.mValid = false;
          this.mNativeContext = 0L;
          sPool.offer(this);
          return;
        } 
        IllegalStateException illegalStateException = new IllegalStateException();
        this("The linear block is invalid");
        throw illegalStateException;
      } 
    }
    
    protected void finalize() {
      native_recycle();
    }
    
    public static boolean isCodecCopyFreeCompatible(String[] param1ArrayOfString) {
      return native_checkCompatible(param1ArrayOfString);
    }
    
    public static LinearBlock obtain(int param1Int, String[] param1ArrayOfString) {
      LinearBlock linearBlock1 = sPool.poll();
      LinearBlock linearBlock2 = linearBlock1;
      if (linearBlock1 == null)
        linearBlock2 = new LinearBlock(); 
      synchronized (linearBlock2.mLock) {
        linearBlock2.native_obtain(param1Int, param1ArrayOfString);
        return linearBlock2;
      } 
    }
    
    private void setInternalStateLocked(long param1Long, boolean param1Boolean) {
      this.mNativeContext = param1Long;
      this.mMappable = param1Boolean;
      if (param1Long != 0L) {
        param1Boolean = true;
      } else {
        param1Boolean = false;
      } 
      this.mValid = param1Boolean;
    }
    
    private static final BlockingQueue<LinearBlock> sPool = new LinkedBlockingQueue<>();
    
    private final Object mLock;
    
    private boolean mMappable;
    
    private ByteBuffer mMapped;
    
    private long mNativeContext;
    
    private boolean mValid;
    
    private static native boolean native_checkCompatible(String[] param1ArrayOfString);
    
    private native ByteBuffer native_map();
    
    private native void native_obtain(int param1Int, String[] param1ArrayOfString);
    
    private native void native_recycle();
  }
  
  public static Image mapHardwareBuffer(HardwareBuffer paramHardwareBuffer) {
    return native_mapHardwareBuffer(paramHardwareBuffer);
  }
  
  public final class QueueRequest {
    private boolean mAccessible;
    
    private final MediaCodec mCodec;
    
    private MediaCodec.CryptoInfo mCryptoInfo;
    
    private int mFlags;
    
    private HardwareBuffer mHardwareBuffer;
    
    private final int mIndex;
    
    private MediaCodec.LinearBlock mLinearBlock;
    
    private int mOffset;
    
    private long mPresentationTimeUs;
    
    private int mSize;
    
    private final ArrayList<String> mTuningKeys;
    
    private final ArrayList<Object> mTuningValues;
    
    final MediaCodec this$0;
    
    private QueueRequest(MediaCodec param1MediaCodec1, int param1Int) {
      this.mLinearBlock = null;
      this.mOffset = 0;
      this.mSize = 0;
      this.mCryptoInfo = null;
      this.mHardwareBuffer = null;
      this.mPresentationTimeUs = 0L;
      this.mFlags = 0;
      this.mTuningKeys = new ArrayList<>();
      this.mTuningValues = new ArrayList();
      this.mAccessible = false;
      this.mCodec = param1MediaCodec1;
      this.mIndex = param1Int;
    }
    
    public QueueRequest setLinearBlock(MediaCodec.LinearBlock param1LinearBlock, int param1Int1, int param1Int2) {
      if (isAccessible()) {
        if (this.mLinearBlock == null && this.mHardwareBuffer == null) {
          this.mLinearBlock = param1LinearBlock;
          this.mOffset = param1Int1;
          this.mSize = param1Int2;
          this.mCryptoInfo = null;
          return this;
        } 
        throw new IllegalStateException("Cannot set block twice");
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setEncryptedLinearBlock(MediaCodec.LinearBlock param1LinearBlock, int param1Int1, int param1Int2, MediaCodec.CryptoInfo param1CryptoInfo) {
      Objects.requireNonNull(param1CryptoInfo);
      if (isAccessible()) {
        if (this.mLinearBlock == null && this.mHardwareBuffer == null) {
          this.mLinearBlock = param1LinearBlock;
          this.mOffset = param1Int1;
          this.mSize = param1Int2;
          this.mCryptoInfo = param1CryptoInfo;
          return this;
        } 
        throw new IllegalStateException("Cannot set block twice");
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setHardwareBuffer(HardwareBuffer param1HardwareBuffer) {
      if (isAccessible()) {
        if (this.mLinearBlock == null && this.mHardwareBuffer == null) {
          this.mHardwareBuffer = param1HardwareBuffer;
          return this;
        } 
        throw new IllegalStateException("Cannot set block twice");
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setPresentationTimeUs(long param1Long) {
      if (isAccessible()) {
        this.mPresentationTimeUs = param1Long;
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setFlags(int param1Int) {
      if (isAccessible()) {
        this.mFlags = param1Int;
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setIntegerParameter(String param1String, int param1Int) {
      if (isAccessible()) {
        this.mTuningKeys.add(param1String);
        this.mTuningValues.add(Integer.valueOf(param1Int));
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setLongParameter(String param1String, long param1Long) {
      if (isAccessible()) {
        this.mTuningKeys.add(param1String);
        this.mTuningValues.add(Long.valueOf(param1Long));
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setFloatParameter(String param1String, float param1Float) {
      if (isAccessible()) {
        this.mTuningKeys.add(param1String);
        this.mTuningValues.add(Float.valueOf(param1Float));
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setByteBufferParameter(String param1String, ByteBuffer param1ByteBuffer) {
      if (isAccessible()) {
        this.mTuningKeys.add(param1String);
        this.mTuningValues.add(param1ByteBuffer);
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public QueueRequest setStringParameter(String param1String1, String param1String2) {
      if (isAccessible()) {
        this.mTuningKeys.add(param1String1);
        this.mTuningValues.add(param1String2);
        return this;
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    public void queue() {
      if (isAccessible()) {
        if (this.mLinearBlock != null || this.mHardwareBuffer != null) {
          setAccessible(false);
          MediaCodec.LinearBlock linearBlock = this.mLinearBlock;
          if (linearBlock != null) {
            this.mCodec.native_queueLinearBlock(this.mIndex, linearBlock, this.mOffset, this.mSize, this.mCryptoInfo, this.mPresentationTimeUs, this.mFlags, this.mTuningKeys, this.mTuningValues);
          } else {
            HardwareBuffer hardwareBuffer = this.mHardwareBuffer;
            if (hardwareBuffer != null)
              this.mCodec.native_queueHardwareBuffer(this.mIndex, hardwareBuffer, this.mPresentationTimeUs, this.mFlags, this.mTuningKeys, this.mTuningValues); 
          } 
          clear();
          return;
        } 
        throw new IllegalStateException("No block is set");
      } 
      throw new IllegalStateException("The request is stale");
    }
    
    QueueRequest clear() {
      this.mLinearBlock = null;
      this.mOffset = 0;
      this.mSize = 0;
      this.mCryptoInfo = null;
      this.mHardwareBuffer = null;
      this.mPresentationTimeUs = 0L;
      this.mFlags = 0;
      this.mTuningKeys.clear();
      this.mTuningValues.clear();
      return this;
    }
    
    boolean isAccessible() {
      return this.mAccessible;
    }
    
    QueueRequest setAccessible(boolean param1Boolean) {
      this.mAccessible = param1Boolean;
      return this;
    }
  }
  
  public QueueRequest getQueueRequest(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode == 1) {
        if (paramInt >= 0 && paramInt < this.mQueueRequests.size()) {
          QueueRequest queueRequest = this.mQueueRequests.get(paramInt);
          if (queueRequest != null) {
            if (queueRequest.isAccessible()) {
              queueRequest = queueRequest.clear();
              return queueRequest;
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            StringBuilder stringBuilder2 = new StringBuilder();
            this();
            stringBuilder2.append("The request is stale at index ");
            stringBuilder2.append(paramInt);
            this(stringBuilder2.toString());
            throw illegalArgumentException1;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Unavailable index: ");
          stringBuilder1.append(paramInt);
          this(stringBuilder1.toString());
          throw illegalArgumentException;
        } 
        IndexOutOfBoundsException indexOutOfBoundsException = new IndexOutOfBoundsException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Expected range of index: [0,");
        ArrayList<QueueRequest> arrayList = this.mQueueRequests;
        stringBuilder.append(arrayList.size() - 1);
        stringBuilder.append("]; actual: ");
        stringBuilder.append(paramInt);
        this(stringBuilder.toString());
        throw indexOutOfBoundsException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("The codec is not configured for block model");
      throw illegalStateException;
    } 
  }
  
  public final int dequeueOutputBuffer(BufferInfo paramBufferInfo, long paramLong) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        int i = native_dequeueOutputBuffer(paramBufferInfo, paramLong);
        null = this.mBufferLock;
        /* monitor enter ClassFileLocalVariableReferenceExpression{type=ObjectType{java/lang/Object}, name=null} */
        if (i == -3) {
          try {
            cacheBuffers(false);
          } finally {}
        } else if (i >= 0) {
          validateOutputByteBuffer(this.mCachedOutputBuffers, i, paramBufferInfo);
          if (this.mHasSurface)
            this.mDequeuedOutputInfos.put(Integer.valueOf(i), paramBufferInfo.dup()); 
        } 
        return i;
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "dequeueOutputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use MediaCodec.Callback objects to get output buffer slots.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public final void releaseOutputBuffer(int paramInt, boolean paramBoolean) {
    releaseOutputBufferInternal(paramInt, paramBoolean, false, 0L);
  }
  
  public final void releaseOutputBuffer(int paramInt, long paramLong) {
    releaseOutputBufferInternal(paramInt, true, true, paramLong);
  }
  
  private void releaseOutputBufferInternal(int paramInt, boolean paramBoolean1, boolean paramBoolean2, long paramLong) {
    synchronized (this.mBufferLock) {
      int i = this.mBufferMode;
      if (i != 0) {
        if (i == 1) {
          OutputFrame outputFrame = this.mOutputFrames.get(paramInt);
          outputFrame.setAccessible(false);
          outputFrame.clear();
        } else {
          IllegalStateException illegalStateException = new IllegalStateException();
          StringBuilder stringBuilder = new StringBuilder();
          this();
          stringBuilder.append("Unrecognized buffer mode: ");
          stringBuilder.append(this.mBufferMode);
          this(stringBuilder.toString());
          throw illegalStateException;
        } 
      } else {
        invalidateByteBuffer(this.mCachedOutputBuffers, paramInt);
        this.mDequeuedOutputBuffers.remove(paramInt);
        if (this.mHasSurface)
          BufferInfo bufferInfo = this.mDequeuedOutputInfos.remove(Integer.valueOf(paramInt)); 
      } 
      releaseOutputBuffer(paramInt, paramBoolean1, paramBoolean2, paramLong);
      return;
    } 
  }
  
  public final MediaFormat getOutputFormat() {
    return new MediaFormat(getFormatNative(false));
  }
  
  public final MediaFormat getInputFormat() {
    return new MediaFormat(getFormatNative(true));
  }
  
  public final MediaFormat getOutputFormat(int paramInt) {
    return new MediaFormat(getOutputFormatNative(paramInt));
  }
  
  class BufferMap {
    private static class CodecBuffer {
      private ByteBuffer mByteBuffer;
      
      private Image mImage;
      
      private CodecBuffer() {}
      
      public void free() {
        ByteBuffer byteBuffer = this.mByteBuffer;
        if (byteBuffer != null) {
          NioUtils.freeDirectBuffer(byteBuffer);
          this.mByteBuffer = null;
        } 
        Image image = this.mImage;
        if (image != null) {
          image.close();
          this.mImage = null;
        } 
      }
      
      public void setImage(Image param2Image) {
        free();
        this.mImage = param2Image;
      }
      
      public void setByteBuffer(ByteBuffer param2ByteBuffer) {
        free();
        this.mByteBuffer = param2ByteBuffer;
      }
    }
    
    private final Map<Integer, CodecBuffer> mMap = new HashMap<>();
    
    public void remove(int param1Int) {
      CodecBuffer codecBuffer = this.mMap.get(Integer.valueOf(param1Int));
      if (codecBuffer != null) {
        codecBuffer.free();
        this.mMap.remove(Integer.valueOf(param1Int));
      } 
    }
    
    public void put(int param1Int, ByteBuffer param1ByteBuffer) {
      CodecBuffer codecBuffer1 = this.mMap.get(Integer.valueOf(param1Int));
      CodecBuffer codecBuffer2 = codecBuffer1;
      if (codecBuffer1 == null) {
        codecBuffer2 = new CodecBuffer();
        this.mMap.put(Integer.valueOf(param1Int), codecBuffer2);
      } 
      codecBuffer2.setByteBuffer(param1ByteBuffer);
    }
    
    public void put(int param1Int, Image param1Image) {
      CodecBuffer codecBuffer1 = this.mMap.get(Integer.valueOf(param1Int));
      CodecBuffer codecBuffer2 = codecBuffer1;
      if (codecBuffer1 == null) {
        codecBuffer2 = new CodecBuffer();
        this.mMap.put(Integer.valueOf(param1Int), codecBuffer2);
      } 
      codecBuffer2.setImage(param1Image);
    }
    
    public void clear() {
      for (CodecBuffer codecBuffer : this.mMap.values())
        codecBuffer.free(); 
      this.mMap.clear();
    }
    
    private BufferMap() {}
  }
  
  private final void invalidateByteBuffer(ByteBuffer[] paramArrayOfByteBuffer, int paramInt) {
    if (paramArrayOfByteBuffer != null && paramInt >= 0 && paramInt < paramArrayOfByteBuffer.length) {
      ByteBuffer byteBuffer = paramArrayOfByteBuffer[paramInt];
      if (byteBuffer != null)
        byteBuffer.setAccessible(false); 
    } 
  }
  
  private final void validateInputByteBuffer(ByteBuffer[] paramArrayOfByteBuffer, int paramInt) {
    if (paramArrayOfByteBuffer != null && paramInt >= 0 && paramInt < paramArrayOfByteBuffer.length) {
      ByteBuffer byteBuffer = paramArrayOfByteBuffer[paramInt];
      if (byteBuffer != null) {
        byteBuffer.setAccessible(true);
        byteBuffer.clear();
      } 
    } 
  }
  
  private final void revalidateByteBuffer(ByteBuffer[] paramArrayOfByteBuffer, int paramInt) {
    // Byte code:
    //   0: aload_0
    //   1: getfield mBufferLock : Ljava/lang/Object;
    //   4: astore_3
    //   5: aload_3
    //   6: monitorenter
    //   7: aload_1
    //   8: ifnull -> 34
    //   11: iload_2
    //   12: iflt -> 34
    //   15: iload_2
    //   16: aload_1
    //   17: arraylength
    //   18: if_icmpge -> 34
    //   21: aload_1
    //   22: iload_2
    //   23: aaload
    //   24: astore_1
    //   25: aload_1
    //   26: ifnull -> 34
    //   29: aload_1
    //   30: iconst_1
    //   31: invokevirtual setAccessible : (Z)V
    //   34: aload_3
    //   35: monitorexit
    //   36: return
    //   37: astore_1
    //   38: aload_3
    //   39: monitorexit
    //   40: aload_1
    //   41: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #3734	-> 0
    //   #3735	-> 7
    //   #3736	-> 21
    //   #3737	-> 25
    //   #3738	-> 29
    //   #3741	-> 34
    //   #3742	-> 36
    //   #3741	-> 37
    // Exception table:
    //   from	to	target	type
    //   15	21	37	finally
    //   29	34	37	finally
    //   34	36	37	finally
    //   38	40	37	finally
  }
  
  private final void validateOutputByteBuffer(ByteBuffer[] paramArrayOfByteBuffer, int paramInt, BufferInfo paramBufferInfo) {
    if (paramArrayOfByteBuffer != null && paramInt >= 0 && paramInt < paramArrayOfByteBuffer.length) {
      ByteBuffer byteBuffer = paramArrayOfByteBuffer[paramInt];
      if (byteBuffer != null) {
        byteBuffer.setAccessible(true);
        byteBuffer.limit(paramBufferInfo.offset + paramBufferInfo.size).position(paramBufferInfo.offset);
      } 
    } 
  }
  
  private final void invalidateByteBuffers(ByteBuffer[] paramArrayOfByteBuffer) {
    if (paramArrayOfByteBuffer != null) {
      int i;
      byte b;
      for (i = paramArrayOfByteBuffer.length, b = 0; b < i; ) {
        ByteBuffer byteBuffer = paramArrayOfByteBuffer[b];
        if (byteBuffer != null)
          byteBuffer.setAccessible(false); 
        b++;
      } 
    } 
  }
  
  private final void freeByteBuffer(ByteBuffer paramByteBuffer) {
    if (paramByteBuffer != null)
      NioUtils.freeDirectBuffer(paramByteBuffer); 
  }
  
  private final void freeByteBuffers(ByteBuffer[] paramArrayOfByteBuffer) {
    if (paramArrayOfByteBuffer != null) {
      int i;
      byte b;
      for (i = paramArrayOfByteBuffer.length, b = 0; b < i; ) {
        ByteBuffer byteBuffer = paramArrayOfByteBuffer[b];
        freeByteBuffer(byteBuffer);
        b++;
      } 
    } 
  }
  
  private final void freeAllTrackedBuffers() {
    synchronized (this.mBufferLock) {
      freeByteBuffers(this.mCachedInputBuffers);
      freeByteBuffers(this.mCachedOutputBuffers);
      this.mCachedInputBuffers = null;
      this.mCachedOutputBuffers = null;
      this.mDequeuedInputBuffers.clear();
      this.mDequeuedOutputBuffers.clear();
      this.mQueueRequests.clear();
      this.mOutputFrames.clear();
      return;
    } 
  }
  
  private final void cacheBuffers(boolean paramBoolean) {
    ByteBuffer[] arrayOfByteBuffer = null;
    try {
      ByteBuffer[] arrayOfByteBuffer1 = getBuffers(paramBoolean);
      arrayOfByteBuffer = arrayOfByteBuffer1;
      invalidateByteBuffers(arrayOfByteBuffer1);
      arrayOfByteBuffer = arrayOfByteBuffer1;
    } catch (IllegalStateException illegalStateException) {}
    if (paramBoolean) {
      this.mCachedInputBuffers = arrayOfByteBuffer;
    } else {
      this.mCachedOutputBuffers = arrayOfByteBuffer;
    } 
  }
  
  public ByteBuffer[] getInputBuffers() {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        if (this.mCachedInputBuffers != null)
          return this.mCachedInputBuffers; 
        IllegalStateException illegalStateException = new IllegalStateException();
        this();
        throw illegalStateException;
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getInputBuffers() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please obtain MediaCodec.LinearBlock or HardwareBuffer objects and attach to QueueRequest objects.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public ByteBuffer[] getOutputBuffers() {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        if (this.mCachedOutputBuffers != null)
          return this.mCachedOutputBuffers; 
        IllegalStateException illegalStateException = new IllegalStateException();
        this();
        throw illegalStateException;
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getOutputBuffers() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use getOutputFrame to get output frames.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public ByteBuffer getInputBuffer(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        null = getBuffer(true, paramInt);
        synchronized (this.mBufferLock) {
          invalidateByteBuffer(this.mCachedInputBuffers, paramInt);
          this.mDequeuedInputBuffers.put(paramInt, null);
          return null;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getInputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please obtain MediaCodec.LinearBlock or HardwareBuffer objects and attach to QueueRequest objects.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public Image getInputImage(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        null = getImage(true, paramInt);
        synchronized (this.mBufferLock) {
          invalidateByteBuffer(this.mCachedInputBuffers, paramInt);
          this.mDequeuedInputBuffers.put(paramInt, null);
          return null;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getInputImage() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please obtain MediaCodec.LinearBlock or HardwareBuffer objects and attach to QueueRequest objects.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public ByteBuffer getOutputBuffer(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        null = getBuffer(false, paramInt);
        synchronized (this.mBufferLock) {
          invalidateByteBuffer(this.mCachedOutputBuffers, paramInt);
          this.mDequeuedOutputBuffers.put(paramInt, null);
          return null;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getOutputBuffer() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use getOutputFrame() to get output frames.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public Image getOutputImage(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode != 1) {
        null = getImage(false, paramInt);
        synchronized (this.mBufferLock) {
          invalidateByteBuffer(this.mCachedOutputBuffers, paramInt);
          this.mDequeuedOutputBuffers.put(paramInt, null);
          return null;
        } 
      } 
      IncompatibleWithBlockModelException incompatibleWithBlockModelException = new IncompatibleWithBlockModelException();
      this(this, "getOutputImage() is not compatible with CONFIGURE_FLAG_USE_BLOCK_MODEL. Please use getOutputFrame() to get output frames.");
      throw incompatibleWithBlockModelException;
    } 
  }
  
  public static final class OutputFrame {
    private boolean mAccessible;
    
    private final ArrayList<String> mChangedKeys;
    
    private int mFlags;
    
    private MediaFormat mFormat;
    
    private HardwareBuffer mHardwareBuffer;
    
    private final int mIndex;
    
    private final Set<String> mKeySet;
    
    private MediaCodec.LinearBlock mLinearBlock;
    
    private boolean mLoaded;
    
    private long mPresentationTimeUs;
    
    OutputFrame(int param1Int) {
      this.mLinearBlock = null;
      this.mHardwareBuffer = null;
      this.mPresentationTimeUs = 0L;
      this.mFlags = 0;
      this.mFormat = null;
      this.mChangedKeys = new ArrayList<>();
      this.mKeySet = new HashSet<>();
      this.mAccessible = false;
      this.mLoaded = false;
      this.mIndex = param1Int;
    }
    
    public MediaCodec.LinearBlock getLinearBlock() {
      if (this.mHardwareBuffer == null)
        return this.mLinearBlock; 
      throw new IllegalStateException("This output frame is not linear");
    }
    
    public HardwareBuffer getHardwareBuffer() {
      if (this.mLinearBlock == null)
        return this.mHardwareBuffer; 
      throw new IllegalStateException("This output frame is not graphic");
    }
    
    public long getPresentationTimeUs() {
      return this.mPresentationTimeUs;
    }
    
    public int getFlags() {
      return this.mFlags;
    }
    
    public MediaFormat getFormat() {
      return this.mFormat;
    }
    
    public Set<String> getChangedKeys() {
      if (this.mKeySet.isEmpty() && !this.mChangedKeys.isEmpty())
        this.mKeySet.addAll(this.mChangedKeys); 
      return Collections.unmodifiableSet(this.mKeySet);
    }
    
    void clear() {
      this.mLinearBlock = null;
      this.mHardwareBuffer = null;
      this.mFormat = null;
      this.mChangedKeys.clear();
      this.mKeySet.clear();
      this.mLoaded = false;
    }
    
    boolean isAccessible() {
      return this.mAccessible;
    }
    
    void setAccessible(boolean param1Boolean) {
      this.mAccessible = param1Boolean;
    }
    
    void setBufferInfo(MediaCodec.BufferInfo param1BufferInfo) {
      this.mPresentationTimeUs = param1BufferInfo.presentationTimeUs;
      this.mFlags = param1BufferInfo.flags;
    }
    
    boolean isLoaded() {
      return this.mLoaded;
    }
    
    void setLoaded(boolean param1Boolean) {
      this.mLoaded = param1Boolean;
    }
  }
  
  public OutputFrame getOutputFrame(int paramInt) {
    synchronized (this.mBufferLock) {
      if (this.mBufferMode == 1) {
        if (paramInt >= 0 && paramInt < this.mOutputFrames.size()) {
          OutputFrame outputFrame = this.mOutputFrames.get(paramInt);
          if (outputFrame != null) {
            if (outputFrame.isAccessible()) {
              if (!outputFrame.isLoaded()) {
                native_getOutputFrame(outputFrame, paramInt);
                outputFrame.setLoaded(true);
              } 
              return outputFrame;
            } 
            IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
            StringBuilder stringBuilder2 = new StringBuilder();
            this();
            stringBuilder2.append("The output frame is stale at index ");
            stringBuilder2.append(paramInt);
            this(stringBuilder2.toString());
            throw illegalArgumentException1;
          } 
          IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Unavailable index: ");
          stringBuilder1.append(paramInt);
          this(stringBuilder1.toString());
          throw illegalArgumentException;
        } 
        IndexOutOfBoundsException indexOutOfBoundsException = new IndexOutOfBoundsException();
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Expected range of index: [0,");
        ArrayList<QueueRequest> arrayList = this.mQueueRequests;
        stringBuilder.append(arrayList.size() - 1);
        stringBuilder.append("]; actual: ");
        stringBuilder.append(paramInt);
        this(stringBuilder.toString());
        throw indexOutOfBoundsException;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      this("The codec is not configured for block model");
      throw illegalStateException;
    } 
  }
  
  public void setAudioPresentation(AudioPresentation paramAudioPresentation) {
    if (paramAudioPresentation != null) {
      native_setAudioPresentation(paramAudioPresentation.getPresentationId(), paramAudioPresentation.getProgramId());
      return;
    } 
    throw new NullPointerException("audio presentation is null");
  }
  
  public final String getName() {
    String str1 = getCanonicalName();
    String str2 = this.mNameAtCreation;
    if (str2 != null)
      str1 = str2; 
    return str1;
  }
  
  public PersistableBundle getMetrics() {
    return native_getMetrics();
  }
  
  public final void setParameters(Bundle paramBundle) {
    if (paramBundle == null)
      return; 
    String[] arrayOfString = new String[paramBundle.size()];
    Object[] arrayOfObject = new Object[paramBundle.size()];
    byte b = 0;
    for (String str : paramBundle.keySet()) {
      arrayOfString[b] = str;
      Object object = paramBundle.get(str);
      if (object instanceof byte[]) {
        arrayOfObject[b] = ByteBuffer.wrap((byte[])object);
      } else {
        arrayOfObject[b] = object;
      } 
      b++;
    } 
    setParameters(arrayOfString, arrayOfObject);
  }
  
  public void setCallback(Callback paramCallback, Handler paramHandler) {
    if (paramCallback != null) {
      synchronized (this.mListenerLock) {
        paramHandler = getEventHandlerOn(paramHandler, this.mCallbackHandler);
        if (paramHandler != this.mCallbackHandler) {
          this.mCallbackHandler.removeMessages(2);
          this.mCallbackHandler.removeMessages(1);
          this.mCallbackHandler = (EventHandler)paramHandler;
        } 
      } 
    } else {
      paramHandler = this.mCallbackHandler;
      if (paramHandler != null) {
        paramHandler.removeMessages(2);
        this.mCallbackHandler.removeMessages(1);
      } 
    } 
    paramHandler = this.mCallbackHandler;
    if (paramHandler != null) {
      Message message = paramHandler.obtainMessage(2, 0, 0, paramCallback);
      this.mCallbackHandler.sendMessage(message);
      native_setCallback(paramCallback);
    } 
  }
  
  public void setCallback(Callback paramCallback) {
    setCallback(paramCallback, null);
  }
  
  public void setOnFrameRenderedListener(OnFrameRenderedListener paramOnFrameRenderedListener, Handler paramHandler) {
    synchronized (this.mListenerLock) {
      boolean bool;
      this.mOnFrameRenderedListener = paramOnFrameRenderedListener;
      if (paramOnFrameRenderedListener != null) {
        paramHandler = getEventHandlerOn(paramHandler, this.mOnFrameRenderedHandler);
        if (paramHandler != this.mOnFrameRenderedHandler)
          this.mOnFrameRenderedHandler.removeMessages(3); 
        this.mOnFrameRenderedHandler = (EventHandler)paramHandler;
      } else if (this.mOnFrameRenderedHandler != null) {
        this.mOnFrameRenderedHandler.removeMessages(3);
      } 
      if (paramOnFrameRenderedListener != null) {
        bool = true;
      } else {
        bool = false;
      } 
      native_enableOnFrameRenderedListener(bool);
      return;
    } 
  }
  
  private EventHandler getEventHandlerOn(Handler paramHandler, EventHandler paramEventHandler) {
    if (paramHandler == null)
      return this.mEventHandler; 
    Looper looper = paramHandler.getLooper();
    if (paramEventHandler.getLooper() == looper)
      return paramEventHandler; 
    return new EventHandler(this, looper);
  }
  
  class Callback {
    public abstract void onError(MediaCodec param1MediaCodec, MediaCodec.CodecException param1CodecException);
    
    public abstract void onInputBufferAvailable(MediaCodec param1MediaCodec, int param1Int);
    
    public abstract void onOutputBufferAvailable(MediaCodec param1MediaCodec, int param1Int, MediaCodec.BufferInfo param1BufferInfo);
    
    public abstract void onOutputFormatChanged(MediaCodec param1MediaCodec, MediaFormat param1MediaFormat);
  }
  
  private void postEventFromNative(int paramInt1, int paramInt2, int paramInt3, Object paramObject) {
    synchronized (this.mListenerLock) {
      EventHandler eventHandler = this.mEventHandler;
      if (paramInt1 == 1) {
        eventHandler = this.mCallbackHandler;
      } else if (paramInt1 == 3) {
        eventHandler = this.mOnFrameRenderedHandler;
      } 
      if (eventHandler != null) {
        paramObject = eventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject);
        eventHandler.sendMessage((Message)paramObject);
      } 
      return;
    } 
  }
  
  public MediaCodecInfo getCodecInfo() {
    null = getName();
    synchronized (this.mCodecInfoLock) {
      if (this.mCodecInfo == null) {
        MediaCodecInfo mediaCodecInfo = getOwnCodecInfo();
        if (mediaCodecInfo == null)
          this.mCodecInfo = MediaCodecList.getInfoFor(null); 
      } 
      return this.mCodecInfo;
    } 
  }
  
  static {
    System.loadLibrary("media_jni");
    native_init();
  }
  
  private final long lockAndGetContext() {
    this.mNativeContextLock.lock();
    return this.mNativeContext;
  }
  
  private final void setAndUnlockContext(long paramLong) {
    this.mNativeContext = paramLong;
    this.mNativeContextLock.unlock();
  }
  
  private final native ByteBuffer getBuffer(boolean paramBoolean, int paramInt);
  
  private final native ByteBuffer[] getBuffers(boolean paramBoolean);
  
  private final native Map<String, Object> getFormatNative(boolean paramBoolean);
  
  private final native Image getImage(boolean paramBoolean, int paramInt);
  
  private final native Map<String, Object> getOutputFormatNative(int paramInt);
  
  private final native MediaCodecInfo getOwnCodecInfo();
  
  private static native void native_closeMediaImage(long paramLong);
  
  private final native void native_configure(String[] paramArrayOfString, Object[] paramArrayOfObject, Surface paramSurface, MediaCrypto paramMediaCrypto, IHwBinder paramIHwBinder, int paramInt);
  
  private static final native PersistentSurface native_createPersistentInputSurface();
  
  private final native int native_dequeueInputBuffer(long paramLong);
  
  private final native int native_dequeueOutputBuffer(BufferInfo paramBufferInfo, long paramLong);
  
  private native void native_enableOnFrameRenderedListener(boolean paramBoolean);
  
  private final native void native_finalize();
  
  private final native void native_flush();
  
  private native PersistableBundle native_getMetrics();
  
  private native void native_getOutputFrame(OutputFrame paramOutputFrame, int paramInt);
  
  private static final native void native_init();
  
  private static native Image native_mapHardwareBuffer(HardwareBuffer paramHardwareBuffer);
  
  private native void native_queueHardwareBuffer(int paramInt1, HardwareBuffer paramHardwareBuffer, long paramLong, int paramInt2, ArrayList<String> paramArrayList, ArrayList<Object> paramArrayList1);
  
  private final native void native_queueInputBuffer(int paramInt1, int paramInt2, int paramInt3, long paramLong, int paramInt4) throws CryptoException;
  
  private native void native_queueLinearBlock(int paramInt1, LinearBlock paramLinearBlock, int paramInt2, int paramInt3, CryptoInfo paramCryptoInfo, long paramLong, int paramInt4, ArrayList<String> paramArrayList, ArrayList<Object> paramArrayList1);
  
  private final native void native_queueSecureInputBuffer(int paramInt1, int paramInt2, CryptoInfo paramCryptoInfo, long paramLong, int paramInt3) throws CryptoException;
  
  private final native void native_release();
  
  private static final native void native_releasePersistentInputSurface(Surface paramSurface);
  
  private final native void native_reset();
  
  private native void native_setAudioPresentation(int paramInt1, int paramInt2);
  
  private final native void native_setCallback(Callback paramCallback);
  
  private final native void native_setInputSurface(Surface paramSurface);
  
  private native void native_setSurface(Surface paramSurface);
  
  private final native void native_setup(String paramString, boolean paramBoolean1, boolean paramBoolean2);
  
  private final native void native_start();
  
  private final native void native_stop();
  
  private final native void releaseOutputBuffer(int paramInt, boolean paramBoolean1, boolean paramBoolean2, long paramLong);
  
  private final native void setParameters(String[] paramArrayOfString, Object[] paramArrayOfObject);
  
  public final native Surface createInputSurface();
  
  public final native String getCanonicalName();
  
  public final native void setVideoScalingMode(int paramInt);
  
  public final native void signalEndOfInputStream();
  
  class MediaImage extends Image {
    private final int mYOffset;
    
    private final int mXOffset;
    
    private final int mWidth;
    
    private final int mTransform = 0;
    
    private long mTimestamp;
    
    private final int mScalingMode = 0;
    
    private final Image.Plane[] mPlanes;
    
    private final boolean mIsReadOnly;
    
    private final ByteBuffer mInfo;
    
    private final int mHeight;
    
    private final int mFormat;
    
    private final long mBufferContext;
    
    private final ByteBuffer mBuffer;
    
    private static final int TYPE_YUV = 1;
    
    public int getFormat() {
      throwISEIfImageIsInvalid();
      return this.mFormat;
    }
    
    public int getHeight() {
      throwISEIfImageIsInvalid();
      return this.mHeight;
    }
    
    public int getWidth() {
      throwISEIfImageIsInvalid();
      return this.mWidth;
    }
    
    public int getTransform() {
      throwISEIfImageIsInvalid();
      return 0;
    }
    
    public int getScalingMode() {
      throwISEIfImageIsInvalid();
      return 0;
    }
    
    public long getTimestamp() {
      throwISEIfImageIsInvalid();
      return this.mTimestamp;
    }
    
    public Image.Plane[] getPlanes() {
      throwISEIfImageIsInvalid();
      Image.Plane[] arrayOfPlane = this.mPlanes;
      return Arrays.<Image.Plane>copyOf(arrayOfPlane, arrayOfPlane.length);
    }
    
    public void close() {
      if (this.mIsImageValid) {
        ByteBuffer byteBuffer = this.mBuffer;
        if (byteBuffer != null)
          NioUtils.freeDirectBuffer(byteBuffer); 
        long l = this.mBufferContext;
        if (l != 0L)
          MediaCodec.native_closeMediaImage(l); 
        this.mIsImageValid = false;
      } 
    }
    
    public void setCropRect(Rect param1Rect) {
      if (!this.mIsReadOnly) {
        super.setCropRect(param1Rect);
        return;
      } 
      throw new ReadOnlyBufferException();
    }
    
    public MediaImage(MediaCodec this$0, ByteBuffer param1ByteBuffer1, boolean param1Boolean, long param1Long, int param1Int1, int param1Int2, Rect param1Rect) {
      this.mFormat = 35;
      this.mTimestamp = param1Long;
      this.mIsImageValid = true;
      this.mIsReadOnly = this$0.isReadOnly();
      this.mBuffer = this$0.duplicate();
      this.mXOffset = param1Int1;
      this.mYOffset = param1Int2;
      this.mInfo = param1ByteBuffer1;
      this.mBufferContext = 0L;
      if (param1ByteBuffer1.remaining() == 104) {
        int i = param1ByteBuffer1.getInt();
        if (i == 1) {
          int j = param1ByteBuffer1.getInt();
          if (j == 3) {
            this.mWidth = param1ByteBuffer1.getInt();
            this.mHeight = i = param1ByteBuffer1.getInt();
            if (this.mWidth >= 1 && i >= 1) {
              int k = param1ByteBuffer1.getInt();
              if (k == 8) {
                i = param1ByteBuffer1.getInt();
                if (i == 8) {
                  Rect rect;
                  this.mPlanes = (Image.Plane[])new MediaPlane[j];
                  for (i = 0; i < j; ) {
                    int m = param1ByteBuffer1.getInt();
                    int n = param1ByteBuffer1.getInt();
                    int i1 = param1ByteBuffer1.getInt();
                    int i2 = param1ByteBuffer1.getInt();
                    int i3 = param1ByteBuffer1.getInt();
                    if (i2 == i3) {
                      byte b;
                      if (i == 0) {
                        b = 1;
                      } else {
                        b = 2;
                      } 
                      if (i2 == b) {
                        if (n >= 1 && i1 >= 1) {
                          this$0.clear();
                          this$0.position(this.mBuffer.position() + m + param1Int1 / i2 * n + param1Int2 / i3 * i1);
                          this$0.limit(this$0.position() + Utils.divUp(k, 8) + (this.mHeight / i3 - 1) * i1 + (this.mWidth / i2 - 1) * n);
                          this.mPlanes[i] = new MediaPlane(this$0.slice(), i1, n);
                          i++;
                        } 
                        StringBuilder stringBuilder7 = new StringBuilder();
                        stringBuilder7.append("unexpected strides: ");
                        stringBuilder7.append(n);
                        stringBuilder7.append(" pixel, ");
                        stringBuilder7.append(i1);
                        stringBuilder7.append(" row on plane ");
                        stringBuilder7.append(i);
                        throw new UnsupportedOperationException(stringBuilder7.toString());
                      } 
                    } 
                    StringBuilder stringBuilder6 = new StringBuilder();
                    stringBuilder6.append("unexpected subsampling: ");
                    stringBuilder6.append(i2);
                    stringBuilder6.append("x");
                    stringBuilder6.append(i3);
                    stringBuilder6.append(" on plane ");
                    stringBuilder6.append(i);
                    throw new UnsupportedOperationException(stringBuilder6.toString());
                  } 
                  if (param1Rect == null) {
                    rect = new Rect(0, 0, this.mWidth, this.mHeight);
                  } else {
                    rect = param1Rect;
                  } 
                  rect.offset(-param1Int1, -param1Int2);
                  super.setCropRect(rect);
                  return;
                } 
                StringBuilder stringBuilder5 = new StringBuilder();
                stringBuilder5.append("unsupported allocated bit depth: ");
                stringBuilder5.append(i);
                throw new UnsupportedOperationException(stringBuilder5.toString());
              } 
              StringBuilder stringBuilder4 = new StringBuilder();
              stringBuilder4.append("unsupported bit depth: ");
              stringBuilder4.append(k);
              throw new UnsupportedOperationException(stringBuilder4.toString());
            } 
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("unsupported size: ");
            stringBuilder3.append(this.mWidth);
            stringBuilder3.append("x");
            stringBuilder3.append(this.mHeight);
            throw new UnsupportedOperationException(stringBuilder3.toString());
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("unexpected number of planes: ");
          stringBuilder2.append(j);
          throw new RuntimeException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("unsupported type: ");
        stringBuilder1.append(i);
        throw new UnsupportedOperationException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("unsupported info length: ");
      stringBuilder.append(param1ByteBuffer1.remaining());
      throw new UnsupportedOperationException(stringBuilder.toString());
    }
    
    public MediaImage(MediaCodec this$0, int[] param1ArrayOfint1, int[] param1ArrayOfint2, int param1Int1, int param1Int2, int param1Int3, boolean param1Boolean, long param1Long1, int param1Int4, int param1Int5, Rect param1Rect, long param1Long2) {
      if (this$0.length == param1ArrayOfint1.length && this$0.length == param1ArrayOfint2.length) {
        this.mWidth = param1Int1;
        this.mHeight = param1Int2;
        this.mFormat = param1Int3;
        this.mTimestamp = param1Long1;
        this.mIsImageValid = true;
        this.mIsReadOnly = param1Boolean;
        this.mBuffer = null;
        this.mInfo = null;
        this.mPlanes = (Image.Plane[])new MediaPlane[this$0.length];
        for (param1Int1 = 0; param1Int1 < this$0.length; param1Int1++)
          this.mPlanes[param1Int1] = new MediaPlane((ByteBuffer)this$0[param1Int1], param1ArrayOfint1[param1Int1], param1ArrayOfint2[param1Int1]); 
        this.mXOffset = param1Int4;
        this.mYOffset = param1Int5;
        if (param1Rect == null)
          param1Rect = new Rect(0, 0, this.mWidth, this.mHeight); 
        param1Rect.offset(-param1Int4, -param1Int5);
        super.setCropRect(param1Rect);
        this.mBufferContext = param1Long2;
        return;
      } 
      throw new IllegalArgumentException("buffers, rowStrides and pixelStrides should have the same length");
    }
    
    private class MediaPlane extends Image.Plane {
      private final int mColInc;
      
      private final ByteBuffer mData;
      
      private final int mRowInc;
      
      final MediaCodec.MediaImage this$0;
      
      public MediaPlane(ByteBuffer param2ByteBuffer, int param2Int1, int param2Int2) {
        this.mData = param2ByteBuffer;
        this.mRowInc = param2Int1;
        this.mColInc = param2Int2;
      }
      
      public int getRowStride() {
        MediaCodec.MediaImage.this.throwISEIfImageIsInvalid();
        return this.mRowInc;
      }
      
      public int getPixelStride() {
        MediaCodec.MediaImage.this.throwISEIfImageIsInvalid();
        return this.mColInc;
      }
      
      public ByteBuffer getBuffer() {
        MediaCodec.MediaImage.this.throwISEIfImageIsInvalid();
        return this.mData;
      }
    }
  }
  
  public static final class MetricsConstants {
    public static final String CODEC = "android.media.mediacodec.codec";
    
    public static final String ENCODER = "android.media.mediacodec.encoder";
    
    public static final String HEIGHT = "android.media.mediacodec.height";
    
    public static final String MIME_TYPE = "android.media.mediacodec.mime";
    
    public static final String MODE = "android.media.mediacodec.mode";
    
    public static final String MODE_AUDIO = "audio";
    
    public static final String MODE_VIDEO = "video";
    
    public static final String ROTATION = "android.media.mediacodec.rotation";
    
    public static final String SECURE = "android.media.mediacodec.secure";
    
    public static final String WIDTH = "android.media.mediacodec.width";
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class BufferFlag implements Annotation {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface ConfigureFlag {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface CryptoErrorCode {}
  
  public static interface OnFrameRenderedListener {
    void onFrameRendered(MediaCodec param1MediaCodec, long param1Long1, long param1Long2);
  }
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface OutputBufferInfo {}
  
  @Retention(RetentionPolicy.SOURCE)
  public static @interface VideoScalingMode {}
}
