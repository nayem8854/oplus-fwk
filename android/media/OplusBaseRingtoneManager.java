package android.media;

public class OplusBaseRingtoneManager {
  public static final int TYPE_NOTIFICATION_CALENDAR = 32;
  
  public static final int TYPE_NOTIFICATION_SIM2 = 16;
  
  public static final int TYPE_NOTIFICATION_SMS = 8;
  
  public static final int TYPE_RINGTONE_SIM2 = 64;
}
