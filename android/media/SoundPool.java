package android.media;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.util.AndroidRuntimeException;
import android.util.Log;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicReference;

public class SoundPool extends PlayerBase {
  static {
    System.loadLibrary("soundpool");
  }
  
  private static final boolean DEBUG = Log.isLoggable("SoundPool", 3);
  
  private static final int SAMPLE_LOADED = 1;
  
  private static final String TAG = "SoundPool";
  
  private final AudioAttributes mAttributes;
  
  private final AtomicReference<EventHandler> mEventHandler = new AtomicReference<>(null);
  
  private boolean mHasAppOpsPlayAudio;
  
  private long mNativeContext;
  
  public SoundPool(int paramInt1, int paramInt2, int paramInt3) {
    this(paramInt1, audioAttributes);
    PlayerBase.deprecateStreamTypeForPlayback(paramInt2, "SoundPool", "SoundPool()");
  }
  
  private SoundPool(int paramInt, AudioAttributes paramAudioAttributes) {
    super(paramAudioAttributes, 3);
    if (native_setup(new WeakReference<>(this), paramInt, paramAudioAttributes) == 0) {
      this.mAttributes = paramAudioAttributes;
      baseRegisterPlayer();
      return;
    } 
    throw new RuntimeException("Native setup failed");
  }
  
  public final void release() {
    baseRelease();
    native_release();
  }
  
  protected void finalize() {
    release();
  }
  
  public int load(String paramString, int paramInt) {
    byte b1 = 0, b2 = 0;
    int i = b1;
    try {
      File file = new File();
      i = b1;
      this(paramString);
      i = b1;
      ParcelFileDescriptor parcelFileDescriptor = ParcelFileDescriptor.open(file, 268435456);
      i = b2;
      if (parcelFileDescriptor != null) {
        i = b1;
        paramInt = _load(parcelFileDescriptor.getFileDescriptor(), 0L, file.length(), paramInt);
        i = paramInt;
        parcelFileDescriptor.close();
        i = paramInt;
      } 
    } catch (IOException iOException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("error loading ");
      stringBuilder.append(paramString);
      Log.e("SoundPool", stringBuilder.toString());
    } 
    return i;
  }
  
  public int load(Context paramContext, int paramInt1, int paramInt2) {
    AssetFileDescriptor assetFileDescriptor = paramContext.getResources().openRawResourceFd(paramInt1);
    paramInt1 = 0;
    if (assetFileDescriptor != null) {
      paramInt1 = _load(assetFileDescriptor.getFileDescriptor(), assetFileDescriptor.getStartOffset(), assetFileDescriptor.getLength(), paramInt2);
      try {
        assetFileDescriptor.close();
      } catch (IOException iOException) {}
    } 
    return paramInt1;
  }
  
  public int load(AssetFileDescriptor paramAssetFileDescriptor, int paramInt) {
    if (paramAssetFileDescriptor != null) {
      long l = paramAssetFileDescriptor.getLength();
      if (l >= 0L)
        return _load(paramAssetFileDescriptor.getFileDescriptor(), paramAssetFileDescriptor.getStartOffset(), l, paramInt); 
      throw new AndroidRuntimeException("no length for fd");
    } 
    return 0;
  }
  
  public int load(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, int paramInt) {
    return _load(paramFileDescriptor, paramLong1, paramLong2, paramInt);
  }
  
  public final int play(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, int paramInt3, float paramFloat3) {
    baseStart();
    return _play(paramInt1, paramFloat1, paramFloat2, paramInt2, paramInt3, paramFloat3);
  }
  
  public final void setVolume(int paramInt, float paramFloat1, float paramFloat2) {
    _setVolume(paramInt, paramFloat1, paramFloat2);
  }
  
  int playerApplyVolumeShaper(VolumeShaper.Configuration paramConfiguration, VolumeShaper.Operation paramOperation) {
    return -1;
  }
  
  VolumeShaper.State playerGetVolumeShaperState(int paramInt) {
    return null;
  }
  
  void playerSetVolume(boolean paramBoolean, float paramFloat1, float paramFloat2) {
    _mute(paramBoolean);
  }
  
  int playerSetAuxEffectSendLevel(boolean paramBoolean, float paramFloat) {
    return 0;
  }
  
  void playerStart() {}
  
  void playerPause() {}
  
  void playerStop() {}
  
  public void setVolume(int paramInt, float paramFloat) {
    setVolume(paramInt, paramFloat, paramFloat);
  }
  
  public void setOnLoadCompleteListener(OnLoadCompleteListener paramOnLoadCompleteListener) {
    if (paramOnLoadCompleteListener == null) {
      this.mEventHandler.set(null);
      return;
    } 
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mEventHandler.set(new EventHandler(looper, paramOnLoadCompleteListener));
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mEventHandler.set(new EventHandler(looper, paramOnLoadCompleteListener));
      } else {
        this.mEventHandler.set(null);
      } 
    } 
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<SoundPool>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    paramObject1 = ((SoundPool)paramObject1).mEventHandler.get();
    if (paramObject1 == null)
      return; 
    paramObject2 = paramObject1.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
    paramObject1.sendMessage((Message)paramObject2);
  }
  
  private final native int _load(FileDescriptor paramFileDescriptor, long paramLong1, long paramLong2, int paramInt);
  
  private final native void _mute(boolean paramBoolean);
  
  private final native int _play(int paramInt1, float paramFloat1, float paramFloat2, int paramInt2, int paramInt3, float paramFloat3);
  
  private final native void _setVolume(int paramInt, float paramFloat1, float paramFloat2);
  
  private final native void native_release();
  
  private final native int native_setup(Object paramObject1, int paramInt, Object paramObject2);
  
  public final native void autoPause();
  
  public final native void autoResume();
  
  public final native void pause(int paramInt);
  
  public final native void resume(int paramInt);
  
  public final native void setLoop(int paramInt1, int paramInt2);
  
  public final native void setPriority(int paramInt1, int paramInt2);
  
  public final native void setRate(int paramInt, float paramFloat);
  
  public final native void stop(int paramInt);
  
  public final native boolean unload(int paramInt);
  
  class OnLoadCompleteListener {
    public abstract void onLoadComplete(SoundPool param1SoundPool, int param1Int1, int param1Int2);
  }
  
  private final class EventHandler extends Handler {
    private final SoundPool.OnLoadCompleteListener mOnLoadCompleteListener;
    
    final SoundPool this$0;
    
    EventHandler(Looper param1Looper, SoundPool.OnLoadCompleteListener param1OnLoadCompleteListener) {
      super(param1Looper);
      this.mOnLoadCompleteListener = param1OnLoadCompleteListener;
    }
    
    public void handleMessage(Message param1Message) {
      if (param1Message.what != 1) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Unknown message type ");
        stringBuilder.append(param1Message.what);
        Log.e("SoundPool", stringBuilder.toString());
        return;
      } 
      if (SoundPool.DEBUG) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Sample ");
        stringBuilder.append(param1Message.arg1);
        stringBuilder.append(" loaded");
        Log.d("SoundPool", stringBuilder.toString());
      } 
      this.mOnLoadCompleteListener.onLoadComplete(SoundPool.this, param1Message.arg1, param1Message.arg2);
    }
  }
  
  class Builder {
    private AudioAttributes mAudioAttributes;
    
    private int mMaxStreams = 1;
    
    public Builder setMaxStreams(int param1Int) throws IllegalArgumentException {
      if (param1Int > 0) {
        this.mMaxStreams = param1Int;
        return this;
      } 
      throw new IllegalArgumentException("Strictly positive value required for the maximum number of streams");
    }
    
    public Builder setAudioAttributes(AudioAttributes param1AudioAttributes) throws IllegalArgumentException {
      if (param1AudioAttributes != null) {
        this.mAudioAttributes = param1AudioAttributes;
        return this;
      } 
      throw new IllegalArgumentException("Invalid null AudioAttributes");
    }
    
    public SoundPool build() {
      if (this.mAudioAttributes == null) {
        AudioAttributes.Builder builder = new AudioAttributes.Builder();
        this.mAudioAttributes = builder.setUsage(1).build();
      } 
      return new SoundPool(this.mMaxStreams, this.mAudioAttributes);
    }
  }
}
