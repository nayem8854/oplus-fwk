package android.media;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.ArrayMap;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

public final class MediaRoute2ProviderInfo implements Parcelable {
  public static final Parcelable.Creator<MediaRoute2ProviderInfo> CREATOR = new Parcelable.Creator<MediaRoute2ProviderInfo>() {
      public MediaRoute2ProviderInfo createFromParcel(Parcel param1Parcel) {
        return new MediaRoute2ProviderInfo(param1Parcel);
      }
      
      public MediaRoute2ProviderInfo[] newArray(int param1Int) {
        return new MediaRoute2ProviderInfo[param1Int];
      }
    };
  
  final ArrayMap<String, MediaRoute2Info> mRoutes;
  
  final String mUniqueId;
  
  MediaRoute2ProviderInfo(Builder paramBuilder) {
    Objects.requireNonNull(paramBuilder, "builder must not be null.");
    this.mUniqueId = paramBuilder.mUniqueId;
    this.mRoutes = paramBuilder.mRoutes;
  }
  
  MediaRoute2ProviderInfo(Parcel paramParcel) {
    this.mUniqueId = paramParcel.readString();
    ArrayMap<String, MediaRoute2Info> arrayMap = paramParcel.createTypedArrayMap(MediaRoute2Info.CREATOR);
    if (arrayMap == null)
      arrayMap = ArrayMap.EMPTY; 
    this.mRoutes = arrayMap;
  }
  
  public boolean isValid() {
    if (this.mUniqueId == null)
      return false; 
    int i = this.mRoutes.size();
    for (byte b = 0; b < i; b++) {
      MediaRoute2Info mediaRoute2Info = (MediaRoute2Info)this.mRoutes.valueAt(b);
      if (mediaRoute2Info == null || !mediaRoute2Info.isValid())
        return false; 
    } 
    return true;
  }
  
  public String getUniqueId() {
    return this.mUniqueId;
  }
  
  public MediaRoute2Info getRoute(String paramString) {
    ArrayMap<String, MediaRoute2Info> arrayMap = this.mRoutes;
    Objects.requireNonNull(paramString, "routeId must not be null");
    return (MediaRoute2Info)arrayMap.get(paramString);
  }
  
  public Collection<MediaRoute2Info> getRoutes() {
    return this.mRoutes.values();
  }
  
  public int describeContents() {
    return 0;
  }
  
  public void writeToParcel(Parcel paramParcel, int paramInt) {
    paramParcel.writeString(this.mUniqueId);
    paramParcel.writeTypedArrayMap(this.mRoutes, paramInt);
  }
  
  public String toString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("MediaRouteProviderInfo { ");
    stringBuilder.append("uniqueId=");
    stringBuilder.append(this.mUniqueId);
    stringBuilder.append(", routes=");
    stringBuilder.append(Arrays.toString(getRoutes().toArray()));
    stringBuilder = stringBuilder.append(" }");
    return stringBuilder.toString();
  }
  
  class Builder {
    final ArrayMap<String, MediaRoute2Info> mRoutes;
    
    String mUniqueId;
    
    public Builder() {
      this.mRoutes = new ArrayMap();
    }
    
    public Builder(MediaRoute2ProviderInfo this$0) {
      Objects.requireNonNull(this$0, "descriptor must not be null");
      this.mUniqueId = this$0.mUniqueId;
      this.mRoutes = new ArrayMap(this$0.mRoutes);
    }
    
    public Builder setUniqueId(String param1String) {
      if (TextUtils.equals(this.mUniqueId, param1String))
        return this; 
      this.mUniqueId = param1String;
      ArrayMap arrayMap = new ArrayMap();
      for (Map.Entry entry : this.mRoutes.entrySet()) {
        MediaRoute2Info.Builder builder2 = new MediaRoute2Info.Builder((MediaRoute2Info)entry.getValue());
        String str = this.mUniqueId;
        MediaRoute2Info.Builder builder1 = builder2.setProviderId(str);
        MediaRoute2Info mediaRoute2Info = builder1.build();
        arrayMap.put(mediaRoute2Info.getOriginalId(), mediaRoute2Info);
      } 
      this.mRoutes.clear();
      this.mRoutes.putAll(arrayMap);
      return this;
    }
    
    public Builder setSystemRouteProvider(boolean param1Boolean) {
      int i = this.mRoutes.size();
      for (byte b = 0; b < i; b++) {
        MediaRoute2Info mediaRoute2Info = (MediaRoute2Info)this.mRoutes.valueAt(b);
        if (mediaRoute2Info.isSystemRoute() != param1Boolean) {
          ArrayMap<String, MediaRoute2Info> arrayMap = this.mRoutes;
          MediaRoute2Info.Builder builder = new MediaRoute2Info.Builder(mediaRoute2Info);
          builder = builder.setSystemRoute(param1Boolean);
          MediaRoute2Info mediaRoute2Info1 = builder.build();
          arrayMap.setValueAt(b, mediaRoute2Info1);
        } 
      } 
      return this;
    }
    
    public Builder addRoute(MediaRoute2Info param1MediaRoute2Info) {
      Objects.requireNonNull(param1MediaRoute2Info, "route must not be null");
      if (!this.mRoutes.containsKey(param1MediaRoute2Info.getOriginalId())) {
        MediaRoute2Info mediaRoute2Info;
        if (this.mUniqueId != null) {
          ArrayMap<String, MediaRoute2Info> arrayMap = this.mRoutes;
          String str2 = param1MediaRoute2Info.getOriginalId();
          MediaRoute2Info.Builder builder = new MediaRoute2Info.Builder(param1MediaRoute2Info);
          String str1 = this.mUniqueId;
          mediaRoute2Info = builder.setProviderId(str1).build();
          arrayMap.put(str2, mediaRoute2Info);
        } else {
          this.mRoutes.put(mediaRoute2Info.getOriginalId(), mediaRoute2Info);
        } 
        return this;
      } 
      throw new IllegalArgumentException("A route with the same id is already added");
    }
    
    public Builder addRoutes(Collection<MediaRoute2Info> param1Collection) {
      Objects.requireNonNull(param1Collection, "routes must not be null");
      if (!param1Collection.isEmpty())
        for (MediaRoute2Info mediaRoute2Info : param1Collection)
          addRoute(mediaRoute2Info);  
      return this;
    }
    
    public MediaRoute2ProviderInfo build() {
      return new MediaRoute2ProviderInfo(this);
    }
  }
}
