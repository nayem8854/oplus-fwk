package android.media;

import android.os.Handler;

class NativeRoutingEventHandlerDelegate {
  private AudioRouting mAudioRouting;
  
  private Handler mHandler;
  
  private AudioRouting.OnRoutingChangedListener mOnRoutingChangedListener;
  
  NativeRoutingEventHandlerDelegate(AudioRouting paramAudioRouting, AudioRouting.OnRoutingChangedListener paramOnRoutingChangedListener, Handler paramHandler) {
    this.mAudioRouting = paramAudioRouting;
    this.mOnRoutingChangedListener = paramOnRoutingChangedListener;
    this.mHandler = paramHandler;
  }
  
  void notifyClient() {
    Handler handler = this.mHandler;
    if (handler != null)
      handler.post(new Runnable() {
            final NativeRoutingEventHandlerDelegate this$0;
            
            public void run() {
              if (NativeRoutingEventHandlerDelegate.this.mOnRoutingChangedListener != null)
                NativeRoutingEventHandlerDelegate.this.mOnRoutingChangedListener.onRoutingChanged(NativeRoutingEventHandlerDelegate.this.mAudioRouting); 
            }
          }); 
  }
}
