package android.media.audiofx;

import android.util.Log;
import java.io.UnsupportedEncodingException;
import java.util.StringTokenizer;

public class Equalizer extends AudioEffect {
  private short mNumBands = 0;
  
  private OnParameterChangeListener mParamListener = null;
  
  private BaseParameterListener mBaseParamListener = null;
  
  private final Object mParamListenerLock = new Object();
  
  public static final int PARAM_BAND_FREQ_RANGE = 4;
  
  public static final int PARAM_BAND_LEVEL = 2;
  
  public static final int PARAM_CENTER_FREQ = 3;
  
  public static final int PARAM_CURRENT_PRESET = 6;
  
  public static final int PARAM_GET_BAND = 5;
  
  public static final int PARAM_GET_NUM_OF_PRESETS = 7;
  
  public static final int PARAM_GET_PRESET_NAME = 8;
  
  public static final int PARAM_LEVEL_RANGE = 1;
  
  public static final int PARAM_NUM_BANDS = 0;
  
  private static final int PARAM_PROPERTIES = 9;
  
  public static final int PARAM_STRING_SIZE_MAX = 32;
  
  private static final String TAG = "Equalizer";
  
  private int mNumPresets;
  
  private String[] mPresetNames;
  
  public Equalizer(int paramInt1, int paramInt2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_EQUALIZER, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    if (paramInt2 == 0)
      Log.w("Equalizer", "WARNING: attaching an Equalizer to global output mix is deprecated!"); 
    getNumberOfBands();
    this.mNumPresets = paramInt1 = getNumberOfPresets();
    if (paramInt1 != 0) {
      this.mPresetNames = new String[paramInt1];
      byte[] arrayOfByte = new byte[32];
      int[] arrayOfInt = new int[2];
      arrayOfInt[0] = 8;
      for (paramInt1 = 0; paramInt1 < this.mNumPresets; paramInt1++) {
        arrayOfInt[1] = paramInt1;
        checkStatus(getParameter(arrayOfInt, arrayOfByte));
        paramInt2 = 0;
        for (; arrayOfByte[paramInt2] != 0; paramInt2++);
        try {
          String arrayOfString[] = this.mPresetNames, str = new String();
          this(arrayOfByte, 0, paramInt2, "ISO-8859-1");
          arrayOfString[paramInt1] = str;
        } catch (UnsupportedEncodingException unsupportedEncodingException) {
          Log.e("Equalizer", "preset name decode error");
        } 
      } 
    } 
  }
  
  public short getNumberOfBands() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short s = this.mNumBands;
    if (s != 0)
      return s; 
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(new int[] { 0 }, arrayOfShort));
    this.mNumBands = s = arrayOfShort[0];
    return s;
  }
  
  public short[] getBandLevelRange() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[2];
    checkStatus(getParameter(1, arrayOfShort));
    return arrayOfShort;
  }
  
  public void setBandLevel(short paramShort1, short paramShort2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(new int[] { 2, paramShort1 }, new short[] { paramShort2 }));
  }
  
  public short getBandLevel(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(new int[] { 2, paramShort }, arrayOfShort));
    return arrayOfShort[0];
  }
  
  public int getCenterFreq(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(new int[] { 3, paramShort }, arrayOfInt));
    return arrayOfInt[0];
  }
  
  public int[] getBandFreqRange(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    int[] arrayOfInt = new int[2];
    checkStatus(getParameter(new int[] { 4, paramShort }, arrayOfInt));
    return arrayOfInt;
  }
  
  public short getBand(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(new int[] { 5, paramInt }, arrayOfShort));
    return arrayOfShort[0];
  }
  
  public short getCurrentPreset() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(6, arrayOfShort));
    return arrayOfShort[0];
  }
  
  public void usePreset(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(6, paramShort));
  }
  
  public short getNumberOfPresets() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(7, arrayOfShort));
    return arrayOfShort[0];
  }
  
  public String getPresetName(short paramShort) {
    if (paramShort >= 0 && paramShort < this.mNumPresets)
      return this.mPresetNames[paramShort]; 
    return "";
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final Equalizer this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      param1AudioEffect = null;
      synchronized (Equalizer.this.mParamListenerLock) {
        Equalizer.OnParameterChangeListener onParameterChangeListener;
        if (Equalizer.this.mParamListener != null)
          onParameterChangeListener = Equalizer.this.mParamListener; 
        if (onParameterChangeListener != null) {
          int i = -1;
          int j = -1;
          int k = j;
          if (param1ArrayOfbyte1.length >= 4) {
            int m = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0);
            i = m;
            k = j;
            if (param1ArrayOfbyte1.length >= 8) {
              k = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 4);
              i = m;
            } 
          } 
          if (param1ArrayOfbyte2.length == 2) {
            j = AudioEffect.byteArrayToShort(param1ArrayOfbyte2, 0);
          } else if (param1ArrayOfbyte2.length == 4) {
            j = AudioEffect.byteArrayToInt(param1ArrayOfbyte2, 0);
          } else {
            j = -1;
          } 
          if (i != -1 && j != -1)
            onParameterChangeListener.onParameterChange(Equalizer.this, param1Int, i, k, j); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        this.mParamListener = paramOnParameterChangeListener;
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      return;
    } 
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(Equalizer param1Equalizer, int param1Int1, int param1Int2, int param1Int3, int param1Int4);
  }
  
  class Settings {
    public short[] bandLevels;
    
    public short curPreset;
    
    public short numBands;
    
    public Settings() {
      this.numBands = 0;
      this.bandLevels = null;
    }
    
    public Settings(Equalizer this$0) {
      StringBuilder stringBuilder1;
      this.numBands = 0;
      this.bandLevels = null;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      stringTokenizer.countTokens();
      if (stringTokenizer.countTokens() >= 5) {
        StringBuilder stringBuilder;
        String str = stringTokenizer.nextToken();
        if (str.equals("Equalizer"))
          try {
            StringBuilder stringBuilder4;
            String str1 = stringTokenizer.nextToken();
            str = str1;
            boolean bool = str1.equals("curPreset");
            if (bool) {
              str = str1;
              this.curPreset = Short.parseShort(stringTokenizer.nextToken());
              str = str1;
              str1 = stringTokenizer.nextToken();
              str = str1;
              if (str1.equals("numBands")) {
                String str2;
                str = str1;
                this.numBands = Short.parseShort(stringTokenizer.nextToken());
                str = str1;
                if (stringTokenizer.countTokens() == this.numBands * 2) {
                  str = str1;
                  this.bandLevels = new short[this.numBands];
                  byte b = 0;
                  str2 = str1;
                  while (true) {
                    str = str2;
                    if (b < this.numBands) {
                      str = str2;
                      str2 = stringTokenizer.nextToken();
                      str = str2;
                      stringBuilder4 = new StringBuilder();
                      str = str2;
                      this();
                      str = str2;
                      stringBuilder4.append("band");
                      str = str2;
                      stringBuilder4.append(b + 1);
                      str = str2;
                      stringBuilder4.append("Level");
                      str = str2;
                      if (str2.equals(stringBuilder4.toString())) {
                        str = str2;
                        this.bandLevels[b] = Short.parseShort(stringTokenizer.nextToken());
                        b++;
                      } 
                      str = str2;
                      IllegalArgumentException illegalArgumentException3 = new IllegalArgumentException();
                      str = str2;
                      stringBuilder4 = new StringBuilder();
                      str = str2;
                      this();
                      str = str2;
                      stringBuilder4.append("invalid key name: ");
                      str = str2;
                      stringBuilder4.append(str2);
                      str = str2;
                      this(stringBuilder4.toString());
                      str = str2;
                      throw illegalArgumentException3;
                    } 
                    break;
                  } 
                  return;
                } 
                StringBuilder stringBuilder7 = stringBuilder4;
                IllegalArgumentException illegalArgumentException2 = new IllegalArgumentException();
                stringBuilder7 = stringBuilder4;
                StringBuilder stringBuilder8 = new StringBuilder();
                stringBuilder7 = stringBuilder4;
                this();
                stringBuilder7 = stringBuilder4;
                stringBuilder8.append("settings: ");
                stringBuilder7 = stringBuilder4;
                stringBuilder8.append(str2);
                stringBuilder7 = stringBuilder4;
                this(stringBuilder8.toString());
                stringBuilder7 = stringBuilder4;
                throw illegalArgumentException2;
              } 
              StringBuilder stringBuilder6 = stringBuilder4;
              IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
              stringBuilder6 = stringBuilder4;
              StringBuilder stringBuilder5 = new StringBuilder();
              stringBuilder6 = stringBuilder4;
              this();
              stringBuilder6 = stringBuilder4;
              stringBuilder5.append("invalid key name: ");
              stringBuilder6 = stringBuilder4;
              stringBuilder5.append((String)stringBuilder4);
              stringBuilder6 = stringBuilder4;
              this(stringBuilder5.toString());
              stringBuilder6 = stringBuilder4;
              throw illegalArgumentException1;
            } 
            stringBuilder = stringBuilder4;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            stringBuilder = stringBuilder4;
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder = stringBuilder4;
            this();
            stringBuilder = stringBuilder4;
            stringBuilder3.append("invalid key name: ");
            stringBuilder = stringBuilder4;
            stringBuilder3.append((String)stringBuilder4);
            stringBuilder = stringBuilder4;
            this(stringBuilder3.toString());
            stringBuilder = stringBuilder4;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("invalid value for key: ");
            stringBuilder3.append((String)stringBuilder);
            throw new IllegalArgumentException(stringBuilder3.toString());
          }  
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for Equalizer: ");
        stringBuilder1.append((String)stringBuilder);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("settings: ");
      stringBuilder2.append((String)stringBuilder1);
      throw new IllegalArgumentException(stringBuilder2.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Equalizer;curPreset=");
      short s = this.curPreset;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";numBands=");
      s = this.numBands;
      stringBuilder.append(Short.toString(s));
      String str = new String(stringBuilder.toString());
      for (byte b = 0; b < this.numBands; b++) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(";band");
        stringBuilder1.append(b + 1);
        stringBuilder1.append("Level=");
        stringBuilder1.append(Short.toString(this.bandLevels[b]));
        str = str.concat(stringBuilder1.toString());
      } 
      return str;
    }
  }
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[this.mNumBands * 2 + 4];
    checkStatus(getParameter(9, arrayOfByte));
    Settings settings = new Settings();
    settings.curPreset = byteArrayToShort(arrayOfByte, 0);
    settings.numBands = byteArrayToShort(arrayOfByte, 2);
    settings.bandLevels = new short[this.mNumBands];
    for (byte b = 0; b < this.mNumBands; b++)
      settings.bandLevels[b] = byteArrayToShort(arrayOfByte, b * 2 + 4); 
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    if (paramSettings.numBands == paramSettings.bandLevels.length && paramSettings.numBands == this.mNumBands) {
      byte[] arrayOfByte1 = shortToByteArray(paramSettings.curPreset);
      short s = this.mNumBands;
      byte[] arrayOfByte2 = shortToByteArray(s);
      arrayOfByte2 = concatArrays(new byte[][] { arrayOfByte1, arrayOfByte2 });
      for (byte b = 0; b < this.mNumBands; b++) {
        s = paramSettings.bandLevels[b];
        arrayOfByte1 = shortToByteArray(s);
        arrayOfByte2 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte1 });
      } 
      checkStatus(setParameter(9, arrayOfByte2));
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("settings invalid band count: ");
    stringBuilder.append(paramSettings.numBands);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
}
