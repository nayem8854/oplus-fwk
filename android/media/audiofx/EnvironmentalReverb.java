package android.media.audiofx;

import java.util.StringTokenizer;

public class EnvironmentalReverb extends AudioEffect {
  private OnParameterChangeListener mParamListener = null;
  
  private BaseParameterListener mBaseParamListener = null;
  
  private final Object mParamListenerLock = new Object();
  
  public EnvironmentalReverb(int paramInt1, int paramInt2) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_ENV_REVERB, EFFECT_TYPE_NULL, paramInt1, paramInt2);
  }
  
  public void setRoomLevel(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(0, arrayOfByte));
  }
  
  public short getRoomLevel() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(0, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setRoomHFLevel(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(1, arrayOfByte));
  }
  
  public short getRoomHFLevel() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(1, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setDecayTime(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = intToByteArray(paramInt);
    checkStatus(setParameter(2, arrayOfByte));
  }
  
  public int getDecayTime() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[4];
    checkStatus(getParameter(2, arrayOfByte));
    return byteArrayToInt(arrayOfByte);
  }
  
  public void setDecayHFRatio(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(3, arrayOfByte));
  }
  
  public short getDecayHFRatio() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(3, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setReflectionsLevel(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(4, arrayOfByte));
  }
  
  public short getReflectionsLevel() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(4, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setReflectionsDelay(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = intToByteArray(paramInt);
    checkStatus(setParameter(5, arrayOfByte));
  }
  
  public int getReflectionsDelay() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[4];
    checkStatus(getParameter(5, arrayOfByte));
    return byteArrayToInt(arrayOfByte);
  }
  
  public void setReverbLevel(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(6, arrayOfByte));
  }
  
  public short getReverbLevel() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(6, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setReverbDelay(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = intToByteArray(paramInt);
    checkStatus(setParameter(7, arrayOfByte));
  }
  
  public int getReverbDelay() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[4];
    checkStatus(getParameter(7, arrayOfByte));
    return byteArrayToInt(arrayOfByte);
  }
  
  public void setDiffusion(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(8, arrayOfByte));
  }
  
  public short getDiffusion() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(8, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  public void setDensity(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = shortToByteArray(paramShort);
    checkStatus(setParameter(9, arrayOfByte));
  }
  
  public short getDensity() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[2];
    checkStatus(getParameter(9, arrayOfByte));
    return byteArrayToShort(arrayOfByte);
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final EnvironmentalReverb this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      param1AudioEffect = null;
      synchronized (EnvironmentalReverb.this.mParamListenerLock) {
        EnvironmentalReverb.OnParameterChangeListener onParameterChangeListener;
        if (EnvironmentalReverb.this.mParamListener != null)
          onParameterChangeListener = EnvironmentalReverb.this.mParamListener; 
        if (onParameterChangeListener != null) {
          int i = -1;
          int j = -1;
          if (param1ArrayOfbyte1.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 2) {
            j = AudioEffect.byteArrayToShort(param1ArrayOfbyte2, 0);
          } else if (param1ArrayOfbyte2.length == 4) {
            j = AudioEffect.byteArrayToInt(param1ArrayOfbyte2, 0);
          } 
          if (i != -1 && j != -1)
            onParameterChangeListener.onParameterChange(EnvironmentalReverb.this, param1Int, i, j); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        this.mParamListener = paramOnParameterChangeListener;
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      return;
    } 
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(EnvironmentalReverb param1EnvironmentalReverb, int param1Int1, int param1Int2, int param1Int3);
  }
  
  class Settings {
    public short decayHFRatio;
    
    public int decayTime;
    
    public short density;
    
    public short diffusion;
    
    public int reflectionsDelay;
    
    public short reflectionsLevel;
    
    public int reverbDelay;
    
    public short reverbLevel;
    
    public short roomHFLevel;
    
    public short roomLevel;
    
    public Settings() {}
    
    public Settings(EnvironmentalReverb this$0) {
      String str;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      stringTokenizer.countTokens();
      if (stringTokenizer.countTokens() == 21) {
        str = stringTokenizer.nextToken();
        if (str.equals("EnvironmentalReverb"))
          try {
            String str1 = stringTokenizer.nextToken();
            str = str1;
            boolean bool = str1.equals("roomLevel");
            if (bool) {
              str = str1;
              this.roomLevel = Short.parseShort(stringTokenizer.nextToken());
              str = str1;
              str1 = stringTokenizer.nextToken();
              str = str1;
              if (str1.equals("roomHFLevel")) {
                str = str1;
                this.roomHFLevel = Short.parseShort(stringTokenizer.nextToken());
                str = str1;
                str1 = stringTokenizer.nextToken();
                str = str1;
                if (str1.equals("decayTime")) {
                  str = str1;
                  this.decayTime = Integer.parseInt(stringTokenizer.nextToken());
                  str = str1;
                  str1 = stringTokenizer.nextToken();
                  str = str1;
                  if (str1.equals("decayHFRatio")) {
                    str = str1;
                    this.decayHFRatio = Short.parseShort(stringTokenizer.nextToken());
                    str = str1;
                    str1 = stringTokenizer.nextToken();
                    str = str1;
                    if (str1.equals("reflectionsLevel")) {
                      str = str1;
                      this.reflectionsLevel = Short.parseShort(stringTokenizer.nextToken());
                      str = str1;
                      str1 = stringTokenizer.nextToken();
                      str = str1;
                      if (str1.equals("reflectionsDelay")) {
                        str = str1;
                        this.reflectionsDelay = Integer.parseInt(stringTokenizer.nextToken());
                        str = str1;
                        str1 = stringTokenizer.nextToken();
                        str = str1;
                        if (str1.equals("reverbLevel")) {
                          str = str1;
                          this.reverbLevel = Short.parseShort(stringTokenizer.nextToken());
                          str = str1;
                          str1 = stringTokenizer.nextToken();
                          str = str1;
                          if (str1.equals("reverbDelay")) {
                            str = str1;
                            this.reverbDelay = Integer.parseInt(stringTokenizer.nextToken());
                            str = str1;
                            str1 = stringTokenizer.nextToken();
                            str = str1;
                            if (str1.equals("diffusion")) {
                              str = str1;
                              this.diffusion = Short.parseShort(stringTokenizer.nextToken());
                              str = str1;
                              str1 = stringTokenizer.nextToken();
                              str = str1;
                              if (str1.equals("density")) {
                                str = str1;
                                this.density = Short.parseShort(stringTokenizer.nextToken());
                                return;
                              } 
                              str = str1;
                              IllegalArgumentException illegalArgumentException9 = new IllegalArgumentException();
                              str = str1;
                              StringBuilder stringBuilder11 = new StringBuilder();
                              str = str1;
                              this();
                              str = str1;
                              stringBuilder11.append("invalid key name: ");
                              str = str1;
                              stringBuilder11.append(str1);
                              str = str1;
                              this(stringBuilder11.toString());
                              str = str1;
                              throw illegalArgumentException9;
                            } 
                            str = str1;
                            IllegalArgumentException illegalArgumentException8 = new IllegalArgumentException();
                            str = str1;
                            StringBuilder stringBuilder10 = new StringBuilder();
                            str = str1;
                            this();
                            str = str1;
                            stringBuilder10.append("invalid key name: ");
                            str = str1;
                            stringBuilder10.append(str1);
                            str = str1;
                            this(stringBuilder10.toString());
                            str = str1;
                            throw illegalArgumentException8;
                          } 
                          str = str1;
                          IllegalArgumentException illegalArgumentException7 = new IllegalArgumentException();
                          str = str1;
                          StringBuilder stringBuilder9 = new StringBuilder();
                          str = str1;
                          this();
                          str = str1;
                          stringBuilder9.append("invalid key name: ");
                          str = str1;
                          stringBuilder9.append(str1);
                          str = str1;
                          this(stringBuilder9.toString());
                          str = str1;
                          throw illegalArgumentException7;
                        } 
                        str = str1;
                        IllegalArgumentException illegalArgumentException6 = new IllegalArgumentException();
                        str = str1;
                        StringBuilder stringBuilder8 = new StringBuilder();
                        str = str1;
                        this();
                        str = str1;
                        stringBuilder8.append("invalid key name: ");
                        str = str1;
                        stringBuilder8.append(str1);
                        str = str1;
                        this(stringBuilder8.toString());
                        str = str1;
                        throw illegalArgumentException6;
                      } 
                      str = str1;
                      IllegalArgumentException illegalArgumentException5 = new IllegalArgumentException();
                      str = str1;
                      StringBuilder stringBuilder7 = new StringBuilder();
                      str = str1;
                      this();
                      str = str1;
                      stringBuilder7.append("invalid key name: ");
                      str = str1;
                      stringBuilder7.append(str1);
                      str = str1;
                      this(stringBuilder7.toString());
                      str = str1;
                      throw illegalArgumentException5;
                    } 
                    str = str1;
                    IllegalArgumentException illegalArgumentException4 = new IllegalArgumentException();
                    str = str1;
                    StringBuilder stringBuilder6 = new StringBuilder();
                    str = str1;
                    this();
                    str = str1;
                    stringBuilder6.append("invalid key name: ");
                    str = str1;
                    stringBuilder6.append(str1);
                    str = str1;
                    this(stringBuilder6.toString());
                    str = str1;
                    throw illegalArgumentException4;
                  } 
                  str = str1;
                  IllegalArgumentException illegalArgumentException3 = new IllegalArgumentException();
                  str = str1;
                  StringBuilder stringBuilder5 = new StringBuilder();
                  str = str1;
                  this();
                  str = str1;
                  stringBuilder5.append("invalid key name: ");
                  str = str1;
                  stringBuilder5.append(str1);
                  str = str1;
                  this(stringBuilder5.toString());
                  str = str1;
                  throw illegalArgumentException3;
                } 
                str = str1;
                IllegalArgumentException illegalArgumentException2 = new IllegalArgumentException();
                str = str1;
                StringBuilder stringBuilder4 = new StringBuilder();
                str = str1;
                this();
                str = str1;
                stringBuilder4.append("invalid key name: ");
                str = str1;
                stringBuilder4.append(str1);
                str = str1;
                this(stringBuilder4.toString());
                str = str1;
                throw illegalArgumentException2;
              } 
              str = str1;
              IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
              str = str1;
              StringBuilder stringBuilder3 = new StringBuilder();
              str = str1;
              this();
              str = str1;
              stringBuilder3.append("invalid key name: ");
              str = str1;
              stringBuilder3.append(str1);
              str = str1;
              this(stringBuilder3.toString());
              str = str1;
              throw illegalArgumentException1;
            } 
            str = str1;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            str = str1;
            StringBuilder stringBuilder2 = new StringBuilder();
            str = str1;
            this();
            str = str1;
            stringBuilder2.append("invalid key name: ");
            str = str1;
            stringBuilder2.append(str1);
            str = str1;
            this(stringBuilder2.toString());
            str = str1;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("invalid value for key: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for EnvironmentalReverb: ");
        stringBuilder1.append(str);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("settings: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("EnvironmentalReverb;roomLevel=");
      short s = this.roomLevel;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";roomHFLevel=");
      s = this.roomHFLevel;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";decayTime=");
      int i = this.decayTime;
      stringBuilder.append(Integer.toString(i));
      stringBuilder.append(";decayHFRatio=");
      s = this.decayHFRatio;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";reflectionsLevel=");
      s = this.reflectionsLevel;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";reflectionsDelay=");
      i = this.reflectionsDelay;
      stringBuilder.append(Integer.toString(i));
      stringBuilder.append(";reverbLevel=");
      s = this.reverbLevel;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";reverbDelay=");
      i = this.reverbDelay;
      stringBuilder.append(Integer.toString(i));
      stringBuilder.append(";diffusion=");
      s = this.diffusion;
      stringBuilder.append(Short.toString(s));
      stringBuilder.append(";density=");
      s = this.density;
      stringBuilder.append(Short.toString(s));
      return new String(stringBuilder.toString());
    }
  }
  
  private static int PROPERTY_SIZE = 26;
  
  public static final int PARAM_DECAY_HF_RATIO = 3;
  
  public static final int PARAM_DECAY_TIME = 2;
  
  public static final int PARAM_DENSITY = 9;
  
  public static final int PARAM_DIFFUSION = 8;
  
  private static final int PARAM_PROPERTIES = 10;
  
  public static final int PARAM_REFLECTIONS_DELAY = 5;
  
  public static final int PARAM_REFLECTIONS_LEVEL = 4;
  
  public static final int PARAM_REVERB_DELAY = 7;
  
  public static final int PARAM_REVERB_LEVEL = 6;
  
  public static final int PARAM_ROOM_HF_LEVEL = 1;
  
  public static final int PARAM_ROOM_LEVEL = 0;
  
  private static final String TAG = "EnvironmentalReverb";
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte = new byte[PROPERTY_SIZE];
    checkStatus(getParameter(10, arrayOfByte));
    Settings settings = new Settings();
    settings.roomLevel = byteArrayToShort(arrayOfByte, 0);
    settings.roomHFLevel = byteArrayToShort(arrayOfByte, 2);
    settings.decayTime = byteArrayToInt(arrayOfByte, 4);
    settings.decayHFRatio = byteArrayToShort(arrayOfByte, 8);
    settings.reflectionsLevel = byteArrayToShort(arrayOfByte, 10);
    settings.reflectionsDelay = byteArrayToInt(arrayOfByte, 12);
    settings.reverbLevel = byteArrayToShort(arrayOfByte, 16);
    settings.reverbDelay = byteArrayToInt(arrayOfByte, 18);
    settings.diffusion = byteArrayToShort(arrayOfByte, 22);
    settings.density = byteArrayToShort(arrayOfByte, 24);
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    byte[] arrayOfByte2 = shortToByteArray(paramSettings.roomLevel);
    short s = paramSettings.roomHFLevel;
    byte[] arrayOfByte3 = shortToByteArray(s);
    int i = paramSettings.decayTime;
    byte[] arrayOfByte4 = intToByteArray(i);
    s = paramSettings.decayHFRatio;
    byte[] arrayOfByte5 = shortToByteArray(s);
    s = paramSettings.reflectionsLevel;
    byte[] arrayOfByte6 = shortToByteArray(s);
    i = paramSettings.reflectionsDelay;
    byte[] arrayOfByte7 = intToByteArray(i);
    s = paramSettings.reverbLevel;
    byte[] arrayOfByte8 = shortToByteArray(s);
    i = paramSettings.reverbDelay;
    byte[] arrayOfByte9 = intToByteArray(i);
    s = paramSettings.diffusion;
    byte[] arrayOfByte10 = shortToByteArray(s);
    s = paramSettings.density;
    byte[] arrayOfByte1 = shortToByteArray(s);
    arrayOfByte1 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte3, arrayOfByte4, arrayOfByte5, arrayOfByte6, arrayOfByte7, arrayOfByte8, arrayOfByte9, arrayOfByte10, arrayOfByte1 });
    checkStatus(setParameter(10, arrayOfByte1));
  }
}
