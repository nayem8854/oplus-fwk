package android.media.audiofx;

import android.util.Log;

public class AutomaticGainControl extends AudioEffect {
  private static final String TAG = "AutomaticGainControl";
  
  public static boolean isAvailable() {
    return AudioEffect.isEffectTypeAvailable(AudioEffect.EFFECT_TYPE_AGC);
  }
  
  public static AutomaticGainControl create(int paramInt) {
    unsupportedOperationException = null;
    AutomaticGainControl automaticGainControl = null;
    try {
      AutomaticGainControl automaticGainControl1 = new AutomaticGainControl();
      this(paramInt);
      automaticGainControl = automaticGainControl1;
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not implemented on this device ");
      stringBuilder.append((Object)null);
      Log.w("AutomaticGainControl", stringBuilder.toString());
    } catch (UnsupportedOperationException unsupportedOperationException) {
      Log.w("AutomaticGainControl", "not enough resources");
    } catch (RuntimeException runtimeException) {
      Log.w("AutomaticGainControl", "not enough memory");
      runtimeException = unsupportedOperationException;
    } 
    return (AutomaticGainControl)runtimeException;
  }
  
  private AutomaticGainControl(int paramInt) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_AGC, EFFECT_TYPE_NULL, 0, paramInt);
  }
}
