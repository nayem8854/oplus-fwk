package android.media.audiofx;

import android.util.Log;

public class NoiseSuppressor extends AudioEffect {
  private static final String TAG = "NoiseSuppressor";
  
  public static boolean isAvailable() {
    return AudioEffect.isEffectTypeAvailable(AudioEffect.EFFECT_TYPE_NS);
  }
  
  public static NoiseSuppressor create(int paramInt) {
    unsupportedOperationException = null;
    NoiseSuppressor noiseSuppressor = null;
    try {
      NoiseSuppressor noiseSuppressor1 = new NoiseSuppressor();
      this(paramInt);
      noiseSuppressor = noiseSuppressor1;
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not implemented on this device ");
      stringBuilder.append((Object)null);
      Log.w("NoiseSuppressor", stringBuilder.toString());
    } catch (UnsupportedOperationException unsupportedOperationException) {
      Log.w("NoiseSuppressor", "not enough resources");
    } catch (RuntimeException runtimeException) {
      Log.w("NoiseSuppressor", "not enough memory");
      runtimeException = unsupportedOperationException;
    } 
    return (NoiseSuppressor)runtimeException;
  }
  
  private NoiseSuppressor(int paramInt) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_NS, EFFECT_TYPE_NULL, 0, paramInt);
  }
}
