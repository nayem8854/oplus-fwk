package android.media.audiofx;

import java.util.StringTokenizer;

public class PresetReverb extends AudioEffect {
  private OnParameterChangeListener mParamListener = null;
  
  private BaseParameterListener mBaseParamListener = null;
  
  private final Object mParamListenerLock = new Object();
  
  public static final int PARAM_PRESET = 0;
  
  public static final short PRESET_LARGEHALL = 5;
  
  public static final short PRESET_LARGEROOM = 3;
  
  public static final short PRESET_MEDIUMHALL = 4;
  
  public static final short PRESET_MEDIUMROOM = 2;
  
  public static final short PRESET_NONE = 0;
  
  public static final short PRESET_PLATE = 6;
  
  public static final short PRESET_SMALLROOM = 1;
  
  private static final String TAG = "PresetReverb";
  
  public PresetReverb(int paramInt1, int paramInt2) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_PRESET_REVERB, EFFECT_TYPE_NULL, paramInt1, paramInt2);
  }
  
  public void setPreset(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(0, paramShort));
  }
  
  public short getPreset() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(0, arrayOfShort));
    return arrayOfShort[0];
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final PresetReverb this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      param1AudioEffect = null;
      synchronized (PresetReverb.this.mParamListenerLock) {
        PresetReverb.OnParameterChangeListener onParameterChangeListener;
        if (PresetReverb.this.mParamListener != null)
          onParameterChangeListener = PresetReverb.this.mParamListener; 
        if (onParameterChangeListener != null) {
          int i = -1;
          short s = -1;
          if (param1ArrayOfbyte1.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 2)
            s = AudioEffect.byteArrayToShort(param1ArrayOfbyte2, 0); 
          if (i != -1 && s != -1)
            onParameterChangeListener.onParameterChange(PresetReverb.this, param1Int, i, s); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        this.mParamListener = paramOnParameterChangeListener;
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      return;
    } 
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(PresetReverb param1PresetReverb, int param1Int1, int param1Int2, short param1Short);
  }
  
  class Settings {
    public short preset;
    
    public Settings() {}
    
    public Settings(PresetReverb this$0) {
      String str;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      stringTokenizer.countTokens();
      if (stringTokenizer.countTokens() == 3) {
        str = stringTokenizer.nextToken();
        if (str.equals("PresetReverb"))
          try {
            String str1 = stringTokenizer.nextToken();
            str = str1;
            if (str1.equals("preset")) {
              str = str1;
              this.preset = Short.parseShort(stringTokenizer.nextToken());
              return;
            } 
            str = str1;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            str = str1;
            StringBuilder stringBuilder2 = new StringBuilder();
            str = str1;
            this();
            str = str1;
            stringBuilder2.append("invalid key name: ");
            str = str1;
            stringBuilder2.append(str1);
            str = str1;
            this(stringBuilder2.toString());
            str = str1;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("invalid value for key: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for PresetReverb: ");
        stringBuilder1.append(str);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("settings: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PresetReverb;preset=");
      short s = this.preset;
      stringBuilder.append(Short.toString(s));
      return new String(stringBuilder.toString());
    }
  }
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    Settings settings = new Settings();
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(0, arrayOfShort));
    settings.preset = arrayOfShort[0];
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(0, paramSettings.preset));
  }
}
