package android.media.audiofx;

import android.app.ActivityThread;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import java.lang.ref.WeakReference;

public class Visualizer {
  static {
    System.loadLibrary("audioeffect_jni");
    native_init();
  }
  
  private int mState = 0;
  
  private final Object mStateLock = new Object();
  
  private final Object mListenerLock = new Object();
  
  private Handler mNativeEventHandler = null;
  
  private OnDataCaptureListener mCaptureListener = null;
  
  private OnServerDiedListener mServerDiedListener = null;
  
  public static final int ALREADY_EXISTS = -2;
  
  public static final int ERROR = -1;
  
  public static final int ERROR_BAD_VALUE = -4;
  
  public static final int ERROR_DEAD_OBJECT = -7;
  
  public static final int ERROR_INVALID_OPERATION = -5;
  
  public static final int ERROR_NO_INIT = -3;
  
  public static final int ERROR_NO_MEMORY = -6;
  
  public static final int MEASUREMENT_MODE_NONE = 0;
  
  public static final int MEASUREMENT_MODE_PEAK_RMS = 1;
  
  private static final int NATIVE_EVENT_FFT_CAPTURE = 1;
  
  private static final int NATIVE_EVENT_PCM_CAPTURE = 0;
  
  private static final int NATIVE_EVENT_SERVER_DIED = 2;
  
  public static final int SCALING_MODE_AS_PLAYED = 1;
  
  public static final int SCALING_MODE_NORMALIZED = 0;
  
  public static final int STATE_ENABLED = 2;
  
  public static final int STATE_INITIALIZED = 1;
  
  public static final int STATE_UNINITIALIZED = 0;
  
  public static final int SUCCESS = 0;
  
  private static final String TAG = "Visualizer-JAVA";
  
  private int mId;
  
  private long mJniData;
  
  private long mNativeVisualizer;
  
  public Visualizer(int paramInt) throws UnsupportedOperationException, RuntimeException {
    null = new int[1];
    synchronized (this.mStateLock) {
      UnsupportedOperationException unsupportedOperationException;
      this.mState = 0;
      WeakReference weakReference = new WeakReference();
      this((T)this);
      String str = ActivityThread.currentOpPackageName();
      paramInt = native_setup(weakReference, paramInt, null, str);
      if (paramInt != 0 && paramInt != -2) {
        StringBuilder stringBuilder = new StringBuilder();
        this();
        stringBuilder.append("Error code ");
        stringBuilder.append(paramInt);
        stringBuilder.append(" when initializing Visualizer.");
        Log.e("Visualizer-JAVA", stringBuilder.toString());
        if (paramInt != -5) {
          RuntimeException runtimeException = new RuntimeException();
          StringBuilder stringBuilder1 = new StringBuilder();
          this();
          stringBuilder1.append("Cannot initialize Visualizer engine, error: ");
          stringBuilder1.append(paramInt);
          this(stringBuilder1.toString());
          throw runtimeException;
        } 
        unsupportedOperationException = new UnsupportedOperationException();
        this("Effect library not loaded");
        throw unsupportedOperationException;
      } 
      this.mId = unsupportedOperationException[0];
      if (native_getEnabled()) {
        this.mState = 2;
      } else {
        this.mState = 1;
      } 
      return;
    } 
  }
  
  public void release() {
    synchronized (this.mStateLock) {
      native_release();
      this.mState = 0;
      return;
    } 
  }
  
  protected void finalize() {
    synchronized (this.mStateLock) {
      native_finalize();
      return;
    } 
  }
  
  public int setEnabled(boolean paramBoolean) throws IllegalStateException {
    // Byte code:
    //   0: aload_0
    //   1: getfield mStateLock : Ljava/lang/Object;
    //   4: astore_2
    //   5: aload_2
    //   6: monitorenter
    //   7: aload_0
    //   8: getfield mState : I
    //   11: ifeq -> 90
    //   14: iconst_0
    //   15: istore_3
    //   16: iconst_2
    //   17: istore #4
    //   19: iload_1
    //   20: ifeq -> 31
    //   23: aload_0
    //   24: getfield mState : I
    //   27: iconst_1
    //   28: if_icmpeq -> 49
    //   31: iload_3
    //   32: istore #5
    //   34: iload_1
    //   35: ifne -> 85
    //   38: iload_3
    //   39: istore #5
    //   41: aload_0
    //   42: getfield mState : I
    //   45: iconst_2
    //   46: if_icmpne -> 85
    //   49: aload_0
    //   50: iload_1
    //   51: invokespecial native_setEnabled : (Z)I
    //   54: istore_3
    //   55: iload_3
    //   56: istore #5
    //   58: iload_3
    //   59: ifne -> 85
    //   62: iload_1
    //   63: ifeq -> 73
    //   66: iload #4
    //   68: istore #5
    //   70: goto -> 76
    //   73: iconst_1
    //   74: istore #5
    //   76: aload_0
    //   77: iload #5
    //   79: putfield mState : I
    //   82: iload_3
    //   83: istore #5
    //   85: aload_2
    //   86: monitorexit
    //   87: iload #5
    //   89: ireturn
    //   90: new java/lang/IllegalStateException
    //   93: astore #6
    //   95: new java/lang/StringBuilder
    //   98: astore #7
    //   100: aload #7
    //   102: invokespecial <init> : ()V
    //   105: aload #7
    //   107: ldc_w 'setEnabled() called in wrong state: '
    //   110: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   113: pop
    //   114: aload #7
    //   116: aload_0
    //   117: getfield mState : I
    //   120: invokevirtual append : (I)Ljava/lang/StringBuilder;
    //   123: pop
    //   124: aload #6
    //   126: aload #7
    //   128: invokevirtual toString : ()Ljava/lang/String;
    //   131: invokespecial <init> : (Ljava/lang/String;)V
    //   134: aload #6
    //   136: athrow
    //   137: astore #7
    //   139: aload_2
    //   140: monitorexit
    //   141: aload #7
    //   143: athrow
    // Line number table:
    //   Java source line number -> byte code offset
    //   #269	-> 0
    //   #270	-> 7
    //   #273	-> 14
    //   #274	-> 16
    //   #276	-> 49
    //   #277	-> 55
    //   #278	-> 62
    //   #281	-> 85
    //   #271	-> 90
    //   #282	-> 137
    // Exception table:
    //   from	to	target	type
    //   7	14	137	finally
    //   23	31	137	finally
    //   41	49	137	finally
    //   49	55	137	finally
    //   76	82	137	finally
    //   85	87	137	finally
    //   90	137	137	finally
    //   139	141	137	finally
  }
  
  public boolean getEnabled() {
    synchronized (this.mStateLock) {
      if (this.mState != 0)
        return native_getEnabled(); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getEnabled() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int setCaptureSize(int paramInt) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState == 1) {
        paramInt = native_setCaptureSize(paramInt);
        return paramInt;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setCaptureSize() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getCaptureSize() throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0)
        return native_getCaptureSize(); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getCaptureSize() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int setScalingMode(int paramInt) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0) {
        paramInt = native_setScalingMode(paramInt);
        return paramInt;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setScalingMode() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getScalingMode() throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0)
        return native_getScalingMode(); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getScalingMode() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int setMeasurementMode(int paramInt) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0) {
        paramInt = native_setMeasurementMode(paramInt);
        return paramInt;
      } 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("setMeasurementMode() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getMeasurementMode() throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0)
        return native_getMeasurementMode(); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getMeasurementMode() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getSamplingRate() throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState != 0)
        return native_getSamplingRate(); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getSamplingRate() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getWaveForm(byte[] paramArrayOfbyte) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState == 2)
        return native_getWaveForm(paramArrayOfbyte); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getWaveForm() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int getFft(byte[] paramArrayOfbyte) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState == 2)
        return native_getFft(paramArrayOfbyte); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getFft() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public static final class MeasurementPeakRms {
    public int mPeak;
    
    public int mRms;
  }
  
  public int getMeasurementPeakRms(MeasurementPeakRms paramMeasurementPeakRms) {
    if (paramMeasurementPeakRms == null) {
      Log.e("Visualizer-JAVA", "Cannot store measurements in a null object");
      return -4;
    } 
    synchronized (this.mStateLock) {
      if (this.mState == 2)
        return native_getPeakRms(paramMeasurementPeakRms); 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append("getMeasurementPeakRms() called in wrong state: ");
      stringBuilder.append(this.mState);
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public int setDataCaptureListener(OnDataCaptureListener paramOnDataCaptureListener, int paramInt, boolean paramBoolean1, boolean paramBoolean2) {
    if (paramOnDataCaptureListener == null) {
      paramBoolean1 = false;
      paramBoolean2 = false;
    } 
    synchronized (this.mStateLock) {
      int i = native_setPeriodicCapture(paramInt, paramBoolean1, paramBoolean2);
      paramInt = i;
      if (i == 0)
        synchronized (this.mListenerLock) {
          this.mCaptureListener = paramOnDataCaptureListener;
          paramInt = i;
          if (paramOnDataCaptureListener != null) {
            paramInt = i;
            if (this.mNativeEventHandler == null) {
              Looper looper = Looper.myLooper();
              if (looper != null) {
                Handler handler = new Handler();
                this(looper);
                this.mNativeEventHandler = handler;
                paramInt = i;
              } else {
                Looper looper1 = Looper.getMainLooper();
                if (looper1 != null) {
                  Handler handler = new Handler();
                  this(looper1);
                  this.mNativeEventHandler = handler;
                  paramInt = i;
                } else {
                  this.mNativeEventHandler = null;
                  paramInt = -3;
                } 
              } 
            } 
          } 
        }  
      return paramInt;
    } 
  }
  
  public int setServerDiedListener(OnServerDiedListener paramOnServerDiedListener) {
    synchronized (this.mListenerLock) {
      this.mServerDiedListener = paramOnServerDiedListener;
      return 0;
    } 
  }
  
  private static void postEventFromNative(Object paramObject, int paramInt1, int paramInt2, byte[] paramArrayOfbyte) {
    Visualizer visualizer = ((WeakReference<Visualizer>)paramObject).get();
    if (visualizer == null)
      return; 
    synchronized (visualizer.mListenerLock) {
      Handler handler = visualizer.mNativeEventHandler;
      if (handler == null)
        return; 
      if (paramInt1 != 0 && paramInt1 != 1) {
        if (paramInt1 != 2) {
          paramObject = new StringBuilder();
          paramObject.append("Unknown native event in postEventFromNative: ");
          paramObject.append(paramInt1);
          Log.e("Visualizer-JAVA", paramObject.toString());
        } else {
          handler.post(new _$$Lambda$Visualizer$wqU8AX5xgtqzJWYkiQiECZdP6iU(visualizer));
        } 
      } else {
        handler.post(new _$$Lambda$Visualizer$k7o3lEP8U0dytML0O3eVt_uZU4U(visualizer, paramInt1, paramArrayOfbyte, paramInt2));
      } 
      return;
    } 
  }
  
  public static native int[] getCaptureSizeRange();
  
  public static native int getMaxCaptureRate();
  
  private final native void native_finalize();
  
  private final native int native_getCaptureSize();
  
  private final native boolean native_getEnabled();
  
  private final native int native_getFft(byte[] paramArrayOfbyte);
  
  private final native int native_getMeasurementMode();
  
  private final native int native_getPeakRms(MeasurementPeakRms paramMeasurementPeakRms);
  
  private final native int native_getSamplingRate();
  
  private final native int native_getScalingMode();
  
  private final native int native_getWaveForm(byte[] paramArrayOfbyte);
  
  private static final native void native_init();
  
  private final native void native_release();
  
  private final native int native_setCaptureSize(int paramInt);
  
  private final native int native_setEnabled(boolean paramBoolean);
  
  private final native int native_setMeasurementMode(int paramInt);
  
  private final native int native_setPeriodicCapture(int paramInt, boolean paramBoolean1, boolean paramBoolean2);
  
  private final native int native_setScalingMode(int paramInt);
  
  private final native int native_setup(Object paramObject, int paramInt, int[] paramArrayOfint, String paramString);
  
  public static interface OnDataCaptureListener {
    void onFftDataCapture(Visualizer param1Visualizer, byte[] param1ArrayOfbyte, int param1Int);
    
    void onWaveFormDataCapture(Visualizer param1Visualizer, byte[] param1ArrayOfbyte, int param1Int);
  }
  
  public static interface OnServerDiedListener {
    void onServerDied();
  }
}
