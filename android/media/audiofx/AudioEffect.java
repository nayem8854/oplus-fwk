package android.media.audiofx;

import android.annotation.SystemApi;
import android.app.ActivityThread;
import android.media.AudioDeviceAttributes;
import android.media.AudioDeviceInfo;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcel;
import android.util.Log;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Objects;
import java.util.UUID;

public class AudioEffect {
  public static final String ACTION_CLOSE_AUDIO_EFFECT_CONTROL_SESSION = "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION";
  
  public static final String ACTION_DISPLAY_AUDIO_EFFECT_CONTROL_PANEL = "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL";
  
  public static final String ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION = "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION";
  
  public static final int ALREADY_EXISTS = -2;
  
  public static final int CONTENT_TYPE_GAME = 2;
  
  public static final int CONTENT_TYPE_MOVIE = 1;
  
  public static final int CONTENT_TYPE_MUSIC = 0;
  
  public static final int CONTENT_TYPE_VOICE = 3;
  
  public static final String EFFECT_AUXILIARY = "Auxiliary";
  
  public static final String EFFECT_INSERT = "Insert";
  
  public static final String EFFECT_POST_PROCESSING = "Post Processing";
  
  public static final String EFFECT_PRE_PROCESSING = "Pre Processing";
  
  public static final UUID EFFECT_TYPE_AEC;
  
  public static final UUID EFFECT_TYPE_AGC;
  
  public static final UUID EFFECT_TYPE_BASS_BOOST;
  
  public static final UUID EFFECT_TYPE_DYNAMICS_PROCESSING;
  
  static {
    System.loadLibrary("audioeffect_jni");
    native_init();
  }
  
  public static final UUID EFFECT_TYPE_ENV_REVERB = UUID.fromString("c2e5d5f0-94bd-4763-9cac-4e234d06839e");
  
  public static final UUID EFFECT_TYPE_EQUALIZER;
  
  public static final UUID EFFECT_TYPE_LOUDNESS_ENHANCER;
  
  public static final UUID EFFECT_TYPE_NS;
  
  public static final UUID EFFECT_TYPE_NULL;
  
  public static final UUID EFFECT_TYPE_PRESET_REVERB = UUID.fromString("47382d60-ddd8-11db-bf3a-0002a5d5c51b");
  
  public static final UUID EFFECT_TYPE_VIRTUALIZER;
  
  public static final int ERROR = -1;
  
  public static final int ERROR_BAD_VALUE = -4;
  
  public static final int ERROR_DEAD_OBJECT = -7;
  
  public static final int ERROR_INVALID_OPERATION = -5;
  
  public static final int ERROR_NO_INIT = -3;
  
  public static final int ERROR_NO_MEMORY = -6;
  
  public static final String EXTRA_AUDIO_SESSION = "android.media.extra.AUDIO_SESSION";
  
  public static final String EXTRA_CONTENT_TYPE = "android.media.extra.CONTENT_TYPE";
  
  public static final String EXTRA_PACKAGE_NAME = "android.media.extra.PACKAGE_NAME";
  
  public static final int NATIVE_EVENT_CONTROL_STATUS = 0;
  
  public static final int NATIVE_EVENT_ENABLED_STATUS = 1;
  
  public static final int NATIVE_EVENT_PARAMETER_CHANGED = 2;
  
  public static final int STATE_INITIALIZED = 1;
  
  public static final int STATE_UNINITIALIZED = 0;
  
  public static final int SUCCESS = 0;
  
  private static final String TAG = "AudioEffect-JAVA";
  
  private OnControlStatusChangeListener mControlChangeStatusListener;
  
  private Descriptor mDescriptor;
  
  private OnEnableStatusChangeListener mEnableStatusChangeListener;
  
  private int mId;
  
  private long mJniData;
  
  public final Object mListenerLock;
  
  private long mNativeAudioEffect;
  
  public NativeEventHandler mNativeEventHandler;
  
  private OnParameterChangeListener mParameterChangeListener;
  
  private int mState;
  
  private final Object mStateLock;
  
  static {
    EFFECT_TYPE_EQUALIZER = UUID.fromString("0bed4300-ddd6-11db-8f34-0002a5d5c51b");
    EFFECT_TYPE_BASS_BOOST = UUID.fromString("0634f220-ddd4-11db-a0fc-0002a5d5c51b");
    EFFECT_TYPE_VIRTUALIZER = UUID.fromString("37cc2c00-dddd-11db-8577-0002a5d5c51b");
    EFFECT_TYPE_AGC = UUID.fromString("0a8abfe0-654c-11e0-ba26-0002a5d5c51b");
    EFFECT_TYPE_AEC = UUID.fromString("7b491460-8d4d-11e0-bd61-0002a5d5c51b");
    EFFECT_TYPE_NS = UUID.fromString("58b4b260-8e06-11e0-aa8e-0002a5d5c51b");
    EFFECT_TYPE_LOUDNESS_ENHANCER = UUID.fromString("fe3199be-aed0-413f-87bb-11260eb63cf1");
    EFFECT_TYPE_DYNAMICS_PROCESSING = UUID.fromString("7261676f-6d75-7369-6364-28e2fd3ac39e");
    EFFECT_TYPE_NULL = UUID.fromString("ec7178ec-e5e1-4432-a3f4-4657e6795210");
  }
  
  class Descriptor {
    public String connectMode;
    
    public String implementor;
    
    public String name;
    
    public UUID type;
    
    public UUID uuid;
    
    public Descriptor() {}
    
    public Descriptor(AudioEffect this$0, String param1String1, String param1String2, String param1String3, String param1String4) {
      this.type = UUID.fromString((String)this$0);
      this.uuid = UUID.fromString(param1String1);
      this.connectMode = param1String2;
      this.name = param1String3;
      this.implementor = param1String4;
    }
    
    public Descriptor(AudioEffect this$0) {
      this.type = UUID.fromString(this$0.readString());
      this.uuid = UUID.fromString(this$0.readString());
      this.connectMode = this$0.readString();
      this.name = this$0.readString();
      this.implementor = this$0.readString();
    }
    
    public int hashCode() {
      return Objects.hash(new Object[] { this.type, this.uuid, this.connectMode, this.name, this.implementor });
    }
    
    public void writeToParcel(Parcel param1Parcel) {
      param1Parcel.writeString(this.type.toString());
      param1Parcel.writeString(this.uuid.toString());
      param1Parcel.writeString(this.connectMode);
      param1Parcel.writeString(this.name);
      param1Parcel.writeString(this.implementor);
    }
    
    public boolean equals(Object param1Object) {
      null = true;
      if (this == param1Object)
        return true; 
      if (param1Object == null || !(param1Object instanceof Descriptor))
        return false; 
      param1Object = param1Object;
      if (this.type.equals(((Descriptor)param1Object).type)) {
        UUID uUID1 = this.uuid, uUID2 = ((Descriptor)param1Object).uuid;
        if (uUID1.equals(uUID2)) {
          String str2 = this.connectMode, str1 = ((Descriptor)param1Object).connectMode;
          if (str2.equals(str1)) {
            str1 = this.name;
            str2 = ((Descriptor)param1Object).name;
            if (str1.equals(str2)) {
              str1 = this.implementor;
              param1Object = ((Descriptor)param1Object).implementor;
              if (str1.equals(param1Object))
                return null; 
            } 
          } 
        } 
      } 
      return false;
    }
  }
  
  public AudioEffect(UUID paramUUID1, UUID paramUUID2, int paramInt1, int paramInt2) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    this(paramUUID1, paramUUID2, paramInt1, paramInt2, null);
  }
  
  @SystemApi
  public AudioEffect(UUID paramUUID, AudioDeviceAttributes paramAudioDeviceAttributes) {
    this(uUID, paramUUID, 0, -2, paramAudioDeviceAttributes);
  }
  
  private AudioEffect(UUID paramUUID1, UUID paramUUID2, int paramInt1, int paramInt2, AudioDeviceAttributes paramAudioDeviceAttributes) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    this(paramUUID1, paramUUID2, paramInt1, paramInt2, paramAudioDeviceAttributes, false);
  }
  
  private AudioEffect(UUID paramUUID1, UUID paramUUID2, int paramInt1, int paramInt2, AudioDeviceAttributes paramAudioDeviceAttributes, boolean paramBoolean) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    String str2;
    boolean bool;
    this.mState = 0;
    this.mStateLock = new Object();
    this.mEnableStatusChangeListener = null;
    this.mControlChangeStatusListener = null;
    this.mParameterChangeListener = null;
    this.mListenerLock = new Object();
    this.mNativeEventHandler = null;
    int[] arrayOfInt = new int[1];
    Descriptor[] arrayOfDescriptor = new Descriptor[1];
    if (paramAudioDeviceAttributes != null) {
      bool = AudioDeviceInfo.convertDeviceTypeToInternalDevice(paramAudioDeviceAttributes.getType());
      str2 = paramAudioDeviceAttributes.getAddress();
    } else {
      bool = false;
      str2 = "";
    } 
    WeakReference<AudioEffect> weakReference = new WeakReference<>(this);
    String str3 = paramUUID1.toString(), str1 = paramUUID2.toString();
    String str4 = ActivityThread.currentOpPackageName();
    paramInt1 = native_setup(weakReference, str3, str1, paramInt1, paramInt2, bool, str2, arrayOfInt, (Object[])arrayOfDescriptor, str4, paramBoolean);
    if (paramInt1 != 0 && paramInt1 != -2) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error code ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" when initializing AudioEffect.");
      Log.e("AudioEffect-JAVA", stringBuilder.toString());
      if (paramInt1 != -5) {
        if (paramInt1 != -4) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot initialize effect engine for type: ");
          stringBuilder.append(paramUUID1);
          stringBuilder.append(" Error: ");
          stringBuilder.append(paramInt1);
          throw new RuntimeException(stringBuilder.toString());
        } 
        stringBuilder = new StringBuilder();
        stringBuilder.append("Effect type: ");
        stringBuilder.append(paramUUID1);
        stringBuilder.append(" not supported.");
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
      throw new UnsupportedOperationException("Effect library not loaded");
    } 
    this.mId = arrayOfInt[0];
    this.mDescriptor = arrayOfDescriptor[0];
    if (!paramBoolean)
      synchronized (this.mStateLock) {
        this.mState = 1;
      }  
  }
  
  @SystemApi
  public static boolean isEffectSupportedForDevice(UUID paramUUID, AudioDeviceAttributes paramAudioDeviceAttributes) {
    try {
      AudioEffect audioEffect = new AudioEffect();
      UUID uUID = EFFECT_TYPE_NULL;
      Objects.requireNonNull(paramUUID);
      paramUUID = paramUUID;
      Objects.requireNonNull(paramAudioDeviceAttributes);
      this(uUID, paramUUID, 0, -2, paramAudioDeviceAttributes, true);
      audioEffect.release();
      return true;
    } catch (Exception exception) {
      return false;
    } 
  }
  
  public void release() {
    synchronized (this.mStateLock) {
      native_release();
      this.mState = 0;
      return;
    } 
  }
  
  protected void finalize() {
    native_finalize();
  }
  
  public Descriptor getDescriptor() throws IllegalStateException {
    checkState("getDescriptor()");
    return this.mDescriptor;
  }
  
  public static Descriptor[] queryEffects() {
    return (Descriptor[])native_query_effects();
  }
  
  public static Descriptor[] queryPreProcessings(int paramInt) {
    return (Descriptor[])native_query_pre_processing(paramInt);
  }
  
  public static boolean isEffectTypeAvailable(UUID paramUUID) {
    Descriptor[] arrayOfDescriptor = queryEffects();
    if (arrayOfDescriptor == null)
      return false; 
    for (byte b = 0; b < arrayOfDescriptor.length; b++) {
      if ((arrayOfDescriptor[b]).type.equals(paramUUID))
        return true; 
    } 
    return false;
  }
  
  public int setEnabled(boolean paramBoolean) throws IllegalStateException {
    checkState("setEnabled()");
    return native_setEnabled(paramBoolean);
  }
  
  public int setParameter(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws IllegalStateException {
    checkState("setParameter()");
    return native_setParameter(paramArrayOfbyte1.length, paramArrayOfbyte1, paramArrayOfbyte2.length, paramArrayOfbyte2);
  }
  
  public int setParameter(int paramInt1, int paramInt2) throws IllegalStateException {
    byte[] arrayOfByte1 = intToByteArray(paramInt1);
    byte[] arrayOfByte2 = intToByteArray(paramInt2);
    return setParameter(arrayOfByte1, arrayOfByte2);
  }
  
  public int setParameter(int paramInt, short paramShort) throws IllegalStateException {
    byte[] arrayOfByte1 = intToByteArray(paramInt);
    byte[] arrayOfByte2 = shortToByteArray(paramShort);
    return setParameter(arrayOfByte1, arrayOfByte2);
  }
  
  public int setParameter(int paramInt, byte[] paramArrayOfbyte) throws IllegalStateException {
    byte[] arrayOfByte = intToByteArray(paramInt);
    return setParameter(arrayOfByte, paramArrayOfbyte);
  }
  
  public int setParameter(int[] paramArrayOfint1, int[] paramArrayOfint2) throws IllegalStateException {
    if (paramArrayOfint1.length > 2 || paramArrayOfint2.length > 2)
      return -4; 
    byte[] arrayOfByte2 = intToByteArray(paramArrayOfint1[0]);
    byte[] arrayOfByte3 = arrayOfByte2;
    if (paramArrayOfint1.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint1[1]);
      arrayOfByte3 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte });
    } 
    arrayOfByte2 = intToByteArray(paramArrayOfint2[0]);
    byte[] arrayOfByte1 = arrayOfByte2;
    if (paramArrayOfint2.length > 1) {
      arrayOfByte1 = intToByteArray(paramArrayOfint2[1]);
      arrayOfByte1 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte1 });
    } 
    return setParameter(arrayOfByte3, arrayOfByte1);
  }
  
  public int setParameter(int[] paramArrayOfint, short[] paramArrayOfshort) throws IllegalStateException {
    if (paramArrayOfint.length > 2 || paramArrayOfshort.length > 2)
      return -4; 
    byte[] arrayOfByte2 = intToByteArray(paramArrayOfint[0]);
    byte[] arrayOfByte3 = arrayOfByte2;
    if (paramArrayOfint.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint[1]);
      arrayOfByte3 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte });
    } 
    arrayOfByte2 = shortToByteArray(paramArrayOfshort[0]);
    byte[] arrayOfByte1 = arrayOfByte2;
    if (paramArrayOfshort.length > 1) {
      arrayOfByte1 = shortToByteArray(paramArrayOfshort[1]);
      arrayOfByte1 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte1 });
    } 
    return setParameter(arrayOfByte3, arrayOfByte1);
  }
  
  public int setParameter(int[] paramArrayOfint, byte[] paramArrayOfbyte) throws IllegalStateException {
    if (paramArrayOfint.length > 2)
      return -4; 
    byte[] arrayOfByte1 = intToByteArray(paramArrayOfint[0]);
    byte[] arrayOfByte2 = arrayOfByte1;
    if (paramArrayOfint.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint[1]);
      arrayOfByte2 = concatArrays(new byte[][] { arrayOfByte1, arrayOfByte });
    } 
    return setParameter(arrayOfByte2, paramArrayOfbyte);
  }
  
  public int getParameter(byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws IllegalStateException {
    checkState("getParameter()");
    return native_getParameter(paramArrayOfbyte1.length, paramArrayOfbyte1, paramArrayOfbyte2.length, paramArrayOfbyte2);
  }
  
  public int getParameter(int paramInt, byte[] paramArrayOfbyte) throws IllegalStateException {
    byte[] arrayOfByte = intToByteArray(paramInt);
    return getParameter(arrayOfByte, paramArrayOfbyte);
  }
  
  public int getParameter(int paramInt, int[] paramArrayOfint) throws IllegalStateException {
    if (paramArrayOfint.length > 2)
      return -4; 
    byte[] arrayOfByte1 = intToByteArray(paramInt);
    byte[] arrayOfByte2 = new byte[paramArrayOfint.length * 4];
    paramInt = getParameter(arrayOfByte1, arrayOfByte2);
    if (paramInt == 4 || paramInt == 8) {
      paramArrayOfint[0] = byteArrayToInt(arrayOfByte2);
      if (paramInt == 8)
        paramArrayOfint[1] = byteArrayToInt(arrayOfByte2, 4); 
      paramInt /= 4;
      return paramInt;
    } 
    paramInt = -1;
    return paramInt;
  }
  
  public int getParameter(int paramInt, short[] paramArrayOfshort) throws IllegalStateException {
    if (paramArrayOfshort.length > 2)
      return -4; 
    byte[] arrayOfByte1 = intToByteArray(paramInt);
    byte[] arrayOfByte2 = new byte[paramArrayOfshort.length * 2];
    paramInt = getParameter(arrayOfByte1, arrayOfByte2);
    if (paramInt == 2 || paramInt == 4) {
      paramArrayOfshort[0] = byteArrayToShort(arrayOfByte2);
      if (paramInt == 4)
        paramArrayOfshort[1] = byteArrayToShort(arrayOfByte2, 2); 
      paramInt /= 2;
      return paramInt;
    } 
    paramInt = -1;
    return paramInt;
  }
  
  public int getParameter(int[] paramArrayOfint1, int[] paramArrayOfint2) throws IllegalStateException {
    if (paramArrayOfint1.length > 2 || paramArrayOfint2.length > 2)
      return -4; 
    byte[] arrayOfByte2 = intToByteArray(paramArrayOfint1[0]);
    byte[] arrayOfByte3 = arrayOfByte2;
    if (paramArrayOfint1.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint1[1]);
      arrayOfByte3 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte });
    } 
    byte[] arrayOfByte1 = new byte[paramArrayOfint2.length * 4];
    int i = getParameter(arrayOfByte3, arrayOfByte1);
    if (i == 4 || i == 8) {
      paramArrayOfint2[0] = byteArrayToInt(arrayOfByte1);
      if (i == 8)
        paramArrayOfint2[1] = byteArrayToInt(arrayOfByte1, 4); 
      i /= 4;
      return i;
    } 
    i = -1;
    return i;
  }
  
  public int getParameter(int[] paramArrayOfint, short[] paramArrayOfshort) throws IllegalStateException {
    if (paramArrayOfint.length > 2 || paramArrayOfshort.length > 2)
      return -4; 
    byte[] arrayOfByte2 = intToByteArray(paramArrayOfint[0]);
    byte[] arrayOfByte3 = arrayOfByte2;
    if (paramArrayOfint.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint[1]);
      arrayOfByte3 = concatArrays(new byte[][] { arrayOfByte2, arrayOfByte });
    } 
    byte[] arrayOfByte1 = new byte[paramArrayOfshort.length * 2];
    int i = getParameter(arrayOfByte3, arrayOfByte1);
    if (i == 2 || i == 4) {
      paramArrayOfshort[0] = byteArrayToShort(arrayOfByte1);
      if (i == 4)
        paramArrayOfshort[1] = byteArrayToShort(arrayOfByte1, 2); 
      i /= 2;
      return i;
    } 
    i = -1;
    return i;
  }
  
  public int getParameter(int[] paramArrayOfint, byte[] paramArrayOfbyte) throws IllegalStateException {
    if (paramArrayOfint.length > 2)
      return -4; 
    byte[] arrayOfByte1 = intToByteArray(paramArrayOfint[0]);
    byte[] arrayOfByte2 = arrayOfByte1;
    if (paramArrayOfint.length > 1) {
      byte[] arrayOfByte = intToByteArray(paramArrayOfint[1]);
      arrayOfByte2 = concatArrays(new byte[][] { arrayOfByte1, arrayOfByte });
    } 
    return getParameter(arrayOfByte2, paramArrayOfbyte);
  }
  
  public int command(int paramInt, byte[] paramArrayOfbyte1, byte[] paramArrayOfbyte2) throws IllegalStateException {
    checkState("command()");
    return native_command(paramInt, paramArrayOfbyte1.length, paramArrayOfbyte1, paramArrayOfbyte2.length, paramArrayOfbyte2);
  }
  
  public int getId() throws IllegalStateException {
    checkState("getId()");
    return this.mId;
  }
  
  public boolean getEnabled() throws IllegalStateException {
    checkState("getEnabled()");
    return native_getEnabled();
  }
  
  public boolean hasControl() throws IllegalStateException {
    checkState("hasControl()");
    return native_hasControl();
  }
  
  public void setEnableStatusListener(OnEnableStatusChangeListener paramOnEnableStatusChangeListener) {
    synchronized (this.mListenerLock) {
      this.mEnableStatusChangeListener = paramOnEnableStatusChangeListener;
      if (paramOnEnableStatusChangeListener != null && this.mNativeEventHandler == null)
        createNativeEventHandler(); 
      return;
    } 
  }
  
  public void setControlStatusListener(OnControlStatusChangeListener paramOnControlStatusChangeListener) {
    synchronized (this.mListenerLock) {
      this.mControlChangeStatusListener = paramOnControlStatusChangeListener;
      if (paramOnControlStatusChangeListener != null && this.mNativeEventHandler == null)
        createNativeEventHandler(); 
      return;
    } 
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mListenerLock) {
      this.mParameterChangeListener = paramOnParameterChangeListener;
      if (paramOnParameterChangeListener != null && this.mNativeEventHandler == null)
        createNativeEventHandler(); 
      return;
    } 
  }
  
  private void createNativeEventHandler() {
    Looper looper = Looper.myLooper();
    if (looper != null) {
      this.mNativeEventHandler = new NativeEventHandler(this, looper);
    } else {
      looper = Looper.getMainLooper();
      if (looper != null) {
        this.mNativeEventHandler = new NativeEventHandler(this, looper);
      } else {
        this.mNativeEventHandler = null;
      } 
    } 
  }
  
  class NativeEventHandler extends Handler {
    private AudioEffect mAudioEffect;
    
    final AudioEffect this$0;
    
    public NativeEventHandler(AudioEffect param1AudioEffect1, Looper param1Looper) {
      super(param1Looper);
      this.mAudioEffect = param1AudioEffect1;
    }
    
    public void handleMessage(Message param1Message) {
      if (this.mAudioEffect == null)
        return; 
      int i = param1Message.what;
      boolean bool1 = true, bool2 = true;
      if (i != 0) {
        if (i != 1) {
          if (i != 2) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("handleMessage() Unknown event type: ");
            stringBuilder.append(param1Message.what);
            Log.e("AudioEffect-JAVA", stringBuilder.toString());
          } else {
            synchronized (AudioEffect.this.mListenerLock) {
              AudioEffect.OnParameterChangeListener onParameterChangeListener = this.mAudioEffect.mParameterChangeListener;
              if (onParameterChangeListener != null) {
                i = param1Message.arg1;
                byte[] arrayOfByte1 = (byte[])param1Message.obj;
                int j = AudioEffect.byteArrayToInt(arrayOfByte1, 0);
                int k = AudioEffect.byteArrayToInt(arrayOfByte1, 4);
                int m = AudioEffect.byteArrayToInt(arrayOfByte1, 8);
                null = new byte[k];
                byte[] arrayOfByte2 = new byte[m];
                System.arraycopy(arrayOfByte1, 12, null, 0, k);
                System.arraycopy(arrayOfByte1, i, arrayOfByte2, 0, m);
                onParameterChangeListener.onParameterChange(this.mAudioEffect, j, (byte[])null, arrayOfByte2);
              } 
            } 
          } 
        } else {
          synchronized (AudioEffect.this.mListenerLock) {
            AudioEffect.OnEnableStatusChangeListener onEnableStatusChangeListener = this.mAudioEffect.mEnableStatusChangeListener;
            if (onEnableStatusChangeListener != null) {
              null = this.mAudioEffect;
              if (param1Message.arg1 == 0)
                bool2 = false; 
              onEnableStatusChangeListener.onEnableStatusChange((AudioEffect)null, bool2);
            } 
          } 
        } 
      } else {
        synchronized (AudioEffect.this.mListenerLock) {
          AudioEffect.OnControlStatusChangeListener onControlStatusChangeListener = this.mAudioEffect.mControlChangeStatusListener;
          if (onControlStatusChangeListener != null) {
            null = this.mAudioEffect;
            if (param1Message.arg1 != 0) {
              bool2 = bool1;
            } else {
              bool2 = false;
            } 
            onControlStatusChangeListener.onControlStatusChange((AudioEffect)null, bool2);
          } 
          return;
        } 
      } 
    }
  }
  
  private static void postEventFromNative(Object paramObject1, int paramInt1, int paramInt2, int paramInt3, Object paramObject2) {
    paramObject1 = ((WeakReference<AudioEffect>)paramObject1).get();
    if (paramObject1 == null)
      return; 
    NativeEventHandler nativeEventHandler = ((AudioEffect)paramObject1).mNativeEventHandler;
    if (nativeEventHandler != null) {
      paramObject2 = nativeEventHandler.obtainMessage(paramInt1, paramInt2, paramInt3, paramObject2);
      ((AudioEffect)paramObject1).mNativeEventHandler.sendMessage((Message)paramObject2);
    } 
  }
  
  public void checkState(String paramString) throws IllegalStateException {
    synchronized (this.mStateLock) {
      if (this.mState == 1)
        return; 
      IllegalStateException illegalStateException = new IllegalStateException();
      StringBuilder stringBuilder = new StringBuilder();
      this();
      stringBuilder.append(paramString);
      stringBuilder.append(" called on uninitialized AudioEffect.");
      this(stringBuilder.toString());
      throw illegalStateException;
    } 
  }
  
  public void checkStatus(int paramInt) {
    if (isError(paramInt)) {
      if (paramInt != -5) {
        if (paramInt != -4)
          throw new RuntimeException("AudioEffect: set/get parameter error"); 
        throw new IllegalArgumentException("AudioEffect: bad parameter value");
      } 
      throw new UnsupportedOperationException("AudioEffect: invalid parameter operation");
    } 
  }
  
  public static boolean isError(int paramInt) {
    boolean bool;
    if (paramInt < 0) {
      bool = true;
    } else {
      bool = false;
    } 
    return bool;
  }
  
  public static int byteArrayToInt(byte[] paramArrayOfbyte) {
    return byteArrayToInt(paramArrayOfbyte, 0);
  }
  
  public static int byteArrayToInt(byte[] paramArrayOfbyte, int paramInt) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
    byteBuffer.order(ByteOrder.nativeOrder());
    return byteBuffer.getInt(paramInt);
  }
  
  public static byte[] intToByteArray(int paramInt) {
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    byteBuffer.order(ByteOrder.nativeOrder());
    byteBuffer.putInt(paramInt);
    return byteBuffer.array();
  }
  
  public static short byteArrayToShort(byte[] paramArrayOfbyte) {
    return byteArrayToShort(paramArrayOfbyte, 0);
  }
  
  public static short byteArrayToShort(byte[] paramArrayOfbyte, int paramInt) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
    byteBuffer.order(ByteOrder.nativeOrder());
    return byteBuffer.getShort(paramInt);
  }
  
  public static byte[] shortToByteArray(short paramShort) {
    ByteBuffer byteBuffer = ByteBuffer.allocate(2);
    byteBuffer.order(ByteOrder.nativeOrder());
    byteBuffer.putShort(paramShort);
    return byteBuffer.array();
  }
  
  public static float byteArrayToFloat(byte[] paramArrayOfbyte) {
    return byteArrayToFloat(paramArrayOfbyte, 0);
  }
  
  public static float byteArrayToFloat(byte[] paramArrayOfbyte, int paramInt) {
    ByteBuffer byteBuffer = ByteBuffer.wrap(paramArrayOfbyte);
    byteBuffer.order(ByteOrder.nativeOrder());
    return byteBuffer.getFloat(paramInt);
  }
  
  public static byte[] floatToByteArray(float paramFloat) {
    ByteBuffer byteBuffer = ByteBuffer.allocate(4);
    byteBuffer.order(ByteOrder.nativeOrder());
    byteBuffer.putFloat(paramFloat);
    return byteBuffer.array();
  }
  
  public static byte[] concatArrays(byte[]... paramVarArgs) {
    int i = 0;
    int j;
    byte b;
    for (j = paramVarArgs.length, b = 0; b < j; ) {
      byte[] arrayOfByte1 = paramVarArgs[b];
      i += arrayOfByte1.length;
      b++;
    } 
    byte[] arrayOfByte = new byte[i];
    i = 0;
    for (j = paramVarArgs.length, b = 0; b < j; ) {
      byte[] arrayOfByte1 = paramVarArgs[b];
      System.arraycopy(arrayOfByte1, 0, arrayOfByte, i, arrayOfByte1.length);
      i += arrayOfByte1.length;
      b++;
    } 
    return arrayOfByte;
  }
  
  private final native int native_command(int paramInt1, int paramInt2, byte[] paramArrayOfbyte1, int paramInt3, byte[] paramArrayOfbyte2);
  
  private final native void native_finalize();
  
  private final native boolean native_getEnabled();
  
  private final native int native_getParameter(int paramInt1, byte[] paramArrayOfbyte1, int paramInt2, byte[] paramArrayOfbyte2);
  
  private final native boolean native_hasControl();
  
  private static final native void native_init();
  
  private static native Object[] native_query_effects();
  
  private static native Object[] native_query_pre_processing(int paramInt);
  
  private final native void native_release();
  
  private final native int native_setEnabled(boolean paramBoolean);
  
  private final native int native_setParameter(int paramInt1, byte[] paramArrayOfbyte1, int paramInt2, byte[] paramArrayOfbyte2);
  
  private final native int native_setup(Object paramObject, String paramString1, String paramString2, int paramInt1, int paramInt2, int paramInt3, String paramString3, int[] paramArrayOfint, Object[] paramArrayOfObject, String paramString4, boolean paramBoolean);
  
  class OnControlStatusChangeListener {
    public abstract void onControlStatusChange(AudioEffect param1AudioEffect, boolean param1Boolean);
  }
  
  class OnEnableStatusChangeListener {
    public abstract void onEnableStatusChange(AudioEffect param1AudioEffect, boolean param1Boolean);
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2);
  }
}
