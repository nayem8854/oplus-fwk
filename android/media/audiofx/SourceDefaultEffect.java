package android.media.audiofx;

import android.app.ActivityThread;
import android.util.Log;
import java.util.UUID;

public class SourceDefaultEffect extends DefaultEffect {
  private static final String TAG = "SourceDefaultEffect-JAVA";
  
  static {
    System.loadLibrary("audioeffect_jni");
  }
  
  public SourceDefaultEffect(UUID paramUUID1, UUID paramUUID2, int paramInt1, int paramInt2) {
    int[] arrayOfInt = new int[1];
    String str2 = paramUUID1.toString();
    String str1 = paramUUID2.toString();
    String str3 = ActivityThread.currentOpPackageName();
    paramInt1 = native_setup(str2, str1, paramInt1, paramInt2, str3, arrayOfInt);
    if (paramInt1 != 0) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Error code ");
      stringBuilder.append(paramInt1);
      stringBuilder.append(" when initializing SourceDefaultEffect");
      Log.e("SourceDefaultEffect-JAVA", stringBuilder.toString());
      if (paramInt1 != -5) {
        if (paramInt1 != -4) {
          stringBuilder = new StringBuilder();
          stringBuilder.append("Cannot initialize effect engine for type: ");
          stringBuilder.append(paramUUID1);
          stringBuilder.append(" Error: ");
          stringBuilder.append(paramInt1);
          throw new RuntimeException(stringBuilder.toString());
        } 
        throw new IllegalArgumentException("Source, type uuid, or implementation uuid not supported.");
      } 
      throw new UnsupportedOperationException("Effect library not loaded");
    } 
    this.mId = arrayOfInt[0];
  }
  
  public void release() {
    native_release(this.mId);
  }
  
  protected void finalize() {
    release();
  }
  
  private final native void native_release(int paramInt);
  
  private final native int native_setup(String paramString1, String paramString2, int paramInt1, int paramInt2, String paramString3, int[] paramArrayOfint);
}
