package android.media.audiofx;

import android.media.AudioDeviceInfo;
import android.media.AudioFormat;
import android.util.Log;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.StringTokenizer;

public class Virtualizer extends AudioEffect {
  private static final boolean DEBUG = false;
  
  public static final int PARAM_FORCE_VIRTUALIZATION_MODE = 3;
  
  public static final int PARAM_STRENGTH = 1;
  
  public static final int PARAM_STRENGTH_SUPPORTED = 0;
  
  public static final int PARAM_VIRTUALIZATION_MODE = 4;
  
  public static final int PARAM_VIRTUAL_SPEAKER_ANGLES = 2;
  
  private static final String TAG = "Virtualizer";
  
  public static final int VIRTUALIZATION_MODE_AUTO = 1;
  
  public static final int VIRTUALIZATION_MODE_BINAURAL = 2;
  
  public static final int VIRTUALIZATION_MODE_OFF = 0;
  
  public static final int VIRTUALIZATION_MODE_TRANSAURAL = 3;
  
  private BaseParameterListener mBaseParamListener;
  
  private OnParameterChangeListener mParamListener;
  
  private final Object mParamListenerLock;
  
  private boolean mStrengthSupported;
  
  public Virtualizer(int paramInt1, int paramInt2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_VIRTUALIZER, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    boolean bool = false;
    this.mStrengthSupported = false;
    this.mParamListener = null;
    this.mBaseParamListener = null;
    this.mParamListenerLock = new Object();
    if (paramInt2 == 0)
      Log.w("Virtualizer", "WARNING: attaching a Virtualizer to global output mix is deprecated!"); 
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(0, arrayOfInt));
    if (arrayOfInt[0] != 0)
      bool = true; 
    this.mStrengthSupported = bool;
  }
  
  public boolean getStrengthSupported() {
    return this.mStrengthSupported;
  }
  
  public void setStrength(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(1, paramShort));
  }
  
  public short getRoundedStrength() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(1, arrayOfShort));
    return arrayOfShort[0];
  }
  
  private boolean getAnglesInt(int paramInt1, int paramInt2, int[] paramArrayOfint) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    if (paramInt1 != 0) {
      StringBuilder stringBuilder1;
      if (paramInt1 == 1)
        paramInt1 = 12; 
      int i = AudioFormat.channelCountFromOutChannelMask(paramInt1);
      if (paramArrayOfint == null || paramArrayOfint.length >= i * 3) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(12);
        byteBuffer.order(ByteOrder.nativeOrder());
        byteBuffer.putInt(2);
        byteBuffer.putInt(AudioFormat.convertChannelOutMaskToNativeMask(paramInt1));
        byteBuffer.putInt(AudioDeviceInfo.convertDeviceTypeToInternalDevice(paramInt2));
        byte[] arrayOfByte = new byte[i * 4 * 3];
        paramInt1 = getParameter(byteBuffer.array(), arrayOfByte);
        if (paramInt1 >= 0) {
          if (paramArrayOfint != null) {
            ByteBuffer byteBuffer1 = ByteBuffer.wrap(arrayOfByte);
            byteBuffer1.order(ByteOrder.nativeOrder());
            for (paramInt1 = 0; paramInt1 < i; paramInt1++) {
              paramInt2 = byteBuffer1.getInt(paramInt1 * 4 * 3);
              paramArrayOfint[paramInt1 * 3] = AudioFormat.convertNativeChannelMaskToOutMask(paramInt2);
              paramArrayOfint[paramInt1 * 3 + 1] = byteBuffer1.getInt(paramInt1 * 4 * 3 + 4);
              paramArrayOfint[paramInt1 * 3 + 2] = byteBuffer1.getInt(paramInt1 * 4 * 3 + 8);
            } 
          } 
          return true;
        } 
        if (paramInt1 == -4)
          return false; 
        checkStatus(paramInt1);
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("unexpected status code ");
        stringBuilder1.append(paramInt1);
        stringBuilder1.append(" after getParameter(PARAM_VIRTUAL_SPEAKER_ANGLES)");
        Log.e("Virtualizer", stringBuilder1.toString());
        return false;
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Size of array for angles cannot accomodate number of channels in mask (");
      stringBuilder2.append(i);
      stringBuilder2.append(")");
      Log.e("Virtualizer", stringBuilder2.toString());
      stringBuilder2 = new StringBuilder();
      stringBuilder2.append("Virtualizer: array for channel / angle pairs is too small: is ");
      stringBuilder2.append(stringBuilder1.length);
      stringBuilder2.append(", should be ");
      stringBuilder2.append(i * 3);
      throw new IllegalArgumentException(stringBuilder2.toString());
    } 
    throw new IllegalArgumentException("Virtualizer: illegal CHANNEL_INVALID channel mask");
  }
  
  private static int getDeviceForModeQuery(int paramInt) throws IllegalArgumentException {
    if (paramInt != 2) {
      if (paramInt == 3)
        return 2; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Virtualizer: illegal virtualization mode ");
      stringBuilder.append(paramInt);
      throw new IllegalArgumentException(stringBuilder.toString());
    } 
    return 4;
  }
  
  private static int getDeviceForModeForce(int paramInt) throws IllegalArgumentException {
    if (paramInt == 1)
      return 0; 
    return getDeviceForModeQuery(paramInt);
  }
  
  private static int deviceToMode(int paramInt) {
    if (paramInt != 19)
      if (paramInt != 22) {
        switch (paramInt) {
          default:
            return 0;
          case 1:
          case 3:
          case 4:
          case 7:
            return 2;
          case 2:
          case 5:
          case 6:
          case 8:
          case 9:
          case 10:
          case 11:
          case 12:
          case 13:
          case 14:
            break;
        } 
        return 3;
      }  
    return 3;
  }
  
  public boolean canVirtualize(int paramInt1, int paramInt2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    return getAnglesInt(paramInt1, getDeviceForModeQuery(paramInt2), (int[])null);
  }
  
  public boolean getSpeakerAngles(int paramInt1, int paramInt2, int[] paramArrayOfint) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    if (paramArrayOfint != null)
      return getAnglesInt(paramInt1, getDeviceForModeQuery(paramInt2), paramArrayOfint); 
    throw new IllegalArgumentException("Virtualizer: illegal null channel / angle array");
  }
  
  public boolean forceVirtualizationMode(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    paramInt = getDeviceForModeForce(paramInt);
    paramInt = AudioDeviceInfo.convertDeviceTypeToInternalDevice(paramInt);
    paramInt = setParameter(3, paramInt);
    if (paramInt >= 0)
      return true; 
    if (paramInt == -4)
      return false; 
    checkStatus(paramInt);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unexpected status code ");
    stringBuilder.append(paramInt);
    stringBuilder.append(" after setParameter(PARAM_FORCE_VIRTUALIZATION_MODE)");
    Log.e("Virtualizer", stringBuilder.toString());
    return false;
  }
  
  public int getVirtualizationMode() throws IllegalStateException, UnsupportedOperationException {
    int[] arrayOfInt = new int[1];
    int i = getParameter(4, arrayOfInt);
    if (i >= 0)
      return deviceToMode(AudioDeviceInfo.convertInternalDeviceToDeviceType(arrayOfInt[0])); 
    if (i == -4)
      return 0; 
    checkStatus(i);
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("unexpected status code ");
    stringBuilder.append(i);
    stringBuilder.append(" after getParameter(PARAM_VIRTUALIZATION_MODE)");
    Log.e("Virtualizer", stringBuilder.toString());
    return 0;
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final Virtualizer this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      param1AudioEffect = null;
      synchronized (Virtualizer.this.mParamListenerLock) {
        Virtualizer.OnParameterChangeListener onParameterChangeListener;
        if (Virtualizer.this.mParamListener != null)
          onParameterChangeListener = Virtualizer.this.mParamListener; 
        if (onParameterChangeListener != null) {
          int i = -1;
          short s = -1;
          if (param1ArrayOfbyte1.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 2)
            s = AudioEffect.byteArrayToShort(param1ArrayOfbyte2, 0); 
          if (i != -1 && s != -1)
            onParameterChangeListener.onParameterChange(Virtualizer.this, param1Int, i, s); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        this.mParamListener = paramOnParameterChangeListener;
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      return;
    } 
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class ForceVirtualizationMode implements Annotation {}
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(Virtualizer param1Virtualizer, int param1Int1, int param1Int2, short param1Short);
  }
  
  class Settings {
    public short strength;
    
    public Settings() {}
    
    public Settings(Virtualizer this$0) {
      String str;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      stringTokenizer.countTokens();
      if (stringTokenizer.countTokens() == 3) {
        str = stringTokenizer.nextToken();
        if (str.equals("Virtualizer"))
          try {
            String str1 = stringTokenizer.nextToken();
            str = str1;
            if (str1.equals("strength")) {
              str = str1;
              this.strength = Short.parseShort(stringTokenizer.nextToken());
              return;
            } 
            str = str1;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            str = str1;
            StringBuilder stringBuilder2 = new StringBuilder();
            str = str1;
            this();
            str = str1;
            stringBuilder2.append("invalid key name: ");
            str = str1;
            stringBuilder2.append(str1);
            str = str1;
            this(stringBuilder2.toString());
            str = str1;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("invalid value for key: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for Virtualizer: ");
        stringBuilder1.append(str);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("settings: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("Virtualizer;strength=");
      short s = this.strength;
      stringBuilder.append(Short.toString(s));
      return new String(stringBuilder.toString());
    }
  }
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    Settings settings = new Settings();
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(1, arrayOfShort));
    settings.strength = arrayOfShort[0];
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(1, paramSettings.strength));
  }
  
  @Retention(RetentionPolicy.SOURCE)
  class VirtualizationMode implements Annotation {}
}
