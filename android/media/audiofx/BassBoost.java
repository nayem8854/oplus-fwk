package android.media.audiofx;

import android.util.Log;
import java.util.StringTokenizer;

public class BassBoost extends AudioEffect {
  public static final int PARAM_STRENGTH = 1;
  
  public static final int PARAM_STRENGTH_SUPPORTED = 0;
  
  private static final String TAG = "BassBoost";
  
  private BaseParameterListener mBaseParamListener;
  
  private OnParameterChangeListener mParamListener;
  
  private final Object mParamListenerLock;
  
  private boolean mStrengthSupported;
  
  public BassBoost(int paramInt1, int paramInt2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_BASS_BOOST, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    boolean bool = false;
    this.mStrengthSupported = false;
    this.mParamListener = null;
    this.mBaseParamListener = null;
    this.mParamListenerLock = new Object();
    if (paramInt2 == 0)
      Log.w("BassBoost", "WARNING: attaching a BassBoost to global output mix is deprecated!"); 
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(0, arrayOfInt));
    if (arrayOfInt[0] != 0)
      bool = true; 
    this.mStrengthSupported = bool;
  }
  
  public boolean getStrengthSupported() {
    return this.mStrengthSupported;
  }
  
  public void setStrength(short paramShort) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(1, paramShort));
  }
  
  public short getRoundedStrength() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(1, arrayOfShort));
    return arrayOfShort[0];
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final BassBoost this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      param1AudioEffect = null;
      synchronized (BassBoost.this.mParamListenerLock) {
        BassBoost.OnParameterChangeListener onParameterChangeListener;
        if (BassBoost.this.mParamListener != null)
          onParameterChangeListener = BassBoost.this.mParamListener; 
        if (onParameterChangeListener != null) {
          int i = -1;
          short s = -1;
          if (param1ArrayOfbyte1.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 2)
            s = AudioEffect.byteArrayToShort(param1ArrayOfbyte2, 0); 
          if (i != -1 && s != -1)
            onParameterChangeListener.onParameterChange(BassBoost.this, param1Int, i, s); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        this.mParamListener = paramOnParameterChangeListener;
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      return;
    } 
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(BassBoost param1BassBoost, int param1Int1, int param1Int2, short param1Short);
  }
  
  class Settings {
    public short strength;
    
    public Settings() {}
    
    public Settings(BassBoost this$0) {
      String str;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      stringTokenizer.countTokens();
      if (stringTokenizer.countTokens() == 3) {
        str = stringTokenizer.nextToken();
        if (str.equals("BassBoost"))
          try {
            String str1 = stringTokenizer.nextToken();
            str = str1;
            if (str1.equals("strength")) {
              str = str1;
              this.strength = Short.parseShort(stringTokenizer.nextToken());
              return;
            } 
            str = str1;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            str = str1;
            StringBuilder stringBuilder2 = new StringBuilder();
            str = str1;
            this();
            str = str1;
            stringBuilder2.append("invalid key name: ");
            str = str1;
            stringBuilder2.append(str1);
            str = str1;
            this(stringBuilder2.toString());
            str = str1;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("invalid value for key: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for BassBoost: ");
        stringBuilder1.append(str);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("settings: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("BassBoost;strength=");
      short s = this.strength;
      stringBuilder.append(Short.toString(s));
      return new String(stringBuilder.toString());
    }
  }
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    Settings settings = new Settings();
    short[] arrayOfShort = new short[1];
    checkStatus(getParameter(1, arrayOfShort));
    settings.strength = arrayOfShort[0];
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(1, paramSettings.strength));
  }
}
