package android.media.audiofx;

import android.util.Log;

public class AcousticEchoCanceler extends AudioEffect {
  private static final String TAG = "AcousticEchoCanceler";
  
  public static boolean isAvailable() {
    return AudioEffect.isEffectTypeAvailable(AudioEffect.EFFECT_TYPE_AEC);
  }
  
  public static AcousticEchoCanceler create(int paramInt) {
    unsupportedOperationException = null;
    AcousticEchoCanceler acousticEchoCanceler = null;
    try {
      AcousticEchoCanceler acousticEchoCanceler1 = new AcousticEchoCanceler();
      this(paramInt);
      acousticEchoCanceler = acousticEchoCanceler1;
    } catch (IllegalArgumentException illegalArgumentException) {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("not implemented on this device");
      stringBuilder.append((Object)null);
      Log.w("AcousticEchoCanceler", stringBuilder.toString());
    } catch (UnsupportedOperationException unsupportedOperationException) {
      Log.w("AcousticEchoCanceler", "not enough resources");
    } catch (RuntimeException runtimeException) {
      Log.w("AcousticEchoCanceler", "not enough memory");
      runtimeException = unsupportedOperationException;
    } 
    return (AcousticEchoCanceler)runtimeException;
  }
  
  private AcousticEchoCanceler(int paramInt) throws IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_AEC, EFFECT_TYPE_NULL, 0, paramInt);
  }
}
