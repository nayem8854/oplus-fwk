package android.media.audiofx;

import android.util.Log;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.StringTokenizer;

public final class DynamicsProcessing extends AudioEffect {
  private static final int CHANNEL_COUNT_MAX = 32;
  
  private static final float CHANNEL_DEFAULT_INPUT_GAIN = 0.0F;
  
  private static final int CONFIG_DEFAULT_MBC_BANDS = 6;
  
  private static final int CONFIG_DEFAULT_POSTEQ_BANDS = 6;
  
  private static final int CONFIG_DEFAULT_PREEQ_BANDS = 6;
  
  private static final boolean CONFIG_DEFAULT_USE_LIMITER = true;
  
  private static final boolean CONFIG_DEFAULT_USE_MBC = true;
  
  private static final boolean CONFIG_DEFAULT_USE_POSTEQ = true;
  
  private static final boolean CONFIG_DEFAULT_USE_PREEQ = true;
  
  private static final int CONFIG_DEFAULT_VARIANT = 0;
  
  private static final float CONFIG_PREFERRED_FRAME_DURATION_MS = 10.0F;
  
  private static final float DEFAULT_MAX_FREQUENCY = 20000.0F;
  
  private static final float DEFAULT_MIN_FREQUENCY = 220.0F;
  
  private static final float EQ_DEFAULT_GAIN = 0.0F;
  
  private static final float LIMITER_DEFAULT_ATTACK_TIME = 1.0F;
  
  private static final boolean LIMITER_DEFAULT_ENABLED = true;
  
  private static final int LIMITER_DEFAULT_LINK_GROUP = 0;
  
  private static final float LIMITER_DEFAULT_POST_GAIN = 0.0F;
  
  private static final float LIMITER_DEFAULT_RATIO = 10.0F;
  
  private static final float LIMITER_DEFAULT_RELEASE_TIME = 60.0F;
  
  private static final float LIMITER_DEFAULT_THRESHOLD = -2.0F;
  
  private static final float MBC_DEFAULT_ATTACK_TIME = 3.0F;
  
  private static final boolean MBC_DEFAULT_ENABLED = true;
  
  private static final float MBC_DEFAULT_EXPANDER_RATIO = 1.0F;
  
  private static final float MBC_DEFAULT_KNEE_WIDTH = 0.0F;
  
  private static final float MBC_DEFAULT_NOISE_GATE_THRESHOLD = -90.0F;
  
  private static final float MBC_DEFAULT_POST_GAIN = 0.0F;
  
  private static final float MBC_DEFAULT_PRE_GAIN = 0.0F;
  
  private static final float MBC_DEFAULT_RATIO = 1.0F;
  
  private static final float MBC_DEFAULT_RELEASE_TIME = 80.0F;
  
  private static final float MBC_DEFAULT_THRESHOLD = -45.0F;
  
  private static final int PARAM_ENGINE_ARCHITECTURE = 48;
  
  private static final int PARAM_GET_CHANNEL_COUNT = 16;
  
  private static final int PARAM_INPUT_GAIN = 32;
  
  private static final int PARAM_LIMITER = 112;
  
  private static final int PARAM_MBC = 80;
  
  private static final int PARAM_MBC_BAND = 85;
  
  private static final int PARAM_POST_EQ = 96;
  
  private static final int PARAM_POST_EQ_BAND = 101;
  
  private static final int PARAM_PRE_EQ = 64;
  
  private static final int PARAM_PRE_EQ_BAND = 69;
  
  private static final boolean POSTEQ_DEFAULT_ENABLED = true;
  
  private static final boolean PREEQ_DEFAULT_ENABLED = true;
  
  private static final String TAG = "DynamicsProcessing";
  
  public static final int VARIANT_FAVOR_FREQUENCY_RESOLUTION = 0;
  
  public static final int VARIANT_FAVOR_TIME_RESOLUTION = 1;
  
  private static final float mMaxFreqLog;
  
  public DynamicsProcessing(int paramInt) {
    this(0, paramInt);
  }
  
  public DynamicsProcessing(int paramInt1, int paramInt2) {
    this(paramInt1, paramInt2, (Config)null);
  }
  
  public DynamicsProcessing(int paramInt1, int paramInt2, Config paramConfig) {
    super(EFFECT_TYPE_DYNAMICS_PROCESSING, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    Config config;
    this.mChannelCount = 0;
    this.mParamListener = null;
    this.mBaseParamListener = null;
    this.mParamListenerLock = new Object();
    if (paramInt2 == 0)
      Log.w("DynamicsProcessing", "WARNING: attaching a DynamicsProcessing to global output mix isdeprecated!"); 
    this.mChannelCount = paramInt1 = getChannelCount();
    if (paramConfig == null) {
      Config.Builder builder = new Config.Builder(0, paramInt1, true, 6, true, 6, true, 6, true);
      config = builder.build();
    } else {
      config = new Config(config);
    } 
    int i = config.getVariant();
    float f = config.getPreferredFrameDuration();
    boolean bool1 = config.isPreEqInUse();
    paramInt1 = config.getPreEqBandCount();
    boolean bool2 = config.isMbcInUse();
    paramInt2 = config.getMbcBandCount();
    boolean bool3 = config.isPostEqInUse();
    int j = config.getPostEqBandCount();
    boolean bool4 = config.isLimiterInUse();
    setEngineArchitecture(i, f, bool1, paramInt1, bool2, paramInt2, bool3, j, bool4);
    for (paramInt1 = 0; paramInt1 < this.mChannelCount; paramInt1++)
      updateEngineChannelByChannelIndex(paramInt1, config.getChannelByChannelIndex(paramInt1)); 
  }
  
  public Config getConfig() {
    boolean bool1, bool2, bool3, bool4;
    Integer integer = Integer.valueOf(0);
    Number[] arrayOfNumber = new Number[9];
    arrayOfNumber[0] = integer;
    arrayOfNumber[1] = Float.valueOf(0.0F);
    arrayOfNumber[2] = integer;
    arrayOfNumber[3] = integer;
    arrayOfNumber[4] = integer;
    arrayOfNumber[5] = integer;
    arrayOfNumber[6] = integer;
    arrayOfNumber[7] = integer;
    arrayOfNumber[8] = integer;
    byte[] arrayOfByte2 = numberArrayToByteArray(new Number[] { Integer.valueOf(48) });
    byte[] arrayOfByte1 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte2, arrayOfByte1);
    byteArrayToNumberArray(arrayOfByte1, arrayOfNumber);
    Number number1 = arrayOfNumber[0];
    int i = number1.intValue(), j = this.mChannelCount;
    number1 = arrayOfNumber[2];
    if (number1.intValue() > 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    number1 = arrayOfNumber[3];
    int k = number1.intValue();
    number1 = arrayOfNumber[4];
    if (number1.intValue() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    number1 = arrayOfNumber[5];
    int m = number1.intValue();
    number1 = arrayOfNumber[6];
    if (number1.intValue() > 0) {
      bool3 = true;
    } else {
      bool3 = false;
    } 
    number1 = arrayOfNumber[7];
    int n = number1.intValue();
    number1 = arrayOfNumber[8];
    if (number1.intValue() > 0) {
      bool4 = true;
    } else {
      bool4 = false;
    } 
    Config.Builder builder1 = new Config.Builder(i, j, bool1, k, bool2, m, bool3, n, bool4);
    Number number2 = arrayOfNumber[1];
    Config.Builder builder2 = builder1.setPreferredFrameDuration(number2.floatValue());
    Config config = builder2.build();
    for (i = 0; i < this.mChannelCount; i++) {
      Channel channel = queryEngineByChannelIndex(i);
      config.setChannelTo(i, channel);
    } 
    return config;
  }
  
  private static final float mMinFreqLog = (float)Math.log10(220.0D);
  
  private BaseParameterListener mBaseParamListener;
  
  private int mChannelCount;
  
  private OnParameterChangeListener mParamListener;
  
  private final Object mParamListenerLock;
  
  static {
    mMaxFreqLog = (float)Math.log10(20000.0D);
  }
  
  class Stage {
    private boolean mEnabled;
    
    private boolean mInUse;
    
    public Stage(DynamicsProcessing this$0, boolean param1Boolean1) {
      this.mInUse = this$0;
      this.mEnabled = param1Boolean1;
    }
    
    public boolean isEnabled() {
      return this.mEnabled;
    }
    
    public void setEnabled(boolean param1Boolean) {
      this.mEnabled = param1Boolean;
    }
    
    public boolean isInUse() {
      return this.mInUse;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(String.format(" Stage InUse: %b\n", new Object[] { Boolean.valueOf(isInUse()) }));
      if (isInUse())
        stringBuilder.append(String.format(" Stage Enabled: %b\n", new Object[] { Boolean.valueOf(this.mEnabled) })); 
      return stringBuilder.toString();
    }
  }
  
  public static class BandStage extends Stage {
    private int mBandCount;
    
    public BandStage(boolean param1Boolean1, boolean param1Boolean2, int param1Int) {
      super(param1Boolean1, param1Boolean2);
      if (!isInUse())
        param1Int = 0; 
      this.mBandCount = param1Int;
    }
    
    public int getBandCount() {
      return this.mBandCount;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      if (isInUse())
        stringBuilder.append(String.format(" Band Count: %d\n", new Object[] { Integer.valueOf(this.mBandCount) })); 
      return stringBuilder.toString();
    }
  }
  
  class BandBase {
    private float mCutoffFrequency;
    
    private boolean mEnabled;
    
    public BandBase(DynamicsProcessing this$0, float param1Float) {
      this.mEnabled = this$0;
      this.mCutoffFrequency = param1Float;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(String.format(" Enabled: %b\n", new Object[] { Boolean.valueOf(this.mEnabled) }));
      stringBuilder.append(String.format(" CutoffFrequency: %f\n", new Object[] { Float.valueOf(this.mCutoffFrequency) }));
      return stringBuilder.toString();
    }
    
    public boolean isEnabled() {
      return this.mEnabled;
    }
    
    public void setEnabled(boolean param1Boolean) {
      this.mEnabled = param1Boolean;
    }
    
    public float getCutoffFrequency() {
      return this.mCutoffFrequency;
    }
    
    public void setCutoffFrequency(float param1Float) {
      this.mCutoffFrequency = param1Float;
    }
  }
  
  public static final class EqBand extends BandBase {
    private float mGain;
    
    public EqBand(boolean param1Boolean, float param1Float1, float param1Float2) {
      super(param1Boolean, param1Float1);
      this.mGain = param1Float2;
    }
    
    public EqBand(EqBand param1EqBand) {
      super(param1EqBand.isEnabled(), param1EqBand.getCutoffFrequency());
      this.mGain = param1EqBand.mGain;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      stringBuilder.append(String.format(" Gain: %f\n", new Object[] { Float.valueOf(this.mGain) }));
      return stringBuilder.toString();
    }
    
    public float getGain() {
      return this.mGain;
    }
    
    public void setGain(float param1Float) {
      this.mGain = param1Float;
    }
  }
  
  public static final class MbcBand extends BandBase {
    private float mAttackTime;
    
    private float mExpanderRatio;
    
    private float mKneeWidth;
    
    private float mNoiseGateThreshold;
    
    private float mPostGain;
    
    private float mPreGain;
    
    private float mRatio;
    
    private float mReleaseTime;
    
    private float mThreshold;
    
    public MbcBand(boolean param1Boolean, float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5, float param1Float6, float param1Float7, float param1Float8, float param1Float9, float param1Float10) {
      super(param1Boolean, param1Float1);
      this.mAttackTime = param1Float2;
      this.mReleaseTime = param1Float3;
      this.mRatio = param1Float4;
      this.mThreshold = param1Float5;
      this.mKneeWidth = param1Float6;
      this.mNoiseGateThreshold = param1Float7;
      this.mExpanderRatio = param1Float8;
      this.mPreGain = param1Float9;
      this.mPostGain = param1Float10;
    }
    
    public MbcBand(MbcBand param1MbcBand) {
      super(param1MbcBand.isEnabled(), param1MbcBand.getCutoffFrequency());
      this.mAttackTime = param1MbcBand.mAttackTime;
      this.mReleaseTime = param1MbcBand.mReleaseTime;
      this.mRatio = param1MbcBand.mRatio;
      this.mThreshold = param1MbcBand.mThreshold;
      this.mKneeWidth = param1MbcBand.mKneeWidth;
      this.mNoiseGateThreshold = param1MbcBand.mNoiseGateThreshold;
      this.mExpanderRatio = param1MbcBand.mExpanderRatio;
      this.mPreGain = param1MbcBand.mPreGain;
      this.mPostGain = param1MbcBand.mPostGain;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      stringBuilder.append(String.format(" AttackTime: %f (ms)\n", new Object[] { Float.valueOf(this.mAttackTime) }));
      stringBuilder.append(String.format(" ReleaseTime: %f (ms)\n", new Object[] { Float.valueOf(this.mReleaseTime) }));
      stringBuilder.append(String.format(" Ratio: 1:%f\n", new Object[] { Float.valueOf(this.mRatio) }));
      stringBuilder.append(String.format(" Threshold: %f (dB)\n", new Object[] { Float.valueOf(this.mThreshold) }));
      stringBuilder.append(String.format(" NoiseGateThreshold: %f(dB)\n", new Object[] { Float.valueOf(this.mNoiseGateThreshold) }));
      stringBuilder.append(String.format(" ExpanderRatio: %f:1\n", new Object[] { Float.valueOf(this.mExpanderRatio) }));
      stringBuilder.append(String.format(" PreGain: %f (dB)\n", new Object[] { Float.valueOf(this.mPreGain) }));
      stringBuilder.append(String.format(" PostGain: %f (dB)\n", new Object[] { Float.valueOf(this.mPostGain) }));
      return stringBuilder.toString();
    }
    
    public float getAttackTime() {
      return this.mAttackTime;
    }
    
    public void setAttackTime(float param1Float) {
      this.mAttackTime = param1Float;
    }
    
    public float getReleaseTime() {
      return this.mReleaseTime;
    }
    
    public void setReleaseTime(float param1Float) {
      this.mReleaseTime = param1Float;
    }
    
    public float getRatio() {
      return this.mRatio;
    }
    
    public void setRatio(float param1Float) {
      this.mRatio = param1Float;
    }
    
    public float getThreshold() {
      return this.mThreshold;
    }
    
    public void setThreshold(float param1Float) {
      this.mThreshold = param1Float;
    }
    
    public float getKneeWidth() {
      return this.mKneeWidth;
    }
    
    public void setKneeWidth(float param1Float) {
      this.mKneeWidth = param1Float;
    }
    
    public float getNoiseGateThreshold() {
      return this.mNoiseGateThreshold;
    }
    
    public void setNoiseGateThreshold(float param1Float) {
      this.mNoiseGateThreshold = param1Float;
    }
    
    public float getExpanderRatio() {
      return this.mExpanderRatio;
    }
    
    public void setExpanderRatio(float param1Float) {
      this.mExpanderRatio = param1Float;
    }
    
    public float getPreGain() {
      return this.mPreGain;
    }
    
    public void setPreGain(float param1Float) {
      this.mPreGain = param1Float;
    }
    
    public float getPostGain() {
      return this.mPostGain;
    }
    
    public void setPostGain(float param1Float) {
      this.mPostGain = param1Float;
    }
  }
  
  class Eq extends BandStage {
    private final DynamicsProcessing.EqBand[] mBands;
    
    public Eq(DynamicsProcessing this$0, boolean param1Boolean1, int param1Int) {
      super(this$0, param1Boolean1, param1Int);
      if (isInUse()) {
        this.mBands = new DynamicsProcessing.EqBand[param1Int];
        for (byte b = 0; b < param1Int; b++) {
          float f = 20000.0F;
          if (param1Int > 1) {
            f = DynamicsProcessing.mMinFreqLog;
            float f1 = b;
            double d = (f + f1 * (DynamicsProcessing.mMaxFreqLog - DynamicsProcessing.mMinFreqLog) / (param1Int - 1));
            f = (float)Math.pow(10.0D, d);
          } 
          this.mBands[b] = new DynamicsProcessing.EqBand(true, f, 0.0F);
        } 
      } else {
        this.mBands = null;
      } 
    }
    
    public Eq(DynamicsProcessing this$0) {
      super(this$0.isInUse(), this$0.isEnabled(), this$0.getBandCount());
      if (isInUse()) {
        this.mBands = new DynamicsProcessing.EqBand[((Eq)this$0).mBands.length];
        byte b = 0;
        while (true) {
          DynamicsProcessing.EqBand[] arrayOfEqBand = this.mBands;
          if (b < arrayOfEqBand.length) {
            arrayOfEqBand[b] = new DynamicsProcessing.EqBand(((Eq)this$0).mBands[b]);
            b++;
            continue;
          } 
          break;
        } 
      } else {
        this.mBands = null;
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      if (isInUse()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("--->EqBands: ");
        stringBuilder1.append(this.mBands.length);
        stringBuilder1.append("\n");
        stringBuilder.append(stringBuilder1.toString());
        for (byte b = 0; b < this.mBands.length; b++) {
          stringBuilder.append(String.format("  Band %d\n", new Object[] { Integer.valueOf(b) }));
          stringBuilder.append(this.mBands[b].toString());
        } 
      } 
      return stringBuilder.toString();
    }
    
    private void checkBand(int param1Int) {
      DynamicsProcessing.EqBand[] arrayOfEqBand = this.mBands;
      if (arrayOfEqBand != null && param1Int >= 0 && param1Int < arrayOfEqBand.length)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("band index ");
      stringBuilder.append(param1Int);
      stringBuilder.append(" out of bounds");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setBand(int param1Int, DynamicsProcessing.EqBand param1EqBand) {
      checkBand(param1Int);
      this.mBands[param1Int] = new DynamicsProcessing.EqBand(param1EqBand);
    }
    
    public DynamicsProcessing.EqBand getBand(int param1Int) {
      checkBand(param1Int);
      return this.mBands[param1Int];
    }
  }
  
  class Mbc extends BandStage {
    private final DynamicsProcessing.MbcBand[] mBands;
    
    public Mbc(DynamicsProcessing this$0, boolean param1Boolean1, int param1Int) {
      super(this$0, param1Boolean1, param1Int);
      if (isInUse()) {
        this.mBands = new DynamicsProcessing.MbcBand[param1Int];
        for (byte b = 0; b < param1Int; b++) {
          float f = 20000.0F;
          if (param1Int > 1) {
            f = DynamicsProcessing.mMinFreqLog;
            float f1 = b;
            double d = (f + f1 * (DynamicsProcessing.mMaxFreqLog - DynamicsProcessing.mMinFreqLog) / (param1Int - 1));
            f = (float)Math.pow(10.0D, d);
          } 
          this.mBands[b] = new DynamicsProcessing.MbcBand(true, f, 3.0F, 80.0F, 1.0F, -45.0F, 0.0F, -90.0F, 1.0F, 0.0F, 0.0F);
        } 
      } else {
        this.mBands = null;
      } 
    }
    
    public Mbc(DynamicsProcessing this$0) {
      super(this$0.isInUse(), this$0.isEnabled(), this$0.getBandCount());
      if (isInUse()) {
        this.mBands = new DynamicsProcessing.MbcBand[((Mbc)this$0).mBands.length];
        byte b = 0;
        while (true) {
          DynamicsProcessing.MbcBand[] arrayOfMbcBand = this.mBands;
          if (b < arrayOfMbcBand.length) {
            arrayOfMbcBand[b] = new DynamicsProcessing.MbcBand(((Mbc)this$0).mBands[b]);
            b++;
            continue;
          } 
          break;
        } 
      } else {
        this.mBands = null;
      } 
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      if (isInUse()) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("--->MbcBands: ");
        stringBuilder1.append(this.mBands.length);
        stringBuilder1.append("\n");
        stringBuilder.append(stringBuilder1.toString());
        for (byte b = 0; b < this.mBands.length; b++) {
          stringBuilder.append(String.format("  Band %d\n", new Object[] { Integer.valueOf(b) }));
          stringBuilder.append(this.mBands[b].toString());
        } 
      } 
      return stringBuilder.toString();
    }
    
    private void checkBand(int param1Int) {
      DynamicsProcessing.MbcBand[] arrayOfMbcBand = this.mBands;
      if (arrayOfMbcBand != null && param1Int >= 0 && param1Int < arrayOfMbcBand.length)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("band index ");
      stringBuilder.append(param1Int);
      stringBuilder.append(" out of bounds");
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setBand(int param1Int, DynamicsProcessing.MbcBand param1MbcBand) {
      checkBand(param1Int);
      this.mBands[param1Int] = new DynamicsProcessing.MbcBand(param1MbcBand);
    }
    
    public DynamicsProcessing.MbcBand getBand(int param1Int) {
      checkBand(param1Int);
      return this.mBands[param1Int];
    }
  }
  
  public static final class Limiter extends Stage {
    private float mAttackTime;
    
    private int mLinkGroup;
    
    private float mPostGain;
    
    private float mRatio;
    
    private float mReleaseTime;
    
    private float mThreshold;
    
    public Limiter(boolean param1Boolean1, boolean param1Boolean2, int param1Int, float param1Float1, float param1Float2, float param1Float3, float param1Float4, float param1Float5) {
      super(param1Boolean1, param1Boolean2);
      this.mLinkGroup = param1Int;
      this.mAttackTime = param1Float1;
      this.mReleaseTime = param1Float2;
      this.mRatio = param1Float3;
      this.mThreshold = param1Float4;
      this.mPostGain = param1Float5;
    }
    
    public Limiter(Limiter param1Limiter) {
      super(param1Limiter.isInUse(), param1Limiter.isEnabled());
      this.mLinkGroup = param1Limiter.mLinkGroup;
      this.mAttackTime = param1Limiter.mAttackTime;
      this.mReleaseTime = param1Limiter.mReleaseTime;
      this.mRatio = param1Limiter.mRatio;
      this.mThreshold = param1Limiter.mThreshold;
      this.mPostGain = param1Limiter.mPostGain;
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(super.toString());
      if (isInUse()) {
        stringBuilder.append(String.format(" LinkGroup: %d (group)\n", new Object[] { Integer.valueOf(this.mLinkGroup) }));
        stringBuilder.append(String.format(" AttackTime: %f (ms)\n", new Object[] { Float.valueOf(this.mAttackTime) }));
        stringBuilder.append(String.format(" ReleaseTime: %f (ms)\n", new Object[] { Float.valueOf(this.mReleaseTime) }));
        stringBuilder.append(String.format(" Ratio: 1:%f\n", new Object[] { Float.valueOf(this.mRatio) }));
        stringBuilder.append(String.format(" Threshold: %f (dB)\n", new Object[] { Float.valueOf(this.mThreshold) }));
        stringBuilder.append(String.format(" PostGain: %f (dB)\n", new Object[] { Float.valueOf(this.mPostGain) }));
      } 
      return stringBuilder.toString();
    }
    
    public int getLinkGroup() {
      return this.mLinkGroup;
    }
    
    public void setLinkGroup(int param1Int) {
      this.mLinkGroup = param1Int;
    }
    
    public float getAttackTime() {
      return this.mAttackTime;
    }
    
    public void setAttackTime(float param1Float) {
      this.mAttackTime = param1Float;
    }
    
    public float getReleaseTime() {
      return this.mReleaseTime;
    }
    
    public void setReleaseTime(float param1Float) {
      this.mReleaseTime = param1Float;
    }
    
    public float getRatio() {
      return this.mRatio;
    }
    
    public void setRatio(float param1Float) {
      this.mRatio = param1Float;
    }
    
    public float getThreshold() {
      return this.mThreshold;
    }
    
    public void setThreshold(float param1Float) {
      this.mThreshold = param1Float;
    }
    
    public float getPostGain() {
      return this.mPostGain;
    }
    
    public void setPostGain(float param1Float) {
      this.mPostGain = param1Float;
    }
  }
  
  class Channel {
    private float mInputGain;
    
    private DynamicsProcessing.Limiter mLimiter;
    
    private DynamicsProcessing.Mbc mMbc;
    
    private DynamicsProcessing.Eq mPostEq;
    
    private DynamicsProcessing.Eq mPreEq;
    
    public Channel(DynamicsProcessing this$0, boolean param1Boolean1, int param1Int1, boolean param1Boolean2, int param1Int2, boolean param1Boolean3, int param1Int3, boolean param1Boolean4) {
      this.mInputGain = this$0;
      this.mPreEq = new DynamicsProcessing.Eq(param1Boolean1, true, param1Int1);
      this.mMbc = new DynamicsProcessing.Mbc(param1Boolean2, true, param1Int2);
      this.mPostEq = new DynamicsProcessing.Eq(param1Boolean3, true, param1Int3);
      this.mLimiter = new DynamicsProcessing.Limiter(param1Boolean4, true, 0, 1.0F, 60.0F, 10.0F, -2.0F, 0.0F);
    }
    
    public Channel(DynamicsProcessing this$0) {
      this.mInputGain = ((Channel)this$0).mInputGain;
      this.mPreEq = new DynamicsProcessing.Eq(((Channel)this$0).mPreEq);
      this.mMbc = new DynamicsProcessing.Mbc(((Channel)this$0).mMbc);
      this.mPostEq = new DynamicsProcessing.Eq(((Channel)this$0).mPostEq);
      this.mLimiter = new DynamicsProcessing.Limiter(((Channel)this$0).mLimiter);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(String.format(" InputGain: %f\n", new Object[] { Float.valueOf(this.mInputGain) }));
      stringBuilder.append("-->PreEq\n");
      stringBuilder.append(this.mPreEq.toString());
      stringBuilder.append("-->MBC\n");
      stringBuilder.append(this.mMbc.toString());
      stringBuilder.append("-->PostEq\n");
      stringBuilder.append(this.mPostEq.toString());
      stringBuilder.append("-->Limiter\n");
      stringBuilder.append(this.mLimiter.toString());
      return stringBuilder.toString();
    }
    
    public float getInputGain() {
      return this.mInputGain;
    }
    
    public void setInputGain(float param1Float) {
      this.mInputGain = param1Float;
    }
    
    public DynamicsProcessing.Eq getPreEq() {
      return this.mPreEq;
    }
    
    public void setPreEq(DynamicsProcessing.Eq param1Eq) {
      if (param1Eq.getBandCount() == this.mPreEq.getBandCount()) {
        this.mPreEq = new DynamicsProcessing.Eq(param1Eq);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PreEqBandCount changed from ");
      DynamicsProcessing.Eq eq = this.mPreEq;
      stringBuilder.append(eq.getBandCount());
      stringBuilder.append(" to ");
      stringBuilder.append(param1Eq.getBandCount());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public DynamicsProcessing.EqBand getPreEqBand(int param1Int) {
      return this.mPreEq.getBand(param1Int);
    }
    
    public void setPreEqBand(int param1Int, DynamicsProcessing.EqBand param1EqBand) {
      this.mPreEq.setBand(param1Int, param1EqBand);
    }
    
    public DynamicsProcessing.Mbc getMbc() {
      return this.mMbc;
    }
    
    public void setMbc(DynamicsProcessing.Mbc param1Mbc) {
      if (param1Mbc.getBandCount() == this.mMbc.getBandCount()) {
        this.mMbc = new DynamicsProcessing.Mbc(param1Mbc);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MbcBandCount changed from ");
      DynamicsProcessing.Mbc mbc = this.mMbc;
      stringBuilder.append(mbc.getBandCount());
      stringBuilder.append(" to ");
      stringBuilder.append(param1Mbc.getBandCount());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public DynamicsProcessing.MbcBand getMbcBand(int param1Int) {
      return this.mMbc.getBand(param1Int);
    }
    
    public void setMbcBand(int param1Int, DynamicsProcessing.MbcBand param1MbcBand) {
      this.mMbc.setBand(param1Int, param1MbcBand);
    }
    
    public DynamicsProcessing.Eq getPostEq() {
      return this.mPostEq;
    }
    
    public void setPostEq(DynamicsProcessing.Eq param1Eq) {
      if (param1Eq.getBandCount() == this.mPostEq.getBandCount()) {
        this.mPostEq = new DynamicsProcessing.Eq(param1Eq);
        return;
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("PostEqBandCount changed from ");
      DynamicsProcessing.Eq eq = this.mPostEq;
      stringBuilder.append(eq.getBandCount());
      stringBuilder.append(" to ");
      stringBuilder.append(param1Eq.getBandCount());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public DynamicsProcessing.EqBand getPostEqBand(int param1Int) {
      return this.mPostEq.getBand(param1Int);
    }
    
    public void setPostEqBand(int param1Int, DynamicsProcessing.EqBand param1EqBand) {
      this.mPostEq.setBand(param1Int, param1EqBand);
    }
    
    public DynamicsProcessing.Limiter getLimiter() {
      return this.mLimiter;
    }
    
    public void setLimiter(DynamicsProcessing.Limiter param1Limiter) {
      this.mLimiter = new DynamicsProcessing.Limiter(param1Limiter);
    }
  }
  
  class Config {
    private final DynamicsProcessing.Channel[] mChannel;
    
    private final int mChannelCount;
    
    private final boolean mLimiterInUse;
    
    private final int mMbcBandCount;
    
    private final boolean mMbcInUse;
    
    private final int mPostEqBandCount;
    
    private final boolean mPostEqInUse;
    
    private final int mPreEqBandCount;
    
    private final boolean mPreEqInUse;
    
    private final float mPreferredFrameDuration;
    
    private final int mVariant;
    
    public Config(DynamicsProcessing this$0, float param1Float, int param1Int1, boolean param1Boolean1, int param1Int2, boolean param1Boolean2, int param1Int3, boolean param1Boolean3, int param1Int4, boolean param1Boolean4, DynamicsProcessing.Channel[] param1ArrayOfChannel) {
      this.mVariant = this$0;
      this.mPreferredFrameDuration = param1Float;
      this.mChannelCount = param1Int1;
      this.mPreEqInUse = param1Boolean1;
      this.mPreEqBandCount = param1Int2;
      this.mMbcInUse = param1Boolean2;
      this.mMbcBandCount = param1Int3;
      this.mPostEqInUse = param1Boolean3;
      this.mPostEqBandCount = param1Int4;
      this.mLimiterInUse = param1Boolean4;
      this.mChannel = new DynamicsProcessing.Channel[param1Int1];
      for (byte b = 0; b < this.mChannelCount; b++) {
        if (b < param1ArrayOfChannel.length)
          this.mChannel[b] = new DynamicsProcessing.Channel(param1ArrayOfChannel[b]); 
      } 
    }
    
    public Config(DynamicsProcessing this$0, Config param1Config) {
      this.mVariant = param1Config.mVariant;
      this.mPreferredFrameDuration = param1Config.mPreferredFrameDuration;
      int i = param1Config.mChannelCount;
      this.mPreEqInUse = param1Config.mPreEqInUse;
      this.mPreEqBandCount = param1Config.mPreEqBandCount;
      this.mMbcInUse = param1Config.mMbcInUse;
      this.mMbcBandCount = param1Config.mMbcBandCount;
      this.mPostEqInUse = param1Config.mPostEqInUse;
      this.mPostEqBandCount = param1Config.mPostEqBandCount;
      this.mLimiterInUse = param1Config.mLimiterInUse;
      if (i == param1Config.mChannel.length) {
        if (this$0 >= true) {
          this.mChannel = new DynamicsProcessing.Channel[this$0];
          for (i = 0; i < this$0; i++) {
            int j = this.mChannelCount;
            if (i < j) {
              this.mChannel[i] = new DynamicsProcessing.Channel(param1Config.mChannel[i]);
            } else {
              this.mChannel[i] = new DynamicsProcessing.Channel(param1Config.mChannel[j - 1]);
            } 
          } 
          return;
        } 
        throw new IllegalArgumentException("channel resizing less than 1 not allowed");
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("configuration channel counts differ ");
      stringBuilder.append(this.mChannelCount);
      stringBuilder.append(" !=");
      stringBuilder.append(param1Config.mChannel.length);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public Config() {
      this(((Config)this$0).mChannelCount, (Config)this$0);
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append(String.format("Variant: %d\n", new Object[] { Integer.valueOf(this.mVariant) }));
      stringBuilder.append(String.format("PreferredFrameDuration: %f\n", new Object[] { Float.valueOf(this.mPreferredFrameDuration) }));
      stringBuilder.append(String.format("ChannelCount: %d\n", new Object[] { Integer.valueOf(this.mChannelCount) }));
      boolean bool = this.mPreEqInUse;
      int i = this.mPreEqBandCount;
      stringBuilder.append(String.format("PreEq inUse: %b, bandCount:%d\n", new Object[] { Boolean.valueOf(bool), Integer.valueOf(i) }));
      stringBuilder.append(String.format("Mbc inUse: %b, bandCount: %d\n", new Object[] { Boolean.valueOf(this.mMbcInUse), Integer.valueOf(this.mMbcBandCount) }));
      bool = this.mPostEqInUse;
      i = this.mPostEqBandCount;
      stringBuilder.append(String.format("PostEq inUse: %b, bandCount: %d\n", new Object[] { Boolean.valueOf(bool), Integer.valueOf(i) }));
      stringBuilder.append(String.format("Limiter inUse: %b\n", new Object[] { Boolean.valueOf(this.mLimiterInUse) }));
      for (i = 0; i < this.mChannel.length; i++) {
        stringBuilder.append(String.format("==Channel %d\n", new Object[] { Integer.valueOf(i) }));
        stringBuilder.append(this.mChannel[i].toString());
      } 
      return stringBuilder.toString();
    }
    
    private void checkChannel(int param1Int) {
      if (param1Int >= 0 && param1Int < this.mChannel.length)
        return; 
      throw new IllegalArgumentException("ChannelIndex out of bounds");
    }
    
    public int getVariant() {
      return this.mVariant;
    }
    
    public float getPreferredFrameDuration() {
      return this.mPreferredFrameDuration;
    }
    
    public boolean isPreEqInUse() {
      return this.mPreEqInUse;
    }
    
    public int getPreEqBandCount() {
      return this.mPreEqBandCount;
    }
    
    public boolean isMbcInUse() {
      return this.mMbcInUse;
    }
    
    public int getMbcBandCount() {
      return this.mMbcBandCount;
    }
    
    public boolean isPostEqInUse() {
      return this.mPostEqInUse;
    }
    
    public int getPostEqBandCount() {
      return this.mPostEqBandCount;
    }
    
    public boolean isLimiterInUse() {
      return this.mLimiterInUse;
    }
    
    public DynamicsProcessing.Channel getChannelByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int];
    }
    
    public void setChannelTo(int param1Int, DynamicsProcessing.Channel param1Channel) {
      checkChannel(param1Int);
      if (this.mMbcBandCount == param1Channel.getMbc().getBandCount()) {
        if (this.mPreEqBandCount == param1Channel.getPreEq().getBandCount()) {
          if (this.mPostEqBandCount == param1Channel.getPostEq().getBandCount()) {
            this.mChannel[param1Int] = new DynamicsProcessing.Channel(param1Channel);
            return;
          } 
          StringBuilder stringBuilder2 = new StringBuilder();
          stringBuilder2.append("PostEqBandCount changed from ");
          stringBuilder2.append(this.mPostEqBandCount);
          stringBuilder2.append(" to ");
          stringBuilder2.append(param1Channel.getPostEq().getBandCount());
          throw new IllegalArgumentException(stringBuilder2.toString());
        } 
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("PreEqBandCount changed from ");
        stringBuilder1.append(this.mPreEqBandCount);
        stringBuilder1.append(" to ");
        stringBuilder1.append(param1Channel.getPreEq().getBandCount());
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("MbcBandCount changed from ");
      stringBuilder.append(this.mMbcBandCount);
      stringBuilder.append(" to ");
      stringBuilder.append(param1Channel.getPreEq().getBandCount());
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public void setAllChannelsTo(DynamicsProcessing.Channel param1Channel) {
      for (byte b = 0; b < this.mChannel.length; b++)
        setChannelTo(b, param1Channel); 
    }
    
    public float getInputGainByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int].getInputGain();
    }
    
    public void setInputGainByChannelIndex(int param1Int, float param1Float) {
      checkChannel(param1Int);
      this.mChannel[param1Int].setInputGain(param1Float);
    }
    
    public void setInputGainAllChannelsTo(float param1Float) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setInputGain(param1Float);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.Eq getPreEqByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int].getPreEq();
    }
    
    public void setPreEqByChannelIndex(int param1Int, DynamicsProcessing.Eq param1Eq) {
      checkChannel(param1Int);
      this.mChannel[param1Int].setPreEq(param1Eq);
    }
    
    public void setPreEqAllChannelsTo(DynamicsProcessing.Eq param1Eq) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setPreEq(param1Eq);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.EqBand getPreEqBandByChannelIndex(int param1Int1, int param1Int2) {
      checkChannel(param1Int1);
      return this.mChannel[param1Int1].getPreEqBand(param1Int2);
    }
    
    public void setPreEqBandByChannelIndex(int param1Int1, int param1Int2, DynamicsProcessing.EqBand param1EqBand) {
      checkChannel(param1Int1);
      this.mChannel[param1Int1].setPreEqBand(param1Int2, param1EqBand);
    }
    
    public void setPreEqBandAllChannelsTo(int param1Int, DynamicsProcessing.EqBand param1EqBand) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setPreEqBand(param1Int, param1EqBand);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.Mbc getMbcByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int].getMbc();
    }
    
    public void setMbcByChannelIndex(int param1Int, DynamicsProcessing.Mbc param1Mbc) {
      checkChannel(param1Int);
      this.mChannel[param1Int].setMbc(param1Mbc);
    }
    
    public void setMbcAllChannelsTo(DynamicsProcessing.Mbc param1Mbc) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setMbc(param1Mbc);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.MbcBand getMbcBandByChannelIndex(int param1Int1, int param1Int2) {
      checkChannel(param1Int1);
      return this.mChannel[param1Int1].getMbcBand(param1Int2);
    }
    
    public void setMbcBandByChannelIndex(int param1Int1, int param1Int2, DynamicsProcessing.MbcBand param1MbcBand) {
      checkChannel(param1Int1);
      this.mChannel[param1Int1].setMbcBand(param1Int2, param1MbcBand);
    }
    
    public void setMbcBandAllChannelsTo(int param1Int, DynamicsProcessing.MbcBand param1MbcBand) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setMbcBand(param1Int, param1MbcBand);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.Eq getPostEqByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int].getPostEq();
    }
    
    public void setPostEqByChannelIndex(int param1Int, DynamicsProcessing.Eq param1Eq) {
      checkChannel(param1Int);
      this.mChannel[param1Int].setPostEq(param1Eq);
    }
    
    public void setPostEqAllChannelsTo(DynamicsProcessing.Eq param1Eq) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setPostEq(param1Eq);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.EqBand getPostEqBandByChannelIndex(int param1Int1, int param1Int2) {
      checkChannel(param1Int1);
      return this.mChannel[param1Int1].getPostEqBand(param1Int2);
    }
    
    public void setPostEqBandByChannelIndex(int param1Int1, int param1Int2, DynamicsProcessing.EqBand param1EqBand) {
      checkChannel(param1Int1);
      this.mChannel[param1Int1].setPostEqBand(param1Int2, param1EqBand);
    }
    
    public void setPostEqBandAllChannelsTo(int param1Int, DynamicsProcessing.EqBand param1EqBand) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setPostEqBand(param1Int, param1EqBand);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public DynamicsProcessing.Limiter getLimiterByChannelIndex(int param1Int) {
      checkChannel(param1Int);
      return this.mChannel[param1Int].getLimiter();
    }
    
    public void setLimiterByChannelIndex(int param1Int, DynamicsProcessing.Limiter param1Limiter) {
      checkChannel(param1Int);
      this.mChannel[param1Int].setLimiter(param1Limiter);
    }
    
    public void setLimiterAllChannelsTo(DynamicsProcessing.Limiter param1Limiter) {
      byte b = 0;
      while (true) {
        DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
        if (b < arrayOfChannel.length) {
          arrayOfChannel[b].setLimiter(param1Limiter);
          b++;
          continue;
        } 
        break;
      } 
    }
    
    public static final class Builder {
      private DynamicsProcessing.Channel[] mChannel;
      
      private int mChannelCount;
      
      private boolean mLimiterInUse;
      
      private int mMbcBandCount;
      
      private boolean mMbcInUse;
      
      private int mPostEqBandCount;
      
      private boolean mPostEqInUse;
      
      private int mPreEqBandCount;
      
      private boolean mPreEqInUse;
      
      private float mPreferredFrameDuration = 10.0F;
      
      private int mVariant;
      
      public Builder(int param2Int1, int param2Int2, boolean param2Boolean1, int param2Int3, boolean param2Boolean2, int param2Int4, boolean param2Boolean3, int param2Int5, boolean param2Boolean4) {
        this.mVariant = param2Int1;
        this.mChannelCount = param2Int2;
        this.mPreEqInUse = param2Boolean1;
        this.mPreEqBandCount = param2Int3;
        this.mMbcInUse = param2Boolean2;
        this.mMbcBandCount = param2Int4;
        this.mPostEqInUse = param2Boolean3;
        this.mPostEqBandCount = param2Int5;
        this.mLimiterInUse = param2Boolean4;
        this.mChannel = new DynamicsProcessing.Channel[param2Int2];
        for (param2Int1 = 0; param2Int1 < this.mChannelCount; param2Int1++)
          this.mChannel[param2Int1] = new DynamicsProcessing.Channel(0.0F, this.mPreEqInUse, this.mPreEqBandCount, this.mMbcInUse, this.mMbcBandCount, this.mPostEqInUse, this.mPostEqBandCount, this.mLimiterInUse); 
      }
      
      private void checkChannel(int param2Int) {
        if (param2Int >= 0 && param2Int < this.mChannel.length)
          return; 
        throw new IllegalArgumentException("ChannelIndex out of bounds");
      }
      
      public Builder setPreferredFrameDuration(float param2Float) {
        if (param2Float >= 0.0F) {
          this.mPreferredFrameDuration = param2Float;
          return this;
        } 
        throw new IllegalArgumentException("Expected positive frameDuration");
      }
      
      public Builder setInputGainByChannelIndex(int param2Int, float param2Float) {
        checkChannel(param2Int);
        this.mChannel[param2Int].setInputGain(param2Float);
        return this;
      }
      
      public Builder setInputGainAllChannelsTo(float param2Float) {
        byte b = 0;
        while (true) {
          DynamicsProcessing.Channel[] arrayOfChannel = this.mChannel;
          if (b < arrayOfChannel.length) {
            arrayOfChannel[b].setInputGain(param2Float);
            b++;
            continue;
          } 
          break;
        } 
        return this;
      }
      
      public Builder setChannelTo(int param2Int, DynamicsProcessing.Channel param2Channel) {
        checkChannel(param2Int);
        if (this.mMbcBandCount == param2Channel.getMbc().getBandCount()) {
          if (this.mPreEqBandCount == param2Channel.getPreEq().getBandCount()) {
            if (this.mPostEqBandCount == param2Channel.getPostEq().getBandCount()) {
              this.mChannel[param2Int] = new DynamicsProcessing.Channel(param2Channel);
              return this;
            } 
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("PostEqBandCount changed from ");
            stringBuilder2.append(this.mPostEqBandCount);
            stringBuilder2.append(" to ");
            stringBuilder2.append(param2Channel.getPostEq().getBandCount());
            throw new IllegalArgumentException(stringBuilder2.toString());
          } 
          StringBuilder stringBuilder1 = new StringBuilder();
          stringBuilder1.append("PreEqBandCount changed from ");
          stringBuilder1.append(this.mPreEqBandCount);
          stringBuilder1.append(" to ");
          stringBuilder1.append(param2Channel.getPreEq().getBandCount());
          throw new IllegalArgumentException(stringBuilder1.toString());
        } 
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("MbcBandCount changed from ");
        stringBuilder.append(this.mMbcBandCount);
        stringBuilder.append(" to ");
        stringBuilder.append(param2Channel.getPreEq().getBandCount());
        throw new IllegalArgumentException(stringBuilder.toString());
      }
      
      public Builder setAllChannelsTo(DynamicsProcessing.Channel param2Channel) {
        for (byte b = 0; b < this.mChannel.length; b++)
          setChannelTo(b, param2Channel); 
        return this;
      }
      
      public Builder setPreEqByChannelIndex(int param2Int, DynamicsProcessing.Eq param2Eq) {
        checkChannel(param2Int);
        this.mChannel[param2Int].setPreEq(param2Eq);
        return this;
      }
      
      public Builder setPreEqAllChannelsTo(DynamicsProcessing.Eq param2Eq) {
        for (byte b = 0; b < this.mChannel.length; b++)
          setPreEqByChannelIndex(b, param2Eq); 
        return this;
      }
      
      public Builder setMbcByChannelIndex(int param2Int, DynamicsProcessing.Mbc param2Mbc) {
        checkChannel(param2Int);
        this.mChannel[param2Int].setMbc(param2Mbc);
        return this;
      }
      
      public Builder setMbcAllChannelsTo(DynamicsProcessing.Mbc param2Mbc) {
        for (byte b = 0; b < this.mChannel.length; b++)
          setMbcByChannelIndex(b, param2Mbc); 
        return this;
      }
      
      public Builder setPostEqByChannelIndex(int param2Int, DynamicsProcessing.Eq param2Eq) {
        checkChannel(param2Int);
        this.mChannel[param2Int].setPostEq(param2Eq);
        return this;
      }
      
      public Builder setPostEqAllChannelsTo(DynamicsProcessing.Eq param2Eq) {
        for (byte b = 0; b < this.mChannel.length; b++)
          setPostEqByChannelIndex(b, param2Eq); 
        return this;
      }
      
      public Builder setLimiterByChannelIndex(int param2Int, DynamicsProcessing.Limiter param2Limiter) {
        checkChannel(param2Int);
        this.mChannel[param2Int].setLimiter(param2Limiter);
        return this;
      }
      
      public Builder setLimiterAllChannelsTo(DynamicsProcessing.Limiter param2Limiter) {
        for (byte b = 0; b < this.mChannel.length; b++)
          setLimiterByChannelIndex(b, param2Limiter); 
        return this;
      }
      
      public DynamicsProcessing.Config build() {
        return new DynamicsProcessing.Config(this.mPreferredFrameDuration, this.mChannelCount, this.mPreEqInUse, this.mPreEqBandCount, this.mMbcInUse, this.mMbcBandCount, this.mPostEqInUse, this.mPostEqBandCount, this.mLimiterInUse, this.mChannel);
      }
    }
  }
  
  public Channel getChannelByChannelIndex(int paramInt) {
    return queryEngineByChannelIndex(paramInt);
  }
  
  public void setChannelTo(int paramInt, Channel paramChannel) {
    updateEngineChannelByChannelIndex(paramInt, paramChannel);
  }
  
  public void setAllChannelsTo(Channel paramChannel) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setChannelTo(b, paramChannel); 
  }
  
  public float getInputGainByChannelIndex(int paramInt) {
    return getTwoFloat(32, paramInt);
  }
  
  public void setInputGainbyChannel(int paramInt, float paramFloat) {
    setTwoFloat(32, paramInt, paramFloat);
  }
  
  public void setInputGainAllChannelsTo(float paramFloat) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setInputGainbyChannel(b, paramFloat); 
  }
  
  public Eq getPreEqByChannelIndex(int paramInt) {
    return queryEngineEqByChannelIndex(64, paramInt);
  }
  
  public void setPreEqByChannelIndex(int paramInt, Eq paramEq) {
    updateEngineEqByChannelIndex(64, paramInt, paramEq);
  }
  
  public void setPreEqAllChannelsTo(Eq paramEq) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setPreEqByChannelIndex(b, paramEq); 
  }
  
  public EqBand getPreEqBandByChannelIndex(int paramInt1, int paramInt2) {
    return queryEngineEqBandByChannelIndex(69, paramInt1, paramInt2);
  }
  
  public void setPreEqBandByChannelIndex(int paramInt1, int paramInt2, EqBand paramEqBand) {
    updateEngineEqBandByChannelIndex(69, paramInt1, paramInt2, paramEqBand);
  }
  
  public void setPreEqBandAllChannelsTo(int paramInt, EqBand paramEqBand) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setPreEqBandByChannelIndex(b, paramInt, paramEqBand); 
  }
  
  public Mbc getMbcByChannelIndex(int paramInt) {
    return queryEngineMbcByChannelIndex(paramInt);
  }
  
  public void setMbcByChannelIndex(int paramInt, Mbc paramMbc) {
    updateEngineMbcByChannelIndex(paramInt, paramMbc);
  }
  
  public void setMbcAllChannelsTo(Mbc paramMbc) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setMbcByChannelIndex(b, paramMbc); 
  }
  
  public MbcBand getMbcBandByChannelIndex(int paramInt1, int paramInt2) {
    return queryEngineMbcBandByChannelIndex(paramInt1, paramInt2);
  }
  
  public void setMbcBandByChannelIndex(int paramInt1, int paramInt2, MbcBand paramMbcBand) {
    updateEngineMbcBandByChannelIndex(paramInt1, paramInt2, paramMbcBand);
  }
  
  public void setMbcBandAllChannelsTo(int paramInt, MbcBand paramMbcBand) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setMbcBandByChannelIndex(b, paramInt, paramMbcBand); 
  }
  
  public Eq getPostEqByChannelIndex(int paramInt) {
    return queryEngineEqByChannelIndex(96, paramInt);
  }
  
  public void setPostEqByChannelIndex(int paramInt, Eq paramEq) {
    updateEngineEqByChannelIndex(96, paramInt, paramEq);
  }
  
  public void setPostEqAllChannelsTo(Eq paramEq) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setPostEqByChannelIndex(b, paramEq); 
  }
  
  public EqBand getPostEqBandByChannelIndex(int paramInt1, int paramInt2) {
    return queryEngineEqBandByChannelIndex(101, paramInt1, paramInt2);
  }
  
  public void setPostEqBandByChannelIndex(int paramInt1, int paramInt2, EqBand paramEqBand) {
    updateEngineEqBandByChannelIndex(101, paramInt1, paramInt2, paramEqBand);
  }
  
  public void setPostEqBandAllChannelsTo(int paramInt, EqBand paramEqBand) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setPostEqBandByChannelIndex(b, paramInt, paramEqBand); 
  }
  
  public Limiter getLimiterByChannelIndex(int paramInt) {
    return queryEngineLimiterByChannelIndex(paramInt);
  }
  
  public void setLimiterByChannelIndex(int paramInt, Limiter paramLimiter) {
    updateEngineLimiterByChannelIndex(paramInt, paramLimiter);
  }
  
  public void setLimiterAllChannelsTo(Limiter paramLimiter) {
    for (byte b = 0; b < this.mChannelCount; b++)
      setLimiterByChannelIndex(b, paramLimiter); 
  }
  
  public int getChannelCount() {
    return getOneInt(16);
  }
  
  private void setEngineArchitecture(int paramInt1, float paramFloat, boolean paramBoolean1, int paramInt2, boolean paramBoolean2, int paramInt3, boolean paramBoolean3, int paramInt4, boolean paramBoolean4) {
    setNumberArray(new Number[] { Integer.valueOf(48) }, new Number[] { Integer.valueOf(paramInt1), Float.valueOf(paramFloat), Integer.valueOf(paramBoolean1), Integer.valueOf(paramInt2), Integer.valueOf(paramBoolean2), Integer.valueOf(paramInt3), Integer.valueOf(paramBoolean3), Integer.valueOf(paramInt4), Integer.valueOf(paramBoolean4) });
  }
  
  private void updateEngineEqBandByChannelIndex(int paramInt1, int paramInt2, int paramInt3, EqBand paramEqBand) {
    boolean bool = paramEqBand.isEnabled();
    float f1 = paramEqBand.getCutoffFrequency();
    float f2 = paramEqBand.getGain();
    setNumberArray(new Number[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) }, new Number[] { Integer.valueOf(bool), Float.valueOf(f1), Float.valueOf(f2) });
  }
  
  private Eq queryEngineEqByChannelIndex(int paramInt1, int paramInt2) {
    boolean bool2;
    if (paramInt1 == 64) {
      b = 64;
    } else {
      b = 96;
    } 
    boolean bool1 = false;
    Number[] arrayOfNumber = new Number[3];
    arrayOfNumber[0] = Integer.valueOf(0);
    arrayOfNumber[1] = Integer.valueOf(0);
    arrayOfNumber[2] = Integer.valueOf(0);
    byte[] arrayOfByte1 = numberArrayToByteArray(new Number[] { Integer.valueOf(b), Integer.valueOf(paramInt2) });
    byte[] arrayOfByte2 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte1, arrayOfByte2);
    byteArrayToNumberArray(arrayOfByte2, arrayOfNumber);
    int i = arrayOfNumber[2].intValue();
    if (arrayOfNumber[0].intValue() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Number number = arrayOfNumber[1];
    if (number.intValue() > 0)
      bool1 = true; 
    Eq eq = new Eq(bool2, bool1, i);
    for (byte b = 0; b < i; b++) {
      byte b1;
      if (paramInt1 == 64) {
        b1 = 69;
      } else {
        b1 = 101;
      } 
      EqBand eqBand = queryEngineEqBandByChannelIndex(b1, paramInt2, b);
      eq.setBand(b, eqBand);
    } 
    return eq;
  }
  
  private EqBand queryEngineEqBandByChannelIndex(int paramInt1, int paramInt2, int paramInt3) {
    boolean bool = false;
    Number[] arrayOfNumber = new Number[3];
    arrayOfNumber[0] = Integer.valueOf(0);
    Float float_ = Float.valueOf(0.0F);
    arrayOfNumber[1] = float_;
    arrayOfNumber[2] = float_;
    byte[] arrayOfByte1 = numberArrayToByteArray(new Number[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2), Integer.valueOf(paramInt3) });
    byte[] arrayOfByte2 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte1, arrayOfByte2);
    byteArrayToNumberArray(arrayOfByte2, arrayOfNumber);
    if (arrayOfNumber[0].intValue() > 0)
      bool = true; 
    Number number2 = arrayOfNumber[1];
    float f = number2.floatValue();
    Number number1 = arrayOfNumber[2];
    return new EqBand(bool, f, number1.floatValue());
  }
  
  private void updateEngineEqByChannelIndex(int paramInt1, int paramInt2, Eq paramEq) {
    int i = paramEq.getBandCount();
    boolean bool1 = paramEq.isInUse();
    boolean bool2 = paramEq.isEnabled();
    setNumberArray(new Number[] { Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }, new Number[] { Integer.valueOf(bool1), Integer.valueOf(bool2), Integer.valueOf(i) });
    for (bool2 = false; bool2 < i; bool2++) {
      byte b;
      EqBand eqBand = paramEq.getBand(bool2);
      if (paramInt1 == 64) {
        b = 69;
      } else {
        b = 101;
      } 
      updateEngineEqBandByChannelIndex(b, paramInt2, bool2, eqBand);
    } 
  }
  
  private Mbc queryEngineMbcByChannelIndex(int paramInt) {
    boolean bool2, bool1 = false;
    Integer integer = Integer.valueOf(0);
    Number[] arrayOfNumber = new Number[3];
    arrayOfNumber[0] = integer;
    arrayOfNumber[1] = integer;
    arrayOfNumber[2] = integer;
    byte[] arrayOfByte1 = numberArrayToByteArray(new Number[] { Integer.valueOf(80), Integer.valueOf(paramInt) });
    byte[] arrayOfByte2 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte1, arrayOfByte2);
    byteArrayToNumberArray(arrayOfByte2, arrayOfNumber);
    int i = arrayOfNumber[2].intValue();
    if (arrayOfNumber[0].intValue() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    Number number = arrayOfNumber[1];
    if (number.intValue() > 0)
      bool1 = true; 
    Mbc mbc = new Mbc(bool2, bool1, i);
    for (byte b = 0; b < i; b++) {
      MbcBand mbcBand = queryEngineMbcBandByChannelIndex(paramInt, b);
      mbc.setBand(b, mbcBand);
    } 
    return mbc;
  }
  
  private MbcBand queryEngineMbcBandByChannelIndex(int paramInt1, int paramInt2) {
    boolean bool;
    Number[] arrayOfNumber = new Number[11];
    arrayOfNumber[0] = Integer.valueOf(0);
    Float float_ = Float.valueOf(0.0F);
    arrayOfNumber[1] = float_;
    arrayOfNumber[2] = float_;
    arrayOfNumber[3] = float_;
    arrayOfNumber[4] = float_;
    arrayOfNumber[5] = float_;
    arrayOfNumber[6] = float_;
    arrayOfNumber[7] = float_;
    arrayOfNumber[8] = float_;
    arrayOfNumber[9] = float_;
    arrayOfNumber[10] = float_;
    byte[] arrayOfByte1 = numberArrayToByteArray(new Number[] { Integer.valueOf(85), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) });
    byte[] arrayOfByte2 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte1, arrayOfByte2);
    byteArrayToNumberArray(arrayOfByte2, arrayOfNumber);
    if (arrayOfNumber[0].intValue() > 0) {
      bool = true;
    } else {
      bool = false;
    } 
    Number number2 = arrayOfNumber[1];
    float f1 = number2.floatValue();
    number2 = arrayOfNumber[2];
    float f2 = number2.floatValue();
    number2 = arrayOfNumber[3];
    float f3 = number2.floatValue();
    number2 = arrayOfNumber[4];
    float f4 = number2.floatValue();
    number2 = arrayOfNumber[5];
    float f5 = number2.floatValue();
    number2 = arrayOfNumber[6];
    float f6 = number2.floatValue();
    number2 = arrayOfNumber[7];
    float f7 = number2.floatValue();
    number2 = arrayOfNumber[8];
    float f8 = number2.floatValue();
    number2 = arrayOfNumber[9];
    float f9 = number2.floatValue();
    Number number1 = arrayOfNumber[10];
    return new MbcBand(bool, f1, f2, f3, f4, f5, f6, f7, f8, f9, number1.floatValue());
  }
  
  private void updateEngineMbcBandByChannelIndex(int paramInt1, int paramInt2, MbcBand paramMbcBand) {
    boolean bool = paramMbcBand.isEnabled();
    float f1 = paramMbcBand.getCutoffFrequency();
    float f2 = paramMbcBand.getAttackTime();
    float f3 = paramMbcBand.getReleaseTime();
    float f4 = paramMbcBand.getRatio();
    float f5 = paramMbcBand.getThreshold();
    float f6 = paramMbcBand.getKneeWidth();
    float f7 = paramMbcBand.getNoiseGateThreshold();
    float f8 = paramMbcBand.getExpanderRatio();
    float f9 = paramMbcBand.getPreGain();
    float f10 = paramMbcBand.getPostGain();
    setNumberArray(new Number[] { Integer.valueOf(85), Integer.valueOf(paramInt1), Integer.valueOf(paramInt2) }, new Number[] { 
          Integer.valueOf(bool), Float.valueOf(f1), Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4), Float.valueOf(f5), Float.valueOf(f6), Float.valueOf(f7), Float.valueOf(f8), Float.valueOf(f9), 
          Float.valueOf(f10) });
  }
  
  private void updateEngineMbcByChannelIndex(int paramInt, Mbc paramMbc) {
    int i = paramMbc.getBandCount();
    boolean bool1 = paramMbc.isInUse();
    boolean bool2 = paramMbc.isEnabled();
    setNumberArray(new Number[] { Integer.valueOf(80), Integer.valueOf(paramInt) }, new Number[] { Integer.valueOf(bool1), Integer.valueOf(bool2), Integer.valueOf(i) });
    for (bool1 = false; bool1 < i; bool1++) {
      MbcBand mbcBand = paramMbc.getBand(bool1);
      updateEngineMbcBandByChannelIndex(paramInt, bool1, mbcBand);
    } 
  }
  
  private void updateEngineLimiterByChannelIndex(int paramInt, Limiter paramLimiter) {
    boolean bool1 = paramLimiter.isInUse();
    boolean bool2 = paramLimiter.isEnabled();
    int i = paramLimiter.getLinkGroup();
    float f1 = paramLimiter.getAttackTime();
    float f2 = paramLimiter.getReleaseTime();
    float f3 = paramLimiter.getRatio();
    float f4 = paramLimiter.getThreshold();
    float f5 = paramLimiter.getPostGain();
    setNumberArray(new Number[] { Integer.valueOf(112), Integer.valueOf(paramInt) }, new Number[] { Integer.valueOf(bool1), Integer.valueOf(bool2), Integer.valueOf(i), Float.valueOf(f1), Float.valueOf(f2), Float.valueOf(f3), Float.valueOf(f4), Float.valueOf(f5) });
  }
  
  private Limiter queryEngineLimiterByChannelIndex(int paramInt) {
    boolean bool1, bool2;
    Integer integer = Integer.valueOf(0);
    Number[] arrayOfNumber = new Number[8];
    arrayOfNumber[0] = integer;
    arrayOfNumber[1] = integer;
    arrayOfNumber[2] = integer;
    Float float_ = Float.valueOf(0.0F);
    arrayOfNumber[3] = float_;
    arrayOfNumber[4] = float_;
    arrayOfNumber[5] = float_;
    arrayOfNumber[6] = float_;
    arrayOfNumber[7] = float_;
    byte[] arrayOfByte1 = numberArrayToByteArray(new Number[] { Integer.valueOf(112), Integer.valueOf(paramInt) });
    byte[] arrayOfByte2 = numberArrayToByteArray(arrayOfNumber);
    getParameter(arrayOfByte1, arrayOfByte2);
    byteArrayToNumberArray(arrayOfByte2, arrayOfNumber);
    if (arrayOfNumber[0].intValue() > 0) {
      bool1 = true;
    } else {
      bool1 = false;
    } 
    Number number1 = arrayOfNumber[1];
    if (number1.intValue() > 0) {
      bool2 = true;
    } else {
      bool2 = false;
    } 
    number1 = arrayOfNumber[2];
    paramInt = number1.intValue();
    number1 = arrayOfNumber[3];
    float f1 = number1.floatValue();
    number1 = arrayOfNumber[4];
    float f2 = number1.floatValue();
    number1 = arrayOfNumber[5];
    float f3 = number1.floatValue();
    number1 = arrayOfNumber[6];
    float f4 = number1.floatValue();
    Number number2 = arrayOfNumber[7];
    return new Limiter(bool1, bool2, paramInt, f1, f2, f3, f4, number2.floatValue());
  }
  
  private Channel queryEngineByChannelIndex(int paramInt) {
    float f = getTwoFloat(32, paramInt);
    Eq eq1 = queryEngineEqByChannelIndex(64, paramInt);
    Mbc mbc = queryEngineMbcByChannelIndex(paramInt);
    Eq eq2 = queryEngineEqByChannelIndex(96, paramInt);
    Limiter limiter = queryEngineLimiterByChannelIndex(paramInt);
    boolean bool1 = eq1.isInUse();
    int i = eq1.getBandCount();
    boolean bool2 = mbc.isInUse();
    paramInt = mbc.getBandCount();
    boolean bool3 = eq2.isInUse();
    int j = eq2.getBandCount();
    Channel channel = new Channel(f, bool1, i, bool2, paramInt, bool3, j, limiter.isInUse());
    channel.setInputGain(f);
    channel.setPreEq(eq1);
    channel.setMbc(mbc);
    channel.setPostEq(eq2);
    channel.setLimiter(limiter);
    return channel;
  }
  
  private void updateEngineChannelByChannelIndex(int paramInt, Channel paramChannel) {
    setTwoFloat(32, paramInt, paramChannel.getInputGain());
    Eq eq2 = paramChannel.getPreEq();
    updateEngineEqByChannelIndex(64, paramInt, eq2);
    Mbc mbc = paramChannel.getMbc();
    updateEngineMbcByChannelIndex(paramInt, mbc);
    Eq eq1 = paramChannel.getPostEq();
    updateEngineEqByChannelIndex(96, paramInt, eq1);
    Limiter limiter = paramChannel.getLimiter();
    updateEngineLimiterByChannelIndex(paramInt, limiter);
  }
  
  private int getOneInt(int paramInt) {
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(new int[] { paramInt }, arrayOfInt));
    return arrayOfInt[0];
  }
  
  private void setTwoFloat(int paramInt1, int paramInt2, float paramFloat) {
    byte[] arrayOfByte = floatToByteArray(paramFloat);
    checkStatus(setParameter(new int[] { paramInt1, paramInt2 }, arrayOfByte));
  }
  
  private byte[] numberArrayToByteArray(Number[] paramArrayOfNumber) {
    Number number;
    byte b1 = 0;
    for (byte b2 = 0; b2 < paramArrayOfNumber.length; b2++) {
      if (paramArrayOfNumber[b2] instanceof Integer) {
        b1 += true;
      } else if (paramArrayOfNumber[b2] instanceof Float) {
        b1 += true;
      } else {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("unknown value type ");
        number = paramArrayOfNumber[b2];
        stringBuilder.append(number.getClass());
        throw new IllegalArgumentException(stringBuilder.toString());
      } 
    } 
    ByteBuffer byteBuffer = ByteBuffer.allocate(b1);
    byteBuffer.order(ByteOrder.nativeOrder());
    for (b1 = 0; b1 < number.length; b1++) {
      if (number[b1] instanceof Integer) {
        byteBuffer.putInt(number[b1].intValue());
      } else if (number[b1] instanceof Float) {
        byteBuffer.putFloat(number[b1].floatValue());
      } 
    } 
    return byteBuffer.array();
  }
  
  private void byteArrayToNumberArray(byte[] paramArrayOfbyte, Number[] paramArrayOfNumber) {
    Number number;
    byte b1 = 0;
    byte b2 = 0;
    while (b1 < paramArrayOfbyte.length && b2 < paramArrayOfNumber.length) {
      if (paramArrayOfNumber[b2] instanceof Integer) {
        paramArrayOfNumber[b2] = Integer.valueOf(byteArrayToInt(paramArrayOfbyte, b1));
        b1 += 4;
        b2++;
        continue;
      } 
      if (paramArrayOfNumber[b2] instanceof Float) {
        paramArrayOfNumber[b2] = Float.valueOf(byteArrayToFloat(paramArrayOfbyte, b1));
        b1 += 4;
        b2++;
        continue;
      } 
      StringBuilder stringBuilder1 = new StringBuilder();
      stringBuilder1.append("can't convert ");
      number = paramArrayOfNumber[b2];
      stringBuilder1.append(number.getClass());
      throw new IllegalArgumentException(stringBuilder1.toString());
    } 
    if (b2 == number.length)
      return; 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("only converted ");
    stringBuilder.append(b2);
    stringBuilder.append(" values out of ");
    stringBuilder.append(number.length);
    stringBuilder.append(" expected");
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  private void setNumberArray(Number[] paramArrayOfNumber1, Number[] paramArrayOfNumber2) {
    byte[] arrayOfByte1 = numberArrayToByteArray(paramArrayOfNumber1);
    byte[] arrayOfByte2 = numberArrayToByteArray(paramArrayOfNumber2);
    checkStatus(setParameter(arrayOfByte1, arrayOfByte2));
  }
  
  private float getTwoFloat(int paramInt1, int paramInt2) {
    byte[] arrayOfByte = new byte[4];
    checkStatus(getParameter(new int[] { paramInt1, paramInt2 }, arrayOfByte));
    return byteArrayToFloat(arrayOfByte);
  }
  
  private void updateEffectArchitecture() {
    this.mChannelCount = getChannelCount();
  }
  
  private class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final DynamicsProcessing this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      if (param1Int != 0)
        return; 
      param1AudioEffect = null;
      synchronized (DynamicsProcessing.this.mParamListenerLock) {
        DynamicsProcessing.OnParameterChangeListener onParameterChangeListener;
        if (DynamicsProcessing.this.mParamListener != null)
          onParameterChangeListener = DynamicsProcessing.this.mParamListener; 
        if (onParameterChangeListener != null) {
          param1Int = -1;
          int i = Integer.MIN_VALUE;
          if (param1ArrayOfbyte1.length == 4)
            param1Int = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte2, 0); 
          if (param1Int != -1 && i != Integer.MIN_VALUE)
            onParameterChangeListener.onParameterChange(DynamicsProcessing.this, param1Int, i); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      this.mParamListener = paramOnParameterChangeListener;
      return;
    } 
  }
  
  class Settings {
    public int channelCount;
    
    public float[] inputGain;
    
    public Settings() {}
    
    public Settings(DynamicsProcessing this$0) {
      StringBuilder stringBuilder1;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      if (stringTokenizer.countTokens() == 3) {
        StringBuilder stringBuilder;
        String str = stringTokenizer.nextToken();
        if (str.equals("DynamicsProcessing"))
          try {
            StringBuilder stringBuilder4;
            String str1 = stringTokenizer.nextToken();
            str = str1;
            boolean bool = str1.equals("channelCount");
            if (bool) {
              String str2;
              str = str1;
              short s = Short.parseShort(stringTokenizer.nextToken());
              str = str1;
              this.channelCount = s;
              if (s <= 32) {
                str = str1;
                if (stringTokenizer.countTokens() == this.channelCount * 1) {
                  str = str1;
                  this.inputGain = new float[this.channelCount];
                  s = 0;
                  str2 = str1;
                  while (true) {
                    str = str2;
                    if (s < this.channelCount) {
                      str = str2;
                      str2 = stringTokenizer.nextToken();
                      str = str2;
                      stringBuilder4 = new StringBuilder();
                      str = str2;
                      this();
                      str = str2;
                      stringBuilder4.append(s);
                      str = str2;
                      stringBuilder4.append("_inputGain");
                      str = str2;
                      if (str2.equals(stringBuilder4.toString())) {
                        str = str2;
                        this.inputGain[s] = Float.parseFloat(stringTokenizer.nextToken());
                        s++;
                      } 
                      str = str2;
                      IllegalArgumentException illegalArgumentException3 = new IllegalArgumentException();
                      str = str2;
                      stringBuilder4 = new StringBuilder();
                      str = str2;
                      this();
                      str = str2;
                      stringBuilder4.append("invalid key name: ");
                      str = str2;
                      stringBuilder4.append(str2);
                      str = str2;
                      this(stringBuilder4.toString());
                      str = str2;
                      throw illegalArgumentException3;
                    } 
                    break;
                  } 
                  return;
                } 
                StringBuilder stringBuilder8 = stringBuilder4;
                IllegalArgumentException illegalArgumentException2 = new IllegalArgumentException();
                stringBuilder8 = stringBuilder4;
                StringBuilder stringBuilder7 = new StringBuilder();
                stringBuilder8 = stringBuilder4;
                this();
                stringBuilder8 = stringBuilder4;
                stringBuilder7.append("settings: ");
                stringBuilder8 = stringBuilder4;
                stringBuilder7.append(str2);
                stringBuilder8 = stringBuilder4;
                this(stringBuilder7.toString());
                stringBuilder8 = stringBuilder4;
                throw illegalArgumentException2;
              } 
              StringBuilder stringBuilder6 = stringBuilder4;
              IllegalArgumentException illegalArgumentException1 = new IllegalArgumentException();
              stringBuilder6 = stringBuilder4;
              StringBuilder stringBuilder5 = new StringBuilder();
              stringBuilder6 = stringBuilder4;
              this();
              stringBuilder6 = stringBuilder4;
              stringBuilder5.append("too many channels Settings:");
              stringBuilder6 = stringBuilder4;
              stringBuilder5.append(str2);
              stringBuilder6 = stringBuilder4;
              this(stringBuilder5.toString());
              stringBuilder6 = stringBuilder4;
              throw illegalArgumentException1;
            } 
            stringBuilder = stringBuilder4;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            stringBuilder = stringBuilder4;
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder = stringBuilder4;
            this();
            stringBuilder = stringBuilder4;
            stringBuilder3.append("invalid key name: ");
            stringBuilder = stringBuilder4;
            stringBuilder3.append((String)stringBuilder4);
            stringBuilder = stringBuilder4;
            this(stringBuilder3.toString());
            stringBuilder = stringBuilder4;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder3 = new StringBuilder();
            stringBuilder3.append("invalid value for key: ");
            stringBuilder3.append((String)stringBuilder);
            throw new IllegalArgumentException(stringBuilder3.toString());
          }  
        stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for DynamicsProcessing: ");
        stringBuilder1.append((String)stringBuilder);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder2 = new StringBuilder();
      stringBuilder2.append("settings: ");
      stringBuilder2.append((String)stringBuilder1);
      throw new IllegalArgumentException(stringBuilder2.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("DynamicsProcessing;channelCount=");
      int i = this.channelCount;
      stringBuilder.append(Integer.toString(i));
      String str = new String(stringBuilder.toString());
      for (i = 0; i < this.channelCount; i++) {
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append(";");
        stringBuilder1.append(i);
        stringBuilder1.append("_inputGain=");
        stringBuilder1.append(Float.toString(this.inputGain[i]));
        str = str.concat(stringBuilder1.toString());
      } 
      return str;
    }
  }
  
  public Settings getProperties() {
    Settings settings = new Settings();
    settings.channelCount = getChannelCount();
    if (settings.channelCount <= 32) {
      settings.inputGain = new float[settings.channelCount];
      for (byte b = 0; b < settings.channelCount; b++);
      return settings;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("too many channels Settings:");
    stringBuilder.append(settings);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  public void setProperties(Settings paramSettings) {
    if (paramSettings.channelCount == paramSettings.inputGain.length && paramSettings.channelCount == this.mChannelCount) {
      for (byte b = 0; b < this.mChannelCount; b++);
      return;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("settings invalid channel count: ");
    stringBuilder.append(paramSettings.channelCount);
    throw new IllegalArgumentException(stringBuilder.toString());
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(DynamicsProcessing param1DynamicsProcessing, int param1Int1, int param1Int2);
  }
}
