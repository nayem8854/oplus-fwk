package android.media.audiofx;

import android.util.Log;
import java.util.StringTokenizer;

public class LoudnessEnhancer extends AudioEffect {
  private OnParameterChangeListener mParamListener = null;
  
  private BaseParameterListener mBaseParamListener = null;
  
  private final Object mParamListenerLock = new Object();
  
  public static final int PARAM_TARGET_GAIN_MB = 0;
  
  private static final String TAG = "LoudnessEnhancer";
  
  public LoudnessEnhancer(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_LOUDNESS_ENHANCER, EFFECT_TYPE_NULL, 0, paramInt);
    if (paramInt == 0)
      Log.w("LoudnessEnhancer", "WARNING: attaching a LoudnessEnhancer to global output mix is deprecated!"); 
  }
  
  public LoudnessEnhancer(int paramInt1, int paramInt2) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException, RuntimeException {
    super(EFFECT_TYPE_LOUDNESS_ENHANCER, EFFECT_TYPE_NULL, paramInt1, paramInt2);
    if (paramInt2 == 0)
      Log.w("LoudnessEnhancer", "WARNING: attaching a LoudnessEnhancer to global output mix is deprecated!"); 
  }
  
  public void setTargetGain(int paramInt) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(0, paramInt));
  }
  
  public float getTargetGain() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(0, arrayOfInt));
    return arrayOfInt[0];
  }
  
  class BaseParameterListener implements AudioEffect.OnParameterChangeListener {
    final LoudnessEnhancer this$0;
    
    private BaseParameterListener() {}
    
    public void onParameterChange(AudioEffect param1AudioEffect, int param1Int, byte[] param1ArrayOfbyte1, byte[] param1ArrayOfbyte2) {
      if (param1Int != 0)
        return; 
      param1AudioEffect = null;
      synchronized (LoudnessEnhancer.this.mParamListenerLock) {
        LoudnessEnhancer.OnParameterChangeListener onParameterChangeListener;
        if (LoudnessEnhancer.this.mParamListener != null)
          onParameterChangeListener = LoudnessEnhancer.this.mParamListener; 
        if (onParameterChangeListener != null) {
          param1Int = -1;
          int i = Integer.MIN_VALUE;
          if (param1ArrayOfbyte1.length == 4)
            param1Int = AudioEffect.byteArrayToInt(param1ArrayOfbyte1, 0); 
          if (param1ArrayOfbyte2.length == 4)
            i = AudioEffect.byteArrayToInt(param1ArrayOfbyte2, 0); 
          if (param1Int != -1 && i != Integer.MIN_VALUE)
            onParameterChangeListener.onParameterChange(LoudnessEnhancer.this, param1Int, i); 
        } 
        return;
      } 
    }
  }
  
  public void setParameterListener(OnParameterChangeListener paramOnParameterChangeListener) {
    synchronized (this.mParamListenerLock) {
      if (this.mParamListener == null) {
        BaseParameterListener baseParameterListener = new BaseParameterListener();
        this(this);
        this.mBaseParamListener = baseParameterListener;
        setParameterListener(baseParameterListener);
      } 
      this.mParamListener = paramOnParameterChangeListener;
      return;
    } 
  }
  
  class OnParameterChangeListener {
    public abstract void onParameterChange(LoudnessEnhancer param1LoudnessEnhancer, int param1Int1, int param1Int2);
  }
  
  class Settings {
    public int targetGainmB;
    
    public Settings() {}
    
    public Settings(LoudnessEnhancer this$0) {
      String str;
      StringTokenizer stringTokenizer = new StringTokenizer((String)this$0, "=;");
      if (stringTokenizer.countTokens() == 3) {
        str = stringTokenizer.nextToken();
        if (str.equals("LoudnessEnhancer"))
          try {
            String str1 = stringTokenizer.nextToken();
            str = str1;
            if (str1.equals("targetGainmB")) {
              str = str1;
              this.targetGainmB = Integer.parseInt(stringTokenizer.nextToken());
              return;
            } 
            str = str1;
            IllegalArgumentException illegalArgumentException = new IllegalArgumentException();
            str = str1;
            StringBuilder stringBuilder2 = new StringBuilder();
            str = str1;
            this();
            str = str1;
            stringBuilder2.append("invalid key name: ");
            str = str1;
            stringBuilder2.append(str1);
            str = str1;
            this(stringBuilder2.toString());
            str = str1;
            throw illegalArgumentException;
          } catch (NumberFormatException numberFormatException) {
            StringBuilder stringBuilder2 = new StringBuilder();
            stringBuilder2.append("invalid value for key: ");
            stringBuilder2.append(str);
            throw new IllegalArgumentException(stringBuilder2.toString());
          }  
        StringBuilder stringBuilder1 = new StringBuilder();
        stringBuilder1.append("invalid settings for LoudnessEnhancer: ");
        stringBuilder1.append(str);
        throw new IllegalArgumentException(stringBuilder1.toString());
      } 
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("settings: ");
      stringBuilder.append(str);
      throw new IllegalArgumentException(stringBuilder.toString());
    }
    
    public String toString() {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("LoudnessEnhancer;targetGainmB=");
      int i = this.targetGainmB;
      stringBuilder.append(Integer.toString(i));
      return new String(stringBuilder.toString());
    }
  }
  
  public Settings getProperties() throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    Settings settings = new Settings();
    int[] arrayOfInt = new int[1];
    checkStatus(getParameter(0, arrayOfInt));
    settings.targetGainmB = arrayOfInt[0];
    return settings;
  }
  
  public void setProperties(Settings paramSettings) throws IllegalStateException, IllegalArgumentException, UnsupportedOperationException {
    checkStatus(setParameter(0, paramSettings.targetGainmB));
  }
}
