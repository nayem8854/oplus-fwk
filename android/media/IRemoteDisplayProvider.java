package android.media;

import android.os.Binder;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

public interface IRemoteDisplayProvider extends IInterface {
  void adjustVolume(String paramString, int paramInt) throws RemoteException;
  
  void connect(String paramString) throws RemoteException;
  
  void disconnect(String paramString) throws RemoteException;
  
  void setCallback(IRemoteDisplayCallback paramIRemoteDisplayCallback) throws RemoteException;
  
  void setDiscoveryMode(int paramInt) throws RemoteException;
  
  void setVolume(String paramString, int paramInt) throws RemoteException;
  
  class Default implements IRemoteDisplayProvider {
    public void setCallback(IRemoteDisplayCallback param1IRemoteDisplayCallback) throws RemoteException {}
    
    public void setDiscoveryMode(int param1Int) throws RemoteException {}
    
    public void connect(String param1String) throws RemoteException {}
    
    public void disconnect(String param1String) throws RemoteException {}
    
    public void setVolume(String param1String, int param1Int) throws RemoteException {}
    
    public void adjustVolume(String param1String, int param1Int) throws RemoteException {}
    
    public IBinder asBinder() {
      return null;
    }
  }
  
  class Stub extends Binder implements IRemoteDisplayProvider {
    private static final String DESCRIPTOR = "android.media.IRemoteDisplayProvider";
    
    static final int TRANSACTION_adjustVolume = 6;
    
    static final int TRANSACTION_connect = 3;
    
    static final int TRANSACTION_disconnect = 4;
    
    static final int TRANSACTION_setCallback = 1;
    
    static final int TRANSACTION_setDiscoveryMode = 2;
    
    static final int TRANSACTION_setVolume = 5;
    
    public Stub() {
      attachInterface(this, "android.media.IRemoteDisplayProvider");
    }
    
    public static IRemoteDisplayProvider asInterface(IBinder param1IBinder) {
      if (param1IBinder == null)
        return null; 
      IInterface iInterface = param1IBinder.queryLocalInterface("android.media.IRemoteDisplayProvider");
      if (iInterface != null && iInterface instanceof IRemoteDisplayProvider)
        return (IRemoteDisplayProvider)iInterface; 
      return new Proxy(param1IBinder);
    }
    
    public IBinder asBinder() {
      return this;
    }
    
    public static String getDefaultTransactionName(int param1Int) {
      switch (param1Int) {
        default:
          return null;
        case 6:
          return "adjustVolume";
        case 5:
          return "setVolume";
        case 4:
          return "disconnect";
        case 3:
          return "connect";
        case 2:
          return "setDiscoveryMode";
        case 1:
          break;
      } 
      return "setCallback";
    }
    
    public String getTransactionName(int param1Int) {
      return getDefaultTransactionName(param1Int);
    }
    
    public boolean onTransact(int param1Int1, Parcel param1Parcel1, Parcel param1Parcel2, int param1Int2) throws RemoteException {
      String str;
      if (param1Int1 != 1598968902) {
        String str1;
        switch (param1Int1) {
          default:
            return super.onTransact(param1Int1, param1Parcel1, param1Parcel2, param1Int2);
          case 6:
            param1Parcel1.enforceInterface("android.media.IRemoteDisplayProvider");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            adjustVolume(str, param1Int1);
            return true;
          case 5:
            param1Parcel1.enforceInterface("android.media.IRemoteDisplayProvider");
            str = param1Parcel1.readString();
            param1Int1 = param1Parcel1.readInt();
            setVolume(str, param1Int1);
            return true;
          case 4:
            param1Parcel1.enforceInterface("android.media.IRemoteDisplayProvider");
            str1 = param1Parcel1.readString();
            disconnect(str1);
            return true;
          case 3:
            str1.enforceInterface("android.media.IRemoteDisplayProvider");
            str1 = str1.readString();
            connect(str1);
            return true;
          case 2:
            str1.enforceInterface("android.media.IRemoteDisplayProvider");
            param1Int1 = str1.readInt();
            setDiscoveryMode(param1Int1);
            return true;
          case 1:
            break;
        } 
        str1.enforceInterface("android.media.IRemoteDisplayProvider");
        IRemoteDisplayCallback iRemoteDisplayCallback = IRemoteDisplayCallback.Stub.asInterface(str1.readStrongBinder());
        setCallback(iRemoteDisplayCallback);
        return true;
      } 
      str.writeString("android.media.IRemoteDisplayProvider");
      return true;
    }
    
    private static class Proxy implements IRemoteDisplayProvider {
      public static IRemoteDisplayProvider sDefaultImpl;
      
      private IBinder mRemote;
      
      Proxy(IBinder param2IBinder) {
        this.mRemote = param2IBinder;
      }
      
      public IBinder asBinder() {
        return this.mRemote;
      }
      
      public String getInterfaceDescriptor() {
        return "android.media.IRemoteDisplayProvider";
      }
      
      public void setCallback(IRemoteDisplayCallback param2IRemoteDisplayCallback) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          IBinder iBinder;
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          if (param2IRemoteDisplayCallback != null) {
            iBinder = param2IRemoteDisplayCallback.asBinder();
          } else {
            iBinder = null;
          } 
          parcel.writeStrongBinder(iBinder);
          boolean bool = this.mRemote.transact(1, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().setCallback(param2IRemoteDisplayCallback);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setDiscoveryMode(int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(2, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().setDiscoveryMode(param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void connect(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(3, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().connect(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void disconnect(String param2String) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          parcel.writeString(param2String);
          boolean bool = this.mRemote.transact(4, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().disconnect(param2String);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void setVolume(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(5, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().setVolume(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
      
      public void adjustVolume(String param2String, int param2Int) throws RemoteException {
        Parcel parcel = Parcel.obtain();
        try {
          parcel.writeInterfaceToken("android.media.IRemoteDisplayProvider");
          parcel.writeString(param2String);
          parcel.writeInt(param2Int);
          boolean bool = this.mRemote.transact(6, parcel, (Parcel)null, 1);
          if (!bool && IRemoteDisplayProvider.Stub.getDefaultImpl() != null) {
            IRemoteDisplayProvider.Stub.getDefaultImpl().adjustVolume(param2String, param2Int);
            return;
          } 
          return;
        } finally {
          parcel.recycle();
        } 
      }
    }
    
    public static boolean setDefaultImpl(IRemoteDisplayProvider param1IRemoteDisplayProvider) {
      if (Proxy.sDefaultImpl == null) {
        if (param1IRemoteDisplayProvider != null) {
          Proxy.sDefaultImpl = param1IRemoteDisplayProvider;
          return true;
        } 
        return false;
      } 
      throw new IllegalStateException("setDefaultImpl() called twice");
    }
    
    public static IRemoteDisplayProvider getDefaultImpl() {
      return Proxy.sDefaultImpl;
    }
  }
}
