package android.media;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.CharacterStyle;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.SubscriptSpan;
import android.text.style.SuperscriptSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.CaptioningManager;
import android.widget.RelativeLayout;
import com.android.internal.widget.SubtitleView;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class Cea708CCWidget extends ClosedCaptionWidget implements Cea708CCParser.DisplayListener {
  private final CCHandler mCCHandler;
  
  public Cea708CCWidget(Context paramContext) {
    this(paramContext, (AttributeSet)null);
  }
  
  public Cea708CCWidget(Context paramContext, AttributeSet paramAttributeSet) {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public Cea708CCWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt) {
    this(paramContext, paramAttributeSet, paramInt, 0);
  }
  
  public Cea708CCWidget(Context paramContext, AttributeSet paramAttributeSet, int paramInt1, int paramInt2) {
    super(paramContext, paramAttributeSet, paramInt1, paramInt2);
    this.mCCHandler = new CCHandler((CCLayout)this.mClosedCaptionLayout);
  }
  
  public ClosedCaptionWidget.ClosedCaptionLayout createCaptionLayout(Context paramContext) {
    return new CCLayout(paramContext);
  }
  
  public void emitEvent(Cea708CCParser.CaptionEvent paramCaptionEvent) {
    this.mCCHandler.processCaptionEvent(paramCaptionEvent);
    setSize(getWidth(), getHeight());
    if (this.mListener != null)
      this.mListener.onChanged(this); 
  }
  
  public void onDraw(Canvas paramCanvas) {
    super.onDraw(paramCanvas);
    ((ViewGroup)this.mClosedCaptionLayout).draw(paramCanvas);
  }
  
  class ScaledLayout extends ViewGroup {
    private static final boolean DEBUG = false;
    
    private static final String TAG = "ScaledLayout";
    
    private static final Comparator<Rect> mRectTopLeftSorter = (Comparator<Rect>)new Object();
    
    private Rect[] mRectArray;
    
    public ScaledLayout(Cea708CCWidget this$0) {
      super((Context)this$0);
    }
    
    class ScaledLayoutParams extends ViewGroup.LayoutParams {
      public static final float SCALE_UNSPECIFIED = -1.0F;
      
      public float scaleEndCol;
      
      public float scaleEndRow;
      
      public float scaleStartCol;
      
      public float scaleStartRow;
      
      public ScaledLayoutParams(Cea708CCWidget.ScaledLayout this$0, float param2Float1, float param2Float2, float param2Float3) {
        super(-1, -1);
        this.scaleStartRow = this$0;
        this.scaleEndRow = param2Float1;
        this.scaleStartCol = param2Float2;
        this.scaleEndCol = param2Float3;
      }
      
      public ScaledLayoutParams(Cea708CCWidget.ScaledLayout this$0, AttributeSet param2AttributeSet) {
        super(-1, -1);
      }
    }
    
    public ViewGroup.LayoutParams generateLayoutParams(AttributeSet param1AttributeSet) {
      return new ScaledLayoutParams(getContext(), param1AttributeSet);
    }
    
    protected boolean checkLayoutParams(ViewGroup.LayoutParams param1LayoutParams) {
      return param1LayoutParams instanceof ScaledLayoutParams;
    }
    
    protected void onMeasure(int param1Int1, int param1Int2) {
      int i = View.MeasureSpec.getSize(param1Int1);
      param1Int1 = View.MeasureSpec.getSize(param1Int2);
      int j = i - getPaddingLeft() - getPaddingRight();
      int k = param1Int1 - getPaddingTop() - getPaddingBottom();
      int m = getChildCount();
      this.mRectArray = new Rect[m];
      int n;
      for (n = 0, param1Int2 = i; n < m; ) {
        View view = getChildAt(n);
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams instanceof ScaledLayoutParams) {
          float f1 = ((ScaledLayoutParams)layoutParams).scaleStartRow;
          float f2 = ((ScaledLayoutParams)layoutParams).scaleEndRow;
          float f3 = ((ScaledLayoutParams)layoutParams).scaleStartCol;
          float f4 = ((ScaledLayoutParams)layoutParams).scaleEndCol;
          if (f1 >= 0.0F && f1 <= 1.0F) {
            if (f2 >= f1 && f1 <= 1.0F) {
              if (f4 >= 0.0F && f4 <= 1.0F) {
                if (f4 >= f3 && f4 <= 1.0F) {
                  this.mRectArray[n] = new Rect((int)(j * f3), (int)(k * f1), (int)(j * f4), (int)(k * f2));
                  i = View.MeasureSpec.makeMeasureSpec((int)(j * (f4 - f3)), 1073741824);
                  int i1 = View.MeasureSpec.makeMeasureSpec(0, 0);
                  view.measure(i, i1);
                  if (view.getMeasuredHeight() > this.mRectArray[n].height()) {
                    int i2 = view.getMeasuredHeight();
                    i1 = this.mRectArray[n].height();
                    i1 = (i2 - i1 + 1) / 2;
                    Rect rect = this.mRectArray[n];
                    rect.bottom += i1;
                    rect = this.mRectArray[n];
                    rect.top -= i1;
                    if ((this.mRectArray[n]).top < 0) {
                      rect = this.mRectArray[n];
                      rect.bottom -= (this.mRectArray[n]).top;
                      (this.mRectArray[n]).top = 0;
                    } 
                    if ((this.mRectArray[n]).bottom > k) {
                      rect = this.mRectArray[n];
                      rect.top -= (this.mRectArray[n]).bottom - k;
                      (this.mRectArray[n]).bottom = k;
                    } 
                  } 
                  i1 = View.MeasureSpec.makeMeasureSpec((int)(k * (f2 - f1)), 1073741824);
                  view.measure(i, i1);
                  n++;
                } 
                throw new RuntimeException("A child of ScaledLayout should have a range of scaleEndCol between scaleStartCol and 1");
              } 
              throw new RuntimeException("A child of ScaledLayout should have a range of scaleStartCol between 0 and 1");
            } 
            throw new RuntimeException("A child of ScaledLayout should have a range of scaleEndRow between scaleStartRow and 1");
          } 
          throw new RuntimeException("A child of ScaledLayout should have a range of scaleStartRow between 0 and 1");
        } 
        throw new RuntimeException("A child of ScaledLayout cannot have the UNSPECIFIED scale factors");
      } 
      n = 0;
      int[] arrayOfInt = new int[m];
      Rect[] arrayOfRect = new Rect[m];
      for (i = 0; i < m; i++, n = j) {
        j = n;
        if (getChildAt(i).getVisibility() == 0) {
          arrayOfInt[n] = n;
          arrayOfRect[n] = this.mRectArray[i];
          j = n + 1;
        } 
      } 
      Arrays.sort(arrayOfRect, 0, n, mRectTopLeftSorter);
      for (i = 0; i < n - 1; i++) {
        for (j = i + 1; j < n; j++) {
          if (Rect.intersects(arrayOfRect[i], arrayOfRect[j])) {
            arrayOfInt[j] = arrayOfInt[i];
            Rect rect1 = arrayOfRect[j];
            m = (arrayOfRect[j]).left;
            int i1 = (arrayOfRect[i]).bottom, i2 = (arrayOfRect[j]).right, i3 = (arrayOfRect[i]).bottom;
            Rect rect2 = arrayOfRect[j];
            int i4 = rect2.height();
            rect1.set(m, i1, i2, i3 + i4);
          } 
        } 
      } 
      for (; --n >= 0; n--) {
        if ((arrayOfRect[n]).bottom > k) {
          j = (arrayOfRect[n]).bottom - k;
          for (i = 0; i <= n; i++) {
            if (arrayOfInt[n] == arrayOfInt[i])
              arrayOfRect[i].set((arrayOfRect[i]).left, (arrayOfRect[i]).top - j, (arrayOfRect[i]).right, (arrayOfRect[i]).bottom - j); 
          } 
        } 
      } 
      setMeasuredDimension(param1Int2, param1Int1);
    }
    
    protected void onLayout(boolean param1Boolean, int param1Int1, int param1Int2, int param1Int3, int param1Int4) {
      param1Int2 = getPaddingLeft();
      param1Int3 = getPaddingTop();
      param1Int4 = getChildCount();
      for (param1Int1 = 0; param1Int1 < param1Int4; param1Int1++) {
        View view = getChildAt(param1Int1);
        if (view.getVisibility() != 8) {
          int i = (this.mRectArray[param1Int1]).left;
          int j = (this.mRectArray[param1Int1]).top;
          int k = (this.mRectArray[param1Int1]).bottom;
          int m = (this.mRectArray[param1Int1]).right;
          view.layout(i + param1Int2, j + param1Int3, m + param1Int3, k + param1Int2);
        } 
      } 
    }
    
    public void dispatchDraw(Canvas param1Canvas) {
      int i = getPaddingLeft();
      int j = getPaddingTop();
      int k = getChildCount();
      for (byte b = 0; b < k; b++) {
        View view = getChildAt(b);
        if (view.getVisibility() != 8) {
          Rect[] arrayOfRect = this.mRectArray;
          if (b >= arrayOfRect.length)
            break; 
          int m = (arrayOfRect[b]).left;
          int n = (this.mRectArray[b]).top;
          int i1 = param1Canvas.save();
          param1Canvas.translate((m + i), (n + j));
          view.draw(param1Canvas);
          param1Canvas.restoreToCount(i1);
        } 
      } 
    }
  }
  
  static class CCLayout extends ScaledLayout implements ClosedCaptionWidget.ClosedCaptionLayout {
    private static final float SAFE_TITLE_AREA_SCALE_END_X = 0.9F;
    
    private static final float SAFE_TITLE_AREA_SCALE_END_Y = 0.9F;
    
    private static final float SAFE_TITLE_AREA_SCALE_START_X = 0.1F;
    
    private static final float SAFE_TITLE_AREA_SCALE_START_Y = 0.1F;
    
    private final Cea708CCWidget.ScaledLayout mSafeTitleAreaLayout;
    
    public CCLayout(Context param1Context) {
      super(param1Context);
      Cea708CCWidget.ScaledLayout scaledLayout = new Cea708CCWidget.ScaledLayout(param1Context);
      addView((View)scaledLayout, new Cea708CCWidget.ScaledLayout.ScaledLayoutParams(0.1F, 0.9F, 0.1F, 0.9F));
    }
    
    public void addOrUpdateViewToSafeTitleArea(Cea708CCWidget.CCWindowLayout param1CCWindowLayout, Cea708CCWidget.ScaledLayout.ScaledLayoutParams param1ScaledLayoutParams) {
      int i = this.mSafeTitleAreaLayout.indexOfChild((View)param1CCWindowLayout);
      if (i < 0) {
        this.mSafeTitleAreaLayout.addView((View)param1CCWindowLayout, param1ScaledLayoutParams);
        return;
      } 
      this.mSafeTitleAreaLayout.updateViewLayout((View)param1CCWindowLayout, param1ScaledLayoutParams);
    }
    
    public void removeViewFromSafeTitleArea(Cea708CCWidget.CCWindowLayout param1CCWindowLayout) {
      this.mSafeTitleAreaLayout.removeView((View)param1CCWindowLayout);
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle) {
      int i = this.mSafeTitleAreaLayout.getChildCount();
      for (byte b = 0; b < i; b++) {
        Cea708CCWidget.ScaledLayout scaledLayout = this.mSafeTitleAreaLayout;
        Cea708CCWidget.CCWindowLayout cCWindowLayout = (Cea708CCWidget.CCWindowLayout)scaledLayout.getChildAt(b);
        cCWindowLayout.setCaptionStyle(param1CaptionStyle);
      } 
    }
    
    public void setFontScale(float param1Float) {
      int i = this.mSafeTitleAreaLayout.getChildCount();
      for (byte b = 0; b < i; b++) {
        Cea708CCWidget.ScaledLayout scaledLayout = this.mSafeTitleAreaLayout;
        Cea708CCWidget.CCWindowLayout cCWindowLayout = (Cea708CCWidget.CCWindowLayout)scaledLayout.getChildAt(b);
        cCWindowLayout.setFontScale(param1Float);
      } 
    }
  }
  
  class CCHandler implements Handler.Callback {
    private boolean mIsDelayed = false;
    
    private final Cea708CCWidget.CCWindowLayout[] mCaptionWindowLayouts = new Cea708CCWidget.CCWindowLayout[8];
    
    private final ArrayList<Cea708CCParser.CaptionEvent> mPendingCaptionEvents = new ArrayList<>();
    
    private static final int CAPTION_ALL_WINDOWS_BITMAP = 255;
    
    private static final long CAPTION_CLEAR_INTERVAL_MS = 60000L;
    
    private static final int CAPTION_WINDOWS_MAX = 8;
    
    private static final boolean DEBUG = false;
    
    private static final int MSG_CAPTION_CLEAR = 2;
    
    private static final int MSG_DELAY_CANCEL = 1;
    
    private static final String TAG = "CCHandler";
    
    private static final int TENTHS_OF_SECOND_IN_MILLIS = 100;
    
    private final Cea708CCWidget.CCLayout mCCLayout;
    
    private Cea708CCWidget.CCWindowLayout mCurrentWindowLayout;
    
    private final Handler mHandler;
    
    public CCHandler(Cea708CCWidget this$0) {
      this.mCCLayout = (Cea708CCWidget.CCLayout)this$0;
      this.mHandler = new Handler(this);
    }
    
    public boolean handleMessage(Message param1Message) {
      int i = param1Message.what;
      if (i != 1) {
        if (i != 2)
          return false; 
        clearWindows(255);
        return true;
      } 
      delayCancel();
      return true;
    }
    
    public void processCaptionEvent(Cea708CCParser.CaptionEvent param1CaptionEvent) {
      if (this.mIsDelayed) {
        this.mPendingCaptionEvents.add(param1CaptionEvent);
        return;
      } 
      switch (param1CaptionEvent.type) {
        default:
          return;
        case 16:
          defineWindow((Cea708CCParser.CaptionWindow)param1CaptionEvent.obj);
        case 15:
          setWindowAttr((Cea708CCParser.CaptionWindowAttr)param1CaptionEvent.obj);
        case 14:
          setPenLocation((Cea708CCParser.CaptionPenLocation)param1CaptionEvent.obj);
        case 13:
          setPenColor((Cea708CCParser.CaptionPenColor)param1CaptionEvent.obj);
        case 12:
          setPenAttr((Cea708CCParser.CaptionPenAttr)param1CaptionEvent.obj);
        case 11:
          reset();
        case 10:
          delayCancel();
        case 9:
          delay(((Integer)param1CaptionEvent.obj).intValue());
        case 8:
          deleteWindows(((Integer)param1CaptionEvent.obj).intValue());
        case 7:
          toggleWindows(((Integer)param1CaptionEvent.obj).intValue());
        case 6:
          hideWindows(((Integer)param1CaptionEvent.obj).intValue());
        case 5:
          displayWindows(((Integer)param1CaptionEvent.obj).intValue());
        case 4:
          clearWindows(((Integer)param1CaptionEvent.obj).intValue());
        case 3:
          setCurrentWindowLayout(((Integer)param1CaptionEvent.obj).intValue());
        case 2:
          sendControlToCurrentWindow(((Character)param1CaptionEvent.obj).charValue());
        case 1:
          break;
      } 
      sendBufferToCurrentWindow((String)param1CaptionEvent.obj);
    }
    
    private void setCurrentWindowLayout(int param1Int) {
      if (param1Int >= 0) {
        Cea708CCWidget.CCWindowLayout[] arrayOfCCWindowLayout = this.mCaptionWindowLayouts;
        if (param1Int < arrayOfCCWindowLayout.length) {
          Cea708CCWidget.CCWindowLayout cCWindowLayout = arrayOfCCWindowLayout[param1Int];
          if (cCWindowLayout == null)
            return; 
          this.mCurrentWindowLayout = cCWindowLayout;
          return;
        } 
      } 
    }
    
    private ArrayList<Cea708CCWidget.CCWindowLayout> getWindowsFromBitmap(int param1Int) {
      ArrayList<Cea708CCWidget.CCWindowLayout> arrayList = new ArrayList();
      for (byte b = 0; b < 8; b++) {
        if ((1 << b & param1Int) != 0) {
          Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCaptionWindowLayouts[b];
          if (cCWindowLayout != null)
            arrayList.add(cCWindowLayout); 
        } 
      } 
      return arrayList;
    }
    
    private void clearWindows(int param1Int) {
      if (param1Int == 0)
        return; 
      for (Cea708CCWidget.CCWindowLayout cCWindowLayout : getWindowsFromBitmap(param1Int))
        cCWindowLayout.clear(); 
    }
    
    private void displayWindows(int param1Int) {
      if (param1Int == 0)
        return; 
      for (Cea708CCWidget.CCWindowLayout cCWindowLayout : getWindowsFromBitmap(param1Int))
        cCWindowLayout.show(); 
    }
    
    private void hideWindows(int param1Int) {
      if (param1Int == 0)
        return; 
      for (Cea708CCWidget.CCWindowLayout cCWindowLayout : getWindowsFromBitmap(param1Int))
        cCWindowLayout.hide(); 
    }
    
    private void toggleWindows(int param1Int) {
      if (param1Int == 0)
        return; 
      for (Cea708CCWidget.CCWindowLayout cCWindowLayout : getWindowsFromBitmap(param1Int)) {
        if (cCWindowLayout.isShown()) {
          cCWindowLayout.hide();
          continue;
        } 
        cCWindowLayout.show();
      } 
    }
    
    private void deleteWindows(int param1Int) {
      if (param1Int == 0)
        return; 
      for (Cea708CCWidget.CCWindowLayout cCWindowLayout : getWindowsFromBitmap(param1Int)) {
        cCWindowLayout.removeFromCaptionView();
        this.mCaptionWindowLayouts[cCWindowLayout.getCaptionWindowId()] = null;
      } 
    }
    
    public void reset() {
      this.mCurrentWindowLayout = null;
      this.mIsDelayed = false;
      this.mPendingCaptionEvents.clear();
      for (byte b = 0; b < 8; b++) {
        Cea708CCWidget.CCWindowLayout[] arrayOfCCWindowLayout = this.mCaptionWindowLayouts;
        if (arrayOfCCWindowLayout[b] != null)
          arrayOfCCWindowLayout[b].removeFromCaptionView(); 
        this.mCaptionWindowLayouts[b] = null;
      } 
      this.mCCLayout.setVisibility(4);
      this.mHandler.removeMessages(2);
    }
    
    private void setWindowAttr(Cea708CCParser.CaptionWindowAttr param1CaptionWindowAttr) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null)
        cCWindowLayout.setWindowAttr(param1CaptionWindowAttr); 
    }
    
    private void defineWindow(Cea708CCParser.CaptionWindow param1CaptionWindow) {
      if (param1CaptionWindow == null)
        return; 
      int i = param1CaptionWindow.id;
      if (i >= 0) {
        Cea708CCWidget.CCWindowLayout[] arrayOfCCWindowLayout = this.mCaptionWindowLayouts;
        if (i < arrayOfCCWindowLayout.length) {
          Cea708CCWidget.CCWindowLayout cCWindowLayout2 = arrayOfCCWindowLayout[i];
          Cea708CCWidget.CCWindowLayout cCWindowLayout1 = cCWindowLayout2;
          if (cCWindowLayout2 == null)
            cCWindowLayout1 = new Cea708CCWidget.CCWindowLayout(this.mCCLayout.getContext()); 
          cCWindowLayout1.initWindow(this.mCCLayout, param1CaptionWindow);
          this.mCaptionWindowLayouts[i] = cCWindowLayout1;
          this.mCurrentWindowLayout = cCWindowLayout1;
          return;
        } 
      } 
    }
    
    private void delay(int param1Int) {
      if (param1Int < 0 || param1Int > 255)
        return; 
      this.mIsDelayed = true;
      Handler handler = this.mHandler;
      handler.sendMessageDelayed(handler.obtainMessage(1), (param1Int * 100));
    }
    
    private void delayCancel() {
      this.mIsDelayed = false;
      processPendingBuffer();
    }
    
    private void processPendingBuffer() {
      for (Cea708CCParser.CaptionEvent captionEvent : this.mPendingCaptionEvents)
        processCaptionEvent(captionEvent); 
      this.mPendingCaptionEvents.clear();
    }
    
    private void sendControlToCurrentWindow(char param1Char) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null)
        cCWindowLayout.sendControl(param1Char); 
    }
    
    private void sendBufferToCurrentWindow(String param1String) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null) {
        cCWindowLayout.sendBuffer(param1String);
        this.mHandler.removeMessages(2);
        Handler handler = this.mHandler;
        handler.sendMessageDelayed(handler.obtainMessage(2), 60000L);
      } 
    }
    
    private void setPenAttr(Cea708CCParser.CaptionPenAttr param1CaptionPenAttr) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null)
        cCWindowLayout.setPenAttr(param1CaptionPenAttr); 
    }
    
    private void setPenColor(Cea708CCParser.CaptionPenColor param1CaptionPenColor) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null)
        cCWindowLayout.setPenColor(param1CaptionPenColor); 
    }
    
    private void setPenLocation(Cea708CCParser.CaptionPenLocation param1CaptionPenLocation) {
      Cea708CCWidget.CCWindowLayout cCWindowLayout = this.mCurrentWindowLayout;
      if (cCWindowLayout != null)
        cCWindowLayout.setPenLocation(param1CaptionPenLocation.row, param1CaptionPenLocation.column); 
    }
  }
  
  static class CCWindowLayout extends RelativeLayout implements View.OnLayoutChangeListener {
    private int mRowLimit = 0;
    
    private final SpannableStringBuilder mBuilder = new SpannableStringBuilder();
    
    private final List<CharacterStyle> mCharacterStyles = new ArrayList<>();
    
    private int mRow = -1;
    
    private static final int ANCHOR_HORIZONTAL_16_9_MAX = 209;
    
    private static final int ANCHOR_HORIZONTAL_MODE_CENTER = 1;
    
    private static final int ANCHOR_HORIZONTAL_MODE_LEFT = 0;
    
    private static final int ANCHOR_HORIZONTAL_MODE_RIGHT = 2;
    
    private static final int ANCHOR_MODE_DIVIDER = 3;
    
    private static final int ANCHOR_RELATIVE_POSITIONING_MAX = 99;
    
    private static final int ANCHOR_VERTICAL_MAX = 74;
    
    private static final int ANCHOR_VERTICAL_MODE_BOTTOM = 2;
    
    private static final int ANCHOR_VERTICAL_MODE_CENTER = 1;
    
    private static final int ANCHOR_VERTICAL_MODE_TOP = 0;
    
    private static final int MAX_COLUMN_COUNT_16_9 = 42;
    
    private static final float PROPORTION_PEN_SIZE_LARGE = 1.25F;
    
    private static final float PROPORTION_PEN_SIZE_SMALL = 0.75F;
    
    private static final String TAG = "CCWindowLayout";
    
    private Cea708CCWidget.CCLayout mCCLayout;
    
    private Cea708CCWidget.CCView mCCView;
    
    private CaptioningManager.CaptionStyle mCaptionStyle;
    
    private int mCaptionWindowId;
    
    private float mFontScale;
    
    private int mLastCaptionLayoutHeight;
    
    private int mLastCaptionLayoutWidth;
    
    private float mTextSize;
    
    private String mWidestChar;
    
    public CCWindowLayout(Context param1Context) {
      this(param1Context, (AttributeSet)null);
    }
    
    public CCWindowLayout(Context param1Context, AttributeSet param1AttributeSet) {
      this(param1Context, param1AttributeSet, 0);
    }
    
    public CCWindowLayout(Context param1Context, AttributeSet param1AttributeSet, int param1Int) {
      this(param1Context, param1AttributeSet, param1Int, 0);
    }
    
    public CCWindowLayout(Context param1Context, AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super(param1Context, param1AttributeSet, param1Int1, param1Int2);
      this.mCCView = new Cea708CCWidget.CCView();
      RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
      addView((View)this.mCCView, (ViewGroup.LayoutParams)layoutParams);
      CaptioningManager captioningManager = (CaptioningManager)param1Context.getSystemService("captioning");
      this.mFontScale = captioningManager.getFontScale();
      setCaptionStyle(captioningManager.getUserStyle());
      this.mCCView.setText("");
      updateWidestChar();
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle) {
      this.mCaptionStyle = param1CaptionStyle;
      this.mCCView.setCaptionStyle(param1CaptionStyle);
    }
    
    public void setFontScale(float param1Float) {
      this.mFontScale = param1Float;
      updateTextSize();
    }
    
    public int getCaptionWindowId() {
      return this.mCaptionWindowId;
    }
    
    public void setCaptionWindowId(int param1Int) {
      this.mCaptionWindowId = param1Int;
    }
    
    public void clear() {
      clearText();
      hide();
    }
    
    public void show() {
      setVisibility(0);
      requestLayout();
    }
    
    public void hide() {
      setVisibility(4);
      requestLayout();
    }
    
    public void setPenAttr(Cea708CCParser.CaptionPenAttr param1CaptionPenAttr) {
      this.mCharacterStyles.clear();
      if (param1CaptionPenAttr.italic)
        this.mCharacterStyles.add(new StyleSpan(2)); 
      if (param1CaptionPenAttr.underline)
        this.mCharacterStyles.add(new UnderlineSpan()); 
      int i = param1CaptionPenAttr.penSize;
      if (i != 0) {
        if (i == 2)
          this.mCharacterStyles.add(new RelativeSizeSpan(1.25F)); 
      } else {
        this.mCharacterStyles.add(new RelativeSizeSpan(0.75F));
      } 
      i = param1CaptionPenAttr.penOffset;
      if (i != 0) {
        if (i == 2)
          this.mCharacterStyles.add(new SuperscriptSpan()); 
      } else {
        this.mCharacterStyles.add(new SubscriptSpan());
      } 
    }
    
    public void setPenColor(Cea708CCParser.CaptionPenColor param1CaptionPenColor) {}
    
    public void setPenLocation(int param1Int1, int param1Int2) {
      if (this.mRow >= 0)
        for (param1Int2 = this.mRow; param1Int2 < param1Int1; param1Int2++)
          appendText("\n");  
      this.mRow = param1Int1;
    }
    
    public void setWindowAttr(Cea708CCParser.CaptionWindowAttr param1CaptionWindowAttr) {}
    
    public void sendBuffer(String param1String) {
      appendText(param1String);
    }
    
    public void sendControl(char param1Char) {}
    
    public void initWindow(Cea708CCWidget.CCLayout param1CCLayout, Cea708CCParser.CaptionWindow param1CaptionWindow) {
      // Byte code:
      //   0: aload_0
      //   1: getfield mCCLayout : Landroid/media/Cea708CCWidget$CCLayout;
      //   4: astore_3
      //   5: aload_3
      //   6: aload_1
      //   7: if_acmpeq -> 33
      //   10: aload_3
      //   11: ifnull -> 19
      //   14: aload_3
      //   15: aload_0
      //   16: invokevirtual removeOnLayoutChangeListener : (Landroid/view/View$OnLayoutChangeListener;)V
      //   19: aload_0
      //   20: aload_1
      //   21: putfield mCCLayout : Landroid/media/Cea708CCWidget$CCLayout;
      //   24: aload_1
      //   25: aload_0
      //   26: invokevirtual addOnLayoutChangeListener : (Landroid/view/View$OnLayoutChangeListener;)V
      //   29: aload_0
      //   30: invokespecial updateWidestChar : ()V
      //   33: aload_2
      //   34: getfield anchorVertical : I
      //   37: i2f
      //   38: fstore #4
      //   40: aload_2
      //   41: getfield relativePositioning : Z
      //   44: istore #5
      //   46: bipush #99
      //   48: istore #6
      //   50: iload #5
      //   52: ifeq -> 62
      //   55: bipush #99
      //   57: istore #7
      //   59: goto -> 66
      //   62: bipush #74
      //   64: istore #7
      //   66: fload #4
      //   68: iload #7
      //   70: i2f
      //   71: fdiv
      //   72: fstore #8
      //   74: aload_2
      //   75: getfield anchorHorizontal : I
      //   78: i2f
      //   79: fstore #4
      //   81: aload_2
      //   82: getfield relativePositioning : Z
      //   85: ifeq -> 95
      //   88: iload #6
      //   90: istore #7
      //   92: goto -> 100
      //   95: sipush #209
      //   98: istore #7
      //   100: fload #4
      //   102: iload #7
      //   104: i2f
      //   105: fdiv
      //   106: fstore #9
      //   108: fload #8
      //   110: fconst_0
      //   111: fcmpg
      //   112: iflt -> 126
      //   115: fload #8
      //   117: fstore #4
      //   119: fload #8
      //   121: fconst_1
      //   122: fcmpl
      //   123: ifle -> 171
      //   126: new java/lang/StringBuilder
      //   129: dup
      //   130: invokespecial <init> : ()V
      //   133: astore_1
      //   134: aload_1
      //   135: ldc_w 'The vertical position of the anchor point should be at the range of 0 and 1 but '
      //   138: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   141: pop
      //   142: aload_1
      //   143: fload #8
      //   145: invokevirtual append : (F)Ljava/lang/StringBuilder;
      //   148: pop
      //   149: ldc 'CCWindowLayout'
      //   151: aload_1
      //   152: invokevirtual toString : ()Ljava/lang/String;
      //   155: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   158: pop
      //   159: fconst_0
      //   160: fload #8
      //   162: fconst_1
      //   163: invokestatic min : (FF)F
      //   166: invokestatic max : (FF)F
      //   169: fstore #4
      //   171: fload #9
      //   173: fconst_0
      //   174: fcmpg
      //   175: iflt -> 189
      //   178: fload #9
      //   180: fstore #8
      //   182: fload #9
      //   184: fconst_1
      //   185: fcmpl
      //   186: ifle -> 234
      //   189: new java/lang/StringBuilder
      //   192: dup
      //   193: invokespecial <init> : ()V
      //   196: astore_1
      //   197: aload_1
      //   198: ldc_w 'The horizontal position of the anchor point should be at the range of 0 and 1 but '
      //   201: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   204: pop
      //   205: aload_1
      //   206: fload #9
      //   208: invokevirtual append : (F)Ljava/lang/StringBuilder;
      //   211: pop
      //   212: ldc 'CCWindowLayout'
      //   214: aload_1
      //   215: invokevirtual toString : ()Ljava/lang/String;
      //   218: invokestatic i : (Ljava/lang/String;Ljava/lang/String;)I
      //   221: pop
      //   222: fconst_0
      //   223: fload #9
      //   225: fconst_1
      //   226: invokestatic min : (FF)F
      //   229: invokestatic max : (FF)F
      //   232: fstore #8
      //   234: bipush #17
      //   236: istore #7
      //   238: aload_2
      //   239: getfield anchorId : I
      //   242: iconst_3
      //   243: irem
      //   244: istore #10
      //   246: aload_2
      //   247: getfield anchorId : I
      //   250: iconst_3
      //   251: idiv
      //   252: istore #6
      //   254: fconst_0
      //   255: fstore #11
      //   257: fconst_1
      //   258: fstore #12
      //   260: fconst_0
      //   261: fstore #13
      //   263: fconst_1
      //   264: fstore #9
      //   266: iload #10
      //   268: ifeq -> 528
      //   271: iload #10
      //   273: iconst_1
      //   274: if_icmpeq -> 314
      //   277: iload #10
      //   279: iconst_2
      //   280: if_icmpeq -> 290
      //   283: fload #13
      //   285: fstore #8
      //   287: goto -> 541
      //   290: iconst_5
      //   291: istore #7
      //   293: aload_0
      //   294: getfield mCCView : Landroid/media/Cea708CCWidget$CCView;
      //   297: getstatic android/text/Layout$Alignment.ALIGN_RIGHT : Landroid/text/Layout$Alignment;
      //   300: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)V
      //   303: fload #8
      //   305: fstore #9
      //   307: fload #13
      //   309: fstore #8
      //   311: goto -> 541
      //   314: fconst_1
      //   315: fload #8
      //   317: fsub
      //   318: fload #8
      //   320: invokestatic min : (FF)F
      //   323: fstore #13
      //   325: aload_2
      //   326: getfield columnCount : I
      //   329: istore #7
      //   331: aload_0
      //   332: invokespecial getScreenColumnCount : ()I
      //   335: iload #7
      //   337: iconst_1
      //   338: iadd
      //   339: invokestatic min : (II)I
      //   342: istore #10
      //   344: new java/lang/StringBuilder
      //   347: dup
      //   348: invokespecial <init> : ()V
      //   351: astore_3
      //   352: iconst_0
      //   353: istore #7
      //   355: iload #7
      //   357: iload #10
      //   359: if_icmpge -> 377
      //   362: aload_3
      //   363: aload_0
      //   364: getfield mWidestChar : Ljava/lang/String;
      //   367: invokevirtual append : (Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   370: pop
      //   371: iinc #7, 1
      //   374: goto -> 355
      //   377: new android/graphics/Paint
      //   380: dup
      //   381: invokespecial <init> : ()V
      //   384: astore_1
      //   385: aload_1
      //   386: aload_0
      //   387: getfield mCaptionStyle : Landroid/view/accessibility/CaptioningManager$CaptionStyle;
      //   390: invokevirtual getTypeface : ()Landroid/graphics/Typeface;
      //   393: invokevirtual setTypeface : (Landroid/graphics/Typeface;)Landroid/graphics/Typeface;
      //   396: pop
      //   397: aload_1
      //   398: aload_0
      //   399: getfield mTextSize : F
      //   402: invokevirtual setTextSize : (F)V
      //   405: aload_1
      //   406: aload_3
      //   407: invokevirtual toString : ()Ljava/lang/String;
      //   410: invokevirtual measureText : (Ljava/lang/String;)F
      //   413: fstore #9
      //   415: aload_0
      //   416: getfield mCCLayout : Landroid/media/Cea708CCWidget$CCLayout;
      //   419: invokevirtual getWidth : ()I
      //   422: ifle -> 446
      //   425: fload #9
      //   427: fconst_2
      //   428: fdiv
      //   429: aload_0
      //   430: getfield mCCLayout : Landroid/media/Cea708CCWidget$CCLayout;
      //   433: invokevirtual getWidth : ()I
      //   436: i2f
      //   437: ldc 0.8
      //   439: fmul
      //   440: fdiv
      //   441: fstore #9
      //   443: goto -> 449
      //   446: fconst_0
      //   447: fstore #9
      //   449: fload #9
      //   451: fconst_0
      //   452: fcmpl
      //   453: ifle -> 490
      //   456: fload #9
      //   458: fload #8
      //   460: fcmpg
      //   461: ifge -> 490
      //   464: aload_0
      //   465: getfield mCCView : Landroid/media/Cea708CCWidget$CCView;
      //   468: getstatic android/text/Layout$Alignment.ALIGN_NORMAL : Landroid/text/Layout$Alignment;
      //   471: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)V
      //   474: fload #8
      //   476: fload #9
      //   478: fsub
      //   479: fstore #8
      //   481: fconst_1
      //   482: fstore #9
      //   484: iconst_3
      //   485: istore #7
      //   487: goto -> 541
      //   490: iconst_1
      //   491: istore #7
      //   493: aload_0
      //   494: getfield mCCView : Landroid/media/Cea708CCWidget$CCView;
      //   497: getstatic android/text/Layout$Alignment.ALIGN_CENTER : Landroid/text/Layout$Alignment;
      //   500: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)V
      //   503: fload #8
      //   505: fload #13
      //   507: fsub
      //   508: fstore #9
      //   510: fload #8
      //   512: fload #13
      //   514: fadd
      //   515: fstore #13
      //   517: fload #9
      //   519: fstore #8
      //   521: fload #13
      //   523: fstore #9
      //   525: goto -> 541
      //   528: iconst_3
      //   529: istore #7
      //   531: aload_0
      //   532: getfield mCCView : Landroid/media/Cea708CCWidget$CCView;
      //   535: getstatic android/text/Layout$Alignment.ALIGN_NORMAL : Landroid/text/Layout$Alignment;
      //   538: invokevirtual setAlignment : (Landroid/text/Layout$Alignment;)V
      //   541: iload #6
      //   543: ifeq -> 626
      //   546: iload #6
      //   548: iconst_1
      //   549: if_icmpeq -> 583
      //   552: iload #6
      //   554: iconst_2
      //   555: if_icmpeq -> 565
      //   558: fload #11
      //   560: fstore #4
      //   562: goto -> 633
      //   565: iload #7
      //   567: bipush #80
      //   569: ior
      //   570: istore #7
      //   572: fload #4
      //   574: fstore #12
      //   576: fload #11
      //   578: fstore #4
      //   580: goto -> 633
      //   583: iload #7
      //   585: bipush #16
      //   587: ior
      //   588: istore #7
      //   590: fconst_1
      //   591: fload #4
      //   593: fsub
      //   594: fload #4
      //   596: invokestatic min : (FF)F
      //   599: fstore #11
      //   601: fload #4
      //   603: fload #11
      //   605: fsub
      //   606: fstore #12
      //   608: fload #4
      //   610: fload #11
      //   612: fadd
      //   613: fstore #11
      //   615: fload #12
      //   617: fstore #4
      //   619: fload #11
      //   621: fstore #12
      //   623: goto -> 633
      //   626: iload #7
      //   628: bipush #48
      //   630: ior
      //   631: istore #7
      //   633: aload_0
      //   634: getfield mCCLayout : Landroid/media/Cea708CCWidget$CCLayout;
      //   637: aload_0
      //   638: new android/media/Cea708CCWidget$ScaledLayout$ScaledLayoutParams
      //   641: dup
      //   642: fload #4
      //   644: fload #12
      //   646: fload #8
      //   648: fload #9
      //   650: invokespecial <init> : (FFFF)V
      //   653: invokevirtual addOrUpdateViewToSafeTitleArea : (Landroid/media/Cea708CCWidget$CCWindowLayout;Landroid/media/Cea708CCWidget$ScaledLayout$ScaledLayoutParams;)V
      //   656: aload_0
      //   657: aload_2
      //   658: getfield id : I
      //   661: invokevirtual setCaptionWindowId : (I)V
      //   664: aload_0
      //   665: aload_2
      //   666: getfield rowCount : I
      //   669: invokevirtual setRowLimit : (I)V
      //   672: aload_0
      //   673: iload #7
      //   675: invokevirtual setGravity : (I)V
      //   678: aload_2
      //   679: getfield visible : Z
      //   682: ifeq -> 692
      //   685: aload_0
      //   686: invokevirtual show : ()V
      //   689: goto -> 696
      //   692: aload_0
      //   693: invokevirtual hide : ()V
      //   696: return
      // Line number table:
      //   Java source line number -> byte code offset
      //   #1865	-> 0
      //   #1866	-> 10
      //   #1867	-> 14
      //   #1869	-> 19
      //   #1870	-> 24
      //   #1871	-> 29
      //   #1875	-> 33
      //   #1876	-> 40
      //   #1877	-> 55
      //   #1880	-> 74
      //   #1881	-> 81
      //   #1882	-> 95
      //   #1886	-> 108
      //   #1887	-> 126
      //   #1889	-> 159
      //   #1891	-> 171
      //   #1892	-> 189
      //   #1894	-> 222
      //   #1896	-> 234
      //   #1897	-> 238
      //   #1898	-> 246
      //   #1899	-> 254
      //   #1900	-> 257
      //   #1901	-> 260
      //   #1902	-> 263
      //   #1903	-> 266
      //   #1949	-> 290
      //   #1950	-> 293
      //   #1951	-> 303
      //   #1910	-> 314
      //   #1914	-> 325
      //   #1915	-> 331
      //   #1916	-> 344
      //   #1917	-> 352
      //   #1918	-> 362
      //   #1917	-> 371
      //   #1920	-> 377
      //   #1921	-> 385
      //   #1922	-> 397
      //   #1923	-> 405
      //   #1924	-> 415
      //   #1925	-> 425
      //   #1926	-> 449
      //   #1930	-> 464
      //   #1931	-> 464
      //   #1932	-> 474
      //   #1933	-> 481
      //   #1942	-> 490
      //   #1943	-> 493
      //   #1944	-> 503
      //   #1945	-> 510
      //   #1947	-> 517
      //   #1905	-> 528
      //   #1906	-> 531
      //   #1907	-> 541
      //   #1908	-> 541
      //   #1954	-> 541
      //   #1968	-> 565
      //   #1969	-> 572
      //   #1960	-> 583
      //   #1963	-> 590
      //   #1964	-> 601
      //   #1965	-> 608
      //   #1966	-> 615
      //   #1956	-> 626
      //   #1957	-> 633
      //   #1958	-> 633
      //   #1972	-> 633
      //   #1974	-> 656
      //   #1975	-> 664
      //   #1976	-> 672
      //   #1977	-> 678
      //   #1978	-> 685
      //   #1980	-> 692
      //   #1982	-> 696
    }
    
    public void onLayoutChange(View param1View, int param1Int1, int param1Int2, int param1Int3, int param1Int4, int param1Int5, int param1Int6, int param1Int7, int param1Int8) {
      param1Int1 = param1Int3 - param1Int1;
      param1Int2 = param1Int4 - param1Int2;
      if (param1Int1 != this.mLastCaptionLayoutWidth || param1Int2 != this.mLastCaptionLayoutHeight) {
        this.mLastCaptionLayoutWidth = param1Int1;
        this.mLastCaptionLayoutHeight = param1Int2;
        updateTextSize();
      } 
    }
    
    private void updateWidestChar() {
      Paint paint = new Paint();
      paint.setTypeface(this.mCaptionStyle.getTypeface());
      Charset charset = Charset.forName("ISO-8859-1");
      float f = 0.0F;
      for (byte b = 0; b < 'Ā'; b++, f = f2) {
        String str = new String(new byte[] { (byte)b }, charset);
        float f1 = paint.measureText(str);
        float f2 = f;
        if (f < f1) {
          f2 = f1;
          this.mWidestChar = str;
        } 
      } 
      updateTextSize();
    }
    
    private void updateTextSize() {
      if (this.mCCLayout == null)
        return; 
      StringBuilder stringBuilder = new StringBuilder();
      int i = getScreenColumnCount();
      for (byte b = 0; b < i; b++)
        stringBuilder.append(this.mWidestChar); 
      String str = stringBuilder.toString();
      Paint paint = new Paint();
      paint.setTypeface(this.mCaptionStyle.getTypeface());
      float f1 = 0.0F;
      float f2 = 255.0F;
      while (f1 < f2) {
        float f3 = (f1 + f2) / 2.0F;
        paint.setTextSize(f3);
        float f4 = paint.measureText(str);
        if (this.mCCLayout.getWidth() * 0.8F > f4) {
          f1 = 0.01F + f3;
          continue;
        } 
        f2 = f3 - 0.01F;
      } 
      this.mTextSize = f2 = this.mFontScale * f2;
      this.mCCView.setTextSize(f2);
    }
    
    private int getScreenColumnCount() {
      return 42;
    }
    
    public void removeFromCaptionView() {
      Cea708CCWidget.CCLayout cCLayout = this.mCCLayout;
      if (cCLayout != null) {
        cCLayout.removeViewFromSafeTitleArea(this);
        this.mCCLayout.removeOnLayoutChangeListener(this);
        this.mCCLayout = null;
      } 
    }
    
    public void setText(String param1String) {
      updateText(param1String, false);
    }
    
    public void appendText(String param1String) {
      updateText(param1String, true);
    }
    
    public void clearText() {
      this.mBuilder.clear();
      this.mCCView.setText("");
    }
    
    private void updateText(String param1String, boolean param1Boolean) {
      int m;
      if (!param1Boolean)
        this.mBuilder.clear(); 
      if (param1String != null && param1String.length() > 0) {
        int n = this.mBuilder.length();
        this.mBuilder.append(param1String);
        for (CharacterStyle characterStyle : this.mCharacterStyles) {
          SpannableStringBuilder spannableStringBuilder1 = this.mBuilder;
          spannableStringBuilder1.setSpan(characterStyle, n, spannableStringBuilder1.length(), 33);
        } 
      } 
      String[] arrayOfString = TextUtils.split(this.mBuilder.toString(), "\n");
      int j = arrayOfString.length, i = this.mRowLimit;
      j = Math.max(0, j - i + 1);
      i = arrayOfString.length;
      String str = TextUtils.join("\n", Arrays.copyOfRange((Object[])arrayOfString, j, i));
      SpannableStringBuilder spannableStringBuilder = this.mBuilder;
      spannableStringBuilder.delete(0, spannableStringBuilder.length() - str.length());
      j = 0;
      int k = this.mBuilder.length() - 1;
      i = k;
      while (true) {
        m = i;
        if (j <= i) {
          m = i;
          if (this.mBuilder.charAt(j) <= ' ') {
            j++;
            continue;
          } 
        } 
        break;
      } 
      while (m >= j && this.mBuilder.charAt(m) <= ' ')
        m--; 
      if (j == 0 && m == k) {
        this.mCCView.setText((CharSequence)this.mBuilder);
      } else {
        SpannableStringBuilder spannableStringBuilder1 = new SpannableStringBuilder();
        spannableStringBuilder1.append((CharSequence)this.mBuilder);
        if (m < k)
          spannableStringBuilder1.delete(m + 1, k + 1); 
        if (j > 0)
          spannableStringBuilder1.delete(0, j); 
        this.mCCView.setText((CharSequence)spannableStringBuilder1);
      } 
    }
    
    public void setRowLimit(int param1Int) {
      if (param1Int >= 0) {
        this.mRowLimit = param1Int;
        return;
      } 
      throw new IllegalArgumentException("A rowLimit should have a positive number");
    }
  }
  
  class CCView extends SubtitleView {
    private static final CaptioningManager.CaptionStyle DEFAULT_CAPTION_STYLE = CaptioningManager.CaptionStyle.DEFAULT;
    
    public CCView() {
      this((Context)this$0, (AttributeSet)null);
    }
    
    public CCView(AttributeSet param1AttributeSet) {
      this((Context)this$0, param1AttributeSet, 0);
    }
    
    public CCView(AttributeSet param1AttributeSet, int param1Int) {
      this((Context)this$0, param1AttributeSet, param1Int, 0);
    }
    
    public CCView(AttributeSet param1AttributeSet, int param1Int1, int param1Int2) {
      super((Context)this$0, param1AttributeSet, param1Int1, param1Int2);
    }
    
    public void setCaptionStyle(CaptioningManager.CaptionStyle param1CaptionStyle) {
      int i;
      if (param1CaptionStyle.hasForegroundColor()) {
        i = param1CaptionStyle.foregroundColor;
      } else {
        i = DEFAULT_CAPTION_STYLE.foregroundColor;
      } 
      setForegroundColor(i);
      if (param1CaptionStyle.hasBackgroundColor()) {
        i = param1CaptionStyle.backgroundColor;
      } else {
        i = DEFAULT_CAPTION_STYLE.backgroundColor;
      } 
      setBackgroundColor(i);
      if (param1CaptionStyle.hasEdgeType()) {
        i = param1CaptionStyle.edgeType;
      } else {
        i = DEFAULT_CAPTION_STYLE.edgeType;
      } 
      setEdgeType(i);
      if (param1CaptionStyle.hasEdgeColor()) {
        i = param1CaptionStyle.edgeColor;
      } else {
        i = DEFAULT_CAPTION_STYLE.edgeColor;
      } 
      setEdgeColor(i);
      setTypeface(param1CaptionStyle.getTypeface());
    }
  }
}
