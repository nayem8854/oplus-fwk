package android.media;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

final class TtmlUtils {
  public static final String ATTR_BEGIN = "begin";
  
  public static final String ATTR_DURATION = "dur";
  
  public static final String ATTR_END = "end";
  
  private static final Pattern CLOCK_TIME = Pattern.compile("^([0-9][0-9]+):([0-9][0-9]):([0-9][0-9])(?:(\\.[0-9]+)|:([0-9][0-9])(?:\\.([0-9]+))?)?$");
  
  public static final long INVALID_TIMESTAMP = 9223372036854775807L;
  
  private static final Pattern OFFSET_TIME = Pattern.compile("^([0-9]+(?:\\.[0-9]+)?)(h|m|s|ms|f|t)$");
  
  public static final String PCDATA = "#pcdata";
  
  public static final String TAG_BODY = "body";
  
  public static final String TAG_BR = "br";
  
  public static final String TAG_DIV = "div";
  
  public static final String TAG_HEAD = "head";
  
  public static final String TAG_LAYOUT = "layout";
  
  public static final String TAG_METADATA = "metadata";
  
  public static final String TAG_P = "p";
  
  public static final String TAG_REGION = "region";
  
  public static final String TAG_SMPTE_DATA = "smpte:data";
  
  public static final String TAG_SMPTE_IMAGE = "smpte:image";
  
  public static final String TAG_SMPTE_INFORMATION = "smpte:information";
  
  public static final String TAG_SPAN = "span";
  
  public static final String TAG_STYLE = "style";
  
  public static final String TAG_STYLING = "styling";
  
  public static final String TAG_TT = "tt";
  
  public static long parseTimeExpression(String paramString, int paramInt1, int paramInt2, int paramInt3) throws NumberFormatException {
    Matcher matcher = CLOCK_TIME.matcher(paramString);
    if (matcher.matches()) {
      double d4, d5, d6;
      paramString = matcher.group(1);
      double d1 = (Long.parseLong(paramString) * 3600L);
      paramString = matcher.group(2);
      double d2 = (Long.parseLong(paramString) * 60L);
      paramString = matcher.group(3);
      double d3 = Long.parseLong(paramString);
      paramString = matcher.group(4);
      if (paramString != null) {
        d4 = Double.parseDouble(paramString);
      } else {
        d4 = 0.0D;
      } 
      paramString = matcher.group(5);
      if (paramString != null) {
        d5 = Long.parseLong(paramString) / paramInt1;
      } else {
        d5 = 0.0D;
      } 
      paramString = matcher.group(6);
      if (paramString != null) {
        d6 = Long.parseLong(paramString) / paramInt2 / paramInt1;
      } else {
        d6 = 0.0D;
      } 
      return (long)(1000.0D * (d1 + d2 + d3 + d4 + d5 + d6));
    } 
    matcher = OFFSET_TIME.matcher(paramString);
    if (matcher.matches()) {
      paramString = matcher.group(1);
      double d = Double.parseDouble(paramString);
      paramString = matcher.group(2);
      if (paramString.equals("h")) {
        d *= 3.6E9D;
      } else if (paramString.equals("m")) {
        d *= 6.0E7D;
      } else if (paramString.equals("s")) {
        d *= 1000000.0D;
      } else if (paramString.equals("ms")) {
        d *= 1000.0D;
      } else if (paramString.equals("f")) {
        d = d / paramInt1 * 1000000.0D;
      } else if (paramString.equals("t")) {
        d = d / paramInt3 * 1000000.0D;
      } 
      return (long)d;
    } 
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("Malformed time expression : ");
    stringBuilder.append(paramString);
    throw new NumberFormatException(stringBuilder.toString());
  }
  
  public static String applyDefaultSpacePolicy(String paramString) {
    return applySpacePolicy(paramString, true);
  }
  
  public static String applySpacePolicy(String paramString, boolean paramBoolean) {
    paramString = paramString.replaceAll("\r\n", "\n");
    paramString = paramString.replaceAll(" *\n *", "\n");
    if (paramBoolean)
      paramString = paramString.replaceAll("\n", " "); 
    paramString = paramString.replaceAll("[ \t\\x0B\f\r]+", " ");
    return paramString;
  }
  
  public static String extractText(TtmlNode paramTtmlNode, long paramLong1, long paramLong2) {
    StringBuilder stringBuilder = new StringBuilder();
    extractText(paramTtmlNode, paramLong1, paramLong2, stringBuilder, false);
    return stringBuilder.toString().replaceAll("\n$", "");
  }
  
  private static void extractText(TtmlNode paramTtmlNode, long paramLong1, long paramLong2, StringBuilder paramStringBuilder, boolean paramBoolean) {
    if (paramTtmlNode.mName.equals("#pcdata") && paramBoolean) {
      paramStringBuilder.append(paramTtmlNode.mText);
    } else if (paramTtmlNode.mName.equals("br") && paramBoolean) {
      paramStringBuilder.append("\n");
    } else if (!paramTtmlNode.mName.equals("metadata")) {
      if (paramTtmlNode.isActive(paramLong1, paramLong2)) {
        boolean bool = paramTtmlNode.mName.equals("p");
        int i = paramStringBuilder.length();
        for (byte b = 0; b < paramTtmlNode.mChildren.size(); b++) {
          boolean bool1;
          TtmlNode ttmlNode = paramTtmlNode.mChildren.get(b);
          if (bool || paramBoolean) {
            bool1 = true;
          } else {
            bool1 = false;
          } 
          extractText(ttmlNode, paramLong1, paramLong2, paramStringBuilder, bool1);
        } 
        if (bool && i != paramStringBuilder.length())
          paramStringBuilder.append("\n"); 
      } 
    } 
  }
  
  public static String extractTtmlFragment(TtmlNode paramTtmlNode, long paramLong1, long paramLong2) {
    StringBuilder stringBuilder = new StringBuilder();
    extractTtmlFragment(paramTtmlNode, paramLong1, paramLong2, stringBuilder);
    return stringBuilder.toString();
  }
  
  private static void extractTtmlFragment(TtmlNode paramTtmlNode, long paramLong1, long paramLong2, StringBuilder paramStringBuilder) {
    if (paramTtmlNode.mName.equals("#pcdata")) {
      paramStringBuilder.append(paramTtmlNode.mText);
    } else if (paramTtmlNode.mName.equals("br")) {
      paramStringBuilder.append("<br/>");
    } else if (paramTtmlNode.isActive(paramLong1, paramLong2)) {
      paramStringBuilder.append("<");
      paramStringBuilder.append(paramTtmlNode.mName);
      paramStringBuilder.append(paramTtmlNode.mAttributes);
      paramStringBuilder.append(">");
      for (byte b = 0; b < paramTtmlNode.mChildren.size(); b++)
        extractTtmlFragment(paramTtmlNode.mChildren.get(b), paramLong1, paramLong2, paramStringBuilder); 
      paramStringBuilder.append("</");
      paramStringBuilder.append(paramTtmlNode.mName);
      paramStringBuilder.append(">");
    } 
  }
}
